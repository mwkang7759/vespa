#pragma once
#include "vector_types.h"
#include "device_launch_parameters.h"

typedef struct CUstream_st* cudaStream_t;
#ifndef TENSORRT_YOLOV4_RESIZE_H
#define TENSORRT_YOLOV4_RESIZE_H
typedef unsigned char uchar;
int resizeAndNorm(void * p, float *d, int w, int h, int in_w, int in_h, cudaStream_t stream);
int cudaDrawLine(uchar4* output, int out_width, int out_height, int x0, int y0, int x1, int y1, cudaStream_t stream);
#endif //TENSORRT_YOLOV4_RESIZE_H