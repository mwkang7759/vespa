#pragma once

//#if defined(EXP_SLD_POSE_ESTIMATOR_LIB)
//#define EXP_SLD_POSE_ESTIMATOR_CLS	__declspec(dllexport)
//#else
//#define EXP_SLD_POSE_ESTIMATOR_CLS	__declspec(dllimport)
//#endif

#include <DeepBase.h>
#include <opencv2/core.hpp>

namespace deep4d
{
	namespace pose
	{
		class estimator : public DeepBase
		{
			class core;
		public:
			typedef struct _context_t
			{
				int32_t width;
				int32_t height;
				std::string enginePath;
				_context_t(void)
					: width(-1)
					, height(-1)
					, enginePath("")
				{}

				~_context_t(void)
				{}
			} context_t;

			estimator(void);
			virtual ~estimator(void);

			int32_t initialize(deep4d::pose::estimator::context_t* ctx);
			int32_t release(void);

			int32_t estimate(uint8_t* input, int32_t inputStride, uint8_t* srcBBox, int32_t bboxSize, uint8_t** output, int32_t& outputStride);
			int32_t estimate(uint8_t* input, int32_t inputStride, const std::array<int, 4>& arrRoi, std::vector<std::array<int, 36>>& vecJointPoint);

			int32_t draw(const std::vector<std::array<int, 36>>& _vecJointPoint, cv::Mat& _mat);
		private:
			deep4d::pose::estimator::core* _core;
		};
	};
};


