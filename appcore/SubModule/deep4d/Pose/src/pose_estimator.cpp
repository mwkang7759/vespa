﻿#include "pose_estimator.h"


#define MAX_WORKSPACE (1 << 30) // 1G workspace memory
//#define _Only_Pose_Esimation
static Logger gLogger;

namespace deep4d
{
	namespace pose
	{

		estimator::core::core(deep4d::pose::estimator* front)
			: _front(front)
			, _ctx(NULL)
			, batchSize(-1)
			, inputHeightSize(-1)
			, inputWidthSize(-1)
			, numChannels(-1)
			, cudaFrame(NULL)
			, cudaStream(NULL)
		{
		}

		estimator::core::~core(void)
		{
			cudaStreamSynchronize(cudaStream);
			cudaStreamDestroy(cudaStream);

			for (int i = 0; i < cudaBuffers.size(); ++i)
			{
				if (cudaBuffers[i])
					cudaFree(cudaBuffers[i]);
			}

			if (input_gpu)
				cudaFree(input_gpu);
		}

		DL_RESULT estimator::core::initialize(deep4d::pose::estimator::context_t* ctx)
		{
			_ctx = ctx;
			LOG_I("[Pose] Loading OpenPose Inference Engine ... ");

			cudaSetDevice(0);
			std::fstream file;

			confThreshold = 0.3f;
			nmsThreshold = 0.3f;
			file.open(_ctx->enginePath, std::ios::binary | std::ios::in);
			if (!file.is_open())
			{
				LOG_E("[Pose] failed to read engine file :(%s)", _ctx->enginePath.c_str());
				return DL_RESULT::ERR_INIT_ESTIMATE_FAILED_READ_ENGINE_FILE;
			}
			file.seekg(0, std::ios::end);
			int length = file.tellg();
			file.seekg(0, std::ios::beg);
			std::unique_ptr<char[]> data(new char[length]);
			file.read(data.get(), length);

			file.close();

			auto runtime = UniquePtr<nvinfer1::IRuntime>(nvinfer1::createInferRuntime(gLogger));
			assert(runtime != nullptr);

			//std::string msg("tes............");
			//gLogger.log(nvinfer1::ILogger::Severity::kERROR, msg.c_str());

			engine = UniquePtr<nvinfer1::ICudaEngine>(runtime->deserializeCudaEngine(data.get(), length, nullptr));
			assert(engine != nullptr);

			LOG_I("[Pose] Done");

			context = UniquePtr<nvinfer1::IExecutionContext>(engine->createExecutionContext());
			assert(context);

			const int numBindingPerProfile = engine->getNbBindings() / engine->getNbOptimizationProfiles();
			LOG_I("[Pose] Number of binding profiles: (%d)", numBindingPerProfile);

			initEngine();
			Openpose openpose(outputDims[0]);
			m_openpose = openpose;
			return DL_RESULT::SUCCESS;
		}

		DL_RESULT estimator::core::release(void)
		{
			return DL_RESULT::SUCCESS;
		}

		DL_RESULT estimator::core::preProc(const cv::cuda::GpuMat& _srcGpuImg)
		{
			cv::cuda::GpuMat resizeImg, normImg;
			cv::cuda::resize(_srcGpuImg, resizeImg, cv::Size(inputWidthSize, inputHeightSize));
			resizeImg.convertTo(normImg, CV_32FC3, 1.0f / 255.0f);

			std::vector<cv::cuda::GpuMat> inputChannels{
				cv::cuda::GpuMat(normImg.rows, normImg.cols, CV_32F, &input_gpu[0]),
				cv::cuda::GpuMat(normImg.rows, normImg.cols, CV_32F, &input_gpu[inputWidthSize * inputHeightSize]),
				cv::cuda::GpuMat(normImg.rows, normImg.cols, CV_32F, &input_gpu[inputWidthSize * inputHeightSize * 2])
			};
			cv::cuda::split(normImg, inputChannels);

			cudaBuffers.data()[0] = input_gpu;

			return DL_RESULT::SUCCESS;
		}

		DL_RESULT estimator::core::postProcCudaMemcpy()
		{
			cudaMemcpy(cpuCmapBuffer.data(), (float*)cudaBuffers[1], cpuCmapBuffer.size() * sizeof(float), cudaMemcpyDeviceToHost);
			cudaMemcpy(cpuPafBuffer.data(), (float*)cudaBuffers[2], cpuPafBuffer.size() * sizeof(float), cudaMemcpyDeviceToHost);
			
			return DL_RESULT::SUCCESS;
		}

		DL_RESULT estimator::core::postProc(std::vector<std::array<int, 36>>& jointPoint, const cv::Size& frameSize)
		{
			postProcCudaMemcpy();
			m_openpose.detect(cpuCmapBuffer, cpuPafBuffer, frameSize.width, frameSize.height, jointPoint);

			return DL_RESULT::SUCCESS;
		}

		DL_RESULT estimator::core::postProc(std::vector<std::array<int, 36>>& jointPoint, const cv::Size& frameSize, std::array<int, 2> arrRoiXY)
		{
			postProcCudaMemcpy();
			m_openpose.detect(cpuCmapBuffer, cpuPafBuffer, frameSize.width, frameSize.height, arrRoiXY, jointPoint);

			return DL_RESULT::SUCCESS;
		}

		DL_RESULT estimator::core::estimate(uint8_t* input, int32_t inputStride, const std::array<int, 4>& arrRoi, std::vector<std::array<int, 36>>& vecJointPoint)
		{
			cv::cuda::GpuMat srcGpuImg = cv::cuda::GpuMat(_ctx->height, _ctx->width, CV_8UC3, input, inputStride);
			
#ifdef _Only_Pose_Esimation
			preProc(srcGpuImg);

			// Inference
			context->enqueue(1, cudaBuffers.data(), cudaStream, nullptr);

			postProc(vecJointPoint, srcGpuImg.size());
#else
			// ROI 영역 Inference
			cv::cuda::GpuMat cropimg = (cv::cuda::GpuMat(srcGpuImg, cv::Rect({ arrRoi[0], arrRoi[1], arrRoi[2], arrRoi[3] })));
			preProc(cropimg);

			bool ret = context->enqueue(1, cudaBuffers.data(), cudaStream, nullptr);
			if (!ret) {
				//gLogger.log(nvinfer1::ILogger::Severity::kERROR, "");
				return DL_RESULT::UNKNOWN;
			}

			postProc(vecJointPoint, cropimg.size(), std::array<int, 2>({ arrRoi[0], arrRoi[1] }));
#endif
			return DL_RESULT::SUCCESS;
		}

		DL_RESULT estimator::core::draw(const std::vector<std::array<int, 36>>& _vecJointPoint, cv::Mat& _mat)
		{
			m_openpose.draw(_vecJointPoint, _mat);

			return DL_RESULT::SUCCESS;
		}

		DL_RESULT estimator::core::estimate(uint8_t* input, int32_t inputStride, uint8_t* srcBBox, int32_t bboxSize, uint8_t** output, int32_t& outputStride)
		{
			cv::cuda::GpuMat img = cv::cuda::GpuMat(_ctx->height, _ctx->width, CV_8UC4, input, inputStride);
			cv::cuda::GpuMat img2, img3, resizeImg, normImg;;
			std::vector<cv::Rect> bboxes;

			// BBox Check ( Object Detection result )
			//if(bboxSize == 0)
			//	return solids::lib::video::nvidia::pose::estimator::err_code_t::success;

			// Do not use BBox
#ifdef _Only_Pose_Esimation
// TODO: GPUMat Crop Detected Image
			cv::Mat mImg1;

			// Pre-Processing 
#ifdef backup
			cv::cuda::cvtColor(img, img2, cv::COLOR_BGRA2RGB);
			img2.download(mImg1);
			cudaMemcpy((void*)img2.ptr(), mImg1.data, mImg1.step[0] * mImg1.rows, cudaMemcpyHostToDevice);
			resizeAndNorm((void*)img2.ptr(), (float*)cudaBuffers[0], _ctx->width, _ctx->height, inputWidthSize, inputHeightSize, cudaStream);
#else
			cv::cuda::resize(img, resizeImg, cv::Size(inputWidthSize, inputHeightSize));
			resizeImg.convertTo(normImg, CV_32FC3, 1.0f / 255.0f);
			cv::cuda::cvtColor(normImg, normImg, cv::COLOR_BGRA2RGB);

			std::vector<cv::cuda::GpuMat> inputChannels{
				cv::cuda::GpuMat(normImg.rows, normImg.cols, CV_32F, cudaBuffers.data()[0]),
				cv::cuda::GpuMat(normImg.rows, normImg.cols, CV_32F, (void**)cudaBuffers.data()[0] + inputWidthSize * inputHeightSize),
				cv::cuda::GpuMat(normImg.rows, normImg.cols, CV_32F, (void**)cudaBuffers.data()[0] + inputWidthSize * inputHeightSize * 2)
			};
			cv::cuda::split(normImg, inputChannels);

#endif		
			// Inference
			context->enqueue(1, cudaBuffers.data(), cudaStream, nullptr);

			// Inference ouput device to host
			cudaMemcpy(cpuCmapBuffer.data(), (float*)cudaBuffers[1], cpuCmapBuffer.size() * sizeof(float), cudaMemcpyDeviceToHost);
			cudaMemcpy(cpuPafBuffer.data(), (float*)cudaBuffers[2], cpuPafBuffer.size() * sizeof(float), cudaMemcpyDeviceToHost);

			// Post-Processing
#ifdef _drawSkeleton
			img.download(mImg1);
			m_openpose.detect(cpuCmapBuffer, cpuPafBuffer, mImg1);
			img.upload(mImg1);
#else
//std::vector<std::array<int, 36>> jointPoint;
//m_openpose.detect(cpuCmapBuffer, cpuPafBuffer, img.cols, img.rows, jointPoint);
			img.download(mImg1);
			m_openpose.detect(cpuCmapBuffer, cpuPafBuffer, mImg1);
			img.upload(mImg1);
#endif


#else
			for (int i = 0; i < bboxSize / sizeof(cv::Rect); ++i)
			{
				// get bbox
				cv::Rect tmpRect;
				::memmove(&tmpRect, srcBBox + (i * sizeof(cv::Rect)), sizeof(cv::Rect));
				
				// GPUMat Crop Detected Image
				if (tmpRect.x < 0) tmpRect.x = 0;
				if (tmpRect.y < 0) tmpRect.y = 0;
				tmpRect.width = (tmpRect.width <= _ctx->width) ? tmpRect.width : _ctx->width;
				tmpRect.height = (tmpRect.height <= _ctx->height) ? tmpRect.height : _ctx->height;
				cv::Rect roi{ tmpRect.x, tmpRect.y, tmpRect.width - tmpRect.x, tmpRect.height - tmpRect.y };

				
				cv::Mat mImg, mImg1, mImg1_2, mImg2, mImg3;
				//cv::cuda::cvtColor(img, img2, cv::COLOR_RGBA2RGB);
				cv::cuda::cvtColor(img, img2, cv::COLOR_BGRA2RGB);

				std::chrono::system_clock::time_point st = std::chrono::system_clock::now();
				img2.download(mImg1);
				cv::cuda::GpuMat cropimg3 = (cv::cuda::GpuMat(img2, roi)).clone();
				cropimg3.download(mImg2);
				
				int elapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - st).count();
				LOG_D("[Pose] download elapsed Time : (%d)", elapsedTime);

				st = std::chrono::system_clock::now();
				cudaMemcpy((void*)cropimg3.ptr(), mImg2.data, mImg2.step[0] * mImg2.rows, cudaMemcpyHostToDevice);

				context->enqueue(1, cudaBuffers.data(), cudaStream, nullptr);

				cudaMemcpy(cpuCmapBuffer.data(), (float*)cudaBuffers[1], cpuCmapBuffer.size() * sizeof(float), cudaMemcpyDeviceToHost);
				cudaMemcpy(cpuPafBuffer.data(), (float*)cudaBuffers[2], cpuPafBuffer.size() * sizeof(float), cudaMemcpyDeviceToHost);
				
				elapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - st).count();
				LOG_D("[Pose] cudamemcpy & inference elapsed Time : (%d)", elapsedTime);

				st = std::chrono::system_clock::now();
				m_openpose.detect(cpuCmapBuffer, cpuPafBuffer, mImg2);
				elapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - st).count();
				LOG_D("[Pose] detect elapsed Time : (%d)", elapsedTime);

				st = std::chrono::system_clock::now();
				// crop image patch to original image
				mImg2.copyTo(mImg1(roi));
				img2.upload(mImg1);

				elapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - st).count();
				LOG_D("[Pose] upload elapsed Time : (%d)", elapsedTime);
				cv::cuda::cvtColor(img2, img, cv::COLOR_RGB2BGRA);

			}
#endif

			* output = (uint8_t*)img.ptr();
			outputStride = inputStride;
			return DL_RESULT::SUCCESS;
		}



		std::size_t estimator::core::getSizeByDim(const nvinfer1::Dims& dims)
		{
			std::size_t size = 1;
			for (std::size_t i = 0; i < dims.nbDims; ++i)
			{
				size *= dims.d[i];
			}
			return size;
		}

		void estimator::core::initEngine()
		{
			cudaBuffers.resize(engine->getNbBindings());
			for (size_t i = 0; i < engine->getNbBindings(); ++i)
			{
				auto bindingSize = getSizeByDim(engine->getBindingDimensions(i)) * 1 * sizeof(float);
				cudaMalloc(&cudaBuffers[i], bindingSize);
				if (engine->bindingIsInput(i))
				{
					inputDims.emplace_back(engine->getBindingDimensions(i));
				}
				else
				{
					outputDims.emplace_back(engine->getBindingDimensions(i));
				}
				LOG_I("[Pose] Binding Name: (%s)", engine->getBindingName(i));
			}
			if (inputDims.empty() || outputDims.empty())
			{
				LOG_E("[Pose] Expect at least one input and one output for network");
			}

			batchSize = inputDims[0].d[0];
			numChannels = inputDims[0].d[1];
			inputHeightSize = inputDims[0].d[2];
			inputWidthSize = inputDims[0].d[3];

			LOG_I("[Pose] output[0] -> cmap");
			LOG_I("[Pose] outputDims[0]: ");
			std::size_t size = 1;
			for (std::size_t i = 0; i < outputDims[0].nbDims; ++i)
			{
				LOG_I("[Pose] out[0]: (%d)", outputDims[0].d[i]);
				size *= outputDims[0].d[i];
			}
			LOG_I("[Pose] out[0].size: (%d)", size);

			LOG_I("[Pose] output[1] -> paf");
			LOG_I("[Pose] outputDims[1]: ");
			size = 1;
			for (std::size_t i = 0; i < outputDims[1].nbDims; ++i)
			{
				LOG_I("[Pose] out[1]: (%d)", outputDims[1].d[i]);
				size *= outputDims[1].d[i];
			}
			//std::cout << "out[1].size: " << size << std::endl;
			LOG_I("[Pose] out[1].size: (%d)", size);

			cpuCmapBuffer.resize(getSizeByDim(outputDims[0]) * batchSize);
			cpuPafBuffer.resize(getSizeByDim(outputDims[1]) * batchSize);

			LOG_I("[Pose] Model input shape: batchSize(%d), numChannels(%d), inputWidthSize(%d), inputHeightSize(%d)", 
				batchSize, numChannels, inputWidthSize, inputHeightSize);

			cudaFrame = safeCudaMalloc(4096 * 4096 * 3 * sizeof(uchar)); // max input image shape
			auto res = cudaMalloc((void**)&input_gpu, inputWidthSize * inputHeightSize * 3 * sizeof(float));
			cudaStreamCreate(&cudaStream);
		}

	};
};