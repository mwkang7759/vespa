#include "sld_pose_estimator.h"
#include "pose_estimator.h"

namespace deep4d
{
	namespace pose
	{

		estimator::estimator(void)
		{
			_core = new deep4d::pose::estimator::core(this);
		}

		estimator::~estimator(void)
		{
			if (_core)
				delete _core;
			_core = NULL;
		}

		int32_t estimator::initialize(deep4d::pose::estimator::context_t* ctx)
		{
			return static_cast<int>(_core->initialize(ctx));
		}

		int32_t estimator::release(void)
		{
			return static_cast<int>(_core->release());
		}

		int32_t estimator::estimate(uint8_t* input, int32_t inputStride, uint8_t* srcBBox, int32_t bboxSize, uint8_t** output, int32_t& outputStride)
		{
			return static_cast<int>(_core->estimate(input, inputStride, srcBBox, bboxSize, output, outputStride));
		}

		int32_t estimator::estimate(uint8_t* input, int32_t inputStride, const std::array<int, 4>& arrRoi, std::vector<std::array<int, 36>>& vecJointPoint)
		{
			return static_cast<int>(_core->estimate(input, inputStride, arrRoi, vecJointPoint));
		}

		int32_t estimator::draw(const std::vector<std::array<int, 36>>& _vecJointPoint, cv::Mat& _mat)
		{
			return static_cast<int>(_core->draw(_vecJointPoint, _mat));
		}

	};
};

