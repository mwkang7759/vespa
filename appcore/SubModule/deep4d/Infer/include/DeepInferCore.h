#pragma once

#include "DeepInfer.h"

#include <sld_object_detector.h>
#include <sld_pose_estimator.h>

#include <opencv2/core/cuda.hpp>
#include <opencv2/cudaimgproc.hpp>

#include "SystemAPI.h"
#include "TraceAPI.h"

class DeepInfer::Core
{
public:
	Core(void);
	virtual ~Core(void);
private:
	deep4d::object::detector* _detector;
	deep4d::object::detector::context_t _detector_ctx;

	deep4d::pose::estimator* _estimator;
	deep4d::pose::estimator::context_t _estimator_ctx;

	BOOL _isInitialized;

	//ESMNvdec m_dec;
public:
	
public:
	BOOL		isInitialized();
	DL_RESULT	initialize(context_t* ctx);
	DL_RESULT	release();
	DL_RESULT	doInference(unsigned char* input, size_t inputPitch, unsigned char** output, size_t& outputSize);
	DL_RESULT	doInference(unsigned char* input, size_t inputPitch, const int& _width, const int& _height, unsigned char** output, size_t& outputSize);
	DL_RESULT	doInference(unsigned char* input, size_t inputPitch, VPdInferenceData_t& vpdInferenceData, size_t& outputSize);
	DL_RESULT	doInference(unsigned char* input, size_t inputPitch, OUTPUT_PARAM_T& inferData);
	
private:
	cv::Rect findROI(std::vector<BBox_t>& srcBBox, const std::vector<int>& classId, const std::vector<float>& prob,
		std::vector<cv::Rect>& _vecRoi, std::vector<BBoxData_t>& _vecBBoxData, const int& _width, const int& _height);
	cv::Rect paddingROI(const cv::Rect& _roi, const int& _width, const int& _height);

	DL_RESULT makeVPdMetaData(std::vector<std::array<int, 36>>& _vecJointPoint, cv::Rect& _roi,
		std::vector<cv::Rect>& _vecBBox, Detection_Data_t& _detectionData, std::vector<PoseData_t>& _vecPoseData);

	DL_RESULT initDetectInferenceEngine(const int& _width, const int& _height, const std::string& _path);
	DL_RESULT initEstimateInferenceEngine(const int& _width, const int& _height, const std::string& _path);
	
	int			calcVPdInferenceDataSize(const VPdInferenceData_t& _data);
	int			convertFrPoint(const std::vector<std::array<int, 36>>& _vecJoint, std::vector<std::array<FrPoint, 18>>& _vecFrPoint);
	int			convertVecArrInt(const std::vector<std::array<FrPoint, 18>>& _vecFrPoint, std::vector<std::array<int, 36>>& _vecJoint);
	
public:
	DL_RESULT poseInitialize(context_t* ctx);
	DL_RESULT doPoseInference(unsigned char* input, size_t inputPitch, const int& _width, const int& _height, std::vector<std::array<FrPoint, 18>>& vecJointPoint);
	BOOL	  isPoseInitialized();

	int			drawPose(const std::vector<std::array<FrPoint, 18>>& _vecFrPoint, cv::Mat& _img);
	// only detect inference
	DL_RESULT doDetectInference(unsigned char* input, size_t inputPitch, const int& _width, const int& _height, Detection_Data_t& detectData);
};

