﻿/*****************************************************************************
*                                                                            *
*					Deep Learning Inference Library							 *
*                                                                            *
*   Copyright (c) 2004 by 4dreplay, Incoporated. All Rights Reserved.        *
******************************************************************************

	File Name       : DeepInfer
	Author(s)       : Gyeong Il Shin
	Created         : 15 Oct 2021

	Description     : 각종 학습된 Network를 Inference 용도로 사용할 수 있도록 작성한 상위 API
	Notes           :

==============================================================================*/

#pragma once

//#if defined(EXPORT_DEEP_INFER)
//#define DEEP_INFER_CLASS __declspec(dllexport)
//#else
//#define DEEP_INFER_CLASS __declspec(dllimport)
//#endif

#include <fstream>
#include <DeepBase.h>
#include <opencv2/core.hpp>

class DeepInfer : public DeepBase
{
public:
	class Core;

public:
	DeepInfer(void);
	virtual ~DeepInfer(void);

public:
	typedef struct _context_t
	{
		int32_t width;
		int32_t height;
		std::string detectPath;
		std::string estimatePath;
		_context_t(void)
			: width(-1)
			, height(-1)
			, detectPath("")
			, estimatePath("")
		{}

		~_context_t(void)
		{}
	}context_t;

	typedef struct _OUTPUT_PARAM_T
	{
		int size;
		char* data;
		
		_OUTPUT_PARAM_T(void)
			: size(0), data(nullptr)
		{
		}
	} OUTPUT_PARAM_T;

public:
	int32_t				initialize(context_t* ctx);
	int32_t				release(void);
	bool				isInitialized(void);
	int32_t				doInference(unsigned char* input, size_t inputPitch, unsigned char** output, size_t& outputSize);
	int32_t				doInference(unsigned char* input, size_t inputPitch, const int& _width, const int& _height, unsigned char** output, size_t& outputSize);
	int32_t				doInference(unsigned char* input, size_t inputPitch, VPdInferenceData_t& vpdInferenceData, size_t& outputSize);
	int32_t				doInference(unsigned char* input, size_t inputPitch, OUTPUT_PARAM_T& inferData);
	
	int32_t				drawPose(const std::vector<std::array<FrPoint, 18>>& _vecFrPoint, cv::Mat& _img);
private:
	DeepInfer::Core*	_core;
	std::vector<std::string> _classesName;

	cv::Scalar ConvObjToColor(int obj_id);

public:
	/* datasetMaker 관련된 API */
	int32_t poseInitialize(context_t* ctx);
	int32_t doPoseInference(unsigned char* input, size_t inputPitch, const int& _width, const int& _height, std::vector<std::array<FrPoint, 18>>& vecJointPoint);
	bool	isPoseInitialize(void);

	// only detect inference
	int32_t doDetectInference(unsigned char* input, size_t inputPitch, const int& _width, const int& _height, Detection_Data_t& detectData);
	void drawDetect(Detection_Data_t& detectData, cv::Mat& img);
};

