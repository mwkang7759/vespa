#include "DeepInfer.h"
#include "DeepInferCore.h"


DeepInfer::DeepInfer(void)
{
	_core = new DeepInfer::Core();
}

DeepInfer::~DeepInfer(void)
{
	if (_core)
	{
		if (_core->isInitialized())
			_core->release();
		delete _core;
		_core = NULL;
	}
}

int32_t DeepInfer::initialize(context_t* ctx)
{
	std::string path = ctx->detectPath;
	std::string classFilePath="";
	size_t pos = path.rfind("\\");
	if (pos != std::string::npos) {
		classFilePath = path.substr(0, pos) + "\\" + "coco.names";
		std::string line;
		std::ifstream ifs(classFilePath.c_str());
		while (std::getline(ifs, line)) _classesName.push_back(line);
		ifs.close();
	}
		
	return static_cast<int32_t>(_core->initialize(ctx));
}

int32_t DeepInfer::poseInitialize(context_t* ctx)
{
	return static_cast<int32_t>(_core->poseInitialize(ctx));
}

bool DeepInfer::isInitialized(void)
{
	return _core->isInitialized();
}

bool DeepInfer::isPoseInitialize(void)
{
	return _core->isPoseInitialized();
}

int32_t DeepInfer::release(void)
{
	return static_cast<int32_t>(_core->release());
}

int32_t DeepInfer::doInference(unsigned char* input, size_t inputPitch, unsigned char** output, size_t& outputSize)
{
	return static_cast<int32_t>(_core->doInference(input, inputPitch, output,outputSize));
}

int32_t DeepInfer::doInference(unsigned char* input, size_t inputPitch, const int& _width, const int& _height, unsigned char** output, size_t& outputSize)
{
	return static_cast<int32_t>(_core->doInference(input, inputPitch, _width, _height, output, outputSize));
}

int32_t DeepInfer::doInference(unsigned char* input, size_t inputPitch, VPdInferenceData_t& vpdInferenceData, size_t& outputSize)
{
	return static_cast<int32_t>(_core->doInference(input, inputPitch, vpdInferenceData, outputSize));
}

int32_t DeepInfer::doInference(unsigned char* input, size_t inputPitch, OUTPUT_PARAM_T& inferData)
{
	return static_cast<int32_t>(_core->doInference(input, inputPitch, inferData));
}

int32_t DeepInfer::drawPose(const std::vector<std::array<FrPoint, 18>>& _vecFrPoint, cv::Mat& _img)
{
	return static_cast<int32_t>(_core->drawPose(_vecFrPoint, _img));
}

int32_t DeepInfer::doPoseInference(unsigned char* input, size_t inputPitch, const int& _width, const int& _height, std::vector<std::array<FrPoint, 18>>& vecJointPoint)
{
	return static_cast<int32_t>(_core->doPoseInference(input, inputPitch, _width, _height, vecJointPoint));
}

int32_t DeepInfer::doDetectInference(unsigned char* input, size_t inputPitch, const int& _width, const int& _height, Detection_Data_t& detectData)
{
	return static_cast<int32_t>(_core->doDetectInference(input, inputPitch, _width, _height, detectData));
}


cv::Scalar DeepInfer::ConvObjToColor(int obj_id) {
	int const colors[6][3] = { { 1,0,1 },{ 0,0,1 },{ 0,1,1 },{ 0,1,0 },{ 1,1,0 },{ 1,0,0 } };
	int const offset = obj_id * 123457 % 6;
	int const color_scale = 150 + (obj_id * 123457) % 100;
	cv::Scalar color(colors[offset][0], colors[offset][1], colors[offset][2]);
	color *= color_scale;
	return color;
}

void DeepInfer::drawDetect(Detection_Data_t& detectData, cv::Mat& img) {
	int const colors[6][3] = { { 1,0,1 },{ 0,0,1 },{ 0,1,1 },{ 0,1,0 },{ 1,1,0 },{ 1,0,0 } };
	for (int i = 0; i < detectData.bboxes.size(); ++i) {
		DeepBase::BBox_t bbox = detectData.bboxes[i];
		cv::Rect rcBox(bbox.x, bbox.y, bbox.w, bbox.h);

		//std::cout << "idx : " << idx << ", box info : " << box.x << ", " << box.y << ", " << box.width << ", " << box.height << ", confidence : " << scores[idx]
		//    << ", label : " << classes[idx] << std::endl;

		cv::Scalar color = ConvObjToColor(detectData.classId[i]);

		std::string outLabel = _classesName[detectData.classId[i]] + " : " + std::to_string(detectData.prob[i]);
		cv::rectangle(img, rcBox, color, 2);
		cv::putText(img, outLabel, cv::Point(rcBox.x, rcBox.y), cv::FONT_HERSHEY_SIMPLEX, 0.50, cv::Scalar(255, 255, 51), 2);
	}
}


