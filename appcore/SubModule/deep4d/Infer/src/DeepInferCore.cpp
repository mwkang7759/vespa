﻿#include "DeepInferCore.h"

DeepInfer::Core::Core(void)
	: _detector(nullptr)
	, _estimator(nullptr)
	, _isInitialized(FALSE)
{
}

DeepInfer::Core::~Core(void)
{
}

DL_RESULT DeepInfer::Core::initialize(context_t* ctx)
{
	DL_RESULT ret = DL_RESULT::UNKNOWN;
	ret = initDetectInferenceEngine(ctx->width, ctx->height, ctx->detectPath);
	if (ret != DL_RESULT::SUCCESS)	return ret;

	ret = initEstimateInferenceEngine(ctx->width, ctx->height, ctx->estimatePath);
	if (ret != DL_RESULT::SUCCESS)	return ret;

	_isInitialized = true;
	return DL_RESULT::SUCCESS;
}

DL_RESULT DeepInfer::Core::poseInitialize(context_t* ctx)
{
	DL_RESULT ret = DL_RESULT::UNKNOWN;

	ret = initEstimateInferenceEngine(ctx->width, ctx->height, ctx->estimatePath);
	if (ret != DL_RESULT::SUCCESS)	return ret;

	_isInitialized = true;
	return DL_RESULT::SUCCESS;
}

BOOL DeepInfer::Core::isInitialized()
{
	if (_detector == nullptr || _estimator == nullptr)
		return FALSE;

	return _isInitialized;
}

BOOL DeepInfer::Core::isPoseInitialized()
{
	if (_estimator == nullptr)
		return FALSE;
	return TRUE;
}

DL_RESULT DeepInfer::Core::release()
{
	if (_detector != nullptr) {
		_detector->release();
		delete _detector; _detector = nullptr;
	}
	if(_estimator != nullptr){
		_estimator->release();
		delete _estimator; _estimator = nullptr;
	}

	return DL_RESULT::SUCCESS;
}

DL_RESULT DeepInfer::Core::doDetectInference(unsigned char* input, size_t inputPitch, const int& _width, const int& _height, Detection_Data_t& detectData)
{
	cv::cuda::GpuMat cvtGpuImg;
	//std::vector<cv::Rect> vecBBox;
	//std::vector<BBoxData_t> vecBBoxData;
	
	int ret = -1;

	// input = bgra
	cv::cuda::GpuMat gpuImg = cv::cuda::GpuMat(_detector_ctx.height, _detector_ctx.width, CV_8UC4, input, inputPitch);

	// common preproc
	cv::cuda::cvtColor(gpuImg, cvtGpuImg, cv::COLOR_BGRA2RGB);

	// object inference
	ret = _detector->detect((uint8_t*)cvtGpuImg.data, cvtGpuImg.step, detectData);
	if (ret != 0)
		return DL_RESULT::ERR_INFERENCE_DETECT;

	//  Grouping object ROI
	//cv::Rect roi = findROI(detectionData.bboxes, detectionData.classId, detectionData.prob, vecBBox, vecBBoxData, _detector_ctx.width, _detector_ctx.height);

	return DL_RESULT::SUCCESS;
}

DL_RESULT DeepInfer::Core::doPoseInference(unsigned char* input, size_t inputPitch, const int& _width, const int& _height, std::vector<std::array<FrPoint, 18>>& vecJointPoint)
{
	DL_RESULT ret = DL_RESULT::UNKNOWN;
	_estimator_ctx.width = _width;
	_estimator_ctx.height = _height;
	std::vector<std::array<int, 36>> tmpVecJointPoint;

	cv::cuda::GpuMat cvtGpuImg;

	// input = bgra
	cv::cuda::GpuMat gpuImg = cv::cuda::GpuMat(_estimator_ctx.height, _estimator_ctx.width, CV_8UC4, input, inputPitch);

	// common preproc
	cv::cuda::cvtColor(gpuImg, cvtGpuImg, cv::COLOR_BGRA2RGB);

	// pose inference
	ret = static_cast<DL_RESULT>(_estimator->estimate((uint8_t*)cvtGpuImg.data, cvtGpuImg.step, { 0, 0, cvtGpuImg.cols, cvtGpuImg.rows }, tmpVecJointPoint));
	if (ret != DL_RESULT::SUCCESS)
		return DL_RESULT::ERR_INFERENCE_ESTIMATE;
	
	// vector<array, 36> convert to vector<FrPoint, 18>
	convertFrPoint(tmpVecJointPoint, vecJointPoint);

	return ret;
}

DL_RESULT DeepInfer::Core::doInference(unsigned char* input, size_t inputPitch, VPdInferenceData_t& vpdInferenceData, size_t& outputSize)
{
	cv::cuda::GpuMat cvtGpuImg;
	Detection_Data_t detectionData;
	std::vector<cv::Rect> vecBBox;
	std::vector<BBoxData_t> vecBBoxData;
	std::vector<std::array<int, 36>> vecJointPoint{};
	std::vector<PoseData_t> vecPoseData;
	
	int ret = -1;

	// input = bgra
	cv::cuda::GpuMat gpuImg = cv::cuda::GpuMat(_detector_ctx.height, _detector_ctx.width, CV_8UC4, input, inputPitch);

	// common preproc
	cv::cuda::cvtColor(gpuImg, cvtGpuImg, cv::COLOR_BGRA2RGB);

	// object inference
	ret = _detector->detect((uint8_t*)cvtGpuImg.data, cvtGpuImg.step, detectionData);
	if (ret != 0)
		return DL_RESULT::ERR_INFERENCE_DETECT;

	//  Grouping object ROI
	cv::Rect roi = findROI(detectionData.bboxes, detectionData.classId, detectionData.prob, vecBBox, vecBBoxData, _detector_ctx.width, _detector_ctx.height);

	
	// pose inference
	ret = _estimator->estimate((uint8_t*)cvtGpuImg.data, cvtGpuImg.step, { roi.x, roi.y, roi.width, roi.height }, vecJointPoint);
	if (ret != 0)
		return DL_RESULT::ERR_INFERENCE_ESTIMATE;

	// make VPd meta data
	makeVPdMetaData(vecJointPoint, roi, vecBBox, detectionData, vecPoseData);

	// calcuate VPd meta data size
	vpdInferenceData.vec_bbox_data = vecBBoxData;
	vpdInferenceData.vec_pose_data = vecPoseData;
	outputSize = sizeof(VPdInferenceData_t);

	return DL_RESULT::SUCCESS;
}

DL_RESULT DeepInfer::Core::doInference(unsigned char* input, size_t inputPitch, const int& _width, const int& _height, unsigned char** output, size_t& outputSize)
{
	DL_RESULT ret = DL_RESULT::SUCCESS;
	_detector_ctx.width = _width;
	_detector_ctx.height = _height;

	_estimator_ctx.width = _width;
	_estimator_ctx.height = _height;

	ret = doInference(input, inputPitch, output, outputSize);
	if (ret != DL_RESULT::SUCCESS)
		return ret;

	return ret;
}

DL_RESULT DeepInfer::Core::doInference(unsigned char* input, size_t inputPitch, unsigned char** output, size_t& outputSize)
{
	cv::cuda::GpuMat cvtGpuImg;
	Detection_Data_t detectionData;
	std::vector<cv::Rect> vecBBox;
	std::vector<BBoxData_t> vecBBoxData;
	std::vector<std::array<int, 36>> vecJointPoint{};
	std::vector<PoseData_t> vecPoseData;
	VPdInferenceData_t* vpdInferenceData = new VPdInferenceData_t;
	int ret = -1;

	// input = bgra
	cv::cuda::GpuMat gpuImg = cv::cuda::GpuMat(_detector_ctx.height, _detector_ctx.width, CV_8UC4, input, inputPitch);

	// common preproc
	cv::cuda::cvtColor(gpuImg, cvtGpuImg, cv::COLOR_BGRA2RGB);
	
	// object inference
	LOG_D("[Infer] detect start !!");
	ret = _detector->detect((uint8_t*)cvtGpuImg.data, cvtGpuImg.step, detectionData);
	if (ret != 0)
		return DL_RESULT::ERR_INFERENCE_DETECT;
	LOG_D("[Infer] detect end !!");
	
	//  Grouping object ROI
	cv::Rect roi = findROI(detectionData.bboxes, detectionData.classId, detectionData.prob, vecBBox, vecBBoxData, _detector_ctx.width, _detector_ctx.height);
	LOG_D("[Infer] roi (%d, %d) (%d, %d)", roi.x, roi.y, roi.width, roi.height);
	
	LOG_D("[Infer] pose start !!");
	
	// pose inference
	ret = _estimator->estimate((uint8_t*)cvtGpuImg.data, cvtGpuImg.step, { roi.x, roi.y, roi.width, roi.height }, vecJointPoint);
	if (ret != 0)
		return DL_RESULT::ERR_INFERENCE_ESTIMATE;
	LOG_D("[Infer] pose end !!");
	
	// make VPd meta data
	makeVPdMetaData(vecJointPoint, roi, vecBBox, detectionData, vecPoseData);
	// calcuate VPd meta data size
	// TODO: 검증필요
	vpdInferenceData->vec_bbox_data = vecBBoxData;
	vpdInferenceData->vec_pose_data = vecPoseData;

	*output = (unsigned char*)vpdInferenceData;
	outputSize = sizeof(VPdInferenceData_t);

	return DL_RESULT::SUCCESS;
}



DL_RESULT DeepInfer::Core::doInference(unsigned char* input, size_t inputPitch, OUTPUT_PARAM_T& inferData)
{
	cv::cuda::GpuMat cvtGpuImg;
	Detection_Data_t detectionData;
	std::vector<cv::Rect> vecBBox;
	std::vector<BBoxData_t> vecBBoxData;
	std::vector<std::array<int, 36>> vecJointPoint{};
	std::vector<PoseData_t> vecPoseData;
	VPdInferenceData_t* vpdInferenceData = new VPdInferenceData_t;
	int ret = -1;

	// input = bgra
	cv::cuda::GpuMat gpuImg = cv::cuda::GpuMat(_detector_ctx.height, _detector_ctx.width, CV_8UC4, input, inputPitch);

	// common preproc
	cv::cuda::cvtColor(gpuImg, cvtGpuImg, cv::COLOR_BGRA2RGB);

	// object inference
	ret = _detector->detect((uint8_t*)cvtGpuImg.data, cvtGpuImg.step, detectionData);
	if (ret != 0)
		return DL_RESULT::ERR_INFERENCE_DETECT;

	// hug skip
	for (auto& id : detectionData.classId)
	{
		if (id == 2) {
			inferData.size = 0;
			return DL_RESULT::SUCCESS;
		}
	}

	//  Grouping object ROI
	cv::Rect roi = findROI(detectionData.bboxes, detectionData.classId, detectionData.prob, vecBBox, vecBBoxData, _detector_ctx.width, _detector_ctx.height);

	// pose inference
	ret = _estimator->estimate((uint8_t*)cvtGpuImg.data, cvtGpuImg.step, { roi.x, roi.y, roi.width, roi.height }, vecJointPoint);
	if (ret != 0)
		return DL_RESULT::ERR_INFERENCE_ESTIMATE;

	// make VPd meta data
	makeVPdMetaData(vecJointPoint, roi, vecBBox, detectionData, vecPoseData);

	// calcuate VPd meta data size
	vpdInferenceData->vec_bbox_data = vecBBoxData;
	vpdInferenceData->vec_pose_data = vecPoseData;

	inferData.data = (char*)vpdInferenceData;
	inferData.size = sizeof(VPdInferenceData_t);

	return DL_RESULT::SUCCESS;
}



cv::Rect DeepInfer::Core::paddingROI(const cv::Rect& _roi, const int& _width, const int& _height)
{
	// Width and Height Rate
	int roiX1 = _roi.x, roiY1 = _roi.y, roiX2 = _roi.width + _roi.x, roiY2 = _roi.height + _roi.y;
	LOG_D("[Infer] x1(%d), y1(%d), x2(%d), y2(%d)", roiX1, roiY1, roiX2, roiY2);

	int cmpLength = (roiX2 - roiX1) - (roiY2 - roiY1);
	if (cmpLength > 0) // 가로가 클경우
	{
		if (cmpLength / 2 <= (_height - roiY2) && cmpLength / 2 <= roiY1) {
			roiY1 -= cmpLength / 2;
			roiY2 += cmpLength / 2;
		}
		else if (cmpLength <= roiY1) {
			roiY1 -= cmpLength;
		}
		else if (cmpLength <= (_height - roiY2)) {
			roiY2 += cmpLength;
		}
	}
	else if (cmpLength < 0) // 세로가 클경우
	{
		if (abs(cmpLength) / 2 <= (_width - roiX2) && abs(cmpLength) / 2 <= roiX1) {
			roiX1 -= abs(cmpLength) / 2;
			roiX2 += abs(cmpLength) / 2;
		}
		else if (abs(cmpLength) <= roiX1) {
			roiX1 -= abs(cmpLength);
		}
		else if (abs(cmpLength) <= (_width - roiX2)) {
			roiX2 += abs(cmpLength);
		}
	}

	return cv::Rect(roiX1, roiY1, roiX2 - roiX1, roiY2 - roiY1);
}

cv::Rect DeepInfer::Core::findROI(std::vector<BBox_t>& srcBBox, const std::vector<int>& classId, const std::vector<float>& prob,
	std::vector<cv::Rect>& _vecRoi, std::vector<BBoxData_t>& _vecBBoxData, const int& _width, const int& _height)
{
	int roiX1 = _width, roiY1 = _height, roiX2 = 0, roiY2 = 0, roiWidth = 0, roiHeight = 0;
	int cnt = 0;
	for (auto& bbox : srcBBox)
	{
		// class가 홍 또는 청 선수일 경우만
		if ((classId[cnt] == 0 || classId[cnt] == 1) && prob[cnt] > 0.7)
		{
			// 좌표의 값이 -가 되는 경우
			if (bbox.x < 0) bbox.x = 0;
			if (bbox.y < 0) bbox.y = 0;

			// image size 넘치는경우
			bbox.w = (bbox.w <= _width) ? bbox.w : _width;
			bbox.h = (bbox.h <= _height) ? bbox.h : _height;
			_vecRoi.push_back({ static_cast<int>(bbox.x)
				, static_cast<int>(bbox.y)
				, static_cast<int>(bbox.w)
				, static_cast<int>(bbox.h) });

			BBoxData_t bboxData;
			bboxData.x = bbox.x; bboxData.y = bbox.y; bboxData.w = bbox.w; bboxData.h = bbox.h;
			bboxData.object_id = classId[cnt];
			bboxData.prob = prob[cnt];
			_vecBBoxData.push_back(bboxData);

			// Compare
			if (bbox.x < roiX1) roiX1 = bbox.x;
			if (bbox.x + bbox.w > roiX2) roiX2 = bbox.x + bbox.w;
			if (bbox.y < roiY1) roiY1 = bbox.y;
			if (bbox.y + bbox.h > roiY2) roiY2 = bbox.y + bbox.h;
		}
		++cnt;
	}
	if (_vecBBoxData.size() <= 0){
		roiX1 = 0; roiY1 = 0; roiX2 = _width; roiY2 = _height;
	}
	else
	{
		if (roiY2 > _height) roiY2 = _height;
		if (roiX2 > _width) roiX2 = _width;
		
		// Width and Height Rate
		cv::Rect tmpRect = paddingROI(cv::Rect(roiX1, roiY1, roiX2 - roiX1, roiY2 - roiY1), _width, _height);
		roiX1 = tmpRect.x; roiY1 = tmpRect.y; roiX2 = tmpRect.width + tmpRect.x; roiY2 = tmpRect.height + tmpRect.y;
		
		if (roiX2 >= _width) roiX2 = _width;
		if (roiY2 >= _height) roiY2 = _height;
		if (roiX1 < 0) roiX1 = 0;
		if (roiY1 < 0) roiY1 = 0;
	}

	return cv::Rect(roiX1, roiY1, roiX2 - roiX1, roiY2 - roiY1);
}

DL_RESULT DeepInfer::Core::makeVPdMetaData(std::vector<std::array<int, 36>>& _vecJointPoint, cv::Rect& _roi,
	std::vector<cv::Rect>& _vecBBox, Detection_Data_t& _detectionData, std::vector<PoseData_t>& _vecPoseData)
{
	for (auto& skeleton : _vecJointPoint)
	{
		int minX = _roi.x + _roi.width, maxX = 0, minY = _roi.y + _roi.height, maxY = 0;
		TRTJointPoint_t arrJoint_data[18];
		for (int i = 0; i < skeleton.size(); ++i)
		{
			if (skeleton[i] == 0)
				continue;
			if (i % 2 == 0) // x axis
			{
				if (skeleton[i] < minX) minX = skeleton[i];
				if (skeleton[i] > maxX) maxX = skeleton[i];
				arrJoint_data[i / 2].px = skeleton[i];
			}
			else // y axis
			{
				if (skeleton[i] < minY) minY = skeleton[i];
				if (skeleton[i] > maxY) maxY = skeleton[i];
				arrJoint_data[i / 2].py = skeleton[i];
			}
		}
		// BBox와 Skeleton min, max좌표값 비교
		int bboxCnt = 0;
		for (auto& _bbox : _vecBBox)
		{
			if (_bbox.x <= minX && _bbox.x + _bbox.width >= maxX &&
				_bbox.y <= minY && _bbox.y + _bbox.height >= maxY)
			{
				PoseData_t poseData_t;
				BBoxData_t bbox_t;
				bbox_t.x = _bbox.x;
				bbox_t.y = _bbox.y;
				bbox_t.w = _bbox.width;
				bbox_t.h = _bbox.height;
				bbox_t.object_id = _detectionData.classId[bboxCnt];
				bbox_t.prob = _detectionData.prob[bboxCnt];
				poseData_t.detection_data = bbox_t;

				memmove(poseData_t.arrJoint_data, arrJoint_data, sizeof(arrJoint_data));
				_vecPoseData.push_back(poseData_t);
				break;
			}
			++bboxCnt;
		}
	}
	return DL_RESULT::SUCCESS;
}

DL_RESULT DeepInfer::Core::initDetectInferenceEngine(const int& _width, const int& _height, const std::string& _path)
{
	if (_detector != nullptr)
		_detector->release();
	_detector = new deep4d::object::detector();
	_detector_ctx.width = _width;
	_detector_ctx.height = _height;
	_detector_ctx.enginePath = _path;
	
	int ret = _detector->initialize(&_detector_ctx);
	if (ret != 0)
		return DL_RESULT::ERR_INIT_DETECT;

	return DL_RESULT::SUCCESS;
}

DL_RESULT DeepInfer::Core::initEstimateInferenceEngine(const int& _width, const int& _height, const std::string& _path)
{
	if (_estimator != nullptr)
		_estimator->release();
	_estimator = new deep4d::pose::estimator();
	_estimator_ctx.width = _width;
	_estimator_ctx.height = _height;
	_estimator_ctx.enginePath = _path;
	
	int ret = _estimator->initialize(&_estimator_ctx);
	if (ret != 0)
		return DL_RESULT::ERR_INIT_ESTIMATE;

	return DL_RESULT::SUCCESS;
}

int DeepInfer::Core::calcVPdInferenceDataSize(const VPdInferenceData_t& _data)
{
	return (_data.vec_bbox_data.size() * sizeof(BBoxData_t))
		+ (_data.vec_pose_data.size() * (sizeof(BBoxData_t) + (sizeof(TRTJointPoint_t) * 18)))
		+ sizeof(TRTJointPoint_t);
}

int DeepInfer::Core::convertFrPoint(const std::vector<std::array<int, 36>>& _vecJoint, std::vector<std::array<FrPoint, 18>>& _vecFrPoint)
{
	for (auto Joint : _vecJoint)
	{
		std::array<FrPoint, 18> arrJoint;
		for (int i = 0; i < Joint.size(); ++i)
		{
			int idx = i / 2;
			if (i % 2 == 0)
				arrJoint[idx].px = Joint[i];
			else
				arrJoint[idx].py = Joint[i];
		}
		_vecFrPoint.push_back(arrJoint);
	}

	return 0;
}

int DeepInfer::Core::convertVecArrInt(const std::vector<std::array<FrPoint, 18>>& _vecFrPoint, std::vector<std::array<int, 36>>& _vecJoint)
{
	for (auto Joint : _vecFrPoint)
	{
		std::array<int, 36> arrJoint;
		for (int i = 0; i < Joint.size(); ++i)
		{
			arrJoint[i*2] = Joint[i].px;
			arrJoint[i*2 + 1] = Joint[i].py;
		}
		_vecJoint.push_back(arrJoint);
	}

	return 0;
}

int DeepInfer::Core::drawPose(const std::vector<std::array<FrPoint, 18>>& _vecFrPoint, cv::Mat& _img)
{
	std::vector<std::array<int, 36>> _tmpVecJoint;
	convertVecArrInt(_vecFrPoint, _tmpVecJoint);
	_estimator->draw(_tmpVecJoint, _img);
	return 0;
}
