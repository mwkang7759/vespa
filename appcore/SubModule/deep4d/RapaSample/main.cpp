﻿#include <iostream>
#include <sld_object_detector.h>
#include <sld_pose_estimator.h>
#include <opencv2/core.hpp>
//#include <opencv2/core/cuda.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/cudaimgproc.hpp>

int main()
{
    /* Video Source Path */
    std::string srcPath = "sample1.mp4";

    solids::lib::video::nvidia::object::detector* _detector;
    solids::lib::video::nvidia::object::detector::context_t _detector_ctx;
    solids::lib::video::nvidia::pose::estimator* _estimator;
    solids::lib::video::nvidia::pose::estimator::context_t _estimator_ctx;

    cv::VideoCapture videoCapture;
    cv::Mat frame;
    videoCapture.open(srcPath);
    int32_t srcWidth = videoCapture.get(cv::CAP_PROP_FRAME_WIDTH);
    int32_t srcHeight = videoCapture.get(cv::CAP_PROP_FRAME_HEIGHT);

    _detector = new solids::lib::video::nvidia::object::detector();
    _detector_ctx.width = srcWidth;
    _detector_ctx.height = srcHeight;
    _detector_ctx.enginePath = "yolov4_1_3_288_288_trt7234_fp16.engine";
    _detector->initialize(&_detector_ctx);

    _estimator = new solids::lib::video::nvidia::pose::estimator();
    _estimator_ctx.width = srcWidth;
    _estimator_ctx.height = srcHeight;
    _estimator_ctx.enginePath = "trt_pose_fp16_rt7234.engine";
    _estimator->initialize(&_estimator_ctx);

    while (videoCapture.read(frame))
    {
        cv::cuda::GpuMat srcGpuImg, cvtGpuImg, dstGpuImg;
        cv::Mat cvtImg, dstImg;
        srcGpuImg.upload(frame);
        
        cv::cuda::cvtColor(srcGpuImg, cvtGpuImg, cv::COLOR_BGR2BGRA);

        uint8_t* bbox = NULL;
        int32_t bboxSize = 0;
        _detector->detect(cvtGpuImg.data, cvtGpuImg.step, &bbox, bboxSize);
        
        uint8_t* render = NULL;
        int32_t pitch = 0;
        _estimator->estimate(cvtGpuImg.data, cvtGpuImg.step, bbox, bboxSize, &render, pitch);

        dstGpuImg = cv::cuda::GpuMat(cvtGpuImg.rows, cvtGpuImg.cols, CV_8UC4, render, pitch);
        dstGpuImg.download(dstImg);

        cv::imshow("test", dstImg);
        cv::waitKey(1);
    }

    std::cout << "done" << std::endl;
}
