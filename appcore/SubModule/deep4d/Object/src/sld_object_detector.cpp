#include "sld_object_detector.h"
#include "object_detector.h"

namespace deep4d
{
	namespace object
	{

		detector::detector(void)
		{
			_core = new deep4d::object::detector::core(this);
		}

		detector::~detector(void)
		{
			if (_core)
				delete _core;
			_core = NULL;
		}

		int32_t detector::initialize(deep4d::object::detector::context_t* ctx)
		{
			return static_cast<int>(_core->initialize(ctx));
		}

		int32_t detector::release(void)
		{
			return static_cast<int>(_core->release());
		}

		int32_t detector::detect(uint8_t* input, int32_t inputStride, uint8_t** output, int32_t& outputStride)
		{
			return static_cast<int>(_core->detect(input, inputStride, output, outputStride));
		}

		int32_t detector::detect(uint8_t* input, int32_t inputStride, uint8_t** output, int32_t& outputStride, std::vector<int>& classes, std::vector<float>& scores)
		{
			return static_cast<int>(_core->detect(input, inputStride, output, outputStride, classes, scores));
		}

		int32_t detector::detect(uint8_t* input, int32_t inputStride, Detection_Data_t& detectionData)
		{
			return static_cast<int>(_core->detect(input, inputStride, detectionData));
		}

	};
};

