﻿#include "object_detector.h"

#define MAX_WORKSPACE (1 << 30) // 1G workspace memory


namespace deep4d
{
	namespace object
	{
		detector::core::core(deep4d::object::detector* front)
			: _front(front)
			, _ctx(NULL)
			, cudaStream(nullptr)
		{
		}

		detector::core::~core(void)
		{
			cudaStreamSynchronize(cudaStream);
			cudaStreamDestroy(cudaStream);

			for (int i = 0; i < cudaBuffers.size(); ++i)
			{
				if (cudaBuffers[i])
					cudaFree(cudaBuffers[i]);
			}
			if (cudaFrame)
				cudaFree(cudaFrame);

			if (input_gpu)
				cudaFree(input_gpu);
		}

		void detector::core::initEngine()
		{
			// TODO: Functionalization (ex. input & output Dimension, etc...)
			cudaBuffers.resize(engine->getNbBindings());
			for (size_t i = 0; i < engine->getNbBindings(); ++i)
			{
				auto bindingSize = getSizeByDim(engine->getBindingDimensions(i)) * 1 * sizeof(float);
				cudaMalloc(&cudaBuffers[i], bindingSize);
				if (engine->bindingIsInput(i))
				{
					inputDims.emplace_back(engine->getBindingDimensions(i));
				}
				else
				{
					outputDims.emplace_back(engine->getBindingDimensions(i));
				}
				LOG_I("[Detect] Binding Name: (%s)", engine->getBindingName(i));
			}
			if (inputDims.empty() || outputDims.empty())
			{
				LOG_E("[Detect] Expect at least one input and one output for network");
			}

			// TODO: dynamic batchsize...
			batchSize = inputDims[0].d[0];
			numChannels = inputDims[0].d[1];
			inputHeightSize = inputDims[0].d[2];
			inputWidthSize = inputDims[0].d[3];

			// TODO: Debug Mode print info
			LOG_I("[Detect] output[0] -> Objectness Score & Box Cordinate");
			LOG_I("[Detect] outputDims[0]: ");
			std::size_t size = 1;
			for (std::size_t i = 0; i < outputDims[0].nbDims; ++i)
			{
				LOG_I("[Detect] out[0]: (%d)", outputDims[0].d[i]);
				size *= outputDims[0].d[i];
			}
			LOG_I("[Detect] out[0].size: (%d)", size);

			LOG_I("[Detect] output[1] -> Class Confidence");
			LOG_I("[Detect] outputDims[1]: ");
			size = 1;
			for (std::size_t i = 0; i < outputDims[1].nbDims; ++i)
			{
				LOG_I("[Detect] out[1]: (%d)", outputDims[1].d[i]);
				size *= outputDims[1].d[i];
			}
			LOG_I("[Detect] out[1].size: (%d)", size);

			cpuBboxBuffer.resize(getSizeByDim(outputDims[0]) * batchSize);
			cpuConfidenceBuffer.resize(getSizeByDim(outputDims[1]) * batchSize);

			LOG_I("[Detect] Model input shape: batchSize(%d), numChannels(%d), inputWidthSize(%d), inputHeightSize(%d)", 
				batchSize, numChannels, inputWidthSize, inputHeightSize);
			
			classesNum = outputDims[1].d[outputDims[1].nbDims - 1];
			cudaFrame = safeCudaMalloc(4096 * 4096 * 3 * sizeof(uchar)); // max input image shape
			auto res = cudaMalloc((void**)&input_gpu, inputWidthSize * inputHeightSize * 3 * sizeof(float));

			cudaStreamCreate(&cudaStream);
		}

		DL_RESULT detector::core::initialize(deep4d::object::detector::context_t* ctx)
		{
			_ctx = ctx;
			LOG_I("[Detect] Loading Obeject Detect Inference Engine..");

			// Select GPU Device Num
			cudaSetDevice(0);

			// File seek function
			std::fstream file;
			file.open(_ctx->enginePath, std::ios::binary | std::ios::in);
			if (!file.is_open())
			{
				LOG_E("[Detect] failed to read engine file: (%s)", _ctx->enginePath);
				return DL_RESULT::ERR_INIT_DETECT_FAILED_READ_ENGINE_FILE;
			}
			file.seekg(0, std::ios::end);
			int length = file.tellg();
			file.seekg(0, std::ios::beg);
			std::unique_ptr<char[]> data(new char[length]);
			file.read(data.get(), length);
			file.close();

			auto runtime = UniquePtr<nvinfer1::IRuntime>(nvinfer1::createInferRuntime(gLogger));
			assert(runtime != nullptr);

			engine = UniquePtr<nvinfer1::ICudaEngine>(runtime->deserializeCudaEngine(data.get(), length, nullptr));
			assert(engine != nullptr);

			LOG_I("[Detect] Engine Initialize Done");

			context = UniquePtr<nvinfer1::IExecutionContext>(engine->createExecutionContext());
			assert(context);

			const int numBindingPerProfile = engine->getNbBindings() / engine->getNbOptimizationProfiles();
			LOG_I("[Detect] Number of binding profiles: (%d)", numBindingPerProfile);

			initEngine();
			return DL_RESULT::SUCCESS;
		}

		DL_RESULT detector::core::release(void)
		{
			return DL_RESULT::SUCCESS;
		}

		DL_RESULT detector::core::preProc(const cv::cuda::GpuMat& _srcGpuImg)
		{
			cv::cuda::GpuMat resizeImg, normImg;
			cv::cuda::resize(_srcGpuImg, resizeImg, cv::Size(inputWidthSize, inputHeightSize));
			resizeImg.convertTo(normImg, CV_32FC3, 1.0f / 255.0f);

			std::vector<cv::cuda::GpuMat> inputChannels{
				cv::cuda::GpuMat(normImg.rows, normImg.cols, CV_32F, &input_gpu[0]),
				cv::cuda::GpuMat(normImg.rows, normImg.cols, CV_32F, &input_gpu[inputWidthSize * inputHeightSize]),
				cv::cuda::GpuMat(normImg.rows, normImg.cols, CV_32F, &input_gpu[inputWidthSize * inputHeightSize * 2])
			};
			cv::cuda::split(normImg, inputChannels);

			cudaBuffers.data()[0] = input_gpu;
			return DL_RESULT::SUCCESS;
		}

		/// <summary>
		/// * bboxes = unsigned int x, y, w, h
		/// </summary>
		/// <param name="bboxes"></param>
		/// <param name="scores"></param>
		/// <param name="classes"></param>
		/// <param name="frameSize"></param>
		/// <returns></returns>
		DL_RESULT detector::core::postProc(std::vector<BBox_t>& bboxes, std::vector<float>& scores, std::vector<int>& classes, const cv::Size& frameSize)
		{
			std::vector<cv::Rect> tmpRect;
			postProc(tmpRect, scores, classes, frameSize);
			for (auto& rect : tmpRect)
			{
				bboxes.push_back(BBox_t{ rect.x
					, rect.y
					, rect.width
					, rect.height });
			}

			return DL_RESULT::SUCCESS;
		}

		/// <summary>
		/// * bboxes = cv::Rect
		/// </summary>
		/// <param name="bboxes"></param>
		/// <param name="scores"></param>
		/// <param name="classes"></param>
		/// <param name="frameSize"></param>
		/// <returns></returns>
		DL_RESULT detector::core::postProc(std::vector<cv::Rect>& bboxes, std::vector<float>& scores, std::vector<int>& classes, const cv::Size& frameSize)
		{
			int32_t stIdx = 0;
			cudaMemcpy(cpuBboxBuffer.data(), (float*)cudaBuffers[1], cpuBboxBuffer.size() * sizeof(float), cudaMemcpyDeviceToHost);
			cudaMemcpy(cpuConfidenceBuffer.data(), (float*)cudaBuffers[2], cpuConfidenceBuffer.size() * sizeof(float), cudaMemcpyDeviceToHost);

			std::vector<cv::Rect> tmpBbox;
			std::vector<float> tmpScore;
			std::vector<int> tmpClasses;
			
			for (int32_t i = 0; i < cpuConfidenceBuffer.size() / classesNum; ++i)
			{
				std::vector<float> tmpVector;

				// Step1 : Score Extract and get max score index(=confidence idx)
				int32_t endIdx = stIdx + classesNum;
				tmpVector = std::vector<float>(cpuConfidenceBuffer.begin() + stIdx, cpuConfidenceBuffer.begin() + endIdx);

				// Get Max Score & Index
				auto result = std::max_element(tmpVector.begin(), tmpVector.end());
				int32_t targetIdx = std::distance(tmpVector.begin(), result);// +stIdx;
				int label = targetIdx;
				targetIdx += stIdx;

				// Step2 : Get Confidence index
				float confidence = cpuConfidenceBuffer[targetIdx];
				stIdx = endIdx;

				// Step3 : Confidence Threshold compare and make box info
				if (confidence < confThreshold)
					continue;

				// Find target coordinate and Get target index
				int32_t targetBoxStartIdx = (targetIdx / classesNum) * 4;

				// Get Boundary Box Info
				int32_t x = int(cpuBboxBuffer[targetBoxStartIdx] * frameSize.width);      // Stary X
				int32_t y = int(cpuBboxBuffer[targetBoxStartIdx + 1] * frameSize.height); // Start Y
				int32_t w = int(cpuBboxBuffer[targetBoxStartIdx + 2] * frameSize.width) - x;  // End X
				int32_t h = int(cpuBboxBuffer[targetBoxStartIdx + 3] * frameSize.height) - y; // End Y
				cv::Rect bbox = { x, y, w, h };
				tmpBbox.push_back(bbox);
				tmpScore.push_back(confidence);
				tmpClasses.push_back(label);
			}

			// Step4 : Calculate NMS with NMSBoxes(opencv cv::dnn::NMSBoxes)
			std::vector<int32_t> indices;
			cv::dnn::NMSBoxes(tmpBbox, tmpScore, confThreshold, nmsThreshold, indices);
			for (int& idx : indices)
			{
				bboxes.push_back(tmpBbox[idx]);
				scores.push_back(tmpScore[idx]);
				classes.push_back(tmpClasses[idx]);
			}
			return DL_RESULT::SUCCESS;
		}

		/// <summary>
		/// * input = rgb data, * detectionData = bboxes, probability, classID
		/// </summary>
		/// <param name="input"> = rgb data</param>
		/// <param name="inputStride"></param>
		/// <param name="detectionData"> = bboxes, probability, classID</param>
		/// <returns></returns>
		DL_RESULT detector::core::detect(uint8_t* input, int32_t inputStride, Detection_Data_t& detectionData)
		{
			cv::cuda::GpuMat srcGpuImg = cv::cuda::GpuMat(_ctx->height, _ctx->width, CV_8UC3, input, inputStride);
			preProc(srcGpuImg);

			// Inference
			context->enqueue(1, cudaBuffers.data(), cudaStream, nullptr);

			// Post-Processing

			postProc(detectionData.bboxes, detectionData.prob, detectionData.classId, srcGpuImg.size());

			// Step5 : draw BBoxes or Send to BBox from Pose Estimation
			return DL_RESULT::SUCCESS;
		}

		/// <summary>
		/// * input = bgra data
		/// </summary>
		/// <param name="input"> = bgra data</param>
		/// <param name="inputStride"></param>
		/// <param name="output"></param>
		/// <param name="outputStride"></param>
		/// <param name="classes"></param>
		/// <param name="scores"></param>
		/// <returns></returns>
		DL_RESULT detector::core::detect(uint8_t* input, int32_t inputStride, uint8_t** output, int32_t& outputStride, std::vector<int>& classes, std::vector<float>& scores)
		{
			cv::cuda::GpuMat srcGpuImg = cv::cuda::GpuMat(_ctx->height, _ctx->width, CV_8UC4, input, inputStride);
			preProc(srcGpuImg);

			// Inference
			context->enqueue(1, cudaBuffers.data(), cudaStream, nullptr);

			// Post-Processing
			std::vector<cv::Rect>* bboxes = new std::vector<cv::Rect>;
			postProc(*bboxes, scores, classes, srcGpuImg.size());

			// Step5 : draw BBoxes or Send to BBox from Pose Estimation
			*output = (uint8_t*)bboxes->data();
			outputStride = bboxes->size() * sizeof(cv::Rect);

			//Detection_Data_t output_t = {bboxes};


			return DL_RESULT::SUCCESS;
		}

		DL_RESULT detector::core::detect(uint8_t* input, int32_t inputStride, uint8_t** output, int32_t& outputStride)
		{
			cv::cuda::GpuMat srcGpuImg = cv::cuda::GpuMat(_ctx->height, _ctx->width, CV_8UC4, input, inputStride);
			cv::cuda::GpuMat resizeImg, normImg;
			cv::Size frameSize = srcGpuImg.size();
			cv::cuda::cvtColor(srcGpuImg, srcGpuImg, cv::COLOR_BGRA2BGR);
			//srcGpuImg.download(rgbImg);

			// Pre-Processing
			cv::cuda::resize(srcGpuImg, resizeImg, cv::Size(inputWidthSize, inputHeightSize));
			resizeImg.convertTo(normImg, CV_32FC3, 1.0f / 255.0f);

			std::vector<cv::cuda::GpuMat> inputChannels{
				cv::cuda::GpuMat(normImg.rows, normImg.cols, CV_32F, &input_gpu[0]),
				cv::cuda::GpuMat(normImg.rows, normImg.cols, CV_32F, &input_gpu[inputWidthSize * inputHeightSize]),
				cv::cuda::GpuMat(normImg.rows, normImg.cols, CV_32F, &input_gpu[inputWidthSize * inputHeightSize * 2])
			};
			cv::cuda::split(normImg, inputChannels);

			cudaBuffers.data()[0] = input_gpu;

			// Inference
			context->enqueue(1, cudaBuffers.data(), cudaStream, nullptr);

			// Inference result device to host
			cudaMemcpy(cpuBboxBuffer.data(), (float*)cudaBuffers[1], cpuBboxBuffer.size() * sizeof(float), cudaMemcpyDeviceToHost);
			cudaMemcpy(cpuConfidenceBuffer.data(), (float*)cudaBuffers[2], cpuConfidenceBuffer.size() * sizeof(float), cudaMemcpyDeviceToHost);

			// Post-Processing
			int32_t stIdx = 0;
			std::vector<cv::Rect>* bboxes = new std::vector<cv::Rect>;
			std::vector<float> scores;
			std::vector<int> classes;

			for (int32_t i = 0; i < cpuConfidenceBuffer.size() / classesNum; ++i)
			{
				std::vector<float> tmpVector;

				// Step1 : Score Extract and get max score index(=confidence idx)
				int32_t endIdx = stIdx + classesNum;
				tmpVector = std::vector<float>(cpuConfidenceBuffer.begin() + stIdx, cpuConfidenceBuffer.begin() + endIdx);

				// Get Max Score & Index
				auto result = std::max_element(tmpVector.begin(), tmpVector.end());
				int32_t targetIdx = std::distance(tmpVector.begin(), result);// +stIdx;
				int label = targetIdx;
				targetIdx += stIdx;

				// Step2 : Get Confidence index
				float confidence = cpuConfidenceBuffer[targetIdx];
				stIdx = endIdx;

				// Step3 : Confidence Threshold compare and make box info
				if (confidence < confThreshold)
					continue;

				// Find target coordinate and Get target index
				int32_t targetBoxStartIdx = (targetIdx / classesNum) * 4;

				// Get Boundary Box Info
				int32_t x = int(cpuBboxBuffer[targetBoxStartIdx] * frameSize.width);      // Stary X
				int32_t y = int(cpuBboxBuffer[targetBoxStartIdx + 1] * frameSize.height); // Start Y
				int32_t w = int(cpuBboxBuffer[targetBoxStartIdx + 2] * frameSize.width) - x;  // End X
				int32_t h = int(cpuBboxBuffer[targetBoxStartIdx + 3] * frameSize.height) - y; // End Y
				//int32_t x = int(centerx - w / 2);
				//int32_t y = int(centery - h / 2);
				cv::Rect bbox = { x, y, w, h };
				bboxes->push_back(bbox);
				scores.push_back(confidence);
				classes.push_back(label);
			}

			// Step4 : Calculate NMS with NMSBoxes(opencv cv::dnn::NMSBoxes)
			std::vector<int32_t> indices;
			cv::dnn::NMSBoxes(*bboxes, scores, confThreshold, nmsThreshold, indices); // Remote to overlap box 

			// Step5 : draw BBoxes or Send to BBox from Pose Estimation
			*output = (uint8_t*)bboxes->data();
			outputStride = bboxes->size() * sizeof(cv::Rect);

			return DL_RESULT::SUCCESS;
		}

		std::size_t detector::core::getSizeByDim(const nvinfer1::Dims& dims)
		{
			std::size_t size = 1;
			for (std::size_t i = 0; i < dims.nbDims; ++i)
			{
				size *= dims.d[i];
			}
			return size;
		}


	}
}
