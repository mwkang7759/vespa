﻿#ifndef _DEEP_BASE_H_
#define _DEEP_BASE_H_

#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <climits>
#include <memory>
#include <string>
#include <algorithm>
#include <iostream>
#include <queue>
#include <string>

//#if defined(EXPORT_DEEP_BASE)
//#define DEEP_BASE_CLASS __declspec(dllexport)
//#else
//#define DEEP_BASE_CLASS __declspec(dllexport)
//#endif

#ifndef FALSE
#define FALSE               0
#endif

#ifndef TRUE
#define TRUE                1
#endif

typedef int BOOL;

static const int UNKNOWN_NUM = -1;
enum class DL_RESULT
{
	UNKNOWN = UNKNOWN_NUM,
	SUCCESS,

	ERR_INIT_DETECT = 10,
	ERR_INIT_ESTIMATE,

	ERR_INFERENCE_DETECT = 20,
	ERR_INFERENCE_ESTIMATE,

	ERR_INIT_DETECT_FAILED_READ_ENGINE_FILE = 100,

	ERR_INIT_ESTIMATE_FAILED_READ_ENGINE_FILE = 200,

};

class DeepBase
{
public:

	typedef struct _TRTJointPoint_t {
		unsigned int    px{ 0 };
		unsigned int    py{ 0 };
	}TRTJointPoint_t;

	typedef struct _BBoxData_t {
		unsigned int    x{ 0 };
		unsigned int    y{ 0 };
		unsigned int    w{ 0 };
		unsigned int    h{ 0 };
		unsigned int    object_id{ 0 };
		unsigned int    track_id{ 0 };
		unsigned int    frames_counter{ 0 };
		float           prob{ 0.f };
		float           x_3d{ 0.f };
		float           y_3d{ 0.f };
		float           z_3d{ 0.f };
	}BBoxData_t;

	typedef struct _PoseData_t {
		BBoxData_t              detection_data;
		TRTJointPoint_t         arrJoint_data[18]{0,};
	}PoseData_t;

	typedef struct _VPdInferenceData_t {
		TRTJointPoint_t         hit_coord;
		std::vector<BBoxData_t> vec_bbox_data;
		std::vector<PoseData_t> vec_pose_data;
	}VPdInferenceData_t;

	typedef struct _BBox_t
	{
		void BBox_t()
		{}
		int    x{ 0 };
		int    y{ 0 };
		int    w{ 0 };
		int    h{ 0 };
	}BBox_t;
		
		
	typedef struct _Detection_Data_t
	{
		void Detection_Data_t()
		{}
		std::vector<BBox_t> bboxes;
		std::vector<int> classId;
		std::vector<float> prob;
	}Detection_Data_t;

	typedef struct _ERR_CODE_T
	{
		static const int UNKNOWN = -1;
		static const int SUCCESS = 0;
		static const int GENERIC_FAIL = 1;
		static const int INVALID_PARAMETER = 2;
	} ERR_CODE_T;

};

using FrPoint = DeepBase::TRTJointPoint_t;

#endif
