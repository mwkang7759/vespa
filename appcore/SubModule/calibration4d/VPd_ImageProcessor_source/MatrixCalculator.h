#pragma once
#include <opencv2/opencv.hpp>

class MatrixCalculator
{
public:
	static bool GetTranslationMatrix(const float tx, const float ty, cv::Mat& m);
	static bool GetRotationMatrix(const float rad, cv::Mat& m);
	static bool GetRotationMatrix(const float rad, const float cx, const float cy, cv::Mat& m);
	static bool GetScaleMarix(const float scaleX, const float scaleY, cv::Mat& m);
	static bool GetScaleMarix(const float scaleX, const float scaleY, const float cx, const float cy, cv::Mat& m);
	static bool GetFlipMatrix(const int width, const int height, const bool isFlipX, const bool isFlipY, cv::Mat& m);
	static bool GetMarginMatrix(const int width, const int height, const int marginX, const int marginY, const int marginW, const int marginH, cv::Mat& m);
	static bool ParseAffineMatrix(const cv::Mat& m33, cv::Mat& m23);
};

