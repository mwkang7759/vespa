#include "ImageProcessorUtil.h"

ImageProcessorUtil::ImageProcessorUtil()
{

}

ImageProcessorUtil::~ImageProcessorUtil()
{

}

IP_RESULT ImageProcessorUtil::CudaToGpuMatYUV420(
	unsigned char* dpYUVFrame, int nWidth, int nHeight, int nPitch,
	vector<cuda::GpuMat>& vecGpuMatYUV420)
{	
	if(!vecGpuMatYUV420.empty())
		vecGpuMatYUV420.clear();

	int lumaWidth = nWidth;
	int lumaHeight = nHeight;
	int chromaWidth = lumaWidth >> 1;
	int chromaHeight = lumaHeight >> 1;
	int lumaPitch = nPitch;
	int chromaPitch = lumaPitch >> 1;
	int dpYUVPitch = lumaPitch;

	unsigned char* yDeviceptr = dpYUVFrame;
	unsigned char* uDeviceptr = yDeviceptr + lumaPitch * lumaHeight;
	unsigned char* vDeviceptr = uDeviceptr + chromaPitch * chromaHeight;

	cuda::GpuMat gpuY(lumaHeight, lumaWidth, CV_8UC1, yDeviceptr, lumaPitch);
	cuda::GpuMat gpuU(chromaHeight, chromaWidth, CV_8UC1, uDeviceptr, chromaPitch);
	cuda::GpuMat gpuV(chromaHeight, chromaWidth, CV_8UC1, vDeviceptr, chromaPitch);

	vecGpuMatYUV420.push_back(gpuY);
	vecGpuMatYUV420.push_back(gpuU);
	vecGpuMatYUV420.push_back(gpuV);

	if (vecGpuMatYUV420.size() != 3 || vecGpuMatYUV420.size() != 3)
	{
		return IP_RESULT::ERROR_INPUT_OR_OUPUT_IS_NOT_3CHANNEL;
	}

	if ((vecGpuMatYUV420[0].cols / 2 != vecGpuMatYUV420[1].cols) ||
		(vecGpuMatYUV420[0].cols / 2 != vecGpuMatYUV420[2].cols) ||
		(vecGpuMatYUV420[1].cols != vecGpuMatYUV420[2].cols) ||
		(vecGpuMatYUV420[0].rows / 2 != vecGpuMatYUV420[1].rows) ||
		(vecGpuMatYUV420[0].rows / 2 != vecGpuMatYUV420[2].rows) ||
		(vecGpuMatYUV420[1].rows != vecGpuMatYUV420[2].rows))
	{
		return IP_RESULT::ERROR_INPUT_IS_NOT_YUV420;
	}

	return IP_RESULT::OK;
}