﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace FDUtilities
{
    struct CvSharpLine
    {
        public OpenCvSharp.Point2f p1;
        public OpenCvSharp.Point2f p2;
    }
    public class ConvexPolygon2D
    {
        public OpenCvSharp.Point2d[] Corners;
        public ConvexPolygon2D(OpenCvSharp.Point2d[] corners)
        {
            Corners = corners;
        }
        public ConvexPolygon2D(OpenCvSharp.Point2f[] corners)
        {
            Corners = new OpenCvSharp.Point2d[corners.Length];
            for(int i=0; i< corners.Length; i++)
            {
                Corners[i].X = corners[i].X;
                Corners[i].Y = corners[i].Y;
            }
        }
        public List<OpenCvSharp.Point2f> ToPoint2fList()
        {
            List<OpenCvSharp.Point2f> pts = new List<OpenCvSharp.Point2f>();

            foreach(OpenCvSharp.Point2d pt in Corners)
            {
                pts.Add(new OpenCvSharp.Point2f((float)pt.X, (float)pt.Y));
            }

            return pts;
        }
    }
    class CropMarginCalculator
    {
        const double EquityTolerance = 0.000000001d;
        private static bool IsEqual(double d1, double d2)
        {
            return Math.Abs(d1 - d2) <= EquityTolerance;
        }
        public static bool GetIntersectionPoint(in OpenCvSharp.Point2d l1p1, in OpenCvSharp.Point2d l1p2, in OpenCvSharp.Point2d l2p1, in OpenCvSharp.Point2d l2p2, out OpenCvSharp.Point2d ptIts)
        {
            double A1 = l1p2.Y - l1p1.Y;
            double B1 = l1p1.X - l1p2.X;
            double C1 = A1 * l1p1.X + B1 * l1p1.Y;
            double A2 = l2p2.Y - l2p1.Y;
            double B2 = l2p1.X - l2p2.X;
            double C2 = A2 * l2p1.X + B2 * l2p1.Y;
            //lines are parallel
            double det = A1 * B2 - A2 * B1;
            if (IsEqual(det, 0d))
            {
                ptIts = new OpenCvSharp.Point2d(0, 0);
                return false; //parallel lines
            }
            else
            {
                double x = (B2 * C1 - B1 * C2) / det;
                double y = (A1 * C2 - A2 * C1) / det;
                bool online1 = ((Math.Min(l1p1.X, l1p2.X) < x || IsEqual(Math.Min(l1p1.X, l1p2.X), x))
                    && (Math.Max(l1p1.X, l1p2.X) > x || IsEqual(Math.Max(l1p1.X, l1p2.X), x))
                    && (Math.Min(l1p1.Y, l1p2.Y) < y || IsEqual(Math.Min(l1p1.Y, l1p2.Y), y))
                    && (Math.Max(l1p1.Y, l1p2.Y) > y || IsEqual(Math.Max(l1p1.Y, l1p2.Y), y))
                    );
                bool online2 = ((Math.Min(l2p1.X, l2p2.X) < x || IsEqual(Math.Min(l2p1.X, l2p2.X), x))
                    && (Math.Max(l2p1.X, l2p2.X) > x || IsEqual(Math.Max(l2p1.X, l2p2.X), x))
                    && (Math.Min(l2p1.Y, l2p2.Y) < y || IsEqual(Math.Min(l2p1.Y, l2p2.Y), y))
                    && (Math.Max(l2p1.Y, l2p2.Y) > y || IsEqual(Math.Max(l2p1.Y, l2p2.Y), y))
                    );
                if (online1 && online2)
                {
                    ptIts = new OpenCvSharp.Point2d(x, y);
                    return true;
                }
            }
            ptIts = new OpenCvSharp.Point2d(0, 0);
            return false; //intersection is at out of at least one segment.
        }
        public static bool IsPointInsidePoly(OpenCvSharp.Point2d test, ConvexPolygon2D poly)
        {
            int i;
            int j;
            bool result = false;
            for (i = 0, j = poly.Corners.Length - 1; i < poly.Corners.Length; j = i++)
            {
                if ((poly.Corners[i].Y > test.Y) != (poly.Corners[j].Y > test.Y) &&
                    (test.X < (poly.Corners[j].X - poly.Corners[i].X) * (test.Y - poly.Corners[i].Y) / (poly.Corners[j].Y - poly.Corners[i].Y) + poly.Corners[i].X))
                {
                    result = !result;
                }
            }
            return result;
        }
        public static OpenCvSharp.Point2d[] GetIntersectionPoints(OpenCvSharp.Point2d l1p1, OpenCvSharp.Point2d l1p2, ConvexPolygon2D poly)
        {
            List<OpenCvSharp.Point2d> intersectionPoints = new List<OpenCvSharp.Point2d>();
            for (int i = 0; i < poly.Corners.Length; i++)
            {
                int next = (i + 1 == poly.Corners.Length) ? 0 : i + 1;
                if (GetIntersectionPoint(l1p1, l1p2, poly.Corners[i], poly.Corners[next], out OpenCvSharp.Point2d ip))
                    intersectionPoints.Add(ip);
            }
            return intersectionPoints.ToArray();
        }
        private static void AddPoints(List<OpenCvSharp.Point2d> pool, OpenCvSharp.Point2d[] newpoints)
        {
            foreach (OpenCvSharp.Point2d np in newpoints)
            {
                bool found = false;
                foreach (OpenCvSharp.Point2d p in pool)
                {
                    if (IsEqual(p.X, np.X) && IsEqual(p.Y, np.Y))
                    {
                        found = true;
                        break;
                    }
                }
                if (!found) pool.Add(np);
            }
        }
        private static OpenCvSharp.Point2d[] OrderClockwise(OpenCvSharp.Point2d[] points)
        {
            double mX = 0;
            double my = 0;
            foreach (OpenCvSharp.Point2d p in points)
            {
                mX += p.X;
                my += p.Y;
            }
            mX /= points.Length;
            my /= points.Length;
            return points.OrderBy(v => Math.Atan2(v.Y - my, v.X - mX)).ToArray();
        }
        public static ConvexPolygon2D GetIntersectionOfPolygons(ConvexPolygon2D poly1, ConvexPolygon2D poly2)
        {
            List<OpenCvSharp.Point2d> clippedCorners = new List<OpenCvSharp.Point2d>();
            //Add  the corners of poly1 which are inside poly2       
            for (int i = 0; i < poly1.Corners.Length; i++)
            {
                if (IsPointInsidePoly(poly1.Corners[i], poly2))
                    AddPoints(clippedCorners, new OpenCvSharp.Point2d[] { poly1.Corners[i] });
            }
            //Add the corners of poly2 which are inside poly1
            for (int i = 0; i < poly2.Corners.Length; i++)
            {
                if (IsPointInsidePoly(poly2.Corners[i], poly1))
                    AddPoints(clippedCorners, new OpenCvSharp.Point2d[] { poly2.Corners[i] });
            }
            //Add  the intersection points
            for (int i = 0, next = 1; i < poly1.Corners.Length; i++, next = (i + 1 == poly1.Corners.Length) ? 0 : i + 1)
            {
                AddPoints(clippedCorners, GetIntersectionPoints(poly1.Corners[i], poly1.Corners[next], poly2));
            }

            return new ConvexPolygon2D(OrderClockwise(clippedCorners.ToArray()));
        }

        private static double EuclideanDistance(in OpenCvSharp.Point2f p1, in OpenCvSharp.Point2f p2)
        {
            return Math.Sqrt((p1.X - p2.X) * (p1.X - p2.X) + (p1.Y - p2.Y) * (p1.Y - p2.Y));
        }
        private static int Ccw(in OpenCvSharp.Point2f p1, in OpenCvSharp.Point2f p2, in OpenCvSharp.Point2f p3)
        {
            int cross_product = (int)((p2.X - p1.X) * (p3.Y - p1.Y) - (p3.X - p1.X) * (p2.Y - p1.Y));

            if (cross_product > 0)
            {
                return 1;
            }
            else if (cross_product < 0)
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }
        /// <summary>
        /// 두 선분이 교차하면 1을 교차하지 않으면 0을 반환합니다.
        /// </summary>
        /// <param name="l1"></param>
        /// <param name="l2"></param>
        /// <returns></returns>
        private static int LineIntersection(CvSharpLine l1, CvSharpLine l2)
        {
            int ret;
            // l1을 기준으로 l2가 교차하는 지 확인한다.
            // Check if l2 intersects based on l1.
            int l1_l2 = Ccw(l1.p1, l1.p2, l2.p1) * Ccw(l1.p1, l1.p2, l2.p2);
            // l2를 기준으로 l1이 교차하는 지 확인한다.
            // Check if l1 intersects based on l2.
            int l2_l1 = Ccw(l2.p1, l2.p2, l1.p1) * Ccw(l2.p1, l2.p2, l1.p2);

            ret = ((l1_l2 < 0) && (l2_l1 < 0)) ? 1 : 0;

            return ret;
        }
        /// <summary>
        /// Polygon 영역의 무게중심 계산
        /// </summary>
        /// <param name="poly"></param>
        /// <returns></returns>
        private static OpenCvSharp.Point2f GetCentroidPolygon(in List<OpenCvSharp.Point2f> poly)
        {
            if (poly.Count == 0)
                return new OpenCvSharp.Point2f(-1.0f, -1.0f);

            float cx = 0, cy = 0, area = 0;
            for (int i = 0; i < poly.Count; i++)
            {
                OpenCvSharp.Point2f pt1 = poly[i];
                OpenCvSharp.Point2f pt2 = poly[(i + 1) % poly.Count];

                area += pt1.X * pt2.Y;
                area -= pt1.Y * pt2.X;

                cx += ((pt1.X + pt2.X) * ((pt1.X * pt2.Y) - pt2.X * pt1.Y));
                cy += ((pt1.Y + pt2.Y) * ((pt1.X * pt2.Y) - pt2.X * pt1.Y));
            }

            area /= 2.0F;
            area = Math.Abs(area);

            return new OpenCvSharp.Point2f((float)(cx / (6.0 * area)), (float)(cy / (6.0 * area)));
        }
        /// <summary>
        /// Polygon 좌표 중에 기준 좌표에서 직선거리가 가장 먼 좌표를 획득
        /// </summary>
        /// <param name="poly"></param>
        /// <param name="pt"></param>
        /// <returns></returns>
        private static OpenCvSharp.Point2f GetFarthestPoint(in List<OpenCvSharp.Point2f> poly, in OpenCvSharp.Point2f pt)
        {
            double dLongest = 0;
            OpenCvSharp.Point2f ptResult = new OpenCvSharp.Point2f();

            for (int i = 0; i < poly.Count; i++)
            {
                double _dist = EuclideanDistance(poly[i], pt);
                if (dLongest < _dist)
                {
                    dLongest = _dist;
                    ptResult = poly[i];
                }
            }

            return ptResult;
        }
        public static OpenCvSharp.Rect2f GetCentroidRectangleInsidePolygon(in List<OpenCvSharp.Point2f> ptsPoly, in double dRatioWH)
        {
            OpenCvSharp.Point2f ptCenter = GetCentroidPolygon(ptsPoly);

            OpenCvSharp.Point2f ptFarthest = GetFarthestPoint(ptsPoly, ptCenter);

            double dIncW, dIncH;
            if (dRatioWH > 1.0) { dIncW = 1.0; dIncH = 1.0 / dRatioWH; }
            else { dIncW = dRatioWH; dIncH = 1.0; }

            double dX, dY;
            if (Math.Abs(ptCenter.X - ptFarthest.X) / Math.Abs(ptCenter.Y - ptFarthest.Y) > dRatioWH)
            {
                dX = Math.Abs(ptCenter.X - ptFarthest.X) - dIncW;
                dY = Math.Abs(ptCenter.X - ptFarthest.X) / dRatioWH - dIncH;
            }
            else
            {
                dX = Math.Abs(ptCenter.Y - ptFarthest.Y) * dRatioWH - dIncW;
                dY = Math.Abs(ptCenter.Y - ptFarthest.Y) - dIncH;
            }

            while (dX > 1.0 || dY > 1.0)
            {
                List<OpenCvSharp.Point2f> corners = new List<OpenCvSharp.Point2f>();
                corners.Add(new OpenCvSharp.Point2f((float)(ptCenter.X - dX), (float)(ptCenter.Y - dY)));
                corners.Add(new OpenCvSharp.Point2f((float)(ptCenter.X + dX), (float)(ptCenter.Y - dY)));
                corners.Add(new OpenCvSharp.Point2f((float)(ptCenter.X + dX), (float)(ptCenter.Y + dY)));
                corners.Add(new OpenCvSharp.Point2f((float)(ptCenter.X - dX), (float)(ptCenter.Y + dY)));

                bool bInt = false;
                for (int i = 0; i < corners.Count; i++)
                {
                    CvSharpLine sideR;
                    sideR.p1 = corners[i];
                    sideR.p2 = corners[(i + 1) % corners.Count];

                    for (int j = 0; j < ptsPoly.Count; j++)
                    {
                        CvSharpLine sideP;
                        sideP.p1 = ptsPoly[j];
                        sideP.p2 = ptsPoly[(j + 1) % ptsPoly.Count];

                        if (LineIntersection(sideR, sideP) == 1)
                        {
                            bInt = true;
                            break;
                        }
                    }
                    if (bInt) break;
                }

                if (!bInt)
                {
                    OpenCvSharp.Rect2f rtInside;
                    rtInside.X = (float)(ptCenter.X - dX);
                    rtInside.Y = (float)(ptCenter.Y - dY);
                    rtInside.Width = (float)(dX * 2 + 1);
                    rtInside.Height = (float)(dY * 2 + 1);

                    return rtInside;
                }

                dX -= dIncW;
                dY -= dIncH;
            }


            return new OpenCvSharp.Rect2f();
        }
    }
}
