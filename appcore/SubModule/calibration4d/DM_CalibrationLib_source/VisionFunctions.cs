﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenCvSharp;

namespace FDUtilities
{
   partial class VisionFunctions
    {
        public static Mat imgWarp_px2(in Mat _img, in Point _px1, in Point _px2, in List<Point2d> _vpx4)
        {///
            Mat img_ = new Mat();
            Point pxDif = new Point(_px1.X - _px2.X, _px1.Y - _px2.Y);
            double dNorm = Math.Sqrt(pxDif.X * pxDif.X + pxDif.Y * pxDif.Y);
            if (dNorm > 0.0)
            {
                Size sz = new Size(Math.Abs(_vpx4[2].X -_vpx4[0].X), Math.Abs(_vpx4[2].Y - _vpx4[0].Y));
                List<Point> vpx4Scaled = vpx4Scal_szGuidHeight(sz, dNorm);

                float th = (float) (0.5 * Cv2.PI + Math.Atan2((float)(pxDif.Y), (float)(pxDif.X))), cos_th = (float)(Math.Cos(th)), sin_th = (float)(Math.Sin(th));
                Mat mBased0 = PointListToMat(vpx4Scaled, vpx4Scaled.Count, 2);
                
                Mat mBased2 = new Mat(4, 2, MatType.CV_32F);
                mBased2.At<float>(0, 0) = vpx4Scaled[0].X;
                mBased2.At<float>(0, 1) = vpx4Scaled[0].Y;

                mBased2.At<float>(1, 0) = vpx4Scaled[1].X;
                mBased2.At<float>(1, 1) = vpx4Scaled[1].Y;

                mBased2.At<float>(2, 0) = vpx4Scaled[2].X;
                mBased2.At<float>(2, 1) = vpx4Scaled[2].Y;

                mBased2.At<float>(3, 0) = vpx4Scaled[3].X;
                mBased2.At<float>(3, 1) = vpx4Scaled[3].Y;

                Mat mBased1 = mBased2.T();

                Mat mRot2d = new Mat(2, 2, MatType.CV_32F);
                mRot2d.At<float>(0, 0) = cos_th;
                mRot2d.At<float>(0, 1) = -sin_th;
                mRot2d.At<float>(1, 0) = sin_th;
                mRot2d.At<float>(1, 1) = cos_th;


                Mat mvpt4 = (mRot2d * mBased1).ToMat();

                List<Point2d> vpt4rot = new List<Point2d>(4);
                Point2f ptDif_2 = pxDif * 0.5f, ptDifCent = _px2 + ptDif_2;
                for (int i = 0; i < 4; ++i)
                {
                    float x = mvpt4.At<float>(0, i) + ptDifCent.X;
                    float y = mvpt4.At<float>(1, i) + ptDifCent.Y;
                    //vpt4rot[i] = new Point2d(x, y);
                    vpt4rot.Add(new Point2d(x, y));
                }

                Mat mHmg = Cv2.FindHomography(vpt4rot, _vpx4, HomographyMethods.Ransac);

                try
                {
                    Cv2.WarpPerspective(_img, img_, mHmg, _img.Size());
                }
                catch (Exception e2)
                {

                }
            }
            else img_ = _img.Clone();

            return img_;
        }
        public static Mat sub_imgpxsz(in Mat _img, in Point _px, in Size _wh)
        {
            Size sz_2 = new Size(_wh.Width >> 1, _wh.Height >> 1);
            Point px_2 = new Point(_px.X - sz_2.Width, _px.Y - sz_2.Height);
            return _img.SubMat(new Rect(px_2, _wh)).Clone();
        }
        public static Point px_imgsub(in Mat _img, in Mat _sub)
        {
            Mat res = new Mat(), mask = new Mat();

            int result_cols = (_img.Cols - _sub.Cols + 1);
            int result_rows = _img.Rows - _sub.Rows + 1;
            res.Create(result_rows, result_cols, MatType.CV_32FC1);

            TemplateMatchModes match_method = TemplateMatchModes.SqDiff;
            bool use_mask = false;

            bool method_accepts_mask = (match_method == TemplateMatchModes.SqDiff || match_method == TemplateMatchModes.CCorrNormed);
            if (use_mask && method_accepts_mask) 
                Cv2.MatchTemplate(_img, _sub, res, match_method, mask);
            else
                Cv2.MatchTemplate(_img, _sub, res, match_method);

            Cv2.Normalize(res, res, 0, 1, NormTypes.MinMax, -1, new Mat());

            double minVal, maxVal;
            Point minLoc, maxLoc, px_ = new Point(0,0);
            Cv2.MinMaxLoc(res, out minVal, out maxVal, out minLoc, out maxLoc, new Mat());

            if (match_method == TemplateMatchModes.SqDiff || match_method == TemplateMatchModes.CCorrNormed) px_ = minLoc; else px_ = maxLoc;

            return px_;
        }
        public static Point pxCnt_imgsub(in Mat _img, in Mat _sub)
        {
            int r_2 = _sub.Cols >> 1;
            Point px = px_imgsub(_img, _sub);
            return new Point(px.X + r_2, px.Y + r_2);
        }
    }
}
