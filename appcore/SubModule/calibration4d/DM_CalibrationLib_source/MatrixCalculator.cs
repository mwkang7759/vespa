﻿using OpenCvSharp;
using System;

namespace FDUtilities
{
    class MatrixCalculator
    {
        /// <summary>
        /// Tranlation Matrix 계산
        /// </summary>
        /// <param name="tx"></param>
        /// <param name="ty"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        public static bool GetTranslationMatrix(in float tx, in float ty, out Mat m)
        {
            m = Mat.Eye(3, 3, MatType.CV_32FC1);

            if (tx == 0 && ty == 0)
                return false;

            m.Set<float>(0, 2, tx);
            m.Set<float>(1, 2, ty);

            return true;
        }
        /// <summary>
        /// Rotation Matrix 계산 (원점 중심)
        /// </summary>
        /// <param name="rad"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        public static bool GetRotationMatrix(in float rad, out Mat m)
        {
            m = Mat.Eye(3, 3, MatType.CV_32FC1);

            if (rad == 0)
                return false;

            m.Set<float>(0, 0, (float)Math.Cos(rad));
            m.Set<float>(0, 1, (float)-Math.Sin(rad));
            m.Set<float>(1, 0, (float)Math.Sin(rad));
            m.Set<float>(1, 1, (float)Math.Cos(rad));

            return true;
        }
        /// <summary>
        /// Rotation Matrix 계산 (특정좌표 중심)
        /// </summary>
        /// <param name="rad"></param>
        /// <param name="cx"></param>
        /// <param name="cy"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        public static bool GetRotationMatrix(in float rad, in float cx, in float cy, out Mat m)
        {
            m = Mat.Eye(3, 3, MatType.CV_32FC1);

            if (rad == 0)
                return false;

            Mat mTrnA, mTrnB, mRot;

            GetTranslationMatrix(-cx, -cy, out mTrnA);
            GetRotationMatrix(rad, out mRot);
            GetTranslationMatrix(cx, cy, out mTrnB);

            m = mTrnB * mRot * mTrnA;

            return true;
        }
        /// <summary>
        /// Scaling Matrix 계산 (원점 중심)
        /// </summary>
        /// <param name="scaleX"></param>
        /// <param name="scaleY"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        public static bool GetScaleMarix(in float scaleX, in float scaleY, out Mat m)
        {
            m = Mat.Eye(3, 3, MatType.CV_32FC1);

            if (scaleX == 0.0 || scaleY == 0.0)
                return false;

            m.Set<float>(0, 0, scaleX);
            m.Set<float>(1, 1, scaleY);

            return true;
        }
        /// <summary>
        /// Scaling Matrix 계산 (특정좌표 중심)
        /// </summary>
        /// <param name="scaleX"></param>
        /// <param name="scaleY"></param>
        /// <param name="cx"></param>
        /// <param name="cy"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        public static bool GetScaleMarix(in float scaleX, in float scaleY, in float cx, in float cy, out Mat m)
        {
            m = Mat.Eye(3, 3, MatType.CV_32FC1);

            if (scaleX == 0.0 || scaleY == 0.0)
                return false;

            Mat mTrnA, mTrnB, mScale;

            GetTranslationMatrix(-cx, -cy, out mTrnA);
            GetScaleMarix(scaleX, scaleY, out mScale);
            GetTranslationMatrix(cx, cy, out mTrnB);

            m = mTrnB * mScale * mTrnA;

            return true;
        }
        /// <summary>
        /// Flip Matrix 계산
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="isFlipX"></param>
        /// <param name="isFlipY"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        public static bool GetFlipMatrix(in int width, in int height, in bool isFlipX, in bool isFlipY, out Mat m)
        {
            m = Mat.Eye(3, 3, MatType.CV_32FC1);

            if (!isFlipX && !isFlipY)
                return false;

            if (isFlipX)
            {
                m.Set<float>(0, 0, -1.0F);
                m.Set<float>(0, 2, width);
            }
            if (isFlipY)
            {
                m.Set<float>(1, 1, -1.0F);
                m.Set<float>(1, 2, height);
            }

            return true;
        }
        /// <summary>
        /// Margin Crop을 위한 Translation/Scaling Matrix 계산
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="marginX"></param>
        /// <param name="marginY"></param>
        /// <param name="marginW"></param>
        /// <param name="marginH"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        public static bool GetMarginMatrix(in int width, in int height, in int marginX, in int marginY, in int marginW, in int marginH, out Mat m)
        {
            m = Mat.Eye(3, 3, MatType.CV_32FC1);

            if (width <= 0 || height <= 0 || marginX < 0 || marginY < 0 || marginW <= 0 || marginH <= 0)
                return false;

            float fCenterX = marginX + marginW / 2.0F;
            float fCenterY = marginY + marginH / 2.0F;
            float fScaleX = width / (float)marginW;
            float fScaleY = height / (float)marginH;

            Mat mTrnA, mTrnB, mScale;

            GetTranslationMatrix(-fCenterX, -fCenterY, out mTrnA);
            GetScaleMarix(fScaleX, fScaleY, out mScale);
            GetTranslationMatrix(width / 2.0F, height / 2.0F, out mTrnB);

            m = mTrnB * mScale * mTrnA;

            return true;
        }
        /// <summary>
        /// 3x3 Matrix을 Affine transform을 위한 2x3 Matrix로 변환
        /// </summary>
        /// <param name="m33"></param>
        /// <param name="m23"></param>
        /// <returns></returns>
        public static bool ParseAffineMatrix(in Mat m33, out Mat m23)
        {
            m23 = Mat.Eye(2, 3, MatType.CV_32FC1);

            if (m33.Cols != 3 || m33.Rows != 3)
                return false;

            m23 = m33.SubMat(0, 2, 0, 3);

            return true;
        }
    }
}
