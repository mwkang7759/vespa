﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace FDUtilities
{

    public enum CalibrationMode
    {
        Mode2D,
        Mode3D,
        Mode2DArea
    }
    public enum CalPos2D
    {
        Upper,
        Middle,
        Lower
    }
    public enum CalPos3D
    {
        First,
        Second,
        Third,
        Forth,
        Center
    }
    public enum CalPosWorld
    {
        First,
        Second,
        Third,
        Forth
    }


    public abstract class FDShape
    {
        public SolidBrush BrushType { get; set; }
        public Pen PenType { get; set; }
        public abstract void Draw(Graphics graphics, double scale, double offsetX, double offsetY);
    }

    public class FDLine : FDShape
    {
        public double X1 { get; set; }
        public double Y1 { get; set; }
        public double X2 { get; set; }
        public double Y2 { get; set; }

        public override void Draw(Graphics graphics, double scale, double offsetX, double offsetY)
        {
            if (X1 < 0 || Y1 < 0 || X2 < 0 || Y2 < 0)
                return;

            int _x1, _y1, _x2, _y2;
            _x1 = (int)Math.Round((X1 - offsetX) * scale);
            _y1 = (int)Math.Round((Y1 - offsetY) * scale);
            _x2 = (int)Math.Round((X2 - offsetX) * scale);
            _y2 = (int)Math.Round((Y2 - offsetY) * scale);

            graphics.DrawLine(PenType, _x1, _y1, _x2, _y2);
        }
    }

    public class FDCross : FDShape
    {
        public string Label { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public double Size { get; set; }

        public override void Draw(Graphics graphics, double scale, double offsetX, double offsetY)
        {
            if (X < 0 || Y < 0 || Size <= 0.0F)
                return;

            int _x, _y;
            _x = (int)Math.Round((X - offsetX) * scale);
            _y = (int)Math.Round((Y - offsetY) * scale);

            graphics.DrawLine(PenType, (int)(_x - Size / 2), _y, (int)(_x + Size / 2), _y);
            graphics.DrawLine(PenType, _x, (int)(_y - Size / 2), _x, (int)(_y + Size / 2));

            if (!string.IsNullOrWhiteSpace(Label))
                graphics.DrawString(Label, new Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0),
                   PenType.Brush, new PointF((int)(_x + Size / 2), (int)(_y + Size / 2)));
        }
    }


    public class FDRectangle : FDShape
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }

        public override void Draw(Graphics graphics, double scale, double offsetX, double offsetY)
        {
            if (X < 0 || Y < 0 || Width <= 0 || Height <= 0)
                return;

            int _x, _y, _width, _height;
            _x = (int)Math.Round((X - offsetX) * scale);
            _y = (int)Math.Round((Y - offsetY) * scale);
            _width = (int)Math.Round(Width * scale);
            _height = (int)Math.Round(Height * scale);

            graphics.DrawRectangle(PenType, new Rectangle((int)X, (int)Y, (int)Width, (int)Height));
        }
    }

    public class FDDrawingManager2D
    {
        public int mode2DPointCount = 3;
        public int Mode2DPointCount
        {
            get => mode2DPointCount; set
            {
                if (mode2DPointCount == value)
                    return;
                mode2DPointCount = value;
            }
        }
        public FDDrawingManager2D()
        {
        }

        public FDCross ReferPointUpper { get; set; }
        public FDCross ReferPointMiddle { get; set; }
        public FDCross ReferPointLower { get; set; }
        public FDLine SeparatorUpper { get; set; }
        public FDLine SeparatorLower { get; set; }

        public void ClearReferPoint()
        {
            ReferPointUpper = null;
            ReferPointMiddle = null;
            ReferPointLower = null;
        }
        public void MoveUpperSeparator(int diff)
        {
            SeparatorUpper.Y1 += diff;
            SeparatorUpper.Y2 += diff;
        }
        public void MoveLowerSeparator(int diff)
        {
            SeparatorLower.Y1 += diff;
            SeparatorLower.Y2 += diff;
        }

        public void Draw(Graphics graphics, double scale, double offsetX, double offsetY)
        {
            if (ReferPointUpper != null)
                ReferPointUpper.Draw(graphics, scale, offsetX, offsetY);

            if (ReferPointLower != null)
                ReferPointLower.Draw(graphics, scale, offsetX, offsetY);
            if (SeparatorUpper != null)
                SeparatorUpper.Draw(graphics, scale, offsetX, offsetY);

            if (Mode2DPointCount <= 2)
                return;

            if (ReferPointMiddle != null)
                ReferPointMiddle.Draw(graphics, scale, offsetX, offsetY);
            if (SeparatorLower != null)
                SeparatorLower.Draw(graphics, scale, offsetX, offsetY);
        }
    }

    public class FDDrawingManager3D
    {
        public FDDrawingManager3D()
        {
            ReferPoints = new List<FDCross>();
        }

        public List<FDCross> ReferPoints { get; set; }
        public FDCross CenterPoint { get; set; }
        public int ManualOffsetY { get; set; } = 0;
        public void ClearReferPoint()
        {
            ReferPoints.Clear();
            CenterPoint = null;
            ManualOffsetY = 0;
        }

        public void Draw(Graphics graphics, double scale, double offsetX, double offsetY)
        {
            for (int i = 0; i < ReferPoints.Count; i++)
            {
                int idxBegin = i;
                int idxEnd = (i + 1) % 4;
                if (ReferPoints.Count > idxEnd)
                {
                    FDLine line;
                    line = new FDLine
                    {
                        PenType = new Pen(Color.Red, 2),
                        X1 = ReferPoints[idxBegin].X,
                        Y1 = ReferPoints[idxBegin].Y,
                        X2 = ReferPoints[idxEnd].X,
                        Y2 = ReferPoints[idxEnd].Y
                    };
                    line.Draw(graphics, scale, offsetX, offsetY);

                    if (ManualOffsetY != 0)
                    {
                        FDLine offsetLine;
                        offsetLine = new FDLine
                        {
                            PenType = new Pen(Color.FromArgb(100, 255, 255, 255), 4),
                            X1 = ReferPoints[idxBegin].X,
                            Y1 = ReferPoints[idxBegin].Y + ManualOffsetY,
                            X2 = ReferPoints[idxEnd].X,
                            Y2 = ReferPoints[idxEnd].Y + ManualOffsetY,
                        };
                        offsetLine.Draw(graphics, scale, offsetX, offsetY);
                        FDLine offsetLinehighlite;
                        offsetLinehighlite = new FDLine
                        {
                            PenType = new Pen(Color.Black, 2),
                            X1 = ReferPoints[idxBegin].X,
                            Y1 = ReferPoints[idxBegin].Y + ManualOffsetY,
                            X2 = ReferPoints[idxEnd].X,
                            Y2 = ReferPoints[idxEnd].Y + ManualOffsetY,
                        };
                        offsetLinehighlite.Draw(graphics, scale, offsetX, offsetY);
                    }
                }

                ReferPoints[i].Draw(graphics, scale, offsetX, offsetY);
            }

            if (CenterPoint != null)
                CenterPoint.Draw(graphics, scale, offsetX, offsetY);
        }
    }
    public class FDDrawingManagerWorld
    {
        public FDDrawingManagerWorld()
        {
            WorldCoordinates = new List<FDCross>();
        }

        public List<FDCross> WorldCoordinates { get; set; }

        public void ClearCoodinates()
        {
            WorldCoordinates.Clear();
        }

        public void Draw(Graphics graphics, double scale, double offsetX, double offsetY)
        {
            for (int i = 0; i < WorldCoordinates.Count; i++)
            {
                if (WorldCoordinates.Count > (i + 1) % 4)
                {
                    FDLine line;
                    line = new FDLine { PenType = new Pen(Color.Red, 2), X1 = WorldCoordinates[i].X, Y1 = WorldCoordinates[i].Y, X2 = WorldCoordinates[(i + 1) % 4].X, Y2 = WorldCoordinates[(i + 1) % 4].Y };

                    line.Draw(graphics, scale, offsetX, offsetY);
                }

                WorldCoordinates[i].Draw(graphics, scale, offsetX, offsetY);
            }
        }
    }
}
