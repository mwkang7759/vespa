﻿using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FDUtilities
{
    class TemplateLine
    {
        public Point2f P1;
        public Point2f P2;
    }

    class TemplateCalculator
    {
        private Point2f PtPos = new Point2f();

        private long dist(in Point2f p1, in Point2f p2)
        {
            return (long)((p1.X - p2.X) * (p1.X - p2.X) + (p1.Y - p2.Y) * (p1.Y - p2.Y));
        }
        private int ccw(in Point2f p1, in Point2f p2, in Point2f p3)
        {
            int cross_product = (int)((p2.X - p1.X) * (p3.Y - p1.Y) - (p3.X - p1.X) * (p2.Y - p1.Y));

            if (cross_product > 0)
                return 1;
            else if (cross_product < 0)
                return -1;
            else
                return 0;
        }
        // 입력받은 Point 구조체의 값을 교환합니다.
        // Exchanges the value of the input Point structure.
        private void swap(ref Point2f p1, ref Point2f p2)
        {
            Point2f temp;
            temp = p1;
            p1 = p2;
            p2 = temp;
        }
        // right가 left의 반시계 방향에 있으면 true이다.
        // true if right is counterclockwise to left.
        private bool SortComparator(in Point2f left, in Point2f right)
        {
            int direction = ccw(PtPos, left, right);
            if (direction == 0)
            {
                return dist(PtPos, left) < dist(PtPos, right);
            }
            else if (direction == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void QuickSort(ref List<Point2f> a, int lo, int hi)
        {
            if (hi - lo <= 0)
            {
                return;
            }

            // 현재 배열 범위의 중앙값을 피벗으로 선택한다.
            // Select the median as pivot in the current array range.
            Point2f pivot = a[lo + (hi - lo + 1) / 2];
            int i = lo, j = hi;

            // 정복 과정
            // Conquer process
            while (i <= j)
            {
                // 피벗의 왼쪽에는 comparator(타겟, "피벗")을 만족하지 않는 인덱스를 선택 (i)
                // On the left side of the pivot, select an index that doesn't satisfy the comparator(target, "pivot"). (i)
                while (SortComparator(a[i], pivot)) i++;

                // 피벗의 오른쪽에는 comparator("피벗", 타겟)을 만족하지 않는 인덱스를 선택 (j)
                // On the right side of the pivot, select an index that doesn't satisfy the comparator("pivot", target). (j)
                while (SortComparator(pivot, a[j])) j--;
                // (i > j) 피벗의 왼쪽에는 모든 값이 피벗보다 작고 피벗의 오른쪽에는 모든 값이 피벗보다 큰 상태가 되었음.
                // (i > j) On the left side of the pivot, all values are smaller than the pivot, and on the right side of the pivot, all values are larger than the pivot.
                if (i > j)
                {
                    break;
                }

                // i번째 값은 피벗 보다 크고 j번째 값은 피벗보다 작으므로 두 값을 스왑한다.
                // The i-th value is larger than the pivot and the j-th value is smaller than the pivot, so swap the two values.
                Point2f temp = a[i];
                a[i] = a[j];
                a[j] = temp;

                // 인덱스 i를 1증가 시키고 인덱스 j를 1 감소 시켜서 탐색 범위를 안쪽으로 좁힌다.
                // Narrow the search inward by increasing index i by one and decreasing index j by one.
                i++;
                j--;
            }

            // 분할 과정
            // Divide process
            QuickSort(ref a, lo, j);
            QuickSort(ref a, i, hi);
        }
        // right가 left 보다 크면 true를 반환합니다.
        // if right is greater than left, it is true
        private bool LineComparator(Point2f left, Point2f right)
        {
            bool ret;
            if (left.X == right.X)
            {
                ret = (left.Y <= right.Y);
            }
            else
            {
                ret = (left.X <= right.X);
            }
            return ret;
        }
        // 두 선분이 교차하면 1을 교차하지 않으면 0을 반환합니다.
        // If the two segments intersect, they will return 1 if they do not intersect 0.
        private bool LineIntersection(TemplateLine l1, TemplateLine l2)
        {
            bool ret;
            // l1을 기준으로 l2가 교차하는 지 확인한다.
            // Check if l2 intersects based on l1.
            int l1_l2 = ccw(l1.P1, l1.P2, l2.P1) * ccw(l1.P1, l1.P2, l2.P2);
            // l2를 기준으로 l1이 교차하는 지 확인한다.
            // Check if l1 intersects based on l2.
            int l2_l1 = ccw(l2.P1, l2.P2, l1.P1) * ccw(l2.P1, l2.P2, l1.P2);

            ret = (l1_l2 < 0) && (l2_l1 < 0);

            return ret;
        }
        private int PolygonInOut(Point2f p, int num_vertex, ref Point2f[] vertices)
        {
            int nRet = 0;

            // 마지막 꼭지점과 첫번째 꼭지점이 연결되어 있지 않다면 오류를 반환한다.
            // If the last vertex and the first vertex are not connected, an error is returned.
            if (vertices[0].X != vertices[num_vertex].X || vertices[0].Y != vertices[num_vertex].Y)
            {
                // Last vertex and first vertex are not connected.
                return -1;
            }

            for (int i = 0; i < num_vertex; ++i)
            {

                // 점 p가 i번째 꼭지점과 i+1번째 꼭지점을 이은 선분 위에 있는 경우
                // If point p is on the line connecting the i and i + 1 vertices
                if (ccw(vertices[i], vertices[i + 1], p) == 0)
                {
                    int min_x = (int)Math.Min(vertices[i].X, vertices[i + 1].X);
                    int max_x = (int)Math.Max(vertices[i].X, vertices[i + 1].X);
                    int min_y = (int)Math.Min(vertices[i].Y, vertices[i + 1].Y);
                    int max_y = (int)Math.Max(vertices[i].Y, vertices[i + 1].Y);

                    // 점 p가 선분 내부의 범위에 있는 지 확인
                    // Determine if point p is in range within line segment
                    if (min_x <= p.X && p.X <= max_x && min_y <= p.Y && p.Y <= max_y)
                    {
                        return 1;
                    }
                }
                else {; }
            }

            // 다각형 외부에 임의의 점과 점 p를 연결한 선분을 만든다.
            // Create a line segment connecting a random point outside the polygon and point p.
            Point2f outside_point;
            outside_point.X = 1; outside_point.Y = 1234567;
            TemplateLine l1 = new TemplateLine();
            l1.P1 = outside_point;
            l1.P2 = p;

            // 앞에서 만든 선분과 다각형을 이루는 선분들이 교차되는 갯수가 센다.
            // Count the number of intersections between the previously created line segments and the polygonal segments.
            for (int i = 0; i < num_vertex; ++i)
            {
                TemplateLine l2 = new TemplateLine();
                l2.P1 = vertices[i];
                l2.P2 = vertices[i + 1];
                nRet += LineIntersection(l1, l2) ? 1 : 0;
            }

            // 교차한 갯수가 짝수인지 홀수인지 확인한다.
            // Check if the number of crossings is even or odd.
            nRet = nRet % 2;
            return nRet;
        }

        // (p1, p2)를 이은 직선과 (p3, p4)를 이은 직선의 교차점을 구하는 함수
        // Function to get intersection point with line connecting points (p1, p2) and another line (p3, p4).
        private Point2f IntersectionPoint(in Point2f p1, in Point2f p2, in Point2f p3, in Point2f p4)
        {

            Point2f ret;

            ret.X = ((p1.X * p2.Y - p1.Y * p2.X) * (p3.X - p4.X) - (p1.X - p2.X) * (p3.X * p4.Y - p3.Y * p4.X)) / ((p1.X - p2.X) * (p3.Y - p4.Y) - (p1.Y - p2.Y) * (p3.X - p4.X));

            ret.Y = ((p1.X * p2.Y - p1.Y * p2.X) * (p3.Y - p4.Y) - (p1.Y - p2.Y) * (p3.X * p4.Y - p3.Y * p4.X)) / ((p1.X - p2.X) * (p3.Y - p4.Y) - (p1.Y - p2.Y) * (p3.X - p4.X));

            return ret;
        }

        // 다각형의 넓이를 구한다.
        // find the area of a polygon
        private double GetPolygonArea(in List<Point2f> points)
        {
            double ret = 0;
            int i, j;
            i = points.Count - 1;
            for (j = 0; j < points.Count; ++j)
            {
                ret += points[i].X * points[j].Y - points[j].X * points[i].Y;
                i = j;
            }
            ret = ret < 0 ? -ret : ret;
            ret /= 2;

            return ret;
        }

        public bool isInside(Point2f B, ref List<Point2f> p)
        {
            //crosses는 점q와 오른쪽 반직선과 다각형과의 교점의 개수
            int crosses = 0;
            for (int i = 0; i < p.Count; i++)
            {
                int j = (i + 1) % p.Count;
                //점 B가 선분 (p[i], p[j])의 y좌표 사이에 있음
                if ((p[i].Y > B.Y) != (p[j].Y > B.Y))
                {
                    //atX는 점 B를 지나는 수평선과 선분 (p[i], p[j])의 교점
                    double atX = (p[j].X - p[i].X) * (B.Y - p[i].Y) / (p[j].Y - p[i].Y) + p[i].X;
                    //atX가 오른쪽 반직선과의 교점이 맞으면 교점의 개수를 증가시킨다.
                    if (B.X < atX)
                        crosses++;
                }
            }
            return crosses % 2 > 0;
        }

        public List<Point2f> GetIntersection(List<Point2f> points1, List<Point2f> points2)
        {
            TemplateLine l1 = new TemplateLine(), l2 = new TemplateLine();

            // points1과 points2 각각을 반시계방향으로 정렬한다.
            // sort by counter clockwise point1 and points2.
            PtPos = points1[0];
            QuickSort(ref points1, 1, points1.Count - 1);

            PtPos = points2[0];
            QuickSort(ref points2, 1, points2.Count - 1);

            // 차례대로 점들을 이었을 때, 다각형이 만들어질 수 있도록 시작점을 마지막에 추가한다.
            // Add the starting point to the last in order to make polygon when connect points in order.
            points1.Add(points1[0]);
            points2.Add(points2[0]);


            List<Point2f> interPoints = new List<Point2f>();

            // points1의 다각형 선분들과 points2의 다각형 선분들의 교차점을 구한다.
            // Find the intersection of the polygon segments of points1 and the polygon segments of points2.
            for (int i = 0; i < points1.Count() - 1; ++i)
            {
                l1.P1 = points1[i];
                l1.P2 = points1[i + 1];
                for (int j = 0; j < points2.Count() - 1; ++j)
                {
                    l2.P1 = points2[j];
                    l2.P2 = points2[j + 1];

                    // 선분 l1과 l2가 교차한다면 교차점을 intersection_point에 저장한다.
                    // If line segments l1 and l2 intersect, store the intersection at intersection_point.
                    if (LineIntersection(l1, l2))
                    {
                        interPoints.Add(IntersectionPoint(l1.P1, l1.P2, l2.P1, l2.P2));
                    }
                }
            }

            for (int i = 0; i < points1.Count - 1; i++)
            {
                if (isInside(points1[i], ref points2))
                {
                    interPoints.Add(new Point2f(points1[i].X, points1[i].Y));
                }
            }
            for (int i = 0; i < points2.Count - 1; i++)
            {
                if (isInside(points2[i], ref points1))
                {
                    interPoints.Add(new Point2f(points2[i].X, points2[i].Y));
                }
            }

            PtPos = interPoints[0];
            QuickSort(ref interPoints, 1, interPoints.Count - 1);

            return interPoints;
        }

        public Point2f GetNearPointPolygon(Point2f pt, List<Point2f> vPoints)
        {
            Point2f ptNear = new Point2f();

            int nSize = vPoints.Count;

            long nDistMin = 999999999;

            for (int i = 0; i < nSize; i++)
            {
                Point2f ptA, ptB, ptCross;
                ptA = vPoints[i];
                ptB = vPoints[(i + 1) % nSize];

                if (ptA.X == ptB.X)
                {
                    ptCross.X = ptA.X;
                    ptCross.Y = pt.Y;
                }
                else if (ptA.Y == ptB.Y)
                {
                    ptCross.X = pt.X;
                    ptCross.Y = ptA.Y;
                }
                else
                {
                    double m1, m2, k1, k2;

                    // 기울기 m1
                    m1 = (ptA.Y - ptB.Y) / (ptA.X - ptB.X);
                    // 상수 k1
                    k1 = -m1 * ptA.X + ptA.Y;

                    // 기울기 m2
                    m2 = -1.0 / m1;
                    // p 를 지나기 때문에 yp = m2 * xp + k2 => k2 = yp - m2 * xp
                    k2 = pt.Y - m2 * pt.X;

                    // 두 직선 y = m1x + k1, y = m2x + k2 의 교점을 구한다
                    ptCross.X = (float)((k2 - k1) / (m1 - m2));
                    ptCross.Y = (float)(m1 * ptCross.X + k1);
                }

                Point2f ptTemp;

                // 구한 점이 선분 위에 있는 지 확인
                if (ptCross.X >= Math.Min(ptA.X, ptB.X) && ptCross.X <= Math.Max(ptA.X, ptB.X) &&
                    ptCross.Y >= Math.Min(ptA.Y, ptB.Y) && ptCross.Y <= Math.Max(ptA.Y, ptB.Y))
                {
                    ptTemp = ptCross;
                }
                else
                {
                    if (dist(pt, ptA) < dist(pt, ptB))
                        ptTemp = ptA;
                    else
                        ptTemp = ptB;
                }

                if (dist(pt, ptTemp) < nDistMin)
                {
                    nDistMin = dist(pt, ptTemp);
                    ptNear = ptTemp;
                }
            }

            return ptNear;
        }

        private bool checkInteriorExterior(in Mat mask, in Rect interiorBB, ref int top, ref int bottom, ref int left, ref int right)
        {
            // return true if the rectangle is fine as it is!
            bool returnVal = true;

            Mat sub = mask.SubMat(interiorBB);

            int x = 0;
            int y = 0;

            // count how many exterior pixels are at the
            uint cTop = 0; // top row
            uint cBottom = 0; // bottom row
            uint cLeft = 0; // left column
            uint cRight = 0; // right column
                             // and choose that side for reduction where mose exterior pixels occured (that's the heuristic)

            for (y = 0, x = 0; x < sub.Cols; ++x)
            {
                // if there is an exterior part in the interior we have to move the top side of the rect a bit to the bottom
                if (sub.At<byte>(y, x) == 0)
                {
                    returnVal = false;
                    ++cTop;
                }
            }

            for (y = sub.Rows - 1, x = 0; x < sub.Cols; ++x)
            {
                // if there is an exterior part in the interior we have to move the bottom side of the rect a bit to the top
                if (sub.At<byte>(y, x) == 0)
                {
                    returnVal = false;
                    ++cBottom;
                }
            }

            for (y = 0, x = 0; y < sub.Rows; ++y)
            {
                // if there is an exterior part in the interior
                if (sub.At<byte>(y, x) == 0)
                {
                    returnVal = false;
                    ++cLeft;
                }
            }

            for (x = sub.Cols - 1, y = 0; y < sub.Rows; ++y)
            {
                // if there is an exterior part in the interior
                if (sub.At<byte>(y, x) == 0)
                {
                    returnVal = false;
                    ++cRight;
                }
            }

            // that part is ugly and maybe not correct, didn't check whether all possible combinations are handled. Check that one please. The idea is to set `top = 1` iff it's better to reduce the rect at the top than anywhere else.
            if (cTop > cBottom)
            {
                if (cTop > cLeft)
                    if (cTop > cRight)
                        top = 1;
            }
            else
                if (cBottom > cLeft)
                if (cBottom > cRight)
                    bottom = 1;

            if (cLeft >= cRight)
            {
                if (cLeft >= cBottom)
                    if (cLeft >= cTop)
                        left = 1;
            }
            else
                if (cRight >= cTop)
                if (cRight >= cBottom)
                    right = 1;

            return returnVal;
        }

        private static bool sortX(Point2f a, Point2f b)
        {
            return a.X < b.X;
        }

        private static bool sortY(Point2f a, Point2f b)
        {
            return a.Y < b.Y;
        }

        public Rect GetInnerRectInPolygon(int nWidth, int nHeight, List<Point2f> poly)
        {
            List<List<Point>> ListOfListOfPoint = new List<List<Point>>();
            List<Point> points = new List<Point>();

            for (int i = 0; i < poly.Count; i++)
            {
                points.Add(new Point(poly[i].X, poly[i].Y));
            }

            ListOfListOfPoint.Add(points);

            Mat contourMask = Mat.Zeros(new Size(nWidth, nHeight), MatType.CV_8UC1);
            contourMask.FillPoly(ListOfListOfPoint, new Scalar(255));

            // sort contour in x/y directions to easily find min/max and next
            List<Point2f> cSortedX = poly.OrderBy(p => p.X).ToList();
            List<Point2f> cSortedY = poly.OrderBy(p => p.Y).ToList();


            int minXId = 0;
            int maxXId = cSortedX.Count - 1;

            int minYId = 0;
            int maxYId = cSortedY.Count - 1;

            Rect interiorBB = new Rect();

            while ((minXId < maxXId) && (minYId < maxYId))
            {
                Point min = new Point((int)cSortedX[minXId].X, (int)cSortedY[minYId].Y);
                Point max = new Point((int)cSortedX[maxXId].X, (int)cSortedY[maxYId].Y);

                interiorBB = new Rect(min.X, min.Y, max.X - min.X, max.Y - min.Y);

                if (interiorBB.X < 0)
                    interiorBB.X = 0;
                if (interiorBB.X + interiorBB.Width > nWidth - 1)
                    interiorBB.Width = nWidth - interiorBB.X - 1;
                if (interiorBB.Y < 0)
                    interiorBB.Y = 0;
                if (interiorBB.Y + interiorBB.Height > nHeight - 1)
                    interiorBB.Height = nHeight - interiorBB.Y - 1;

                // out-codes: if one of them is set, the rectangle size has to be reduced at that border
                int ocTop = 0;
                int ocBottom = 0;
                int ocLeft = 0;
                int ocRight = 0;

                bool finished = checkInteriorExterior(contourMask, interiorBB, ref ocTop, ref ocBottom, ref ocLeft, ref ocRight);
                if (finished)
                {
                    break;
                }

                // reduce rectangle at border if necessary
                if (ocLeft != 0) ++minXId;
                if (ocRight != 0) --maxXId;

                if (ocTop != 0) ++minYId;
                if (ocBottom != 0) --maxYId;
            }

            Rect rtResult = new Rect(interiorBB.X, interiorBB.Y, interiorBB.Width, interiorBB.Height);

            if ((double)rtResult.Width / rtResult.Height > (double)nWidth / nHeight)
            {
                float centerX = (rtResult.X + rtResult.Width) / 2.0F;
                rtResult.Width = (int)(rtResult.Height * ((float)nWidth / nHeight));
                rtResult.X = (int)(centerX - rtResult.Width / 2.0F);
            }
            else
            {
                float centerY = (rtResult.Y + rtResult.Height) / 2.0F;
                rtResult.Height = (int)(rtResult.Width / ((float)nWidth / nHeight));
                rtResult.Y = (int)(centerY - rtResult.Height / 2.0F);
            }

            return rtResult;
        }
    }
}
