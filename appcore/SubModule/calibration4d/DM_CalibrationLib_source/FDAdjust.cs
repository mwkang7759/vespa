﻿using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace FDUtilities
{
    public enum StadiumType
    {
        BaseballHome,
        BaseballGround,
        BasketballHalf,
        BasketballGround,
        Boxing,
        IceLinkHalf,
        IceLink,
        SoccerHalf,
        Soccer,
        Taekwondo,
        TennisHalf,
        Tennis,
        Ufc,
        VolleyballHalf,
        VolleyballGround,
        Football
    };

    public class Homo_t
    {
        public double[] r1 { get; set; }

        public double[] r2 { get; set; }

        public double[] r3 { get; set; }
    }

    /// <summary>
    /// 임시로 사용할 DSC 정보
    /// </summary>
    public class DscInfo
    {
        public string DscID { get; set; }
        public string Group { get; set; }
        public string LiveGroup { get; set; }

        public int Width { get; set; } = 0;
        public int Height { get; set; } = 0;
        public bool IsReg { get; set; } = false;
        public bool IsAdj { get; set; } = false;

        public ReferencePoint2D Points2D = new ReferencePoint2D();
        public ReferencePoint3D Points3D = new ReferencePoint3D();
        public PreCalibrationData PreCalcData = new PreCalibrationData();

        public AdjustInfo Adjust = new AdjustInfo();

        private Mat srcImg;
        private Mat thumbImg;

        public Mat SrcImg
        {
            get => srcImg;
            set
            {
                srcImg?.Dispose();
                srcImg = value;

                thumbImg?.Dispose();
                thumbImg = null;


                _BmpImgThumb?.Dispose();
                _BmpImgThumb = null;

                if (srcImg == null) return;

                thumbImg = new Mat();
                if (srcImg.Height > 0 && srcImg.Width > 0)
                {
                    const int thumbHeight = 82;
                    Cv2.Resize(srcImg,
                        thumbImg,
                        new OpenCvSharp.Size((int)(thumbHeight * ((float)srcImg.Width / srcImg.Height)),
                            thumbHeight),
                        0, 0, InterpolationFlags.Area //축소시 선이 사라지지 않는 알고리즘
                    );
                }
            }
        }

        public enum ImageLoadState : int
        {
            NotYet = 0,
            LoadFail,
            Ignore,
            Loaded
        }

        public int SrcImgMode { get; set; } = (int)ImageLoadState.NotYet;

        public bool IsLoaded => (SrcImgMode == (int)ImageLoadState.Loaded && SrcImg != null && SrcImg.Width > 0);

        public bool IsCalDataLoaded { get; set; } = false;


        public Bitmap OrgImg
        {
            get
            {
                if (SrcImg == null) return null;

                try
                {
                    return OpenCvSharp.Extensions.BitmapConverter.ToBitmap(SrcImg);
                }
                catch
                {
                    return null;
                }
            }
        }

        public Bitmap BmpImg
        {
            get
            {
                if (SrcImg == null) return null;
                try
                {
                    if (!IsFlip) return OpenCvSharp.Extensions.BitmapConverter.ToBitmap(SrcImg);
                    Mat rotateMat = new Mat();
                    Cv2.Rotate(SrcImg, rotateMat, RotateFlags.Rotate180);
                    return OpenCvSharp.Extensions.BitmapConverter.ToBitmap(rotateMat);
                }
                catch
                {
                    return null;
                }
            }
        }

        private Bitmap _BmpImgThumb;

        public Bitmap BmpImgThumb
        {
            get
            {
                if (thumbImg == null || thumbImg.Empty()) return null;
                try
                {
                    if (_BmpImgThumb == null)
                    {
                        if (!IsFlip)
                            _BmpImgThumb = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(thumbImg);
                        else
                        {
                            Mat rotateMat = new Mat();
                            Cv2.Rotate(thumbImg, rotateMat, RotateFlags.Rotate180);
                            _BmpImgThumb = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(rotateMat);
                        }
                    }
                    return _BmpImgThumb;
                }
                catch
                {
                    return null;
                }
            }
        }

        private readonly Font infoDrawFont = new Font("Tahoma", 80, FontStyle.Regular);
        private readonly Font infoDrawFontShadow = new Font("Tahoma", 20, FontStyle.Bold);
        private readonly Brush infoDrawBrush = new SolidBrush(Color.Yellow);
        private readonly Brush infoDrawBrushShadow = new SolidBrush(Color.Black);

        private bool _isFlip;

        public bool IsFlip
        {
            get => _isFlip; set
            {
                if (_BmpImgThumb != null)
                {
                    _BmpImgThumb.Dispose();
                    _BmpImgThumb = null;
                }
                _isFlip = value;
            }
        }

        public bool UseLogo { get; set; } = true;
        public int LiveIndex { get; set; } = -1;

        public int CameraIndex { get; set; } = 0;
        public double SwipeBaseLength { get; set; } = -1;
        public ReferencePointLength SwipePoint = new ReferencePointLength();

        /// <summary>
        /// 렌즈 초점 거리
        /// </summary>
        public double FocalLength { get; set; } = 0;
        /// <summary>
        /// 3d 포인트 Y축 오프셋
        /// </summary>
        public int ManualOffsetY { get; set; } = 0;


        public ReferencePoint3D TargetArea = new ReferencePoint3D();
        public GimbalInfo Gimbal = new GimbalInfo();

        public Homo_t Homo_T = new Homo_t();

        /// <summary>
        /// 현재 HomoT 유효성 검사
        /// </summary>
        /// <returns></returns>
        public bool CheckHomoT()
        {
            if (this.Homo_T.r1[0] == 1 && this.Homo_T.r1[1] == 0 && this.Homo_T.r1[2] == 0
                && this.Homo_T.r2[0] == 0 && this.Homo_T.r2[1] == 1 && this.Homo_T.r2[2] == 0
                && this.Homo_T.r3[0] == 0 && this.Homo_T.r3[1] == 0 && this.Homo_T.r1[0] == 1)
                return false;

            return true;
        }

        public string RefImagePath
        {
            get
            {
                return System.IO.Path.Combine(System.IO.Path.GetDirectoryName(VideoFilePath)
              , $"{DscID}_ref.png");
            }
        }
        public DscInfo() { }
        ~DscInfo()
        {
            RotateUse = true;
        }

        public bool IsAdaptiveCalibrationMode { get; set; }

        public bool RotateUse { get; set; }

        public string VideoFilePath { get; set; }

        public int VideoFrameNumber { get; set; }

        public void VideoCapture(int deltaFrame)
        {
            int targetFrame = VideoFrameNumber + deltaFrame;

            targetFrame = Math.Max(0, targetFrame);

            VideoFrameNumber = targetFrame;

            SrcImg = CalibrationLib.VideoHandler.Capture(VideoFilePath, VideoFrameNumber);
        }

        public DscInfo Clone()
        {
            DscInfo ret = new DscInfo();

            ret.DscID = this.DscID;
            ret.Group = this.Group;
            ret.Width = this.Width;
            ret.Height = this.Height;
            ret.IsReg = this.IsReg;
            ret.IsAdj = this.IsAdj;
            ret.Points2D = new ReferencePoint2D()
            {
                LowerPosX = this.Points2D.LowerPosX,
                LowerPosY = this.Points2D.LowerPosY,
                MiddlePosX = this.Points2D.MiddlePosX,
                MiddlePosY = this.Points2D.MiddlePosY,
                UpperPosX = this.Points2D.UpperPosX,
                UpperPosY = this.Points2D.UpperPosY,
            };
            ret.Points3D = new ReferencePoint3D()
            {
                X1 = this.Points3D.X1,
                Y1 = this.Points3D.Y1,
                X2 = this.Points3D.X2,
                Y2 = this.Points3D.Y2,
                X3 = this.Points3D.X3,
                Y3 = this.Points3D.Y3,
                X4 = this.Points3D.X4,
                Y4 = this.Points3D.Y4,
                CenterX = this.Points3D.CenterX,
                CenterY = this.Points3D.CenterY,


            };
            ret.PreCalcData = new PreCalibrationData()
            {
                Degree = this.PreCalcData.Degree,
                Norm = this.PreCalcData.Norm,
                RotMatrix = this.PreCalcData.RotMatrix
            };
            ret.Adjust = new AdjustInfo()
            {
                AdjustX = this.Adjust.AdjustX,
                AdjustY = this.Adjust.AdjustY,
                Angle = this.Adjust.Angle,
                RotateX = this.Adjust.RotateX,
                RotateY = this.Adjust.RotateY
            };
            // ret.SrcImg = this.SrcImg?.Clone();

            ret.IsFlip = this.IsFlip;
            ret.UseLogo = this.UseLogo;
            ret.SwipePoint = new ReferencePointLength()
            {
                X1 = this.SwipePoint.X1,
                Y1 = this.SwipePoint.Y1,
                X2 = this.SwipePoint.X2,
                Y2 = this.SwipePoint.Y2
            };
            ret.LiveIndex = this.LiveIndex;
            ret.CameraIndex = this.CameraIndex;
            ret.SwipeBaseLength = this.SwipeBaseLength;
            ret.FocalLength = this.FocalLength;
            ret.ManualOffsetY = this.ManualOffsetY;

            return ret;
        }

        //public Mat AdjustImageSrc { get; set; }

        /// <summary>
        /// 과거 방식 2D, 3D, 메타 정보 그려짐 (크롭영역, 중심점)
        /// </summary>
        /// <param name="bOrignSize"></param>
        /// <returns></returns>
        //public Mat GetAdjustImage(bool bOrignSize = false)
        //{
        //    Mat src = new Mat();
        //    Mat dst = new Mat();

        //    if (IsAdaptiveCalibrationMode)
        //    {
        //        if (AdjustImageSrc == null)
        //        {
        //            AdjustImageSrc = Cv2.ImRead(RefImagePath);
        //        }

        //        src = AdjustImageSrc.Clone();
        //    }
        //    else
        //    {
        //        src = this.SrcImg;
        //    }

        //    if (src.Empty() == false)
        //    {
        //        try
        //        {
        //            using (Mat resizedSrc = new Mat())
        //            {
        //                int resizeHeight = 1080;
        //                if (bOrignSize)
        //                {
        //                    resizeHeight = src.Height;
        //                }

        //                Cv2.Resize(src, resizedSrc, new OpenCvSharp.Size((int)(resizeHeight * ((float)src.Width / src.Height)), resizeHeight));

        //                double ratioX = (double)resizedSrc.Width / src.Width;
        //                double ratioY = (double)resizedSrc.Height / src.Height;
        //                if (bOrignSize)
        //                {
        //                    ratioX = 1;
        //                    ratioY = 1;
        //                }

        //                AdjustInfo adj = new AdjustInfo();
        //                adj.AdjustX = Adjust.AdjustX * ratioX;
        //                adj.AdjustY = Adjust.AdjustY * ratioY;
        //                adj.RotateX = Adjust.RotateX * ratioX;
        //                adj.RotateY = Adjust.RotateY * ratioY;
        //                adj.Angle = Adjust.Angle;
        //                adj.Scale = Adjust.Scale;
        //                adj.RectMargin = new RectangleF(
        //                    (float)(Adjust.RectMargin.X * ratioX),
        //                    (float)(Adjust.RectMargin.Y * ratioY),
        //                    (float)(Adjust.RectMargin.Width * ratioX),
        //                    (float)(Adjust.RectMargin.Height * ratioY)
        //                    );

        //                FDAdjust.AdjustImage(resizedSrc,
        //                    ref dst, 
        //                    new OpenCvSharp.Size(resizedSrc.Cols, resizedSrc.Rows),
        //                    adj,
        //                    (this.IsFlip && RotateUse));

        //                FDAdjust.DrawROI(ref dst, adj.RectMargin);
        //            }
        //        }
        //        catch { }
        //    }

        //    return dst;
        //}

        public Mat GetAdjustImage(bool bOrignSize = false)
        {
            if (this.SrcImg == null || this.SrcImg.Width <= 0)
                return null;
            try
            {
                Mat adjustMat = new Mat();
                using (Mat source = new Mat())
                {
                    int resizeHeight = 1080;
                    if (bOrignSize)
                    {
                        resizeHeight = this.SrcImg.Height;
                    }
                    Cv2.Resize(this.SrcImg, source, new OpenCvSharp.Size((int)(resizeHeight * ((float)this.SrcImg.Width / this.SrcImg.Height)), resizeHeight));

                    double ratioX = (double)source.Width / this.SrcImg.Width;
                    double ratioY = (double)source.Height / this.SrcImg.Height;
                    if (bOrignSize)
                    {
                        ratioX = 1;
                        ratioY = 1;
                    }

                    AdjustInfo adj = new AdjustInfo();
                    adj.AdjustX = Adjust.AdjustX * ratioX;
                    adj.AdjustY = Adjust.AdjustY * ratioY;
                    adj.RotateX = Adjust.RotateX * ratioX;
                    adj.RotateY = Adjust.RotateY * ratioY;
                    adj.Angle = Adjust.Angle;
                    adj.Scale = Adjust.Scale;
                    adj.RectMargin = new RectangleF(
                        (float)(Adjust.RectMargin.X * ratioX),
                        (float)(Adjust.RectMargin.Y * ratioY),
                        (float)(Adjust.RectMargin.Width * ratioX),
                        (float)(Adjust.RectMargin.Height * ratioY)
                        );

                    FDAdjust.AdjustImage(source, ref adjustMat, new OpenCvSharp.Size(source.Cols, source.Rows),
                        adj,
                        this.IsFlip);

                    FDAdjust.DrawROI(ref adjustMat, adj.RectMargin);
                }

                return adjustMat;
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// 변환된 크롭 이미지. (메타정보 그리기 없음)
        /// </summary>
        /// <param name="mode"></param>
        /// <returns></returns>
        public Mat GetRectificationImage(CalibrationMode mode)
        {
            if (this.SrcImg == null || this.SrcImg.Width <= 0) return null;

            try
            {
                if (mode != CalibrationMode.Mode2DArea)
                {
                    return FDAdjust.AdjustImage(this.SrcImg, new OpenCvSharp.Size(this.SrcImg.Cols, this.SrcImg.Rows), Adjust, this.IsFlip);
                }

                if (this.Points2D.LowerPosX < 0 || this.Points2D.UpperPosX < 0) return null;

                return VisionFunctions.imgWarp_px2(this.SrcImg, new OpenCvSharp.Point(this.Points2D.UpperPosX, this.Points2D.UpperPosY),
                    new OpenCvSharp.Point(this.Points2D.LowerPosX, this.Points2D.LowerPosY),
                    new List<OpenCvSharp.Point2d>() {
                        new OpenCvSharp.Point2d(TargetArea.X1, TargetArea.Y1),
                        new OpenCvSharp.Point2d(TargetArea.X2, TargetArea.Y2),
                        new OpenCvSharp.Point2d(TargetArea.X3, TargetArea.Y3),
                        new OpenCvSharp.Point2d(TargetArea.X4, TargetArea.Y4), 
                    });
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }

    /// <summary>
    /// 기준봉 또는 가상 기준봉에 대한 Input 정보
    /// </summary>
    public class ReferencePoint2D
    {
        public ReferencePoint2D()
        {
            Reset();
        }
        public void Reset()
        {
            UpperPosX = -1.0F;
            UpperPosY = -1.0F;
            MiddlePosX = -1.0F;
            MiddlePosY = -1.0F;
            LowerPosX = -1.0F;
            LowerPosY = -1.0F;
        }
        public double UpperPosX { get; set; }
        public double UpperPosY { get; set; }
        public double MiddlePosX { get; set; }
        public double MiddlePosY { get; set; }
        public double LowerPosX { get; set; }
        public double LowerPosY { get; set; }

        public PointF Upper => new PointF((float)UpperPosX, (float)UpperPosY);
        public PointF Middle => new PointF((float)MiddlePosX, (float)MiddlePosY);
        public PointF Lower => new PointF((float)LowerPosX, (float)LowerPosY);
    }
    /// <summary>
    /// 바닥면의 4 points 정보
    /// </summary>
    public class ReferencePoint3D
    {
        public ReferencePoint3D()
        {
            Reset();
        }
        public void Reset()
        {
            X1 = -1.0F;
            Y1 = -1.0F;
            X2 = -1.0F;
            Y2 = -1.0F;
            X3 = -1.0F;
            Y3 = -1.0F;
            X4 = -1.0F;
            Y4 = -1.0F;
            CenterX = -1.0F;
            CenterY = -1.0F;
        }
        public double X1 { get; set; }
        public double Y1 { get; set; }
        public double X2 { get; set; }
        public double Y2 { get; set; }
        public double X3 { get; set; }
        public double Y3 { get; set; }
        public double X4 { get; set; }
        public double Y4 { get; set; }
        public double CenterX { get; set; }
        public double CenterY { get; set; }

        public PointF Point1 => new PointF((float)X1, (float)Y1);
        public PointF Point2 => new PointF((float)X2, (float)Y2);
        public PointF Point3 => new PointF((float)X3, (float)Y3);
        public PointF Point4 => new PointF((float)X4, (float)Y4);
        public PointF Center => new PointF((float)CenterX, (float)CenterY);

    }
    /// <summary>
    /// 길이 측정 2 points 정보
    /// </summary>
    public class ReferencePointLength
    {
        public ReferencePointLength()
        {
            Reset();
        }
        public void Reset()
        {
            X1 = -1.0F;
            Y1 = -1.0F;
            X2 = -1.0F;
            Y2 = -1.0F;
        }
        public double X1 { get; set; }
        public double Y1 { get; set; }
        public double X2 { get; set; }
        public double Y2 { get; set; }
    }

    /// <summary>
    /// 길이 측정 2 points 정보
    /// </summary>
    public class GimbalInfo
    {
        public GimbalInfo()
        {
            Reset();
        }
        public void Reset()
        {
            //X = -1.0F;
            //Y = -1.0F;
            //Z = -1.0F;
            HW_Preset = -1;
            X = 9999;
            Y = 9999;
            Z = 9999;
            Zoom = -1.0F;
            FocusDiv5 = 0;
            Reserved = 0;
        }
        public int HW_Preset { get; set; }    // -1 : S/W Preset, 0~7 : H/W Preset Num
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
        public double Zoom { get; set; }
        public double FocusDiv5 { get; set; }
        public double Reserved { get; set; }
    }
    /// <summary>
    /// 3D Calibration시 임시로 저장할 데이터
    /// </summary>
    public class PreCalibrationData
    {
        public PreCalibrationData()
        {
            Norm = 0.0;
            Degree = 0.0;
            RotMatrix = new Mat();
        }
        public double Norm { get; set; }
        public double Degree { get; set; }
        public Mat RotMatrix { get; set; }
    }


    public class WorldCoordinates
    {
        public string group { get; set; } = "Group1";
        public StadiumType stadiumType { get; set; } = StadiumType.BaseballGround;
        public WorldCoordinates4d worldCoordinates4d = new WorldCoordinates4d();
    }
    /// <summary>
    /// 기준 좌표로 사용할 월드 좌표 4개
    /// </summary>
    public class WorldCoordinates4d
    {
        public WorldCoordinates4d()
        {
            X1 = -1.0F;
            Y1 = -1.0F;
            X2 = -1.0F;
            Y2 = -1.0F;
            X3 = -1.0F;
            Y3 = -1.0F;
            X4 = -1.0F;
            Y4 = -1.0F;
        }
        public double X1 { get; set; }
        public double Y1 { get; set; }
        public double X2 { get; set; }
        public double Y2 { get; set; }
        public double X3 { get; set; }
        public double Y3 { get; set; }
        public double X4 { get; set; }
        public double Y4 { get; set; }
    }
    /// <summary>
    /// 영상에 적용될 Adjust 정보
    /// </summary>
    public class AdjustInfo
    {
        public double AdjustX { get; set; }
        public double AdjustY { get; set; }
        public double Angle { get; set; }
        public double RotateX { get; set; }
        public double RotateY { get; set; }
        public double Scale { get; set; }

        public double normAdjustX { get { return AdjustX / RectMargin.Width; } }
        public double normAdjustY { get { return AdjustY / RectMargin.Height; } }
        public double normRotateX { get { return RotateX / RectMargin.Width; } }
        public double normRotateY { get { return RotateY / RectMargin.Height; } }

        public System.Drawing.RectangleF RectMargin { get; set; }
    }
    public class Pair<T, U>
    {
        public Pair()
        {
        }
        public Pair(T first, U second)
        {
            this.First = first;
            this.Second = second;
        }
        public T First { get; set; }
        public U Second { get; set; }
    };

    public class FDAdjust
    {
        public static int MaxRange = 100;
        private static void Normalization3DSquarePoint(ref List<Point3f> ptSquarePoint, int nMaxRange)
        {
            float fMin_x = ptSquarePoint[0].X;
            float fMin_y = ptSquarePoint[0].Y;
            float fMax_x = ptSquarePoint[0].X;
            float fMax_y = ptSquarePoint[0].Y;

            foreach (Point3f pt in ptSquarePoint)
            {
                if (fMax_x < pt.X)
                    fMax_x = pt.X;
                if (fMax_y < pt.Y)
                    fMax_y = pt.Y;
                if (fMin_x > pt.X)
                    fMin_x = pt.X;
                if (fMin_y > pt.Y)
                    fMin_y = pt.Y;
            }

            float fRange;
            float fMargin_X = 0;
            float fMargin_Y = 0;

            if ((fMax_x - fMin_x) > (fMax_y - fMin_y))
            {
                fRange = nMaxRange / (fMax_x - fMin_x);
                fMargin_Y = (nMaxRange - ((fMax_y - fMin_y) * (fRange))) / 2.0F;
            }
            else
            {
                fRange = nMaxRange / (fMax_y - fMin_y);
                fMargin_X = (nMaxRange - ((fMax_x - fMin_x) * (fRange))) / 2.0F;
            }

            for (int i = 0; i < ptSquarePoint.Count; i++)
            {
                Point3f ptNew = new Point3f();
                ptNew.X = (ptSquarePoint[i].X - fMin_x) * fRange + fMargin_X;
                ptNew.Y = (ptSquarePoint[i].Y - fMin_y) * fRange + fMargin_Y;
                ptNew.Z = ptSquarePoint[i].Z;

                ptSquarePoint[i] = ptNew;
            }
        }
        /// <summary>
        /// 회전된 좌표 계산
        /// </summary>
        /// <param name="ptCenter"></param>
        /// <param name="ptRot"></param>
        /// <param name="dbAngle"></param>
        /// <returns></returns>
        public static Point2f GetRotatePoint(Point2f ptCenter, Point2f ptRot, double dbAngle)
        {
            Point2f ptResult = new Point2f();

            // 회전 중심좌표와의 상대좌표
            ptRot.X = ptRot.X - ptCenter.X;
            ptRot.Y = -(ptRot.Y - ptCenter.Y);

            double cosX = Math.Cos(dbAngle);
            double sinX = Math.Sin(dbAngle);

            ptResult.X = (float)(ptRot.X * cosX - ptRot.Y * sinX);
            ptResult.Y = (float)(ptRot.X * sinX + ptRot.Y * cosX);

            ptResult.X = ptResult.X + ptCenter.X;
            ptResult.Y = -(ptResult.Y - ptCenter.Y);

            return ptResult;
        }

        public static bool TrackCenterPoint(List<DscInfo> dscList, int idx, double ptX, double ptY)
        {
            return TrackCenterPoint(dscList, idx, new Point2d(ptX, ptY));

        }
        /// <summary>
        /// 3D Calibration의 4점 좌표를 통해 각 카메라별 기준 center 위치 지정
        /// </summary>
        /// <param name="inList"></param>
        /// <param name="outList"></param>
        /// <returns></returns>
        public static bool TrackCenterPoint(List<DscInfo> dscList, int idx, Point2d ptCenter)
        {
            if (ptCenter.X == -1 || ptCenter.Y == -1)
                return false;
            List<Point2d> corners = new List<Point2d>();
            corners.Add(new Point2d(dscList[idx].Points3D.X1, dscList[idx].Points3D.Y1));
            corners.Add(new Point2d(dscList[idx].Points3D.X2, dscList[idx].Points3D.Y2));
            corners.Add(new Point2d(dscList[idx].Points3D.X3, dscList[idx].Points3D.Y3));
            corners.Add(new Point2d(dscList[idx].Points3D.X4, dscList[idx].Points3D.Y4));

            // Reg된 idx 쌍을 탐색하여 추가
            for (int i = idx + 1; i < dscList.Count; i++)
            {
                ReferencePoint3D pts = dscList[i].Points3D;

                if (pts.X1 == -1 || pts.Y1 == -1 || pts.X2 == -1 || pts.Y2 == -1 || pts.X3 == -1 || pts.Y3 == -1 || pts.X4 == -1 || pts.Y4 == -1)
                    continue;

                List<Point2d> cornersNew = new List<Point2d>();
                cornersNew.Add(new Point2d(dscList[i].Points3D.X1, dscList[i].Points3D.Y1));
                cornersNew.Add(new Point2d(dscList[i].Points3D.X2, dscList[i].Points3D.Y2));
                cornersNew.Add(new Point2d(dscList[i].Points3D.X3, dscList[i].Points3D.Y3));
                cornersNew.Add(new Point2d(dscList[i].Points3D.X4, dscList[i].Points3D.Y4));

                Mat h = Cv2.FindHomography(corners, cornersNew);

                Mat<double> mCenterPoint = new Mat<double>(3, 1);


                mCenterPoint.At<double>(0) = ptCenter.X;
                mCenterPoint.At<double>(1) = ptCenter.Y;
                mCenterPoint.At<double>(2) = 1;

                Mat mResult = h * mCenterPoint;

                dscList[i].Points3D.CenterX = mResult.At<double>(0) / mResult.At<double>(2);
                dscList[i].Points3D.CenterY = mResult.At<double>(1) / mResult.At<double>(2);
            }

            return true;
        }
        /// <summary>
        /// 3D Calibration의 4점 좌표와 World 좌표를 통해 예비 계산 결과 생성
        /// </summary>
        /// <param name="wc"></param>
        /// <param name="dscList"></param>
        /// <returns></returns>
        public static bool CalcPreValue3DCalbration(in WorldCoordinates4d wc, List<DscInfo> dscList)
        {
            if (wc.X1 == -1 || wc.Y1 == -1 || wc.X2 == -1 || wc.Y2 == -1 || wc.X3 == -1 || wc.Y3 == -1 || wc.X4 == -1 || wc.Y4 == -1)
                return false;

            List<Point3f> ptsSquare3d = new List<Point3f>();
            ptsSquare3d.Add(new Point3f((float)wc.X1, (float)wc.Y1, 0));
            ptsSquare3d.Add(new Point3f((float)wc.X2, (float)wc.Y2, 0));
            ptsSquare3d.Add(new Point3f((float)wc.X3, (float)wc.Y3, 0));
            ptsSquare3d.Add(new Point3f((float)wc.X4, (float)wc.Y4, 0));

            Normalization3DSquarePoint(ref ptsSquare3d, 100);



            List<Pair<int, int>> regList = new List<Pair<int, int>>();

            // Reg된 idx 쌍을 탐색하여 추가
            for (int i = 0; i < dscList.Count; i++)
            {
                dscList[i].IsAdj = false;
                if (dscList[i].IsReg)
                {
                    Pair<int, int> idxPair = new Pair<int, int>(-1, -1);

                    idxPair.First = i;

                    for (int j = i + 1; j < dscList.Count; j++)
                    {
                        if (dscList[j].IsReg)
                        {
                            idxPair.Second = j;
                            break;
                        }
                    }

                    if (idxPair.First != -1 && idxPair.Second != -1 && idxPair.Second - idxPair.First > 1)
                    {
                        regList.Add(idxPair);

                        i = idxPair.Second - 1;
                    }
                }
            }

            for (int i = 0; i < regList.Count; i++)
            {
                int cnt = regList[i].Second - regList[i].First + 1;

                double dFirstNorm = 0;

                for (int j = regList[i].First; j <= regList[i].Second; j++)
                {
                    double dSensorSize = 17.30;

                    if (dscList[j].Width == 3840)
                        dSensorSize = 17.30 / 1.35;
                    else
                        dSensorSize = 17.30;

                    double dFocal;
                    double dFocalLength = dscList[j].FocalLength;
                    if (dFocalLength <= 0)
                    {
                        dFocal = dscList[j].Width;
                    }
                    else
                    {
                        dFocal = (dFocalLength / dSensorSize) * dscList[j].Width;
                    }

                    var mCameraParameter = new float[,] {
                        {(float)dFocal, 0, dscList[j].Width / 2 },
                        { 0, (float)dFocal, dscList[j].Height / 2 },
                        { 0, 0, 1 }
                    };
                    var mDistCoeffs = new float[] { 0, 0, 0, 0 };

                    List<Point2f> pts2d = new List<Point2f>();
                    pts2d.Add(new Point2f((float)dscList[j].Points3D.X1, (float)dscList[j].Points3D.Y1 + dscList[j].ManualOffsetY));
                    pts2d.Add(new Point2f((float)dscList[j].Points3D.X2, (float)dscList[j].Points3D.Y2 + dscList[j].ManualOffsetY));
                    pts2d.Add(new Point2f((float)dscList[j].Points3D.X3, (float)dscList[j].Points3D.Y3 + dscList[j].ManualOffsetY));
                    pts2d.Add(new Point2f((float)dscList[j].Points3D.X4, (float)dscList[j].Points3D.Y4 + dscList[j].ManualOffsetY));

                    Mat _mRotationMatrix = new Mat(3, 1, MatType.CV_32F);
                    Mat _mTranslationMatrix = new Mat(3, 1, MatType.CV_32F);

                    Cv2.SolvePnP(
                        new Mat(ptsSquare3d.Count, 3, MatType.CV_32F, ptsSquare3d.ToArray()),
                        new Mat(pts2d.Count, 2, MatType.CV_32F, pts2d.ToArray()),
                        new Mat(3, 3, MatType.CV_32F, mCameraParameter),
                        new Mat(4, 1, MatType.CV_32F, mDistCoeffs),
                        _mRotationMatrix, _mTranslationMatrix, false, SolvePnPFlags.Iterative);

                    var normalVector = new float[,] { { 50.0F, 50.0F, 0.0F }, { 50.0F, 50.0F, -100.0F } };

                    Mat projectedNormalVector = new Mat();
                    Cv2.ProjectPoints(new Mat(2, 3, MatType.CV_32F, normalVector), _mRotationMatrix, _mTranslationMatrix, new Mat(3, 3, MatType.CV_32F, mCameraParameter), new Mat(4, 1, MatType.CV_32F, mDistCoeffs), projectedNormalVector);

                    Vec2f vec0 = projectedNormalVector.At<Vec2f>(0);
                    Vec2f vec1 = projectedNormalVector.At<Vec2f>(1);

                    Point2f ptAngleVector = new Point2f(vec1[0] - vec0[0], vec1[1] - vec0[1]);

                    double dNorm = Cv2.Norm(vec0, vec1);
                    double dDegree = Cv2.FastAtan2(ptAngleVector.X, ptAngleVector.Y);

                    if (vec1[1] > vec0[1])
                    {
                        dDegree = 360.0 - dDegree;
                        if (dDegree >= 360)
                            dDegree -= 360;
                    }
                    else
                    {
                        dDegree = 180.0 - dDegree;
                    }

                    if (j == regList[i].First)
                        dFirstNorm = dNorm;

                    Mat mRotMatrix = Cv2.GetRotationMatrix2D(
                        new Point2f((float)dscList[j].Points3D.CenterX, (float)dscList[j].Points3D.CenterY),
                        dDegree, dFirstNorm / dNorm);

                    dscList[j].PreCalcData.Norm = dNorm;
                    dscList[j].PreCalcData.Degree = dDegree;
                    dscList[j].PreCalcData.RotMatrix = mRotMatrix;
                }
            }

            return true;
        }
        /// <summary>
        /// 3D Calibration의 데이터들을 통해 Adjust data 계산
        /// </summary>
        /// <param name="dscList"></param>
        /// <returns></returns>
        public static bool CalcAdjustData3D(List<DscInfo> dscList)
        {
            List<Pair<int, int>> regList = new List<Pair<int, int>>();

            // Reg된 idx 쌍을 탐색하여 추가
            for (int i = 0; i < dscList.Count; i++)
            {
                dscList[i].IsAdj = false;
                if (dscList[i].IsReg)
                {
                    Pair<int, int> idxPair = new Pair<int, int>(i, -1);

                    for (int j = i + 1; j < dscList.Count; j++)
                    {
                        if (dscList[j].IsReg)
                        {
                            idxPair.Second = j;
                            break;
                        }
                    }

                    if (idxPair.First != -1 && idxPair.Second != -1 && idxPair.Second - idxPair.First > 1)
                    {
                        regList.Add(idxPair);

                        i = idxPair.Second - 1;
                    }
                }
            }

            for (int i = 0; i < regList.Count; i++)
            {
                int cnt = regList[i].Second - regList[i].First + 1;

                double distStart = dscList[regList[i].First].PreCalcData.Norm;

                double distEnd = dscList[regList[i].Second].PreCalcData.Norm;

                // Start-End 화면의 기준봉 distance 차이에 대한 interval value 계산
                double interval = (distEnd - distStart) / (cnt - 1);

                // 평균 중심 좌표 계산
                double sumX = 0, sumY = 0, avgX, avgY;
                for (int j = regList[i].First; j <= regList[i].Second; j++)
                {
                    sumX += dscList[j].Points3D.CenterX;
                    sumY += dscList[j].Points3D.CenterY;
                }

                avgX = sumX / cnt;
                avgY = sumY / cnt;

                // Adjust data 계산
                for (int j = regList[i].First; j <= regList[i].Second; j++)
                {
                    dscList[j].Adjust.Angle = -dscList[j].PreCalcData.Degree;
                    if (dscList[j].Adjust.Angle >= -180)
                        dscList[j].Adjust.Angle += -90;
                    else
                        dscList[j].Adjust.Angle += 270;


                    double targetDist = distStart + (interval * (j - regList[i].First));

                    dscList[j].Adjust.Scale = targetDist / dscList[j].PreCalcData.Norm;

                    dscList[j].Adjust.AdjustX = avgX - dscList[j].Points3D.CenterX;
                    dscList[j].Adjust.AdjustY = avgY - dscList[j].Points3D.CenterY;
                    dscList[j].Adjust.RotateX = dscList[j].Points3D.CenterX;
                    dscList[j].Adjust.RotateY = dscList[j].Points3D.CenterY;
                }


                #region Margin 계산

                List<RectangleF> lstMinRect = new List<RectangleF>();
                RectangleF rtMinRect = new RectangleF(0, 0, dscList[regList[i].First].Width, dscList[regList[i].First].Height);

                for (int j = regList[i].First; j <= regList[i].Second; j++)
                {
                    if (dscList[j].Adjust.AdjustX == 0 || dscList[j].Adjust.AdjustY == 0 || dscList[j].Points3D.CenterX == -1 || dscList[j].Points3D.CenterY == -1)
                    {
                        dscList[j].Adjust.RectMargin = new RectangleF(0, 0, 0, 0);
                        continue;
                    }

                    int nWidth, nHeight;
                    Point2f pt1, pt2, pt3, pt4, ptR1, ptR2, ptR3, ptR4;

                    Point2f ptCenter = new Point2f((float)dscList[j].Adjust.RotateX, (float)dscList[j].Adjust.RotateY);

                    nWidth = dscList[j].Width;
                    nHeight = dscList[j].Height;

                    double dbAngleAdjust = -1 * (dscList[j].Adjust.Angle + 90);
                    double dbRAngle = dbAngleAdjust * Math.PI / 180;

                    // Get Resize Rect Point
                    pt1.X = (float)(ptCenter.X * (1 - dscList[j].Adjust.Scale));
                    pt1.Y = (float)(ptCenter.Y * (1 - dscList[j].Adjust.Scale));

                    pt2.X = (float)(pt1.X + nWidth * dscList[j].Adjust.Scale);
                    pt2.Y = pt1.Y;

                    pt3.X = pt2.X;
                    pt3.Y = (float)(pt1.Y + nHeight * dscList[j].Adjust.Scale);

                    pt4.X = pt1.X;
                    pt4.Y = pt3.Y;

                    ptR1 = FDAdjust.GetRotatePoint(ptCenter, pt1, dbRAngle);
                    ptR2 = FDAdjust.GetRotatePoint(ptCenter, pt2, dbRAngle);
                    ptR3 = FDAdjust.GetRotatePoint(ptCenter, pt3, dbRAngle);
                    ptR4 = FDAdjust.GetRotatePoint(ptCenter, pt4, dbRAngle);

                    int nNewMarginLeft = 0;
                    int nNewMarginTop = 0;
                    int nNewMarginRight = nWidth;
                    int nNewMarginBottom = nHeight;

                    if (ptR1.X + dscList[j].Adjust.AdjustX > nNewMarginLeft)
                        nNewMarginLeft = (int)(ptR1.X + dscList[j].Adjust.AdjustX);
                    if (ptR1.Y + dscList[j].Adjust.AdjustY > nNewMarginTop)
                        nNewMarginTop = (int)(ptR1.Y + dscList[j].Adjust.AdjustY);

                    if (ptR2.X + dscList[j].Adjust.AdjustX < nNewMarginRight)
                        nNewMarginRight = (int)(ptR2.X + dscList[j].Adjust.AdjustX);
                    if (ptR2.Y + dscList[j].Adjust.AdjustY > nNewMarginTop)
                        nNewMarginTop = (int)(ptR2.Y + dscList[j].Adjust.AdjustY);

                    if (ptR3.X + dscList[j].Adjust.AdjustX < nNewMarginRight)
                        nNewMarginRight = (int)(ptR3.X + dscList[j].Adjust.AdjustX);
                    if (ptR3.Y + dscList[j].Adjust.AdjustY < nNewMarginBottom)
                        nNewMarginBottom = (int)(ptR3.Y + dscList[j].Adjust.AdjustY);

                    if (ptR4.X + dscList[j].Adjust.AdjustX > nNewMarginLeft)
                        nNewMarginLeft = (int)(ptR4.X + dscList[j].Adjust.AdjustX);
                    if (ptR4.Y + dscList[j].Adjust.AdjustY < nNewMarginBottom)
                        nNewMarginBottom = (int)(ptR4.Y + dscList[j].Adjust.AdjustY);

                    if (nNewMarginLeft > nNewMarginTop * nWidth / nHeight)
                        nNewMarginTop = nNewMarginLeft * nHeight / nWidth;
                    else
                        nNewMarginLeft = nNewMarginTop * nWidth / nHeight;

                    if (nNewMarginRight < nNewMarginBottom * nWidth / nHeight)
                        nNewMarginBottom = nNewMarginRight * nHeight / nWidth;
                    else
                        nNewMarginRight = nNewMarginBottom * nWidth / nHeight;

                    // Margin 뒤집힘 현상 임시 방지
                    if (nNewMarginLeft > nNewMarginRight)
                    {
                        int nTemp = nNewMarginLeft;
                        nNewMarginLeft = nNewMarginRight;
                        nNewMarginRight = nTemp;
                    }
                    if (nNewMarginTop > nNewMarginBottom)
                    {
                        int nTemp = nNewMarginTop;
                        nNewMarginTop = nNewMarginBottom;
                        nNewMarginBottom = nTemp;
                    }

                    lstMinRect.Add(new RectangleF(nNewMarginLeft, nNewMarginTop, nNewMarginRight - nNewMarginLeft, nNewMarginBottom - nNewMarginTop));
                }

                int nMinTop = 0;
                int nMinLeft = 0;
                int nMinRight = dscList[regList[i].First].Width;
                int nMinBottom = dscList[regList[i].First].Height;

                for (int j = 0; j < lstMinRect.Count; j++)
                {
                    if (lstMinRect[j].Left > nMinLeft && lstMinRect[j].Top > nMinTop)
                    {
                        nMinLeft = (int)lstMinRect[j].Left;
                        nMinTop = (int)lstMinRect[j].Top;

                        rtMinRect = new RectangleF(nMinLeft, nMinTop, rtMinRect.Right - nMinLeft, rtMinRect.Bottom - nMinTop);
                    }

                    if (lstMinRect[j].Right < nMinRight && lstMinRect[j].Bottom < nMinBottom)
                    {
                        if (lstMinRect[j].Right == 0 || lstMinRect[j].Bottom == 0)
                            continue;

                        nMinRight = (int)lstMinRect[j].Right;
                        nMinBottom = (int)lstMinRect[j].Bottom;

                        rtMinRect = new RectangleF(rtMinRect.Left, rtMinRect.Top, nMinRight - rtMinRect.Left, nMinBottom - rtMinRect.Top);
                    }
                }

                for (int j = regList[i].First; j <= regList[i].Second; j++)
                {
                    dscList[j].Adjust.RectMargin = rtMinRect;
                    dscList[j].IsAdj = true;
                }
                #endregion
            }

            return true;
        }
        /// <summary>
        /// 2D Calibration의 기준봉 또는 가상기준봉 좌표들을 통해 Adjust data 계산
        /// </summary>
        /// <param name="inList"></param>
        /// <param name="outList"></param>
        /// <returns></returns>
        public static bool CalcAdjustData2D(List<DscInfo> dscList)
        {
            List<Pair<int, int>> regList = new List<Pair<int, int>>();

            // Reg된 idx 쌍을 탐색하여 추가
            for (int i = 0; i < dscList.Count; i++)
            {
                dscList[i].IsAdj = false;
                if (dscList[i].IsReg)
                {
                    Pair<int, int> idxPair = new Pair<int, int>(i, -1);

                    for (int j = i + 1; j < dscList.Count; j++)
                    {
                        if (dscList[j].IsReg)
                        {
                            idxPair.Second = j;
                            break;
                        }
                    }

                    if (idxPair.First != -1 && idxPair.Second != -1 && idxPair.Second - idxPair.First > 1)
                    {
                        regList.Add(idxPair);

                        i = idxPair.Second - 1;
                    }
                }
            }

            for (int i = 0; i < regList.Count; i++)
            {
                int cnt = regList[i].Second - regList[i].First + 1;

                double distStart = 0;
                double diffStartX = (dscList[regList[i].First].Points2D.LowerPosX - dscList[regList[i].First].Points2D.UpperPosX);
                double diffStartY = (dscList[regList[i].First].Points2D.LowerPosY - dscList[regList[i].First].Points2D.UpperPosY);
                if (diffStartX == 0.0F)
                    distStart = diffStartY;
                else
                    distStart = Math.Sqrt((diffStartX * diffStartX) + (diffStartY * diffStartY));

                double distEnd = 0;
                double diffEndX = (dscList[regList[i].Second].Points2D.LowerPosX - dscList[regList[i].Second].Points2D.UpperPosX);
                double diffEndY = (dscList[regList[i].Second].Points2D.LowerPosY - dscList[regList[i].Second].Points2D.UpperPosY);
                if (diffEndX == 0.0F)
                    distEnd = diffEndY;
                else
                    distEnd = Math.Sqrt((diffEndX * diffEndX) + (diffEndY * diffEndY));

                // Start-End 화면의 기준봉 distance 차이에 대한 interval value 계산
                double interval = (distEnd - distStart) / (cnt - 1);

                // 평균 중심 좌표 계산
                double sumX = 0, sumY = 0, avgX, avgY;
                for (int j = regList[i].First; j <= regList[i].Second; j++)
                {
                    sumX += dscList[j].Points2D.MiddlePosX;
                    sumY += dscList[j].Points2D.MiddlePosY;
                }

                avgX = sumX / cnt;
                avgY = sumY / cnt;

                // Adjust data 계산
                for (int j = regList[i].First; j <= regList[i].Second; j++)
                {
                    dscList[j].Adjust.Angle = -Math.Atan2((dscList[j].Points2D.LowerPosY - dscList[j].Points2D.UpperPosY),
(dscList[j].Points2D.LowerPosX - dscList[j].Points2D.UpperPosX)) * (180 / Math.PI);

                    double dist = 0;
                    double diffX = (dscList[j].Points2D.LowerPosX - dscList[j].Points2D.UpperPosX);
                    double diffY = (dscList[j].Points2D.LowerPosY - dscList[j].Points2D.UpperPosY);
                    if (diffX == 0.0F)
                        dist = diffY;
                    else
                        dist = Math.Sqrt((diffX * diffX) + (diffY * diffY));

                    double targetDist = distStart + (interval * (j - regList[i].First));

                    dscList[j].Adjust.AdjustX = avgX - dscList[j].Points2D.MiddlePosX;
                    dscList[j].Adjust.AdjustY = avgY - dscList[j].Points2D.MiddlePosY;
                    dscList[j].Adjust.Scale = targetDist / dist;
                    dscList[j].Adjust.RotateX = dscList[j].Points2D.MiddlePosX;
                    dscList[j].Adjust.RotateY = dscList[j].Points2D.MiddlePosY;
                }

                // Margin 계산				

                #region Margin 계산

                List<RectangleF> lstMinRect = new List<RectangleF>();
                RectangleF rtMinRect = new RectangleF(0, 0, dscList[regList[i].First].Width, dscList[regList[i].First].Height);

                for (int j = regList[i].First; j <= regList[i].Second; j++)
                {
                    if (dscList[j].Adjust.AdjustX == 0 || dscList[j].Adjust.AdjustY == 0 || dscList[j].Points2D.MiddlePosX == -1 || dscList[j].Points2D.MiddlePosY == -1)
                    {
                        dscList[j].Adjust.RectMargin = new RectangleF(0, 0, 0, 0);
                        continue;
                    }

                    int nWidth, nHeight;
                    Point2f pt1, pt2, pt3, pt4, ptR1, ptR2, ptR3, ptR4;

                    Point2f ptCenter = new Point2f((float)dscList[j].Adjust.RotateX, (float)dscList[j].Adjust.RotateY);

                    nWidth = dscList[j].Width;
                    nHeight = dscList[j].Height;

                    double dbAngleAdjust = -1 * (dscList[j].Adjust.Angle + 90);
                    double dbRAngle = dbAngleAdjust * Math.PI / 180;

                    // Get Resize Rect Point
                    pt1.X = (float)(ptCenter.X * (1 - dscList[j].Adjust.Scale));
                    pt1.Y = (float)(ptCenter.Y * (1 - dscList[j].Adjust.Scale));

                    pt2.X = (float)(pt1.X + nWidth * dscList[j].Adjust.Scale);
                    pt2.Y = pt1.Y;

                    pt3.X = pt2.X;
                    pt3.Y = (float)(pt1.Y + nHeight * dscList[j].Adjust.Scale);

                    pt4.X = pt1.X;
                    pt4.Y = pt3.Y;

                    ptR1 = FDAdjust.GetRotatePoint(ptCenter, pt1, dbRAngle);
                    ptR2 = FDAdjust.GetRotatePoint(ptCenter, pt2, dbRAngle);
                    ptR3 = FDAdjust.GetRotatePoint(ptCenter, pt3, dbRAngle);
                    ptR4 = FDAdjust.GetRotatePoint(ptCenter, pt4, dbRAngle);

                    int nNewMarginLeft = 0;
                    int nNewMarginTop = 0;
                    int nNewMarginRight = nWidth;
                    int nNewMarginBottom = nHeight;

                    if (ptR1.X + dscList[j].Adjust.AdjustX > nNewMarginLeft)
                        nNewMarginLeft = (int)(ptR1.X + dscList[j].Adjust.AdjustX);
                    if (ptR1.Y + dscList[j].Adjust.AdjustY > nNewMarginTop)
                        nNewMarginTop = (int)(ptR1.Y + dscList[j].Adjust.AdjustY);

                    if (ptR2.X + dscList[j].Adjust.AdjustX < nNewMarginRight)
                        nNewMarginRight = (int)(ptR2.X + dscList[j].Adjust.AdjustX);
                    if (ptR2.Y + dscList[j].Adjust.AdjustY > nNewMarginTop)
                        nNewMarginTop = (int)(ptR2.Y + dscList[j].Adjust.AdjustY);

                    if (ptR3.X + dscList[j].Adjust.AdjustX < nNewMarginRight)
                        nNewMarginRight = (int)(ptR3.X + dscList[j].Adjust.AdjustX);
                    if (ptR3.Y + dscList[j].Adjust.AdjustY < nNewMarginBottom)
                        nNewMarginBottom = (int)(ptR3.Y + dscList[j].Adjust.AdjustY);

                    if (ptR4.X + dscList[j].Adjust.AdjustX > nNewMarginLeft)
                        nNewMarginLeft = (int)(ptR4.X + dscList[j].Adjust.AdjustX);
                    if (ptR4.Y + dscList[j].Adjust.AdjustY < nNewMarginBottom)
                        nNewMarginBottom = (int)(ptR4.Y + dscList[j].Adjust.AdjustY);

                    if (nNewMarginLeft > nNewMarginTop * nWidth / nHeight)
                        nNewMarginTop = nNewMarginLeft * nHeight / nWidth;
                    else
                        nNewMarginLeft = nNewMarginTop * nWidth / nHeight;

                    if (nNewMarginRight < nNewMarginBottom * nWidth / nHeight)
                        nNewMarginBottom = nNewMarginRight * nHeight / nWidth;
                    else
                        nNewMarginRight = nNewMarginBottom * nWidth / nHeight;

                    // Margin 뒤집힘 현상 임시 방지
                    if (nNewMarginLeft > nNewMarginRight)
                    {
                        int nTemp = nNewMarginLeft;
                        nNewMarginLeft = nNewMarginRight;
                        nNewMarginRight = nTemp;
                    }
                    if (nNewMarginTop > nNewMarginBottom)
                    {
                        int nTemp = nNewMarginTop;
                        nNewMarginTop = nNewMarginBottom;
                        nNewMarginBottom = nTemp;
                    }

                    lstMinRect.Add(new RectangleF(nNewMarginLeft, nNewMarginTop, nNewMarginRight - nNewMarginLeft, nNewMarginBottom - nNewMarginTop));
                }

                int nMinTop = 0;
                int nMinLeft = 0;
                int nMinRight = dscList[regList[i].First].Width;
                int nMinBottom = dscList[regList[i].First].Height;

                for (int j = 0; j < lstMinRect.Count; j++)
                {
                    if (lstMinRect[j].Left > nMinLeft && lstMinRect[j].Top > nMinTop)
                    {
                        nMinLeft = (int)lstMinRect[j].Left;
                        nMinTop = (int)lstMinRect[j].Top;

                        rtMinRect = new RectangleF(nMinLeft, nMinTop, rtMinRect.Right - nMinLeft, rtMinRect.Bottom - nMinTop);
                    }

                    if (lstMinRect[j].Right < nMinRight && lstMinRect[j].Bottom < nMinBottom)
                    {
                        if (lstMinRect[j].Right == 0 || lstMinRect[j].Bottom == 0)
                            continue;

                        nMinRight = (int)lstMinRect[j].Right;
                        nMinBottom = (int)lstMinRect[j].Bottom;

                        rtMinRect = new RectangleF(rtMinRect.Left, rtMinRect.Top, nMinRight - rtMinRect.Left, nMinBottom - rtMinRect.Top);
                    }
                }

                for (int j = regList[i].First; j <= regList[i].Second; j++)
                {
                    dscList[j].Adjust.RectMargin = rtMinRect;
                    dscList[j].IsAdj = true;
                }
                #endregion
            }

            return true;
        }

        public static bool DrawCalibrationBar(ref Mat img, in WorldCoordinates4d wc, in DscInfo dscInfo)
        {
            if (wc.X1 == -1 || wc.Y1 == -1 || wc.X2 == -1 || wc.Y2 == -1 || wc.X3 == -1 || wc.Y3 == -1 || wc.X4 == -1 || wc.Y4 == -1)
                return false;

            double dSensorSize = 17.30;

            if (dscInfo.Width == 3840)
                dSensorSize = 17.30 / 1.35;
            else
                dSensorSize = 17.30;

            double dFocal;
            double dFocalLength = dscInfo.FocalLength;
            if (dFocalLength <= 0)
            {
                dFocal = dscInfo.Width;
            }
            else
            {
                dFocal = (dFocalLength / dSensorSize) * dscInfo.Width;
            }

            var mCameraParameter = new float[,] {
                        {(float)dFocal, 0, dscInfo.Width / 2 },
                        { 0, (float)dFocal, dscInfo.Height / 2 },
                        { 0, 0, 1 }
                    };
            var mDistCoeffs = new float[] { 0, 0, 0, 0 };

            List<Point3f> ptsSquare3d = new List<Point3f>();
            ptsSquare3d.Add(new Point3f((float)wc.X1, (float)wc.Y1, 0));
            ptsSquare3d.Add(new Point3f((float)wc.X2, (float)wc.Y2, 0));
            ptsSquare3d.Add(new Point3f((float)wc.X3, (float)wc.Y3, 0));
            ptsSquare3d.Add(new Point3f((float)wc.X4, (float)wc.Y4, 0));

            Normalization3DSquarePoint(ref ptsSquare3d, 100);

            List<Point2f> pts2d = new List<Point2f>();
            pts2d.Add(new Point2f((float)dscInfo.Points3D.X1, (float)dscInfo.Points3D.Y1 + dscInfo.ManualOffsetY));
            pts2d.Add(new Point2f((float)dscInfo.Points3D.X2, (float)dscInfo.Points3D.Y2 + dscInfo.ManualOffsetY));
            pts2d.Add(new Point2f((float)dscInfo.Points3D.X3, (float)dscInfo.Points3D.Y3 + dscInfo.ManualOffsetY));
            pts2d.Add(new Point2f((float)dscInfo.Points3D.X4, (float)dscInfo.Points3D.Y4 + dscInfo.ManualOffsetY));

            Mat _mRotationMatrix = new Mat(3, 1, MatType.CV_32F);
            Mat _mTranslationMatrix = new Mat(3, 1, MatType.CV_32F);

            Cv2.SolvePnP(
                new Mat(ptsSquare3d.Count, 3, MatType.CV_32F, ptsSquare3d.ToArray()),
                new Mat(pts2d.Count, 2, MatType.CV_32F, pts2d.ToArray()),
                new Mat(3, 3, MatType.CV_32F, mCameraParameter),
                new Mat(4, 1, MatType.CV_32F, mDistCoeffs),
                _mRotationMatrix, _mTranslationMatrix, false, SolvePnPFlags.Iterative);

            List<Point3f> normalVector = new List<Point3f>();
            int nLocation = FDAdjust.MaxRange / 2;
            int nLength = 6;
            normalVector.Add(new Point3f(nLocation, nLocation, 0));
            normalVector.Add(new Point3f(nLocation + nLength, nLocation, 0));
            normalVector.Add(new Point3f(nLocation, nLocation + nLength, 0));
            normalVector.Add(new Point3f(nLocation, nLocation, -nLength * 10));
            normalVector.Add(new Point3f(nLocation - nLength, nLocation, 0));
            normalVector.Add(new Point3f(nLocation, nLocation - nLength, 0));

            Mat projectedNormalVector = new Mat();
            Cv2.ProjectPoints(new Mat(normalVector.Count, 3, MatType.CV_32F, normalVector.ToArray()), _mRotationMatrix, _mTranslationMatrix, new Mat(3, 3, MatType.CV_32F, mCameraParameter), new Mat(4, 1, MatType.CV_32F, mDistCoeffs), projectedNormalVector);

            var colors = new Scalar[]
            {
                new Scalar(0, 0, 255),
                new Scalar(0, 255, 0),
                new Scalar(255, 0, 0),
                new Scalar(255, 255, 0),
                new Scalar(255, 0, 255)
            };

            for (int i = 1; i < projectedNormalVector.Rows; i++)
            {
                Vec2f vecA = projectedNormalVector.At<Vec2f>(0);
                Vec2f vecB = projectedNormalVector.At<Vec2f>(i);

                OpenCvSharp.Point ptA = new OpenCvSharp.Point(vecA[0], vecA[1]);
                OpenCvSharp.Point ptB = new OpenCvSharp.Point(vecB[0], vecB[1]);

                Cv2.Line(img, ptA, ptB, colors[i - 1], 3, LineTypes.AntiAlias, 0);
            }

            return true;
        }

        public static bool DrawROI(ref Mat img, in RectangleF rtMargin)
        {
            Rect rt = new Rect((int)Math.Round(rtMargin.X), (int)Math.Round(rtMargin.Y), (int)Math.Round(rtMargin.Width), (int)Math.Round(rtMargin.Height));
            Cv2.Rectangle(img, rt, Scalar.Red, 3);
            return true;
        }

        public static bool PreviewImage(in Mat src, ref Mat dst, OpenCvSharp.Size szDst, Mat rotMat, double dX, double dY)
        {
            if (szDst.Width == 0 || szDst.Height == 0 || rotMat.Empty())
                return false;

            Cv2.WarpAffine(src, dst, rotMat, src.Size());


            int nCutTop, nCutHeight, nCutLeft, nCutWidth;
            int nPasteTop = 0, nPasteBottom = 0, nPasteLeft = 0, nPasteRight = 0;

            if (dX > 0)
            {
                nCutLeft = 0;
                nCutWidth = (int)(src.Cols - dX);

                nPasteLeft = (int)dX;
            }
            else
            {
                nCutLeft = (int)-dX;
                nCutWidth = (int)(src.Cols + dX);

                nPasteRight = (int)-dX;
            }

            if (dY > 0)
            {
                nCutTop = 0;
                nCutHeight = (int)(src.Rows - dY);

                nPasteTop = (int)dY;
            }
            else
            {
                nCutTop = (int)-dY;
                nCutHeight = (int)(src.Rows + dY);

                nPasteBottom = (int)-dY;
            }

            Mat gMatCut = dst.SubMat(new Rect(nCutLeft, nCutTop, nCutWidth, nCutHeight));
            Mat gMatPaste = new Mat();
            Cv2.CopyMakeBorder(gMatCut, gMatPaste, nPasteTop, nPasteBottom, nPasteLeft, nPasteRight, BorderTypes.Constant, 0);
            gMatPaste.CopyTo(dst);

            Cv2.Resize(dst, dst, szDst);

            return true;
        }

        //public static bool AdjustImage(in Mat src, ref Mat dst, OpenCvSharp.Size szDst, AdjustInfo adj)
        //      {
        //          if (szDst.Width == 0 || szDst.Height == 0)
        //              szDst = src.Size();

        //          double dMoveX, dMoveY;
        //          dMoveX = adj.AdjustX;
        //          dMoveY = adj.AdjustY;

        //          double dRotateX, dRotateY;
        //          dRotateX = adj.RotateX;
        //          dRotateY = adj.RotateY;

        //	double dScale = adj.Scale;

        //	double dAngle = -(adj.Angle + 90.0);
        //          double dRad = dAngle * Math.PI / 180.0;
        //          double cosX, cosY, sinX, sinY;
        //          cosX = dScale * Math.Cos(dRad);
        //          cosY = dScale * Math.Cos(dRad);
        //          sinX = dScale * Math.Sin(dRad);
        //          sinY = dScale * Math.Sin(dRad);

        //          double val00, val10, val20, val01, val11, val21;

        //	val00 = cosX;
        //	val10 = sinY;
        //	val01 = -sinX;
        //	val11 = cosY;
        //	val20 = (-( cosX * dRotateX + sinX * dRotateY) + (dRotateX + dMoveX));
        //	val21 = (-(-sinX * dRotateX + cosX * dRotateY) + (dRotateY + dMoveY));

        //	Mat m = new Mat(2, 3, MatType.CV_32FC1);
        //          m.Set<float>(0, 0, (float)val00);
        //          m.Set<float>(0, 1, (float)val10);
        //          m.Set<float>(0, 2, (float)val20);
        //          m.Set<float>(1, 0, (float)val01);
        //          m.Set<float>(1, 1, (float)val11);
        //          m.Set<float>(1, 2, (float)val21);

        //	Mat res = new Mat();

        //          src.Circle(new OpenCvSharp.Point(adj.RotateX, adj.RotateY), 10, Scalar.FromRgb(255, 0, 0), 5);

        //          res = Mat.Zeros((int)src.Cols, (int)src.Rows, src.Type());
        //          Cv2.WarpAffine(src, res, m, src.Size(), InterpolationFlags.Linear, BorderTypes.Constant);

        //	if (src.Cols != szDst.Width)
        //		Cv2.Resize(res, dst, szDst);
        //	else
        //		dst = res;

        //          return true;
        //      }

        public static bool AdjustImage(in Mat src, ref Mat dst, OpenCvSharp.Size szDst, AdjustInfo adj, bool isFlip = false)
        {
            if (szDst.Width == 0 || szDst.Height == 0)
                szDst = src.Size();

            double dMoveX, dMoveY;
            dMoveX = adj.AdjustX;
            dMoveY = adj.AdjustY;

            double dRotateX, dRotateY;
            dRotateX = adj.RotateX;
            dRotateY = adj.RotateY;

            double dScale = adj.Scale;

            double dAngle = (adj.Angle + 90.0);
            double dRad = dAngle * Math.PI / 180.0;


            Mat m, mTfm, mRot, mTrn, mScale, mFlip, mScaleOutput;

            MatrixCalculator.GetFlipMatrix(src.Cols, src.Height, isFlip, isFlip, out mFlip);
            MatrixCalculator.GetRotationMatrix((float)dRad, (float)dRotateX, (float)dRotateY, out mRot);
            MatrixCalculator.GetScaleMarix((float)dScale, (float)dScale, (float)dRotateX, (float)dRotateY, out mScale);
            MatrixCalculator.GetTranslationMatrix((float)dMoveX, (float)dMoveY, out mTrn);
            MatrixCalculator.GetScaleMarix((float)szDst.Width / src.Cols, (float)szDst.Height / src.Rows, out mScaleOutput);

            mTfm = mScaleOutput * mTrn * mScale * mRot * mFlip;

            MatrixCalculator.ParseAffineMatrix(mTfm, out m);


            Mat res = new Mat();

            if (!isFlip)
                src.Circle(new OpenCvSharp.Point(adj.RotateX, adj.RotateY), 10, Scalar.FromRgb(255, 0, 0), 5);
            else
                src.Circle(new OpenCvSharp.Point(src.Cols - adj.RotateX, src.Rows - adj.RotateY), 10, Scalar.FromRgb(255, 0, 0), 5);

            res = Mat.Zeros(szDst, src.Type());
            Cv2.WarpAffine(src, res, m, szDst, InterpolationFlags.Linear, BorderTypes.Constant);

            dst = res;

            return true;
        }

        public static Mat AdjustImage(in Mat src, OpenCvSharp.Size szDst, AdjustInfo adj, bool isFlip = false)
        {
            if (szDst.Width == 0 || szDst.Height == 0)
                szDst = src.Size();

            double dMoveX, dMoveY;
            dMoveX = adj.AdjustX;
            dMoveY = adj.AdjustY;

            double dRotateX, dRotateY;
            dRotateX = adj.RotateX;
            dRotateY = adj.RotateY;

            double dScale = adj.Scale;

            double dAngle = (adj.Angle + 90.0);
            double dRad = dAngle * Math.PI / 180.0;


            Mat m, mTfm, mRot, mTrn, mScale, mFlip, mScaleOutput;

            MatrixCalculator.GetFlipMatrix(src.Cols, src.Height, isFlip, isFlip, out mFlip);
            MatrixCalculator.GetRotationMatrix((float)dRad, (float)dRotateX, (float)dRotateY, out mRot);
            MatrixCalculator.GetScaleMarix((float)dScale, (float)dScale, (float)dRotateX, (float)dRotateY, out mScale);
            MatrixCalculator.GetTranslationMatrix((float)dMoveX, (float)dMoveY, out mTrn);
            MatrixCalculator.GetScaleMarix((float)szDst.Width / src.Cols, (float)szDst.Height / src.Rows, out mScaleOutput);

            mTfm = mScaleOutput * mTrn * mScale * mRot * mFlip;

            MatrixCalculator.ParseAffineMatrix(mTfm, out m);


            Mat res = new Mat();

            res = Mat.Zeros(szDst, src.Type());
            Cv2.WarpAffine(src, res, m, szDst, InterpolationFlags.Linear, BorderTypes.Constant);

            Rect rt = new Rect((int)Math.Round(adj.RectMargin.X), (int)Math.Round(adj.RectMargin.Y), (int)Math.Round(adj.RectMargin.Width), (int)Math.Round(adj.RectMargin.Height));
            return res.SubMat(rt);
        }


        public static Point2f GetRotatedCoord(in Point2f ptSrc, in float deg)
        {
            float r = (float)(Cv2.PI / 180.0);
            float rad = deg * r;
            float s = (float)Math.Sin(rad);
            float c = (float)Math.Cos(rad);
            float x = ptSrc.X * c - (ptSrc.Y * s);
            float y = ptSrc.X * s + (ptSrc.Y * c);

            return new Point2f(x, y);
        }
        public static Point2f GetTransformCoord(in Point2f ptSrc, in float mx, in float my, in float scale, in float deg, in float rx, in float ry)
        {

            float r = (float)(Cv2.PI / 180.0);
            float rad = deg * r;
            float s = (float)Math.Sin(rad);
            float c = (float)Math.Cos(rad);
            float x = ((ptSrc.X - rx) * c - ((ptSrc.Y - ry) * s)) * scale + rx + mx;
            float y = ((ptSrc.X - rx) * s + ((ptSrc.Y - ry) * c)) * scale + ry + my;

            return new Point2f(x, y);
        }

        public static bool CalCropHomography(ref List<DscInfo> dscList)
        {
            if (dscList.Count == 0)
                return false;

            try
            {
                int nW = dscList[0].Width;
                int nH = dscList[0].Height;

                List<Point2f> cornersRef = new List<Point2f>();
                cornersRef.Add(new Point2f(0, 0));
                cornersRef.Add(new Point2f(nW, 0));
                cornersRef.Add(new Point2f(nW, nH));
                cornersRef.Add(new Point2f(0, nH));

                // Warping 된 좌표 취득
                List<List<Point2f>> ptsWarp = new List<List<Point2f>>();

                ptsWarp.Add(cornersRef);

                for (int i = 0; i < dscList.Count; i++)
                {
                    if (!dscList[i].CheckHomoT())
                        continue;

                    Mat mHomo = new Mat(3, 3, MatType.CV_32FC1);
                    for (int j = 0; j < 3; j++)
                    {
                        mHomo.At<float>(0, j) = (float)dscList[i].Homo_T.r1[j];
                        mHomo.At<float>(1, j) = (float)dscList[i].Homo_T.r2[j];
                        mHomo.At<float>(2, j) = (float)dscList[i].Homo_T.r3[j];
                    }

                    //Mat mScalePreset, mScaleOrigin;
                    //OpenCvSharp.Size2f szPreset = new OpenCvSharp.Size2f(960.0f, 540.0f);
                    //MatrixCalculator.GetScaleMarix(szPreset.Width / (float)nW, szPreset.Height / (float)nH, out mScalePreset);
                    //MatrixCalculator.GetScaleMarix((float)nW / szPreset.Width, (float)nH / szPreset.Height, out mScaleOrigin);
                    //Mat mTfm = mScaleOrigin * mHomo * mScalePreset;

                    List<Point2f> cornersW = new List<Point2f>(Cv2.PerspectiveTransform(cornersRef, mHomo));

                    float fBound = 0.5f;

                    if (cornersW[0].X > nW * fBound || cornersW[0].Y > nH * fBound || cornersW[1].X > nW * (1.0f - fBound) || cornersW[1].Y > nH * fBound
                        || cornersW[2].X < nW * (1.0f - fBound) || cornersW[2].Y < nH * (1.0f - fBound) || cornersW[3].X < nW * fBound || cornersW[3].Y < nH * (1.0f - fBound))
                    {
                        dscList[i].Homo_T.r1[0] = dscList[i].Homo_T.r2[1] = dscList[i].Homo_T.r3[2] = 1.0;
                        dscList[i].Homo_T.r1[1] = dscList[i].Homo_T.r1[2] = dscList[i].Homo_T.r2[0] = dscList[i].Homo_T.r2[2] = dscList[i].Homo_T.r3[0] = dscList[i].Homo_T.r3[1] = 0.0;
                    }
                    else
                    {
                        ptsWarp.Add(cornersW);
                    }
                }

                if (ptsWarp.Count < 2)
                    return false;

                List<ConvexPolygon2D> listPoly = new List<ConvexPolygon2D>();

                foreach (List<Point2f> corners in ptsWarp)
                {
                    listPoly.Add(new ConvexPolygon2D(corners.ToArray()));
                }

                ConvexPolygon2D polyInter = listPoly[0];

                for (int i = 1; i < listPoly.Count; i++)
                {
                    polyInter = CropMarginCalculator.GetIntersectionOfPolygons(polyInter, listPoly[i]);
                }

                // 공집합 영역 계산
                List<Point2f> ptsInter = polyInter.ToPoint2fList();

                // Crop 영역 계산
                Rect2f rtCrop = CropMarginCalculator.GetCentroidRectangleInsidePolygon(ptsInter, (double)nW / (double)nH);

                // Crop을 위한 Mat로 변환
                Mat mCrop;
                MatrixCalculator.GetMarginMatrix(nW, nH, (int)rtCrop.X, (int)rtCrop.Y, (int)rtCrop.Width, (int)rtCrop.Height, out mCrop);


                // 각 DscInfo내 Homography 행렬 값 Crop처리한 Homography로 변경

                for (int i = 0; i < dscList.Count; i++)
                {
                    if (!dscList[i].CheckHomoT())
                        continue;

                    Mat mHomo = new Mat(3, 3, MatType.CV_32FC1);
                    for (int j = 0; j < 3; j++)
                    {
                        mHomo.At<float>(0, j) = (float)dscList[i].Homo_T.r1[j];
                        mHomo.At<float>(1, j) = (float)dscList[i].Homo_T.r2[j];
                        mHomo.At<float>(2, j) = (float)dscList[i].Homo_T.r3[j];
                    }

                    //Mat mScalePreset, mScaleOrigin;
                    //OpenCvSharp.Size2f szPreset = new OpenCvSharp.Size2f(960.0f, 540.0f);
                    //MatrixCalculator.GetScaleMarix(szPreset.Width / (float)nW, szPreset.Height / (float)nH, out mScalePreset);
                    //MatrixCalculator.GetScaleMarix((float)nW / szPreset.Width, (float)nH / szPreset.Height, out mScaleOrigin);

                    //// Crop matrix convolution
                    Mat mTfm = mCrop * mHomo;

                    for (int j = 0; j < 3; j++)
                    {
                        dscList[i].Homo_T.r1[j] = mTfm.At<float>(0, j);
                        dscList[i].Homo_T.r2[j] = mTfm.At<float>(1, j);
                        dscList[i].Homo_T.r3[j] = mTfm.At<float>(2, j);
                    }
                }
            }
            catch(Exception ex)
            {
                return false;
            }

            return true;
        }
    }
}