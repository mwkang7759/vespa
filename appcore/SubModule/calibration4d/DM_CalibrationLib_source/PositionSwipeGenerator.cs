﻿using OpenCvSharp;
using System;
using System.Collections.Generic;

namespace FDUtilities
{
    public class PositionSwipeGenerator
    {
        public PositionSwipeGenerator()
        {
            m_nWidth = 0;
            m_nHeight = 0;
            m_nPositionSwipeLength = 0;

            m_listStFourPoints = new List<StFourPoints>();
            m_listStAdjustData = new List<StAdjustData>();

        }

        ~PositionSwipeGenerator()
        {

        }

        public int SetPositionSwipeHeaderData(int nWidth, int nHeight, int nPositionSwipeLength)
        {
            m_nWidth = nWidth;
            m_nHeight = nHeight;
            m_nPositionSwipeLength = nPositionSwipeLength;

            return (int)POSITION_SWIPE_GENERATOR.POSITION_SWIPE_GENERATOR_OK;
        }

        public int AddAdjustData(int liveChannelID, AdjustInfo adjust)
        {
            return AddAdjustData(liveChannelID,
                adjust.AdjustX,
                adjust.AdjustY,
                adjust.RotateX,
                adjust.RotateY,
                adjust.Angle,
                adjust.Scale,
                (int)adjust.RectMargin.Top,
                (int)adjust.RectMargin.Left,
                (int)adjust.RectMargin.Bottom,
                (int)adjust.RectMargin.Right
                );
        }
        public int AddAdjustData(
            int nChannel,
            double dMoveX, double dMoveY,
            double dRotateX, double dRotateY,
            double dAngle, double dScale,
            int nTop, int nLeft, int nBottom, int nRight)
        {
            int nRet = SetAdjustData(nChannel, dMoveX, dMoveY, dRotateX, dRotateY, dAngle, dScale, nTop, nLeft, nBottom, nRight);

            if (nRet == (int)POSITION_SWIPE_GENERATOR.POSITION_SWIPE_GENERATOR_OK)
            {
                return (int)POSITION_SWIPE_GENERATOR.POSITION_SWIPE_GENERATOR_ERROR_ADJUST_DATA_ALREADY_EXIST;
            }
            else
            {
                StAdjustData stAdjustData = new StAdjustData(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

                stAdjustData.nChannel = nChannel;

                stAdjustData.dAdjustMoveX = dMoveX;
                stAdjustData.dAdjustMoveY = dMoveY;
                stAdjustData.dAdjustRotateX = dRotateX;
                stAdjustData.dAdjustRotateY = dRotateY;
                stAdjustData.dAdjustAngle = dAngle;
                stAdjustData.dAdjustScale = dScale;

                stAdjustData.nTop = nTop;
                stAdjustData.nLeft = nLeft;
                stAdjustData.nBottom = nBottom;
                stAdjustData.nRight = nRight;

                m_listStAdjustData.Add(stAdjustData);
                //m_vecStAdjustData.push_back(stAdjustData);

                return (int)POSITION_SWIPE_GENERATOR.POSITION_SWIPE_GENERATOR_OK;
            }
        }

        public int SetAdjustData(
            int nChannel,
            double dMoveX, double dMoveY,
            double dRotateX, double dRotateY,
            double dAngle, double dScale,
            int nTop, int nLeft, int nBottom, int nRight)
        {
            int nIndex = 0;
            nIndex = IsExistAdjustData(nChannel);

            if (nIndex == (int)POSITION_SWIPE_GENERATOR.POSITION_SWIPE_GENERATOR_ERROR_ADJUST_DATA_IS_NOT_EXIST)
                return (int)POSITION_SWIPE_GENERATOR.POSITION_SWIPE_GENERATOR_ERROR_ADJUST_DATA_IS_NOT_EXIST;

            if (nIndex == (int)POSITION_SWIPE_GENERATOR.POSITION_SWIPE_GENERATOR_ERROR_ADJUST_DATA_IS_EMPTY)
                return (int)POSITION_SWIPE_GENERATOR.POSITION_SWIPE_GENERATOR_ERROR_ADJUST_DATA_IS_EMPTY;

            StAdjustData stAdjustData = m_listStAdjustData[nIndex];

            stAdjustData.nChannel = nChannel;

            stAdjustData.dAdjustMoveX = dMoveX;
            stAdjustData.dAdjustMoveY = dMoveY;
            stAdjustData.dAdjustRotateX = dRotateX;
            stAdjustData.dAdjustRotateY = dRotateY;
            stAdjustData.dAdjustAngle = dAngle;
            stAdjustData.dAdjustScale = dScale;

            stAdjustData.nTop = nTop;
            stAdjustData.nLeft = nLeft;
            stAdjustData.nBottom = nBottom;
            stAdjustData.nRight = nRight;

            m_listStAdjustData[nIndex] = stAdjustData;

            return (int)POSITION_SWIPE_GENERATOR.POSITION_SWIPE_GENERATOR_OK;
        }


        public int IsExistAdjustData(int nChannel)
        {
            if (GetAdjustDataSize() <= 0) return (int)POSITION_SWIPE_GENERATOR.POSITION_SWIPE_GENERATOR_ERROR_ADJUST_DATA_IS_EMPTY;

            for (int i = 0; i < m_listStAdjustData.Count; i++)
            {
                if (m_listStAdjustData[i].nChannel == nChannel)
                {
                    return i;
                }
            }

            return (int)POSITION_SWIPE_GENERATOR.POSITION_SWIPE_GENERATOR_ERROR_ADJUST_DATA_IS_NOT_EXIST;
        }

        public int GetAdjustDataSize()
        {
            return m_listStAdjustData.Count;
        }
        public int AddFourPoints(int liveIndex, ReferencePoint3D points3D, int manualOffsetY)
        {
            return AddFourPoints(liveIndex,
            (int)points3D.X1, (int)points3D.Y1 + manualOffsetY,
            (int)points3D.X2, (int)points3D.Y2 + manualOffsetY,
            (int)points3D.X3, (int)points3D.Y3 + manualOffsetY,
            (int)points3D.X4, (int)points3D.Y4 + manualOffsetY);
        }
        public int AddFourPoints(
            int nChannel,
            int nX1, int nY1,
            int nX2, int nY2,
            int nX3, int nY3,
            int nX4, int nY4)
        {
            int nRet = SetFourPoints(nChannel, nX1, nY1, nX2, nY2, nX3, nY3, nX4, nY4);

            if (nRet == (int)POSITION_SWIPE_GENERATOR.POSITION_SWIPE_GENERATOR_OK)
            {
                return (int)POSITION_SWIPE_GENERATOR.POSITION_SWIPE_GENERATOR_ERROR_FOUR_POINTS_ALREADY_EXIST;
            }
            else
            {
                StFourPoints stFourPoints;

                stFourPoints.nChannel = nChannel;

                stFourPoints.nX1 = nX1;
                stFourPoints.nY1 = nY1;
                stFourPoints.nX2 = nX2;
                stFourPoints.nY2 = nY2;
                stFourPoints.nX3 = nX3;
                stFourPoints.nY3 = nY3;
                stFourPoints.nX4 = nX4;
                stFourPoints.nY4 = nY4;

                m_listStFourPoints.Add(stFourPoints);
                //m_vecStFourPoints.push_back(stFourPoints);

                return (int)POSITION_SWIPE_GENERATOR.POSITION_SWIPE_GENERATOR_OK;
            }
        }

        public int SetFourPoints(
            int nChannel,
            int nX1, int nY1,
            int nX2, int nY2,
            int nX3, int nY3,
            int nX4, int nY4)
        {
            int nIndex = 0;
            nIndex = IsExistFourPoints(nChannel);

            if (nIndex == (int)POSITION_SWIPE_GENERATOR.POSITION_SWIPE_GENERATOR_ERROR_FOUR_POINTS_IS_NOT_EXIST)
                return (int)POSITION_SWIPE_GENERATOR.POSITION_SWIPE_GENERATOR_ERROR_FOUR_POINTS_IS_NOT_EXIST;

            if (nIndex == (int)POSITION_SWIPE_GENERATOR.POSITION_SWIPE_GENERATOR_ERROR_FOUR_POINTS_IS_EMPTY)
                return (int)POSITION_SWIPE_GENERATOR.POSITION_SWIPE_GENERATOR_ERROR_FOUR_POINTS_IS_EMPTY;

            StFourPoints stFourPoints = m_listStFourPoints[nIndex];

            stFourPoints.nChannel = nChannel;

            stFourPoints.nX1 = nX1;
            stFourPoints.nY1 = nY1;
            stFourPoints.nX2 = nX2;
            stFourPoints.nY2 = nY2;
            stFourPoints.nX3 = nX3;
            stFourPoints.nY3 = nY3;
            stFourPoints.nX4 = nX4;
            stFourPoints.nY4 = nY4;

            m_listStFourPoints[nIndex] = stFourPoints;

            return (int)POSITION_SWIPE_GENERATOR.POSITION_SWIPE_GENERATOR_OK;
        }
        public int IsExistFourPoints(int nChannel)
        {
            if (GetFourPointsSize() <= 0) return (int)POSITION_SWIPE_GENERATOR.POSITION_SWIPE_GENERATOR_ERROR_FOUR_POINTS_IS_EMPTY;

            for (int i = 0; i < m_listStFourPoints.Count; i++)
            {
                if (m_listStFourPoints[i].nChannel == nChannel)
                {
                    return i;
                }
            }

            return (int)POSITION_SWIPE_GENERATOR.POSITION_SWIPE_GENERATOR_ERROR_FOUR_POINTS_IS_NOT_EXIST;
        }
        public int GetFourPointsSize()
        {
            return m_listStFourPoints.Count;
        }

        public int ConvertArrSquaresUsingAdjustData()
        {
            if (GetAdjustDataSize() <= 0) return (int)POSITION_SWIPE_GENERATOR.POSITION_SWIPE_GENERATOR_ERROR_ADJUST_DATA_IS_EMPTY;
            if (GetFourPointsSize() <= 0) return (int)POSITION_SWIPE_GENERATOR.POSITION_SWIPE_GENERATOR_ERROR_FOUR_POINTS_IS_EMPTY;

            for (int i = 0; i < GetAdjustDataSize(); i++)
            {
                StAdjustData stAdjustData = m_listStAdjustData[i];

                int nFourPointsIndex = IsExistFourPoints(stAdjustData.nChannel);
                if (nFourPointsIndex == (int)POSITION_SWIPE_GENERATOR.POSITION_SWIPE_GENERATOR_ERROR_FOUR_POINTS_IS_NOT_EXIST) continue;

                StFourPoints stFourPoints = m_listStFourPoints[nFourPointsIndex];

                Mat rotationMat;
                rotationMat = GetWarpMat(stAdjustData);

                ConvertSquare(stAdjustData, rotationMat, ref stFourPoints.nX2, ref stFourPoints.nY2);
                ConvertSquare(stAdjustData, rotationMat, ref stFourPoints.nX3, ref stFourPoints.nY3);
                ConvertSquare(stAdjustData, rotationMat, ref stFourPoints.nX4, ref stFourPoints.nY4);
                ConvertSquare(stAdjustData, rotationMat, ref stFourPoints.nX1, ref stFourPoints.nY1);

                m_listStFourPoints[nFourPointsIndex] = stFourPoints;
            }

            return (int)POSITION_SWIPE_GENERATOR.POSITION_SWIPE_GENERATOR_OK;
        }

        public void GeneratePositionSwipePayload()
        {
            int nPositionSwipeDataSize = 0;

            int nByteSize = /*sizeof(short) * */(4 + 9 * GetAdjustDataSize());
            int nShortSize = sizeof(short) * (4 + 9 * GetAdjustDataSize());

            PositionSwipeUnion uPositionSwipeUnion = new PositionSwipeUnion(nByteSize, nShortSize);

            //byte[] szPositionSwipeData = new byte[nByteSize];

            short nWidth = (short)m_nWidth;
            short nHeight = (short)m_nHeight;
            short nPositionLength = (short)m_nPositionSwipeLength;
            short nCount = (short)GetAdjustDataSize();

            uPositionSwipeUnion.s[nPositionSwipeDataSize] = System.Net.IPAddress.HostToNetworkOrder(nWidth);
            nPositionSwipeDataSize++;
            uPositionSwipeUnion.s[nPositionSwipeDataSize] = System.Net.IPAddress.HostToNetworkOrder(nHeight);
            nPositionSwipeDataSize++;
            uPositionSwipeUnion.s[nPositionSwipeDataSize] = System.Net.IPAddress.HostToNetworkOrder(nPositionLength);
            nPositionSwipeDataSize++;
            uPositionSwipeUnion.s[nPositionSwipeDataSize] = System.Net.IPAddress.HostToNetworkOrder(nCount);
            nPositionSwipeDataSize++;

            for (int i = 0; i < GetAdjustDataSize(); i++)
            {
                StAdjustData stAdjustData = m_listStAdjustData[i];

                int nFourPointsIndex = IsExistFourPoints(stAdjustData.nChannel);
                if (nFourPointsIndex == (int)POSITION_SWIPE_GENERATOR.POSITION_SWIPE_GENERATOR_ERROR_FOUR_POINTS_IS_NOT_EXIST) continue;

                StFourPoints stFourPoints = m_listStFourPoints[nFourPointsIndex];

                short nValue = (short)stFourPoints.nChannel;
                uPositionSwipeUnion.s[nPositionSwipeDataSize] = System.Net.IPAddress.HostToNetworkOrder(nValue);
                nPositionSwipeDataSize++;

                nValue = (short)stFourPoints.nX1;
                uPositionSwipeUnion.s[nPositionSwipeDataSize] = System.Net.IPAddress.HostToNetworkOrder(nValue);
                nPositionSwipeDataSize++;

                nValue = (short)stFourPoints.nY1;
                uPositionSwipeUnion.s[nPositionSwipeDataSize] = System.Net.IPAddress.HostToNetworkOrder(nValue);
                nPositionSwipeDataSize++;

                nValue = (short)stFourPoints.nX2;
                uPositionSwipeUnion.s[nPositionSwipeDataSize] = System.Net.IPAddress.HostToNetworkOrder(nValue);
                nPositionSwipeDataSize++;

                nValue = (short)stFourPoints.nY2;
                uPositionSwipeUnion.s[nPositionSwipeDataSize] = System.Net.IPAddress.HostToNetworkOrder(nValue);
                nPositionSwipeDataSize++;

                nValue = (short)stFourPoints.nX3;
                uPositionSwipeUnion.s[nPositionSwipeDataSize] = System.Net.IPAddress.HostToNetworkOrder(nValue);
                nPositionSwipeDataSize++;

                nValue = (short)stFourPoints.nY3;
                uPositionSwipeUnion.s[nPositionSwipeDataSize] = System.Net.IPAddress.HostToNetworkOrder(nValue);
                nPositionSwipeDataSize++;

                nValue = (short)stFourPoints.nX4;
                uPositionSwipeUnion.s[nPositionSwipeDataSize] = System.Net.IPAddress.HostToNetworkOrder(nValue);
                nPositionSwipeDataSize++;

                nValue = (short)stFourPoints.nY4;
                uPositionSwipeUnion.s[nPositionSwipeDataSize] = System.Net.IPAddress.HostToNetworkOrder(nValue);
                nPositionSwipeDataSize++;
            }

            int nPositionSwipeDataByteSize = nPositionSwipeDataSize * 2;
            m_szPositionSwipeData = rbsp2ebsp(ref nPositionSwipeDataByteSize, uPositionSwipeUnion.b, nPositionSwipeDataSize * 2);
            m_nPositionSwipeDataSize = nPositionSwipeDataByteSize;
        }

        public byte[] GetPositionSwipeData()
        {
            return m_szPositionSwipeData;
        }

        public int GetPositionSwipeDataSize()
        {
            return m_nPositionSwipeDataSize;
        }

        private Mat GetWarpMat(StAdjustData stAdjustData)
        {
            double dScale = stAdjustData.dAdjustScale;
            double dAngle = stAdjustData.dAdjustAngle;
            Point2f ptRotateCenter = new Point2f((float)stAdjustData.dAdjustRotateX, (float)stAdjustData.dAdjustRotateY);

            if (dScale == 0)
            {
                dScale = 1.0;
                dAngle += -90.0;
            }

            dAngle = -1 * (dAngle + 90.0);
            Mat mResult = Cv2.GetRotationMatrix2D(ptRotateCenter, dAngle, dScale);
            return mResult;
        }
        private void ConvertSquare(StAdjustData stAdjustData, Mat rotationMat, ref int nX, ref int nY)
        {
            double dbMarginScale = 1.0 / ((stAdjustData.nRight - stAdjustData.nLeft) / (double)m_nWidth);

            double[] tempMat_ = new double[3] { nX, nY, 1.0 };
            Mat tempMat = new Mat(3, 1, MatType.CV_64F, tempMat_);
            Mat resultMat = rotationMat * tempMat;

            nX = (int)resultMat.At<double>(0);
            nY = (int)resultMat.At<double>(1);

            nX += (int)stAdjustData.dAdjustMoveX;
            nY += (int)stAdjustData.dAdjustMoveY;

            int nTempSquareX = nX - stAdjustData.nLeft;
            int nTempSquarey = nY - stAdjustData.nTop;

            nX = (int)(nTempSquareX * dbMarginScale);
            nY = (int)(nTempSquarey * dbMarginScale);

            tempMat.Release();
            resultMat.Release();
        }
        private byte[] rbsp2ebsp(ref int ebsp_size, byte[] rbsp, int rbsp_size)
        {
            byte[] ebsp = new byte[rbsp_size];
            int j = 0;
            int count = 0;

            for (int i = 0; i < rbsp_size; i++)
            {
                bool bEmulationPrevention = Convert.ToBoolean(~(rbsp[i] & 0xFC));
                if (count >= 2 && bEmulationPrevention)
                {
                    j++;
                    System.Array.Resize(ref ebsp, rbsp_size + j - i);
                    ebsp[j - 1] = 0x03;
                    count = 0;
                }
                ebsp[j] = rbsp[i];

                if (rbsp[i] == 0x00)
                    count++;
                else
                    count = 0;
                j++;
            }
            ebsp_size = j;

            return ebsp;
        }


        public struct StFourPoints
        {
            public int nChannel;

            public int nX1;
            public int nY1;
            public int nX2;
            public int nY2;
            public int nX3;
            public int nY3;
            public int nX4;
            public int nY4;

            public StFourPoints(int nChannel, int nX1, int nY1, int nX2, int nY2, int nX3, int nY3, int nX4, int nY4)
            {
                this.nChannel = nChannel;

                this.nX1 = nX1;
                this.nY1 = nY1;
                this.nX2 = nX2;
                this.nY2 = nY2;
                this.nX3 = nX3;
                this.nY3 = nY3;
                this.nX4 = nX4;
                this.nY4 = nY4;
            }
        };

        public struct StAdjustData
        {
            public StAdjustData(
                int nChannel,

                double dAdjustMoveX,
                double dAdjustMoveY,
                double dAdjustRotateX,
                double dAdjustRotateY,
                double dAdjustAngle,
                double dAdjustScale,

                int nTop,
                int nLeft,
                int nBottom,
                int nRight)
            {
                this.nChannel = nChannel;

                this.dAdjustMoveX = dAdjustMoveX;
                this.dAdjustMoveY = dAdjustMoveY;
                this.dAdjustRotateX = dAdjustRotateX;
                this.dAdjustRotateY = dAdjustRotateY;
                this.dAdjustAngle = dAdjustAngle;
                this.dAdjustScale = dAdjustScale;

                this.nTop = 0;
                this.nLeft = 0;
                this.nBottom = 0;
                this.nRight = 0;
            }
            public int nChannel;

            public double dAdjustMoveX;
            public double dAdjustMoveY;
            public double dAdjustRotateX;
            public double dAdjustRotateY;
            public double dAdjustAngle;
            public double dAdjustScale;

            public int nTop;
            public int nLeft;
            public int nBottom;
            public int nRight;
        };

        enum POSITION_SWIPE_GENERATOR
        {
            POSITION_SWIPE_GENERATOR_OK = 0x01,

            POSITION_SWIPE_GENERATOR_ERROR_FOUR_POINTS_IS_NOT_EXIST = -0x02,
            POSITION_SWIPE_GENERATOR_ERROR_FOUR_POINTS_ALREADY_EXIST = -0x03,
            POSITION_SWIPE_GENERATOR_ERROR_ADJUST_DATA_IS_NOT_EXIST = -0x04,
            POSITION_SWIPE_GENERATOR_ERROR_ADJUST_DATA_ALREADY_EXIST = -0x05,
            POSITION_SWIPE_GENERATOR_ERROR_FOUR_POINTS_IS_EMPTY = -0x06,
            POSITION_SWIPE_GENERATOR_ERROR_ADJUST_DATA_IS_EMPTY = -0x07,
        };

        private int m_nWidth;
        private int m_nHeight;
        private int m_nPositionSwipeLength;
        private List<StFourPoints> m_listStFourPoints;
        private List<StAdjustData> m_listStAdjustData;

        private byte[] m_szPositionSwipeData;
        private int m_nPositionSwipeDataSize;

        [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Explicit)]
        struct PositionSwipeUnion
        {
            [System.Runtime.InteropServices.FieldOffset(0)]
            public byte[] b;

            [System.Runtime.InteropServices.FieldOffset(0)]
            public short[] s;

            public PositionSwipeUnion(int b, int s)
            {
                this.b = new byte[b];
                this.s = new short[s];
            }
        }
    }
}
