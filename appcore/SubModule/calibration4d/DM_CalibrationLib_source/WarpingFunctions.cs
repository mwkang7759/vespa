﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenCvSharp;

namespace FDUtilities
{
    partial class VisionFunctions
    {
        private static List<Point> vpx4Scal_szGuidHeight(in Size _sz, in double _h)
        {

            double dX_y = ((double)(_sz.Width) / (double)(_sz.Height));
            int w = ((int)(Math.Round(_h * dX_y))), w_2 = w >> 1, h = (int)(Math.Round(_h)), h_2 = h >> 1;
            Point px0 = new Point(-w_2, -h_2), px1 = new Point(w_2, -h_2), px2 = new Point(w_2, h_2), px3 = new Point(-w_2, h_2);

            List<Point> vpx4_ = new List<Point>(4) { px0, px1, px2, px3 };
            //vpx4_[0] = px0; vpx4_[1] = px1; vpx4_[2] = px2; vpx4_[3] = px3;
            return vpx4_;
        }
        private static List<Point> PointMatToList(Mat mat)
        {
            Point[] copied = new Point[mat.Total() * mat.Channels()];
            mat.GetArray<Point>(out copied);
            return new List<Point>(copied);
        }
        private static Mat PointListToMat(List<Point> list, int r, int c)
        {
            Array arr = list.ToArray();
            var ret = new Mat(new int[] { r, c }, MatType.CV_32FC2, arr);
            return ret;
        }

        public static Mat hmgWarp_px2(in Point _px1, in Point _px2, in List<Point2d> _vpx4Guid)
        {///
            Mat mHmg_ = new Mat();
            Point pxDif = new Point(_px1.X - _px2.X, _px1.Y - _px2.Y);
            double dNorm = Math.Sqrt(pxDif.X * pxDif.X + pxDif.Y * pxDif.Y);
            if (dNorm > 0.0)
            {
                Size sz = new Size(Math.Abs(_vpx4Guid[2].X - _vpx4Guid[0].X), Math.Abs(_vpx4Guid[2].Y - _vpx4Guid[0].Y));
                List<Point> vpx4Scaled = vpx4Scal_szGuidHeight(sz, dNorm);

                float th = (float)(0.5 * Cv2.PI + Math.Atan2((float)(pxDif.Y), (float)(pxDif.X))), cos_th = (float)(Math.Cos(th)), sin_th = (float)(Math.Sin(th));
                Mat mBased0 = PointListToMat(vpx4Scaled, vpx4Scaled.Count, 1);

                var reshape = mBased0.Reshape(1, mBased0.Rows + mBased0.Cols);
                Mat mBased1 = new Mat(mBased0.Reshape(1, mBased0.Rows * mBased0.Cols)).T();
                Mat mBased2 = new Mat(4, 2, MatType.CV_32F);
                mBased1.ConvertTo(mBased2, MatType.CV_32F);
                Mat mRot2d = new Mat(2, 2, MatType.CV_32F);
                mRot2d.At<float>(0, 0) = cos_th;
                mRot2d.At<float>(0, 1) = -sin_th;
                mRot2d.At<float>(1, 0) = sin_th;
                mRot2d.At<float>(1, 1) = cos_th;

                Mat mvpt4 = new Mat(mRot2d * mBased2);

                List<Point2d> vpt4rot = new List<Point2d>(4);
                Point2f ptDif_2 = pxDif * 0.5f, ptDifCent = _px2 + ptDif_2;
                for (int i = 0; i < 4; ++i)
                {
                    float x = mvpt4.At<float>(0, i) + ptDifCent.X;
                    float y = mvpt4.At<float>(1, i) + ptDifCent.Y;
                    vpt4rot[i] = new Point2d(x, y);
                }

                mHmg_ = Cv2.FindHomography(vpt4rot, _vpx4Guid, HomographyMethods.Ransac); 
            }

            return mHmg_;
        }

        public static Mat imgWarp_hmgimg(in Mat _mHmg, in Mat _img)
        {///
            Mat img_ = new Mat();
            Cv2.WarpPerspective(_img, img_, _mHmg, _img.Size());
            return img_;
        }

        public static Mat imgWarp_px2(in Point _px1, in Point _px2, in List<Point2d> _vpx4Guid, in Mat _img)
        {///
            Mat mHmg = hmgWarp_px2(_px1, _px2, _vpx4Guid);
            Mat img_ = imgWarp_hmgimg(mHmg, _img);
            return img_;
        }

        public static List<Point2d> vpx4Warp_hmgvpt4(in Mat _mHmg, in List<Point2d> _vpx4)
        {///
            List<Point2d> vpx4_ = new List<Point2d>();
            vpx4_.AddRange(Cv2.PerspectiveTransform(_vpx4, _mHmg));
            return vpx4_;
        }
        public static List<Point2d> vpx4Warp_px2(in Point _px1, in Point _px2, in List<Point2d> _vpx4Guid, in List<Point2d> _vpx4)
        {///
            Mat mHmg = hmgWarp_px2(_px1, _px2, _vpx4Guid);
            List<Point2d> vpx4_ = vpx4Warp_hmgvpt4(mHmg, _vpx4);
            return vpx4_;

        }
    }
}