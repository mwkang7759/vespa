﻿#include "ESMMovieMakerCore.h"
#include <ESMLocks.h>
#include "cuda_runtime.h"

#include "opencv2/cudawarping.hpp"
#include "opencv2/cudaarithm.hpp"
#include "MatrixCalculator.h"

#if defined(_DEBUG)
#pragma comment(lib, "opencv_core440d.lib")
#pragma comment(lib, "opencv_highgui440d.lib")
#pragma comment(lib, "opencv_imgproc440d.lib")
#pragma comment(lib, "opencv_imgcodecs440d.lib")
#pragma comment(lib, "opencv_calib3d440d.lib")
#pragma comment(lib, "opencv_cudaimgproc440d.lib")
#pragma comment(lib, "opencv_cudawarping440d.lib")
#pragma comment(lib, "opencv_cudaarithm440d.lib")
//#pragma comment(lib, "opencv_videoio440d.lib")
#else
#pragma comment(lib, "opencv_core440.lib")
#pragma comment(lib, "opencv_highgui440.lib")
#pragma comment(lib, "opencv_imgproc440.lib")
#pragma comment(lib, "opencv_imgcodecs440.lib")
#pragma comment(lib, "opencv_calib3d440.lib")
#pragma comment(lib, "opencv_cudaimgproc440.lib")
#pragma comment(lib, "opencv_cudawarping440.lib")
#pragma comment(lib, "opencv_cudaarithm440.lib")
//#pragma comment(lib, "opencv_videoio440.lib")
#endif

ESMMovieMaker::Core::Core(VOID)
	: _encodeInput(NULL)
	, _encodeInputPitch(0)
	, _encodeIndex(0)
{
	_isInitialized = FALSE;

	_bitstream = nullptr;
	_bitstreamCapacity = 0;

	_encodeInput = nullptr;
	_encodeInputPitch = 0;

	_context = nullptr;
	_param = nullptr;

	_frameCount = 0;
	_seekTime = 0;
	_gopSize = 0;
	_startGopNum = 0;
	_globalCnt = 0;
	_localStartCnt = 0;
	_localEndCnt = 0;

#if defined(WITH_SMOOTH_FILTER)
	_smooth = nullptr;
#endif

	_bReverse = false;

	_fw_use_qpc = QueryPerformanceFrequency(&_fw_frequency);
}

ESMMovieMaker::Core::~Core(VOID)
{

}

BOOL ESMMovieMaker::Core::IsInitialized(VOID)
{
	return _isInitialized;
}

int32_t ESMMovieMaker::Core::Initialize(ESMMovieMaker::CONTEXT_T* ctx)
{
	int32_t status = ESMMovieMaker::ERR_CODE_T::GENERIC_FAIL;
	if (ctx == NULL)
		return ESMMovieMaker::ERR_CODE_T::INVALID_PARAMETER;

	_context = ctx;
	_isInitialized = TRUE;
	status = ESMMovieMaker::ERR_CODE_T::SUCCESS;
	return status;
}

int32_t ESMMovieMaker::Core::Release(VOID)
{
	if (_encoder.IsInitialized()) {
		_encoder.Release();
		if (_encodeInput && _encodeInputPitch > 0)
			cudaFree(_encodeInput);
		if (_bitstream && _bitstreamCapacity > 0) {
			free(_bitstream);
			_bitstream = NULL;
		}

	}

	if (_converter.IsInitialized())
		_converter.Release();
	if (_decoder.IsInitialized())
		_decoder.Release();

	_isInitialized = FALSE;
	return ESMMovieMaker::ERR_CODE_T::SUCCESS;
}

int32_t ESMMovieMaker::Core::Process(ESMMovieMaker::PARAM_T* param)
{
	int32_t status = ESMMovieMaker::ERR_CODE_T::GENERIC_FAIL;
	if (!_isInitialized || !param)
		return ESMMovieMaker::ERR_CODE_T::UNINITIALIZED_ERROR;

	_param = param;
	strncpy_s(_demuxerCtx.path, param->srcFile, MAX_PATH);
	//_demuxerCtx.nStartFrameNum = param->nStartFrameNum;
	//_demuxerCtx.nEndFrameNum = param->nEndFrameNum;
	_demuxerCtx.nSrcGopSize = param->nSrcGopSize;
	_demuxerCtx.handler = this;
	if (param->nStartFrameNum > param->nEndFrameNum) {
		// TODO: Time reverse
		//_bReverse = true;
	}
	else {
		_startGopNum = std::stoi(param->frameInfo[param->nStartFrameNum].matchingFrameIdx) / param->nSrcGopSize;
		_localStartCnt = std::stoi(param->frameInfo[param->nStartFrameNum].matchingFrameIdx);
		_localEndCnt = std::stoi(param->frameInfo[param->nEndFrameNum].matchingFrameIdx);
		_globalCnt = param->nStartFrameNum - 1;
	}
	_demuxerCtx.fWantedFPS = _param->fWantedFPS;
	_demuxerCtx.nStartGopNum = _startGopNum;
	_demuxerCtx.nStartFrameNum = _localStartCnt;
	_demuxerCtx.nEndFrameNum = _localEndCnt;

	char err[MAX_PATH] = { 0 };
	status = _demuxer.Demux(&_demuxerCtx);
	switch (status)
	{
	case ESMMovieMaker::ERR_CODE_T::OPEN_INPUT_ERROR:
	{
		//SPd_ERROR("[ESMMovieMaker] Media File is not Exist[{}]", _demuxerCtx.path);
		if (_context && _context->handler)
		{
			_snprintf_s(err, sizeof(err), "[ESMMovieMaker] Media File is not Exist[%s]", _demuxerCtx.path);
			_context->handler->onMovieMakerError(status, err);
		}
		break;
	}
	case ESMMovieMaker::ERR_CODE_T::FIND_STREAM_INFO_ERROR:
	{
		//SPd_ERROR("[ESMMovieMaker] Media File Does Not Contain Video[{}]", _demuxerCtx.path);
		if (_context && _context->handler)
		{
			_snprintf_s(err, sizeof(err), "[ESMMovieMaker] Media File Does Not Contain Video[%s]", _demuxerCtx.path);
			_context->handler->onMovieMakerError(status, err);
		}
		break;
	}
	case ESMMovieMaker::ERR_CODE_T::STREAM_INDEX_ERROR:
	{
		//SPd_ERROR("[ESMMovieMaker] Media File Does Not Contain Video[{}]", _demuxerCtx.path);
		if (_context && _context->handler)
		{
			_snprintf_s(err, sizeof(err), "[ESMMovieMaker] Media File Does Not Contain Video[%s]", _demuxerCtx.path);
			_context->handler->onMovieMakerError(status, err);
		}
		break;
	}
	case ESMMovieMaker::ERR_CODE_T::GOPSIZE_IS_NOT_EQUAL:
	{
		//SPd_ERROR("[ESMMovieMaker] Media File FPS IS Not Same Value Required{}", _param->nSrcGopSize);
		if (_context && _context->handler)
		{
			_snprintf_s(err, sizeof(err), "[ESMMovieMaker] Media File FPS IS Not Same Value Required %d", _param->nSrcGopSize);
			_context->handler->onMovieMakerError(status, err);
		}
		break;
	}
	case ESMMovieMaker::ERR_CODE_T::FRAMECOUNT_DIFFER_FROM_FPS:
	{
		//SPd_ERROR("[ESMMovieMaker] Media File FrameCount Differ From FPS{}", (int32_t)ceil(_param->fWantedFPS));
		if (_context && _context->handler)
		{
			_snprintf_s(err, sizeof(err), "[ESMMovieMaker] Media File FrameCount Differ From FPS %d", (int32_t)ceil(_param->fWantedFPS));
			_context->handler->onMovieMakerError(status, err);
		}
		break;
	}
	case ESMMovieMaker::ERR_CODE_T::FILEWRITE_ERROR:
	{
		//SPd_ERROR("[ESMMovieMaker] Making File Source can not be Created, Check MMd Server Disk Space");
		if (_context && _context->handler)
		{
			_snprintf_s(err, sizeof(err), "[ESMMovieMaker] Making File Source can not be Created, Check MMd Server Disk Space");
			_context->handler->onMovieMakerError(status, err);
		}
		break;
	}
	case ESMMovieMaker::ERR_CODE_T::GENERIC_FAIL:
	{
		//SPd_ERROR("[ESMMovieMaker] Media File Is Not MP4 Format[{}]", _demuxerCtx.path);
		if (_context && _context->handler)
		{
			_snprintf_s(err, sizeof(err), "[ESMMovieMaker] Media File Is Not MP4 Format[%s]", _demuxerCtx.path);
			_context->handler->onMovieMakerError(status, err);
		}
		break;
	}
	}
	return status;

}

int32_t ESMMovieMaker::Core::OnVideoBegin(int32_t codec, const uint8_t* extradata, int32_t extradataSize, int32_t width, int32_t height, double fps, int32_t framecount, int32_t seektime)
{
	if (!_decoder.IsInitialized()) 
	{
		_decoderCtx.deviceIndex = _context->deviceIndex;
		_decoderCtx.codec = codec;
		_decoderCtx.colorspace = ESMNvdec::COLORSPACE_T::BGRA;
		_decoderCtx.width = width;
		_decoderCtx.height = height;
		_decoder.Initialize(&_decoderCtx);
	}
	if (!_converter.IsInitialized()) 
	{
		_converterCtx.deviceIndex = _context->deviceIndex;
		_converterCtx.width = _context->width;
		_converterCtx.height = _context->height;
		_converterCtx.inputColorspace = ESMNvconverter::COLORSPACE_T::BGRA;
		_converterCtx.outputColorspace = ESMNvconverter::COLORSPACE_T::NV12;
		_converter.Initialize(&_converterCtx);
	}
	if (!_encoder.IsInitialized()) 
	{
		_encoderCtx.deviceIndex = _context->deviceIndex;
		_encoderCtx.bitrate = _context->bitrate;
		_encoderCtx.codec = _context->codec;
		_encoderCtx.colorspace = _converterCtx.outputColorspace;
		_encoderCtx.fps_den = _context->fps_den;
		_encoderCtx.fps_num = _context->fps_num;
		_encoderCtx.gop = _context->gop;
		_encoderCtx.width = _context->width;
		_encoderCtx.height = _context->height;
		_encoderCtx.profile = _context->profile;
		_encoder.Initialize(&_encoderCtx);

		cudaMallocPitch((void**)&_encodeInput, &_encodeInputPitch, _encoderCtx.width, (_encoderCtx.height >> 1) * 3);

		_bitstreamCapacity = _context->bitrate << 1;
		_bitstream = (uint8_t*)malloc(_bitstreamCapacity);

#if defined(WITH_SMOOTH_FILTER)
		if(!_smooth)
			_smooth = cv::cuda::createGaussianFilter(CV_8UC4, CV_8UC4, cv::Size(3, 3), 3);
#endif
	}
	int32_t status = ESMMovieMaker::ERR_CODE_T::SUCCESS;
	if (_param->bUseExtradata) 
	{
#if 0
		long long begin = MillisecondsNow();
		long long end = MillisecondsNow();

		int32_t extradataSize = 0;
		uint8_t* extradata = _encoder.GetExtradata(extradataSize);
		std::string filename = _param->dstFilePath;
		filename += "\\extraData.ext";
		status = WriteFile(filename.c_str(), extradata, extradataSize);

		end = MillisecondsNow();
		//SPd_INFO("{}.h264 File Writing Duration is {} msec", (_making_end - _making_begin));
		SPd_INFO("extraData File Writing Duration is {} msec", (end - begin));
#else
		int32_t extradataSize = 0;
		uint8_t* extradata = _encoder.GetExtradata(extradataSize);
		if (_context != NULL && _context->handler != NULL)
			_context->handler->OnExtraData(extradata, extradataSize, _param->dstFilePath);
#endif
	}
	_frameCount = 0;
	//_encodeIndex = 0;
	_seekTime = seektime;
	_gopSize = (int32_t(ceil(fps)) == 60 ? 12 : 10);

	if (status != ESMMovieMaker::ERR_CODE_T::SUCCESS)
		return ESMMovieMaker::ERR_CODE_T::FILEWRITE_ERROR;

	if (_gopSize != _param->nSrcGopSize)
		return ESMMovieMaker::ERR_CODE_T::GOPSIZE_IS_NOT_EQUAL;
	return ESMMovieMaker::ERR_CODE_T::SUCCESS;
}

int32_t ESMMovieMaker::Core::OnVideoRecv(uint8_t* bytes, int32_t nbytes, int32_t& nFrameIdx)
{
	int32_t nDecoded = -1;
	uint8_t** ppDecoded = NULL;
	long long* tsDecoded = 0;
	_decoder.Decode(bytes, nbytes, _frameCount++, &ppDecoded, &nDecoded, &tsDecoded, NULL, NULL);

	int32_t status = ESMMovieMaker::ERR_CODE_T::GENERIC_FAIL;
	for (int32_t idx = 0; idx < nDecoded; idx++) 
	{
		nFrameIdx++;
		if (_localStartCnt <= nFrameIdx && nFrameIdx <= _localEndCnt) 
		{
			_globalCnt++;
			// TODO: Idx for frameInfo should be changed to frame number which is counting
			//cv::cuda::GpuMat img2 = cv::cuda::GpuMat(_decoderCtx.height, _decoderCtx.width, CV_8UC4, ppDecoded[idx], _decoder.GetPitch2()).clone();
			cv::cuda::GpuMat img = cv::cuda::GpuMat(_decoderCtx.height, _decoderCtx.width, CV_8UC4, ppDecoded[idx], _decoder.GetPitch2()).clone();


			//char filename[MAX_PATH] = { 0 };
			//snprintf(filename, MAX_PATH, "before_adjustH_%d_%s.png", _globalCnt, _param->frameInfo[_globalCnt].prefix);
			//cv::Mat download;
			//img.download(download);
			//cv::imwrite(filename, download);

			if (_context && _context->gimbal)
			{
				status = DoAdjustH(img, _param->adjustInfo[_globalCnt], _encoderCtx.width, _encoderCtx.height, img);
			}
			else
			{
				status = DoAdjust(img, _param->adjustInfo[_globalCnt], _encoderCtx.width, _encoderCtx.height, img);
			}

			//snprintf(filename, MAX_PATH, "after_adjustH_%d_%s.png", _globalCnt, _param->frameInfo[_globalCnt].prefix);
			//img.download(download);
			//cv::imwrite(filename, download);

			if (status != ESMMovieMaker::ERR_CODE_T::SUCCESS)
				break;
			status = DoVMCC(img, { _param->frameInfo[_globalCnt].x, _param->frameInfo[_globalCnt].y }, _param->frameInfo[_globalCnt].zoom, _encoderCtx.width, _encoderCtx.height, img);
			if (status != ESMMovieMaker::ERR_CODE_T::SUCCESS)
				break;

			//snprintf(filename, MAX_PATH, "after_vmcc_%d_%s.png", _globalCnt, _param->frameInfo[_globalCnt].prefix);
			//img.download(download);
			//cv::imwrite(filename, download);


			if (_param->bDebugNumber) 
			{
				status = DebugNumberOnMat(img, _globalCnt, _param->frameInfo[_globalCnt].prefix);
				if (status != ESMMovieMaker::ERR_CODE_T::SUCCESS)
					break;
			}
			cv::cuda::cvtColor(img, img, cv::COLOR_BGRA2RGBA);

			status = _converter.ConvertBGRA2YUV(img.data, img.step, _encodeInput, _encodeInputPitch, _context->width, _context->height);
			if (status != ESMMovieMaker::ERR_CODE_T::SUCCESS)
				break;

			//snprintf(filename, MAX_PATH, "before_enc_%d_%s.png", _globalCnt, _param->frameInfo[_globalCnt].prefix);
			//img.download(download);
			//cv::imwrite(filename, download);

			int32_t bitstreamSize = 0;
			long long bitstreamTimestamp = 0;
			status = _encoder.Encode(_encodeInput, _encodeInputPitch, 0, _bitstream, _bitstreamCapacity, bitstreamSize, bitstreamTimestamp);
			if (status != ESMMovieMaker::ERR_CODE_T::SUCCESS)
				break;

			_encodeIndex++;
			img.release();


#if 0
			long long begin = MillisecondsNow();
			long long end = MillisecondsNow();

			std::string filename = _param->dstFilePath;
			filename += "\\";
			filename += std::to_string(_globalCnt);
			filename += ".h264";
			status = WriteFile(filename.c_str(), _bitstream, bitstreamSize);

			end = MillisecondsNow();
			SPd_INFO("{}.h264 File Writing Duration is {} msec", _globalCnt, (end - begin));

			if (status != ESMMovieMaker::ERR_CODE_T::SUCCESS)
				return status;

			if (_context != NULL && _context->handler != NULL) 
				_context->handler->OnEncodingComplete(status, _globalCnt);
#else
			SPd_INFO("{}.h264 File is Sended", _globalCnt);
			if (_context != NULL && _context->handler != NULL)
				_context->handler->OnEncodingComplete(status, _globalCnt, _bitstream, bitstreamSize, std::string(_param->dstFilePath));
#endif

			if (nFrameIdx >= _localEndCnt) 
				return ESMMovieMaker::ERR_CODE_T::FINISH_DECODE;
		}
	}
	return ESMMovieMaker::ERR_CODE_T::ERR_AGAIN;
}

int32_t ESMMovieMaker::Core::OnVideoEnd(int32_t& nFrameIdx)
{
	int32_t nDecoded = -1;
	uint8_t** ppDecoded = NULL;
	long long* tsDecoded = 0;
	_decoder.Decode(NULL, 0, _frameCount++, &ppDecoded, &nDecoded, &tsDecoded, NULL, NULL);

	int32_t status = ESMMovieMaker::ERR_CODE_T::GENERIC_FAIL;
	for (int32_t idx = 0; idx < nDecoded; idx++) 
	{
		nFrameIdx++;
		if (_localStartCnt <= nFrameIdx && nFrameIdx <= _localEndCnt) 
		{
			_globalCnt++;
			// TODO: Idx for frameInfo should be changed to frame number which is counting
			cv::cuda::GpuMat img = cv::cuda::GpuMat(_decoderCtx.height, _decoderCtx.width, CV_8UC4, ppDecoded[idx], _decoder.GetPitch2()).clone();
			if (_context && _context->gimbal)
				status = DoAdjustH(img, _param->adjustInfo[_globalCnt], _encoderCtx.width, _encoderCtx.height, img);
			else
				status = DoAdjust(img, _param->adjustInfo[_globalCnt], _encoderCtx.width, _encoderCtx.height, img);
			if (status != ESMMovieMaker::ERR_CODE_T::SUCCESS)
				break;
			status = DoVMCC(img, { _param->frameInfo[_globalCnt].x, _param->frameInfo[_globalCnt].y }, _param->frameInfo[_globalCnt].zoom, _encoderCtx.width, _encoderCtx.height, img);
			if (status != ESMMovieMaker::ERR_CODE_T::SUCCESS)
				break;
			if (_param->bDebugNumber) 
			{
				status = DebugNumberOnMat(img, _globalCnt, _param->frameInfo[_globalCnt].prefix);
				if (status != ESMMovieMaker::ERR_CODE_T::SUCCESS)
					break;
			}
			cv::cuda::cvtColor(img, img, cv::COLOR_BGRA2RGBA);

			status = _converter.ConvertBGRA2YUV(img.data, img.step, _encodeInput, _encodeInputPitch, _context->width, _context->height);
			if (status != ESMMovieMaker::ERR_CODE_T::SUCCESS)
				break;
			int32_t bitstreamSize = 0;
			long long bitstreamTimestamp = 0;
			status = _encoder.Encode(_encodeInput, _encodeInputPitch, 0, _bitstream, _bitstreamCapacity, bitstreamSize, bitstreamTimestamp);
			if (status != ESMMovieMaker::ERR_CODE_T::SUCCESS)
				break;

			_encodeIndex++;
			img.release();

#if 0
			long long begin = MillisecondsNow();
			long long end = MillisecondsNow();

			std::string filename = _param->dstFilePath;
			filename += "\\";
			filename += std::to_string(_globalCnt);
			filename += ".h264";
			status = WriteFile(filename.c_str(), _bitstream, bitstreamSize);

			end = MillisecondsNow();
			SPd_INFO("{}.h264 File Writing Duration is {} msec", _globalCnt, (end - begin));

			if (status != ESMMovieMaker::ERR_CODE_T::SUCCESS)
				return status;

			if (_context != NULL && _context->handler != NULL) 
				_context->handler->OnEncodingComplete(status, _globalCnt);
#else
			SPd_INFO("{}.h264 File is Sended", _globalCnt);
			if (_context != NULL && _context->handler != NULL)
				_context->handler->OnEncodingComplete(status, _globalCnt, _bitstream, bitstreamSize, std::string(_param->dstFilePath));
#endif

			if (nFrameIdx >= _localEndCnt) 
				return ESMMovieMaker::ERR_CODE_T::FINISH_DECODE;
		}
	}
	return ESMMovieMaker::ERR_CODE_T::SUCCESS;
}

bool ESMMovieMaker::Core::CheckValidH(HOMO_T& t)
{
	if (t.r1[0] == 1.0 && t.r1[1] == 0.0 && t.r1[2] == 0.0
		&& t.r2[0] == 0.0 && t.r2[1] == 1.0 && t.r2[2] == 0.0
		&& t.r3[0] == 0.0 && t.r3[1] == 0.0 && t.r3[2] == 1.0)
		return false;

	return true;
}

int32_t ESMMovieMaker::Core::DoAdjust(cv::cuda::GpuMat& srcMat, ESMMovieMaker::ADJUST_INFO_T& adjustInfo, const int32_t dstWidth, const int32_t dstHeight, cv::cuda::GpuMat& dstMat)
{
	try 
	{
		if (adjustInfo.dAdjustX == 0.0 && adjustInfo.dAdjustY == 0.0) 
		{
			printf("***************************************** Adjust null: %s *****************************************\n", adjustInfo.strDscId.c_str());
		}

		//cv::Size dstSize(srcMat.cols, srcMat.rows);
		//if (dstSize.width == 0 || dstSize.height == 0)
		//	dstSize = srcMat.size();

		cv::Rect2d rtMargin(adjustInfo.dMarginX, adjustInfo.dMarginY, adjustInfo.dMarginW, adjustInfo.dMarginH);

		double dMoveX, dMoveY;
		dMoveX = adjustInfo.dAdjustX;
		dMoveY = adjustInfo.dAdjustY;

		double dRotateX, dRotateY;
		dRotateX = adjustInfo.dRotateX;
		dRotateY = adjustInfo.dRotateY;

		double dAngle = (adjustInfo.dAngle + 90.0);
		double dRad = dAngle * CV_PI / 180.0;

		double dScale = adjustInfo.dScale;

		cv::Mat m, mTfm, mRot, mTrn, mScale, mFlip, mMargin, mScaleOutput;
		MatrixCalculator::GetFlipMatrix(srcMat.cols, srcMat.rows, adjustInfo.bFlip, adjustInfo.bFlip, mFlip);
		MatrixCalculator::GetRotationMatrix(dRad, dRotateX, dRotateY, mRot);
		MatrixCalculator::GetScaleMarix(dScale, dScale, dRotateX, dRotateY, mScale);
		MatrixCalculator::GetTranslationMatrix(dMoveX, dMoveY, mTrn);
		MatrixCalculator::GetMarginMatrix(srcMat.cols, srcMat.rows, rtMargin.x, rtMargin.y, rtMargin.width, rtMargin.height, mMargin);
		MatrixCalculator::GetScaleMarix((float)dstWidth / srcMat.cols, (float)dstHeight / srcMat.rows, mScaleOutput);


		 
		 

		mTfm = mScaleOutput * mMargin * mTrn * mScale * mRot * mFlip;
		MatrixCalculator::ParseAffineMatrix(mTfm, m);

		//if (g_VMdebugMode) {
		//	cv::Mat debugMat;
		//	_dstMat.download(debugMat);
		//	cv::line(debugMat, cv::Point(static_cast<int>(rtMargin.x), static_cast<int>(rtMargin.y)),
		//		cv::Point(static_cast<int>(rtMargin.x + rtMargin.width), static_cast<int>(rtMargin.y + rtMargin.height)),
		//		cv::Scalar(25, 75, 225), 5);
		//	cv::rectangle(debugMat, cv::Point(static_cast<int>(rtMargin.x), static_cast<int>(rtMargin.y)),
		//		cv::Point(static_cast<int>(rtMargin.x + rtMargin.width), static_cast<int>(rtMargin.y + rtMargin.height)),
		//		cv::Scalar(25, 75, 225), 5);
		//	cv::circle(debugMat, cv::Point(static_cast<int>(rtMargin.x), static_cast<int>(rtMargin.y)), 20, cv::Scalar(0, 0, 255), 4);
		//	cv::circle(debugMat, cv::Point(static_cast<int>(rtMargin.x + rtMargin.width), static_cast<int>(rtMargin.y + rtMargin.height)), 20, cv::Scalar(0, 0, 255), 4);
		//	_dstMat.upload(debugMat);
		//}

		//cv::cuda::GpuMat res;
		cv::cuda::GpuMat temp;
		srcMat.copyTo(temp);
		cv::cuda::warpAffine(temp, dstMat, m, cv::Size(dstWidth, dstHeight), cv::INTER_CUBIC, cv::BORDER_CONSTANT);

		return ESMMovieMaker::ERR_CODE_T::SUCCESS;
	}
	catch (std::exception e) 
	{
		DWORD err = ::GetLastError();
		return ESMMovieMaker::ERR_CODE_T::IMAGE_ADJUST_ERROR;
	}
}

int32_t ESMMovieMaker::Core::DoAdjustH(cv::cuda::GpuMat& srcMat, ESMMovieMaker::ADJUST_INFO_T& adjustInfo, const int32_t dstWidth, const int32_t dstHeight, cv::cuda::GpuMat& dstMat)
{
	try
	{
		if (!CheckValidH(adjustInfo.homo))
		{
			//SPd_ERROR("AdjustH Error : {}");
			if (_context && _context->handler)
			{
				_context->handler->onMovieMakerError(ESMMovieMaker::ERR_CODE_T::VMCC_ERROR, "[ESMMovieMaker] Invalid Homograph Data");
			}
			return ESMMovieMaker::ERR_CODE_T::VMCC_ERROR;
		}
		cv::Mat mH = cv::Mat::eye(3, 3, CV_32FC1);

		for (int i = 0; i < 3; i++)
		{
			mH.at<float>(0, i) = adjustInfo.homo.r1[i];
			mH.at<float>(1, i) = adjustInfo.homo.r2[i];
			mH.at<float>(2, i) = adjustInfo.homo.r3[i];
		}

		cv::Mat mFlip, mS, mTfm;
		MatrixCalculator::GetFlipMatrix(srcMat.cols, srcMat.rows, adjustInfo.bFlip, adjustInfo.bFlip, mFlip);
		MatrixCalculator::GetScaleMarix((float)dstWidth / srcMat.cols, (float)dstHeight / srcMat.rows, mS);

		mTfm = mS * mH * mFlip;

		//cv::Mat mAfn;
		//MatrixCalculator::ParseAffineMatrix(mTfm, mAfn);
		//cv::cuda::warpAffine(srcMat, dstMat, mAfn, cv::Size(dstWidth, dstHeight));

		cv::cuda::warpPerspective(srcMat, dstMat, mTfm, cv::Size(dstWidth, dstHeight));

		return ESMMovieMaker::ERR_CODE_T::SUCCESS;
	}
	catch (std::exception& e)
	{
		//SPd_ERROR("AdjustH Error : {}", e.what());
		if (_context && _context->handler)
		{
			char err[MAX_PATH] = { 0 };
			_snprintf_s(err, sizeof(err), "[ESMMovieMaker] AdjustH Error[%s]", e.what());
			_context->handler->onMovieMakerError(ESMMovieMaker::ERR_CODE_T::VMCC_ERROR, err);
		}
		return ESMMovieMaker::ERR_CODE_T::VMCC_ERROR;
	}
}

int32_t ESMMovieMaker::Core::DoVMCC(cv::cuda::GpuMat& srcMat, std::array<int32_t, 2> inCenterPt, const int32_t inZoom, const int32_t inDstWidth, const int32_t inDstHeight, cv::cuda::GpuMat& dstMat)
{
	try 
	{
		cv::cuda::GpuMat cropMat;
		auto dScale = inZoom / 100.;
		auto nLeft = static_cast<int>(inCenterPt[0] * 0.5 - (static_cast<int>(srcMat.cols / dScale) >> 1));
		auto nTop = static_cast<int>(inCenterPt[1] * 0.5 - (static_cast<int>(srcMat.rows / dScale) >> 1));
		auto nWidth = static_cast<int32_t>(srcMat.cols / dScale - 1);
		if ((nLeft + nWidth) > srcMat.cols)
			nLeft = nLeft - ((nLeft + nWidth) - srcMat.cols);
		else if (nLeft < 0)
			nLeft = 0;

		auto nHeight = static_cast<int32_t>(srcMat.rows / dScale - 1);
		if ((nTop + nHeight) > srcMat.rows)
			nTop = nTop - ((nTop + nHeight) - srcMat.rows);
		else if (nTop < 0)
			nTop = 0;

		if (srcMat.cols >= (nLeft + nWidth) && srcMat.rows >= (nTop + nHeight))
			cropMat = srcMat(cv::Rect(nLeft, nTop, nWidth, nHeight)).clone();
		else
			cropMat = srcMat.clone();

		cv::cuda::resize(cropMat, dstMat, cv::Size(inDstWidth, inDstHeight), 0, 0, cv::INTER_CUBIC);
#if defined(WITH_SMOOTH_FILTER)
		_smooth->apply(dstMat, dstMat);
#endif
		return ESMMovieMaker::ERR_CODE_T::SUCCESS;
	}
	catch (std::exception) 
	{
		return ESMMovieMaker::ERR_CODE_T::VMCC_ERROR;
	}
}

int32_t ESMMovieMaker::Core::DebugNumberOnMat(cv::cuda::GpuMat& srcMat, const int32_t idx, const std::string prefix)
{
	cv::Mat tmpSrcMat;
	srcMat.download(tmpSrcMat);
	cv::putText(tmpSrcMat, prefix + ", " + std::to_string(idx), cv::Point(100, 100), cv::QT_FONT_NORMAL, 3.0, cv::Scalar(255, 125, 125), 5);
	srcMat.upload(tmpSrcMat);

	return ESMMovieMaker::ERR_CODE_T::SUCCESS;
}

int32_t ESMMovieMaker::Core::WriteFile(const char* filename, uint8_t* data, int32_t size)
{
	BOOL bWritten = FALSE;
	HANDLE pFile = ::CreateFileA(filename, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (pFile != NULL && pFile != INVALID_HANDLE_VALUE) 
	{

		uint32_t bytes_written = 0;
		for(int32_t index=0; index<10; index++)
		{
			uint32_t nb_write = 0;
			::WriteFile(pFile, data, size, (LPDWORD)&nb_write, 0);
			bytes_written += nb_write;
			if (size == bytes_written)
			{
				bWritten = TRUE;
				break;
			}
			::Sleep(10);
		}

		::CloseHandle(pFile);
		pFile = INVALID_HANDLE_VALUE;
	}
	if(bWritten)
		return ESMMovieMaker::ERR_CODE_T::SUCCESS;
	else
		return ESMMovieMaker::ERR_CODE_T::FILEWRITE_ERROR;
}

long long ESMMovieMaker::Core::MillisecondsNow(void)
{
	if (_fw_use_qpc)
	{
		LARGE_INTEGER now;
		QueryPerformanceCounter(&now);
		return (1000LL * now.QuadPart) / _fw_frequency.QuadPart;
	}
	else
	{
		return GetTickCount();
	}
}