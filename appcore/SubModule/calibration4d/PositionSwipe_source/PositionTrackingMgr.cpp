#include <vector>
#include <mutex>

#include "PositionSwipeStruct.h"
#include "PositionTrackingMgr.h"

#include "opencv2/opencv.hpp"
#include "PositionSwipeStruct.h"
#include "PositionDataParser.h"

using namespace cv;
using namespace std;

struct StPositionDataBody
{
	StPositionDataBody()
	{
		nChannel = 0;

		nX1 = 0;
		nY1 = 0;
		nX2 = 0;
		nY2 = 0;
		nX3 = 0;
		nY3 = 0;
		nX4 = 0;
		nY4 = 0;
	}
	int nChannel;

	int nX1;
	int nY1;
	int nX2;
	int nY2;
	int nX3;
	int nY3;
	int nX4;
	int nY4;

	cv::Mat mHomography;
};

class CPositionTrackingMgr_
{
public:
	CPositionTrackingMgr_();
	~CPositionTrackingMgr_();

private:
	struct TemplatePoint
	{
		int nChannel;
		Point2f centerPointOfRotation;
		Point2f viewPoint;
	};

	CPositionDataParser *m_pPositionDataParser;

	bool m_bReverseMode;

	StPositionDataHead m_stPositionDataHead;
	vector<StPositionDataBody> m_vecStPositionData;
	vector<TemplatePoint> m_vecPositionAxis;

private:
	int SetPositionDataHead(StPositionDataHead stPositionDataHead);
	int SetPositionDataBody(vector<StPositionDataBody> vecStPositionData);

	int SetPositionTrackingInfo(char* szPtaFilePath);

	StPositionDataBody GetPositionTrackingInfoAt(int nChannel);
	int GetDscIndexFromtPositionTrackingInfo(int nChannel);
	void ClearPositionTrackingInfo();
	int GetPositionTrackingInfoSize();

private:
	void ApplyHomographiesFromArrSquares();
	Mat CalcHomographyAt(int nIndex);
	int IsSquareDataValid(int nIndex);
	vector<Point2f> PositionTrackingInfoToVecPoint2f(int nIndex);
	int PositionTrackingInfoToVecPoint2f(int nIndex, vector<Point2f>& vecPoint2f);
	int calcPoint(Mat& homography, vector<Point2f>& currentPoint);
	void SetViewPointsInVecPositionAxis(int nDscIndex);

#ifdef _TAG_FOR_ANDROID
	//wgkim 210204
    int ErrorCheck();
#endif

private:
	//float m_fZoomRatio;
	//int m_nPosX;
	//int m_nPosY;
	float m_nPositionLength;

private:
    //recursive_mutex m_mObjectMutex;
	mutex m_mObjectMutex;

    int MutexLock();
    int MutexUnlock();

public:
	int InitPositionData(int nMetadataLength, unsigned char* szMetadata);

	int SetVecPositionAxis(int nChannel, float fZoomRatio, int nPosX, int nPosY);
	int GetMovedPosX(int nChannel, int& nPosX);
	int GetMovedPosY(int nChannel, int& nPosY);

	int GetPositionLength();
};



CPositionTrackingMgr_::CPositionTrackingMgr_()
{
	m_bReverseMode = 0;

	m_pPositionDataParser = new CPositionDataParser;
}

CPositionTrackingMgr_::~CPositionTrackingMgr_()
{
	delete m_pPositionDataParser;
}

int CPositionTrackingMgr_::InitPositionData(int nMetadataLength, unsigned char* szMetadata)
{
    MutexLock();
	int nRet = 0;
	nRet = m_pPositionDataParser->InitPositionData(nMetadataLength, szMetadata);
	if (nRet != POSITION_SWIPE_OK && nRet != POSITION_SWIPE_UPDATE) {
		MutexUnlock();
		return nRet;
	}

	SetPositionDataHead(m_pPositionDataParser->GetPositionDataHead());
	SetPositionDataBody(m_pPositionDataParser->GetPositionDataBody());
    MutexUnlock();

	return 1;
}

int CPositionTrackingMgr_::SetPositionDataHead(StPositionDataHead stPositionDataHead)
{
	m_stPositionDataHead = stPositionDataHead;

#ifdef _TAG_FOR_ANDROID
	__android_log_print(ANDROID_LOG_DEBUG, LOG_POSITION_SWIPE_TAG, "[CallCount %d]CPositionTrackingMgr_::InitPositionData : SetPositionDataHead",
						m_pPositionDataParser->m_nInitPositionCallCount);
#endif

	return 1;
}

int CPositionTrackingMgr_::SetPositionDataBody(vector<StPositionDataBody> vecStPositionData)
{
	m_vecStPositionData = vecStPositionData;

	ApplyHomographiesFromArrSquares();

#ifdef _TAG_FOR_ANDROID
	__android_log_print(ANDROID_LOG_DEBUG, LOG_POSITION_SWIPE_TAG, "[CallCount %d]CPositionTrackingMgr_::InitPositionData : SetPositionDataBody",
						m_pPositionDataParser->m_nInitPositionCallCount);
#endif

	return 1;
}

void CPositionTrackingMgr_::ClearPositionTrackingInfo()
{
	m_vecStPositionData.clear();
}

StPositionDataBody CPositionTrackingMgr_::GetPositionTrackingInfoAt(int nChannel)
{
	for(int i = 0; i < m_vecStPositionData.size(); i++)
	{
		if(m_vecStPositionData[i].nChannel == nChannel)
		{
			return m_vecStPositionData[i];
		}
	}

	return StPositionDataBody();
}

int CPositionTrackingMgr_::GetDscIndexFromtPositionTrackingInfo(int nChannel)
{
	for(int i = 0; i < m_vecStPositionData.size(); i++)
	{
		if(m_vecStPositionData[i].nChannel == nChannel)
		{
			return i;
		}
	}

	return POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL;
}

int CPositionTrackingMgr_::GetPositionTrackingInfoSize()
{
	return m_vecStPositionData.size();
}

void CPositionTrackingMgr_::ApplyHomographiesFromArrSquares()
{
	//for(int i = 0; i < GetPositionTrackingInfoSize(); i++)
	//{
	//if(m_bReverseMode)
	//{
	//	setSquareForReverseMode(squares[i]);
	//}
	//pushArrSquares(squares.at(i));
	//}
	if(GetPositionTrackingInfoSize() > 1)
	{
		for(int i = 0; i <= GetPositionTrackingInfoSize(); i++)
		{
			if(i != GetPositionTrackingInfoSize())
				m_vecStPositionData[i].mHomography = CalcHomographyAt(i);
			else
				m_vecStPositionData[0].mHomography = CalcHomographyAt(i);
		}
	}
#ifdef _TAG_FOR_ANDROID
    __android_log_print(ANDROID_LOG_DEBUG, LOG_POSITION_SWIPE_TAG, "[CallCount %d]CPositionTrackingMgr_::ApplyHomographiesFromArrSquares : Get Homography",
    		m_pPositionDataParser->m_nInitPositionCallCount);
#endif
}

Mat CPositionTrackingMgr_::CalcHomographyAt(int nIndex)
{
	int nNullCheck = 1;
	while(1)
	{
		int nCircularArrayIndex = nIndex - nNullCheck ;

		if(nCircularArrayIndex < 0)
			nCircularArrayIndex += GetPositionTrackingInfoSize();

		if(!IsSquareDataValid(nCircularArrayIndex))
		{
			nNullCheck++;
		}
		else
		{	
			nIndex %= GetPositionTrackingInfoSize();

			vector<Point2f> vecSquaresCircularArrayIndex;
			PositionTrackingInfoToVecPoint2f(nCircularArrayIndex, vecSquaresCircularArrayIndex);

			vector<Point2f> vecSquaresIndex;
			PositionTrackingInfoToVecPoint2f(nIndex, vecSquaresIndex);

			Mat mResult = findHomography(vecSquaresCircularArrayIndex, vecSquaresIndex);//RANSAC, 0.001);

			if(mResult.empty())
			{
#ifdef _TAG_FOR_ANDROID
                ErrorCheck();

                __android_log_print(ANDROID_LOG_ERROR, LOG_POSITION_SWIPE_TAG,
                                    "[CallCount %d]CPositionTrackingMgr_::CalcHomographyAt : Homography is NULL(Index : %d)",
                                    m_pPositionDataParser->m_nInitPositionCallCount, nIndex);
                for(int i = 0; i < vecSquaresIndex.size(); i++) {
                    __android_log_print(ANDROID_LOG_ERROR, LOG_POSITION_SWIPE_TAG,
                                        "[CallCount %d]CPositionTrackingMgr_::CalcHomographyAt : vecSquaresIndex[%d](%f, %f)",
                                        m_pPositionDataParser->m_nInitPositionCallCount, vecSquaresIndex[i].x, vecSquaresIndex[i].y);
                }
                for(int i = 0; i < vecSquaresCircularArrayIndex.size(); i++) {
                    __android_log_print(ANDROID_LOG_ERROR, LOG_POSITION_SWIPE_TAG,
                                        "[CallCount %d]CPositionTrackingMgr_::CalcHomographyAt : vecSquaresCircularArrayIndex[%d](%f, %f)",
                                        m_pPositionDataParser->m_nInitPositionCallCount, i, vecSquaresCircularArrayIndex[i].x, vecSquaresCircularArrayIndex[i].y);
                }
#endif
            }

			return  mResult;
		}
	}
}

int CPositionTrackingMgr_::IsSquareDataValid(int nIndex)
{
	if (GetPositionTrackingInfoSize() < 1)
	{
#ifdef _TAG_FOR_ANDROID
		ErrorCheck();
#endif
		return POSITION_SWIPE_ERROR_DATA_EMPTY;
	}

	if (GetPositionTrackingInfoSize() <= nIndex)
	{
#ifdef _TAG_FOR_ANDROID
		ErrorCheck();
#endif
		return POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL;
	}

	if ((m_vecStPositionData[nIndex].nX1 == 0 && m_vecStPositionData[nIndex].nY1 == 0) && 
		(m_vecStPositionData[nIndex].nX2 == 0 && m_vecStPositionData[nIndex].nY2 == 0) && 
		(m_vecStPositionData[nIndex].nX3 == 0 && m_vecStPositionData[nIndex].nY3== 0) && 
		(m_vecStPositionData[nIndex].nX4 == 0 && m_vecStPositionData[nIndex].nY4 == 0))
	{
#ifdef _TAG_FOR_ANDROID
		ErrorCheck();
#endif
		return POSITION_SWIPE_ERROR_INVALID_BODY;
	}
	else
		return POSITION_SWIPE_OK;
}

int CPositionTrackingMgr_::PositionTrackingInfoToVecPoint2f(int nIndex, vector<Point2f>& vecPoint2f)
{
	int nRet = IsSquareDataValid(nIndex);
	if (nRet == POSITION_SWIPE_ERROR_DATA_EMPTY ||
		nRet == POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL)
	{
		return nRet;
	}

	vecPoint2f.push_back( Point2f(
		m_vecStPositionData[nIndex].nX1, 
		m_vecStPositionData[nIndex].nY1));
	vecPoint2f.push_back( Point2f(
		m_vecStPositionData[nIndex].nX2, 
		m_vecStPositionData[nIndex].nY2));
	vecPoint2f.push_back( Point2f(
		m_vecStPositionData[nIndex].nX3, 
		m_vecStPositionData[nIndex].nY3));
	vecPoint2f.push_back( Point2f(
		m_vecStPositionData[nIndex].nX4, 
		m_vecStPositionData[nIndex].nY4));

	return nRet;
}

int CPositionTrackingMgr_::SetVecPositionAxis(int nChannel, float fZoomRatio, int nPosX, int nPosY)
{
    MutexLock();
	int nRet = POSITION_SWIPE_OK;
	m_nPositionLength = m_stPositionDataHead.nPositionLength;

	//Point2f ptTestPoint= Point2f(m_nPosX, m_nPosY + m_stPositionDataHead.nPositionLength/**fZoomRatio*/);
	Point2f ptTestPoint= Point2f(nPosX, nPosY + m_nPositionLength/**fZoomRatio*/);

	if(m_vecStPositionData.empty())
	{
#ifdef _TAG_FOR_ANDROID
        ErrorCheck();
#endif
		MutexUnlock();
		return POSITION_SWIPE_ERROR_DATA_EMPTY;
	}
#ifdef _TAG_FOR_ANDROID
	__android_log_print(ANDROID_LOG_DEBUG, LOG_POSITION_SWIPE_TAG, "[CallCount %d]CPositionTrackingMgr_::SetVecPositionAxis : m_vecStPositionData Empty Check",
			m_pPositionDataParser->m_nInitPositionCallCount);
#endif

	TemplatePoint templetePoint;
	int nDscIndex = GetDscIndexFromtPositionTrackingInfo(nChannel);
	if (nDscIndex == POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL)
	{
#ifdef _TAG_FOR_ANDROID
        ErrorCheck();
#endif
		MutexUnlock();
		return POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL;
	}

	templetePoint.nChannel = m_vecStPositionData[nDscIndex].nChannel;
	templetePoint.centerPointOfRotation = ptTestPoint;

	m_vecPositionAxis.resize(GetPositionTrackingInfoSize());
	m_vecPositionAxis[nDscIndex] = templetePoint;

#ifdef _TAG_FOR_ANDROID
	__android_log_print(ANDROID_LOG_DEBUG, LOG_POSITION_SWIPE_TAG, "[CallCount %d]CPositionTrackingMgr_::SetVecPositionAxis : m_vecPositionAxis Check (Data Size : %d, Axis Size : %d)",
			m_pPositionDataParser->m_nInitPositionCallCount, m_vecStPositionData.size(), m_vecPositionAxis.size());
#endif

	for(int i = nDscIndex+1; i != nDscIndex; i++)
	{
		i %= GetPositionTrackingInfoSize();
		if(IsSquareDataValid(i))
		{
			vector<Point2f> vectorPoint;
			vectorPoint.push_back(templetePoint.centerPointOfRotation);
			if(m_vecPositionAxis.size() > 1)
			{
				nRet = calcPoint(m_vecStPositionData[i].mHomography, vectorPoint);
			}
			else
            {
#ifdef _TAG_FOR_ANDROID
                __android_log_print(ANDROID_LOG_ERROR, LOG_POSITION_SWIPE_TAG, "[CallCount %d]CPositionTrackingMgr_::SetVecPositionAxis m_vecPositionAxis.size() <= 1 --> (Data Size : %d, Axis Size : %d)",
                                    m_pPositionDataParser->m_nInitPositionCallCount, m_vecStPositionData.size(), m_vecPositionAxis.size());
#endif
            }

			templetePoint.nChannel = m_vecStPositionData[i].nChannel;
			templetePoint.centerPointOfRotation = vectorPoint[0];

			m_vecPositionAxis[i] = templetePoint;
			templetePoint = m_vecPositionAxis[i];
		}
		else
		{
			m_vecPositionAxis[i] = templetePoint;
			templetePoint.nChannel = m_vecStPositionData[i].nChannel;
		}

		if(nDscIndex == 0 && i == GetPositionTrackingInfoSize()-1)
			i = -1;
	}

#ifdef _TAG_FOR_ANDROID
	__android_log_print(ANDROID_LOG_DEBUG, LOG_POSITION_SWIPE_TAG, "[CallCount %d]CPositionTrackingMgr_::SetVecPositionAxis : Complete Set CenterPoint(%d)",
			m_pPositionDataParser->m_nInitPositionCallCount, m_vecPositionAxis.size());
#endif

	SetViewPointsInVecPositionAxis(nDscIndex);
#ifdef _TAG_FOR_ANDROID
	__android_log_print(ANDROID_LOG_DEBUG, LOG_POSITION_SWIPE_TAG, "[CallCount %d]CPositionTrackingMgr_::SetVecPositionAxis : Complete Set ViewPoint(%d)",
			m_pPositionDataParser->m_nInitPositionCallCount, m_vecPositionAxis.size());
#endif
    MutexUnlock();
	return nRet;
}

void CPositionTrackingMgr_::SetViewPointsInVecPositionAxis(int nDscIndex)
{
	//for(int i = 0; i < GetPositionTrackingInfoSize(); i++)
	for(int i = 0; i < m_vecPositionAxis.size(); i++)
	{
		if (IsSquareDataValid(i))
		{
			m_vecPositionAxis[i].viewPoint = Point2f(
			m_vecPositionAxis[i].centerPointOfRotation.x,
			m_vecPositionAxis[i].centerPointOfRotation.y - m_stPositionDataHead.nPositionLength);
		}
	}
}

int CPositionTrackingMgr_::calcPoint(Mat& homography, vector<Point2f>& currentPoint)
{
	if(homography.empty())
	{
#ifdef _TAG_FOR_ANDROID
	    ErrorCheck();
#endif
        return POSITION_SWIPE_ERROR_HOMOGRAPHY_EMPTY;
    }

	if(homography.cols != 3 || homography.rows != 3)
	{
#ifdef _TAG_FOR_ANDROID
        ErrorCheck();
#endif
        return POSITION_SWIPE_ERROR_INVALID_HOMOGRAPHY;
    }

	for(int i = 0; i < currentPoint.size(); i++)
	{
		double tempMat_[3] = {(double)currentPoint[i].x, (double)currentPoint[i].y, 1.};
		Mat tempMat(3, 1, CV_64F, tempMat_);
		Mat resultMat = homography * tempMat;

		if(resultMat.at<double>(2) == 0 || resultMat.at<double>(2) == 0)
        {
#ifdef _TAG_FOR_ANDROID
            __android_log_print(ANDROID_LOG_DEBUG, LOG_POSITION_SWIPE_TAG, "[CallCount %d]CPositionTrackingMgr_::calcPoint : Divide Zero..",
            m_pPositionDataParser->m_nInitPositionCallCount);
            ErrorCheck();
#endif
        }

		currentPoint[i] = Point2f(resultMat.at<double>(0)/resultMat.at<double>(2),
			resultMat.at<double>(1)/resultMat.at<double>(2));
	}

	return POSITION_SWIPE_OK;
}

int CPositionTrackingMgr_::GetMovedPosX(int nChannel, int& nPosX)
{
    MutexLock();
	if(GetPositionTrackingInfoSize() != m_vecPositionAxis.size())
	{
#ifdef _TAG_FOR_ANDROID
		ErrorCheck();
#endif
		MutexUnlock();
		return POSITION_SWIPE_ERROR_INCONSISTENTY_BODYDATASIZE_AXISSIZE;
	}
	for(int i = 0; i < m_vecPositionAxis.size(); i++)
	//for(int i = 0; i < GetPositionTrackingInfoSize(); i++)
	{
		if(m_vecPositionAxis[i].nChannel == nChannel)
		{
			nPosX = m_vecPositionAxis[i].viewPoint.x;
			MutexUnlock();
			return POSITION_SWIPE_OK;
		}
	}

#ifdef _TAG_FOR_ANDROID
	__android_log_print(ANDROID_LOG_DEBUG, LOG_POSITION_SWIPE_TAG, "[CallCount %d]CPositionTrackingMgr_::GetMovedPosX",
						m_pPositionDataParser->m_nInitPositionCallCount);
#endif

    MutexUnlock();

	return POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL;
}

int CPositionTrackingMgr_::GetMovedPosY(int nChannel, int& nPosY)
{
    MutexLock();
	if(GetPositionTrackingInfoSize() != m_vecPositionAxis.size())
	{
#ifdef _TAG_FOR_ANDROID
		ErrorCheck();
#endif
		MutexUnlock();
		return POSITION_SWIPE_ERROR_INCONSISTENTY_BODYDATASIZE_AXISSIZE;
	}

	for(int i = 0; i < m_vecPositionAxis.size(); i++)
	//for(int i = 0; i < GetPositionTrackingInfoSize(); i++)
	{
		if(m_vecPositionAxis[i].nChannel == nChannel)
		{
			nPosY = m_vecPositionAxis[i].viewPoint.y;
			MutexUnlock();
			return POSITION_SWIPE_OK;
		}
	}

#ifdef _TAG_FOR_ANDROID
	__android_log_print(ANDROID_LOG_DEBUG, LOG_POSITION_SWIPE_TAG, "[CallCount %d]CPositionTrackingMgr_::GetMovedPosY",
						m_pPositionDataParser->m_nInitPositionCallCount);
#endif

    MutexUnlock();

	return POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL;
}

int CPositionTrackingMgr_::GetPositionLength()
{
#ifdef _TAG_FOR_ANDROID
	__android_log_print(ANDROID_LOG_DEBUG, LOG_POSITION_SWIPE_TAG, "[CallCount %d]CPositionTrackingMgr_::GetPositionLength",
						m_pPositionDataParser->m_nInitPositionCallCount);
#endif

	return m_nPositionLength;
}

int CPositionTrackingMgr_::MutexLock()
{
    m_mObjectMutex.lock();
    return 1;
}

int CPositionTrackingMgr_::MutexUnlock()
{
    m_mObjectMutex.unlock();
    return 1;
}

#ifdef _TAG_FOR_ANDROID
int CPositionTrackingMgr_::ErrorCheck() {
    m_pPositionDataParser->ErrorCheck();
    __android_log_print(ANDROID_LOG_DEBUG, LOG_POSITION_SWIPE_TAG,
                        "[CallCount %d]Position Swipe Data Head : %d, %d, %d, %d)",
                        m_pPositionDataParser->m_nInitPositionCallCount,
                        m_stPositionDataHead.nWidth, m_stPositionDataHead.nHeight,
                        m_stPositionDataHead.nPositionLength,
                        m_stPositionDataHead.nPositionDataCount);
    __android_log_print(ANDROID_LOG_DEBUG, LOG_POSITION_SWIPE_TAG,
                        "[CallCount %d]Position Swipe Data Head : %d, %d, %d, %d)",
                        m_pPositionDataParser->m_nInitPositionCallCount,
                        m_stPositionDataHead.nWidth, m_stPositionDataHead.nHeight,
                        m_stPositionDataHead.nPositionLength,
                        m_stPositionDataHead.nPositionDataCount);

    for (int i = 0; i < m_vecStPositionData.size(); i++) {
        __android_log_print(ANDROID_LOG_DEBUG, LOG_POSITION_SWIPE_TAG,
                            "[CallCount %d][Channel:%d]Position Swipe Data Body : %d %d, %d %d, %d %d, %d %d / Homography size (%d, %d)",
                            m_pPositionDataParser->m_nInitPositionCallCount,
                            m_vecStPositionData[i].nChannel,
                            m_vecStPositionData[i].nX1, m_vecStPositionData[i].nY1,
                            m_vecStPositionData[i].nX2, m_vecStPositionData[i].nY2,
                            m_vecStPositionData[i].nX3, m_vecStPositionData[i].nY3,
                            m_vecStPositionData[i].nX4, m_vecStPositionData[i].nY4,
                            m_vecStPositionData[i].mHomography.cols,
                            m_vecStPositionData[i].mHomography.rows);
    }

    for (int i = 0; i < m_vecPositionAxis.size(); i++)
    {
        __android_log_print(ANDROID_LOG_DEBUG, LOG_POSITION_SWIPE_TAG, "[CallCount %d][Channel:%d]Set Current Axis : centerPos(%d, %d), viewPos(%d, %d)",
                            m_pPositionDataParser->m_nInitPositionCallCount,
                            m_vecPositionAxis[i].nChannel,
                            m_vecPositionAxis[i].centerPointOfRotation.x, m_vecPositionAxis[i].centerPointOfRotation.y,
                            m_vecPositionAxis[i].viewPoint.x, m_vecPositionAxis[i].viewPoint.y);
    }

	__android_log_print(ANDROID_LOG_DEBUG, LOG_POSITION_SWIPE_TAG, "[CallCount %d]PositionDataSize(%d) / PositionAxisSize(%d)",
                        m_pPositionDataParser->m_nInitPositionCallCount, GetPositionTrackingInfoSize(), m_vecPositionAxis.size());

    return 1;
}
#endif

CPositionTrackingMgr::CPositionTrackingMgr()
{
	m_pPositionTrackingMgr = new CPositionTrackingMgr_;
}

CPositionTrackingMgr::~CPositionTrackingMgr()
{
	delete m_pPositionTrackingMgr;
}

int CPositionTrackingMgr::InitPositionData(int nMetadataLength, unsigned char* szMetadata)
{
	return m_pPositionTrackingMgr->InitPositionData(nMetadataLength, szMetadata);
}

int CPositionTrackingMgr::SetVecPositionAxis(int nChannel, float fZoomRatio, int nPosX, int nPosY)
{
	return m_pPositionTrackingMgr->SetVecPositionAxis(nChannel, fZoomRatio, nPosX, nPosY);
}

int CPositionTrackingMgr::GetMovedPosX(int nChannel, int& nPosX)
{
	return m_pPositionTrackingMgr->GetMovedPosX(nChannel, nPosX);
}

int CPositionTrackingMgr::GetMovedPosY(int nChannel, int& nPosY)
{
	return m_pPositionTrackingMgr->GetMovedPosY(nChannel, nPosY);
}

int CPositionTrackingMgr::GetPositionLength()
{
	return m_pPositionTrackingMgr->GetPositionLength();
}

