#pragma once

#include <iostream>
#include <vector>
using namespace std;

struct StPositionDataBody;
class CPositionDataParser
{
public:
	CPositionDataParser(void);
	~CPositionDataParser(void);

public:
	int InitPositionData(int nMetadataLength, unsigned char* szMetadata);

private:
	const static int m_nPositionDataHeadSize = 4;
	const static int m_nPositionDataBodySize = 9;

	StPositionDataHead m_stPositionDataHead;
	vector<StPositionDataBody> m_vecStPositionData;

private:
	int RemoveEmulationPreventionByte(unsigned char* szMetadata, int &nLength);
	int DataParsingCharToShort(unsigned char* szMetadata, int nMetadataLength);
	int DataHeadParsing();
	int DataBodyParsing();

	unsigned char* m_szMetadata;
	int m_nMetadataSize;
	vector<int> m_vecNPositionData;

public:
	int SetPositionDataHeader(
		int nWidth,
		int nHeight,
		int nPositionLength,
		int nPositionDataCount);

	int AddPositionDataBody(int nChannel,
		int nX1, int nY1,
		int nX2, int nY2,
		int nX3, int nY3,
		int nX4, int nY4);
	int SetPositionDataBody(int nChannel,
		int nX1, int nY1,
		int nX2, int nY2,
		int nX3, int nY3,
		int nX4, int nY4);
	int IsExistPositionDataBodyAs(int nChannel);

	StPositionDataHead GetPositionDataHead(){return m_stPositionDataHead;};
	vector<StPositionDataBody> GetPositionDataBody(){return m_vecStPositionData;};

public:
	int GetPositionDataWidth(int& nWidth);
	int GetPositionDataHeight(int& nWidth);
	int GetPositionDataPositionLength(int& nWidth);
	int GetPositionDataPositionDataCount(int& nWidth);

	int GetPositionDataBodyX1As(int nChannel, int& nValue);
	int GetPositionDataBodyY1As(int nChannel, int& nValue);
	int GetPositionDataBodyX2As(int nChannel, int& nValue);
	int GetPositionDataBodyY2As(int nChannel, int& nValue);
	int GetPositionDataBodyX3As(int nChannel, int& nValue);
	int GetPositionDataBodyY3As(int nChannel, int& nValue);
	int GetPositionDataBodyX4As(int nChannel, int& nValue);
	int GetPositionDataBodyY4As(int nChannel, int& nValue);


#ifdef _TAG_FOR_ANDROID
    int ErrorCheck();
    int m_nInitPositionCallCount;
#endif
};

