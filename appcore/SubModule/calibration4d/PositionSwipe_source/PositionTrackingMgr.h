#pragma once

class CPositionTrackingMgr_;
class CPositionTrackingMgr
{
public:
	CPositionTrackingMgr();
	~CPositionTrackingMgr();

public:
	int InitPositionData(int nMetadataLength, unsigned char* szMetadata);
	int SetVecPositionAxis(int nChannel, float fZoomRatio, int nPosX, int nPosY);
	int GetMovedPosX(int nChannel, int& nPosX);
	int GetMovedPosY(int nChannel, int& nPosY);
	int GetPositionLength();

private:
	CPositionTrackingMgr_* m_pPositionTrackingMgr;
};

