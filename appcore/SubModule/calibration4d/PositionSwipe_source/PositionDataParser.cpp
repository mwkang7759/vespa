#include "PositionSwipeStruct.h"
#include "PositionDataParser.h"

#define _TAG_FOR_MOBILE
#ifndef _TAG_FOR_MOBILE
#include <winsock.h>
#pragma comment(lib,"ws2_32.lib")
#else
#include <arpa/inet.h>
#endif

#include "opencv2/opencv.hpp"


struct StPositionDataBody
{
	StPositionDataBody()
	{
		nChannel = 0;

		nX1 = 0;
		nY1 = 0;
		nX2 = 0;
		nY2 = 0;
		nX3 = 0;
		nY3 = 0;
		nX4 = 0;
		nY4 = 0;
	}
	int nChannel;

	int nX1;
	int nY1;
	int nX2;
	int nY2;
	int nX3;
	int nY3;
	int nX4;
	int nY4;

	cv::Mat mHomography;
};


CPositionDataParser::CPositionDataParser(void)
{
#ifdef _TAG_FOR_ANDROID
    int m_nInitPositionCallCount = 0;
#endif

	m_szMetadata = nullptr;
	m_nMetadataSize = 0;
}

CPositionDataParser::~CPositionDataParser(void)
{
	if(m_szMetadata != nullptr)
		delete m_szMetadata;
}

int CPositionDataParser::InitPositionData(int nMetadataLength, unsigned char* szMetadata)
{
	if(szMetadata == NULL) {
        return POSITION_SWIPE_ERROR_INVALID_METADATA;
    }
#ifdef _TAG_FOR_ANDROID
    m_nInitPositionCallCount++;
    __android_log_print(ANDROID_LOG_DEBUG, LOG_POSITION_SWIPE_TAG, "[CallCount %d]CPositionDataParser::InitPositionData : Metadata Check", m_nInitPositionCallCount);
#endif

	if(m_szMetadata != nullptr)
	{
	    int nTest = memcmp(m_szMetadata, szMetadata, nMetadataLength);
		if(memcmp(m_szMetadata, szMetadata, nMetadataLength) == 0)
			return POSITION_SWIPE_OK;

		delete m_szMetadata;
		m_szMetadata = nullptr;
	}

#ifdef _TAG_FOR_ANDROID
    __android_log_print(ANDROID_LOG_DEBUG, LOG_POSITION_SWIPE_TAG, "[CallCount %d]CPositionDataParser::InitPositionData : memcmp check", m_nInitPositionCallCount);
#endif

	//0x3c
	//unsigned char* pch = (unsigned char*)memchr(szMetadata, 0x00, nMetadataLength);
	//int nLength = sizeof(unsigned char) * nMetadataLength;
	//while(nLength > 3)
	//{
	//	unsigned char* pch1 = (unsigned char*)memchr(pch, 0x00, nLength);
	//	int nIndex = pch1 - pch;

	//	if(pch1 != NULL)
	//	{
	//		nLength = nLength - nIndex;
	//		if(pch1[0] == 0x00 && pch1[1] == 0x00 && pch1[2] == 0x03)
	//		{
	//			memmove(&pch1[2], &pch1[3], nLength-3);
	//			nLength--;
	//			nMetadataLength--;
	//		}
	//	}
	//	else
	//	{
	//		break;
	//	}
	//	nLength--;
	//	pch = &pch1[1];
	//}

	m_szMetadata = (unsigned char*)malloc(sizeof(unsigned char) * nMetadataLength);
	if (m_szMetadata == NULL)
		return POSITION_SWIPE_ERROR_MEMORY_ALLOC_FAIL;
#ifdef _TAG_FOR_ANDROID
    __android_log_print(ANDROID_LOG_DEBUG, LOG_POSITION_SWIPE_TAG, "[CallCount %d]CPositionDataParser::InitPositionData : malloc", m_nInitPositionCallCount);
#endif

	memcpy(m_szMetadata, szMetadata, nMetadataLength);

#ifdef _TAG_FOR_ANDROID
    __android_log_print(ANDROID_LOG_DEBUG, LOG_POSITION_SWIPE_TAG, "[CallCount %d]CPositionDataParser::InitPositionData : memcpy", m_nInitPositionCallCount);
#endif

	int nChangedMetadataLength = nMetadataLength;
	RemoveEmulationPreventionByte(m_szMetadata, nChangedMetadataLength );
	m_nMetadataSize = nChangedMetadataLength;

#ifdef _TAG_FOR_ANDROID
	//ErrorCheck();
    __android_log_print(ANDROID_LOG_DEBUG, LOG_POSITION_SWIPE_TAG, "[CallCount %d]CPositionDataParser::InitPositionData : Emulation prevention byte", m_nInitPositionCallCount);
#endif

	int nRet = DataParsingCharToShort(m_szMetadata, nChangedMetadataLength );
	if(nRet != POSITION_SWIPE_OK)
		return nRet;

#ifdef _TAG_FOR_ANDROID
    __android_log_print(ANDROID_LOG_DEBUG, LOG_POSITION_SWIPE_TAG, "[CallCount %d]CPositionDataParser::InitPositionData : Char to short", m_nInitPositionCallCount);
#endif

	nRet = DataHeadParsing();
	if(nRet != POSITION_SWIPE_OK)
		return nRet;

#ifdef _TAG_FOR_ANDROID
    __android_log_print(ANDROID_LOG_DEBUG, LOG_POSITION_SWIPE_TAG, "[CallCount %d]CPositionDataParser::InitPositionData : Head parsing", m_nInitPositionCallCount);
#endif

	nRet = DataBodyParsing();
	if(nRet != POSITION_SWIPE_OK)
		return nRet;

#ifdef _TAG_FOR_ANDROID
    __android_log_print(ANDROID_LOG_DEBUG, LOG_POSITION_SWIPE_TAG, "[CallCount %d]CPositionDataParser::InitPositionData : Body parsing", m_nInitPositionCallCount);
#endif

	return POSITION_SWIPE_UPDATE;
}

int CPositionDataParser::RemoveEmulationPreventionByte(unsigned char* szMetadata, int &nMetadataLength)
{
	//0x3c
	unsigned char* pszMovedByte = (unsigned char*)memchr(szMetadata, 0x00, nMetadataLength);
	int nIndex = pszMovedByte - szMetadata;
	int nLength = sizeof(unsigned char) * nMetadataLength - nIndex;
	while(nLength > 3)
	{
		unsigned char* pszEmulationPreventionByte = (unsigned char*)memchr(pszMovedByte, 0x00, nLength);
		nIndex = pszEmulationPreventionByte - pszMovedByte;

		if(pszEmulationPreventionByte != NULL)
		{
			nLength = nLength - nIndex;
			if (pszEmulationPreventionByte[0] == 0x00 && 
				pszEmulationPreventionByte[1] == 0x00 && 
				pszEmulationPreventionByte[2] == 0x03)
			{
				memmove(&pszEmulationPreventionByte[2], &pszEmulationPreventionByte[3], nLength-3);
				nLength--;
				nMetadataLength--;
			}
		}
		else
		{
			break;
		}
		nLength--;
		pszMovedByte = &pszEmulationPreventionByte[1];
	}

	return POSITION_SWIPE_OK;
}

int CPositionDataParser::DataParsingCharToShort(unsigned char* szMetadata, int nLength)
{
	m_vecNPositionData.clear();
	short int* m_arrNBuffer = (short int*)szMetadata;
	for(int i = 0; i < nLength/sizeof(short int); i++)
	{
		short int nValue = htons(m_arrNBuffer[i]);
		m_vecNPositionData.push_back(static_cast<int>(nValue));
	}

	if(m_vecNPositionData.size() < m_nPositionDataHeadSize)
		return POSITION_SWIPE_ERROR_INVALID_HEADER;

	if(m_vecNPositionData.size() != m_nPositionDataHeadSize + m_nPositionDataBodySize*m_vecNPositionData.at(3))
		return POSITION_SWIPE_ERROR_INVALID_BODY;

	return POSITION_SWIPE_OK;
}

int CPositionDataParser::DataHeadParsing()
{
	if(m_vecNPositionData.size() < m_nPositionDataHeadSize)
		return POSITION_SWIPE_ERROR_INVALID_HEADER;

	int nWidth = m_vecNPositionData.at(0);
	int nHeight = m_vecNPositionData.at(1);
	int nPositionLength = m_vecNPositionData.at(2);
	int nPositionDataCount = m_vecNPositionData.at(3);

	SetPositionDataHeader(nWidth, nHeight, nPositionLength, nPositionDataCount);

	return POSITION_SWIPE_OK;
}

int CPositionDataParser::DataBodyParsing()
{
	if (m_vecNPositionData.size() < m_nPositionDataHeadSize + m_nPositionDataBodySize || 
		m_vecNPositionData.size() % m_nPositionDataBodySize != m_nPositionDataHeadSize)
		return POSITION_SWIPE_ERROR_INVALID_BODY;

	if (!m_vecStPositionData.empty())
		m_vecStPositionData.clear();

	for(int i = 0; i < m_stPositionDataHead.nPositionDataCount; i++)
	{
		AddPositionDataBody(
			m_vecNPositionData.at(m_nPositionDataHeadSize + (i*m_nPositionDataBodySize + 0)), 
			m_vecNPositionData.at(m_nPositionDataHeadSize + (i*m_nPositionDataBodySize + 1)),
			m_vecNPositionData.at(m_nPositionDataHeadSize + (i*m_nPositionDataBodySize + 2)),
			m_vecNPositionData.at(m_nPositionDataHeadSize + (i*m_nPositionDataBodySize + 3)),
			m_vecNPositionData.at(m_nPositionDataHeadSize + (i*m_nPositionDataBodySize + 4)),
			m_vecNPositionData.at(m_nPositionDataHeadSize + (i*m_nPositionDataBodySize + 5)),
			m_vecNPositionData.at(m_nPositionDataHeadSize + (i*m_nPositionDataBodySize + 6)),
			m_vecNPositionData.at(m_nPositionDataHeadSize + (i*m_nPositionDataBodySize + 7)),
			m_vecNPositionData.at(m_nPositionDataHeadSize + (i*m_nPositionDataBodySize + 8))
			);
	}

	return POSITION_SWIPE_OK;
}

int CPositionDataParser::SetPositionDataHeader(
	int nWidth,
	int nHeight,
	int nPositionLength,
	int nPositionDataCount)
{
	StPositionDataHead stPositionDataHead;

	stPositionDataHead.nWidth = nWidth/2;
	stPositionDataHead.nHeight = nHeight/2;
	stPositionDataHead.nPositionLength = nPositionLength/2;
	stPositionDataHead.nPositionDataCount = nPositionDataCount;

	//Check Data

	m_stPositionDataHead = stPositionDataHead;

	return POSITION_SWIPE_OK;
}

int CPositionDataParser::AddPositionDataBody(int nChannel,
	int nX1, int nY1,
	int nX2, int nY2,
	int nX3, int nY3,
	int nX4, int nY4)
{
	if(IsExistPositionDataBodyAs(nChannel) != POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL)
	{
		SetPositionDataBody(nChannel, nX1, nY1, nX2, nY2, nX3, nY3, nX4, nY4);
		return POSITION_SWIPE_ERROR_CHANNEL_ALREADY_EXIST; 
	}

	StPositionDataBody stPositionDataBody;

	stPositionDataBody.nChannel = nChannel;

	stPositionDataBody.nX1 = nX1/2;
	stPositionDataBody.nY1 = nY1/2;
	stPositionDataBody.nX2 = nX2/2;
	stPositionDataBody.nY2 = nY2/2;
	stPositionDataBody.nX3 = nX3/2;
	stPositionDataBody.nY3 = nY3/2;
	stPositionDataBody.nX4 = nX4/2;
	stPositionDataBody.nY4 = nY4/2;

	m_vecStPositionData.push_back(stPositionDataBody);

	return POSITION_SWIPE_OK;
}

int CPositionDataParser::SetPositionDataBody(int nChannel,
	int nX1, int nY1,
	int nX2, int nY2,
	int nX3, int nY3,
	int nX4, int nY4)
{
	int nIndex = IsExistPositionDataBodyAs(nChannel);
	if (nIndex == POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL)
		return POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL;

	StPositionDataBody stPositionDataBody;

	stPositionDataBody.nChannel = nChannel;

	stPositionDataBody.nX1 = nX1/2;
	stPositionDataBody.nY1 = nY1/2;
	stPositionDataBody.nX2 = nX2/2;
	stPositionDataBody.nY2 = nY2/2;
	stPositionDataBody.nX3 = nX3/2;
	stPositionDataBody.nY3 = nY3/2;
	stPositionDataBody.nX4 = nX4/2;
	stPositionDataBody.nY4 = nY4/2;

	m_vecStPositionData[nIndex] = stPositionDataBody;

	return POSITION_SWIPE_OK;
}

int CPositionDataParser::IsExistPositionDataBodyAs(int nChannel)
{
	for(int i = 0; i < m_vecStPositionData.size(); i++)
	{
		if(m_vecStPositionData[i].nChannel == nChannel)
			return i;
	}

	return POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL;
}

int CPositionDataParser::GetPositionDataWidth(int& nWidth)
{
	nWidth = m_stPositionDataHead.nWidth;
	return POSITION_SWIPE_OK;
}

int CPositionDataParser::GetPositionDataHeight(int& nHeight)
{
	nHeight = m_stPositionDataHead.nHeight;
	return POSITION_SWIPE_OK;
}

int CPositionDataParser::GetPositionDataPositionLength(int& nPositionLength)
{
	nPositionLength = m_stPositionDataHead.nPositionLength;
	return POSITION_SWIPE_OK;
}

int CPositionDataParser::GetPositionDataPositionDataCount(int& nPositionDataCount)
{
	nPositionDataCount = m_stPositionDataHead.nPositionDataCount;
	return POSITION_SWIPE_OK;
}


int CPositionDataParser::GetPositionDataBodyX1As(int nChannel, int& nValue)
{
	int nIndex = IsExistPositionDataBodyAs(nChannel);
	if (nIndex == POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL)
		return POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL;

	for(int i = 0; i < m_vecStPositionData.size(); i++)
	{
		if(m_vecStPositionData[i].nChannel == nChannel)
		{ 
			nValue = m_vecStPositionData[i].nX1;
			return POSITION_SWIPE_OK;
		}
	}

	return POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL;
}

int CPositionDataParser::GetPositionDataBodyY1As(int nChannel, int& nValue)
{
	int nIndex = IsExistPositionDataBodyAs(nChannel);
	if (nIndex == POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL)
		return POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL;

	for(int i = 0; i < m_vecStPositionData.size(); i++)
	{
		if(m_vecStPositionData[i].nChannel == nChannel)
		{ 
			nValue = m_vecStPositionData[i].nY1;
			return POSITION_SWIPE_OK;
		}
	}

	return POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL;
}

int CPositionDataParser::GetPositionDataBodyX2As(int nChannel, int& nValue)
{
	int nIndex = IsExistPositionDataBodyAs(nChannel);
	if (nIndex == POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL)
		return POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL;

	for(int i = 0; i < m_vecStPositionData.size(); i++)
	{
		if(m_vecStPositionData[i].nChannel == nChannel)
		{ 
			nValue = m_vecStPositionData[i].nX2;
			return POSITION_SWIPE_OK;
		}
	}

	return POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL;
}

int CPositionDataParser::GetPositionDataBodyY2As(int nChannel, int& nValue)
{
	int nIndex = IsExistPositionDataBodyAs(nChannel);
	if (nIndex == POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL)
		return POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL;

	for(int i = 0; i < m_vecStPositionData.size(); i++)
	{
		if(m_vecStPositionData[i].nChannel == nChannel)
		{ 
			nValue = m_vecStPositionData[i].nY2;
			return POSITION_SWIPE_OK;
		}
	}

	return POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL;
}

int CPositionDataParser::GetPositionDataBodyX3As(int nChannel, int& nValue)
{
	int nIndex = IsExistPositionDataBodyAs(nChannel);
	if (nIndex == POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL)
		return POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL;

	for(int i = 0; i < m_vecStPositionData.size(); i++)
	{
		if(m_vecStPositionData[i].nChannel == nChannel)
		{ 
			nValue = m_vecStPositionData[i].nX3;
			return POSITION_SWIPE_OK;
		}
	}

	return POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL;
}

int CPositionDataParser::GetPositionDataBodyY3As(int nChannel, int& nValue)
{
	int nIndex = IsExistPositionDataBodyAs(nChannel);
	if (nIndex == POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL)
		return POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL;

	for(int i = 0; i < m_vecStPositionData.size(); i++)
	{
		if(m_vecStPositionData[i].nChannel == nChannel)
		{ 
			nValue = m_vecStPositionData[i].nY3;
			return POSITION_SWIPE_OK;
		}
	}

	return POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL;
}

int CPositionDataParser::GetPositionDataBodyX4As(int nChannel, int& nValue)
{
	int nIndex = IsExistPositionDataBodyAs(nChannel);
	if (nIndex == POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL)
		return POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL;

	for(int i = 0; i < m_vecStPositionData.size(); i++)
	{
		if(m_vecStPositionData[i].nChannel == nChannel)
		{ 
			nValue = m_vecStPositionData[i].nX4;
			return POSITION_SWIPE_OK;
		}
	}

	return POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL;
}

int CPositionDataParser::GetPositionDataBodyY4As(int nChannel, int& nValue)
{
	int nIndex = IsExistPositionDataBodyAs(nChannel);
	if (nIndex == POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL)
		return POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL;

	for(int i = 0; i < m_vecStPositionData.size(); i++)
	{
		if(m_vecStPositionData[i].nChannel == nChannel)
		{ 
			nValue = m_vecStPositionData[i].nY4;
			return POSITION_SWIPE_OK;
		}
	}

	return POSITION_SWIPE_ERROR_NOT_EXIST_CHANNEL;
}

#ifdef _TAG_FOR_ANDROID
int CPositionDataParser::ErrorCheck()
{
	int nSize = m_nMetadataSize/sizeof(unsigned char);

	const int nLine = 10;
	int nQuotes = nSize/nLine;
	int nRemain = nSize%nLine;

	for(int i = 0; i < nQuotes; i++)
	{
		string strMetadata;
		for(int j = 0; j < nLine; j++)
		{
			strMetadata.append(to_string(m_szMetadata[i*nLine + j]));
			strMetadata.append(1, ' ');
		}
		__android_log_print(ANDROID_LOG_ERROR, LOG_POSITION_SWIPE_TAG,
				"[CallCount %d]Metadata(%d Lines) : %s",
				m_nInitPositionCallCount, i, strMetadata.c_str());
	}

  string strMetadataRemain;
	for(int i = nQuotes*nLine; i < nQuotes*nLine + nRemain; i++)
	{
      strMetadataRemain.append(to_string(m_szMetadata[i]));
      strMetadataRemain.append(1, ' ');
	}
    __android_log_print(ANDROID_LOG_ERROR, LOG_POSITION_SWIPE_TAG,
                        "[CallCount %d]Metadata(E Lines) : %s",
                        m_nInitPositionCallCount, strMetadataRemain.c_str());

	return 1;
}
#endif

