﻿////////////////////////////////////////////////////////////////////////////////
//
//	ESMMovieThreadManager.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-05-28
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMMovieThreadManager.h"
#include "ESMMovieMgr.h"
#include "FFmpegManager.h"
#include "ESMFunc.h"
#include "DllFunc.h"
#include "ESMGpuDecode.h"
#include "cudaKernel.cuh"
#include "opencv2\xphoto.hpp"
#include "ESMMovieFileOperation.h"
#include <afxmt.h>

#include "ImageProcessor.h"
#include "CTemplateCalculator.h"

#define MAX_CORE 6
//hjcho For Test
#define WRITER
#define  INSERT_TEXT

//#define LOGO_ZOOM
#ifndef PHI
#define PHI 3.1415926535897932384626433832795
#endif
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
using namespace cv::xphoto;

IMPLEMENT_DYNCREATE(CESMMovieThreadManager, CESMMovieThread)

int cnt = 0;

CMutex g_mutex(FALSE, _T("1"));

// CESMMovieEncoder
CESMMovieThreadManager::CESMMovieThreadManager(CWinThread* pParent, short nMessage, ESMMovieMsg* pMsg)
:CESMMovieThread(pParent, nMessage, pMsg)
{
	//17-07-24 wgkim Using Metadata
	m_nRepeatCount= 0;
	m_nPrevRepeatCount = -1;
	m_strFrameMetadata = _T("");
	m_bRotate = FALSE;
	m_nCore = 0;
	m_bBoth	 = FALSE;
}

CESMMovieThreadManager::~CESMMovieThreadManager()
{	
}

//--------------------------------------------------------------------------
//!@brief	RUN THREAD
//!@param   
//!@return	
//--------------------------------------------------------------------------
int CESMMovieThreadManager::Run(void)
{
	switch(m_nMessage)
	{
	case ESM_MOVIE_MANAGER_MAKINGSTOP:
		{
			((CESMMovieMgr*)m_pParent)->SetMakingStop(TRUE);
		}
		break;
	case ESM_MOVIE_MANAGER_ADJ_INFO:
		{
			TRACE(_T("[ESM MGR] Set Adjust Info\n"));
			stAdjustInfo* pAdjInfo = (stAdjustInfo*)m_pMsg->pValue1;
			//CMiLRe 20160202 Memory leak remove
			SendLog(5, _T("+++++++++++++++++++++++++++++"));
			((CESMMovieMgr*)m_pParent)->SetAdjustData(_T(""), pAdjInfo);
			SendLog(5, _T("-----------------------------"));
			//jhhan 190125 - 메모리해제 이슈 임시 제거
			/*
			delete pAdjInfo;
			pAdjInfo = NULL;
			*/
		}
		break;
	case ESM_MOVIE_MANAGER_SET_MARGIN:
		{
			((CESMMovieMgr*)m_pParent)->SetMargin(m_pMsg->nParam1, m_pMsg->nParam2);
		}
		break;
	case ESM_MOVIE_MANAGER_SETMOIVECOUNT:
		{
			((CESMMovieMgr*)m_pParent)->SetDscCount(m_pMsg->nOpt);			
		}
		break;
	case ESM_MOVIE_MANAGER_SET_FPS:
		{
			FFmpegManager FFmpegMgr(FALSE);
			FFmpegMgr.m_nFrameRate = m_pMsg->nParam1;
			//((CESMMovieMgr*)m_pParent)->SetDscCount(m_pMsg->nOpt);			
		}
		break;
	case ESM_MOVIE_MANAGER_SET_IMAGESIZE:
		{
			imgproc::SetMovieSize(m_pMsg->nParam1);	
			imgproc::SetUHDtoFHD(m_pMsg->nParam2);	
			imgproc::SetVMCCFlag(m_pMsg->nParam3);	
		}
		break;
	//wgkim 170727
	case ESM_MOVIE_MANAGER_SET_LIGHT_WEIGHT:
		{				
			((CESMMovieMgr*)m_pParent)->SetLightWeight(m_pMsg->nParam1);			
		}
		break;
	//wgkim 180629
	case ESM_MOVIE_MANAGER_SET_FRMAERATE:
		{				
			((CESMMovieMgr*)m_pParent)->SetFrameRateIndex(m_pMsg->nParam1);
		}
		break;

	case ESM_MOVIE_MANAGER_MOVIELOAD:
		{
			TCHAR* pPath = (TCHAR*)m_pMsg->pValue1;
			TCHAR* pStrId = (TCHAR*)m_pMsg->pValue2;
			CString strFilePath = pPath;
			CString strMovieId = pStrId;
			((CESMMovieMgr*)m_pParent)->SetMoviePath(strMovieId, strFilePath);
			delete[] pPath;
			delete[] pStrId;
			TRACE(_T("ESM_MOVIE_MANAGER_LOAD\n"));
		}
		break;
	case ESM_MOVIE_MANAGER_CREATE:
		{
			TRACE(_T("[ESM MGR] Create Object (mp4) with Adjust\n"));
		}
		break;
	case ESM_MOVIE_MANAGER_MOVIEMAKE:
		{
			MakeMovie((vector<TimeLineInfo>*)m_pMsg->pValue1);			
		}
		break;
	case ESM_MOVIE_MANAGER_MAKEFRAME_ADJUST:
		{
			MakeFrame((vector<MakeFrameInfo>*)m_pMsg->pValue1,(ESMAdjustInfo*)m_pMsg->pValue2 ,m_pMsg->nParam3);
		}
		break;
	case ESM_MOVIE_MANAGER_REQUEST_DATA:
		{
			RequestData((vector<MakeFrameInfo>*)m_pMsg->pValue1);
		}
		break;
	case ESM_MOVIE_MANAGER_MAKEFRAME:
		{
			MakeFrame((vector<MakeFrameInfo>*)m_pMsg->pValue1, m_pMsg->nParam1, m_pMsg->nParam2, (TCHAR*)m_pMsg->pValue2, m_pMsg->nParam3, FALSE, m_pMsg->pValue3, m_pMsg->nParam4, FALSE);

			ESMEvent* pMsg = new ESMEvent();
			pMsg->message = WM_ESM_MOVIE_START_FILETRANSFER;
			::SendMessage(((CESMMovieMgr*)m_pParent)->GetParentWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
		}
		break;
	case ESM_MOVIE_MANAGER_MAKEFRAME_REVERSE:
		{
			MakeFrame((vector<MakeFrameInfo>*)m_pMsg->pValue1, m_pMsg->nParam1, m_pMsg->nParam2, (TCHAR*)m_pMsg->pValue2, m_pMsg->nParam3, TRUE, m_pMsg->pValue3, m_pMsg->nParam4, FALSE);	

			ESMEvent* pMsg = new ESMEvent();
			pMsg->message = WM_ESM_MOVIE_START_FILETRANSFER;
			::SendMessage(((CESMMovieMgr*)m_pParent)->GetParentWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
		}
		break;
	case ESM_MOVIE_MANAGER_MAKEFRAME_LOCAL:
		{
			MakeFrame((vector<MakeFrameInfo>*)m_pMsg->pValue1, m_pMsg->nParam1, m_pMsg->nParam2, (TCHAR*)m_pMsg->pValue2, m_pMsg->nParam3, FALSE, m_pMsg->pValue3, m_pMsg->nParam4, TRUE);
		}
		break;
	case ESM_MOVIE_MANAGER_MAKEFRAME_REVERSE_LOCAL:
		{
			MakeFrame((vector<MakeFrameInfo>*)m_pMsg->pValue1, m_pMsg->nParam1, m_pMsg->nParam2, (TCHAR*)m_pMsg->pValue2, m_pMsg->nParam3, TRUE, m_pMsg->pValue3, m_pMsg->nParam4, TRUE);	
		}
		break;
	case ESM_MOVIE_MANAGER_INFORESET:
		{
			((CESMMovieMgr*)m_pParent)->ReSetMovieInfo();
		}
		break;
	//-- 2014-09-17 hongsu@esmlab.com
	//-- VALID FRAME INFO	
	case ESM_MOVIE_MANAGER_VALIDFRAME:
		{
			((CESMMovieMgr*)m_pParent)->AddValidFrame((ValidFrame*)m_pMsg->pValue1);
		}
		break;
	case ESM_MOVIE_MANAGER_VALIDRESET:
		{
			((CESMMovieMgr*)m_pParent)->RemoveAllValid();
		}
		break;
	case ESM_MOVIE_MANAGER_MUX:
		{
			m_bRotate = m_pMsg->nParam1;
			if(m_bRotate)
				SendLog(5,_T("ROTATE"));

			MakeGPUSequentialFrame((MuxDataInfo *)m_pMsg->pValue1);
		}
		break;
	//wgkim 20181001
	case ESM_MOVIE_MANAGER_SET_KZONE_POINTS:
		{
			int nDscCount						= m_pMsg->nParam1;
			KZoneDataInfo* pKZoneDataInfo		= (KZoneDataInfo*)m_pMsg->pValue1;
			CESMMovieKZoneMgr* pMovieKZoneMgr	= ((CESMMovieMgr*)m_pParent)->GetMovieKZoneMgr();

			pMovieKZoneMgr->SetKZoneDscCount(nDscCount);
			pMovieKZoneMgr->SetKZoneDataInfo(pKZoneDataInfo);
		}
		break;
	case ESM_MOVIE_MANAGER_OPTION_AUTOADJUST_KZONE:
		{
			BOOL bOptKZone						= (BOOL)m_pMsg->nParam1;
			CESMMovieKZoneMgr* pMovieKZoneMgr	= ((CESMMovieMgr*)m_pParent)->GetMovieKZoneMgr();

			pMovieKZoneMgr->SetOptKZone(bOptKZone);
		}
		break;
	case ESM_MOVIE_MANAGER_MULTIVIEW_MAKING:
		{
			MakeMultiView* pMultiView = (MakeMultiView*) m_pMsg->pValue1;
			RunMultiViewMaking(pMultiView);
		}
		break;
	default:
		{
			TRACE(_T("[ERROR] Unknown Message on Thread Codec\n"));
		}
		break;
	}

	if( m_pMsg )
	{
		delete m_pMsg;
		m_pMsg = NULL;
	}
	
	//-- 2014-05-26 hongsu@esmlab.com
	//-- Should be called FinishThread
	FinishThread();
 	return 1;
}

void CESMMovieThreadManager::MakeMovie(vector<TimeLineInfo>* arrTimeLineObject)
{
	MakeMovieInfo stMovieData;
	vector<FrameInfo>* pArrFramePosition = NULL;
	pArrFramePosition = new vector<FrameInfo>;
	ESMMovieMsg* pMsg = NULL;	
	CString strMovieName, strSrcFile;
	TCHAR* pMovieName = NULL;
	TCHAR* pMovieID = NULL;	
	EffectInfo* pEffectData;

	if( arrTimeLineObject->size() > 0)
	{
		stMovieData = arrTimeLineObject->at(0).stMovieData;

		strMovieName = stMovieData.strMovieName;
		((CESMMovieMgr*)m_pParent)->AddMovieObject(strMovieName);

		for( int i =0 ;i < arrTimeLineObject->size(); i++)
		{
			vector<EffectInfo> arrEffectData;
			stMovieData = arrTimeLineObject->at(i).stMovieData;

			// arrMovieObject 이정보를 기준으로 영상을 제작.
			// 여기서 영상의 모든 순서를 배치한다.( 순차 생성 )
			// 1. 영상 디코딩
			// 2. 영상 AdAdjust
			// 3. 영상 인코딩

			ESMMovieData* pMovieData = ((CESMMovieMgr*)m_pParent)->GetMovieData(strMovieName, stMovieData.strDscID);
			if( pMovieData == NULL || pMovieData->GetSizeHight() == 0)
			{
				TRACE(_T("Movie Pass [%s]"), stMovieData.strDscID);
				continue ;
			}

			pMovieName = new TCHAR[15];
			pMovieID = new TCHAR[15];
			_tcscpy(pMovieName, strMovieName);
			_tcscpy(pMovieID, stMovieData.strDscID);

			//-- Confirm Start/End Frame
			int nStartFrame, nEndFrame;
			//-- Check Start Frame
			nStartFrame	 = ((CESMMovieMgr*)m_pParent)->CheckValidIndex(stMovieData.strDscID, stMovieData.nStartFrame);
			if(nStartFrame == -1 )
				nStartFrame = stMovieData.nStartFrame;

			nEndFrame	 = ((CESMMovieMgr*)m_pParent)->CheckValidIndex(stMovieData.strDscID, stMovieData.nEndFrame);
			if(nEndFrame == -1 )
				nEndFrame = stMovieData.nEndFrame;

			if( pMovieData->GetBufferSize() <= nStartFrame + 3)
			{
				nStartFrame = pMovieData->GetBufferSize() - 3;
				stMovieData.nStartFrame = pMovieData->GetBufferSize() - 3;
			}
			else if( pMovieData->GetBufferSize() <= nEndFrame + 3)
			{
				nEndFrame = pMovieData->GetBufferSize() - 3;
				stMovieData.nEndFrame = pMovieData->GetBufferSize() - 3;
			}

			TRACE(_T("DSC [%s], nStartFrame [%d], nEndFrame[%d] \r\n"), stMovieData.strDscID, nStartFrame, nEndFrame);
			pMsg = new ESMMovieMsg;
			pMsg->pValue1 = (PVOID)pMovieName;
			pMsg->pValue2 = (PVOID)pMovieID;
			pMsg->nParam1 = nStartFrame;	// 시작 Frame 위치
			pMsg->nParam2 = nEndFrame;		// 끝 Frame 위치
			pMsg->nMsg = ESM_MOVIE_CODEC_H264_DECODE;
			((CESMMovieMgr*)m_pParent)->AddEvent(pMsg);

			// 영상 보정
			FrameInfo FrameData;
			int nCurIndex = stMovieData.nStartFrame;
			int nValidIndex;							//-- 2014-09-17 hongsu@esmlab.com		Real Create Frame
			int nEffectIndex = 0;

			while(1)
			{
				//-- Check Index
				nValidIndex = ((CESMMovieMgr*)m_pParent)->CheckValidIndex(stMovieData.strDscID, nCurIndex);

				//-- Add Frame
				//-- if nValidIndex == -1 : Skip Create Frame
				if(nValidIndex >= 0)
				{
					TRACE(_T("DSC [%s], nValidIndex [%d] \r\n"), stMovieData.strDscID, nValidIndex);

					pEffectData = new EffectInfo();
					if( arrTimeLineObject->at(i).arrEffectData.size() <= nEffectIndex)
						nEffectIndex--;

					memcpy(pEffectData, &(arrTimeLineObject->at(i).arrEffectData.at(nEffectIndex++)), sizeof(EffectInfo));

					ThreadData* pTheadData = new ThreadData;
					pTheadData->nFrameNum = nValidIndex;
					pTheadData->nDscIndex = stMovieData.nDscIndex;
					pTheadData->pMovieId = new TCHAR[15];
					pTheadData->pMovieName = new TCHAR[15];
					_tcscpy(pTheadData->pMovieId, stMovieData.strDscID);
					_tcscpy(pTheadData->pMovieName, strMovieName);

					pTheadData->pMovieThreadManager = this;
					pTheadData->pEffectData = pEffectData;

					HANDLE hSyncTime = NULL;
					hSyncTime = (HANDLE) _beginthreadex(NULL, 0, ImageAdjustThread, (void *)pTheadData, 0, NULL);
					CloseHandle(hSyncTime);

// 					//-- RESIZE EVENT 
// 					pMovieName = new TCHAR[15];		
// 					pMovieID = new TCHAR[15];	
// 					_tcscpy(pMovieName, strMovieName);
// 					_tcscpy(pMovieID, stMovieData.strDscID);
// 					pMsg = new ESMMovieMsg;
// 					pMsg->pValue1 = (PVOID)pMovieName;	
// 					pMsg->pValue2 = (PVOID)pMovieID;	
// 					pMsg->nParam1 = nValidIndex ;		// Frame 번호
// 					pMsg->nMsg = ESM_MOVIE_IMAGE_RESIZE;
// 					((CESMMovieMgr*)m_pParent)->AddEvent(pMsg);
// 
// 					//-- ROTATE EVENT 
// 					pMovieName = new TCHAR[15];		
// 					pMovieID = new TCHAR[15];
// 					_tcscpy(pMovieName, strMovieName);
// 					_tcscpy(pMovieID, stMovieData.strDscID);
// 					pMsg = new ESMMovieMsg;
// 					pMsg->pValue1 = (PVOID)pMovieName;	
// 					pMsg->pValue2 = (PVOID)pMovieID;	
// 					pMsg->nParam1 = nValidIndex ;		// Frame 번호
// 					pMsg->nMsg = ESM_MOVIE_IMAGE_ROTATE;
// 					((CESMMovieMgr*)m_pParent)->AddEvent(pMsg);
// 
// 					//-- MOVE EVENT
// 					pMovieName = new TCHAR[15];		
// 					pMovieID = new TCHAR[15];
// 					_tcscpy(pMovieName, strMovieName);
// 					_tcscpy(pMovieID, stMovieData.strDscID);
// 					pMsg = new ESMMovieMsg;
// 					pMsg->pValue1 = (PVOID)pMovieName;	
// 					pMsg->pValue2 = (PVOID)pMovieID;	
// 					pMsg->nParam1 = nValidIndex ;		// Frame 번호
// 					pMsg->nMsg = ESM_MOVIE_IMAGE_MOVE;
// 					((CESMMovieMgr*)m_pParent)->AddEvent(pMsg);
// 
// 					//-- IMAGE CUT EVENT
// 					pMovieName = new TCHAR[15];		
// 					pMovieID = new TCHAR[15];
// 					_tcscpy(pMovieName, strMovieName);
// 					_tcscpy(pMovieID, stMovieData.strDscID);
// 					pMsg = new ESMMovieMsg;
// 					pMsg->pValue1 = (PVOID)pMovieName;	
// 					pMsg->pValue2 = (PVOID)pMovieID;	
// 					pMsg->nParam1 = nValidIndex ;		// Frame 번호
// 					pMsg->nMsg = ESM_MOVIE_IMAGE_CUT;
// 					((CESMMovieMgr*)m_pParent)->AddEvent(pMsg);			
// 
// 					//-- Insert Logo
// 					pMovieName = new TCHAR[15];		
// 					pMovieID = new TCHAR[15];
// 					_tcscpy(pMovieName, strMovieName);
// 					_tcscpy(pMovieID, stMovieData.strDscID);
// 					pMsg = new ESMMovieMsg;
// 					pMsg->pValue1 = (PVOID)pMovieName;	
// 					pMsg->pValue2 = (PVOID)pMovieID;	
// 					pMsg->nParam1 = nValidIndex ;		// Frame 번호
// 					pMsg->nParam2 = stMovieData.nDscIndex;
// 					pMsg->nMsg = ESM_MOVIE_IMAGE_LOGO;
// 					((CESMMovieMgr*)m_pParent)->AddEvent(pMsg);			
// 
// 					//-- MOVIE EFFECT
// 					pEffectData = new EffectInfo();
// 					memcpy(pEffectData, &(arrTimeLineObject->at(i).arrEffectData.at(nEffectIndex)), sizeof(EffectInfo));
// 					pMovieName = new TCHAR[15];		
// 					pMovieID = new TCHAR[15];
// 					_tcscpy(pMovieName, strMovieName);
// 					_tcscpy(pMovieID, stMovieData.strDscID);
// 					pMsg = new ESMMovieMsg;
// 					pMsg->pValue1 = (PVOID)pMovieName;	
// 					pMsg->pValue2 = (PVOID)pMovieID;
// 					pMsg->pValue3 = (PVOID)pEffectData;
// 					pMsg->nParam1 = nValidIndex ;		// Frame 번호
// 					pMsg->nMsg = ESM_MOVIE_IMAGE_EFFECT;
// 					((CESMMovieMgr*)m_pParent)->AddEvent(pMsg);
// 					nEffectIndex++;


					// Encoding 된 영상들의 집합
					_tcscpy(FrameData.strMovieID, stMovieData.strDscID);
					FrameData.nFramePos = nValidIndex ;
					if( stMovieData.nEndFrame == stMovieData.nStartFrame && i + 1 == arrTimeLineObject->size())
						FrameData.nIsEndFrame = TRUE ;
					pArrFramePosition->push_back(FrameData);
				}
				else
				{
					// 서버로 메세지 전송.( 여긴 Client Movie Class )

					ESMEvent* pMsg = new ESMEvent();
					pMsg->nParam1 = nCurIndex;
					pMovieID = new TCHAR[15];	
					_tcscpy(pMovieID, stMovieData.strDscID);
					pMsg->pParam = (LPARAM)pMovieID;
					pMsg->message = WM_ESM_MOVIE_VALIDSKIP_FROM_MOVIEMGR;
					::SendMessage(((CESMMovieMgr*)m_pParent)->GetParentWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
					TRACE(_T("Remove Frame"));
				}

				//-- 2014-09-17 hongsu@esmlab.com
				//-- Do Next Frame
				if( nCurIndex == stMovieData.nEndFrame)
					break;
				if( nCurIndex > stMovieData.nEndFrame)
					nCurIndex--;
				else
					nCurIndex++;
			}
		}

// 		// 영상 보정
// 		if( pArrFramePosition != NULL)
// 		{
// 			pMsg = new ESMMovieMsg;
// 			pMovieName = new TCHAR[15];
// 			_tcscpy(pMovieName, strMovieName);
// 			pMsg->pValue1 = (PVOID)pArrFramePosition;
// 			pMsg->pValue2 = (PVOID)pMovieName;
// 			pMsg->nMsg = ESM_MOVIE_FILTER_COLORADJUST;
// 			((CESMMovieMgr*)m_pParent)->AddEvent(pMsg);
// 		}

		// 영상 생성.
		if( pArrFramePosition != NULL)
		{
			pMsg = new ESMMovieMsg;
			TCHAR* strMovieFile = new TCHAR[MAX_PATH];
			pMovieName = new TCHAR[15];
			_tcscpy(pMovieName, strMovieName);
			_stprintf(strMovieFile, _T("%s\\%s.mp4"),((CESMMovieMgr*)m_pParent)->GetSaveFolderPath(), stMovieData.strMovieName);
			pMsg->pValue1 = (PVOID)pArrFramePosition;
			pMsg->pValue2 = (PVOID)strMovieFile;
			pMsg->pValue3 = (PVOID)pMovieName;
			pMsg->nParam1 = stMovieData.nFrameSpeed;
			pMsg->nMsg = ESM_MOVIE_CODEC_H264_ENCODE;
			((CESMMovieMgr*)m_pParent)->AddEvent(pMsg);
		}
	}
	if( arrTimeLineObject )
	{
		delete arrTimeLineObject;
	}
}

#if 0
 unsigned WINAPI CESMMovieThreadManager::ImageAdjustThread(LPVOID param)
 {	
 	ThreadData* pTheadData = (ThreadData*)param;
 	CString strMovieId, strMovieName;
 	strMovieId = pTheadData->pMovieId;
 	strMovieName = pTheadData->pMovieName;
 	int nFrameNum = pTheadData->nFrameNum;
	int nDscIndex = pTheadData->nDscIndex;
	EffectInfo* pEffectData = pTheadData->pEffectData;
 	CESMMovieMgr* pMovieMgr = (CESMMovieMgr*)(pTheadData->pMovieThreadManager->m_pParent);
	delete pTheadData;

	WaitForSingleObject(pMovieMgr->hSemaphore, INFINITE);
	TRACE(_T("Start %s, %s, %d\n"), strMovieName, strMovieId, nFrameNum);

 	ESMMovieData* pMovieData = NULL;
 	pMovieData = pMovieMgr->GetMovieData(strMovieName, strMovieId);
 	if( pMovieData == NULL)
	{
		ReleaseSemaphore(pMovieMgr->hSemaphore, 1, NULL);
 		return 0;
	}

	while(1)
	{
		Sleep(1);
		if(ESM_CREATE_MOIVE_RESIZE <= pMovieData->GetFrameState(nFrameNum) + 1)
			break;
	}
 
	//-- RESIZE EVENT 
 	if(pMovieData->GetBufferSize() <= nFrameNum)
	{
		ReleaseSemaphore(pMovieMgr->hSemaphore, 1, NULL);
 		return 0;
	}
 
 	stAdjustInfo AdjustData = pMovieData->GetAdjustinfo();
 
 	int nWidth = 0, nHeight = 0;
 	nWidth = pMovieData->GetSizeWidth();
 	nHeight = pMovieData->GetSizeHight();
 	pMovieData->OrderAdd(nFrameNum);


 	//-- ROTATE EVENT 
 	cuda::GpuMat* pImageMat = pMovieData->GetImage(nFrameNum); 
	if( pImageMat == NULL)
	{
		ReleaseSemaphore(pMovieMgr->hSemaphore, 1, NULL);
		return 0;
	}

 	// Rotate 구현
 	int nRotateX = 0, nRotateY = 0, nSize = 0;
 	double dSize = 0.0;
 	double dRatio = (double)pImageMat->cols / (double)nWidth;
 	nRotateX = imgproc::Round(AdjustData.AdjptRotate.x  * dRatio );	 // 반올림
 	nRotateY = imgproc::Round(AdjustData.AdjptRotate.y  * dRatio );
 	imgproc::GpuRotateImage(pImageMat, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle);
 	pMovieData->OrderAdd(nFrameNum);
 
 	//-- MOVE EVENT
 	int nMoveX = 0, nMoveY = 0;
 	nMoveX = imgproc::Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
 	nMoveY = imgproc::Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
 	imgproc::GpuMoveImage(pImageMat, nMoveX,  nMoveY);
 	pMovieData->OrderAdd(nFrameNum);

	//-- IMAGE CUT EVENT
	int nMarginX = pMovieMgr->GetMarginX();
	int nMarginY = pMovieMgr->GetMarginY();
	if ( nMarginX < 0 || nMarginX >= nWidth/2 || nMarginY < 0 || nMarginY >= nHeight/2  )
	{
		pMovieData->OrderAdd(nFrameNum);
		ReleaseSemaphore(pMovieMgr->hSemaphore, 1, NULL);
		return 0;
	}

	if( pImageMat->cols > MOVIE_OUTPUT_FHD_WIDTH)
	{
		int nModifyMarginX = (nWidth - MOVIE_OUTPUT_FHD_WIDTH) /2;
		int nModifyMarginY = (nHeight - MOVIE_OUTPUT_FHD_HEIGHT) /2;
		cuda::GpuMat pMatCut =  (*pImageMat)(Rect(nModifyMarginX, nModifyMarginY, MOVIE_OUTPUT_FHD_WIDTH, MOVIE_OUTPUT_FHD_HEIGHT));
		pMatCut.copyTo(*pImageMat);
	}
	else
	{
		//imgproc::GpuMakeMargin(pImageMat, nMarginX, nMarginY);
		double dbMarginScale = 1 / ( (double)( nWidth - nMarginX * 2) /(double) nWidth  );
		cuda::GpuMat* pMatResize = new cuda::GpuMat;
		cuda::resize(*pImageMat, *pMatResize, Size(nWidth*dbMarginScale ,nHeight*dbMarginScale ), 0.0, 0.0 ,INTER_CUBIC );
		int nLeft = (int)((nWidth*dbMarginScale - nWidth)/2);
		int nTop = (int)((nHeight*dbMarginScale - nHeight)/2);
		cuda::GpuMat pMatCut =  (*pMatResize)(Rect(nLeft, nTop, nWidth, nHeight));
		pMatCut.copyTo(*pImageMat);
		delete pMatResize;
	}
	pMovieData->OrderAdd(nFrameNum);

	//-- Insert Logo
	if( nDscIndex > -1)
	{
		stAdjustInfo AdjustData = pMovieData->GetAdjustinfo();
		cuda::GpuMat* pImageMat = pMovieData->GetImage(nFrameNum);

		int nWidth = pMovieData->GetSizeWidth();
		int nHeight = pMovieData->GetSizeHight();

		// insert Logo 구현
		imgproc::GpuInsertLogo(pImageMat, nDscIndex, nWidth, nHeight);
	}
	pMovieData->OrderAdd(nFrameNum);

	//-- MOVIE EFFECT
	if (pEffectData->bZoom == FALSE)
	{
		pMovieData->OrderAdd(nFrameNum);
		delete pEffectData;
		ReleaseSemaphore(pMovieMgr->hSemaphore, 1, NULL);
		return 0;
	}

	if ( pEffectData->nZoomRatio != 100 )
		imgproc::GpuZoomImage(pMovieData->GetImage(nFrameNum), pMovieData->GetSizeWidth(), pMovieData->GetSizeHight(), pEffectData->nPosX,  pEffectData->nPosY, pEffectData->nZoomRatio);

	pMovieData->OrderAdd(nFrameNum);
	delete pEffectData;
	TRACE(_T("End %s, %s, %d\n"), strMovieName, strMovieId, nFrameNum);
	ReleaseSemaphore(pMovieMgr->hSemaphore, 1, NULL);
	return 0;
}
#else
//unsigned WINAPI CESMMovieThreadManager::ImageAdjustThread(LPVOID param)
//{	
//	ThreadData* pTheadData = (ThreadData*)param;
//	CString strMovieId, strMovieName;
//	strMovieId = pTheadData->pMovieId;
//	strMovieName = pTheadData->pMovieName;
//	int nFrameNum = pTheadData->nFrameNum;
//	int nDscIndex = pTheadData->nDscIndex;
//	EffectInfo* pEffectData = pTheadData->pEffectData;
//	CESMMovieMgr* pMovieMgr = (CESMMovieMgr*)(pTheadData->pMovieThreadManager->m_pParent);
//	delete pTheadData;
//
//	WaitForSingleObject(pMovieMgr->hSemaphore, INFINITE);
//	TRACE(_T("Start %s, %s, %d\n"), strMovieName, strMovieId, nFrameNum);
//
//	ESMMovieData* pMovieData = NULL;
//	pMovieData = pMovieMgr->GetMovieData(strMovieName, strMovieId);
//	if( pMovieData == NULL)
//	{
//		ReleaseSemaphore(pMovieMgr->hSemaphore, 1, NULL);
//		return 0;
//	}
//
//	while(1)
//	{
//		Sleep(1);
//		if(ESM_CREATE_MOIVE_RESIZE <= pMovieData->GetFrameState(nFrameNum) + 1)
//			break;
//	}
//
//	//-- RESIZE EVENT 
//	if(pMovieData->GetBufferSize() <= nFrameNum)
//	{
//		ReleaseSemaphore(pMovieMgr->hSemaphore, 1, NULL);
//		return 0;
//	}
//
//	stAdjustInfo AdjustData = pMovieData->GetAdjustinfo();
//
//	int nWidth = 0, nHeight = 0;
//	nWidth = pMovieData->GetSizeWidth();
//	nHeight = pMovieData->GetSizeHight();
//	pMovieData->OrderAdd(nFrameNum);
//
//
//	//-- ROTATE EVENT 
//	cuda::GpuMat* pImageMat = pMovieData->GetImage(nFrameNum); 
//	if( pImageMat == NULL)
//	{
//		ReleaseSemaphore(pMovieMgr->hSemaphore, 1, NULL);
//		return 0;
//	}
//	//CMiLRe 20151020 VMCC Effect(ZOOM) 기능 추가
//	int nSrcWidth = pImageMat->cols;
//	int nSrcHeight = pImageMat->rows;
//	int nOutputWidth = MOVIE_OUTPUT_FHD_WIDTH;
//	int nOutputHeight = MOVIE_OUTPUT_FHD_HEIGHT;
//
//	double dResizeScale = (double)nSrcWidth / (double)nOutputWidth;
//
//	int nSize = 0;
//	double dSize = 0.0;
//	double dRatio = (double)pImageMat->cols / (double)nWidth;
//	int nRotateX = imgproc::Round(AdjustData.AdjptRotate.x * dRatio );	 // 반올림
//	int nRotateY = imgproc::Round(AdjustData.AdjptRotate.y * dRatio );
//	int nMoveX = imgproc::Round(AdjustData.AdjMove.x * dRatio);	 // 반올림
//	int nMoveY = imgproc::Round(AdjustData.AdjMove.y * dRatio);	 // 반올림
//	int nMarginX = AdjustData.stMargin.nMarginX;//pMovieMgr->GetMarginX();
//	int nMarginY = AdjustData.stMargin.nMarginY;//pMovieMgr->GetMarginY();
//	double dbMarginScale = 1 / ( (double)( nWidth - nMarginX * 2) /(double) nWidth  );
//	int nResizeWidth = (nOutputWidth * dbMarginScale) + (nMarginX * 2);
//	int nResizeHeight = (nOutputHeight * dbMarginScale) + (nMarginY * 2);
//
//	if ( nMarginX < 0 || nMarginX >= nWidth/2 || nMarginY < 0 || nMarginY >= nHeight/2  )
//	{
//		pMovieData->OrderAdd(nFrameNum);
//		ReleaseSemaphore(pMovieMgr->hSemaphore, 1, NULL);
//		return 0;
//	}
//
//	if(nWidth > nOutputWidth && nHeight > nOutputHeight)
//	{
//		if (pEffectData->bZoom == TRUE && pEffectData->nZoomRatio != 100)
//		{
//			//imgproc::GpuZoomImage(pImageMat, nWidth, nHeight, pEffectData->nPosX, pEffectData->nPosY, pEffectData->nZoomRatio, nResizeWidth, nResizeHeight);
//
//			double dScale = (double)pEffectData->nZoomRatio / 100.0;
//			cuda::GpuMat gMatCut, gMatResize;
//
//			int nLeft = (pEffectData->nPosX- nSrcWidth/dScale/2) - nMarginX;
//			int nTop = (pEffectData->nPosY - nSrcHeight/dScale/2) - nMarginY;
//			int nWidth = (nSrcWidth/dScale - 1) + (nMarginX * 2);
//			if((nLeft + nWidth) > nSrcWidth)
//				nLeft = nLeft - ((nLeft + nWidth) - nSrcWidth);
//			else if(nLeft < 0)
//				nLeft = 0;
//			int nHeight = (nSrcHeight/dScale - 1) + (nMarginY * 2);
//			if((nTop + nHeight) > nSrcHeight)
//				nTop = nTop - ((nTop + nHeight) - nSrcHeight);
//			else if (nTop < 0)
//				nTop = 0;
//			gMatCut =  (*pImageMat)(Rect(nLeft, nTop, nWidth, nHeight));
//			gMatCut.copyTo(*pImageMat);
//
//			nRotateX = nRotateX - nLeft;
//			nRotateY = nRotateY - nTop;
//
//			nMarginX = nMarginX/dScale;
//			nMarginY = nMarginY/dScale;
//
//			// Rotate 구현
//			imgproc::GpuRotateImage(pImageMat, nRotateX, nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle);
//			pMovieData->OrderAdd(nFrameNum);
//
//			//-- MOVE EVENT
//			imgproc::GpuMoveImage(pImageMat, nMoveX,  nMoveY);
//			pMovieData->OrderAdd(nFrameNum);
//
//			//imgproc::GpuMakeMargin(pImageMat, nMarginX, nMarginY);
//
//			nWidth = pImageMat->cols - nMarginX*2;
//			nHeight = pImageMat->rows - nMarginY*2;
//			gMatCut = (*pImageMat)(Rect(nMarginX, nMarginY, nWidth, nHeight));
//			gMatCut.copyTo(*pImageMat);
//
//			cuda::resize(*pImageMat, gMatResize, Size(nOutputWidth ,nOutputHeight), 0.0, 0.0, INTER_CUBIC );
//			gMatResize.copyTo(*pImageMat);
//		}
//		else
//		{
//			cuda::GpuMat* pMatResize = new cuda::GpuMat;
//			if(nWidth > nOutputWidth && nHeight > nOutputHeight)
//			{
//				cuda::resize(*pImageMat, *pMatResize, Size(nOutputWidth ,nOutputHeight), 0.0, 0.0, INTER_CUBIC );
//				pMatResize->copyTo(*pImageMat);
//			}
//			delete pEffectData;
//
//			nWidth = nOutputWidth;
//			nHeight = nOutputHeight;
//			nRotateX = nRotateX / dResizeScale;
//			nRotateY = nRotateY / dResizeScale;
//			nMoveX = nMoveX / dResizeScale;
//			nMoveY = nMoveY / dResizeScale;
//			nMarginX = nMarginX / dResizeScale;
//			nMarginY = nMarginX / dResizeScale;
//
//			// Rotate 구현
//			imgproc::GpuRotateImage(pImageMat, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle);
//			pMovieData->OrderAdd(nFrameNum);
//
//			//-- MOVE EVENT
//			imgproc::GpuMoveImage(pImageMat, nMoveX,  nMoveY);
//			pMovieData->OrderAdd(nFrameNum);
//
//			//imgproc::GpuMakeMargin(pImageMat, nMarginX, nMarginY);
//			double dbMarginScale = 1 / ((double)(nWidth - nMarginX * 2) / (double)nWidth);
//
//			cuda::resize(*pImageMat, *pMatResize, Size(nWidth*dbMarginScale ,nHeight*dbMarginScale ), 0.0, 0.0 ,INTER_CUBIC );
//			int nLeft = (int)((nWidth*dbMarginScale - nWidth)/2);
//			int nTop = (int)((nHeight*dbMarginScale - nHeight)/2);
//			cuda::GpuMat pMatCut =  (*pMatResize)(Rect(nLeft, nTop, nWidth, nHeight));
//			pMatCut.copyTo(*pImageMat);
//			delete pMatResize;
//		}
//	}
//	else
//	{
//		if (pEffectData->bZoom == TRUE && pEffectData->nZoomRatio != 100)
//			imgproc::GpuZoomImage(pImageMat, nWidth, nHeight, pEffectData->nPosX,  pEffectData->nPosY, pEffectData->nZoomRatio, nResizeWidth, nResizeHeight);
//
//		delete pEffectData;
//
//		// Rotate 구현
//		imgproc::GpuRotateImage(pImageMat, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle);
//		pMovieData->OrderAdd(nFrameNum);
//		//-- MOVE EVENT
//		imgproc::GpuMoveImage(pImageMat, nMoveX,  nMoveY);
//		pMovieData->OrderAdd(nFrameNum);
//		//-- IMAGE CUT EVENT
//		//imgproc::GpuMakeMargin(pImageMat, nMarginX, nMarginY);
//		double dbMarginScale = 1 / ( (double)( nWidth - nMarginX * 2) /(double) nWidth  );
//		cuda::GpuMat* pMatResize = new cuda::GpuMat;
//		cuda::resize(*pImageMat, *pMatResize, Size(nWidth*dbMarginScale ,nHeight*dbMarginScale ), 0.0, 0.0 ,INTER_CUBIC );
//		int nLeft = (int)((nWidth*dbMarginScale - nWidth)/2);
//		int nTop = (int)((nHeight*dbMarginScale - nHeight)/2);
//		cuda::GpuMat pMatCut =  (*pMatResize)(Rect(nLeft, nTop, nWidth, nHeight));
//		pMatCut.copyTo(*pImageMat);
//		delete pMatResize;
//	}
//
//	// Rotate 구현
//	//  	int nRotateX = 0, nRotateY = 0, nSize = 0;
//	//  	double dSize = 0.0;
//	//  	double dRatio = (double)pImageMat->cols / (double)nWidth;
//	//  	nRotateX = imgproc::Round(AdjustData.AdjptRotate.x  * dRatio );	 // 반올림
//	//  	nRotateY = imgproc::Round(AdjustData.AdjptRotate.y  * dRatio );
//	// 	imgproc::GpuRotateImage(pImageMat, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle);
//	// 	pMovieData->OrderAdd(nFrameNum);
//	//  
//	//  	//-- MOVE EVENT
//	//  	int nMoveX = 0, nMoveY = 0;
//	//  	nMoveX = imgproc::Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
//	//  	nMoveY = imgproc::Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
//	//  	imgproc::GpuMoveImage(pImageMat, nMoveX,  nMoveY);
//	//  	pMovieData->OrderAdd(nFrameNum);
//	// 
//	// 	//-- IMAGE CUT EVENT
//	// 	int nMarginX = pMovieMgr->GetMarginX();
//	// 	int nMarginY = pMovieMgr->GetMarginY();
//	// 	if ( nMarginX < 0 || nMarginX >= nWidth/2 || nMarginY < 0 || nMarginY >= nHeight/2  )
//	// 	{
//	// 		pMovieData->OrderAdd(nFrameNum);
//	// 		ReleaseSemaphore(pMovieMgr->hSemaphore, 1, NULL);
//	// 		return 0;
//	// 	}
//	// 
//	// 	if( pImageMat->cols > nOutputWidth)
//	// 	{
//	// 		int nModifyMarginX = (nWidth - nOutputWidth) /2;
//	// 		int nModifyMarginY = (nHeight - nOutputHeight) /2;
//	// 		cuda::GpuMat pMatCut =  (*pImageMat)(Rect(nModifyMarginX, nModifyMarginY, nOutputWidth, nOutputHeight));
//	// 		pMatCut.copyTo(*pImageMat);
//	// 	}
//	// 	else
//	// 	{
//	// 		imgproc::GpuMakeMargin(pImageMat, nMarginX, nMarginY);
//	// 		double dbMarginScale = 1 / ( (double)( nWidth - nMarginX * 2) /(double) nWidth  );
//	// 		cuda::GpuMat* pMatResize = new cuda::GpuMat;
//	// 		cuda::resize(*pImageMat, *pMatResize, Size(nWidth*dbMarginScale ,nHeight*dbMarginScale ), 0.0, 0.0 ,INTER_CUBIC );
//	// 		int nLeft = (int)((nWidth*dbMarginScale - nWidth)/2);
//	// 		int nTop = (int)((nHeight*dbMarginScale - nHeight)/2);
//	// 		cuda::GpuMat pMatCut =  (*pMatResize)(Rect(nLeft, nTop, nWidth, nHeight));
//	// 		pMatCut.copyTo(*pImageMat);
//	// 		delete pMatResize;
//	// 	}
//	pMovieData->OrderAdd(nFrameNum);
//
//	//-- Insert Logo
//	if( nDscIndex > -1)
//	{
//		// 		stAdjustInfo AdjustData = pMovieData->GetAdjustinfo();
//		// 		cuda::GpuMat* pImageMat = pMovieData->GetImage(nFrameNum);
//		// 
//		// 		int nWidth = pMovieData->GetSizeWidth();
//		// 		int nHeight = pMovieData->GetSizeHight();
//
//		// insert Logo 구현
//		imgproc::GpuInsertLogo(pImageMat, nDscIndex, nOutputWidth, nOutputHeight);
//	}
//	pMovieData->OrderAdd(nFrameNum);
//	// 
//	// 	//-- MOVIE EFFECT
//	// 	if (pEffectData->bZoom == FALSE)
//	// 	{
//	// 		pMovieData->OrderAdd(nFrameNum);
//	// 		delete pEffectData;
//	// 		ReleaseSemaphore(pMovieMgr->hSemaphore, 1, NULL);
//	// 		return 0;
//	// 	}
//	// 
//	//  	if ( pEffectData->nZoomRatio != 100 )
//	//  		imgproc::GpuZoomImage(pMovieData->GetImage(nFrameNum), pMovieData->GetSizeWidth(), pMovieData->GetSizeHight(), pEffectData->nPosX,  pEffectData->nPosY, pEffectData->nZoomRatio);
//	// 
//	pMovieData->OrderAdd(nFrameNum);
//	// 	delete pEffectData;
//	TRACE(_T("End %s, %s, %d\n"), strMovieName, strMovieId, nFrameNum);
//	ReleaseSemaphore(pMovieMgr->hSemaphore, 1, NULL);
//	return 0;
//}

unsigned WINAPI CESMMovieThreadManager::ImageAdjustThread(LPVOID param)
{	
	ThreadData* pTheadData = (ThreadData*)param;
	CString strMovieId, strMovieName;
	strMovieId = pTheadData->pMovieId;
	strMovieName = pTheadData->pMovieName;
	int nFrameNum = pTheadData->nFrameNum;
	int nDscIndex = pTheadData->nDscIndex;
	EffectInfo* pEffectData = pTheadData->pEffectData;
	CESMMovieMgr* pMovieMgr = (CESMMovieMgr*)(pTheadData->pMovieThreadManager->m_pParent);
	delete pTheadData;

	return imgproc::AdjustMovieFrame(pMovieMgr, pEffectData, strMovieId, strMovieName, nFrameNum, nDscIndex);
}

#endif


void CESMMovieThreadManager::TimeCheck(CString str)
{
	CString timeStr;
	CString timeValue;
	SYSTEMTIME *SystemTime;
	SystemTime = new SYSTEMTIME;
	GetLocalTime(SystemTime);

	timeStr.Format(_T("%d : %d : %d.%d %s"),SystemTime->wHour,SystemTime->wMinute,SystemTime->wSecond,SystemTime->wMilliseconds,str);

	FILE *f;
	char tstr[MAX_PATH];

	sprintf(tstr,"%S",timeStr);

	f = fopen("c:\\Text.txt","at");
	if (f != NULL)
	{
		fputs(tstr, f);
		fclose(f);
	}
}


void CESMMovieThreadManager::MakeFrame(vector<MakeFrameInfo>* pArrFrameInfo, ESMAdjustInfo* pInfo, BOOL bReverse)
{
	FFmpegManager ffmpeg;
	if(ffmpeg.CheckCudaSupport())
	{
		for(int i = 0; i < pArrFrameInfo->size(); i++)
			pArrFrameInfo->at(i).bPlay = TRUE;

		CString strServerPath = pInfo->strTargetPath;
		DecodingAdjustFrame(pArrFrameInfo, bReverse, TRUE);
		EncodingAdjustMovie(pArrFrameInfo, pInfo);
	}
	else
	{
		CString strServerPath = pInfo->strTargetPath;
		DecodingFrame(pArrFrameInfo);
		ImageProcessFrame(pArrFrameInfo);
		EncodingAdjustMovie(pArrFrameInfo, pInfo);
	}
}

void CESMMovieThreadManager::RequestData(vector<MakeFrameInfo>* pArrFrameInfo)
{
	FILE *fp;
	CString strLog;

	char filename[255] = {0,};	
	sprintf(filename, "%S", pArrFrameInfo->at(0).strLocal);

	fp = fopen(filename, "rb");

	if(fp == NULL)
	{
		strLog.Format(_T("[#NETWORK] Not Exist File [%s]"), pArrFrameInfo->at(0).strLocal);
		SendLog(0,strLog);
		return;
	}

	strLog.Format(_T("[#NETWORK] Send File [%s]"), pArrFrameInfo->at(0).strLocal);
	SendLog(1,strLog);

	fseek(fp, 0L, SEEK_END);
	int nLength = ftell(fp);
	char* buf = new char[nLength];
	fseek(fp, 0, SEEK_SET);
    fread(buf, sizeof(char), nLength, fp);
	
	CString *pPath = new CString;
	pPath->Format(_T("%s"),pArrFrameInfo->at(0).strLocal);

	ESMEvent* pMsg = new ESMEvent();
	pMsg->message = WM_ESM_NET_REQUEST_DATA_TOSERVER;
	pMsg->pParam = (LPARAM)pPath;
	pMsg->pDest = (LPARAM)buf;
	pMsg->nParam3 = nLength;
	::SendMessage(((CESMMovieMgr*)m_pParent)->GetParentWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);


	fclose(fp);
/*
	//File Read
	CFile file;
	CString strLog;
	if(file.Open(pArrFrameInfo->at(0).strLocal, CFile::modeRead))
	{
		strLog.Format(_T("[#NETWORK] Send File [%s]"), pArrFrameInfo->at(0).strLocal);
		SendLog(1,strLog);
		int nLength = file.GetLength();
		char* buf = new char[nLength];
		file.Read(buf, nLength);

		CString *pPath = new CString;
		pPath->Format(_T("%s"),pArrFrameInfo->at(0).strLocal);

		ESMEvent* pMsg = new ESMEvent();
		pMsg->message = WM_ESM_NET_REQUEST_DATA_TOSERVER;
		pMsg->pParam = (LPARAM)pPath;
		pMsg->pDest = (LPARAM)buf;
		pMsg->nParam3 = nLength;
		::SendMessage(((CESMMovieMgr*)m_pParent)->GetParentWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);

		file.Close();
	}
	else
	{
		strLog.Format(_T("[#NETWORK] Not Exist File [%s]"), pArrFrameInfo->at(0).strLocal);
		SendLog(0,strLog);
	}
	*/
}

void CESMMovieThreadManager::RequestData(CString strPath,int nIdx,BOOL bAJAMaking /*= FALSE*/)
{
	FILE *fp;
	CString strLog;

	char filename[255] = {0,};	
	sprintf(filename, "%S", strPath);

	fp = fopen(filename, "rb");

	if(fp == NULL)
	{
		strLog.Format(_T("[#NETWORK] Not Exist File [%s]"), strPath);
		SendLog(0,strLog);
		return;
	}

	strLog.Format(_T("[#NETWORK] Send File [%s]"), strPath);
	//SendLog(1,strLog);

	fseek(fp, 0L, SEEK_END);
	int nLength = ftell(fp);
	char* buf = new char[nLength];
	fseek(fp, 0, SEEK_SET);
    fread(buf, sizeof(char), nLength, fp);
	
	CString *pPath = new CString;
	pPath->Format(_T("%s"),strPath);

	ESMEvent* pMsg = new ESMEvent();
	pMsg->message = WM_ESM_NET_REQUEST_DATA_TOSERVER;
	pMsg->pParam = (LPARAM)pPath;
	pMsg->pDest = (LPARAM)buf;
	pMsg->nParam1 = nIdx;
	pMsg->nParam2 = bAJAMaking;
	pMsg->nParam3 = nLength;
	::SendMessage(((CESMMovieMgr*)m_pParent)->GetParentWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);

	fclose(fp);

}

void CESMMovieThreadManager::MakeFrame(vector<MakeFrameInfo>* pArrFrameInfo, int nNetMode, int nMovieNum, TCHAR* strPath, BOOL bPlay, BOOL bReverse, void* pDest, BOOL bCopy, BOOL bLocal)
{	
	g_mutex.Lock();

	CString str;
	str.Format(_T("###########MAKE FRAME START[%d]"),pArrFrameInfo->size());
	SendLog(5, str);
	CString strServerPath = strPath;
	delete[] strPath;

	//jhhan 170309
	/*CESMMovieFileOperation opt;
	for(int i = 0; i< pArrFrameInfo->size(); i++)
	{
		MakeFrameInfo * pInfo = &pArrFrameInfo->at(i);
		CString strPath = pInfo->strFramePath;
		int nFind = strPath.ReverseFind('\\') + 1;
		CString strFile = _T("M:\\Movie\\");
		strFile.Append(strPath.Right(strPath.GetLength()-nFind));
		
		if(opt.IsFileExist(strFile))
		{
			_stprintf(pInfo->strFramePath, strFile);
		}
	}*/
	
	/*
	if(((CESMMovieMgr*)m_pParent)->GetGPUState())
	{
		SendLog(5,_T("Waiting...."));
		int nCnt = 0;
		while(1)
		{
			if(!((CESMMovieMgr*)m_pParent)->GetGPUState())
			{
				CString strLog;
				strLog.Format(_T("Waiting Time: %d"),nCnt*10);
				SendLog(5,strLog);
				break;
			}

			nCnt++;
			Sleep(10);
		}
	}
	*/

	FFmpegManager ffmpeg;
	if((ffmpeg.CheckCudaSupport()) && bPlay != -100 && bPlay == TRUE)
	{
		int nSatrt = GetTickCount();
		DecodingFrame(pArrFrameInfo,bReverse, bPlay, bCopy,bLocal);
		EncodingMovie(pArrFrameInfo, nNetMode, nMovieNum, strServerPath, bPlay);
		int nEnd = GetTickCount();
		CString strLog;
		strLog.Format(_T("[GPU]###########MAKE FRAME End  total Time %d ms"), nEnd - nSatrt);
		SendLog(1,strLog);
	}
	else
	{
		int nSatrt = GetTickCount();

		DecodingFrame(pArrFrameInfo);
		ImageProcessFrame(pArrFrameInfo, FALSE, bReverse, pDest);
		EncodingMovie(pArrFrameInfo, nNetMode, nMovieNum, strServerPath, bPlay);
		int nEnd = GetTickCount();
		CString strLog;
		strLog.Format(_T("[CPU]###########MAKE FRAME End  total Time %d ms"), nEnd - nSatrt);
		SendLog(1,strLog);
	}

	g_mutex.Unlock();
}
void CESMMovieThreadManager::DecodingAdjustFrame(vector<MakeFrameInfo>* pArrFrameInfo, BOOL bRotate , BOOL bPlay )
{
	FFmpegManager ffmpeg;
#if 1
	if((ffmpeg.CheckCudaSupport()) && bPlay)
		MakeGPUFrame(pArrFrameInfo,bRotate,FALSE);
#else
	if(ffmpeg.CheckCudaSupport()&&(bPlay))
	{
		//GPU Decoding
		HANDLE hSyncTime = NULL;
		ThreadDecodingData *pDecodingData = new ThreadDecodingData;
		pDecodingData->pMovieThreadManager = this;
		pDecodingData->pArrFrameInfo = pArrFrameInfo;
		pDecodingData->bGPUMakeFile = TRUE;
		pDecodingData->bRotate = bRotate;
		hSyncTime = (HANDLE) _beginthreadex(NULL, 0, ImageGpuDecodingThread, (void *)pDecodingData, 0, NULL);
		CloseHandle(hSyncTime);
	}
#endif
}

void CESMMovieThreadManager::DecodingFrame(vector<MakeFrameInfo>* pArrFrameInfo, BOOL bRotate, BOOL bPlay, BOOL bCopy, BOOL bLocal)
{
	int nStartIndex = 0;
	int nArrayStartIndex = 0;
	int nCurObjectIndex = -1;
	BOOL bSameFrame = FALSE;	// cygil 2015-11-18 동일Frame Not Decoding
	CString strCurPaht;
	int nFrame = 0;

	FFmpegManager ffmpeg;

	//For KT Project: Possible GTX 1060 & GTX 760
	if((ffmpeg.CheckCudaSupport()) &&(pArrFrameInfo->at(0).bGPUPlay))
	{
		MakeGPUFrame(pArrFrameInfo,bRotate,TRUE);
		//MakeSequentialFrame(pArrFrameInfo,bRotate);
	}
	//GPU Making : Only Use [GTX 1060 3G] Graphic Card

	else if(ffmpeg.CheckCudaSupport() &&(bPlay)) 
	{
		CString str;
		str.Format(L"[%d] Frame",pArrFrameInfo->size());
		SendLog(5,str);
		//GPU Decoding
		HANDLE hSyncTime = NULL;
		ThreadDecodingData *pDecodingData = new ThreadDecodingData;
		pDecodingData->pMovieThreadManager = this;
		pDecodingData->pArrFrameInfo = pArrFrameInfo;
		pDecodingData->bGPUMakeFile = pArrFrameInfo->at(0).bGPUPlay;
		pDecodingData->bRotate = bRotate;
		pDecodingData->bCopy = bCopy;
		pDecodingData->bLocal = bLocal;
		hSyncTime = (HANDLE) _beginthreadex(NULL, 0, ImageGpuDecodingThread, (void *)pDecodingData, 0, NULL);
		
		//Thread Core
		//if (((CESMMovieMgr*)m_pParent)->GetModel() == (SDI_MODEL)SDI_MODEL_GH5)
		{
			if(m_nCore > MAX_CORE)
				m_nCore = 0;

			SetThreadAffinityMask(hSyncTime,1<<m_nCore);
			m_nCore++;
		}

		CloseHandle(hSyncTime);
	}
	else
	{
#if 1
		//161220 hjcho : Reverse Decoding
		/*for( int i = 0; i< pArrFrameInfo->size(); i++)
		{
			MakeFrameInfo pFrameInfo = pArrFrameInfo->at(i);
			ThreadDecodingData *pDecodingData = new ThreadDecodingData;
			pDecodingData->pMovieThreadManager = this;
			pDecodingData->pArrFrameInfo = pArrFrameInfo;
			pDecodingData->nArrayStartIndex = i;
			pDecodingData->nStartIndex = pFrameInfo.nFrameIndex;
			pDecodingData->nEndIndex = pFrameInfo.nFrameIndex;
			//DoEncodingFrame(pArrFrameInfo, i, pFrameInfo.nFrameIndex, pFrameInfo.nFrameIndex);
			HANDLE hSyncTime = NULL;
			hSyncTime = (HANDLE) _beginthreadex(NULL, 0, ImageDecodingThread, (void *)pDecodingData, 0, NULL);
			CloseHandle(hSyncTime);
		}*/

		CString strBaseFramePath;
		int nBaseFrameIdx;
		int nBaseArrFrameIdx;

		//180308 ESMGetIdentify() 동일 //SDIDevice 기반
		int nFrame = 0;
		nFrame = m_nFrameRate * 100 + GetModel();

		for(int i = 0; i < pArrFrameInfo->size(); i++)
		{
			MakeFrameInfo *pFrameInfo = &(pArrFrameInfo->at(i));
			strBaseFramePath = pFrameInfo->strFramePath;
			nBaseArrFrameIdx = i;
			nBaseFrameIdx = pFrameInfo->nFrameIndex;

			if(i==0)
			{
				ThreadDecodingData *pDecodingData = new ThreadDecodingData;
				pDecodingData->pMovieThreadManager = this;
				pDecodingData->pArrFrameInfo = pArrFrameInfo;
				pDecodingData->nArrayStartIndex = i;
				pDecodingData->nStartIndex = pFrameInfo->nFrameIndex;
				pDecodingData->nEndIndex = pFrameInfo->nFrameIndex;
				/*pDecodingData->nFrameRate = m_nFrameRate*/;
				pDecodingData->nFrameRate = nFrame;

				//DoEncodingFrame(pArrFrameInfo, i, pFrameInfo.nFrameIndex, pFrameInfo.nFrameIndex);
				HANDLE hSyncTime = NULL;
				hSyncTime = (HANDLE) _beginthreadex(NULL, 0, ImageDecodingThread, (void *)pDecodingData, 0, NULL);
				
				//Thread Core
				//if (((CESMMovieMgr*)m_pParent)->GetModel() == (SDI_MODEL)SDI_MODEL_GH5)
				{
					if(m_nCore > MAX_CORE)
						m_nCore = 0;

					SetThreadAffinityMask(hSyncTime,1<<m_nCore);
					m_nCore++;
				}

				CloseHandle(hSyncTime);

				//Check Same Frame
				for(int j = 1; j < pArrFrameInfo->size(); j++)
				{
					MakeFrameInfo* pSearchFrameInfo = &(pArrFrameInfo->at(j));

					CString strCurFramePath = pSearchFrameInfo->strFramePath;
					int nCurFrameIdx = pSearchFrameInfo->nFrameIndex;

					if(strCurFramePath.CompareNoCase(strBaseFramePath) == 0 && nCurFrameIdx == nBaseFrameIdx)
					{
						//같은 frame이면
						pSearchFrameInfo->nSameFrameArrIdx = nBaseArrFrameIdx;
						pSearchFrameInfo->nSameFrameIdx    = nBaseFrameIdx;
					}
					else
					{
						//다른 프레임이면 교체
						nBaseFrameIdx    = nCurFrameIdx;
						nBaseArrFrameIdx = j;
						strBaseFramePath = strCurFramePath;	
					}
				}

				continue;
			}

			if(pFrameInfo->nSameFrameIdx != -1 && pFrameInfo->nSameFrameArrIdx != -1)
				continue;
			else
			{
				ThreadDecodingData *pDecodingData = new ThreadDecodingData;
				pDecodingData->pMovieThreadManager = this;
				pDecodingData->pArrFrameInfo = pArrFrameInfo;
				pDecodingData->nArrayStartIndex = i;
				pDecodingData->nStartIndex = pFrameInfo->nFrameIndex;
				pDecodingData->nEndIndex = pFrameInfo->nFrameIndex;
				/*pDecodingData->nFrameRate = m_nFrameRate*/;
				pDecodingData->nFrameRate = nFrame;
				//DoEncodingFrame(pArrFrameInfo, i, pFrameInfo.nFrameIndex, pFrameInfo.nFrameIndex);
				HANDLE hSyncTime = NULL;
				hSyncTime = (HANDLE) _beginthreadex(NULL, 0, ImageDecodingThread, (void *)pDecodingData, 0, NULL);
				
				//Thread Core
				//if (((CESMMovieMgr*)m_pParent)->GetModel() == (SDI_MODEL)SDI_MODEL_GH5)
				{
					if(m_nCore > MAX_CORE)
						m_nCore = 0;

					SetThreadAffinityMask(hSyncTime,1<<m_nCore);
					m_nCore++;
				}

				CloseHandle(hSyncTime);
			}
		}

#else
		for( int i = 0; i< pArrFrameInfo->size(); i++)
		{
			MakeFrameInfo pFrameInfo = pArrFrameInfo->at(i);

			// cygil 2015-11-18 동일Frame Not Decoding 
			bSameFrame = FALSE;
			if(i > 0)
			{
				for(int nPreIdx = 0; nPreIdx < i; nPreIdx++)
				{
					MakeFrameInfo stMakeFrameInfo = pArrFrameInfo->at(nPreIdx);
					if((_tcscmp(stMakeFrameInfo.strFramePath, pFrameInfo.strFramePath) == 0) && stMakeFrameInfo.nFrameIndex == pFrameInfo.nFrameIndex)
					{
						pArrFrameInfo->at(i).nSameFrameIdx = nPreIdx;
						pArrFrameInfo->at(i).bComplete = 1;
						bSameFrame = TRUE;
						break;
					}
				}
				if(bSameFrame)
					continue;
			}
			// cygil 2015-11-18 동일Frame Not Decoding

		//	if( pFrameInfo.nObjectIndex != -1)				// 연속적 이미지		
		//	{
		//		if( nCurObjectIndex != pFrameInfo.nObjectIndex || strCurPaht != pFrameInfo.strFramePath)		// 연속적 Object 의 첫프레임
		//		{
		//			if(pArrFrameInfo->size() - 1 == i)															// Object 의 마지막 Frame일 경우
		//			{
		//				ThreadDecodingData *pDecodingData = new ThreadDecodingData;
		//				pDecodingData->pMovieThreadManager = this;
		//				pDecodingData->pArrFrameInfo = pArrFrameInfo;
		//				pDecodingData->nArrayStartIndex = i;
		//				pDecodingData->nStartIndex = pFrameInfo.nFrameIndex;
		//				pDecodingData->nEndIndex = pFrameInfo.nFrameIndex;
		//				HANDLE hSyncTime = NULL;
		//				hSyncTime = (HANDLE) _beginthreadex(NULL, 0, ImageDecodingThread, (void *)pDecodingData, 0, NULL);
		//				CloseHandle(hSyncTime);
		//				TRACE(_T("DecodingFrame Only FirstFrameObject nArrayStartIndex[%d] nCurFrameIndex[%d]]\n"), nArrayStartIndex, pFrameInfo.nFrameIndex);
		//				continue;
		//			}
		//			else if((pArrFrameInfo->at(i + 1).nObjectIndex != pFrameInfo.nObjectIndex  ||				//Object 의 첫프레임이 마지막 Object 인 경우.		
		//				_tcscmp(pArrFrameInfo->at(i + 1).strFramePath,pFrameInfo.strFramePath) != 0))
		//			{
		//				ThreadDecodingData *pDecodingData = new ThreadDecodingData;
		//				pDecodingData->pMovieThreadManager = this;
		//				pDecodingData->pArrFrameInfo = pArrFrameInfo;
		//				pDecodingData->nArrayStartIndex = i;
		//				pDecodingData->nStartIndex = pFrameInfo.nFrameIndex;
		//				pDecodingData->nEndIndex = pFrameInfo.nFrameIndex;
		//				HANDLE hSyncTime = NULL;
		//				hSyncTime = (HANDLE) _beginthreadex(NULL, 0, ImageDecodingThread, (void *)pDecodingData, 0, NULL);
		//				CloseHandle(hSyncTime);
		//				TRACE(_T("DecodingFrame Only FirstFrameObject nArrayStartIndex[%d] nCurFrameIndex[%d]]\n"), nArrayStartIndex, pFrameInfo.nFrameIndex);
		//				continue;
		//			}
		//			else
		//			{
		//				strCurPaht = pFrameInfo.strFramePath;
		//				nCurObjectIndex = pFrameInfo.nObjectIndex;
		//				nStartIndex = pFrameInfo.nFrameIndex;
		//				nArrayStartIndex = i;
		//				continue;
		//			}
		//		}
		//		else if(nCurObjectIndex == pFrameInfo.nObjectIndex)	
		//		{
		//			if(pArrFrameInfo->size() - 1 == i || 
		//				(pArrFrameInfo->size() - 1 != i &&										//연속적 Object 의 마지막 프레임
		//				(pArrFrameInfo->at(i + 1).nObjectIndex != pFrameInfo.nObjectIndex  ||
		//				_tcscmp(pArrFrameInfo->at(i + 1).strFramePath,pFrameInfo.strFramePath) != 0)))
		//			{
		//				ThreadDecodingData *pDecodingData = new ThreadDecodingData;
		//				pDecodingData->pMovieThreadManager = this;
		//				pDecodingData->pArrFrameInfo = pArrFrameInfo;
		//				pDecodingData->nArrayStartIndex = nArrayStartIndex;
		//				pDecodingData->nStartIndex = nStartIndex;
		//				pDecodingData->nEndIndex = pFrameInfo.nFrameIndex;
		//				HANDLE hSyncTime = NULL;
		//				hSyncTime = (HANDLE) _beginthreadex(NULL, 0, ImageDecodingThread, (void *)pDecodingData, 0, NULL);
		//				CloseHandle(hSyncTime);

		//				//DoEncodingFrame(pArrFrameInfo, nArrayStartIndex, nStartIndex, pFrameInfo.nFrameIndex);
		//				continue;
		//			}
		//			else	// 연속적 Object 의 중간 프레임
		//				continue;
		//		}
		//	}
		//	else		// 개별
			{
				ThreadDecodingData *pDecodingData = new ThreadDecodingData;
				pDecodingData->pMovieThreadManager = this;
				pDecodingData->pArrFrameInfo = pArrFrameInfo;
				pDecodingData->nArrayStartIndex = i;
				pDecodingData->nStartIndex = pFrameInfo.nFrameIndex;
				pDecodingData->nEndIndex = pFrameInfo.nFrameIndex;
				//DoEncodingFrame(pArrFrameInfo, i, pFrameInfo.nFrameIndex, pFrameInfo.nFrameIndex);
				HANDLE hSyncTime = NULL;
				hSyncTime = (HANDLE) _beginthreadex(NULL, 0, ImageDecodingThread, (void *)pDecodingData, 0, NULL);
				CloseHandle(hSyncTime);
			}
		}
#endif
		
	}
}

//161219 hjcho: If bCheckMethod true, for KT Project. Else for Adjust Movie
void CESMMovieThreadManager::MakeGPUFrame(vector<MakeFrameInfo>* pArrFrameInfo,BOOL bRotate, BOOL bCheckMethod, BOOL bLocal)
{
	//int start = GetTickCount();

	//CString strPath = pArrFrameInfo->at(0).strFramePath;
	//int nSecIndex = pArrFrameInfo->at(0).nSecIdx;
	//CT2CA pszConvertedAnsiString (strPath);
	//string strDst(pszConvertedAnsiString);

	//CESMMovieFileOperation fo;
	//int Cnt=0;
	////while(1)
	////{
	////	if(!fo.IsFileExist(strPath))
	////	{
	////		CString strt;
	////		strt.Format(_T("File None [%s]"),strPath);
	////		SendLog(5,strt);
	////		Cnt++;
	////	}
	////	else
	////		break;

	////	if(Cnt == 100)
	////	{
	////		SendLog(5,_T("No way "));
	////		return;
	////	}
	////	Sleep(10);
	////}



	//ThreadDecodingData *pDecodingData = new ThreadDecodingData;
	//pDecodingData->pMovieThreadManager = this;

	//CESMMovieThreadManager *pMovieThreadManager = pDecodingData->pMovieThreadManager;
	//CESMMovieThreadImage MovieImage;

	//CString strDscId;
	//int nIndex = strPath.ReverseFind('\\') + 1;
	//CString strTpPath = strPath.Right(strPath.GetLength() - nIndex);
	//strDscId = strTpPath.Left(5);
	//stAdjustInfo AdjustData = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetAdjustData(strDscId);

	//int nWidth = 1920, nHeight = 1080;
	//int nSrcWidth=0;
	//int nSrcHeight=0;
	//int nOutputWidth=0;
	//int nOutputHeight=0;
	//if(!bCheckMethod)
	//{
	//	((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMovieSizeData(nSrcWidth, nSrcHeight, nOutputWidth, nOutputHeight);
	//	nWidth = nSrcWidth;
	//	nHeight = nSrcHeight;
	//}
	////GetVideoSize(strPath,&nWidth,&nHeight);	
	////int n1 = GetTickCount();
	//////복사
	////CESMMovieFileOperation fo;
	////CString strDSCA,strRealDst;
	////strDSCA.Format(_T("M:\\%s.mp4"),strDscId);

	////if(fo.Copy(strPath,strDSCA))
	////{
	////	SendLog(5,_T("Copy"));
	////	strRealDst = strDSCA;
	////}
	////else
	////{
	////	SendLog(5,_T("Network"));
	////	strRealDst = strPath;
	////}
	////int n2 = GetTickCount();
	////CT2CA pszConvertedAnsiString (strRealDst);
	////string strDst(pszConvertedAnsiString);

	//VideoCapture vc(strDst);
	//if(!vc.isOpened()){
	//	CString strLog;
	//	strLog.Format(_T("File Error %s"),strPath);
	//	SendLog(1,strLog);
	//	return;
	//}
	////int n2 = GetTickCount();
	//int nImageSize = nHeight * nWidth * 3;
	//int nYUVSize = nWidth * (nHeight+nHeight/2);

	//int pCnt = 0;
	//Mat frame;
	//Mat result(nHeight,nWidth,CV_8UC3);
	//Mat srcImage(nHeight,nWidth,CV_8UC3);
	//Mat YUVImage(nHeight+nHeight/2,nWidth,CV_8UC1);

	////Angle
	//double degree = AdjustData.AdjAngle;

	//double dbAngleAdjust = -1 * (degree + 90);
	//double theta =  (-dbAngleAdjust)*(PHI/180);
	//if(degree == 0) theta = 0;
	//double dRatio = 1;

	//double RotX  = imgproc::Round(AdjustData.AdjptRotate.x * dRatio);
	//double RotY  = imgproc::Round(AdjustData.AdjptRotate.y * dRatio);

	////Move
	//double AdjX  = imgproc::Round(AdjustData.AdjMove.x * dRatio);
	//double AdjY  = imgproc::Round(AdjustData.AdjMove.y * dRatio);

	//int nPasteTop,nPasteBottom,nPasteLeft,nPasteRight;

	//////Margin
	//int nMarginX =  AdjustData.stMargin.nMarginX;//((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginX() * dRatio;
	//int nMarginY =  AdjustData.stMargin.nMarginY;//((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginY() * dRatio;
	//
	////CString strTempLog;
	////strTempLog.Format(_T("[%s]Margin(%d,%d)"),strDscId,AdjustData.stMargin.nMarginX,AdjustData.stMargin.nMarginY);
	//
	//double dbMarginScale = 1 / ( (double)( nWidth - nMarginX * 2) /(double) nWidth);
	//int nLeft = (int)((nWidth  * dbMarginScale - nWidth)/2);
	//int nTop  = (int)((nHeight * dbMarginScale - nHeight)/2);

	//int nH = nHeight*dbMarginScale;
	//int nW = nWidth*dbMarginScale;
	//Mat Resize  (nH,nW ,CV_8UC3);
	//Mat tmp (1,1,CV_8UC1);
	////int n3 = GetTickCount();
	////CString strInit;
	////strInit.Format(_T("LoadAdj(%d) VideoLoad(%d) CalAdj(%d)"),n1 - start, n2-n1,n3-n2);
	////SendLog(5,strInit);
	////int nS = GetTickCount();
	//CCudaFunc cuKernel;// = new CCudaFunc;	
	//cuKernel.cuSetProperty(nHeight,nWidth,nH,nW,theta,RotX,RotY,AdjustData.AdjSize,AdjX,AdjY);
	////int nE = GetTickCount();

	//int tSum = 0;
	//int aSum = 0;
	//int dSum = 0;
	//int iSum = 0;

	//while(1)
	//{
	//	//int nStart = GetTickCount();
	//	MakeFrameInfo *pFrame = &(pArrFrameInfo->at(pCnt));

	//	vc>>frame;

	//	if(frame.empty()) break;

	//	if(pFrame->bMovieReverse)
	//		flip(frame,frame,-1);
	//	//int nDe  = GetTickCount();
	//	//Revision
	//	//cuKernel.cuDipAll(frame,AdjX,AdjY,Resize);
	//	cuKernel.cuDipAll(frame,Resize);
	//	
	//	result = (Resize)(cv::Rect(nLeft,nTop,nWidth,nHeight));
	//	srcImage = result.clone();

	//	if(bRotate) //For Rotation
	//		flip(srcImage,srcImage,-1);
	//	//int nEnd = GetTickCount();

	//	//tSum += (nEnd - nStart);
	//	//dSum += (nDe - nStart);
	//	//iSum += (nEnd - nDe);
	//	//int mS = GetTickCount();

	//	
	//	
	//	if(bCheckMethod) // For KT Project
	//	{
	//		pFrame->pYUVImg = new BYTE[nYUVSize];

	//		pFrame->nHeight = nHeight;
	//		pFrame->nWidth = nWidth;
	//		pFrame->nImageSize = nYUVSize;
	//		cvtColor(srcImage,YUVImage,CV_BGR2YUV_I420);

	//		memcpy(pFrame->pYUVImg,YUVImage.data,nYUVSize);
	//	}
	//	else	//For Adjust Movie
	//	{
	//		pFrame->Image = (BYTE*) cvCreateImage(cv::Size(nWidth,nHeight),IPL_DEPTH_8U,3);//new BYTE[nImageSize];

	//		IplImage* img = (IplImage*) pFrame->Image;
	//		pFrame->nHeight = nHeight;
	//		pFrame->nWidth  = nWidth;
	//		pFrame->nImageSize = nImageSize;

	//		memcpy(img->imageData,srcImage.data,nImageSize);
	//		pFrame->bComplete++;
	//		//cvReleaseImage(&img);
	//	}

	//	//int mE = GetTickCount();
	//	//aSum += (mE - mS);
	//	pCnt++;
	//}
	//cuKernel.cuFreeProperty();

	////if(fo.IsFileExist(strDSCA))
	////	fo.Delete(strDSCA);

	//int end = GetTickCount();
	//
	//CString str;
	//str.Format(_T("[%d] Decoding & DIP Total Time %dms."),nSecIndex,end-start);
	//SendLog(1,str);
}

void CESMMovieThreadManager::MakeSequentialFrame(vector<MakeFrameInfo>* pArrFrameInfo,BOOL bRotate, BOOL bCopy, BOOL bLocal)
{

	ThreadDecodingData *pDecodingData = new ThreadDecodingData;
	pDecodingData->pMovieThreadManager = this;

	CESMMovieThreadManager *pMovieThreadManager = pDecodingData->pMovieThreadManager;

	((CESMMovieMgr*)pMovieThreadManager->m_pParent)->SetGPUState(TRUE);

	int start = GetTickCount();
	CString strPath;
	if(bLocal)
		strPath = pArrFrameInfo->at(0).strLocal;
	else
		strPath = pArrFrameInfo->at(0).strFramePath;

	int nSecIndex = pArrFrameInfo->at(0).nSecIdx;

	/*
	//jhhan 170328 Local
	CString strLocal = pArrFrameInfo->at(0).strLocal;
	//SendLog(5, strLocal);
	CESMMovieFileOperation fo;
	if(fo.IsFileExist(strLocal))
	{
		strPath = strLocal;
		CString strLog;
		strLog.Format(_T("MakeSequentialFrame - Local : %s"), strPath);
		SendLog(5, strLog);
	}*/

	CString strDscId;
	int nIndex = strPath.ReverseFind('\\') + 1;
	CString strTpPath = strPath.Right(strPath.GetLength() - nIndex);
	strDscId = strTpPath.Left(5);
	stAdjustInfo AdjustData = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetAdjustData(strDscId);

	int nWidth = 1920, nHeight = 1080;
	int nSrcWidth=0;
	int nSrcHeight=0;
	int nOutputWidth=0;
	int nOutputHeight=0;
	int nPreWidth ;
	int nPreHeight;

	int nSelMode = -1;
	imgproc::GetMovieSizeData(nSrcWidth, nSrcHeight, nOutputWidth, nOutputHeight);
	nWidth = nSrcWidth;
	nHeight = nSrcHeight;


	
	//복사
#if 0
	CESMMovieFileOperation fo;
	CString strDSCP,strRealDst;
	strDSCP.Format(_T("M:\\%s.mp4"),strDscId);

	if(fo.Copy(strPath,strDSCP))
	{
		SendLog(5,_T("Copy %s -> %s"),strPath,strDSCP);
		strRealDst = strDSCP;
	}
	else
	{
		SendLog(5,_T("Network"));
		strRealDst = strPath;
	}
	
	CT2CA pszConvertedAnsiString (strRealDst);
	string strDst(pszConvertedAnsiString);
#else
	//171113
	int nCopyTime = 0;
	int nLoadFrameTime = 0;
	if(bCopy)
	{
		CESMMovieFileOperation fo;
		CString strLog;
		CString strTargetPath;
		strTargetPath.Format(_T("M:\\Movie\\%s.mp4"), strDscId);

		int nStart = GetTickCount();
		int nEnd;

		fo.Delete(strTargetPath);
		if(fo.Copy(strPath,strTargetPath))
		{
			nEnd = GetTickCount();
			strLog.Format(_T("### Copy File %s -> %s  Time %dms"), strPath, strTargetPath, nEnd-nStart);
			SendLog(1,strLog);
			strPath = strTargetPath;
			nCopyTime = nEnd-nStart;
		}
		else
		{
			nEnd = GetTickCount();
			strLog.Format(_T("### Copy fail %s -> %s  Time %dms"), strPath, strTargetPath, nEnd-nStart);
			SendLog(0,strLog);
		}
	}
	else
		SendLog(1,_T("###non Copy"));


	CT2CA pszConvertedAnsiString (strPath);
	string strDst(pszConvertedAnsiString);
#endif

	int nTickStart = GetTickCount();

	// Local File Check   joonho.kim
	if(bLocal)
	{
		CString strLog;
		int nLoop = 0;
		while(1)
		{
			CESMMovieFileOperation fo;
			if(fo.IsFileExist(strPath))
			{
				strLog.Format(_T("[#NETWORK] Local File Check OK [%s]"), strPath);
				SendLog(1, strLog);
				break;
			}
			else
			{
				strLog.Format(_T("[#NETWORK] Local File Check... [%s]"), strPath);
				SendLog(1, strLog);
				Sleep(500);

				if(nLoop > 10)
					break;
			}
			nLoop++;
		}
	}
	
	VideoCapture vc(strDst);
	if(!vc.isOpened())
	{
		CString strLog;
		strLog.Format(_T("File Error %s"),strPath);
		SendLog(1,strLog);

		for(int i = 0 ; i <pArrFrameInfo->size(); i++)
		{
			pArrFrameInfo->at(i).bComplete = -100;
		}
		return;
	}
	
	if(bLocal)
	{
		CESMMovieFileOperation fo;

		//Test Code // 4DP 백업폴더 삭제 //jhhan 확인요망
		if(strPath.Find(_T("RecordSave")) == -1)
		{
			CString str;
			str.Format(_T("#####Delete File %s"), strPath);
			SendLog(1,str);
			fo.Delete(strPath);
		}
		//fo.Delete(strPath);
	}

	int nTickEnd = GetTickCount();
	CString strLog1;
	strLog1.Format(_T("Load Frame: %d"),nTickEnd-nTickStart);
	SendLog(5,strLog1);
	nLoadFrameTime = nTickEnd-nTickStart;

	//joonho.kim Send Time
	{
		//IP 얻기
		char name[255];
		CString strLocalIP;
		CString strToken;
		PHOSTENT hostinfo;
		int nIP = 0;
		int nPos = 0;
		if( gethostname(name,sizeof(name)) == 0)
			if((hostinfo = gethostbyname(name)) != NULL)
				strLocalIP = inet_ntoa(*(struct in_addr*)*hostinfo->h_addr_list);
		while((strToken = strLocalIP.Tokenize(_T("."),nPos)) != "")
		{
			nIP = _ttoi(strToken);
		}	


		ESMEvent* pMsg = new ESMEvent();
		pMsg->nParam1 = nIP;
		pMsg->nParam2 = nCopyTime;
		pMsg->nParam3 = nLoadFrameTime;
		pMsg->message = WM_ESM_NET_FRAME_LOAD_TIMETOSERVER;
		::SendMessage(((CESMMovieMgr*)m_pParent)->GetParentWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
	}

	nSelMode = SelectSelMode(pMovieThreadManager,nSrcHeight,nSrcWidth);

	double dRatio = 1;
	if(nSelMode ==1)
	{
		if(nOutputWidth != nSrcWidth)
		{
			nWidth = nOutputWidth;
			nHeight = nOutputHeight;
		}
		dRatio = (double)nOutputWidth / (double)nSrcWidth;
	}
	else if(nSelMode == 3)
	{
		nPreWidth = nOutputWidth;
		nPreHeight = nOutputHeight;

		nOutputWidth = nSrcWidth;
		nOutputHeight = nSrcHeight;
	}

	int pCnt = 0;
	Mat frame;
	Mat result(nHeight,nWidth,CV_8UC3);
	Mat srcImage(nHeight,nWidth,CV_8UC3);

	//Angle
	double degree = AdjustData.AdjAngle;

	double dbAngleAdjust = -1 * (degree + 90);
	double theta =  -dbAngleAdjust*(CV_PI/180);

	if(degree == 0) theta = 0;

	double RotX  = imgproc::Round(AdjustData.AdjptRotate.x * dRatio);
	double RotY  = imgproc::Round(AdjustData.AdjptRotate.y * dRatio);

	//Move
	double AdjX  = imgproc::Round(AdjustData.AdjMove.x * dRatio);
	double AdjY  = imgproc::Round(AdjustData.AdjMove.y * dRatio);

	////Margin
	int nMarginX = (int) (AdjustData.stMargin.nMarginX * dRatio);//((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginX() * dRatio;
	int nMarginY = (int) (AdjustData.stMargin.nMarginY * dRatio);//((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginY() * dRatio;

	double dbMarginScale = 1 / ( (double)( nWidth - nMarginX * 2) /(double) nWidth);
	int nLeft = (int)((nWidth  * dbMarginScale - nWidth)/2);
	int nTop  = (int)((nHeight * dbMarginScale - nHeight)/2);

	int nH = (int) (nHeight*dbMarginScale);
	int nW = (int) (nWidth*dbMarginScale);
	Mat Resize  (nH,nW ,CV_8UC3);

	int nStart,nEnd;
	int nArrSize = (int) pArrFrameInfo->size();
	nStart =pArrFrameInfo->at(0).nFrameIndex;
	nEnd = pArrFrameInfo->at(nArrSize-1).nFrameIndex;
	BOOL bStart = FALSE;

//	CCudaFunc cuKernel;
//	cuKernel.cuSetProperty(nHeight,nWidth,nH,nW,theta,RotX,RotY,AdjustData.AdjSize,AdjX,AdjY);

	CString strLog;
	strLog.Format(_T("Sequential Frame [%d][%d,%d][%s] %d -> %d"),nSelMode,nSrcWidth,nSrcHeight,strPath,nStart,nEnd);
	SendLog(5,strLog);
	strLog.Format(_T("[%s] (%d,%d)->(%f->%f)(%f,%f)->(%f,%f) : (%d,%d) (%d,%d)"),strDscId,nWidth,nHeight,degree,theta,RotX,RotY,AdjX,AdjY,nMarginX,nMarginY,nW,nH);
	SendLog(5,strLog);

	int nRotCenterX = nWidth/2;
	int nRotCenterY = nHeight/2;
	while(1)
	{
		int nBlack = 0;
		vc>>frame;

		if(pCnt != nStart && !bStart)
		{
			pCnt++;
			continue;
		}
		if(frame.empty())	break;
		
		MakeFrameInfo *pFrame = &(pArrFrameInfo->at(pCnt-nStart));

		if(pFrame->bMovieReverse)
		{
			//flip(frame,frame,-1);
			imgproc::CpuRotateImage(frame,nRotCenterX,nRotCenterY,AdjustData.AdjSize,AdjustData.AdjAngle,TRUE);
		}

		if(pFrame->bColorRevision)
		{
			FrameRGBInfo stColorInfo = pFrame->stColorInfo;
			imgproc::DoColorRevision(&frame,stColorInfo);
		}
		bStart = TRUE;

		if(nSelMode == 1)
		{
			Mat MatResize;
			resize(frame,MatResize,cv::Size(nOutputWidth,nOutputHeight),0,0,INTER_CUBIC);
			MatResize.copyTo(frame);
		}
		//Making Start!
		//cuKernel.cuDipAll(frame,AdjX,AdjY,Resize);
		//cuKernel.cuDipAll(frame,Resize);

		cuda::GpuMat gpuFrame(frame);

		imgproc::GpuRotateImage(&gpuFrame, RotX ,RotY, AdjustData.AdjSize, AdjustData.AdjAngle,0);
		imgproc::GpuMoveImage(&gpuFrame, AdjX, AdjY);
		cuda::resize(gpuFrame,gpuFrame,cv::Size(nW,nH),0,0,INTER_CUBIC);
		gpuFrame.download(frame);

		/*Mat MatResize;
		resize(frame,MatResize,cv::Size(nW,nH),0,0,INTER_CUBIC);
		MatResize.copyTo(frame);*/

		if(pCnt == nStart)
		{
			for(int i = 0 ; i < frame.rows*frame.cols*3;i++)
			{
				if(frame.data[i] == 0)
					nBlack ++;
			}

			if(nBlack == frame.rows*frame.cols*3 )
				SendLog(0,_T("GPU Load Error"));
		}

		result = (frame)(cv::Rect(nLeft,nTop,nWidth,nHeight));
		srcImage = result.clone();

		//Reverse
		if(bRotate)
		{
			imgproc::CpuRotateImage(frame,nRotCenterX,nRotCenterY,AdjustData.AdjSize,AdjustData.AdjAngle,TRUE);
			//flip(srcImage,srcImage,-1);
		}
			

		//SelMode
		/* 1: VMCC X
		   2: VMCC O, FHD
		   3: VMCC O, UHD
		   */
		if(nSelMode == 2 || nSelMode == 3)
		{
			//Effect Data
			EffectInfo pEffectData = pFrame->stEffectData;

			Mat MatCut, MatResize;
			double dScale = (double)pEffectData.nZoomRatio / 100.0;

			int nLeft = (int) (pEffectData.nPosX- nSrcWidth/dScale/2);
			int nTop = (int) (pEffectData.nPosY - nSrcHeight/dScale/2);
			int nWidth = (int) (nSrcWidth/dScale - 1);
			if((nLeft + nWidth) > nSrcWidth)
				nLeft = nLeft - ((nLeft + nWidth) - nSrcWidth);
			else if(nLeft < 0)
				nLeft = 0;
			int nHeight = (int) (nSrcHeight/dScale - 1);
			if((nTop + nHeight) > nSrcHeight)
				nTop = nTop - ((nTop + nHeight) - nSrcHeight);
			else if (nTop < 0)
				nTop = 0;
			MatCut =  (srcImage)(Rect(nLeft, nTop, nWidth, nHeight));
			srcImage = MatCut.clone();

			if(nSelMode ==3)
			{
				Mat MatResize;
				//UHD ? FHD?
				if(imgproc::GetUHDtoFHD())
				{
					//FHD
					resize(srcImage, MatResize, Size(nPreWidth ,nPreHeight), 0.0, 0.0, INTER_CUBIC );
					nOutputHeight = nPreHeight;
					nOutputWidth  = nPreWidth;
					MatResize.copyTo(srcImage);
				}
				else
				{
					//UHD
					resize(srcImage, MatResize, Size(nOutputWidth ,nOutputHeight), 0.0, 0.0, INTER_CUBIC );
					MatResize.copyTo(srcImage);
				}
			}
			else
			{
				resize(srcImage, MatResize, Size(nSrcWidth ,nSrcHeight), 0.0, 0.0, INTER_CUBIC );
				MatResize.copyTo(srcImage);
			}
		}

		//Logo
		if(pFrame->nDscIndex > -1)
		{
			imgproc::GpuInsertLogo2(&srcImage, pFrame->nDscIndex, pFrame->b3DLogo);
		}
		//Prism
		if( pFrame->nPriIndex > -1 || pFrame->nPrinumIndex > 0)
		{
			// insert Logo 구현
			imgproc::KzonePrism(&srcImage,pFrame->nPriIndex,pFrame->nPriValue,pFrame->nPriValue2,pFrame->nPrinumIndex);
		}
		//Camera ID
		//if( pFrame->strCameraID[0] != _T('\0'))
			imgproc::GpuInsertCameraID(&srcImage,pFrame->nPriIndex,pFrame->strCameraID,pFrame->strDSC_IP,pFrame->bPrnColorInfo);
		//WhiteBalance
		if(pFrame->bInsertWB)
		{
			Ptr<xphoto::WhiteBalancer> wb;
			wb = xphoto::createGrayworldWB();
			wb->balanceWhite(srcImage, srcImage);
		}

		pFrame->Image = (BYTE*) cvCreateImage(cvSize(nOutputWidth,nOutputHeight),IPL_DEPTH_8U,3);
		int nImageSize = nOutputWidth * nOutputHeight * 3;

		IplImage* img = (IplImage*) pFrame->Image;
		pFrame->nHeight = nHeight;
		pFrame->nWidth  = nWidth;
		pFrame->nImageSize = nImageSize;
		memcpy(img->imageData,srcImage.data,nImageSize);
		pFrame->bComplete++;

		pCnt++;

		if(pCnt > nEnd) break;
	}
//	cuKernel.cuFreeProperty();
	
	int end = GetTickCount();

	for(int i = 0; i < pArrFrameInfo->size(); i++)
	{
		if(pArrFrameInfo->at(i).bComplete == 1)
			continue;
		pArrFrameInfo->at(i).bComplete = -100;
	}

	vc.release();
	CESMMovieFileOperation fo;
	if(fo.IsFileExist(strPath) && bLocal)
	{
		int nLength = strPath.ReverseFind(_T('\\'));
		CString strDel = strPath.Left(nLength);
		//SendLog(1, strDel);
		//Test Code // 4DP 백업폴더 삭제 //jhhan 확인요망
		if(strPath.Find(_T("RecordSave")) == -1)
		{
			CString str;
			str.Format(_T("#####Delete File %s"), strDel);
			SendLog(1,str);
			fo.Delete(strDel);
		}
	}
	CString str;
	str.Format(_T("[%d]Conscutive Total Time %dms."),pCnt-nStart,end-start);
	SendLog(1,str);
}

int CESMMovieThreadManager::GetVideoSize(CString strpath,int* nSrcWidth,int* nSrcHeight)
{
	CT2CA pszConvertedAnsiString (strpath);
	string strDst(pszConvertedAnsiString);
	VideoCapture vc(strDst);
	Mat tmp;
	if(!vc.isOpened())
	{
		CString strLog;
		strLog.Format(_T("Cannot Open %s"),strpath);

		SendLog(1,strLog);
		vc.release();	
		return -1;
	}
	vc>>tmp;
	*nSrcHeight = tmp.rows;
	*nSrcWidth  = tmp.cols;
	vc.release();

	return 0;
}

void CESMMovieThreadManager::SubProcessing(MakeFrameInfo *pFrame,Mat srcImage,BOOL bRotate)
{
	Mat tmp = srcImage.clone();
	//Reverse
	if(bRotate)
		flip(srcImage,srcImage,-1);

	//Logo
	if(pFrame->nDscIndex > -1)
	{
		imgproc::GpuInsertLogo2(&tmp, pFrame->nDscIndex, pFrame->b3DLogo);
	}
	//Prism
	if( pFrame->nPriIndex > -1 || pFrame->nPrinumIndex > 0)
	{
		// insert Logo 구현
		imgproc::KzonePrism(&tmp,pFrame->nPriIndex,pFrame->nPriValue,pFrame->nPriValue2,pFrame->nPrinumIndex);
	}
	//Camera ID
	//if( pFrame->strCameraID[0] != _T('\0'))
		imgproc::GpuInsertCameraID(&tmp,pFrame->nPriIndex,pFrame->strCameraID,pFrame->strDSC_IP,pFrame->bPrnColorInfo);
	//WhiteBalance
	if(pFrame->bInsertWB)
	{
		Ptr<xphoto::WhiteBalancer> wb;
		wb = xphoto::createGrayworldWB();
		wb->balanceWhite(tmp, tmp);
	}
	srcImage = tmp.clone();
}

int CESMMovieThreadManager::SelectSelMode(CESMMovieThreadManager *pMovieThreadManager,int nSrcHeight,int nSrcWidth)
{
	if(!imgproc::GetVMCCFlag())
	{
		return 1;
	}
	else if(imgproc::GetVMCCFlag() && nSrcWidth == FULL_HD_WIDTH && nSrcHeight == FULL_HD_HEIGHT)
	{
		return 2;
	}
	else
	{
		return 3;
	}
}

void CESMMovieThreadManager::ImageProcessFrame(vector<MakeFrameInfo>* pArrFrameInfo, BOOL bSaveImg, BOOL bReverse, void* pDest)
{	
#if 0
	CString* strPath;
	strPath = (CString*)pDest;
	CString strDate;
	CTime time = CTime::GetCurrentTime();
	if(strPath)
		strDate.Format(_T("%s\\%04d_%02d_%02d_%02d_%02d_%02d"), strPath->GetString(), time.GetYear(),time.GetMonth(),time.GetDay(),time.GetHour(),time.GetMinute(),time.GetSecond());

 	//for( int i = pArrFrameInfo->size() - 1; i >= 0; i--)
	for( int i = 0; i < pArrFrameInfo->size(); i++)
 	{
 		MakeFrameInfo* pFrameInfo = &(pArrFrameInfo->at(i));
 	//	TRACE(_T("strFramePath[%s], nFrameIndex[%d], nFrameSpeed[%d]\n"), pFrameInfo.strFramePath, pFrameInfo.nFrameIndex, pFrameInfo.nFrameSpeed);
		while(1)
		{
			Sleep(1);
			if(pFrameInfo->bComplete > 0)
				break;
			if( pFrameInfo->bComplete == -100)
				break;
		}
		if( pFrameInfo->bComplete == -100)
			continue;

		// cygil 2015-11-18 동일Frame Not Decoding 
		if(pFrameInfo->bComplete == 1 && pFrameInfo->nImageSize == 0 && pFrameInfo->nSameFrameIdx >= 0)
		{
			while(1)
			{
				if(pArrFrameInfo->at(pFrameInfo->nSameFrameIdx).bComplete != 0)
					break;
			}
			if(pArrFrameInfo->at(pFrameInfo->nSameFrameIdx).bComplete == -100)
			{
				pFrameInfo->bComplete = -100;
				continue;
			}
			pFrameInfo->nImageSize = pArrFrameInfo->at(pFrameInfo->nSameFrameIdx).nImageSize;
			pFrameInfo->Image = new BYTE[pFrameInfo->nImageSize];
			memcpy(pFrameInfo->Image, pArrFrameInfo->at(pFrameInfo->nSameFrameIdx).Image, pFrameInfo->nImageSize);
		}
		// cygil 2015-11-18 동일Frame Not Decoding 

		TRACE(_T("strFramePath[%s], nFrameIndex[%d], nFrameSpeed[%d]\n"), pFrameInfo->strFramePath, pFrameInfo->nFrameIndex, pFrameInfo->nFrameSpeed);

		ThreadFrameData *pFrameData = new ThreadFrameData;
		pFrameData->pFrameInfo = pFrameInfo;
		pFrameData->pMovieThreadManager = this;
		pFrameData->bSaveImg = bSaveImg;
		pFrameData->strPath = strDate;
		pFrameData->bReverse = bReverse;
		
		ImageAdjust((void*)pFrameData);	

		CString strLog;
		strLog.Format(_T("ImageAdjust Index %d"), i);
		//SendLog(1,strLog);
	}
#else
	CString* strPath;
	strPath = (CString*)pDest;
	CString strDate;
	CTime time = CTime::GetCurrentTime();
	if(strPath)
		strDate.Format(_T("%s\\%04d_%02d_%02d_%02d_%02d_%02d"), strPath->GetString(), time.GetYear(),time.GetMonth(),time.GetDay(),time.GetHour(),time.GetMinute(),time.GetSecond());

	int nCurIdx = -1;
	int nImageSize = 0;
	CString strFirstPath = pArrFrameInfo->at(0).strFramePath;

	//Test Code
	/*for(int n = 0; n < pArrFrameInfo->size(); n++)
	{
		while(1)
		{
			Sleep(1);
			if(pArrFrameInfo->at(n).bComplete > 0)
				break;
			if( pArrFrameInfo->at(n).bComplete == -100)
				break;
		}
		if( pArrFrameInfo->at(n).bComplete == -100)
			continue;
	}*/



	//for( int i = pArrFrameInfo->size() - 1; i >= 0; i--)
	
	BYTE* pImg = NULL;

	BYTE* pY = NULL;
	BYTE* pU = NULL;
	BYTE *pV = NULL;

	BOOL bYUV = TRUE;
	
	//if(pArrFrameInfo->at(0).nDscIndex > -1 
	//	|| pArrFrameInfo->at(0).nPriIndex > -1 || pArrFrameInfo->at(0).nPrinumIndex > 0)
	//	bYUV = FALSE;
	//else
	//	bYUV = TRUE;

	TRACE("bYUV: %d\n",bYUV);

	/////////////////////////////////////////////////////////////
	// slee 200623 - Adjust 영역 계산 구문 추가

	CTemplateCalculator tempObj;
	vector<Point2f> vInterPts;

	//bool bDrawPoly = true;
	//Mat tempMat = Mat::zeros(Size(1920,1080), CV_8UC3);

	for( int i = 0; i < pArrFrameInfo->size(); i++)
	{
		MakeFrameInfo* pFrameInfo = &(pArrFrameInfo->at(i));

		CESMMovieMgr *pMovieManager = (CESMMovieMgr *) this->m_pParent;

		WaitForSingleObject(pMovieManager->hSemaphore, INFINITE);
		
		// Frame Path의 DSC ID를 통해 Adjust Data Load
		CString strDscId;
		CString strFramePath = pFrameInfo->strFramePath;
		int nIndex = strFramePath.ReverseFind('\\') + 1;
		CString strTpPath = strFramePath.Right(strFramePath.GetLength() - nIndex);
		strDscId = strTpPath.Left(5);
		stAdjustInfo adjustData = pMovieManager->GetAdjustData(strDscId);
		
		ReleaseSemaphore(pMovieManager->hSemaphore, 1, NULL);



		Mat m(2, 3, CV_32FC1);

		Rect2f rtMargin(adjustData.rtMargin.left, adjustData.rtMargin.top, adjustData.rtMargin.right - adjustData.rtMargin.left, adjustData.rtMargin.bottom - adjustData.rtMargin.top);

		int nMoveX = 0, nMoveY = 0;
		nMoveX = imgproc::Round(adjustData.AdjMove.x);
		nMoveY = imgproc::Round(adjustData.AdjMove.y);

		float fAngle = adjustData.AdjAngle + 90.f;
		int nRotateX = 0, nRotateY = 0;
		nRotateX = imgproc::Round(adjustData.AdjptRotate.x - adjustData.nWidth / 2.f );
		nRotateY = imgproc::Round(adjustData.AdjptRotate.y - adjustData.nHeight / 2.f );

		float fScale = adjustData.AdjSize;

		//Cpu Margin
		int nMarginX = (int) adjustData.stMargin.nMarginX;
		int nMarginY = (int) adjustData.stMargin.nMarginY;

		float fRatioW = 1.f, fRatioH = 1.f;
		if(rtMargin.width != 0 && rtMargin.height != 0)
		{
			fRatioW = (float)adjustData.nWidth / rtMargin.width;
			fRatioH = (float)adjustData.nHeight / rtMargin.height;
		}
		else
		{
			fRatioW = (float)adjustData.nWidth / (float)(adjustData.nWidth - nMarginX * 2.f);
			fRatioH = (float)adjustData.nHeight / (float)(adjustData.nHeight - nMarginY * 2.f);
		}

		float fWarpW = 0., fWarpH = 0.;
		fWarpW = fScale * adjustData.nWidth * fRatioW;
		fWarpH = fScale * adjustData.nHeight * fRatioH;

		float fScaleX = 1., fScaleY = 1.;
		fScaleX = fWarpW / adjustData.nWidth;
		fScaleY = fWarpH / adjustData.nHeight;


		if(adjustData.dBorderScale > 1.)
		{
			fScaleX /= adjustData.dBorderScale * fRatioW;
			fScaleY /= adjustData.dBorderScale * fRatioH;
		}

		float fOffsetX = 0., fOffsetY = 0.;
		if(rtMargin.width != 0 && rtMargin.height != 0)
		{
			fOffsetX = (((rtMargin.x + rtMargin.width / 2.f) - adjustData.nWidth / 2.f) + nMoveX) * fScaleX;
			fOffsetY = (((rtMargin.y + rtMargin.height / 2.f) - adjustData.nHeight / 2.f) + nMoveY) * fScaleY;
		}
		else
		{
			fOffsetX = (nMarginX) * fScaleX;
			fOffsetY = (nMarginY) * fScaleY;
		}

		float fRad = fAngle * CV_PI / 180.f;


		Point2f arrCornerPts[4];
		arrCornerPts[0] = Point2f(-adjustData.nWidth / 2., -adjustData.nHeight / 2.);
		arrCornerPts[1] = Point2f(-adjustData.nWidth / 2., adjustData.nHeight / 2.);
		arrCornerPts[2] = Point2f(adjustData.nWidth / 2., adjustData.nHeight / 2.);
		arrCornerPts[3] = Point2f(adjustData.nWidth / 2., -adjustData.nHeight / 2.);

		vector<Point2f> vPts;
		for(int j=0; j<4; j++)
		{
			Point2f new_pt = imgproc::GetRotatedCoord(Point2f(arrCornerPts[j].x - nRotateX, arrCornerPts[j].y - nRotateY), fRad);

			vPts.push_back(Point2f((new_pt.x + nRotateX) * fScaleX + fOffsetX, (new_pt.y + nRotateY) * fScaleY + fOffsetY));
		}

		////Draw
		//if(bDrawPoly)
		//{
		//	vector<Point2f> vPtsDraw;
		//	for(int j=0; j < vPts.size(); j++)
		//	{
		//		vPtsDraw.push_back(Point2f((vPts[j].x + adjustData.nWidth / 2.) / 4., (vPts[j].y + adjustData.nHeight / 2.) / 4.));
		//	}

		//	const Point* pts = (const cv::Point*) Mat(vPtsDraw).data;
		//	int npts = Mat(vPtsDraw).rows;
		//	polylines(tempMat, &pts, &npts, 1, true, CV_RGB(255, 255, 255), 1);
		//}

		if(i == 0)
		{
			vInterPts = vPts;
			continue;
		}

		vector<Point2f> vInterTemp;
		bool bSame = true;

		if(vInterPts.size() == vPts.size())
		{
			for(int j=0; j<vInterPts.size(); j++)
			{
				if(vInterPts[j] != vPts[j])
				{
					bSame = false;
					break;
				}
			}
		}
		else
			bSame = false;

		if(bSame)
		{
			vInterTemp = vInterPts;
		}
		else
		{
			vInterTemp = tempObj.GetIntersection(vInterPts, vPts);
		}

		vInterPts = vInterTemp;		


		//if(bDrawPoly)
		//{
		//	vector<Point2f> vInterPtsDraw;
		//	for(int j=0; j < vInterPts.size(); j++)
		//	{
		//		vInterPtsDraw.push_back(Point2f((vInterPts[j].x + adjustData.nWidth / 2.) / 4., (vInterPts[j].y + adjustData.nHeight / 2.) / 4.));
		//	}

		//	const Point* ptsI = (const cv::Point*) Mat(vInterPtsDraw).data;
		//	int nptsI = Mat(vInterPtsDraw).rows;
 		//	fillPoly(tempMat, &ptsI, &nptsI, 1, CV_RGB(255, 0, 0));

		//	imshow("poly", tempMat);
		//	waitKey(0);
		//}
	}

	imgproc::SetInterAdjPts(vInterPts);
	/////////////////////////////////////////////////////////////


	for( int i = 0; i < pArrFrameInfo->size(); i++)
	{
		MakeFrameInfo* pFrameInfo = &(pArrFrameInfo->at(i));
		//	TRACE(_T("strFramePath[%s], nFrameIndex[%d], nFrameSpeed[%d]\n"), pFrameInfo.strFramePath, pFrameInfo.nFrameIndex, pFrameInfo.nFrameSpeed);
		
		if(pFrameInfo->nSameFrameIdx == -1)//Not Same Frame
		{
			while(1)
			{
				Sleep(1);
				if(pFrameInfo->bComplete > 0)
					break;
				if( pFrameInfo->bComplete == -100)
					break;
			}
			if( pFrameInfo->bComplete == -100)
				continue;
		}
		/*Mat test(2160,3840,CV_8UC3,pFrameInfo->Image);
		char strIndex[20];
		sprintf(strIndex,"%d",i);
		putText(test,strIndex,cv::Point(1920,1080),CV_FONT_HERSHEY_COMPLEX,3,Scalar(255,255,255),2,8);
		sprintf(strIndex,"M:\\%d.jpg",i);
		imwrite(strIndex,test);*/

		//// cygil 2015-11-18 동일Frame Not Decoding 
		//if(pFrameInfo->bComplete == 1 && pFrameInfo->nImageSize == 0 && pFrameInfo->nSameFrameIdx >= 0)
		//{
		//	while(1)
		//	{
		//		if(pArrFrameInfo->at(pFrameInfo->nSameFrameIdx).bComplete != 0)
		//			break;
		//	}
		//	if(pArrFrameInfo->at(pFrameInfo->nSameFrameIdx).bComplete == -100)
		//	{
		//		pFrameInfo->bComplete = -100;
		//		continue;
		//	}
		//	pFrameInfo->nImageSize = pArrFrameInfo->at(pFrameInfo->nSameFrameIdx).nImageSize;
		//	pFrameInfo->Image = new BYTE[pFrameInfo->nImageSize];
		//	memcpy(pFrameInfo->Image, pArrFrameInfo->at(pFrameInfo->nSameFrameIdx).Image, pFrameInfo->nImageSize);
		//}
		// cygil 2015-11-18 동일Frame Not Decoding 

		TRACE(_T("strFramePath[%s], nFrameIndex[%d], nFrameSpeed[%d]\n"), pFrameInfo->strFramePath, pFrameInfo->nFrameIndex, pFrameInfo->nFrameSpeed);
#if 0
		EffectInfo* pEffectData;
		pEffectData = new EffectInfo();
		if( pArrFrameInfo->at(i).stEffectData <= nEffectIndex)
			nEffectIndex--;

		memcpy(pEffectData, &(pArrFrameInfo->at(i).stEffectData.at(nEffectIndex++)), sizeof(EffectInfo));
#endif
		
		if(pFrameInfo->nSameFrameIdx != -1)
		{
			int nSameFrameArrIdx = pFrameInfo->nSameFrameArrIdx;

			MakeFrameInfo* pCopyFrameInfo = &(pArrFrameInfo->at(nSameFrameArrIdx));

			nImageSize = pCopyFrameInfo->nImageSize;

			if(nCurIdx != pFrameInfo->nSameFrameArrIdx && nCurIdx != -1)
			{
				if(pCopyFrameInfo->Image)
				{
					memcpy(pImg,pCopyFrameInfo->Image,nImageSize);
				}
				else
				{
					SendLog(5,_T("Image Processing Frame Error"));
				}
				nCurIdx = nSameFrameArrIdx;
			}

			if(pImg == NULL)
			{
				pImg = new BYTE[nImageSize];
				memcpy(pImg,pCopyFrameInfo->Image,nImageSize);
				nCurIdx = nSameFrameArrIdx;
			}

			if(pFrameInfo->Image == NULL)
				pFrameInfo->Image = new BYTE[nImageSize];

			pFrameInfo->nImageSize = nImageSize;
			memcpy(pFrameInfo->Image,pImg,nImageSize);

			pFrameInfo->bComplete = 1;

			ThreadFrameData *pFrameData = new ThreadFrameData;
			pFrameData->pFrameInfo = pFrameInfo;
			pFrameData->pMovieThreadManager = this;
			pFrameData->bSaveImg = bSaveImg;
			pFrameData->strPath = strDate;
			pFrameData->bReverse = bReverse;
			HANDLE hSyncTime = NULL;
			hSyncTime = (HANDLE) _beginthreadex(NULL, 0, ImageAdjustThread2, (void *)pFrameData, 0, NULL);

			//Thread Core
			//if (((CESMMovieMgr*)m_pParent)->GetModel() == (SDI_MODEL)SDI_MODEL_GH5)
			{
				if(m_nCore > MAX_CORE)
					m_nCore = 0;

				SetThreadAffinityMask(hSyncTime,1<<m_nCore);
				m_nCore++;
			}

			CloseHandle(hSyncTime);
		}
		else
		{
			ThreadFrameData *pFrameData = new ThreadFrameData;
			pFrameData->pFrameInfo = pFrameInfo;
			pFrameData->pMovieThreadManager = this;
			pFrameData->bSaveImg = bSaveImg;
			pFrameData->strPath = strDate;
			pFrameData->bReverse = bReverse;
			HANDLE hSyncTime = NULL;
			//hSyncTime = (HANDLE) _beginthreadex(NULL, 0, ImageAdjustThread2, (void *)pFrameData, 0, NULL);
			
			//if(bYUV == FALSE)
				hSyncTime = (HANDLE) _beginthreadex(NULL, 0, ImageAdjustThread2, (void *)pFrameData, 0, NULL);
			//else
			//	hSyncTime = (HANDLE) _beginthreadex(NULL, 0, ImageAdjustThread3, (void *)pFrameData, 0, NULL);
			
			//Thread Core
			//if (((CESMMovieMgr*)m_pParent)->GetModel() == (SDI_MODEL)SDI_MODEL_GH5)
			{
				if(m_nCore > MAX_CORE)
					m_nCore = 0;

				SetThreadAffinityMask(hSyncTime,1<<m_nCore);
				m_nCore++;
			}

			CloseHandle(hSyncTime);
		}
		//CString strLog;
		//strLog.Format(_T("### ImageAdjust Thread Start %d"), pFrameData->pFrameInfo->nFrameIndex);
		//SendLog(1,strLog);


		//		CString strLog;
		//		strLog.Format(_T("ImageAdjust Index %d"), i);
		//		SendLog(1,strLog);
	}
	delete[] pImg;
	pImg = NULL;
#endif
}

void CESMMovieThreadManager::EncodingAdjustMovie(vector<MakeFrameInfo>* pArrFrameInfo,ESMAdjustInfo* pInfo)
{
	FFmpegManager FFmpegMgr;
	CString strTargetFile;

	strTargetFile.Format(_T("%s\\%s_%d.mp4"),pInfo->strTargetPath ,pInfo->strDSC, pInfo->nCount);
	//CMiLRe 20160202 m:Movie 폴더 없을 경우 죽는 버그 수정
	if(!IsFileFolder(pInfo->strTargetPath))
	{
		CreateFolder(pInfo->strTargetPath);
	}
	BOOL bEncode =FALSE;
	int nOutputWidht = MOVIE_OUTPUT_FHD_WIDTH, nOutputHeight = MOVIE_OUTPUT_FHD_HEIGHT;
	if(imgproc::GetMovieSize() == ESM_MOVIESIZE_FHD)
	{
		nOutputWidht = MOVIE_OUTPUT_FHD_WIDTH;
		nOutputHeight = MOVIE_OUTPUT_FHD_HEIGHT;
	}
	else
	{
		if(imgproc::GetUHDtoFHD())
		{
			nOutputWidht = MOVIE_OUTPUT_FHD_WIDTH;
			nOutputHeight = MOVIE_OUTPUT_FHD_HEIGHT;
		}
		else
		{
			nOutputWidht = MOVIE_OUTPUT_UHD_WIDTH;
			nOutputHeight = MOVIE_OUTPUT_UHD_HEIGHT;
		}
	}
	bEncode = FFmpegMgr.InitEncode(strTargetFile, 30, AV_CODEC_ID_MPEG2VIDEO, 1, nOutputWidht, nOutputHeight);


	if(bEncode)
	{
		FFmpegManager ffmpeg;
		if(ffmpeg.CheckCudaSupport())
		{
			//If Use This function, declare an Image as IplImage, not BYTE!
			for( int i = 0; i< pArrFrameInfo->size(); i++)
			{
				MakeFrameInfo* pFrameInfo = &(pArrFrameInfo->at(i));

				int nStart = GetTickCount();

				while(1)
				{
					Sleep(1);
					if(pFrameInfo->bComplete > 0)
						break;
					if( pFrameInfo->bComplete == -100)
						break;

					int nEnd = GetTickCount();
					//10초 이상 delay 시 -100
					if(nEnd-nStart > 10000)
					{
						i = (int) pArrFrameInfo->size();
						pFrameInfo->bComplete = -100;
					}
				}

				if( pFrameInfo->bComplete == -100)
					continue;


				/*IplImage* img = (IplImage*)pFrameInfo->Image;
				cvShowImage("test", img);
				cvWaitKey(0);*/

				int nRepeatCount = pFrameInfo->nFrameSpeed/30;
				if(nRepeatCount < 1)
					nRepeatCount = 1;
				for (int i=0 ; i<nRepeatCount ; i++)
					FFmpegMgr.EncodePushImage(pFrameInfo->Image, FALSE, TRUE);

				//delete[] pFrameInfo->Image;
				//EncodingMovie();
			}

			FFmpegMgr.CloseEncode();

			MakeFrameInfo pFrameInfo;

			for( int i = 0; i< pArrFrameInfo->size(); i++)
			{
				pFrameInfo = pArrFrameInfo->at(i);
				if(pFrameInfo.pYUVImg)
				{
					delete pFrameInfo.pYUVImg;
					pFrameInfo.pYUVImg = NULL;
				}

				if(pFrameInfo.Image)
				{
					IplImage* pImg = (IplImage*)pFrameInfo.Image;
					cvReleaseImage(&pImg);

					pFrameInfo.Image = NULL;
				}	
			}

			pArrFrameInfo->clear();
			delete pArrFrameInfo;

		}
		else
		{
			for( int i = 0; i< pArrFrameInfo->size(); i++)
			{
				MakeFrameInfo* pFrameInfo = &(pArrFrameInfo->at(i));
				while(1)
				{
					Sleep(1);
					if(pFrameInfo->bComplete > 1)
						break;
					if( pFrameInfo->bComplete == -100)
						break;
				}
				if( pFrameInfo->bComplete == -100)
					continue;

				int nRepeatCount = pFrameInfo->nFrameSpeed/30;
				if(nRepeatCount < 1)
					nRepeatCount = 1;
				for (int i=0 ; i<nRepeatCount ; i++)
					FFmpegMgr.EncodePushImage(pFrameInfo->Image, FALSE);

				delete[] pFrameInfo->Image;
				//EncodingMovie();
			}
			FFmpegMgr.CloseEncode();
		}
	}
	else
	{
		{
			TRACE(_T("DoEncoding Fail.\n"));
			ESMEvent* pMsg = new ESMEvent();
			pMsg->nParam1 = ESM_ERR_MSG_FFMPEG_INIT;
			pMsg->message = WM_MAKEMOVIE_MSG;
			::SendMessage(((CESMMovieMgr*)m_pParent)->GetParentWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
		}
	}
	

	ESMEvent* pMsg = new ESMEvent();
	//pMsg->nParam1 = nMovieNum;
	pMsg->message = WM_MAKEMOVIE_DP_MOVIE_FINISH;
	::SendMessage(((CESMMovieMgr*)m_pParent)->GetParentWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
	

	/*if (nNetMode == ESM_MODESTATE_SERVER)
	{
		ESMEvent* pMsg = new ESMEvent();
		pMsg->nParam1 = nMovieNum;
		pMsg->message = WM_MAKEMOVIE_DP_MOVIE_FINISH;
		::SendMessage(((CESMMovieMgr*)m_pParent)->GetParentWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
	}
	else
	{
		ESMEvent* pMsg = new ESMEvent();
		pMsg->nParam1 = nMovieNum;
		pMsg->message = WM_ESM_NET_MAKEFRAMEMOVIEFINISHTOSERVER;
		::SendMessage(((CESMMovieMgr*)m_pParent)->GetParentWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
	}*/
	TRACE(_T("Encoding End\n"));
}

//CMiLRe 20160202 m:Movie 폴더 없을 경우 죽는 버그 수정
BOOL CESMMovieThreadManager::IsFileFolder(CString sPath)
{
	DWORD dwAttr = GetFileAttributes(sPath);
	if (dwAttr == 0xffffffff) 
		return FALSE;
	if (dwAttr & FILE_ATTRIBUTE_DIRECTORY)
		return TRUE;
	return FALSE;
}

//CMiLRe 20160202 m:Movie 폴더 없을 경우 죽는 버그 수정
void CESMMovieThreadManager::CreateFolder(CString strDestDir)
{
	CStringArray arParse;
	CString strNowDir, strCreateDir;
	strNowDir = strDestDir;
	DWORD dwAttr;

	while(1)
	{
		dwAttr = GetFileAttributes(strNowDir);
		if (dwAttr == 0xffffffff) 
		{
			CString sFolderName = strNowDir;
			int pos = sFolderName.ReverseFind('\\');
			if (pos != -1) 
			{
				sFolderName = sFolderName.Right(sFolderName.GetLength() - pos - 1);
				strNowDir .Delete(pos, strNowDir .GetLength() - pos);
			}			
			if( sFolderName == _T(""))
				break;

			arParse.Add(sFolderName);			
		}
		else
			break;
	}

	//-- CREATE NEW DIRECTORY
	int nAll = (int)arParse.GetCount ();
	while(nAll--)
	{
		strCreateDir = arParse.GetAt(nAll);
		strNowDir.Format(_T("%s\\%s"),strNowDir, strCreateDir);
		::CreateDirectory (strNowDir, NULL);
	}

	//-- 2012-05-22 hongsu
	nAll = (int)arParse.GetSize();
	while(nAll--)
		arParse.RemoveAt(nAll);
	arParse.RemoveAll();
}

void CESMMovieThreadManager::EncodingMovie(vector<MakeFrameInfo>* pArrFrameInfo, int nNetMode, int nMovieNum, CString strServerPath, BOOL bPlay)
{
	//SendLog(1, _T("Start EncodingMovie()"));
	//Test Code 
	/*for(int n = 0; n < pArrFrameInfo->size(); n++)
	{
		while(1)
		{
			Sleep(1);
			if(pArrFrameInfo->at(n).bComplete > 1)
				break;
			if(pArrFrameInfo->at(n).bComplete == -100)
				break;
		}
	}*/

	int nSize = (int) pArrFrameInfo->size();//To Send processing counting number
	//int nGPUuse;

	int nCount = 1;
	if(bPlay != -100 && (bPlay || pArrFrameInfo->at(0).bGPUPlay))
		nCount = 0;

	int nErrCnt = 0;
	BOOL bError = FALSE;

	BOOL bAJANonMaking = pArrFrameInfo->at(0).bAJAMaking;

	FFmpegManager ffmpeg;
	if((ffmpeg.CheckCudaSupport()) && pArrFrameInfo->at(0).bGPUPlay)	{
		ESMEvent* pMsg	= new ESMEvent;
		pMsg->message = WM_MAKEMOVIE_GPU_FILE_END;
		pMsg->pParam	= (LPARAM)pArrFrameInfo;
		::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
	}
	/*else if(ffmpeg.CheckCudaSupport() && bPlay != -100)
	{
		//Wait Complete
		for( int i = 0; i< pArrFrameInfo->size(); i++)
		{
			MakeFrameInfo* pFrameInfo = &(pArrFrameInfo->at(i));
			while(1)
			{
				Sleep(1);
				if(pFrameInfo->bComplete > nCount)
					break;
				if( pFrameInfo->bComplete == -100)
					break;
			}
			if( pFrameInfo->bComplete == -100)
				continue;		
		}

		ESMEvent* pMsg	= new ESMEvent;
		pMsg->message = WM_MAKEMOVIE_GPU_FILE_END;
		pMsg->pParam	= (LPARAM)pArrFrameInfo;
		::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
	}*/
	else 
	{
		FFmpegManager FFmpegMgr;
		CString strTargetFile;
		int nFrameCount = 0;
		CString strTime  = pArrFrameInfo->at(0).strTime;
		CString strNewServerPath;//Forder Path
		strNewServerPath.Format(_T("%s"),strServerPath );

		//if(bAJAOn)
		strNewServerPath.Format(_T("%s\\%s"),strServerPath ,strTime);
		//else
			//strNewServerPath.Format(_T("%s"),strServerPath);

		strTargetFile.Format(_T("%s\\%d.mp4"),strNewServerPath,nMovieNum);

		SendLog(5,strTargetFile);
		//SendLog(5, strTargetFile);

		//CMiLRe 20160202 m:Movie 폴더 없을 경우 죽는 버그 수정
		if(!IsFileFolder(strNewServerPath))
		{
			CreateFolder(strNewServerPath);
		}
		
		BOOL bEncode =FALSE;
		int nOutputWidht = MOVIE_OUTPUT_FHD_WIDTH, nOutputHeight = MOVIE_OUTPUT_FHD_HEIGHT;
		if(imgproc::GetMovieSize() == ESM_MOVIESIZE_FHD)
		{
			nOutputWidht = MOVIE_OUTPUT_FHD_WIDTH;
			nOutputHeight = MOVIE_OUTPUT_FHD_HEIGHT;
		}
		else
		{

			//VMCC UHD->UHD
			if(imgproc::GetUHDtoFHD())
			{
				nOutputWidht = MOVIE_OUTPUT_FHD_WIDTH;
				nOutputHeight = MOVIE_OUTPUT_FHD_HEIGHT;
			}
			else
			{
				nOutputWidht = MOVIE_OUTPUT_UHD_WIDTH;
				nOutputHeight = MOVIE_OUTPUT_UHD_HEIGHT;
			}
		}
#ifdef WRITER
		int nFrameRateIndex = ((CESMMovieMgr*)m_pParent)->GetFrameRateIndex();
		double dFrameRate = GetEncodingFrameRate(nFrameRateIndex);
		bEncode = FFmpegMgr.InitEncode(strTargetFile, (int)dFrameRate, AV_CODEC_ID_MPEG2VIDEO, 1, nOutputWidht, nOutputHeight);
#else
		bEncode = FFmpegMgr.SetWriter(strTargetFile,nOutputHeight,nOutputWidht);
#endif

		CString strLog;
		BOOL bYUV = TRUE;

		//metadataTest
		CESMMovieMetadataMgr *pMovieMetadataMgr;
		if(((CESMMovieMgr*)m_pParent)->GetLightWeight())
		{
			pMovieMetadataMgr = new CESMMovieMetadataMgr;
		}

		if(bEncode)
		{
			/*if(pArrFrameInfo->at(0).nDscIndex > -1 
			|| pArrFrameInfo->at(0).nPriIndex > -1 || pArrFrameInfo->at(0).nPrinumIndex > 0)
			bYUV = FALSE;
			else
			bYUV = TRUE;*/
			int nFrame = 30;
			MakeFrameInfo* pEndFrameInfo = NULL;
			for( int i = 0; i< pArrFrameInfo->size(); i++)
			{
				MakeFrameInfo* pFrameInfo = &(pArrFrameInfo->at(i));
				int nSkipCnt = 0;
				int nStart = GetTickCount();
				while(1)
				{
					Sleep(1);
					if(pFrameInfo->bComplete > nCount)
						break;
					if( pFrameInfo->bComplete == -100)
						break;

					//영상생성 딜레이발생시 처리

					//if(bPlay != -100)
					//{
					//	nSkipCnt++;
					//	if(nSkipCnt > 100 )
					//	{
					//		if(GetTickCount() - nStart > 1000 )
					//		{
					//			strLog.Format(_T("Skip Frame! [%d]"), pFrameInfo->bComplete);
					//			//AfxMessageBox(_T("Skip Frame!"));
					//			SendLog(0, strLog);
					//			pFrameInfo->bComplete = -100;
					//			break; 
					//		}
					//		else
					//			nSkipCnt = 0;
					//	}
					//}
					
				}
				if( pFrameInfo->bComplete == -100)
				{
					nErrCnt++;
					continue;
				}

				//CString strLog;
				//strLog.Format(_T("Index = %d %s [%d]"), i, pArrFrameInfo->at(i).strFramePath, pArrFrameInfo->at(i).nFrameIndex);
				//SendLog(1,strLog);
#if 0  //LogoMovie 생성 Test Code
				//Unicode CString to char*
				CString  str;           //형변환할 문자열이 저장된 CString 변수 
				str.Format(_T("C:\\Program Files\\ESMLab\\4DMaker\\img\\intro\\intro_%05d.jpg"), i);
				wchar_t* wchar_str;     //첫번째 단계(CString to wchar_t*)를 위한 변수 
				char*    char_str;      //char* 형의 변수 
				int  char_str_len;  //char* 형 변수의 길이를 위한 변수   

				//1. CString to wchar_t* conversion 
				wchar_str = str.GetBuffer(str.GetLength());   
				str.ReleaseBuffer();
				//2. wchar_t* to char* conversion 
				//char* 형에 대한길이를 구함 
				char_str_len = WideCharToMultiByte(CP_ACP, 0, wchar_str, -1, NULL, 0, NULL, NULL); 
				char_str = new char[char_str_len];  
				//메모리 할당 
				//wchar_t* to char* conversion 
				WideCharToMultiByte(CP_ACP, 0, wchar_str, -1, char_str, char_str_len, 0,0);     
				//Done. 
				IplImage* img = cvLoadImage(char_str);

				pArrFrameInfo->at(i).Image = (BYTE*)img->imageData;


				int nRepeatCount = pFrameInfo->nFrameSpeed/30;
				if(nRepeatCount < 1)
					nRepeatCount = 1;
				for (int i=0 ; i<nRepeatCount ; i++)
					FFmpegMgr.EncodePushImage(pFrameInfo->Image, FALSE);
				
#else
				//FrameRate
				//int nFrame = 30;
				if(GetModel() == SDI_MODEL_NX1)
				{
					if(GetFrameRate() == MOVIE_FPS_UHD_25P || GetFrameRate() == MOVIE_FPS_FHD_25P)
					{
						nFrame = 25;
					}else if (GetFrameRate() == MOVIE_FPS_UHD_30P || GetFrameRate() == MOVIE_FPS_FHD_30P)
					{
						nFrame = 30;
					}
					else if(GetFrameRate() == MOVIE_FPS_FHD_50P)
					{
						nFrame = 50;
					}
					else if(GetFrameRate() == MOVIE_FPS_FHD_60P)
					{
						nFrame = 60;
					}
					else if(GetFrameRate() == MOVIE_FPS_FHD_120P)
					{
						nFrame = 120;
					}
					else
					{
						nFrame = 30;
					}
				}else if(GetModel() == SDI_MODEL_GH5)
				{
					if(GetFrameRate() == MOVIE_FPS_FHD_30P || GetFrameRate() == MOVIE_FPS_UHD_30P || GetFrameRate() == MOVIE_FPS_UHD_30P_X1 || GetFrameRate() == MOVIE_FPS_UHD_30P_X2)
					{
						nFrame = 30;
					}
					else if(GetFrameRate() == MOVIE_FPS_FHD_60P || GetFrameRate() == MOVIE_FPS_UHD_60P || GetFrameRate() == MOVIE_FPS_UHD_60P_X1 || GetFrameRate() == MOVIE_FPS_UHD_60P_X2)
					{
						nFrame = 60;
					}
					else if(GetFrameRate() == MOVIE_FPS_FHD_25P || GetFrameRate() == MOVIE_FPS_UHD_25P)
					{
						nFrame = 25;
					}
					else if(GetFrameRate() == MOVIE_FPS_FHD_50P || GetFrameRate() == MOVIE_FPS_UHD_50P)
					{
						nFrame = 50;
					}
					else
					{
						nFrame = 30;
					}
				}
				else
				{
					if(GetFrameRate() == MOVIE_FPS_UHD_30P || GetFrameRate() == MOVIE_FPS_FHD_30P)
					{
						nFrame = 30;
					}else if(GetFrameRate() == MOVIE_FPS_UHD_60P || GetFrameRate() == MOVIE_FPS_FHD_60P)
					{
						nFrame = 60;
					}else if(GetFrameRate() == MOVIE_FPS_FHD_120P)
					{
						nFrame = 120;
					}else if(GetFrameRate() == MOVIE_FPS_FHD_180P)
					{
						nFrame = 180;
					}
				}

				//int nRepeatCount = pFrameInfo->nFrameSpeed/30;
				int nRepeatCount = pFrameInfo->nFrameSpeed/nFrame;
				if(nRepeatCount < 1)
					nRepeatCount = 1;

				//metadataTest											
				if(((CESMMovieMgr*)m_pParent)->GetLightWeight())
				{					
					pMovieMetadataMgr->SetFrameCountInMetadata(pFrameInfo->strDSC_ID, i, (int) pArrFrameInfo->size(), nRepeatCount);											
#ifdef WRITER
					//if(bYUV == FALSE)
						FFmpegMgr.EncodePushImage(pFrameInfo->Image, FALSE, bPlay);	
					//else
					//	FFmpegMgr.EncodePushUsingYUV(pFrameInfo->pY,pFrameInfo->pU,pFrameInfo->pV,nOutputWidht,nOutputHeight);

					pEndFrameInfo = pFrameInfo;
#else
					FFmpegMgr.PushImage(pFrameInfo->Image,nOutputHeight,nOutputWidht);
#endif
				}
				else
				{
#if 1
					EffectInfo stEffect = pFrameInfo->stEffectData;
					if( stEffect.bStepFrame &&
						stEffect.nStartZoom != stEffect.nEndZoom)
					{
						double nStartZoom = (double)stEffect.nStartZoom;
						double nEndZoom   = (double)stEffect.nEndZoom;
						//double nStep	  = (double)stEffect.nStepFrameCount;
						double nStep	  = (double)pFrameInfo->nFrameSpeed/nFrame;
						if(nStep < 2)
							nStep = 2;
						nRepeatCount = 1;

						double nIncCnt = (nEndZoom - nStartZoom) / (nStep - 1);
						int nCnt = 0;

						if(nEndZoom > nStartZoom)
						{
							for(double dbZoom = nStartZoom ; dbZoom <= nEndZoom; dbZoom+=nIncCnt)
							{
								Mat frame = DoVMCCMovie(pFrameInfo,stEffect,dbZoom,nOutputWidht,nOutputHeight);
								TRACE("[%d]ZoomRatio: %f\n",nCnt,dbZoom);
								for(int n = 0 ; n < nRepeatCount; n++)
								{
									FFmpegMgr.EncodePushImage(frame.data, FALSE, bPlay);
								}
								nCnt++;
							}
						}
						else
						{
							for(double dbZoom = nStartZoom ; dbZoom >= nEndZoom; dbZoom+=nIncCnt)
							{
								Mat frame = DoVMCCMovie(pFrameInfo,stEffect,dbZoom,nOutputWidht,nOutputHeight);
								TRACE("[%d]ZoomRatio: %f\n",nCnt,dbZoom);
								for(int n = 0 ; n < nRepeatCount; n++)
								{
									FFmpegMgr.EncodePushImage(frame.data, FALSE, bPlay);
								}
								nCnt++;
							}
						}
						if(nCnt != nStep)
						{
							double dbZoom = (double) stEffect.nEndZoom;
							Mat frame = DoVMCCMovie(pFrameInfo,stEffect,dbZoom,nOutputWidht,nOutputHeight);

							for(int n = 0 ; n < nRepeatCount; n++)
							{
								FFmpegMgr.EncodePushImage(frame.data, FALSE, bPlay);
							}
						}
						pEndFrameInfo = pFrameInfo;
					}
					else
					{
						for (int n=0 ; n<nRepeatCount ; n++)
						{
							//if(bYUV == FALSE)
							FFmpegMgr.EncodePushImage(pFrameInfo->Image, FALSE, bPlay);

							//else
							//	FFmpegMgr.EncodePushUsingYUV(pFrameInfo->pY,pFrameInfo->pU,pFrameInfo->pV,nOutputWidht,nOutputHeight);

							pEndFrameInfo = pFrameInfo;
						}
					}
#else
					for (int n=0 ; n<nRepeatCount ; n++)
					{
						//if(bYUV == FALSE)
							FFmpegMgr.EncodePushImage(pFrameInfo->Image, FALSE, bPlay);	
						//else
						//	FFmpegMgr.EncodePushUsingYUV(pFrameInfo->pY,pFrameInfo->pU,pFrameInfo->pV,nOutputWidht,nOutputHeight);
						
						pEndFrameInfo = pFrameInfo;
					}
#endif
					/*char strTempPath[200];
					sprintf(strTempPath,"M:\\Movie\\%d.jpg",i);
					Mat img(nOutputHeight,nOutputWidht,CV_8UC3,pFrameInfo->Image);
					imwrite(strTempPath,img);*/
#endif
				}

				/*for (int n=0 ; n<nRepeatCount ; n++)
				{
#ifdef WRITER
					FFmpegMgr.EncodePushImage(pFrameInfo->Image, FALSE, bPlay);	
					pEndFrameInfo = pFrameInfo;
#else
					FFmpegMgr.PushImage(pFrameInfo->Image,nOutputHeight,nOutputWidht);
#endif
				}
#endif*/

				/*if(bPlay)
				{
					IplImage* img = (IplImage*)pFrameInfo->Image;
					cvReleaseImage(&img);
				}
				else
					delete[] pFrameInfo->Image;*/


				//EncodingMovie();
			}

#ifdef WRITER
			if(pEndFrameInfo)
			{
#if 1
				EffectInfo stEffect = pEndFrameInfo->stEffectData;

				if(stEffect.bStepFrame &&
					stEffect.nStartZoom != stEffect.nEndZoom)
				{
					double dbZoom = (double) stEffect.nEndZoom;
					TRACE("[##]ZoomRatio: %f\n",dbZoom);
					Mat frame = DoVMCCMovie(pEndFrameInfo,stEffect,dbZoom,nOutputWidht,nOutputHeight);
					FFmpegMgr.EncodePushImage(frame.data, FALSE, bPlay);
				}
				else
					FFmpegMgr.EncodePushImage(pEndFrameInfo->Image, FALSE, bPlay);
				


#else
				//if(bYUV == FALSE)
					FFmpegMgr.EncodePushImage(pEndFrameInfo->Image, FALSE, bPlay);
				//else
				//	FFmpegMgr.EncodePushUsingYUV(pEndFrameInfo->pY,pEndFrameInfo->pU,pEndFrameInfo->pV,nOutputWidht,nOutputHeight);
#endif
			}
#endif
			//180305 hjcho - 로드 에러 시 에러 메세지 전송용
			if(nErrCnt == pArrFrameInfo->size())
				bError = TRUE;

			for(int i = 0; i < pArrFrameInfo->size(); i++)
			{
				MakeFrameInfo* pFrameInfo2 = &(pArrFrameInfo->at(i));
				if(bPlay && bPlay != -100)
				{
					IplImage* img = (IplImage*)pFrameInfo2->Image;
					if(img)
						cvReleaseImage(&img);
				}
				else
				{
					//if(bYUV == FALSE)
					{
						if(pFrameInfo2->Image)
							delete[] pFrameInfo2->Image;
					}
					/*else
					{
						if(pFrameInfo2->pY)
							delete[] pFrameInfo2->pY;
						if(pFrameInfo2->pU)
							delete[] pFrameInfo2->pU;
						if(pFrameInfo2->pV)
							delete[] pFrameInfo2->pV;
					}*/
				}
			}
#ifdef WRITER
			//FFmpegMgr.CloseEncode();
			//metadataTest
			if(((CESMMovieMgr*)m_pParent)->GetLightWeight())
			{
				FFmpegMgr.CloseEncode(true, pMovieMetadataMgr);
				delete pMovieMetadataMgr;
			}
			else
				FFmpegMgr.CloseEncode();
#else
			FFmpegMgr.ReleaseWriter();
#endif

		}
		else
		{
			for( int i = 0; i< pArrFrameInfo->size(); i++)
			{
				MakeFrameInfo* pFrameInfo = &(pArrFrameInfo->at(i));
				int nSkipCnt = 0;
				int nStart = GetTickCount();
				while(1)
				{
					Sleep(1);
					if(pFrameInfo->bComplete > nCount)
						break;
					if( pFrameInfo->bComplete == -100)
						break;
				}
				if( pFrameInfo->bComplete == -100)
					continue;
			}
			CString strLog;
			strLog.Format(_T("FFMPEG Init Error!!   [%s] [%dx%d]"), strTargetFile, nOutputWidht, nOutputHeight);
			SendLog(0, strLog);
			/*TRACE(_T("DoEncoding Fail.\n"));
			ESMEvent* pMsg = new ESMEvent();
			pMsg->nParam1 = ESM_ERR_MSG_FFMPEG_INIT;
			pMsg->message = WM_MAKEMOVIE_MSG;
			::SendMessage(((CESMMovieMgr*)m_pParent)->GetParentWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);*/
		}

		CString *pPath = new CString;
		pPath->Format(_T("%s"),strTime);

		if (nNetMode == ESM_MODESTATE_SERVER)
		{
			ESMEvent* pMsg = new ESMEvent();
			pMsg->nParam1 = nMovieNum;
			pMsg->nParam2 = nSize;
			pMsg->pParam  = bError;
			pMsg->pDest = (LPARAM) pPath;
			pMsg->message = WM_MAKEMOVIE_DP_MOVIE_FINISH;
			::SendMessage(((CESMMovieMgr*)m_pParent)->GetParentWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
		}
		else
		{
#ifndef _TCP_SENDER
			CString csLog;
			RequestData(strTargetFile,nMovieNum,bAJANonMaking);
#endif

			ESMEvent* pMsg = new ESMEvent();
			pMsg->nParam1 = nMovieNum;
			pMsg->nParam2 = nSize;
			pMsg->nParam3 = bError;
			pMsg->message = WM_ESM_NET_MAKEFRAMEMOVIEFINISHTOSERVER;
			pMsg->pParam = (LPARAM) pPath;
			::SendMessage(((CESMMovieMgr*)m_pParent)->GetParentWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);

		}
		TRACE(_T("Encoding End\n"));
	}
	((CESMMovieMgr*)this->m_pParent)->SetGPUState(FALSE);
}

void CESMMovieThreadManager::DoYUVtoRGB(vector<MakeFrameInfo>* pArrFrameInfo, int nIndex)
{
	MakeFrameInfo* pFrame = &pArrFrameInfo->at(nIndex);
	IplImage* img = cvCreateImage(cvSize(pFrame->nWidth , pFrame->nHeight), IPL_DEPTH_8U, 3);


	FFmpegManager ffmpeg;
	ffmpeg.ConvertYUV(pFrame->pYUVImg, (unsigned char*)img->imageData,pFrame->nWidth, pFrame->nHeight );
	int nImageSize = img->imageSize;
	pFrame->Image = new BYTE[nImageSize];
	pFrame->nImageSize = nImageSize;
	memcpy(pFrame->Image, (BYTE*)img->imageData, nImageSize);
	pFrame->bComplete = TRUE;
	CString strLog;
	strLog.Format(_T("[%d] Convert YUV Index %d \n"),GetTickCount(), nIndex);
	//SendLog(1,strLog);
	TRACE(strLog);
	cvReleaseImage(&img);
}

void CESMMovieThreadManager::DoGpuDecodingFrame(vector<MakeFrameInfo>* pArrFrameInfo, int nStartIndex, int nEndIndex, int nArrayIndex , BOOL bGPUMakeFile)
{
	int nStart = GetTickCount();
	FFmpegManager FFmpegMgr(FALSE);

	FFmpegMgr.InsertMovieDataDS(pArrFrameInfo, nStartIndex, nEndIndex, nArrayIndex, bGPUMakeFile);
	
}

void CESMMovieThreadManager::DoEncodingFrame(vector<MakeFrameInfo>* pArrFrameInfo, int nArrayStartIndex, int nStartIndex, int nEndIndex, int nFrameRate)
{
	FFmpegManager FFmpegMgr(FALSE);
	int nMovieIndex = 0, nRealStartIndex = 0, nRealEndIndex = 0;

	//180308 ESMSetIdentify() 동일
	int _nFrame = nFrameRate / 100;
	int _nDevice = nFrameRate % 100;

	/*CString strLog;
	strLog.Format(_T("Value : %d , Frame : %d , Device : %d "), nFrameRate, _nFrame, _nDevice);
	SendLog(5, strLog);*/
	
	((CESMMovieMgr*)m_pParent)->GetMovieIndex(nStartIndex, nMovieIndex, nRealStartIndex, -1, _nFrame/*nFrameRate*/);

	((CESMMovieMgr*)m_pParent)->GetMovieIndex(nEndIndex, nMovieIndex, nRealEndIndex, -1, _nFrame/*nFrameRate*/);

	//bFrameRate //jhhan170713
	FFmpegMgr.SetFrameRate(_nFrame /*nFrameRate*/);
	FFmpegMgr.SetDevice(_nDevice);

	FFmpegMgr.InsertMovieDataDS(pArrFrameInfo, nRealStartIndex, nEndIndex, nArrayStartIndex);
	//종료시점
}

int CESMMovieThreadManager::ImageAdjust(LPVOID param)
{	
	ThreadFrameData *pFrameData = (ThreadFrameData*)param;
	BOOL bSaveImg = FALSE;
	BOOL bReverse = FALSE;
	CString strPath;
	bSaveImg = pFrameData->bSaveImg;
	strPath = pFrameData->strPath;
	bReverse = pFrameData->bReverse;
	MakeFrameInfo* pFrameInfo = pFrameData->pFrameInfo ;
	CESMMovieThreadManager *pMovieThreadManager = pFrameData->pMovieThreadManager;
	delete pFrameData;
	EffectInfo pEffectData = pFrameInfo->stEffectData;

	//WaitForSingleObject(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, INFINITE);
	int nSrcWidth = 0, nSrcHeight = 0, nOutputWidth = 0, nOutputHeight = 0 ;

	//Output 형식에 따른 데이터 변경.
	imgproc::GetMovieSizeData(nSrcWidth, nSrcHeight, nOutputWidth, nOutputHeight);
	
	CString strDscId;
	CString strFramePath = pFrameInfo->strFramePath;
	int nIndex = strFramePath.ReverseFind('\\') + 1;
	CString strTpPath = strFramePath.Right(strFramePath.GetLength() - nIndex);
	strDscId = strTpPath.Left(5);
	stAdjustInfo AdjustData = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetAdjustData(strDscId);
		
	
	if(nSrcHeight == 1080 || nSrcWidth == 1920)
		TRACE(_T("==============Image Data Size Height = %d Width = %d\n"), nSrcHeight, nSrcWidth);

	int m_imagesize;
	m_imagesize = nSrcHeight*nSrcWidth*3;
	if(pFrameInfo->nImageSize != m_imagesize)
	{
		if(pFrameInfo->nImageSize == 1920 * 1080 * 3)
		{
			nSrcWidth = 1920;
			nSrcHeight = 1080;
		}
		else if(pFrameInfo->nImageSize == 3840 * 2160 * 3)
		{
			nSrcWidth = 3840;
			nSrcHeight = 2160;
		}
		m_imagesize = nSrcHeight*nSrcWidth*3;
	}

	Mat src(nSrcHeight, nSrcWidth, CV_8UC3);
	

	if(pFrameInfo->nImageSize != m_imagesize)
	{
		CString strMsg;
		strMsg.Format(_T("%s  record mode error"), pFrameInfo->strCameraID);
		pFrameInfo->bComplete++;
		//ReleaseSemaphore(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, 1, NULL);
		return 0;
	}
	else
	{
		FFmpegManager ffmpeg;
		if(ffmpeg.CheckCudaSupport())
			src = cvarrToMat(pFrameInfo->Image);
		else
			memcpy(src.data, pFrameInfo->Image, pFrameInfo->nImageSize);
	}

	if(!imgproc::GetVMCCFlag())
	{	
		if(nOutputWidth != nSrcWidth)
		{
			Mat d_resize;
			resize(src, d_resize, Size(nOutputWidth, nOutputHeight), 0,0,INTER_CUBIC );
			d_resize.copyTo(src);
		}
		//Cpu Rotate
		int nRotateX = 0, nRotateY = 0, nSize = 0;
		double dSize = 0.0;
		double dRatio = (double)nOutputWidth / (double)nSrcWidth;
		nRotateX = imgproc::Round(AdjustData.AdjptRotate.x  * dRatio );	 // 반올림
		nRotateY = imgproc::Round(AdjustData.AdjptRotate.y  * dRatio );
		imgproc::CpuRotateImage(src, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle,0);

		//Cpu Move
		int nMoveX = 0, nMoveY = 0;
		nMoveX = imgproc::Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
		nMoveY = imgproc::Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
		imgproc::CpuMoveImage(&src, nMoveX,  nMoveY);

		//Cpu Move
		int nMarginX = (int) (AdjustData.stMargin.nMarginX * dRatio);//((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginX() * dRatio;
		int nMarginY = (int) (AdjustData.stMargin.nMarginY * dRatio);//((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginY() * dRatio;
		if ( nMarginX < 0 || nMarginX >= nSrcWidth/2 || nMarginY < 0 || nMarginY >= nSrcHeight/2  )
		{
			//ReleaseSemaphore(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, 1, NULL);
			TRACE(_T("Image Adjust Error 2\n"));
			pFrameInfo->bComplete++;
			return 0;
		}
		if(src.cols > nOutputWidth)
		{
			int nLeft = (int)((src.cols - nOutputWidth)/2);
			int nTop  = (int)((src.rows - nOutputHeight)/2);
			Mat pMatCut =  (src)(cv::Rect(nLeft, nTop, nOutputWidth, nOutputHeight));
			pMatCut.copyTo(src);
		}
		else
		{
			//imgproc::CpuMakeMargin(&src,nMarginX,nMarginY);

			double dbMarginScale = 1 / ( (double)( nOutputWidth - nMarginX * 2) /(double) nOutputWidth  );	
			Mat  pMatResize;
			resize(src, pMatResize, cv::Size((int)(nOutputWidth*dbMarginScale) ,(int)(nOutputHeight * dbMarginScale) ), 0.0, 0.0 ,INTER_CUBIC);
			int nLeft = (int)((nOutputWidth*dbMarginScale - nOutputWidth)/2);
			int nTop = (int)((nOutputHeight*dbMarginScale - nOutputHeight)/2);
			Mat pMatCut =  (pMatResize)(cv::Rect(nLeft, nTop, nOutputWidth, nOutputHeight));
			pMatCut.copyTo(src);
			pMatResize = NULL;
		}
		if(bReverse)
		{
			nRotateX = src.cols / 2;
			nRotateY = src.rows / 2;
			imgproc::CpuRotateImage(src, nRotateX ,nRotateY, 1, AdjustData.AdjAngle, bReverse);
		}
		Mat srcImage = src.clone();
		//Insert Logo

#if 0	//-- 할리데이비슨 테스트 코드
		if( 1)
#else
		if( pFrameInfo->nDscIndex > -1)
#endif
		
		{
			// insert Logo 구현
			imgproc::GpuInsertLogo2(&srcImage, pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
		}
		if( pFrameInfo->nPriIndex > -1 || pFrameInfo->nPrinumIndex > 0)
		{
			// insert Logo 구현
			imgproc::KzonePrism(&srcImage,pFrameInfo->nPriIndex,pFrameInfo->nPriValue,pFrameInfo->nPriValue2,pFrameInfo->nPrinumIndex);
		}

#if 1 //-- 카메라 ID 표시
		if( pFrameInfo->strCameraID[0] != _T('\0'))
			imgproc::GpuInsertCameraID(&srcImage,pFrameInfo->nPriIndex,pFrameInfo->strCameraID,pFrameInfo->strDSC_IP,pFrameInfo->bPrnColorInfo);
#endif


		delete[] pFrameInfo->Image;
		int size = (int) (srcImage.total() * srcImage.elemSize());
		pFrameInfo->Image = new BYTE[size];
		memcpy( pFrameInfo->Image, srcImage.data, size);
		//delete pImageMat;
		pFrameInfo->bComplete++;

		//BMP Save
		if(bSaveImg)
		{
			IplImage*	img = new IplImage(srcImage);
			CString strData , strTemp;
			char path[MAX_PATH];
			strData.Format(_T("%s"), strPath);
			CreateDirectory(strData,NULL);
			strTemp.Format(_T("\\%s_%d_%d.bmp"), strDscId, nIndex, pFrameInfo->nFrameIndex);
			strData.Append(strTemp);
			sprintf(path, "%S", strData);
			cvSaveImage(path, img);
		}


		//ReleaseSemaphore(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, 1, NULL);
	}
	else if(imgproc::GetVMCCFlag() && nSrcWidth == FULL_HD_WIDTH && nSrcHeight == FULL_HD_HEIGHT)
	{
		int nPreWidth = nOutputWidth;
		int nPreHeight = nOutputHeight;

		nOutputWidth = nSrcWidth;
		nOutputHeight = nSrcHeight;

		//Cpu Rotate
		int nRotateX = 0, nRotateY = 0, nSize = 0;
		double dSize = 0.0;
		double dRatio = (double)nOutputWidth / (double)nSrcWidth;
		nRotateX = imgproc::Round(AdjustData.AdjptRotate.x  * dRatio );	 // 반올림
		nRotateY = imgproc::Round(AdjustData.AdjptRotate.y  * dRatio );
		imgproc::CpuRotateImage(src, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle,0);

		//Cpu Move
		int nMoveX = 0, nMoveY = 0;
		nMoveX = imgproc::Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
		nMoveY = imgproc::Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
		imgproc::CpuMoveImage(&src, nMoveX,  nMoveY);

		//Cpu Margin
		int nMarginX = (int) (AdjustData.stMargin.nMarginX * dRatio);//((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginX() * dRatio;
		int nMarginY = (int) (AdjustData.stMargin.nMarginY * dRatio);//((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginY() * dRatio;
		if ( nMarginX < 0 || nMarginX >= nSrcWidth/2 || nMarginY < 0 || nMarginY >= nSrcHeight/2  )
		{
			//ReleaseSemaphore(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, 1, NULL);
			TRACE(_T("Image Adjust Error 2\n"));
			pFrameInfo->bComplete++;
			return 0;
		}

		if( src.cols > nOutputWidth)	
		{
			int nModifyMarginX = (src.cols - nOutputWidth) /2;
			int nModifyMarginY = (src.rows - nOutputHeight) /2;
			Mat pMatCut =  (src)(Rect(nModifyMarginX, nModifyMarginY, nOutputWidth, nOutputHeight));
			src = pMatCut.clone();
		}
		else
		{
			//imgproc::CpuMakeMargin(&src,nMarginX,nMarginY);

			double dbMarginScale = 1 / ( (double)( nOutputWidth - nMarginX * 2) /(double) nOutputWidth  );	
			Mat  pMatResize;
			resize(src, pMatResize, cv::Size((int) (nOutputWidth*dbMarginScale) , (int) (nOutputHeight * dbMarginScale) ), 0.0, 0.0 ,INTER_CUBIC);
			int nLeft = (int)((nOutputWidth*dbMarginScale - nOutputWidth)/2);
			int nTop = (int)((nOutputHeight*dbMarginScale - nOutputHeight)/2);
			Mat pMatCut =  (pMatResize)(cv::Rect(nLeft, nTop, nOutputWidth, nOutputHeight));
			pMatCut.copyTo(src);
			pMatResize = NULL;
		}

		if(pFrameInfo->bLogoZoom)
		{
//#ifdef LOGO_ZOOM //Logo Zoom 전
			if( pFrameInfo->nDscIndex > -1)
			{
				Mat srcLogo(src.rows, src.cols, CV_8UC3);
				srcLogo = src.clone();
				// insert Logo 구현
				imgproc::GpuInsertLogo2(&srcLogo, pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
				src = srcLogo.clone();
			}
			if( pFrameInfo->nPriIndex > -1 || pFrameInfo->nPrinumIndex > 0)
			{
				Mat srcPrism(src.rows, src.cols, CV_8UC3);
				srcPrism = srcPrism.clone();
				imgproc::KzonePrism(&srcPrism,pFrameInfo->nPriIndex,pFrameInfo->nPriValue,pFrameInfo->nPriValue2,pFrameInfo->nPrinumIndex);
				src = srcPrism.clone();
			}
//#endif
		}

		double dScale = (double)pEffectData.nZoomRatio / 100.0;

		if(dScale == 0)
		{
			//Template 오류
			pFrameInfo->bComplete++;
			TRACE(_T("Template Error"));
			return 0;
		}

		Mat MatCut, MatResize;

		int nLeft = (int) (pEffectData.nPosX- nSrcWidth/dScale/2);
		int nTop = (int) (pEffectData.nPosY - nSrcHeight/dScale/2);
		int nWidth = (int) (nSrcWidth/dScale - 1);
		if((nLeft + nWidth) > nSrcWidth)
			nLeft = nLeft - ((nLeft + nWidth) - nSrcWidth);
		else if(nLeft < 0)
			nLeft = 0;
		int nHeight = (int) (nSrcHeight/dScale - 1);
		if((nTop + nHeight) > nSrcHeight)
			nTop = nTop - ((nTop + nHeight) - nSrcHeight);
		else if (nTop < 0)
			nTop = 0;
		MatCut =  (src)(Rect(nLeft, nTop, nWidth, nHeight));
		src = MatCut.clone();

		////VMCC FHD->FHD
		resize(src, MatResize, Size(nOutputWidth ,nOutputHeight), 0.0, 0.0, INTER_CUBIC );
		MatResize.copyTo(src);


		////-- Reverse Movie
		if(bReverse)
		{
			nRotateX = src.cols / 2;
			nRotateY = src.rows / 2;
			imgproc::CpuRotateImage(src, nRotateX ,nRotateY, 1, AdjustData.AdjAngle, bReverse);
		}

		Mat srcImage = src.clone();

		if(!pFrameInfo->bLogoZoom)
		{
//#ifndef LOGO_ZOOM //Logo Zoom 후
			//Insert Logo
			if( pFrameInfo->nDscIndex > -1)
			{
				// insert Logo 구현
				imgproc::GpuInsertLogo2(&srcImage, pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
			}
//#endif
		}


#if 1 //-- 카메라 ID 표시
		if( pFrameInfo->strCameraID[0] != _T('\0'))
			imgproc::GpuInsertCameraID(&srcImage,pFrameInfo->nPriIndex,pFrameInfo->strCameraID,pFrameInfo->strDSC_IP,pFrameInfo->bPrnColorInfo);
#endif

		delete[] pFrameInfo->Image;
		int size = (int) (srcImage.total() * srcImage.elemSize());
		pFrameInfo->Image = new BYTE[size];
		memcpy( pFrameInfo->Image, srcImage.data, size);
		//delete pImageMat;
		pFrameInfo->bComplete++;

		//BMP Save
		if(bSaveImg)
		{
			IplImage*	img = new IplImage(srcImage);
			CString strData , strTemp;
			char path[MAX_PATH];
			strData.Format(_T("%s"), strPath);
			CreateDirectory(strData,NULL);
			strTemp.Format(_T("\\%s_%d_%d.bmp"), strDscId, nIndex, pFrameInfo->nFrameIndex);
			strData.Append(strTemp);
			sprintf(path, "%S", strData);
			cvSaveImage(path, img);
		}

		//ReleaseSemaphore(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, 1, NULL);
	}
	else
	{
		int nPreWidth = nOutputWidth;
		int nPreHeight = nOutputHeight;

		nOutputWidth = nSrcWidth;
		nOutputHeight = nSrcHeight;

		//Cpu Rotate
		int nRotateX = 0, nRotateY = 0, nSize = 0;
		double dSize = 0.0;
		double dRatio = (double)nOutputWidth / (double)nSrcWidth;
		nRotateX = imgproc::Round(AdjustData.AdjptRotate.x  * dRatio );	 // 반올림
		nRotateY = imgproc::Round(AdjustData.AdjptRotate.y  * dRatio );
		imgproc::CpuRotateImage(src, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle,0);

		//Cpu Move
		int nMoveX = 0, nMoveY = 0;
		nMoveX = imgproc::Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
		nMoveY = imgproc::Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
		imgproc::CpuMoveImage(&src, nMoveX,  nMoveY);

		//Cpu Margin
		int nMarginX = (int) (AdjustData.stMargin.nMarginX * dRatio);//((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginX() * dRatio;
		int nMarginY = (int) (AdjustData.stMargin.nMarginY * dRatio);//((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginY() * dRatio;
		if ( nMarginX < 0 || nMarginX >= nSrcWidth/2 || nMarginY < 0 || nMarginY >= nSrcHeight/2  )
		{
			//ReleaseSemaphore(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, 1, NULL);
			TRACE(_T("Image Adjust Error 2\n"));
			pFrameInfo->bComplete++;
			return 0;
		}

		if( src.cols > nOutputWidth)	
		{
			int nModifyMarginX = (src.cols - nOutputWidth) /2;
			int nModifyMarginY = (src.rows - nOutputHeight) /2;
			Mat pMatCut =  (src)(Rect(nModifyMarginX, nModifyMarginY, nOutputWidth, nOutputHeight));
			src = pMatCut.clone();
		}
		else
		{
			//imgproc::CpuMakeMargin(&src,nMarginX,nMarginY);

			double dbMarginScale = 1 / ( (double)( nOutputWidth - nMarginX * 2) /(double) nOutputWidth  );	
			Mat  pMatResize;
			resize(src, pMatResize, cv::Size((int)(nOutputWidth*dbMarginScale) , (int)(nOutputHeight * dbMarginScale) ), 0.0, 0.0 ,INTER_CUBIC);
			int nLeft = (int)((nOutputWidth*dbMarginScale - nOutputWidth)/2);
			int nTop = (int)((nOutputHeight*dbMarginScale - nOutputHeight)/2);
			Mat pMatCut =  (pMatResize)(cv::Rect(nLeft, nTop, nOutputWidth, nOutputHeight));
			pMatCut.copyTo(src);
			pMatResize = NULL;
		}

		if(pFrameInfo->bLogoZoom)
		{
//#ifdef LOGO_ZOOM //Logo Zoom 전
			if( pFrameInfo->nDscIndex > -1)
			{
				Mat srcLogo(src.rows, src.cols, CV_8UC3);
				srcLogo = src.clone();
				// insert Logo 구현
				imgproc::GpuInsertLogo2(&srcLogo, pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
				src = srcLogo.clone();
			}
			if( pFrameInfo->nPriIndex > -1 || pFrameInfo->nPrinumIndex > 0)
			{
				Mat srcPrism(src.rows, src.cols, CV_8UC3);
				srcPrism = srcPrism.clone();
				imgproc::KzonePrism(&srcPrism,pFrameInfo->nPriIndex,pFrameInfo->nPriValue,pFrameInfo->nPriValue2,pFrameInfo->nPrinumIndex);
				src = srcPrism.clone();
			}
//#endif
		}
		
		double dScale = (double)pEffectData.nZoomRatio / 100.0;

		if(dScale == 0)
		{
			//Template 오류
			TRACE(_T("Template Error"));
			return 0;
		}

		Mat MatCut, MatResize;

		int nLeft = (int) (pEffectData.nPosX- nSrcWidth/dScale/2);
		int nTop = (int) (pEffectData.nPosY - nSrcHeight/dScale/2);
		int nWidth = (int) (nSrcWidth/dScale - 1);
		if((nLeft + nWidth) > nSrcWidth)
			nLeft = nLeft - ((nLeft + nWidth) - nSrcWidth);
		else if(nLeft < 0)
			nLeft = 0;
		int nHeight = (int) (nSrcHeight/dScale - 1);
		if((nTop + nHeight) > nSrcHeight)
			nTop = nTop - ((nTop + nHeight) - nSrcHeight);
		else if (nTop < 0)
			nTop = 0;
		MatCut =  (src)(Rect(nLeft, nTop, nWidth, nHeight));
		src = MatCut.clone();

		if(imgproc::GetUHDtoFHD())
		{
			resize(src, MatResize, Size(nPreWidth ,nPreHeight), 0.0, 0.0, INTER_CUBIC );
			MatResize.copyTo(src);
		}
		else
		{
			resize(src, MatResize, Size(nOutputWidth ,nOutputHeight), 0.0, 0.0, INTER_CUBIC );
			MatResize.copyTo(src);
		}
		


		////-- Reverse Movie
		if(bReverse)
		{
			nRotateX = src.cols / 2;
			nRotateY = src.rows/ 2;
			imgproc::CpuRotateImage(src, nRotateX ,nRotateY, 1, AdjustData.AdjAngle, bReverse);
		}

		Mat srcImage = src.clone();
#if 1 //-- 카메라 ID 표시
		if( pFrameInfo->strCameraID[0] != _T('\0'))
			imgproc::GpuInsertCameraID(&srcImage,pFrameInfo->nPriIndex,pFrameInfo->strCameraID,pFrameInfo->strDSC_IP,pFrameInfo->bPrnColorInfo);
#endif

		if(!pFrameInfo->bLogoZoom)
		{
//#ifndef LOGO_ZOOM //Logo Zoom 후
			//Insert Logo
			if( pFrameInfo->nDscIndex > -1)
			{
				// insert Logo 구현
				imgproc::GpuInsertLogo2(&srcImage, pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
			}
//#endif
		}




		delete[] pFrameInfo->Image;
		int size = (int) (srcImage.total() * srcImage.elemSize());
		pFrameInfo->Image = new BYTE[size];
		memcpy( pFrameInfo->Image, srcImage.data, size);
		//delete pImageMat;
		pFrameInfo->bComplete++;

		//BMP Save
		if(bSaveImg)
		{
			IplImage*	img = new IplImage(srcImage);
			CString strData , strTemp;
			char path[MAX_PATH];
			strData.Format(_T("%s"), strPath);
			CreateDirectory(strData,NULL);
			strTemp.Format(_T("\\%s_%d_%d.bmp"), strDscId, nIndex, pFrameInfo->nFrameIndex);
			strData.Append(strTemp);
			sprintf(path, "%S", strData);
			cvSaveImage(path, img);
		}

		//ReleaseSemaphore(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, 1, NULL);
	}
	
	return 0;
}

//unsigned WINAPI CESMMovieThreadManager::ImageAdjustThread2(LPVOID param)
//{	
//	ThreadFrameData *pFrameData = (ThreadFrameData*)param;
//	BOOL bSaveImg = FALSE;
//	BOOL bReverse = FALSE;
//	CString strPath;
//	bSaveImg = pFrameData->bSaveImg;
//	strPath = pFrameData->strPath;
//	bReverse = pFrameData->bReverse;
//	MakeFrameInfo* pFrameInfo = pFrameData->pFrameInfo ;
//	CESMMovieThreadManager *pMovieThreadManager = pFrameData->pMovieThreadManager;
//	delete pFrameData;
//	EffectInfo pEffectData = pFrameInfo->stEffectData;
//
//	WaitForSingleObject(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, INFINITE);
//	int nSrcWidth = 0, nSrcHeight = 0, nOutputWidth = 0, nOutputHeight = 0 ;
//
//	//Output 형식에 따른 데이터 변경.
//	imgproc::GetMovieSizeData(nSrcWidth, nSrcHeight, nOutputWidth, nOutputHeight);
//
//	//Movie Size Check
//	if(nOutputWidth > nSrcWidth || nOutputHeight > nSrcHeight)
//	{
//		nOutputHeight = nSrcHeight;
//		nOutputWidth = nSrcWidth;
//	}
//	
//	CString strDscId;
//	CString strFramePath = pFrameInfo->strFramePath;
//	int nIndex = strFramePath.ReverseFind('\\') + 1;
//	CString strTpPath = strFramePath.Right(strFramePath.GetLength() - nIndex);
//	strDscId = strTpPath.Left(5);
//	stAdjustInfo AdjustData = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetAdjustData(strDscId);
//	
//	
//	if(AdjustData.AdjAngle == 0 && AdjustData.AdjMove.x == 0 && AdjustData.AdjMove.y == 0)
//	{
//		CString strLog;
//		strLog.Format(_T("[%s] Cannot Load Adjust"),strDscId);
//		SendLog(5,strLog);
//	}
//	if(nSrcHeight == 1080 || nSrcWidth == 1920)
//		TRACE(_T("==============Image Data Size Height = %d Width = %d\n"), nSrcHeight, nSrcWidth);
//
//	int m_imagesize;
//	m_imagesize = nSrcHeight*nSrcWidth*3;
//	if(pFrameInfo->nImageSize != m_imagesize)
//	{
//		if(pFrameInfo->nImageSize == 1920 * 1080 * 3)
//		{
//			nSrcWidth = 1920;
//			nSrcHeight = 1080;
//		}
//		else if(pFrameInfo->nImageSize == 3840 * 2160 * 3)
//		{
//			nSrcWidth = 3840;
//			nSrcHeight = 2160;
//		}
//		m_imagesize = nSrcHeight*nSrcWidth*3;
//	}
//
//	Mat src(nSrcHeight, nSrcWidth, CV_8UC3);
//	
//
//	if(pFrameInfo->nImageSize != m_imagesize)
//	{
//		CString strMsg;
//		strMsg.Format(_T("%s  record mode error"), pFrameInfo->strCameraID);
//		//pFrameInfo->bComplete++;
//		pFrameInfo->bComplete = -100;
//		SendLog(0, strMsg);
//		ReleaseSemaphore(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, 1, NULL);
//		return 0;
//	}
//	else
//	{
//		memcpy(src.data, pFrameInfo->Image, pFrameInfo->nImageSize);
//	}
//
//	int nRotCenterX = src.cols/2;
//	int nRotCenterY = src.rows/2;
//	if(pFrameInfo->bMovieReverse)
//	{
//		//flip(src,src,-1);
//		imgproc::CpuRotateImage(src,nRotCenterX,nRotCenterY,1.,1,TRUE);
//	}
//
////	//memcpy(src.data, pFrameInfo->Image, m_imagesize);//pFrameInfo->nImageSize);
////	cuda::GpuMat* pImageMat = new cuda::GpuMat;
////	pImageMat->upload(src);
////
//////-- ROTATE EVENT 
////	if( pImageMat == NULL)
////	{
////		TRACE(_T("Image Adjust Error 1\n"));
////		pFrameInfo->bComplete++;
////		ReleaseSemaphore(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, 1, NULL);
////		return 0;
////	}
//	if(pFrameInfo->bColorRevision)
//	{
//		FrameRGBInfo stColorInfo = pFrameInfo->stColorInfo;
//		imgproc::DoColorRevision(&src,stColorInfo);
//	}
//
//	if(!imgproc::GetVMCCFlag())
//	{	
//#if 0
//		//-- IMAGE RESIZE
//		if(nOutputWidth != nSrcWidth)
//		{
//			cuda::GpuMat d_resize;
//			cuda::resize(*pImageMat, d_resize, Size(nOutputWidth, nOutputHeight), 0,0,INTER_CUBIC );
//			d_resize.copyTo(*pImageMat);
//
//		}
//		// Rotate 구현
//		//SendLog(1,_T("[%d] GPU Rotate start!"),cnt);
//		int nRotateX = 0, nRotateY = 0, nSize = 0;
//		double dSize = 0.0;
//		double dRatio = (double)nOutputWidth / (double)nSrcWidth;
//		nRotateX = imgproc::Round(AdjustData.AdjptRotate.x  * dRatio );	 // 반올림
//		nRotateY = imgproc::Round(AdjustData.AdjptRotate.y  * dRatio );
//		imgproc::GpuRotateImage(pImageMat, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle);
//		//SendLog(1,_T("[%d] GPU Rotate end!"),cnt);
//
//
//		//-- MOVE EVENT
//		//SendLog(1,_T("[%d] GPU Move start!"),cnt);
//		int nMoveX = 0, nMoveY = 0;
//		nMoveX = imgproc::Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
//		nMoveY = imgproc::Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
//		imgproc::GpuMoveImage(pImageMat, nMoveX,  nMoveY);
//		//SendLog(1,_T("[%d] GPU Move end!"),cnt);
//
//
//		//-- IMAGE CUT EVENT
//		int nMarginX = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginX() * dRatio;
//		int nMarginY = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginY() * dRatio;
//		if ( nMarginX < 0 || nMarginX >= nSrcWidth/2 || nMarginY < 0 || nMarginY >= nSrcHeight/2  )
//		{
//			ReleaseSemaphore(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, 1, NULL);
//			TRACE(_T("Image Adjust Error 2\n"));
//			pFrameInfo->bComplete++;
//			return 0;
//		}
//		
//		if( pImageMat->cols > nOutputWidth)	
//		{
//			int nModifyMarginX = (pImageMat->cols - nOutputWidth) /2;
//			int nModifyMarginY = (pImageMat->rows - nOutputHeight) /2;
//			cuda::GpuMat pMatCut =  (*pImageMat)(Rect(nModifyMarginX, nModifyMarginY, nOutputWidth, nOutputHeight));
//			pMatCut.copyTo(*pImageMat);
//		}
//		else
//		{
//			//SendLog(1,_T("[%d] GPU Matgin start!"),cnt);
//			//imgproc::GpuMakeMargin(pImageMat, nMarginX, nMarginY);
//			//SendLog(1,_T("[%d] GPU Matgin end!"),cnt);
//
//
//			double dbMarginScale = 1 / ( (double)( nOutputWidth - nMarginX * 2) /(double) nOutputWidth  );
//			cuda::GpuMat* pMatResize = new cuda::GpuMat;
//			//SendLog(1,_T("[%d]GPU Resize start!"),cnt);
//			cuda::resize(*pImageMat, *pMatResize, Size(nOutputWidth*dbMarginScale ,nOutputHeight * dbMarginScale ), 0.0, 0.0 ,INTER_CUBIC );
//			//SendLog(1,_T("[%d]GPU Resize end!"),cnt);
//			int nLeft = (int)((nOutputWidth*dbMarginScale - nOutputWidth)/2);
//			int nTop = (int)((nOutputHeight*dbMarginScale - nOutputHeight)/2);
//			cuda::GpuMat pMatCut =  (*pMatResize)(Rect(nLeft, nTop, nOutputWidth, nOutputHeight));
//			pMatCut.copyTo(*pImageMat);
//			delete pMatResize;
//		}
//
//		//-- Reverse Movie
//		if(bReverse)
//		{
//			nRotateX = pImageMat->cols / 2;
//			nRotateY = pImageMat->rows / 2;
//			imgproc::GpuRotateImage(pImageMat, nRotateX ,nRotateY, 1, AdjustData.AdjAngle, bReverse);
//		}
//
//		Mat srcImage(pImageMat->rows, pImageMat->cols, CV_8UC3);
//		pImageMat->download(srcImage);
//		//imshow("srcImage",srcImage);
//		//waitKey(0);
//#else
//		if(nOutputWidth != nSrcWidth)
//		{
//			Mat d_resize;
//			resize(src, d_resize, Size(nOutputWidth, nOutputHeight), 0,0,INTER_CUBIC );
//			d_resize.copyTo(src);
//		}
//		//Cpu Rotate
//		int nRotateX = 0, nRotateY = 0, nSize = 0;
//		double dSize = 0.0;
//		double dRatio = (double)nOutputWidth / (double)nSrcWidth;
//		nRotateX = imgproc::Round(AdjustData.AdjptRotate.x  * dRatio );	 // 반올림
//		nRotateY = imgproc::Round(AdjustData.AdjptRotate.y  * dRatio );
//		imgproc::CpuRotateImage(src, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle,0);
//
//		//Cpu Move
//		int nMoveX = 0, nMoveY = 0;
//		nMoveX = imgproc::Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
//		nMoveY = imgproc::Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
//		imgproc::CpuMoveImage(&src, nMoveX,  nMoveY);
//
//		//Cpu Move
//		int nMarginX = AdjustData.stMargin.nMarginX * dRatio;//((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginX() * dRatio;
//		int nMarginY = AdjustData.stMargin.nMarginY * dRatio;//((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginY() * dRatio;
//		if ( nMarginX < 0 || nMarginX >= nSrcWidth/2 || nMarginY < 0 || nMarginY >= nSrcHeight/2  )
//		{
//			ReleaseSemaphore(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, 1, NULL);
//			TRACE(_T("Image Adjust Error 2\n"));
//			pFrameInfo->bComplete++;
//			return 0;
//		}
//		if(src.cols > nOutputWidth)
//		{
//			int nLeft = (int)((src.cols - nOutputWidth)/2);
//			int nTop  = (int)((src.rows - nOutputHeight)/2);
//			Mat pMatCut =  (src)(cv::Rect(nLeft, nTop, nOutputWidth, nOutputHeight));
//			pMatCut.copyTo(src);
//		}
//		else
//		{
//			//imgproc::CpuMakeMargin(&src,nMarginX,nMarginY);
//
//			double dbMarginScale = 1 / ( (double)( nOutputWidth - nMarginX * 2) /(double) nOutputWidth  );	
//			Mat  pMatResize;
//			resize(src, pMatResize, cv::Size(nOutputWidth*dbMarginScale ,nOutputHeight * dbMarginScale ), 0.0, 0.0 ,INTER_CUBIC);
//			int nLeft = (int)((nOutputWidth*dbMarginScale - nOutputWidth)/2);
//			int nTop = (int)((nOutputHeight*dbMarginScale - nOutputHeight)/2);
//			Mat pMatCut =  (pMatResize)(cv::Rect(nLeft, nTop, nOutputWidth, nOutputHeight));
//			pMatCut.copyTo(src);
//			pMatResize = NULL;
//		}
//		if(bReverse)
//		{
//			nRotateX = src.cols / 2;
//			nRotateY = src.rows / 2;
//			imgproc::CpuRotateImage(src, nRotateX ,nRotateY, 1, AdjustData.AdjAngle, bReverse);
//		}
//		Mat srcImage = src.clone();
//		//Insert Logo
//
//#endif
//
//#if 0	//-- 할리데이비슨 테스트 코드
//		if( 1)
//#else
//		if( pFrameInfo->nDscIndex > -1)
//#endif	
//		{
//			// insert Logo 구현
//			imgproc::GpuInsertLogo2(&srcImage, pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
//		}
//		if( pFrameInfo->nPriIndex > -1 || pFrameInfo->nPrinumIndex > 0)
//		{
//			// insert Logo 구현
//			int taValue = pFrameInfo->nPriValue;
//			int saValue = pFrameInfo->nPriValue2;
//			imgproc::KzonePrism(&srcImage,pFrameInfo->nPriIndex,taValue,saValue,pFrameInfo->nPrinumIndex);
//		}
//
//#if 1 //-- 카메라 ID 표시
//		if( pFrameInfo->strCameraID[0] != _T('\0'))
//			imgproc::GpuInsertCameraID(&srcImage,pFrameInfo->nPriIndex,pFrameInfo->strCameraID,pFrameInfo->strDSC_IP,pFrameInfo->bPrnColorInfo);
//#endif
//
//		if(pFrameInfo->Image)
//			delete[] pFrameInfo->Image;
//
//		if(pFrameInfo->bInsertWB)
//		{
//			Ptr<xphoto::WhiteBalancer> wb;
//			wb = xphoto::createGrayworldWB();
//			wb->balanceWhite(srcImage, srcImage);
//		}
//#ifndef INSERT_TEXT
//		char strText[MAX_PATH];
//		sprintf(strText,"%d_%d",pFrameInfo->nSecIdx,pFrameInfo->nFrameIndex);
//		putText(srcImage,strText,cv::Point(1920,1080),CV_FONT_HERSHEY_COMPLEX,3,Scalar(0,0,0),1,8);
//#endif
//
//		int size = srcImage.total() * srcImage.elemSize();
//		pFrameInfo->Image = new BYTE[size];
//		memcpy( pFrameInfo->Image, srcImage.data, size);
//		//delete pImageMat;
//		pFrameInfo->bComplete++;
//
//		//BMP Save
//		if(bSaveImg)
//		{
//			IplImage*	img = new IplImage(srcImage);
//			CString strData , strTemp;
//			char path[MAX_PATH];
//			strData.Format(_T("%s"), strPath);
//			CreateDirectory(strData,NULL);
//			strTemp.Format(_T("\\%s_%d_%d.bmp"), strDscId, nIndex, pFrameInfo->nFrameIndex);
//			strData.Append(strTemp);
//			sprintf(path, "%S", strData);
//			cvSaveImage(path, img);
//		}
//
//
//		ReleaseSemaphore(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, 1, NULL);
//	}
//	else if(imgproc::GetVMCCFlag() && nSrcWidth == FULL_HD_WIDTH && nSrcHeight == FULL_HD_HEIGHT)
//	{
//#if 0
//		int nPreWidth = nOutputWidth;
//		int nPreHeight = nOutputHeight;
//
//		nOutputWidth = nSrcWidth;
//		nOutputHeight = nSrcHeight;
//
//		// Rotate 구현
//		int nRotateX = 0, nRotateY = 0, nSize = 0;
//		double dSize = 0.0;
//		double dRatio = (double)nOutputWidth / (double)nSrcWidth;
//		nRotateX = imgproc::Round(AdjustData.AdjptRotate.x  * dRatio );	 // 반올림
//		nRotateY = imgproc::Round(AdjustData.AdjptRotate.y  * dRatio );
//		imgproc::GpuRotateImage(pImageMat, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle);
//
//		//-- MOVE EVENT
//		int nMoveX = 0, nMoveY = 0;
//		nMoveX = imgproc::Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
//		nMoveY = imgproc::Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
//		imgproc::GpuMoveImage(pImageMat, nMoveX,  nMoveY);
//
//		//-- IMAGE CUT EVENT
//		int nMarginX = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginX() * dRatio;
//		int nMarginY = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginY() * dRatio;
//		if ( nMarginX < 0 || nMarginX >= nSrcWidth/2 || nMarginY < 0 || nMarginY >= nSrcHeight/2  )
//		{
//			ReleaseSemaphore(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, 1, NULL);
//			TRACE(_T("Image Adjust Error 2\n"));
//			pFrameInfo->bComplete++;
//			return 0;
//		}
//
//		if( pImageMat->cols > nOutputWidth)	
//		{
//			int nModifyMarginX = (pImageMat->cols - nOutputWidth) /2;
//			int nModifyMarginY = (pImageMat->rows - nOutputHeight) /2;
//			cuda::GpuMat pMatCut =  (*pImageMat)(Rect(nModifyMarginX, nModifyMarginY, nOutputWidth, nOutputHeight));
//			pMatCut.copyTo(*pImageMat);
//		}
//		else
//		{
//			//imgproc::GpuMakeMargin(pImageMat, nMarginX, nMarginY);
//			double dbMarginScale = 1 / ( (double)( nOutputWidth - nMarginX * 2) /(double) nOutputWidth  );
//			cuda::GpuMat* pMatResize = new cuda::GpuMat;
//			cuda::resize(*pImageMat, *pMatResize, Size(nOutputWidth*dbMarginScale ,nOutputHeight * dbMarginScale ), 0.0, 0.0 ,INTER_CUBIC );
//			int nLeft = (int)((nOutputWidth*dbMarginScale - nOutputWidth)/2);
//			int nTop = (int)((nOutputHeight*dbMarginScale - nOutputHeight)/2);
//			cuda::GpuMat pMatCut =  (*pMatResize)(Rect(nLeft, nTop, nOutputWidth, nOutputHeight));
//			pMatCut.copyTo(*pImageMat);
//			delete pMatResize;
//		}
//
//		if(pFrameInfo->bLogoZoom)
//		{
////#ifdef LOGO_ZOOM //Logo Zoom 전
//			if( pFrameInfo->nDscIndex > -1)
//			{
//				Mat srcLogo(pImageMat->rows, pImageMat->cols, CV_8UC3);
//				pImageMat->download(srcLogo);
//				// insert Logo 구현
//				imgproc::GpuInsertLogo2(&srcLogo, pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
//				pImageMat->upload(srcLogo);
//			}
//			if( pFrameInfo->nPriIndex > -1 || pFrameInfo->nPrinumIndex > 0)
//			{
//				Mat srcPrism(pImageMat->rows, pImageMat->cols, CV_8UC3);
//				pImageMat->download(srcPrism);
//				imgproc::KzonePrism(&srcPrism,pFrameInfo->nPriIndex,pFrameInfo->nPriValue,pFrameInfo->nPriValue2,pFrameInfo->nPrinumIndex);
//				pImageMat->upload(srcPrism);
//			}
////#endif
//		}
//
//		//Zoom Move
//		double dScale = (double)pEffectData.nZoomRatio / 100.0;
//
//		if(dScale == 0)
//		{
//			//Template 오류
//			ReleaseSemaphore(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, 1, NULL);
//			TRACE(_T("Template Error"));
//			return 0;
//		}
//
//		cuda::GpuMat gMatCut, gMatResize;
//
//		int nLeft = (pEffectData.nPosX- nSrcWidth/dScale/2);
//		int nTop = (pEffectData.nPosY - nSrcHeight/dScale/2);
//		int nWidth = (nSrcWidth/dScale - 1);
//		if((nLeft + nWidth) > nSrcWidth)
//			nLeft = nLeft - ((nLeft + nWidth) - nSrcWidth);
//		else if(nLeft < 0)
//			nLeft = 0;
//		int nHeight = (nSrcHeight/dScale - 1);
//		if((nTop + nHeight) > nSrcHeight)
//			nTop = nTop - ((nTop + nHeight) - nSrcHeight);
//		else if (nTop < 0)
//			nTop = 0;
//		gMatCut =  (*pImageMat)(Rect(nLeft, nTop, nWidth, nHeight));
//		gMatCut.copyTo(*pImageMat);
//
//		//VMCC FHD->FHD
//		cuda::resize(*pImageMat, gMatResize, Size(nOutputWidth ,nOutputHeight), 0.0, 0.0, INTER_CUBIC );
//
//		gMatResize.copyTo(*pImageMat);
//
//		
//		//-- Reverse Movie
//		if(bReverse)
//		{
//			nRotateX = pImageMat->cols / 2;
//			nRotateY = pImageMat->rows / 2;
//			imgproc::GpuRotateImage(pImageMat, nRotateX ,nRotateY, 1, AdjustData.AdjAngle, bReverse);
//		}
//
//		Mat srcImage(pImageMat->rows, pImageMat->cols, CV_8UC3);
//		pImageMat->download(srcImage);
//#else
//		int nPreWidth = nOutputWidth;
//		int nPreHeight = nOutputHeight;
//
//		nOutputWidth = nSrcWidth;
//		nOutputHeight = nSrcHeight;
//
//		//wgkim 181004 K-Zone
//		CESMMovieKZoneMgr* pMovieKZoneMgr = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMovieKZoneMgr();
//		pMovieKZoneMgr->DoDrawKZone(strDscId, src);
//
//		//wgkim 180513
//		if(pFrameInfo->nVertical == 0)
//		{
//			//Cpu Move
//			int nMoveX = 0, nMoveY = 0;
//			nMoveX = imgproc::Round(AdjustData.AdjMove.x  );	 // 반올림
//			nMoveY = imgproc::Round(AdjustData.AdjMove.y  );	 // 반올림
//			imgproc::CpuMoveImage(&src, nMoveX,  nMoveY);
//
//			//Cpu Rotate
//			int nRotateX = 0, nRotateY = 0, nSize = 0;
//			double dSize = 0.0;
//			double dRatio = (double)nOutputWidth / (double)nSrcWidth;
//			nRotateX = imgproc::Round((AdjustData.AdjMove.x+AdjustData.AdjptRotate.x) * dRatio );	 // 반올림
//			nRotateY = imgproc::Round((AdjustData.AdjMove.y+AdjustData.AdjptRotate.y) * dRatio );
//			imgproc::CpuRotateImage(src, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle,0);
//
//			//Cpu Margin
//			int nMarginX = AdjustData.stMargin.nMarginX * dRatio;//((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginX() * dRatio;
//			int nMarginY = AdjustData.stMargin.nMarginY * dRatio;//((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginY() * dRatio;
//			if ( nMarginX < 0 || nMarginX >= nSrcWidth/2 || nMarginY < 0 || nMarginY >= nSrcHeight/2  )
//			{
//				ReleaseSemaphore(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, 1, NULL);
//				TRACE(_T("Image Adjust Error 2\n"));
//				pFrameInfo->bComplete++;
//				return 0;
//			}
//
//			if( src.cols > nOutputWidth)	
//			{
//				int nModifyMarginX = (src.cols - nOutputWidth) /2;
//				int nModifyMarginY = (src.rows - nOutputHeight) /2;
//				Mat pMatCut =  (src)(Rect(nModifyMarginX, nModifyMarginY, nOutputWidth, nOutputHeight));
//				src = pMatCut.clone();
//			}
//			else
//			{
//				CRect rtMargin = AdjustData.rtMargin;
//
//				if(	rtMargin.top == 0 &&
//					rtMargin.left == 0 &&
//					rtMargin.right == 0 &&
//					rtMargin.bottom == 0)
//				{
//					double dbMarginScale = 1 / ( (double)( nOutputWidth - nMarginX * 2) /(double) nOutputWidth  );	
//					Mat  pMatResize;
//					resize(src, pMatResize, cv::Size(nOutputWidth*dbMarginScale ,nOutputHeight * dbMarginScale ), 0.0, 0.0 ,INTER_CUBIC);
//					int nLeft = (int)((nOutputWidth*dbMarginScale - nOutputWidth)/2);
//					int nTop = (int)((nOutputHeight*dbMarginScale - nOutputHeight)/2);
//					Mat pMatCut =  (pMatResize)(cv::Rect(nLeft, nTop, nOutputWidth, nOutputHeight));
//					pMatCut.copyTo(src);
//					pMatResize = NULL;
//				}
//				else
//				{
//					Mat pMatCut =  src(Rect(
//						rtMargin.left, 
//						rtMargin.top, 
//						rtMargin.right-rtMargin.left, 
//						rtMargin.bottom-rtMargin.top
//						));
//					resize(pMatCut, pMatCut, cv::Size(nOutputWidth, nOutputHeight), 0.0, 0.0 ,INTER_CUBIC);
//					pMatCut.copyTo(src);
//				}
//			}
//
//			if(pFrameInfo->bLogoZoom)
//			{
//				//#ifdef LOGO_ZOOM //Logo Zoom 전
//				if( pFrameInfo->nDscIndex > -1)
//				{
//					Mat srcLogo(src.rows, src.cols, CV_8UC3);
//					srcLogo = src.clone();
//					// insert Logo 구현
//					imgproc::GpuInsertLogo2(&srcLogo, pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
//					src = srcLogo.clone();
//				}
//				if( pFrameInfo->nPriIndex > -1 || pFrameInfo->nPrinumIndex > 0)
//				{
//					//형준 확인요망
//					/*Mat srcPrism(src.rows, src.cols, CV_8UC3);
//					srcPrism = src.clone();
//					int taValue = pFrameInfo->nPriValue;
//					int saValue = pFrameInfo->nPriValue1;
//					imgproc::KzonePrism(&srcPrism,pFrameInfo->nPriIndex,taValue,saValue,pFrameInfo->nPrinumIndex);
//					src = srcPrism.clone();*/
//				}
//				//#endif
//			}
//			if(bReverse)
//			{
//				imgproc::CpuRotateImage(src,nRotCenterX,nRotCenterY,1,AdjustData.AdjAngle,TRUE);
//				//flip(src,src,-1);
//				//pEffectData.nPosX = nSrcWidth - pEffectData.nPosX;
//				//pEffectData.nPosY = nSrcHeight - pEffectData.nPosY;
//			}
//
//			if( pEffectData.bStepFrame == TRUE &&
//				pEffectData.nStartZoom != pEffectData.nEndZoom)
//			{
//				pFrameInfo->nWidth = nSrcWidth;
//				pFrameInfo->nHeight = nSrcHeight;
//				memcpy(pFrameInfo->Image,src.data,nSrcWidth*nSrcHeight*3);
//				pFrameInfo->bComplete++;
//				return TRUE;
//			}
//
//			double dScale = (double)pEffectData.nZoomRatio / 100.0;
//
//			if(dScale == 0)
//			{
//				//Template 오류
//				//ReleaseSemaphore(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, 1, NULL);
//				//TRACE(_T("Template Error"));
//				//pFrameInfo->bComplete = -100;
//				dScale = 1;
//				SendLog(5,_T("Template Error"));
//				//return 0;
//			}
//			if(pEffectData.nPosX == 0 && pEffectData.nPosY == 0)
//			{
//				pEffectData.nPosX = nSrcWidth/2;
//				pEffectData.nPosY = nSrcHeight/2;
//			}
//
//			Mat MatCut;
//
//			int nLeft = (pEffectData.nPosX- nSrcWidth/dScale/2);
//			int nTop = (pEffectData.nPosY - nSrcHeight/dScale/2);
//			int nWidth = (nSrcWidth/dScale - 1);
//			if((nLeft + nWidth) > nSrcWidth)
//				nLeft = nLeft - ((nLeft + nWidth) - nSrcWidth);
//			else if(nLeft < 0)
//				nLeft = 0;
//			int nHeight = (nSrcHeight/dScale - 1);
//			if((nTop + nHeight) > nSrcHeight)
//				nTop = nTop - ((nTop + nHeight) - nSrcHeight);
//			else if (nTop < 0)
//				nTop = 0;
//			MatCut =  (src)(Rect(nLeft, nTop, nWidth, nHeight));
//			src = MatCut.clone();
//		}
//		else
//		{
//			////wgkim 180513 Vertical 변환 코드
//			//if(bReverse)
//			//{
//			//	flip(src,src,-1);
//			//}
//			//double degree = 0;
//			//double resizeRatio = (16./9);
//
//			//Mat rotateImage(src.cols, src.rows, CV_8UC3);
//			//if(pFrameInfo->nVertical == -1)
//			//{
//			//	transpose(src, rotateImage);
//			//	flip(rotateImage, rotateImage, 1);
//			//}
//
//			//else if(pFrameInfo->nVertical == 1)
//			//{
//			//	transpose(src, rotateImage);
//			//	flip(rotateImage, rotateImage, 0);
//			//}
//
//			//Point2f ptVerticalCenterPoint;
//			//ptVerticalCenterPoint.x = pFrameInfo->ptVerticalCenterPoint.x;
//			//ptVerticalCenterPoint.y = pFrameInfo->ptVerticalCenterPoint.y;
//
//			//Rect2d rtRoiRect;
//			//rtRoiRect.width = src.rows;
//			//rtRoiRect.height = src.rows* (9./16);
//			//rtRoiRect.x = 0;
//			//rtRoiRect.y = (src.cols * (ptVerticalCenterPoint.y / src.rows)) - (rtRoiRect.height/2);
//
//			//rotateImage(rtRoiRect).copyTo(src);
//			//wgkim 180513 Vertical 변환 코드
//			if(bReverse)
//			{
//				flip(src,src,-1);
//			}
//
//			double degree = 0;
//			double resizeRatio = (16./9);
//
//			Mat rotateImage(src.cols, src.rows, CV_8UC3);
//			if(pFrameInfo->nVertical == -1)
//			{
//				transpose(src, rotateImage);
//				flip(rotateImage, rotateImage, 1);
//			}
//
//			else if(pFrameInfo->nVertical == 1)
//			{
//				transpose(src, rotateImage);
//				flip(rotateImage, rotateImage, 0);
//			}
//
//			Point2f ptVerticalCenterPoint;
//			ptVerticalCenterPoint.x = pFrameInfo->ptVerticalCenterPoint.x;
//			ptVerticalCenterPoint.y = pFrameInfo->ptVerticalCenterPoint.y;
//
//			Rect2d rtRoiRect;
//			rtRoiRect.width = src.rows;
//			rtRoiRect.height = src.rows* (9./16);
//			rtRoiRect.x = 0;
//			rtRoiRect.y = (src.cols * (ptVerticalCenterPoint.y / src.rows)) - (rtRoiRect.height/2);
//
//			if(rtRoiRect.y < 0)
//				rtRoiRect.y = 0;
//			if(rtRoiRect.y + rtRoiRect.height > src.cols)
//				rtRoiRect.y = src.cols - rtRoiRect.height-1;
//			rotateImage(rtRoiRect).copyTo(src);
//
//			Mat MatCut;
//
//			//Template/////////////////////
//			//////////////
//			//resize(src, src, Size(nSrcWidth, nSrcHeight), CV_INTER_LINEAR);
//
//			double dScale = (double)pEffectData.nZoomRatio / 100.0;
//
//			pFrameInfo->nWidth = src.cols;
//			pFrameInfo->nHeight= src.rows;
//
//			//180524
//			ptVerticalCenterPoint.x *= (double)src.cols/nSrcWidth;
//			ptVerticalCenterPoint.y = (double)src.rows/2;
//
//			//180525
//			double nWidthBoundRatio_Left =  ((double)src.cols/2) - ((double)src.cols/2) * pow((double)src.rows/src.cols, 2);
//			double nWidthBoundRatio_Right = ((double)src.cols/2) + ((double)src.cols/2) * pow((double)src.rows/src.cols, 2);
//
//			if( ptVerticalCenterPoint.x < nWidthBoundRatio_Left)
//				ptVerticalCenterPoint.x = nWidthBoundRatio_Left;
//			if( ptVerticalCenterPoint.x > nWidthBoundRatio_Right)
//				ptVerticalCenterPoint.x = nWidthBoundRatio_Right;
//
//			ptVerticalCenterPoint.x = 
//				abs(ptVerticalCenterPoint.x - nWidthBoundRatio_Left) * 
//				((double)src.cols/(nWidthBoundRatio_Right- nWidthBoundRatio_Left));
//
//			if(dScale == 0)
//			{
//				dScale = 1;
//				SendLog(5,_T("Vertical Error"));
//			}
//
//			if(pEffectData.nPosX == 0 && pEffectData.nPosY == 0)
//			{
//				pEffectData.nPosX = src.cols * (ptVerticalCenterPoint.x/src.cols);//nSrcWidth/2;
//				pEffectData.nPosY = src.rows/2;
//			}
//			pEffectData.nPosX = src.cols * (ptVerticalCenterPoint.x/src.cols);//nSrcWidth/2;
//			pEffectData.nPosY = ptVerticalCenterPoint.y;
//
//			if( pEffectData.bStepFrame == TRUE &&
//				pEffectData.nStartZoom != pEffectData.nEndZoom)
//			{
//				pFrameInfo->nWidth = src.cols;
//				pFrameInfo->nHeight= src.rows;
//				memcpy(pFrameInfo->Image,src.data,src.cols*src.rows*3);
//				pFrameInfo->bComplete++;
//
//				//wgkim 180528
//				pFrameInfo->stEffectData.nPosX = pEffectData.nPosX;
//				pFrameInfo->stEffectData.nPosY = pEffectData.nPosY;
//
//				return TRUE;
//			}
//
//			int nLeft = (pEffectData.nPosX- src.cols/dScale/2);
//			int nTop = (pEffectData.nPosY - src.rows/dScale/2);
//			int nWidth = (src.cols/dScale - 1);
//			if((nLeft + nWidth) > src.cols)
//				nLeft = nLeft - ((nLeft + nWidth) - src.cols);
//			else if(nLeft < 0)
//				nLeft = 0;
//
//			int nHeight = (src.rows/dScale - 1);
//			if((nTop + nHeight) > src.rows)
//				nTop = nTop - ((nTop + nHeight) - src.rows);
//			else if (nTop < 0)
//				nTop = 0;
//			MatCut =  (src)(Rect(nLeft, nTop, nWidth, nHeight));
//			src = MatCut.clone();
//
//			//wgkim 180528
//			pFrameInfo->stEffectData.nPosX = pEffectData.nPosX;
//			pFrameInfo->stEffectData.nPosY = pEffectData.nPosY;
//			//////////////////////////////////////////////////////////
//		}
//		Mat MatResize;
//
//		////VMCC FHD->FHD
//		resize(src, MatResize, Size(nOutputWidth ,nOutputHeight), 0.0, 0.0, INTER_CUBIC );
//		MatResize.copyTo(src);
//		
//
//		////-- Reverse Movie
//		/*if(bReverse)
//		{
//			nRotateX = src.cols / 2;
//			nRotateY = src.rows / 2;
//			imgproc::CpuRotateImage(src, nRotateX ,nRotateY, 1, AdjustData.AdjAngle, bReverse);
//		}*/
//
//		Mat srcImage = src.clone();
//#endif
//
//		if(!pFrameInfo->bLogoZoom)
//		{
////#ifndef LOGO_ZOOM //Logo Zoom 후
//			//Insert Logo
//			if( pFrameInfo->nDscIndex > -1)
//			{
//				// insert Logo 구현
//				imgproc::GpuInsertLogo2(&srcImage, pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
//			}
////#endif
//		}
//
//
//#if 1 //-- 카메라 ID 표시
//		if( pFrameInfo->strCameraID[0] != _T('\0'))
//			imgproc::GpuInsertCameraID(&srcImage,pFrameInfo->nPriIndex,pFrameInfo->strCameraID,pFrameInfo->strDSC_IP,pFrameInfo->bPrnColorInfo);
//#endif
//		if(pFrameInfo->bInsertWB)
//		{
//			Ptr<xphoto::WhiteBalancer> wb;
//			wb = xphoto::createGrayworldWB();
//			wb->balanceWhite(srcImage, srcImage);
//		}
//
//		delete[] pFrameInfo->Image;
//		pFrameInfo->Image = NULL;
//
//		int size = srcImage.total() * srcImage.elemSize();
//		pFrameInfo->Image = new BYTE[size];
//
//#ifndef INSERT_TEXT
//		char strText[MAX_PATH];
//		sprintf(strText,"%d_%d",pFrameInfo->nSecIdx,pFrameInfo->nFrameIndex);
//		putText(srcImage,strText,cv::Point(960,450),CV_FONT_HERSHEY_COMPLEX,3,Scalar(0,0,0),1,8);
//#endif
//		memcpy( pFrameInfo->Image, srcImage.data, size);
//		//delete pImageMat;
//		pFrameInfo->bComplete++;
//
//		//BMP Save
//		if(bSaveImg)
//		{
//			IplImage*	img = new IplImage(srcImage);
//			CString strData , strTemp;
//			char path[MAX_PATH];
//			strData.Format(_T("%s"), strPath);
//			CreateDirectory(strData,NULL);
//			strTemp.Format(_T("\\%s_%d_%d.bmp"), strDscId, nIndex, pFrameInfo->nFrameIndex);
//			strData.Append(strTemp);
//			sprintf(path, "%S", strData);
//			cvSaveImage(path, img);
//		}
//
//		ReleaseSemaphore(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, 1, NULL);
//		/*//-- IMAGE RESIZE
//		if(nOutputWidth != nSrcWidth)
//		{
//			cuda::GpuMat d_resize;
//			cuda::resize(*pImageMat, d_resize, Size(nOutputWidth, nOutputHeight), 0,0,INTER_CUBIC );
//			d_resize.copyTo(*pImageMat);
//
//		}		
//
//		// Rotate 구현
//		int nRotateX = 0, nRotateY = 0, nSize = 0;
//		double dSize = 0.0;
//		double dRatio = (double)nOutputWidth / (double)nSrcWidth;
//		nRotateX = imgproc::Round(AdjustData.AdjptRotate.x  * dRatio );	 // 반올림
//		nRotateY = imgproc::Round(AdjustData.AdjptRotate.y  * dRatio );
//		imgproc::GpuRotateImage(pImageMat, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle);
//
//		//-- MOVE EVENT
//		int nMoveX = 0, nMoveY = 0;
//		nMoveX = imgproc::Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
//		nMoveY = imgproc::Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
//		imgproc::GpuMoveImage(pImageMat, nMoveX,  nMoveY);
//
//		//-- IMAGE CUT EVENT
//		int nMarginX = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginX() * dRatio;
//		int nMarginY = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginY() * dRatio;
//		if ( nMarginX < 0 || nMarginX >= nSrcWidth/2 || nMarginY < 0 || nMarginY >= nSrcHeight/2  )
//		{
//			ReleaseSemaphore(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, 1, NULL);
//			TRACE(_T("Image Adjust Error 2\n"));
//			pFrameInfo->bComplete++;
//			return 0;
//		}
//
//		if( pImageMat->cols > nOutputWidth)	
//		{
//			int nModifyMarginX = (pImageMat->cols - nOutputWidth) /2;
//			int nModifyMarginY = (pImageMat->rows - nOutputHeight) /2;
//			cuda::GpuMat pMatCut =  (*pImageMat)(Rect(nModifyMarginX, nModifyMarginY, nOutputWidth, nOutputHeight));
//			pMatCut.copyTo(*pImageMat);
//		}
//		else
//		{
//			//imgproc::GpuMakeMargin(pImageMat, nMarginX, nMarginY);
//			double dbMarginScale = 1 / ( (double)( nOutputWidth - nMarginX * 2) /(double) nOutputWidth  );
//			cuda::GpuMat* pMatResize = new cuda::GpuMat;
//			cuda::resize(*pImageMat, *pMatResize, Size(nOutputWidth*dbMarginScale ,nOutputHeight * dbMarginScale ), 0.0, 0.0 ,INTER_CUBIC );
//			int nLeft = (int)((nOutputWidth*dbMarginScale - nOutputWidth)/2);
//			int nTop = (int)((nOutputHeight*dbMarginScale - nOutputHeight)/2);
//			cuda::GpuMat pMatCut =  (*pMatResize)(Rect(nLeft, nTop, nOutputWidth, nOutputHeight));
//			pMatCut.copyTo(*pImageMat);
//			delete pMatResize;
//		}
//
//		//-- Reverse Movie
//		if(bReverse)
//		{
//			nRotateX = pImageMat->cols / 2;
//			nRotateY = pImageMat->rows / 2;
//			imgproc::GpuRotateImage(pImageMat, nRotateX ,nRotateY, 1, AdjustData.AdjAngle, bReverse);
//		}
//
//		Mat srcImage(pImageMat->rows, pImageMat->cols, CV_8UC3);
//		pImageMat->download(srcImage);
//
//		//Insert Logo
//		if( pFrameInfo->nDscIndex > -1)
//		{
//			// insert Logo 구현
//			imgproc::GpuInsertLogo2(&srcImage, pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
//		}
//
//		delete[] pFrameInfo->Image;
//		int size = srcImage.total() * srcImage.elemSize();
//		pFrameInfo->Image = new BYTE[size];
//		memcpy( pFrameInfo->Image, srcImage.data, size);
//		delete pImageMat;
//		pFrameInfo->bComplete++;
//		ReleaseSemaphore(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, 1, NULL);*/
//	}
//	else
//	{
//#if 0
//		int nPreWidth = nOutputWidth;
//		int nPreHeight = nOutputHeight;
//
//		nOutputWidth = nSrcWidth;
//		nOutputHeight = nSrcHeight;
//
//		// Rotate 구현
//		int nRotateX = 0, nRotateY = 0, nSize = 0;
//		double dSize = 0.0;
//		double dRatio = (double)nOutputWidth / (double)nSrcWidth;
//		nRotateX = imgproc::Round(AdjustData.AdjptRotate.x  * dRatio );	 // 반올림
//		nRotateY = imgproc::Round(AdjustData.AdjptRotate.y  * dRatio );
//		imgproc::GpuRotateImage(pImageMat, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle);
//
//		//-- MOVE EVENT
//		int nMoveX = 0, nMoveY = 0;
//		nMoveX = imgproc::Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
//		nMoveY = imgproc::Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
//		imgproc::GpuMoveImage(pImageMat, nMoveX,  nMoveY);
//
//		//-- IMAGE CUT EVENT
//		int nMarginX = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginX() * dRatio;
//		int nMarginY = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginY() * dRatio;
//		if ( nMarginX < 0 || nMarginX >= nSrcWidth/2 || nMarginY < 0 || nMarginY >= nSrcHeight/2  )
//		{
//			ReleaseSemaphore(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, 1, NULL);
//			TRACE(_T("Image Adjust Error 2\n"));
//			pFrameInfo->bComplete++;
//			return 0;
//		}
//
//		if( pImageMat->cols > nOutputWidth)	
//		{
//			int nModifyMarginX = (pImageMat->cols - nOutputWidth) /2;
//			int nModifyMarginY = (pImageMat->rows - nOutputHeight) /2;
//			cuda::GpuMat pMatCut =  (*pImageMat)(Rect(nModifyMarginX, nModifyMarginY, nOutputWidth, nOutputHeight));
//			pMatCut.copyTo(*pImageMat);
//		}
//		else
//		{
//			//imgproc::GpuMakeMargin(pImageMat, nMarginX, nMarginY);
//			double dbMarginScale = 1 / ( (double)( nOutputWidth - nMarginX * 2) /(double) nOutputWidth  );
//			cuda::GpuMat* pMatResize = new cuda::GpuMat;
//			cuda::resize(*pImageMat, *pMatResize, Size(nOutputWidth*dbMarginScale ,nOutputHeight * dbMarginScale ), 0.0, 0.0 ,INTER_CUBIC );
//			int nLeft = (int)((nOutputWidth*dbMarginScale - nOutputWidth)/2);
//			int nTop = (int)((nOutputHeight*dbMarginScale - nOutputHeight)/2);
//			cuda::GpuMat pMatCut =  (*pMatResize)(Rect(nLeft, nTop, nOutputWidth, nOutputHeight));
//			pMatCut.copyTo(*pImageMat);
//			delete pMatResize;
//		}
//
//		if(!pFrameInfo->bLogoZoom)
//		{
////#ifdef LOGO_ZOOM //Logo Zoom 전
//			if( pFrameInfo->nDscIndex > -1)
//			{
//				Mat srcLogo(pImageMat->rows, pImageMat->cols, CV_8UC3);
//				pImageMat->download(srcLogo);
//				// insert Logo 구현
//				imgproc::GpuInsertLogo2(&srcLogo, pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
//				pImageMat->upload(srcLogo);
//			}
//			if( pFrameInfo->nPriIndex > -1 || pFrameInfo->nPrinumIndex > 0)
//			{
//				Mat srcLogo(pImageMat->rows, pImageMat->cols, CV_8UC3);
//				pImageMat->download(srcLogo);
//				int taValue = pFrameInfo->nPriValue;
//				int saValue = pFrameInfo->nPriValue;
//				imgproc::KzonePrism(&srcPrism,pFrameInfo->nPriIndex,pFrameInfo->nPriValue,pFrameInfo->nPriValue2,pFrameInfo->nPrinumIndex);
//			}
////#endif
//		}
//
//
//		//Zoom Move
//		double dScale = (double)pEffectData.nZoomRatio / 100.0;
//
//		if(dScale == 0)
//		{
//			SendLog(5,_T("Zoom value Error"));
//			dScale = 1;
//		}
//
//		cuda::GpuMat gMatCut, gMatResize;
//
//		int nLeft = (pEffectData.nPosX- nSrcWidth/dScale/2);
//		int nTop = (pEffectData.nPosY - nSrcHeight/dScale/2);
//		int nWidth = (nSrcWidth/dScale - 1);
//		if((nLeft + nWidth) > nSrcWidth)
//			nLeft = nLeft - ((nLeft + nWidth) - nSrcWidth);
//		else if(nLeft < 0)
//			nLeft = 0;
//		int nHeight = (nSrcHeight/dScale - 1);
//		if((nTop + nHeight) > nSrcHeight)
//			nTop = nTop - ((nTop + nHeight) - nSrcHeight);
//		else if (nTop < 0)
//			nTop = 0;
//		gMatCut =  (*pImageMat)(Rect(nLeft, nTop, nWidth, nHeight));
//		gMatCut.copyTo(*pImageMat);
//
//		//VMCC UHD->UHD
//		if(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetUHDtoFHD())
//			cuda::resize(*pImageMat, gMatResize, Size(nPreWidth ,nPreHeight), 0.0, 0.0, INTER_CUBIC );
//		else
//			cuda::resize(*pImageMat, gMatResize, Size(nOutputWidth ,nOutputHeight), 0.0, 0.0, INTER_CUBIC );
//
//		gMatResize.copyTo(*pImageMat);
//
//		//
//		//-- Reverse Movie
//		if(bReverse)
//		{
//			nRotateX = pImageMat->cols / 2;
//			nRotateY = pImageMat->rows / 2;
//			imgproc::GpuRotateImage(pImageMat, nRotateX ,nRotateY, 1, AdjustData.AdjAngle, bReverse);
//		}
//
//		Mat srcImage(pImageMat->rows, pImageMat->cols, CV_8UC3);
//		pImageMat->download(srcImage);
//
//#if 1 //-- 카메라 ID 표시
//		if( pFrameInfo->strCameraID[0] != _T('\0'))
//			imgproc::GpuInsertCameraID(&srcImage,pFrameInfo->nPriIndex,pFrameInfo->strCameraID,pFrame->strDSC_IP,pFrameInfo->bPrnColorInfo);
//#endif
//
//#else
//		int nPreWidth = nOutputWidth;
//		int nPreHeight = nOutputHeight;
//
//		nOutputWidth = nSrcWidth;
//		nOutputHeight = nSrcHeight;
//
//		//wgkim 181004 K-Zone
//		CESMMovieKZoneMgr* pMovieKZoneMgr = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMovieKZoneMgr();
//		pMovieKZoneMgr->DoDrawKZone(strDscId, src);
//
//		//wgkim 180513
//		if(pFrameInfo->nVertical == 0)
//		{
//			//Cpu Move
//			int nMoveX = 0, nMoveY = 0;
//			nMoveX = imgproc::Round(AdjustData.AdjMove.x);	 // 반올림
//			nMoveY = imgproc::Round(AdjustData.AdjMove.y);	 // 반올림
//			imgproc::CpuMoveImage(&src, nMoveX,  nMoveY);
//
//			//Cpu Rotate
//			int nRotateX = 0, nRotateY = 0, nSize = 0;
//			double dSize = 0.0;
//			double dRatio = (double)nOutputWidth / (double)nSrcWidth;
//			nRotateX = imgproc::Round((AdjustData.AdjMove.x+AdjustData.AdjptRotate.x) * dRatio );	 // 반올림
//			nRotateY = imgproc::Round((AdjustData.AdjMove.y+AdjustData.AdjptRotate.y) * dRatio );
//			imgproc::CpuRotateImage(src, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle,0);
//
//			//Cpu Margin
//			int nMarginX = AdjustData.stMargin.nMarginX * dRatio;//((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginX() * dRatio;
//			int nMarginY = AdjustData.stMargin.nMarginY * dRatio;//((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginY() * dRatio;
//			if ( nMarginX < 0 || nMarginX >= nSrcWidth/2 || nMarginY < 0 || nMarginY >= nSrcHeight/2  )
//			{
//				ReleaseSemaphore(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, 1, NULL);
//				TRACE(_T("Image Adjust Error 2\n"));
//				pFrameInfo->bComplete++;
//				return 0;
//			}
//
//			if( src.cols > nOutputWidth)
//			{
//				int nModifyMarginX = (src.cols - nOutputWidth) /2;
//				int nModifyMarginY = (src.rows - nOutputHeight) /2;
//				Mat pMatCut =  (src)(Rect(nModifyMarginX, nModifyMarginY, nOutputWidth, nOutputHeight));
//				src = pMatCut.clone();
//			}
//			else
//			{
//				CRect rtMargin = AdjustData.rtMargin;
//
//				if(	rtMargin.top == 0 &&
//					rtMargin.left == 0 &&
//					rtMargin.right == 0 &&
//					rtMargin.bottom == 0)
//				{
//					double dbMarginScale = 1 / ( (double)( nOutputWidth - nMarginX * 2) /(double) nOutputWidth  );	
//					Mat  pMatResize;
//					resize(src, pMatResize, cv::Size(nOutputWidth*dbMarginScale ,nOutputHeight * dbMarginScale ), 0.0, 0.0 ,INTER_CUBIC);
//					int nLeft = (int)((nOutputWidth*dbMarginScale - nOutputWidth)/2);
//					int nTop = (int)((nOutputHeight*dbMarginScale - nOutputHeight)/2);
//					Mat pMatCut =  (pMatResize)(cv::Rect(nLeft, nTop, nOutputWidth, nOutputHeight));
//					pMatCut.copyTo(src);
//					pMatResize = NULL;
//				}
//				else
//				{
//					Mat pMatCut =  src(Rect(
//						rtMargin.left, 
//						rtMargin.top, 
//						rtMargin.right-rtMargin.left, 
//						rtMargin.bottom-rtMargin.top));
//					resize(pMatCut, pMatCut, cv::Size(nOutputWidth, nOutputHeight), 0.0, 0.0 ,INTER_CUBIC);
//					pMatCut.copyTo(src);
//				}
//			}
//
//			if(pFrameInfo->bLogoZoom)
//			{
//				//#ifdef LOGO_ZOOM //Logo Zoom 전
//				if( pFrameInfo->nDscIndex > -1)
//				{
//					Mat srcLogo(src.rows, src.cols, CV_8UC3);
//					srcLogo = src.clone();
//					// insert Logo 구현
//					imgproc::GpuInsertLogo2(&srcLogo, pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
//					src = srcLogo.clone();
//				}
//				if( pFrameInfo->nPriIndex > -1 || pFrameInfo->nPrinumIndex > 0)
//				{
//					Mat srcPrism(src.rows, src.cols, CV_8UC3);
//					srcPrism = src.clone();
//					imgproc::KzonePrism(&srcPrism,pFrameInfo->nPriIndex, pFrameInfo->nPriValue,pFrameInfo->nPriValue2,pFrameInfo->nPrinumIndex);
//					src = srcPrism.clone();
//				}
//				//#endif
//			}
//
//			if(bReverse)
//			{
//				imgproc::CpuRotateImage(src,nRotCenterX,nRotCenterY,1,AdjustData.AdjAngle,TRUE);
//				//flip(src,src,-1);
//				//pEffectData.nPosX = nSrcWidth - pEffectData.nPosX;
//				//pEffectData.nPosY = nSrcHeight - pEffectData.nPosY;
//			}
//
//			if( pEffectData.bStepFrame == TRUE &&
//				pEffectData.nStartZoom != pEffectData.nEndZoom)
//			{
//				pFrameInfo->nWidth = nSrcWidth;
//				pFrameInfo->nHeight= nSrcHeight;
//				memcpy(pFrameInfo->Image,src.data,nSrcWidth*nSrcHeight*3);
//				pFrameInfo->bComplete++;
//				return TRUE;
//			}
//			double dScale = (double)pEffectData.nZoomRatio / 100.0;
//
//			if(dScale == 0)
//			{
//				//Template 오류
//				//ReleaseSemaphore(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, 1, NULL);
//				//TRACE(_T("Template Error"));
//				//pFrameInfo->bComplete = -100;
//				dScale = 1;
//				SendLog(5,_T("Template Error"));
//				//return 0;
//			}
//			if(pEffectData.nPosX == 0 && pEffectData.nPosY == 0)
//			{
//				pEffectData.nPosX = nSrcWidth/2;
//				pEffectData.nPosY = nSrcHeight/2;
//			}
//
//#ifndef INSERT_TEXT
//			char strText[MAX_PATH];
//			sprintf(strText,"%d_%d",pFrameInfo->nSecIdx,pFrameInfo->nFrameIndex);
//			putText(src,strText,cv::Point(1920,1080),CV_FONT_HERSHEY_COMPLEX,3,Scalar(0,0,0),1,8);
//#endif
//
//			Mat MatCut;
//
//			int nLeft = (pEffectData.nPosX- nSrcWidth/dScale/2);
//			int nTop = (pEffectData.nPosY - nSrcHeight/dScale/2);
//			int nWidth = (nSrcWidth/dScale - 1);
//			if((nLeft + nWidth) > nSrcWidth)
//				nLeft = nLeft - ((nLeft + nWidth) - nSrcWidth);
//			else if(nLeft < 0)
//				nLeft = 0;
//			int nHeight = (nSrcHeight/dScale - 1);
//			if((nTop + nHeight) > nSrcHeight)
//				nTop = nTop - ((nTop + nHeight) - nSrcHeight);
//			else if (nTop < 0)
//				nTop = 0;
//			MatCut =  (src)(Rect(nLeft, nTop, nWidth, nHeight));
//			src = MatCut.clone();
//		}
//		else
//		{
//			//wgkim 180513 Vertical 변환 코드
//			if(bReverse)
//			{
//				flip(src,src,-1);
//			}
//
//			double degree = 0;
//			double resizeRatio = (16./9);
//			
//			Mat rotateImage(src.cols, src.rows, CV_8UC3);
//			if(pFrameInfo->nVertical == -1)
//			{
//				transpose(src, rotateImage);
//				flip(rotateImage, rotateImage, 1);
//			}
//
//			else if(pFrameInfo->nVertical == 1)
//			{
//				transpose(src, rotateImage);
//				flip(rotateImage, rotateImage, 0);
//			}
//
//			Point2f ptVerticalCenterPoint;
//			ptVerticalCenterPoint.x = pFrameInfo->ptVerticalCenterPoint.x;
//			ptVerticalCenterPoint.y = pFrameInfo->ptVerticalCenterPoint.y;
//
//			Rect2d rtRoiRect;
//			rtRoiRect.width = src.rows;
//			rtRoiRect.height = src.rows* (9./16);
//			rtRoiRect.x = 0;
//			rtRoiRect.y = (src.cols * (ptVerticalCenterPoint.y / src.rows)) - (rtRoiRect.height/2);
//
//			if(rtRoiRect.y < 0)
//				rtRoiRect.y = 0;
//			if(rtRoiRect.y + rtRoiRect.height > src.cols)
//				rtRoiRect.y = src.cols - rtRoiRect.height-1;
//			rotateImage(rtRoiRect).copyTo(src);
//
//			Mat MatCut;
//
//			//Template/////////////////////
//			//////////////
//			//resize(src, src, Size(nSrcWidth, nSrcHeight), CV_INTER_LINEAR);
//
//			double dScale = (double)pEffectData.nZoomRatio / 100.0;
//			
//			pFrameInfo->nWidth = src.cols;
//			pFrameInfo->nHeight= src.rows;
//
//			//180524
//			ptVerticalCenterPoint.x *= (double)src.cols/nSrcWidth;
//			ptVerticalCenterPoint.y = (double)src.rows/2;
//
//			//180525
//			double nWidthBoundRatio_Left =  ((double)src.cols/2) - ((double)src.cols/2) * pow((double)src.rows/src.cols, 2);
//			double nWidthBoundRatio_Right = ((double)src.cols/2) + ((double)src.cols/2) * pow((double)src.rows/src.cols, 2);
//
//			if( ptVerticalCenterPoint.x < nWidthBoundRatio_Left)
//				ptVerticalCenterPoint.x = nWidthBoundRatio_Left;
//			if( ptVerticalCenterPoint.x > nWidthBoundRatio_Right)
//				ptVerticalCenterPoint.x = nWidthBoundRatio_Right;
//
//			ptVerticalCenterPoint.x = 
//				abs(ptVerticalCenterPoint.x - nWidthBoundRatio_Left) * 
//				((double)src.cols/(nWidthBoundRatio_Right- nWidthBoundRatio_Left));
//
//			if(dScale == 0)
//			{
//				dScale = 1;
//				SendLog(5,_T("Vertical Error"));
//			}
//
//			if(pEffectData.nPosX == 0 && pEffectData.nPosY == 0)
//			{
//				pEffectData.nPosX = src.cols * (ptVerticalCenterPoint.x/src.cols);//nSrcWidth/2;
//				pEffectData.nPosY = src.rows/2;
//			}
//			pEffectData.nPosX = src.cols * (ptVerticalCenterPoint.x/src.cols);//nSrcWidth/2;
//			pEffectData.nPosY = ptVerticalCenterPoint.y;
//
//			if( pEffectData.bStepFrame == TRUE &&
//				pEffectData.nStartZoom != pEffectData.nEndZoom)
//			{
//				pFrameInfo->nWidth = src.cols;
//				pFrameInfo->nHeight= src.rows;
//				memcpy(pFrameInfo->Image,src.data,src.cols*src.rows*3);
//				pFrameInfo->bComplete++;
//
//				//wgkim 180528
//				pFrameInfo->stEffectData.nPosX = pEffectData.nPosX;
//				pFrameInfo->stEffectData.nPosY = pEffectData.nPosY;
//
//				return TRUE;
//			}
//
//			int nLeft = (pEffectData.nPosX- src.cols/dScale/2);
//			int nTop = (pEffectData.nPosY - src.rows/dScale/2);
//			int nWidth = (src.cols/dScale - 1);
//			if((nLeft + nWidth) > src.cols)
//				nLeft = nLeft - ((nLeft + nWidth) - src.cols);
//			else if(nLeft < 0)
//				nLeft = 0;
//
//			int nHeight = (src.rows/dScale - 1);
//			if((nTop + nHeight) > src.rows)
//				nTop = nTop - ((nTop + nHeight) - src.rows);
//			else if (nTop < 0)
//				nTop = 0;
//			MatCut =  (src)(Rect(nLeft, nTop, nWidth, nHeight));
//			src = MatCut.clone();
//
//			//wgkim 180528
//			pFrameInfo->stEffectData.nPosX = pEffectData.nPosX;
//			pFrameInfo->stEffectData.nPosY = pEffectData.nPosY;
//			//////////////////////////////////////////////////////////
//		}
//		Mat MatResize;
//
//		if(imgproc::GetUHDtoFHD())
//		{
//			resize(src, MatResize, Size(nPreWidth ,nPreHeight), 0.0, 0.0, INTER_CUBIC );
//			pFrameInfo->nWidth = nPreWidth;
//			pFrameInfo->nHeight = nPreHeight;
//
//			MatResize.copyTo(src);
//		}
//		else
//		{
//			resize(src, MatResize, Size(nOutputWidth ,nOutputHeight), 0.0, 0.0, INTER_CUBIC );
//			pFrameInfo->nWidth = nOutputWidth;
//			pFrameInfo->nHeight = nOutputHeight;
//
//			MatResize.copyTo(src);
//		}
//		////-- Reverse Movie
//		/*if(bReverse)
//		{
//		nRotateX = src.cols / 2;
//		nRotateY = src.rows/ 2;
//		imgproc::CpuRotateImage(src, nRotateX ,nRotateY, 1, AdjustData.AdjAngle, bReverse);
//		}*/
//
//		Mat srcImage = src.clone();
//#if 1 //-- 카메라 ID 표시
//		if( pFrameInfo->strCameraID[0] != _T('\0'))
//			imgproc::GpuInsertCameraID(&srcImage,pFrameInfo->nPriIndex,pFrameInfo->strCameraID,pFrameInfo->strDSC_IP,pFrameInfo->bPrnColorInfo);
//#endif
//#endif
//
//		if(!pFrameInfo->bLogoZoom)
//		{
////#ifndef LOGO_ZOOM //Logo Zoom 후
//			//Insert Logo
//			if( pFrameInfo->nDscIndex > -1)
//			{
//				// insert Logo 구현
//				imgproc::GpuInsertLogo2(&srcImage, pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
//			}
////#endif
//		}
//
//		if(pFrameInfo->bInsertWB)
//		{
//			Ptr<xphoto::WhiteBalancer> wb;
//			wb = xphoto::createGrayworldWB();
//			wb->balanceWhite(srcImage, srcImage);
//		}
//
//
//		delete[] pFrameInfo->Image;
//		int size = srcImage.total() * srcImage.elemSize();
//		pFrameInfo->Image = new BYTE[size];
//		memcpy( pFrameInfo->Image, srcImage.data, size);
//		//delete pImageMat;
//		pFrameInfo->bComplete++;
//
//		//BMP Save
//		if(bSaveImg)
//		{
//			IplImage*	img = new IplImage(srcImage);
//			CString strData , strTemp;
//			char path[MAX_PATH];
//			strData.Format(_T("%s"), strPath);
//			CreateDirectory(strData,NULL);
//			strTemp.Format(_T("\\%s_%d_%d.bmp"), strDscId, nIndex, pFrameInfo->nFrameIndex);
//			strData.Append(strTemp);
//			sprintf(path, "%S", strData);
//			cvSaveImage(path, img);
//		}
//
//		ReleaseSemaphore(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, 1, NULL);
////#else
////		double dResizeScale = (double)nSrcWidth / (double)nOutputWidth;
////
////		int nSize = 0;
////		double dSize = 0.0;
////		double dRatio = (double)pImageMat->cols / (double)nSrcWidth;
////		int nRotateX = imgproc::Round(AdjustData.AdjptRotate.x * dRatio );	 // 반올림
////		int nRotateY = imgproc::Round(AdjustData.AdjptRotate.y * dRatio );
////		int nMoveX = imgproc::Round(AdjustData.AdjMove.x * dRatio);	 // 반올림
////		int nMoveY = imgproc::Round(AdjustData.AdjMove.y * dRatio);	 // 반올림
////		int nMarginX = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginX();
////		int nMarginY = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginY();
////		double dbMarginScale = 1 / ( (double)( nSrcWidth - nMarginX * 2) /(double) nSrcWidth  );
////		int nResizeWidth = (nOutputWidth * dbMarginScale) + (nMarginX * 2);
////		int nResizeHeight = (nOutputHeight * dbMarginScale) + (nMarginY * 2);
////
////		if ( nMarginX < 0 || nMarginX >= nSrcWidth/2 || nMarginY < 0 || nMarginY >= nSrcHeight/2  )
////		{
////			ReleaseSemaphore(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, 1, NULL);
////			TRACE(_T("Image Adjust Error 2\n"));
////			pFrameInfo->bComplete++;
////			return 0;
////		}
////
////		if(nSrcWidth > nOutputWidth && nSrcHeight > nOutputHeight)
////		{//소스와 아웃풋이 다를때 UHD->FHD
////			if (pEffectData.bZoom == TRUE && pEffectData.nZoomRatio != 100)
////			{
////				//imgproc::GpuZoomImage(pImageMat, nWidth, nHeight, pEffectData->nPosX, pEffectData->nPosY, pEffectData->nZoomRatio, nResizeWidth, nResizeHeight);
////
////				double dScale = (double)pEffectData.nZoomRatio / 100.0;
////				cuda::GpuMat gMatCut, gMatResize;
////
////				int nLeft = (pEffectData.nPosX- nSrcWidth/dScale/2);
////				int nTop = (pEffectData.nPosY - nSrcHeight/dScale/2);
////				int nWidth = (nSrcWidth/dScale - 1);
////				if((nLeft + nWidth) > nSrcWidth)
////					nLeft = nLeft - ((nLeft + nWidth) - nSrcWidth);
////				else if(nLeft < 0)
////					nLeft = 0;
////				int nHeight = (nSrcHeight/dScale - 1);
////				if((nTop + nHeight) > nSrcHeight)
////					nTop = nTop - ((nTop + nHeight) - nSrcHeight);
////				else if (nTop < 0)
////					nTop = 0;
////				gMatCut =  (*pImageMat)(Rect(nLeft, nTop, nWidth, nHeight));
////				gMatCut.copyTo(*pImageMat);
////
////				nRotateX = nRotateX - nLeft;
////				nRotateY = nRotateY - nTop;
////
////				// Rotate 구현
////				imgproc::GpuRotateImage(pImageMat, nRotateX, nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle);
////
////				//-- MOVE EVENT
////				imgproc::GpuMoveImage(pImageMat, nMoveX,  nMoveY);
////
////				imgproc::GpuMakeMargin(pImageMat, nMarginX, nMarginY);
////
////				nWidth = pImageMat->cols - nMarginX*2;
////				nHeight = pImageMat->rows - nMarginY*2;
////				gMatCut = (*pImageMat)(Rect(nMarginX, nMarginY, nWidth, nHeight));
////				gMatCut.copyTo(*pImageMat);
////
////				cuda::resize(*pImageMat, gMatResize, Size(nOutputWidth ,nOutputHeight), 0.0, 0.0, INTER_CUBIC );
////				gMatResize.copyTo(*pImageMat);
////			}
////			else
////			{
////				cuda::GpuMat* pMatResize = new cuda::GpuMat;
////				if(nSrcWidth > nOutputWidth && nSrcHeight > nOutputHeight)
////				{
////					cuda::resize(*pImageMat, *pMatResize, Size(nOutputWidth ,nOutputHeight), 0.0, 0.0, INTER_CUBIC );
////					pMatResize->copyTo(*pImageMat);
////				}
////				//delete pEffectData;
////
////				int nWidth = nOutputWidth;
////				int nHeight = nOutputHeight;
////				nRotateX = nRotateX / dResizeScale;
////				nRotateY = nRotateY / dResizeScale;
////				nMoveX = nMoveX / dResizeScale;
////				nMoveY = nMoveY / dResizeScale;
////				nMarginX = nMarginX / dResizeScale;
////				nMarginY = nMarginX / dResizeScale;
////
////				// Rotate 구현
////				imgproc::GpuRotateImage(pImageMat, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle);
////
////				//-- MOVE EVENT
////				imgproc::GpuMoveImage(pImageMat, nMoveX,  nMoveY);
////
////				imgproc::GpuMakeMargin(pImageMat, nMarginX, nMarginY);
////				double dbMarginScale = 1 / ((double)(nWidth - nMarginX * 2) / (double)nWidth);
////
////				cuda::resize(*pImageMat, *pMatResize, Size(nWidth*dbMarginScale ,nHeight*dbMarginScale ), 0.0, 0.0 ,INTER_CUBIC );
////				int nLeft = (int)((nWidth*dbMarginScale - nWidth)/2);
////				int nTop = (int)((nHeight*dbMarginScale - nHeight)/2);
////				cuda::GpuMat pMatCut =  (*pMatResize)(Rect(nLeft, nTop, nWidth, nHeight));
////				pMatCut.copyTo(*pImageMat);
////				delete pMatResize;
////			}
////		}
////		Mat srcImage(pImageMat->rows, pImageMat->cols, CV_8UC3);
////		pImageMat->download(srcImage);
////		delete[] pFrameInfo->Image;
////		int size = srcImage.total() * srcImage.elemSize();
////		pFrameInfo->Image = new BYTE[size];
////		memcpy( pFrameInfo->Image, srcImage.data, size);
////		delete pImageMat;
////		pFrameInfo->bComplete++;
////		ReleaseSemaphore(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, 1, NULL);
////#endif
//		
//	}
//	
//	
//		
//	
//// 	//-- Insert Logo
//// 	if( pFrameInfo->nDscIndex > -1)
//// 	{
//// 		int nWidth = pMovieData->GetSizeWidth();
//// 		int nHeight = pMovieData->GetSizeHight();
//// 
//// 		// insert Logo 구현
//// 		imgproc::GpuInsertLogo(pImageMat, nDscIndex, nWidth, nHeight);
//// 	}
//// 	pMovieData->OrderAdd(nFrameNum);
//
//// 	//-- MOVIE EFFECT
//// 	if (pEffectData->bZoom == FALSE)
//// 	{
//// 		pMovieData->OrderAdd(nFrameNum);
//// 		delete pEffectData;
//// 		//ReleaseSemaphore(pMovieMgr->hSemaphore, 1, NULL);
//// 		return 0;
//// 	}
//
//// 	if ( pEffectData->nZoomRatio != 100 )
//// 		imgproc::GpuZoomImage(pMovieData->GetImage(nFrameNum), pMovieData->GetSizeWidth(), pMovieData->GetSizeHight(), pEffectData->nPosX,  pEffectData->nPosY, pEffectData->nZoomRatio);
//
//	//TRACE(_T("End %s, %s, %d\n"), strMovieName, strMovieId, nFrameNum);
//	//ReleaseSemaphore(pMovieMgr->hSemaphore, 1, NULL);
//
//	ReleaseSemaphore(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, 1, NULL);
//	return 0;
//}





unsigned WINAPI CESMMovieThreadManager::ImageAdjustThread2(LPVOID param)
{	
	ThreadFrameData *pFrameData = (ThreadFrameData*)param;
	BOOL bSaveImg = FALSE;
	BOOL bReverse = FALSE;
	CString strPath;
	bSaveImg = pFrameData->bSaveImg;
	strPath = pFrameData->strPath;
	bReverse = pFrameData->bReverse;
	MakeFrameInfo* pFrameInfo = pFrameData->pFrameInfo ;
	CESMMovieThreadManager *pMovieThreadManager = pFrameData->pMovieThreadManager;
	delete pFrameData;

	int nSrcWidth = 0, nSrcHeight = 0, nOutputWidth = 0, nOutputHeight = 0 ;

	//Output 형식에 따른 데이터 변경.
	imgproc::GetMovieSizeData(nSrcWidth, nSrcHeight, nOutputWidth, nOutputHeight);
	
	if(!imgproc::GetVMCCFlag())
		return imgproc::AdjustFrame((CESMMovieMgr*)pMovieThreadManager->m_pParent, pFrameInfo, strPath, bSaveImg, bReverse);
	else
		return imgproc::AdjustFrameUsingVMCC((CESMMovieMgr*)pMovieThreadManager->m_pParent, pFrameInfo, nSrcWidth == FULL_HD_WIDTH && nSrcHeight == FULL_HD_HEIGHT, strPath, bSaveImg, bReverse);

}

unsigned WINAPI CESMMovieThreadManager::ImageAdjustThread3(LPVOID param)
{
	ThreadFrameData *pFrameData = (ThreadFrameData*)param;
	BOOL bSaveImg = FALSE;
	BOOL bReverse = FALSE;
	CString strPath;
	bSaveImg = pFrameData->bSaveImg;
	strPath = pFrameData->strPath;
	bReverse = pFrameData->bReverse;
	MakeFrameInfo* pFrameInfo = pFrameData->pFrameInfo ;
	CESMMovieThreadManager *pMovieThreadManager = pFrameData->pMovieThreadManager;
	delete pFrameData;

	WaitForSingleObject(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, INFINITE);

	int nSrcWidth = 0, nSrcHeight = 0, nOutputWidth = 0, nOutputHeight = 0 ;
	imgproc::GetMovieSizeData(nSrcWidth, nSrcHeight, nOutputWidth, nOutputHeight);	

	CString strDSCID = pMovieThreadManager->GetDSCIDFromPath(pFrameInfo->strFramePath);
	stAdjustInfo AdjustData = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetAdjustData(strDSCID);
	if(AdjustData.AdjAngle == 0 && AdjustData.AdjMove.x == 0 && AdjustData.AdjMove.y == 0)
	{
		CString strLog;
		strLog.Format(_T("[%s] Cannot Load Adjust"),strDSCID);
		SendLog(5,strLog);
	}

	int nMarginX = AdjustData.stMargin.nMarginX;//((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginX();
	int nMarginY = AdjustData.stMargin.nMarginY;//((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginY();

	int nSelMode = pMovieThreadManager->SelectSelMode(pMovieThreadManager,nSrcHeight,nSrcWidth);

	cv::Size szSrc(nSrcWidth,nSrcHeight);
	cv::Size szOutput(nOutputWidth,nOutputHeight);

	CESMMovieThreadImage movieImg;
	movieImg.m_bUHDtoFHD = imgproc::GetUHDtoFHD();
	movieImg.DoAdjustYUV(pFrameInfo,AdjustData,szSrc,szOutput,nMarginX,nMarginY,nSelMode);

	pFrameInfo->bComplete++;

	ReleaseSemaphore(((CESMMovieMgr*)pMovieThreadManager->m_pParent)->hSemaphore, 1, NULL);

	return TRUE;
}
unsigned WINAPI CESMMovieThreadManager::ImageYUVtoRGBThread(LPVOID param)
{
	//SendLog(5,_T("Make Thread YUV"));
	ThreadDecodingData *pDecodingData = (ThreadDecodingData*)param;
	vector<MakeFrameInfo>* pArrFrameInfo = pDecodingData->pArrFrameInfo;
	int nIndex = pDecodingData->nArrayStartIndex;

	while(1)
	{
		Sleep(1);
		if(pArrFrameInfo->at(nIndex).bCompleteGPU > 0)
			break;
		if(pArrFrameInfo->at(nIndex).bCompleteGPU == -100)
			break;
	}

	CString strLog;
	strLog.Format(_T("Start YUV Index = %d"), nIndex);
	//SendLog(5, strLog);
	if( pArrFrameInfo->at(nIndex).bCompleteGPU == -100)
	{
		pArrFrameInfo->at(nIndex).bComplete = -100;
		return 0;
	}
		
	CESMMovieThreadManager* pMovieThreadManager = pDecodingData->pMovieThreadManager;
	pMovieThreadManager->DoYUVtoRGB(pArrFrameInfo, nIndex);

	return 0;
}

unsigned WINAPI CESMMovieThreadManager::ImageGpuDecodingThread(LPVOID param)
{
	int nStart = GetTickCount();
	ThreadDecodingData *pDecodingData = (ThreadDecodingData*)param;
	vector<MakeFrameInfo>* pArrFrameInfo = pDecodingData->pArrFrameInfo;
	CESMMovieThreadManager* pMovieThreadManager = pDecodingData->pMovieThreadManager;
	BOOL bRotate = pDecodingData->bRotate;
	BOOL bCopy = pDecodingData->bCopy;
	BOOL bLocal = pDecodingData->bLocal;
#if 1
	pMovieThreadManager->MakeSequentialFrame(pArrFrameInfo,bRotate,bCopy,bLocal);

#else
	for(int n = 0; n < pArrFrameInfo->size(); n++)
	{
		pArrFrameInfo->at(n).pParent = (void*)pMovieThreadManager;
		pArrFrameInfo->at(n).bReverse = pDecodingData->bRotate;
	}

	CString strPath;
	int nStartIndex = 0, nEndIndex = 0;
	if(pDecodingData->bGPUMakeFile)//연속영상 생성
	{
		SendLog(1,_T("###1 DoGpuDecodingFrame"));
		pMovieThreadManager->DoGpuDecodingFrame(pArrFrameInfo, 0, 29, 0, TRUE);
	}
	else
	{
		BOOL bLast = FALSE;
		for(int i = 0; i < pArrFrameInfo->size(); i++)
		{

			if(i == 0)
				strPath = pArrFrameInfo->at(i).strFramePath;

			if(strPath.CompareNoCase(pArrFrameInfo->at(i).strFramePath) != 0)
			{
				strPath = pArrFrameInfo->at(i).strFramePath;
				pMovieThreadManager->DoGpuDecodingFrame(pArrFrameInfo, nStartIndex, i-1, 0,  TRUE);
				nStartIndex = i;
				bLast = TRUE;
			}

			nEndIndex++;
			if(nEndIndex == pArrFrameInfo->size() && bLast == FALSE)
			{
				pMovieThreadManager->DoGpuDecodingFrame(pArrFrameInfo, nStartIndex, i, 0, TRUE);
			}
			bLast = FALSE;
		}
	}
	

	int nEnd = GetTickCount();
	CString strLog;
	strLog.Format(_T("Decoding total Time = %d"), nEnd-nStart);
	SendLog(1,strLog);


	if(param)
	{
		delete param;
		param = NULL;
	}
#endif
	
	return 0;
}

unsigned WINAPI CESMMovieThreadManager::ImageDecodingThread(LPVOID param)
{
	ThreadDecodingData *pDecodingData = (ThreadDecodingData*)param;
	vector<MakeFrameInfo>* pArrFrameInfo = pDecodingData->pArrFrameInfo;
	CESMMovieThreadManager* pMovieThreadManager = pDecodingData->pMovieThreadManager;
	int nArrayStartIndex = pDecodingData->nArrayStartIndex;
	int nStartIndex = pDecodingData->nStartIndex;
	int nEndIndex = pDecodingData->nEndIndex;
	TRACE(_T("ImageDecodingThread nArrayStartIndex[%d] nStartIndex[%d] nEndIndex[%d]\n"), nArrayStartIndex, nStartIndex, nEndIndex);
	pMovieThreadManager->DoEncodingFrame(pArrFrameInfo, nArrayStartIndex, nStartIndex, nEndIndex, pDecodingData->nFrameRate);

	return 0;
}
int CESMMovieThreadManager::MakeGPUSequentialFrame(MuxDataInfo *pMuxDataInfo)
{
	//CString strStartPath,CString strCamID,CString strSavePath,int nStartFrame,int nEndFrame,BOOL bWB,BOOL bInsertCamID
	SendLog(5,_T("Making Start"));
	//FFmpegManager **pffmpeg = new FFmpegManager *[2](FALSE); 
	vector<FFmpegManager*>pArrFFMpeg;
	for(int i = 0 ; i < ENCODE_MAX; i++)
	{
		FFmpegManager* pffmpeg = new FFmpegManager(FALSE);
		pArrFFMpeg.push_back(pffmpeg);
	}

//	CString strCamID;
//	strCamID = pMuxDataInfo->strCamID;
	CString *strCamID = new CString;
	strCamID->Format(_T("%s"),pMuxDataInfo->strCamID);

	m_nFinish = 0;
	m_nMuxErr = 0;
	m_nMuxProcessing = 0;
	m_strHomePath = pMuxDataInfo->strHomePath;
	SendLog(5,m_strHomePath);
	BOOL bGPULoad = FALSE;
	m_bBoth = FALSE;
	if(1/*pArrFFMpeg.at(0)->CheckCudaSupport() == TRUE*/ || pMuxDataInfo->bRefereeMode == TRUE)
	{
		bGPULoad = TRUE;
		//Go to Thread............
		ThreadSequentialData *pSequentialData = new ThreadSequentialData;
		pSequentialData->pParent = this;
		pSequentialData->pMuxDataInfo = pMuxDataInfo;
		pSequentialData->pArrFFMpeg	  = pArrFFMpeg;
		
		HANDLE hSyncTime = NULL;
		hSyncTime = (HANDLE) _beginthreadex(NULL, 0, SequentialImageThread, (void *)pSequentialData, 0, NULL);
		CloseHandle(hSyncTime);
	}
	else
	{
		m_nFinish = -100;
		m_nMuxProcessing = 1;
	}

	while(1)
	{
		Sleep(10);
		if(m_nFinish == 1 && m_nMuxProcessing == 1)
		{
			ESMEvent* pMsg = new ESMEvent();
			pMsg->nParam1 = m_nFinish;
			//pMsg->nParam2 = _ttoi(strCamID);
			pMsg->pDest = (LPARAM)strCamID;
			//pMsg->pDest = (LPARAM) pPath;
			pMsg->message = WM_MAKEMOVIE_MUX_MOVIE_FINISH;
			::SendMessage(((CESMMovieMgr*)m_pParent)->GetParentWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
			SendLog(5,_T("Mux Making Finish"));

			break;
		}
		else if((m_nFinish == -100 || m_nFinish == -50))
		{
			ESMEvent* pMsg = new ESMEvent();
			pMsg->nParam1 = m_nFinish;
			//pMsg->nParam2 = _ttoi(strCamID);
			pMsg->pDest = (LPARAM)strCamID;
			//pMsg->pDest = (LPARAM) pPath;
			pMsg->message = WM_MAKEMOVIE_MUX_MOVIE_FINISH;
			::SendMessage(((CESMMovieMgr*)m_pParent)->GetParentWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
			
			if(m_nFinish == -50)
				SendLog(0,_T("Mux Making Error"));
			else if(m_nFinish == -100)
				SendLog(0,_T("Encoding Fail"));
			
			if(bGPULoad == FALSE)
				SendLog(0,_T("Cannot Load GPU"));

			while(1)
			{
				if(m_nMuxProcessing == 1)
					break;

				Sleep(10);
			}
			break;
		}
	}
	for(int i = 0 ; i < ENCODE_MAX; i++)
	{
		FFmpegManager* pFFmpeg = (pArrFFMpeg.at(i));
		if(pFFmpeg)
		{
			delete pFFmpeg;
			pFFmpeg = NULL;
		}
	}
	pArrFFMpeg.clear();
	
	return 1;
}
unsigned WINAPI CESMMovieThreadManager::SequentialImageThread(LPVOID param)
{
	ThreadSequentialData *pSequentialData = (ThreadSequentialData*)param;
	CESMMovieThreadManager *pMovieThreadManager = pSequentialData->pParent;
	//FFmpegManager** pffmpeg = pSequentialData->pFFmpeg;
	vector<FFmpegManager*>pArrFFMpeg = pSequentialData->pArrFFMpeg;
	MuxDataInfo stMuxDataInfo;

	memcpy(&stMuxDataInfo ,pSequentialData->pMuxDataInfo, sizeof(MuxDataInfo));
	
	stAdjustInfo* pAdjInfo = stMuxDataInfo.pAdjInfo;

	//stAdjustInfo AdjustData;// = *stMuxDataInfo.AdjInfo;
	delete pSequentialData->pMuxDataInfo;
	delete pSequentialData;

	BOOL bColorTrans = stMuxDataInfo.bColorTrans;
	BOOL bTCP		 = stMuxDataInfo.bSendTo4DP;
	BOOL bMovieReverse	= stMuxDataInfo.bMovieReverse;
	int nStartFrame = stMuxDataInfo.nStartFrame;
	int nEndFrame  = stMuxDataInfo.nEndFrame;
	double dbZoomInfo = stMuxDataInfo.dbZoomRatio;

	int nSelectedContainer = stMuxDataInfo.nSelectedContainer; //wgkim 190214
	int nBitrate = stMuxDataInfo.nBitrate; //wgkim 190214

	CString strCamID	= (LPCTSTR)stMuxDataInfo.strCamID;
	CString strPath		= (LPCTSTR)stMuxDataInfo.strPath;
	/*CString	strPath		= (LPCTSTR)stMuxDataInfo.strPath;
	CString strSavePath = (LPCTSTR)stMuxDataInfo.strSavePath;*/

	//CString strFileName;//	= (LPCTSTR)stMuxDataInfo.strFileName;
	//if(!pMovieThreadManager->IsFileFolder(strMP4SavePath))
	//	pMovieThreadManager->CreateFolder(strMP4SavePath);

	//if(stMuxDataInfo.bDirect)
	//{
	//	strMP4SavePath.Format(_T("%s\\%s.ts"),stMuxDataInfo.strSavePath,stMuxDataInfo.strFileName);//	= (LPCTSTR);
	//	
	//	if(stMuxDataInfo.bMuxBoth)
	//	{
	//		strMP4SavePath.Format(_T("%s\\%s.mp4"),stMuxDataInfo.strSavePath,strCamID);//	= (LPCTSTR);
	//		if(stMuxDataInfo.bPSCP)
	//			strTSSavePath.Format(_T("%s/%s.ts"),stMuxDataInfo.strTSFilePath,stMuxDataInfo.strFileName);
	//		else
	//			strTSSavePath.Format(_T("%s\\%s.ts"),stMuxDataInfo.strTSFilePath,stMuxDataInfo.strFileName);
	//	}
	//}
	//else
	//	strMP4SavePath.Format(_T("%s\\%s.mp4"),stMuxDataInfo.strSavePath,strCamID);

	//SendLog(5,strMP4SavePath);

	//Index define
	int nMovieStart=0,nMovieEnd=0;
	int nStartIndex = 0,nEndIndex = 0;

	int nFrameRate = pMovieThreadManager->GetFrameRate();

	//int nFrame = 30;
	//if(nFrameRate == 0 || nFrameRate == 4)//25p
	//{
	//	nFrame = 40;
	//	nFrame = 1000/nFrame;
	//}else if(nFrameRate == 1 || nFrameRate == 3)//30p
	//{
	//	nFrame = 30;
	//}else if(nFrameRate == 2) //50p
	//{
	//	nFrame = 20;
	//	nFrame = 1000/nFrame;
	//}else
	//{
	//	nFrame = 30;
	//}
	CString strTmp;
	strTmp.Format(_T("nFrameRate: %d "),nFrameRate);
	SendLog(5,strTmp);

	//nFrame = 1000/nFrame;

	((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMovieIndex(nStartFrame,nMovieStart,nStartIndex,-1,nFrameRate/*nFrame*/);
	((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMovieIndex(nEndFrame,nMovieEnd,nEndIndex,-1,nFrameRate/*nFrame*/);

	int nMovieTotalCnt = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMovieFrameCount(nFrameRate);

	CString strTemp;
	strTemp.Format(_T("%d (%d - %d) ~ %d (%d - %d) "),nStartFrame,nMovieStart,nStartIndex,
		nEndFrame,nMovieEnd,nEndIndex);
	SendLog(5,strTemp);

	//Get Movie Size
	int nWidth = 1920, nHeight = 1080;
	int nSrcWidth=0, nSrcHeight=0;
	int nOutputWidth = 0, nOutputHeight = 0;

	
	/*CString strTempPath,strFirstPath;
	strTempPath.Format(_T("%s%d.mp4"),strPath,nMovieStart);*/

#if 0
	if(bTCP)
	{
		for(int i = nMovieStart; i <= nMovieEnd; i++)
		{
			strTempPath.Format(_T("%s%d.mp4"),strPath,i);
			
			CESMMovieFileOperation fo;
			CString strTempLog;
			
			int nLoop = 0;
			if(!fo.IsFileExist(strTempPath))
			{
				while(1)
				{
					if(fo.IsFileExist(strTempPath))
					{
						if(i == nMovieStart)
							strFirstPath = strTempPath;

						strTempLog.Format(_T("[#NETWORK] Local File Check OK..! [%s]"),strTempPath);
						SendLog(5,strTempLog);
						break;
					}
					else
					{
						nLoop++;
						if(nLoop > 60)
						{
							strTempLog.Format(_T("[#NETWORK] Local File Check Fail [%s]"),strTempPath);
							SendLog(5,strTempLog);
							break;
						}
						continue;
					}

					Sleep(100);
				}
			}
			else
			{
				if(i == nMovieStart)
					strFirstPath = strTempPath;

				strTempLog.Format(_T("[#NETWORK] Local File Check OK..! [%s]"),strTempPath);
				SendLog(5,strTempLog);
			}
		}
	}
#endif
	CString strTempPath;
	strTempPath.Format(_T("%s%s_%d.mp4"),strPath,stMuxDataInfo.strCamID,nMovieStart);
	pMovieThreadManager->GetVideoSize(strTempPath,&nSrcWidth,&nSrcHeight);
	nWidth = nSrcWidth;nHeight = nSrcHeight;
	nOutputWidth = stMuxDataInfo.nOutWidth, nOutputHeight = stMuxDataInfo.nOutHeight;

	int nRemakeCnt = nEndFrame - nStartFrame + 1;
	BOOL bHalf = FALSE;
	if(stMuxDataInfo.bLowBitrate && nMovieTotalCnt >= 50)
	{
		nRemakeCnt /= 2;
		bHalf = TRUE;
	}
	vector<Mat>*ArrImage = new vector<Mat>(nRemakeCnt);

	int nProcessingFrame = nRemakeCnt;//ArrImage->size();
	CString str;
	str.Format(_T("Processing - %d"),nRemakeCnt);
	SendLog(5,str);
	
	//Get Adjust Data
	// = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetAdjustData(strCamID);	
	double RotX,RotY,AdjX,AdjY;
	int nW,nH,nMarginX,nMarginY,nLeft,nTop;
	Mat Resize;
	BOOL bAdjLoad = TRUE;
	if(!stMuxDataInfo.bRefereeMode)
	{
#if 0		
		AdjustData = ((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetAdjustData(strCamID);
#endif
		//Ratio(Default)
		double dRatio = 1;
		//Angle
		double degree = pAdjInfo->AdjAngle;

		double dbAngleAdjust = -1 * (degree + 90);
		double theta =  (-dbAngleAdjust)*(PHI/180);
		if(degree == 0) theta = 0;

		RotX  = imgproc::Round(pAdjInfo->AdjptRotate.x * dRatio);
		RotY  = imgproc::Round(pAdjInfo->AdjptRotate.y * dRatio);

		//Move
		AdjX  = imgproc::Round(pAdjInfo->AdjMove.x * dRatio);
		AdjY  = imgproc::Round(pAdjInfo->AdjMove.y * dRatio);

		////Margin
		nMarginX =  pAdjInfo->stMargin.nMarginX;//((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginX() * dRatio;
		nMarginY =  pAdjInfo->stMargin.nMarginY;//((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginY() * dRatio;

		double dbMarginScale = 1 / ( (double)( nWidth - nMarginX * 2) /(double) nWidth);
		nLeft = (int)((nWidth  * dbMarginScale - nWidth)/2);
		nTop  = (int)((nHeight * dbMarginScale - nHeight)/2);

		nH = (int) (nHeight*dbMarginScale);
		nW = (int) (nWidth*dbMarginScale);
		Resize.create(nH,nW ,CV_8UC3);

		CString strAdjLog;
		strAdjLog.Format(_T("[%s][Angle%f->(%f,%f)],Move[(%f,%f)],Margin[(%d,%d)]"),strCamID,pAdjInfo->AdjAngle,
			pAdjInfo->AdjptRotate.x,pAdjInfo->AdjptRotate.y,pAdjInfo->AdjMove.x,pAdjInfo->AdjMove.y,nMarginX,nMarginY);

		if(pAdjInfo->AdjAngle == 0.0 || pAdjInfo->AdjptRotate.x == 0.0 || pAdjInfo->AdjptRotate.y == 0.0)
		{
			bAdjLoad = FALSE;
			SendLog(0,_T("Adjust Load Error"));
			/*pAdjInfo->AdjAngle = -90.1;
			pAdjInfo->AdjptRotate.x = nWidth / 2;
			pAdjInfo->AdjptRotate.y = nHeight / 2;
			pAdjInfo->AdjMove.x = 1;
			pAdjInfo->AdjMove.y = 1;*/
			/*pMovieThreadManager->m_nFinish = -50;
			pMovieThreadManager->m_nMuxErr = -50;
			if(ArrImage)
			{
				ArrImage->clear();
				delete ArrImage;
				ArrImage = NULL;
			}
			return FALSE;*/
		}
		SendLog(5,strAdjLog);
		if(stMuxDataInfo.bMuxBoth == TRUE)
		{
			pMovieThreadManager->m_bBoth = TRUE;

			CString strSavePath = (LPCTSTR) stMuxDataInfo.strSavePath[ENCODE_WINDOW];
			CString strFileName = (LPCTSTR) stMuxDataInfo.strFileName[ENCODE_WINDOW];
			CString strRamdiskPath = (LPCTSTR) stMuxDataInfo.strRamdiskPath; //wgkim 200206

			pMovieThreadManager->DoMuxEncodeThreadWindow(ArrImage,cv::Size(nOutputWidth,nOutputHeight),
				strSavePath,strFileName,strRamdiskPath,pArrFFMpeg.at(ENCODE_WINDOW),FALSE,nBitrate,nSelectedContainer,stMuxDataInfo.bLowBitrate);

			strSavePath = (LPCTSTR) stMuxDataInfo.strSavePath[ENCODE_ANDROID];
			strFileName = (LPCTSTR) stMuxDataInfo.strFileName[ENCODE_ANDROID];

			pMovieThreadManager->DoMuxEncodeThreadAndroid(ArrImage,cv::Size(nOutputWidth,nOutputHeight),
				strSavePath,strFileName,pArrFFMpeg.at(ENCODE_ANDROID),stMuxDataInfo.bPSCP);
		}
		else
		{
			if(stMuxDataInfo.nEncodingMethod == ENCODE_WINDOW)
			{
				CString strSavePath = (LPCTSTR) stMuxDataInfo.strSavePath[ENCODE_WINDOW];
				CString strFileName = (LPCTSTR) stMuxDataInfo.strFileName[ENCODE_WINDOW];
				CString strRamdiskPath = (LPCTSTR) stMuxDataInfo.strRamdiskPath;

				pMovieThreadManager->DoMuxEncodeThreadWindow(ArrImage,cv::Size(nOutputWidth,nOutputHeight),
					strSavePath,strFileName,strRamdiskPath,pArrFFMpeg.at(ENCODE_WINDOW),nBitrate,nSelectedContainer,FALSE,stMuxDataInfo.bLowBitrate);
			}
			else if(stMuxDataInfo.nEncodingMethod == ENCODE_ANDROID)
			{
				CString strSavePath = (LPCTSTR) stMuxDataInfo.strSavePath[ENCODE_ANDROID];
				CString strFileName = (LPCTSTR) stMuxDataInfo.strFileName[ENCODE_ANDROID];

				pMovieThreadManager->DoMuxEncodeThreadAndroid(ArrImage,cv::Size(nOutputWidth,nOutputHeight),
					strSavePath,strFileName,pArrFFMpeg.at(ENCODE_ANDROID),stMuxDataInfo.bPSCP);
			}
		}
	}
	else
	{
		CString strSavePath = (LPCTSTR) stMuxDataInfo.strSavePath[ENCODE_WINDOW];
		CString strFileName = (LPCTSTR) stMuxDataInfo.strFileName[ENCODE_WINDOW];
		CString strRamdiskPath = (LPCTSTR) stMuxDataInfo.strRamdiskPath;

		pMovieThreadManager->DoMuxEncodeThreadWindow(ArrImage,cv::Size(nOutputWidth,nOutputHeight),
			strSavePath,strFileName,strRamdiskPath,pArrFFMpeg.at(ENCODE_WINDOW),nBitrate,nSelectedContainer,TRUE,stMuxDataInfo.bLowBitrate);
	}

	//Processing
	Mat frame,srcImage,result,resultImage;
	int nTotalCnt = 0;
	int nStartTick = GetTickCount();

	BOOL bRotate = pMovieThreadManager->m_bRotate;

	//wgkim 190702
	if(pArrFFMpeg.at(0)->CheckCudaSupport()/*((CESMMovieMgr*)pMovieThreadManager->m_pParent)->m_bGPUCheck*/)
	{
		SendLog(5, _T("[Muxing]Using GPU"));
	}
	else
	{
		SendLog(5, _T("[Muxing]Using CPU"));
	}
	int nDecodingTotalTime = 0;
	int nAdjustTotalTime = 0;

	for(int i = nMovieStart; i <= nMovieEnd; i++)
	{
		//wgkim 190702
		int nInitDecodingStartTime = GetTickCount();

		CString strUpdatePath;
		strUpdatePath.Format(_T("%s%s_%d.mp4"),strPath,stMuxDataInfo.strCamID,i);

		CT2CA pszConvert(strUpdatePath);
		string strMoviePath(pszConvert);
				
		VideoCapture vc(strMoviePath);
		if(!vc.isOpened())
		{
			pMovieThreadManager->m_nFinish = -50;
			pMovieThreadManager->m_nMuxErr = -50;
			return FALSE;
		}

		int nDecodingStart = 0 ,nDecodingEnd = nMovieTotalCnt - 1;

		if(i == nMovieStart) 
			nDecodingStart = nStartIndex;
		if(i == nMovieEnd)
			nDecodingEnd = nEndIndex;

		int nCnt = 0;
		BOOL bStart = FALSE;

		//wgkim 190702
		int nInitDecodingEndTime = GetTickCount();
		nDecodingTotalTime += nInitDecodingEndTime - nInitDecodingStartTime;

		while(1)
		{
			//wgkim 190702
			int nDecodingStartTime = GetTickCount();

			vc>>frame;
			if(frame.empty())	break;

			if(nCnt != nDecodingStart && !bStart) 
			{
				nCnt++;
				continue;
			}

			if(nCnt == nDecodingEnd+1)
			{
				break;
			}

			//wgkim 190702
			int nDecodingEndTime = GetTickCount();
			nDecodingTotalTime += nDecodingEndTime - nDecodingStartTime;

			bStart = TRUE;
			if(nCnt %2 == 0 && bHalf)
			{
				nCnt++;
				continue;
			}
			
			if(stMuxDataInfo.bRefereeMode)
			{
				//Save VideoFile
				Mat re;
				cv::resize(frame,re,cv::Size(nOutputWidth,nOutputHeight),0,0,CV_INTER_LINEAR);
				ArrImage->at(nTotalCnt) = re.clone();
			}
			else
			{
				//190702 wgkim
				int nAdjustStartTime = GetTickCount();

				if(bMovieReverse)
					flip(frame,frame,-1);

				if(bColorTrans)
					pMovieThreadManager->ColorTransUsingCC(&frame,stMuxDataInfo.dbArrColorInfo);

				//wgkim 190616
				if(bAdjLoad)
				{
#if 1
					if(pArrFFMpeg.at(0)->CheckCudaSupport())
					{
						cuda::GpuMat gpuMat(frame);
						cuda::GpuMat gpuResize;
						imgproc::GpuRotateImage(&gpuMat, RotX, RotY, pAdjInfo->AdjSize, pAdjInfo->AdjAngle);
						imgproc::GpuMoveImage(&gpuMat, AdjX, AdjY);
						cuda::resize(gpuMat, gpuResize, cv::Size(nW,nH), 0, 0, CV_INTER_CUBIC);
						gpuResize.download(Resize);
					}
					else
					{
						Mat gpuResize;
						imgproc::CpuRotateImage(frame, RotX, RotY, pAdjInfo->AdjSize, pAdjInfo->AdjAngle, false);
						imgproc::CpuMoveImage(&frame, AdjX, AdjY);
						resize(frame, Resize, cv::Size(nW, nH), 0, 0, CV_INTER_CUBIC);						
					}
#else
					Adapting Adjust
						cuKernel.cuDipAll(frame,AdjX,AdjY,Resize);
					cuKernel.cuDipAll(frame,Resize);
#endif
					result = (Resize)(cv::Rect(nLeft,nTop,nWidth,nHeight));

					//srcImage = result.clone();

					double dZoomRatio = dbZoomInfo / 100.;
					if(dZoomRatio > 1.0)
					{
						Mat frameZoom;
						int nCenterX = nWidth/2,nCenterY = nHeight/2;

						//wgkim 190113
						CPoint ptCenterPoint = stMuxDataInfo.ptCenterPoint;
						if(ptCenterPoint.x != 0 && ptCenterPoint.y != 0)
						{
							nCenterX = ptCenterPoint.x;
							nCenterY = ptCenterPoint.y;
						}

						int nZoomLeft = (int) (nCenterX - nWidth/dZoomRatio/2);
						int nZoomTop  = (int) (nCenterY - nHeight/dZoomRatio/2);

						int nZoomWidth = (int) (nWidth/dZoomRatio - 1);
						if((nZoomLeft + nZoomWidth) > nWidth)
							nZoomLeft = nZoomLeft - ((nZoomLeft + nZoomWidth) - nWidth);
						else if(nZoomLeft < 0)
							nZoomLeft = 0;

						int nZoomHeight = (int) (nHeight/dZoomRatio - 1);
						if((nZoomTop + nZoomHeight) > nHeight)
							nZoomTop = nZoomTop - ((nZoomTop + nZoomHeight) - nHeight);
						else if (nZoomTop < 0)
							nZoomTop = 0;

						frameZoom = (result)(Rect(nZoomLeft,nZoomTop,nZoomWidth,nZoomHeight));

						result = frameZoom.clone();
					}
				}
				else
				{
					result = frame.clone();
				}

				//Resize Image
				resize(result,resultImage,cv::Size(nOutputWidth,nOutputHeight),0,0,CV_INTER_CUBIC);

				if(bRotate)
					flip(resultImage,resultImage,-1);

				//190702 wgkim
				int nAdjustEndTime = GetTickCount();
				nAdjustTotalTime += nAdjustEndTime - nAdjustStartTime;

				//Save VideoFile
				ArrImage->at(nTotalCnt) = resultImage.clone();
			}
			nCnt ++;
			nTotalCnt ++;
		}

		vc.release();
	}
	int nEndTick = GetTickCount();
	
	//writer.release();
	//cuKernel.cuFreeProperty();

	//pMovieThreadManager->m_nFinish = 1;
	if(nProcessingFrame == nTotalCnt)
		pMovieThreadManager->m_nMuxProcessing = 1;
	else
		pMovieThreadManager->m_nMuxProcessing = -100;

	CString strLog;
	strLog.Format(_T("Decoding Time : %dms / Adjust Time : %dms / Mux Processing Time : %dms[%d][%d]"), nDecodingTotalTime, nAdjustTotalTime, nEndTick-nStartTick,nTotalCnt,pMovieThreadManager->m_nMuxProcessing);
	SendLog(5,strLog);

	if(bTCP)
	{
		CESMMovieFileOperation fo;
		CString strFolder;
		//strFolder.Format(_T("M:\\Movie\\%s"),strCamID);
		strFolder.Format(_T("%s\\%s"),(LPCTSTR)stMuxDataInfo.strRamdiskPath); //wgkim 200206

		if(fo.IsFileFolder(strFolder))
			fo.Delete(strFolder,1);
	}
	if(pAdjInfo && stMuxDataInfo.bLocal == FALSE)
	{
		delete pAdjInfo;
		pAdjInfo = NULL;
	}	
	
	return TRUE;
}
void CESMMovieThreadManager::ColorTransUsingCC(Mat *img,double *dbArrColor)
{
#if 1
	CESMMovieThreadImage movieImg;
	if(dbArrColor[0] == 0 && dbArrColor[1] == 0 && dbArrColor[2] == 0)
		return;

	Mat frame = img->clone();
	
	int nIdx = 0;
	double dB = 0,dG = 0,dR = 0;
	for(int i = 0 ; i < frame.total(); i++)
	{
		dB += frame.data[nIdx++];
		dG += frame.data[nIdx++];
		dR += frame.data[nIdx++];
	}
	//현재 프레임의 평균
	dB = dB / frame.total();
	dG = dG / frame.total();
	dR = dR / frame.total();

	int nBlue = (int) (dB - dbArrColor[0]);
	int nGreen = (int) (dG - dbArrColor[1]);
	int nRed = (int) (dR - dbArrColor[2]);

	//모든 색이 일치한 경우..
	if(nBlue == 0 && nGreen == 0 &&nRed ==0)
		return;

	for(int i = 0 ; i < frame.rows; i++)
	{
		for(int j = 0; j < frame.cols; j++)
		{
			int nIdx = frame.cols * (i*3) + (j*3);

			frame.data[nIdx]	= movieImg.CalcTransValue(frame.data[nIdx],nBlue);
			frame.data[nIdx+1]	= movieImg.CalcTransValue(frame.data[nIdx+1],nGreen);
			frame.data[nIdx+2]	= movieImg.CalcTransValue(frame.data[nIdx+2],nRed);
		}
	}
	*img = frame.clone();
#else
	Mat tmp = img->clone();
	int nHeight = img->rows;
	int nWidth = img->cols;

	for(int i = 0 ; i < nHeight; i++)
	{
		for(int j = 0 ; j < nWidth; j++)
		{
			int nIdx = nWidth * (i*3) + (j*3);

			for(int c = 0 ; c < 3; c++)
			{
				int nRealIndex = nIdx + c;

				int nData = img->data[nRealIndex];
				tmp.data[nRealIndex] = TransColor(nData,dbArrColor[c]);
			}
		}
	}
	*img = tmp.clone();
#endif
}
int CESMMovieThreadManager::TransColor(int nData,double dbWeight)
{
	double nColor;

	nColor = (double)nData * (dbWeight/100);

	if(nColor > 255)
		nColor = 255;
	if(nColor < 0)
		nColor = 0;

	return (int)nColor;
}
CString CESMMovieThreadManager::GetDSCIDFromPath(CString strPath)
{
	CString strDscId;
	int nIndex = strPath.ReverseFind('\\') + 1;
	
	CString strTpPath = strPath.Right(strPath.GetLength() - nIndex);
	strDscId = strTpPath.Left(5);

	return strDscId;
}
cv::Mat CESMMovieThreadManager::DoVMCCMovie(MakeFrameInfo* pFrame,EffectInfo stEffect,double dbZoomRatio,int nOutputWidth,int nOutputHeight)
{
	int nSrcWidth  = pFrame->nWidth;
	int nSrcHeight = pFrame->nHeight;

	Mat frame(nSrcHeight,nSrcWidth,CV_8UC3,pFrame->Image);

	double dScale = (double) dbZoomRatio / 100.;

	Mat MatCut, MatResize;

	int nLeft = (int) (stEffect.nPosX- nSrcWidth/dScale/2);
	int nTop = (int) (stEffect.nPosY - nSrcHeight/dScale/2);
	
	int nWidth = (int) (nSrcWidth/dScale - 1);
	if((nLeft + nWidth) > nSrcWidth)
		nLeft = nLeft - ((nLeft + nWidth) - nSrcWidth);
	else if(nLeft < 0)
		nLeft = 0;

	int nHeight = (int) (nSrcHeight/dScale - 1);
	if((nTop + nHeight) > nSrcHeight)
		nTop = nTop - ((nTop + nHeight) - nSrcHeight);
	else if (nTop < 0)
		nTop = 0;

	MatCut =  (frame)(Rect(nLeft, nTop, nWidth, nHeight));
	
	resize(MatCut,MatResize,cv::Size(nOutputWidth,nOutputHeight),0,0,CV_INTER_CUBIC);

	return MatResize;
}
void CESMMovieThreadManager::DoMuxEncodeThreadWindow(vector<Mat>* ImageArray,cv::Size szOutput,CString strSavePath,CString strFileName,CString strRamDiskPath,FFmpegManager* pffmpeg,int nBitrate,int nSelectedContainer,BOOL bRefereeMode /*= FALSE*/,BOOL bLowBitrate /*= FALSE*/)
{
	MuxEncodingData* pMuxEncoding		= new MuxEncodingData;
	pMuxEncoding->m_ArrImage			= ImageArray;
	pMuxEncoding->pParent				= this;
	pMuxEncoding->szOutput				= szOutput;
	pMuxEncoding->strSavePath			= strSavePath;
	pMuxEncoding->pffmpeg				= pffmpeg;
	pMuxEncoding->strFileName			= strFileName;
	pMuxEncoding->bRefereeMode			= bRefereeMode;
	pMuxEncoding->bLowBitrate			= bLowBitrate;
	pMuxEncoding->nBitrate				= nBitrate;
	pMuxEncoding->nSelectedContainer	= nSelectedContainer;
	pMuxEncoding->strRamDiskPath		= strRamDiskPath;	//wgkim 200206

	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE) _beginthreadex(NULL, 0, MuxEncodingWindow, (void *)pMuxEncoding, 0, NULL);
	CloseHandle(hSyncTime);
}
unsigned WINAPI CESMMovieThreadManager::MuxEncodingWindow(LPVOID param)
{
	MuxEncodingData* pMux = (MuxEncodingData*) param;
	
	vector<Mat>*m_ArrImage = pMux->m_ArrImage;
	cv::Size szOutput	   = pMux->szOutput;
	CString strSavePath    = pMux->strSavePath;
	CString strFileName    = pMux->strFileName;
	CESMMovieThreadManager* pMgr = pMux->pParent;
	FFmpegManager		  * pffmpeg = pMux->pffmpeg;
	BOOL bRefereeMode	  = pMux->bRefereeMode;
	BOOL bLowBitrate	  = pMux->bLowBitrate;
	int nBitrate		  = pMux->nBitrate;
	int nSelectedContainer= pMux->nSelectedContainer;
	CString strRamDiskPath= pMux->strRamDiskPath;//wgkim 200206
	delete pMux;

	CString strRealPath	   = strSavePath + _T("\\") + strFileName;

	int nMuxFinish = TRUE;
	//FFmpegManager ffmpeg(FALSE);
	CString strTmpPath;
	//strTmpPath.Format(_T("M:\\Movie\\%s"),strFileName);
	strTmpPath.Format(_T("%s\\%s"),strRamDiskPath, strFileName);	//wgkim 200206
	//CreateDirectory(strTmpPath,NULL);	//wgkim 200206
	
	int nEncodingStartTime = 0;
	int nEncodingEndTime = 0;

	nEncodingStartTime = GetTickCount();
	
	int nGop = 1;
	if(bRefereeMode)
		nGop = 1;
	/*if(bLowBitrate)
		nGop = 1;*/

	BOOL bEncode = FALSE;

	if(nBitrate <= 0 ||  nBitrate > 100*1024*1024)
		nBitrate = 100*1024*1024;

	if(nSelectedContainer == 0)
	{
		bEncode = pffmpeg->InitEncodeForMuxing(
			strTmpPath/*strSavePath*/,
			30,
			AV_CODEC_ID_H264,
			nGop,
			szOutput.width,
			szOutput.height,
			bRefereeMode,
			bLowBitrate, 
			nBitrate,
			TRUE);
	}
	else if(nSelectedContainer == 1)
	{
		bEncode = pffmpeg->InitEncodeTS(
			strTmpPath, 
			30, 
			AV_CODEC_ID_H264, 
			1, 
			szOutput.width,
			szOutput.height, 
			nBitrate,
			TRUE);
	}
	else
	{
		bEncode = pffmpeg->InitEncodeForMuxing(
			strTmpPath/*strSavePath*/,
			30,
			AV_CODEC_ID_H264,
			nGop,
			szOutput.width,
			szOutput.height,
			bRefereeMode,
			bLowBitrate, 
			nBitrate,
			TRUE);
	}

	if(bEncode)
	{
		for(int i = 0 ; i < m_ArrImage->size(); i++)
		{
			while(1)
			{
				if(m_ArrImage->at(i).data)
				{
					if(nSelectedContainer == 0)
						pffmpeg->EncodePushImageForMuxing(m_ArrImage->at(i).data);
					else if(nSelectedContainer == 1)
						pffmpeg->EncodePushImageTS(m_ArrImage->at(i).data);
					else
						pffmpeg->EncodePushImageForMuxing(m_ArrImage->at(i).data);

					m_ArrImage->at(i).release();
					break;
				}
				if(pMgr->m_nMuxErr == -50 || pMgr->m_nMuxProcessing == -100)
				{
					nMuxFinish = -50;
					break;
				}
				Sleep(10);
			}

			if(pMgr->m_nMuxErr == -50 || pMgr->m_nMuxProcessing == -100)
			{
				if(pMgr->m_nMuxProcessing == -100)
					nMuxFinish = -100;
				else
					nMuxFinish = -50;
				break;
			}
		}
		if(nSelectedContainer == 0)
			pffmpeg->CloseEncodeForMuxing();
		else if(nSelectedContainer == 1)
			pffmpeg->CloseEncodeTS();
		else
			pffmpeg->CloseEncodeForMuxing();
		nEncodingEndTime = GetTickCount();

		CString strEncodingTime;
		strEncodingTime.Format(_T("MP4 Encoding Time %s->%dms"), strFileName, nEncodingEndTime - nEncodingStartTime);

		SendLog(5,strEncodingTime);

		SendLog(5,_T("Encoding Finish"));
		CESMMovieFileOperation fo;
		if(nMuxFinish != -100 && nMuxFinish != -50)
		{
			int nStart = GetTickCount();
			
			if(bRefereeMode)
			{
				pMgr->RequestMuxData(strTmpPath);
			}
			else
			{
#if 0
				//RequestData;
				pMgr->TCPSendData(strTmpPath);
#else		
				int nCount = 1;

				if(!fo.Copy(strTmpPath,strRealPath,1))
				{
					while(1)
					{
						if(fo.Copy(strTmpPath,strRealPath,1))
							break;

						Sleep(10);
						nCount ++;
					}
				}
				int nEnd = GetTickCount();

				CString str;
				str.Format(_T("MP4 COPY %d->%dms"),nCount,nEnd-nStart);
				SendLog(5,str);
#endif
			}

			fo.Delete(strTmpPath);
		}
		else
		{
			if(fo.IsFileExist(strTmpPath))
				fo.Delete(strTmpPath);
		}

		if(pMgr->m_bBoth == FALSE)
			pMgr->m_nFinish = nMuxFinish;
	}
	else
	{
		SendLog(5,_T("[MUX] Encoding Init Fail"));
		while(1)
		{
			if(pMgr->m_nMuxProcessing == 1 || pMgr->m_nMuxProcessing == -100)
				break;

			Sleep(10);
		}
		pMgr->m_nFinish = -100;
	}

	if(pMgr->m_bBoth == FALSE)
	{
		m_ArrImage->clear();
		delete m_ArrImage;
		m_ArrImage = NULL;
	}

	return TRUE;
}
void CESMMovieThreadManager::DoMuxEncodeThreadAndroid(vector<Mat>* ImageArray,cv::Size szOutput,CString strSavePath,CString strFileName,FFmpegManager* pffmpeg,BOOL bPSCP/* = FALSE*/)
{
	MuxEncodingData* pMuxEncoding = new MuxEncodingData;
	pMuxEncoding->m_ArrImage = ImageArray;
	pMuxEncoding->pParent    = this;
	pMuxEncoding->szOutput   = szOutput;
	pMuxEncoding->strSavePath= strSavePath;
	pMuxEncoding->pffmpeg    = pffmpeg;
	pMuxEncoding->strFileName= strFileName;
	pMuxEncoding->bCommunication = bPSCP;

	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE) _beginthreadex(NULL, 0, MuxEncodingAndroid, (void *)pMuxEncoding, 0, NULL);
	CloseHandle(hSyncTime);
}
unsigned WINAPI CESMMovieThreadManager::MuxEncodingAndroid(LPVOID param)
{
	MuxEncodingData* pMux = (MuxEncodingData*) param;
	
	vector<Mat>*m_ArrImage = pMux->m_ArrImage;
	cv::Size szOutput	   = pMux->szOutput;
	CString strSavePath    = pMux->strSavePath;
	CString strFileName    = pMux->strFileName;
	CESMMovieThreadManager* pMgr = pMux->pParent;
	FFmpegManager		  * pffmpeg = pMux->pffmpeg;
	BOOL	bCommunication = pMux->bCommunication;
	
	delete pMux;

	CString strRealPath;
	if(bCommunication)
		strRealPath = strSavePath + strFileName;
	else
		strRealPath = strSavePath + _T("\\") + strFileName;

	int nMuxFinish = TRUE;
	//FFmpegManager ffmpeg(FALSE);
	CString strTmpPath;
	strTmpPath.Format(_T("M:\\Movie\\%s"),strFileName);//임시저장경로

	int nEncodingStartTime = 0;
	int nEncodingEndTime = 0;

	nEncodingStartTime = GetTickCount();

	BOOL bEncode = FALSE;
	bEncode = pffmpeg->InitEncodeTS(strTmpPath, 30, AV_CODEC_ID_H264, 6, 
		szOutput.width,szOutput.height);	
	
	if(bEncode)
	{
		for(int i = 0 ; i < m_ArrImage->size(); i++)
		{
			while(1)
			{
				if(m_ArrImage->at(i).data)
				{
					pffmpeg->EncodePushImageTS(m_ArrImage->at(i).data);
					break;
				}
				if(pMgr->m_nMuxErr == -50 || pMgr->m_nMuxProcessing == -100)
				{
					nMuxFinish = -50;
					break;
				}
				Sleep(10);
			}

			if(pMgr->m_nMuxErr == -50 || pMgr->m_nMuxProcessing == -100)
			{
				if(pMgr->m_nMuxProcessing == -100)
					nMuxFinish = -100;
				else
					nMuxFinish = -50;
				break;
			}
		}
		pffmpeg->CloseEncodeTS();

		nEncodingEndTime = GetTickCount();

		CString strEncodingTime;
		strEncodingTime.Format(_T("TS Encoding Time %s->%dms"), strFileName, nEncodingEndTime - nEncodingStartTime);

		SendLog(5,strEncodingTime);

		SendLog(5,_T("Encoding Finish"));

		CESMMovieFileOperation fo;
		if(nMuxFinish != -100 && nMuxFinish != -50)
		{
			int nStart = GetTickCount();
			int nCount = 1;
			
			if(bCommunication)
			{
				CString strOpt;
				strOpt.Format(_T("-u esmlab:5dldptmdpa! -T %s %s"),strTmpPath,strRealPath);
				SendLog(5,strOpt);

				CString strFile;
				strFile = pMgr->m_strHomePath + _T("curl.exe");

				SHELLEXECUTEINFO lpExecInfo;
				lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
				lpExecInfo.lpFile = strFile;//pMgr->m_strHomePath + _T("curl.exe");
				lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
				lpExecInfo.hwnd = NULL;  
				lpExecInfo.lpVerb = L"open";
				lpExecInfo.lpParameters = strOpt;
				lpExecInfo.lpDirectory = NULL;
				lpExecInfo.nShow	= SW_HIDE;
				//lpExecInfo.nShow = SW_HIDE; // hide shell during execution
				lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
				ShellExecuteEx(&lpExecInfo);//201 create
				
				if (lpExecInfo.hProcess != NULL)
				{
					::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
					::CloseHandle(lpExecInfo.hProcess);
				}
				lpExecInfo.lpFile = _T("exit");
				lpExecInfo.lpVerb = L"close";
				lpExecInfo.lpParameters = NULL;

				if(TerminateProcess(lpExecInfo.hProcess,0))
				{
					lpExecInfo.hProcess = 0;
					SendLog(5,_T("End"));
				}
			}
			else if(!fo.Copy(strTmpPath,strSavePath,1))
			{
				while(1)
				{
					if(fo.Copy(strTmpPath,strSavePath,1))
						break;

					Sleep(10);
					nCount ++;
				}
			}
			int nEnd = GetTickCount();

			CString str;
			str.Format(_T("TS COPY %d->%dms"),nCount,nEnd-nStart);
			SendLog(5,str);

			fo.Delete(strTmpPath);
		}
		else
		{
		//	if(fo.IsFileExist(strTmpPath))
		//		fo.Delete(strTmpPath);
		}
		/*CString strLog;
		strLog.Format(_T("m_nFinish = %d - %d"),nMuxFinish,pMgr->m_nMuxProcessing);
		SendLog(5,strLog);*/
		pMgr->m_nFinish = nMuxFinish;
	}
	else
	{
		SendLog(5,_T("[MUX] Encoding Init Fail"));
		while(1)
		{
			if(pMgr->m_nMuxProcessing == 1 || pMgr->m_nMuxProcessing == -100)
				break;

			Sleep(10);
		}
		pMgr->m_nFinish = -100;
	}

	m_ArrImage->clear();
	delete m_ArrImage;
	m_ArrImage = NULL;
	
	return TRUE;
}

double CESMMovieThreadManager::GetEncodingFrameRate(int nFrameRateIndex)
{
	double dFrameRate = 30.f;

	if(nFrameRateIndex == 0)
		dFrameRate = 30.f;
	else if(nFrameRateIndex == 1)
		dFrameRate = 60.f;
	else if(nFrameRateIndex == 2)
		dFrameRate = 25.f;
	else if(nFrameRateIndex == 3)
		dFrameRate = 50.f;

	return dFrameRate;
}
void CESMMovieThreadManager::RequestMuxData(CString strPath)
{
	FILE *fp;
	CString strLog;

	char filename[255] = {0,};	
	sprintf(filename, "%S", strPath);

	fp = fopen(filename, "rb");

	if(fp == NULL)
	{
		strLog.Format(_T("[#NETWORK] Not Exist File [%s]"), strPath);
		SendLog(0,strLog);
		return;
	}

	strLog.Format(_T("[#NETWORK] Send File [%s]"), strPath);
	//SendLog(1,strLog);

	fseek(fp, 0L, SEEK_END);
	int nLength = ftell(fp);
	char* buf = new char[nLength];
	fseek(fp, 0, SEEK_SET);
	fread(buf, sizeof(char), nLength, fp);

	CString *pPath = new CString;
	pPath->Format(_T("%s"),strPath);

	ESMEvent* pMsg = new ESMEvent();
	pMsg->message = WM_ESM_NET_REFEREE_MUX_FINISH;
	pMsg->pParam = (LPARAM)pPath;
	pMsg->pDest = (LPARAM)buf;
	pMsg->nParam3 = nLength;
	::SendMessage(((CESMMovieMgr*)m_pParent)->GetParentWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);

	fclose(fp);
}

void CESMMovieThreadManager::RunMultiViewMaking(MakeMultiView* pView)
{
	int nFrameRate = GetFrameRate();

	CString strTmp,strLog;
	strTmp.Format(_T("nFrameRate: %d "),nFrameRate);
	SendLog(5,strTmp);

	int nStartFrame = pView->nStartFrame;
	int nEndFrame  = pView->nEndFrame;

	int nMovieStart=0,nMovieEnd=0;
	int nStartIndex = 0,nEndIndex = 0;

	((CESMMovieMgr*)m_pParent)->GetMovieIndex(nStartFrame,nMovieStart,nStartIndex,-1,nFrameRate/*nFrame*/);
	((CESMMovieMgr*)m_pParent)->GetMovieIndex(nEndFrame,nMovieEnd,nEndIndex,-1,nFrameRate/*nFrame*/);
	strTmp.Format(_T("%d (%d - %d) ~ %d (%d - %d) "),nStartFrame,nMovieStart,nStartIndex,
		nEndFrame,nMovieEnd,nEndIndex);
	SendLog(5,strTmp);

	FFmpegManager ffmpeg(FALSE);
	CString strSavePath;
	strSavePath.Format(_T("M:\\Movie\\%d.mp4"),pView->nMovIdx);
	BOOL bEncode = ffmpeg.InitEncode(strSavePath,30,AV_CODEC_ID_MPEG2VIDEO,1,
		960,540);

	CString strCamID	= (LPCTSTR)pView->strCamID;
	CString strPath		= (LPCTSTR)pView->strPath;

	int nMovieTotalCnt = ((CESMMovieMgr*)m_pParent)->GetMovieFrameCount(nFrameRate);
	BOOL bReverse = pView->bReverse;
	
	strLog.Format(_T("[MULTIVIEW] - %d "),pView->nMovIdx);
	SendLog(5,strLog+_T("Start!"));
	BOOL bMakingFinish = TRUE;
	if(bEncode)
	{
		double dScale;
		int nLeft,nTop,nWidth,nHeight;
		dScale = (double)pView->nZoom / 100.;

		BOOL bCalc = FALSE;
		for(int i = nMovieStart; i <= nMovieEnd ; i++)
		{
			CString strUpdatePath;
			strUpdatePath.Format(_T("%s%s_%d.mp4"),strPath,strCamID,i);

			CT2CA pszConvert(strUpdatePath);
			string strMoviePath(pszConvert);

			VideoCapture vc(strMoviePath);
			if(!vc.isOpened())
			{
				bMakingFinish = FALSE;
				SendLog(0,strLog+_T("Open Movie File Error!"));
				break;
			}

			int nDecodingStart = 0 ,nDecodingEnd = nMovieTotalCnt - 1;

			if(i == nMovieStart) 
				nDecodingStart = nStartIndex;
			if(i == nMovieEnd)
				nDecodingEnd = nEndIndex;

			int nCnt = 0;
			BOOL bStart = FALSE;

			while(1)
			{
				Mat frame;
				vc>>frame;
				if(frame.empty())
					break;

				if(nCnt != nDecodingStart && !bStart) 
				{
					nCnt++;
					continue;
				}

				if(nCnt == nDecodingEnd+1)
				{
					break;
				}

				bStart = TRUE;
				if(bCalc == FALSE)
				{
					bCalc = TRUE;
					int nSrcWidth = frame.cols;
					int nSrcHeight = frame.rows;

					nLeft = (int) (pView->nPointX- nSrcWidth/dScale/2);
					nTop = (int) (pView->nPointY - nSrcHeight/dScale/2);
					nWidth = (int) (nSrcWidth/dScale - 1);
					if((nLeft + nWidth) > nSrcWidth)
						nLeft = nLeft - ((nLeft + nWidth) - nSrcWidth);
					else if(nLeft < 0)
						nLeft = 0;

					nHeight = (int) (nSrcHeight/dScale - 1);
					if((nTop + nHeight) > nSrcHeight)
						nTop = nTop - ((nTop + nHeight) - nSrcHeight);
					else if (nTop < 0)
						nTop = 0;
				}
				Mat MatCut = (frame)(Rect(nLeft,nTop,nWidth,nHeight));
				frame = MatCut.clone();
				resize(frame,frame,cv::Size(960,540),0,0,CV_INTER_CUBIC);
				if(bReverse)
					flip(frame,frame,-1);

				ffmpeg.EncodePushImage(frame.data);
			}
			vc.release();
		}
		ffmpeg.CloseEncode();

		SendLog(5,strLog+_T("Making Finish!"));
	}
	else
		SendLog(0,strLog+_T("Encoding Fail!"));

	FILE *fp;

	char filename[255] = {0,};	
	sprintf(filename, "%S", strSavePath);

	fp = fopen(filename, "rb");

	if(fp == NULL)
	{
		strLog.Format(_T("[#NETWORK] Not Exist File [%s]"), strPath);
		SendLog(0,strLog);
		return;
	}

	strLog.Format(_T("[#NETWORK] Send File [%s]"), strPath);
	//SendLog(1,strLog);

	fseek(fp, 0L, SEEK_END);
	int nLength = ftell(fp);
	char* buf = new char[nLength];
	fseek(fp, 0, SEEK_SET);
	fread(buf, sizeof(char), nLength, fp);

	CString *pPath = new CString;
	pPath->Format(_T("%s"),strPath);

	//Making Finish..!
	ESMEvent* pMsg = new ESMEvent();
	pMsg->message = WM_ESM_NET_MULTIVIEW_FINISH;
	pMsg->pParam = (LPARAM)pPath;
	pMsg->pDest = (LPARAM)buf;
	pMsg->nParam1 = pView->nMovIdx;
	if(bEncode && bMakingFinish)
		pMsg->nParam2 = TRUE;
	else
		pMsg->nParam2 = FALSE;

	pMsg->nParam3 = nLength;
	::SendMessage(((CESMMovieMgr*)m_pParent)->GetParentWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);

	if(pView)
	{
		delete pView;
		pView = NULL;
	}
}

void CESMMovieThreadManager::TCPSendData(CString strPath)
{
	FILE *fp;
	CString strLog;

	char filename[255] = {0,};	
	sprintf(filename, "%S", strPath);

	fp = fopen(filename, "rb");

	if(fp == NULL)
	{
		strLog.Format(_T("[#NETWORK] Not Exist File [%s]"), strPath);
		SendLog(0,strLog);
		return;
	}

	strLog.Format(_T("[#NETWORK] Send File [%s]"), strPath);
	//SendLog(1,strLog);

	fseek(fp, 0L, SEEK_END);
	int nLength = ftell(fp);
	char* buf = new char[nLength];
	fseek(fp, 0, SEEK_SET);
	fread(buf, sizeof(char), nLength, fp);

	CString *pPath = new CString;
	pPath->Format(_T("%s"),strPath);

	ESMEvent* pMsg = new ESMEvent();
	pMsg->message = WM_ESM_NET_MUX_TCP_SEND;
	pMsg->pParam = (LPARAM)pPath;
	pMsg->pDest = (LPARAM)buf;
	pMsg->nParam3 = nLength;
	::SendMessage(((CESMMovieMgr*)m_pParent)->GetParentWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);

	fclose(fp);
}