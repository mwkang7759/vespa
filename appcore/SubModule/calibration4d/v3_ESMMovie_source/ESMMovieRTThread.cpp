﻿////////////////////////////////////////////////////////////////////////////////
//
//	ESMMovieRTThread.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-05-28
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMMovieRTThread.h"
#include "ESMMovieMgr.h"
#include "FFmpegManager.h"
#include "ESMFunc.h"
#include "DllFunc.h"
#include "ESMGpuDecode.h"
#include "cudaKernel.cuh"
#include "opencv2\xphoto.hpp"
#include "ESMMovieFileOperation.h"
#include "ESMIni.h"

//#define LOGO_ZOOM
#define PHI 3.1415926535897932
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
using namespace cv::xphoto;

IMPLEMENT_DYNCREATE(CESMMovieRTThread, CESMMovieThread)

// CESMMovieEncoder
CESMMovieRTThread::CESMMovieRTThread()
{
	m_Index = 0;
	m_bSetProperty = FALSE;
	m_bGPUUse = FALSE;
	nWidth = 1920;
	nHeight = 1080;

	nReWidth = 1920;
	nReHeight = 1080;

	nImageSize = 6220800;
	nYUVSize    = 3110400;

	nMarginX = 0;
	nMarginY = 0;
	for(int i = 0 ; i < 100; i++)
	{
		m_ArrstrPath[i] = _T("");
	}
	//Dummy
	//int _nPrevIdx = 0;
	//for(int i = 0 ; i < 10 ; i++)
	//{
	//	if(i == 3)
	//		continue;
	//	CString strPath;
	//	m_ArrstrPath[i].Format(_T("M:\\Movie\\20503_%d.mp4"),i);//= _T("M:\\Movie\\1.mp4");

	//	int nIdx = m_ArrstrPath[i].ReverseFind('\\') + 1;
	//	CString strTemp = m_ArrstrPath[i].Right(m_ArrstrPath[i].GetLength() - nIdx);
	//	strTemp.Replace(_T(".mp4"), _T(""));
	//	nIdx = strTemp.ReverseFind('_') + 1;
	//	CString strCam = strTemp.Left(5);
	//	CString strIdx = strTemp.Right(strTemp.GetLength() - nIdx);
	//	int nCurrent = _ttoi(strIdx);

	//	if(nCurrent - _nPrevIdx > 1)
	//	{
	//		strPath.Format(_T("M:\\Movie\\%s_%d.mp4"), strCam, ++_nPrevIdx);
	//		//_nPrevIdx = _nPrevIdx + 1;
	//		TRACE(strPath);
	//		TRACE(_T("\r\n"));
	//	}//else
	//	{
	//		_nPrevIdx = nCurrent;
	//	}

	//	TRACE(m_ArrstrPath[i]);
	//	TRACE(_T("\r\n"));
	//}
	m_bOpenClose = FALSE;

	m_nDecoding = FALSE;
	bAdjust = FALSE;

	m_nFrameinfoCnt = 0;
	nDecodingCnt	= 0;
	nAdjustCnt		= 0;
}

CESMMovieRTThread::~CESMMovieRTThread()
{	
}

//--------------------------------------------------------------------------
//!@brief	RUN THREAD
//!@param   
//!@return	
//--------------------------------------------------------------------------
int CESMMovieRTThread::Run(void)
{
	CString strPath;

	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE) _beginthreadex(NULL, 0, RTMakeGPUFrame, (void *)this, 0, NULL);
	CloseHandle(hSyncTime);

	HANDLE hSyncTime1 = NULL;
	hSyncTime1 = (HANDLE) _beginthreadex(NULL, 0, RTMakeGPUAdjust, (void *)this, 0, NULL);
	CloseHandle(hSyncTime1);
	
	int _nPrevIdx = 0;
	UINT nCnt = 0;
	CESMMovieFileOperation _file;

#if 1
	while(1)
	{
		if(m_strArrPath.GetCount() == 0)
		{
			Sleep(10);
			continue;
		}
		if(m_strArrPath.GetCount() >= 1 && m_strArrPath.GetCount() == _nPrevIdx+1)
		{
			Sleep(10);
			
			m_ArrstrPath[m_Index++] = GetRTSendPath(_nPrevIdx);
			
			_nPrevIdx++;

			if (m_Index > 99)
			{
				m_Index = 0;
			}

			nCnt = 0;
			if(m_bOpenClose == FALSE)
			{
				m_bOpenClose = TRUE;

				ESMEvent* pMsg	= new ESMEvent;
				pMsg->message = WM_MAKEMOVIE_GPU_FILE_OPEN_CLOSE;
				pMsg->nParam1	= (int)m_bOpenClose;
				::SendMessage(m_pWnd->m_hWnd , WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
			}
		}	
		else
		{
			nCnt++;
			Sleep(10);
			if(nCnt > 250)
			{
				if(m_bOpenClose == TRUE)
				{
					m_bOpenClose = FALSE;

					ESMEvent* pMsg	= new ESMEvent;
					pMsg->message = WM_MAKEMOVIE_GPU_FILE_OPEN_CLOSE;
					pMsg->nParam1	= (int)m_bOpenClose;
					::SendMessage(m_pWnd->m_hWnd , WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);

					_file.Delete(_T("M:\\Movie"), FALSE);

					_nPrevIdx = 0;
					nCnt = 0;
					m_strArrPath.RemoveAll();
				}
			}
			TRACE(_T("ELSE\n"));
		}
	}
#else
	/*
	HANDLE hSyncTime1 = NULL;
	hSyncTime1 = (HANDLE) _beginthreadex(NULL, 0, RTMakeGPUFrame, (void *)this, 0, NULL);
	CloseHandle(hSyncTime1);
	*/
	while(1)
	{
		//strPath = FileReadDone();
		strPath.Format(_T("M:\\Movie\\%s_%d.mp4"), m_strDSC, _nPrevIdx);
		//_file.IsFileExist(strPath)
		//if(strPath.IsEmpty())
		if(_file.IsFileExist(strPath) == FALSE)
		{
			sleep(10);
			if(++nCnt > 500)	//Sleep 10 기준 75번 반복 = 7.5초 GAP
			{
				if(m_bOpenClose == TRUE)
				{
					m_bOpenClose = FALSE;
						
					ESMEvent* pMsg	= new ESMEvent;
					pMsg->message = WM_MAKEMOVIE_GPU_FILE_OPEN_CLOSE;
					pMsg->nParam1	= (int)m_bOpenClose;
					::SendMessage(m_pWnd->m_hWnd , WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
					
					_file.Delete(_T("M:\\Movie"), FALSE);
					
					_nPrevIdx = 0;
				}
			}
		}
		else
		{
			sleep(10);
			//TEST JHHAN

			
			/*int nIdx = strPath.ReverseFind('\\') + 1;
			CString strTemp = strPath.Right(strPath.GetLength() - nIdx);
			strTemp.Replace(_T(".mp4"), _T(""));
			nIdx = strTemp.ReverseFind('_') + 1;
			CString strCam = strTemp.Left(5);
			CString strIdx = strTemp.Right(strTemp.GetLength() - nIdx);
			int nCurrent = _ttoi(strIdx);*/

			//if(_nPrevIdx == 0 && nCurrent != 0)
			//{
			//	CString strTmp;
			//	strTemp.Format(_T("M:\\Movie\\%s_%d.mp4"), strCam, _nPrevIdx++);
			//	m_ArrstrPath[m_Index++] = strTemp;

			//	if(nCurrent - _nPrevIdx > 1)
			//	{
			//		int nCnt = nCurrent - _nPrevIdx;
			//		for(int i = 0; i < nCnt; i++)
			//		{
			//			CString strTmp;
			//			strTemp.Format(_T("M:\\Movie\\%s_%d.mp4"), strCam, _nPrevIdx++);
			//			m_ArrstrPath[m_Index++] = strTemp;
			//			//_nPrevIdx = _nPrevIdx + 1;
			//			TRACE(strTemp);
			//			TRACE(_T("\r\n"));
			//			strTmp.Format(_T("%s : [%d] - [%d]"), strTemp, nCurrent, _nPrevIdx-1);
			//			SendLog(1,strTmp);
			//		}
			//	}
			//}
			//else 
			//if(nCurrent - _nPrevIdx > 1)
			//{
			//	int nCnt = nCurrent - _nPrevIdx;
			//	for(int i = 1; i < nCnt; i++)
			//	{
			//		CString strTmp;
			//		strTemp.Format(_T("M:\\Movie\\%s_%d.mp4"), strCam, ++_nPrevIdx);
			//		m_ArrstrPath[m_Index++] = strTemp;
			//		//_nPrevIdx = _nPrevIdx + 1;
			//		TRACE(strTemp);
			//		TRACE(_T("\r\n"));
			//		strTmp.Format(_T("%s : [%d] - [%d]"), strTemp, nCurrent, _nPrevIdx-1);
			//		SendLog(1,strTmp);
			//	}
			//}
			/*else if(_nPrevIdx == 0 && nCurrent == 1)
			{
				int nCnt = nCurrent - _nPrevIdx;
				for(int i = 0; i < nCnt; i++)
				{
					CString strTmp;
					strTemp.Format(_T("M:\\Movie\\%s_%d.mp4"), strCam, i);
					m_ArrstrPath[m_Index++] = strTemp;
				}
			}*//*else if(_nPrevIdx == 0 && nCurrent > 1)
			{
				int nCnt = nCurrent - _nPrevIdx;
				for(int i = 0; i < nCnt; i++)
				{
					CString strTmp;
					strTemp.Format(_T("M:\\Movie\\%s_%d.mp4"), strCam, i);
					m_ArrstrPath[m_Index++] = strTemp;
				}
			}*/

			//_nPrevIdx = nCurrent;
			

			/*if(_nPrevIdx == 0 && nCurrent == 0)
			{
				_nPrevIdx = 1;
			}*/

			TRACE(strPath);
			TRACE(_T("\r\n"));

			//if(m_ArrstrPath[m_Index] != strPath)
			
			m_ArrstrPath[m_Index++] = strPath;
			_nPrevIdx++;

			if (m_Index > 99)
			{
				m_Index = 0;
			}
			nCnt = 0;
			if(m_bOpenClose == FALSE)
			{
				m_bOpenClose = TRUE;

				ESMEvent* pMsg	= new ESMEvent;
				pMsg->message = WM_MAKEMOVIE_GPU_FILE_OPEN_CLOSE;
				pMsg->nParam1	= (int)m_bOpenClose;
				::SendMessage(m_pWnd->m_hWnd , WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
			}
			
		}
	}
#endif
	return TRUE;
}

CString CESMMovieRTThread::GetDSCIDFromPath(CString strPath)
{
	int nFind = strPath.ReverseFind('\\') + 1;
	CString csFile = strPath.Right(strPath.GetLength()-nFind);
	csFile.Trim();
	//csFile.Trim(_T("\\"));
	nFind = csFile.Find('.');
	CString csCam = csFile.Left(nFind);
	csCam.Trim();
	nFind = csCam.Find('_');

	CString strCamID = csCam.Mid(0, nFind);

	return strCamID;
}

CString CESMMovieRTThread::GetSecIdxFromPath(CString strPath)
{
	int nFind = strPath.ReverseFind('\\') + 1;
	CString csFile = strPath.Right(strPath.GetLength()-nFind);
	csFile.Trim();
	//csFile.Trim(_T("\\"));
	nFind = csFile.Find('.');
	CString csCam = csFile.Left(nFind);
	csCam.Trim();
	nFind = csCam.ReverseFind('_') + 1;

	CString strSecIdx = csCam.Right(csCam.GetLength()-nFind);
	
	return strSecIdx;
}

CString CESMMovieRTThread::FileReadDone()
{
	CString strMovie = _T("");
	CString strFile;

//#ifdef _INI_4DA_LOCAL
	//네트워크 접근 경로 - 4DA ini 저장
	strFile.Format(_T("\\\\%s\\movie\\%s.ini"),m_strIP, m_strDSC);
//#endif
//#ifdef _INI_4DP_NET_RAM
	//로컬 경로 - 4DP ini 저장
	//strFile.Format(_T("%s\\%s.ini"),_T("M:\\movie"),strSourceDSC);
//#endif
//#ifdef _INI_4DP_NET_RT
	//로컬 경로 - 4DP RT
	//strFile.Format(_T("C:\\Program Files\\ESMLab\\4DMaker\\RT\\RT.ini"));
//#endif

	CESMIni ini;
	if(ini.SetIniFilename (strFile))
	{
		CString strFlag, strPath;
		strFlag = ini.GetString(_T("RT"), _T("FLAG"), _T(""));
		strPath = ini.GetString(_T("RT"), _T("PATH"), _T(""));
		
		if(strFlag == _T("1") && strPath.IsEmpty() == FALSE)
		{
			ini.DeleteSection(_T("RT"));
			strMovie = strPath;
		}

	}
	return strMovie;
}

CCudaFunc cuKernel;

unsigned WINAPI CESMMovieRTThread::RTMakeGPUFrame(LPVOID param)
{
	CESMMovieRTThread *pMgr = (CESMMovieRTThread*)param;
	int nPos = 0;
	vector<MakeFrameInfo>* pArrFrameInfo;
	int nImageSize = pMgr->nImageSize;
	int nYUVSize = pMgr->nYUVSize;
	int nWidth = pMgr->nWidth;
	int nHeight = pMgr->nHeight;
	int dec = 0;
	while(1)
	{

		if(pMgr->m_ArrstrPath[nPos].GetLength())
		{
			//strPath = pMgr->m_ArrstrPath[nPos];//유니코드
			pArrFrameInfo = new vector<MakeFrameInfo>;
			int SecIndex;
			for(int i =0; i < 30; i++)
			{
				MakeFrameInfo stMakeFrameInfo;
				_stprintf(stMakeFrameInfo.strFramePath, pMgr->m_ArrstrPath[nPos]);//유니코드
				stMakeFrameInfo.nFrameIndex = i;
				stMakeFrameInfo.nFrameSpeed = 33;
				stMakeFrameInfo.bGPUPlay = TRUE;
				CString strCamID = pMgr->GetDSCIDFromPath(pMgr->m_ArrstrPath[nPos]);
				CString strSecIdx = pMgr->GetSecIdxFromPath(pMgr->m_ArrstrPath[nPos]);

				_stprintf(stMakeFrameInfo.strDSC_ID, _T("%s"), strCamID);//유니코드
				_stprintf(stMakeFrameInfo.strCameraID, _T("%s"), strCamID);
				stMakeFrameInfo.nSecIdx = _ttol(strSecIdx);			//jhhan 16-11-29 SecIdx
				SecIndex = _ttoi(strSecIdx);

				stMakeFrameInfo.pYUVImg = NULL;
				stMakeFrameInfo.Image = NULL;
				pArrFrameInfo->push_back(stMakeFrameInfo);
			}
		}
		else
		{
			Sleep(5);
			dec++;
			if(dec>1000)
			{
				dec = 0;
				continue;
			}
			continue;
		}

		CString strPath = pMgr->m_ArrstrPath[nPos];
		CT2CA pszConvertedAnsiString (strPath);
		string strDst(pszConvertedAnsiString);	

// 		CString strDscId;
// 		int nIndex = strPath.ReverseFind('\\') + 1;
// 		CString strTpPath = strPath.Right(strPath.GetLength() - nIndex);
// 		strDscId = strTpPath.Left(5);

		VideoCapture vc;
		vc.open(strDst);
		if(!vc.isOpened())
		{
			Sleep(50);
			vc.open(strDst);
			if(!vc.isOpened())
			{
				Sleep(100);
				vc.open(strDst);
				if(!vc.isOpened())
				{
					CString strLog;
					strLog.Format(_T("[File Error] ---------- %s"),strPath);
					SendLog(1,strLog);

					pMgr->m_bGPUUse = FALSE;
					pMgr->m_ArrstrPath[nPos++] = _T("");
					if(nPos > 99)
						nPos = 0;

					continue;
				}
			}
		}
		int pCnt = 0;
		int nSec = 0;

		DecodingFrameInfo* DecodingInfo = new DecodingFrameInfo;
		DecodingInfo->pArrFrameInfo = pArrFrameInfo;
		DecodingInfo->pArrMatInfo = new vector<Mat>;
		int start = GetTickCount();
		char strSave[100];

		Mat frame;
		//pMgr->nDecodingCnt++;

		//while(1)
		//{
		//	if(pMgr->nDecodingCnt >10 && pMgr->nAdjustCnt <10)
		//	{
		//		Sleep(10);
		//		SendLog(5,_T("Waiting....."));
		//	}
		//	else
		//	{
		//		CString strLog;
		//		strLog.Format(_T("Decoding Cnt %d <->Adjust Cnt %d[DECODING]"),pMgr->nDecodingCnt,pMgr->nAdjustCnt);
		//		SendLog(5,strLog);
		//		break;
		//	}
		//}
		while(1)
		{
			vc>>frame;
			if(frame.empty())
			{	
				pMgr->m_DecodingMain.push_back(DecodingInfo);
				pMgr->m_nDecoding = nPos;
				nSec ++;
				break;
			}
			DecodingInfo->pArrMatInfo->push_back(frame.clone());

			pCnt++;
		}
		
		int end = GetTickCount();
		
		CString str;
		str.Format(_T("[%d]Decoding Time %dms."),pArrFrameInfo->at(0).nSecIdx,end-start);
		SendLog(5,strPath);	
		SendLog(5,str);

#if 0
		char strOri[100],strAdj[100];
		int DscIndex = _ttoi(strDscId);
		sprintf(strOri,"M:\\(Ori)%d_%d.jpg",DscIndex,nSecIndex);
		sprintf(strAdj,"M:\\(Adj)%d_%d.jpg",DscIndex,nSecIndex);
#endif
//		while(1)
//		{
//			vc>>frame;
//
//			if(frame.empty()) break;
//			
//			//Revision
//			cuKernel.cuDipAll(frame,pMgr->m_CalcData.AdjX,pMgr->m_CalcData.AdjY,Resize);
//		
//			result = (Resize)(cv::Rect(pMgr->m_CalcData.nLeft,pMgr->m_CalcData.nTop,nWidth,nHeight));
//			srcImage = result.clone();
//#if 0
//			if(pCnt == 0)
//			{
//				imwrite(strOri,frame);
//				imwrite(strAdj,srcImage);
//			}
//#endif
//			MakeFrameInfo *pFrame = &(pArrFrameInfo->at(pCnt));
//
//			pFrame->pYUVImg = new BYTE[nYUVSize];
//
//			pFrame->nHeight = nHeight;
//			pFrame->nWidth = nWidth;
//			pFrame->nImageSize = nYUVSize;
//			cvtColor(srcImage,YUVImage,CV_BGR2YUV_I420);
//
//			memcpy(pFrame->pYUVImg,YUVImage.data,nYUVSize);
//
//			pCnt++;
//		}
//		destroyAllWindows();
//		pMgr->m_bGPUUse = FALSE;

		pMgr->m_ArrstrPath[nPos++] = _T("");
		if(nPos > 99)
		{
			nPos = 0;
		}
	}

	return 0;
}

void CESMMovieRTThread::SetAdjustData(stAdjustInfo *AdjustData)
{
	m_AdjustData.push_back(*AdjustData);
}

void CESMMovieRTThread::SetMargin(int nMarX,int nMarY)
{
	nMarginX = nMarX;
	nMarginY = nMarY;
}

void CESMMovieRTThread::CalcAdjust(stAdjustInfo AdjustData)
{
	//Angle
	m_CalcData.angle = AdjustData.AdjAngle;

	double dbAngleAdjust = -1 * (m_CalcData.angle + 90);
	double theta =  (-dbAngleAdjust)*(PHI/180);
	if(m_CalcData.angle == 0) theta = 0;
	m_CalcData.theta = theta;
	double dRatio = 1;

	m_CalcData.RotX  = Round(AdjustData.AdjptRotate.x * dRatio);
	m_CalcData.RotY  = Round(AdjustData.AdjptRotate.y * dRatio);

	//Move
	m_CalcData.AdjX  = Round(AdjustData.AdjMove.x * dRatio);
	m_CalcData.AdjY  = Round(AdjustData.AdjMove.y * dRatio);

	////Margin
	nMarginX *= dRatio;
	nMarginY *= dRatio;

	double dbMarginScale = 1 / ( (double)( nWidth - nMarginX * 2) /(double) nWidth);
	int nLeft = (int)((nWidth  * dbMarginScale - nWidth)/2);
	int nTop  = (int)((nHeight * dbMarginScale - nHeight)/2);
	m_CalcData.nLeft = nLeft;
	m_CalcData.nTop = nTop;
	
	m_CalcData.nH = nHeight*dbMarginScale;
	m_CalcData.nW = nWidth*dbMarginScale;

	nReHeight = m_CalcData.nH;
	nReWidth = m_CalcData.nW;

	CString strAdjLog;
	//int DscIndex = _ttoi(AdjustData.strDSC);
	strAdjLog.Format(_T("[%s][Angle%f->(%f,%f)],Move[(%f,%f)],Margin[(%d,%d)]"),AdjustData.strDSC,AdjustData.AdjAngle,
		AdjustData.AdjptRotate.x,AdjustData.AdjptRotate.y,AdjustData.AdjMove.x,AdjustData.AdjMove.y,nMarginX,nMarginY);
	SendLog(5,strAdjLog);
	//SendLog(5,_T("Set Adjust Finish!"));
}

int CESMMovieRTThread::Round(double dData)
{
	int nResult = 0;

	if( dData > 0.0)
		nResult = int(dData + 0.5);
	else if( dData < 0.0)
		nResult = int(dData - 0.5);
	else
		nResult = 0;

	return nResult;
}

stAdjustInfo CESMMovieRTThread::GetAdjustData(CString strDscId)
{
	stAdjustInfo AdjustData;

	for(int i =0 ;i < m_AdjustData.size(); i++)
	{
		if(m_AdjustData.at(i).strDSC == strDscId)
		{
			AdjustData = m_AdjustData.at(i);
			break;
		}
		//AdjustData = (m_AdjustData.at(i));
		//if( AdjustData.strDSC == strDscId )
		//{
		//	break;
		//}
	}
	return AdjustData;
}

unsigned WINAPI CESMMovieRTThread::RTMakeGPUAdjust(LPVOID param)
{
	CESMMovieRTThread *pMgr = (CESMMovieRTThread*)param;
	int nHeight = pMgr->nHeight;
	int nWidth = pMgr->nWidth;

	Mat result(nHeight,nWidth,CV_8UC3);
	Mat srcImage(nHeight,nWidth,CV_8UC3);
	Mat YUVImage(nHeight+nHeight/2,nWidth,CV_8UC1);
	Mat frame;
	Mat Resize;
	int nYUVSize = pMgr->nYUVSize;
	int dec = 0;
	while(1)
	{
		if(pMgr->m_DecodingMain.size())
		{
			int nStart = GetTickCount();

			DecodingFrameInfo * pInfo = pMgr->m_DecodingMain.at(0);

			//Declare Only One time.
			if(!pMgr->m_bSetProperty)
			{
				CString strCamID = pMgr->GetDSCIDFromPath(pInfo->pArrFrameInfo->at(0).strFramePath);
				stAdjustInfo AdjustData = pMgr->GetAdjustData(strCamID);
				pMgr->CalcAdjust(AdjustData);

				CalcValue CalcData = pMgr->m_CalcData;
				cuKernel.cuSetProperty(nHeight,nWidth,CalcData.nH,CalcData.nW,
					CalcData.theta,CalcData.RotX,CalcData.RotY,AdjustData.AdjSize,CalcData.AdjX,CalcData.AdjY);
				Resize.create(pMgr->m_CalcData.nH,pMgr->m_CalcData.nW,CV_8UC3);

				pMgr->m_bSetProperty = TRUE;

			}

			//Mat Resize(pMgr->m_CalcData.nH,pMgr->m_CalcData.nW,CV_8UC3);
			char strPath[100];
			for(int i=0; i<pInfo->pArrMatInfo->size(); i++)
			{
				Mat frame = pInfo->pArrMatInfo->at(i).clone();

				//cuda 변경
				//cuKernel.cuDipAll(frame,pMgr->m_CalcData.AdjX,pMgr->m_CalcData.AdjY,Resize);
				cuKernel.cuDipAll(frame,Resize);
				result = (Resize)(cv::Rect(pMgr->m_CalcData.nLeft,pMgr->m_CalcData.nTop,nWidth,nHeight));
				srcImage = result.clone();

				pInfo->pArrFrameInfo->at(i).pYUVImg = new BYTE[nYUVSize];
				pInfo->pArrFrameInfo->at(i).nImageSize = nYUVSize;
				pInfo->pArrFrameInfo->at(i).nWidth = nWidth;
				pInfo->pArrFrameInfo->at(i).nHeight = nHeight;
				cvtColor(srcImage,YUVImage,CV_BGR2YUV_I420);
				memcpy(pInfo->pArrFrameInfo->at(i).pYUVImg,YUVImage.data,nYUVSize);

				/*if(i==0)
				{
				int nMovieIdx = pInfo->pArrFrameInfo->at(i).nSecIdx;
				sprintf(strPath,"M:\\%d_%d.jpg",nMovieIdx,i);
				imwrite(strPath,srcImage);
				}*/
			}
			ESMEvent* pMsg	= new ESMEvent;
			pMsg->message = WM_MAKEMOVIE_GPU_FILE_END;
			pMsg->pParam	= (LPARAM)pInfo->pArrFrameInfo;
			::SendMessage(pMgr->m_pWnd->m_hWnd , WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);

			int nEnd = GetTickCount();

			CString str;
			str.Format(_T("[%d]Adjust : %d"),pInfo->pArrFrameInfo->at(0).nSecIdx,nEnd-nStart);
			//SendLog(5,pInfo->pArrFrameInfo->at(0).strFramePath);
   			SendLog(5,str);
			//pMgr->nAdjustCnt++;

			//if(pMgr->nAdjustCnt >9)
			//{
			//	CString strLog;
			//	strLog.Format(_T("Decoding Cnt %d <->Adjust Cnt %d[ADJUST]"),pMgr->nDecodingCnt,pMgr->nAdjustCnt);

			//	SendLog(5,strLog);
			//	pMgr->nAdjustCnt = 0;
			//	pMgr->nDecodingCnt = 0;
			//}

			//for(int i = 0 ; i < pInfo->pArrMatInfo->size();i++)
			//{
			//	//SendLog(5,_T("Release Arr Image"));
			//	pInfo->pArrMatInfo->at(i).release();
			//}
			pInfo->pArrMatInfo->clear();
			pInfo->pArrMatInfo = NULL;
			pMgr->m_DecodingMain.erase(pMgr->m_DecodingMain.begin());
		}
		/*else
		{
			Sleep(10);
			dec++;
			if(dec > 500)
			{
				dec = 0;
				continue;
			}
		}*/
		Sleep(10);

		//for(int i = 0 ;i < 500; i++)
		//{
		//	Mat Resize(pMgr->m_CalcData.nH,pMgr->m_CalcData.nW,CV_8UC3);
		//	if(i==pMgr->m_nDecoding)
		//	{
		//		int nSecIndex = pMgr->m_ArrFrameInfo->at(0).nSecIdx;

		//		for(int i = 0 ; i < pMgr->m_DecodingData->size();i++)
		//		{
		//			//Revision
		//			cuKernel.cuDipAll(pMgr->m_DecodingData->at(i),pMgr->m_CalcData.AdjX,pMgr->m_CalcData.AdjY,Resize);

		//			result = (Resize)(cv::Rect(pMgr->m_CalcData.nLeft,pMgr->m_CalcData.nTop,nWidth,nHeight));
		//			srcImage = result.clone();

		//			pMgr->m_ArrFrameInfo->at(i).pYUVImg = new BYTE[nYUVSize];
		//			cvtColor(srcImage,YUVImage,CV_BGR2YUV_I420);
		//			memcpy(pMgr->m_ArrFrameInfo->at(i).pYUVImg,YUVImage.data,nYUVSize);
		//		}

		//	}
		//	else
		//	{
		//		Sleep(10);
		//		continue;
		//	}
		//}
		//if(pMgr->bDecoding)
		//{
		//	int nSecIndex = pMgr->m_ArrFrameInfo->at(0).nSecIdx;

		//	for(int i = 0 ; i < pMgr->m_DecodingData.size();i++)
		//	{
		//		//Revision
		//		cuKernel.cuDipAll(pMgr->m_DecodingData.at(i),pMgr->m_CalcData.AdjX,pMgr->m_CalcData.AdjY,Resize);

		//		result = (Resize)(cv::Rect(pMgr->m_CalcData.nLeft,pMgr->m_CalcData.nTop,nWidth,nHeight));
		//		srcImage = result.clone();

		//		pMgr->m_ArrFrameInfo->at(i).pYUVImg = new BYTE[nYUVSize];
		//		cvtColor(srcImage,YUVImage,CV_BGR2YUV_I420);
		//		memcpy(pMgr->m_ArrFrameInfo->at(i).pYUVImg,YUVImage.data,nYUVSize);
		//	}

		//	ESMEvent* pMsg	= new ESMEvent;
		//	pMsg->message = WM_MAKEMOVIE_GPU_FILE_END;
		//	pMsg->pParam	= (LPARAM)pMgr->m_ArrFrameInfo;
		//	::SendMessage(pMgr->m_pWnd->m_hWnd , WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);

		//	pMgr->bDecoding = FALSE;
		//	pMgr->m_DecodingData.clear();
		//}
		//else
		//{
		//	Sleep(10);
		//}
	}

	cuKernel.cuFreeProperty();

	return 0;
}