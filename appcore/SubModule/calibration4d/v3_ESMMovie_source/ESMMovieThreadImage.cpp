////////////////////////////////////////////////////////////////////////////////
//
//	ESMMovieThreadImage.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-05-26
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMMovieThreadImage.h"
#include "ESMMovieIni.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CESMMovieThreadImage, CESMMovieThread)


// CESMMovieEncoder
CESMMovieThreadImage::CESMMovieThreadImage(CWinThread* pParent, short nMessage, ESMMovieMsg* pMsg)
:CESMMovieThread(pParent, nMessage, pMsg)
{
	//SetOrder(nMessage);	
	m_bUHDtoFHD = FALSE;
}

CESMMovieThreadImage::~CESMMovieThreadImage()
{
	
}

//--------------------------------------------------------------------------
//!@brief	RUN THREAD
//!@param   
//!@return	
//--------------------------------------------------------------------------
int CESMMovieThreadImage::Run(void)
{
	int nRet = 0;
	if(!WaitOrder((TCHAR* )m_pMsg->pValue1, (TCHAR* )m_pMsg->pValue2, m_pMsg->nParam1))
	{	
		// To Pass Non Nomal Frame
		m_nMessage = ESM_MOVIE_IMAGE_NONFRAME;
	}

// 	if( ((CESMMovieMgr*)m_pParent)->GetMakingStop())
// 		return 0;

	switch(m_nMessage)
	{
	case ESM_MOVIE_IMAGE_RESIZE:
		ResizeImage((TCHAR* )m_pMsg->pValue1, (TCHAR* )m_pMsg->pValue2, m_pMsg->nParam1);
		break;
	case ESM_MOVIE_IMAGE_ROTATE:
		RotateImage((TCHAR* )m_pMsg->pValue1, (TCHAR* )m_pMsg->pValue2,  m_pMsg->nParam1);		
		break;
	case ESM_MOVIE_IMAGE_MOVE:
		MoveImage((TCHAR* )m_pMsg->pValue1, (TCHAR* )m_pMsg->pValue2,  m_pMsg->nParam1);
		break;
	case ESM_MOVIE_IMAGE_CUT:
		CutImage((TCHAR* )m_pMsg->pValue1, (TCHAR* )m_pMsg->pValue2, m_pMsg->nParam1);
		break;
	case ESM_MOVIE_IMAGE_EFFECT:
		EffectImage((TCHAR* )m_pMsg->pValue1, (TCHAR* )m_pMsg->pValue2, (EffectInfo* )m_pMsg->pValue3, m_pMsg->nParam1);
		break;
	case ESM_MOVIE_IMAGE_LOGO:
		InsertLogo((TCHAR* )m_pMsg->pValue1, (TCHAR* )m_pMsg->pValue2, m_pMsg->nParam1, m_pMsg->nParam2);
		break;
	case ESM_MOVIE_IMAGE_NONFRAME:
		break;
	default:
		TRACE(_T("[ERROR] Unknown Message on Thread Codec\n"));
		break;
	}
	if( m_pMsg )
	{
		delete m_pMsg;
		m_pMsg = NULL;
	}
	//-- 2014-05-26 hongsu@esmlab.com
	//-- Should be called FinishThread
	FinishThread();
	return 1;
}

void CESMMovieThreadImage::ResizeImage(TCHAR* pMovieName, TCHAR* pMovieId, int nFrameNum)
{
	ESMMovieData* pMovieData = NULL;
	CString strMovieId, strMovieName;
	strMovieId.Format(_T("%s"), pMovieId);
	strMovieName.Format(_T("%s"), pMovieName);
	pMovieData = ((CESMMovieMgr*)m_pParent)->GetMovieData(strMovieName, strMovieId);
	delete[] pMovieName;
	delete[] pMovieId;

	if( pMovieData == NULL)
		return ;

	stAdjustInfo AdjustData = pMovieData->GetAdjustinfo();

	int nWidth = 0, nHeight = 0;
	nWidth = pMovieData->GetSizeWidth();
	nHeight = pMovieData->GetSizeHight();
	pMovieData->OrderAdd(nFrameNum);
}

void CESMMovieThreadImage::RotateImage(TCHAR* pMovieName, TCHAR* pMovieId, int nFrameNum)
{
	ESMMovieData* pMovieData = NULL;
	CString strMovieId, strMovieName;
	strMovieId.Format(_T("%s"), pMovieId);
	strMovieName.Format(_T("%s"), pMovieName);
	pMovieData = ((CESMMovieMgr*)m_pParent)->GetMovieData(strMovieName, strMovieId);
	delete[] pMovieName;
	delete[] pMovieId;

	if( pMovieData == NULL)
		return ;

	if(pMovieData->GetBufferSize() <= nFrameNum)
		return ;

	stAdjustInfo AdjustData = pMovieData->GetAdjustinfo();
	cuda::GpuMat* pImageMat = pMovieData->GetImage(nFrameNum);

	// Rotate 구현
	int nWidth = pMovieData->GetSizeWidth();
	int nHeight = pMovieData->GetSizeHight();
	int nRotateX = 0, nRotateY = 0, nSize = 0;
	double dSize = 0.0;
	double dRatio = (double)pImageMat->cols / (double)nWidth;
// 	if( pImageMat->cols > MOVIE_OUTPUT_WIDTH)
// 	{
		nRotateX = Round(AdjustData.AdjptRotate.x  * dRatio );	 // 반올림
		nRotateY = Round(AdjustData.AdjptRotate.y  * dRatio );
		//dSize  = ((AdjustData.AdjSize  * dRatio) * 10 + 0.5) / 10;
//	}
	GpuRotateImage(pImageMat, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle);
	
// 	cv::Mat result_host;
// 	pImageMat->download(result_host);
// 	IplImage* pImg;
// 	pImg=&IplImage(result_host);
// 
// 	char strPath[MAX_PATH];
// 	char strtp[MAX_PATH];
// 	WideCharToMultiByte(CP_ACP, NULL, strMovieId, -1, strtp,  10, NULL, FALSE);
// 	sprintf(strPath, "C:\\Test\\Prev%s.bmp", strtp);
	// 	cvSaveImage(strPath, pImg);

	pMovieData->OrderAdd(nFrameNum);
	//pMovieData->SetState(nFrameNum, ESMFRAME_PROCESS_IMGROTATE);
}

void CESMMovieThreadImage::CutImage(TCHAR* pMovieName, TCHAR* pMovieId, int nFrameNum)
{
	ESMMovieData* pMovieData = NULL;
	CString strMovieId, strMovieName;
	strMovieId.Format(_T("%s"), pMovieId);
	strMovieName.Format(_T("%s"), pMovieName);
	pMovieData = ((CESMMovieMgr*)m_pParent)->GetMovieData(strMovieName, strMovieId);
	delete[] pMovieName;
	delete[] pMovieId;

	if( pMovieData == NULL)
		return ;

	if(pMovieData->GetBufferSize() <= nFrameNum)
		return ;

	cuda::GpuMat* pImageMat = pMovieData->GetImage(nFrameNum);
	
 	int nMarginX = imgproc::GetMarginX();
 	int nMarginY = imgproc::GetMarginY();
// 	nMarginX = 20;
// 	nMarginY = 10;
// 	int nWidth = pMovieData->GetSizeWidth();
// 	int nHeight = pMovieData->GetSizeHight();
	int nWidth = pImageMat->cols;
	int nHeight = pImageMat->rows;

	if ( nMarginX < 0 || nMarginX >= nWidth/2 || nMarginY < 0 || nMarginY >= nHeight/2  )
	{
		pMovieData->OrderAdd(nFrameNum);
		return;
	}

	if( pImageMat->cols > MOVIE_OUTPUT_FHD_WIDTH)
	{
		int nModifyMarginX = (nWidth - MOVIE_OUTPUT_FHD_WIDTH) /2;
		int nModifyMarginY = (nHeight - MOVIE_OUTPUT_FHD_HEIGHT) /2;
		cuda::GpuMat pMatCut =  (*pImageMat)(Rect(nModifyMarginX, nModifyMarginY, MOVIE_OUTPUT_FHD_WIDTH, MOVIE_OUTPUT_FHD_HEIGHT));
		pMatCut.copyTo(*pImageMat);
	}
	else
	{
		//GpuMakeMargin(pImageMat, nMarginX, nMarginY);
		double dbMarginScale = 1 / ( (double)( nWidth - nMarginX * 2) /(double) nWidth  );
		cuda::GpuMat* pMatResize = new cuda::GpuMat;
		cuda::resize(*pImageMat, *pMatResize, Size((int)(nWidth*dbMarginScale) ,(int)(nHeight*dbMarginScale) ), 0.0, 0.0 ,INTER_CUBIC );
		int nLeft = (int)((nWidth*dbMarginScale - nWidth)/2);
		int nTop = (int)((nHeight*dbMarginScale - nHeight)/2);
		cuda::GpuMat pMatCut =  (*pMatResize)(Rect(nLeft, nTop, nWidth, nHeight));
		pMatCut.copyTo(*pImageMat);
		delete pMatResize;
	}

	pMovieData->OrderAdd(nFrameNum);
	//pMovieData->SetState(nFrameNum, ESMFRAME_PROCESS_IMGROTATE);
}

void CESMMovieThreadImage::InsertLogo(TCHAR* pMovieName, TCHAR* pMovieId, int nFrameNum, int nDscNum)
{
	ESMMovieData* pMovieData = NULL;
	CString strMovieId, strMovieName;
	strMovieId.Format(_T("%s"), pMovieId);
	strMovieName.Format(_T("%s"), pMovieName);
	pMovieData = ((CESMMovieMgr*)m_pParent)->GetMovieData(strMovieName, strMovieId);
	delete[] pMovieName;
	delete[] pMovieId;

	if( pMovieData == NULL)
		return ;

	if(pMovieData->GetBufferSize() <= nFrameNum)
		return ;

	if( nDscNum > -1)
	{
		stAdjustInfo AdjustData = pMovieData->GetAdjustinfo();
		cuda::GpuMat* pImageMat = pMovieData->GetImage(nFrameNum);

		int nWidth = pMovieData->GetSizeWidth();
		int nHeight = pMovieData->GetSizeHight();

		// insert Logo 구현
		GpuInsertLogo(pImageMat, nDscNum, nWidth, nHeight);
	}
	pMovieData->OrderAdd(nFrameNum);
}

void CESMMovieThreadImage::MoveImage(TCHAR* pMovieName, TCHAR* pMovieId, int nFrameNum)
{
	ESMMovieData* pMovieData = NULL;
	CString strMovieId, strMovieName;
	strMovieId.Format(_T("%s"), pMovieId);
	strMovieName.Format(_T("%s"), pMovieName);
	pMovieData = ((CESMMovieMgr*)m_pParent)->GetMovieData(strMovieName, strMovieId);
	delete[] pMovieName;
	delete[] pMovieId;
	cuda::GpuMat* pImageMat = pMovieData->GetImage(nFrameNum);

	if( pMovieData == NULL)
		return ;

	if(pMovieData->GetBufferSize() <= nFrameNum)
		return ;

	stAdjustInfo AdjustData = pMovieData->GetAdjustinfo();

	int nMoveX = 0, nMoveY = 0;
	int nWidth = pMovieData->GetSizeWidth();
	double dRatio = (double)pImageMat->cols / (double)nWidth;
// 	if( pImageMat->cols > MOVIE_OUTPUT_WIDTH)
// 	{
		nMoveX = Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
		nMoveY = Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
//	}
	GpuMoveImage(pImageMat, nMoveX,  nMoveY);

// 	cv::Mat result_host;
// 	pImageMat->download(result_host);
// 	IplImage* pImg;
// 	pImg=&IplImage(result_host);
// 
// 	char strPath[MAX_PATH];
// 	char strtp[MAX_PATH];
// 	WideCharToMultiByte(CP_ACP, NULL, strMovieId, -1, strtp,  10, NULL, FALSE);
// 	sprintf(strPath, "C:\\Test\\Move%s.bmp", strtp);
// 	cvSaveImage(strPath, pImg);

	pMovieData->OrderAdd(nFrameNum);
	//pMovieData->SetState(nFrameNum, ESMFRAME_PROCESS_IMGMOVE);
}

int CESMMovieThreadImage::Round(double dData)
{
	int nResult = 0;
	if( dData == 0)
		nResult = 0;
	else if( dData > 0.0)
		nResult = int(dData + 0.5);
	else if( dData < 0.0)
		nResult = int(dData - 0.5);

	return nResult;
}

void CESMMovieThreadImage::EffectImage(TCHAR* pMovieName, TCHAR* pMovieId, EffectInfo* pEffectData, int nFrameNum)
{
	ESMMovieData* pMovieData = NULL;
	CString strMovieId, strMovieName;
	strMovieId.Format(_T("%s"), pMovieId);
	strMovieName.Format(_T("%s"), pMovieName);
	pMovieData = ((CESMMovieMgr*)m_pParent)->GetMovieData(strMovieName, strMovieId);
	delete[] pMovieName;
	delete[] pMovieId;

	if (pEffectData->bZoom == FALSE)
	{
		pMovieData->OrderAdd(nFrameNum);
		delete pEffectData;
		return;
	}

	if( pMovieData == NULL)
		return ;

	if(pMovieData->GetBufferSize() <= nFrameNum)
		return ;

	if ( pEffectData->nZoomRatio != 100 )
		GpuZoomImage(pMovieData->GetImage(nFrameNum), pMovieData->GetSizeWidth(), pMovieData->GetSizeHight(), pEffectData->nPosX,  pEffectData->nPosY, pEffectData->nZoomRatio);

	pMovieData->OrderAdd(nFrameNum);

	delete pEffectData;
}

void CESMMovieThreadImage::GpuRotateImage(cuda::GpuMat* gMat, double nCenterX, double nCenterY,  double dScale, double dAngle, BOOL bReverse)
{
	if(dScale == 0)
	{
		dScale = 1;
		dAngle += -90;
	}

	if( dAngle == 0.0 && dScale == 0.0)
		return ;

	Mat rot_mat(cv::Size(2, 3), CV_32FC1);
	cv::Point rot_center( (int)nCenterX, (int)nCenterY);
	if( dScale == 0.0 && dAngle == 0.0 )
		return;

	double dbAngleAdjust;
	if(bReverse) //Reverse Movie
		dbAngleAdjust = -1 * (180);   
	else
		dbAngleAdjust = -1 * (dAngle + 90);   //Normal Movie

	rot_mat = getRotationMatrix2D(rot_center, dbAngleAdjust, dScale);

	//cuda::warpAffine(*gMat,d_rotate, rot_mat, Size(2500, 1500), CV_INTER_CUBIC+CV_WARP_FILL_OUTLIERS);
	cuda::GpuMat d_rotate;

	//-- 2014-07-16 hongsu@esmlab.com
	//-- ? CvPoint2D32f srcTri[3], dstTri[3];
	cuda::warpAffine(*gMat, d_rotate, rot_mat, Size(gMat->cols, gMat->rows), CV_INTER_CUBIC);
	d_rotate.copyTo(*gMat);

	//cvReleaseMat(&rot_mat);

}

void CESMMovieThreadImage::GpuMoveImage(cuda::GpuMat* gMat, int nX, int nY)
{
	cuda::GpuMat gMatCut, gMatPaste;
	int nCutTop,nCutHeight,nCutLeft,nCutWidth;
	int nPasteTop=0,nPasteBottom=0,nPasteLeft=0,nPasteRight=0;

	if ( nX > 0 )
	{
		nCutLeft = 0;
		nCutWidth = gMat->cols - nX;

		nPasteLeft = nX;
	}
	else
	{
		nCutLeft = -nX;
		nCutWidth = gMat->cols + nX;

		nPasteRight = -nX;
	}

	if ( nY > 0 )
	{
		nCutTop = 0;
		nCutHeight = gMat->rows - nY;

		nPasteTop = nY;
	}
	else
	{
		nCutTop = -nY;
		nCutHeight = gMat->rows + nY;

		nPasteBottom = -nY;
	}

	gMatCut = (*gMat)(Rect(nCutLeft, nCutTop,nCutWidth, nCutHeight));
	cuda::copyMakeBorder(gMatCut, gMatPaste, nPasteTop,nPasteBottom,nPasteLeft,nPasteRight, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}

void CESMMovieThreadImage::GpuMakeMargin(cuda::GpuMat* gMat, int nX, int nY)
{
	cuda::GpuMat gMatCut, gMatPaste; 
	int nHeight,nWidth;

	nWidth = gMat->cols - nX*2;
	nHeight = gMat->rows - nY*2;


	gMatCut = (*gMat)(Rect(nX, nY,nWidth, nHeight));
	cuda::copyMakeBorder(gMatCut, gMatPaste, nY,nY,nX,nX, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}

//CMiLRe 20151020 VMCC Effect(ZOOM) 기능 추가
void CESMMovieThreadImage::GpuZoomImage(cuda::GpuMat* gMat, int nRotateX, int nRotateY, int nX, int nY, int nRatio, int nOutputWidth, int nOutputHeight)
{
	double dScale = (double)nRatio / 100.0;
	cuda::GpuMat gMatCut, gMatResize;

	int nLeft = (int) (nX- gMat->cols/dScale/2);
	int nTop = (int) (nY - gMat->rows/dScale/2);
	int nWidth = (int) (gMat->cols/dScale - 1);
	int nHeight = (int) (gMat->rows/dScale - 1);

	cuda::GpuMat pMatCut =  (*gMat)(Rect(nLeft, nTop, nWidth, nHeight));

	cuda::resize(pMatCut, gMatResize, Size(nOutputWidth ,nOutputHeight ), 0.0, 0.0 ,INTER_CUBIC );
	gMatResize.copyTo(*gMat);
}

void CESMMovieThreadImage::GpuZoomImage(cuda::GpuMat* gMat, int nWidth, int nHeight, int nX, int nY, int nRatio)
{
	double dScale = (double)nRatio / 100.0;
	cuda::GpuMat gMatCut, gMatResize;

	int nLeft = (int) (nX- nWidth/dScale/2);
	int nTop = (int) (nY - nHeight/dScale/2);

	cuda::GpuMat pMatCut =  (*gMat)(Rect(nLeft, nTop, (int) (nWidth/dScale - 1), (int) (nHeight/dScale - 1)));
	cuda::resize(pMatCut, gMatResize, Size(nWidth ,nHeight ), 0.0, 0.0 ,INTER_CUBIC );
	gMatResize.copyTo(*gMat);

	/*
	int nLeft = nX*dScale - nWidth/2;
	int nTop = nY*dScale - nHeight/2;

	cuda::resize(*gMat, gMatResize, Size(nWidth*dScale ,nHeight*dScale ), 0.0, 0.0 ,INTER_CUBIC );

	if( nLeft < 0 )
		nLeft = 0;
	if ( nTop < 0 )
		nTop = 0;
	if ( nLeft > nWidth*dScale - nWidth/2 )
		nLeft = nWidth*dScale - nWidth/2;
	if ( nTop > nHeight*dScale - nHeight/2 )
		nTop = nHeight*dScale - nHeight/2;

	cuda::GpuMat pMatCut =  (gMatResize)(Rect(nLeft, nTop, nWidth, nHeight));
	pMatCut.copyTo(*gMat);
	*/
}

void CESMMovieThreadImage::GpuInsertLogo(cuda::GpuMat* gMat, int nDscNum, int nWidth, int nHeight)
{
	CString strBannerPath;
	strBannerPath.Format(_T("C:\\Program Files\\ESMLab\\4DMaker\\img\\05.Banner\\%d.png"), nDscNum + 1);
	char pBannerPath[MAX_PATH] = {0};
	wcstombs(pBannerPath, strBannerPath, MAX_PATH);
	if(::PathFileExists(strBannerPath) == FALSE)
		return ;


	cv::Mat tpMat;
	gMat->download(tpMat);
	IplImage* pImg;
	pImg=&IplImage(tpMat);

	IplImage* pBanner = cvLoadImage(pBannerPath, CV_LOAD_IMAGE_UNCHANGED);
	IplImage* pResult = cvCreateImage(cvSize(gMat->cols, gMat->rows), pImg->depth, pImg->nChannels);

	OverlayImage(pImg, pBanner, pResult, cvPoint(0,0));
	tpMat = cvarrToMat(pResult);
	gMat->upload(tpMat);
}

void CESMMovieThreadImage::GpuInsertLogo2(Mat* tpMat, int nDscNum, BOOL b3DLogo)
{
	CString strBannerPath;

	WORD wVersionRequested;
	WSADATA wsaData;
	char name[255];
	CString ip; // 여기에 lcoal ip가 저장됩니다.
	PHOSTENT hostinfo;
	wVersionRequested = MAKEWORD( 2, 0 );
	if ( WSAStartup( wVersionRequested, &wsaData ) == 0 ) 
	{
		if( gethostname ( name, sizeof(name)) == 0)
		{
			if((hostinfo = gethostbyname(name)) != NULL)
			{
				ip = inet_ntoa (*(struct in_addr *)*hostinfo->h_addr_list);
			}
		}      
		WSACleanup( );
	} 
	CString ipaddr;
	AfxExtractSubString( ipaddr, ip, 3, '.');
	
	
	//INI 읽어서 File Server 경로얻기
	CESMMovieIni ini;
	CString strInfoConfig;
	strInfoConfig.Format(_T("C:\\Program Files\\ESMLab\\4DMaker\\config\\4DMaker.info"));
	ini.SetIniFilename(strInfoConfig);
	CString strPath = ini.GetString(_T("Path"), _T("FileServer"));

	if(b3DLogo)
		strBannerPath.Format(_T("%s\\img\\%d.png"),strPath, nDscNum+1);
	else
		strBannerPath.Format(_T("%s\\img\\Logo.png"),strPath);
	/*
#if 1 //3D 로고
	strBannerPath.Format(_T("%s\\img\\04.Banner\\%d.png"),strPath, nDscNum+1);
#else //일반 로고
	strBannerPath.Format(_T("%s\\img\\Logo.png"),strPath);
#endif*/

	char pBannerPath[MAX_PATH] = {0};
	wcstombs(pBannerPath, strBannerPath, MAX_PATH);
	if(::PathFileExists(strBannerPath) == FALSE)
		return ;

	IplImage* pImg;
	pImg=&IplImage(*tpMat);
	 
	IplImage* pBanner = cvLoadImage(pBannerPath, CV_LOAD_IMAGE_UNCHANGED);
	IplImage* pResult = cvCreateImage(cvSize(tpMat->cols, tpMat->rows), pImg->depth, pImg->nChannels);

	OverlayImage(pImg, pBanner, pResult, cvPoint(0,0));
	*tpMat = cvarrToMat(pResult);
}

void CESMMovieThreadImage::GpuInsertCameraID(Mat* tpMat, int nIndex, CString strDscNum, CString strDscIp, BOOL bPntColorInfo)
{
	char szCmeraID[10];
	char szCameraIp[17];
	sprintf(szCmeraID, "%3d:%S", nIndex+1, strDscNum); 
	sprintf(szCameraIp, "%S", strDscIp);
	//IplImage *img = cvCreateImage( cvSize( 1920, 1080 ), IPL_DEPTH_8U, 1 );

	//rectangle(*tpMat, Rect(100, 10, 500, 200), Scalar(0, 0, 0), CV_FILLED );
	rectangle(*tpMat, Point(100, 10), Point(1000, 250), Scalar(0, 0, 0), CV_FILLED );

	/* 이미지에 넣을 폰트 초기화 */
	CvFont pFontId;
	cvInitFont(&pFontId, CV_FONT_HERSHEY_SIMPLEX, 5, 5, 0, 3, 8);
	/* 이미지에 글자 삽입 */
	//IplImage* pImg;
	//pImg=&IplImage(*tpMat);	
	//cvPutText(pImg, szCmeraID, cvPoint(100, 150), &Font, CV_RGB(255, 255, 255));
	putText(*tpMat,szCmeraID,cv::Point(100,150),CV_FONT_HERSHEY_SIMPLEX,5,Scalar(255,255,255),10,8);
	putText(*tpMat,szCameraIp,cv::Point(110,220),CV_FONT_HERSHEY_SIMPLEX,2,Scalar(255,255,255),4,8);


	//IplImage* pResult = cvCreateImage(cvSize(tpMat->cols, tpMat->rows), pImg->depth, pImg->nChannels);		
	//
	//OverlayImage(pImg, img, pResult, cvPoint(0,0));


	if(bPntColorInfo)
	{
		/*색감 보정 Data 출력*/
		//Average
		CalcFrameInfo(tpMat);
	}
	//*tpMat = cvarrToMat(pImg);
}

/*
int CESMMovieThreadImage::AddBanner(stImageBuffer* pImageBuffer)
{
	if (!pImageBuffer->bLogo)
		return 0;

	CString strBannerPath;
	int nIndex=0;

	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	CDSCItem* pItem = NULL;
	for( int i =0 ;i < arDSCList.GetSize(); i++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		if (pImageBuffer->strDscName == pItem->GetDeviceDSCID() )
		{
			nIndex = i;
			break;
		}
	}

	strBannerPath.Format(_T("%s\\05.Banner\\%d.png") ,ESMGetPath(ESM_PATH_IMAGE), nIndex, nIndex);
	char pBannerPath[MAX_PATH] = {0};
	wcstombs(pBannerPath, strBannerPath, MAX_PATH);
	if(FileExists(strBannerPath) == FALSE)
		return 0;

	IplImage* pBanner = cvLoadImage(pBannerPath, CV_LOAD_IMAGE_UNCHANGED);
	IplImage* pResult = cvCreateImage(cvSize(pImageBuffer->cvImage->width, pImageBuffer->cvImage->height), pImageBuffer->cvImage->depth, pImageBuffer->cvImage->nChannels);

	overlayImage2(pImageBuffer->cvImage, pBanner, pResult, cvPoint(0,0));
	cvCopy(pResult, pImageBuffer->cvImage);

	return 0;;
}
*/
void CESMMovieThreadImage::OverlayImage(const IplImage *background, const IplImage *foreground, IplImage *output, CvPoint location)
{
	cvCopy(background, output);

	// start at the row indicated by location, or at row 0 if location.y is negative.
	for(int y = std::max(location.y , 0); y < background->height; ++y)
	{
		int fY = y - location.y; // because of the translation

		// we are done of we have processed all rows of the foreground image.
		if(fY >= foreground->height)
			break;

		// start at the column indicated by location, 

		// or at column 0 if location.x is negative.
		for(int x = std::max(location.x, 0); x < background->width; ++x)
		{
			int fX = x - location.x; // because of the translation.

			// we are done with this row if the column is outside of the foreground image.
			if(fX >= foreground->width)
				break;

			// determine the opacity of the foregrond pixel, using its fourth (alpha) channel.
			double opacity = ((double)foreground->imageData[fY * foreground->widthStep + fX * foreground->nChannels + 3]) / 255.;
			if ( opacity < 0 )
				opacity = opacity + 1.;

			// and now combine the background and foreground pixel, using the opacity, 

			// but only if opacity > 0.
			for(int c = 0; opacity > 0 && c < output->nChannels; ++c)
			{
				unsigned char foregroundPx =
					foreground->imageData[fY * foreground->widthStep + fX * foreground->nChannels + c];
				unsigned char backgroundPx =
					background->imageData[y * background->widthStep + x * background->nChannels + c];
				output->imageData[y*(output->widthStep) + output->nChannels*x + c] = (char)
					(backgroundPx * (1.-opacity) + foregroundPx * opacity);

				double value = backgroundPx * (1.-opacity) + foregroundPx * opacity;
			}

		}
	}
}
void CESMMovieThreadImage::KzonePrism(Mat *background, int nDscNum, int taValue,int saValue, int nPriIndex)
{
	WORD wVersionRequested;
	WSADATA wsaData;
	char name[255];
	CString ip; // 여기에 lcoal ip가 저장됩니다.
	PHOSTENT hostinfo;
	wVersionRequested = MAKEWORD( 2, 0 );
	if ( WSAStartup( wVersionRequested, &wsaData ) == 0 ) 
	{
		if( gethostname ( name, sizeof(name)) == 0)
		{
			if((hostinfo = gethostbyname(name)) != NULL)
			{
				ip = inet_ntoa (*(struct in_addr *)*hostinfo->h_addr_list);
			}
		}      
		WSACleanup();
	} 
	CString ipaddr;
	AfxExtractSubString( ipaddr, ip, 3, '.');


	//INI 읽어서 File Server 경로얻기
	CESMMovieIni ini;
	CString strInfoConfig;
	strInfoConfig.Format(_T("C:\\Program Files\\ESMLab\\4DMaker\\config\\4DMaker.info"));
	ini.SetIniFilename(strInfoConfig);
	CString strPath = ini.GetString(_T("Path"), _T("FileServer"));


	CString strBannerPath0;

	strBannerPath0.Format(_T("%s\\img\\06.Prism\\[%d]%d_0.png"),strPath, nPriIndex, nDscNum);

	char pBannerPath0[MAX_PATH] = {0};
	wcstombs(pBannerPath0, strBannerPath0, MAX_PATH);
	if(::PathFileExists(strBannerPath0) == FALSE)
		return ;

	CString strBannerPath1;

	strBannerPath1.Format(_T("%s\\img\\06.Prism\\[%d]%d_1.png"),strPath,nPriIndex, nDscNum);

	char pBannerPath1[MAX_PATH] = {0};
	wcstombs(pBannerPath1, strBannerPath1, MAX_PATH);
	if(::PathFileExists(strBannerPath1) == FALSE)
		return ;

	Mat banner0 = imread(pBannerPath0,1);
	Mat banner1 = imread(pBannerPath1,1);

	Mat Result0(background->rows,background->cols,CV_8UC3);
	Mat Result1(background->rows,background->cols,CV_8UC3);

	AlphaBlending(*background,banner0,Result0,taValue);
	AlphaBlending(Result0,banner1,Result1,saValue);
	*background = Result1.clone();
}
void CESMMovieThreadImage::AlphaBlending(Mat background, Mat pr, Mat result, int alpha)
{
	int row = pr.rows;
	int col = pr.cols;
	int ori_col = background.cols;
	int i,j;

	for(i=0;i<row;i++)
	{
		for(j=0;j<col;j++)
		{
			double b_dst = (double) (background.data[ori_col*(i*3)+(j*3)] * (100-alpha)/100  );
			double g_dst = (double) (background.data[ori_col*(i*3)+(j*3)+1] * (100-alpha)/100);
			double r_dst = (double) (background.data[ori_col*(i*3)+(j*3)+2] * (100-alpha)/100);

			double b_src = (double) (pr.data[col*(i*3)+(j*3)] * alpha / 100 );
			if(b_src < 1.0f)
				b_src = (double)(background.data[ori_col * (i*3) + (j*3)] * alpha / 100);
			double g_src = (double) (pr.data[col*(i*3)+(j*3)+1] * alpha / 100);
			if(g_src < 1.0f)
				g_src = (double)(background.data[ori_col * (i*3) + (j*3)+1] * alpha / 100);
			double r_src = (double) (pr.data[col*(i*3)+(j*3)+2] * alpha / 100);
			if(r_src < 1.0f)
				r_src = (double)(background.data[ori_col * (i*3) + (j*3)+2] * alpha / 100);

			result.data[ori_col*(i*3)+(j*3)] = (int) b_src + (int) b_dst;
			result.data[ori_col*(i*3)+(j*3)+1] = (int) g_src + (int) g_dst;
			result.data[ori_col*(i*3)+(j*3)+2] = (int) r_src + (int) r_dst;
		}
	}
}
void CESMMovieThreadImage::IplAlphablending(IplImage *background, IplImage* pr, IplImage *result, int alpha)
{
	int row = pr->height;
	int col = pr->width;
	int ori_col = background->width;
	int i,j;

	for(i=0;i<row;i++)
	{
		for(j=0;j<col;j++)
		{
			double b_dst = (double) (background->imageData[ori_col*(i*3)+(j*3)] * (100-alpha)/100  );
			double g_dst = (double) (background->imageData[ori_col*(i*3)+(j*3)+1] * (100-alpha)/100);
			double r_dst = (double) (background->imageData[ori_col*(i*3)+(j*3)+2] * (100-alpha)/100);

			double b_src = (double) (pr->imageData[col*(i*3)+(j*3)] * alpha / 100 );
			if(b_src == 0)
				b_src = (double)(background->imageData[ori_col*(i*3)+(j*3)] * alpha / 100);
			double g_src = (double) (pr->imageData[col*(i*3)+(j*3)+1] * alpha / 100);
			if(g_src == 0)
				g_src = (double)(background->imageData[ori_col*(i*3)+(j*3)+1] * alpha / 100);
			double r_src = (double) (pr->imageData[col*(i*3)+(j*3)+2] * alpha / 100);
			if(r_src == 0)
				r_src = (double)(background->imageData[ori_col*(i*3)+(j*3)+2] * alpha / 100);

			result->imageData[ori_col*(i*3)+(j*3)] = (int) b_src + (int) b_dst;
			result->imageData[ori_col*(i*3)+(j*3)+1] = (int) g_src + (int) g_dst;
			result->imageData[ori_col*(i*3)+(j*3)+2] = (int) r_src + (int) r_dst;
		}
	}
}

void CESMMovieThreadImage::CpuMakeMargin(Mat* gMat, int nX, int nY)
{
	Mat gMatCut, gMatPaste;
	int nHeight,nWidth;

	nWidth = gMat->cols - nX*2;
	nHeight = gMat->rows - nY*2;


	gMatCut = (*gMat)(cv::Rect(nX, nY,nWidth, nHeight));
	copyMakeBorder(gMatCut, gMatPaste, nY,nY,nX,nX, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}
void CESMMovieThreadImage::CpuMoveImage(Mat* gMat, int nX, int nY,double dbSize /*= 1.0*/)
{
	Mat gMatCut, gMatPaste;
	int nCutTop,nCutHeight,nCutLeft,nCutWidth;
	int nPasteTop=0,nPasteBottom=0,nPasteLeft=0,nPasteRight=0;
	nX *= (int) dbSize;
	nY *= (int) dbSize;

	if ( nX > 0 )
	{
		nCutLeft = 0;
		nCutWidth = gMat->cols - nX;

		nPasteLeft = nX;
	}
	else
	{
		nCutLeft = -nX;
		nCutWidth = gMat->cols + nX;

		nPasteRight = -nX;
	}

	if ( nY > 0 )
	{
		nCutTop = 0;
		nCutHeight = gMat->rows - nY;

		nPasteTop = nY;
	}
	else
	{
		nCutTop = -nY;
		nCutHeight = gMat->rows + nY;

		nPasteBottom = -nY;
	}

	gMatCut = (*gMat)(cv::Rect(nCutLeft, nCutTop,nCutWidth, nCutHeight));
	copyMakeBorder(gMatCut, gMatPaste, nPasteTop,nPasteBottom,nPasteLeft,nPasteRight, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}
void CESMMovieThreadImage::CpuRotateImage(Mat Iimage, double nCenterX, double nCenterY,  double dScale, double dAngle, BOOL bReverse, double dbSize)
{
	Mat Iimage2;// = cvCreateImage(cvGetSize(Iimage), IPL_DEPTH_8U, 3);

	if(dScale == 0)
	{
		dScale = 1;
		dAngle += -90;
	}

	if( dAngle == 0.0 && dScale == 0.0)
		return ;
	//cvShowImage("2",Iimage);
	CvMat *rot_mat = cvCreateMat( 2, 3, CV_32FC1);
	CvPoint2D32f rot_center = cvPoint2D32f( nCenterX * dbSize, nCenterY * dbSize);
	if( dScale == 0.0 && dAngle == 0.0 )
		return;

	double dbAngleAdjust;// = -1 * (dAngle + 90);
	if(bReverse) //Reverse Movie
		dbAngleAdjust = -1 * (180);   
	else
		dbAngleAdjust = -1 * (dAngle + 90);   //Normal Movie

	//CString strTemp;
	//strTemp.Format(_T("%f"), dbAngleAdjust);
	//m_AdjustDsc2ndList.SetItemText(m_nSelectedNum, 17, strTemp);

	cv2DRotationMatrix( rot_center, dbAngleAdjust, dScale, rot_mat);

	//cuda::warpAffine(*gMat,d_rotate, rot_mat, Size(2500, 1500), CV_INTER_CUBIC+CV_WARP_FILL_OUTLIERS);
	//Mat d_rotate;

	//-- 2014-07-16 hongsu@esmlab.com
	//-- ? CvPoint2D32f srcTri[3], dstTri[3];
	cvWarpAffine(&IplImage(Iimage), &IplImage(Iimage), rot_mat,CV_INTER_LINEAR+CV_WARP_FILL_OUTLIERS);	//d_rotate.copyTo(*gMat);
	//imshow("Iimage",Iimage);
	//cvWaitKey(0);
	//gMat =&(cvarrToMat(Iimage2));//
	//Iimage = cvCreateImage(cvGetSize(&Iimage), IPL_DEPTH_8U, 3);
	cvReleaseMat(&rot_mat);
}

void CESMMovieThreadImage::DiorMovie(Mat *background, int nDscNum, int nPriNumIndex)
{
	WORD wVersionRequested;
	WSADATA wsaData;
	char name[255];
	CString ip; // 여기에 lcoal ip가 저장됩니다.
	PHOSTENT hostinfo;
	wVersionRequested = MAKEWORD( 2, 0 );
	if ( WSAStartup( wVersionRequested, &wsaData ) == 0 ) 
	{
		if( gethostname ( name, sizeof(name)) == 0)
		{
			if((hostinfo = gethostbyname(name)) != NULL)
			{
				ip = inet_ntoa (*(struct in_addr *)*hostinfo->h_addr_list);
			}
		}      
		WSACleanup();
	} 
	CString ipaddr;
	AfxExtractSubString( ipaddr, ip, 3, '.');


	//INI 읽어서 File Server 경로얻기
	CESMMovieIni ini;
	CString strInfoConfig;
	strInfoConfig.Format(_T("C:\\Program Files\\ESMLab\\4DMaker\\config\\4DMaker.info"));
	ini.SetIniFilename(strInfoConfig);
	CString strPath = ini.GetString(_T("Path"), _T("FileServer"));


	CString strBannerPath0;

	strBannerPath0.Format(_T("%s\\img\\06.Prism\\[%d]%d_0.png"),strPath, nPriNumIndex,nDscNum-1 );

	char pBannerPath0[MAX_PATH] = {0};
	wcstombs(pBannerPath0, strBannerPath0, MAX_PATH);
	if(::PathFileExists(strBannerPath0) == FALSE)
		return ;

	Mat banner0 = imread(pBannerPath0,1);

	Mat Result0(background->rows,background->cols,CV_8UC3);
	//imshow("background",*background);
	AlphaBlending(background,banner0,Result0);
	//imshow("Result0",Result0);
	//imshow("banner",banner0);

	*background = Result0.clone();
}
void CESMMovieThreadImage::AlphaBlending(cv::Mat *Frame,cv::Mat Logo,cv::Mat Output)
{
	int i,j;
	int nWidth = Logo.cols;
	int nHeight = Logo.rows;
	int alpha = 100;
	int nX,nY;

	if(Frame->cols != Logo.cols) 
	{
		return;
	}

	for(i = 0 ; i < nHeight ; i++)
	{
		nY = nWidth*(i*3);

		for(j = 0 ; j < nWidth ; j++)
		{
			nX = nY + (j*3);

			double b_dst = (double) (Frame->data[nX]   * (100-alpha)/100);
			double g_dst = (double) (Frame->data[nX+1] * (100-alpha)/100);
			double r_dst = (double) (Frame->data[nX+2]* (100-alpha)/100);

			double b_src = (double) (Logo.data[nX] * alpha / 100 );
			if(b_src >254)
				b_src = (double)(Frame->data[nX] * alpha / 100);

			double g_src = (double) (Logo.data[nX+1] * alpha / 100);
			if(g_src >254)
				g_src = (double)(Frame->data[nX+1] * alpha / 100);

			double r_src = (double) (Logo.data[nX+2] * alpha / 100);
			if(r_src >254)
				r_src = (double)(Frame->data[nX+2] * alpha / 100);

			Output.data[nX] =   (int) b_src + (int) b_dst;
			Output.data[nX+1] = (int) g_src + (int) g_dst;
			Output.data[nX+2] = (int) r_src + (int) r_dst;
		}
	}
}
void CESMMovieThreadImage::DoColorRevision(cv::Mat *frame,FrameRGBInfo stColorInfo)
{
	int nIdx = 0;
	double dB = 0,dG = 0,dR = 0;
	for(int i = 0 ; i <frame->total(); i++)
	{
		dB += frame->data[nIdx++];
		dG += frame->data[nIdx++];
		dR += frame->data[nIdx++];
	}
	//현재 프레임의 평균
	dB = dB / frame->total();
	dG = dG / frame->total();
	dR = dR / frame->total();

	int nBlue = (int) (dB - stColorInfo.dBlue);
	int nGreen = (int) (dG - stColorInfo.dGreen);
	int nRed = (int) (dR - stColorInfo.dRed);

	//모든 색이 일치한 경우..
	if(nBlue == 0 && nGreen == 0 &&nRed ==0)
		return;

	for(int i = 0 ; i < frame->rows; i++)
	{
		for(int j = 0; j < frame->cols; j++)
		{
			int nIdx = frame->cols * (i*3) + (j*3);

			frame->data[nIdx] = CalcTransValue(frame->data[nIdx],nBlue);
			frame->data[nIdx+1] = CalcTransValue(frame->data[nIdx+1],nGreen);
			frame->data[nIdx+2] = CalcTransValue(frame->data[nIdx+2],nRed);
		}
	}

}
int CESMMovieThreadImage::CalcTransValue(int nPixel, int nRefValue)
{
	int nResult = nPixel - nRefValue;

	if(nResult > 255)
		nResult = 255;
	if(nResult < 0)
		nResult = 0;

	return nResult;
}
void CESMMovieThreadImage::CalcFrameInfo(Mat *pImage)
{
	Mat img = pImage->clone();
	//Mat img(pImage->height,pImage->width,CV_8UC3,pImage->imageData);
	//img = cvarrToMat(&pImage);
	int nViewerSize = 200;
	Mat HueViewer(nViewerSize,nViewerSize,CV_8UC3,Scalar(0,0,0));
	double dBlue=0,dGreen=0,dRed=0;

	CalcFrameAvg(img,dBlue,dGreen,dRed);
	BGR2HSV(img,HueViewer);

	int nWidth = img.cols;
	int nHeight = img.rows;
	int nRowStart = nHeight- nViewerSize;
	int nColStart = nWidth - nViewerSize;

	for(int i =nRowStart; i < nHeight; i++)
	{
		for(int j = nColStart; j < nWidth; j++)
		{
			img.data[nWidth*(i*3)+(j*3)]   = HueViewer.data[nViewerSize*((i-nRowStart)*3)+((j-nColStart)*3)];
			img.data[nWidth*(i*3)+(j*3)+1] = HueViewer.data[nViewerSize*((i-nRowStart)*3)+((j-nColStart)*3)+1];
			img.data[nWidth*(i*3)+(j*3)+2] = HueViewer.data[nViewerSize*((i-nRowStart)*3)+((j-nColStart)*3)+2];
		}
	}
	char strData[200];
	sprintf(strData,"B:%.2f G:%.2f R:%.2f",dBlue,dGreen,dRed);
	putText(img,strData,cv::Point(270,1070),CV_FONT_HERSHEY_TRIPLEX,2,Scalar(255,255,255),1,8);

	/*IplImage temp;
	temp = img;*/
	*pImage = img.clone();
}
void CESMMovieThreadImage::BGR2HSV(Mat img,Mat result)
{
	int nHeight = img.rows;
	int nWidth  = img.cols;

	double dMin;
	double dValue;
	double dHue;
	double dSaturation;
	int nIndex = 0;
	SampleLining(result);

	for(int i = 0 ; i < nHeight; i++)
	{
		for( int j = 0 ; j < nWidth; j++)
		{
			//int nIndex = nWidth * (i*3) + (j*3);
			double B = NormValue(img.data[nIndex++]);
			double G = NormValue(img.data[nIndex++]);
			double R = NormValue(img.data[nIndex++]);

			dMin = GetMinValue(B,G,R);
			dValue = GetMaxValue(B,G,R);
			if(dValue == 0 || B==G && G == R)
			{
				dSaturation = 0;
				dHue = 0;
			}
			else
			{
				dSaturation = GetSaturation(dValue,dMin);
				dHue = GetHue(B,G,R,dValue,dMin);
			}
			DrawColorValue(result,dHue,dSaturation,dValue,B,G,R);
		}
	}
}
double CESMMovieThreadImage::NormValue(int nValue)
{
	double dValue = (double)nValue / (double)MAXDEPTH;

	return dValue;
}
double CESMMovieThreadImage::GetSaturation(double dbValue,double dbMin)
{
	double dbResult;

	if(dbValue == 0.0)
		dbResult = 0;
	else
		dbResult = (dbValue- dbMin) / dbValue;

	return dbResult;
}
double CESMMovieThreadImage::GetHue(double dbBlue,double dbGreen,double dbRed,double dValue,double dMin)
{
	double dHue;

	if(dValue == dMin)
		dHue = 0;
	else if(dValue == dbBlue)
		dHue = 60 * ((dbRed - dbGreen)/ (dValue - dMin) + 4);
	else if(dValue == dbGreen)
		dHue = 60 * ((dbBlue - dbRed)/ (dValue - dMin) + 2);
	else
		dHue = 60 * ((dbGreen - dbBlue)/ (dValue - dMin));

	if(dHue < 0)
		dHue += 360;

	return dHue;
}
double CESMMovieThreadImage::GetMaxValue(double dbBlue,double dbGreen,double dbRed)
{
	double dMax = dbBlue;

	if(dbGreen > dMax)
		dMax = dbGreen;

	if(dbRed > dMax)
		dMax = dbRed;

	return dMax;
}
double CESMMovieThreadImage::GetMinValue(double dbBlue,double dbGreen,double dbRed)
{
	double dMin = dbBlue;

	if(dbGreen < dMin)
		dMin = dbGreen;

	if(dbRed < dMin)
		dMin = dbRed;

	return dMin;
}
void CESMMovieThreadImage::DrawColorValue(Mat result,double dbHue,double dbSaturation,double dbValue,double dbBlue,double dbGreen,double dbRed)
{
	int nRadius = result.rows/2;
	int nWidth = result.cols;
	int nHeight = result.rows;
	double dRadian = ((dbHue+270) * PHI) / 180;

	Point2f hsvpoint;			

	hsvpoint.x = (float) (int) (dbSaturation*nRadius * cos(dRadian) + nRadius);
	hsvpoint.y = (float) (int) (dbSaturation*nRadius * sin(dRadian) + nRadius);

	if(hsvpoint.x < 0)
		hsvpoint.x = 0;

	if(hsvpoint.y < 0)
		hsvpoint.y = 0;

	if(hsvpoint.x >= nWidth -1)
		hsvpoint.x = (float) (nWidth - 1);

	if(hsvpoint.y > nHeight -1)
		hsvpoint.y = (float) (nHeight - 1);

	result.at<Vec3b>(hsvpoint)[0] = (uchar) (dbBlue*255);
	result.at<Vec3b>(hsvpoint)[1] = (uchar) (dbGreen*255);
	result.at<Vec3b>(hsvpoint)[2] = (uchar) (dbRed*255);
}
void CESMMovieThreadImage::SampleLining(Mat img)
{
	double dBlue,dGreen,dRed;
	double dMin,dValue,dSaturation,dHue;
	cv::Point pCenter(img.cols/2,img.rows/2);
	int nRadian = img.rows/2;
	circle(img,pCenter,nRadian,Scalar(255,255,255),1,8);
	for(int i = 0 ; i < 7 ; i++)
	{
		switch(i)
		{
		case 0://Blue
			{
				dBlue = 1,dGreen = 0,dRed = 0;
				dMin = GetMinValue(dBlue,dGreen,dRed);
				dValue = GetMaxValue(dBlue,dGreen,dRed);
				dSaturation = GetSaturation(dValue,dMin);
				dHue = GetHue(dBlue,dGreen,dRed,dValue,dMin);	

				line(img,pCenter,CalcLiningPoint(nRadian,dHue,dSaturation),Scalar(255,0,0),1,8);
			}
			break;
		case 1://Green
			{
				dBlue = 0,dGreen = 1,dRed = 0;
				dMin = GetMinValue(dBlue,dGreen,dRed);
				dValue = GetMaxValue(dBlue,dGreen,dRed);
				dSaturation = GetSaturation(dValue,dMin);
				dHue = GetHue(dBlue,dGreen,dRed,dValue,dMin);	

				line(img,pCenter,CalcLiningPoint(nRadian,dHue,dSaturation),Scalar(0,255,0),1,8);
			}
			break;
		case 2://Red
			{
				dBlue = 0,dGreen = 0,dRed = 1;
				dMin = GetMinValue(dBlue,dGreen,dRed);
				dValue = GetMaxValue(dBlue,dGreen,dRed);
				dSaturation = GetSaturation(dValue,dMin);
				dHue = GetHue(dBlue,dGreen,dRed,dValue,dMin);	

				line(img,pCenter,CalcLiningPoint(nRadian,dHue,dSaturation),Scalar(0,0,255),1,8);
			}
			break;
		case 3://Yellow
			{
				dBlue = 0,dGreen = 1,dRed = 1;
				dMin = GetMinValue(dBlue,dGreen,dRed);
				dValue = GetMaxValue(dBlue,dGreen,dRed);
				dSaturation = GetSaturation(dValue,dMin);
				dHue = GetHue(dBlue,dGreen,dRed,dValue,dMin);	

				line(img,pCenter,CalcLiningPoint(nRadian,dHue,dSaturation),Scalar(0,255,255),1,8);
			}
			break;
		case 4://Cyan
			{
				dBlue = 1,dGreen = 1,dRed = 0;
				dMin = GetMinValue(dBlue,dGreen,dRed);
				dValue = GetMaxValue(dBlue,dGreen,dRed);
				dSaturation = GetSaturation(dValue,dMin);
				dHue = GetHue(dBlue,dGreen,dRed,dValue,dMin);	

				line(img,pCenter,CalcLiningPoint(nRadian,dHue,dSaturation),Scalar(255,255,0),1,8);
			}
			break;
		case 5://Magenta
			{
				dBlue = 1,dGreen = 0,dRed = 1;
				dMin = GetMinValue(dBlue,dGreen,dRed);
				dValue = GetMaxValue(dBlue,dGreen,dRed);
				dSaturation = GetSaturation(dValue,dMin);
				dHue = GetHue(dBlue,dGreen,dRed,dValue,dMin);	

				line(img,pCenter,CalcLiningPoint(nRadian,dHue,dSaturation),Scalar(255,0,255),1,8);
			}
			break;
		}
	}
}
cv::Point2f CESMMovieThreadImage::CalcLiningPoint(int nRadian,double dbHue,double dbSaturation)
{
	double dRadian = ((dbHue+270) * PHI) / 180;

	Point2f hsvpoint;			

	hsvpoint.x = (float) (dbSaturation*nRadian * cos(dRadian) + nRadian);
	hsvpoint.y = (float) (dbSaturation*nRadian * sin(dRadian) + nRadian);

	return hsvpoint;
}
void CESMMovieThreadImage::CalcFrameAvg(Mat img,double &dBlue,double &dGreen,double &dRed)
{
	int nTotal = (int) img.total();
	int nHeight = img.rows;
	int nWidth = img.cols;
	double dBlueSum = 0;
	double dGreenSum = 0;
	double dRedSum = 0;

	for(int i = 0 ; i < nHeight; i++)
	{
		for(int j = 0 ; j < nWidth; j++)
		{
			dBlueSum += img.data[nWidth*(i*3)+(j*3)];
			dGreenSum += img.data[nWidth*(i*3)+(j*3)+1];
			dRedSum += img.data[nWidth*(i*3)+(j*3)+2];
		}
	}

	dBlue = dBlueSum/nTotal;
	dGreen = dGreenSum/nTotal;
	dRed = dRedSum/nTotal;
}
void CESMMovieThreadImage::DoAdjustYUV(MakeFrameInfo* pFrameInfo,stAdjustInfo AdjustData,
	cv::Size szSrc,cv::Size szOutput,int nMarginX,int nMarginY,int nSelMode)
{
	int nYWidth  = szSrc.width, nYHeight  = szSrc.height;
	int nUVWidth = nYWidth/2,   nUVHeight = nYHeight/2;

	Mat Y(nYHeight,nYWidth,CV_8UC1);
	memcpy(Y.data,pFrameInfo->pY,nYWidth*nYHeight);
	Mat U(nUVHeight,nUVWidth,CV_8UC1);
	memcpy(U.data,pFrameInfo->pU,nUVWidth*nUVHeight);
	Mat V(nUVHeight,nUVWidth,CV_8UC1);
	memcpy(V.data,pFrameInfo->pV,nUVWidth*nUVHeight);
	
	if(pFrameInfo->bMovieReverse)//Reverse Movie
	{
		flip(Y,Y,-1);flip(U,U,-1);flip(V,V,-1);
	}

	if(pFrameInfo->bColorRevision) //Color Revision
		DoColorRevisionYUV(&Y,&U,&V,pFrameInfo->stColorInfo);

	double dRatio = 1.;
	if(nSelMode == 1)
		dRatio = (double)szOutput.width/(double)szSrc.width;

	if(DoAdjustAll(AdjustData,nMarginX,nMarginY,szSrc,szOutput,&Y,&U,&V,nSelMode,dRatio))
	{
		if(nSelMode == 2 || nSelMode == 3)//Zoom (VMCC)
		{
			EffectInfo EffectData = pFrameInfo->stEffectData;
			double dbEffect = (double) EffectData.nZoomRatio / 100.0;

			DoVMCCYUV(&Y,&U,&V,szOutput,nSelMode,dbEffect,EffectData.nPosX,EffectData.nPosY,m_bUHDtoFHD);
		}
		if(pFrameInfo->bReverse)//Reverse
		{
			flip(Y,Y,-1);flip(U,U,-1);flip(V,V,-1);
		}
		//if(pFrameInfo->nDscIndex > -1)//Logo
		//	DoInsertLOGO(&Y,&U,&V,pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
		//
		//if(pFrameInfo->nPriIndex > -1 || pFrameInfo->nPrinumIndex >0)//Prism
		//	DoInsertPrism();

		if(pFrameInfo->strCameraID[0] != _T('\0'))//Camera ID			
			DoInsertCAMID(&Y,&U,&V,pFrameInfo->nPriIndex,pFrameInfo->strCameraID,pFrameInfo->strDSC_IP);

		DoMemoryReAlloc(pFrameInfo,&Y,&U,&V,szOutput);
	}
}
void CESMMovieThreadImage::DoColorRevisionYUV(Mat *Y,Mat *U,Mat *V,FrameRGBInfo stInfo)
{
	double dbY = 0, dbU = 0, dbV = 0;

	int nYWidth  = Y->cols;
	int nYHeight = Y->rows;

	int nYSize = nYWidth * nYHeight;
	int nUVSize = nYSize / 4;

	for(int i = 0 ; i < nYSize ; i++)
		dbY += Y->data[i];

	for(int i = 0 ; i < nUVSize; i++)
	{
		dbU += U->data[i];
		dbV += V->data[i];
	}

	dbY /= nYSize;
	dbU /= nUVSize;
	dbV /= nUVSize;

	dbY -= stInfo.dBlue;
	dbU -= stInfo.dGreen;
	dbV -= stInfo.dRed;

	for(int i = 0 ; i < nYSize; i++)
		Y->data[i] = CalcTransValue(Y->data[i],(int)dbY);

	for(int i = 0 ; i < nUVSize; i++)
	{
		U->data[i] = CalcTransValue(U->data[i],(int)dbU);
		V->data[i]= CalcTransValue(V->data[i],(int)dbV);
	}
}
BOOL CESMMovieThreadImage::DoAdjustAll(stAdjustInfo AdjustData,int nMarginX,int nMarginY,cv::Size szSrc,cv::Size szOutput,
	Mat*Y,Mat*U,Mat*V,int nSelMode,double dRatio/* = 1*/)
{
	int nYWidth = Y->cols, nYHeight = Y->rows;
	int nUVWidth = U->cols, nUVHeight = U->rows;

	if(nSelMode == 1)
	{
		nYWidth  *= (int) dRatio,nYHeight  *= (int) dRatio;
		nUVWidth *= (int) dRatio,nUVHeight *= (int) dRatio;
		nMarginX *= (int) dRatio,nMarginY  *= (int) dRatio;
		
		Mat re;
		resize(*Y,re,cv::Size(nYWidth,nYHeight),0,0,CV_INTER_CUBIC);
		re.copyTo(*Y);
		resize(*U,re,cv::Size(nUVWidth,nUVHeight),0,0,CV_INTER_CUBIC);
		re.copyTo(*U);
		resize(*V,re,cv::Size(nUVWidth,nUVHeight),0,0,CV_INTER_CUBIC);
		re.copyTo(*V);
	}
	//Rotate
	int nRotateX = Round(AdjustData.AdjptRotate.x * dRatio);
	int nRotateY = Round(AdjustData.AdjptRotate.y * dRatio);
	CpuRotateImage(*Y,nRotateX,nRotateY,AdjustData.AdjSize,AdjustData.AdjAngle,0);
	CpuRotateImage(*U,nRotateX,nRotateY,AdjustData.AdjSize,AdjustData.AdjAngle,0,0.5);
	CpuRotateImage(*V,nRotateX,nRotateY,AdjustData.AdjSize,AdjustData.AdjAngle,0,0.5);

	//Move
	int nMoveX = Round(AdjustData.AdjMove.x * dRatio);
	int nMoveY = Round(AdjustData.AdjMove.y * dRatio);
	CpuMoveImage(Y,nMoveX,nMoveY);
	CpuMoveImage(U,nMoveX,nMoveY,0.5);
	CpuMoveImage(V,nMoveX,nMoveY,0.5);

	cv::Size szTemp;
	if(/*nSelMode == 2 || */nSelMode == 3)
	{
		szTemp = szOutput;
		szOutput = szSrc;
	}

	//Margin
	double dbMarginScale = 1 / ( (double)( nYWidth - nMarginX * 2) /(double) nYWidth  );
	Mat  pMatResize;
	resize(*Y, pMatResize, cv::Size((int) (nYWidth*dbMarginScale), (int) (nYHeight * dbMarginScale) ), 0.0, 0.0 ,INTER_CUBIC);
	int nLeft = (int)((nYWidth*dbMarginScale - nYWidth)/2);
	int nTop = (int)((nYHeight*dbMarginScale - nYHeight)/2);
	Mat pMatCut =  (pMatResize)(cv::Rect(nLeft, nTop, nYWidth, nYHeight));
	pMatCut.copyTo(*Y);

	dbMarginScale = 1 / ( (double)( nYWidth/2 - (nMarginX/2) * 2) /(double) (nYWidth/2));
	resize(*U,pMatResize,cv::Size((int) ((nYWidth/2)*dbMarginScale), (int) ((nYHeight/2)*dbMarginScale)),0,0,CV_INTER_CUBIC);
	nLeft = (int)(((nYWidth/2)*dbMarginScale - (nYWidth/2))/2);
	nTop = (int)(((nYHeight/2)*dbMarginScale - (nYHeight/2))/2);
	pMatCut = (pMatResize)(cv::Rect(nLeft,nTop,nYWidth/2,nYHeight/2));
	pMatCut.copyTo(*U);

	resize(*V,pMatResize,cv::Size((int) ((nYWidth/2)*dbMarginScale), (int) ((nYHeight/2)*dbMarginScale)),0,0,CV_INTER_CUBIC);
	pMatCut = (pMatResize)(cv::Rect(nLeft,nTop,nYWidth/2,nYHeight/2));
	pMatCut.copyTo(*V);

	return TRUE;
}
void CESMMovieThreadImage::DoVMCCYUV(Mat *Y,Mat*U, Mat*V,cv::Size szOutput,int nSelMode,
								double dbEffect,int nPosX,int nPosY,BOOL bUHD)
{
	int nYWidth  = Y->cols;
	int nYHeight = Y->rows;

	int nLeft = (int) (nPosX- nYWidth/dbEffect/2);
	int nTop  = (int) (nPosY- nYHeight/dbEffect/2);

	int nWidth = (int) (nYWidth/dbEffect - 1);
	if((nLeft + nWidth) > nYWidth)
		nLeft = nLeft - ((nLeft + nWidth) - nYWidth);
	else if(nLeft < 0)
		nLeft = 0;

	int nHeight = (int) (nYHeight/dbEffect - 1);
	if((nTop + nHeight) > nYHeight)
		nTop = nTop - ((nTop + nHeight) - nYHeight);
	else if (nTop < 0)
		nTop = 0;

	Mat YCut =  (*Y)(Rect(nLeft, nTop, nWidth, nHeight));
	Mat UCut =  (*U)(Rect(nLeft/2, nTop/2, nWidth/2, nHeight/2));
	Mat VCut =  (*V)(Rect(nLeft/2, nTop/2, nWidth/2, nHeight/2));

	cv::Size szRe(szOutput.width/2,szOutput.height/2);

	Mat re;
	resize(YCut,re,szOutput,0,0,CV_INTER_CUBIC);
	re.copyTo(*Y);
	resize(UCut,re,szRe,0,0,CV_INTER_CUBIC);
	re.copyTo(*U);
	resize(VCut,re,szRe,0,0,CV_INTER_CUBIC);
	re.copyTo(*V);
}
void CESMMovieThreadImage::DoInsertCAMID(Mat *Y,Mat* U,Mat* V,int nIndex,CString strDscNum,CString strDscIp)
{
	char szCmeraID[10];
	char szCameraIp[17];
	sprintf(szCmeraID, "%3d:%S", nIndex+1, strDscNum); 
	sprintf(szCameraIp, "%S", strDscIp);

	rectangle(*Y, Point(100, 10), Point(1000, 250), Scalar(0, 0, 0), CV_FILLED );
	rectangle(*U, Point(50, 5), Point(500, 125), Scalar(128,128,128), CV_FILLED );
	rectangle(*V, Point(50, 5), Point(500, 125), Scalar(128,128,128), CV_FILLED );

	putText(*Y,szCmeraID,cv::Point(100,150),CV_FONT_HERSHEY_SIMPLEX,5,Scalar(255,255,255),10,8);
	putText(*Y,szCameraIp,cv::Point(110,220),CV_FONT_HERSHEY_SIMPLEX,2,Scalar(255,255,255),4,8);
}
void CESMMovieThreadImage::DoInsertLOGO(Mat *Y,Mat*U,Mat*V,int nDscNum,BOOL b3DLogo)
{
	//RGB 형태로 복원
	Mat BGR;

	//Logo data 입력
	CESMMovieIni ini;
	CString strInfoConfig;
	strInfoConfig.Format(_T("C:\\Program Files\\ESMLab\\4DMaker\\config\\4DMaker.info"));
	ini.SetIniFilename(strInfoConfig);
	CString strPath = ini.GetString(_T("Path"), _T("FileServer"));

	CString strBannerPath;
	if(b3DLogo)
		strBannerPath.Format(_T("%s\\img\\%d.png"),strPath, nDscNum+1);
	else
		strBannerPath.Format(_T("%s\\img\\Logo.png"),strPath);

	char pBannerPath[MAX_PATH] = {0};
	wcstombs(pBannerPath, strBannerPath, MAX_PATH);
	if(::PathFileExists(strBannerPath) == FALSE)
		return ;

	//Alpha 채널 유무에 따라 영상 적용
	IplImage* pBGR  = &IplImage(BGR);
	IplImage* pLogo = cvLoadImage(pBannerPath,CV_LOAD_IMAGE_UNCHANGED);//&IplImage(Logo);
	IplImage* pResult = cvCreateImage(CvSize(pBGR->width,pBGR->height),8,3);

	OverlayImage(pBGR,pLogo,pResult,cvPoint(0,0));
	
	//YUV422형태로 변경 및 삭제
	int nYSize  = Y->rows * Y->cols;
	int nUVSize = U->rows * U->cols;
	Mat result = cvarrToMat(pResult);
	Mat YUV;
	cvtColor(result,YUV,CV_BGR2YUV_I420);
	memcpy(Y->data,YUV.data		          ,nYSize);
	memcpy(U->data,YUV.data+nYSize		  ,nUVSize);
	memcpy(V->data,YUV.data+nYSize+nUVSize,nUVSize);

	//삭제
	cvReleaseImage(&pLogo);
	cvReleaseImage(&pResult);
}
void CESMMovieThreadImage::DoInsertPrism()
{

}
void CESMMovieThreadImage::DoMemoryReAlloc(MakeFrameInfo* pFrameInfo,Mat *Y,Mat *U, Mat *V,cv::Size szOutput)
{
	delete[] pFrameInfo->pY;
	pFrameInfo->pY = new BYTE[szOutput.width * szOutput.height];
	memcpy(pFrameInfo->pY,Y->data,szOutput.width * szOutput.height);

	delete[] pFrameInfo->pU;
	pFrameInfo->pU = new BYTE[szOutput.width * szOutput.height / 4];
	memcpy(pFrameInfo->pU,U->data,szOutput.width * szOutput.height / 4);

	delete[] pFrameInfo->pV;
	pFrameInfo->pV = new BYTE[szOutput.width * szOutput.height / 4];
	memcpy(pFrameInfo->pV,V->data,szOutput.width * szOutput.height / 4);
}