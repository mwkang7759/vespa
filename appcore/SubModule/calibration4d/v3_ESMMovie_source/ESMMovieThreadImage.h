////////////////////////////////////////////////////////////////////////////////
//
//	ESMMovieThreadImage.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-05-26
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "ESMMovieThread.h"
#include "ESMMovieMgr.h"
#include "cv.h"
#include "highgui.h"
#include "ImageProcessor.h"

#ifndef PHI
#define PHI 3.1415926535897932
#endif
#ifndef MAXDEPTH
#define MAXDEPTH 255
#endif

class CESMMovieThreadImage: public CESMMovieThread
{
	DECLARE_DYNCREATE(CESMMovieThreadImage)

public:
	CESMMovieThreadImage(void) {};
	CESMMovieThreadImage(CWinThread* pParent, short nMessage, ESMMovieMsg* pMsg);	
	virtual ~CESMMovieThreadImage();

public:
	void ResizeImage(TCHAR* pMovieName, TCHAR* pMovieId, int nFrameNum);
	void RotateImage(TCHAR* pMovieName, TCHAR* pMovieId, int nFrameNum);
	void MoveImage(TCHAR* pMovieName, TCHAR* pMovieId, int nFrameNum);
	void CutImage(TCHAR* pMovieName, TCHAR* pMovieId, int nFrameNum);
	void InsertLogo(TCHAR* pMovieName, TCHAR* pMovieId, int nFrameNum, int nDscNum);
	void EffectImage(TCHAR* pMovieName, TCHAR* pMovieId, EffectInfo* pEffectData, int nFrameNum);
	//GpuMaking using CUDA 3.1	
	void GpuMoveImage(cuda::GpuMat* gMat, int nX, int nY);
	void GpuRotateImage(cuda::GpuMat* gMat, double nCenterX, double nCenterY,  double dScale, double dAngle, BOOL bReverse = FALSE);
	void GpuMakeMargin(cuda::GpuMat* gMat, int nX, int nY);
	//test
	void CpuRotateImage(Mat Iimage, double nCenterX, double nCenterY,  double dScale, double dAngle, BOOL bReverse,double dbSize = 1.0);
	void CpuMoveImage(Mat* gMat, int nX, int nY,double dbSize = 1.0);
	void CpuMakeMargin(Mat* gMat, int nX, int nY);
	//CMiLRe 20151020 VMCC Effect(ZOOM) ��� �߰�
	void GpuZoomImage(cuda::GpuMat* gMat, int nOrgWidth, int nOrgHeight, int nX, int nY, int nRatio, int nOutputWidth, int nOutputHeight);
	void GpuZoomImage(cuda::GpuMat* gMat, int nWidth, int nHeight, int nX, int nY, int nRatio);
	void GpuInsertLogo(cuda::GpuMat* gMat, int nDscNum, int nWidth, int nHeight);
	void GpuInsertLogo2(Mat* tpMat, int nDscNum, BOOL b3DLogo);
	void GpuInsertCameraID(Mat* tpMat, int nIndex, CString strDscNum, CString strDscIp, BOOL bPntColorInfo);
// 	void AddBanner(stImageBuffer* pImageBuffer);
 	void OverlayImage(const IplImage *background, const IplImage *foreground, IplImage *output, CvPoint location);
	int Round(double dData);
	void AlphaBlending(Mat background, Mat pr, Mat result, int alpha);
	void KzonePrism(Mat *background, int nDscNum, int taValue,int saValue,int nPrinum);
	void IplAlphablending(IplImage *background, IplImage* pr, IplImage *result, int alpha);
	void DiorMovie(Mat *background, int nDscNum, int nPriNumIndex);
	void AlphaBlending(cv::Mat *Frame,cv::Mat Logo,cv::Mat Output);

	/* Color Revision Code */
	void DoColorRevision(cv::Mat *frame,FrameRGBInfo stColorInfo);
	int CalcTransValue(int nPixel, int nRefValue);
	void CalcFrameInfo(Mat *Image);
	void CalcFrameAvg(Mat img,double &dBlue,double &dGreen,double &dRed);
	void BGR2HSV(Mat img,Mat result);
	double GetMaxValue(double dbBlue,double dbGreen,double dbRed);
	double GetMinValue(double dbBlue,double dbGreen,double dbRed);
	double NormValue(int nValue);
	double GetSaturation(double dbValue,double dbMin);
	double GetHue(double dbBlue,double dbGreen,double dbRed,double dValue,double dMin);	
	void DrawColorValue(Mat result,double dbHue,double dbSaturation,double dbValue,double dbBlue,double dbGreen,double dbRed);
	void SampleLining(Mat img);
	cv::Point2f CalcLiningPoint(int nRadian,double dbHue,double dbSaturation);

	//YUV Processing
	void DoColorRevisionYUV(Mat *Y,Mat *U,Mat *V,FrameRGBInfo stInfo);
	void InsertDSCID(Mat*Y, Mat*U, Mat* V,int nDSCID);
	void DoAdjustYUV(MakeFrameInfo* pFrameInfo,stAdjustInfo AdjustData
		,cv::Size szSrc,cv::Size szOutput,int nMarginX,int nMarginY,int nSelMode);
	BOOL DoAdjustAll(stAdjustInfo AdjustData,int nMarginX,int nMarginY,cv::Size szSrc,cv::Size szOutput,
		Mat*Y,Mat*U,Mat*V,int nSelMode,double dRatio = 1);
	void DoVMCCYUV(Mat *Y,Mat*U, Mat*V,cv::Size szOutput,int nSelMode,double dbEffect,int nPosX,int nPosY,BOOL bUHD);
	void DoInsertCAMID(Mat *Y,Mat* U,Mat* V,int nIndex,CString strDscNum,CString strDscIp);
	void DoInsertLOGO(Mat *Y,Mat*U,Mat*V,int nDscNum,BOOL b3DLogo);
	void DoInsertPrism();
	void DoMemoryReAlloc(MakeFrameInfo* pFrameInfo,Mat *Y,Mat *U, Mat *V,cv::Size szOutput);
	BOOL m_bUHDtoFHD;
public:	 
	virtual int Run(void);
};


