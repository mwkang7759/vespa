#include "stdafx.h"
#include "ImageProcessor.h"
#include "ESMMovieIni.h"
#include "ESMMovieThreadManager.h"
#include "DllFunc.h"
#include "CTemplateCalculator.h"
#include <afxmt.h>


#ifndef __IMGPROC__

#define __IMGPROC__

int	CImageProcessor::m_nMovieSize = 0;
BOOL CImageProcessor::m_bUHDtoFHD = FALSE;
BOOL CImageProcessor::m_bVMCCFlag = FALSE;
int	CImageProcessor::m_nMarginX = 0;
int	CImageProcessor::m_nMarginY = 0;
BOOL CImageProcessor::m_bMakeBorder = FALSE;
vector<Point2f> CImageProcessor::m_vInterAdjPts;

CMutex g_mutexMovieSize(FALSE, NULL);
CMutex g_mutexUHDtoFHD(FALSE, NULL);
CMutex g_mutexVMCCFlag(FALSE, NULL);
CMutex g_mutexMarginX(FALSE, NULL);
CMutex g_mutexMarginY(FALSE, NULL);
CMutex g_mutexMakeBorder(FALSE, NULL);
CMutex g_mutexInterAdjPts(FALSE, NULL);

#endif

int CImageProcessor::GetMovieSize()
{
	return m_nMovieSize;
}

BOOL CImageProcessor::GetUHDtoFHD()
{
	return m_bUHDtoFHD;
}

BOOL CImageProcessor::GetVMCCFlag()
{
	return m_bVMCCFlag;
}

int CImageProcessor::GetMarginX()
{
	return m_nMarginX;
}

int CImageProcessor::GetMarginY()
{
	return m_nMarginY;
}

void CImageProcessor::GetMovieSizeData(int& nSrcWidth, int& nSrcHeight, int& nOutputWidth, int& nOutputHeight)
{
	if(GetMovieSize() == ESM_MOVIESIZE_FHD)
	{
		nSrcWidth = FULL_HD_WIDTH;
		nSrcHeight = FULL_HD_HEIGHT;
		nOutputWidth = MOVIE_OUTPUT_FHD_WIDTH;
		nOutputHeight = MOVIE_OUTPUT_FHD_HEIGHT;
	}
	else
	{
		if(GetUHDtoFHD())
		{
			nSrcWidth = FULL_UHD_WIDTH;
			nSrcHeight = FULL_UHD_HEIGHT;
			nOutputWidth = MOVIE_OUTPUT_FHD_WIDTH;
			nOutputHeight = MOVIE_OUTPUT_FHD_HEIGHT;
		}
		else
		{
			nSrcWidth = FULL_UHD_WIDTH;
			nSrcHeight = FULL_UHD_HEIGHT;
			nOutputWidth = MOVIE_OUTPUT_UHD_WIDTH;
			nOutputHeight = MOVIE_OUTPUT_UHD_HEIGHT;
		}

	}
}

std::vector<cv::Point2f> CImageProcessor::GetInterAdjPts()
{
	return m_vInterAdjPts;
}

void CImageProcessor::SetMovieSize(int nMovieSize)
{
	g_mutexMovieSize.Lock();
	m_nMovieSize = nMovieSize;
	g_mutexMovieSize.Unlock();
}

void CImageProcessor::SetUHDtoFHD(BOOL bUHDtoFHD)
{
	g_mutexUHDtoFHD.Lock();
	m_bUHDtoFHD = bUHDtoFHD;
	g_mutexUHDtoFHD.Unlock();
}

void CImageProcessor::SetVMCCFlag(BOOL bVMCCFlag)
{
	g_mutexVMCCFlag.Lock();
	m_bVMCCFlag = bVMCCFlag;
	g_mutexVMCCFlag.Unlock();
}

void CImageProcessor::SetMarginX(int nMarginX)
{
	g_mutexMarginX.Lock();
	m_nMarginX = nMarginX;
	g_mutexMarginX.Unlock();
}

void CImageProcessor::SetMarginY(int nMarginY)
{
	g_mutexMarginY.Lock();
	m_nMarginY = nMarginY;
	g_mutexMarginY.Unlock();
}

void CImageProcessor::SetMakeBorder(BOOL bMakeBorder)
{
	g_mutexMakeBorder.Lock();
	m_bMakeBorder = bMakeBorder;
	g_mutexMakeBorder.Unlock();
}

void CImageProcessor::SetInterAdjPts(vector<Point2f> vPts)
{
	g_mutexInterAdjPts.Lock();
	m_vInterAdjPts = vPts;
	g_mutexInterAdjPts.Unlock();
}

int CImageProcessor::AdjustMovieFrame(CESMMovieMgr *pMovieMgr, EffectInfo* pEffectData, CString strMovieId, CString strMovieName, int nFrameNum, int nDscIndex)
{
	WaitForSingleObject(pMovieMgr->hSemaphore, INFINITE);
	TRACE(_T("Start %s, %s, %d\n"), strMovieName, strMovieId, nFrameNum);

	ESMMovieData* pMovieData = NULL;
	pMovieData = pMovieMgr->GetMovieData(strMovieName, strMovieId);
	if( pMovieData == NULL)
	{
		ReleaseSemaphore(pMovieMgr->hSemaphore, 1, NULL);
		return 0;
	}

	// 이전 프로세스 진행중일 경우 대기
	while(1)
	{
		Sleep(1);
		if(ESM_CREATE_MOIVE_RESIZE <= pMovieData->GetFrameState(nFrameNum) + 1)
			break;
	}

	//-- RESIZE EVENT 
	if(pMovieData->GetBufferSize() <= nFrameNum)
	{
		ReleaseSemaphore(pMovieMgr->hSemaphore, 1, NULL);
		return 0;
	}

	stAdjustInfo AdjustData = pMovieData->GetAdjustinfo();

	int nWidth = 0, nHeight = 0;
	nWidth = pMovieData->GetSizeWidth();
	nHeight = pMovieData->GetSizeHight();
	pMovieData->OrderAdd(nFrameNum);


	//-- ROTATE EVENT 
	cuda::GpuMat* pImageMat = pMovieData->GetImage(nFrameNum); 
	if( pImageMat == NULL)
	{
		ReleaseSemaphore(pMovieMgr->hSemaphore, 1, NULL);
		return 0;
	}
	//CMiLRe 20151020 VMCC Effect(ZOOM) 기능 추가
	int nSrcWidth = pImageMat->cols;
	int nSrcHeight = pImageMat->rows;
	int nOutputWidth = MOVIE_OUTPUT_FHD_WIDTH;
	int nOutputHeight = MOVIE_OUTPUT_FHD_HEIGHT;

	double dResizeScale = (double) nSrcWidth / (double) nOutputWidth;

	int nSize = 0;

	double dSize = 0.0;
	double dRatio = (double) pImageMat->cols / (double) nWidth;
	int nRotateX = imgproc::Round(AdjustData.AdjptRotate.x * dRatio );	 // 반올림
	int nRotateY = imgproc::Round(AdjustData.AdjptRotate.y * dRatio );
	int nMoveX = imgproc::Round(AdjustData.AdjMove.x * dRatio);	 // 반올림
	int nMoveY = imgproc::Round(AdjustData.AdjMove.y * dRatio);	 // 반올림
	int nMarginX = AdjustData.stMargin.nMarginX;
	int nMarginY = AdjustData.stMargin.nMarginY;
	double dbMarginScale = (double) nWidth / (double) ( nWidth - nMarginX * 2 );
	int nResizeWidth = (int) (( nOutputWidth * dbMarginScale ) + ( nMarginX * 2 ));
	int nResizeHeight = (int) (( nOutputHeight * dbMarginScale ) + ( nMarginY * 2 ));

	if ( nMarginX < 0 || nMarginX >= nWidth / 2 || nMarginY < 0 || nMarginY >= nHeight / 2  )
	{
		pMovieData->OrderAdd(nFrameNum);
		ReleaseSemaphore(pMovieMgr->hSemaphore, 1, NULL);
		return 0;
	}

	if(nWidth > nOutputWidth && nHeight > nOutputHeight)
	{
		if (pEffectData->bZoom == TRUE && pEffectData->nZoomRatio != 100)
		{
			double dScale = (double)pEffectData->nZoomRatio / 100.0;
			cuda::GpuMat gMatCut, gMatResize;

			int nLeft = (int) ((pEffectData->nPosX - nSrcWidth/dScale/2) - nMarginX);
			int nTop = (int) ((pEffectData->nPosY - nSrcHeight/dScale/2) - nMarginY);
			int nWidth = (int) ((nSrcWidth / dScale - 1) + (nMarginX * 2));
			if((nLeft + nWidth) > nSrcWidth)
				nLeft = nLeft - ((nLeft + nWidth) - nSrcWidth);
			else if(nLeft < 0)
				nLeft = 0;
			int nHeight = (int) ((nSrcHeight / dScale - 1) + (nMarginY * 2));
			if((nTop + nHeight) > nSrcHeight)
				nTop = nTop - ((nTop + nHeight) - nSrcHeight);
			else if (nTop < 0)
				nTop = 0;
			gMatCut =  (*pImageMat)(Rect(nLeft, nTop, nWidth, nHeight));
			gMatCut.copyTo(*pImageMat);

			nRotateX = nRotateX - nLeft;
			nRotateY = nRotateY - nTop;

			nMarginX = (int) (nMarginX/dScale);
			nMarginY = (int) (nMarginY/dScale);

			// Rotate 구현
			imgproc::GpuRotateImage(pImageMat, nRotateX, nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle);
			pMovieData->OrderAdd(nFrameNum);

			//-- MOVE EVENT
			imgproc::GpuMoveImage(pImageMat, nMoveX,  nMoveY);
			pMovieData->OrderAdd(nFrameNum);

			//imgproc::GpuMakeMargin(pImageMat, nMarginX, nMarginY);

			nWidth = pImageMat->cols - nMarginX*2;
			nHeight = pImageMat->rows - nMarginY*2;
			gMatCut = (*pImageMat)(Rect(nMarginX, nMarginY, nWidth, nHeight));
			gMatCut.copyTo(*pImageMat);

			cuda::resize(*pImageMat, gMatResize, Size(nOutputWidth ,nOutputHeight), 0.0, 0.0, INTER_CUBIC );
			gMatResize.copyTo(*pImageMat);
		}
		else
		{
			cuda::GpuMat* pMatResize = new cuda::GpuMat;
			if(nWidth > nOutputWidth && nHeight > nOutputHeight)
			{
				cuda::resize(*pImageMat, *pMatResize, Size(nOutputWidth ,nOutputHeight), 0.0, 0.0, INTER_CUBIC );
				pMatResize->copyTo(*pImageMat);
			}
			delete pEffectData;

			nWidth = nOutputWidth;
			nHeight = nOutputHeight;
			nRotateX = (int) (nRotateX / dResizeScale);
			nRotateY = (int) (nRotateY / dResizeScale);
			nMoveX = (int) (nMoveX / dResizeScale);
			nMoveY = (int) (nMoveY / dResizeScale);
			nMarginX = (int) (nMarginX / dResizeScale);
			nMarginY = (int) (nMarginX / dResizeScale);

			// Rotate 구현
			imgproc::GpuRotateImage(pImageMat, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle);
			pMovieData->OrderAdd(nFrameNum);

			//-- MOVE EVENT
			imgproc::GpuMoveImage(pImageMat, nMoveX,  nMoveY);
			pMovieData->OrderAdd(nFrameNum);

			//imgproc::GpuMakeMargin(pImageMat, nMarginX, nMarginY);
			double dbMarginScale = 1 / ((double)(nWidth - nMarginX * 2) / (double)nWidth);

			cuda::resize(*pImageMat, *pMatResize, Size( (int) (nWidth*dbMarginScale), (int) (nHeight*dbMarginScale) ), 0.0, 0.0 ,INTER_CUBIC );
			int nLeft = (int)((nWidth*dbMarginScale - nWidth)/2);
			int nTop = (int)((nHeight*dbMarginScale - nHeight)/2);
			cuda::GpuMat pMatCut =  (*pMatResize)(Rect(nLeft, nTop, nWidth, nHeight));
			pMatCut.copyTo(*pImageMat);
			delete pMatResize;
		}
	}
	else
	{
		if (pEffectData->bZoom == TRUE && pEffectData->nZoomRatio != 100)
			imgproc::GpuZoomImage(pImageMat, nWidth, nHeight, pEffectData->nPosX,  pEffectData->nPosY, pEffectData->nZoomRatio, nResizeWidth, nResizeHeight);

		delete pEffectData;

		// Rotate 구현
		imgproc::GpuRotateImage(pImageMat, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle);
		pMovieData->OrderAdd(nFrameNum);

		//-- MOVE EVENT
		imgproc::GpuMoveImage(pImageMat, nMoveX,  nMoveY);
		pMovieData->OrderAdd(nFrameNum);

		//-- IMAGE CUT EVENT
		double dbMarginScale = 1 / ( (double)( nWidth - nMarginX * 2) /(double) nWidth  );
		cuda::GpuMat* pMatResize = new cuda::GpuMat;
		cuda::resize(*pImageMat, *pMatResize, Size( (int) (nWidth*dbMarginScale), (int) (nHeight*dbMarginScale) ), 0.0, 0.0 ,INTER_CUBIC );
		int nLeft = (int)((nWidth*dbMarginScale - nWidth)/2);
		int nTop = (int)((nHeight*dbMarginScale - nHeight)/2);
		cuda::GpuMat pMatCut =  (*pMatResize)(Rect(nLeft, nTop, nWidth, nHeight));
		pMatCut.copyTo(*pImageMat);
		delete pMatResize;
	}

	pMovieData->OrderAdd(nFrameNum);

	//-- Insert Logo
	if( nDscIndex > -1)
	{
		// insert Logo 구현
		imgproc::GpuInsertLogo(pImageMat, nDscIndex, nOutputWidth, nOutputHeight);
	}
	pMovieData->OrderAdd(nFrameNum);
	pMovieData->OrderAdd(nFrameNum);

	// 	delete pEffectData;
	TRACE(_T("End %s, %s, %d\n"), strMovieName, strMovieId, nFrameNum);
	ReleaseSemaphore(pMovieMgr->hSemaphore, 1, NULL);
	return 0;
}

int CImageProcessor::AdjustFrame(CESMMovieMgr *pMovieManager, MakeFrameInfo* pFrameInfo, CString strPath, BOOL bSaveImg /*= FALSE*/, BOOL bReverse /*= FALSE*/)
{
	EffectInfo pEffectData = pFrameInfo->stEffectData;

	WaitForSingleObject(pMovieManager->hSemaphore, INFINITE);

	int nSrcWidth = 0, nSrcHeight = 0, nOutputWidth = 0, nOutputHeight = 0 ;

	//Output 형식에 따른 데이터 변경.
	imgproc::GetMovieSizeData(nSrcWidth, nSrcHeight, nOutputWidth, nOutputHeight);

	//Movie Size Check
	if(nOutputWidth > nSrcWidth || nOutputHeight > nSrcHeight)
	{
		nOutputHeight = nSrcHeight;
		nOutputWidth = nSrcWidth;
	}

	CString strDscId;
	CString strFramePath = pFrameInfo->strFramePath;
	int nIndex = strFramePath.ReverseFind('\\') + 1;
	CString strTpPath = strFramePath.Right(strFramePath.GetLength() - nIndex);
	strDscId = strTpPath.Left(5);

	stAdjustInfo AdjustData = pMovieManager->GetAdjustData(strDscId);

	if(AdjustData.AdjAngle == 0 && AdjustData.AdjMove.x == 0 && AdjustData.AdjMove.y == 0)
	{
		CString strLog;
		strLog.Format(_T("[%s] Cannot Load Adjust"),strDscId);
		SendLog(5,strLog);
	}
	if(nSrcHeight == 1080 || nSrcWidth == 1920)
		TRACE(_T("==============Image Data Size Height = %d Width = %d\n"), nSrcHeight, nSrcWidth);

	int m_imagesize;
	m_imagesize = nSrcHeight*nSrcWidth*3;
	if(pFrameInfo->nImageSize != m_imagesize)
	{
		if(pFrameInfo->nImageSize == 1920 * 1080 * 3)
		{
			nSrcWidth = 1920;
			nSrcHeight = 1080;
		}
		else if(pFrameInfo->nImageSize == 3840 * 2160 * 3)
		{
			nSrcWidth = 3840;
			nSrcHeight = 2160;
		}
		m_imagesize = nSrcHeight*nSrcWidth*3;
	}

	Mat src(nSrcHeight, nSrcWidth, CV_8UC3);

	if(pFrameInfo->nImageSize != m_imagesize)
	{
		CString strMsg;
		strMsg.Format(_T("%s  record mode error"), pFrameInfo->strCameraID);

		pFrameInfo->bComplete = -100;
		SendLog(0, strMsg);
		ReleaseSemaphore(pMovieManager->hSemaphore, 1, NULL);
		return 0;
	}
	else
	{
		memcpy(src.data, pFrameInfo->Image, pFrameInfo->nImageSize);
	}

	int nRotCenterX = src.cols/2;
	int nRotCenterY = src.rows/2;
	if(pFrameInfo->bMovieReverse)
	{
		imgproc::CpuRotateImage(src,nRotCenterX,nRotCenterY,1.,1,TRUE);
	}

	if(pFrameInfo->bColorRevision)
	{
		FrameRGBInfo stColorInfo = pFrameInfo->stColorInfo;
		imgproc::DoColorRevision(&src,stColorInfo);
	}

	if(nOutputWidth != nSrcWidth)
	{
		Mat d_resize;
		resize(src, d_resize, Size(nOutputWidth, nOutputHeight), 0,0,INTER_CUBIC );
		d_resize.copyTo(src);
	}

	//Cpu Rotate
	int nRotateX = 0, nRotateY = 0, nSize = 0;
	double dSize = 0.0;
	double dRatio = (double)nOutputWidth / (double)nSrcWidth;
	nRotateX = imgproc::Round(AdjustData.AdjptRotate.x  * dRatio );	 // 반올림
	nRotateY = imgproc::Round(AdjustData.AdjptRotate.y  * dRatio );
	imgproc::CpuRotateImage(src, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle,0);

	//Cpu Move
	int nMoveX = 0, nMoveY = 0;
	nMoveX = imgproc::Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
	nMoveY = imgproc::Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
	imgproc::CpuMoveImage(&src, nMoveX,  nMoveY);

	//Cpu Move
	int nMarginX = (int) (AdjustData.stMargin.nMarginX * dRatio);
	int nMarginY = (int) (AdjustData.stMargin.nMarginY * dRatio);

	if ( nMarginX < 0 || nMarginX >= nSrcWidth/2 || nMarginY < 0 || nMarginY >= nSrcHeight/2  )
	{
		ReleaseSemaphore(pMovieManager->hSemaphore, 1, NULL);
		TRACE(_T("Image Adjust Error 2\n"));
		pFrameInfo->bComplete++;
		return 0;
	}

	if(src.cols > nOutputWidth)
	{
		int nLeft = (int)((src.cols - nOutputWidth)/2);
		int nTop  = (int)((src.rows - nOutputHeight)/2);
		Mat pMatCut =  (src)(cv::Rect(nLeft, nTop, nOutputWidth, nOutputHeight));
		pMatCut.copyTo(src);
	}
	else
	{
		//imgproc::CpuMakeMargin(&src,nMarginX,nMarginY);

		double dbMarginScale = (double) nOutputWidth / (double) (nOutputWidth - nMarginX * 2);	
		Mat  pMatResize;
		resize(src, pMatResize, cv::Size( (int) (nOutputWidth*dbMarginScale), (int) (nOutputHeight * dbMarginScale) ), 0.0, 0.0 ,INTER_CUBIC);
		int nLeft = (int)((nOutputWidth*dbMarginScale - nOutputWidth)/2);
		int nTop = (int)((nOutputHeight*dbMarginScale - nOutputHeight)/2);
		Mat pMatCut =  (pMatResize)(cv::Rect(nLeft, nTop, nOutputWidth, nOutputHeight));
		pMatCut.copyTo(src);
		pMatResize = NULL;
	}

	if(bReverse)
	{
		nRotateX = src.cols / 2;
		nRotateY = src.rows / 2;
		imgproc::CpuRotateImage(src, nRotateX ,nRotateY, 1, AdjustData.AdjAngle, bReverse);
	}

	Mat srcImage = src.clone();

	if( pFrameInfo->nDscIndex > -1)
	{
		// insert Logo 구현
		imgproc::GpuInsertLogo2(&srcImage, pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
	}

	if( pFrameInfo->nPriIndex > -1 || pFrameInfo->nPrinumIndex > 0)
	{
		// K-Zone Prism Draw
		int taValue = pFrameInfo->nPriValue;
		int saValue = pFrameInfo->nPriValue2;
		imgproc::KzonePrism(&srcImage,pFrameInfo->nPriIndex,taValue,saValue,pFrameInfo->nPrinumIndex);
	}

	//-- 카메라 ID 표시
	if( pFrameInfo->strCameraID[0] != _T('\0'))
		imgproc::GpuInsertCameraID(&srcImage,pFrameInfo->nPriIndex,pFrameInfo->strCameraID,pFrameInfo->strDSC_IP,pFrameInfo->bPrnColorInfo);

	if(pFrameInfo->Image)
		delete[] pFrameInfo->Image;

	if(pFrameInfo->bInsertWB)
	{
		Ptr<xphoto::WhiteBalancer> wb;
		wb = xphoto::createGrayworldWB();
		wb->balanceWhite(srcImage, srcImage);
	}

#ifdef INSERT_TEXT
	char strText[MAX_PATH];
	sprintf(strText,"%d_%d",pFrameInfo->nSecIdx,pFrameInfo->nFrameIndex);
	putText(srcImage,strText,cv::Point(1920,1080),CV_FONT_HERSHEY_COMPLEX,3,Scalar(0,0,0),1,8);
#endif

	int size = (int) (srcImage.total() * srcImage.elemSize());
	pFrameInfo->Image = new BYTE[size];
	memcpy( pFrameInfo->Image, srcImage.data, size);
	//delete pImageMat;
	pFrameInfo->bComplete++;

	//BMP Save
	if(bSaveImg)
	{
		IplImage*	img = new IplImage(srcImage);
		CString strData , strTemp;
		char path[MAX_PATH];
		strData.Format(_T("%s"), strPath);
		CreateDirectory(strData,NULL);
		strTemp.Format(_T("\\%s_%d_%d.bmp"), strDscId, nIndex, pFrameInfo->nFrameIndex);
		strData.Append(strTemp);
		sprintf(path, "%S", strData);
		cvSaveImage(path, img);
	}

	ReleaseSemaphore(pMovieManager->hSemaphore, 1, NULL);

	return TRUE;
}

//int CImageProcessor::AdjustFrameUsingVMCC(CESMMovieMgr *pMovieManager, MakeFrameInfo* pFrameInfo, bool bIsFullHD, CString strPath, BOOL bSaveImg, BOOL bReverse)
//{
//	EffectInfo pEffectData = pFrameInfo->stEffectData;
//
//	WaitForSingleObject(pMovieManager->hSemaphore, INFINITE);
//
//	int nSrcWidth = 0, nSrcHeight = 0, nOutputWidth = 0, nOutputHeight = 0 ;
//
//	// Output 형식에 따른 Size 정보 변경.
//	imgproc::GetMovieSizeData(nSrcWidth, nSrcHeight, nOutputWidth, nOutputHeight);
//
//	// Movie Size Check
//	if(nOutputWidth > nSrcWidth || nOutputHeight > nSrcHeight)
//	{
//		nOutputHeight = nSrcHeight;
//		nOutputWidth = nSrcWidth;
//	}
//
//	// Frame Path의 DSC ID를 통해 Adjust Data Load
//	CString strDscId;
//	CString strFramePath = pFrameInfo->strFramePath;
//	int nIndex = strFramePath.ReverseFind('\\') + 1;
//	CString strTpPath = strFramePath.Right(strFramePath.GetLength() - nIndex);
//	strDscId = strTpPath.Left(5);
//	stAdjustInfo AdjustData = pMovieManager->GetAdjustData(strDscId);
//
//	// Adjust Data 정보가 모두 0일 경우 비정상 로드 처리
//	if(AdjustData.AdjAngle == 0 && AdjustData.AdjMove.x == 0 && AdjustData.AdjMove.y == 0)
//	{
//		CString strLog;
//		strLog.Format(_T("[%s] Cannot Load Adjust"),strDscId);
//		SendLog(5,strLog);
//	}
//
//	// 입력 영상 크기가 FHD일 경우 Size 정보 출력
//	if(nSrcHeight == 1080 || nSrcWidth == 1920)
//		TRACE(_T("==============Image Data Size Height = %d Width = %d\n"), nSrcHeight, nSrcWidth);
//
//	// Frame Info의 Image Size로 입력 영상의 Width/Height 계산
//	//slee 200108 중복 연산 제거
//	int m_imagesize;
//
//	if(pFrameInfo->nImageSize == 1920 * 1080 * 3)
//	{
//		nSrcWidth = 1920;
//		nSrcHeight = 1080;
//	}
//	else if(pFrameInfo->nImageSize == 3840 * 2160 * 3)
//	{
//		nSrcWidth = 3840;
//		nSrcHeight = 2160;
//	}
//
//	m_imagesize = nSrcHeight*nSrcWidth*3;
//
//	// Frame Info에서 입력 영상 가져옴
//	Mat src(nSrcHeight, nSrcWidth, CV_8UC3);
//
//	memcpy(src.data, pFrameInfo->Image, pFrameInfo->nImageSize);
//
//	//slee 200108 발생 가능성 없는 Interlock 코드 제거
//	//if(pFrameInfo->nImageSize != m_imagesize)
//	//{
//	//	CString strMsg;
//	//	strMsg.Format(_T("%s  record mode error"), pFrameInfo->strCameraID);
//
//	//	pFrameInfo->bComplete = -100;
//	//	SendLog(0, strMsg);
//	//	ReleaseSemaphore(pMovieManager->hSemaphore, 1, NULL);
//	//	return 0;
//	//}
//	//else
//	//{
//	//	memcpy(src.data, pFrameInfo->Image, pFrameInfo->nImageSize);
//	//}
//
//	//slee 200108 CpuRotateImage() 구문 통합으로 주석처리
//	//int nRotCenterX = src.cols/2;
//	//int nRotCenterY = src.rows/2;
//	//if(pFrameInfo->bMovieReverse)
//	//{
//	//	imgproc::CpuRotateImage(src,nRotCenterX,nRotCenterY,1.,1,TRUE);
//	//}
//
//	// Color Revision 적용시 지정된 Color balance로 조절
//	if(pFrameInfo->bColorRevision)
//	{
//		FrameRGBInfo stColorInfo = pFrameInfo->stColorInfo;
//		imgproc::DoColorRevision(&src,stColorInfo);
//	}
//
//	// 출력 영상 Size 백업 (why?)
//	int nPreWidth = nOutputWidth;
//	int nPreHeight = nOutputHeight;
//
//	// 입출력 영상 크기 동일하게 설정 (why?)
//	nOutputWidth = nSrcWidth;
//	nOutputHeight = nSrcHeight;
//
//	// K-Zone Draw
//	//wgkim 181004 K-Zone
//	CESMMovieKZoneMgr* pMovieKZoneMgr = pMovieManager->GetMovieKZoneMgr();
//	pMovieKZoneMgr->DoDrawKZone(strDscId, src);
//
//	//wgkim 180513
//	// 정방향일 경우
//	if(pFrameInfo->nVertical == 0)
//	{
//		//Cpu Move
//		int nMoveX = 0, nMoveY = 0;
//		nMoveX = imgproc::Round(AdjustData.AdjMove.x);	 // 반올림
//		nMoveY = imgproc::Round(AdjustData.AdjMove.y);	 // 반올림
//		imgproc::CpuMoveImage(&src, nMoveX,  nMoveY);
//
//		//Cpu Rotate
//		int nRotateX = 0, nRotateY = 0;
//		double dRatio = (double)nOutputWidth / (double)nSrcWidth; // 무조건 1 (why?)
//		nRotateX = imgproc::Round((AdjustData.AdjMove.x+AdjustData.AdjptRotate.x) * dRatio );	 // 반올림
//		nRotateY = imgproc::Round((AdjustData.AdjMove.y+AdjustData.AdjptRotate.y) * dRatio );
//
//		//slee 200108 Reverse 관련 CpuRotateImage() function 통합
//		if(pFrameInfo->bMovieReverse || bReverse)
//			imgproc::CpuRotateImage(src, nRotateX, nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle, TRUE);
//		else
//			imgproc::CpuRotateImage(src, nRotateX, nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle, FALSE);
//
//		//Cpu Margin
//		int nMarginX = (int) (AdjustData.stMargin.nMarginX * dRatio);//((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginX() * dRatio;
//		int nMarginY = (int) (AdjustData.stMargin.nMarginY * dRatio);//((CESMMovieMgr*)pMovieThreadManager->m_pParent)->GetMarginY() * dRatio;
//
//		// Margin이 음수거나 너무 클 경우 Error 처리
//		if ( nMarginX < 0 || nMarginX >= nSrcWidth/2 || nMarginY < 0 || nMarginY >= nSrcHeight/2  )
//		{
//			ReleaseSemaphore(pMovieManager->hSemaphore, 1, NULL);
//			TRACE(_T("Image Adjust Error 2\n"));
//			pFrameInfo->bComplete++;
//			return 0;
//		}
//
//		// 입력 크기가 출력 크기보다 클 경우 Crop
//		if( src.cols > nOutputWidth)
//		{
//			int nModifyMarginX = (src.cols - nOutputWidth) /2;
//			int nModifyMarginY = (src.rows - nOutputHeight) /2;
//			Mat pMatCut =  (src)(Rect(nModifyMarginX, nModifyMarginY, nOutputWidth, nOutputHeight));
//			src = pMatCut.clone();
//		}
//		// 입력 크기가 출력 크기와 같거나 작을 경우
//		else
//		{
//			CRect rtMargin = AdjustData.rtMargin;
//
//			// 지정된 Margin 위치가 없을 경우
//			if(	rtMargin.top == 0 &&
//				rtMargin.left == 0 &&
//				rtMargin.right == 0 &&
//				rtMargin.bottom == 0)
//			{
//				//slee 200108 수식 수정 (굳이 왜 역수로 계산?)
//				//double dbMarginScale = 1 / ( (double)( nOutputWidth - nMarginX * 2) /(double) nOutputWidth  );	
//				double dbMarginScale = (double) nOutputWidth / (double)(nOutputWidth - nMarginX * 2);	
//				Mat  pMatResize;
//				// Margin 제외한 영역이 출력 크기가 되도록 resize
//				resize(src, pMatResize, cv::Size( (int) (nOutputWidth * dbMarginScale), (int) (nOutputHeight * dbMarginScale) ), 0.0, 0.0, INTER_CUBIC);
//				
//				// Margin 제외하여 출력 크기로 Crop
//				int nLeft = (int)((nOutputWidth*dbMarginScale - nOutputWidth)/2);
//				int nTop = (int)((nOutputHeight*dbMarginScale - nOutputHeight)/2);
//
//				Mat pMatCut =  (pMatResize)(cv::Rect(nLeft, nTop, nOutputWidth, nOutputHeight));
//				pMatCut.copyTo(src);
//				pMatResize = NULL;
//			}
//			// 지정된 Margin 위치가 있을 경우
//			else
//			{
//				// 지정된 Margin 위치 Crop 후 Resize
//				Mat pMatCut =  src(Rect(
//					rtMargin.left, 
//					rtMargin.top, 
//					rtMargin.right-rtMargin.left, 
//					rtMargin.bottom-rtMargin.top));
//
//				resize(pMatCut, pMatCut, cv::Size(nOutputWidth, nOutputHeight), 0.0, 0.0, INTER_CUBIC);
//				pMatCut.copyTo(src);
//			}
//		}
//
//		// Logo Zoom 사용시
//		if(pFrameInfo->bLogoZoom)
//		{
//			// DSC ID가 정상적일 경우
//			if( pFrameInfo->nDscIndex > -1)
//			{
//				Mat srcLogo(src.rows, src.cols, CV_8UC3);
//				srcLogo = src.clone();
//				// Insert logo
//				imgproc::GpuInsertLogo2(&srcLogo, pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
//				src = srcLogo.clone();
//			}
//
//			// 영상크기가 FHD가 아니면서 DSC ID가 유효하거나 Prism ID가 유효할 경우
//			if( !bIsFullHD && (pFrameInfo->nPriIndex > -1 || pFrameInfo->nPrinumIndex > 0))
//			{
//				Mat srcPrism(src.rows, src.cols, CV_8UC3);
//				srcPrism = src.clone();
//				
//				// 지정 인덱스의 K-Zone Draw (Transparency 지정)
//				int taValue = pFrameInfo->nPriValue;
//				int saValue = pFrameInfo->nPriValue2;
//				imgproc::KzonePrism(&srcPrism, pFrameInfo->nPriIndex, taValue, saValue, pFrameInfo->nPrinumIndex);
//				src = srcPrism.clone();
//			}
//		}
//
//		//if(bReverse)
//		//{
//		//	imgproc::CpuRotateImage(src,nRotCenterX,nRotCenterY,1,AdjustData.AdjAngle,TRUE);
//		//	//flip(src,src,-1);
//		//	//pEffectData.nPosX = nSrcWidth - pEffectData.nPosX;
//		//	//pEffectData.nPosY = nSrcHeight - pEffectData.nPosY;
//		//}
//
//		// Step Frame 일 경우 추가 Processing 없이 완료 처리 후 결과 영상 Frame Info에 전달
//		if( pEffectData.bStepFrame == TRUE &&
//			pEffectData.nStartZoom != pEffectData.nEndZoom)
//		{
//			pFrameInfo->nWidth = nSrcWidth;
//			pFrameInfo->nHeight = nSrcHeight;
//			memcpy(pFrameInfo->Image,src.data,nSrcWidth*nSrcHeight*3);
//			pFrameInfo->bComplete++;
//			return TRUE;
//		}
//
//		double dScale = (double)pEffectData.nZoomRatio / 100.0;
//
//		if(dScale == 0)
//		{
//			dScale = 1;
//			SendLog(5,_T("Template Error"));
//		}
//
//		if(pEffectData.nPosX == 0 && pEffectData.nPosY == 0)
//		{
//			pEffectData.nPosX = nSrcWidth/2;
//			pEffectData.nPosY = nSrcHeight/2;
//		}
//
//		// Effect Data의 Zoom scale에 따른 영상 사이즈 Crop
//		Mat MatCut;
//		int nLeft = (int) (pEffectData.nPosX- nSrcWidth/dScale/2);
//		int nTop = (int) (pEffectData.nPosY - nSrcHeight/dScale/2);
//		int nWidth = (int) (nSrcWidth/dScale - 1);
//		if((nLeft + nWidth) > nSrcWidth)
//			nLeft = nLeft - ((nLeft + nWidth) - nSrcWidth); 
//		else if(nLeft < 0)
//			nLeft = 0;
//		int nHeight = (int) (nSrcHeight/dScale - 1);
//		if((nTop + nHeight) > nSrcHeight)
//			nTop = nTop - ((nTop + nHeight) - nSrcHeight);
//		else if (nTop < 0)
//			nTop = 0;
//		MatCut =  (src)(Rect(nLeft, nTop, nWidth, nHeight));
//		src = MatCut.clone();
//	}
//	// 세로 방향일 경우
//	else
//	{
//		//wgkim 180513 Vertical 변환 코드
//		// vertial & horizontal flip
//		if(bReverse)
//		{
//			flip(src,src,-1);
//		}
//
//		double degree = 0;
//		double resizeRatio = (16./9);
//
//		Mat rotateImage(src.cols, src.rows, CV_8UC3);
//		if(pFrameInfo->nVertical == -1)
//		{
//			transpose(src, rotateImage);
//			flip(rotateImage, rotateImage, 1); // horizontal flip
//		}
//
//		else if(pFrameInfo->nVertical == 1)
//		{
//			transpose(src, rotateImage);
//			flip(rotateImage, rotateImage, 0); // vertical flip
//		}
//
//		Point2f ptVerticalCenterPoint;
//		ptVerticalCenterPoint.x = (float) pFrameInfo->ptVerticalCenterPoint.x;
//		ptVerticalCenterPoint.y = (float) pFrameInfo->ptVerticalCenterPoint.y;
//
//		Rect2d rtRoiRect;
//		rtRoiRect.width = src.rows;
//		rtRoiRect.height = src.rows* (9./16);
//		rtRoiRect.x = 0;
//		rtRoiRect.y = (src.cols * (ptVerticalCenterPoint.y / src.rows)) - (rtRoiRect.height/2);
//
//		if(rtRoiRect.y < 0)
//			rtRoiRect.y = 0;
//		if(rtRoiRect.y + rtRoiRect.height > src.cols)
//			rtRoiRect.y = src.cols - rtRoiRect.height-1;
//		rotateImage(rtRoiRect).copyTo(src);
//
//		Mat MatCut;
//
//		double dScale = (double)pEffectData.nZoomRatio / 100.0;
//
//		pFrameInfo->nWidth = src.cols;
//		pFrameInfo->nHeight= src.rows;
//
//		//180524
//		ptVerticalCenterPoint.x *= (float) (src.cols/nSrcWidth);
//		ptVerticalCenterPoint.y = (float) (src.rows/2);
//
//		//180525
//		double nWidthBoundRatio_Left =  ((double)src.cols/2) - ((double)src.cols/2) * pow((double)src.rows/src.cols, 2);
//		double nWidthBoundRatio_Right = ((double)src.cols/2) + ((double)src.cols/2) * pow((double)src.rows/src.cols, 2);
//
//		if( ptVerticalCenterPoint.x < nWidthBoundRatio_Left)
//			ptVerticalCenterPoint.x = (float) nWidthBoundRatio_Left;
//		if( ptVerticalCenterPoint.x > nWidthBoundRatio_Right)
//			ptVerticalCenterPoint.x = (float) nWidthBoundRatio_Right;
//
//		ptVerticalCenterPoint.x = (float)
//			(abs(ptVerticalCenterPoint.x - nWidthBoundRatio_Left) * 
//			((double)src.cols/(nWidthBoundRatio_Right- nWidthBoundRatio_Left)));
//
//		if(dScale == 0)
//		{
//			dScale = 1;
//			SendLog(5,_T("Vertical Error"));
//		}
//
//		if(pEffectData.nPosX == 0 && pEffectData.nPosY == 0)
//		{
//			pEffectData.nPosX = (int) (src.cols * (ptVerticalCenterPoint.x/src.cols));//nSrcWidth/2;
//			pEffectData.nPosY = src.rows/2;
//		}
//		pEffectData.nPosX = (int) (src.cols * (ptVerticalCenterPoint.x/src.cols));//nSrcWidth/2;
//		pEffectData.nPosY = (int) ptVerticalCenterPoint.y;
//
//		if( pEffectData.bStepFrame == TRUE &&
//			pEffectData.nStartZoom != pEffectData.nEndZoom)
//		{
//			pFrameInfo->nWidth = src.cols;
//			pFrameInfo->nHeight= src.rows;
//			memcpy(pFrameInfo->Image,src.data,src.cols*src.rows*3);
//			pFrameInfo->bComplete++;
//
//			//wgkim 180528
//			pFrameInfo->stEffectData.nPosX = pEffectData.nPosX;
//			pFrameInfo->stEffectData.nPosY = pEffectData.nPosY;
//
//			return TRUE;
//		}
//
//		int nLeft = (int) (pEffectData.nPosX- src.cols/dScale/2);
//		int nTop = (int) (pEffectData.nPosY - src.rows/dScale/2);
//		int nWidth = (int) (src.cols/dScale - 1);
//		if((nLeft + nWidth) > src.cols)
//			nLeft = nLeft - ((nLeft + nWidth) - src.cols);
//		else if(nLeft < 0)
//			nLeft = 0;
//
//		int nHeight = (int) (src.rows/dScale - 1);
//		if((nTop + nHeight) > src.rows)
//			nTop = nTop - ((nTop + nHeight) - src.rows);
//		else if (nTop < 0)
//			nTop = 0;
//		MatCut =  (src)(Rect(nLeft, nTop, nWidth, nHeight));
//		src = MatCut.clone();
//
//		//wgkim 180528
//		pFrameInfo->stEffectData.nPosX = pEffectData.nPosX;
//		pFrameInfo->stEffectData.nPosY = pEffectData.nPosY;
//		//////////////////////////////////////////////////////////
//	}
//	Mat MatResize;
//
//	// 출력 형식이 Full HD가 아닌 경우 출력 크기로 resize
//	if(!bIsFullHD)
//	{
//		resize(src, MatResize, Size(nPreWidth, nPreHeight), 0.0, 0.0, INTER_CUBIC );
//		pFrameInfo->nWidth = nPreWidth;
//		pFrameInfo->nHeight = nPreHeight;
//
//		MatResize.copyTo(src);
//	}
//	// Full HD인 경우 변경된 출력 크기로 Resize
//	else
//	{
//		resize(src, MatResize, Size(nOutputWidth, nOutputHeight), 0.0, 0.0, INTER_CUBIC );
//		pFrameInfo->nWidth = nOutputWidth;
//		pFrameInfo->nHeight = nOutputHeight;
//
//		MatResize.copyTo(src);
//	}
//
//	Mat srcImage = src.clone();
//
//	if(!pFrameInfo->bLogoZoom)
//	{
//		//Insert Logo
//		if( pFrameInfo->nDscIndex > -1)
//		{
//			// insert Logo 구현
//			imgproc::GpuInsertLogo2(&srcImage, pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
//		}
//		//#endif
//	}
//
//
//	// Full HD 형식일 경우 Camera ID 출력
//	if(bIsFullHD)
//	{
//		if( pFrameInfo->strCameraID[0] != _T('\0'))
//			imgproc::GpuInsertCameraID(&srcImage,pFrameInfo->nPriIndex,pFrameInfo->strCameraID,pFrameInfo->strDSC_IP,pFrameInfo->bPrnColorInfo);
//	}
//
//	// White balance 여부 확인 후 적용
//	if(pFrameInfo->bInsertWB)
//	{
//		Ptr<xphoto::WhiteBalancer> wb;
//		wb = xphoto::createGrayworldWB();
//		wb->balanceWhite(srcImage, srcImage);
//	}
//
//	delete[] pFrameInfo->Image;
//	pFrameInfo->Image = NULL;
//
//#ifdef INSERT_TEXT
//	char strText[MAX_PATH];
//	sprintf(strText,"%d_%d",pFrameInfo->nSecIdx,pFrameInfo->nFrameIndex);
//	putText(srcImage,strText,cv::Point(960,450),CV_FONT_HERSHEY_COMPLEX,3,Scalar(0,0,0),1,8);
//#endif
//
//	int size = (int) (srcImage.total() * srcImage.elemSize());
//	pFrameInfo->Image = new BYTE[size];
//
//	memcpy( pFrameInfo->Image, srcImage.data, size);
//	//delete pImageMat;
//	pFrameInfo->bComplete++;
//
//	//BMP Save
//	if(bSaveImg)
//	{
//		IplImage*	img = new IplImage(srcImage);
//		CString strData , strTemp;
//		char path[MAX_PATH];
//		strData.Format(_T("%s"), strPath);
//		CreateDirectory(strData,NULL);
//		strTemp.Format(_T("\\%s_%d_%d.bmp"), strDscId, nIndex, pFrameInfo->nFrameIndex);
//		strData.Append(strTemp);
//		sprintf(path, "%S", strData);
//		cvSaveImage(path, img);
//	}
//
//	ReleaseSemaphore(pMovieManager->hSemaphore, 1, NULL);
//
//	return TRUE;
//}

int CImageProcessor::AdjustFrameUsingVMCC(CESMMovieMgr *pMovieManager, MakeFrameInfo* pFrameInfo, bool bIsFullHD, CString strPath, BOOL bSaveImg, BOOL bReverse)
{
	EffectInfo pEffectData = pFrameInfo->stEffectData;

	WaitForSingleObject(pMovieManager->hSemaphore, INFINITE);

	int nSrcWidth = 0, nSrcHeight = 0, nOutputWidth = 0, nOutputHeight = 0 ;

	// Output 형식에 따른 Size 정보 변경.
	imgproc::GetMovieSizeData(nSrcWidth, nSrcHeight, nOutputWidth, nOutputHeight);

	// Movie Size Check
	if(nOutputWidth > nSrcWidth || nOutputHeight > nSrcHeight)
	{
		nOutputHeight = nSrcHeight;
		nOutputWidth = nSrcWidth;
	}

	// Frame Path의 DSC ID를 통해 Adjust Data Load
	CString strDscId;
	CString strFramePath = pFrameInfo->strFramePath;
	int nIndex = strFramePath.ReverseFind('\\') + 1;
	CString strTpPath = strFramePath.Right(strFramePath.GetLength() - nIndex);
	strDscId = strTpPath.Left(5);
	stAdjustInfo AdjustData = pMovieManager->GetAdjustData(strDscId);

	// Adjust Data 정보가 모두 0일 경우 비정상 로드 처리
	if(AdjustData.AdjAngle == 0 && AdjustData.AdjMove.x == 0 && AdjustData.AdjMove.y == 0)
	{
		CString strLog;
		strLog.Format(_T("[%s] Cannot Load Adjust"),strDscId);
		SendLog(5,strLog);
	}

	// 입력 영상 크기가 FHD일 경우 Size 정보 출력
	if(nSrcHeight == 1080 || nSrcWidth == 1920)
		TRACE(_T("==============Image Data Size Height = %d Width = %d\n"), nSrcHeight, nSrcWidth);

	// Frame Info의 Image Size로 입력 영상의 Width/Height 계산
	//slee 200108 중복 연산 제거
	int m_imagesize;

	if(pFrameInfo->nImageSize == 1920 * 1080 * 3)
	{
		nSrcWidth = 1920;
		nSrcHeight = 1080;
	}
	else if(pFrameInfo->nImageSize == 3840 * 2160 * 3)
	{
		nSrcWidth = 3840;
		nSrcHeight = 2160;
	}

	m_imagesize = nSrcHeight*nSrcWidth*3;

	// Frame Info에서 입력 영상 가져옴
	Mat src(nSrcHeight, nSrcWidth, CV_8UC3);

	memcpy(src.data, pFrameInfo->Image, pFrameInfo->nImageSize);

	//slee 200108 발생 가능성 없는 Interlock 코드 제거
	//if(pFrameInfo->nImageSize != m_imagesize)
	//{
	//	CString strMsg;
	//	strMsg.Format(_T("%s  record mode error"), pFrameInfo->strCameraID);

	//	pFrameInfo->bComplete = -100;
	//	SendLog(0, strMsg);
	//	ReleaseSemaphore(pMovieManager->hSemaphore, 1, NULL);
	//	return 0;
	//}
	//else
	//{
	//	memcpy(src.data, pFrameInfo->Image, pFrameInfo->nImageSize);
	//}

	//slee 200108 CpuRotateImage() 구문 통합으로 주석처리
	//int nRotCenterX = src.cols/2;
	//int nRotCenterY = src.rows/2;
	//if(pFrameInfo->bMovieReverse)
	//{
	//	imgproc::CpuRotateImage(src,nRotCenterX,nRotCenterY,1.,1,TRUE);
	//}

	// Color Revision 적용시 지정된 Color balance로 조절
	if(pFrameInfo->bColorRevision)
	{
		FrameRGBInfo stColorInfo = pFrameInfo->stColorInfo;
		imgproc::DoColorRevision(&src,stColorInfo);
	}

	// 출력 영상 Size 백업 (why?)
	int nPreWidth = nOutputWidth;
	int nPreHeight = nOutputHeight;

	// 입출력 영상 크기 동일하게 설정 (why?)
	nOutputWidth = nSrcWidth;
	nOutputHeight = nSrcHeight;

	// K-Zone Draw
	//wgkim 181004 K-Zone
	CESMMovieKZoneMgr* pMovieKZoneMgr = pMovieManager->GetMovieKZoneMgr();
	pMovieKZoneMgr->DoDrawKZone(strDscId, src);

	//wgkim 180513
	// 정방향일 경우
	if(pFrameInfo->nVertical == 0)
	{
		//Rect2f rtMargin(AdjustData.rtMargin.left, AdjustData.rtMargin.top, AdjustData.rtMargin.right - AdjustData.rtMargin.left, AdjustData.rtMargin.bottom - AdjustData.rtMargin.top);

		// slee 200217 Adjust 통합
		//AdjustImage(src, src, Size(nOutputWidth, nOutputHeight), rtMargin, AdjustData.AdjAngle - 90.f, AdjustData.AdjSize, AdjustData.AdjMove.x, AdjustData.AdjMove.y);

		// Adjust Data가 전부 0일 경우 Adjust 처리 배제
		if((AdjustData.AdjMove.x == 0 && AdjustData.AdjMove.y == 0 && AdjustData.AdjAngle == 0. && AdjustData.AdjSize == 0.) ||
			abs(AdjustData.AdjMove.x) > nSrcWidth / 2 || abs(AdjustData.AdjMove.y) > nSrcHeight / 2 ||
				AdjustData.AdjptRotate.x < 0 || AdjustData.AdjptRotate.y < 0 ||
				AdjustData.AdjptRotate.x > nSrcWidth || AdjustData.AdjptRotate.y > nSrcHeight)
		{
			SendLog(5,_T("Adjust Data Error!"));
		}
		else
		{
			// Step Frame 일 경우 기본 Adjust 적용
			if( pEffectData.bStepFrame == TRUE &&
				pEffectData.nStartZoom != pEffectData.nEndZoom)
			{
				AdjustImage(src, src, Size(nOutputWidth, nOutputHeight), AdjustData, m_bMakeBorder, bReverse);
			}
			else
			{
				AdjustImageUsingEffectData(src, src, Size(nOutputWidth, nOutputHeight), AdjustData, pEffectData, m_bMakeBorder, bReverse);
			}
		}

		// Logo Zoom 사용시
		if(pFrameInfo->bLogoZoom)
		{
			// DSC ID가 정상적일 경우
			if( pFrameInfo->nDscIndex > -1)
			{
				Mat srcLogo(src.rows, src.cols, CV_8UC3);
				srcLogo = src.clone();
				// Insert logo
				imgproc::GpuInsertLogo2(&srcLogo, pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
				src = srcLogo.clone();
			}

			// 영상크기가 FHD가 아니면서 DSC ID가 유효하거나 Prism ID가 유효할 경우
			if( !bIsFullHD && (pFrameInfo->nPriIndex > -1 || pFrameInfo->nPrinumIndex > 0))
			{
				Mat srcPrism(src.rows, src.cols, CV_8UC3);
				srcPrism = src.clone();

				// 지정 인덱스의 K-Zone Draw (Transparency 지정)
				int taValue = pFrameInfo->nPriValue;
				int saValue = pFrameInfo->nPriValue2;
				imgproc::KzonePrism(&srcPrism, pFrameInfo->nPriIndex, taValue, saValue, pFrameInfo->nPrinumIndex);
				src = srcPrism.clone();
			}
		}

		//if(bReverse)
		//{
		//	imgproc::CpuRotateImage(src,nRotCenterX,nRotCenterY,1,AdjustData.AdjAngle,TRUE);
		//	//flip(src,src,-1);
		//	//pEffectData.nPosX = nSrcWidth - pEffectData.nPosX;
		//	//pEffectData.nPosY = nSrcHeight - pEffectData.nPosY;
		//}

		// Step Frame 일 경우 추가 Processing 없이 완료 처리 후 결과 영상 Frame Info에 전달
		if( pEffectData.bStepFrame == TRUE &&
			pEffectData.nStartZoom != pEffectData.nEndZoom)
		{
			pFrameInfo->nWidth = nSrcWidth;
			pFrameInfo->nHeight = nSrcHeight;
			memcpy(pFrameInfo->Image,src.data,nSrcWidth*nSrcHeight*3);
			pFrameInfo->bComplete++;
			
			//wgkim 180528
			//pFrameInfo->stEffectData.nPosX = pEffectData.nPosX;
			//pFrameInfo->stEffectData.nPosY = pEffectData.nPosY;

			//return TRUE;
		}

		//double dScale = (double)pEffectData.nZoomRatio / 100.0;

		//if(dScale == 0)
		//{
		//	dScale = 1;
		//	SendLog(5,_T("Template Error"));
		//}

		//if(pEffectData.nPosX == 0 && pEffectData.nPosY == 0)
		//{
		//	pEffectData.nPosX = nSrcWidth/2;
		//	pEffectData.nPosY = nSrcHeight/2;
		//}

		//// Effect Data의 Zoom scale에 따른 영상 사이즈 Crop
		//Mat MatCut;
		//int nLeft = (int) (pEffectData.nPosX- nSrcWidth/dScale/2);
		//int nTop = (int) (pEffectData.nPosY - nSrcHeight/dScale/2);
		//int nWidth = (int) (nSrcWidth/dScale - 1);
		//if((nLeft + nWidth) > nSrcWidth)
		//	nLeft = nLeft - ((nLeft + nWidth) - nSrcWidth); 
		//else if(nLeft < 0)
		//	nLeft = 0;
		//int nHeight = (int) (nSrcHeight/dScale - 1);
		//if((nTop + nHeight) > nSrcHeight)
		//	nTop = nTop - ((nTop + nHeight) - nSrcHeight);
		//else if (nTop < 0)
		//	nTop = 0;
		//MatCut =  (src)(Rect(nLeft, nTop, nWidth, nHeight));
		//src = MatCut.clone();
	}
	// 세로 방향일 경우
	else
	{
		//wgkim 180513 Vertical 변환 코드
		// vertial & horizontal flip
		if(bReverse)
		{
			flip(src,src,-1);
		}

		double degree = 0;
		double resizeRatio = (16./9);

		Mat rotateImage(src.cols, src.rows, CV_8UC3);
		if(pFrameInfo->nVertical == -1)
		{
			transpose(src, rotateImage);
			flip(rotateImage, rotateImage, 1); // horizontal flip
		}

		else if(pFrameInfo->nVertical == 1)
		{
			transpose(src, rotateImage);
			flip(rotateImage, rotateImage, 0); // vertical flip
		}

		Point2f ptVerticalCenterPoint;
		ptVerticalCenterPoint.x = (float) pFrameInfo->ptVerticalCenterPoint.x;
		ptVerticalCenterPoint.y = (float) pFrameInfo->ptVerticalCenterPoint.y;

		Rect2d rtRoiRect;
		rtRoiRect.width = src.rows;
		rtRoiRect.height = src.rows* (9./16);
		rtRoiRect.x = 0;
		rtRoiRect.y = (src.cols * (ptVerticalCenterPoint.y / src.rows)) - (rtRoiRect.height/2);

		if(rtRoiRect.y < 0)
			rtRoiRect.y = 0;
		if(rtRoiRect.y + rtRoiRect.height > src.cols)
			rtRoiRect.y = src.cols - rtRoiRect.height-1;
		rotateImage(rtRoiRect).copyTo(src);

		Mat MatCut;

		double dScale = (double)pEffectData.nZoomRatio / 100.0;

		pFrameInfo->nWidth = src.cols;
		pFrameInfo->nHeight= src.rows;

		//180524
		ptVerticalCenterPoint.x *= (float) (src.cols/nSrcWidth);
		ptVerticalCenterPoint.y = (float) (src.rows/2);

		//180525
		double nWidthBoundRatio_Left =  ((double)src.cols/2) - ((double)src.cols/2) * pow((double)src.rows/src.cols, 2);
		double nWidthBoundRatio_Right = ((double)src.cols/2) + ((double)src.cols/2) * pow((double)src.rows/src.cols, 2);

		if( ptVerticalCenterPoint.x < nWidthBoundRatio_Left)
			ptVerticalCenterPoint.x = (float) nWidthBoundRatio_Left;
		if( ptVerticalCenterPoint.x > nWidthBoundRatio_Right)
			ptVerticalCenterPoint.x = (float) nWidthBoundRatio_Right;

		ptVerticalCenterPoint.x = (float)
			(abs(ptVerticalCenterPoint.x - nWidthBoundRatio_Left) * 
			((double)src.cols/(nWidthBoundRatio_Right- nWidthBoundRatio_Left)));

		if(dScale == 0)
		{
			dScale = 1;
			SendLog(5,_T("Vertical Error"));
		}

		if(pEffectData.nPosX == 0 && pEffectData.nPosY == 0)
		{
			pEffectData.nPosX = (int) (src.cols * (ptVerticalCenterPoint.x/src.cols));//nSrcWidth/2;
			pEffectData.nPosY = src.rows/2;
		}
		pEffectData.nPosX = (int) (src.cols * (ptVerticalCenterPoint.x/src.cols));//nSrcWidth/2;
		pEffectData.nPosY = (int) ptVerticalCenterPoint.y;

		if( pEffectData.bStepFrame == TRUE &&
			pEffectData.nStartZoom != pEffectData.nEndZoom)
		{
			pFrameInfo->nWidth = src.cols;
			pFrameInfo->nHeight= src.rows;
			memcpy(pFrameInfo->Image,src.data,src.cols*src.rows*3);
			pFrameInfo->bComplete++;

			//wgkim 180528
			pFrameInfo->stEffectData.nPosX = pEffectData.nPosX;
			pFrameInfo->stEffectData.nPosY = pEffectData.nPosY;

			return TRUE;
		}

		int nLeft = (int) (pEffectData.nPosX- src.cols/dScale/2);
		int nTop = (int) (pEffectData.nPosY - src.rows/dScale/2);
		int nWidth = (int) (src.cols/dScale - 1);
		if((nLeft + nWidth) > src.cols)
			nLeft = nLeft - ((nLeft + nWidth) - src.cols);
		else if(nLeft < 0)
			nLeft = 0;

		int nHeight = (int) (src.rows/dScale - 1);
		if((nTop + nHeight) > src.rows)
			nTop = nTop - ((nTop + nHeight) - src.rows);
		else if (nTop < 0)
			nTop = 0;
		MatCut =  (src)(Rect(nLeft, nTop, nWidth, nHeight));
		src = MatCut.clone();

		//wgkim 180528
		pFrameInfo->stEffectData.nPosX = pEffectData.nPosX;
		pFrameInfo->stEffectData.nPosY = pEffectData.nPosY;
		//////////////////////////////////////////////////////////
	}
	Mat MatResize;

	// 출력 형식이 Full HD가 아닌 경우 출력 크기로 resize
	if(!bIsFullHD)
	{
		resize(src, MatResize, Size(nPreWidth, nPreHeight), 0.0, 0.0, INTER_CUBIC );
		pFrameInfo->nWidth = nPreWidth;
		pFrameInfo->nHeight = nPreHeight;

		MatResize.copyTo(src);
	}
	// Full HD인 경우 변경된 출력 크기로 Resize
	else
	{
		resize(src, MatResize, Size(nOutputWidth, nOutputHeight), 0.0, 0.0, INTER_CUBIC );
		pFrameInfo->nWidth = nOutputWidth;
		pFrameInfo->nHeight = nOutputHeight;

		MatResize.copyTo(src);
	}

	Mat srcImage = src.clone();

	if(!pFrameInfo->bLogoZoom)
	{
		//Insert Logo
		if( pFrameInfo->nDscIndex > -1)
		{
			// insert Logo 구현
			imgproc::GpuInsertLogo2(&srcImage, pFrameInfo->nDscIndex, pFrameInfo->b3DLogo);
		}
		//#endif
	}


	// Full HD 형식일 경우 Camera ID 출력
	if(bIsFullHD)
	{
		if( pFrameInfo->strCameraID[0] != _T('\0'))
			imgproc::GpuInsertCameraID(&srcImage,pFrameInfo->nPriIndex,pFrameInfo->strCameraID,pFrameInfo->strDSC_IP,pFrameInfo->bPrnColorInfo);
	}

	// White balance 여부 확인 후 적용
	if(pFrameInfo->bInsertWB)
	{
		Ptr<xphoto::WhiteBalancer> wb;
		wb = xphoto::createGrayworldWB();
		wb->balanceWhite(srcImage, srcImage);
	}

	delete[] pFrameInfo->Image;
	pFrameInfo->Image = NULL;

#ifdef INSERT_TEXT
	char strText[MAX_PATH];
	sprintf(strText,"%d_%d",pFrameInfo->nSecIdx,pFrameInfo->nFrameIndex);
	putText(srcImage,strText,cv::Point(960,450),CV_FONT_HERSHEY_COMPLEX,3,Scalar(0,0,0),1,8);
#endif

	int size = (int) (srcImage.total() * srcImage.elemSize());
	pFrameInfo->Image = new BYTE[size];

	memcpy( pFrameInfo->Image, srcImage.data, size);
	//delete pImageMat;
	pFrameInfo->bComplete++;

	//BMP Save
	if(bSaveImg)
	{
		IplImage*	img = new IplImage(srcImage);
		CString strData , strTemp;
		char path[MAX_PATH];
		strData.Format(_T("%s"), strPath);
		CreateDirectory(strData,NULL);
		strTemp.Format(_T("\\%s_%d_%d.bmp"), strDscId, nIndex, pFrameInfo->nFrameIndex);
		strData.Append(strTemp);
		sprintf(path, "%S", strData);
		cvSaveImage(path, img);
	}

	ReleaseSemaphore(pMovieManager->hSemaphore, 1, NULL);

	return TRUE;
}


int CImageProcessor::Round(double dData)
{
	int nResult = 0;
	if( dData == 0)
		nResult = 0;
	else if( dData > 0.0)
		nResult = int(dData + 0.5);
	else if( dData < 0.0)
		nResult = int(dData - 0.5);

	return nResult;
}

double CImageProcessor::GetMaxValue(double dbBlue, double dbGreen, double dbRed)
{
	double dMax = dbBlue;

	if(dbGreen > dMax)
		dMax = dbGreen;

	if(dbRed > dMax)
		dMax = dbRed;

	return dMax;
}

double CImageProcessor::GetMinValue(double dbBlue, double dbGreen, double dbRed)
{
	double dMin = dbBlue;

	if(dbGreen < dMin)
		dMin = dbGreen;

	if(dbRed < dMin)
		dMin = dbRed;

	return dMin;
}

double CImageProcessor::NormValue(int nValue)
{
	double dValue = (double)nValue / (double)MAXDEPTH;

	return dValue;
}

double CImageProcessor::GetSaturation(double dbValue, double dbMin)
{
	double dbResult;

	if(dbValue == 0.0)
		dbResult = 0;
	else
		dbResult = (dbValue- dbMin) / dbValue;

	return dbResult;
}

double CImageProcessor::GetHue(double dbBlue, double dbGreen, double dbRed, double dValue, double dMin)
{
	double dHue;

	if(dValue == dMin)
		dHue = 0;
	else if(dValue == dbBlue)
		dHue = 60 * ((dbRed - dbGreen)/ (dValue - dMin) + 4);
	else if(dValue == dbGreen)
		dHue = 60 * ((dbBlue - dbRed)/ (dValue - dMin) + 2);
	else
		dHue = 60 * ((dbGreen - dbBlue)/ (dValue - dMin));

	if(dHue < 0)
		dHue += 360;

	return dHue;
}

cv::Point2f CImageProcessor::CalcLiningPoint(int nRadian, double dbHue, double dbSaturation)
{
	double dRadian = ((dbHue+270) * PHI) / 180;

	Point2f hsvpoint;			

	hsvpoint.x = (float) (dbSaturation*nRadian * cos(dRadian) + nRadian);
	hsvpoint.y = (float) (dbSaturation*nRadian * sin(dRadian) + nRadian);

	return hsvpoint;
}

int CImageProcessor::CalcTransValue(int nPixel, int nRefValue)
{
	int nResult = nPixel - nRefValue;

	if(nResult > 255)
		nResult = 255;
	if(nResult < 0)
		nResult = 0;

	return nResult;
}

void CImageProcessor::CpuMoveImage(Mat* gMat, int nX, int nY)
{
	Mat gMatCut, gMatPaste;
	int nCutTop,nCutHeight,nCutLeft,nCutWidth;
	int nPasteTop=0,nPasteBottom=0,nPasteLeft=0,nPasteRight=0;

	if ( nX > 0 )
	{
		nCutLeft = 0;
		nCutWidth = gMat->cols - nX;

		nPasteLeft = nX;
	}
	else
	{
		nCutLeft = -nX;
		nCutWidth = gMat->cols + nX;

		nPasteRight = -nX;
	}

	if ( nY > 0 )
	{
		nCutTop = 0;
		nCutHeight = gMat->rows - nY;

		nPasteTop = nY;
	}
	else
	{
		nCutTop = -nY;
		nCutHeight = gMat->rows + nY;

		nPasteBottom = -nY;
	}

	gMatCut = (*gMat)(cv::Rect(nCutLeft, nCutTop,nCutWidth, nCutHeight));
	copyMakeBorder(gMatCut, gMatPaste, nPasteTop,nPasteBottom,nPasteLeft,nPasteRight, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}

void CImageProcessor::CpuRotateImage(Mat Iimage, double nCenterX, double nCenterY, double dScale, double dAngle, BOOL bReverse /*= FALSE*/, double dbSize /*= 1.0*/)
{
	Mat Iimage2;// = cvCreateImage(cvGetSize(Iimage), IPL_DEPTH_8U, 3);

	if(dScale == 0)
	{
		dScale = 1;
		dAngle += -90;
	}

	if( dAngle == 0.0 && dScale == 0.0)
		return ;
	//cvShowImage("2",Iimage);
	CvMat *rot_mat = cvCreateMat( 2, 3, CV_32FC1);
	CvPoint2D32f rot_center = cvPoint2D32f( nCenterX * dbSize, nCenterY * dbSize);
	if( dScale == 0.0 && dAngle == 0.0 )
		return;

	double dbAngleAdjust;// = -1 * (dAngle + 90);
	if(bReverse) //Reverse Movie
		dbAngleAdjust = -1 * (180);   
	else
		dbAngleAdjust = -1 * (dAngle + 90);   //Normal Movie

	//CString strTemp;
	//strTemp.Format(_T("%f"), dbAngleAdjust);
	//m_AdjustDsc2ndList.SetItemText(m_nSelectedNum, 17, strTemp);

	cv2DRotationMatrix( rot_center, dbAngleAdjust, dScale, rot_mat);

	//cuda::warpAffine(*gMat,d_rotate, rot_mat, Size(2500, 1500), CV_INTER_CUBIC+CV_WARP_FILL_OUTLIERS);
	//Mat d_rotate;

	//-- 2014-07-16 hongsu@esmlab.com
	//-- ? CvPoint2D32f srcTri[3], dstTri[3];
	cvWarpAffine(&IplImage(Iimage), &IplImage(Iimage), rot_mat,CV_INTER_LINEAR+CV_WARP_FILL_OUTLIERS);	//d_rotate.copyTo(*gMat);
	//imshow("Iimage",Iimage);
	//cvWaitKey(0);
	//gMat =&(cvarrToMat(Iimage2));//
	//Iimage = cvCreateImage(cvGetSize(&Iimage), IPL_DEPTH_8U, 3);
	cvReleaseMat(&rot_mat);
}

void CImageProcessor::CpuMakeMargin(Mat* gMat, int nX, int nY)
{
	Mat gMatCut, gMatPaste;
	int nHeight,nWidth;

	nWidth = gMat->cols - nX*2;
	nHeight = gMat->rows - nY*2;


	gMatCut = (*gMat)(cv::Rect(nX, nY,nWidth, nHeight));
	copyMakeBorder(gMatCut, gMatPaste, nY,nY,nX,nX, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}

void CImageProcessor::GpuMoveImage(cuda::GpuMat* gMat, int nX, int nY)
{
	cuda::GpuMat gMatCut, gMatPaste;
	int nCutTop,nCutHeight,nCutLeft,nCutWidth;
	int nPasteTop=0,nPasteBottom=0,nPasteLeft=0,nPasteRight=0;

	if ( nX > 0 )
	{
		nCutLeft = 0;
		nCutWidth = gMat->cols - nX;

		nPasteLeft = nX;
	}
	else
	{
		nCutLeft = -nX;
		nCutWidth = gMat->cols + nX;

		nPasteRight = -nX;
	}

	if ( nY > 0 )
	{
		nCutTop = 0;
		nCutHeight = gMat->rows - nY;

		nPasteTop = nY;
	}
	else
	{
		nCutTop = -nY;
		nCutHeight = gMat->rows + nY;

		nPasteBottom = -nY;
	}

	gMatCut = (*gMat)(cv::Rect(nCutLeft, nCutTop,nCutWidth, nCutHeight));
	cuda::copyMakeBorder(gMatCut, gMatPaste, nPasteTop,nPasteBottom,nPasteLeft,nPasteRight, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}

void CImageProcessor::GpuRotateImage(cuda::GpuMat* gMat, double nCenterX, double nCenterY, double dScale, double dAngle, BOOL bReverse)
{
	if(dScale == 0)
	{
		dScale = 1;
		dAngle += -90;
	}

	if( dAngle == 0.0 && dScale == 0.0)
		return ;

	Mat rot_mat(cv::Size(2, 3), CV_32FC1);
	cv::Point rot_center( (int) nCenterX, (int) nCenterY);
	if( dScale == 0.0 && dAngle == 0.0 )
		return;

	double dbAngleAdjust;
	if(bReverse) //Reverse Movie
		dbAngleAdjust = -1 * (180);   
	else
		dbAngleAdjust = -1 * (dAngle + 90);   //Normal Movie

	rot_mat = getRotationMatrix2D(rot_center, dbAngleAdjust, dScale);

	//cuda::warpAffine(*gMat,d_rotate, rot_mat, Size(2500, 1500), CV_INTER_CUBIC+CV_WARP_FILL_OUTLIERS);
	cuda::GpuMat d_rotate;

	//-- 2014-07-16 hongsu@esmlab.com
	//-- ? CvPoint2D32f srcTri[3], dstTri[3];
	cuda::warpAffine(*gMat, d_rotate, rot_mat, Size(gMat->cols, gMat->rows), CV_INTER_CUBIC);
	d_rotate.copyTo(*gMat);
}

void CImageProcessor::GpuMakeMargin(cuda::GpuMat* gMat, int nX, int nY)
{
	cuda::GpuMat gMatCut, gMatPaste;
	int nHeight,nWidth;

	nWidth = gMat->cols - nX*2;
	nHeight = gMat->rows - nY*2;

	gMatCut = (*gMat)(cv::Rect(nX, nY,nWidth, nHeight));
	cuda::copyMakeBorder(gMatCut, gMatPaste, nY,nY,nX,nX, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}

void CImageProcessor::GpuZoomImage(cuda::GpuMat* gMat, int nRotateX, int nRotateY, int nX, int nY, int nRatio, int nOutputWidth, int nOutputHeight)
{
	double dScale = (double)nRatio / 100.0;
	cuda::GpuMat gMatCut, gMatResize;

	int nLeft = (int) (nX- gMat->cols/dScale/2);
	int nTop = (int) (nY - gMat->rows/dScale/2);
	int nWidth = (int) (gMat->cols/dScale - 1);
	int nHeight = (int) (gMat->rows/dScale - 1);

	cuda::GpuMat pMatCut =  (*gMat)(Rect(nLeft, nTop, nWidth, nHeight));

	cuda::resize(pMatCut, gMatResize, Size(nOutputWidth ,nOutputHeight ), 0.0, 0.0 ,INTER_CUBIC );
	gMatResize.copyTo(*gMat);
}

void CImageProcessor::GpuInsertLogo(cuda::GpuMat* gMat, int nDscNum, int nWidth, int nHeight)
{
	CString strBannerPath;
	strBannerPath.Format(_T("C:\\Program Files\\ESMLab\\4DMaker\\img\\05.Banner\\%d.png"), nDscNum + 1);
	char pBannerPath[MAX_PATH] = {0};
	wcstombs(pBannerPath, strBannerPath, MAX_PATH);
	if(::PathFileExists(strBannerPath) == FALSE)
		return ;


	cv::Mat tpMat;
	gMat->download(tpMat);
	IplImage* pImg;
	pImg=&IplImage(tpMat);

	IplImage* pBanner = cvLoadImage(pBannerPath, CV_LOAD_IMAGE_UNCHANGED);
	IplImage* pResult = cvCreateImage(cvSize(gMat->cols, gMat->rows), pImg->depth, pImg->nChannels);

	OverlayImage(pImg, pBanner, pResult, cvPoint(0,0));
	tpMat = cvarrToMat(pResult);
	gMat->upload(tpMat);
}

void CImageProcessor::GpuInsertLogo2(Mat* tpMat, int nDscNum, BOOL b3DLogo)
{
	CString strBannerPath;

	WORD wVersionRequested;
	WSADATA wsaData;
	char name[255];
	CString ip; // 여기에 lcoal ip가 저장됩니다.
	PHOSTENT hostinfo;
	wVersionRequested = MAKEWORD( 2, 0 );
	if ( WSAStartup( wVersionRequested, &wsaData ) == 0 ) 
	{
		if( gethostname ( name, sizeof(name)) == 0)
		{
			if((hostinfo = gethostbyname(name)) != NULL)
			{
				ip = inet_ntoa (*(struct in_addr *)*hostinfo->h_addr_list);
			}
		}      
		WSACleanup( );
	} 
	CString ipaddr;
	AfxExtractSubString( ipaddr, ip, 3, '.');
	
	
	//INI 읽어서 File Server 경로얻기
	CESMMovieIni ini;
	CString strInfoConfig;
	strInfoConfig.Format(_T("C:\\Program Files\\ESMLab\\4DMaker\\config\\4DMaker.info"));
	ini.SetIniFilename(strInfoConfig);
	CString strPath = ini.GetString(_T("Path"), _T("FileServer"));

	if(b3DLogo)
		strBannerPath.Format(_T("%s\\img\\%d.png"),strPath, nDscNum+1);
	else
		strBannerPath.Format(_T("%s\\img\\Logo.png"),strPath);

	char pBannerPath[MAX_PATH] = {0};
	wcstombs(pBannerPath, strBannerPath, MAX_PATH);
	if(::PathFileExists(strBannerPath) == FALSE)
		return ;

	IplImage* pImg;
	pImg=&IplImage(*tpMat);
	 
	IplImage* pBanner = cvLoadImage(pBannerPath, CV_LOAD_IMAGE_UNCHANGED);
	IplImage* pResult = cvCreateImage(cvSize(tpMat->cols, tpMat->rows), pImg->depth, pImg->nChannels);

	OverlayImage(pImg, pBanner, pResult, cvPoint(0,0));
	*tpMat = cvarrToMat(pResult);
}

void CImageProcessor::GpuInsertCameraID(Mat* tpMat, int nIndex, CString strDscNum, CString strDscIp, BOOL bPntColorInfo)
{
	char szCmeraID[10];
	char szCameraIp[17];
	sprintf(szCmeraID, "%3d:%S", nIndex+1, strDscNum); 
	sprintf(szCameraIp, "%S", strDscIp);

	rectangle(*tpMat, Point(100, 10), Point(1000, 250), Scalar(0, 0, 0), CV_FILLED );

	/* 이미지에 넣을 폰트 초기화 */
	CvFont pFontId;
	cvInitFont(&pFontId, CV_FONT_HERSHEY_SIMPLEX, 5, 5, 0, 3, 8);
	/* 이미지에 글자 삽입 */
	putText(*tpMat,szCmeraID,cv::Point(100,150),CV_FONT_HERSHEY_SIMPLEX,5,Scalar(255,255,255),10,8);
	putText(*tpMat,szCameraIp,cv::Point(110,220),CV_FONT_HERSHEY_SIMPLEX,2,Scalar(255,255,255),4,8);
	
	if(bPntColorInfo)
	{
		/*색감 보정 Data 출력*/
		//Average
		CalcFrameInfo(tpMat);
	}
}

void CImageProcessor::KzonePrism(Mat *background, int nDscNum, int taValue,int saValue, int nPriIndex)
{
	WORD wVersionRequested;
	WSADATA wsaData;
	char name[255];
	CString ip; // 여기에 lcoal ip가 저장됩니다.
	PHOSTENT hostinfo;
	wVersionRequested = MAKEWORD( 2, 0 );
	if ( WSAStartup( wVersionRequested, &wsaData ) == 0 ) 
	{
		if( gethostname ( name, sizeof(name)) == 0)
		{
			if((hostinfo = gethostbyname(name)) != NULL)
			{
				ip = inet_ntoa (*(struct in_addr *)*hostinfo->h_addr_list);
			}
		}      
		WSACleanup();
	} 
	CString ipaddr;
	AfxExtractSubString( ipaddr, ip, 3, '.');


	//INI 읽어서 File Server 경로얻기
	CESMMovieIni ini;
	CString strInfoConfig;
	strInfoConfig.Format(_T("C:\\Program Files\\ESMLab\\4DMaker\\config\\4DMaker.info"));
	ini.SetIniFilename(strInfoConfig);
	CString strPath = ini.GetString(_T("Path"), _T("FileServer"));


	CString strBannerPath0;

	strBannerPath0.Format(_T("%s\\img\\06.Prism\\[%d]%d_0.png"),strPath, nPriIndex, nDscNum);

	char pBannerPath0[MAX_PATH] = {0};
	wcstombs(pBannerPath0, strBannerPath0, MAX_PATH);
	if(::PathFileExists(strBannerPath0) == FALSE)
		return ;

	CString strBannerPath1;

	strBannerPath1.Format(_T("%s\\img\\06.Prism\\[%d]%d_1.png"),strPath,nPriIndex, nDscNum);

	char pBannerPath1[MAX_PATH] = {0};
	wcstombs(pBannerPath1, strBannerPath1, MAX_PATH);
	if(::PathFileExists(strBannerPath1) == FALSE)
		return ;

	Mat banner0 = imread(pBannerPath0,1);
	Mat banner1 = imread(pBannerPath1,1);

	Mat Result0(background->rows,background->cols,CV_8UC3);
	Mat Result1(background->rows,background->cols,CV_8UC3);

	AlphaBlending(*background,banner0,Result0,taValue);
	AlphaBlending(Result0,banner1,Result1,saValue);
	*background = Result1.clone();
}

void CImageProcessor::OverlayImage(const IplImage *background, const IplImage *foreground, IplImage *output, CvPoint location)
{
	cvCopy(background, output);

	// start at the row indicated by location, or at row 0 if location.y is negative.
	for(int y = std::max(location.y , 0); y < background->height; ++y)
	{
		int fY = y - location.y; // because of the translation

		// we are done of we have processed all rows of the foreground image.
		if(fY >= foreground->height)
			break;

		// start at the column indicated by location, 

		// or at column 0 if location.x is negative.
		for(int x = std::max(location.x, 0); x < background->width; ++x)
		{
			int fX = x - location.x; // because of the translation.

			// we are done with this row if the column is outside of the foreground image.
			if(fX >= foreground->width)
				break;

			// determine the opacity of the foregrond pixel, using its fourth (alpha) channel.
			double opacity = ((double)foreground->imageData[fY * foreground->widthStep + fX * foreground->nChannels + 3]) / 255.;
			if ( opacity < 0 )
				opacity = opacity + 1.;

			// and now combine the background and foreground pixel, using the opacity, 

			// but only if opacity > 0.
			for(int c = 0; opacity > 0 && c < output->nChannels; ++c)
			{
				unsigned char foregroundPx =
					foreground->imageData[fY * foreground->widthStep + fX * foreground->nChannels + c];
				unsigned char backgroundPx =
					background->imageData[y * background->widthStep + x * background->nChannels + c];
				output->imageData[y*(output->widthStep) + output->nChannels*x + c] =
					(char) (backgroundPx * (1.-opacity) + foregroundPx * opacity);

				double value = backgroundPx * (1.-opacity) + foregroundPx * opacity;
			}

		}
	}
}

void CImageProcessor::BGR2HSV(Mat img, Mat result)
{
	int nHeight = img.rows;
	int nWidth  = img.cols;

	double dMin;
	double dValue;
	double dHue;
	double dSaturation;
	int nIndex = 0;
	SampleLining(result);

	for(int i = 0 ; i < nHeight; i++)
	{
		for( int j = 0 ; j < nWidth; j++)
		{
			//int nIndex = nWidth * (i*3) + (j*3);
			double B = NormValue(img.data[nIndex++]);
			double G = NormValue(img.data[nIndex++]);
			double R = NormValue(img.data[nIndex++]);

			dMin = GetMinValue(B,G,R);
			dValue = GetMaxValue(B,G,R);
			if(dValue == 0 || B==G && G == R)
			{
				dSaturation = 0;
				dHue = 0;
			}
			else
			{
				dSaturation = GetSaturation(dValue,dMin);
				dHue = GetHue(B,G,R,dValue,dMin);
			}
			DrawColorValue(result,dHue,dSaturation,dValue,B,G,R);
		}
	}
}

void CImageProcessor::AlphaBlending(Mat background, Mat pr, Mat result, int alpha)
{
	int row = pr.rows;
	int col = pr.cols;
	int ori_col = background.cols;
	int i,j;

	for(i=0;i<row;i++)
	{
		for(j=0;j<col;j++)
		{
			double b_dst = (double) (background.data[ori_col*(i*3)+(j*3)] * (100-alpha)/100  );
			double g_dst = (double) (background.data[ori_col*(i*3)+(j*3)+1] * (100-alpha)/100);
			double r_dst = (double) (background.data[ori_col*(i*3)+(j*3)+2] * (100-alpha)/100);

			double b_src = (double) (pr.data[col*(i*3)+(j*3)] * alpha / 100 );
			if(b_src < 1.0f)
				b_src = (double)(background.data[ori_col * (i*3) + (j*3)] * alpha / 100);
			double g_src = (double) (pr.data[col*(i*3)+(j*3)+1] * alpha / 100);
			if(g_src < 1.0f)
				g_src = (double)(background.data[ori_col * (i*3) + (j*3)+1] * alpha / 100);
			double r_src = (double) (pr.data[col*(i*3)+(j*3)+2] * alpha / 100);
			if(r_src < 1.0f)
				r_src = (double)(background.data[ori_col * (i*3) + (j*3)+2] * alpha / 100);

			result.data[ori_col*(i*3)+(j*3)] = (int) b_src + (int) b_dst;
			result.data[ori_col*(i*3)+(j*3)+1] = (int) g_src + (int) g_dst;
			result.data[ori_col*(i*3)+(j*3)+2] = (int) r_src + (int) r_dst;
		}
	}
}

void CImageProcessor::CalcFrameInfo(Mat *pImage)
{
	Mat img = pImage->clone();

	int nViewerSize = 200;
	Mat HueViewer(nViewerSize,nViewerSize,CV_8UC3,Scalar(0,0,0));
	double dBlue=0,dGreen=0,dRed=0;

	CalcFrameAvg(img,dBlue,dGreen,dRed);
	BGR2HSV(img,HueViewer);

	int nWidth = img.cols;
	int nHeight = img.rows;
	int nRowStart = nHeight- nViewerSize;
	int nColStart = nWidth - nViewerSize;

	for(int i =nRowStart; i < nHeight; i++)
	{
		for(int j = nColStart; j < nWidth; j++)
		{
			img.data[nWidth*(i*3)+(j*3)]   = HueViewer.data[nViewerSize*((i-nRowStart)*3)+((j-nColStart)*3)];
			img.data[nWidth*(i*3)+(j*3)+1] = HueViewer.data[nViewerSize*((i-nRowStart)*3)+((j-nColStart)*3)+1];
			img.data[nWidth*(i*3)+(j*3)+2] = HueViewer.data[nViewerSize*((i-nRowStart)*3)+((j-nColStart)*3)+2];
		}
	}
	char strData[200];
	sprintf(strData,"B:%.2f G:%.2f R:%.2f",dBlue,dGreen,dRed);
	putText(img,strData,cv::Point(270,1070),CV_FONT_HERSHEY_TRIPLEX,2,Scalar(255,255,255),1,8);

	*pImage = img.clone();
}

void CImageProcessor::CalcFrameAvg(Mat img, double &dBlue, double &dGreen, double &dRed)
{
	int nTotal = (int) img.total();
	int nHeight = img.rows;
	int nWidth = img.cols;
	double dBlueSum = 0;
	double dGreenSum = 0;
	double dRedSum = 0;

	for(int i = 0 ; i < nHeight; i++)
	{
		for(int j = 0 ; j < nWidth; j++)
		{
			dBlueSum += img.data[nWidth*(i*3)+(j*3)];
			dGreenSum += img.data[nWidth*(i*3)+(j*3)+1];
			dRedSum += img.data[nWidth*(i*3)+(j*3)+2];
		}
	}

	dBlue = dBlueSum/nTotal;
	dGreen = dGreenSum/nTotal;
	dRed = dRedSum/nTotal;
}

void CImageProcessor::DrawColorValue(Mat result, double dbHue, double dbSaturation, double dbValue, double dbBlue, double dbGreen, double dbRed)
{
	int nRadius = result.rows/2;
	int nWidth = result.cols;
	int nHeight = result.rows;
	double dRadian = ((dbHue+270) * PHI) / 180;

	Point2f hsvpoint;			

	hsvpoint.x = (float) (dbSaturation*nRadius * cos(dRadian) + nRadius);
	hsvpoint.y = (float) (dbSaturation*nRadius * sin(dRadian) + nRadius);

	if(hsvpoint.x < 0)
		hsvpoint.x = 0;

	if(hsvpoint.y < 0)
		hsvpoint.y = 0;

	if(hsvpoint.x >= nWidth -1)
		hsvpoint.x = (float) (nWidth - 1);

	if(hsvpoint.y > nHeight -1)
		hsvpoint.y = (float) (nHeight - 1);

	result.at<Vec3b>(hsvpoint)[0] = (uchar) (dbBlue*255);
	result.at<Vec3b>(hsvpoint)[1] = (uchar) (dbGreen*255);
	result.at<Vec3b>(hsvpoint)[2] = (uchar) (dbRed*255);
}

void CImageProcessor::SampleLining(Mat img)
{
	double dBlue,dGreen,dRed;
	double dMin,dValue,dSaturation,dHue;
	cv::Point pCenter(img.cols/2,img.rows/2);
	int nRadian = img.rows/2;
	circle(img,pCenter,nRadian,Scalar(255,255,255),1,8);
	for(int i = 0 ; i < 7 ; i++)
	{
		switch(i)
		{
		case 0://Blue
			{
				dBlue = 1,dGreen = 0,dRed = 0;
				dMin = GetMinValue(dBlue,dGreen,dRed);
				dValue = GetMaxValue(dBlue,dGreen,dRed);
				dSaturation = GetSaturation(dValue,dMin);
				dHue = GetHue(dBlue,dGreen,dRed,dValue,dMin);	

				line(img,pCenter,CalcLiningPoint(nRadian,dHue,dSaturation),Scalar(255,0,0),1,8);
			}
			break;
		case 1://Green
			{
				dBlue = 0,dGreen = 1,dRed = 0;
				dMin = GetMinValue(dBlue,dGreen,dRed);
				dValue = GetMaxValue(dBlue,dGreen,dRed);
				dSaturation = GetSaturation(dValue,dMin);
				dHue = GetHue(dBlue,dGreen,dRed,dValue,dMin);	

				line(img,pCenter,CalcLiningPoint(nRadian,dHue,dSaturation),Scalar(0,255,0),1,8);
			}
			break;
		case 2://Red
			{
				dBlue = 0,dGreen = 0,dRed = 1;
				dMin = GetMinValue(dBlue,dGreen,dRed);
				dValue = GetMaxValue(dBlue,dGreen,dRed);
				dSaturation = GetSaturation(dValue,dMin);
				dHue = GetHue(dBlue,dGreen,dRed,dValue,dMin);	

				line(img,pCenter,CalcLiningPoint(nRadian,dHue,dSaturation),Scalar(0,0,255),1,8);
			}
			break;
		case 3://Yellow
			{
				dBlue = 0,dGreen = 1,dRed = 1;
				dMin = GetMinValue(dBlue,dGreen,dRed);
				dValue = GetMaxValue(dBlue,dGreen,dRed);
				dSaturation = GetSaturation(dValue,dMin);
				dHue = GetHue(dBlue,dGreen,dRed,dValue,dMin);	

				line(img,pCenter,CalcLiningPoint(nRadian,dHue,dSaturation),Scalar(0,255,255),1,8);
			}
			break;
		case 4://Cyan
			{
				dBlue = 1,dGreen = 1,dRed = 0;
				dMin = GetMinValue(dBlue,dGreen,dRed);
				dValue = GetMaxValue(dBlue,dGreen,dRed);
				dSaturation = GetSaturation(dValue,dMin);
				dHue = GetHue(dBlue,dGreen,dRed,dValue,dMin);	

				line(img,pCenter,CalcLiningPoint(nRadian,dHue,dSaturation),Scalar(255,255,0),1,8);
			}
			break;
		case 5://Magenta
			{
				dBlue = 1,dGreen = 0,dRed = 1;
				dMin = GetMinValue(dBlue,dGreen,dRed);
				dValue = GetMaxValue(dBlue,dGreen,dRed);
				dSaturation = GetSaturation(dValue,dMin);
				dHue = GetHue(dBlue,dGreen,dRed,dValue,dMin);	

				line(img,pCenter,CalcLiningPoint(nRadian,dHue,dSaturation),Scalar(255,0,255),1,8);
			}
			break;
		}
	}
}

void CImageProcessor::DoColorRevision(cv::Mat *frame,FrameRGBInfo stColorInfo)
{
	int nIdx = 0;
	double dB = 0,dG = 0,dR = 0;
	for(int i = 0 ; i <frame->total(); i++)
	{
		dB += frame->data[nIdx++];
		dG += frame->data[nIdx++];
		dR += frame->data[nIdx++];
	}
	//현재 프레임의 평균
	dB = dB / frame->total();
	dG = dG / frame->total();
	dR = dR / frame->total();

	int nBlue = (int) (dB - stColorInfo.dBlue);
	int nGreen = (int) (dG - stColorInfo.dGreen);
	int nRed = (int) (dR - stColorInfo.dRed);

	//모든 색이 일치한 경우..
	if(nBlue == 0 && nGreen == 0 &&nRed ==0)
		return;

	for(int i = 0 ; i < frame->rows; i++)
	{
		for(int j = 0; j < frame->cols; j++)
		{
			int nIdx = frame->cols * (i*3) + (j*3);

			frame->data[nIdx] = CalcTransValue(frame->data[nIdx],nBlue);
			frame->data[nIdx+1] = CalcTransValue(frame->data[nIdx+1],nGreen);
			frame->data[nIdx+2] = CalcTransValue(frame->data[nIdx+2],nRed);
		}
	}
}

void CImageProcessor::FindPrismPixel(int t_Peak_x,int t_Peak_y,int t_LB_x,int t_LB_y,int t_RB_x, int t_RB_y, int *t_LT_x, int *t_LT_y,int *t_RT_x, int *t_RT_y)
{
	double aslope,acen;
	aslope = ((double(t_LB_y-t_RB_y)/double(t_LB_x-t_RB_x)));
	acen = t_Peak_y - (aslope*t_Peak_x);//center점을 지나는 원점 방정식

	double cen_slope,left_cen,right_cen;
	cv::Point n_cen = cv::Point( (t_LB_x + t_RB_x)/2, (t_LB_y + t_RB_y)/2);
	cen_slope =  ((double(t_Peak_y-n_cen.y)/double(t_Peak_x-n_cen.x)));
	right_cen = t_RB_y - (cen_slope * t_RB_x);
	left_cen = t_LB_y - (cen_slope * t_LB_x);

	//Left top
	int L_Px,L_Py,n_LPx,n_LPy;
	L_Px = (int) ((left_cen - acen)/(aslope-cen_slope));
	L_Py = (int) (((left_cen - acen)/(aslope-cen_slope))*aslope + acen);
	n_LPx = (L_Px + t_LB_x)/2;
	n_LPy = (L_Py + t_LB_y)/2;

	//Right top
	int R_Px,R_Py,n_RPx,n_RPy;
	R_Px = (int) ((right_cen - acen)/(aslope-cen_slope));
	R_Py = (int) (((right_cen - acen)/(aslope-cen_slope))*aslope + acen);
	n_RPx = (R_Px + t_RB_x)/2;
	n_RPy = (R_Py + t_RB_y)/2;

	*t_LT_x = n_LPx;
	*t_LT_y = n_LPy;

	*t_RT_x = n_RPx;
	*t_RT_y = n_RPy;

	if(t_Peak_x == 0 && t_Peak_y == 0 && t_RB_x == 0 && t_RB_y == 0 && t_LB_x == 0 && t_LB_y == 0)
	{
		*t_RT_x = 0;
		*t_RT_y = 0;
		*t_LT_x = 0;
		*t_LT_y = 0;
	}
}

void CImageProcessor::affinetransform(Mat img,cv::Point a,cv::Point b, cv::Point c, Point2f srcTri[3],Mat dst)
{
	Mat warp_mat(2,3,CV_64FC1);
	Point2f dstTri[3];
	dstTri[0] = a;	dstTri[1] = b;	dstTri[2] = c;

	warp_mat = getAffineTransform(srcTri,dstTri);
	warpAffine(img,dst,warp_mat,dst.size());
}

void CImageProcessor::AdjustImage(Mat& src, Mat& dst, Size2f szDst, stAdjustInfo &adjustData, bool bMakeBorder, bool bReverse)
{
	if (szDst.width == 0 || szDst.height == 0)
		szDst = src.size();

	int nSrcWidth = src.cols, nSrcHeight = src.rows;

	Mat m(2, 3, CV_32FC1);

	Rect2f rtMargin(adjustData.rtMargin.left, adjustData.rtMargin.top, adjustData.rtMargin.right - adjustData.rtMargin.left, adjustData.rtMargin.bottom - adjustData.rtMargin.top);

	//AdjustImage(src, src, Size(nOutputWidth, nOutputHeight), rtMargin, AdjustData.AdjAngle - 90.f, AdjustData.AdjSize, AdjustData.AdjMove.x, AdjustData.AdjMove.y);

	int nMoveX = 0, nMoveY = 0;
	nMoveX = imgproc::Round(adjustData.AdjMove.x);
	nMoveY = imgproc::Round(adjustData.AdjMove.y);

	float fAngle = adjustData.AdjAngle + 90.f;
	int nRotateX = 0, nRotateY = 0;
	nRotateX = imgproc::Round(adjustData.AdjptRotate.x - src.cols / 2.f );
	nRotateY = imgproc::Round(adjustData.AdjptRotate.y - src.rows / 2.f );

	float fScale = adjustData.AdjSize;

	//Cpu Margin
	int nMarginX = (int) adjustData.stMargin.nMarginX;
	int nMarginY = (int) adjustData.stMargin.nMarginY;

	float fRatioW = 1.f, fRatioH = 1.f;
	if(rtMargin.width != 0 && rtMargin.height != 0)
	{
		fRatioW = (float)src.cols / rtMargin.width;
		fRatioH = (float)src.rows / rtMargin.height;
	}
	else
	{
		fRatioW = (float)src.cols / (float)(src.cols - nMarginX * 2.f);
		fRatioH = (float)src.rows / (float)(src.rows - nMarginY * 2.f);
	}

	float fWarpW, fWarpH;
	fWarpW = fScale * szDst.width * fRatioW;
	fWarpH = fScale * szDst.height * fRatioH;

	float fScaleX, fScaleY;
	fScaleX = fWarpW / src.cols;
	fScaleY = fWarpH / src.rows;

	
	if(bMakeBorder && adjustData.dBorderScale > 1.)
	{
		double dMarginCenterX = (adjustData.rtMargin.left + adjustData.rtMargin.right) / 2.;
		double dMarginCenterY = (adjustData.rtMargin.top + adjustData.rtMargin.bottom) / 2.;

		fScaleX /= adjustData.dBorderScale * fRatioW;
		fScaleY /= adjustData.dBorderScale * fRatioH;
	}

	float fOffsetX, fOffsetY;
	if(rtMargin.width != 0 && rtMargin.height != 0)
	{
		fOffsetX = (((rtMargin.x + rtMargin.width / 2.f) - src.cols / 2.f) - nMoveX) * fScaleX;
		fOffsetY = (((rtMargin.y + rtMargin.height / 2.f) - src.rows / 2.f) - nMoveY) * fScaleY;
	}
	else
	{
		fOffsetX = (-nMarginX) * fScaleX;
		fOffsetY = (-nMarginY) * fScaleY;
	}

	Point2f center;

	center = Point2f(fWarpW / 2.f, fWarpH / 2.f);

	if (center.x < src.cols / 2.f)
		center.x = src.cols / 2.f;
	if (center.y < src.rows / 2.f)
		center.y = src.rows / 2.f;


	float* pWarpMat = m.ptr<float>(0);

	float fRad = fAngle * CV_PI / 180.f;
	float cosX, cosY, sinX, sinY;
	cosX = fScaleX * cos(fRad);
	cosY = fScaleY * cos(fRad);
	sinX = fScaleX * sin(fRad);
	sinY = fScaleY * sin(fRad);

	double val00, val10, val20, val01, val11, val21;
	
	val00 = cosX;
	val10 = -sinY;
	val20 = -(cosX * (center.x + nRotateX) - sinY * (center.y + nRotateY) - center.x - nRotateX * fScaleX + fOffsetX);
	val01 = sinX;
	val11 = cosY;
	val21 = -(sinX * (center.x + nRotateY) + cosY * (center.y + nRotateY) - center.y - nRotateY * fScaleY + fOffsetY);

	if(bReverse)
	{
		pWarpMat[0] = -val00;
		pWarpMat[1] = -val10;
		pWarpMat[3] = -val01;
		pWarpMat[4] = -val11;

		if (fWarpW > nSrcWidth || fWarpH > nSrcHeight)
		{
			pWarpMat[2] = fWarpW - val20;
			pWarpMat[5] = fWarpH - val21;
		}
		else
		{
			pWarpMat[2] = nSrcWidth - val20;
			pWarpMat[5] = nSrcHeight - val21;
		}
	}
	else
	{
		pWarpMat[0] = val00;
		pWarpMat[1] = val10;
		pWarpMat[2] = val20;
		pWarpMat[3] = val01;
		pWarpMat[4] = val11;
		pWarpMat[5] = val21;
		//pWarpMat[6] = 0.f;
		//pWarpMat[7] = 0.f;
		//pWarpMat[8] = 1.f;
	}

	Mat res;

	if (fWarpW > src.cols || fWarpH > src.rows)
	{
		Mat border = Mat::zeros(fWarpH, fWarpW, src.type());
		src.copyTo(border(Rect((fWarpW - src.cols) / 2, (fWarpH - src.rows) / 2, src.cols, src.rows)));
		res = Mat::zeros(fWarpH, fWarpW, src.type());
		warpAffine(border, res, m, Size(fWarpW, fWarpH), INTER_LINEAR, BORDER_CONSTANT);
	}
	else
	{
		res = Mat::zeros(src.rows, src.cols, src.type());
		warpAffine(src, res, m, src.size(), INTER_LINEAR, BORDER_CONSTANT);
	}
	
	dst = res(Rect((res.cols - szDst.width) / 2.f, (res.rows - szDst.height) / 2.f, szDst.width, szDst.height));
}

void CImageProcessor::AdjustImageUsingEffectData(Mat& src, Mat& dst, Size2f szDst, stAdjustInfo &adjustData, EffectInfo &effectData, bool bMakeBorder, bool bReverse)
{
	if (szDst.width == 0 || szDst.height == 0)
		szDst = src.size();

	int nSrcWidth = src.cols, nSrcHeight = src.rows;

	Mat m(2, 3, CV_32FC1);

	Rect2f rtMargin(adjustData.rtMargin.left, adjustData.rtMargin.top, adjustData.rtMargin.right - adjustData.rtMargin.left, adjustData.rtMargin.bottom - adjustData.rtMargin.top);
	
	int nMoveX = 0, nMoveY = 0;
	nMoveX = imgproc::Round(adjustData.AdjMove.x);
	nMoveY = imgproc::Round(adjustData.AdjMove.y);

	float fAngle = adjustData.AdjAngle + 90.f;

	int nRotateX = 0, nRotateY = 0;
	nRotateX = imgproc::Round(adjustData.AdjptRotate.x - src.cols / 2.f );
	nRotateY = imgproc::Round(adjustData.AdjptRotate.y - src.rows / 2.f );

	float fScale = adjustData.AdjSize;

	double dEffectScale = (double)effectData.nZoomRatio / 100.0;

	if(dEffectScale == 0)
	{
		dEffectScale = 1;
		SendLog(5,_T("Template Error"));
	}

	fScale *= dEffectScale;

	//Cpu Margin
	int nMarginX = (int) adjustData.stMargin.nMarginX;
	int nMarginY = (int) adjustData.stMargin.nMarginY;

	float fRatioW = 1.f, fRatioH = 1.f;
	if(rtMargin.width != 0 && rtMargin.height != 0)
	{
		fRatioW = (float)nSrcWidth / rtMargin.width;
		fRatioH = (float)nSrcHeight / rtMargin.height;
	}
	else
	{
		fRatioW = (float)nSrcWidth / (float)(nSrcWidth - nMarginX * 2.f);
		fRatioH = (float)nSrcHeight / (float)(nSrcHeight - nMarginY * 2.f);
	}

	float fWarpW, fWarpH;
	fWarpW = fScale * szDst.width * fRatioW;
	fWarpH = fScale * szDst.height * fRatioH;

	float fScaleX, fScaleY;
	fScaleX = fWarpW / nSrcWidth;
	fScaleY = fWarpH / nSrcHeight;


	if(bMakeBorder && adjustData.dBorderScale > 1.)
	{
		double dMarginCenterX = (adjustData.rtMargin.left + adjustData.rtMargin.right) / 2.;
		double dMarginCenterY = (adjustData.rtMargin.top + adjustData.rtMargin.bottom) / 2.;

		fScaleX /= adjustData.dBorderScale * fRatioW;
		fScaleY /= adjustData.dBorderScale * fRatioH;
	}

	float fOffsetX, fOffsetY;
	if(rtMargin.width != 0 && rtMargin.height != 0)
	{
		fOffsetX = (((rtMargin.x + rtMargin.width / 2.f) - nSrcWidth / 2.f) - nMoveX) * fScaleX;
		fOffsetY = (((rtMargin.y + rtMargin.height / 2.f) - nSrcHeight / 2.f) - nMoveY) * fScaleY;
	}
	else
	{
		fOffsetX = (-nMarginX) * fScaleX;
		fOffsetY = (-nMarginY) * fScaleY;
	}

	Point2f center;

	center = Point2f(fWarpW / 2.f, fWarpH / 2.f);

	if (center.x < nSrcWidth / 2.f)
		center.x = nSrcWidth / 2.f;
	if (center.y < nSrcHeight / 2.f)
		center.y = nSrcHeight / 2.f;


	float* pWarpMat = m.ptr<float>(0);

	float fRad = fAngle * CV_PI / 180.f;
	float cosX, cosY, sinX, sinY;
	cosX = fScaleX * cos(fRad);
	cosY = fScaleY * cos(fRad);
	sinX = fScaleX * sin(fRad);
	sinY = fScaleY * sin(fRad);

	//////////////////////////////////////////////////////////////////////////
	// 200701 slee Effect 관련 Zoom/Offset 연산 추가

	CTemplateCalculator tempObj;

	// Boundary over check

	Point2f ptLT, ptRT, ptLB, ptRB;

	int nPosX, nPosY;
	nPosX = effectData.nPosX - nSrcWidth/2;
	nPosY = effectData.nPosY - nSrcHeight/2;

	ptLT = Point2f(nPosX - nSrcWidth/dEffectScale/2, nPosY - nSrcHeight/dEffectScale/2);
	ptRT = Point2f(nPosX + nSrcWidth/dEffectScale/2, nPosY - nSrcHeight/dEffectScale/2);
	ptLB = Point2f(nPosX - nSrcWidth/dEffectScale/2, nPosY + nSrcHeight/dEffectScale/2);
	ptRB = Point2f(nPosX + nSrcWidth/dEffectScale/2, nPosY + nSrcHeight/dEffectScale/2);

	if(!tempObj.isInside(ptLT, imgproc::m_vInterAdjPts))
	{
		Point2f ptNear = tempObj.GetNearPointPolygon(ptLT, imgproc::m_vInterAdjPts);

		fOffsetX += ptNear.x - ptLT.x;
		fOffsetY += ptNear.y - ptLT.y;
	}
	else if(!tempObj.isInside(ptRT, imgproc::m_vInterAdjPts))
	{
		Point2f ptNear = tempObj.GetNearPointPolygon(ptRT, imgproc::m_vInterAdjPts);

		fOffsetX += ptNear.x - ptRT.x;
		fOffsetY += ptNear.y - ptRT.y;
	}
	else if(!tempObj.isInside(ptLB, imgproc::m_vInterAdjPts))
	{
		Point2f ptNear = tempObj.GetNearPointPolygon(ptLB, imgproc::m_vInterAdjPts);

		fOffsetX += ptNear.x - ptLB.x;
		fOffsetY += ptNear.y - ptLB.y;
	}
	else if(!tempObj.isInside(ptRB, imgproc::m_vInterAdjPts))
	{
		Point2f ptNear = tempObj.GetNearPointPolygon(ptRB, imgproc::m_vInterAdjPts);

		fOffsetX += ptNear.x - ptRB.x;
		fOffsetY += ptNear.y - ptRB.y;
	}

	//////////////////////////////////////////////////////////////////////////


	double val00, val10, val20, val01, val11, val21;

	val00 = cosX;
	val10 = -sinY;
	val20 = -(cosX * (center.x + nRotateX) - sinY * (center.y + nRotateY) - center.x - nRotateX * fScaleX + fOffsetX);
	val01 = sinX;
	val11 = cosY;
	val21 = -(sinX * (center.x + nRotateY) + cosY * (center.y + nRotateY) - center.y - nRotateY * fScaleY + fOffsetY);

	if(bReverse)
	{
		pWarpMat[0] = -val00;
		pWarpMat[1] = -val10;
		pWarpMat[3] = -val01;
		pWarpMat[4] = -val11;

		if (fWarpW > nSrcWidth || fWarpH > nSrcHeight)
		{
			pWarpMat[2] = fWarpW - val20;
			pWarpMat[5] = fWarpH - val21;
		}
		else
		{
			pWarpMat[2] = nSrcWidth - val20;
			pWarpMat[5] = nSrcHeight - val21;
		}
	}
	else
	{
		pWarpMat[0] = val00;
		pWarpMat[1] = val10;
		pWarpMat[2] = val20;
		pWarpMat[3] = val01;
		pWarpMat[4] = val11;
		pWarpMat[5] = val21;
		//pWarpMat[6] = 0.f;
		//pWarpMat[7] = 0.f;
		//pWarpMat[8] = 1.f;
	}

	Mat res;

	if (fWarpW > nSrcWidth || fWarpH > nSrcHeight)
	{
		Mat border = Mat::zeros(fWarpH, fWarpW, src.type());
		src.copyTo(border(Rect((fWarpW - nSrcWidth) / 2, (fWarpH - nSrcHeight) / 2, nSrcWidth, nSrcHeight)));
		res = Mat::zeros(fWarpH, fWarpW, src.type());
		warpAffine(border, res, m, Size(fWarpW, fWarpH), INTER_LINEAR, BORDER_CONSTANT);
	}
	else
	{
		res = Mat::zeros(nSrcHeight, nSrcWidth, src.type());
		warpAffine(src, res, m, src.size(), INTER_LINEAR, BORDER_CONSTANT);
	}

	dst = res(Rect((res.cols - szDst.width) / 2.f, (res.rows - szDst.height) / 2.f, szDst.width, szDst.height));
}


void CImageProcessor::AdjustImage(Mat& src, Mat& dst, Size2f szDst, Rect2f rtMargin, float fAngle, float fScale, float fShiftX, float fShiftY, bool bMakeBorder)
{
	if (szDst.width == 0 || szDst.height == 0)
		szDst = src.size();

	if (rtMargin.width == 0 || rtMargin.height == 0)
	{
		rtMargin.x = 0;
		rtMargin.y = 0;
		rtMargin.width = src.cols;
		rtMargin.height = src.rows;
	}

	Mat m(3, 3, CV_32FC1);

	float fRatioW, fRatioH;
	fRatioW = (float)src.cols / rtMargin.width;
	fRatioH = (float)src.rows / rtMargin.height;

	float fWarpW, fWarpH;
	fWarpW = fScale * szDst.width * fRatioW;
	fWarpH = fScale * szDst.height * fRatioH;

	float fScaleX, fScaleY;
	fScaleX = fWarpW / src.cols;
	fScaleY = fWarpH / src.rows;

	float fOffsetX, fOffsetY;
	fOffsetX = (((rtMargin.x + rtMargin.width / 2.f) - src.cols / 2.f) - fShiftX) * fScaleX;
	fOffsetY = (((rtMargin.y + rtMargin.height / 2.f) - src.rows / 2.f) - fShiftY) * fScaleY;

	Size2f szBorder;
	Rect rtIns;
	Point2f center;

	rtIns = GetRotatedRectInfo(Size2f(fWarpW, fWarpH), szBorder, fAngle);

	if (!bMakeBorder)
	{
		fScaleX *= fWarpW / rtIns.width;
		fScaleY *= fWarpH / rtIns.height;
	}

	center = Point2f(szBorder.width / 2.f, szBorder.height / 2.f);

	if (center.x < src.cols / 2.f)
		center.x = src.cols / 2.f;
	if (center.y < src.rows / 2.f)
		center.y = src.rows / 2.f;


	float* pWarpMat = m.ptr<float>(0);

	float fRad = fAngle * CV_PI / 180.f;
	float cosX, cosY, sinX, sinY;
	cosX = fScaleX * cos(fRad);
	cosY = fScaleY * cos(fRad);
	sinX = fScaleX * sin(fRad);
	sinY = fScaleY * sin(fRad);

	pWarpMat[0] = cosX;
	pWarpMat[1] = -sinY;
	pWarpMat[2] = -(cosX * center.x - sinY * center.y - center.x + fOffsetX);
	pWarpMat[3] = sinX;
	pWarpMat[4] = cosY;
	pWarpMat[5] = -(sinX * center.x + cosY * center.y - center.y + fOffsetY);
	pWarpMat[6] = 0.f;
	pWarpMat[7] = 0.f;
	pWarpMat[8] = 1.f;

	Mat res;

	if (szBorder.width > src.cols || szBorder.height > src.rows)
	{
		Mat border = Mat::zeros(szBorder.height, szBorder.width, src.type());
		src.copyTo(border(Rect((szBorder.width - src.cols) / 2, (szBorder.height - src.rows) / 2, src.cols, src.rows)));
		res = Mat::zeros(szBorder.height, szBorder.width, src.type());
		warpPerspective(border, res, m, Size(szBorder.width, szBorder.height), INTER_LINEAR, BORDER_CONSTANT);
	}
	else
	{
		res = Mat::zeros(src.rows, src.cols, src.type());
		warpPerspective(src, res, m, src.size(), INTER_LINEAR, BORDER_CONSTANT);
	}

	if (bMakeBorder)
	{
		dst = res(Rect((res.cols - szBorder.width) / 2.f, (res.rows - szBorder.height) / 2.f, szBorder.width, szBorder.height));
	}
	else
	{
		dst = res(Rect((res.cols - szDst.width) / 2.f, (res.rows - szDst.height) / 2.f, szDst.width, szDst.height));
	}
}

Point2f CImageProcessor::GetRotatedCoord(const Point2f ptSrc, const float deg)
{
	float r = CV_PI / 180.f;
	float rad = deg * r;
	float s = sinf(rad);
	float c = cosf(rad);
	float x = ptSrc.x * c - (ptSrc.y * s);
	float y = ptSrc.x * s + (ptSrc.y * c);

	return Point2f(x, y);
}

bool CImageProcessor::GetIntersectPoint(Point2f pt1_1, Point2f pt1_2, Point2f pt2_1, Point2f pt2_2, Point2f& ptRes)
{
	Point2f x = pt2_1 - pt1_1;
	Point2f d1 = pt1_2 - pt1_1;
	Point2f d2 = pt2_2 - pt2_1;

	float cross = d1.x * d2.y - d1.y * d2.x;
	if (abs(cross) < /*EPS*/ 1e-8)
		return false;

	double t1 = (x.x * d2.y - x.y * d2.x) / cross;
	ptRes = pt1_1 + d1 * t1;
	return true;
}

Rect2f CImageProcessor::GetRotatedRectInfo(const Size2f szSrc, Size2f& szDst, const float deg)
{
	Rect rtRes;

	Point2f lt, rt, rb, lb, _lt, _rt, _rb, _lb;
	Point2f center;
	Point2f ltInter, rtInter, rbInter, lbInter;

	lt = Point2f(-szSrc.width / 2.f, -szSrc.height / 2.f);
	rt = Point2f(szSrc.width / 2.f, -szSrc.height / 2.f);
	rb = Point2f(szSrc.width / 2.f, szSrc.height / 2.f);
	lb = Point2f(-szSrc.width / 2.f, szSrc.height / 2.f);
	center = Point2f(0.f, 0.f);

	_lt = GetRotatedCoord(lt, deg);
	_rt = GetRotatedCoord(rt, deg);
	_rb = GetRotatedCoord(rb, deg);
	_lb = GetRotatedCoord(lb, deg);

	float left, top, right, bottom;

	left = _lt.x < _lb.x ? _lt.x : _lb.x;
	top = _lt.y < _rt.y ? _lt.y : _rt.y;
	right = _rt.x > _rb.x ? _rt.x : _rb.x;
	bottom = _lb.y > _rb.y ? _lb.y : _rb.y;

	szDst.width = right - left; 
	szDst.height = bottom - top;

	if (deg > 0.f)
	{
		GetIntersectPoint(_lt, _rt, center, rt, rtInter);
		GetIntersectPoint(_rt, _rb, center, rb, rbInter);
		GetIntersectPoint(_lb, _rb, center, lb, lbInter);
		GetIntersectPoint(_lt, _lb, center, lt, ltInter);
	}
	else
	{
		GetIntersectPoint(_lt, _rt, center, lt, ltInter);
		GetIntersectPoint(_rt, _rb, center, rt, rtInter);
		GetIntersectPoint(_lb, _rb, center, rb, rbInter);
		GetIntersectPoint(_lt, _lb, center, lb, lbInter);
	}

	left = ltInter.x > lbInter.x ? ltInter.x : lbInter.x;
	top = ltInter.y > rtInter.y ? ltInter.y : rtInter.y;
	right = rtInter.x < rbInter.x ? rtInter.x : rbInter.x;
	bottom = lbInter.y < rbInter.y ? lbInter.y : rbInter.y;

	rtRes = Rect2f(szDst.width / 2.f + left, szDst.height / 2.f + top, right - left, bottom - top);

	return rtRes;
}

void CImageProcessor::GetRotatedOuterBoxSize(Size2f szSrc, double& dScale, float deg, float fOffsetX, float fOffsetY)
{
	Rect rtRes;

	Point2f lt, rt, rb, lb, _lt, _rt, _rb, _lb;
	Point2f center;
	Point2f ltInter, rtInter, rbInter, lbInter;

	lt = Point2f(-szSrc.width / 2.f + fOffsetX, -szSrc.height / 2.f + fOffsetY);
	rt = Point2f(szSrc.width / 2.f + fOffsetX, -szSrc.height / 2.f + fOffsetY);
	rb = Point2f(szSrc.width / 2.f + fOffsetX, szSrc.height / 2.f + fOffsetY);
	lb = Point2f(-szSrc.width / 2.f + fOffsetX, szSrc.height / 2.f + fOffsetY);
	center = Point2f(0.f, 0.f);

	_lt = GetRotatedCoord(lt, deg);
	_rt = GetRotatedCoord(rt, deg);
	_rb = GetRotatedCoord(rb, deg);
	_lb = GetRotatedCoord(lb, deg);

	float left, top, right, bottom;

	left = _lt.x < _lb.x ? _lt.x : _lb.x;
	top = _lt.y < _rt.y ? _lt.y : _rt.y;
	right = _rt.x > _rb.x ? _rt.x : _rb.x;
	bottom = _lb.y > _rb.y ? _lb.y : _rb.y;

	double w = abs(left) > abs(right) ? abs(left) : abs(right);
	double h = abs(top) > abs(bottom) ? abs(top) : abs(bottom);

	double dScaleX = (double) w * 2. / szSrc.width;
	double dScaleY = (double) h * 2. / szSrc.height;

	dScale = dScaleX > dScaleY ? dScaleX : dScaleY;
}
