////////////////////////////////////////////////////////////////////////////////
//
//	ESMMovieThreadManager.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-05-28
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "ESMMovieThread.h"
#include "ESMMovieThreadImage.h"
#include "ESMMovieMetadataMgr.h"
#include <vector>
#include "ESMDefine.h"
#include "FFmpegManager.h"
using namespace std;

class CESMMovieThreadManager: public CESMMovieThread
{
	DECLARE_DYNCREATE(CESMMovieThreadManager)
public:
	int m_nMovieSize;

public:
	CESMMovieThreadManager(void) {m_nCore = 0;};
	CESMMovieThreadManager(CWinThread* pParent, short nMessage, ESMMovieMsg* pMsg);	
	virtual ~CESMMovieThreadManager();

public:	
	static unsigned WINAPI ImageAdjustThread(LPVOID param);
	static unsigned WINAPI ImageAdjustThread2(LPVOID param);
	static unsigned WINAPI ImageAdjustThread3(LPVOID param);//180404 hjcho

	static unsigned WINAPI ImageDecodingThread(LPVOID param);
	static unsigned WINAPI ImageGpuDecodingThread(LPVOID param);
	static unsigned WINAPI ImageYUVtoRGBThread(LPVOID param);
	virtual int Run(void);
	void RequestData(vector<MakeFrameInfo>* pArrFrameInfo);
	void RequestData(CString strPath,int nIdx = 0,BOOL bAJAMaking = FALSE);
	void MakeMovie(vector<TimeLineInfo>* arrMovieObject);
	void MakeFrame(vector<MakeFrameInfo>* pArrFrameInfo, int nNetMode, int nMovieNum, TCHAR* strPath, BOOL bPlay = FALSE, BOOL bReverse = FALSE, void* pDest = NULL, BOOL bCopy = FALSE, BOOL bLocal = FALSE);	
	void MakeFrame(vector<MakeFrameInfo>* pArrFrameInfo, ESMAdjustInfo* pInfo, BOOL bReverse);
	void DecodingFrame(vector<MakeFrameInfo>* pArrFrameInfo, BOOL bRotate = FALSE, BOOL bPlay = FALSE, BOOL bCopy = FALSE, BOOL bLocal = FALSE);
	void DecodingAdjustFrame(vector<MakeFrameInfo>* pArrFrameInfo, BOOL bRotate = FALSE, BOOL bPlay = FALSE);
	void DoEncodingFrame(vector<MakeFrameInfo>* pArrFrameInfo, int nArrayStartIndex, int nStartIndex, int nEndIndex, int nFrameRate = 1);
	void DoGpuDecodingFrame(vector<MakeFrameInfo>* pArrFrameInfo, int nStartIndex, int nEndIndex, int nArrayIndex, BOOL bGPUMakeFile = FALSE);
	void ImageProcessFrame(vector<MakeFrameInfo>* pArrFrameInfo, BOOL bSaveImg = FALSE, BOOL bReverse = FALSE, void* pDest = NULL);
	void EncodingMovie(vector<MakeFrameInfo>* pArrFrameInfo, int nNetMode, int nMovieNum, CString strServerPath, BOOL bPlay = FALSE);
	void EncodingAdjustMovie(vector<MakeFrameInfo>* pArrFrameInfo,ESMAdjustInfo* pInfo);
	void DoYUVtoRGB(vector<MakeFrameInfo>* pArrFrameInfo, int nIndex);
	int ImageAdjust(LPVOID param);
	
	 //161219 hjcho: If bCheckMethod true, for KT Project. Else for Adjust Movie
	void MakeGPUFrame(vector<MakeFrameInfo>* pArrFrameInfo,BOOL bRotate = FALSE,BOOL bCheckMethod = TRUE, BOOL bLocal = FALSE); 	
	int GetVideoSize(CString strpath,int* nSrcWidth,int* nSrcHeight);

	//CMiLRe 20160202 m:Movie 폴더 없을 경우 죽는 버그 수정
	BOOL IsFileFolder(CString sPath);
	void CreateFolder(CString strDestDir);

	void TimeCheck(CString str);
	HANDLE hFile;

	//170206 hjcho
	void MakeSequentialFrame(vector<MakeFrameInfo>* pArrFrameInfo,BOOL bRotate = FALSE, BOOL bCopy = FALSE, BOOL bLocal = FALSE);
	void SubProcessing(MakeFrameInfo* pFrame,Mat srcImage,BOOL bRotate);
	int    SelectSelMode(CESMMovieThreadManager *pMovieThreadManager,int nSrcHeight,int nSrcWidth);

	//170317 hjcho: For 4DLive
	int MakeGPUSequentialFrame(MuxDataInfo *pMuxDataInfo);
	static unsigned WINAPI SequentialImageThread(LPVOID param);
	void EncodingMuxMovie();
	int m_nFinish;

	//170622 hjcho : For 4D Live & Color Trans
	void ColorTransUsingCC(Mat *img,double *dbArrColor);
	int TransColor(int nData,double dbWeight);

	//170724 wgkim Using Metadata
	int m_nRepeatCount;
	int m_nPrevRepeatCount;
	CString m_strFrameMetadata;

	void SetFrameCountInMetadata();

	//170830 hjcho cuda Adjust
	//void DoMuxEncodeThread(vector<Mat>* ImageArray,cv::Size szOutput,CString strSavePath,CString strFileName,FFmpegManager* pffmpeg,BOOL bDirect = FALSE);
	//static unsigned WINAPI MuxEncoding(LPVOID param);

	//180320 hjcho
	//void DoMuxEncodeThreadBoth(vector<Mat>* ImageArray,cv::Size szOutput,CString strMP4SavePath,CString strCamID,CString strTSSavePath,CString strFileName,FFmpegManager* pffmpeg,BOOL bPSCP = FALSE);
	//static unsigned WINAPI MuxEncodingBoth(LPVOID param);
	CString m_strHomePath;

	int m_nMuxErr;
	int m_nMuxProcessing;
	cuda::GpuMat m_GpuOriImage;
	cuda::GpuMat m_GpuRotImage;
	cuda::GpuMat m_GpuReImage;

	BOOL m_bRotate;
	int m_nCore;

	//180404 hjcho
	CString GetDSCIDFromPath(CString strPath);

	//180413 hjcho
	cv::Mat DoVMCCMovie(MakeFrameInfo* pFrame,EffectInfo stEffect,double dbZoomRatio,int nOutputWidth,int nOutputHeight);

	//180416 hjcho
	BOOL m_bBoth;
	//void DoMuxEncodeThreadWindow(vector<Mat>* ImageArray,cv::Size szOutput,CString strSavePath,CString strFileName,FFmpegManager* pffmpeg,int nBitrate,int nSelectedContainer,BOOL bRefereeMode /*= FALSE*/,BOOL bLowBitrate /*= FALSE*/);
	
	void DoMuxEncodeThreadWindow(
		vector<Mat>* ImageArray,
		cv::Size szOutput,
		CString strSavePath,
		CString strFileName,
		CString strRamDiskPath,	//wgkim 200206
		FFmpegManager* pffmpeg,
		int nBitrate,
		int nSelectedContainer,
		BOOL bRefereeMode = FALSE,
		BOOL bLowBitrate = FALSE);
	
	static unsigned WINAPI MuxEncodingWindow(LPVOID param);
	void DoMuxEncodeThreadAndroid(vector<Mat>* ImageArray,cv::Size szOutput,CString strSavePath,CString strFileName,FFmpegManager* pffmpeg,BOOL bPSCP = FALSE);
	static unsigned WINAPI MuxEncodingAndroid(LPVOID param);

	//180629 wgkim
	double GetEncodingFrameRate(int nFrameRateIndex);

	//180725 hjcho
	void RequestMuxData(CString strPath);

	//181221 hjcho
	void RunMultiViewMaking(MakeMultiView* pView);

	//190827 jhhan
	void TCPSendData(CString strPath);
};

struct MuxEncodingData
{
	MuxEncodingData()
	{
		m_ArrImage = NULL;
		pParent    = NULL;
		pffmpeg    = NULL;
		bCommunication	= FALSE;
		bRefereeMode	= FALSE;
	}
	vector<Mat>*m_ArrImage;
	CESMMovieThreadManager* pParent;
	CString strSavePath;
	CString strFileName;
	cv::Size szOutput;
	FFmpegManager* pffmpeg;
	BOOL bCommunication;
	BOOL bRefereeMode;
	BOOL bLowBitrate;

	BOOL nSelectedContainer; //wgkim 190214
	int nBitrate; //wgkim 190214

	//wgkim 200206
	CString strRamDiskPath;
};
struct MuxEncodingBothData
{
	MuxEncodingBothData()
	{
		m_ArrImage = NULL;
		pParent    = NULL;
		pffmpeg	   = NULL;
		bPSCP	   = FALSE;
	}
	vector<Mat>*m_ArrImage;
	CESMMovieThreadManager* pParent;
	FFmpegManager* pffmpeg;
	cv::Size szOutput;
	CString strMP4Path;
	CString strCamID;
	CString strTSSavePath;
	CString strFileName;
	BOOL	bPSCP;
};
struct ThreadData
{
	ThreadData()
	{
		nFrameNum = 0;
		nDscIndex = 0;
		pMovieId = NULL;
		pMovieName = NULL;
		pMovieThreadManager = NULL;
		pEffectData = NULL;
	}
	int nFrameNum;
	int nDscIndex;
	TCHAR* pMovieId;
	TCHAR* pMovieName;
	CESMMovieThreadManager* pMovieThreadManager;
	EffectInfo* pEffectData;
};

struct ThreadFrameData
{
	ThreadFrameData()
	{
		pFrameInfo = NULL;
		pMovieThreadManager = NULL;
		pEffectData = NULL;
	}
	MakeFrameInfo* pFrameInfo;
	CESMMovieThreadManager *pMovieThreadManager;
	EffectInfo* pEffectData;
	BOOL bSaveImg;
	BOOL bReverse;
	CString strPath;
};

struct ThreadDecodingData
{
 	ThreadDecodingData()
 	{
 		pMovieThreadManager = NULL;
 		pArrFrameInfo = NULL;
 		nArrayStartIndex = 0;
 		nStartIndex = 0;
 		nEndIndex = 0;
		bGPUMakeFile = FALSE;
		bRotate = FALSE;
		bCopy = FALSE;
		bLocal = FALSE;
		nFrameRate = 1; // 1 = 30P, 0 = 25P 
 	}
	CESMMovieThreadManager* pMovieThreadManager;
 	vector<MakeFrameInfo>* pArrFrameInfo;
 	int nArrayStartIndex;
 	int nStartIndex;
	int nEndIndex;
	BOOL bGPUMakeFile;
	BOOL bRotate;
	BOOL bCopy;
	BOOL bLocal;
	int nFrameRate;
};

struct ThreadSequentialData
{
	ThreadSequentialData()
	{
		pParent = NULL;
		//pArrFFMpeg = NULL;
	}
	CESMMovieThreadManager *pParent;
	MuxDataInfo *pMuxDataInfo;
	//FFmpegManager **pFFmpeg;
	vector<FFmpegManager*>pArrFFMpeg;
};