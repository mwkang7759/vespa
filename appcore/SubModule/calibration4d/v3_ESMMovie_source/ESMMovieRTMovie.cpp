﻿#pragma once
#include "stdafx.h"
#include "ESMMovieRTMovie.h"
#include "ESMMovieMgr.h"
#include "FFmpegManager.h"
#include "ESMFunc.h"
#include "DllFunc.h"
#include "ESMMovieFileOperation.h"
#include "ESMIni.h"
#include <wchar.h>
#include "sockutil.h"
#define CPUDECODE

// muhwan
#if LIBAVCODEC_VERSION_INT >= AV_VERSION_INT(55, 45, 101)
#define avcodec_alloc_frame		av_frame_alloc  
#endif

#include <cuda_runtime.h>
#include <ESMFFMuxer.h>
#pragma comment(lib, "ESMNvdec.lib")
#pragma comment(lib, "ESMNvenc.lib")
#pragma comment(lib, "ESMNvresizer.lib")
#pragma comment(lib, "ESMNvblender.lib")
#pragma comment(lib, "ESMNvrotator.lib")
#pragma comment(lib, "ESMFFMuxer.lib")
#pragma comment(lib, "ESMDRMHandler.lib")

#if defined(WITH_HEVC_TEST) || defined(WITH_AVC_TEST)
#include <ESMRTSPServer.h>
#pragma comment(lib, "ESMRTSPServer.lib")
#endif

CESMMovieRTMovie* CESMMovieRTMovie::m_pInstance = NULL;

class CESMMovieRTMovie::CESMMovieRTRun;

CPreRecordPlayManager::CPreRecordPlayManager()
{
	m_strFolder = _T("");
}

CPreRecordPlayManager::~CPreRecordPlayManager()
{
	 
}

void CPreRecordPlayManager::SetLoadFolder(CString strPath)
{
	m_strFolder = strPath;
}

void CPreRecordPlayManager::Ready(CString strPath)
{
	SetLoadFolder(strPath);
	HANDLE hThread = NULL;
	hThread = (HANDLE) _beginthreadex(NULL,0,_FileLoader,(void*)this,0,NULL);
	CloseHandle(hThread);
}

void CPreRecordPlayManager::Start()
{	 
	HANDLE hThread = NULL;
	hThread = (HANDLE) _beginthreadex(NULL,0,_AddLoadingFile,(void*)this,0,NULL);
	CloseHandle(hThread);
}

unsigned WINAPI CPreRecordPlayManager::_FileLoader(LPVOID param)
{
	CPreRecordPlayManager* pManager = (CPreRecordPlayManager*) param;
	CString strPath = pManager->m_strFolder;
	CString tempLog;

	if (pManager->GetArrayFile().size() > 0) {
		pManager->GetArrayFile().erase(pManager->GetArrayFile().begin(), pManager->GetArrayFile().end());
	}

	tempLog = _T("[CPreRecordPlayManager]- Loading start:") + strPath;
	SendLog(5,tempLog);
	
	CFileFind finder;
	BOOL working = finder.FindFile( strPath + _T("*.*") );

	while ( working ) {
		working = finder.FindNextFile();
		if(!finder.IsDirectory() && !finder.IsDots()) {
			CString strPathData;
			CString curfile = finder.GetFileName(); 
			strPathData = strPath + curfile;
			pManager->AddFile(strPathData);			
		}  
	}

	/*std::map<int, CString> kk = pManager->GetArr();
	map<int,CString> :: iterator it; 

	for(it = kk.begin() ; it != kk.end(); it++) {
		CString r = it->second;
		CT2CA pszConvertedAnsiString (r);
		std::string s(pszConvertedAnsiString); 

		cout << "                                        " <<  s.c_str() << endl;
	}
	pManager->FileSort();*/
	
	tempLog.Format(_T("[CPreRecordPlayManager]- Loading completed:[%d]"), pManager->GetFileCount()); 
	SendLog(5,tempLog);

	
	//ESMEvent* pMsg = new ESMEvent();
	//pMsg->message = WM_ESM_NET_CAPTURE_LOG;
	//CString *pLog = new CString;
	//pLog->Format(_T("File list Loading : %s\\"),  pManager->GetLoadFolder());
	//pMsg->pParam = (LPARAM)pLog;
	//CString *pCamera = new CString;
	//pCamera->Format(_T("%s"), mSrcID.c_str());
	//pMsg->pParent = (LPARAM)pCamera;
	//::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);

	return TRUE;
}

unsigned WINAPI CPreRecordPlayManager::_AddLoadingFile(LPVOID param)
{
	CPreRecordPlayManager* pManager = (CPreRecordPlayManager*) param;
	CESMMovieRTMovie::CESMMovieRTRun * RTRun =  CESMMovieRTMovie::GetInstance()->GetFirstESMMovieRun();
	CString strPath = pManager->m_strFolder;
		

	/*for (int i = 0; i < pManager->GetArrayFile().size(); i++)
	{
		CString strFullName;
		strFullName.Format(_T("%s%s_%d.mp4"), strPath, RTRun->GetDsc(), pManager->GetArrayFile().at(i));  
		if (RTRun) {
			RTRun->AddMoviePath(strFullName);
		 
			CT2CA pszConvertedAnsiString (strFullName);
			std::string s(pszConvertedAnsiString); 

			cout << "                                        " <<  s.c_str() << endl;
 
		}
	}*/

	std::map<int, CString> filemap = pManager->GetArr();
	map<int,CString> :: iterator it; 

	for(it = filemap.begin() ; it != filemap.end(); it++) {
		CString strFilePath = it->second;
		if (RTRun)
			RTRun->AddMoviePath(strFilePath);

		CT2CA pszConvertedAnsiString (strFilePath);
		std::string s(pszConvertedAnsiString); 
		cout << "                                        " <<  s.c_str() << endl;
	}

	return TRUE;
}

int CPreRecordPlayManager::GetFileCount()
{
	return sorted_map.size();
}

void CPreRecordPlayManager::AddFile(CString strFile)
{
	if (strFile.GetLength() > 0) {
		int nFind = strFile.ReverseFind('\\') + 1;
		CString csFile = strFile.Right(strFile.GetLength()-nFind);
		csFile.Trim();
	 
		nFind = csFile.Find('.');
		CString csCam = csFile.Left(nFind);
		csCam.Trim();
		nFind = csCam.ReverseFind('_') + 1;

		CString strSecIdx = csCam.Right(csCam.GetLength()-nFind);

		int secIdx = _ttoi(strSecIdx);
		if ( secIdx >= 0 ) {
			//m_arrFileName.push_back(secIdx);
			sorted_map.insert(std::make_pair(secIdx, strFile));
		}
	}
}

void CPreRecordPlayManager::FileSort() 
{
	//sort(m_arrFileName.begin(), m_arrFileName.end());
}


CESMMovieRTMovie::CESMMovieRTMovie()
{
	m_pInstance = this;
	m_nDSCIdx = 0;
	m_nFrameRate = 30;
	m_nMarginX = 0;
	m_nMarginY = 0;
	
	m_bLiveStreaming = TRUE;
	m_bMovieStart = FALSE;
	m_bThreadRun   = FALSE;
	m_bExternalCamera = FALSE;
	m_bAudioEnable = FALSE;

	m_bMulticast = FALSE;
	m_bPush = TRUE;
	m_nLive_framerate = 30;
	m_nLive_bitrate = 1024*1024*20;
	m_nLive_resolution_width = 1920;
	m_nLive_resolution_height = 1080;


	m_pRTSPServerMgr = NULL;
	m_pClock		 = NULL;
	m_pWnd			 = NULL;

	m_bGPUCheck		= FALSE;

	m_bUpdateAdj	= FALSE;
	m_bRotate		= FALSE;
	m_fRotateDegree = 180.f;

	InitializeCriticalSection(&m_criImage);
	InitializeCriticalSection(&m_criPathMgr);
	
}
CESMMovieRTMovie::~CESMMovieRTMovie()
{
	SetThreadRun(TRUE);
	
	DeleteCriticalSection(&m_criImage);
	DeleteCriticalSection(&m_criPathMgr);

#if defined(WITH_HEVC_TEST) || defined(WITH_AVC_TEST)
	if (m_pRTSPServer)
	{
		m_pRTSPServer->Stop();
		delete m_pRTSPServer;
		m_pRTSPServer = NULL;
	}
#endif
	//close
	//if (m_pRTSPServerMgr) {
	//	delete m_pRTSPServerMgr;
	//	m_pRTSPServerMgr = NULL;
	//}
	
}
void CESMMovieRTMovie::Run()
{
	m_pRTSPServerMgr = new CRTSPServerMgr;
	m_pRTSPServerMgr->SetDSCIndex(m_nDSCIdx);
	m_pRTSPServerMgr->SetDSC(m_strDSC);
	m_pRTSPServerMgr->SetFrameRate(m_nLive_framerate);
	m_pRTSPServerMgr->SetAudioEnable(m_bAudioEnable);
	m_pRTSPServerMgr->SetExternalCamera(m_bExternalCamera);
	m_pRTSPServerMgr->SetMainWnd(m_pWnd);
	m_pRTSPServerMgr->SetMulticastEnable(m_bMulticast);
	m_pRTSPServerMgr->SetPushEnable(m_bPush);
		
	m_pRTSPServerMgr->InitRTSPServer();

#if defined(WITH_HEVC_TEST)
	m_pRTSPServer = new ESMRTSPServer;
	m_pRTSPServer->Start(ESMRTSPServer::VIDEO_CODEC_T::HEVC, 554);
	::Sleep(10);
#elif defined(WITH_AVC_TEST)
	m_pRTSPServer = new ESMRTSPServer;
	m_pRTSPServer->Start(ESMRTSPServer::VIDEO_CODEC_T::AVC, 554);
	::Sleep(10);
#endif
}

void CESMMovieRTMovie::UpdateDSCIndex(int nIdx) 
{ 
	m_nDSCIdx = nIdx;

	if (m_pRTSPServerMgr) {
		m_pRTSPServerMgr->UpdateDSCIndex(nIdx);
	}
}

CString CESMMovieRTMovie::Get4DMNameFromPath(CString strPath)
{
	CString str = strPath;
	int nFind = str.ReverseFind(_T('\\'));

	str = str.Mid(0,nFind);

	nFind = str.ReverseFind(_T('\\'));
	str = str.Mid(nFind+1,str.GetLength()-nFind);

	return str;
}
void CESMMovieRTMovie::SetAdjustData(stAdjustInfo* AdjustData)
{
	CString strDSCID = AdjustData->strDSC;
	m_mapAdjData[strDSCID] = *AdjustData;

	/*
	char debug[MAX_PATH] = {0};
	_snprintf_s(debug, MAX_PATH, " %f, %f, %f, %f, %f, %f, %f, %f, %d, %d, %d, %d, %d, %d, %d, %d\n", 
						AdjustData->AdjMove.x, AdjustData->AdjMove.y,
						AdjustData->AdjptRotate.x, AdjustData->AdjptRotate.y,
						AdjustData->AdjptRotate2.x, AdjustData->AdjptRotate2.y,
						AdjustData->AdjAngle, AdjustData->AdjSize,
						AdjustData->nWidth, AdjustData->nHeight,
						AdjustData->stMargin.nMarginX, AdjustData->stMargin.nMarginY,
						AdjustData->rtMargin.left, AdjustData->rtMargin.right, AdjustData->rtMargin.top, AdjustData->rtMargin.bottom);
	::OutputDebugStringW(AdjustData->strDSC);
	::OutputDebugStringA(debug);
	*/

	m_bUpdateAdj = TRUE;
}
stAdjustInfo CESMMovieRTMovie::GetAdjustData(CString strDSCID)
{
	stAdjustInfo adjinfo = m_mapAdjData[strDSCID];

	/*
	char debug[MAX_PATH] = {0};
	_snprintf_s(debug, MAX_PATH, " %f, %f, %f, %f, %f, %f, %f, %f, %d, %d, %d, %d, %d, %d, %d, %d\n", 
						adjinfo.AdjMove.x, adjinfo.AdjMove.y,
						adjinfo.AdjptRotate.x, adjinfo.AdjptRotate.y,
						adjinfo.AdjptRotate2.x, adjinfo.AdjptRotate2.y,
						adjinfo.AdjAngle, adjinfo.AdjSize,
						adjinfo.nWidth, adjinfo.nHeight,
						adjinfo.stMargin.nMarginX, adjinfo.stMargin.nMarginY,
						adjinfo.rtMargin.left, adjinfo.rtMargin.right, adjinfo.rtMargin.top, adjinfo.rtMargin.bottom);
	::OutputDebugStringW(adjinfo.strDSC);
	::OutputDebugStringA(debug);
	*/

	return adjinfo;
}
void CESMMovieRTMovie::SetMargin(int nMarginX,int nMarginY)
{
	m_nMarginX = nMarginX;
	m_nMarginY = nMarginY;
}
void CESMMovieRTMovie::SetSyncTime(CString str4DM,int nTime,int nESMTick)
{
	CESMMovieRTRun* pRTRun = GetESMMovieRun(str4DM);
	
	pRTRun->SetSyncTime(nTime);
}
void CESMMovieRTMovie::SetSavePath(CString strPath)
{
	CString str4DM = Get4DMNameFromPath(strPath);
	CESMMovieRTRun* pRTRun = GetESMMovieRun(str4DM);

	pRTRun->SetSavePath(strPath);
	SendLog(5,_T("[ESMMovieRTMovie] - Save Path:") + strPath);
}

void CESMMovieRTMovie::SetAudioEnable(CString strPath, BOOL bEnable)
{
	CString str4DM = Get4DMNameFromPath(strPath);
	CESMMovieRTRun* pRTRun = GetESMMovieRun(str4DM);

	pRTRun->SetAudioEnable(bEnable);
	CString tempLog;
	tempLog.Format(_T("[ESMMovieRTMovie] - AudioEnable: %s"), (bEnable==TRUE)? "Y":"N" );
	SendLog(5,tempLog);	 
}

void CESMMovieRTMovie::SetExternalCamera(CString strPath, BOOL bValue)
{
	CString str4DM = Get4DMNameFromPath(strPath);
	CESMMovieRTRun* pRTRun = GetESMMovieRun(str4DM);

	pRTRun->SetExternalCamera(bValue);
	CString tempLog;
	tempLog.Format(_T("[ESMMovieRTMovie] - ExternalCamera: %s"), (bValue==TRUE)? "Y":"N" );
	SendLog(5,tempLog);
}

void CESMMovieRTMovie::SetPushInfo(CString strPath, CString strUUID, CString strPushIP, int nPort)
{
	CString str4DM = Get4DMNameFromPath(strPath);
	CESMMovieRTRun* pRTRun = GetESMMovieRun(str4DM);

	pRTRun->SetPushInfo(strUUID, strPushIP, nPort);
	CString tempLog;
	tempLog.Format(_T("[ESMMovieRTMovie] - Push Info : IP[%s], Port[%d], UUID[%s]"), strPushIP, nPort, strUUID);
	SendLog(5,tempLog);
}

void CESMMovieRTMovie::SetActiveChannel(BOOL isActiveChannel) 
{
	if (m_pRTSPServerMgr) {
		m_pRTSPServerMgr->SetActiveChannel(isActiveChannel);
	}

}

void CESMMovieRTMovie::SetStopIdx(CString str4DM,int n)
{
	CESMMovieRTRun* pRTRun = GetESMMovieRun(str4DM);
	pRTRun->SetStopIdx(n);
}


void CESMMovieRTMovie::OnStreamRecordStart(CString str4DM, BOOL bRTSP)
{
	CESMMovieRTRun* pRTRun = GetESMMovieRun(str4DM);
	pRTRun->OnStreamRecordStart(bRTSP);
}
void CESMMovieRTMovie::OnStreamRecordStop(CString str4DM)
{
	if ( IsExistESMMovieRun(str4DM) == TRUE ) {
		CESMMovieRTRun* pRTRun = GetESMMovieRun(str4DM);
		pRTRun->OnStreamRecordStop();
		pRTRun->SetWaitingTimeFinish(TRUE);	
	}
	else {
		CString strLog;
		strLog.Format(_T("OnStreamRecordStop : Not found 4DM[%s]"), str4DM);
		SendLog(5,strLog);
	}
}
void CESMMovieRTMovie::OnStreamSync(int nMedia, int nDirection, int nValue)
{
	CESMMovieRTRun* pRTRun = GetFirstESMMovieRun();
	pRTRun->OnStreamSync(nMedia, nDirection, nValue);
}
void CESMMovieRTMovie::OnStreamWrite(CString str4DM, CString strSavePath, CString strRamPath, int nParam, int nFrame, int nExtFrame, int nVrFrame)
{
	//CString str4DM = Get4DMNameFromPath(strSavePath);
	if ( str4DM.GetLength() > 0 ) {
		CESMMovieRTRun* pRTRun = GetESMMovieRun(str4DM);
		pRTRun->OnStreamWrite(strSavePath, strRamPath, nParam, nFrame, nExtFrame, nVrFrame);
	}
	else {
		CString strLog;
		strLog.Format(_T("OnStreamWrite : Invalid 4DM[%s]"), str4DM);
		SendLog(5,strLog);
	}
}
void CESMMovieRTMovie::OnStreamPlay(int nParam, CString strFolder, CString strPath)
{
	CESMMovieRTRun* pRTRun = GetFirstESMMovieRun();
	pRTRun->OnStreamPlay(nParam, strFolder, strPath);
}
void CESMMovieRTMovie::SetLiveStreaming(BOOL bEnable)
{
	m_bLiveStreaming = bEnable;
}
BOOL CESMMovieRTMovie::GetLiveStreaming()
{
	return m_bLiveStreaming;
}


////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////

CESMMovieRTMovie::CESMMovieRTRun::CESMMovieRTRun(CESMMovieRTMovie* pRTMovie,CString str4DM,CStopWatch* pStopWatch)
{
	avcodec_register_all();
	av_register_all();

	m_pParent = pRTMovie;
	m_str4DM = str4DM;
	m_pClock = pStopWatch; 

	m_bAudioEnabled = FALSE;
	m_bExternalCamera = FALSE;
	m_strPushIP = _T("");
	m_strPushUUID = _T("");
	m_nPushPort = 0;

	m_nSyncTime = 0;
	m_nFrameRate = 30;
	m_nWidth = 1920;
	m_nHeight = 1080;
	m_nYSize = m_nWidth * m_nHeight;
	m_nUVSize = m_nYSize / 4;

	m_nYWidth = m_nWidth;
	m_nYHeight = m_nHeight;
	m_nUVWidth = m_nWidth / 2;
	m_nUVHeight = m_nHeight / 2;

	m_nYTop = 0;
	m_nYLeft = 0;
	m_nUVTop = 0;
	m_nUVLeft = 0;

	m_bSetProperty = FALSE;
	m_bLoadAdj	   = FALSE;
	m_bPathMgrRun  = FALSE;
	m_bWaitingTimeRun = FALSE;
	m_bUseRect = FALSE;

	m_nStopIdx = 0x7fffffff; //99999999;

	InitializeCriticalSection(&m_criPathMgr);
	InitializeCriticalSection(&m_criDecode);
	InitializeCriticalSection(&m_criSyncMgr);
	InitializeCriticalSection(&m_criEncode);
	InitializeCriticalSection(&m_criStopTime);
	InitializeCriticalSection(&m_criBitStream);

	//InitializeCriticalSection(&m_criProcess);
	InitializeCriticalSection(&m_criAdjust);
	m_ContainerEvent = ::CreateEvent(NULL, FALSE, FALSE, NULL);
	
	m_matLogoImg = imread("C:\\Program Files\\ESMLab\\4DMaker\\img\\WaitImg.png",IMREAD_UNCHANGED);
	if(m_matLogoImg.empty())
	{
		m_nvBlender = NULL;
		SendLog(5,_T("Do not loaded background image...."));
	}
	else
	{
		m_nvBlender = new ESMNvblender();
		//m_nvBlender = NULL;
		strncpy_s(m_nvBlenderCtx.file, "C:\\Program Files\\ESMLab\\4DMaker\\img\\WaitImg.png", MAX_PATH);
		CString strLog;
		strLog.Format(_T("Logo Image Loaded(%dx%dx%d)"),m_matLogoImg.cols,m_matLogoImg.rows,m_matLogoImg.channels());
		SendLog(5,strLog);
	}

	m_nSecIdx		= 0;
	m_nFrameCnt		= 0;
	m_nvDecoder		= NULL;
	m_nvEncoder		= NULL;
	m_nvResizer		= NULL;
	m_nvRotator		= NULL;
	m_bRotate		= FALSE;
	m_fRotateDegree = 180;
#if defined(WITH_DRM)
	m_drmHandler	= NULL;
#endif
	RunThread();
}

CESMMovieRTMovie::CESMMovieRTRun::~CESMMovieRTRun()
{
	if(m_nvEncoder)
	{
		m_nvEncoder->Release();
		delete m_nvEncoder;
		m_nvEncoder = NULL;
	}
	if(m_nvDecoder)
	{
		m_nvDecoder->Release();
		delete m_nvDecoder;
		m_nvDecoder = NULL;
	}
	if(m_nvResizer)
	{
		m_nvResizer->Release();
		delete m_nvResizer;
		m_nvResizer = NULL;
	}

	if(m_nvBlender)
	{
		//m_nvBlender->EndAlphaNV12Debug();
		m_nvBlender->Release();
		delete m_nvBlender;
		m_nvBlender = NULL;
	}

	if(m_nvRotator)
	{
		m_nvRotator->Release();
		delete m_nvRotator;
		m_nvRotator = NULL;
	}

#if defined(WITH_DRM)
	if(m_drmHandler)
	{
		m_drmHandler->Release();
		delete m_drmHandler;
		m_drmHandler = NULL;
	}
#endif

	map<CString, CPreRecordPlayManager*>::iterator iter;
	for(iter = m_mpPreRecordPlayer.begin() ; iter != m_mpPreRecordPlayer.end(); iter++){
		CPreRecordPlayManager* pManager = iter->second;
		delete pManager;
		m_mpPreRecordPlayer.erase (iter);
	}

	DeleteCriticalSection(&m_criPathMgr);
	DeleteCriticalSection(&m_criDecode);
	DeleteCriticalSection(&m_criSyncMgr);
	DeleteCriticalSection(&m_criEncode);
	DeleteCriticalSection(&m_criStopTime);
	DeleteCriticalSection(&m_criBitStream);

	//DeleteCriticalSection(&m_criProcess);
	DeleteCriticalSection(&m_criAdjust);

	::CloseHandle(m_ContainerEvent);

	m_pParent->m_bUpdateAdj = FALSE;
}

void CESMMovieRTMovie::CESMMovieRTRun::RunThread()
{
	HANDLE hIS = NULL;
	hIS = (HANDLE) _beginthreadex(NULL,0, CESMMovieRTMovie::CESMMovieRTRun::StreamWaitingProcessCB,this,0,NULL);
	CloseHandle(hIS);
}

unsigned WINAPI CESMMovieRTMovie::CESMMovieRTRun::StreamWaitingProcessCB(LPVOID param)
{
	CESMMovieRTRun* pRTRun = (CESMMovieRTRun*)param;
	pRTRun->StreamWaitingProcess();
	return 0;	
}

void CESMMovieRTMovie::CESMMovieRTRun::StreamWaitingProcess(void) 
{
	CESMMovieRTMovie * pRTMovie = m_pParent;
	int		nCnt = 0;
 
	CString strLog;
	strLog.Format(_T("[ESMMovieRun] - StreamWaitingProcess Started. Clock[%d] SyncTime[%d]"), GetClock(), GetSyncTime());
	SendLog(5, strLog);

	while(!GetWaitingTimeFinish())
	{
		if(GetClock() >= GetSyncTime()) {
			if ( GetVideoBitstreamCount() > 0 ) {
				std::shared_ptr<BITSTREAM_INFO> info = GetFirstIndexVideoBitstream();
				CRTSPServerMgr* pRTSPServer = m_pParent->GetRTSPServer();
				if (pRTSPServer) {
					pRTSPServer->PutWaitedBitstream(info);
				}
				
				DeleteFirstIndexVideoBitstream();
				nCnt = 0;
			}
			else 
			{
				/*wait_for_usec(1000);				
				if(++nCnt > 10000)	{
					break;
				}*/	
				SleepShort(1);
				if(++nCnt > 10000)	{
					break;
				}
			}
		}
		else {
			SleepShort(1);			 
		}
	}

	SendLog(5,_T("[ESMMovieRun] - ") + m_str4DM + _T("_StreamWaitingProcess Finish"));
	pRTMovie->DeleteRTRun(m_str4DM);
	
}

int CESMMovieRTMovie::CESMMovieRTRun::GetSecIdxFromPath(CString strPath)
{
	int nFind = strPath.ReverseFind('\\') + 1;
	CString csFile = strPath.Right(strPath.GetLength()-nFind);
	csFile.Trim();
	//csFile.Trim(_T("\\"));
	nFind = csFile.Find('.');
	CString csCam = csFile.Left(nFind);
	csCam.Trim();
	nFind = csCam.ReverseFind('_') + 1;

	CString strSecIdx = csCam.Right(csCam.GetLength()-nFind);

	return _ttoi(strSecIdx);
}
int CESMMovieRTMovie::CESMMovieRTRun::GetClock()
{
	m_pClock->End();

	return (int)(m_pClock->GetDurationMilliSecond() * 10);
}

//static long g_ts = 0;
int	CESMMovieRTMovie::CESMMovieRTRun::TranscodeAndProcessImage(int nCodec, int nWidth, int nHeight, double fps, unsigned char * bitstream, int bitstreamSize, CString str4DM, CString strInit4DM, int & nIndex, int & nFrameCnt, int ts, int nGap)
{
	//::EnterCriticalSection(&m_criProcess);

	double outputFps = m_pParent->m_nLive_framerate;
	int encodeCycle =  m_pParent->m_nFrameRate/outputFps;
	if(encodeCycle<1)
		encodeCycle = 1;

	static int inputCount = 0;
	static int decodeCount = 0;
	static int encodeCount = 0;

	int nOutputWidth = m_pParent->m_nLive_resolution_width;
	int nOutputHeight = m_pParent->m_nLive_resolution_height;
	int nOutputBitrate = m_pParent->m_nLive_bitrate;

	char debug[MAX_PATH] = {0};
	_snprintf_s(debug, MAX_PATH, "############################# %s[inputCount] : %d\n", __FILE__, inputCount);
	//::OutputDebugStringA(debug);
	inputCount++;

	CRTSPServerMgr* pRTSPServer = CESMMovieRTMovie::GetInstance()->GetRTSPServer();
	if(m_bLoadAdj == FALSE && m_bSetProperty == FALSE)
	{
		LoadAdjustData(m_pParent->m_strDSC,nWidth, nHeight);
	}
	else if(m_bLoadAdj==TRUE && m_bSetProperty==TRUE && m_pParent->m_bUpdateAdj==TRUE)
	{
		LoadAdjustData(m_pParent->m_strDSC,nWidth, nHeight);
		::OutputDebugStringA("##########################Load Adjust##############################\n");
		if(m_bLoadAdj==FALSE || m_bSetProperty==FALSE)
		{
			::OutputDebugStringA("##########################Load Adjust Fail##############################\n");
		}
	}

	if (GetExternalCamera() == FALSE && !(m_bSetProperty && m_bLoadAdj)) 
	{  // For MBC
		nWidth = nOutputWidth;
		nHeight = nOutputHeight;
	}

	int decoderCodec = ESMNvdec::VIDEO_CODEC_T::AVC;
	int encoderCodec = ESMNvenc::VIDEO_CODEC_T::AVC;
	int encoderExtradataSize = 0;
	uint8_t * encoderExtradata = NULL;
	switch(nCodec)
	{
	case AV_CODEC_ID_H264 : 
		decoderCodec = ESMNvdec::VIDEO_CODEC_T::AVC;
		encoderCodec = ESMNvenc::VIDEO_CODEC_T::AVC;
		break;
	case AV_CODEC_ID_HEVC : 
		decoderCodec = ESMNvdec::VIDEO_CODEC_T::HEVC;
		encoderCodec = ESMNvenc::VIDEO_CODEC_T::HEVC;
		break;
	default:
		decoderCodec = ESMNvdec::VIDEO_CODEC_T::AVC;
		encoderCodec = ESMNvenc::VIDEO_CODEC_T::AVC;
		break;
	}

	if(m_nvDecoder==NULL)
	{
		m_nvDecoder = new ESMNvdec();
		m_nvDecoderCtx.codec = decoderCodec;	
		m_nvDecoderCtx.deviceIndex = 0;
		m_nvDecoderCtx.width = nWidth;
		m_nvDecoderCtx.height = nHeight;
		if(m_bSetProperty && m_bLoadAdj)
		{
			m_nvDecoderCtx.colorspace = ESMNvdec::COLORSPACE_T::YV12;

			m_nvBlenderCtx.deviceIndex	= m_nvDecoderCtx.deviceIndex;
			m_nvBlenderCtx.width		= nOutputWidth;
			m_nvBlenderCtx.height		= nOutputHeight;
			m_nvBlenderCtx.colorspace	= ESMNvblender::COLORSPACE_T::YV12;
		}
		else
		{
			m_nvDecoderCtx.colorspace = ESMNvdec::COLORSPACE_T::NV12;

			m_nvBlenderCtx.deviceIndex	= m_nvDecoderCtx.deviceIndex;
			m_nvBlenderCtx.width		= nOutputWidth;
			m_nvBlenderCtx.height		= nOutputHeight;
			m_nvBlenderCtx.colorspace	= ESMNvblender::COLORSPACE_T::NV12;
		}
		m_nvDecoder->Initialize(&m_nvDecoderCtx);
		if(m_nvBlender)
		{
			m_nvBlender->Initialize(&m_nvBlenderCtx);
			//m_nvBlender->BeginAlphaNV12Debug();
		}
	}
	else if(!m_nvDecoder->IsInitialized())
	{
		m_nvDecoderCtx.codec = decoderCodec;
		m_nvDecoderCtx.deviceIndex = 0;
		m_nvDecoderCtx.width = nWidth;
		m_nvDecoderCtx.height = nHeight;
		if(m_bSetProperty && m_bLoadAdj)
		{
			m_nvDecoderCtx.colorspace = ESMNvdec::COLORSPACE_T::YV12;

			m_nvBlenderCtx.deviceIndex	= m_nvDecoderCtx.deviceIndex;
			m_nvBlenderCtx.width		= nOutputWidth;
			m_nvBlenderCtx.height		= nOutputHeight;
			m_nvBlenderCtx.colorspace	= ESMNvblender::COLORSPACE_T::YV12;
		}
		else
		{
			m_nvDecoderCtx.colorspace = ESMNvdec::COLORSPACE_T::NV12;

			m_nvBlenderCtx.deviceIndex	= 0;
			m_nvBlenderCtx.width		= nOutputWidth;
			m_nvBlenderCtx.height		= nOutputHeight;
			m_nvBlenderCtx.colorspace	= ESMNvblender::COLORSPACE_T::NV12;
		}
		m_nvDecoder->Initialize(&m_nvDecoderCtx);
		if(m_nvBlender)
			m_nvBlender->Initialize(&m_nvBlenderCtx);
	}
	else
	{
		if(m_nvDecoderCtx.codec!=decoderCodec || m_nvDecoderCtx.width!=nWidth || m_nvDecoderCtx.height!=nHeight)
		{
			m_nvDecoder->Release();
			m_nvDecoderCtx.codec = decoderCodec;
			m_nvDecoderCtx.deviceIndex = 0;
			m_nvDecoderCtx.width = nWidth;
			m_nvDecoderCtx.height = nHeight;
			if(m_bSetProperty && m_bLoadAdj)
			{
				m_nvDecoderCtx.colorspace = ESMNvdec::COLORSPACE_T::YV12;

				m_nvBlenderCtx.deviceIndex	= 0;
				m_nvBlenderCtx.width		= nOutputWidth;
				m_nvBlenderCtx.height		= nOutputHeight;
				m_nvBlenderCtx.colorspace	= ESMNvblender::COLORSPACE_T::YV12;
			}
			else
			{
				m_nvDecoderCtx.colorspace = ESMNvdec::COLORSPACE_T::NV12;

				m_nvBlenderCtx.deviceIndex	= 0;
				m_nvBlenderCtx.width		= nOutputWidth;
				m_nvBlenderCtx.height		= nOutputHeight;
				m_nvBlenderCtx.colorspace	= ESMNvblender::COLORSPACE_T::NV12;
			}
			m_nvDecoder->Initialize(&m_nvDecoderCtx);
			if(m_nvBlender)
				m_nvBlender->Initialize(&m_nvBlenderCtx);
		}
	}

	if(m_bRotate)
	{
		if(m_nvRotator==NULL)
		{
			m_nvRotator = new ESMNvrotator();
			m_nvRotatorCtx.deviceIndex = 0;
			m_nvRotatorCtx.width = nOutputWidth;
			m_nvRotatorCtx.height = nOutputHeight;
			if(m_bSetProperty && m_bLoadAdj)
			{
				m_nvRotatorCtx.colorspace = ESMNvdec::COLORSPACE_T::YV12;
			}
			else
			{
				m_nvRotatorCtx.colorspace = ESMNvdec::COLORSPACE_T::NV12;
			}
			m_nvRotator->Initialize(&m_nvRotatorCtx);
		}
		else if(!m_nvRotator->IsInitialized())
		{
			m_nvRotatorCtx.deviceIndex = 0;
			m_nvRotatorCtx.width = nOutputWidth;
			m_nvRotatorCtx.height = nOutputHeight;
			if(m_bSetProperty && m_bLoadAdj)
			{
				m_nvRotatorCtx.colorspace = ESMNvdec::COLORSPACE_T::YV12;
			}
			else
			{
				m_nvRotatorCtx.colorspace = ESMNvdec::COLORSPACE_T::NV12;
			}
			m_nvRotator->Initialize(&m_nvRotatorCtx);
		}
		else
		{
			if(m_nvRotatorCtx.width!=nOutputWidth || m_nvRotatorCtx.height!=nOutputHeight)
			{
				m_nvRotator->Release();
				m_nvRotatorCtx.deviceIndex = 0;
				m_nvRotatorCtx.width = nOutputWidth;
				m_nvRotatorCtx.height = nOutputHeight;
				if(m_bSetProperty && m_bLoadAdj)
				{
					m_nvRotatorCtx.colorspace = ESMNvdec::COLORSPACE_T::YV12;
				}
				else
				{
					m_nvRotatorCtx.colorspace = ESMNvdec::COLORSPACE_T::NV12;
				}
				m_nvRotator->Initialize(&m_nvRotatorCtx);
			}
		}
	}
/*
	do 
	{
*/
		int nDecoded = 0;
		unsigned char ** ppDecoded = NULL;
		long long * pTimetstamp = NULL;
		tc_userdata * pReturedUserData;

		tc_userdata tempData; 
		tempData.frameNum = nFrameCnt;
		tempData.secIndex = nIndex;

		m_nvDecoder->Decode(bitstream, bitstreamSize, ts, &ppDecoded, &nDecoded, &pTimetstamp, (void *)&tempData, (void **)&pReturedUserData);
		for(int i=0; i<nDecoded; i++)
		{
			_snprintf_s(debug, MAX_PATH, "############################# %s[decodeCount] : %d\n", __FILE__, decodeCount);
			decodeCount++;

			/*if((m_nFrameCnt+1)%encodeCycle!=0)
			{
				m_nFrameCnt++;
				continue;
			}*/

			tc_userdata udData = *(pReturedUserData+i);
			
			//cout << "-------------------------   " <<	nFrameCnt  << "  -------------------------------"  << udData.frameNum << "-------" << udData.secIndex << endl;	
			if((udData.frameNum)%encodeCycle!=0)
			{
				//m_nFrameCnt++;
				continue;
			}

			std::shared_ptr<FRAME_INFO> info = std::shared_ptr<FRAME_INFO>(new FRAME_INFO(ENCODE_FHD_BITRATE));

			info->nCodec				= nCodec;
			info->nWidth				= nWidth;
			info->nHeight				= nHeight;
			info->nFps					= fps;
			int lumaWidth				= nWidth;
			int lumaHeight				= nHeight;
			int chromaWidth				= lumaWidth >> 1;
			int chromaHeight			= lumaHeight >> 1;
			int lumaPitch				= m_nvDecoder->GetPitch2();
			int chromaPitch				= lumaPitch >> 1;
			int dpYUVPitch				= lumaPitch;
			int dpResizedYUVPitch		= 0;
			int dpRotatedYUVPitch		= 0;
			unsigned char * dpYUVFrame	= ppDecoded[i];
			unsigned char * dpResizedYUVFrame = NULL;
			unsigned char * dpRotatedYUVFrame = NULL;
			if(m_bSetProperty && m_bLoadAdj)
			{
				unsigned char * yDeviceptr = dpYUVFrame;
				unsigned char * uDeviceptr = yDeviceptr + lumaPitch * lumaHeight;
				unsigned char * vDeviceptr = uDeviceptr + chromaPitch * chromaHeight;

				cv::cuda::GpuMat gpuY(lumaHeight, lumaWidth, CV_8UC1, yDeviceptr, lumaPitch);
				cv::cuda::GpuMat gpuV(chromaHeight, chromaWidth, CV_8UC1, uDeviceptr, chromaPitch);
				cv::cuda::GpuMat gpuU(chromaHeight, chromaWidth, CV_8UC1, vDeviceptr, chromaPitch);


				info->gpuY = gpuY;
				info->gpuU = gpuU;
				info->gpuV = gpuV;

				::EnterCriticalSection(&m_criAdjust);
				ImageProcessFrame(info);
				::LeaveCriticalSection(&m_criAdjust);

				gpuY.release();
				gpuU.release();
				gpuV.release();
			}

			info->nSecIdx = udData.secIndex;
			info->nFrameIdx = udData.frameNum / encodeCycle ;  //m_nFrameCnt++; 
			info->nGap = 1;

			//if(!m_matLogoImg.empty())
			//	CreateLogoImage(info);

			if(m_bSetProperty && m_bLoadAdj)
			{
				if(nWidth!=nOutputWidth || nHeight!=nOutputHeight)
				{
					if(m_nvResizer==NULL)
					{
						m_nvResizer = new ESMNvresizer();
						m_nvResizerCtx.deviceIndex = m_nvDecoderCtx.deviceIndex;
						m_nvResizerCtx.colorspace = ESMNvresizer::COLORSPACE_T::YV12;
						m_nvResizerCtx.inputWidth = nWidth;
						m_nvResizerCtx.inputHeight = nHeight;
						m_nvResizerCtx.outputWidth = nOutputWidth;
						m_nvResizerCtx.outputHeight = nOutputHeight;
						m_nvResizer->Initialize(&m_nvResizerCtx);
					}
					else if(!m_nvResizer->IsInitialized())
					{
						m_nvDecoderCtx.codec = decoderCodec;	
						m_nvResizerCtx.deviceIndex = m_nvDecoderCtx.deviceIndex;
						m_nvResizerCtx.colorspace = ESMNvresizer::COLORSPACE_T::YV12;
						m_nvResizerCtx.inputWidth = nWidth;
						m_nvResizerCtx.inputHeight = nHeight;
						m_nvResizerCtx.outputWidth = nOutputWidth;
						m_nvResizerCtx.outputHeight = nOutputHeight;
						m_nvResizer->Initialize(&m_nvResizerCtx);
					}
					::EnterCriticalSection(&m_criAdjust);
					m_nvResizer->Resize(dpYUVFrame, dpYUVPitch, &dpResizedYUVFrame, dpResizedYUVPitch);
					::LeaveCriticalSection(&m_criAdjust);
				}
			}

			if(m_nvRotator)
			{
				if(m_bSetProperty && m_bLoadAdj)
				{
					if(nWidth!=nOutputWidth || nHeight!=nOutputHeight)
						m_nvRotator->Rotate(dpResizedYUVFrame, dpResizedYUVPitch, &dpRotatedYUVFrame, dpRotatedYUVPitch, m_fRotateDegree);
					else
						m_nvRotator->Rotate(dpYUVFrame, dpYUVPitch, &dpRotatedYUVFrame, dpRotatedYUVPitch, m_fRotateDegree);
				}
				else
				{
					m_nvRotator->Rotate(dpYUVFrame, dpYUVPitch, &dpRotatedYUVFrame, dpRotatedYUVPitch, m_fRotateDegree);
				}
			}

			if(m_nvBlender)
			{
				if(m_nvRotator)
				{
					m_nvBlender->Blend(NULL, 0, dpRotatedYUVFrame, dpRotatedYUVPitch, NULL, 0, NULL, 0);
				}
				else
				{
					if(m_bSetProperty && m_bLoadAdj)
					{
						if(nWidth!=nOutputWidth || nHeight!=nOutputHeight)
							m_nvBlender->Blend(NULL, 0, dpResizedYUVFrame, dpResizedYUVPitch, NULL, 0, NULL, 0);
						else
							m_nvBlender->Blend(NULL, 0, dpYUVFrame, dpYUVPitch, NULL, 0, NULL, 0);
					}
					else
					{
						m_nvBlender->Blend(NULL, 0, dpYUVFrame, dpYUVPitch, NULL, 0, NULL, 0);
					}
				}
			}

			if(m_nvEncoder==NULL)
			{
				m_nvEncoder = new ESMNvenc();
				::memset(&m_nvEncoderCtx, 0x00, sizeof(m_nvEncoderCtx));
				m_nvEncoderCtx.deviceIndex	= m_nvDecoderCtx.deviceIndex;
				m_nvEncoderCtx.width		= nOutputWidth;
				m_nvEncoderCtx.height		= nOutputHeight;
				m_nvEncoderCtx.codec		= encoderCodec;
				m_nvEncoderCtx.profile		= ESMNvenc::AVC_PROFILE_T::BP;
				m_nvEncoderCtx.gop			= (GetExternalCamera() && ((nWidth * nHeight) > (1920 * 1080))) ? 10 : 1;    //for MBC, VR Camera Check 
				m_nvEncoderCtx.fps			= round(outputFps);
				m_nvEncoderCtx.bitrate		= (GetExternalCamera() && ((nWidth * nHeight) > (1920 * 1080))) ? (1024 * 1024 * 50) : (nOutputBitrate);    //for MBC, VR Camera Check 
				if(m_bSetProperty && m_bLoadAdj)
					m_nvEncoderCtx.colorspace = ESMNvenc::COLORSPACE_T::YV12;
				else
					m_nvEncoderCtx.colorspace = ESMNvenc::COLORSPACE_T::NV12;
				m_nvEncoder->Initialize(&m_nvEncoderCtx);
				encoderExtradata = m_nvEncoder->GetExtradata(encoderExtradataSize);
#if defined(WITH_HEVC_TEST) || defined(WITH_AVC_TEST)
				ESMRTSPServer * pRTSPServer = m_pParent->GetRTSPServer2();
				pRTSPServer->SetExtradata(encoderExtradata, encoderExtradataSize);
#endif
			}
			else if(!m_nvEncoder->IsInitialized())
			{
				::memset(&m_nvEncoderCtx, 0x00, sizeof(m_nvEncoderCtx));
				m_nvEncoderCtx.deviceIndex	= m_nvDecoderCtx.deviceIndex;
				m_nvEncoderCtx.width		= nOutputWidth;
				m_nvEncoderCtx.height		= nOutputHeight;
				m_nvEncoderCtx.codec		= encoderCodec;
				m_nvEncoderCtx.profile		= ESMNvenc::AVC_PROFILE_T::BP;
				m_nvEncoderCtx.gop			= (GetExternalCamera() && ((nWidth * nHeight) > (1920 * 1080))) ? 10 : 1;
				m_nvEncoderCtx.fps			= round(outputFps);
				m_nvEncoderCtx.bitrate		= (GetExternalCamera() && ((nWidth * nHeight) > (1920 * 1080)))? (1024 * 1024 * 50) : (nOutputBitrate);
				if(m_bSetProperty && m_bLoadAdj)
					m_nvEncoderCtx.colorspace = ESMNvenc::COLORSPACE_T::YV12;
				else
					m_nvEncoderCtx.colorspace = ESMNvenc::COLORSPACE_T::NV12;
				m_nvEncoder->Initialize(&m_nvEncoderCtx);
				encoderExtradata = m_nvEncoder->GetExtradata(encoderExtradataSize);
#if defined(WITH_HEVC_TEST) || defined(WITH_AVC_TEST)
				ESMRTSPServer * pRTSPServer = m_pParent->GetRTSPServer2();
				pRTSPServer->SetExtradata(encoderExtradata, encoderExtradataSize);
#endif
			}
			else
			{
				if((m_nvEncoderCtx.codec!=encoderCodec) || (m_nvEncoderCtx.width != nOutputWidth) || (m_nvEncoderCtx.height != nOutputHeight))
				{
					m_nvEncoder->Release();
					m_nvEncoderCtx.deviceIndex	= m_nvDecoderCtx.deviceIndex;
					m_nvEncoderCtx.width		= nOutputWidth;
					m_nvEncoderCtx.height		= nOutputHeight;
					m_nvEncoderCtx.codec		= encoderCodec;
					m_nvEncoderCtx.profile		= ESMNvenc::AVC_PROFILE_T::BP;
					m_nvEncoderCtx.bitrate		= (GetExternalCamera() && ((nWidth * nHeight) > (1920 * 1080)))? (1024 * 1024 * 50) : (nOutputBitrate);
					if(m_bSetProperty && m_bLoadAdj)
						m_nvEncoderCtx.colorspace = ESMNvenc::COLORSPACE_T::YV12;
					else
						m_nvEncoderCtx.colorspace = ESMNvenc::COLORSPACE_T::NV12;
					m_nvEncoder->Initialize(&m_nvEncoderCtx);
					encoderExtradata = m_nvEncoder->GetExtradata(encoderExtradataSize);
#if defined(WITH_HEVC_TEST)  || defined(WITH_AVC_TEST)
					ESMRTSPServer* pRTSPServer = m_pParent->GetRTSPServer2();
					pRTSPServer->SetExtradata(encoderExtradata, encoderExtradataSize);
#endif
				}
				else
				{
					encoderExtradata = m_nvEncoder->GetExtradata(encoderExtradataSize);
				}
			}

#if defined(WITH_DRM)
			if(m_drmHandler==NULL)
			{
				m_drmHandler = new ESMDRMHandler();
				::memset(&m_drmHandlerCtx, 0x00, sizeof(m_drmHandlerCtx));
				m_drmHandlerCtx.type = ESMDRMHandler::DRM_TYPE_T::CARM_SCRAMBLER;
				strncpy_s(m_drmHandlerCtx.address, "192.168.11.51", strlen("192.168.11.51") + 1);
				m_drmHandlerCtx.portnumber = 7412;
				m_drmHandler->Initialize(&m_drmHandlerCtx);
			}
			else if(!m_drmHandler->IsInitialized())
			{
				::memset(&m_drmHandlerCtx, 0x00, sizeof(m_drmHandlerCtx));
				m_drmHandlerCtx.type = ESMDRMHandler::DRM_TYPE_T::CARM_SCRAMBLER;
				strncpy_s(m_drmHandlerCtx.address, "192.168.11.51", strlen("192.168.11.51") + 1);
				m_drmHandlerCtx.portnumber = 7412;
				m_drmHandler->Initialize(&m_drmHandlerCtx);
			}
#endif

			/////////////////////////////////////////////POSITION SWIPE/////////////////////////////////////////////
			std::shared_ptr<SEI_MESSAGE_T> ptrPositionSwipeSeiMessage;
			std::map<SEI_TYPE_T, std::shared_ptr<SEI_MESSAGE_T>>::iterator seiMsgIter = m_pParent->m_seiMessages.find(POSITION_SWIPE);
			if(seiMsgIter == m_pParent->m_seiMessages.end())
			{
				ptrPositionSwipeSeiMessage = std::shared_ptr<SEI_MESSAGE_T>(new SEI_MESSAGE_T());
				ptrPositionSwipeSeiMessage->seiType = POSITION_SWIPE;

				char *	positionSwipeMessage = NULL;
				char	startCode[4] = {0x00, 0x00, 0x00, 0x01};
				int		seiHeaderSize = 2; //nalu + seitype


				

				int positionSwipeElemCount	= sizeof(positionSwipeBodyElem)/sizeof(POSITION_SWIPE_BODY_ELEMENT_T);
				int positionSwipeHeaderSize	= sizeof(ISO_IEC_11578_POSITION_SWIPE) + 8;
				int positionSwipeElemSize	= 18;

				//CString tempLog;
				//tempLog.Format(_T("SEI Length = %d, %d, %d, %d"), sizeof(positionSwipeBodyElem), sizeof(POSITION_SWIPE_BODY_ELEMENT_T), sizeof(ISO_IEC_11578_POSITION_SWIPE), positionSwipeHeaderSize + positionSwipeElemCount * positionSwipeElemSize + 1);
				//SendLog(5, tempLog);


				int positionSwipeSize		= positionSwipeHeaderSize + positionSwipeElemCount * positionSwipeElemSize;
				int seiSizeByteCount		= positionSwipeSize/255;
				int seiSizeByteRemain		= positionSwipeSize%255;
				if(seiSizeByteRemain>0)
					seiSizeByteCount++;
				else if (seiSizeByteCount >= 1 && seiSizeByteRemain == 0)
					seiSizeByteCount++;
				seiHeaderSize += seiSizeByteCount;

				positionSwipeMessage = (char*)malloc(sizeof(startCode) + seiHeaderSize + positionSwipeSize + 1);
				POSITION_SWIPE_T positionSwipe = {0};
				positionSwipe.w	= 1920;
				positionSwipe.h	= 1080;
				positionSwipe.l	= 100;
				positionSwipe.count = positionSwipeElemCount;
				positionSwipe.w	= htons(positionSwipe.w);
				positionSwipe.h	= htons(positionSwipe.h);
				positionSwipe.l	= htons(positionSwipe.l);
				positionSwipe.count	= htons(positionSwipe.count);

				int positionSwipeIndex = 0;
				::memmove(&positionSwipeMessage[positionSwipeIndex], &startCode, sizeof(startCode));
				positionSwipeIndex += 4;
				positionSwipeMessage[positionSwipeIndex] = 0x06;//nalu type
				positionSwipeIndex++;
				positionSwipeMessage[positionSwipeIndex] = 0x05;//user data unregistered sei message
				positionSwipeIndex++;

				for(int i=0; i<seiSizeByteCount; i++)
				{
					if(i==(seiSizeByteCount-1))
					{
						if(seiSizeByteRemain>0)
						{
							positionSwipeMessage[positionSwipeIndex] = seiSizeByteRemain;
							positionSwipeIndex++;								
						}
						else
						{
							positionSwipeMessage[positionSwipeIndex] = 0xFF;
							positionSwipeIndex++;

							positionSwipeMessage[positionSwipeIndex] = 0x00;
							positionSwipeIndex++;
						}
					}
					else
					{
						positionSwipeMessage[positionSwipeIndex] = 0xFF;
						positionSwipeIndex++;
					}
				}
				::memmove(&positionSwipeMessage[positionSwipeIndex], ISO_IEC_11578_POSITION_SWIPE, sizeof(ISO_IEC_11578_POSITION_SWIPE));
				positionSwipeIndex += sizeof(ISO_IEC_11578_POSITION_SWIPE);
				::memmove(&positionSwipeMessage[positionSwipeIndex], &positionSwipe.w, sizeof(positionSwipe.w));
				positionSwipeIndex += 2;
				::memmove(&positionSwipeMessage[positionSwipeIndex], &positionSwipe.h, sizeof(positionSwipe.h));
				positionSwipeIndex += 2;
				::memmove(&positionSwipeMessage[positionSwipeIndex], &positionSwipe.l, sizeof(positionSwipe.l));
				positionSwipeIndex += 2;
				::memmove(&positionSwipeMessage[positionSwipeIndex], &positionSwipe.count, sizeof(positionSwipe.count));
				positionSwipeIndex += 2;

				for(int x=0; x<positionSwipeElemCount; x++)
				{
					POSITION_SWIPE_BODY_ELEMENT_T elem;
					elem.c  = positionSwipeBodyElem[x].c;
					elem.x1 = positionSwipeBodyElem[x].x1;
					elem.y1 = positionSwipeBodyElem[x].y1;
					elem.x2 = positionSwipeBodyElem[x].x2;
					elem.y2 = positionSwipeBodyElem[x].y2;
					elem.x3 = positionSwipeBodyElem[x].x3;
					elem.y3 = positionSwipeBodyElem[x].y3;
					elem.x4 = positionSwipeBodyElem[x].x4;
					elem.y4 = positionSwipeBodyElem[x].y4;
					elem.c	= htons(elem.c);
					elem.x1	= htons(elem.x1);
					elem.y1	= htons(elem.y1);
					elem.x2	= htons(elem.x2);
					elem.y2	= htons(elem.y2);
					elem.x3	= htons(elem.x3);
					elem.y3	= htons(elem.y3);
					elem.x4	= htons(elem.x4);
					elem.y4	= htons(elem.y4);

					::memmove(&positionSwipeMessage[positionSwipeIndex], &elem.c, sizeof(elem.c));
					positionSwipeIndex+= 2;
					::memmove(&positionSwipeMessage[positionSwipeIndex], &elem.x1, sizeof(elem.x1));
					positionSwipeIndex+= 2;
					::memmove(&positionSwipeMessage[positionSwipeIndex], &elem.y1, sizeof(elem.y1));
					positionSwipeIndex+= 2;
					::memmove(&positionSwipeMessage[positionSwipeIndex], &elem.x2, sizeof(elem.x2));
					positionSwipeIndex+= 2;
					::memmove(&positionSwipeMessage[positionSwipeIndex], &elem.y2, sizeof(elem.y2));
					positionSwipeIndex+= 2;
					::memmove(&positionSwipeMessage[positionSwipeIndex], &elem.x3, sizeof(elem.x3));
					positionSwipeIndex+= 2;
					::memmove(&positionSwipeMessage[positionSwipeIndex], &elem.y3, sizeof(elem.y3));
					positionSwipeIndex+= 2;
					::memmove(&positionSwipeMessage[positionSwipeIndex], &elem.x4, sizeof(elem.x4));
					positionSwipeIndex+= 2;
					::memmove(&positionSwipeMessage[positionSwipeIndex], &elem.y4, sizeof(elem.y4));
					positionSwipeIndex+= 2;
				}

				positionSwipeMessage[positionSwipeIndex] = 0x80; // rbsp stop one bit and trailing bits
                positionSwipeIndex += 1;

				ptrPositionSwipeSeiMessage->seiMessage = positionSwipeMessage;
				ptrPositionSwipeSeiMessage->seiMessageLength = sizeof(startCode) + seiHeaderSize + positionSwipeSize + 1 ; //positionSwipeIndex

				m_pParent->m_seiMessages.insert(std::make_pair(POSITION_SWIPE, ptrPositionSwipeSeiMessage));
			}
			else
			{
				ptrPositionSwipeSeiMessage = seiMsgIter->second;
			}
			/////////////////////////////////////////////End of POSITION SWIPE/////////////////////////////////////////////

			/////////////////////////////////////////////MP4 Muxer/////////////////////////////////////////////
#if defined(WITH_OUTPUT_MUXER)
			static BOOL bMuxerConfSetup = FALSE;
			BOOL bNeedSetup = FALSE;
			if(!bMuxerConfSetup)
			{
				bNeedSetup = TRUE;
				bMuxerConfSetup = TRUE;
			}
			else
			{
				if ((ESMFFMuxer::CONTEXT_T::Instance().videoExtradataSize != encoderExtradataSize) || 
					::memcmp(ESMFFMuxer::CONTEXT_T::Instance().videoExtradata, encoderExtradata, ESMFFMuxer::CONTEXT_T::Instance().videoExtradataSize))
				{
					bNeedSetup = TRUE;
					bMuxerConfSetup = TRUE;
				}
			}
			
			if(bNeedSetup)
			{
				switch(m_nvEncoderCtx.codec)
				{
				case ESMNvenc::CODEC_T::AVC :
					ESMFFMuxer::CONTEXT_T::Instance().videoCodec = ESMFFMuxer::VIDEO_CODEC_T::AVC;
					break;
				case ESMNvenc::CODEC_T::HEVC :
					ESMFFMuxer::CONTEXT_T::Instance().videoCodec = ESMFFMuxer::VIDEO_CODEC_T::HEVC;
					break;
				}

				CString muxDstPath;
				muxDstPath.Format(L"F:\\RecordSave\\Record\\Movie\\files\\%s\\out", this->m_str4DM);
				::wcscpy_s(ESMFFMuxer::CONTEXT_T::Instance().muxTmpPath, L"M:");
				::wcscpy_s(ESMFFMuxer::CONTEXT_T::Instance().muxDstPath, muxDstPath);
				::wcscpy_s(ESMFFMuxer::CONTEXT_T::Instance().muxPrefix, m_pParent->m_pRTSPServerMgr->GetDSC());

				ESMFFMuxer::CONTEXT_T::Instance().onlyOnceAtFirstSeiMessages.push_back(ptrPositionSwipeSeiMessage->seiMessage);
				ESMFFMuxer::CONTEXT_T::Instance().onlyOnceAtFirstSeiMessageLength.push_back(ptrPositionSwipeSeiMessage->seiMessageLength);

				ESMFFMuxer::CONTEXT_T::Instance().videoExtradataSize = encoderExtradataSize;
				if(ESMFFMuxer::CONTEXT_T::Instance().videoExtradataSize>0)
				{
					::memmove(ESMFFMuxer::CONTEXT_T::Instance().videoExtradata, encoderExtradata, ESMFFMuxer::CONTEXT_T::Instance().videoExtradataSize);
				}
				ESMFFMuxer::CONTEXT_T::Instance().videoWidth	= nOutputWidth;
				ESMFFMuxer::CONTEXT_T::Instance().videoHeight	= nOutputHeight;
				ESMFFMuxer::CONTEXT_T::Instance().videoFps		= round(outputFps);
				ESMFFMuxer::CONTEXT_T::Instance().option		|= ESMFFMuxer::MEDIA_TYPE_T::VIDEO;
				ESMFFMuxer::CONTEXT_T::Instance().singleContainer= FALSE;
				if(ESMFFMuxer::Instance().IsInitialized())
				{
					ESMFFMuxer::Instance().Release();
					ESMFFMuxer::Instance().Initialize(&ESMFFMuxer::CONTEXT_T::Instance());
				}
			}
#endif
			/////////////////////////////////////////////End of MP4 Muxer/////////////////////////////////////////////

			long long pts = 0;//av_frame_get_best_effort_timestamp(pFrame);
			if(m_nvRotator)
			{
				m_nvEncoder->Encode(dpRotatedYUVFrame, dpRotatedYUVPitch, pts, info->bitstream, info->bitstreamCapacity, info->bitstreamSize, info->bitstreamTimestamp);
			}
			else
			{
				if(m_bSetProperty && m_bLoadAdj)
				{
					if(nWidth!=nOutputWidth || nHeight!=nOutputHeight)
					{
						::EnterCriticalSection(&m_criAdjust);
						m_nvEncoder->Encode(dpResizedYUVFrame, dpResizedYUVPitch, pts, info->bitstream, info->bitstreamCapacity, info->bitstreamSize, info->bitstreamTimestamp);
						::LeaveCriticalSection(&m_criAdjust);
					}
					else
					{
						m_nvEncoder->Encode(dpYUVFrame, dpYUVPitch, pts, info->bitstream, info->bitstreamCapacity, info->bitstreamSize, info->bitstreamTimestamp);
					}
				}
				else
				{
					m_nvEncoder->Encode(dpYUVFrame, dpYUVPitch, pts, info->bitstream, info->bitstreamCapacity, info->bitstreamSize, info->bitstreamTimestamp);
				}
			}
			if(info->bitstreamSize>0)
			{
#if defined(WITH_HEVC_TEST)  || defined(WITH_AVC_TEST)
				ESMRTSPServer * pRTSPServer = m_pParent->GetRTSPServer2();
				if (pRTSPServer)
				{
					long long duration = 0;
					duration = /*m_nFrameNumber **/ (1000000 / round(outputFps));

#if 0
					uint8_t * finalBitstream = (uint8_t*)malloc(info->bitstreamSize + ptrPositionSwipeSeiMessage->seiMessageLength);
					memmove(finalBitstream, ptrPositionSwipeSeiMessage->seiMessage, ptrPositionSwipeSeiMessage->seiMessageLength);
					memmove(finalBitstream + ptrPositionSwipeSeiMessage->seiMessageLength, info->bitstream, info->bitstreamSize);
					pRTSPServer->PushVideo(finalBitstream, info->bitstreamSize + ptrPositionSwipeSeiMessage->seiMessageLength, duration);
					free(finalBitstream);
#else
#if 1
					pRTSPServer->PushVideo(info->bitstream, info->bitstreamSize, 0);
					pRTSPServer->PushVideo((unsigned char*)ptrPositionSwipeSeiMessage->seiMessage, ptrPositionSwipeSeiMessage->seiMessageLength, 0);
#else
					pRTSPServer->PushVideo(encoderExtradata, encoderExtradataSize, 0);
					pRTSPServer->PushVideo((const unsigned char*)ptrPositionSwipeSeiMessage->seiMessage, ptrPositionSwipeSeiMessage->seiMessageLength, 0);
					pRTSPServer->PushVideo(info->bitstream, info->bitstreamSize, 0);
#endif
#endif

				}
#endif
				_snprintf_s(debug, MAX_PATH, "############################# %s[encodeCount] : %d\n", __FILE__, encodeCount);
				//::OutputDebugStringA(debug);
				encodeCount++;

#if defined(WITH_OUTPUT_MUXER)
				if(!ESMFFMuxer::Instance().IsInitialized())
					ESMFFMuxer::Instance().Initialize(&ESMFFMuxer::CONTEXT_T::Instance());
				ESMFFMuxer::Instance().PutVideoStreamByFrameCount(info->bitstream, info->bitstreamSize, info->nSecIdx, info->nFrameIdx);
#endif

#if defined(WITH_DRM)
				unsigned char * encrypted = NULL;
				int encryptedLength = 0;
				if(m_drmHandler)
				{
					m_drmHandler->Encrypt(info->bitstream, info->bitstreamSize, &encrypted, encryptedLength);
				}

				std::shared_ptr<BITSTREAM_INFO> vinfo = NULL;
				if(m_drmHandler && encrypted && encryptedLength>0)
				{
					vinfo = std::shared_ptr<BITSTREAM_INFO>(new BITSTREAM_INFO(encryptedLength));
				}
				else
				{
					vinfo = std::shared_ptr<BITSTREAM_INFO>(new BITSTREAM_INFO(info->bitstreamSize));
				}
#else
				std::shared_ptr<BITSTREAM_INFO> vinfo = std::shared_ptr<BITSTREAM_INFO>(new BITSTREAM_INFO(info->bitstreamSize));
#endif
				
				vinfo->codec = info->nCodec;
				vinfo->extradataSize = ESMFFMuxer::CONTEXT_T::Instance().videoExtradataSize;
				if(ESMFFMuxer::CONTEXT_T::Instance().videoExtradataSize>0)
					::memmove(vinfo->extradata, (char*)ESMFFMuxer::CONTEXT_T::Instance().videoExtradata, ESMFFMuxer::CONTEXT_T::Instance().videoExtradataSize);
				vinfo->width = info->nWidth;
				vinfo->height = info->nHeight;
				vinfo->fps = info->nFps;

#if defined(WITH_DRM)
				if(m_drmHandler && encrypted && encryptedLength>0)
				{
					::memmove(vinfo->bitstream, encrypted, encryptedLength);
					vinfo->bitstreamSize = encryptedLength;
				}
				else
				{
					::memmove(vinfo->bitstream, info->bitstream, info->bitstreamSize);
					vinfo->bitstreamSize = info->bitstreamSize;
				}
#else
				::memmove(vinfo->bitstream, info->bitstream, info->bitstreamSize);
				vinfo->bitstreamSize = info->bitstreamSize;
#endif
				vinfo->duration = -1;
				vinfo->nSecIdx = info->nSecIdx;
				vinfo->nFrameIdx = info->nFrameIdx;
				vinfo->nStopIdx = info->nSecIdx+1;
				vinfo->nGap = info->nGap;
				vinfo->seiMessages = m_pParent->m_seiMessages;
				if(CESMMovieRTMovie::GetInstance()) {
					CESMMovieRTMovie::CESMMovieRTRun * RTRun =  CESMMovieRTMovie::GetInstance()->GetESMMovieRun(m_str4DM);
					if (RTRun) {
						if (info->nSecIdx == -1 ) {
							SendLog(5,_T("[Transcoder] - invalid SecIndex, FrameNumber"));
						}
						else {
							str4DM = RTRun->m_str4DM;
							vinfo->str4DM.Format(_T("%s"), str4DM);
							vinfo->strInit4DM.Format(_T("%s"), strInit4DM);
							RTRun->PutVideoBitstream(vinfo);
						}
					}
				}
			}
		}
/*
		if ( nGap  > m_pParent->m_nFrameRate*2 )  // 2 sec
			break;

	} while (--nGap > 0);
*/
	//::LeaveCriticalSection(&m_criProcess);
	return 0;
}

void CESMMovieRTMovie::CESMMovieRTRun::LoadAdjustData(CString strDSC, int nWidth, int nHeight)
{
	m_bSetProperty = TRUE;
	SendLog(5,_T("[ESMMovieRun] - Set Property Finish"));

	stAdjustInfo adjinfo = m_pParent->GetAdjustData(strDSC);
	
	/*
	adjinfo.AdjAngle = -89.0452;
	adjinfo.AdjSize = 0.8273;
	adjinfo.AdjMove.x = -2.0000;
	adjinfo.AdjMove.y = 17.9153;
	adjinfo.AdjptRotate.x = 962.0000;
	adjinfo.AdjptRotate.y = 516.0000;
	m_pParent->m_nMarginX = 195;
	m_pParent->m_nMarginY = 110;
	adjinfo.stMargin.nMarginX = 195;
	adjinfo.stMargin.nMarginY = 110;
	adjinfo.rtMargin = CRect(213,120,1724,970);

	adjinfo.nWidth = 3840;
	adjinfo.nHeight = 2160;
	*/
	//adjinfo.AdjptRotate.x = 0.1;
	//adjinfo.AdjptRotate.y = 0.1;
	if(adjinfo.AdjptRotate.x == 0 || adjinfo.AdjptRotate.y == 0)
	{
		if(m_pParent->m_bUpdateAdj)
			m_pParent->m_bUpdateAdj = FALSE;
		return;
	}

	//Create
	SetYAdjustData(adjinfo,nWidth,nHeight,1);
	SetUVAdjustData(adjinfo,nWidth/2,nHeight/2,0.5);

	m_bLoadAdj = TRUE;
	if(m_pParent->m_bUpdateAdj)
		m_pParent->m_bUpdateAdj = FALSE;
	SendLog(5,_T("[ESMMovieRun] - Set Adjust Finish"));
}

int CESMMovieRTMovie::CESMMovieRTRun::Round(double dData)
{
	int nResult = 0;

	if( dData > 0.0)
		nResult = int(dData + 0.5);
	else if( dData < 0.0)
		nResult = int(dData - 0.5);
	else
		nResult = 0;

	return nResult;
}
void CESMMovieRTMovie::CESMMovieRTRun::SetYAdjustData(stAdjustInfo AdjustData,int nWidth,int nHeight,double dRatio,BOOL bLoad /* = TRUE */)
{
	/*
	char debug[MAX_PATH] = {0};
	_snprintf_s(debug, MAX_PATH, " nWidth=%d, nHeight=%d, dRatio=%f, %f, %f, %f, %f, %f, %f, %f, %f, %d, %d, %d, %d, %d, %d, %d, %d\n", 
						nWidth, nHeight, dRatio,
						AdjustData.AdjMove.x, AdjustData.AdjMove.y,
						AdjustData.AdjptRotate.x, AdjustData.AdjptRotate.y,
						AdjustData.AdjptRotate2.x, AdjustData.AdjptRotate2.y,
						AdjustData.AdjAngle, AdjustData.AdjSize,
						AdjustData.nWidth, AdjustData.nHeight,
						AdjustData.stMargin.nMarginX, AdjustData.stMargin.nMarginY,
						AdjustData.rtMargin.left, AdjustData.rtMargin.right, AdjustData.rtMargin.top, AdjustData.rtMargin.bottom);
	::OutputDebugStringW(L"###########################SetYAdjustData##########################\n");
	::OutputDebugStringW(AdjustData.strDSC);
	::OutputDebugStringA(debug);
	*/

	double dbScale = AdjustData.AdjSize;

	double degree = AdjustData.AdjAngle;

	double dbRotX = Round(AdjustData.AdjptRotate.x * dRatio);
	double dbRotY = Round(AdjustData.AdjptRotate.y * dRatio);
	double dbMovX = Round(AdjustData.AdjMove.x * dRatio);
	double dbMovY = Round(AdjustData.AdjMove.y * dRatio);

	int nMarginX = (int) (m_pParent->m_nMarginX * dRatio);
	int nMarginY = (int) (m_pParent->m_nMarginY * dRatio);

	if(bLoad == FALSE)
	{
		dbScale = 1.0;
		degree = -90.0;
		dbRotX = nWidth/2;
		dbRotY = nHeight/2;
		dbMovX = 0.0;
		dbMovY = 0.0;
		nMarginX = 0;
		nMarginY = 0;
	}
	double dbAngleAdjust = -1*(degree+90);

	Mat RotMat(2,3,CV_64FC1);
	RotMat = getRotationMatrix2D(Point2f((float)dbRotX,(float)dbRotY),dbAngleAdjust,dbScale);

	RotMat.at<double>(0,2) += (dbMovX/* / dbScale*/);
	RotMat.at<double>(1,2) += (dbMovY/* / dbScale*/);

	double dbMarginScale = 1 / ((double)(nWidth - nMarginX * 2)/ nWidth);

	if(AdjustData.rtMargin.left == 0 && AdjustData.rtMargin.top == 0
		&& AdjustData.rtMargin.right == 0 && AdjustData.rtMargin.bottom == 0)
	{
		m_matYAdj = RotMat.clone();
		//m_gpuMatYAdj.upload(m_matYAdj);
		m_nYWidth = nWidth;
		m_nYHeight = nHeight;
		m_szYRe.width = (int) (nWidth * dbMarginScale);
		m_szYRe.height = (int) (nHeight * dbMarginScale);
		m_nYLeft = (int) ((nWidth  * dbMarginScale - nWidth)/2);
		m_nYTop  = (int) ((nHeight * dbMarginScale - nHeight)/2);
	}
	else
	{
		m_matYAdj = RotMat.clone();
		//m_gpuMatYAdj.upload(m_matYAdj);
		m_nYWidth = nWidth;
		m_nYHeight = nHeight;
		m_szYRe.width  = AdjustData.rtMargin.right - AdjustData.rtMargin.left + 1;
		m_szYRe.height = AdjustData.rtMargin.bottom - AdjustData.rtMargin.top + 1;
		m_nYLeft = AdjustData.rtMargin.left;
		m_nYTop  = AdjustData.rtMargin.top;
		m_bUseRect = TRUE;
	}


	m_nYSize = nWidth * nHeight;
	CString str;
	str.Format(_T("%d - %d - %d - %d"),m_nYLeft,m_nYTop,m_szYRe.width,m_szYRe.height);
	SendLog(5,str);
	/*CString strLog,strTemp;
	for(int i = 0 ; i < RotMat.rows; i++)
		for(int j = 0 ; j < RotMat.cols ; j++)
		{
			strTemp.Format(_T(".2f% "),RotMat.at<double>(i,j));
			strLog.Append(strTemp);
		}

	SendLog(5,strLog);*/
}
void CESMMovieRTMovie::CESMMovieRTRun::SetUVAdjustData(stAdjustInfo AdjustData,int nWidth,int nHeight,double dRatio,BOOL bLoad /* = TRUE */)
{
	/*
	char debug[MAX_PATH] = {0};
	_snprintf_s(debug, MAX_PATH, " nWidth=%d, nHeight=%d, dRatio=%f, %f, %f, %f, %f, %f, %f, %f, %f, %d, %d, %d, %d, %d, %d, %d, %d\n", 
						nWidth, nHeight, dRatio,
						AdjustData.AdjMove.x, AdjustData.AdjMove.y,
						AdjustData.AdjptRotate.x, AdjustData.AdjptRotate.y,
						AdjustData.AdjptRotate2.x, AdjustData.AdjptRotate2.y,
						AdjustData.AdjAngle, AdjustData.AdjSize,
						AdjustData.nWidth, AdjustData.nHeight,
						AdjustData.stMargin.nMarginX, AdjustData.stMargin.nMarginY,
						AdjustData.rtMargin.left, AdjustData.rtMargin.right, AdjustData.rtMargin.top, AdjustData.rtMargin.bottom);
	::OutputDebugStringW(L"###########################SetUVAdjustData##########################\n");
	::OutputDebugStringW(AdjustData.strDSC);
	::OutputDebugStringA(debug);
	*/

	double dbScale = AdjustData.AdjSize;

	double degree = AdjustData.AdjAngle;

	double dbRotX = Round(AdjustData.AdjptRotate.x * dRatio);
	double dbRotY = Round(AdjustData.AdjptRotate.y * dRatio);
	double dbMovX = Round(AdjustData.AdjMove.x * dRatio);
	double dbMovY = Round(AdjustData.AdjMove.y * dRatio);

	int nMarginX = (int) (m_pParent->m_nMarginX * dRatio);
	int nMarginY = (int) (m_pParent->m_nMarginY * dRatio);

	if(bLoad == FALSE)
	{
		dbScale = 1.0;
		degree = -90.0;
		dbRotX = nWidth/2;
		dbRotY = nHeight/2;
		dbMovX = 0.0;
		dbMovY = 0.0;
		nMarginX = 0;
		nMarginY = 0;
	}
	double dbAngleAdjust = -1*(degree+90);

	Mat RotMat(2,3,CV_64FC1);
	RotMat = getRotationMatrix2D(Point2f((float)dbRotX,(float)dbRotY),dbAngleAdjust,dbScale);

	RotMat.at<double>(0,2) += (dbMovX /*/ dbScale*/);
	RotMat.at<double>(1,2) += (dbMovY /*/ dbScale*/);

	double dbMarginScale = 1 / ((double)(nWidth - nMarginX * 2)/ nWidth);

	if(AdjustData.rtMargin.left == 0 && AdjustData.rtMargin.top == 0
		&& AdjustData.rtMargin.right == 0 && AdjustData.rtMargin.bottom == 0)
	{
		m_matUVAdj = RotMat.clone();
		//m_gpuMatUVAdj.upload(m_matUVAdj);
		m_nUVWidth = nWidth;
		m_nUVHeight = nHeight;
		m_szUVRe.width = (int) (nWidth * dbMarginScale);
		m_szUVRe.height = (int) (nHeight * dbMarginScale);
		m_nUVLeft = (int) ((nWidth  * dbMarginScale - nWidth)/2);
		m_nUVTop  = (int) ((nHeight * dbMarginScale - nHeight)/2);
	}
	else
	{
		m_matUVAdj = RotMat.clone();
		//m_gpuMatUVAdj.upload(m_matUVAdj);
		m_nUVWidth = nWidth;
		m_nUVHeight = nHeight;
		m_szUVRe.width  = ((AdjustData.rtMargin.right - AdjustData.rtMargin.left)+1)/2;
		m_szUVRe.height = ((AdjustData.rtMargin.bottom - AdjustData.rtMargin.top)+1)/2;
		m_nUVLeft = AdjustData.rtMargin.left/2;
		m_nUVTop  = AdjustData.rtMargin.top/2;
		m_bUseRect = TRUE;
	}
	CString str;
	str.Format(_T("%d - %d - %d - %d"),m_nUVLeft,m_nUVTop,m_szUVRe.width,m_szUVRe.height);
	SendLog(5,str);
	m_nUVSize = nWidth * nHeight;
}

BOOL CESMMovieRTMovie::CESMMovieRTRun::ImageProcessFrame(std::shared_ptr<FRAME_INFO> info)
{
	int nInterFlag = CV_INTER_LINEAR;
	int nHDWidth = ENCODE_HD_WIDTH,nHDHeight = ENCODE_HD_HEIGHT;

	cv::Size szY_HD;
	szY_HD.width = nHDWidth;
	szY_HD.height = nHDHeight;

	cv::Size szUV_HD;
	szUV_HD.width = nHDWidth/2;
	szUV_HD.height = nHDHeight/2;

	cv::cuda::GpuMat gpuY, gpuYRot, gpuYRe, gpuYRe2;
	cv::cuda::GpuMat gpuU, gpuURot, gpuURe, gpuURe2;
	cv::cuda::GpuMat gpuV, gpuVRot, gpuVRe, gpuVRe2;
	//cv::cuda::GpuMat gpuHDY,gpuHDU,gpuHDV;

	CString strLog;
	if(m_bUseRect)
	{
		gpuY = info->gpuY.clone();
		cv::cuda::warpAffine(gpuY, gpuYRot, m_matYAdj, cv::Size(m_nYWidth,m_nYHeight), nInterFlag);
		cv::cuda::GpuMat gpuYResult = (gpuYRot) (cv::Rect(m_nYLeft, m_nYTop, m_szYRe.width, m_szYRe.height));
		cv::cuda::resize(gpuYResult, gpuYRe, cv::Size(m_nYWidth, m_nYHeight), 0, 0, nInterFlag);
		//cv::cuda::resize(gpuYRe, gpuHDY, szY_HD, 0, 0, nInterFlag);
		gpuYRe.copyTo(info->gpuY);

		gpuU = info->gpuU.clone();
		cv::cuda::warpAffine(gpuU, gpuURot, m_matUVAdj, cv::Size(m_nUVWidth, m_nUVHeight), nInterFlag);
		cv::cuda::GpuMat gpuUResult = (gpuURot) (cv::Rect(m_nUVLeft, m_nUVTop, m_szUVRe.width, m_szUVRe.height));
		cv::cuda::resize(gpuUResult, gpuURe, cv::Size(m_nUVWidth, m_nUVHeight), 0, 0, nInterFlag);
		//cv::cuda::resize(gpuURe, gpuHDU, szUV_HD, 0, 0, nInterFlag);
		gpuURe.copyTo(info->gpuU);

		gpuV = info->gpuV.clone();
		cv::cuda::warpAffine(gpuV, gpuVRot, m_matUVAdj, cv::Size(m_nUVWidth, m_nUVHeight), nInterFlag);
		cv::cuda::GpuMat gpuVResult = (gpuVRot) (cv::Rect(m_nUVLeft, m_nUVTop, m_szUVRe.width, m_szUVRe.height));
		cv::cuda::resize(gpuVResult, gpuVRe, cv::Size(m_nUVWidth, m_nUVHeight), 0, 0, nInterFlag);
		//cv::cuda::resize(gpuVRe, gpuHDV, szUV_HD, 0, 0, nInterFlag);
		gpuVRe.copyTo(info->gpuV);
	}
	else
	{
		gpuY = info->gpuY.clone();
		cv::cuda::warpAffine(gpuY, gpuYRot, m_matYAdj, cv::Size(m_nYWidth, m_nYHeight), nInterFlag);
		cv::cuda::resize(gpuYRot, gpuYRe, m_szYRe, 0, 0, nInterFlag);
		cv::cuda::GpuMat gpuYResult = (gpuYRe) (cv::Rect(m_nYLeft, m_nYTop, m_nYWidth, m_nYHeight));
		//cv::cuda::resize(gpuYResult, gpuHDY, szY_HD, 0, 0, nInterFlag);
		gpuYResult.copyTo(info->gpuY);

		//cv::cuda::rotate(gpuYResult, gpuYRe2, cv::Size(m_nYWidth, m_nYHeight), 180);
		//gpuYRe2.copyTo(info->gpuY);

		gpuU = info->gpuU.clone();
		cv::cuda::warpAffine(gpuU, gpuURot, m_matUVAdj, cv::Size(m_nUVWidth, m_nUVHeight), nInterFlag);
		cv::cuda::resize(gpuURot, gpuURe, m_szUVRe, 0, 0, nInterFlag);
		cv::cuda::GpuMat gpuUResult = (gpuURe) (cv::Rect(m_nUVLeft, m_nUVTop, m_nUVWidth, m_nUVHeight));
		//cv::cuda::resize(gpuUResult, gpuHDU, szUV_HD, 0, 0, nInterFlag);
		gpuUResult.copyTo(info->gpuU);

		//cv::cuda::rotate(gpuUResult, gpuURe2, cv::Size(m_nUVWidth, m_nUVHeight), 180);
		//gpuURe2.copyTo(info->gpuU);

		gpuV = info->gpuV.clone();
		cv::cuda::warpAffine(gpuV, gpuVRot, m_matUVAdj, cv::Size(m_nUVWidth, m_nUVHeight), nInterFlag);
		cv::cuda::resize(gpuVRot, gpuVRe, m_szUVRe, 0, 0, nInterFlag);
		cv::cuda::GpuMat gpuVResult = (gpuVRe) (cv::Rect(m_nUVLeft, m_nUVTop, m_nUVWidth, m_nUVHeight));
		//cv::cuda::resize(gpuVResult, gpuHDV, szUV_HD, 0, 0, nInterFlag);
		gpuVResult.copyTo(info->gpuV);

		//cv::cuda::rotate(gpuVResult, gpuVRe2, cv::Size(m_nUVWidth, m_nUVHeight), 180);
		//gpuVRe2.copyTo(info->gpuV);
	}

	gpuU.release();
	gpuURot.release();
	gpuURe.release();

	gpuV.release();
	gpuVRot.release();
	gpuVRe.release();

	gpuY.release();
	gpuYRot.release();
	gpuYRe.release();
	//gpuHDY.release();
	//gpuHDU.release();
	//gpuHDV.release();
	return TRUE;
}

BOOL CESMMovieRTMovie::CESMMovieRTRun::GetWaitingTimeFinish()
{
	return m_bWaitingTimeRun;
}

void CESMMovieRTMovie::CESMMovieRTRun::SetWaitingTimeFinish(BOOL b)
{
	m_bWaitingTimeRun = b;
}


void CESMMovieRTMovie::CESMMovieRTRun::SendRTSPStateMessage(int nState,int nData)
{
	//190320 hjcho - LGU+ AO½A AUμa
	int nIdx = m_pParent->GetDSCIndex()/* - 30*/;
	/*if(nIdx != 60)
	{
		nIdx = 60 - nIdx;
	}*/
	CString* pstr4DM = new CString;
	pstr4DM->Format(_T("%s"),m_str4DM);

	ESMEvent* pEvent = new ESMEvent;
	pEvent->message  = WM_ESM_NET_RTSP_STATE_CHECK;
	pEvent->nParam1  = nState;
	pEvent->nParam2  = nIdx/*m_pParent->GetDSCIndex()*/;
	pEvent->nParam3  = nData;
	pEvent->pParam	 = (LPARAM)pstr4DM;
	//::SendMessage(m_pParent->m_pWnd->m_hWnd , WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pEvent);
	::SendMessage(m_pParent->m_pWnd->m_hWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pEvent);
}

void  CESMMovieRTMovie::RTPInfoData_Callback(CString str4DM, int rtp_frmNum)
{
	double outputFps = CESMMovieRTMovie::GetInstance()->m_nLive_framerate;
	int encodeCycle =  CESMMovieRTMovie::GetInstance()->m_nFrameRate/outputFps;
	if(encodeCycle<1)
		encodeCycle = 1;
 
	if(CESMMovieRTMovie::GetInstance()) {	 
		CESMMovieRTMovie::CESMMovieRTRun * RTRun =  CESMMovieRTMovie::GetInstance()->GetESMMovieRun(str4DM);			
		if (RTRun) { 
			std::shared_ptr<BITSTREAM_INFO> info = std::shared_ptr<BITSTREAM_INFO>(new BITSTREAM_INFO(1));
			info->nType = RTSP_DATA_SIGNAL;
			info->str4DM.Format(_T("%s"), str4DM);
			info->nFrameIdx = rtp_frmNum / encodeCycle;
			RTRun->PutVideoBitstream(info);
		}
	} 
}

void CESMMovieRTMovie::RTPVideoData_Callback(AVPacket* pPk, int codec, char * extradata, int extradataSize, int width, int height, double fps, int tsnum, int tsden, int secIndex, int rtp_frmNum, int ts, CString str4DM, int gap)
{
	int nSecIdx = 0;
	CString strInit4DM=_T("");
	CRTSPServerMgr* pRTSPServer = GetInstance()->GetRTSPServer();
	if(GetInstance()->GetLiveStreaming()==TRUE && pRTSPServer)
	{
		if(pRTSPServer->GetExternalCamera() == FALSE) 
		{
			if(CESMMovieRTMovie::GetInstance())
			{
				CESMMovieRTMovie::CESMMovieRTRun * RTRun =  CESMMovieRTMovie::GetInstance()->GetESMMovieRun(str4DM);
				RTRun->TranscodeAndProcessImage(codec, width, height, round(fps), pPk->data, pPk->size, str4DM, strInit4DM, secIndex, rtp_frmNum, ts, gap);
			}
		}
		else
		{
			if ( (width * height) > (1920 * 1080) ) 
			{  // 360 VR Camera
				if(CESMMovieRTMovie::GetInstance()) {					 
					CESMMovieRTMovie::CESMMovieRTRun * RTRun =  CESMMovieRTMovie::GetInstance()->GetESMMovieRun(str4DM);
					RTRun->TranscodeAndProcessImage(codec, width, height, round(fps), pPk->data, pPk->size, str4DM, strInit4DM, secIndex, rtp_frmNum, ts, gap);
				}
			}
			else 
			{
				BOOL bNeedSetup = FALSE;
				if(ESMFFMuxer::CONTEXT_T::Instance().videoExtradataSize != extradataSize)
					bNeedSetup = TRUE;
				else if((ESMFFMuxer::CONTEXT_T::Instance().videoExtradataSize > 0) && (extradataSize > 0))
				{
					if(::memcmp(ESMFFMuxer::CONTEXT_T::Instance().videoExtradata, extradata, ESMFFMuxer::CONTEXT_T::Instance().videoExtradataSize))
						bNeedSetup = TRUE;
				}
				if(bNeedSetup)
				{
					switch(codec)
					{
					case AV_CODEC_ID_H264 :
						ESMFFMuxer::CONTEXT_T::Instance().videoCodec = ESMFFMuxer::VIDEO_CODEC_T::AVC;
						break;
					case AV_CODEC_ID_HEVC :
						ESMFFMuxer::CONTEXT_T::Instance().videoCodec = ESMFFMuxer::VIDEO_CODEC_T::HEVC;
						break;
					}
					ESMFFMuxer::CONTEXT_T::Instance().videoExtradataSize = extradataSize;
					if(ESMFFMuxer::CONTEXT_T::Instance().videoExtradataSize>0)
						::memmove(ESMFFMuxer::CONTEXT_T::Instance().videoExtradata, extradata, ESMFFMuxer::CONTEXT_T::Instance().videoExtradataSize);
					ESMFFMuxer::CONTEXT_T::Instance().videoWidth = width;
					ESMFFMuxer::CONTEXT_T::Instance().videoHeight = height;
					ESMFFMuxer::CONTEXT_T::Instance().videoFps = round(fps);
					ESMFFMuxer::CONTEXT_T::Instance().option |= ESMFFMuxer::MEDIA_TYPE_T::VIDEO;
//					ESMFFMuxer::CONTEXT_T::Instance().genFrameNumber = TRUE;
				}
				
				
				//pRTSPServer->PutBitstream(codec, extradata, extradataSize, width, height, round(fps), pPk->data, pPk->size, pPk->duration, str4DM, strInit4DM, nSecIdx, rtp_frmNum, nSecIdx+1, gap);
				std::shared_ptr<BITSTREAM_INFO> info = std::shared_ptr<BITSTREAM_INFO>(new BITSTREAM_INFO(pPk->size));
				info->codec = codec;
				info->extradataSize = extradataSize;
				if(info->extradataSize>0)
					::memmove(info->extradata, extradata, info->extradataSize);
				info->width = width;
				info->height = height;
				info->fps = fps;
				::memmove(info->bitstream, pPk->data, pPk->size);
				info->bitstreamSize = pPk->size;
				info->duration = pPk->duration;
				info->nSecIdx = nSecIdx;
				info->nFrameIdx = rtp_frmNum;
				info->nStopIdx = nSecIdx+1;
				info->nGap = gap;
				if(CESMMovieRTMovie::GetInstance()) {					 
					CESMMovieRTMovie::CESMMovieRTRun * RTRun =  CESMMovieRTMovie::GetInstance()->GetESMMovieRun(str4DM);			
					if (RTRun) {
						str4DM = RTRun->m_str4DM;
						info->str4DM.Format(_T("%s"), str4DM);
						info->strInit4DM.Format(_T("%s"), strInit4DM);
						RTRun->PutVideoBitstream(info);
					}
				}
			}
		}	
	}
}

BOOL bRecvFirstSample = FALSE;
void CESMMovieRTMovie::RTPAudioData_Callback(AVPacket* pPk, int codec, char * extradata, int extradataSize, int samplerate, int channels, int sampleFormat, int tsnum, int tsden, CString str4DM, int gap)
{
	int nSecIdx = 0;
	CRTSPServerMgr* pRTSPServer = GetInstance()->GetRTSPServer();
	if(GetInstance()->GetLiveStreaming()==TRUE && pRTSPServer)
	{
		BOOL bNeedSetup = FALSE;
		if(ESMFFMuxer::CONTEXT_T::Instance().audioExtradataSize != extradataSize)
			bNeedSetup = TRUE;
		else if((ESMFFMuxer::CONTEXT_T::Instance().audioExtradataSize > 0) && (extradataSize > 0))
		{
			if(::memcmp(ESMFFMuxer::CONTEXT_T::Instance().audioExtradata, extradata, ESMFFMuxer::CONTEXT_T::Instance().audioExtradataSize))
				bNeedSetup = TRUE;
		}
		else
		{
			if(!bRecvFirstSample)
			{
				bNeedSetup = TRUE;
				bRecvFirstSample = TRUE;
			}
		}
		if(bNeedSetup)
		{
			switch(codec)
			{
			case AV_CODEC_ID_AAC :
				ESMFFMuxer::CONTEXT_T::Instance().videoCodec = ESMFFMuxer::AUDIO_CODEC_T::AAC;
				break;
			case AV_CODEC_ID_MP3 :
				ESMFFMuxer::CONTEXT_T::Instance().videoCodec = ESMFFMuxer::AUDIO_CODEC_T::MP3;
				break;
			case AV_CODEC_ID_AC3 :
				ESMFFMuxer::CONTEXT_T::Instance().videoCodec = ESMFFMuxer::AUDIO_CODEC_T::AC3;
				break;
			}
			ESMFFMuxer::CONTEXT_T::Instance().audioExtradataSize = extradataSize;
			if(ESMFFMuxer::CONTEXT_T::Instance().audioExtradataSize>0)
				::memmove(ESMFFMuxer::CONTEXT_T::Instance().audioExtradata, extradata, ESMFFMuxer::CONTEXT_T::Instance().audioExtradataSize);
			ESMFFMuxer::CONTEXT_T::Instance().audioSamplerate = samplerate;
			ESMFFMuxer::CONTEXT_T::Instance().audioChannels = channels;
			ESMFFMuxer::CONTEXT_T::Instance().audioSampleFormat = sampleFormat;
			ESMFFMuxer::CONTEXT_T::Instance().option |= ESMFFMuxer::MEDIA_TYPE_T::AUDIO;
		}
		pRTSPServer->PutAudioBitstream(codec, extradata, extradataSize, samplerate, channels, sampleFormat, pPk->data, pPk->size, pPk->duration, nSecIdx, -1, nSecIdx+1, gap);
	}
}

void CESMMovieRTMovie::CESMMovieRTRun::OnStreamRecordStart(BOOL bRTSP)
{
	if(ESMFFMuxer::Instance().IsInitialized())
		ESMFFMuxer::Instance().Release();
	//ESMFFMuxer::Instance().SetSectionIndex(0);
	//ESMFFMuxer::Instance().SetFrameIndex(0);
	if ( bRTSP==TRUE && m_pParent != NULL && m_pParent->m_pRTSPServerMgr != NULL ) {		 
		m_pParent->m_pRTSPServerMgr->SetPushInfo(GetUUID(), GetPushIP(), GetPushPort());
		m_pParent->m_pRTSPServerMgr->OnStreamRecordStart(); 
	}
	 
}

void CESMMovieRTMovie::CESMMovieRTRun::OnStreamRecordStop()
{
	if(ESMFFMuxer::Instance().IsInitialized())
		ESMFFMuxer::Instance().Release();
	if ( m_pParent != NULL && m_pParent->m_pRTSPServerMgr != NULL )
		m_pParent->m_pRTSPServerMgr->OnStreamRecordStop();	 
}

//  Media       : 1(video), 0(audio)
//  nDirection  : 1(fast),  0(slow)
//  nValue      : frame  
void CESMMovieRTMovie::CESMMovieRTRun::OnStreamSync(int nMedia, int nDirection, int nValue)
{
	if ( m_pParent != NULL && m_pParent->m_pRTSPServerMgr != NULL )
		m_pParent->m_pRTSPServerMgr->SetStreamSync(nMedia, nDirection, nValue);
}

// strSavePath : file path
// nParam :  1(start), 0(stop)
void CESMMovieRTMovie::CESMMovieRTRun::OnStreamWrite(CString strSavePath, CString strRamPath, int nParam, int nFrame, int nExtFrame, int nVrFrame)
{
	//if (nParam==1)
	//{
	//	m_nSecIdx = 0;
	//	m_nFrameCnt = 0;
	//}
	if ( m_pParent != NULL && m_pParent->m_pRTSPServerMgr != NULL )
	{
		m_pParent->m_pRTSPServerMgr->SetStreamWrite(strSavePath, nParam, nFrame, nExtFrame, nVrFrame);
		if(nParam==1)
		{
/*
			if(ESMFFMuxer::Instance().IsInitialized())
				ESMFFMuxer::Instance().Release();
			CString muxDstPath;
			muxDstPath.Format(L"%s\\out", strSavePath);
			::wcscpy_s(ESMFFMuxer::CONTEXT_T::Instance().muxTmpPath, L"M:");
			::wcscpy_s(ESMFFMuxer::CONTEXT_T::Instance().muxDstPath, muxDstPath);
			::wcscpy_s(ESMFFMuxer::CONTEXT_T::Instance().muxPrefix, m_pParent->m_pRTSPServerMgr->GetDSC());
			//ESMFFMuxer::CONTEXT_T::Instance().startFrameNumber = nStartFrame;
			//ESMFFMuxer::Instance().Initialize(&ESMFFMuxer::CONTEXT_T::Instance());
*/
		}
		else if (nParam==0)
		{
			/*if(ESMFFMuxer::Instance().IsInitialized())
				ESMFFMuxer::Instance().Release();*/
		}
	}
}

// strSavePath :  file path
// nParam :  0(pause), 1(play), 2(ready)
void CESMMovieRTMovie::CESMMovieRTRun::OnStreamPlay(int nParam, CString strFolder, CString strPath)
{ 
	map<CString, CPreRecordPlayManager*>::iterator iter;
 	switch (nParam) {
		case 2 : //ready  
			{
				iter = m_mpPreRecordPlayer.find(strFolder);
				if (iter != m_mpPreRecordPlayer.end()) {
					CPreRecordPlayManager* pManager = iter->second;
					delete pManager;
					m_mpPreRecordPlayer.erase (iter);
				}
				CPreRecordPlayManager* pManager = new CPreRecordPlayManager();
				m_mpPreRecordPlayer.insert(make_pair(strFolder, pManager));				 
				//strPath.Format(_T("F:\\RecordSave\\Record\\Movie\\files\\2019_12_10_17_03_42"));
				//strPath.Format(_T("F:\\RecordSave\\Record\\Movie\\files\\2019_12_10_20_25_32"));
				pManager->Ready(strPath);
 					
				break;
			}		
		case 1 : // play
			{
				iter = m_mpPreRecordPlayer.find(strFolder);
				if (iter != m_mpPreRecordPlayer.end()) {
					CPreRecordPlayManager* pManager = iter->second;
					if (m_pParent)
						m_pParent->SetLiveStreaming(FALSE);
					pManager->Start();
				}

				break;
			}
		case 0 : //pause 
			{
				iter = m_mpPreRecordPlayer.find(strFolder);
				if (iter != m_mpPreRecordPlayer.end()) {
					CPreRecordPlayManager* pManager = iter->second;
					delete pManager;
					m_mpPreRecordPlayer.erase (iter);
					if (m_pParent)
						m_pParent->SetLiveStreaming(TRUE);
				}					
				break;
			}
		default :

			break;	
	}	
}
