#pragma once
#include "stdafx.h"
#include <vector>
#include "ESMDefine.h"
#include "FFmpegManager.h"
#include "ESMMovieThread.h"
#include "ESMMovieThreadImage.h"
#include "RTSPServerManager.h"
#include "StopWatch.h"
#include "define.h"

#include <ppl.h>
#include <concurrent_queue.h>
#include <cuda.h>
#include <ESMNvdec.h>
#include <ESMNvenc.h>
#include <ESMNvresizer.h>
#include <ESMNvblender.h>
#include <ESMNvrotator.h>
#include <ESMDRMHandler.h>

using namespace Concurrency;

using namespace std;

#if defined(WITH_HEVC_TEST) || defined(WITH_AVC_TEST)
class ESMRTSPServer;
#endif

typedef struct _TC_USERDATA {

	int secIndex;
	int frameNum;
} tc_userdata;

struct ENCODE_FRAME_INFO
{
	Mat Y;
	Mat U;
	Mat V;
};

struct ENCODE_DATA_INFO
{
	CString strPath;
	vector<ENCODE_FRAME_INFO>ArrEncodeFrames;
	int nSecIdx;
};

const char ISO_IEC_11578_AR[16] = { 0xbc, 0x97, 0xb8, 0x4d, 0x96, 0x9f, 0x48, 0xb9, 0xbc, 0xe4, 0x7c, 0x1c, 0x1a, 0x39, 0x2f, 0x37 };
const char ISO_IEC_11578_POSITION_SWIPE[16] = { 0x62, 0x63, 0x00, 0xe0, 0x97, 0x87, 0x46, 0x1d, 0xa1, 0x25, 0x4d, 0x5c, 0x12, 0x49, 0xbc, 0x99 };

struct POSITION_SWIPE_T
{
	unsigned short w;
	unsigned short h;
	unsigned short l;
	unsigned short count;
};

struct POSITION_SWIPE_BODY_ELEMENT_T
{
	unsigned short c;
	unsigned short x1;
	unsigned short y1;
	unsigned short x2;
	unsigned short y2;
	unsigned short x3;
	unsigned short y3;
	unsigned short x4;
	unsigned short y4;
};

const POSITION_SWIPE_BODY_ELEMENT_T positionSwipeBodyElem[] = {
		{ 1, 404, 1088, 1938, 974, 2139, 1339, 472, 1491 },
		{ 2, 400, 1121, 1918, 979, 2162, 1334, 517, 1517 },
		{ 3, 387, 1147, 1891, 982, 2180, 1326, 560, 1538 },
		{ 4, 389, 1195, 1848, 984, 2214, 1303, 659, 1566 },
		{ 5, 400, 1239, 1827, 1004, 2238, 1306, 730, 1595 },
		{ 6, 407, 1254, 1805, 998, 2252, 1288, 787, 1600 },
		{ 7, 422, 1274, 1789, 997, 2271, 1276, 848, 1609 },
		{ 8, 434, 1299, 1765, 1002, 2281, 1268, 908, 1622 },
		{ 9, 448, 1316, 1744, 1004, 2288, 1258, 963, 1628 },
		{ 10, 469, 1337, 1731, 1013, 2301, 1258, 1020, 1640 },
		{ 11, 494, 1352, 1718, 1013, 2312, 1247, 1081, 1646 },
		{ 12, 521, 1375, 1699, 1017, 2321, 1239, 1147, 1655 },
		{ 13, 550, 1395, 1676, 1021, 2327, 1229, 1221, 1661 },
		{ 14, 593, 1422, 1652, 1027, 2338, 1217, 1322, 1668 },
		{ 15, 732, 1483, 1606, 1041, 2359, 1194, 1551, 1688 },
		{ 16, 797, 1505, 1582, 1040, 2361, 1175, 1662, 1686 },
		{ 17, 891, 1547, 1553, 1052, 2365, 1162, 1827, 1697 },
		{ 18, 1005, 1584, 1531, 1066, 2367, 1150, 1983, 1702 },
		{ 19, 1148, 1627, 1509, 1075, 2366, 1128, 2158, 1702 },
		{ 20, 1276, 1661, 1469, 1089, 2330, 1110, 2305, 1694 },
		{ 21, 1494, 1692, 1481, 1115, 2338, 1097, 2519, 1672 },
		{ 22, 1693, 1698, 1489, 1131, 2333, 1080, 2689, 1627 },
		{ 23, 1860, 1690, 1477, 1152, 2291, 1070, 2813, 1582 },
		{ 24, 1987, 1683, 1476, 1159, 2263, 1056, 2895, 1544 },
		{ 25, 2141, 1670, 1487, 1174, 2241, 1047, 2990, 1502 },
		{ 26, 2263, 1666, 1490, 1187, 2215, 1039, 3065, 1472 },
		{ 27, 2384, 1660, 1503, 1200, 2197, 1032, 3136, 1443 },
		{ 28, 2491, 1656, 1517, 1217, 2181, 1033, 3197, 1423 },
		{ 29, 2581, 1649, 1517, 1226, 2149, 1024, 3235, 1395 },
		{ 30, 2662, 1637, 1528, 1231, 2133, 1014, 3273, 1369 },
		{ 31, 2716, 1630, 1532, 1236, 2116, 1009, 3295, 1350 },
		{ 32, 2793, 1614, 1551, 1238, 2107, 999, 3328, 1322 },
		{ 33, 2836, 1614, 1552, 1251, 2085, 1001, 3341, 1311 },
		{ 34, 2893, 1611, 1562, 1259, 2072, 999, 3362, 1298 },
		{ 35, 2944, 1601, 1570, 1265, 2055, 992, 3381, 1276 },
		{ 36, 3000, 1600, 1583, 1279, 2040, 995, 3398, 1265 },
		{ 37, 3049, 1586, 1591, 1282, 2021, 989, 3407, 1242 },
		{ 38, 3086, 1574, 1594, 1287, 1998, 984, 3408, 1223 },
		{ 39, 3159, 1562, 1622, 1299, 1985, 979, 3428, 1194 },
		{ 40, 3207, 1549, 1630, 1309, 1958, 975, 3430, 1170 },
		{ 41, 3267, 1534, 1657, 1320, 1944, 974, 3438, 1146 },
		{ 42, 3313, 1511, 1674, 1329, 1916, 970, 3428, 1115 },
		{ 43, 3366, 1486, 1700, 1339, 1893, 967, 3424, 1083 },
		{ 44, 3406, 1457, 1722, 1348, 1864, 966, 3408, 1049 },
		{ 45, 3486, 1385, 1785, 1363, 1806, 969, 3363, 977 },
		{ 46, 3513, 1351, 1812, 1370, 1777, 972, 3332, 947 },/**/
		{ 47, 2662, 1637, 1528, 1231, 2133, 1014, 3273, 1369 },
		{ 48, 2716, 1630, 1532, 1236, 2116, 1009, 3295, 1350 },
		{ 49, 2793, 1614, 1551, 1238, 2107, 999, 3328, 1322 },
		{ 50, 2836, 1614, 1552, 1251, 2085, 1001, 3341, 1311 },
		{ 51, 2893, 1611, 1562, 1259, 2072, 999, 3362, 1298 },
		{ 52, 2944, 1601, 1570, 1265, 2055, 992, 3381, 1276 },
		{ 53, 3000, 1600, 1583, 1279, 2040, 995, 3398, 1265 },
		{ 54, 3049, 1586, 1591, 1282, 2021, 989, 3407, 1242 },
		{ 55, 3086, 1574, 1594, 1287, 1998, 984, 3408, 1223 },
		{ 56, 3159, 1562, 1622, 1299, 1985, 979, 3428, 1194 },
		{ 57, 3207, 1549, 1630, 1309, 1958, 975, 3430, 1170 },
		{ 58, 3267, 1534, 1657, 1320, 1944, 974, 3438, 1146 },
		{ 59, 3313, 1511, 1674, 1329, 1916, 970, 3428, 1115 },
		{ 60, 3366, 1486, 1700, 1339, 1893, 967, 3424, 1083 },
		{ 61, 3406, 1457, 1722, 1348, 1864, 966, 3408, 1049 },
		{ 62, 3486, 1385, 1785, 1363, 1806, 969, 3363, 977 },
		{ 63, 2662, 1637, 1528, 1231, 2133, 1014, 3273, 1369 },
		{ 64, 2716, 1630, 1532, 1236, 2116, 1009, 3295, 1350 },
		{ 65, 2793, 1614, 1551, 1238, 2107, 999, 3328, 1322 },
		{ 66, 2836, 1614, 1552, 1251, 2085, 1001, 3341, 1311 },
		{ 67, 2893, 1611, 1562, 1259, 2072, 999, 3362, 1298 },
		{ 68, 2944, 1601, 1570, 1265, 2055, 992, 3381, 1276 },
		{ 69, 3000, 1600, 1583, 1279, 2040, 995, 3398, 1265 },
		{ 70, 3049, 1586, 1591, 1282, 2021, 989, 3407, 1242 },
		{ 71, 3086, 1574, 1594, 1287, 1998, 984, 3408, 1223 },
		{ 72, 3159, 1562, 1622, 1299, 1985, 979, 3428, 1194 },
		{ 73, 3207, 1549, 1630, 1309, 1958, 975, 3430, 1170 },
		{ 74, 3267, 1534, 1657, 1320, 1944, 974, 3438, 1146 },
		{ 75, 3313, 1511, 1674, 1329, 1916, 970, 3428, 1115 },
		{ 76, 3366, 1486, 1700, 1339, 1893, 967, 3424, 1083 },
		{ 77, 3406, 1457, 1722, 1348, 1864, 966, 3408, 1049 },
		{ 78, 3313, 1511, 1674, 1329, 1916, 970, 3428, 1115 },
		{ 79, 3366, 1486, 1700, 1339, 1893, 967, 3424, 1083 },
		{ 80, 3406, 1457, 1722, 1348, 1864, 966, 3408, 1049 },
		{ 81, 3366, 1486, 1700, 1339, 1893, 967, 3424, 1083 },
		{ 82, 3406, 1457, 1722, 1348, 1864, 966, 3408, 1049 },
		{ 83, 3313, 1511, 1674, 1329, 1916, 970, 3428, 1115 },
		{ 84, 3366, 1486, 1700, 1339, 1893, 967, 3424, 1083 },
		{ 85, 3406, 1457, 1722, 1348, 1864, 966, 3408, 1049 }
	};
 

class CPreRecordPlayManager
{
	
public:
	CPreRecordPlayManager();
	~CPreRecordPlayManager();

	static unsigned WINAPI _FileLoader(LPVOID param);
	static unsigned WINAPI _AddLoadingFile(LPVOID param);

	void SetLoadFolder(CString strPath);
	CString GetLoadFolder();

	void Ready(CString strPath);
	void Start();
	void FileSort();
	void AddFile(CString strFile);
	int  GetFileCount();
	vector<int> GetArrayFile() {return m_arrFileName;}

	std::map<int, CString> GetArr() {return sorted_map;}
	CString m_strFolder;

	std::map<int, CString> sorted_map;

private :	 

private :
	
	vector<int> m_arrFileName;
};



class AFX_EXT_CLASS CESMMovieRTMovie
{
	//DECLARE_DYNCREATE(CESMMovieRTSend)
public:
	CESMMovieRTMovie();
	~CESMMovieRTMovie();
	void Set4DAInfo(CString strIP,CString strDSC,int nIdx, BOOL bAudioEnable, BOOL bExternalCamera, int nFrameRate)
	{m_strIP = strIP; m_strDSC = strDSC;m_nDSCIdx = nIdx;m_nFrameRate = nFrameRate; m_bAudioEnable=bAudioEnable;m_bExternalCamera=bExternalCamera;}
	void Run();

	void UpdateDSCIndex(int nIdx);
	
	/*Thread*/
	//static unsigned WINAPI RTRunThread(LPVOID param);

	/*Functions*/
	CString Get4DMNameFromPath(CString strPath);
	static CESMMovieRTMovie*  m_pInstance;

public:/*Adjust Data*/
	void SetAdjustData(stAdjustInfo* AdjustData);
	stAdjustInfo GetAdjustData(CString strDSCID);
	void SetMargin(int nMarginX,int nMarginY);
private:
	map<CString,stAdjustInfo>m_mapAdjData;
	

public:
	/*Inline func - Set clock*/
	void SetClock(CStopWatch* stopwatch)
	{
		m_pClock = stopwatch;
	}
	/*Inline func - main*/
	void SetParent(CWnd * pWnd) 
	{ 
		m_pWnd = pWnd; 
	}
	/*Inline func - Load check*/
	void SetMovieStart(BOOL b)
	{
		m_bMovieStart = b;
	}
	BOOL GetMovieStart()
	{
		return m_bMovieStart;
	}
	/*Inline func - Thread run*/
	void SetThreadRun(BOOL b)
	{
		m_bThreadRun = b;
	}
	BOOL GetThreadRun()
	{
		return m_bThreadRun;
	}
	/*Inline func - RTSPServerMgr*/
	CRTSPServerMgr* GetRTSPServer()
	{
		return m_pRTSPServerMgr;
	}

#if defined(WITH_HEVC_TEST) || defined(WITH_AVC_TEST)
	ESMRTSPServer* GetRTSPServer2()
	{
		return m_pRTSPServer;
	}
#endif

	void SetGPUCheck(bool bGPUCheck){m_bGPUCheck = bGPUCheck;};
	BOOL GetGPUCheck(){return m_bGPUCheck;};
	void SetGPUEnabled(bool bGPUEnabled) { m_bGPUEnabled = bGPUEnabled; };
	BOOL GetGPUEnabled(){return m_bGPUEnabled;};
	static void RTPVideoData_Callback(AVPacket* pPk, int codec, char * extradata, int extradataSize, int width, int height, double fps, int tsnum, int tsden, int secIndex, int rtp_frmNum, int ts, CString str4DM, int gap);
	static void RTPAudioData_Callback(AVPacket* pPk, int codec, char * extradata, int extradataSize, int samplerate, int channels, int sampleFormat, int tsnum, int tsden, CString str4DM, int gap);
	static void RTPInfoData_Callback(CString str4DM, int rtp_frmNum);

	static CESMMovieRTMovie* GetInstance() 
	{
		return m_pInstance;
	}

	void SetRotationParameter(BOOL bRotate, float degree)
	{
		m_bRotate = bRotate;
		m_fRotateDegree = degree;
	}

private:/*Critical section*/
	CRITICAL_SECTION m_criImage;
	CRITICAL_SECTION m_criPathMgr;
	

private:/*Valiable*/
	vector<CString> m_strArrPath;

	CString m_strIP;
	CString m_strDSC;
	
	int m_nDSCIdx;
	int m_nFrameRate;
	int m_nMarginX;
	int m_nMarginY;

	BOOL m_bMovieStart;
	BOOL m_bThreadRun;

	BOOL		m_bLiveStreaming;
	
	//wgkim 190607
	BOOL m_bGPUCheck;
	BOOL m_bGPUEnabled;

	BOOL	m_bUpdateAdj;
	BOOL	m_bRotate;
	float	m_fRotateDegree;

	std::map<SEI_TYPE_T, std::shared_ptr<SEI_MESSAGE_T>> m_seiMessages;

private:/*classes*/
	CWnd * m_pWnd;

	CRTSPServerMgr* m_pRTSPServerMgr;
	CStopWatch *m_pClock;

	BOOL m_bExternalCamera;
	BOOL m_bAudioEnable;

#if defined(WITH_HEVC_TEST) || defined(WITH_AVC_TEST)
	ESMRTSPServer * m_pRTSPServer;
#endif	
	
public:
	class CESMMovieRTRun
	{

	public:
		CESMMovieRTRun(CESMMovieRTMovie* pRTMovie,CString str4DM,CStopWatch* pStopWatch);
		~CESMMovieRTRun();

		CString m_str4DM;
		/*Thread*/
		void RunThread();
		static unsigned WINAPI	StreamWaitingProcessCB(LPVOID param);
		void					StreamWaitingProcess(void);
		int GetSecIdxFromPath(CString strPath);
		/*Message management*/
		void SendRTSPStateMessage(int nState,int nData);
		/*Image Process*/
		int Round(double dData);

		map<CString, CPreRecordPlayManager*> m_mpPreRecordPlayer;

		void SetYAdjustData(stAdjustInfo AdjustData,int nWidth,int nHeight,double dRatio,BOOL bLoad = TRUE);
		void SetUVAdjustData(stAdjustInfo AdjustData,int nWidth,int nHeight,double dRatio,BOOL bLoad = TRUE);
		void LoadAdjustData(CString strDSC, int nWidth, int nHeight);
		int	 TranscodeAndProcessImage(int nCodec, int nWidth, int nHeight, double nFps, unsigned char * bitstream, int bitstreamSize, CString str4DM, CString strInit4DM, int & nIndex, int & nFrameCnt, int ts,  int nGap);
		BOOL ImageProcessFrame(std::shared_ptr<FRAME_INFO> info);

		BOOL GetWaitingTimeFinish();
		void SetWaitingTimeFinish(BOOL b);


		void SetSavePath(CString strSavePath)
		{
			m_strSavePath = strSavePath;
		}
		CString GetSavePath()
		{
			return m_strSavePath;
		}

		void SetExternalCamera(BOOL bValue)
		{
			m_bExternalCamera = bValue;	
		}
		BOOL GetExternalCamera()
		{
			return m_bExternalCamera;
		}
		void SetAudioEnable(BOOL bEnable)
		{
			m_bAudioEnabled = bEnable;
		}
		BOOL GetAudioEnable()
		{
			return m_bAudioEnabled;
		}
		void SetPushInfo(CString strUUID, CString strPushIP, int nPort)
		{
			m_strPushUUID=strUUID;
			m_strPushIP=strPushIP;
			m_nPushPort=nPort;
		}		
		CString GetUUID() { return m_strPushUUID; }
		CString GetPushIP() { return m_strPushIP; }
		int GetPushPort() { return m_nPushPort; } 
 
		CString GetDsc() {  return m_pParent->m_strDSC; }
	

		void OnStreamRecordStart(BOOL bRTSP);
		void OnStreamRecordStop();
		void OnStreamSync(int nMedia, int nDirection, int nValue);
		void OnStreamWrite(CString strSavePath, CString strRamPath, int nParam, int nFrame, int nExtFrame, int nVrFrame);
		void OnStreamPlay(int nParam, CString strFolder, CString strPath);
		
		/*Inline func - Path*/
		vector<CString>m_strArrPath;
		void AddMoviePath(CString strPath)
		{
			EnterCriticalSection(&m_criPathMgr);
			m_strArrPath.push_back(strPath);
			LeaveCriticalSection(&m_criPathMgr);
		}
		void DeleteMoviePath()
		{
			EnterCriticalSection(&m_criPathMgr);
			m_strArrPath.erase(m_strArrPath.begin());
			LeaveCriticalSection(&m_criPathMgr);
		}
		CString GetMoviePath()
		{
			CString str;
			EnterCriticalSection(&m_criPathMgr);
			str = m_strArrPath.at(0);
			LeaveCriticalSection(&m_criPathMgr);
			return str;
		}
		int GetMoviePathSize()
		{
			int n = 0;
			EnterCriticalSection(&m_criPathMgr);
			n = m_strArrPath.size();
			LeaveCriticalSection(&m_criPathMgr);
			return n;
		}
		BOOL GetPathMgrFinish()
		{
			return m_bPathMgrRun;
		}
		void SetPathMgrFinish(BOOL b)
		{
			m_bPathMgrRun = b;
		}
		/*Inline func - Decode Array*/
		//static unsigned WINAPI _ImageProcessingThread(LPVOID param);
		vector<MAKE_FRAME_INFO>m_stArrFrameInfo;
		int GetFrameCount()
		{
			EnterCriticalSection(&m_criDecode);
			int nSize = m_stArrFrameInfo.size();
			LeaveCriticalSection(&m_criDecode);
			return nSize;
		}
		MAKE_FRAME_INFO GetFrameInfo()
		{
			EnterCriticalSection(&m_criDecode);
			MAKE_FRAME_INFO info = m_stArrFrameInfo.at(0);
			LeaveCriticalSection(&m_criDecode);
			return info;
		}
		void DeleteFrameInfo()
		{
			EnterCriticalSection(&m_criDecode);
			m_stArrFrameInfo.erase(m_stArrFrameInfo.begin());
			LeaveCriticalSection(&m_criDecode);
		}
		void SetFrameInfo(MAKE_FRAME_INFO info)
		{
			EnterCriticalSection(&m_criDecode);
			m_stArrFrameInfo.push_back(info);
			LeaveCriticalSection(&m_criDecode);
		}

		std::vector<std::shared_ptr<BITSTREAM_INFO>> m_ArrBistream;
		void PutVideoBitstream(std::shared_ptr<BITSTREAM_INFO> info)
		{
			EnterCriticalSection(&m_criBitStream);
			m_ArrBistream.push_back(info);
			LeaveCriticalSection(&m_criBitStream);
		}

		std::shared_ptr<BITSTREAM_INFO> GetFirstIndexVideoBitstream()
		{
			std::shared_ptr<BITSTREAM_INFO> info;
			EnterCriticalSection(&m_criBitStream);
			info = m_ArrBistream.at(0);
			LeaveCriticalSection(&m_criBitStream);
			return info;
		}

		int GetVideoBitstreamCount()
		{
			int nSize = 0;
			EnterCriticalSection(&m_criBitStream);
			nSize = m_ArrBistream.size();
			LeaveCriticalSection(&m_criBitStream);
			return nSize;
		}

		void DeleteFirstIndexVideoBitstream()
		{
			EnterCriticalSection(&m_criBitStream);
			m_ArrBistream.erase(m_ArrBistream.begin());
			LeaveCriticalSection(&m_criBitStream);
		}


		/*Sync Manage*/
		void SetSyncTime(int nTime)
		{
			m_nSyncTime = nTime;
		}
		int GetSyncTime(){return m_nSyncTime;}
		int GetClock();

		void SetStopIdx(int n)
		{
			EnterCriticalSection(&m_criStopTime);
			m_nStopIdx = n;
			LeaveCriticalSection(&m_criStopTime);

			CString strLog;
			strLog.Format(_T("[%s] Stop Time: %d"),m_str4DM,n);
			SendLog(5,strLog);
		}
		int GetStopIdx()
		{
			int n = 99999;
			EnterCriticalSection(&m_criStopTime);
			n = m_nStopIdx;
			LeaveCriticalSection(&m_criStopTime);

			return n;
		}

		void SetRotationParameter(BOOL bRotate, float degree)
		{
			m_bRotate = bRotate;
			m_fRotateDegree = degree;
		}

	private:
		/*Critical section*/
		CRITICAL_SECTION m_criPathMgr;
		CRITICAL_SECTION m_criDecode;
		CRITICAL_SECTION m_criSyncMgr;
		CRITICAL_SECTION m_criEncode;
		CRITICAL_SECTION m_criStopTime;
		CRITICAL_SECTION m_criBitStream;


		//CRITICAL_SECTION m_criProcess;
		CRITICAL_SECTION m_criAdjust;
		CESMMovieRTMovie* m_pParent;
		CStopWatch* m_pClock;

		CString		m_strSavePath;
		BOOL		m_bAudioEnabled;
		BOOL		m_bExternalCamera;

		CString		m_strPushUUID;
		CString		m_strPushIP;
		int			m_nPushPort;


		int m_nSyncTime;
		int m_nFrameRate;
		int m_nWidth;
		int m_nHeight;
		int m_nYSize;
		int m_nUVSize;
		int m_nYWidth;
		int m_nYHeight;
		int m_nUVWidth;
		int m_nUVHeight;
		int m_nYTop;
		int m_nYLeft;
		int m_nUVTop;
		int m_nUVLeft;

		BOOL m_bSetProperty;
		BOOL m_bLoadAdj;
		BOOL m_bPathMgrRun;
		BOOL m_bWaitingTimeRun;
		Mat m_matYAdj;
		Mat m_matUVAdj;

		cv::Size m_szYRe;
		cv::Size m_szUVRe;

		//Logo Image
		cv::Mat m_matLogoImg;

		int m_nStopIdx;

		BOOL m_bUseRect;

		HANDLE m_ContainerEvent;
		concurrent_queue<std::shared_ptr<CONTAINER_BITSTREAM_INFO>> m_ContainerBitstreamInfos;

		ESMNvdec::CONTEXT_T m_nvDecoderCtx;
		ESMNvdec * m_nvDecoder;
		ESMNvenc::CONTEXT_T m_nvEncoderCtx;
		ESMNvenc * m_nvEncoder;
		ESMNvresizer::CONTEXT_T m_nvResizerCtx;
		ESMNvresizer * m_nvResizer;
		ESMNvblender::CONTEXT_T m_nvBlenderCtx;
		ESMNvblender * m_nvBlender;
		ESMNvrotator::CONTEXT_T m_nvRotatorCtx;
		ESMNvrotator * m_nvRotator;
		BOOL	m_bRotate;
		float	m_fRotateDegree;
#if defined(WITH_DRM)
		ESMDRMHandler::CONTEXT_T m_drmHandlerCtx;
		ESMDRMHandler * m_drmHandler;
#endif

		int m_nSecIdx;
		int m_nFrameCnt;
	};
	int GetDSCIndex(){return m_nDSCIdx;}
	void SetSyncTime(CString str4DM,int nTime,int nESMTick);
	map<CString,CESMMovieRTRun*> m_mapRun;
	CESMMovieRTRun* GetESMMovieRun(CString str4DM)
	{
		CESMMovieRTRun* pRTRun = m_mapRun[str4DM];

		if(pRTRun == NULL)
		{
			pRTRun = new CESMMovieRTRun(this,str4DM,m_pClock);
			m_mapRun[str4DM] = pRTRun;
			if(m_bRotate)
			{
				pRTRun->SetRotationParameter(TRUE, m_fRotateDegree);
			}
			else
			{
				pRTRun->SetRotationParameter(FALSE, m_fRotateDegree);
			}

			//m_pRTSPServerMgr->SetRTSPStopTime(999999);
			SendLog(5,_T("[ESMRTMovie] - Create: ")+str4DM);
		}
		else
		{
			if(m_bRotate)
			{
				pRTRun->SetRotationParameter(TRUE, m_fRotateDegree);
			}
			else
			{
				pRTRun->SetRotationParameter(FALSE, m_fRotateDegree);
			}
		}

		return pRTRun;
	}

	CESMMovieRTRun* GetFirstESMMovieRun()
	{
		CESMMovieRTRun* pRTRun = NULL;

		if(m_mapRun.empty()) {
			SYSTEMTIME cur_time; 
			CString strDate;
			GetLocalTime(&cur_time); 
			strDate.Format(_T("%04d_%02d_%02d_%02d_%02d_%02d"), cur_time.wYear, cur_time.wMonth, cur_time.wDay, cur_time.wHour, cur_time.wMinute, cur_time.wSecond);
 
			pRTRun = new CESMMovieRTRun(this,strDate,m_pClock);
			m_mapRun[strDate] = pRTRun;
			if(m_bRotate)
			{
				pRTRun->SetRotationParameter(TRUE, m_fRotateDegree);
			}
			else
			{
				pRTRun->SetRotationParameter(FALSE, m_fRotateDegree);
			}

			//m_pRTSPServerMgr->SetRTSPStopTime(999999);
			SendLog(5,_T("[ESMRTMovie] - Create: ")+strDate);
		}
		else {
			map<CString,CESMMovieRTRun*>::iterator itmapList = m_mapRun.begin();

			if ( itmapList != m_mapRun.end() ) {

				pRTRun = itmapList->second;
				if(m_bRotate)
				{
					pRTRun->SetRotationParameter(TRUE, m_fRotateDegree);
				}
				else
				{
					pRTRun->SetRotationParameter(FALSE, m_fRotateDegree);
				}
			}
		}

		return pRTRun;
	}

	BOOL IsEmptyESMMovieRun()
	{
		if(m_mapRun.empty()) {
			return TRUE;
		}

		return FALSE;
	}

	BOOL IsExistESMMovieRun(CString str4DM)
	{
		CESMMovieRTRun* pRTRun = m_mapRun[str4DM];

		if(pRTRun == NULL) {
			return FALSE;
		}

		return TRUE;
	}

	CESMMovieRTRun* GetLastESMMovieRun()
	{
		CESMMovieRTRun* pRTRun = NULL;

		if(m_mapRun.empty()) {
			SYSTEMTIME cur_time; 
			CString strDate;
			GetLocalTime(&cur_time); 
			strDate.Format(_T("%04d_%02d_%02d_%02d_%02d_%02d"), cur_time.wYear, cur_time.wMonth, cur_time.wDay, cur_time.wHour, cur_time.wMinute, cur_time.wSecond);
 
			pRTRun = new CESMMovieRTRun(this,strDate,m_pClock);
			m_mapRun[strDate] = pRTRun;
			if(m_bRotate)
			{
				pRTRun->SetRotationParameter(TRUE, m_fRotateDegree);
			}
			else
			{
				pRTRun->SetRotationParameter(FALSE, m_fRotateDegree);
			}

			//m_pRTSPServerMgr->SetRTSPStopTime(999999);
			SendLog(5,_T("[ESMRTMovie] - Create: ")+strDate);
		}
		else {
			map<CString,CESMMovieRTRun*>::reverse_iterator itmapList = m_mapRun.rbegin();

			if ( itmapList != m_mapRun.rend() ) {

				pRTRun = itmapList->second;
				if(m_bRotate)
				{
					pRTRun->SetRotationParameter(TRUE, m_fRotateDegree);
				}
				else
				{
					pRTRun->SetRotationParameter(FALSE, m_fRotateDegree);
				}
			}
		}
		return pRTRun;
	}	

	/*Inline func - Path*/
	void SetRTSendPath(CString strPath)
	{
		SendLog(5,_T("[SetRTSendPath]")+strPath);
		CString str4DMName = Get4DMNameFromPath(strPath);
		CESMMovieRTRun* pRTRun = GetESMMovieRun(str4DMName);
		pRTRun->AddMoviePath(strPath);
	}
	void DeleteRTRun(CString str4DM)
	{
		CESMMovieRTRun* pRTRun = m_mapRun[str4DM];
		if(pRTRun)
		{
			delete pRTRun;
			pRTRun = NULL;
		}
		m_mapRun.erase(str4DM);
		SendLog(5,_T("[ESMRTMovie]-Delete ")+str4DM);
	}
	void SetSavePath(CString strPath);
	void SetAudioEnable(CString strPath, BOOL bEnable);
	void SetExternalCamera(CString strPath, BOOL bValue);	
	void SetPushInfo(CString strPath, CString strUUID, CString strPushIP, int nPort);
	void SetActiveChannel(BOOL isActiveChannel);
	void OnStreamRecordStart(CString str4DM, BOOL bRTSP);
	void OnStreamRecordStop(CString str4DM);
	void OnStreamSync(int nMedia, int nDirection, int nValue);
	void OnStreamWrite(CString str4DM, CString strSavePath, CString strRamPath, int nParam, int nFrame, int nExtFrame, int nVrFrame);
	void OnStreamPlay(int nParam, CString strFolder, CString strPath);
	void SetLiveStreaming(BOOL bEnable);		 
	BOOL GetLiveStreaming();

	//ESMMovieRTMovie.h
	void SetStopIdx(CString str4DM,int n);

private:
	int m_bMulticast;
	int m_bPush;
	int m_nLive_framerate;
	int m_nLive_bitrate;
	int m_nLive_resolution_width;
	int m_nLive_resolution_height;

public:
	void SetMulticast(int nValue) { m_bMulticast = nValue; };
	void SetPush(int nValue) { m_bPush = nValue; };
	void SetLiveBitrate(int nValue) { m_nLive_bitrate = nValue * 1024 * 1024; };
	void SetLiveFramerate(int nValue) { m_nLive_framerate = nValue; };
	void SetLiveResolution(int nWidth, int nHeight) { m_nLive_resolution_width = nWidth; m_nLive_resolution_height = nHeight; };


	int GetMulticast() { return m_bMulticast; };
	int GetPush() { return m_bPush; };
	int GetLiveBitrate() { return m_nLive_bitrate; };
	int GetliveFramerate() {return m_nLive_framerate; };
	int GetLiveResolutionWidth() { return m_nLive_resolution_width; };
	int GetLiveResolutionHeight() { return m_nLive_resolution_height; };

};

struct RTMovieConsoleData
{
	CESMMovieRTMovie* pParent;
	CString strPath;
	CString strSavePath;
	CString strSaveFileName;
	int nGPUIdx;
	int nSecIdx;	
	int nMakeFrame;
};