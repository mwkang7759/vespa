/// @file	ImageProcessor.h
/// @date	2019/11/22
/// @author	slee
// --------------------------------------------------------------------

#pragma once

#include "ESMMovieMgr.h"

using namespace cv;

#ifndef PHI
#define PHI 3.1415926535897932
#endif
#ifndef MAXDEPTH
#define MAXDEPTH 255
#endif

// --------------------------------------------------------------
/// @brief	Common static params and functions relate to adjust
/// @author	slee
/// @date	2019-12-18
/// @todo	have to modify details
// --------------------------------------------------------------
typedef class AFX_EXT_CLASS CImageProcessor
{
	//////////////////////////////////////////////////////////////////////////
	///@ingroup Movie Params
private:
	static int	m_nMovieSize;		///< 0: FHD, 1: UHD
	static BOOL	m_bUHDtoFHD;		///< UHDtoFHD Flag
	static BOOL	m_bVMCCFlag;		///< VMCC Flag
	static int	m_nMarginX;			///< Margin X (int)
	static int	m_nMarginY;			///< Margin Y (int)
	static BOOL m_bMakeBorder;
	static vector<Point2f> m_vInterAdjPts;

	//////////////////////////////////////////////////////////////////////////
	///@ingroup Getter
public:
	static int GetMovieSize();		///< Getter m_nMovieSize
	static BOOL GetUHDtoFHD();		///< Getter m_bUHDtoFHD
	static BOOL GetVMCCFlag();		///< Getter m_bVMCCFlag
	static int GetMarginX();		///< Getter m_nMarginX
	static int GetMarginY();		///< Getter m_nMarginY
	static void GetMovieSizeData(int& nSrcWidth, int& nSrcHeight, int& nOutputWidth, int& nOutputHeight);	///< Get size of source/output images
	static vector<Point2f> GetInterAdjPts();	///< Get size of source/output images

	//////////////////////////////////////////////////////////////////////////
	///@ingroup Setter
public:
	static void SetMovieSize(int nMovieSize);	///< Setter m_nMovieSize
	static void SetUHDtoFHD(BOOL bUHDtoFHD);	///< Setter m_bUHDtoFHD
	static void SetVMCCFlag(BOOL bVMCCFlag);	///< Setter m_bVMCCFlag
	static void SetMarginX(int nMarginX);		///< Setter m_nMarginX
	static void SetMarginY(int nMarginY);		///< Setter m_nMarginY
	static void SetMakeBorder(BOOL bMakeBorder);
	static void SetInterAdjPts(vector<Point2f> vPts);	///< Get size of source/output images

	//////////////////////////////////////////////////////////////////////////
	///@ingroup Adjust Functions
public:
	/// Adjust frame 
	static int AdjustMovieFrame(CESMMovieMgr *pMovieManager, EffectInfo* pEffectData, CString strMovieId, CString strMovieName, int nFrameNum, int nDscIndex);
	/// Adjust single frame
	static int AdjustFrame(CESMMovieMgr *pMovieManager, MakeFrameInfo* pFrameInfo, CString strPath, BOOL bSaveImg = FALSE, BOOL bReverse = FALSE);
	/// Adjust single frame using VMCC
	static int AdjustFrameUsingVMCC(CESMMovieMgr *pMovieManager, MakeFrameInfo* pFrameInfo, bool bIsFullHD, CString strPath, BOOL bSaveImg = FALSE, BOOL bReverse = FALSE);

	//////////////////////////////////////////////////////////////////////////
	///@ingroup Math Functions
public:
	/// Round floating number
	static int Round(double dData);
	/// Get maximum value in three values.
	static double GetMaxValue(double dbBlue, double dbGreen, double dbRed);
	/// Get minimum value in three values.
	static double GetMinValue(double dbBlue, double dbGreen, double dbRed);
	/// Get normalization value
	static double NormValue(int nValue);
	/// Get saturation value
	static double GetSaturation(double dbValue, double dbMin);
	/// Get hue value
	static double GetHue(double dbBlue, double dbGreen, double dbRed, double dValue, double dMin);	
	/// Calculate lining point
	static cv::Point2f CalcLiningPoint(int nRadian, double dbHue, double dbSaturation);
	/// Calculate trans value
	static int CalcTransValue(int nPixel, int nRefValue);

	//////////////////////////////////////////////////////////////////////////
	///@ingroup CPU Processing Functions
public:
	/// Move image using CPU
	static void CpuMoveImage(Mat* gMat, int nX, int nY);
	/// Rotate image using CPU
	static void CpuRotateImage(Mat Iimage, double nCenterX, double nCenterY,  double dScale, double dAngle, BOOL bReverse = FALSE, double dbSize = 1.0);
	/// Make margin to image using CPU
	static void CpuMakeMargin(Mat* gMat, int nX, int nY);

	///@ingroup GPU Procssing Functions
public:
	/// Move image using GPU
	static void GpuMoveImage(cuda::GpuMat* gMat, int nX, int nY);
	/// Rotate image using GPU
	static void GpuRotateImage(cuda::GpuMat* gMat, double nCenterX, double nCenterY,  double dScale, double dAngle, BOOL bReverse = FALSE);
	/// Make margin to image using GPU
	static void GpuMakeMargin(cuda::GpuMat* gMat, int nX, int nY);
	/// Zoom image using GPU
	static void GpuZoomImage(cuda::GpuMat* gMat, int nRotateX, int nRotateY, int nX, int nY, int nRatio, int nOutputWidth, int nOutputHeight);

	//////////////////////////////////////////////////////////////////////////
	///@ingroup Overlay Functions
public:
	/// Insert logo in GpuMat Image using GPU
	static void GpuInsertLogo(cuda::GpuMat* gMat, int nDscNum, int nWidth, int nHeight);
	/// Insert logo in Mat Image using GPU
	static void GpuInsertLogo2(Mat* tpMat, int nDscNum, BOOL b3DLogo);
	/// Insert Camera ID using GPU
	static void GpuInsertCameraID(Mat* tpMat, int nIndex, CString strDscNum, CString strDscIp, BOOL bPntColorInfo);
	/// Insert strike zone prism
	static void KzonePrism(Mat *background, int nDscNum, int taValue,int saValue,int nPrinum);
	/// Overlay foreground image on background image
	static void OverlayImage(const IplImage *background, const IplImage *foreground, IplImage *output, CvPoint location);

	//////////////////////////////////////////////////////////////////////////
	///@ingroup Other Functions
public:
	/// Convert color model BGR to HSV
	static void BGR2HSV(Mat img, Mat result);
	/// Make alpha blending image
	static void AlphaBlending(Mat background, Mat pr, Mat result, int alpha);
	/// Calculate frame information
	static void CalcFrameInfo(Mat *pImage);
	/// Calculate average RGB value.
	static void CalcFrameAvg(Mat img, double &dBlue, double &dGreen, double &dRed);
	/// Draw color value on Mat Image
	static void DrawColorValue(Mat result, double dbHue, double dbSaturation, double dbValue, double dbBlue, double dbGreen, double dbRed);
	/// Draw lining point
	static void SampleLining(Mat img);
	/// Apply color revision to Mat Image
	static void DoColorRevision(cv::Mat *frame,FrameRGBInfo stColorInfo);

	// Find prism pixel
	static void FindPrismPixel(int t_Peak_x,int t_Peak_y,int t_LB_x,int t_LB_y,int t_RB_x, int t_RB_y, int *t_LT_x, int *t_LT_y,int *t_RT_x, int *t_RT_y);
	// Apply affine transform
	static void affinetransform(Mat img,cv::Point a,cv::Point b, cv::Point c, Point2f srcTri[3],Mat dst);


	//////////////////////////////////////////////////////////////////////////
	///@ingroup New version adjust function (Add 200205 slee)
public:
	static void AdjustImage(Mat& src, Mat& dst, Size2f szDst, stAdjustInfo &adjustData, bool bMakeBorder=false, bool bReverse = false);
	static void AdjustImageUsingEffectData(Mat& src, Mat& dst, Size2f szDst, stAdjustInfo &adjustData, EffectInfo &effectData, bool bMakeBorder=false, bool bReverse = false);
	static void AdjustImage(Mat& src, Mat& dst, Size2f szDst, Rect2f rtMargin, float angle=0.f, float scale=1.f, float shift_x = 0.f, float shift_y = 0.f, bool bMakeBorder=false);
	static Point2f GetRotatedCoord(Point2f ptSrc, float deg);
	static bool GetIntersectPoint(Point2f pt1_1, Point2f pt1_2, Point2f pt2_1, Point2f pt2_2, Point2f& ptRes);
	static Rect2f GetRotatedRectInfo(Size2f szSrc, Size2f& szDst, float deg);
	static void GetRotatedOuterBoxSize(Size2f szSrc, double& dScale, float deg, float fOffsetX=0.f, float fOffsetY=0.f);

} imgproc;
