﻿#include "ESMFFDemuxer.h"
#include <vector>
#include <string>
#include <memory>

extern "C"{
	#include <libavcodec/avcodec.h>
	#include <libavformat/avformat.h>
	#include <libavutil/mem.h>
	#include <libavutil/opt.h>
	#include <libavutil/imgutils.h>
	#include <libswscale/swscale.h>
	#include <libavutil/mathematics.h>
	#include <libavutil/time.h>
};

class ESMFFDemuxer::Core
{
public:
	Core(VOID);
	virtual ~Core(VOID);

	int32_t Demux(std::shared_ptr<ESMFFDemuxer::CONTEXT_T> ctx);

private:
	int32_t Process(std::shared_ptr<ESMFFDemuxer::CONTEXT_T> _ctx);

private:
	CRITICAL_SECTION	_lock;
};

