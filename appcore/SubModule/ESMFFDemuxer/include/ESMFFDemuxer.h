﻿#pragma once

#if defined(EXPORT_ESM_FF_DEMUXER_LIB)
#  define EXP_ESM_FF_DEMUXER_CLASS __declspec(dllexport)
#else
#  define EXP_ESM_FF_DEMUXER_CLASS __declspec(dllimport)
#endif

#include <ESMBase.h>
//#include <MppcPointData.h>

class ESMFFDemuxer : public ESMBase
{
	class Core;
public:
	class Handler;

	typedef struct _CONTEXT_T
	{
		char path[MAX_PATH];
		float fWantedFPS{};
		int32_t videoFrameCount{}; // Note: This value is for the output use. Not for the input use. The purpose is the debugging.
		int32_t nStartFrameNum{};
		int32_t nEndFrameNum{};
		int32_t nSrcGopSize{};
		int32_t nStartGopNum{};
		std::string dsc_id;
		bool mppc{};
		ESMFFDemuxer::Handler* handler{ nullptr };

		_CONTEXT_T(void)
		{
			::memset(path, 0x00, sizeof(path));
		}
	} CONTEXT_T;

	class Handler
	{
	public:
		virtual int32_t OnVideoBegin(std::shared_ptr<CONTEXT_T> ctx, int32_t codec, const uint8_t* extradata, int32_t extradataSize, int32_t width, int32_t height, double fps, int32_t framecount, int32_t seektime) = 0;
		virtual int32_t OnVideoRecv(std::shared_ptr<CONTEXT_T> ctx, uint8_t* bytes, int32_t nbytes, int32_t& nFrameIdx, int nFrameIndex) = 0;
		virtual int32_t OnVideoEnd(std::shared_ptr<CONTEXT_T> ctx, int32_t& nFrameIdx, int nFrameIndex) = 0;
	};

	ESMFFDemuxer(VOID);
	virtual ~ESMFFDemuxer(VOID);

	int32_t Demux(std::shared_ptr<ESMFFDemuxer::CONTEXT_T> ctx);

private:
	ESMFFDemuxer::Core* _core;
};