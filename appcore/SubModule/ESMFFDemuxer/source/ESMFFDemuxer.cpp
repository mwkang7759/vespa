﻿#include "ESMFFDemuxer.h"
#include "ESMFFDemuxerCore.h"

ESMFFDemuxer::ESMFFDemuxer(VOID)
{
	_core = new ESMFFDemuxer::Core();
}

ESMFFDemuxer::~ESMFFDemuxer(VOID)
{
	if (_core)
	{
		delete _core;
		_core = NULL;
	}
}

int32_t ESMFFDemuxer::Demux(std::shared_ptr<ESMFFDemuxer::CONTEXT_T> ctx)
{
	return _core->Demux(ctx);
}