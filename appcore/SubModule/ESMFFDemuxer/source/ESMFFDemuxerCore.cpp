﻿#include "ESMFFDemuxerCore.h"
#include <ESMLocks.h>
#include <process.h>

ESMFFDemuxer::Core::Core(VOID)
{
	::InitializeCriticalSection(&_lock);
}

ESMFFDemuxer::Core::~Core(VOID)
{
	::DeleteCriticalSection(&_lock);
}

int32_t ESMFFDemuxer::Core::Demux(std::shared_ptr<ESMFFDemuxer::CONTEXT_T> ctx)
{
	if ((ctx == NULL) || (ctx->path.length() < 1) || (ctx->handler == NULL))
		return ESMFFDemuxer::ERR_CODE_T::INVALID_PARAMETER;

	//_handler = ctx->handler;
	//_ctx = ctx;
	return Process(ctx);
}

int32_t ESMFFDemuxer::Core::Process(std::shared_ptr<ESMFFDemuxer::CONTEXT_T> ctx)
{
	AVFormatContext* formatCtx = NULL;
	int32_t				streamIndex = -1;
	AVPacket			packet;
	AVPacket			packetFiltered;
	AVBSFContext* bsfc = NULL;
	int		nFrameCount = 0;
	av_init_packet(&packet);
	packet.data = NULL;
	packet.size = 0;
	av_init_packet(&packetFiltered);
	packetFiltered.data = NULL;
	packetFiltered.size = 0;

	if (avformat_open_input(&formatCtx, ctx->path.c_str(), 0, NULL) != 0) {
		return ESMFFDemuxer::ERR_CODE_T::OPEN_INPUT_ERROR;
	}
	if (avformat_find_stream_info(formatCtx, NULL) < 0) {
		return ESMFFDemuxer::ERR_CODE_T::FIND_STREAM_INFO_ERROR;
	}
	streamIndex = av_find_best_stream(formatCtx, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
	if (streamIndex == -1) {
		return ESMFFDemuxer::ERR_CODE_T::STREAM_INDEX_ERROR;
	}

	BOOL bMP4 = FALSE;
	if (streamIndex >= 0)
	{
		AVCodecID videoCodec = formatCtx->streams[streamIndex]->codecpar->codec_id;
		int32_t videoExtradataSize = formatCtx->streams[streamIndex]->codecpar->extradata_size;
		uint8_t* videoExtradata = formatCtx->streams[streamIndex]->codecpar->extradata;
		int32_t videoWidth = formatCtx->streams[streamIndex]->codecpar->width;
		int32_t videoHeight = formatCtx->streams[streamIndex]->codecpar->height;
		int32_t videoFPSNum = formatCtx->streams[streamIndex]->r_frame_rate.num;
		int32_t videoFPSDen = formatCtx->streams[streamIndex]->r_frame_rate.den;
		int32_t videoFrameCount = formatCtx->streams[streamIndex]->nb_frames;
		double videoFPS = (double)videoFPSNum / (double)videoFPSDen;
		int32_t videoCodec2 = ESMFFDemuxer::VIDEO_CODEC_T::AVC;
		bMP4 = (videoCodec == AV_CODEC_ID_MPEG4) ||

			(videoCodec == AV_CODEC_ID_H264) ||
			(videoCodec == AV_CODEC_ID_HEVC) && (!strcmp(formatCtx->iformat->long_name, "QuickTime / MOV") ||
				!strcmp(formatCtx->iformat->long_name, "FLV (Flash Video)") ||
				!strcmp(formatCtx->iformat->long_name, "Matroska / WebM"));
		if (bMP4)
		{
			const AVBitStreamFilter* bsf = NULL;
			if (videoCodec == AV_CODEC_ID_MPEG4)
				bsf = av_bsf_get_by_name("mpeg4_unpack_bframes");
			else if (videoCodec == AV_CODEC_ID_H264)
				bsf = av_bsf_get_by_name("h264_mp4toannexb");
			else if (videoCodec == AV_CODEC_ID_HEVC)
				bsf = av_bsf_get_by_name("hevc_mp4toannexb");

			if (!bsf)
				return ESMFFDemuxer::ERR_CODE_T::GENERIC_FAIL;

			av_bsf_alloc(bsf, &bsfc);
			bsfc->par_in = formatCtx->streams[streamIndex]->codecpar;
			av_bsf_init(bsfc);
		}

		switch (videoCodec)
		{
		case AV_CODEC_ID_H264:
			videoCodec2 = ESMFFDemuxer::VIDEO_CODEC_T::AVC;
			break;
		case AV_CODEC_ID_HEVC:
			videoCodec2 = ESMFFDemuxer::VIDEO_CODEC_T::HEVC;
			break;
		}

		if (ctx->handler)
		{
			int32_t videoSeekTime = (static_cast<int64_t>(formatCtx->streams[streamIndex]->r_frame_rate.den) * static_cast<int64_t>(formatCtx->streams[streamIndex]->time_base.den))
				/ (static_cast<int64_t>(formatCtx->streams[streamIndex]->r_frame_rate.num) * static_cast<int64_t>(formatCtx->streams[streamIndex]->time_base.num));
			int32_t seekGopTime = ctx->nStartGopNum * ctx->nSrcGopSize * videoSeekTime;
			int32_t wantedFPS = (int32_t)ceil(ctx->fWantedFPS);

			if (wantedFPS == 25)
			{
				// If wantedFPS(srcFPS) is 25, the comparison is done with the 50 because the frame count of the PreSD was doubled.
				if ((wantedFPS * 2) != videoFrameCount)
				{
					ctx->videoFrameCount = videoFrameCount;
					return ESMFFDemuxer::ERR_CODE_T::FRAMECOUNT_DIFFER_FROM_FPS;
				}
			}
			else
			{
				if (wantedFPS != videoFrameCount)
				{
					ctx->videoFrameCount = videoFrameCount;
					return ESMFFDemuxer::ERR_CODE_T::FRAMECOUNT_DIFFER_FROM_FPS;
				}
			}

			int32_t error = ctx->handler->OnVideoBegin(ctx, videoCodec2, videoExtradata, videoExtradataSize, videoWidth, videoHeight, videoFPS, videoFrameCount, videoSeekTime);
			if (error != ESMFFDemuxer::ERR_CODE_T::SUCCESS)
				return error;

			error = av_seek_frame(formatCtx, streamIndex, seekGopTime, AVSEEK_FLAG_ANY);
			if (error != ESMFFDemuxer::ERR_CODE_T::SUCCESS)
				return error;
		}
	}

	int32_t e = 0;
	int32_t frameindex = ctx->nStartFrameNum - (ctx->nStartFrameNum % ctx->nSrcGopSize) - 1;
	int32_t decode_error = 0;
	while ((e = av_read_frame(formatCtx, &packet)) >= 0)
	{
		if (packet.stream_index == streamIndex)
		{
			if (bMP4)
			{
				if (packetFiltered.data)
					av_packet_unref(&packetFiltered);
				av_bsf_send_packet(bsfc, &packet);
				av_bsf_receive_packet(bsfc, &packetFiltered);
				if (packetFiltered.size > 0)
				{
					if (ctx->handler)
					{
						decode_error = ctx->handler->OnVideoRecv(ctx, packetFiltered.data, packetFiltered.size, frameindex, nFrameCount++);
						if (decode_error == ESMFFDemuxer::ERR_CODE_T::ERR_AGAIN)
							continue;
						else if (decode_error == ESMFFDemuxer::ERR_CODE_T::FINISH_DECODE)
							break;
						else if (decode_error != ESMFFDemuxer::ERR_CODE_T::SUCCESS)
						{
							if (packet.data)
								av_packet_unref(&packet);

							if (packetFiltered.data)
								av_packet_unref(&packetFiltered);

							avformat_close_input(&formatCtx);
							return decode_error;
						}
					}
				}
			}
			else
			{
				if (packet.size > 0)
				{
					if (ctx->handler)
					{
						decode_error = ctx->handler->OnVideoRecv(ctx, packet.data, packet.size, frameindex, nFrameCount++);
						if (decode_error == ESMFFDemuxer::ERR_CODE_T::ERR_AGAIN)
							continue;
						else if (decode_error == ESMFFDemuxer::ERR_CODE_T::FINISH_DECODE)
							break;
						else if (decode_error != ESMFFDemuxer::ERR_CODE_T::SUCCESS)
						{
							if (packet.data)
								av_packet_unref(&packet);

							if (packetFiltered.data)
								av_packet_unref(&packetFiltered);

							avformat_close_input(&formatCtx);
							return decode_error;
						}
					}
				}
			}
		}
	}

	av_bsf_flush(bsfc);

	if (packet.data)
		av_packet_unref(&packet);

	if (packetFiltered.data)
		av_packet_unref(&packetFiltered);

	if (ctx->handler)
	{
		decode_error = ctx->handler->OnVideoEnd(ctx, frameindex, nFrameCount++);
		if (decode_error != ESMFFDemuxer::ERR_CODE_T::SUCCESS && decode_error != ESMFFDemuxer::ERR_CODE_T::FINISH_DECODE)
			return decode_error;
	}

	avformat_close_input(&formatCtx);
	return ESMFFDemuxer::ERR_CODE_T::SUCCESS;
}