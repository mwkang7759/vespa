﻿// TestBase.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

#include <iostream>

#include "SystemAPI.h"
#include "TraceAPI.h"
#include "SocketAPI.h"
#include "UtilAPI.h"

#include "StreamReaderAPI.h"
#include "StreamWriterAPI.h"
#include "DecoderAPI.h"
#include "EncoderAPI.h"
#include "ConverterAPI.h"

#include <array>

int main()
{
    // std::string 생성자 테스트
    /*std::string s(3, '\0');
    LOG_D("string data(%s)", s.c_str());
    std::cout << s << std::endl;*/

    std::array<int, 3> arr = { 9,8,7 }; // std:array 는 크기 고정
    std::cout << "Array size = " << arr.size() << std::endl;
    std::cout << "2nd element = " << arr[1] << std::endl;



    //LRSLT lRet;
    //DWORD32 dwStart = 0;
    //FrReaderHandle	hReader;
    //FrVideoDecHandle hDecVideo;
    //FrURLInfo	    UrlInfo;
    //FrMediaInfo     SrcInfo;

    //
    //FrMediaStream VideoData;
    //FrMediaStream EncStream;
    //FrRawVideo  DecVideo;
    //FrRawVideo  ScaleVideo;

    //memset(&EncStream, 0, sizeof(FrMediaStream));
    //memset(&VideoData, 0, sizeof(FrMediaStream));
    //VideoData.tMediaType = VIDEO_MEDIA;
    //EncStream.tMediaType = VIDEO_MEDIA;

    //UrlInfo.pUrl = (char*)"c:\\test_contents\\hevc_10102_0.mp4";

   

    //// reader
    //lRet = FrReaderOpen(&hReader, &UrlInfo);
    //if (FRFAILED(lRet)) {
    //    return -1;
    //}

    //lRet = FrReaderGetInfo(hReader, &SrcInfo);
    //if (FRFAILED(lRet)) {
    //    return -1;
    //}

    //// decoder
    //FrVideoInfo* pVideoInfo = &SrcInfo.FrVideo;
    //pVideoInfo->eColorFormat = VideoFmtYUV420P;     // output color format
    //pVideoInfo->eCodecType = CODEC_SW;
    //pVideoInfo->bLowDelayDecoding = TRUE;
    //lRet = FrVideoDecOpen(&hDecVideo, NULL, pVideoInfo);
    //if (FRFAILED(lRet)) {
    //    return -1;
    //}

    //lRet = FrReaderStart(hReader, &dwStart);
    //if (FRFAILED(lRet)) {
    //    return -1;
    //}

    //while (1) {
    //    lRet = FrReaderReadFrame(hReader, &VideoData);
    //    if (FRFAILED(lRet)) {
    //        if (lRet == COMMON_ERR_ENDOFDATA)
    //            break;

    //        FrSleep(10);
    //        continue;
    //    }

    //    lRet = FrVideoDecDecode(hDecVideo, &VideoData, &DecVideo);
    //    if (FRFAILED(lRet)) {
    //        continue;
    //    }
    //    
    //    if (DecVideo.dwSrcCnt) {
    //        break;
    //    }
    //}

    //if (hDecVideo != nullptr) {
    //    FrVideoDecClose(hDecVideo);
    //    hDecVideo = nullptr;
    //}

    //if (hReader != nullptr) {
    //    FrReaderClose(hReader);
    //    hReader = nullptr;
    //}

    return 1;
}
