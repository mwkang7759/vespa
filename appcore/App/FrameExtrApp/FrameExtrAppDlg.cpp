﻿
// FrameExtrAppDlg.cpp: 구현 파일
//

#include "pch.h"
#include "framework.h"
#include "FrameExtrApp.h"
#include "FrameExtrAppDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CFrameExtrAppDlg 대화 상자



CFrameExtrAppDlg::CFrameExtrAppDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_FRAMEEXTRAPP_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CFrameExtrAppDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_DIR_PATH, m_editDirPath);
	DDX_Control(pDX, IDC_EDIT_FILE_PATH, m_editFilePath);
	DDX_Control(pDX, IDC_RB_DIR, m_rbDir);
	DDX_Control(pDX, IDC_RB_FILE, m_rbFile);
	DDX_Control(pDX, IDC_CB_OUTPUT_SIZE, m_cbOutputSize);
	DDX_Control(pDX, IDC_EDIT_OUTPUT_WIDTH, m_editOutputWidth);
	DDX_Control(pDX, IDC_EDIT_OUTPUT_HEIGHT, m_editOutputHeight);
}

BEGIN_MESSAGE_MAP(CFrameExtrAppDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &CFrameExtrAppDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CFrameExtrAppDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BTN_SET_PATH, &CFrameExtrAppDlg::OnBnClickedBtnSetPath)
	ON_BN_CLICKED(IDC_BTN_SET_FILE_PATH, &CFrameExtrAppDlg::OnBnClickedBtnSetFilePath)
	ON_BN_CLICKED(IDC_BTN_EXTRACT_FRAME, &CFrameExtrAppDlg::OnBnClickedBtnExtractFrame)
	ON_CBN_SELCHANGE(IDC_CB_OUTPUT_SIZE, &CFrameExtrAppDlg::OnCbnSelchangeCbOutputSize)
END_MESSAGE_MAP()


// CFrameExtrAppDlg 메시지 처리기

BOOL CFrameExtrAppDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	m_rbDir.SetCheck(TRUE);
	m_cbOutputSize.SetCurSel(0);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CFrameExtrAppDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 애플리케이션의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CFrameExtrAppDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CFrameExtrAppDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CFrameExtrAppDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialogEx::OnOK();
}


void CFrameExtrAppDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialogEx::OnCancel();
}


void CFrameExtrAppDlg::OnBnClickedBtnSetPath()
{
	CFolderPickerDialog dlg;

	if (dlg.DoModal() == IDOK)
	{
		CString strDirPath = dlg.GetPathName();

		m_editDirPath.SetWindowText(strDirPath);
	}
}


void CFrameExtrAppDlg::OnBnClickedBtnSetFilePath()
{
	static TCHAR BASED_CODE szFilter[] = _T("동영상 파일(*.MP4) | *.MP4;*.mp4 |모든파일(*.*)|*.*||");

	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, szFilter);
	
	if (dlg.DoModal() == IDOK)
	{
		CString strFilePath = dlg.GetPathName();

		m_editFilePath.SetWindowText(strFilePath);

		m_vcObj.open(std::string(CT2CA(strFilePath)));

		if (!m_vcObj.isOpened())
		{
			MessageBox(_T("Video open failed!"));
			return;
		}

		if(m_cbOutputSize.GetCurSel() == 0)
			UpdateOutputSize(m_vcObj.get(cv::CAP_PROP_FRAME_WIDTH), m_vcObj.get(cv::CAP_PROP_FRAME_HEIGHT));
	}
}

void CFrameExtrAppDlg::ExtractFrameFromDir(CString strPath)
{
	//검색 클래스
	CFileFind finder;

	//CFileFind는 파일, 디렉터리가 존재하면 TRUE 를 반환함
	BOOL bWorking = finder.FindFile(strPath + _T("\\*.*"));

	int nCnt = 0;
	while (bWorking)
	{
		//다음 파일 / 폴더 가 존재하면다면 TRUE 반환
		bWorking = finder.FindNextFile();
		//파일 일때
		if (finder.IsArchived())
		{
			//파일의 이름
			CString strFilePath = strPath + "\\" + finder.GetFileName();

			// 현재폴더 상위폴더 썸네일파일은 제외
			if (strFilePath == _T(".") ||
				strFilePath == _T("..") ||
				strFilePath == _T("Thumbs.db")) continue;

			if (strFilePath.Right(3) != "MP4" && strFilePath.Right(3) != "mp4")
				continue;

			cv::VideoCapture vc;
			vc.open(std::string(CT2CA(strFilePath)));

			if (!vc.isOpened())
				continue;

			cv::Mat frame;
			vc >> frame;
			vc.release();

			CString strJpgPath = strFilePath.Left(strFilePath.GetLength() - 3) + _T("png");

			if (!frame.empty())
			{
				cv::imwrite(std::string(CT2CA(strJpgPath)), frame);
				nCnt++;
			}
		}
	}

	CString strMsg;
	strMsg.Format(_T("Done! (cnt=%d)"), nCnt);
	MessageBox(strMsg);
}

void CFrameExtrAppDlg::ExtractFrameFromFile(CString strPath)
{
	CString strWidth, strHeight;
	m_editOutputWidth.GetWindowText(strWidth);
	m_editOutputHeight.GetWindowText(strHeight);
	
	m_nOutputWidth = _ttoi(strWidth);
	m_nOutputHeight = _ttoi(strHeight);

	CString strDir = strPath.Left(strPath.ReverseFind('\\'));

	int nIdx = 0;
	while (1)
	{
		cv::Mat frame;

		if (!m_vcObj.read(frame))
			break;

		CString strFileName;
		strFileName.Format(_T("%06d.jpg"), nIdx + 1);

		CString strFullPath = strPath.Left(strPath.ReverseFind('\\')) + "\\" + strFileName;

		cv::Mat res;
		if (frame.cols != m_nOutputWidth || frame.rows != m_nOutputHeight)
			cv::resize(frame, res, cv::Size(m_nOutputWidth, m_nOutputHeight));
		else
			res = frame;

		cv::imwrite(std::string(CT2CA(strFullPath)), res);

		nIdx++;
	}
	m_vcObj.release();

	CString strMsg;
	strMsg.Format(_T("Complete to save %d images!"), nIdx);
	MessageBox(strMsg);
}

void CFrameExtrAppDlg::UpdateOutputSize(int nWidth, int nHeight)
{
	m_nOutputWidth = nWidth;
	m_nOutputHeight = nHeight;

	CString strW, strH;
	strW.Format(_T("%d"), nWidth);
	strH.Format(_T("%d"), nHeight);
	m_editOutputWidth.SetWindowText(strW);
	m_editOutputHeight.SetWindowText(strH);
}


void CFrameExtrAppDlg::OnBnClickedBtnExtractFrame()
{
	if (m_rbDir.GetCheck())
	{
		CString strDirPath;
		m_editDirPath.GetWindowText(strDirPath);

		if (strDirPath.GetLength() <= 0)
			return;

		ExtractFrameFromDir(strDirPath);
	}
	else
	{
		CString strFilePath;
		m_editFilePath.GetWindowText(strFilePath);

		if (strFilePath.GetLength() <= 0)
			return;

		ExtractFrameFromFile(strFilePath);
	}
}


void CFrameExtrAppDlg::OnCbnSelchangeCbOutputSize()
{
	BOOL bEnable = FALSE;
	int nWidth, nHeight;

	switch (m_cbOutputSize.GetCurSel())
	{
	case 0:
		nWidth = m_vcObj.get(cv::CAP_PROP_FRAME_WIDTH);
		nHeight = m_vcObj.get(cv::CAP_PROP_FRAME_HEIGHT);
		break;
	case 1:
		nWidth = 3840;
		nHeight = 2160;
		break;
	case 2:
		nWidth = 1920;
		nHeight = 1080;
		break;
	case 3:
		nWidth = 960;
		nHeight = 540;
		break;
	case 4:
		bEnable = TRUE;
		break;
	}

	UpdateOutputSize(nWidth, nHeight);
	m_editOutputWidth.EnableWindow(bEnable);
	m_editOutputHeight.EnableWindow(bEnable);
}
