﻿//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// FrameExtrApp.rc에서 사용되고 있습니다.
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_FRAMEEXTRAPP_DIALOG         102
#define IDR_MAINFRAME                   128
#define IDC_BTN_SET_PATH                1000
#define IDC_EDIT_DIR_PATH               1001
#define IDC_BTN_EXTRACT_FRAME           1002
#define IDC_RB_DIR                      1003
#define IDC_RB_FILE                     1004
#define IDC_EDIT_FILE_PATH              1005
#define IDC_CB_OUTPUT_SIZE              1006
#define IDC_EDIT_OUTPUT_WIDTH           1007
#define IDC_BTN_SET_FILE_PATH           1008
#define IDC_EDIT_OUTPUT_HEIGHT          1009

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
