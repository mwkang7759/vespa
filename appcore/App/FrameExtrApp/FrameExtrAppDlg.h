﻿
// FrameExtrAppDlg.h: 헤더 파일
//

#pragma once


#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/cudaimgproc.hpp"
#include "opencv2/cudawarping.hpp"
#include "opencv2/cudaarithm.hpp"
#include "opencv2/videoio.hpp"

#if defined(_DEBUG)
#pragma comment(lib, "opencv_core440d.lib")
#pragma comment(lib, "opencv_highgui440d.lib")
#pragma comment(lib, "opencv_imgproc440d.lib")
#pragma comment(lib, "opencv_imgcodecs440d.lib")
#pragma comment(lib, "opencv_calib3d440d.lib")
#pragma comment(lib, "opencv_cudaimgproc440d.lib")
#pragma comment(lib, "opencv_cudawarping440d.lib")
#pragma comment(lib, "opencv_cudaarithm440d.lib")
#pragma comment(lib, "opencv_videoio440d.lib")
#else
#pragma comment(lib, "opencv_core440.lib")
#pragma comment(lib, "opencv_highgui440.lib")
#pragma comment(lib, "opencv_imgproc440.lib")
#pragma comment(lib, "opencv_imgcodecs440.lib")
#pragma comment(lib, "opencv_calib3d440.lib")
#pragma comment(lib, "opencv_cudaimgproc440.lib")
#pragma comment(lib, "opencv_cudawarping440.lib")
#pragma comment(lib, "opencv_cudaarithm440.lib")
#pragma comment(lib, "opencv_videoio440.lib")
#endif

// CFrameExtrAppDlg 대화 상자
class CFrameExtrAppDlg : public CDialogEx
{
// 생성입니다.
public:
	CFrameExtrAppDlg(CWnd* pParent = nullptr);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_FRAMEEXTRAPP_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();

	CEdit m_editDirPath;
	CEdit m_editFilePath;
	CButton m_rbDir;
	CButton m_rbFile;
	CComboBox m_cbOutputSize;
	CEdit m_editOutputWidth;
	CEdit m_editOutputHeight;

	afx_msg void OnBnClickedBtnSetPath();
	afx_msg void OnBnClickedBtnExtractFrame();
	afx_msg void OnBnClickedBtnSetFilePath();
	afx_msg void OnCbnSelchangeCbOutputSize();

	void ExtractFrameFromDir(CString strPath);
	void ExtractFrameFromFile(CString strPath);
	void UpdateOutputSize(int nWidth, int nHeight);

private:
	cv::VideoCapture m_vcObj;

	int m_nOutputWidth;
	int m_nOutputHeight;
};
