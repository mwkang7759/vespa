﻿
// MgHelperAppDlg.cpp: 구현 파일
//

#include "pch.h"
#include "framework.h"
#include "MgHelperApp.h"
#include "MgHelperAppDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/cudaimgproc.hpp"
#include "opencv2/cudawarping.hpp"
#include "opencv2/cudaarithm.hpp"

#if defined(_DEBUG)
#pragma comment(lib, "opencv_core440d.lib")
#pragma comment(lib, "opencv_highgui440d.lib")
#pragma comment(lib, "opencv_imgproc440d.lib")
#pragma comment(lib, "opencv_imgcodecs440d.lib")
#pragma comment(lib, "opencv_calib3d440d.lib")
#pragma comment(lib, "opencv_cudaimgproc440d.lib")
#pragma comment(lib, "opencv_cudawarping440d.lib")
#pragma comment(lib, "opencv_cudaarithm440d.lib")
#else
#pragma comment(lib, "opencv_core440.lib")
#pragma comment(lib, "opencv_highgui440.lib")
#pragma comment(lib, "opencv_imgproc440.lib")
#pragma comment(lib, "opencv_imgcodecs440.lib")
#pragma comment(lib, "opencv_calib3d440.lib")
#pragma comment(lib, "opencv_cudaimgproc440.lib")
#pragma comment(lib, "opencv_cudawarping440.lib")
#pragma comment(lib, "opencv_cudaarithm440.lib")
#endif

// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CMgHelperAppDlg 대화 상자



CMgHelperAppDlg::CMgHelperAppDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_MGHELPERAPP_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMgHelperAppDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CMgHelperAppDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BTN_START, &CMgHelperAppDlg::OnBnClickedBtnStart)
	ON_BN_CLICKED(IDC_BTN_STOP, &CMgHelperAppDlg::OnBnClickedBtnStop)
END_MESSAGE_MAP()


// CMgHelperAppDlg 메시지 처리기

BOOL CMgHelperAppDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	CString strData;

	//FrSetTraceFileName((char*)"mghelper_video.log");

	CWnd* pWnd = GetDlgItem(IDC_EDIT_URL);
	//strData = "rtsp://192.168.22.123:8554/main";
	//strData = "c:\\test_contents\\test.mp4";
	strData = "C:\\vspd_movie\\4dreplay_2021_08_24_10_21_38.mp4";

	pWnd->SetWindowText(strData);

	pWnd = GetDlgItem(IDC_BTN_STOP);
	pWnd->EnableWindow(FALSE);

	pWnd = GetDlgItem(IDC_BTN_START);
	pWnd->EnableWindow(TRUE);

	

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CMgHelperAppDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 애플리케이션의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CMgHelperAppDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CMgHelperAppDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CMgHelperAppDlg::OnBnClickedBtnStart()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strData;
	char szType[1024] = "";
	MgVideoParam param;

	m_pYUV = new BYTE[3840 * 2160 * 3];
	m_bEnd = false;

	CWnd* pWnd = GetDlgItem(IDC_EDIT_URL);
	pWnd->GetWindowText(strData);
	strcpy(m_szUrl, strData);

	
	param.codec_type = CODEC_FFMPEG;
	

	param.pUrl = m_szUrl;
	param.newWidth = 1920;
	param.newHeight = 1080;
	m_hMgVideo = FrMgVideoOpen(&param);
	FrMgVideoRegCallback(m_hMgVideo, GetRawVideoCallback, this);

	FrMgVideoStart(m_hMgVideo);

	m_pRawVQ = new RawVideoQue();
	m_pRawVQ->Init(size_t(3840 * 2160 * 3), 5);
	m_hDisplayThread = new std::thread(&CMgHelperAppDlg::DisplayThread, this);


	pWnd = GetDlgItem(IDC_BTN_START);
	pWnd->EnableWindow(FALSE);

	pWnd = GetDlgItem(IDC_BTN_STOP);
	pWnd->EnableWindow(TRUE);
}


void CMgHelperAppDlg::OnBnClickedBtnStop()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_bEnd = true;

	FrMgVideoStop(m_hMgVideo);
	FrMgVideoClose(m_hMgVideo);

	while (m_hDisplayThread != nullptr) {
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}


	delete m_pRawVQ;
	delete m_pYUV;

	CWnd* pWnd = GetDlgItem(IDC_BTN_STOP);
	pWnd->EnableWindow(FALSE);

	pWnd = GetDlgItem(IDC_BTN_START);
	pWnd->EnableWindow(TRUE);
}

void CMgHelperAppDlg::DisplayThread() {
	TRACE("ArServerThread() Begin..");

	while (!m_bEnd) {
		CbData tmpData = m_pRawVQ->GetData();
		if (!tmpData.pData) {
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
			continue;
		}
		// opencv processing and displaying..
		// ..
		std::vector<cv::Mat> vecImage, vecOutput(3);

		int lumaWidth = tmpData.width;
		int lumaHeight = tmpData.height;
		int chromaWidth = lumaWidth >> 1;
		int chromaHeight = lumaHeight >> 1;
		int lumaPitch = tmpData.pitch;
		int chromaPitch = lumaPitch >> 1;
		int dpYUVPitch = lumaPitch;

		unsigned char* yDeviceptr = (unsigned char*)tmpData.pData;
		unsigned char* uDeviceptr = yDeviceptr + lumaPitch * lumaHeight;
		unsigned char* vDeviceptr = uDeviceptr + chromaPitch * chromaHeight;

		/*unsigned char* yDeviceptr = (unsigned char*)tmpData.pV;
		unsigned char* uDeviceptr = (unsigned char*)tmpData.pU;
		unsigned char* vDeviceptr = (unsigned char*)tmpData.pV;*/

		cv::Mat gpuY(lumaHeight, lumaWidth, CV_8UC1, yDeviceptr, lumaPitch);
		cv::Mat gpuU(chromaHeight, chromaWidth, CV_8UC1, uDeviceptr, chromaPitch);
		cv::Mat gpuV(chromaHeight, chromaWidth, CV_8UC1, vDeviceptr, chromaPitch);

		vecImage.push_back(gpuY);
		vecImage.push_back(gpuU);
		vecImage.push_back(gpuV);

		vecImage[0].copyTo(vecOutput[0]);
		vecImage[1].copyTo(vecOutput[1]);
		vecImage[2].copyTo(vecOutput[2]);

		cv::resize(vecOutput[1], vecOutput[1], cv::Size(vecOutput[1].cols * 2, vecOutput[1].rows * 2), cv::InterpolationFlags::INTER_CUBIC);
		cv::resize(vecOutput[2], vecOutput[2], cv::Size(vecOutput[2].cols * 2, vecOutput[2].rows * 2), cv::InterpolationFlags::INTER_CUBIC);

		cv::Mat gResult;
		cv::merge(vecOutput, gResult);
		cv::cvtColor(gResult, gResult, cv::ColorConversionCodes::COLOR_YUV2BGR); //COLOR_YUV2BGR      = 84, //COLOR_YUV2RGB

		try
		{
			cv::imshow("tmp", gResult);
			cv::waitKey(1);
		}
		catch (cv::Exception& e)
		{
			std::string err_msg = e.what();
		}

		m_pRawVQ->Pop();
	}


	TRACE("ArServerThread() End..");

	m_hDisplayThread = nullptr;
}

void CALLBACK CMgHelperAppDlg::GetRawVideoCallback(void* phandle, void* pdata)
{
	CMgHelperAppDlg* pThis = (CMgHelperAppDlg*)phandle;
	CbData* pcbData = (CbData*)pdata;

	if (pcbData->codec_type == CODEC_NVIDIA) {
		std::vector<cv::cuda::GpuMat> vecGpuImage, vecGpuOutput(3);

		int lumaWidth = pcbData->width;
		int lumaHeight = pcbData->height;
		int chromaWidth = lumaWidth >> 1;
		int chromaHeight = lumaHeight >> 1;
		int lumaPitch = pcbData->pitch;
		int chromaPitch = lumaPitch >> 1;
		int dpYUVPitch = lumaPitch;

		unsigned char* yDeviceptr = (unsigned char*)pcbData->pData;
		unsigned char* uDeviceptr = yDeviceptr + lumaPitch * lumaHeight;
		unsigned char* vDeviceptr = uDeviceptr + chromaPitch * chromaHeight;

		cv::cuda::GpuMat gpuY(lumaHeight, lumaWidth, CV_8UC1, yDeviceptr, lumaPitch);
		cv::cuda::GpuMat gpuU(chromaHeight, chromaWidth, CV_8UC1, uDeviceptr, chromaPitch);
		cv::cuda::GpuMat gpuV(chromaHeight, chromaWidth, CV_8UC1, vDeviceptr, chromaPitch);

		vecGpuImage.push_back(gpuY);
		vecGpuImage.push_back(gpuU);
		vecGpuImage.push_back(gpuV);

		vecGpuImage[0].copyTo(vecGpuOutput[0]);
		vecGpuImage[1].copyTo(vecGpuOutput[1]);
		vecGpuImage[2].copyTo(vecGpuOutput[2]);

		cv::cuda::resize(vecGpuOutput[1], vecGpuOutput[1], cv::Size(vecGpuOutput[1].cols * 2, vecGpuOutput[1].rows * 2), cv::InterpolationFlags::INTER_CUBIC);
		cv::cuda::resize(vecGpuOutput[2], vecGpuOutput[2], cv::Size(vecGpuOutput[2].cols * 2, vecGpuOutput[2].rows * 2), cv::InterpolationFlags::INTER_CUBIC);

		cv::cuda::GpuMat gResult;
		cv::cuda::merge(vecGpuOutput, gResult);
		cv::cuda::cvtColor(gResult, gResult, cv::ColorConversionCodes::COLOR_YUV2RGB);
		cv::Mat mResult;
		gResult.download(mResult);

		try
		{
			//cv::imwrite("c:\\cv_out.bmp", mResult);
			cv::imshow("tmp", mResult);
			cv::waitKey(1);
		}
		catch (cv::Exception& e)
		{
			std::string err_msg = e.what();
		}
	}
	else {
		// ffmpeg..
		int pos = 0;
		pcbData->pData = pThis->m_pYUV;
		pcbData->size = pcbData->width * pcbData->height * 3 / 2;
		memcpy(pThis->m_pYUV + pos, pcbData->pY, pcbData->width * pcbData->height);
		pos += pcbData->width * pcbData->height;
		memcpy(pThis->m_pYUV + pos, pcbData->pU, pcbData->width * pcbData->height / 4);
		pos += pcbData->width * pcbData->height / 4;
		memcpy(pThis->m_pYUV + pos, pcbData->pV, pcbData->width * pcbData->height / 4);

		pThis->m_pRawVQ->Insert(pcbData); // will be stored by copy

	}
}


RawVideoQue::~RawVideoQue() {
	Finish();
}

int RawVideoQue::Init(size_t videoSize, int videoCount) {
	if (m_bInit)
		return -1;

	while (videoCount) {
		void* pBuf = nullptr;
		size_t pitch = 0;
		pBuf = malloc(videoSize);
		if (nullptr == pBuf)
			goto cleanup;
		std::lock_guard<std::mutex> lock(m_xPool);
		m_qPool.push(pBuf);
		--videoCount;
	}
	return 0;
cleanup:
	Finish();
	return -1;
}

void RawVideoQue::Finish() {
	std::lock_guard<std::mutex> lock(m_xPool);
	if (!m_bInit)
		return;

	while (m_qPool.size() > 0) {
		void* p = m_qPool.front();
		if (p) free(p);
		m_qPool.pop();
	}
}

void RawVideoQue::Insert(const CbData& cbData) {
	CbData tmpData = cbData;
	while (1) {
		void* pRaw = AllocMemPool();
		if (nullptr == pRaw) {
			//std::this_thread::sleep_for(1ms);
			std::this_thread::sleep_for(std::chrono::milliseconds(1));
			continue;
		}
		memcpy(pRaw, cbData.pData, cbData.size);
		tmpData.pData = (unsigned char*)pRaw;
		break;
	}
	std::lock_guard<std::mutex> lock(m_xData);
	m_qData.push(tmpData);
}

void RawVideoQue::Insert(const CbData* pcbData) {
	CbData tmpData;;
	while (1) {
		void* pRaw = AllocMemPool();
		if (nullptr == pRaw) {
			//std::this_thread::sleep_for(1ms);
			std::this_thread::sleep_for(std::chrono::milliseconds(1));
			continue;
		}
		memcpy(&tmpData, pcbData, sizeof(CbData));
		memcpy(pRaw, pcbData->pData, pcbData->size);
		tmpData.pData = (unsigned char*)pRaw;

		break;
	}
	std::lock_guard<std::mutex> lock(m_xData);
	m_qData.push(tmpData);
}

CbData RawVideoQue::GetData() {
	std::lock_guard<std::mutex> lock(m_xData);
	if (m_qData.size() > 0)
		return m_qData.front();
	return CbData{ 0, };
}

void RawVideoQue::Pop() {
	std::lock_guard<std::mutex> lock(m_xData);
	if (m_qData.size() > 0) {
		if (m_qData.front().pData)
			FreeMemPool(m_qData.front().pData);
		m_qData.pop();
	}
}

void* RawVideoQue::AllocMemPool() {
	std::lock_guard<std::mutex> lock(m_xPool);
	if (m_qPool.size() == 0)
		return nullptr;

	auto ret = m_qPool.front();
	m_qPool.pop();
	m_mUsing[ret] = 1;
	return ret;
}

void RawVideoQue::FreeMemPool(void* pBuf) {
	std::lock_guard<std::mutex> lock(m_xPool);
	if (m_mUsing.count(pBuf) > 0) {
		m_mUsing.erase(pBuf);
		m_qPool.push(pBuf);
	}
}
