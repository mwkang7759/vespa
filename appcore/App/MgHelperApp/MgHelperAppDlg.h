﻿
// MgHelperAppDlg.h: 헤더 파일
//

#pragma once

#include "MgVideoSrcAPI.h"

#include <queue>
#include <mutex>
#include <map>
#include <vector>
#include <chrono>
#include <thread>

class RawVideoQue {
public:
	RawVideoQue() : m_bInit(false) {}
	~RawVideoQue();

	int Init(size_t videoSize, int videoCount = 10);
	void Finish();
	void Insert(const CbData& cbData);
	void Insert(const CbData* pcbData);
	CbData GetData();
	void Pop();

private:
	void* AllocMemPool();
	void FreeMemPool(void* pBuf);

private:
	bool m_bInit;
	std::queue<CbData> m_qData;
	std::mutex m_xData;
	std::queue<void*> m_qPool;
	std::mutex m_xPool;
	std::map<void*, int> m_mUsing;

};

// CMgHelperAppDlg 대화 상자
class CMgHelperAppDlg : public CDialogEx
{
// 생성입니다.
public:
	CMgHelperAppDlg(CWnd* pParent = nullptr);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MGHELPERAPP_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	//FrMgHelper* m_hHelper;
	FrMgVideoHandle m_hMgVideo;
	char			m_szUrl[4096] = "";
	RawVideoQue* m_pRawVQ;

	std::thread* m_hDisplayThread = nullptr;
	bool m_bEnd = false;

	BYTE* m_pYUV = nullptr;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	static void CALLBACK GetRawVideoCallback(void* phandle, void* pdata);
	void DisplayThread();

	afx_msg void OnBnClickedBtnStart();
	afx_msg void OnBnClickedBtnStop();
};
