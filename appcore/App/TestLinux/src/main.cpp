
#include "ArTransServer.h"

#include <string>
#include <thread>

void taskBench(int num) {
    FrArServer* hArSvr;
    ArSvrParam svrParam;
    std::string outname = "test_out_" + std::to_string(num) + ".mp4";

    //printf("output name: (%s)\n", argv[1]);
    printf("output name: (%s)\n", outname.c_str());

    svrParam.url = "C:\\test_contents\\test.mp4";
    //svrParam.dstUrl = "test_out.mp4";
    svrParam.dstUrl = outname;
    svrParam.wSvrPort = 9900;
    svrParam.codec = "H.264";
    svrParam.codecType = "HW";
    svrParam.dwBitrate = 20 * 1024 * 1024;
    svrParam.dwGopLength = 12;      // 1
    svrParam.dwFramerate = 30;
    svrParam.dwWidth = 1920;
    svrParam.dwHeight = 1080;
    svrParam.dwInterval = 15;
    svrParam.wPushPort = 9900;
    //svrParam.bEnableStreaming = TRUE;
    svrParam.bEnableStreaming = FALSE;
    svrParam.bBenchTest = TRUE;


    printf("test 1\n");
    std::string logname = "test_linux_" + std::to_string(num) + ".log";
    printf("test 1.1\n");
    if (num == 1)
        FrSetTraceFileName((char*)"c:\\test_linux.log");
    //FrSetTraceFileName((char*)logname.c_str());

    printf("test 1.2\n");

    hArSvr = new FrArServer();
    printf("test 2\n");

    hArSvr->FrBenchTest(svrParam);

    printf("test 3\n");
}

int main() {

	//FrArServer* hArSvr;
	//ArSvrParam svrParam;
	//
	//svrParam.url = "C:\\test_contents\\test.mp4";
	//svrParam.dstUrl = "test_out.mp4";
	//svrParam.wSvrPort = 9900;
	//svrParam.codec = "H.264";
	//svrParam.codecType = "HW";
	//svrParam.dwBitrate = 20 * 1024 * 1024;
	//svrParam.dwGopLength = 12;	// 1
	//svrParam.dwFramerate = 30;
	//svrParam.dwWidth = 1920;
	//svrParam.dwHeight = 1080;
	//svrParam.dwInterval = 15;
	//svrParam.wPushPort = 9900;
	////svrParam.bEnableStreaming = TRUE;
	//svrParam.bEnableStreaming = FALSE;
	//svrParam.bBenchTest = TRUE;


	//printf("test 1\n");
	//FrSetTraceFileName((char*)"c:\\test_linux.log");
	//
	//
	//hArSvr = new FrArServer();
	//printf("test 2\n");



	//hArSvr->FrBenchTest(svrParam);

	//printf("test 3\n");

    std::thread t1(taskBench, 1);
    std::thread t2(taskBench, 2);
    std::thread t3(taskBench, 3);
    std::thread t4(taskBench, 4);

    t1.join();
    t2.join();
    t3.join();
    t4.join();
	

    char pStr[100];
    while (1)
    {
        memset(pStr, 0, 100);

        scanf("%s", &pStr);
        if (strcmp(pStr, "exit") == 0)
        {
            break;
        }        
    }


	return 1;
}
