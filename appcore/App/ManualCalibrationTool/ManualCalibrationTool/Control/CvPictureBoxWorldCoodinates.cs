﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FDUtilities
{
    public delegate void PointMsgHandlerWorld(CalPosWorld pos, float x, float y);

    public partial class CvPictureBoxScene : System.Windows.Forms.PictureBox
    {
        private PointMsgHandlerWorld SetWorldCoordinates;

        private FDDrawingManagerWorld dmWorld = new FDDrawingManagerWorld();

        private int ImageWidth = 0;
        private int ImageHeight = 0;

        private System.Drawing.Rectangle ImgRect = new System.Drawing.Rectangle();
        private float OffsetX { get; set; }
        private float OffsetY { get; set; }
        private float ScaleFactor = 1.0F;
        private float MinScale = 1.0F;

        private bool IsLBD = false;
        private System.Drawing.Point PointLBD = new System.Drawing.Point();

        private bool IsZoomIn = false;

        private readonly int MaxCountWorldCoordinates = 4;
        private readonly Color[] Colors = { Color.Red, Color.Yellow, Color.LightGreen, Color.Blue };

        private bool IsAutoSelect = true;
        private int IdxWorldCoordinates = 0;

        public CvPictureBoxScene()
        {
            InitializeComponent();

            this.MouseDown += MouseDownOnImage;
            this.MouseUp += MouseUpOnImage;
            this.MouseWheel += MouseWheelOnImage;
            this.MouseMove += MouseMoveOnImage;
        }
        protected override void OnPaint(PaintEventArgs pe)
        {
            //base.OnPaint(pe);

            if (this.Image != null)
            {
                pe.Graphics.DrawImage(this.Image, ImgRect);

                dmWorld.Draw(pe.Graphics, ScaleFactor, OffsetX, OffsetY);
            }
        }
        public void SetImage(Bitmap img, int w, int h)
        {
            this.Image = img;
            this.ImageWidth = w;
            this.ImageHeight = h;

            if ((float)ImageWidth / this.Width < (float)ImageHeight / this.Height)
                ScaleFactor = (float)this.Width / ImageWidth;
            else
                ScaleFactor = (float)this.Height / ImageHeight;

            MinScale = ScaleFactor;

            ImgRect.X = 0;
            ImgRect.Y = 0;
            ImgRect.Width = (int)(ImageWidth * ScaleFactor);
            ImgRect.Height = (int)(ImageHeight * ScaleFactor);

            OffsetX = 0;
            OffsetY = 0;
        }
        public void SetPointMsgHandlerWorld(PointMsgHandlerWorld handler)
        {
            SetWorldCoordinates = handler;
        }
        public void SetWorldCoords(WorldCoordinates4d pts)
        {
            dmWorld.WorldCoordinates.Add(new FDCross { PenType = new Pen(Colors[0], 2), X = pts.X1, Y = pts.Y1, Size = 15 });
            dmWorld.WorldCoordinates.Add(new FDCross { PenType = new Pen(Colors[1], 2), X = pts.X2, Y = pts.Y2, Size = 15 });
            dmWorld.WorldCoordinates.Add(new FDCross { PenType = new Pen(Colors[2], 2), X = pts.X3, Y = pts.Y3, Size = 15 });
            dmWorld.WorldCoordinates.Add(new FDCross { PenType = new Pen(Colors[3], 2), X = pts.X4, Y = pts.Y4, Size = 15 });
        }
        public void SetAutoSelect(bool mode)
        {
            IsAutoSelect = mode;
        }
        public void SetIdx(int idx)
        {
            IdxWorldCoordinates = idx;
        }
        private void AddWorldCoordinates(int posX, int posY)
        {
            int _posX, _posY;
            _posX = (int)Math.Round(OffsetX + posX / ScaleFactor);
            _posY = (int)Math.Round(OffsetY + posY / ScaleFactor);

            Pen pen = new Pen(Color.LightGreen, 2);


            if (dmWorld.WorldCoordinates.Count == MaxCountWorldCoordinates)
            {

                if (IsAutoSelect)
                {
                    int idxMin = -1;
                    float distMin = 500.0F;

                    for (int i = 0; i < MaxCountWorldCoordinates; i++)
                    {
                        float dist = (float)Math.Sqrt((_posX - dmWorld.WorldCoordinates[i].X) * (_posX - dmWorld.WorldCoordinates[i].X) + (_posY - dmWorld.WorldCoordinates[i].Y) * (_posY - dmWorld.WorldCoordinates[i].Y));

                        if (distMin > dist)
                        {
                            distMin = dist;
                            idxMin = i;
                        }
                    }

                    if (idxMin != -1)
                    {
                        dmWorld.WorldCoordinates[idxMin].X = _posX;
                        dmWorld.WorldCoordinates[idxMin].Y = _posY;
                    }
                }
                else
                {
                    dmWorld.WorldCoordinates[IdxWorldCoordinates] = new FDCross { PenType = new Pen(Colors[IdxWorldCoordinates], 2), X = _posX, Y = _posY, Size = 15 };

                    CalPosWorld pos = (new CalPosWorld[] { CalPosWorld.First, CalPosWorld.Second, CalPosWorld.Third, CalPosWorld.Forth })[IdxWorldCoordinates];
                    SetWorldCoordinates(pos, _posX, _posY);
                }
            }
            else
            {
                if (IsAutoSelect)
                {
                    dmWorld.WorldCoordinates.Add(new FDCross { PenType = new Pen(Colors[IdxWorldCoordinates], 2), X = _posX, Y = _posY, Size = 15 });
                    CalPosWorld pos = (new CalPosWorld[] { CalPosWorld.First, CalPosWorld.Second, CalPosWorld.Third, CalPosWorld.Forth })[IdxWorldCoordinates];
                    SetWorldCoordinates(pos, _posX, _posY);
                    IdxWorldCoordinates = (IdxWorldCoordinates + 1) % 4;
                }
                else
                {
                    if (IdxWorldCoordinates < dmWorld.WorldCoordinates.Count)
                    {
                        dmWorld.WorldCoordinates[IdxWorldCoordinates] = new FDCross { PenType = new Pen(Colors[IdxWorldCoordinates], 2), X = _posX, Y = _posY, Size = 15 };

                        CalPosWorld pos = (new CalPosWorld[] { CalPosWorld.First, CalPosWorld.Second, CalPosWorld.Third, CalPosWorld.Forth })[IdxWorldCoordinates];
                        SetWorldCoordinates(pos, _posX, _posY);
                    }
                }
            }
        }
        private void MouseDownOnImage(object sender, MouseEventArgs e)
        {
            System.Drawing.Point mouseDownLocation = new System.Drawing.Point(e.X, e.Y);

            switch (e.Button)
            {
                case MouseButtons.Left:
                    PointLBD = e.Location;
                    IsLBD = true;
                    break;
                case MouseButtons.Right:
                    if (!IsZoomIn)
                    {
                        ZoomView(10.0F, e.Location.X, e.Location.Y);
                        IsZoomIn = true;
                    }
                    else
                    {
                        AddWorldCoordinates(mouseDownLocation.X, mouseDownLocation.Y);

                        ZoomView(1.0F / 10.0F, e.Location.X, e.Location.Y);

                        IsZoomIn = false;
                    }
                    break;
                default:
                    break;
            }

            this.Focus();
            this.Invalidate();
        }
        private void MouseUpOnImage(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    IsLBD = false;
                    break;
                case MouseButtons.Right:
                    break;
                default:
                    break;
            }
        }

        private void ZoomView(float zoomRate, int centerX, int centerY)
        {
            ScaleFactor *= zoomRate;
            if (ScaleFactor > 100.0) ScaleFactor = 100.0F;
            else if (ScaleFactor < MinScale) ScaleFactor = MinScale;

            OffsetX += centerX * (zoomRate - 1.0F) / ScaleFactor;
            OffsetY += centerY * (zoomRate - 1.0F) / ScaleFactor;

            if (OffsetX < 0) OffsetX = 0;
            if (OffsetX > ImageWidth - this.Width / ScaleFactor)
                OffsetX = ImageWidth - this.Width / ScaleFactor;
            if (OffsetY < 0) OffsetY = 0;
            if (OffsetY > ImageHeight - this.Height / ScaleFactor)
                OffsetY = ImageHeight - this.Height / ScaleFactor;

            ImgRect.Width = (int)Math.Round(ImageWidth * ScaleFactor);
            ImgRect.Height = (int)Math.Round(ImageHeight * ScaleFactor);
            ImgRect.X = (int)Math.Round(-OffsetX * ScaleFactor);
            ImgRect.Y = (int)Math.Round(-OffsetY * ScaleFactor);

            Invalidate();
        }
        private void MouseWheelOnImage(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            float _scale = 1.0F;

            if (e.Delta > 0)
                _scale *= 1.25F;
            else if (e.Delta < 0)
                _scale /= 1.25F;

            ZoomView(_scale, e.Location.X, e.Location.Y);
        }
        private void MouseMoveOnImage(object sender, MouseEventArgs e)
        {
            float _cursorX, _cursorY;
            _cursorX = OffsetX + e.X / ScaleFactor;
            _cursorY = OffsetY + e.Y / ScaleFactor;

            if (IsLBD)
            {
                OffsetX = OffsetX - (int)Math.Round((double)(e.X - PointLBD.X) / ScaleFactor);
                if (OffsetX < 0) OffsetX = 0;
                if (OffsetX > ImageWidth - this.Width / ScaleFactor)
                    OffsetX = ImageWidth - this.Width / ScaleFactor;
                OffsetY = OffsetY - (int)Math.Round((double)(e.Y - PointLBD.Y) / ScaleFactor);
                if (OffsetY < 0) OffsetY = 0;
                if (OffsetY > ImageHeight - this.Height / ScaleFactor)
                    OffsetY = ImageHeight - this.Height / ScaleFactor;

                ImgRect.Width = (int)Math.Round(ImageWidth * ScaleFactor);
                ImgRect.Height = (int)Math.Round(ImageHeight * ScaleFactor);
                ImgRect.X = (int)Math.Round(-OffsetX * ScaleFactor);
                ImgRect.Y = (int)Math.Round(-OffsetY * ScaleFactor);

                PointLBD = e.Location;

                Invalidate();
            }
        }
    }
}
