﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FDUtilities
{
    public enum LogType
    {
        Info,
        Error,
        Warning,
        System
    }

    public partial class LogListView : System.Windows.Forms.ListView
    {
        public LogListView()
        {
            InitializeComponent();

            this.View = View.Details;
            this.GridLines = true;
            this.FullRowSelect = true;

            this.Columns.Add("Date", 120);
            this.Columns.Add("Type", 70);
            this.Columns.Add("Message", -2);
        }

        public void AddLogMessage(LogType type, string msg)
        {
            ListViewItem item = new ListViewItem(System.DateTime.Now.ToString());
            item.SubItems.Add(type.ToString());
            item.SubItems.Add(msg);

            this.Items.Add(item);

            this.Items[this.Items.Count - 1].EnsureVisible();
        }
    }
}
