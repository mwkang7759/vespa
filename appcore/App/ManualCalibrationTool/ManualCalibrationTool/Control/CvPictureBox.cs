﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FDUtilities
{
    public delegate void ZoomMsgHandler(float rate);
    public delegate void CursorMsgHandler(int idx, float x, float y);
    public delegate void PointMsgHandler2D(CalPos2D pos, float x, float y);
    public delegate void PointMsgHandler3D(CalPos3D pos, float x, float y);

    public partial class CvPictureBox : System.Windows.Forms.PictureBox
    {
        private CalibrationMode CalMode = CalibrationMode.Mode2D;

        private const int CursorSens = 5;

        private ZoomMsgHandler UpdateZoom;
        private CursorMsgHandler UpdateCursor;
        private PointMsgHandler2D SetPoint2D;
        private PointMsgHandler3D SetPoint3D;

        private FDDrawingManager2D dm2d = new FDDrawingManager2D();
        private FDDrawingManager3D dm3d = new FDDrawingManager3D();

        private int ImageWidth = 0;
        private int ImageHeight = 0;

        private System.Drawing.Rectangle ImgRect = new System.Drawing.Rectangle();
        private float OffsetX { get; set; }
        private float OffsetY { get; set; }
        private float ScaleFactor = 1.0F;
        private float MinScale = 1.0F;

        private bool IsLBD = false;
        private System.Drawing.Point PointLBD = new System.Drawing.Point();
        private int IdxMouseOn = -1;

        private bool IsZoomIn = false;

        private readonly int MaxCountPoint3D = 4;
        private readonly Color[] Colors = { Color.Red, Color.Yellow, Color.LightGreen, Color.Blue };

        private bool IsAutoSelect = true;
        private int IdxRefPoint3D = 0;

        public CvPictureBox()
        {
            InitializeComponent();

            this.MouseDown += MouseDownOnImage;
            this.MouseUp += MouseUpOnImage;
            this.MouseWheel += MouseWheelOnImage;
            this.MouseMove += MouseMoveOnImage;
            this.MouseDoubleClick += MouseDoubleClickOnImage;
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            //base.OnPaint(pe);

            if (this.Image != null)
            {
                pe.Graphics.DrawImage(this.Image, ImgRect);

                if(CalMode == CalibrationMode.Mode2D)
                    dm2d.Draw(pe.Graphics, ScaleFactor, OffsetX, OffsetY);
                else
                    dm3d.Draw(pe.Graphics, ScaleFactor, OffsetX, OffsetY);
            }
        }

        public void SetCalibrationMode(CalibrationMode mode)
        {
            CalMode = mode;
            this.Refresh();
        }

        public void SetImage(Bitmap img, int w, int h)
        {
            this.Image = img;
            this.ImageWidth = w;
            this.ImageHeight = h;

            if ((float)ImageWidth / this.Width < (float)ImageHeight / this.Height)
                ScaleFactor = (float) this.Width / ImageWidth;
            else
                ScaleFactor = (float) this.Height / ImageHeight;

            MinScale = ScaleFactor;

            if(UpdateZoom != null)
                UpdateZoom(ScaleFactor);

            ImgRect.X = 0;
            ImgRect.Y = 0;
            ImgRect.Width = (int) (ImageWidth * ScaleFactor);
            ImgRect.Height = (int) (ImageHeight * ScaleFactor);

            OffsetX = 0;
            OffsetY = 0;

            // Separator 설정
            Pen pen = new Pen(Color.Blue, 1);
            pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;

            dm2d.SeparatorUpper = new FDLine { PenType = pen, X1 = 0, Y1 = ImageHeight / 3, X2 = ImageWidth, Y2 = ImageHeight / 3 };
            dm2d.SeparatorLower = new FDLine { PenType = pen, X1 = 0, Y1 = ImageHeight * 2 / 3, X2 = ImageWidth, Y2 = ImageHeight * 2 / 3 };
        }
        public void SetReferencePoint(ReferencePoint2D points2d, ReferencePoint3D points3d)
        {
            Pen pen = new Pen(Color.LightGreen, 2);
            dm2d.ReferPointUpper = new FDCross { PenType = pen, X = points2d.UpperPosX, Y = points2d.UpperPosY, Size = 15 };
            dm2d.ReferPointMiddle = new FDCross { PenType = pen, X = points2d.MiddlePosX, Y = points2d.MiddlePosY, Size = 15 };
            dm2d.ReferPointLower = new FDCross { PenType = pen, X = points2d.LowerPosX, Y = points2d.LowerPosY, Size = 15 };

            dm3d.ReferPoints.Clear();
            if (points3d.X1 != -1 && points3d.Y1 != -1)
                dm3d.ReferPoints.Add(new FDCross { PenType = new Pen(Colors[0], 2), X = points3d.X1, Y = points3d.Y1, Size = 15 });
            if (points3d.X2 != -1 && points3d.Y2 != -1)
                dm3d.ReferPoints.Add(new FDCross { PenType = new Pen(Colors[1], 2), X = points3d.X2, Y = points3d.Y2, Size = 15 });
            if (points3d.X3 != -1 && points3d.Y3 != -1)
                dm3d.ReferPoints.Add(new FDCross { PenType = new Pen(Colors[2], 2), X = points3d.X3, Y = points3d.Y3, Size = 15 });
            if (points3d.X4 != -1 && points3d.Y4 != -1)
                dm3d.ReferPoints.Add(new FDCross { PenType = new Pen(Colors[3], 2), X = points3d.X4, Y = points3d.Y4, Size = 15 });
            dm3d.CenterPoint = new FDCross { PenType = new Pen(Color.White, 2), X = points3d.CenterX, Y = points3d.CenterY, Size = 15 };
        }
        public void SetZoomMsgHandler(ZoomMsgHandler handler)
        {
            UpdateZoom = handler;
        }
        public void SetCursorMsgHandler(CursorMsgHandler handler)
        {
            UpdateCursor = handler;
        }
        public void SetPointMsgHandler2D(PointMsgHandler2D handler)
        {
            SetPoint2D = handler;
        }
        public void SetPointMsgHandler3D(PointMsgHandler3D handler)
        {
            SetPoint3D = handler;
        }

        public void SetAutoSelect(bool mode)
        {
            IsAutoSelect = mode;
        }
        public void SetIdx(int idx)
        {
            IdxRefPoint3D = idx;
        }
        private void AddReferencePoint(int posX, int posY)
        {
            int _posX, _posY;
            _posX = (int)Math.Round(OffsetX + posX / ScaleFactor);
            _posY = (int)Math.Round(OffsetY + posY / ScaleFactor);

            Pen pen = new Pen(Color.LightGreen, 2);

            if (CalMode == CalibrationMode.Mode2D)
            {
                if (_posY < dm2d.SeparatorUpper.Y1)
                {
                    dm2d.ReferPointUpper = new FDCross { PenType = pen, X = _posX, Y = _posY, Size = 15 };
                    SetPoint2D(CalPos2D.Upper, _posX, _posY);
                }
                else if (_posY < dm2d.SeparatorLower.Y1)
                {
                    dm2d.ReferPointMiddle = new FDCross { PenType = pen, X = _posX, Y = _posY, Size = 15 };
                    SetPoint2D(CalPos2D.Middle, _posX, _posY);
                }
                else
                {
                    dm2d.ReferPointLower = new FDCross { PenType = pen, X = _posX, Y = _posY, Size = 15 };
                    SetPoint2D(CalPos2D.Lower, _posX, _posY);
                }
            }
            else
            {
                if (dm3d.ReferPoints.Count == MaxCountPoint3D)
                {

                    if (IsAutoSelect)
                    {
                        int idxMin = -1;
                        float distMin = 500.0F;

                        for (int i = 0; i < MaxCountPoint3D; i++)
                        {
                            float dist = (float)Math.Sqrt((_posX - dm3d.ReferPoints[i].X) * (_posX - dm3d.ReferPoints[i].X) + (_posY - dm3d.ReferPoints[i].Y) * (_posY - dm3d.ReferPoints[i].Y));

                            if (distMin > dist)
                            {
                                distMin = dist;
                                idxMin = i;
                            }
                        }

                        if (idxMin != -1)
                        {
                            dm3d.ReferPoints[idxMin].X = _posX;
                            dm3d.ReferPoints[idxMin].Y = _posY;
                        }
                    }
                    else
                    {
                        dm3d.ReferPoints[IdxRefPoint3D] = new FDCross { PenType = new Pen(Colors[IdxRefPoint3D], 2), X = _posX, Y = _posY, Size = 15 };

                        CalPos3D pos = (new CalPos3D[] { CalPos3D.First, CalPos3D.Second, CalPos3D.Third, CalPos3D.Forth })[IdxRefPoint3D];
                        SetPoint3D(pos, _posX, _posY);
                    }
                }
                else
                {
                    if (IsAutoSelect)
                    {
                        dm3d.ReferPoints.Add(new FDCross { PenType = new Pen(Colors[IdxRefPoint3D], 2), X = _posX, Y = _posY, Size = 15 });
                        CalPos3D pos = (new CalPos3D[] { CalPos3D.First, CalPos3D.Second, CalPos3D.Third, CalPos3D.Forth })[IdxRefPoint3D];
                        SetPoint3D(pos, _posX, _posY);
                        IdxRefPoint3D = (IdxRefPoint3D + 1) % 4;
                    }
                    else
                    {
                        if (IdxRefPoint3D < dm3d.ReferPoints.Count)
                        {
                            dm3d.ReferPoints[IdxRefPoint3D] = new FDCross { PenType = new Pen(Colors[IdxRefPoint3D], 2), X = _posX, Y = _posY, Size = 15 };

                            CalPos3D pos = (new CalPos3D[] { CalPos3D.First, CalPos3D.Second, CalPos3D.Third, CalPos3D.Forth })[IdxRefPoint3D];
                            SetPoint3D(pos, _posX, _posY);
                        }
                    }
                }
            }
        }
        private void SetCenterPoint(int posX, int posY)
        {
            int _posX, _posY;
            _posX = (int)Math.Round(OffsetX + posX / ScaleFactor);
            _posY = (int)Math.Round(OffsetY + posY / ScaleFactor);

            Pen pen = new Pen(Color.White, 2);

            if (CalMode == CalibrationMode.Mode3D)
            {
                dm3d.CenterPoint = new FDCross { PenType = pen, X = _posX, Y = _posY, Size = 15 };
                SetPoint3D(CalPos3D.Center, _posX, _posY);
            }
        }

        private void MouseDownOnImage(object sender, MouseEventArgs e)
        {
            System.Drawing.Point mouseDownLocation = new System.Drawing.Point(e.X, e.Y);

            switch (e.Button)
            {
                case MouseButtons.Left:
                    PointLBD = e.Location;
                    IsLBD = true;
                    break;
                case MouseButtons.Right:
                    if (!IsZoomIn)
                    {
                        ZoomView(30.0F, e.Location.X, e.Location.Y);
                        IsZoomIn = true;
                    }
                    else
                    {
                        AddReferencePoint(mouseDownLocation.X, mouseDownLocation.Y);

                        ZoomView(1.0F / 30.0F, e.Location.X, e.Location.Y);

                        IsZoomIn = false;
                    }
                    break;
                default:
                    break;
            }

            this.Focus();
            this.Invalidate();
        }
        private void MouseUpOnImage(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    IsLBD = false;
                    break;
                case MouseButtons.Right:
                    break;
                default:
                    break;
            }
        }
        private void MouseDoubleClickOnImage(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                System.Drawing.Point mouseDownLocation = new System.Drawing.Point(e.X, e.Y);

                if (CalMode == CalibrationMode.Mode3D)
                    SetCenterPoint(mouseDownLocation.X, mouseDownLocation.Y);

                this.Focus();
                this.Invalidate();
            }
        }
        public void ZoomIn()
        {
            ZoomView(1.25F, this.Width / 2, this.Height / 2);
        }
        public void ZoomOut()
        {
            ZoomView(1 / 1.25F, this.Width / 2, this.Height / 2);
        }

        private void ZoomView(float zoomRate, int centerX, int centerY)
        {
            ScaleFactor *= zoomRate;
            if (ScaleFactor > 100.0) ScaleFactor = 100.0F;
            else if (ScaleFactor < MinScale) ScaleFactor = MinScale;

            if (UpdateZoom != null)
                UpdateZoom(ScaleFactor);

            OffsetX += centerX * (zoomRate - 1.0F) / ScaleFactor;
            OffsetY += centerY * (zoomRate - 1.0F) / ScaleFactor;

            if (OffsetX < 0) OffsetX = 0;
            if (OffsetX > ImageWidth - this.Width / ScaleFactor)
                OffsetX = ImageWidth - this.Width / ScaleFactor;
            if (OffsetY < 0) OffsetY = 0;
            if (OffsetY > ImageHeight - this.Height / ScaleFactor)
                OffsetY = ImageHeight - this.Height / ScaleFactor;

            ImgRect.Width = (int)Math.Round(ImageWidth * ScaleFactor);
            ImgRect.Height = (int)Math.Round(ImageHeight * ScaleFactor);
            ImgRect.X = (int)Math.Round(-OffsetX * ScaleFactor);
            ImgRect.Y = (int)Math.Round(-OffsetY * ScaleFactor);

            Invalidate();
        }

        private void MouseWheelOnImage(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            float _scale = 1.0F;

            if (e.Delta > 0)
                _scale *= 1.25F;
            else if (e.Delta < 0)
                _scale /= 1.25F;

            ZoomView(_scale, e.Location.X, e.Location.Y);
        }
        private void MouseMoveOnImage(object sender, MouseEventArgs e)
        {
            float _cursorX, _cursorY;
            _cursorX = OffsetX + e.X / ScaleFactor;
            _cursorY = OffsetY + e.Y / ScaleFactor;

            if (UpdateCursor != null)
            {

                if (CalMode == CalibrationMode.Mode2D)
                {
                    UpdateCursor(-1, _cursorX, _cursorY);
                }
                else
                {
                    if (IsAutoSelect)
                    {
                        if (dm3d.ReferPoints.Count == 4)
                            UpdateCursor(-1, _cursorX, _cursorY);
                        else
                            UpdateCursor(dm3d.ReferPoints.Count, _cursorX, _cursorY);
                    }
                    else
                    {
                        UpdateCursor(IdxRefPoint3D, _cursorX, _cursorY);
                    }
                }
            }

            if (CalMode == CalibrationMode.Mode2D)
            {
                if (IsLBD)
                {
                    if (IdxMouseOn == 0)
                        dm2d.MoveUpperSeparator((int)Math.Round((double)(e.Y - PointLBD.Y) / ScaleFactor));
                    else if (IdxMouseOn == 1)
                        dm2d.MoveLowerSeparator((int)Math.Round((double)(e.Y - PointLBD.Y) / ScaleFactor));
                    else
                    {
                        OffsetX = OffsetX - (int)Math.Round((double)(e.X - PointLBD.X) / ScaleFactor);
                        if (OffsetX < 0) OffsetX = 0;
                        if (OffsetX > ImageWidth - this.Width / ScaleFactor)
                            OffsetX = ImageWidth - this.Width / ScaleFactor;
                        OffsetY = OffsetY - (int)Math.Round((double)(e.Y - PointLBD.Y) / ScaleFactor);
                        if (OffsetY < 0) OffsetY = 0;
                        if (OffsetY > ImageHeight - this.Height / ScaleFactor)
                            OffsetY = ImageHeight - this.Height / ScaleFactor;

                        ImgRect.Width = (int)Math.Round(ImageWidth * ScaleFactor);
                        ImgRect.Height = (int)Math.Round(ImageHeight * ScaleFactor);
                        ImgRect.X = (int)Math.Round(-OffsetX * ScaleFactor);
                        ImgRect.Y = (int)Math.Round(-OffsetY * ScaleFactor);
                    }

                    PointLBD = e.Location;

                    Invalidate();
                }
                else
                {
                    IdxMouseOn = GetSeparatorId(_cursorY);
                }

                if (IdxMouseOn == -1)
                    Cursor.Current = Cursors.Default;
                else
                    Cursor.Current = Cursors.SizeNS;
            }
            else
            {
                if (IsLBD)
                {
                    OffsetX = OffsetX - (int)Math.Round((double)(e.X - PointLBD.X) / ScaleFactor);
                    if (OffsetX < 0) OffsetX = 0;
                    if (OffsetX > ImageWidth - this.Width / ScaleFactor)
                        OffsetX = ImageWidth - this.Width / ScaleFactor;
                    OffsetY = OffsetY - (int)Math.Round((double)(e.Y - PointLBD.Y) / ScaleFactor);
                    if (OffsetY < 0) OffsetY = 0;
                    if (OffsetY > ImageHeight - this.Height / ScaleFactor)
                        OffsetY = ImageHeight - this.Height / ScaleFactor;

                    ImgRect.Width = (int)Math.Round(ImageWidth * ScaleFactor);
                    ImgRect.Height = (int)Math.Round(ImageHeight * ScaleFactor);
                    ImgRect.X = (int)Math.Round(-OffsetX * ScaleFactor);
                    ImgRect.Y = (int)Math.Round(-OffsetY * ScaleFactor);

                    PointLBD = e.Location;

                    Invalidate();
                }
            }
        }

        private int GetSeparatorId(float y)
        {
            if (dm2d.SeparatorUpper == null || dm2d.SeparatorLower == null)
                return -1;

            if (y > dm2d.SeparatorUpper.Y1 - CursorSens / ScaleFactor && y < dm2d.SeparatorUpper.Y1 + CursorSens / ScaleFactor)
                return 0;
            else if (y > dm2d.SeparatorLower.Y1 - CursorSens / ScaleFactor && y < dm2d.SeparatorLower.Y1 + CursorSens / ScaleFactor)
                return 1;
            
            return -1;
        }
    }
}
