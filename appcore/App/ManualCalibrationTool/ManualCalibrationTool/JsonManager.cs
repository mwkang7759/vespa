﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace FDUtilities
{
    public class JsonManager
    {
        /// <summary>
        /// Calibration 좌표들을 json 형식의 .pts 파일로 저장
        /// </summary>
        /// <param name="dscList"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static bool PointsToJsonFile(in StadiumType type, in WorldCoordinates4d pts,  in List<DscInfo> dscList, string path)
        {
            if (dscList.Count == 0)
                return false;

            var jObj = new JObject();
            var jArr = new JArray();

            jObj.Add("stadium", type.ToString());
            var jWorldCoords = JObject.FromObject(pts);
            jObj.Add("world_coords", jWorldCoords);

            foreach (DscInfo info in dscList)
            {
                var jDsc = new JObject();
                jDsc.Add("dsc_id", info.DscID);

                var jPoints2d = JObject.FromObject(info.Points2D);
                jDsc.Add("pts_2d", jPoints2d);

                var jPoints3d = JObject.FromObject(info.Points3D);
                jDsc.Add("pts_3d", jPoints3d);

                jArr.Add(jDsc);
            }
            jObj.Add("points", jArr);

            using (StreamWriter outputFile = new StreamWriter(path))
            {
                outputFile.WriteLine(jObj.ToString());
            }

            return true;
        }
        /// <summary>
        /// json 형식의 .pts 파일에서 Calibration 좌표들을 로드
        /// </summary>
        /// <param name="path"></param>
        /// <param name="dscList"></param>
        /// <returns></returns>
        public static bool JsonFileToPoints(string path, ref StadiumType type, ref WorldCoordinates4d pts, ref List<DscInfo> dscList)
        {
            if (dscList.Count == 0)
                return false;

            string strJson;
            using (StreamReader r = new StreamReader(path))
            {
                strJson = r.ReadToEnd();
            }

            if (strJson.Length == 0)
                return false;

            JObject jObj = JObject.Parse(strJson);

            type = jObj["stadium"].ToObject<StadiumType>();
            pts = jObj["world_coords"].ToObject<WorldCoordinates4d>();


            foreach (var jDsc in jObj["points"])
            {
                string strDsc = jDsc["dsc_id"].ToString();

                int nFound = -1;
                for (int i = 0; i < dscList.Count; i++)
                {
                    if(dscList[i].DscID == strDsc)
                    {
                        nFound = i;
                        break;
                    }
                }

                if (nFound == -1)
                    continue;

                dscList[nFound].Points2D = jDsc["pts_2d"].ToObject<ReferencePoint2D>();
                dscList[nFound].Points3D = jDsc["pts_3d"].ToObject<ReferencePoint3D>();
            }

            return true;
        }
        /// <summary>
        /// Adjust data 계산 결과를 json 형식의 .adj 파일로 저장
        /// </summary>
        /// <param name="dscList"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static bool AdjustToJsonFile(List<DscInfo> dscList, string path)
        {
            if (dscList.Count == 0)
                return false;

            var jObj = new JObject();
            var jArr = new JArray();

            foreach (DscInfo info in dscList)
            {
                var jDsc = new JObject();
                jDsc.Add("dsc_id", info.DscID);

                var jAdjust = JObject.FromObject(info.Adjust);
                jDsc.Add("adjust", jAdjust);

                jArr.Add(jDsc);
            }
            jObj.Add("adjust_list", jArr);

            using (StreamWriter outputFile = new StreamWriter(path))
            {
                outputFile.WriteLine(jObj.ToString());
            }

            return true;
        }
        /// <summary>
        /// Position swipe data를 json 형식의 파일로 저장
        /// </summary>
        /// <param name="nLength"></param>
        /// <param name="arrData"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static bool PositionSwipeToJsonFile(in int nLength, in byte[] arrData, string path)
        {
            var jObj = new JObject();
            
            var jPosSwp = new JObject();
            jPosSwp.Add("length", nLength);

            var jArr = new JArray();
            foreach (byte data in arrData)
            {
                jArr.Add(data);
            }
            jPosSwp.Add("position_swipe_data", jArr);

            jObj.Add("position_swipe", jPosSwp);

            using (StreamWriter outputFile = new StreamWriter(path))
            {
                outputFile.WriteLine(jObj.ToString());
            }

            return true;
        }
    }
}
