﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;
using OpenCvSharp;
using OpenCvSharp.Extensions;
//using CalculateCal;

namespace FDUtilities
{
	public enum StadiumType
	{
		BaseballHome,
		BaseballGround,
		BasketballHalf,
		BasketballGround,
		Boxing,
		IceLinkHalf,
		IceLink,
		SoccerHalf,
		Soccer,
		Taekwondo,
		TennisHalf,
		Tennis,
		Ufc,
		VolleyballHalf,
		VolleyballGround,
		Football
	};

	/// <summary>
	/// 임시로 사용할 DSC 정보
	/// </summary>
	public class DscInfo
	{
		public DscInfo()
		{
			Width = 0;
			Height = 0;
			IsReg = false;
			IsAdj = false;
			IsStd = false;
			IsReverse = false;
		}
		public String DscID { get; set; }
		public Mat SrcImg { get; set; }
		public int Width { get; set; }
		public int Height { get; set; }
		public bool IsReg { get; set; }
		public bool IsAdj { get; set; }
		public bool IsStd { get; set; }
		public bool IsReverse { get; set; }

		public ReferencePoint2D Points2D = new ReferencePoint2D();
		public ReferencePoint3D Points3D = new ReferencePoint3D();
		public PreCalibrationData PreCalcData = new PreCalibrationData();

		public AdjustInfo Adjust = new AdjustInfo();
	}

	/// <summary>
	/// 기준봉 또는 가상 기준봉에 대한 Input 정보
	/// </summary>
	public class ReferencePoint2D
	{
		public ReferencePoint2D()
		{
			UpperPosX = -1.0F;
			UpperPosY = -1.0F;
			MiddlePosX = -1.0F;
			MiddlePosY = -1.0F;
			LowerPosX = -1.0F;
			LowerPosY = -1.0F;
		}
		public double UpperPosX { get; set; }
		public double UpperPosY { get; set; }
		public double MiddlePosX { get; set; }
		public double MiddlePosY { get; set; }
		public double LowerPosX { get; set; }
		public double LowerPosY { get; set; }
	}
	/// <summary>
	/// 바닥면의 4 points 정보
	/// </summary>
	public class ReferencePoint3D
	{
		public ReferencePoint3D()
		{
			X1 = -1.0F;
			Y1 = -1.0F;
			X2 = -1.0F;
			Y2 = -1.0F;
			X3 = -1.0F;
			Y3 = -1.0F;
			X4 = -1.0F;
			Y4 = -1.0F;
			CenterX = -1.0F;
			CenterY = -1.0F;
		}
		public double X1 { get; set; }
		public double Y1 { get; set; }
		public double X2 { get; set; }
		public double Y2 { get; set; }
		public double X3 { get; set; }
		public double Y3 { get; set; }
		public double X4 { get; set; }
		public double Y4 { get; set; }
		public double CenterX { get; set; }
		public double CenterY { get; set; }
	}
	/// <summary>
	/// 3D Calibration시 임시로 저장할 데이터
	/// </summary>
	public class PreCalibrationData
	{
		public PreCalibrationData()
		{
			Norm = 0.0;
			Degree = 0.0;
			RotMatrix = new Mat();
		}
		public double Norm { get; set; }
		public double Degree { get; set; }
		public double Radian { get; set; }
		public Mat RotMatrix { get; set; }
	}
	/// <summary>
	/// 기준 좌표로 사용할 월드 좌표 4개
	/// </summary>
	public class WorldCoordinates4d
	{
		public WorldCoordinates4d()
		{
			X1 = -1.0F;
			Y1 = -1.0F;
			X2 = -1.0F;
			Y2 = -1.0F;
			X3 = -1.0F;
			Y3 = -1.0F;
			X4 = -1.0F;
			Y4 = -1.0F;
		}
		public double X1 { get; set; }
		public double Y1 { get; set; }
		public double X2 { get; set; }
		public double Y2 { get; set; }
		public double X3 { get; set; }
		public double Y3 { get; set; }
		public double X4 { get; set; }
		public double Y4 { get; set; }
	}
	/// <summary>
	/// 영상에 적용될 Adjust 정보
	/// </summary>
	public class AdjustInfo
	{
		public double AdjustX { get; set; }
		public double AdjustY { get; set; }
		public double Angle { get; set; }
		public double RotateX { get; set; }
		public double RotateY { get; set; }
		public double Scale { get; set; }

		public System.Drawing.RectangleF RectMargin { get; set; }
	}
	public class Pair<T, U>
	{
		public Pair()
		{
		}
		public Pair(T first, U second)
		{
			this.First = first;
			this.Second = second;
		}
		public T First { get; set; }
		public U Second { get; set; }
	};

	public class FDAdjust
	{
		public static int MaxRange = 100;
		private static void Normalization3DSquarePoint(ref List<Point3f> ptSquarePoint, int nMaxRange)
		{
			float fMin_x = (float)ptSquarePoint[0].X;
			float fMin_y = (float)ptSquarePoint[0].Y;
			float fMax_x = (float)ptSquarePoint[0].X;
			float fMax_y = (float)ptSquarePoint[0].Y;

			foreach (Point3f pt in ptSquarePoint)
			{
				if (fMax_x < pt.X)
					fMax_x = pt.X;
				if (fMax_y < pt.Y)
					fMax_y = pt.Y;
				if (fMin_x > pt.X)
					fMin_x = pt.X;
				if (fMin_y > pt.Y)
					fMin_y = pt.Y;
			}

			float fRange;
			float fMargin_X = 0;
			float fMargin_Y = 0;

			if ((fMax_x - fMin_x) > (fMax_y - fMin_y))
			{
				fRange = nMaxRange / (fMax_x - fMin_x);
				fMargin_Y = (nMaxRange - ((fMax_y - fMin_y) * (fRange))) / 2;
			}
			else
			{
				fRange = nMaxRange / (fMax_y - fMin_y);
				fMargin_X = (nMaxRange - ((fMax_x - fMin_x) * (fRange))) / 2;
			}

			for (int i = 0; i < ptSquarePoint.Count; i++)
			{
				Point3f ptNew = new Point3f();
				ptNew.X = (ptSquarePoint[i].X - fMin_x) * fRange + fMargin_X;
				ptNew.Y = (ptSquarePoint[i].Y - fMin_y) * fRange + fMargin_Y;
				ptNew.Z = ptSquarePoint[i].Z;

				ptSquarePoint[i] = ptNew;
			}
		}
		/// <summary>
		/// 회전된 좌표 계산
		/// </summary>
		/// <param name="ptCenter"></param>
		/// <param name="ptRot"></param>
		/// <param name="dbAngle"></param>
		/// <returns></returns>
		public static Point2f GetRotatePoint(Point2f ptCenter, Point2f ptRot, double dbAngle)
		{
			Point2f ptResult = new Point2f();

			// 회전 중심좌표와의 상대좌표
			ptRot.X = ptRot.X - ptCenter.X;
			ptRot.Y = -(ptRot.Y - ptCenter.Y);

			double cosX = Math.Cos(dbAngle);
			double sinX = Math.Sin(dbAngle);

			ptResult.X = (float)(ptRot.X * cosX - ptRot.Y * sinX);
			ptResult.Y = (float)(ptRot.X * sinX + ptRot.Y * cosX);

			ptResult.X = ptResult.X + ptCenter.X;
			ptResult.Y = -(ptResult.Y - ptCenter.Y);

			return ptResult;
		}

		/// <summary>
		/// 3D Calibration의 4점 좌표를 통해 각 카메라별 기준 center 위치 지정
		/// </summary>
		/// <param name="inList"></param>
		/// <param name="outList"></param>
		/// <returns></returns>
		public static bool TrackCenterPoint(ref List<DscInfo> dscList, int idx, Point2d ptCenter)
		{
			List<Point2d> corners = new List<Point2d>();
			corners.Add(new Point2d(dscList[idx].Points3D.X1, dscList[idx].Points3D.Y1));
			corners.Add(new Point2d(dscList[idx].Points3D.X2, dscList[idx].Points3D.Y2));
			corners.Add(new Point2d(dscList[idx].Points3D.X3, dscList[idx].Points3D.Y3));
			corners.Add(new Point2d(dscList[idx].Points3D.X4, dscList[idx].Points3D.Y4));
			//Console.WriteLine("TrackCenterPoint dscid {0}, ptCenter {1}, {2}", idx, ptCenter.X, ptCenter.Y);
			// Reg된 idx 쌍을 탐색하여 추가
			for (int i = idx + 1; i < dscList.Count; i++)
			{
				ReferencePoint3D pts = dscList[i].Points3D;

				if (pts.X1 == -1 || pts.Y1 == -1 || pts.X2 == -1 || pts.Y2 == -1 || pts.X3 == -1 || pts.Y3 == -1 || pts.X4 == -1 || pts.Y4 == -1)
					continue;

				List<Point2d> cornersNew = new List<Point2d>();
				cornersNew.Add(new Point2d(dscList[i].Points3D.X1, dscList[i].Points3D.Y1));
				cornersNew.Add(new Point2d(dscList[i].Points3D.X2, dscList[i].Points3D.Y2));
				cornersNew.Add(new Point2d(dscList[i].Points3D.X3, dscList[i].Points3D.Y3));
				cornersNew.Add(new Point2d(dscList[i].Points3D.X4, dscList[i].Points3D.Y4));

				Mat h = Cv2.FindHomography(corners, cornersNew);

				Mat<double> mCenterPoint = new Mat<double>(3, 1);


				mCenterPoint.At<double>(0) = ptCenter.X;
				mCenterPoint.At<double>(1) = ptCenter.Y;
				mCenterPoint.At<double>(2) = 1;

				Mat mResult = h * mCenterPoint;

				dscList[i].Points3D.CenterX = mResult.At<double>(0) / mResult.At<double>(2);
				dscList[i].Points3D.CenterY = mResult.At<double>(1) / mResult.At<double>(2);
				//Console.WriteLine("TrackCenterPoint dscid {0}, ptCenter output {1}, {2}", i, dscList[i].Points3D.CenterX, dscList[i].Points3D.CenterY);
			}

			return true;
		}
		/// <summary>
		/// 3D Calibration의 4점 좌표와 World 좌표를 통해 예비 계산 결과 생성
		/// </summary>
		/// <param name="wc"></param>
		/// <param name="dscList"></param>
		/// <returns></returns>
        /// 
		public static bool CalcPreValue3DCalbration(in WorldCoordinates4d wc, ref List<DscInfo> dscList)
		{
			//CalCali.Cal3D(wc, dscList);
			if (wc.X1 == -1 || wc.Y1 == -1 || wc.X2 == -1 || wc.Y2 == -1 || wc.X3 == -1 || wc.Y3 == -1 || wc.X4 == -1 || wc.Y4 == -1)
				return false;

			List<Point3f> ptsSquare3d = new List<Point3f>();
			ptsSquare3d.Add(new Point3f((float)wc.X1, (float)wc.Y1, 0));
			ptsSquare3d.Add(new Point3f((float)wc.X2, (float)wc.Y2, 0));
			ptsSquare3d.Add(new Point3f((float)wc.X3, (float)wc.Y3, 0));
			ptsSquare3d.Add(new Point3f((float)wc.X4, (float)wc.Y4, 0));

			Normalization3DSquarePoint(ref ptsSquare3d, 100);



			List<Pair<int, int>> regList = new List<Pair<int, int>>();

			// Reg된 idx 쌍을 탐색하여 추가
			for (int i = 0; i < dscList.Count; i++)
			{
				dscList[i].IsAdj = false;
				if (dscList[i].IsReg)
				{
					Pair<int, int> idxPair = new Pair<int, int>(-1, -1);

					idxPair.First = i;

					for (int j = i + 1; j < dscList.Count; j++)
					{
						if (dscList[j].IsReg)
						{
							idxPair.Second = j;
							break;
						}
					}

					if (idxPair.First != -1 && idxPair.Second != -1 && idxPair.Second - idxPair.First > 1)
					{
						regList.Add(idxPair);

						i = idxPair.Second - 1;
					}
				}
			}

			for (int i = 0; i < regList.Count; i++)
			{
				int cnt = regList[i].Second - regList[i].First + 1;

				double dFirstNorm = 0;

				for (int j = regList[i].First; j <= regList[i].Second; j++)
				{
					double dSensorSize = 17.30;

					if (dscList[j].Width == 3840)
						dSensorSize = 17.30 / 1.35;
					else
						dSensorSize = 17.30;

					double dFocal;
					double dFocalLength = 20;
					if (dFocalLength <= 0)
					{
						dFocal = dscList[j].Width;
					}
					else
					{
						dFocal = (dFocalLength / dSensorSize) * dscList[j].Width;
					}

					var mCameraParameter = new float[,] {
						{(float)dFocal, 0, dscList[j].Width / 2 },
						{ 0, (float)dFocal, dscList[j].Height / 2 },
						{ 0, 0, 1 }
					};
					var mDistCoeffs = new float[] { 0, 0, 0, 0 };

					List<Point2f> pts2d = new List<Point2f>();
					pts2d.Add(new Point2f((float)dscList[j].Points3D.X1, (float)dscList[j].Points3D.Y1));
					pts2d.Add(new Point2f((float)dscList[j].Points3D.X2, (float)dscList[j].Points3D.Y2));
					pts2d.Add(new Point2f((float)dscList[j].Points3D.X3, (float)dscList[j].Points3D.Y3));
					pts2d.Add(new Point2f((float)dscList[j].Points3D.X4, (float)dscList[j].Points3D.Y4));

					Mat _mRotationMatrix = new Mat(3, 1, MatType.CV_32F);
					Mat _mTranslationMatrix = new Mat(3, 1, MatType.CV_32F);

					Cv2.SolvePnP(
						new Mat(ptsSquare3d.Count, 3, MatType.CV_32F, ptsSquare3d.ToArray()),
						new Mat(pts2d.Count, 2, MatType.CV_32F, pts2d.ToArray()),
						new Mat(3, 3, MatType.CV_32F, mCameraParameter),
						new Mat(4, 1, MatType.CV_32F, mDistCoeffs),
						_mRotationMatrix, _mTranslationMatrix, false, SolvePnPFlags.Iterative);

					var normalVector = new float[,] { { 50.0F, 50.0F, 0.0F }, { 50.0F, 50.0F, -30.0F } };

					Mat projectedNormalVector = new Mat();
					Cv2.ProjectPoints(new Mat(2, 3, MatType.CV_32F, normalVector), _mRotationMatrix, _mTranslationMatrix, new Mat(3, 3, MatType.CV_32F, mCameraParameter), new Mat(4, 1, MatType.CV_32F, mDistCoeffs), projectedNormalVector);

					Vec2f vec0 = projectedNormalVector.At<Vec2f>(0);
					Vec2f vec1 = projectedNormalVector.At<Vec2f>(1);

					Point2f ptAngleVector = new Point2f(vec1[0] - vec0[0], vec1[1] - vec0[1]);
					double midx = (vec0[0] + vec1[0]) / 2;
					double midy = (vec0[1] + vec1[1]) / 2;
					//Console.WriteLine("normalVector{0} {1},     {2},{3} {4},{5} {6},{7}", i, j, vec0[0], vec0[1], midx, midy, vec1[0], vec1[1]);
					//Console.WriteLine("anglevector {0} {1}", ptAngleVector.X, ptAngleVector.Y);

					double dNorm = Cv2.Norm(vec0, vec1);
					//Console.WriteLine("norm .. {0} {1}", j, dNorm);
					double dDegree = Cv2.FastAtan2(ptAngleVector.X, ptAngleVector.Y);

					if (vec1[1] > vec0[1])
					{
						dDegree = 360.0 - dDegree;
						if (dDegree >= 360)
							dDegree -= 360;
					}
					else
					{
						dDegree = 180.0 - dDegree;
					}

					if (j == regList[i].First)
						dFirstNorm = dNorm;

					Mat mRotMatrix = Cv2.GetRotationMatrix2D(
						new Point2f((float)dscList[j].Points3D.CenterX, (float)dscList[j].Points3D.CenterY),
						dDegree, dFirstNorm / dNorm);

					dscList[j].PreCalcData.Norm = dNorm;
					dscList[j].PreCalcData.Degree = dDegree;
					dscList[j].PreCalcData.RotMatrix = mRotMatrix;
				}
			}

			return true;
		}
		/// <summary>
		/// 3D Calibration의 데이터들을 통해 Adjust data 계산
		/// </summary>
		/// <param name="dscList"></param>
		/// <returns></returns>
		public static bool CalcAdjustData3D(ref List<DscInfo> dscList)
		{
			List<Pair<int, int>> regList = new List<Pair<int, int>>();

			// Reg된 idx 쌍을 탐색하여 추가
			for (int i = 0; i < dscList.Count; i++)
			{
				dscList[i].IsAdj = false;
				if (dscList[i].IsReg)
				{
					Pair<int, int> idxPair = new Pair<int, int>(i, -1);

					for (int j = i + 1; j < dscList.Count; j++)
					{
						if (dscList[j].IsReg)
						{
							idxPair.Second = j;
							break;
						}
					}

					if (idxPair.First != -1 && idxPair.Second != -1 && idxPair.Second - idxPair.First > 1)
					{
						regList.Add(idxPair);

						i = idxPair.Second - 1;
					}
				}
			}

			for (int i = 0; i < regList.Count; i++)
			{
				int cnt = regList[i].Second - regList[i].First + 1;

				double distStart = dscList[regList[i].First].PreCalcData.Norm;

				double distEnd = dscList[regList[i].Second].PreCalcData.Norm;

				// Start-End 화면의 기준봉 distance 차이에 대한 interval value 계산
				double interval = (distEnd - distStart) / (cnt - 1);

				// 평균 중심 좌표 계산
				double sumX = 0, sumY = 0, avgX, avgY;
				for (int j = regList[i].First; j <= regList[i].Second; j++)
				{
					sumX += dscList[j].Points3D.CenterX;
					sumY += dscList[j].Points3D.CenterY;
				}

				avgX = sumX / cnt;
				avgY = sumY / cnt;
				//Console.WriteLine("\n avg {0}, {1}", avgX, avgY);
				// Adjust data 계산
				for (int j = regList[i].First; j <= regList[i].Second; j++)
				{
					dscList[j].Adjust.Angle = -dscList[j].PreCalcData.Degree;
					if (dscList[j].Adjust.Angle >= -180)
						dscList[j].Adjust.Angle += -90;
					else
						dscList[j].Adjust.Angle += 270;


					double targetDist = distStart + (interval * (j - regList[i].First));

					dscList[j].Adjust.Scale = targetDist / dscList[j].PreCalcData.Norm;

					dscList[j].Adjust.AdjustX = avgX - dscList[j].Points3D.CenterX;
					dscList[j].Adjust.AdjustY = avgY - dscList[j].Points3D.CenterY;
					dscList[j].Adjust.RotateX = dscList[j].Points3D.CenterX;
					dscList[j].Adjust.RotateY = dscList[j].Points3D.CenterY;
					Console.WriteLine("j == {0} adj {1}, {2} center {3}, {4} Scale {5} Radian {6}", j, dscList[j].Adjust.AdjustX, dscList[j].Adjust.AdjustY,
						dscList[j].Points3D.CenterX, dscList[j].Points3D.CenterY, dscList[j].Adjust.Scale, dscList[j].Adjust.Angle);
				}


				#region Margin 계산

				List<RectangleF> lstMinRect = new List<RectangleF>();
				List<float> left = new List<float>();
				List<float> right = new List<float>();
				List<float> top = new List<float>();
				List<float> bottom = new List<float>();
				int nWidth, nHeight;
				nWidth = dscList[0].Width;
				nHeight = dscList[0].Height;

				RectangleF rtMinRect = new RectangleF(0, 0, dscList[regList[i].First].Width, dscList[regList[i].First].Height);

				for (int j = regList[i].First; j <= regList[i].Second; j++)
				{
					if (dscList[j].Adjust.AdjustX == 0 || dscList[j].Adjust.AdjustY == 0 || dscList[j].Points3D.CenterX == -1 || dscList[j].Points3D.CenterY == -1)
					{
						dscList[j].Adjust.RectMargin = new RectangleF(0, 0, 0, 0);
						continue;
					}

					Point2f pt1, pt2, pt3, pt4, ptR1, ptR2, ptR3, ptR4;

					Point2f ptCenter = new Point2f((float)dscList[j].Adjust.RotateX, (float)dscList[j].Adjust.RotateY);
					//Console.WriteLine("ptCenter {0} {1}", dscList[j].Adjust.RotateX, dscList[j].Adjust.RotateY);


					double dbAngleAdjust = -1 * (dscList[j].Adjust.Angle + 90);
					double dbRAngle = dbAngleAdjust * Math.PI / 180;

					// Get Resize Rect Point
					pt1.X = (float)(ptCenter.X * (1 - dscList[j].Adjust.Scale));
					pt1.Y = (float)(ptCenter.Y * (1 - dscList[j].Adjust.Scale));

					pt2.X = (float)(pt1.X + nWidth * dscList[j].Adjust.Scale);
					pt2.Y = pt1.Y;

					pt3.X = pt2.X;
					pt3.Y = (float)(pt1.Y + nHeight * dscList[j].Adjust.Scale);

					pt4.X = pt1.X;
					pt4.Y = pt3.Y;

					// Console.WriteLine("scale initial rect {0} {1} {2} {3}", pt1, pt2, pt3, pt4);
					ptR1 = FDAdjust.GetRotatePoint(ptCenter, pt1, dbRAngle);
					ptR2 = FDAdjust.GetRotatePoint(ptCenter, pt2, dbRAngle);
					ptR3 = FDAdjust.GetRotatePoint(ptCenter, pt3, dbRAngle);
					ptR4 = FDAdjust.GetRotatePoint(ptCenter, pt4, dbRAngle);
					// Console.WriteLine("---rotated rect {0} {1} {2} {3}", ptR1, ptR2, ptR3, ptR4);
					int nNewMarginLeft = 0;
					int nNewMarginTop = 0;
					int nNewMarginRight = nWidth;
					int nNewMarginBottom = nHeight;

					if (ptR1.X + dscList[j].Adjust.AdjustX > nNewMarginLeft)
						nNewMarginLeft = (int)(ptR1.X + dscList[j].Adjust.AdjustX);
					if (ptR1.Y + dscList[j].Adjust.AdjustY > nNewMarginTop)
						nNewMarginTop = (int)(ptR1.Y + dscList[j].Adjust.AdjustY);

					if (ptR2.X + dscList[j].Adjust.AdjustX < nNewMarginRight)
						nNewMarginRight = (int)(ptR2.X + dscList[j].Adjust.AdjustX);
					if (ptR2.Y + dscList[j].Adjust.AdjustY > nNewMarginTop)
						nNewMarginTop = (int)(ptR2.Y + dscList[j].Adjust.AdjustY);

					if (ptR3.X + dscList[j].Adjust.AdjustX < nNewMarginRight)
						nNewMarginRight = (int)(ptR3.X + dscList[j].Adjust.AdjustX);
					if (ptR3.Y + dscList[j].Adjust.AdjustY < nNewMarginBottom)
						nNewMarginBottom = (int)(ptR3.Y + dscList[j].Adjust.AdjustY);

					if (ptR4.X + dscList[j].Adjust.AdjustX > nNewMarginLeft)
						nNewMarginLeft = (int)(ptR4.X + dscList[j].Adjust.AdjustX);
					if (ptR4.Y + dscList[j].Adjust.AdjustY < nNewMarginBottom)
						nNewMarginBottom = (int)(ptR4.Y + dscList[j].Adjust.AdjustY);


					Console.WriteLine("-----nNewmargin1 left {0} top {1} right {2} bottom {3}", nNewMarginLeft, nNewMarginTop, nNewMarginRight, nNewMarginBottom);
					// Margin 뒤집힘 현상 임시 방지

					if (nNewMarginLeft > nNewMarginRight)
					{
						int nTemp = nNewMarginLeft;
						nNewMarginLeft = nNewMarginRight;
						nNewMarginRight = nTemp;
					}
					if (nNewMarginTop > nNewMarginBottom)
					{
						int nTemp = nNewMarginTop;
						nNewMarginTop = nNewMarginBottom;
						nNewMarginBottom = nTemp;
					}

					left.Add(nNewMarginLeft);
					right.Add(nNewMarginRight);
					top.Add(nNewMarginTop);
					bottom.Add(nNewMarginBottom);

					Console.WriteLine("--------add list left {0} top {1} width {2} height {3}", nNewMarginLeft, nNewMarginTop, nNewMarginRight, nNewMarginBottom);
				}

				left.Sort(delegate (float a, float b) { return b.CompareTo(a); });
				right.Sort(delegate (float a, float b) { return a.CompareTo(b); });
				top.Sort(delegate (float a, float b) { return b.CompareTo(a); });
				bottom.Sort(delegate (float a, float b) { return a.CompareTo(b); });

				// for (int k = 0; k < left.Count; k++)
				// {
				// 	Console.WriteLine("left {0} {1}",k, left[k]);
				// 	Console.WriteLine("right{0} {1}",k, right[k]);
				// 	Console.WriteLine("top  {0} {1}",k, top[k]);
				// 	Console.WriteLine("bottom{0} {1}", k, bottom[k]);
				// }

				int nMinTop = (int)top[0];
				int nMinLeft = (int)left[0];
				int nMinRight = (int)right[0];
				int nMinBottom = (int)bottom[0];
				Console.WriteLine("-------- list1 left {0} top {1} right {2} bottom {3}", nMinLeft, nMinTop, nMinRight, nMinBottom);

				if (nMinLeft > nMinTop * nWidth / nHeight)
					nMinTop = nMinLeft * nHeight / nWidth;
				else
					nMinLeft = nMinTop * nWidth / nHeight;

				if (nMinRight < nMinBottom * nWidth / nHeight)
					nMinBottom = nMinRight * nHeight / nWidth;
				else
					nMinRight = nMinBottom * nWidth / nHeight;

				Console.WriteLine("-------- list2 left {0} top {1} right {2} bottom {3}", nMinLeft, nMinTop, nMinRight, nMinBottom);

				rtMinRect = new RectangleF(nMinLeft, nMinTop, nMinRight - nMinLeft, nMinBottom - nMinTop);
				Console.WriteLine(" ---------------------- Final rect {0}", rtMinRect);

				for (int j = regList[i].First; j <= regList[i].Second; j++)
				{
					dscList[j].Adjust.RectMargin = rtMinRect;
					dscList[j].IsAdj = true;
				}
				#endregion
			}

			return true;
		}
		/// <summary>
		/// 2D Calibration의 기준봉 또는 가상기준봉 좌표들을 통해 Adjust data 계산
		/// </summary>
		/// <param name="inList"></param>
		/// <param name="outList"></param>
		/// <returns></returns>
		public static bool CalcAdjustData2D(ref List<DscInfo> dscList)
		{
			List<Pair<int, int>> regList = new List<Pair<int, int>>();

			// Reg된 idx 쌍을 탐색하여 추가
			for (int i = 0; i < dscList.Count; i++)
			{
				dscList[i].IsAdj = false;
				if (dscList[i].IsReg)
				{
					Pair<int, int> idxPair = new Pair<int, int>(i, -1);

					for (int j = i + 1; j < dscList.Count; j++)
					{
						if (dscList[j].IsReg)
						{
							idxPair.Second = j;
							break;
						}
					}

					if (idxPair.First != -1 && idxPair.Second != -1)
					{
						regList.Add(idxPair);

						i = idxPair.Second - 1;
					}
				}
			}

			for (int i = 0; i < regList.Count; i++)
			{
				int cnt = regList[i].Second - regList[i].First + 1;

				double distStart = 0;
				double diffStartX = (dscList[regList[i].First].Points2D.LowerPosX - dscList[regList[i].First].Points2D.UpperPosX);
				double diffStartY = (dscList[regList[i].First].Points2D.LowerPosY - dscList[regList[i].First].Points2D.UpperPosY);
				if (diffStartX == 0.0F)
					distStart = diffStartY;
				else
					distStart = Math.Sqrt((diffStartX * diffStartX) + (diffStartY * diffStartY));

				double distEnd = 0;
				double diffEndX = (dscList[regList[i].Second].Points2D.LowerPosX - dscList[regList[i].Second].Points2D.UpperPosX);
				double diffEndY = (dscList[regList[i].Second].Points2D.LowerPosY - dscList[regList[i].Second].Points2D.UpperPosY);
				if (diffEndX == 0.0F)
					distEnd = diffEndY;
				else
					distEnd = Math.Sqrt((diffEndX * diffEndX) + (diffEndY * diffEndY));

				// Start-End 화면의 기준봉 distance 차이에 대한 interval value 계산
				double interval = (distEnd - distStart) / (cnt - 1);

				// 평균 중심 좌표 계산
				double sumX = 0, sumY = 0, avgX, avgY;
				for (int j = regList[i].First; j <= regList[i].Second; j++)
				{
					sumX += dscList[j].Points2D.MiddlePosX;
					sumY += dscList[j].Points2D.MiddlePosY;
				}

				avgX = sumX / cnt;
				avgY = sumY / cnt;
				Console.WriteLine("2D avg {0} {1}", avgX, avgY);

				// Adjust data 계산
				for (int j = regList[i].First; j <= regList[i].Second; j++)
				{
					dscList[j].Adjust.Angle = -Math.Atan2((double)(dscList[j].Points2D.LowerPosY - dscList[j].Points2D.UpperPosY),
						(double)(dscList[j].Points2D.LowerPosX - dscList[j].Points2D.UpperPosX)) * (180 / Math.PI);

					double dist = 0;
					double diffX = (dscList[j].Points2D.LowerPosX - dscList[j].Points2D.UpperPosX);
					double diffY = (dscList[j].Points2D.LowerPosY - dscList[j].Points2D.UpperPosY);
					if (diffX == 0.0F)
						dist = diffY;
					else
						dist = Math.Sqrt((diffX * diffX) + (diffY * diffY));

					double targetDist = distStart + (interval * (j - regList[i].First));

					dscList[j].Adjust.AdjustX = avgX - dscList[j].Points2D.MiddlePosX;
					dscList[j].Adjust.AdjustY = avgY - dscList[j].Points2D.MiddlePosY;
					dscList[j].Adjust.Scale = targetDist / dist;
					dscList[j].Adjust.RotateX = dscList[j].Points2D.MiddlePosX;
					dscList[j].Adjust.RotateY = dscList[j].Points2D.MiddlePosY;
					Console.WriteLine("2D Adjust {0},{1} AdjustRotate {2},{3}", dscList[j].Adjust.AdjustX, dscList[j].Adjust.AdjustY,
						dscList[j].Adjust.RotateX, dscList[j].Adjust.RotateY);
				}

				// Margin 계산
				#region

				RectangleF rtMinRect = new RectangleF(0, 0, (int)dscList[0].Width, (int)dscList[0].Height);

				int nMinTop = 0;
				int nMinLeft = 0;
				int nMinBottom = (int)dscList[0].Width;
				int nMinRight = (int)dscList[0].Height;

				for (int j = regList[i].First; j <= regList[i].Second; j++)
				{
					if (dscList[j].Adjust.AdjustX == 0 || dscList[j].Adjust.AdjustY == 0)
					{
						dscList[j].Adjust.RectMargin = new RectangleF(0, 0, 0, 0);
						continue;
					}

					int nWidth, nHeight;
					Point2f pt1, pt2, pt3, pt4, ptR1, ptR2, ptR3, ptR4;

					Point2f ptCenter = new Point2f((float)dscList[j].Adjust.RotateX, (float)dscList[j].Adjust.RotateY);

					nWidth = dscList[j].Width;
					nHeight = dscList[j].Height;

					double dbAngleAdjust = -1 * (dscList[j].Adjust.Angle + 90);
					double dbRAngle = dbAngleAdjust * Math.PI / 180;

					// Get Resize Rect Point
					pt1.X = (float)(ptCenter.X * (1 - dscList[j].Adjust.Scale));
					pt1.Y = (float)(ptCenter.Y * (1 - dscList[j].Adjust.Scale));

					pt2.X = (float)(pt1.X + nWidth * dscList[j].Adjust.Scale);
					pt2.Y = pt1.Y;

					pt3.X = pt2.X;
					pt3.Y = (float)(pt1.Y + nHeight * dscList[j].Adjust.Scale);

					pt4.X = pt1.X;
					pt4.Y = pt3.Y;

					ptR1 = FDAdjust.GetRotatePoint(ptCenter, pt1, dbRAngle);
					ptR2 = FDAdjust.GetRotatePoint(ptCenter, pt2, dbRAngle);
					ptR3 = FDAdjust.GetRotatePoint(ptCenter, pt3, dbRAngle);
					ptR4 = FDAdjust.GetRotatePoint(ptCenter, pt4, dbRAngle);

					int nNewMarginLeft = 0;
					int nNewMarginTop = 0;
					int nNewMarginRight = nWidth;
					int nNewMarginBottom = nHeight;

					if (ptR1.X + dscList[j].Adjust.AdjustX > nNewMarginLeft)
						nNewMarginLeft = (int)(ptR1.X + dscList[j].Adjust.AdjustX);
					if (ptR1.Y + dscList[j].Adjust.AdjustY > nNewMarginTop)
						nNewMarginTop = (int)(ptR1.Y + dscList[j].Adjust.AdjustY);

					if (ptR2.X + dscList[j].Adjust.AdjustX < nNewMarginRight)
						nNewMarginRight = (int)(ptR2.X + dscList[j].Adjust.AdjustX);
					if (ptR2.Y + dscList[j].Adjust.AdjustY > nNewMarginTop)
						nNewMarginTop = (int)(ptR2.Y + dscList[j].Adjust.AdjustY);

					if (ptR3.X + dscList[j].Adjust.AdjustX < nNewMarginRight)
						nNewMarginRight = (int)(ptR3.X + dscList[j].Adjust.AdjustX);
					if (ptR3.Y + dscList[j].Adjust.AdjustY < nNewMarginBottom)
						nNewMarginBottom = (int)(ptR3.Y + dscList[j].Adjust.AdjustY);

					if (ptR4.X + dscList[j].Adjust.AdjustX > nNewMarginLeft)
						nNewMarginLeft = (int)(ptR4.X + dscList[j].Adjust.AdjustX);
					if (ptR4.Y + dscList[j].Adjust.AdjustY < nNewMarginBottom)
						nNewMarginBottom = (int)(ptR4.Y + dscList[j].Adjust.AdjustY);

					if (nNewMarginLeft > nNewMarginTop * nWidth / nHeight)
						nNewMarginTop = nNewMarginLeft * nHeight / nWidth;
					else
						nNewMarginLeft = nNewMarginTop * nWidth / nHeight;

					if (nNewMarginRight < nNewMarginBottom * nWidth / nHeight)
						nNewMarginBottom = nNewMarginRight * nHeight / nWidth;
					else
						nNewMarginRight = nNewMarginBottom * nWidth / nHeight;

					if (nNewMarginLeft > nMinLeft && nNewMarginTop > nMinTop)
					{
						nMinLeft = nNewMarginLeft;
						nMinTop = nNewMarginTop;

						rtMinRect = new RectangleF(nMinLeft, nMinTop, rtMinRect.Right - nMinLeft, rtMinRect.Bottom - nMinTop);
					}

					if (nNewMarginRight < nMinRight && nNewMarginBottom < nMinBottom)
					{
						if (nNewMarginRight == 0 || nNewMarginBottom == 0)
							continue;

						nMinRight = nNewMarginRight;
						nMinBottom = nNewMarginBottom;

						rtMinRect = new RectangleF(rtMinRect.Left, rtMinRect.Top, nMinRight - rtMinRect.Left, nMinBottom - rtMinRect.Top);
					}
				}

				for (int j = regList[i].First; j <= regList[i].Second; j++)
				{
					dscList[j].Adjust.RectMargin = rtMinRect;
					dscList[j].IsAdj = true;
				}
				#endregion


			}


			return true;
		}

		public static bool DrawCalibrationBar(ref Mat img, in WorldCoordinates4d wc, in DscInfo dscInfo)
		{
			double dSensorSize = 17.30 / 1.35;
			double dFocal;
			double dFocalLength = 20;
			dFocal = (dFocalLength / dSensorSize) * dscInfo.Width;

			var mCameraParameter = new float[,] {
						{(float)dFocal, 0, dscInfo.Width / 2 },
						{ 0, (float)dFocal, dscInfo.Height / 2 },
						{ 0, 0, 1 }
					};


			var mDistCoeffs = new float[] { 0, 0, 0, 0 };

			List<Point3f> ptsSquare3d = new List<Point3f>();
			ptsSquare3d.Add(new Point3f((float)wc.X1, (float)wc.Y1, 0));
			ptsSquare3d.Add(new Point3f((float)wc.X2, (float)wc.Y2, 0));
			ptsSquare3d.Add(new Point3f((float)wc.X3, (float)wc.Y3, 0));
			ptsSquare3d.Add(new Point3f((float)wc.X4, (float)wc.Y4, 0));

			Normalization3DSquarePoint(ref ptsSquare3d, 100);

			List<Point2f> pts2d = new List<Point2f>();
			pts2d.Add(new Point2f((float)dscInfo.Points3D.X1, (float)dscInfo.Points3D.Y1));
			pts2d.Add(new Point2f((float)dscInfo.Points3D.X2, (float)dscInfo.Points3D.Y2));
			pts2d.Add(new Point2f((float)dscInfo.Points3D.X3, (float)dscInfo.Points3D.Y3));
			pts2d.Add(new Point2f((float)dscInfo.Points3D.X4, (float)dscInfo.Points3D.Y4));

			Mat _mRotationMatrix = new Mat(3, 1, MatType.CV_32F);
			Mat _mTranslationMatrix = new Mat(3, 1, MatType.CV_32F);

			Cv2.SolvePnP(
				new Mat(ptsSquare3d.Count, 3, MatType.CV_32F, ptsSquare3d.ToArray()),
				new Mat(pts2d.Count, 2, MatType.CV_32F, pts2d.ToArray()),
				new Mat(3, 3, MatType.CV_32F, mCameraParameter),
				new Mat(4, 1, MatType.CV_32F, mDistCoeffs),
				_mRotationMatrix, _mTranslationMatrix, false, SolvePnPFlags.Iterative);

			List<Point3f> normalVector = new List<Point3f>();
			int nLocation = FDAdjust.MaxRange / 2;
			int nLength = 3;
			normalVector.Add(new Point3f(nLocation, nLocation, 0));
			normalVector.Add(new Point3f(nLocation + nLength * 2, nLocation, 0));
			normalVector.Add(new Point3f(nLocation, nLocation + nLength * 2, 0));
			normalVector.Add(new Point3f(nLocation, nLocation, -nLength * 10));
			normalVector.Add(new Point3f(nLocation - nLength * 2, nLocation, 0));
			normalVector.Add(new Point3f(nLocation, nLocation - nLength * 2, 0));

			Mat projectedNormalVector = new Mat();
			Cv2.ProjectPoints(new Mat(normalVector.Count, 3, MatType.CV_32F, normalVector.ToArray()), _mRotationMatrix, _mTranslationMatrix, new Mat(3, 3, MatType.CV_32F, mCameraParameter), new Mat(4, 1, MatType.CV_32F, mDistCoeffs), projectedNormalVector);

			var colors = new Scalar[]
			{
				new Scalar(0, 0, 255),
				new Scalar(0, 255, 0),
				new Scalar(255, 0, 0),
				new Scalar(255, 255, 0),
				new Scalar(255, 0, 255)
			};

			for (int i = 1; i < projectedNormalVector.Rows; i++)
			{
				Vec2f vecA = projectedNormalVector.At<Vec2f>(0);
				Vec2f vecB = projectedNormalVector.At<Vec2f>(i);

				OpenCvSharp.Point ptA = new OpenCvSharp.Point(vecA[0], vecA[1]);
				OpenCvSharp.Point ptB = new OpenCvSharp.Point(vecB[0], vecB[1]);

				Cv2.Line(img, ptA, ptB, colors[i - 1], 3, LineTypes.AntiAlias, 0);
			}

			return true;
		}
		public static bool PreviewImage(in Mat src, ref Mat dst, OpenCvSharp.Size szDst, Mat rotMat, double dX, double dY, RectangleF rtMargin, bool bRotated = false)
		{
			if (szDst.Width == 0 || szDst.Height == 0 || rotMat.Empty())
				return false;

			if (bRotated)
			{
				Mat _r = Mat.Eye(3, 3, MatType.CV_32FC1);
				_r.Set<float>(0, 0, -1);
				_r.Set<float>(1, 1, -1);
				_r.Set<float>(0, 2, src.Cols);
				_r.Set<float>(1, 2, src.Rows);

				Mat _t = rotMat.CopyMakeBorder(0, 1, 0, 0, BorderTypes.Constant, 0);
				_t.Set<float>(2, 0, 1);

				Mat _result = _t * _r;

				rotMat = _result.SubMat(new Rect(0, 0, 3, 2));
			}


			Cv2.WarpAffine(src, dst, rotMat, src.Size());


			int nCutTop, nCutHeight, nCutLeft, nCutWidth;
			int nPasteTop = 0, nPasteBottom = 0, nPasteLeft = 0, nPasteRight = 0;

			if (dX > 0)
			{
				nCutLeft = 0;
				nCutWidth = (int)(src.Cols - dX);

				nPasteLeft = (int)dX;
			}
			else
			{
				nCutLeft = (int)-dX;
				nCutWidth = (int)(src.Cols + dX);

				nPasteRight = (int)-dX;
			}

			if (dY > 0)
			{
				nCutTop = 0;
				nCutHeight = (int)(src.Rows - dY);

				nPasteTop = (int)dY;
			}
			else
			{
				nCutTop = (int)-dY;
				nCutHeight = (int)(src.Rows + dY);

				nPasteBottom = (int)-dY;
			}

			Mat gMatCut = dst.SubMat(new Rect(nCutLeft, nCutTop, nCutWidth, nCutHeight));
			Mat gMatPaste = new Mat();
			Cv2.CopyMakeBorder(gMatCut, gMatPaste, nPasteTop, nPasteBottom, nPasteLeft, nPasteRight, BorderTypes.Constant, 0);
			gMatPaste.CopyTo(dst);

			Cv2.Resize(dst, dst, szDst);

			return true;
		}
		public static bool AdjustImage(in Mat src, ref Mat dst, OpenCvSharp.Size szDst, AdjustInfo adj, bool isFlip = false)
		{
			if (szDst.Width == 0 || szDst.Height == 0)
				szDst = src.Size();

			double dMoveX, dMoveY;
			dMoveX = adj.AdjustX;
			dMoveY = adj.AdjustY;

			double dRotateX, dRotateY;
			dRotateX = adj.RotateX;
			dRotateY = adj.RotateY;

			double dScale = adj.Scale;

			double dAngle = (adj.Angle + 90.0);
			double dRad = dAngle * Math.PI / 180.0;


			Mat m, mTfm, mRot, mTrn, mScale, mFlip, mMargin, mScaleOutput;

			MatrixCalculator.GetFlipMatrix(src.Cols, src.Height, isFlip, isFlip, out mFlip);
			MatrixCalculator.GetRotationMatrix((float)dRad, (float)dRotateX, (float)dRotateY, out mRot);
			MatrixCalculator.GetScaleMarix((float)dScale, (float)dScale, (float)dRotateX, (float)dRotateY, out mScale);			
			MatrixCalculator.GetTranslationMatrix((float)dMoveX, (float)dMoveY, out mTrn);
			MatrixCalculator.GetMarginMatrix(src.Cols, src.Rows, (int)adj.RectMargin.Left, (int)adj.RectMargin.Top, (int)adj.RectMargin.Width, (int)adj.RectMargin.Height, out mMargin);
			MatrixCalculator.GetScaleMarix((float)szDst.Width / src.Cols, (float)szDst.Height / src.Rows, out mScaleOutput);

			mTfm = mScaleOutput * mMargin* mTrn * mScale * mRot * mFlip;

			MatrixCalculator.ParseAffineMatrix(mTfm, out m);
			Console.WriteLine("[{0}] Apply Rad {1}, Scale {2} tran {3} {4} 2ndScale{5} last scale {6}", 1, dRad, dScale, dMoveX, dMoveY, szDst.Width/src.Cols, m.At<float>(1,1));

			Mat res = new Mat();

			if (!isFlip)
				src.Circle(new OpenCvSharp.Point(adj.RotateX, adj.RotateY), 10, Scalar.FromRgb(255, 0, 0), 5);
			else
				src.Circle(new OpenCvSharp.Point(src.Cols - adj.RotateX, src.Rows - adj.RotateY), 10, Scalar.FromRgb(255, 0, 0), 5);
		
			res = Mat.Zeros(szDst, src.Type());
			Cv2.WarpAffine(src, res, m, szDst, InterpolationFlags.Linear, BorderTypes.Constant);
			//Cv2.Rectangle(res, new Rect(new OpenCvSharp.Point((int)adj.RectMargin.Left/2, (int)adj.RectMargin.Top/2), 
			//			new OpenCvSharp.Size((int)adj.RectMargin.Width/2, (int)adj.RectMargin.Height/2)), new Scalar(125,200, 50), 3);
		

			dst = res;

			return true;
		}

		public static Mat AdjustImage(in Mat src, OpenCvSharp.Size szDst, AdjustInfo adj, bool isFlip = false)
		{
			if (szDst.Width == 0 || szDst.Height == 0)
				szDst = src.Size();

			double dMoveX, dMoveY;
			dMoveX = adj.AdjustX;
			dMoveY = adj.AdjustY;

			double dRotateX, dRotateY;
			dRotateX = adj.RotateX;
			dRotateY = adj.RotateY;

			double dScale = adj.Scale;

			double dAngle = (adj.Angle + 90.0);
			double dRad = dAngle * Math.PI / 180.0;


			Mat m, mTfm, mRot, mTrn, mScale, mFlip, mScaleOutput;

			MatrixCalculator.GetFlipMatrix(src.Cols, src.Height, isFlip, isFlip, out mFlip);
			MatrixCalculator.GetRotationMatrix((float)dRad, (float)dRotateX, (float)dRotateY, out mRot);
			MatrixCalculator.GetScaleMarix((float)dScale, (float)dScale, (float)dRotateX, (float)dRotateY, out mScale);
			MatrixCalculator.GetTranslationMatrix((float)dMoveX, (float)dMoveY, out mTrn);
			MatrixCalculator.GetScaleMarix((float)szDst.Width / src.Cols, (float)szDst.Height / src.Rows, out mScaleOutput);

			mTfm = mScaleOutput * mTrn * mScale * mRot * mFlip;

			MatrixCalculator.ParseAffineMatrix(mTfm, out m);
			Console.WriteLine("[{0}]Adjust -- Rad {}");

			Mat res = new Mat();

			res = Mat.Zeros(szDst, src.Type());
			Cv2.WarpAffine(src, res, m, szDst, InterpolationFlags.Linear, BorderTypes.Constant);

			Rect rt = new Rect((int)Math.Round(adj.RectMargin.X), (int)Math.Round(adj.RectMargin.Y), (int)Math.Round(adj.RectMargin.Width), (int)Math.Round(adj.RectMargin.Height));
			return res.SubMat(rt);
		}

		public static Point2f GetRotatedCoord(in Point2f ptSrc, in float deg)
		{
			float r = (float)(Cv2.PI / 180.0);
			float rad = deg * r;
			float s = (float)Math.Sin(rad);
			float c = (float)Math.Cos(rad);
			float x = ptSrc.X * c - (ptSrc.Y * s);
			float y = ptSrc.X * s + (ptSrc.Y * c);

			return new Point2f(x, y);
		}
		public static Point2f GetTransformCoord(in Point2f ptSrc, in float mx, in float my, in float scale, in float deg, in float rx, in float ry)
		{

			float r = (float)(Cv2.PI / 180.0);
			float rad = deg * r;
			float s = (float)Math.Sin(rad);
			float c = (float)Math.Cos(rad);
			float x = ((ptSrc.X - rx) * c - ((ptSrc.Y - ry) * s)) * scale + rx + mx;
			float y = ((ptSrc.X - rx) * s + ((ptSrc.Y - ry) * c)) * scale + ry + my;

			return new Point2f(x, y);
		}
	}
}
