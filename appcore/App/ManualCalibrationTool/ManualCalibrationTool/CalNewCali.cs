﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;
using OpenCvSharp;
using OpenCvSharp.Extensions;

namespace FDUtilities
{
	public class CalNewCali_3D
	{		
		public static int nCalibType = 1;
		public enum eCalibType {
			Avaerage = 0,
			Min = 1,
			Max = 2,
			Linear = 3,
			Halfway = 4
		};

		public static bool NormalizeWorldPt(in WorldCoordinates4d wc, int nMaxRange, ref List<Point3f> ptSquarePoint)
        {
			if (wc.X1 == -1 || wc.Y1 == -1 || wc.X2 == -1 || wc.Y2 == -1 || wc.X3 == -1 || wc.Y3 == -1 || wc.X4 == -1 || wc.Y4 == -1)
				return false;

			ptSquarePoint.Add(new Point3f((float)wc.X1, (float)wc.Y1, 0));
			ptSquarePoint.Add(new Point3f((float)wc.X2, (float)wc.Y2, 0));
			ptSquarePoint.Add(new Point3f((float)wc.X3, (float)wc.Y3, 0));
			ptSquarePoint.Add(new Point3f((float)wc.X4, (float)wc.Y4, 0));
			float fMin_x = (float)ptSquarePoint[0].X;
			float fMin_y = (float)ptSquarePoint[0].Y;
			float fMax_x = (float)ptSquarePoint[0].X;
			float fMax_y = (float)ptSquarePoint[0].Y;

			foreach (Point3f pt in ptSquarePoint) {
				if (fMax_x < pt.X)
					fMax_x = pt.X;
				if (fMax_y < pt.Y)
					fMax_y = pt.Y;
				if (fMin_x > pt.X)
					fMin_x = pt.X;
				if (fMin_y > pt.Y)
					fMin_y = pt.Y;
			}

			float fRange;
			float fMargin_X = 0;
			float fMargin_Y = 0;

			if ((fMax_x - fMin_x) > (fMax_y - fMin_y)) {
				fRange = nMaxRange / (fMax_x - fMin_x);
				fMargin_Y = (nMaxRange - ((fMax_y - fMin_y) * (fRange))) / 2;
			}
			else {
				fRange = nMaxRange / (fMax_y - fMin_y);
				fMargin_X = (nMaxRange - ((fMax_x - fMin_x) * (fRange))) / 2;
			}

			for (int i = 0; i < ptSquarePoint.Count; i++) {
				Point3f ptNew = new Point3f();
				ptNew.X = (ptSquarePoint[i].X - fMin_x) * fRange + fMargin_X;
				ptNew.Y = (ptSquarePoint[i].Y - fMin_y) * fRange + fMargin_Y;
				ptNew.Z = ptSquarePoint[i].Z;

				ptSquarePoint[i] = ptNew;
			}
			return true;
		}

		public static double ConvertFocal2Px(double dFocal, int width, int height)
		{
			double dSensorSize = 17.30;
			if (width == 3840)
				dSensorSize = 17.30 / 1.35;
			else
				dSensorSize = 17.30;

			double dFocalPx;
			if (dFocal <= 0)
				dFocalPx = width;
			else
				dFocalPx = (dFocal / dSensorSize) * width;
			return dFocalPx;
		}

		public static void CalculateNormDegree(List<Pair<int, int>> regList, List<Point3f> ptsSquare3d, ref List<DscInfo>dscList)
		{
			for (int i = 0; i < regList.Count; i++) {
				for (int j = regList[i].First; j <= regList[i].Second; j++) {

					double dFocal = 50; //insert from camera property
					double dFocalPx = ConvertFocal2Px(dFocal, dscList[j].Width, dscList[j].Height);
		
					var mCameraParameter = new float[,] {
								{(float)dFocalPx, 0, dscList[j].Width / 2 },
								{ 0, (float)dFocalPx, dscList[j].Height / 2 },
								{ 0, 0, 1 }
					};
					var mDistCoeffs = new float[] { 0, 0, 0, 0 };

					List<Point2f> pts2d = new List<Point2f>();
					pts2d.Add(new Point2f((float)dscList[j].Points3D.X1, (float)dscList[j].Points3D.Y1));
					pts2d.Add(new Point2f((float)dscList[j].Points3D.X2, (float)dscList[j].Points3D.Y2));
					pts2d.Add(new Point2f((float)dscList[j].Points3D.X3, (float)dscList[j].Points3D.Y3));
					pts2d.Add(new Point2f((float)dscList[j].Points3D.X4, (float)dscList[j].Points3D.Y4));

					Mat _mRotationMatrix = new Mat(3, 1, MatType.CV_32F);
					Mat _mTranslationMatrix = new Mat(3, 1, MatType.CV_32F);

					Cv2.SolvePnP(
						new Mat(ptsSquare3d.Count, 3, MatType.CV_32F, ptsSquare3d.ToArray()),
						new Mat(pts2d.Count, 2, MatType.CV_32F, pts2d.ToArray()),
						new Mat(3, 3, MatType.CV_32F, mCameraParameter),
						new Mat(4, 1, MatType.CV_32F, mDistCoeffs),
						_mRotationMatrix, _mTranslationMatrix, false, SolvePnPFlags.Iterative);

					var normalVector = new float[,] { { 50.0F, 50.0F, 0.0F }, { 50.0F, 50.0F, -50.0F } };

					Mat projectedNormalVector = new Mat();
					Cv2.ProjectPoints(new Mat(2, 3, MatType.CV_32F, normalVector), _mRotationMatrix, _mTranslationMatrix, new Mat(3, 3, MatType.CV_32F, mCameraParameter), new Mat(4, 1, MatType.CV_32F, mDistCoeffs), projectedNormalVector);

					Vec2f vec0 = projectedNormalVector.At<Vec2f>(0);
					Vec2f vec1 = projectedNormalVector.At<Vec2f>(1);

					Point2f ptAngleVector = new Point2f(vec1[0] - vec0[0], vec1[1] - vec0[1]);
					double dNorm = Cv2.Norm(vec0, vec1);
					double dDegree = Cv2.FastAtan2(ptAngleVector.Y, ptAngleVector.X);
					//Console.WriteLine("0 : {0} {1}, 1: {2} {3} -- {4}", vec0.Item0, vec0.Item1, vec1.Item0, vec1.Item1, dDegree);
					if(vec0[1] < vec1[1])
						dDegree = 90 - dDegree;
					else 
						dDegree = 270 - dDegree;

					dscList[j].PreCalcData.Norm = dNorm;
					dscList[j].PreCalcData.Degree = dDegree < 0 ? dDegree + 360 : dDegree; //degree counter clockwise
					dscList[j].PreCalcData.Radian = dDegree * Math.PI / 180; //radian
					dscList[j].Adjust.Angle = dscList[j].PreCalcData.Radian;
					Console.WriteLine("Norm {0} Angle(Rad) {1} ", dNorm, dscList[j].Adjust.Angle);
				}
			}	
		}

		public static void CalculateScalenCenter(List<Pair<int, int>> regList, List<Pair<int, int>> stdList, ref List<DscInfo>dscList) {
			for (int i = 0; i < regList.Count; i++) {
				double targetNorm = 0;
				double targetX = 0;
				double targetY = 0;
				double startNorm = 0;
				double startX = 0;
				double startY = 0;
				bool bAnchor = false;
				nCalibType = 3; //Avaerage = 0, Min = 1, Max = 2, Linear = 3, Halfway = 4
				
				if (nCalibType == 4) {
					stdList.Add(new Pair<int, int>(0, 15));
					if (stdList[i].First > -1) {
						bAnchor = true;
						targetNorm = dscList[stdList[i].Second].PreCalcData.Norm;
						targetX = dscList[stdList[i].Second].Points3D.CenterX;				
						targetY = dscList[stdList[i].Second].Points3D.CenterY;
						Console.WriteLine("anchor true. TargetNorm {0} X {1} {2} ", targetNorm, targetX, targetY);
					}
				}
				else if (nCalibType == 3) {
					double sumX = 0; double sumY = 0;
					double avX = 0; double avY = 0;
					int dsccnt = 0;					
					startNorm = dscList[regList[i].First].PreCalcData.Norm;
					startX = dscList[regList[i].First].Points3D.CenterX;				
					startY = dscList[regList[i].First].Points3D.CenterY;					
					targetNorm = dscList[regList[i].Second].PreCalcData.Norm;
					targetX = dscList[regList[i].Second].Points3D.CenterX;				
					targetY = dscList[regList[i].Second].Points3D.CenterY;
					for (int j = regList[i].First; j <= regList[i].Second; j++) {
						sumX += dscList[j].Points3D.CenterX;
						sumY += dscList[j].Points3D.CenterY;
						dsccnt++;
					}
					avX = sumX / dsccnt;
					avY = sumY / dsccnt;
					targetX = avX; targetY = avY;
					Console.Write("anchor false + linear mode");
				}
				else if( nCalibType == 0 ) {
					double dsumNorm = 0;
					double sumX = 0; double sumY = 0;
					double avX = 0; double avY = 0;
					int dsccnt = 0;
					for (int j = regList[i].First; j <= regList[i].Second; j++) {
						dsumNorm += dscList[j].PreCalcData.Norm;
						sumX += dscList[j].Points3D.CenterX;
						sumY += dscList[j].Points3D.CenterY;
						dsccnt++;
					}
					targetNorm = dsumNorm / (double)dsccnt;
					avX = sumX / (int)dsccnt;
					avY = sumY / (int)dsccnt;
					targetX = avX; targetY = avY;
					Console.WriteLine("anchor false + Average mode  TargetNorm {0} X {1} Y {2}", targetNorm, avX, avY);
				} else if ( nCalibType == 2) {
					double maxX = 0; double maxY = 0;
					double maxNorm = 0;
					for (int j = regList[i].First; j <= regList[i].Second; j++) {
						if(dscList[j].PreCalcData.Norm > maxNorm) {
							maxNorm = dscList[j].PreCalcData.Norm;
							maxX = dscList[j].Points3D.CenterX;
							maxY = dscList[j].Points3D.CenterY;
						}
					}
					targetNorm = maxNorm;
					targetX = maxX; targetY = maxY;
					Console.WriteLine("anchor false + max mode  TargetNorm {0} X {1} Y {2}", targetNorm, targetX, targetY);
				}
				else if ( nCalibType == 1) {
					double minX = 3840; double minY = 2160;
					double minNorm = 1000000000;
					for (int j = regList[i].First; j <= regList[i].Second; j++) {
						if(dscList[j].PreCalcData.Norm < minNorm) {
							minNorm = dscList[j].PreCalcData.Norm;
							minX = dscList[j].Points3D.CenterX;
							minY = dscList[j].Points3D.CenterY;
						}
					}
					targetNorm = minNorm;
					targetX = minX; targetY = minY;
					Console.WriteLine("anchor false + min mode  TargetNorm {0} X {1} Y {2}", targetNorm, targetX, targetY);
				}

				if (nCalibType == 4) {
					for (int j = regList[i].First; j <= regList[i].Second; j++) {
						if (j < stdList[i].Second) {
							int cnt = stdList[i].Second - regList[i].First + 1;
							double interval = (targetNorm - dscList[regList[i].First].PreCalcData.Norm) / (cnt - 1);
							double targetNormDsc = dscList[regList[i].First].PreCalcData.Norm + (interval * (j - regList[i].First));
							dscList[j].Adjust.Scale = targetNormDsc / dscList[j].PreCalcData.Norm;
						}
						else if (j == stdList[i].Second) {
							dscList[j].Adjust.Scale = 1;
						}
						else {
							int cnt = regList[i].Second - stdList[i].Second + 1;
							double interval = (dscList[regList[i].Second].PreCalcData.Norm - targetNorm) / (cnt - 1);
							double targetNormDsc = dscList[regList[i].Second].PreCalcData.Norm + (interval * (j - regList[i].Second));
							dscList[j].Adjust.Scale = targetNormDsc / dscList[j].PreCalcData.Norm;
						}
						dscList[j].Adjust.AdjustX = targetX - dscList[j].Points3D.CenterX;
						dscList[j].Adjust.AdjustY = targetY - dscList[j].Points3D.CenterY;
						dscList[j].Adjust.RotateX = dscList[j].Points3D.CenterX;
						dscList[j].Adjust.RotateY = dscList[j].Points3D.CenterY;
					}
				} else if (nCalibType == 3) {
					int cnt = regList[i].Second - regList[i].First + 1;					
					double interval = (targetNorm - startNorm) / (cnt - 1);
					for (int j = regList[i].First; j <= regList[i].Second; j++) {
						double distNorm = startNorm + (interval * (j - regList[i].First));
						dscList[j].Adjust.Scale = distNorm / dscList[j].PreCalcData.Norm;						
						dscList[j].Adjust.AdjustX = targetX - dscList[j].Points3D.CenterX;
						dscList[j].Adjust.AdjustY = targetY - dscList[j].Points3D.CenterY;
						dscList[j].Adjust.RotateX = dscList[j].Points3D.CenterX;
						dscList[j].Adjust.RotateY = dscList[j].Points3D.CenterY;
					}
				} 
				else {
					for (int j = regList[i].First; j <= regList[i].Second; j++) {
						dscList[j].Adjust.Scale = targetNorm / dscList[j].PreCalcData.Norm;
						dscList[j].Adjust.AdjustX = targetX - dscList[j].Points3D.CenterX;
						dscList[j].Adjust.AdjustY = targetY - dscList[j].Points3D.CenterY;
						dscList[j].Adjust.RotateX = dscList[j].Points3D.CenterX;
						dscList[j].Adjust.RotateY = dscList[j].Points3D.CenterY;
						Console.WriteLine("{0} {1} {2} {3} ", j, dscList[j].Adjust.Scale, dscList[j].Adjust.AdjustX, dscList[j].Adjust.AdjustY);
					}
				}
			}
		}

		public static bool Cal3D(in WorldCoordinates4d wc, ref List<DscInfo> dscList)
        {
			List<Pair<int, int>> regList = new List<Pair<int, int>>();
			List<Pair<int, int>> stdList = new List<Pair<int, int>>();
			//dscList[15].IsStd = true;
			// Reg된 idx 쌍을 탐색하여 추가
			for (int i = 0; i < dscList.Count; i++)
			{
				dscList[i].IsAdj = false;
				if (dscList[i].IsReg)
				{
					Pair<int, int> idxPair = new Pair<int, int>(-1, -1);
					bool insertStd = false;
					idxPair.First = i;

					for (int j = i + 1; j < dscList.Count; j++)
					{
						if (dscList[j].IsStd && insertStd == false) {
							Pair<int, int> tc = new Pair<int, int>(regList.Count, j);
							stdList.Add(tc);
							insertStd = true;
						}
						if (dscList[j].IsReg) {
							if (insertStd == false) {
								Pair<int, int> tc = new Pair<int, int>(-1, j);
								stdList.Add(tc);
							}
							idxPair.Second = j;
							break;
						}
					}

					if (idxPair.First != -1 && idxPair.Second != -1 && idxPair.Second - idxPair.First > 1) {
						regList.Add(idxPair);
						i = idxPair.Second - 1;
					}
				}
			}

			//stadard dsc check for test 
			for (int i = 0; i < regList.Count; i ++) {
				Console.WriteLine("Standard Dsc IS ==== {0} : {1} {2}", i, stdList[i].First, stdList[i].Second);
            } //test 
			List<Point3f> ptsSquare3d = new List<Point3f>();
			NormalizeWorldPt(wc, 100, ref ptsSquare3d);
			CalculateNormDegree(regList, ptsSquare3d, ref dscList);
			CalculateScalenCenter(regList, stdList, ref dscList);

			//test print for debug 
			for (int i = 0; i < regList.Count; i++) {
				for (int j = regList[i].First; j <= regList[i].Second; j++) {
					Console.WriteLine("[{0}] scale {1} adj {2} {3} rot {4} {5}",
						j, dscList[j].Adjust.Scale, dscList[j].Adjust.AdjustX, dscList[j].Adjust.AdjustY,
						dscList[j].Adjust.RotateX, dscList[j].Adjust.RotateY);
				}
			} //test print

			CalNewCali.CalculateMargin(regList, ref dscList);
			return true;
		}

    }

	public class CalNewCali_2D {

	}

	public class CalNewCali {
		public static bool imgDebug = false;
		public static int CalculateMargin(List<Pair <int, int>>regList, ref List<DscInfo> dscList)
        {
			for (int i = 0; i < regList.Count; i++)
			{
				List<RectangleF> lstMinRect = new List<RectangleF>();
				List<float> left = new List<float>();
				List<float> right = new List<float>();
				List<float> top = new List<float>();
				List<float> bottom = new List<float>();
				int nWidth, nHeight;
				nWidth = dscList[0].Width;
				nHeight = dscList[0].Height;

				RectangleF rtMinRect = new RectangleF(0, 0, dscList[regList[i].First].Width, dscList[regList[i].First].Height);

				for (int j = regList[i].First; j <= regList[i].Second; j++)
				{
					if (dscList[j].Adjust.AdjustX == 0 || dscList[j].Adjust.AdjustY == 0 || dscList[j].Points3D.CenterX == -1 || dscList[j].Points3D.CenterY == -1)
					{
						dscList[j].Adjust.RectMargin = new RectangleF(0, 0, 0, 0);
						continue;
					}

					Point2f pt1, pt2, pt3, pt4, ptR1, ptR2, ptR3, ptR4;

					Point2f ptCenter = new Point2f((float)dscList[j].Adjust.RotateX, (float)dscList[j].Adjust.RotateY);
					//Console.WriteLine("ptCenter {0} {1}", dscList[j].Adjust.RotateX, dscList[j].Adjust.RotateY);

					double dbRAngle = dscList[j].Adjust.Angle; //ccwise radian angle

					// Get Resize Rect Point
					pt1.X = (float)(ptCenter.X * (1 - dscList[j].Adjust.Scale));
					pt1.Y = (float)(ptCenter.Y * (1 - dscList[j].Adjust.Scale));

					pt2.X = (float)(pt1.X + nWidth * dscList[j].Adjust.Scale);
					pt2.Y = pt1.Y;

					pt3.X = pt2.X;
					pt3.Y = (float)(pt1.Y + nHeight * dscList[j].Adjust.Scale);

					pt4.X = pt1.X;
					pt4.Y = pt3.Y;

					// Console.WriteLine("scale initial rect {0} {1} {2} {3}", pt1, pt2, pt3, pt4);
					ptR1 = FDAdjust.GetRotatePoint(ptCenter, pt1, dbRAngle);
					ptR2 = FDAdjust.GetRotatePoint(ptCenter, pt2, dbRAngle);
					ptR3 = FDAdjust.GetRotatePoint(ptCenter, pt3, dbRAngle);
					ptR4 = FDAdjust.GetRotatePoint(ptCenter, pt4, dbRAngle);
					// Console.WriteLine("---rotated rect {0} {1} {2} {3}", ptR1, ptR2, ptR3, ptR4);
					int nNewMarginLeft = 0;
					int nNewMarginTop = 0;
					int nNewMarginRight = nWidth;
					int nNewMarginBottom = nHeight;

					if (ptR1.X + dscList[j].Adjust.AdjustX > nNewMarginLeft)
						nNewMarginLeft = (int)(ptR1.X + dscList[j].Adjust.AdjustX);
					if (ptR1.Y + dscList[j].Adjust.AdjustY > nNewMarginTop)
						nNewMarginTop = (int)(ptR1.Y + dscList[j].Adjust.AdjustY);

					if (ptR2.X + dscList[j].Adjust.AdjustX < nNewMarginRight)
						nNewMarginRight = (int)(ptR2.X + dscList[j].Adjust.AdjustX);
					if (ptR2.Y + dscList[j].Adjust.AdjustY > nNewMarginTop)
						nNewMarginTop = (int)(ptR2.Y + dscList[j].Adjust.AdjustY);

					if (ptR3.X + dscList[j].Adjust.AdjustX < nNewMarginRight)
						nNewMarginRight = (int)(ptR3.X + dscList[j].Adjust.AdjustX);
					if (ptR3.Y + dscList[j].Adjust.AdjustY < nNewMarginBottom)
						nNewMarginBottom = (int)(ptR3.Y + dscList[j].Adjust.AdjustY);

					if (ptR4.X + dscList[j].Adjust.AdjustX > nNewMarginLeft)
						nNewMarginLeft = (int)(ptR4.X + dscList[j].Adjust.AdjustX);
					if (ptR4.Y + dscList[j].Adjust.AdjustY < nNewMarginBottom)
						nNewMarginBottom = (int)(ptR4.Y + dscList[j].Adjust.AdjustY);

					if (nNewMarginLeft > nNewMarginTop * nWidth / nHeight)
						nNewMarginTop = nNewMarginLeft * nHeight / nWidth;
					else
						nNewMarginLeft = nNewMarginTop * nWidth / nHeight;

					if (nNewMarginRight < nNewMarginBottom * nWidth / nHeight)
						nNewMarginBottom = nNewMarginRight * nHeight / nWidth;
					else
						nNewMarginRight = nNewMarginBottom * nWidth / nHeight;

					// Margin 뒤집힘 현상 임시 방지

					if (nNewMarginLeft > nNewMarginRight)
					{
						int nTemp = nNewMarginLeft;
						nNewMarginLeft = nNewMarginRight;
						nNewMarginRight = nTemp;
					}
					if (nNewMarginTop > nNewMarginBottom)
					{
						int nTemp = nNewMarginTop;
						nNewMarginTop = nNewMarginBottom;
						nNewMarginBottom = nTemp;
					}
					lstMinRect.Add(new RectangleF(nNewMarginLeft, nNewMarginTop, nNewMarginRight - nNewMarginLeft, nNewMarginBottom - nNewMarginTop));
					Console.WriteLine("-----nNewmargin1 left {0} top {1} right {2} bottom {3}", nNewMarginLeft, nNewMarginTop, nNewMarginRight, nNewMarginBottom);}

				int nMinTop = 0;
				int nMinLeft = 0;
				int nMinRight = dscList[regList[i].First].Width;
				int nMinBottom = dscList[regList[i].First].Height;

				for (int j = 0; j < lstMinRect.Count; j++)
				{
					if (lstMinRect[j].Left > nMinLeft && lstMinRect[j].Top > nMinTop)
					{
						nMinLeft = (int)lstMinRect[j].Left;
						nMinTop = (int)lstMinRect[j].Top;

						rtMinRect = new RectangleF(nMinLeft, nMinTop, rtMinRect.Right - nMinLeft, rtMinRect.Bottom - nMinTop);
					}

					if (lstMinRect[j].Right < nMinRight && lstMinRect[j].Bottom < nMinBottom)
					{
						if (lstMinRect[j].Right == 0 || lstMinRect[j].Bottom == 0)
							continue;

						nMinRight = (int)lstMinRect[j].Right;
						nMinBottom = (int)lstMinRect[j].Bottom;

						rtMinRect = new RectangleF(rtMinRect.Left, rtMinRect.Top, nMinRight - rtMinRect.Left, nMinBottom - rtMinRect.Top);
					}
				}
				Console.WriteLine("Rect margin left {0} top {1} right {2} bottom {3} width {4} height {5}", rtMinRect.Left, rtMinRect.Top, rtMinRect.Right, rtMinRect.Bottom, rtMinRect.Right-rtMinRect.Left, rtMinRect.Bottom - rtMinRect.Top);
				for (int j = regList[i].First; j <= regList[i].Second; j++)
				{
					dscList[j].Adjust.RectMargin = rtMinRect;
					dscList[j].IsAdj = true;
				}
			}
			return 1;
		}
		public static int CalculateMarginNew(List< Pair<int, int>>regList, ref List<DscInfo> dscList)
        {
			for (int i = 0; i < regList.Count; i++) {
				List<RectangleF> lstMinRect = new List<RectangleF>();
				RectangleF rtMinRect = new RectangleF(0, 0, dscList[regList[i].First].Width, dscList[regList[i].First].Height);
				List<float> left = new List<float>();
				List<float> right = new List<float>();
				List<float> top = new List<float>();
				List<float> bottom = new List<float>();
				left.Add(0); right.Add(dscList[regList[i].First].Width);
				top.Add(0); bottom.Add(dscList[regList[i].First].Height);
				
				for (int j = regList[i].First; j <= regList[i].Second; j++) {
					if (dscList[j].Adjust.AdjustX == 0 || dscList[j].Adjust.AdjustY == 0 || dscList[j].Points3D.CenterX == -1 || dscList[j].Points3D.CenterY == -1) {
						dscList[j].Adjust.RectMargin = new RectangleF(0, 0, 0, 0);
						continue;
					}
					List<Point2f> vertices = new List<Point2f>();
					float dScale = (float)dscList[j].Adjust.Scale;
					float delScaleX = (1 - (float)dscList[j].Adjust.Scale)* (float)dscList[j].Adjust.RotateX;
					float delScaleY = (1 - (float)dscList[j].Adjust.Scale)* (float)dscList[j].Adjust.RotateY;			
					//Point2f ptCenter = new Point2f((float)dscList[j].Adjust.RotateX, (float)dscList[j].Adjust.RotateY);

					Size2f ns; 
					ns.Width = (float)dscList[j].Width * (float)dscList[j].Adjust.Scale; 
					ns.Height = (float)dscList[j].Height * (float)dscList[j].Adjust.Scale;
					Point2f ptCenter = new Point2f ((float)ns.Width/2, (float)ns.Height/2);
					//if(imgDebug)
					Mat canvas = new Mat((int)ns.Height+ 500, (int)ns.Width + 600, MatType.CV_8UC3);

					RotatedRect rec1 = new RotatedRect(ptCenter, ns, 0);
					RotatedRect rec2 = new RotatedRect(ptCenter, ns, (float)dscList[j].PreCalcData.Degree);
					Mat intersect = new Mat();
					Cv2.RotatedRectangleIntersection(rec1, rec2, intersect);
					if (imgDebug)
						Cv2.Circle(canvas, (int)ptCenter.X+30, (int)ptCenter.Y+30, 3 , new Scalar(255,0,0), 3);

					for (int k = 0; k < intersect.Height; k++) {
						Point2f tp = intersect.At<Point2f>(k);
						tp.X = tp.X + delScaleX + (float)dscList[j].Adjust.AdjustX;
						tp.Y = tp.Y + delScaleY + (float)dscList[j].Adjust.AdjustY;
						// if(tp.X < 0) tp.X = 0;
						// else if (tp.X > dscList[j].Width ) tp.X = dscList[j].Width;

						// if(tp.Y < 0) tp.Y = 0;
						// else if (tp.Y > dscList[j].Height ) tp.Y = dscList[j].Height;
				
						vertices.Add(tp);
						Console.WriteLine("{0} point2f origin {1} {2} -- add {3} {4}", j, intersect.At<Point2f>(k).X, intersect.At<Point2f>(k).Y, tp.X, tp.Y);
						if (imgDebug)
							Cv2.Circle(canvas, (int)tp.X+30, (int)tp.Y+30, 3, new Scalar(0, 255, 0), 3);
					}

					Point2f[] contour = Cv2.ApproxPolyDP(vertices, Cv2.ArcLength(vertices, true)* 0.02, true);

					if(contour.Length ==  4) {
						float tleft = 0; float ttop = 0; float tright = (float)dscList[j].Width; float tbottom = (float)dscList[j].Height;
						float x1 = -1; float x2 = -1; float x3 = -1; float x4 = -1;
						float y1 = -1; float y2 = -1; float y3 = -1; float y4 = -1;

						
						for(int l = 0 ; l < 4 ; l ++) {
							if(contour[l].X < ptCenter.X) {
								if(x1 == -1)
									x1 = contour[l].X;
								else x2 = contour[l].X;
							}
							else {
								if( x3 == -1)
									x3 = contour[l].X;
								else 
									x4 = contour[l].X;
							}
							if(contour[l].Y < ptCenter.Y) {
								if(y1 == -1)
									y1 = contour[l].Y;
								else y2 = contour[l].Y;
							} else {
								if(y3 == -1)
									y3 = contour[l].Y;
								else y4 = contour[l].Y;
							}
						}

						for (int l = 0 ; l < 4 ; l ++) {
							tleft = x1 < x2 ? x2 : x1;
							tright = x3 < x4 ? x3 : x4;
							ttop = y1 < y2 ? y2 : y1;
							tbottom = y3 < y4 ? y3 : y4;;
						}

						if(tleft > 0 )
							left.Add(tleft);
						if (tright> 0)
							right.Add(tright);
						if (ttop > 0 )
							top.Add(ttop);
						if (tbottom > 0)
							bottom.Add(tbottom);

						if (imgDebug) {
							Cv2.Circle(canvas, (int)tleft + 30, (int)ttop + 30, 3, new Scalar(0, 0, 255), 3);
							Cv2.Circle(canvas, (int)tright + 30, (int)ttop + 30, 3, new Scalar(0, 0, 255), 3);
							Cv2.Circle(canvas, (int)tleft + 30, (int)tbottom + 30, 3, new Scalar(0, 0, 255), 3);
							Cv2.Circle(canvas, (int)tright + 30, (int)tbottom + 30, 3, new Scalar(0, 0, 255), 3);
						}
						Console.WriteLine("[{0}]contour {1} {2}, {3} {4}  Width {5} Height {6}", j, tleft, ttop, tright, tbottom, tright-tleft, tbottom - ttop);
					}
					if (imgDebug) {
						String filename = "canvas_" + j + ".png";
						Cv2.ImWrite(filename, canvas);
					}
				}

				left.Sort(delegate (float a, float b) { return b.CompareTo(a); });
				right.Sort(delegate (float a, float b) { return a.CompareTo(b); });
				top.Sort(delegate (float a, float b) { return b.CompareTo(a); });
				bottom.Sort(delegate (float a, float b) { return a.CompareTo(b); });

				float nMinLeft = left[0];
				float nMinTop = top[0];				
				float nMinRight = right[0];
				float nMinBottom = bottom[0];
				//for (int m = 0 ; m < 5; m ++) {
				//	Console.WriteLine("left {0} top {1} right {2} bottom {3}", left[m], top[m], right[m], bottom[m]);
				//}
 				double wratio = 3840.0 / 2160.0;
				double hratio = 2160.0 / 3840.0;
				//Console.WriteLine("-------- list1 left {0} top {1} right {2} bottom {3}", nMinLeft, nMinTop, nMinRight, nMinBottom);

				if (nMinLeft > nMinTop * (float)wratio)
					nMinTop = nMinLeft * (float)hratio;
				else
					nMinLeft = nMinTop * (float)wratio;

				if (nMinRight < nMinBottom * (float)wratio)
					nMinBottom = nMinRight * (float)hratio;
				else
					nMinRight = nMinBottom * (float)wratio;

				Console.WriteLine("-------- list2 left {0} top {1} right {2} bottom {3}", nMinLeft, nMinTop, nMinRight, nMinBottom);

				rtMinRect = new RectangleF((int)nMinLeft, (int)nMinTop, (int)nMinRight - (int)nMinLeft, (int)nMinBottom - (int)nMinTop);
				Console.WriteLine(" ---------------------- Final rect {0}", rtMinRect);

				for (int j = regList[i].First; j <= regList[i].Second; j++) {
					dscList[j].Adjust.RectMargin = rtMinRect;
					dscList[j].IsAdj = true;
				}				
			}
			return 1;
        }
		public static bool AdjustImage(in Mat src, ref Mat dst, OpenCvSharp.Size szDst, AdjustInfo adj, bool isFlip = false)
		{
			if (szDst.Width == 0 || szDst.Height == 0)
				szDst = src.Size();

			Mat m, mTfm, mRot, mTrn, mScale, mFlip, mMargin, mScaleOutput;

			MatrixCalculator.GetFlipMatrix(src.Cols, src.Height, isFlip, isFlip, out mFlip);
			MatrixCalculator.GetRotationMatrix((float)adj.Angle, (float)adj.RotateX, (float)adj.RotateY, out mRot);
			MatrixCalculator.GetScaleMarix((float)adj.Scale, (float)adj.Scale, (float)adj.RotateX, (float)adj.RotateY, out mScale);
			MatrixCalculator.GetTranslationMatrix((float)adj.AdjustX, (float)adj.AdjustY, out mTrn);
			MatrixCalculator.GetMarginMatrix(src.Cols, src.Rows, (int)adj.RectMargin.Left, (int)adj.RectMargin.Top, (int)adj.RectMargin.Width, (int)adj.RectMargin.Height, out mMargin);
			MatrixCalculator.GetScaleMarix((float)szDst.Width / src.Cols, (float)szDst.Height / src.Rows, out mScaleOutput);

			mTfm = mScaleOutput * mMargin * mTrn * mScale * mRot * mFlip;

			MatrixCalculator.ParseAffineMatrix(mTfm, out m);
			Console.WriteLine("[{0}] Apply Rad {1}, Scale {2} tran {3} {4} 2ndScale{5} last scale {6}", 1, adj.Angle, adj.Scale, adj.AdjustX, adj.AdjustY, (float)szDst.Width/src.Cols, m.At<float>(1,1));

			Mat res = new Mat();

			if (!isFlip)
				src.Circle(new OpenCvSharp.Point(adj.RotateX, adj.RotateY), 10, Scalar.FromRgb(255, 0, 0), 5);
			else
				src.Circle(new OpenCvSharp.Point(src.Cols - adj.RotateX, src.Rows - adj.RotateY), 10, Scalar.FromRgb(255, 0, 0), 5);
			
			res = Mat.Zeros(szDst, src.Type());

			Cv2.WarpAffine(src, res, m, szDst, InterpolationFlags.Linear, BorderTypes.Constant);
			//Cv2.Rectangle(res, new Rect(new OpenCvSharp.Point((int)adj.RectMargin.Left/2, (int)adj.RectMargin.Top/2), 
			//  			new OpenCvSharp.Size((int)adj.RectMargin.Width/2, (int)adj.RectMargin.Height/2)), new Scalar(125,200, 50), 2);


			dst = res;

			return true;
		}
	}
}
