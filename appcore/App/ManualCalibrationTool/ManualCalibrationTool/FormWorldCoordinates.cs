﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenCvSharp;
using OpenCvSharp.Extensions;
using FDUtilities;

namespace ManualCalibrationTool
{

	public partial class FormWorldCoordinates : Form
	{
		public WorldCoordinates4d WorldCoords = new WorldCoordinates4d();
		public const int StadiumTypeCount = 16;
		public StadiumType Stardium = StadiumType.BaseballHome;

		public FormWorldCoordinates()
        {
            InitializeComponent();

			this.Deactivate += new EventHandler(FormWorldCoordinates_Deactivate);

			for (int i = 0; i < StadiumTypeCount; i++)
            {
                cbWorldCoordinates.Items.Add(((StadiumType) i).ToString());
            }
			cbWorldCoordinates.SelectedIndex = 0;

			pbWorldCoordinatesImage.SetPointMsgHandlerWorld(SetWorldCoordinates);
		}

        public Bitmap getWorldCoordinageImage(StadiumType eWorldCoordinateImage)
		{
			Bitmap WorldCoordinageImage = Properties.Resources.BaseballHome;
			switch (eWorldCoordinateImage)
			{
				case StadiumType.BaseballHome:
					WorldCoordinageImage = Properties.Resources.BaseballHome;
					break;
				case StadiumType.BaseballGround:
					WorldCoordinageImage = Properties.Resources.Baseball;
					break;
				case StadiumType.BasketballHalf:
					WorldCoordinageImage = Properties.Resources.BasketballHalf;
					break;
				case StadiumType.BasketballGround:
					WorldCoordinageImage = Properties.Resources.Basketball;
					break;
				case StadiumType.Boxing:
					WorldCoordinageImage = Properties.Resources.Boxing;
					break;
				case StadiumType.IceLinkHalf:
					WorldCoordinageImage = Properties.Resources.IceLinkHalf;
					break;
				case StadiumType.IceLink:
					WorldCoordinageImage = Properties.Resources.IceLink;
					break;
				case StadiumType.SoccerHalf:
					WorldCoordinageImage = Properties.Resources.SoccerHalf;
					break;
				case StadiumType.Soccer:
					WorldCoordinageImage = Properties.Resources.Soccer;
					break;
				case StadiumType.Taekwondo:
					WorldCoordinageImage = Properties.Resources.Taekwondo;
					break;
				case StadiumType.TennisHalf:
					WorldCoordinageImage = Properties.Resources.TennisHalf;
					break;
				case StadiumType.Tennis:
					WorldCoordinageImage = Properties.Resources.Tennis;
					break;
				case StadiumType.Ufc:
					WorldCoordinageImage = Properties.Resources.UFC;
					break;
				case StadiumType.VolleyballHalf:
					WorldCoordinageImage = Properties.Resources.VolleyballHalf;
					break;
				case StadiumType.VolleyballGround:
					WorldCoordinageImage = Properties.Resources.Volleyball;
					break;
				case StadiumType.Football:
					WorldCoordinageImage = Properties.Resources.Football;
					break;
				default:
					break;
			}
			return WorldCoordinageImage;
		}

        private void btnApplyWorldCoordinates_Click(object sender, EventArgs e)
        {
			this.Visible = false;
        }

        private void cbWorldCoordinates_SelectedIndexChanged(object sender, EventArgs e)
        {
			SetStadiumType((StadiumType)cbWorldCoordinates.SelectedIndex);
		}
		public WorldCoordinates4d GetWorldCoords()
		{
			return this.WorldCoords;
		}

		public StadiumType GetStadiumType()
		{
			return this.Stardium;
		}

		public void SetStadiumType(StadiumType type)
        {
			this.Stardium = type;
			cbWorldCoordinates.SelectedIndex = (int)type;

			System.Drawing.Bitmap ImageBitmap = getWorldCoordinageImage(type);
			pbWorldCoordinatesImage.SetImage(ImageBitmap, ImageBitmap.Width, ImageBitmap.Height);
		}
		public void SetWorldCoordinates(WorldCoordinates4d pts)
		{
			this.WorldCoords = pts;
			pbWorldCoordinatesImage.SetWorldCoords(pts);
		}

		public void SetWorldCoordinates(CalPosWorld pos, float x, float y)
		{
			if (pos == CalPosWorld.First)
			{
				WorldCoords.X1 = x;
				WorldCoords.Y1 = y;
			}
			else if (pos == CalPosWorld.Second)
			{
				WorldCoords.X2 = x;
				WorldCoords.Y2 = y;
			}
			else if (pos == CalPosWorld.Third)
			{
				WorldCoords.X3 = x;
				WorldCoords.Y3 = y;
			}
			else
			{
				WorldCoords.X4 = x;
				WorldCoords.Y4 = y;
			}
		}

		public void SetIdx(bool mode, int idx)
		{
			pbWorldCoordinatesImage.SetAutoSelect(mode);
			pbWorldCoordinatesImage.SetIdx(idx);
		}

		private void FormWorldCoordinates_Deactivate(object sender, EventArgs e)
		{
			this.Visible = false;
		}
	}
}
