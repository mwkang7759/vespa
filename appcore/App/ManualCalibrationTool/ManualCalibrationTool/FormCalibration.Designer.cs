﻿
namespace ManualCalibrationTool
{
    partial class FormCalibration
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.listDsc = new System.Windows.Forms.ListView();
            this.btnRegScale = new System.Windows.Forms.Button();
            this.btnRegScaleNone = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnRegScaleAll = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnResetPoints = new System.Windows.Forms.Button();
            this.btnImportPoints = new System.Windows.Forms.Button();
            this.btnExportPoints = new System.Windows.Forms.Button();
            this.btnZoomOut = new System.Windows.Forms.Button();
            this.btnZoomIn = new System.Windows.Forms.Button();
            this.lblZoom = new System.Windows.Forms.Label();
            this.lblCursor = new System.Windows.Forms.Label();
            this.btnCalcAdjustData = new System.Windows.Forms.Button();
            this.btnPreviewAdjustResult = new System.Windows.Forms.Button();
            this.btnSaveAsAdjustData = new System.Windows.Forms.Button();
            this.btnLoadSample = new System.Windows.Forms.Button();
            this.rbCalibrationMode2D = new System.Windows.Forms.RadioButton();
            this.rbCalibrationMode3D = new System.Windows.Forms.RadioButton();
            this.cbIdxReferPoint = new System.Windows.Forms.ComboBox();
            this.listLog = new FDUtilities.LogListView();
            this.pbMainImage = new FDUtilities.CvPictureBox();
            this.btnSetWorldCoordinates = new System.Windows.Forms.Button();
            this.chkReverse = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbMainImage)).BeginInit();
            this.SuspendLayout();
            // 
            // listDsc
            // 
            this.listDsc.HideSelection = false;
            this.listDsc.Location = new System.Drawing.Point(12, 58);
            this.listDsc.Name = "listDsc";
            this.listDsc.Size = new System.Drawing.Size(243, 851);
            this.listDsc.TabIndex = 0;
            this.listDsc.UseCompatibleStateImageBehavior = false;
            this.listDsc.View = System.Windows.Forms.View.List;
            this.listDsc.SelectedIndexChanged += new System.EventHandler(this.listDsc_SelectedIndexChanged);
            // 
            // btnRegScale
            // 
            this.btnRegScale.Location = new System.Drawing.Point(15, 24);
            this.btnRegScale.Name = "btnRegScale";
            this.btnRegScale.Size = new System.Drawing.Size(63, 23);
            this.btnRegScale.TabIndex = 1;
            this.btnRegScale.Text = "REG.";
            this.btnRegScale.UseVisualStyleBackColor = true;
            this.btnRegScale.Click += new System.EventHandler(this.btnRegScale_Click);
            // 
            // btnRegScaleNone
            // 
            this.btnRegScaleNone.Location = new System.Drawing.Point(154, 24);
            this.btnRegScaleNone.Name = "btnRegScaleNone";
            this.btnRegScaleNone.Size = new System.Drawing.Size(63, 23);
            this.btnRegScaleNone.TabIndex = 1;
            this.btnRegScaleNone.Text = "NONE";
            this.btnRegScaleNone.UseVisualStyleBackColor = true;
            this.btnRegScaleNone.Click += new System.EventHandler(this.btnRegScaleNone_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnRegScaleAll);
            this.groupBox1.Controls.Add(this.btnRegScale);
            this.groupBox1.Controls.Add(this.btnRegScaleNone);
            this.groupBox1.Location = new System.Drawing.Point(261, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(235, 71);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "FIXED SCALE";
            // 
            // btnRegScaleAll
            // 
            this.btnRegScaleAll.Location = new System.Drawing.Point(84, 24);
            this.btnRegScaleAll.Name = "btnRegScaleAll";
            this.btnRegScaleAll.Size = new System.Drawing.Size(63, 23);
            this.btnRegScaleAll.TabIndex = 1;
            this.btnRegScaleAll.Text = "ALL";
            this.btnRegScaleAll.UseVisualStyleBackColor = true;
            this.btnRegScaleAll.Click += new System.EventHandler(this.btnRegScaleAll_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnResetPoints);
            this.groupBox2.Controls.Add(this.btnImportPoints);
            this.groupBox2.Controls.Add(this.btnExportPoints);
            this.groupBox2.Location = new System.Drawing.Point(502, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(235, 71);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "REF. POINT";
            // 
            // btnResetPoints
            // 
            this.btnResetPoints.Location = new System.Drawing.Point(155, 24);
            this.btnResetPoints.Name = "btnResetPoints";
            this.btnResetPoints.Size = new System.Drawing.Size(63, 23);
            this.btnResetPoints.TabIndex = 0;
            this.btnResetPoints.Text = "RESET";
            this.btnResetPoints.UseVisualStyleBackColor = true;
            this.btnResetPoints.Click += new System.EventHandler(this.btnResetPoints_Click);
            // 
            // btnImportPoints
            // 
            this.btnImportPoints.Location = new System.Drawing.Point(86, 24);
            this.btnImportPoints.Name = "btnImportPoints";
            this.btnImportPoints.Size = new System.Drawing.Size(63, 23);
            this.btnImportPoints.TabIndex = 0;
            this.btnImportPoints.Text = "IMPORT";
            this.btnImportPoints.UseVisualStyleBackColor = true;
            this.btnImportPoints.Click += new System.EventHandler(this.btnImportPoints_Click);
            // 
            // btnExportPoints
            // 
            this.btnExportPoints.Location = new System.Drawing.Point(17, 24);
            this.btnExportPoints.Name = "btnExportPoints";
            this.btnExportPoints.Size = new System.Drawing.Size(63, 23);
            this.btnExportPoints.TabIndex = 0;
            this.btnExportPoints.Text = "EXPORT";
            this.btnExportPoints.UseVisualStyleBackColor = true;
            this.btnExportPoints.Click += new System.EventHandler(this.btnExportPoints_Click);
            // 
            // btnZoomOut
            // 
            this.btnZoomOut.Location = new System.Drawing.Point(1146, 89);
            this.btnZoomOut.Name = "btnZoomOut";
            this.btnZoomOut.Size = new System.Drawing.Size(18, 18);
            this.btnZoomOut.TabIndex = 3;
            this.btnZoomOut.Text = "-";
            this.btnZoomOut.UseVisualStyleBackColor = true;
            this.btnZoomOut.Click += new System.EventHandler(this.btnZoomOut_Click);
            // 
            // btnZoomIn
            // 
            this.btnZoomIn.Location = new System.Drawing.Point(1234, 89);
            this.btnZoomIn.Name = "btnZoomIn";
            this.btnZoomIn.Size = new System.Drawing.Size(18, 18);
            this.btnZoomIn.TabIndex = 3;
            this.btnZoomIn.Text = "+";
            this.btnZoomIn.UseVisualStyleBackColor = true;
            this.btnZoomIn.Click += new System.EventHandler(this.btnZoomIn_Click);
            // 
            // lblZoom
            // 
            this.lblZoom.Location = new System.Drawing.Point(1170, 92);
            this.lblZoom.Name = "lblZoom";
            this.lblZoom.Size = new System.Drawing.Size(58, 15);
            this.lblZoom.TabIndex = 2;
            this.lblZoom.Text = "100%";
            this.lblZoom.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCursor
            // 
            this.lblCursor.AutoSize = true;
            this.lblCursor.Location = new System.Drawing.Point(261, 90);
            this.lblCursor.Name = "lblCursor";
            this.lblCursor.Size = new System.Drawing.Size(95, 12);
            this.lblCursor.TabIndex = 2;
            this.lblCursor.Text = "x=0.000, y=0.000";
            // 
            // btnCalcAdjustData
            // 
            this.btnCalcAdjustData.Location = new System.Drawing.Point(994, 12);
            this.btnCalcAdjustData.Name = "btnCalcAdjustData";
            this.btnCalcAdjustData.Size = new System.Drawing.Size(82, 71);
            this.btnCalcAdjustData.TabIndex = 1;
            this.btnCalcAdjustData.Text = "CALC.";
            this.btnCalcAdjustData.UseVisualStyleBackColor = true;
            this.btnCalcAdjustData.Click += new System.EventHandler(this.btnCalcAdjustData_Click);
            // 
            // btnPreviewAdjustResult
            // 
            this.btnPreviewAdjustResult.Location = new System.Drawing.Point(1082, 12);
            this.btnPreviewAdjustResult.Name = "btnPreviewAdjustResult";
            this.btnPreviewAdjustResult.Size = new System.Drawing.Size(82, 71);
            this.btnPreviewAdjustResult.TabIndex = 1;
            this.btnPreviewAdjustResult.Text = "PREVIEW";
            this.btnPreviewAdjustResult.UseVisualStyleBackColor = true;
            this.btnPreviewAdjustResult.Click += new System.EventHandler(this.btnPreviewAdjustResult_Click);
            // 
            // btnSaveAsAdjustData
            // 
            this.btnSaveAsAdjustData.Location = new System.Drawing.Point(1170, 12);
            this.btnSaveAsAdjustData.Name = "btnSaveAsAdjustData";
            this.btnSaveAsAdjustData.Size = new System.Drawing.Size(82, 71);
            this.btnSaveAsAdjustData.TabIndex = 1;
            this.btnSaveAsAdjustData.Text = "SAVE AS";
            this.btnSaveAsAdjustData.UseVisualStyleBackColor = true;
            this.btnSaveAsAdjustData.Click += new System.EventHandler(this.btnSaveAsAdjustData_Click);
            // 
            // btnLoadSample
            // 
            this.btnLoadSample.Location = new System.Drawing.Point(906, 12);
            this.btnLoadSample.Name = "btnLoadSample";
            this.btnLoadSample.Size = new System.Drawing.Size(82, 71);
            this.btnLoadSample.TabIndex = 6;
            this.btnLoadSample.Text = "LOAD SAMPLE";
            this.btnLoadSample.UseVisualStyleBackColor = true;
            this.btnLoadSample.Click += new System.EventHandler(this.btnLoadSample_Click);
            // 
            // rbCalibrationMode2D
            // 
            this.rbCalibrationMode2D.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbCalibrationMode2D.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbCalibrationMode2D.Checked = true;
            this.rbCalibrationMode2D.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rbCalibrationMode2D.Location = new System.Drawing.Point(12, 12);
            this.rbCalibrationMode2D.Name = "rbCalibrationMode2D";
            this.rbCalibrationMode2D.Size = new System.Drawing.Size(120, 40);
            this.rbCalibrationMode2D.TabIndex = 9;
            this.rbCalibrationMode2D.TabStop = true;
            this.rbCalibrationMode2D.Text = "2D";
            this.rbCalibrationMode2D.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbCalibrationMode2D.UseVisualStyleBackColor = true;
            this.rbCalibrationMode2D.CheckedChanged += new System.EventHandler(this.rbCalibrationMode2D_CheckedChanged);
            // 
            // rbCalibrationMode3D
            // 
            this.rbCalibrationMode3D.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbCalibrationMode3D.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbCalibrationMode3D.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rbCalibrationMode3D.Location = new System.Drawing.Point(135, 12);
            this.rbCalibrationMode3D.Name = "rbCalibrationMode3D";
            this.rbCalibrationMode3D.Size = new System.Drawing.Size(120, 40);
            this.rbCalibrationMode3D.TabIndex = 9;
            this.rbCalibrationMode3D.Text = "3D";
            this.rbCalibrationMode3D.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbCalibrationMode3D.UseVisualStyleBackColor = true;
            // 
            // cbIdxReferPoint
            // 
            this.cbIdxReferPoint.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbIdxReferPoint.FormattingEnabled = true;
            this.cbIdxReferPoint.Items.AddRange(new object[] {
            "AUTO",
            "1st",
            "2nd",
            "3rd",
            "4th"});
            this.cbIdxReferPoint.Location = new System.Drawing.Point(261, 87);
            this.cbIdxReferPoint.Name = "cbIdxReferPoint";
            this.cbIdxReferPoint.Size = new System.Drawing.Size(76, 20);
            this.cbIdxReferPoint.TabIndex = 10;
            this.cbIdxReferPoint.Visible = false;
            this.cbIdxReferPoint.SelectedIndexChanged += new System.EventHandler(this.cbIdxReferPoint_SelectedIndexChanged);
            // 
            // listLog
            // 
            this.listLog.FullRowSelect = true;
            this.listLog.GridLines = true;
            this.listLog.HideSelection = false;
            this.listLog.Location = new System.Drawing.Point(261, 770);
            this.listLog.Name = "listLog";
            this.listLog.Size = new System.Drawing.Size(991, 139);
            this.listLog.TabIndex = 11;
            this.listLog.UseCompatibleStateImageBehavior = false;
            this.listLog.View = System.Windows.Forms.View.Details;
            // 
            // pbMainImage
            // 
            this.pbMainImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbMainImage.Location = new System.Drawing.Point(261, 110);
            this.pbMainImage.Name = "pbMainImage";
            this.pbMainImage.Size = new System.Drawing.Size(991, 654);
            this.pbMainImage.TabIndex = 8;
            this.pbMainImage.TabStop = false;
            // 
            // btnSetWorldCoordinates
            // 
            this.btnSetWorldCoordinates.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnSetWorldCoordinates.Location = new System.Drawing.Point(12, 58);
            this.btnSetWorldCoordinates.Name = "btnSetWorldCoordinates";
            this.btnSetWorldCoordinates.Size = new System.Drawing.Size(243, 49);
            this.btnSetWorldCoordinates.TabIndex = 12;
            this.btnSetWorldCoordinates.Text = "STADIUM";
            this.btnSetWorldCoordinates.UseVisualStyleBackColor = true;
            this.btnSetWorldCoordinates.Visible = false;
            this.btnSetWorldCoordinates.Click += new System.EventHandler(this.btnSetWorldCoordinates_Click);
            // 
            // chkReverse
            // 
            this.chkReverse.AutoSize = true;
            this.chkReverse.Location = new System.Drawing.Point(1070, 88);
            this.chkReverse.Name = "chkReverse";
            this.chkReverse.Size = new System.Drawing.Size(70, 16);
            this.chkReverse.TabIndex = 13;
            this.chkReverse.Text = "Reverse";
            this.chkReverse.UseVisualStyleBackColor = true;
            this.chkReverse.CheckedChanged += new System.EventHandler(this.chkReverse_CheckedChanged);
            // 
            // FormCalibration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 921);
            this.Controls.Add(this.chkReverse);
            this.Controls.Add(this.btnSetWorldCoordinates);
            this.Controls.Add(this.listLog);
            this.Controls.Add(this.cbIdxReferPoint);
            this.Controls.Add(this.rbCalibrationMode3D);
            this.Controls.Add(this.rbCalibrationMode2D);
            this.Controls.Add(this.pbMainImage);
            this.Controls.Add(this.btnLoadSample);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnZoomIn);
            this.Controls.Add(this.lblCursor);
            this.Controls.Add(this.lblZoom);
            this.Controls.Add(this.btnZoomOut);
            this.Controls.Add(this.btnSaveAsAdjustData);
            this.Controls.Add(this.btnPreviewAdjustResult);
            this.Controls.Add(this.btnCalcAdjustData);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.listDsc);
            this.Name = "FormCalibration";
            this.Text = "Calibration";
            this.Move += new System.EventHandler(this.FormCalibration_Move);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbMainImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listDsc;
        private System.Windows.Forms.Button btnRegScale;
        private System.Windows.Forms.Button btnRegScaleNone;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnResetPoints;
        private System.Windows.Forms.Button btnImportPoints;
        private System.Windows.Forms.Button btnExportPoints;
        private System.Windows.Forms.Button btnZoomOut;
        private System.Windows.Forms.Button btnZoomIn;
        private System.Windows.Forms.Label lblZoom;
        private System.Windows.Forms.Label lblCursor;
        private System.Windows.Forms.Button btnRegScaleAll;
        private System.Windows.Forms.Button btnCalcAdjustData;
        private System.Windows.Forms.Button btnPreviewAdjustResult;
        private System.Windows.Forms.Button btnSaveAsAdjustData;
        private FDUtilities.CvPictureBox pbMainImage;
        private System.Windows.Forms.Button btnLoadSample;
        private System.Windows.Forms.RadioButton rbCalibrationMode2D;
        private System.Windows.Forms.RadioButton rbCalibrationMode3D;
        private System.Windows.Forms.ComboBox cbIdxReferPoint;
        private FDUtilities.LogListView listLog;
        private System.Windows.Forms.Button btnSetWorldCoordinates;
        private System.Windows.Forms.CheckBox chkReverse;
    }
}

