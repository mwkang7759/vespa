﻿
namespace ManualCalibrationTool
{
    partial class FormWorldCoordinates
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbWorldCoordinates = new System.Windows.Forms.ComboBox();
            this.btnApplyWorldCoordinates = new System.Windows.Forms.Button();
            this.pbWorldCoordinatesImage = new FDUtilities.CvPictureBoxScene();
            ((System.ComponentModel.ISupportInitialize)(this.pbWorldCoordinatesImage)).BeginInit();
            this.SuspendLayout();
            // 
            // cbWorldCoordinates
            // 
            this.cbWorldCoordinates.FormattingEnabled = true;
            this.cbWorldCoordinates.Location = new System.Drawing.Point(12, 12);
            this.cbWorldCoordinates.Name = "cbWorldCoordinates";
            this.cbWorldCoordinates.Size = new System.Drawing.Size(210, 20);
            this.cbWorldCoordinates.TabIndex = 0;
            this.cbWorldCoordinates.SelectedIndexChanged += new System.EventHandler(this.cbWorldCoordinates_SelectedIndexChanged);
            // 
            // btnApplyWorldCoordinates
            // 
            this.btnApplyWorldCoordinates.Location = new System.Drawing.Point(12, 255);
            this.btnApplyWorldCoordinates.Name = "btnApplyWorldCoordinates";
            this.btnApplyWorldCoordinates.Size = new System.Drawing.Size(210, 26);
            this.btnApplyWorldCoordinates.TabIndex = 2;
            this.btnApplyWorldCoordinates.Text = "APPLY";
            this.btnApplyWorldCoordinates.UseVisualStyleBackColor = true;
            this.btnApplyWorldCoordinates.Click += new System.EventHandler(this.btnApplyWorldCoordinates_Click);
            // 
            // pbWorldCoordinatesImage
            // 
            this.pbWorldCoordinatesImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbWorldCoordinatesImage.Location = new System.Drawing.Point(12, 38);
            this.pbWorldCoordinatesImage.Name = "pbWorldCoordinatesImage";
            this.pbWorldCoordinatesImage.Size = new System.Drawing.Size(210, 211);
            this.pbWorldCoordinatesImage.TabIndex = 1;
            this.pbWorldCoordinatesImage.TabStop = false;
            // 
            // FormWorldCoordinates
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(234, 293);
            this.Controls.Add(this.btnApplyWorldCoordinates);
            this.Controls.Add(this.pbWorldCoordinatesImage);
            this.Controls.Add(this.cbWorldCoordinates);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormWorldCoordinates";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FormSceneSelector";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.pbWorldCoordinatesImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbWorldCoordinates;
        private FDUtilities.CvPictureBoxScene pbWorldCoordinatesImage;
        private System.Windows.Forms.Button btnApplyWorldCoordinates;
    }
}