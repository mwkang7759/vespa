﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenCvSharp;
using OpenCvSharp.Extensions;
using FDUtilities;

namespace ManualCalibrationTool
{
    public partial class FormCalibration : Form
    {
        private List<DscInfo> DscList = new List<DscInfo>();
        private int SelectedIndex = -1;
        private bool IsReverse = false;
        private bool newcal = true;
        private FormWorldCoordinates WorldCoordinatesForm = new FormWorldCoordinates();
        public FormCalibration()
        {
            InitializeComponent();

            listDsc.View = View.Details;
            listDsc.GridLines = true;
            listDsc.FullRowSelect = true;

            listDsc.Columns.Add("ID", 50);
            listDsc.Columns.Add("DSC", 80);
            listDsc.Columns.Add("REG", 50);
            listDsc.Columns.Add("ADJ", 50);

            pbMainImage.SetZoomMsgHandler(UpdateZoom);
            pbMainImage.SetCursorMsgHandler(UpdateCursor);
            pbMainImage.SetPointMsgHandler2D(SetPoint2D);
            pbMainImage.SetPointMsgHandler3D(SetPoint3D);

            cbIdxReferPoint.SelectedIndex = 0;

            WorldCoordinatesForm.Location = this.PointToScreen(new System.Drawing.Point(btnSetWorldCoordinates.Left, btnSetWorldCoordinates.Bottom));
            WorldCoordinatesForm.Show();
            WorldCoordinatesForm.Visible = false;
        }

        public void UpdateZoom(float rate)
        {
            lblZoom.Text = (rate * 100).ToString("F1") + "%";
        }
        public void UpdateCursor(int idx, float x, float y)
        {
            if (idx == -1)
                lblCursor.Text = "idx : Auto, x : " + x.ToString("F3") + ", y : " + y.ToString("F3");
            else
                lblCursor.Text = "idx : " + (idx + 1).ToString() + ", x : " + x.ToString("F3") + ", y : " + y.ToString("F3");
        }
        public void SetPoint2D(CalPos2D pos, float x, float y)
        {
            if (pos == CalPos2D.Upper)
            {
                DscList[SelectedIndex].Points2D.UpperPosX = x;
                DscList[SelectedIndex].Points2D.UpperPosY = y;
            }
            else if (pos == CalPos2D.Middle)
            {
                DscList[SelectedIndex].Points2D.MiddlePosX = x;
                DscList[SelectedIndex].Points2D.MiddlePosY = y;
            }
            else
            {
                DscList[SelectedIndex].Points2D.LowerPosX = x;
                DscList[SelectedIndex].Points2D.LowerPosY = y;
            }

            string msg = "[DSC ID = " + DscList[SelectedIndex].DscID + "] 영상의 2D Calibration " + pos.ToString() + " 좌표가 (" + x.ToString() + "," + y.ToString() + ") 로 설정되었습니다.";
            listLog.AddLogMessage(LogType.Info, msg);
        }
        public void SetPoint3D(CalPos3D pos, float x, float y)
        {
            if (pos == CalPos3D.First)
            {
                DscList[SelectedIndex].Points3D.X1 = x;
                DscList[SelectedIndex].Points3D.Y1 = y;
            }
            else if (pos == CalPos3D.Second)
            {
                DscList[SelectedIndex].Points3D.X2 = x;
                DscList[SelectedIndex].Points3D.Y2 = y;
            }
            else if (pos == CalPos3D.Third)
            {
                DscList[SelectedIndex].Points3D.X3 = x;
                DscList[SelectedIndex].Points3D.Y3 = y;
            }
            else if (pos == CalPos3D.Forth)
            {
                DscList[SelectedIndex].Points3D.X4 = x;
                DscList[SelectedIndex].Points3D.Y4 = y;
            }
            else
            {
                DscList[SelectedIndex].Points3D.CenterX = x;
                DscList[SelectedIndex].Points3D.CenterY = y;

                FDAdjust.TrackCenterPoint(ref DscList, SelectedIndex, new Point2d(x, y));
            }

            string msg = "[DSC ID = " + DscList[SelectedIndex].DscID + "] 영상의 3D Calibration " + pos.ToString() + " 좌표가 (" + x.ToString() + "," + y.ToString() + ") 로 설정되었습니다.";
            listLog.AddLogMessage(LogType.Info, msg);
        }

        private void btnZoomOut_Click(object sender, EventArgs e)
        {
            pbMainImage.ZoomOut();
        }

        private void btnZoomIn_Click(object sender, EventArgs e)
        {
            pbMainImage.ZoomIn();
        }

        private void btnLoadSample_Click(object sender, EventArgs e)
        {
            OpenFileDialog openPanel = new OpenFileDialog();
            openPanel.InitialDirectory = Environment.CurrentDirectory;
            openPanel.Filter = "Path txt File (*.txt)|*.txt|All files (*.*)|(*.*)";
            if (openPanel.ShowDialog() == DialogResult.OK)
            {
                DscList.Clear();
                GC.Collect();

                string fullName = openPanel.FileName;
                string fileName = openPanel.SafeFileName;
                string pathName = fullName.Substring(0, (fullName.Length - fileName.Length));

                string[] paths = File.ReadAllLines(fullName);

                foreach (string fileNameImage in paths)
                {
                    DscInfo info = new DscInfo();

                    info.DscID = fileNameImage.Substring(0, fileNameImage.Length - 6);

                    VideoCapture vc = new VideoCapture(pathName + fileNameImage);
                    Mat frame = new Mat();
                    vc.Read(frame);
                    info.SrcImg = frame;

                    info.Width = frame.Cols;
                    info.Height = frame.Rows;

                    info.IsReg = false;
                    info.IsAdj = false;
                    info.IsReverse = IsReverse;

                    vc.Release();

                    //Cv2.ImShow(info.DscID, frame);
                    //Cv2.WaitKey(0);

                    DscList.Add(info);
                }
                DscList[0].IsReg = true;
                DscList[DscList.Count - 1].IsReg = true;
                UpdateDscList();
                SetDscImage(0);
            }
        }

        private bool UpdateDscList()
        {
            if (DscList.Count == 0)
                return false;

            listDsc.Items.Clear();

            for ( int i=0; i< DscList.Count; i++ )
            {

                ListViewItem item = new ListViewItem( i.ToString());
                item.SubItems.Add(DscList[i].DscID);
                if (DscList[i].IsReg)
                    item.SubItems.Add("O");
                else
                    item.SubItems.Add("");
                if (DscList[i].IsAdj)
                    item.SubItems.Add("O");
                else
                    item.SubItems.Add("");

                listDsc.Items.Add(item);
            }
            SelectedIndex = 0;


            return true;
        }

        private bool SetDscImage(int idx)
        {
            if (DscList.Count <= idx)
                return false;

            System.Drawing.Bitmap ImageBitmap = BitmapConverter.ToBitmap(DscList[idx].SrcImg);
            if (DscList[idx].IsReverse)
                ImageBitmap.RotateFlip(RotateFlipType.Rotate180FlipNone);
            pbMainImage.SetImage(ImageBitmap, DscList[idx].Width, DscList[idx].Height);
            pbMainImage.SetReferencePoint(DscList[idx].Points2D, DscList[idx].Points3D);

            string msg = "[DSC ID = " + DscList[SelectedIndex].DscID + "] 영상이 로드되었습니다.";
            listLog.AddLogMessage(LogType.Info, msg);

            return true;
        }
        private void listDsc_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listDsc.SelectedItems.Count > 0)
            {
                SelectedIndex = listDsc.SelectedIndices[0];
                SetDscImage(SelectedIndex);
            }
        }

        private void rbCalibrationMode2D_CheckedChanged(object sender, EventArgs e)
        {
            if (rbCalibrationMode2D.Checked)
            {
                pbMainImage.SetCalibrationMode(CalibrationMode.Mode2D);
                btnSetWorldCoordinates.Visible = false;
                cbIdxReferPoint.Visible = false;
                WorldCoordinatesForm.Visible = false;
                lblCursor.Location = new System.Drawing.Point(pbMainImage.Location.X, lblCursor.Location.Y);
                listDsc.Size = new System.Drawing.Size(listDsc.Width, listLog.Bottom - btnSetWorldCoordinates.Top );
                listDsc.Location = new System.Drawing.Point(listDsc.Location.X, btnSetWorldCoordinates.Location.Y);
            }
            else
            {
                pbMainImage.SetCalibrationMode(CalibrationMode.Mode3D);
                listDsc.Size = new System.Drawing.Size(listDsc.Size.Width, listLog.Bottom - pbMainImage.Top);
                listDsc.Location = new System.Drawing.Point(listDsc.Location.X, pbMainImage.Top);
                lblCursor.Location = new System.Drawing.Point(cbIdxReferPoint.Location.X + cbIdxReferPoint.Size.Width + 10, lblCursor.Location.Y);
                cbIdxReferPoint.Visible = true;
                btnSetWorldCoordinates.Visible = true;
            }
        }

        private void cbIdxReferPoint_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cbIdxReferPoint.SelectedIndex == 0)
            {
                pbMainImage.SetAutoSelect(true);
                pbMainImage.SetIdx(0);
                WorldCoordinatesForm.SetIdx(true, 0);
            }
            else
            {
                pbMainImage.SetAutoSelect(false);
                pbMainImage.SetIdx(cbIdxReferPoint.SelectedIndex - 1);
                if(cbIdxReferPoint.SelectedIndex != 5)
                    WorldCoordinatesForm.SetIdx(false, cbIdxReferPoint.SelectedIndex - 1);
            }
        }

        private void btnRegScale_Click(object sender, EventArgs e)
        {
            int idx = SelectedIndex;

            DscList[idx].IsReg = !DscList[idx].IsReg;

            UpdateDscList();

            listDsc.Items[idx].Focused = true;
            listDsc.Items[idx].Selected = true;
        }

        private void btnRegScaleAll_Click(object sender, EventArgs e)
        {
            foreach(DscInfo info in DscList)
            {
                info.IsReg = true;
            }
            UpdateDscList();
        }

        private void btnRegScaleNone_Click(object sender, EventArgs e)
        {
            foreach (DscInfo info in DscList)
            {
                info.IsReg = false;
            }
            UpdateDscList();
        }

        private void btnExportPoints_Click(object sender, EventArgs e)
        {
            SaveFileDialog savePanel = new SaveFileDialog();
            savePanel.InitialDirectory = Environment.CurrentDirectory;
            savePanel.Filter = "Points file (*.pts)|*.pts|All files (*.*)|(*.*)";
            if (savePanel.ShowDialog() == DialogResult.OK)
            {
                if (JsonManager.PointsToJsonFile(WorldCoordinatesForm.GetStadiumType(), WorldCoordinatesForm.GetWorldCoords(), DscList, savePanel.FileName))
                    listLog.AddLogMessage(LogType.Info, "Points 정보가 [" + savePanel.FileName + "] 경로에 저장되었습니다.");
                else
                    listLog.AddLogMessage(LogType.Error, "Points 정보 저장에 실패하였습니다.");
            }
        }

        private void btnImportPoints_Click(object sender, EventArgs e)
        {
            OpenFileDialog openPanel = new OpenFileDialog();
            openPanel.InitialDirectory = Environment.CurrentDirectory;
            openPanel.Filter = "Points File (*.pts)|*.pts|All files (*.*)|(*.*)";
            if (openPanel.ShowDialog() == DialogResult.OK)
            {
                StadiumType type = StadiumType.BaseballGround;
                WorldCoordinates4d pts = new WorldCoordinates4d();

                if (JsonManager.JsonFileToPoints(openPanel.FileName, ref type, ref pts, ref DscList))
                {
                    WorldCoordinatesForm.SetStadiumType(type);
                    WorldCoordinatesForm.SetWorldCoordinates(pts);

                    listLog.AddLogMessage(LogType.Info, "[" + openPanel.FileName + "] 경로에서 Points 정보를 불러왔습니다.");
                }
                else
                    listLog.AddLogMessage(LogType.Error, "Points 정보 불러오기에 실패하였습니다.");

                SetDscImage(SelectedIndex);
            }
        }

        private void btnResetPoints_Click(object sender, EventArgs e)
        {
            foreach(DscInfo info in DscList)
            {
                if (rbCalibrationMode2D.Checked)
                    info.Points2D = new ReferencePoint2D();
                else
                    info.Points3D = new ReferencePoint3D();
            }
            SetDscImage(SelectedIndex);
        }

        private void btnTrackPoints_Click(object sender, EventArgs e)
        {

        }

        private void btnCalcAdjustData_Click(object sender, EventArgs e)
        {
            if (rbCalibrationMode3D.Checked)
            {
                if(newcal == false) {
                    if (FDAdjust.CalcPreValue3DCalbration(WorldCoordinatesForm.WorldCoords, ref DscList))
                        listLog.AddLogMessage(LogType.Info, "3D Calibration 변환에 성공하였습니다.");
                    else
                    {
                        listLog.AddLogMessage(LogType.Error, "3D Calibration 변환에 실패하였습니다.");
                        return;
                    }

                    if (FDAdjust.CalcAdjustData3D(ref DscList))
                    listLog.AddLogMessage(LogType.Info, "Adjust data 값이 정상적으로 계산되었습니다.");
                    else
                    {
                    listLog.AddLogMessage(LogType.Error, "Adjust data 계산에 실패하였습니다.");
                    return;
                    }
                } else {
                    if(CalNewCali_3D.Cal3D(WorldCoordinatesForm.WorldCoords, ref DscList))
                        listLog.AddLogMessage(LogType.Info, "3D Calibration 변환에 성공하였습니다.");
                    else
                    {
                        listLog.AddLogMessage(LogType.Error, "3D Calibration 변환에 실패하였습니다.");
                        return;
                    }
                }
            }
            else
            {
                if (FDAdjust.CalcAdjustData2D(ref DscList))
                {
                    listLog.AddLogMessage(LogType.Info, "Adjust data 값이 정상적으로 계산되었습니다.");
                    UpdateDscList();
                }
                else
                {
                    listLog.AddLogMessage(LogType.Error, "Adjust data 계산에 실패하였습니다.");
                    return;
                }
            }
        }

        private void btnPreviewAdjustResult_Click(object sender, EventArgs e)
        {

            List<Pair<int, int>> regList = new List<Pair<int, int>>();

            // Reg된 idx 쌍을 탐색하여 추가
            for (int i = 0; i < DscList.Count; i++)
            {
                DscList[i].IsAdj = false;
                if (DscList[i].IsReg)
                {
                    Pair<int, int> idxPair = new Pair<int, int>(i, -1);

                    for (int j = i + 1; j < DscList.Count; j++)
                    {
                        if (DscList[j].IsReg)
                        {
                            idxPair.Second = j;
                            break;
                        }
                    }

                    if (idxPair.First != -1 && idxPair.Second != -1)
                    {
                        regList.Add(idxPair);

                        i = idxPair.Second - 1;
                    }
                }
            }

            for (int i = 0; i < 1; i++)
            {

                for (int j = 0; j < regList.Count; j++)
                {
                    for (int k = regList[j].First; k <= regList[j].Second; k++)
                    {
                        Mat dstImg = new Mat();
                       
                        //
                      
                        //FDAdjust.AdjustImage(dsc.SrcImg, ref dstImg, new OpenCvSharp.Size(640, dsc.SrcImg.Rows * 640 / dsc.SrcImg.Cols), dsc.Adjust);
                        if (this.rbCalibrationMode2D.Checked)
                        {
                            FDAdjust.AdjustImage(DscList[k].SrcImg, ref dstImg, new OpenCvSharp.Size(1280, DscList[k].SrcImg.Rows * 1280 / DscList[k].SrcImg.Cols), DscList[k].Adjust, DscList[k].IsReverse);
                        }
                        else
                        {
                            Mat drawImg = DscList[k].SrcImg.Clone();
                            FDAdjust.DrawCalibrationBar(ref drawImg, WorldCoordinatesForm.WorldCoords, DscList[k]);
                            if(newcal == false) {
                                FDAdjust.AdjustImage(drawImg, ref dstImg, new OpenCvSharp.Size(1920, DscList[k].SrcImg.Rows * 1920 / DscList[k].SrcImg.Cols), DscList[k].Adjust, DscList[k].IsReverse);
                            } else {
                                CalNewCali.AdjustImage(drawImg, ref dstImg, new OpenCvSharp.Size(1920, DscList[k].SrcImg.Rows * 1920 / DscList[k].SrcImg.Cols), DscList[k].Adjust, DscList[k].IsReverse);
                            }
                            //double dx, dy;
                            //dx = DscList[regList[j].First].Points3D.CenterX - DscList[k].Points3D.CenterX;
                            //dy = DscList[regList[j].First].Points3D.CenterY - DscList[k].Points3D.CenterY;

                            //FDAdjust.PreviewImage(drawImg, ref dstImg, new OpenCvSharp.Size(640, DscList[k].SrcImg.Rows * 640 / DscList[k].SrcImg.Cols), 
                            //    DscList[k].PreCalcData.RotMatrix, dx, dy, DscList[k].Adjust.RectMargin);
                        }

                        Cv2.ImShow("PREVIEW", dstImg);
                        Cv2.ImWrite("Output" + k + ".png", dstImg);

                        Cv2.WaitKey(200);
                        GC.Collect();
                        
                        

                    }
                    
                }
                
            }
           
            Cv2.DestroyWindow("PREVIEW");
        }

        private void btnSaveAsAdjustData_Click(object sender, EventArgs e)
        {
            SaveFileDialog savePanel = new SaveFileDialog();
            savePanel.InitialDirectory = Environment.CurrentDirectory;
            savePanel.Filter = "Adjust file (*.adj)|*.adj|All files (*.*)|(*.*)";
            if (savePanel.ShowDialog() == DialogResult.OK)
            {
                if (JsonManager.AdjustToJsonFile(DscList, savePanel.FileName))
                    listLog.AddLogMessage(LogType.Info, "Adjust 정보가 [" + savePanel.FileName + "] 경로에 저장되었습니다.");
                else
                    listLog.AddLogMessage(LogType.Error, "Adjust 정보 저장에 실패하였습니다.");
            }
        }

        private void btnSetWorldCoordinates_Click(object sender, EventArgs e)
        {
            WorldCoordinatesForm.Location = this.PointToScreen(new System.Drawing.Point(btnSetWorldCoordinates.Left, btnSetWorldCoordinates.Bottom));
            WorldCoordinatesForm.Visible = !WorldCoordinatesForm.Visible;
        }


        private void FormCalibration_Move(object sender, EventArgs e)
        {
            WorldCoordinatesForm.Location = this.PointToScreen(new System.Drawing.Point(btnSetWorldCoordinates.Left, btnSetWorldCoordinates.Bottom));
        }

        private void chkReverse_CheckedChanged(object sender, EventArgs e)
        {
            IsReverse = chkReverse.Checked;

            foreach(DscInfo dsc in DscList)
            {
                dsc.IsReverse = IsReverse;
                dsc.Points2D = ReversePoint(dsc.Points2D, dsc.Width, dsc.Height);
                dsc.Points3D = ReversePoint(dsc.Points3D, dsc.Width, dsc.Height);
            }

            SetDscImage(SelectedIndex);
        }
        private ReferencePoint2D ReversePoint(ReferencePoint2D pts, int width, int height)
        {
            ReferencePoint2D ptsRvs = new ReferencePoint2D();

            if(pts.LowerPosX != -1)
                ptsRvs.LowerPosX = width - pts.LowerPosX;
            if (pts.LowerPosY != -1)
                ptsRvs.LowerPosY = height - pts.LowerPosY;
            if (pts.MiddlePosX != -1)
                ptsRvs.MiddlePosX = width - pts.MiddlePosX;
            if (pts.MiddlePosY != -1)
                ptsRvs.MiddlePosY = height - pts.MiddlePosY;
            if (pts.UpperPosX != -1)
                ptsRvs.UpperPosX = width - pts.UpperPosX;
            if (pts.UpperPosY != -1)
                ptsRvs.UpperPosY = height - pts.UpperPosY;

            return ptsRvs;
        }
        private ReferencePoint3D ReversePoint(ReferencePoint3D pts, int width, int height)
        {
            ReferencePoint3D ptsRvs = new ReferencePoint3D();

            if (pts.X1 != -1)
                ptsRvs.X1 = width - pts.X1;
            if (pts.X2 != -1)
                ptsRvs.X2 = width - pts.X2;
            if (pts.X3 != -1)
                ptsRvs.X3 = width - pts.X3;
            if (pts.Y3 != -1)
                ptsRvs.Y3 = height - pts.Y3;
            if (pts.X4 != -1)
                ptsRvs.X4 = width - pts.X4;
            if (pts.Y1 != -1)
                ptsRvs.Y1 = height - pts.Y1;
            if (pts.Y2 != -1)
                ptsRvs.Y2 = height - pts.Y2;
            if (pts.Y4 != -1)
                ptsRvs.Y4 = height - pts.Y4;
            if (pts.CenterX != -1)
                ptsRvs.CenterX = width - pts.CenterX;
            if (pts.CenterY != -1)
                ptsRvs.CenterY = height - pts.CenterY;

            return ptsRvs;
        }
    }
}
