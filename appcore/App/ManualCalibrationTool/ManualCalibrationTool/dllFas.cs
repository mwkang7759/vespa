﻿using System.Runtime.InteropServices;

static class dllFAS
{
    [DllImport("FDVision.dll",
        CallingConvention = CallingConvention.Cdecl)]
    public static extern int dllTestFunction(
        int[] arr, int nsize, ref int sum);
}