﻿
// TestLiveStabilDlg.cpp: 구현 파일
//

#include "pch.h"
#include "framework.h"
#include "TestLiveStabil.h"
#include "TestLiveStabilDlg.h"
#include "afxdialogex.h"

#include "TraceAPI.h"
#include "FrUtil.h"
#include "StreamReaderAPI.h"
#include "DecoderAPI.h"
#include "DeviceOutAPI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CTestLiveStabilDlg 대화 상자



CTestLiveStabilDlg::CTestLiveStabilDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_TESTLIVESTABIL_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTestLiveStabilDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CTestLiveStabilDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BTN_STABIL, &CTestLiveStabilDlg::OnBnClickedBtnStabil)
	ON_BN_CLICKED(IDC_BTN_STOP, &CTestLiveStabilDlg::OnBnClickedBtnStop)
END_MESSAGE_MAP()


// CTestLiveStabilDlg 메시지 처리기

BOOL CTestLiveStabilDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CTestLiveStabilDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 애플리케이션의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CTestLiveStabilDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CTestLiveStabilDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CTestLiveStabilDlg::OnBnClickedBtnStabil()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	fr::Tick tick;

	//mLvStabil = std::make_unique<LiveStabil>(1080, 1920);
	LiveStabil LvStabil;

	uint32_t	dwStart = 0;
	bool		bEnd = false;
	FrURLInfo	UrlInfo;
	FrRawVideo  DecVideo{};
	FrRawVideo  StabilVideo{};
	FrMediaInfo SrcInfo{};
	FrMediaStream	VideoData;
	memset(&VideoData, 0, sizeof(FrMediaStream));
	VideoData.tMediaType = VIDEO_MEDIA;

	FrReaderHandle hReader;
	FrVideoDecHandle hDecoder;
	
	McVideoOutHandle hSrcOut{};
	McVideoOutHandle hDstOut{};

	HANDLE hSrcWnd = GetDlgItem(IDC_STATIC_SRC_PICTURE)->m_hWnd;
	HANDLE hDstWnd = GetDlgItem(IDC_STATIC_STABIL_PICTURE)->m_hWnd;


	FrSetTraceFileName("live_stabil.log");
	mStop = false;


	// read and dec
	std::string name = "c:\\test_contents\\003060_join2.mp4";
	UrlInfo.pUrl = (char*)name.c_str();
	LRSLT lRet = FrReaderOpen(&hReader, &UrlInfo);
	if (FRFAILED(lRet)) {
		return;
	}

	lRet = FrReaderGetInfo(hReader, &SrcInfo);
	if (FRFAILED(lRet)) {
		return ;
	}

	// decoder
	FrVideoInfo* pSrcVideoInfo = &SrcInfo.FrVideo;
	
	pSrcVideoInfo->eCodecType = CODEC_HW;
	pSrcVideoInfo->eColorFormat = VideoFmtIYUV;     // output color format, //VideoFmtBGRA
	pSrcVideoInfo->bLowDelayDecoding = TRUE;
	pSrcVideoInfo->memDir = MEM_GPU;   // or MEM_GPU
	lRet = FrVideoDecOpen(&hDecoder, NULL, pSrcVideoInfo);
	if (FRFAILED(lRet)) {
		return ;
	}

	SrcInfo.FrVideo.dwBitCount = 24;
	McVideoOutOpen(&hSrcOut, &SrcInfo.FrVideo, hSrcWnd, FALSE);
	McVideoOutOpen(&hDstOut, &SrcInfo.FrVideo, hDstWnd, FALSE);

	lRet = FrReaderStart(hReader, &dwStart);
	if (FRFAILED(lRet)) {
		return ;
	}

	//namedWindow("src");
	cv::namedWindow("src", WINDOW_NORMAL);
	cv::resizeWindow("src", cv::Size(720, 480));

	cv::namedWindow("stabil", WINDOW_NORMAL);
	cv::resizeWindow("stabil", cv::Size(720, 480));

	
	while (!mStop) {
		lRet = FrReaderReadFrame(hReader, &VideoData);
		if (FRFAILED(lRet)) {
			if (lRet == COMMON_ERR_ENDOFDATA)
				break;

			FrSleep(10);
			continue;
		}

		// output format: bgra
		lRet = FrVideoDecDecode(hDecoder, &VideoData, &DecVideo);
		if (FRFAILED(lRet)) {
			continue;
		}
		for (int i = 0; i < DecVideo.nDecoded; i++) {
			// repos
			DecVideo.pY = DecVideo.ppDecoded[i];
			cv::Mat src, dst;
			cv::cuda::GpuMat gpuCur;

			tick.Start();

			//int ret = mLvStabil->StabilImage(DecVideo.pY, DecVideo.dwPitch, cur, mLvStabil->stabImg);
			//int ret = mLvStabil->StabilImageCuda(DecVideo.pY, DecVideo.dwPitch, gpuCur, mLvStabil->stabImgCuda);
			int ret = LvStabil.StabilImageCuda(DecVideo.pY, DecVideo.dwPitch, gpuCur, LvStabil.stabImgCuda);
			

			long end = tick.Stop();
			LOG_D("stabil time : (%d)", end);

			if (!ret)
				continue;

			// use only with cpu
			gpuCur.download(src);
			imshow("src", src);

			/*DecVideo.pY = src.data;
			DecVideo.eColorFormat = VideoFmtBGR24;
			McVideoOutConversion(hSrcOut, &DecVideo);
			McVideoOutPlay(hSrcOut);*/

			//mLvStabil->stabImgCuda.download(dst);
			LvStabil.stabImgCuda.download(dst);
			imshow("stabil", dst);

			waitKey(10);

			/*StabilVideo.dwPitch = dst.cols;
			StabilVideo.pY = dst.data;
			StabilVideo.eColorFormat = VideoFmtBGR24;
			McVideoOutConversion(hDstOut, &StabilVideo);
			McVideoOutPlay(hDstOut);*/

			// Step5
			/*gpuImg.download(tmpImg);
			_deepInfer->drawDetect(detectData, tmpImg);
			gpuImg.upload(tmpImg);
			if (_output) {
				delete _output;
			}*/

			

			// bgra -> yv12
			
			/*lRet = FrVideoEncEncode(m_hEncVideo, &ConvVideo, &EncStream);
			if (FRFAILED(lRet)) {
				continue;
			}*/
		}
	}

	// on test..
	AfxMessageBox(_T("complete..."));

	
	if (hDecoder != nullptr) {
		FrVideoDecClose(hDecoder);
		hDecoder = nullptr;
	}
	
	if (hReader != nullptr) {
		FrReaderClose(hReader);
		hReader = nullptr;
	}

	McVideoOutClose(hSrcOut);
	McVideoOutClose(hDstOut);

}


void CTestLiveStabilDlg::OnBnClickedBtnStop()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	mStop = true;
}
