﻿#include "pch.h"
#include "ESMImageCtr.h"
#include "DatasetMakerDlg.h"
#include "resource.h"

StClassModel g_ColorModel[MAX_CLASS_COUNT];		// 클래스별 컬러 정보
const CString g_SkeletonModel[MAX_SKELETON_COUNT] = {
	_T("NOSE"), _T("LEFT EYE"), _T("RIGHT EYE"), _T("LEFT EAR"), _T("RIGHT EAR"),
	_T("LEFT SHOULDER"), _T("RIGHT SHOULDER"), _T("LEFT ELBOW"), _T("RIGHT ELBOW"), _T("LEFT WRIST"), _T("RIGHT WRIST"),
	_T("LEFT HIP"), _T("RIGHT HIP"), _T("LEFT KNEE"), _T("RIGHT KNEE"), _T("LEFT ANKLE"), _T("RIGHT ANKLE")
};	// Skeleton의 각 vertex별 string 정보
const int g_Links[MAX_LINK_COUNT][2] =
{ {15, 13}, {13, 11}, {16, 14}, {14, 12}, {11, 12}, {5, 11}, {6, 12}, {5, 6}, {5, 7},
	{6, 8}, {7, 9}, {8, 10}, {1, 2}, {0, 1}, {0, 2}, {1, 3}, {2, 4}, {3, 5}, {4, 6} };

void CESMImageCtr::SetParent(CWnd* pParent)
{
	m_pParent = pParent;
}

CESMImageCtr::CESMImageCtr()
{
	m_pParent = NULL;

	m_nCursorMode = DISP_CURSOR_MODE_MOVE_LABEL;

	m_pBitmapInfo = NULL;

	m_bLBtnDown = false;
	m_bRBtnDown = false;
	m_bPolyDrawing = false;

	m_nImgWidth = 0;
	m_nImgHeight = 0;

	m_dOffsetX = 0.;
	m_dOffsetY = 0.;
	m_dZoomFactor = 1.;
	m_dMinZoomFactor = 1.;

	m_dBoxRatio = 0.;

	m_nSelLabel = 0;
	m_stMouseOnStatus = { -1, -1, -1 };

	m_nIdxSkel = 0;

	srand(GetTickCount());
	for (int i = 0; i < MAX_CLASS_COUNT; i++)
	{
		StClassModel cm = { rand() % 255, rand() % 255, rand() % 255 };
		g_ColorModel[i] = cm;
	}
}

CESMImageCtr::~CESMImageCtr()
{
}

BEGIN_MESSAGE_MAP(CESMImageCtr, CStatic)
	ON_WM_PAINT()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSEWHEEL()
	ON_WM_SETCURSOR()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_CONTEXTMENU()
END_MESSAGE_MAP()

/// <summary>
/// Display 영역의 DC 출력 정의
/// </summary>
void CESMImageCtr::OnPaint()
{
	CPaintDC dc(this); // device context for painting
					   // TODO: 여기에 메시지 처리기 코드를 추가합니다.
					   // 그리기 메시지에 대해서는 CStatic::OnPaint()을(를) 호출하지 마십시오.

	if (m_matImage.empty())
	{
		CRect rtView;
		GetClientRect(&rtView);
		CBrush brush(GRAY_BRUSH);

		CBrush* pOld = dc.SelectObject(&brush);
		BOOL bRes = dc.PatBlt(0, 0, rtView.Width(), rtView.Height(), PATCOPY);
		dc.SelectObject(pOld);

		CFont font;
		font.CreatePointFont(200, _T("Arial"));
		dc.SelectObject(&font);

		dc.SetTextColor(RGB(255, 255, 255));
		dc.SetBkMode(TRANSPARENT);
		dc.DrawText(_T("NO IMAGE"), rtView, DT_SINGLELINE | DT_CENTER | DT_VCENTER);

		return;
	}

	// Image 출력부
	DrawImage(dc);

	// Label 출력부
	if (m_nCursorMode == DISP_CURSOR_MODE_INSERT_BOX_LABEL)
		DrawROI(dc);
	else if (m_nCursorMode == DISP_CURSOR_MODE_INSERT_POLY_LABEL)
		DrawPoly(dc);
	else if (m_nCursorMode == DISP_CURSOR_MODE_INSERT_SKEL_LABEL)
		DrawSkeleton(dc);
	else
		DrawLabels(dc);

	// Scene title 출력부
	DrawSceneTitle(dc);

}

/// <summary>
/// 비트맵 정보 생성
/// </summary>
/// <param name="w"></param>
/// <param name="h"></param>
/// <param name="bpp"></param>
void CESMImageCtr::CreateBitmapInfo(int w, int h, int bpp)
{
	if (m_pBitmapInfo != NULL)
	{
		delete[]m_pBitmapInfo;
		m_pBitmapInfo = NULL;
	}

	if (bpp == 8)
		m_pBitmapInfo = (BITMAPINFO*) new BYTE[sizeof(BITMAPINFO) + 255 * sizeof(RGBQUAD)];
	else // 24 or 32bit
		m_pBitmapInfo = (BITMAPINFO*) new BYTE[sizeof(BITMAPINFO)];

	m_pBitmapInfo->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	m_pBitmapInfo->bmiHeader.biPlanes = 1;
	m_pBitmapInfo->bmiHeader.biBitCount = bpp;
	m_pBitmapInfo->bmiHeader.biCompression = BI_RGB;
	m_pBitmapInfo->bmiHeader.biSizeImage = 0;
	m_pBitmapInfo->bmiHeader.biXPelsPerMeter = 0;
	m_pBitmapInfo->bmiHeader.biYPelsPerMeter = 0;
	m_pBitmapInfo->bmiHeader.biClrUsed = 0;
	m_pBitmapInfo->bmiHeader.biClrImportant = 0;

	if (bpp == 8)
	{
		for (int i = 0; i < 256; i++)
		{
			m_pBitmapInfo->bmiColors[i].rgbBlue = (BYTE)i;
			m_pBitmapInfo->bmiColors[i].rgbGreen = (BYTE)i;
			m_pBitmapInfo->bmiColors[i].rgbRed = (BYTE)i;
			m_pBitmapInfo->bmiColors[i].rgbReserved = 0;
		}
	}

	m_pBitmapInfo->bmiHeader.biWidth = w;
	m_pBitmapInfo->bmiHeader.biHeight = -h;
}

/// <summary>
/// Display cursor mode 설정
/// </summary>
/// <param name="nMode"></param>
void CESMImageCtr::SetCursorMode(int nMode)
{
	if (m_nCursorMode == nMode)
	{
		m_vecPolyLines.clear();
		m_nCursorMode = DISP_CURSOR_MODE_MOVE_LABEL;
	}
	else
	{
		m_nCursorMode = nMode;

		// Skeleton일 경우 초기 좌표 설정
		if (nMode == DISP_CURSOR_MODE_INSERT_SKEL_LABEL && !m_bPolyDrawing)
		{
			m_vecPolyLines.clear();
			m_bPolyDrawing = true;

			for (int i = 0; i < MAX_SKELETON_COUNT; i++)
			{
				m_vecPolyLines.push_back({ false, Point(0, 0) });
			}
			m_vecPolyLines[0].bVis = true;
			m_nIdxSkel = 0;
		}
	}

	m_bLBtnDown = false;
	m_bRBtnDown = false;
}

/// <summary>
/// Display에 출력할 영상 설정
/// </summary>
/// <param name="img"></param>
void CESMImageCtr::SetImage(Mat img)
{
	this->GetClientRect(&m_rtCtrl);

	// Bitmap 오류 방지를 위한 border 처리 (width 4의 배수 아닐시 깨짐 현상)
	if (img.cols % 4 > 0)
	{
		Mat temp(Size(img.cols + (4 - img.cols % 4), img.rows), img.type());
		img.copyTo(temp(Rect(0, 0, img.cols, img.rows)));
		m_matImage = temp;
	}
	else
		m_matImage = img;

	m_nImgWidth = img.cols;
	m_nImgHeight = img.rows;

	if (m_rtCtrl.Width() / (double)m_nImgWidth > m_rtCtrl.Height() / (double)m_nImgHeight)
		m_dZoomFactor = m_rtCtrl.Height() / (double)m_nImgHeight;
	else
		m_dZoomFactor = m_rtCtrl.Width() / (double)m_nImgWidth;

	m_dMinZoomFactor = m_dZoomFactor;

	m_dOffsetX = 0;
	m_dOffsetY = 0;

	CreateBitmapInfo(m_matImage.cols, m_matImage.rows, m_matImage.channels() * 8);

	Invalidate(TRUE);
}

/// <summary>
/// 출력할 Scene title 설정
/// </summary>
/// <param name="str"></param>
void CESMImageCtr::SetSceneTitle(CString str)
{
	m_strSceneTitle = str;
}

/// <summary>
/// nIdx번째 Label 선택
/// </summary>
/// <param name="nIdx"></param>
void CESMImageCtr::SelectLabel(int nIdx)
{
	m_nSelLabel = nIdx;

	ReDrawImage();
}

/// <summary>
/// 현재 마우스 커서에서 가까운 label과 위치에 따른 resize 모드를 설정
/// </summary>
/// <param name="pt"></param>
/// <returns></returns>
StMouseOnStatus CESMImageCtr::FindNearestLabel(Point2f pt)
{
	StMouseOnStatus stt = { -1, -1, -1 };

	if (m_pLabels == NULL)
		return stt;


	int nSize = m_pLabels->size();
	if (m_nSelLabel >= nSize)
		return stt;

	vector<StLabelUnit>& labels = *m_pLabels;
	int nSens = 3 / m_dZoomFactor;	// Resize cursor 민감도

	// 선택된 Label이 있을경우
	if (m_nSelLabel != -1)
	{
		// 선택된 Label이 box 형태일 경우
		if (labels[m_nSelLabel].nStyle == LABEL_UNIT_STYLE_BOX)
		{
			int nPos = -1;

			int lMin, lMax, tMin, tMax, rMin, rMax, bMin, bMax;
			lMin = labels[m_nSelLabel].vPts[BOX_LABEL_PT_POS_LT].ptCoord.x - nSens;
			lMax = labels[m_nSelLabel].vPts[BOX_LABEL_PT_POS_LT].ptCoord.x + nSens;
			tMin = labels[m_nSelLabel].vPts[BOX_LABEL_PT_POS_LT].ptCoord.y - nSens;
			tMax = labels[m_nSelLabel].vPts[BOX_LABEL_PT_POS_LT].ptCoord.y + nSens;
			rMin = labels[m_nSelLabel].vPts[BOX_LABEL_PT_POS_RB].ptCoord.x - nSens;
			rMax = labels[m_nSelLabel].vPts[BOX_LABEL_PT_POS_RB].ptCoord.x + nSens;
			bMin = labels[m_nSelLabel].vPts[BOX_LABEL_PT_POS_RB].ptCoord.y - nSens;
			bMax = labels[m_nSelLabel].vPts[BOX_LABEL_PT_POS_RB].ptCoord.y + nSens;

			if (pt.x >= lMin && pt.x <= lMax && pt.y >= tMin && pt.y <= tMax)
				nPos = ROI_RESIZE_LT;
			else if (pt.x >= rMin && pt.x <= rMax && pt.y >= tMin && pt.y <= tMax)
				nPos = ROI_RESIZE_RT;
			else if (pt.x >= rMin && pt.x <= rMax && pt.y >= bMin && pt.y <= bMax)
				nPos = ROI_RESIZE_RB;
			else if (pt.x >= lMin && pt.x <= lMax && pt.y >= bMin && pt.y <= bMax)
				nPos = ROI_RESIZE_LB;
			else if (pt.x >= lMin && pt.x <= rMax && pt.y >= tMin && pt.y <= tMax)
				nPos = ROI_RESIZE_T;
			else if (pt.x >= rMin && pt.x <= rMax && pt.y >= tMin && pt.y <= bMax)
				nPos = ROI_RESIZE_R;
			else if (pt.x >= lMin && pt.x <= rMax && pt.y >= bMin && pt.y <= bMax)
				nPos = ROI_RESIZE_B;
			else if (pt.x >= lMin && pt.x <= lMax && pt.y >= tMin && pt.y <= bMax)
				nPos = ROI_RESIZE_L;
			else if (pt.x >= lMax && pt.x <= rMin && pt.y >= tMax && pt.y <= bMin)
				nPos = ROI_RESIZE_ALL;

			// 모서리 또는 선분 위에 있거나 box 내공간에 있을 경우 해당 정보 반환
			if (nPos != -1)
			{
				stt.nType = labels[m_nSelLabel].nStyle;	// Label style
				stt.nIdx = m_nSelLabel;					// Label index
				stt.nPos = nPos;						// Move/Resize 위치 정보

				return stt;
			}
		}
	}

	// 전체 Label 중 cursor 위에 있는 label 존재하는지 확인
	for (int i = 0; i < labels.size(); i++)
	{
		if (labels[i].nStyle == LABEL_UNIT_STYLE_BOX)
		{
			int nPos = -1;

			int lMin, lMax, tMin, tMax, rMin, rMax, bMin, bMax;
			lMin = labels[i].vPts[BOX_LABEL_PT_POS_LT].ptCoord.x - nSens;
			lMax = labels[i].vPts[BOX_LABEL_PT_POS_LT].ptCoord.x + nSens;
			tMin = labels[i].vPts[BOX_LABEL_PT_POS_LT].ptCoord.y - nSens;
			tMax = labels[i].vPts[BOX_LABEL_PT_POS_LT].ptCoord.y + nSens;
			rMin = labels[i].vPts[BOX_LABEL_PT_POS_RB].ptCoord.x - nSens;
			rMax = labels[i].vPts[BOX_LABEL_PT_POS_RB].ptCoord.x + nSens;
			bMin = labels[i].vPts[BOX_LABEL_PT_POS_RB].ptCoord.y - nSens;
			bMax = labels[i].vPts[BOX_LABEL_PT_POS_RB].ptCoord.y + nSens;

			if (pt.x >= lMin && pt.x <= lMax && pt.y >= tMin && pt.y <= tMax)
				nPos = ROI_RESIZE_LT;
			else if (pt.x >= rMin && pt.x <= rMax && pt.y >= tMin && pt.y <= tMax)
				nPos = ROI_RESIZE_RT;
			else if (pt.x >= rMin && pt.x <= rMax && pt.y >= bMin && pt.y <= bMax)
				nPos = ROI_RESIZE_RB;
			else if (pt.x >= lMin && pt.x <= lMax && pt.y >= bMin && pt.y <= bMax)
				nPos = ROI_RESIZE_LB;
			else if (pt.x >= lMin && pt.x <= rMax && pt.y >= tMin && pt.y <= tMax)
				nPos = ROI_RESIZE_T;
			else if (pt.x >= rMin && pt.x <= rMax && pt.y >= tMin && pt.y <= bMax)
				nPos = ROI_RESIZE_R;
			else if (pt.x >= lMin && pt.x <= rMax && pt.y >= bMin && pt.y <= bMax)
				nPos = ROI_RESIZE_B;
			else if (pt.x >= lMin && pt.x <= lMax && pt.y >= tMin && pt.y <= bMax)
				nPos = ROI_RESIZE_L;
			else if (pt.x >= lMax && pt.x <= rMin && pt.y >= tMax && pt.y <= bMin)
				nPos = ROI_RESIZE_ALL;

			if (nPos != -1)
			{
				stt.nType = labels[i].nStyle;
				stt.nIdx = i;
				stt.nPos = nPos;

				return stt;
			}
		}
		else
		{
			int nFind = -1;

			for (int j = 0; j < labels[i].vPts.size(); j++)
			{
				if (pt.x >= labels[i].vPts[j].ptCoord.x - nSens && pt.y >= labels[i].vPts[j].ptCoord.y - nSens &&
					pt.x <= labels[i].vPts[j].ptCoord.x + nSens && pt.y <= labels[i].vPts[j].ptCoord.y + nSens)
				{
					nFind = j;
					break;
				}
			}

			if (nFind != -1)
			{
				stt.nType = labels[i].nStyle;
				stt.nIdx = i;
				stt.nPos = nFind;

				return stt;
			}

			vector<Point> vPtTemp;
			for (StPolyVertex vt : labels[i].vPts)
			{
				if (vt.bVis)
					vPtTemp.push_back(vt.ptCoord);
			}

			// Cursor가 다각형 공간 내에 있을경우
			if (PointInPolygon(Point(pt), vPtTemp))
			{
				stt.nType = labels[i].nStyle;
				stt.nIdx = i;
				stt.nPos = -1;

				return stt;
			}
		}
	}

	return stt;
}

/// <summary>
/// Box 형태의 Label의 커서의 위치별 모드에 따라 dx, dy 만큼 vertex 이동
/// </summary>
/// <param name="nMode"></param>
/// <param name="dx"></param>
/// <param name="dy"></param>
void CESMImageCtr::ResizeLabel(int nMode, float dx, float dy)
{
	if (nMode == -1)
		return;

	if (m_pLabels == NULL)
		return;

	int ltx, lty, rbx, rby;
	ltx = (*m_pLabels)[m_nSelLabel].vPts[BOX_LABEL_PT_POS_LT].ptCoord.x;
	lty = (*m_pLabels)[m_nSelLabel].vPts[BOX_LABEL_PT_POS_LT].ptCoord.y;
	rbx = (*m_pLabels)[m_nSelLabel].vPts[BOX_LABEL_PT_POS_RB].ptCoord.x;
	rby = (*m_pLabels)[m_nSelLabel].vPts[BOX_LABEL_PT_POS_RB].ptCoord.y;


	switch (nMode)
	{
	case ROI_RESIZE_LT:
		ltx += (int)dx;
		lty += (int)dy;
		break;
	case ROI_RESIZE_RT:
		rbx += (int)dx;
		lty += (int)dy;
		break;
	case ROI_RESIZE_RB:
		rbx += (int)dx;
		rby += (int)dy;
		break;
	case ROI_RESIZE_LB:
		ltx += (int)dx;
		rby += (int)dy;
		break;
	case ROI_RESIZE_T:
		lty += (int)dy;
		break;
	case ROI_RESIZE_R:
		rbx += (int)dx;
		break;
	case ROI_RESIZE_B:
		rby += (int)dy;
		break;
	case ROI_RESIZE_L:
		ltx += (int)dx;
		break;
	case ROI_RESIZE_ALL:
		ltx += (int)dx;
		lty += (int)dy;
		rbx += (int)dx;
		rby += (int)dy;
		break;
	}

	if (ltx < 0 || lty < 0 || rbx >= m_matImage.cols || rby >= m_matImage.rows)
		return;

	(*m_pLabels)[m_nSelLabel].vPts[BOX_LABEL_PT_POS_LT].ptCoord.x = ltx;
	(*m_pLabels)[m_nSelLabel].vPts[BOX_LABEL_PT_POS_LT].ptCoord.y = lty;
	(*m_pLabels)[m_nSelLabel].vPts[BOX_LABEL_PT_POS_RB].ptCoord.x = rbx;
	(*m_pLabels)[m_nSelLabel].vPts[BOX_LABEL_PT_POS_RB].ptCoord.y = rby;
}

/// <summary>
/// Polygon 형태의 Label인 경우 nIdx의 vertex를 dx, dy 만큼 이동 (-1일 경우 전체)
/// </summary>
/// <param name="nIdx"></param>
/// <param name="dx"></param>
/// <param name="dy"></param>
void CESMImageCtr::MovePolyLabelVertex(int nIdx, float dx, float dy)
{
	if (m_pLabels == NULL)
		return;

	vector<StPolyVertex>& pts = (*m_pLabels)[m_nSelLabel].vPts;

	if (nIdx == -1)
	{
		for (int i = 0; i < pts.size(); i++)
		{
			if (pts[i].bVis && (pts[i].ptCoord.x + dx < 0 || pts[i].ptCoord.x + dx > m_nImgWidth - 1 || pts[i].ptCoord.y + dy < 0 || pts[i].ptCoord.y + dy > m_nImgHeight - 1))
				return;
		}

		for (int i = 0; i < pts.size(); i++)
		{
			if (pts[i].bVis)
			{
				pts[i].ptCoord.x += dx;
				pts[i].ptCoord.y += dy;
			}
		}
	}
	else
	{
		if (pts[nIdx].ptCoord.x + dx < 0 || pts[nIdx].ptCoord.x + dx > m_nImgWidth - 1 || pts[nIdx].ptCoord.y + dy < 0 || pts[nIdx].ptCoord.y + dy > m_nImgHeight - 1)
			return;

		pts[nIdx].ptCoord.x += dx;
		pts[nIdx].ptCoord.y += dy;
	}
}

/// <summary>
/// Left button down message handler
/// </summary>
/// <param name="nFlags"></param>
/// <param name="point"></param>
void CESMImageCtr::OnLButtonDown(UINT nFlags, CPoint point)
{
	if (m_matImage.empty())
		return;

	m_bLBtnDown = true;

	// Move mode일 경우 cursor 위치의 label check
	if (m_nCursorMode == DISP_CURSOR_MODE_MOVE_LABEL)
	{
		m_ptCursor = point;

		if (m_stMouseOnStatus.nIdx != -1)
		{
			m_stMouseOnStatus = FindNearestLabel(Point(m_dOffsetX + point.x / m_dZoomFactor, m_dOffsetY + point.y / m_dZoomFactor));
		}
	}
	// Box drawing mode일경우 시작 좌표 저장
	else if (m_nCursorMode == DISP_CURSOR_MODE_INSERT_BOX_LABEL)
	{
		m_ptStartROI = point;
	}
	// Polygon drawing mode일 경우 현재 cursor 위치를 vertex로 등록
	else if (m_nCursorMode == DISP_CURSOR_MODE_INSERT_POLY_LABEL)
	{
		// 최초 좌표일 경우 초기화
		if (!m_bPolyDrawing)
		{
			m_vecPolyLines.clear();
			m_bPolyDrawing = true;

			m_vecPolyLines.push_back({ true, Point(m_dOffsetX + (point.x / m_dZoomFactor), m_dOffsetY + (point.y / m_dZoomFactor)) });
		}

		m_vecPolyLines.push_back({ true, Point(m_dOffsetX + (point.x / m_dZoomFactor), m_dOffsetY + (point.y / m_dZoomFactor)) });
	}
	// Skeleton drawing mode일 경우 현재 cursor 위치를 vertex로 등록
	else
	{
		if (m_nIdxSkel + 1 < MAX_SKELETON_COUNT)
		{
			m_vecPolyLines[m_nIdxSkel].ptCoord = Point(m_dOffsetX + (point.x / m_dZoomFactor), m_dOffsetY + (point.y / m_dZoomFactor));
			m_vecPolyLines[m_nIdxSkel + 1].bVis = true;
			m_vecPolyLines[m_nIdxSkel + 1].ptCoord = m_vecPolyLines[m_nIdxSkel].ptCoord;
			m_nIdxSkel++;
		}
		else	// 최대 개수를 초과하면 등록 후 종료
		{
			m_bLBtnDown = false;

			((CDatasetMakerDlg*)m_pParent)->SetCheckAddSkelLabelBtn(FALSE);

			m_nCursorMode = DISP_CURSOR_MODE_MOVE_LABEL;

			((CDatasetMakerDlg*)m_pParent)->InsertSkelLabel(m_vecPolyLines);
			m_vecPolyLines.clear();
			m_nIdxSkel = 0;

			m_bPolyDrawing = false;
		}
	}

	Invalidate(FALSE);


	CStatic::OnLButtonDown(nFlags, point);
}

/// <summary>
/// Left button up message handler
/// </summary>
/// <param name="nFlags"></param>
/// <param name="point"></param>
void CESMImageCtr::OnLButtonUp(UINT nFlags, CPoint point)
{
	if (m_matImage.empty())
		return;

	// Move mode일 경우 cursor 위에 있는 label 선택
	if (m_nCursorMode == DISP_CURSOR_MODE_MOVE_LABEL)
	{
		if (m_bLBtnDown)
		{
			SelectLabel(m_stMouseOnStatus.nIdx);
			((CDatasetMakerDlg*)m_pParent)->UpdateLabelList();
			((CDatasetMakerDlg*)m_pParent)->SelectLabel(m_stMouseOnStatus.nIdx);
		}
	}
	// Box drawing mode일 경우 box label 추가
	else if (m_nCursorMode == DISP_CURSOR_MODE_INSERT_BOX_LABEL)
	{
		if (m_pParent != NULL)
			((CDatasetMakerDlg*)m_pParent)->SetCheckAddBoxLabelBtn(FALSE);

		m_nCursorMode = DISP_CURSOR_MODE_MOVE_LABEL;

		if (m_rtROI.x < 0 || m_rtROI.y < 0 || m_rtROI.x + m_rtROI.width >= m_matImage.cols || m_rtROI.y + m_rtROI.height >= m_matImage.rows)
		{
			AfxMessageBox(_T("ROI가 영상 영역을 벗어났습니다."));
			m_rtROI = Rect(0, 0, 0, 0);
			return;
		}

		((CDatasetMakerDlg*)m_pParent)->InsertBoxLabel(m_rtROI);
		m_rtROI = Rect(0, 0, 0, 0);
	}

	m_bLBtnDown = false;

	Invalidate(FALSE);

	CStatic::OnLButtonUp(nFlags, point);
}

/// <summary>
/// Left button double click message handler
/// </summary>
/// <param name="nFlags"></param>
/// <param name="point"></param>
void CESMImageCtr::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	// Move mode일 경우 선택된 라벨 편집
	if (m_nCursorMode == DISP_CURSOR_MODE_MOVE_LABEL && m_nSelLabel != -1)
		((CDatasetMakerDlg*)m_pParent)->OnBnClickedBtnEditLabel();
	// Polygon drawing mode일 경우 polygon label 추가
	else if (m_nCursorMode == DISP_CURSOR_MODE_INSERT_POLY_LABEL)
	{
		m_bLBtnDown = false;

		if (m_bPolyDrawing)
		{
			if (m_pParent != NULL)
				((CDatasetMakerDlg*)m_pParent)->SetCheckAddPolyLabelBtn(FALSE);

			m_nCursorMode = DISP_CURSOR_MODE_MOVE_LABEL;

			for (int i = 0; i < m_vecPolyLines.size(); i++)
			{
				if (m_vecPolyLines[i].ptCoord.x < 0 || m_vecPolyLines[i].ptCoord.y < 0 || m_vecPolyLines[i].ptCoord.x >= m_matImage.cols || m_vecPolyLines[i].ptCoord.y >= m_matImage.rows)
				{
					AfxMessageBox(_T("일부 좌표가 영상 영역을 벗어났습니다."));
					m_vecPolyLines.clear();
					return;
				}

				if (i > 0)
				{
					if (m_vecPolyLines[i].ptCoord.x == m_vecPolyLines[i - 1].ptCoord.x && m_vecPolyLines[i].ptCoord.y == m_vecPolyLines[i - 1].ptCoord.y)
					{
						m_vecPolyLines.erase(m_vecPolyLines.begin() + i);
						i--;
					}
				}
			}

			((CDatasetMakerDlg*)m_pParent)->InsertPolyLabel(m_vecPolyLines);
			m_vecPolyLines.clear();

			m_bPolyDrawing = false;
		}
	}

	Invalidate(FALSE);

	CStatic::OnLButtonDblClk(nFlags, point);
}

/// <summary>
/// Right button down message handler
/// </summary>
/// <param name="nFlags"></param>
/// <param name="point"></param>
void CESMImageCtr::OnRButtonDown(UINT nFlags, CPoint point)
{
	if (m_matImage.empty())
		return;

	m_bRBtnDown = true;

	m_ptCursor = point;

	// Polygon drawing mode일 경우 이전 vertex 취소
	if (m_nCursorMode == DISP_CURSOR_MODE_INSERT_POLY_LABEL && m_bPolyDrawing && m_vecPolyLines.size() > 2)
	{
		m_vecPolyLines.erase(m_vecPolyLines.end() - 1);

		Invalidate(FALSE);
	}
	// Skeleton drawing mode일 경우 현재 vertex를 건너뜀
	else if (m_nCursorMode == DISP_CURSOR_MODE_INSERT_SKEL_LABEL && m_vecPolyLines.size() > 2)
	{
		if (m_nIdxSkel + 1 < MAX_SKELETON_COUNT)
		{
			//m_vecPolyLines.push_back({ true, Point(m_dOffsetX + (point.x / m_dZoomFactor), m_dOffsetY + (point.y / m_dZoomFactor)) });
			m_vecPolyLines[m_nIdxSkel].bVis = false;
			m_vecPolyLines[m_nIdxSkel + 1].bVis = true;
			m_vecPolyLines[m_nIdxSkel + 1].ptCoord = m_vecPolyLines[m_nIdxSkel].ptCoord;
			m_vecPolyLines[m_nIdxSkel].ptCoord = Point(0, 0);
			m_nIdxSkel++;
		}
		else
		{
			m_vecPolyLines[m_nIdxSkel].bVis = false;
			m_vecPolyLines[m_nIdxSkel].ptCoord = Point(0, 0);

			((CDatasetMakerDlg*)m_pParent)->SetCheckAddSkelLabelBtn(FALSE);

			m_nCursorMode = DISP_CURSOR_MODE_MOVE_LABEL;

			((CDatasetMakerDlg*)m_pParent)->InsertSkelLabel(m_vecPolyLines);
			m_vecPolyLines.clear();
			m_nIdxSkel = 0;

			m_bPolyDrawing = false;
		}
	}

	CStatic::OnRButtonDown(nFlags, point);
}

/// <summary>
/// Right button up message handler
/// </summary>
/// <param name="nFlags"></param>
/// <param name="point"></param>
void CESMImageCtr::OnRButtonUp(UINT nFlags, CPoint point)
{
	if (m_matImage.empty())
		return;

	m_bRBtnDown = false;

	CStatic::OnRButtonUp(nFlags, point);
}

/// <summary>
/// Mouse move message handler
/// </summary>
/// <param name="nFlags"></param>
/// <param name="point"></param>
void CESMImageCtr::OnMouseMove(UINT nFlags, CPoint point)
{
	if (m_matImage.empty())
		return;

	if (m_nCursorMode == DISP_CURSOR_MODE_MOVE_LABEL)
	{
		if (m_bLBtnDown)
		{
			// ROI Resize
			if (m_stMouseOnStatus.nIdx == m_nSelLabel)
			{
				int dx = (point.x - m_ptCursor.x) / m_dZoomFactor;
				int dy = (point.y - m_ptCursor.y) / m_dZoomFactor;

				if (m_stMouseOnStatus.nType == LABEL_UNIT_STYLE_BOX)
					ResizeLabel(m_stMouseOnStatus.nPos, dx, dy);
				else if (m_stMouseOnStatus.nType == LABEL_UNIT_STYLE_POLY)
					MovePolyLabelVertex(m_stMouseOnStatus.nPos, dx, dy);
				else if (m_stMouseOnStatus.nType == LABEL_UNIT_STYLE_SKEL)
					MovePolyLabelVertex(m_stMouseOnStatus.nPos, dx, dy);
			}

			m_ptCursor = point;
		}
		else
		{
			// 인접한 Label 탐색
			m_stMouseOnStatus = FindNearestLabel(Point(m_dOffsetX + point.x / m_dZoomFactor, m_dOffsetY + point.y / m_dZoomFactor));
		}
	}
	else if (m_nCursorMode == DISP_CURSOR_MODE_INSERT_BOX_LABEL)
	{
		if (m_bLBtnDown)
		{
			CPoint newPt = point;
			int nLeft, nTop, nRight, nBottom;


			if (m_dBoxRatio > 0.)
			{
				if (abs(point.x - m_ptStartROI.x) > abs(point.y - m_ptStartROI.y))
				{
					if (point.y - m_ptStartROI.y >= 0)
						newPt.y = m_ptStartROI.y + abs(point.x - m_ptStartROI.x) / m_dBoxRatio;
					else
						newPt.y = m_ptStartROI.y - abs(point.x - m_ptStartROI.x) / m_dBoxRatio;
				}
				else
				{
					if (point.x - m_ptStartROI.x >= 0)
						newPt.x = m_ptStartROI.x + abs(point.y - m_ptStartROI.y) * m_dBoxRatio;
					else
						newPt.x = m_ptStartROI.x - abs(point.y - m_ptStartROI.y) * m_dBoxRatio;
				}


			}

			nLeft = min(newPt.x, m_ptStartROI.x);
			nRight = max(newPt.x, m_ptStartROI.x);
			nTop = min(newPt.y, m_ptStartROI.y);
			nBottom = max(newPt.y, m_ptStartROI.y);

			if (m_dOffsetX + (nLeft / m_dZoomFactor) < 0 || m_dOffsetY + (nTop / m_dZoomFactor) < 0 ||
				m_dOffsetX + (nRight / m_dZoomFactor) > m_nImgWidth - 1 || m_dOffsetY + (nBottom / m_dZoomFactor) > m_nImgHeight - 1)
				return;

			m_rtROI.x = m_dOffsetX + (nLeft / m_dZoomFactor);
			m_rtROI.y = m_dOffsetY + (nTop / m_dZoomFactor);
			m_rtROI.width = ((nRight - nLeft) / m_dZoomFactor);
			m_rtROI.height = ((nBottom - nTop) / m_dZoomFactor);
		}
	}
	else if (m_nCursorMode == DISP_CURSOR_MODE_INSERT_POLY_LABEL)
	{
		if (m_bPolyDrawing)
		{
			if (m_vecPolyLines.size() > 0)
				m_vecPolyLines[m_vecPolyLines.size() - 1].ptCoord = Point(m_dOffsetX + (point.x / m_dZoomFactor), m_dOffsetY + (point.y / m_dZoomFactor));
		}
	}
	else
	{
		if (m_bPolyDrawing)
		{
			if (m_nIdxSkel < MAX_SKELETON_COUNT)
				m_vecPolyLines[m_nIdxSkel].ptCoord = Point(m_dOffsetX + (point.x / m_dZoomFactor), m_dOffsetY + (point.y / m_dZoomFactor));
		}
	}

	// Right button down 상태일 경우 이전 좌표와의 차이만큼 영상 Offset 조정
	if (m_bRBtnDown)
	{
		//m_dOffsetX += (m_ptCursor.x - point.x) / m_dZoomFactor;
		//m_dOffsetY += (m_ptCursor.y - point.y) / m_dZoomFactor;
		m_dOffsetX = m_dOffsetX - round((point.x - m_ptCursor.x) / m_dZoomFactor);
		if (m_dOffsetX < 0 || m_nImgWidth < m_rtCtrl.Width() / m_dZoomFactor)
			m_dOffsetX = 0;
		if (m_dOffsetX > m_matImage.cols - m_rtCtrl.Width() / m_dZoomFactor)
			m_dOffsetX = m_matImage.cols - m_rtCtrl.Width() / m_dZoomFactor;

		m_dOffsetY = m_dOffsetY - round((point.y - m_ptCursor.y) / m_dZoomFactor);
		if (m_dOffsetY < 0 || m_nImgHeight < m_rtCtrl.Height() / m_dZoomFactor)
			m_dOffsetY = 0;
		if (m_dOffsetY > m_matImage.rows - m_rtCtrl.Height() / m_dZoomFactor)
			m_dOffsetY = m_matImage.rows - m_rtCtrl.Height() / m_dZoomFactor;

		m_ptCursor = point;
	}

	if (m_bLBtnDown || m_bRBtnDown || m_bPolyDrawing)
		Invalidate(FALSE);

	if (m_stMouseOnStatus.nIdx != -1 && m_stMouseOnStatus.nType == LABEL_UNIT_STYLE_POLY && m_stMouseOnStatus.nPos != -1)
		Invalidate(FALSE);

	CStatic::OnMouseMove(nFlags, point);
}

/// <summary>
/// Mouse wheel message handler
/// </summary>
/// <param name="nFlags"></param>
/// <param name="zDelta"></param>
/// <param name="pt"></param>
/// <returns></returns>
BOOL CESMImageCtr::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	double dZoom = 1.;
	if (!m_matImage.empty())
	{
		if (zDelta >= 0)
			dZoom *= 1.25;
		else
			dZoom /= 1.25;
	}

	m_dZoomFactor *= dZoom;
	if (m_dZoomFactor > 100.0) m_dZoomFactor = 100.0F;
	else if (m_dZoomFactor < m_dMinZoomFactor) m_dZoomFactor = m_dMinZoomFactor;

	// Control내 좌표로 변환
	ScreenToClient(&pt);

	// cursor 중심으로 확대
	m_dOffsetX += pt.x * (dZoom - 1.) / m_dZoomFactor;
	m_dOffsetY += pt.y * (dZoom - 1.) / m_dZoomFactor;

	if (m_dOffsetX < 0 || m_nImgWidth < m_rtCtrl.Width() / m_dZoomFactor)
		m_dOffsetX = 0;
	if (m_dOffsetY < 0 || m_nImgHeight < m_rtCtrl.Height() / m_dZoomFactor)
		m_dOffsetY = 0;
	if (m_dOffsetX > m_matImage.cols - m_rtCtrl.Width() / m_dZoomFactor)
		m_dOffsetX = m_matImage.cols - m_rtCtrl.Width() / m_dZoomFactor;
	if (m_dOffsetY > m_matImage.rows - m_rtCtrl.Height() / m_dZoomFactor)
		m_dOffsetY = m_matImage.rows - m_rtCtrl.Height() / m_dZoomFactor;


	Invalidate(FALSE);

	return CStatic::OnMouseWheel(nFlags, zDelta, pt);
}

/// <summary>
/// Mouse cursor set message handler
/// </summary>
/// <param name="pWnd"></param>
/// <param name="nHitTest"></param>
/// <param name="message"></param>
/// <returns></returns>
BOOL CESMImageCtr::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	if (m_bRBtnDown)
	{
		::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_HAND));
	}
	else if (m_nCursorMode == DISP_CURSOR_MODE_MOVE_LABEL)
	{
		if (m_stMouseOnStatus.nIdx == -1)
			::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		else
		{
			if (m_stMouseOnStatus.nType == LABEL_UNIT_STYLE_BOX)
			{
				if (m_stMouseOnStatus.nIdx != m_nSelLabel)
				{
					if (m_stMouseOnStatus.nPos != ROI_RESIZE_ALL)
						::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEALL));
					else
						::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				}
				else
				{
					switch (m_stMouseOnStatus.nPos)
					{
					case ROI_RESIZE_LT:
						::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZENWSE));
						break;
					case ROI_RESIZE_RT:
						::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZENESW));
						break;
					case ROI_RESIZE_RB:
						::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZENWSE));
						break;
					case ROI_RESIZE_LB:
						::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZENESW));
						break;
					case ROI_RESIZE_T:
						::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZENS));
						break;
					case ROI_RESIZE_R:
						::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEWE));
						break;
					case ROI_RESIZE_B:
						::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZENS));
						break;
					case ROI_RESIZE_L:
						::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEWE));
						break;
					case ROI_RESIZE_ALL:
						::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEALL));
						break;
					}
				}
			}
			else if (m_stMouseOnStatus.nType == LABEL_UNIT_STYLE_POLY)
			{
				::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEALL));
			}
			else if (m_stMouseOnStatus.nType == LABEL_UNIT_STYLE_SKEL)
			{
				::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEALL));
			}
		}
	}
	else if (m_nCursorMode == DISP_CURSOR_MODE_INSERT_BOX_LABEL)
	{
		::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_CROSS));
	}

	return TRUE;

	//return CStatic::OnSetCursor(pWnd, nHitTest, message);
}

/// <summary>
/// Load menu message handler
/// </summary>
/// <param name="pWnd"></param>
/// <param name="point"></param>
void CESMImageCtr::OnContextMenu(CWnd* pWnd, CPoint point)
{
	if (m_nSelLabel == -1)
		return;

	if (m_pLabels == NULL || (*m_pLabels).size() <= m_nSelLabel)
		return;

	// Polygon drawing mode일 경우에만 vertex add/del menu 출력
	if ((*m_pLabels)[m_nSelLabel].nStyle == LABEL_UNIT_STYLE_POLY)
	{
		CMenu muTemp, * pContextMenu;
		muTemp.LoadMenu(IDR_MENU_RBUTTON);
		pContextMenu = muTemp.GetSubMenu(0);
		pContextMenu->TrackPopupMenu(TPM_LEFTALIGN, point.x, point.y, AfxGetMainWnd());
	}
}

/// <summary>
/// Box drawing ratio setter
/// </summary>
/// <param name="dRatio"></param>
void CESMImageCtr::SetBoxRatio(double dRatio)
{
	m_dBoxRatio = dRatio;
}

/// <summary>
/// Drawing image
/// </summary>
/// <param name="dc"></param>
void CESMImageCtr::DrawImage(CPaintDC& dc)
{
	CRgn rgn;
	rgn.CreateRectRgnIndirect(m_rtCtrl);
	dc.SelectClipRgn(&rgn);

	CRect rect;
	GetClientRect(&rect);

	int nSrcWidth = rect.Width() / m_dZoomFactor;
	int nSrcHeight = rect.Height() / m_dZoomFactor;

	int nSrcX = (int)m_dOffsetX;
	int nSrcY = m_matImage.rows - nSrcHeight - m_dOffsetY;

	SetStretchBltMode(dc.GetSafeHdc(), COLORONCOLOR);
	StretchDIBits(dc.GetSafeHdc(), 0, 0, rect.Width(), rect.Height(), nSrcX, nSrcY, nSrcWidth, nSrcHeight, m_matImage.data, m_pBitmapInfo, DIB_RGB_COLORS, SRCCOPY);

	// 영상 공간 외 비어있는 부분은 black 색상으로 채우기
	dc.SelectStockObject(BLACK_BRUSH);
	if (m_matImage.cols * m_dZoomFactor < rect.Width())
	{
		dc.Rectangle(0, 0, -m_dOffsetX * m_dZoomFactor, rect.Height());
		dc.Rectangle((-m_dOffsetX + m_matImage.cols) * m_dZoomFactor, 0, rect.Width(), rect.Height());
	}
	if (m_matImage.rows * m_dZoomFactor < rect.Height())
	{
		dc.Rectangle(0, 0, rect.Width(), -m_dOffsetY * m_dZoomFactor);
		dc.Rectangle(0, (-m_dOffsetY + m_matImage.rows) * m_dZoomFactor, rect.Width(), rect.Height());
	}

}

/// <summary>
/// 현재 drawing 중인 box 표시
/// </summary>
/// <param name="dc"></param>
void CESMImageCtr::DrawROI(CPaintDC& dc)
{
	int nLeft, nTop, nRight, nBottom;

	nLeft = (m_rtROI.x - m_dOffsetX) * m_dZoomFactor;
	nTop = (m_rtROI.y - m_dOffsetY) * m_dZoomFactor;
	nRight = nLeft + (m_rtROI.width * m_dZoomFactor);
	nBottom = nTop + (m_rtROI.height * m_dZoomFactor);

	CPen pen(PS_SOLID, 1, RGB(255, 0, 0));

	dc.SelectObject(&pen);
	dc.SelectStockObject(NULL_BRUSH);
	dc.Rectangle(nLeft, nTop, nRight, nBottom);
}

/// <summary>
/// 현재 drawing 중인 polygon 표시
/// </summary>
/// <param name="dc"></param>
void CESMImageCtr::DrawPoly(CPaintDC& dc)
{
	POINT* poly = new POINT[m_vecPolyLines.size()];

	for (int i = 0; i < m_vecPolyLines.size(); i++)
	{
		poly[i].x = (m_vecPolyLines[i].ptCoord.x - m_dOffsetX) * m_dZoomFactor;
		poly[i].y = (m_vecPolyLines[i].ptCoord.y - m_dOffsetY) * m_dZoomFactor;
	}

	CPen penLine(PS_SOLID, 2, RGB(255, 0, 0));

	dc.SelectObject(&penLine);
	dc.SelectStockObject(NULL_BRUSH);
	dc.Polyline(poly, m_vecPolyLines.size());

	for (int i = 0; i < m_vecPolyLines.size(); i++)
	{
		CPen penVertex(PS_SOLID, 2, RGB(255, 255, 0));
		dc.SelectObject(&penVertex);
		dc.SelectStockObject(NULL_BRUSH);
		dc.Ellipse(poly[i].x - 3, poly[i].y - 3, poly[i].x + 3, poly[i].y + 3);
	}

	delete[] poly;
}

/// <summary>
/// 현재 drawing 중인 Skeleton 표시
/// </summary>
/// <param name="dc"></param>
void CESMImageCtr::DrawSkeleton(CPaintDC& dc)
{
	CPen penLine(PS_SOLID, 2, RGB(255, 0, 0));

	dc.SelectObject(&penLine);
	dc.SelectStockObject(NULL_BRUSH);

	for (int i = 0; i < MAX_LINK_COUNT; i++)
	{
		int idxA, idxB;
		idxA = g_Links[i][0];
		idxB = g_Links[i][1];

		if (m_vecPolyLines[idxA].bVis && m_vecPolyLines[idxB].bVis)
		{
			CPoint ptA, ptB;
			ptA = CPoint((m_vecPolyLines[idxA].ptCoord.x - m_dOffsetX) * m_dZoomFactor, (m_vecPolyLines[idxA].ptCoord.y - m_dOffsetY) * m_dZoomFactor);
			ptB = CPoint((m_vecPolyLines[idxB].ptCoord.x - m_dOffsetX) * m_dZoomFactor, (m_vecPolyLines[idxB].ptCoord.y - m_dOffsetY) * m_dZoomFactor);

			dc.MoveTo(ptA);
			dc.LineTo(ptB);
		}
	}

	for (int i = 0; i < m_vecPolyLines.size(); i++)
	{
		if (m_vecPolyLines[i].bVis)
		{
			CPoint pt((m_vecPolyLines[i].ptCoord.x - m_dOffsetX) * m_dZoomFactor, (m_vecPolyLines[i].ptCoord.y - m_dOffsetY) * m_dZoomFactor);
			CPen penVertex(PS_SOLID, 2, RGB(255, 255, 0));
			dc.SelectObject(&penVertex);
			dc.SelectStockObject(NULL_BRUSH);
			dc.Ellipse(pt.x - 3, pt.y - 3, pt.x + 3, pt.y + 3);
		}
	}

	CPoint ptText((m_vecPolyLines[m_nIdxSkel].ptCoord.x - m_dOffsetX) * m_dZoomFactor, (m_vecPolyLines[m_nIdxSkel].ptCoord.y - m_dOffsetY) * m_dZoomFactor);
	CRect rtText;

	rtText.left = ptText.x;
	rtText.right = rtText.left + g_SkeletonModel[m_nIdxSkel].GetLength() * 9;
	rtText.top = ptText.y + 1;
	rtText.bottom = rtText.top + 15;


	dc.SetTextColor(RGB(255, 255, 255));
	dc.SetBkMode(TRANSPARENT);

	dc.DrawText(g_SkeletonModel[m_nIdxSkel], rtText, DT_LEFT | DT_SINGLELINE);
}

/// <summary>
/// 전체 Label 표시
/// </summary>
/// <param name="dc"></param>
void CESMImageCtr::DrawLabels(CPaintDC& dc)
{
	if (m_pLabels == NULL)
		return;

	for (int i = 0; i < m_pLabels->size(); i++)
	{
		int nClass = (*m_pLabels)[i].nClass;

		CRect rtText;

		CString strClass;
		if (((CDatasetMakerDlg*)m_pParent)->m_vecClassNames.size() > nClass)
		{
			strClass.Format(_T("%d : ") + ((CDatasetMakerDlg*)m_pParent)->m_vecClassNames[nClass], nClass);
		}
		else
		{
			strClass.Format(_T("%d"), nClass);
		}

		if ((*m_pLabels)[i].nStyle == LABEL_UNIT_STYLE_BOX)
		{
			int nLeft, nTop, nRight, nBottom;

			// Box label의 2 vertex 정보를 기준으로 drawing 영역 결정
			nLeft = ((*m_pLabels)[i].vPts[BOX_LABEL_PT_POS_LT].ptCoord.x - m_dOffsetX) * m_dZoomFactor;
			nTop = ((*m_pLabels)[i].vPts[BOX_LABEL_PT_POS_LT].ptCoord.y - m_dOffsetY) * m_dZoomFactor;
			nRight = ((*m_pLabels)[i].vPts[BOX_LABEL_PT_POS_RB].ptCoord.x - m_dOffsetX) * m_dZoomFactor;
			nBottom = ((*m_pLabels)[i].vPts[BOX_LABEL_PT_POS_RB].ptCoord.y - m_dOffsetY) * m_dZoomFactor;

			int nPenSize = i != m_nSelLabel ? 1 : 3;

			CPen pen(PS_SOLID, nPenSize, RGB(g_ColorModel[nClass].r, g_ColorModel[nClass].g, g_ColorModel[nClass].b));

			dc.SelectObject(&pen);
			dc.SelectStockObject(NULL_BRUSH);
			dc.Rectangle(nLeft, nTop, nRight, nBottom);

			// 영상 중심보다 위에 있을경우 class name 위치를 label 하단으로 변경
			rtText.left = nLeft;
			rtText.right = nLeft + strClass.GetLength() * 9;
			if ((nTop + nBottom) / 2 < m_rtCtrl.Height() / 2)
			{
				rtText.top = nBottom + 1;
				rtText.bottom = nBottom + 16;
			}
			// 영상 중심보다 아래에 있을경우 class name 위치를 label 상단으로 변경
			else
			{
				rtText.top = nTop - 16;
				rtText.bottom = nTop + 1;
			}
		}
		else if ((*m_pLabels)[i].nStyle == LABEL_UNIT_STYLE_POLY)
		{
			POINT* poly = new POINT[(*m_pLabels)[i].vPts.size()];


			double dSumX = 0, dSumY = 0;

			for (int j = 0; j < (*m_pLabels)[i].vPts.size(); j++)
			{
				poly[j].x = ((*m_pLabels)[i].vPts[j].ptCoord.x - m_dOffsetX) * m_dZoomFactor;
				poly[j].y = ((*m_pLabels)[i].vPts[j].ptCoord.y - m_dOffsetY) * m_dZoomFactor;

				dSumX += poly[j].x;
				dSumY += poly[j].y;
			}

			dSumX /= (*m_pLabels)[i].vPts.size();
			dSumY /= (*m_pLabels)[i].vPts.size();

			// Polygon의 중심으로 class name 표시 위치 변경
			rtText.left = dSumX - strClass.GetLength() * 4;
			rtText.right = dSumX + strClass.GetLength() * 4;
			rtText.top = dSumY - 7;
			rtText.bottom = dSumY + 7;

			int nPenSize = i != m_nSelLabel ? 1 : 3;

			CPen pen(PS_SOLID, nPenSize, RGB(g_ColorModel[nClass].r, g_ColorModel[nClass].g, g_ColorModel[nClass].b));

			dc.SelectObject(&pen);
			dc.SelectStockObject(NULL_BRUSH);
			dc.Polygon(poly, (*m_pLabels)[i].vPts.size());

			// 선택된 Label인 경우 vertex drawing
			if (m_nSelLabel == i)
			{
				CPen penVertex(PS_SOLID, 2, RGB(255, 255, 0));
				CPen penMouseOn(PS_SOLID, 2, RGB(255, 0, 0));

				for (int j = 0; j < (*m_pLabels)[i].vPts.size(); j++)
				{
					// Vertex가 cursor 위에 있을 경우 색상 변경
					if (m_stMouseOnStatus.nType == LABEL_UNIT_STYLE_POLY && m_stMouseOnStatus.nIdx == i && m_stMouseOnStatus.nPos == j)
						dc.SelectObject(&penMouseOn);
					else
						dc.SelectObject(&penVertex);

					dc.Ellipse(poly[j].x - 3, poly[j].y - 3, poly[j].x + 3, poly[j].y + 3);
				}
			}

			delete[] poly;
		}
		else
		{
			if ((*m_pLabels)[i].vPts.size() < MAX_SKELETON_COUNT)
				continue;

			CPoint ptText(((*m_pLabels)[i].vPts[0].ptCoord.x - m_dOffsetX) * m_dZoomFactor, ((*m_pLabels)[i].vPts[0].ptCoord.y - m_dOffsetY) * m_dZoomFactor);

			rtText.left = ptText.x - strClass.GetLength() * 4;
			rtText.right = ptText.x + strClass.GetLength() * 4;
			rtText.top = ptText.y - 15;
			rtText.bottom = ptText.y;

			int nPenSize = i != m_nSelLabel ? 1 : 3;

			CPen pen(PS_SOLID, nPenSize, RGB(g_ColorModel[nClass].r, g_ColorModel[nClass].g, g_ColorModel[nClass].b));

			dc.SelectObject(&pen);
			dc.SelectStockObject(NULL_BRUSH);

			for (int j = 0; j < MAX_LINK_COUNT; j++)
			{
				int idxA, idxB;
				idxA = g_Links[j][0];
				idxB = g_Links[j][1];

				if ((*m_pLabels)[i].vPts[idxA].bVis && (*m_pLabels)[i].vPts[idxB].bVis)
				{
					CPoint ptA, ptB;
					ptA = CPoint(((*m_pLabels)[i].vPts[idxA].ptCoord.x - m_dOffsetX) * m_dZoomFactor, ((*m_pLabels)[i].vPts[idxA].ptCoord.y - m_dOffsetY) * m_dZoomFactor);
					ptB = CPoint(((*m_pLabels)[i].vPts[idxB].ptCoord.x - m_dOffsetX) * m_dZoomFactor, ((*m_pLabels)[i].vPts[idxB].ptCoord.y - m_dOffsetY) * m_dZoomFactor);

					dc.MoveTo(ptA);
					dc.LineTo(ptB);
				}
			}

			// vertex drawing
			CPen penVertex(PS_SOLID, 2, RGB(255, 255, 0));
			CPen penMouseOn(PS_SOLID, 2, RGB(255, 0, 0));
			CPen penNone(PS_SOLID, 2, RGB(150, 150, 150));

			for (int j = 0; j < (*m_pLabels)[i].vPts.size(); j++)
			{
				// Vertex가 cursor 위에 있을 경우 색상 변경
				if (!(*m_pLabels)[i].vPts[j].bVis)
					continue;

				if (m_nSelLabel == i)
				{
					if (m_stMouseOnStatus.nType == LABEL_UNIT_STYLE_SKEL && m_stMouseOnStatus.nIdx == i && m_stMouseOnStatus.nPos == j)
						dc.SelectObject(&penMouseOn);
					else
						dc.SelectObject(&penVertex);
				}
				else
					dc.SelectObject(&penNone);

				dc.SelectStockObject(NULL_BRUSH);

				CPoint pt(((*m_pLabels)[i].vPts[j].ptCoord.x - m_dOffsetX) * m_dZoomFactor, ((*m_pLabels)[i].vPts[j].ptCoord.y - m_dOffsetY) * m_dZoomFactor);

				dc.Ellipse(pt.x - 3, pt.y - 3, pt.x + 3, pt.y + 3);
			}
		}

		dc.SetTextColor(RGB(g_ColorModel[nClass].r, g_ColorModel[nClass].g, g_ColorModel[nClass].b));
		dc.SetBkMode(TRANSPARENT);

		// Class name 출력
		dc.DrawText(strClass, rtText, DT_LEFT | DT_SINGLELINE);
	}
}

/// <summary>
/// Scene title 표시
/// </summary>
/// <param name="dc"></param>
void CESMImageCtr::DrawSceneTitle(CPaintDC& dc)
{
	CRect rtView, rtText;

	this->GetClientRect(&rtView);

	rtText.left = rtView.Width() / 2 - m_strSceneTitle.GetLength() * 4.5;
	rtText.right = rtView.Width() / 2 + m_strSceneTitle.GetLength() * 4.5;
	rtText.top = 0;
	rtText.bottom = 15;

	dc.SetTextColor(RGB(255, 255, 255));
	dc.SetBkMode(TRANSPARENT);

	dc.DrawText(m_strSceneTitle, rtText, DT_CENTER | DT_SINGLELINE);
}

/// <summary>
/// Polygon label에 vertex 추가
/// </summary>
void CESMImageCtr::Addvertex()
{
	if (m_pLabels == NULL)
		return;

	vector<StPolyVertex>& pts = (*m_pLabels)[m_nSelLabel].vPts;

	Point pt;
	pt.x = m_dOffsetX + (m_ptCursor.x / m_dZoomFactor);
	pt.y = m_dOffsetY + (m_ptCursor.y / m_dZoomFactor);

	int nNearestIdx = 0;
	double dMinDist = 99999999.;

	// 가장 인접한 선분 구하기
	for (int i = 0; i < pts.size(); i++)
	{
		Point2f ptA, ptB, ptCross;
		ptA = pts[i].ptCoord;
		ptB = pts[(i + 1) % pts.size()].ptCoord;

		if (ptA.x == ptB.x)
		{
			ptCross.x = ptA.x;
			ptCross.y = pt.y;
		}
		else if (ptA.y == ptB.y)
		{
			ptCross.x = pt.x;
			ptCross.y = ptA.y;
		}
		else
		{
			double m1, m2, k1, k2;

			// 기울기 m1
			m1 = (ptA.y - ptB.y) / (ptA.x - ptB.x);
			// 상수 k1
			k1 = -m1 * ptA.x + ptA.y;

			// 기울기 m2
			m2 = -1.0 / m1;
			// p 를 지나기 때문에 yp = m2 * xp + k2 => k2 = yp - m2 * xp
			k2 = pt.y - m2 * pt.x;

			// 두 직선 y = m1x + k1, y = m2x + k2 의 교점을 구한다
			ptCross.x = (k2 - k1) / (m1 - m2);
			ptCross.y = m1 * ptCross.x + k1;
		}

		double dDist = 99999999.;

		// 구한 점이 선분 위에 있는 지 확인
		if (ptCross.x >= MIN(ptA.x, ptB.x) && ptCross.x <= MAX(ptA.x, ptB.x) &&
			ptCross.y >= MIN(ptA.y, ptB.y) && ptCross.y <= MAX(ptA.y, ptB.y))
		{
			dDist = dist(Point(ptCross), pt);
		}

		if (dDist < dMinDist)
		{
			nNearestIdx = i;
			dMinDist = dDist;
		}
	}

	// 가장 가까운 인덱스 다음에 vertex 추가
	pts.insert(pts.begin() + ((nNearestIdx + 1) % pts.size()), { true, pt });

	Invalidate(FALSE);
}

/// <summary>
/// 선택된 polygon vertex 삭제
/// </summary>
void CESMImageCtr::Deletevertex()
{
	if (m_pLabels == NULL)
		return;

	if (m_stMouseOnStatus.nIdx != m_nSelLabel || m_stMouseOnStatus.nPos == -1)
		return;

	vector<StPolyVertex>& pts = (*m_pLabels)[m_nSelLabel].vPts;

	pts.erase(pts.begin() + m_stMouseOnStatus.nPos);

	Invalidate(FALSE);
}

/// <summary>
/// p1과 p2 사이의 euclidean distance 계산
/// </summary>
/// <param name="p1"></param>
/// <param name="p2"></param>
/// <returns></returns>
double CESMImageCtr::dist(Point& p1, Point& p2)
{
	return sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2));
}

/// <summary>
/// 입력한 point의 위치가 polygon 공간내에 존재하는지 확인
/// </summary>
/// <param name="point"></param>
/// <param name="vecPoly"></param>
/// <returns></returns>
bool CESMImageCtr::PointInPolygon(Point& point, vector<Point>& vecPoly)
{
	bool bPointInPolygon = false;

	int iCrosses = 0; // 교차 횟수

	for (int i = 0; i < vecPoly.size(); i++)
	{
		// 0,1 선분, 1,2선분, ... , n-1,n 선분 조사 n,0 선분 조사(cross조사)
		int j = (i + 1) % vecPoly.size();

		//점(point)이 선분(m_apnt[i], m_apnt[j])의 y좌표 사이에 있음
		if ((vecPoly[i].y > point.y) != (vecPoly[j].y > point.y))
		{
			//atX는 점(point)을 지나는 수평선과 선분(m_apnt[i], m_apnt[j])의 교점
			double atX = (((double)(vecPoly[j].x - vecPoly[i].x) / (vecPoly[j].y - vecPoly[i].y)) * (point.y - vecPoly[i].y)) + vecPoly[i].x;

			//atX가 오른쪽 반직선과의 교점이 맞으면 교점의 개수를 증가시킨다.
			if (point.x < atX)
				iCrosses++;
		}
	}

	// 홀수면 내부, 짝수면 외부에 있음
	if (0 == (iCrosses % 2))
		bPointInPolygon = false;
	else
		bPointInPolygon = true;

	return bPointInPolygon;
}
