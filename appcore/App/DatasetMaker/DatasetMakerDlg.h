﻿
// DatasetMakerDlg.h: 헤더 파일
//

#pragma once

#include "ESMImageCtr.h"
#include "ESMCommonDefine.h"
#include "OptFlowTracker.h"

#include <DeepInfer.h>
#define MAX_CASHING_SIZE	50		// 빠른 영상 출력을 위한 Image cashing max size


// CDatasetMakerDlg 대화 상자
class CDatasetMakerDlg : public CDialogEx
{

// 생성입니다.
public:
	CDatasetMakerDlg(CWnd* pParent = nullptr);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DLG_DATASET_MAKER };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	/* 메시지 처리기 */
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnClose();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedRbSourceVideo();
	afx_msg void OnBnClickedRbSourceFolder();
	afx_msg void OnBnClickedBtnPathSource();
	afx_msg void OnBnClickedBtnHeadFrame();
	afx_msg void OnBnClickedBtnPrevFrame();
	afx_msg void OnBnClickedBtnNextFrame();
	afx_msg void OnBnClickedBtnTailFrame();
	afx_msg void OnBnClickedChkSelectAllIdx();
	afx_msg void OnBnClickedBtnAddSource();
	afx_msg void OnLvnItemchangedListSourceImages(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnNMSetfocusListSourceImages(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBnClickedBtnAddSourceImage();
	afx_msg void OnBnClickedBtnDelSourceImage();
	afx_msg void OnBnClickedBtnClearSources();
	afx_msg void OnBnClickedBtnSelectSourceAll();
	afx_msg void OnBnClickedBtnSelectSourceNone();
	afx_msg void OnBnClickedBtnPrevPage();
	afx_msg void OnBnClickedBtnNextPage();
	afx_msg void OnCbnSelchangeCbViewLength();
	afx_msg void OnBnClickedBtnSetSceneTitle();
	afx_msg void OnLbnSelchangeListClasses();
	afx_msg void OnBnClickedBtnSetClassName();
	afx_msg void OnBnClickedBtnAddClass();
	afx_msg void OnBnClickedBtnDelClass();
	afx_msg void OnLvnItemchangedListLabels(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBnClickedBtnClassColor();
	afx_msg void OnBnClickedChkSelectMultiFrames();
	afx_msg void OnBnClickedRbBoxRatioFree();
	afx_msg void OnBnClickedRbBoxRatioXy();
	afx_msg void OnBnClickedChkAddBoxLabel();
	afx_msg void OnBnClickedChkAddPolyLabel();
	afx_msg void OnBnClickedChkAddSkelLabel();
	afx_msg	void OnBnClickedBtnDeleteLabel();
	afx_msg void OnBnClickedBtnEditLabel();
	afx_msg void OnBnClickedBtnTrackLabel();
	afx_msg void OnBnClickedBtnClearLabels();
	afx_msg void OnMenuAddvertex();
	afx_msg void OnMenuDeletevertex();
	afx_msg void OnBnClickedBtnImportJson();
	afx_msg void OnBnClickedBtnExportJson();
	afx_msg void OnBnClickedBtnParsingMpii();
	afx_msg void OnBnClickedBtnParsingCoco();
	afx_msg void OnBnClickedBtnTest();
	

	/* Frame 처리 관련 함수 */
	void DisplaySourceFrame();
	void AddSourceFrame();
	void UpdateLoadButtons();
	void UpdateFrameLabel();
	void UpdateToolBtnImg();
	void UpdateDatasetFrameList();
	Mat GetCashingImage(int nIdx);
	Mat GetDatasetItemImage(int nIdx);
	bool LoadSelectFrame(Mat& frame, CString strFilePath, int nIdx);
	bool LoadStillImage(Mat& frame, CString strFilePath);
	void SelectFrame(int idx);


	/* Dataset import/export/parsing 관련 함수 */
	vector<string> SplitPath(const string& str, const set<char> delimiters);
	vector<CString> GetImageFilesInDirectory(const CString strPath);
	int SaveLabelsJson(const CString strPath);
	int SaveSkeletonsCOCOJson(const CString strPath);
	int LoadExistingLabelsJson(const CString strPath);
	int LoadBBoxTxt(const CString strJsonPath);
	int LoadSkeletonsCOCOJson(const CString strJsonPath, const CString strImgFolderPath);
	int LoadMPII(const CString strJsonPath, const CString strImgFolderPath);
	int LoadCOCO(const CString strJsonPath, const CString strImgFolderPath);
	vector<vector<Size2f>> GetAnchorsUsingKmeans();
	int SaveAnchorsInfo(const vector<vector<Size2f>>& anchors, const CString strPath);

	void SetCheckSelectMultiFrames(bool bChk) { m_chkSelectMultiFrames.SetCheck(bChk); }
	void SetCheckAddBoxLabelBtn(bool bChk) { m_chkAddBoxLabel.SetCheck(bChk); }
	void SetCheckAddPolyLabelBtn(bool bChk) { m_chkAddPolyLabel.SetCheck(bChk); }
	void SetCheckAddSkelLabelBtn(bool bChk) { m_chkAddSkelLabel.SetCheck(bChk); }
	void UpdateLabelButtons();
	void UpdateLabelList();
	void InsertBoxLabel(Rect rt);
	void InsertPolyLabel(vector<StPolyVertex>& poly);
	void InsertSkelLabel(vector<StPolyVertex>& skel);
	void SelectLabel(int idx);
	void UpdateClassCount();
	void UpdateClassInfo();

	void SetCurLabel(StLabelUnit label);

	void StartProgress();
	void SetProgress(int nPos, CString strMsg = _T(""));
	void EndProgress();

	/* semi-supervised learning 관련 함수 */
	int LoadPoseEngine(const CString strModelPath);

public:

	/* Control 관련 변수 */
	CButton m_rbSourceVideo;
	CButton m_rbSourceFolder;
	CEdit m_editPathSource;
	CButton m_chkSelectAllIdx;
	CEdit m_editSourceIdx;
	CEdit m_editSourceCnt;
	CSliderCtrl m_sliFrames;
	CStatic m_labelFrameName;

	CListCtrl m_lstSourceImages;
	CESMImageCtr m_dispMainImage;

	CComboBox m_cbViewLength;
	CEdit m_editPageIdx;
	CEdit m_editPageCount;

	CMFCButton m_btnBrushColor;

	CButton m_chkSelectMultiFrames;
	CButton m_chkAddBoxLabel;
	CButton m_rbBoxRatioFree;
	CButton m_rbBoxRatioXY;
	CButton m_chkAddPolyLabel;
	CButton m_chkAddSkelLabel;

	CEdit m_editSceneTitle;

	CListBox m_lstClasses;
	CEdit m_editClassName;

	CButton m_btnDelLabel;
	CButton m_btnEditLabel;
	CListCtrl m_lstLabels;

	CButton m_chkOptionUseAnchors;

private:
	/* Frame 로드 관련 private 변수 */
	VideoCapture m_vcObj;			// Video caputure를 위한 객체

	bool m_bInitializing;	// 초기화 수행 여부

	MODE_SRC m_nSourceMode; // 0 : Video, 1 : Folder (Images)

	int m_nSourceIdx;		// 현재 선택된 Dataset frame index

	bool m_bSelectAllIdx;	// 전체 index 선택 여부

	int m_nFrameIdx;		// 로드된 frame index
	int m_nFrameCnt;		// 로드된 frame count

	int m_nBoxRatioX;		// Box label drawing시 ratio x
	int m_nBoxRatioY;		// Box label drawing시 ratio y

	int m_nClassCount;		// 현재 Class의 개수
	int m_nSelClass;		// 현재 선택된 Class index
	int m_nSelLabel;		// 현재 선택된 Label index

	Mat m_matCashing[MAX_CASHING_SIZE];		// Image cashing을 위한 mat 배열
	int m_nCashingSize;						// Image cashing 크기
	int m_nPageNum;							// 현재 page index

public:
	/* Frame 로드 관련 public 변수 */
	vector<StFrameInfo> m_vecFrameList;			// 로드 후 dataset 추가를 위해 대기중인 frames list
	vector<StDataSetItem> m_vecDataSetItems;	// Dataset items list
	vector<CString> m_vecClassNames;			// Class names list

	StMultiDrawing m_stMultiDrawing;			// Multi drawing을 위한 상태 변수


	/* Tracking 기능 관련 변수 (Box label) */
	COptFlowTracker m_optFlow;		// Optical flow 수행을 위한 객체
	vector<coord_t> m_vecCurCoord;
	atomic<bool> m_bTrackerOn;


	/* Image 관련 변수 */
	CString m_strPathSource;		// 로드시 소스 경로
	CString m_strPathResult;		// Import/Export시 경로


	/* Anchors 사용 관련 변수 */
	int m_nOptionKCount;			// Anchors 적용시 K-means clustering에 사용될 K 값
	afx_msg void OnBnClickedBtnImportSkeleton();
	afx_msg void OnBnClickedBtnExportSkeleton();
	afx_msg void OnBnClickedBtnImportBbox();
	afx_msg void OnBnClickedBtnLoadPoseModel();

private:
	/* tensorRT 관련 변수 */
	DeepInfer* _deepInfer;
	DeepInfer::context_t _deepInfer_ctx;

	
};
