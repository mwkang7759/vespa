#include "pch.h"
#include "OptFlowTracker.h"

COptFlowTracker::COptFlowTracker(int win_size, int max_level, int iterations, int _flow_error) :
    m_nFlowError((_flow_error > 0) ? _flow_error : (win_size * 4))
{
}


void COptFlowTracker::UpdateBbox(std::vector<coord_t> _cur_bbox_vec)
{
    m_vecBboxCur = _cur_bbox_vec;
    m_vecFlagsBboxGood = std::vector<bool>(m_vecBboxCur.size(), true);

    m_ptsFlowPrev.clear();

    for (auto& i : m_vecBboxCur) {
        float x_center = (i.abs_rect.x + i.abs_rect.width / 2.0F);
        float y_center = (i.abs_rect.y + i.abs_rect.height / 2.0F);
        m_ptsFlowPrev.push_back(cv::Point2f(x_center, y_center));
    }
}

void COptFlowTracker::UpdateTrackingFlow(cv::Mat new_src_mat, std::vector<coord_t> _cur_bbox_vec)
{
    if (new_src_mat.channels() == 1) {
        m_matSrcGray = new_src_mat.clone();
    }
    else if (new_src_mat.channels() == 3) {
        cv::cvtColor(new_src_mat, m_matSrcGray, cv::COLOR_BGR2GRAY, 1);
    }
    else if (new_src_mat.channels() == 4) {
        cv::cvtColor(new_src_mat, m_matSrcGray, cv::COLOR_BGRA2GRAY, 1);
    }
    else {
        std::cerr << " Warning: new_src_mat.channels() is not: 1, 3 or 4. It is = " << new_src_mat.channels() << " \n";
        return;
    }
    UpdateBbox(_cur_bbox_vec);
}

std::vector<coord_t> COptFlowTracker::TrackingFlow(cv::Mat new_dst_mat, bool check_error)
{
    cv::cvtColor(new_dst_mat, m_matDstGray, cv::COLOR_BGR2GRAY, 1);

    if (m_matSrcGray.rows != m_matDstGray.rows || m_matSrcGray.cols != m_matDstGray.cols) {
        m_matSrcGray = m_matDstGray.clone();
        //std::cerr << " Warning: src_grey.rows != dst_grey.rows || src_grey.cols != dst_grey.cols \n";
        return m_vecBboxCur;
    }

    if (m_ptsFlowPrev.size() < 1) {
        return m_vecBboxCur;
    }

    calcOpticalFlowPyrLK(m_matSrcGray, m_matDstGray, m_ptsFlowPrev, m_ptsFlowCur, m_vecStatus, m_vecErr);

    m_matDstGray.copyTo(m_matSrcGray);

    std::vector<coord_t> result_bbox_vec;

    if (m_vecErr.size() == m_vecBboxCur.size() && m_vecStatus.size() == m_vecBboxCur.size())
    {
        for (int i = 0; i < m_vecBboxCur.size(); ++i)
        {
            cv::Point2f cur_key_pt = m_ptsFlowCur[i];
            cv::Point2f prev_key_pt = m_ptsFlowPrev[i];

            float moved_x = cur_key_pt.x - prev_key_pt.x;
            float moved_y = cur_key_pt.y - prev_key_pt.y;

            if (abs(moved_x) < 100 && abs(moved_y) < 100 && m_vecFlagsBboxGood[i])
                if (m_vecErr[i] < m_nFlowError && m_vecStatus[i] != 0 &&
                    ((float)m_vecBboxCur[i].abs_rect.x + moved_x) > 0 && ((float)m_vecBboxCur[i].abs_rect.y + moved_y) > 0)
                {
                    m_vecBboxCur[i].abs_rect.x += moved_x;// +0.5;
                    m_vecBboxCur[i].abs_rect.y += moved_y;// +0.5;
                    result_bbox_vec.push_back(m_vecBboxCur[i]);
                }
                else m_vecFlagsBboxGood[i] = false;
            else m_vecFlagsBboxGood[i] = false;
        }
    }

    m_ptsFlowPrev = m_ptsFlowCur;

    return result_bbox_vec;
}