﻿// EditLabelDlg.cpp: 구현 파일
//

#include "pch.h"
#include "DatasetMaker.h"
#include "EditLabelDlg.h"
#include "afxdialogex.h"
#include "DatasetMakerDlg.h"


// CEditLabelDlg 대화 상자

IMPLEMENT_DYNAMIC(CEditLabelDlg, CDialogEx)

CEditLabelDlg::CEditLabelDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DLG_EDIT_LABEL, pParent)
	, m_nModLabelClass(0)
	, m_nModLabelX(0)
	, m_nModLabelY(0)
	, m_nModLabelWidth(0)
	, m_nModLabelHeight(0)
{
	m_pParent = pParent;
}

CEditLabelDlg::~CEditLabelDlg()
{
}

void CEditLabelDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_MOD_LABEL_CLASS, m_nModLabelClass);
	DDX_Text(pDX, IDC_EDIT_MOD_LABEL_X, m_nModLabelX);
	DDX_Text(pDX, IDC_EDIT_MOD_LABEL_Y, m_nModLabelY);
	DDX_Text(pDX, IDC_EDIT_MOD_LABEL_WIDTH, m_nModLabelWidth);
	DDX_Text(pDX, IDC_EDIT_MOD_LABEL_HEIGHT, m_nModLabelHeight);
}


BEGIN_MESSAGE_MAP(CEditLabelDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CEditLabelDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CEditLabelDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// CEditLabelDlg 메시지 처리기


void CEditLabelDlg::OnBnClickedOk()
{
	UpdateData(TRUE);

	StLabelUnit label;
	if (m_nModLabelClass < 0)
		m_nModLabelClass = 0;
	else if (m_nModLabelClass > ((CDatasetMakerDlg*)m_pParent)->m_vecClassNames.size() - 1)
		m_nModLabelClass = ((CDatasetMakerDlg*)m_pParent)->m_vecClassNames.size() - 1;

	label.nClass = m_nModLabelClass;
	label.nStyle = m_nModLabelStyle;

	if (m_nModLabelStyle == LABEL_UNIT_STYLE_BOX)
	{
		label.vPts.push_back({ true, Point(m_nModLabelX, m_nModLabelY) });
		label.vPts.push_back({ true, Point(m_nModLabelX + m_nModLabelWidth, m_nModLabelY + m_nModLabelHeight) });
	}
	else
	{
		label.vPts = m_pLabel->vPts;
	}
	((CDatasetMakerDlg*)m_pParent)->SetCurLabel(label);

	CDialogEx::OnOK();
}


void CEditLabelDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialogEx::OnCancel();
}


BOOL CEditLabelDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	if (m_pLabel != NULL)
	{
		m_nModLabelStyle = m_pLabel->nStyle;
		m_nModLabelClass = m_pLabel->nClass;
		if (m_pLabel->nStyle == LABEL_UNIT_STYLE_BOX)
		{
			m_nModLabelX = m_pLabel->vPts[BOX_LABEL_PT_POS_LT].ptCoord.x;
			m_nModLabelY = m_pLabel->vPts[BOX_LABEL_PT_POS_LT].ptCoord.y;
			m_nModLabelWidth = m_pLabel->vPts[BOX_LABEL_PT_POS_RB].ptCoord.x - m_pLabel->vPts[BOX_LABEL_PT_POS_LT].ptCoord.x;
			m_nModLabelHeight = m_pLabel->vPts[BOX_LABEL_PT_POS_RB].ptCoord.y - m_pLabel->vPts[BOX_LABEL_PT_POS_LT].ptCoord.y;

			UpdateData(FALSE);
		}
	}
	else
	{
		OnCancel();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
