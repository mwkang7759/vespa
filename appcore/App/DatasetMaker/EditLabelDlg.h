﻿#pragma once

#include "ESMCommonDefine.h"

// CEditLabelDlg 대화 상자

class CEditLabelDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CEditLabelDlg)

public:
	CEditLabelDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CEditLabelDlg();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DLG_EDIT_LABEL	};
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();

	StLabelUnit* m_pLabel;

public:
	CWnd* m_pParent;
	virtual BOOL OnInitDialog();
	int m_nModLabelClass;
	int m_nModLabelStyle;
	int m_nModLabelX;
	int m_nModLabelY;
	int m_nModLabelWidth;
	int m_nModLabelHeight;
};
