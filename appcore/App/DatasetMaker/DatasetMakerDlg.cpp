﻿
// DatasetMakerDlg.cpp: 구현 파일
//

#include "pch.h"
#include "framework.h"
#include "DatasetMaker.h"
#include "DatasetMakerDlg.h"
#include "afxdialogex.h"
//#include <vld.h>
#include "MultiDrawingDlg.h"
#include "EditLabelDlg.h"
#include "EditRatioDlg.h"
#include "SelectClassDlg.h"
#include "ProgressDlg.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

extern StClassModel g_ColorModel[MAX_CLASS_COUNT];	// 클래스별 컬러 정보

const string g_SkeletonModel[MAX_SKELETON_COUNT] = {
	"nose", "left_eye", "right_eye", "left_ear", "right_ear",
	"left_shoulder", "right_shoulder", "left_elbow", "right_elbow", "left_wrist", "right_wrist",
	"left_hip", "right_hip", "left_knee", "right_knee", "left_ankle", "right_ankle"	
};	// Skeleton의 각 vertex별 string 정보
const int g_Links[MAX_LINK_COUNT][2] = 
{ {16, 14}, {14, 12}, {17, 15}, {15, 13}, {12, 13}, {6, 12}, {7, 13}, {6, 7}, {6, 8},
	{7, 9}, {8, 10}, {9, 11}, {2, 3}, {1, 2}, {1, 3}, {2, 4}, {3, 5}, {4, 6}, {5, 7} };


CProgressDlg* m_dlgProgress;		// 진행 상태 확인을 위한 프로그레스 바 

// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


// CDatasetMakerDlg 대화 상자



CDatasetMakerDlg::CDatasetMakerDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DLG_DATASET_MAKER, pParent)
	, m_bInitializing(false)
	, m_nPageNum(0)
	, m_bSelectAllIdx(false)
	, m_nFrameIdx(0)
	, m_nFrameCnt(0)
	, m_nSourceIdx(0)
	, m_nBoxRatioX(0)
	, m_nBoxRatioY(0)
	, m_nClassCount(1)
	, m_nOptionKCount(6)
	, m_bTrackerOn(false)
	, m_nCashingSize(30)
	, m_nSelClass(1)
	, m_nSelLabel(-1)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CDatasetMakerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_RB_SOURCE_VIDEO, m_rbSourceVideo);
	DDX_Control(pDX, IDC_RB_SOURCE_FOLDER, m_rbSourceFolder);
	DDX_Control(pDX, IDC_EDIT_PATH_SOURCE, m_editPathSource);
	DDX_Control(pDX, IDC_CHK_SELECT_ALL_IDX, m_chkSelectAllIdx);
	DDX_Control(pDX, IDC_EDIT_SOURCE_IDX, m_editSourceIdx);
	DDX_Control(pDX, IDC_EDIT_SOURCE_CNT, m_editSourceCnt);
	DDX_Control(pDX, IDC_SLIDER_FRAMES, m_sliFrames);
	DDX_Control(pDX, IDC_LABEL_FRAME_NAME, m_labelFrameName);
	DDX_Control(pDX, IDC_LIST_SOURCE_IMAGES, m_lstSourceImages);
	DDX_Control(pDX, IDC_DISP_MAIN_IMAGE, m_dispMainImage);
	DDX_Control(pDX, IDC_CB_VIEW_LENGTH, m_cbViewLength);
	DDX_Control(pDX, IDC_EDIT_PAGE_IDX, m_editPageIdx);
	DDX_Control(pDX, IDC_EDIT_PAGE_COUNT, m_editPageCount);
	DDX_Control(pDX, IDC_BTN_CLASS_COLOR, m_btnBrushColor);
	DDX_Control(pDX, IDC_CHK_SELECT_MULTI_FRAMES, m_chkSelectMultiFrames);
	DDX_Control(pDX, IDC_CHK_ADD_BOX_LABEL, m_chkAddBoxLabel);
	DDX_Control(pDX, IDC_RB_BOX_RATIO_FREE, m_rbBoxRatioFree);
	DDX_Control(pDX, IDC_RB_BOX_RATIO_XY, m_rbBoxRatioXY);
	DDX_Control(pDX, IDC_CHK_ADD_POLY_LABEL, m_chkAddPolyLabel);
	DDX_Control(pDX, IDC_CHK_ADD_SKEL_LABEL, m_chkAddSkelLabel);
	DDX_Text(pDX, IDC_EDIT_CLASS_COUNT, m_nClassCount);
	DDX_Control(pDX, IDC_LIST_CLASSES, m_lstClasses);
	DDX_Control(pDX, IDC_EDIT_CLASS_NAME, m_editClassName);
	DDX_Control(pDX, IDC_CHK_OPTION_USE_ANCHORS, m_chkOptionUseAnchors);
	DDX_Text(pDX, IDC_EDIT_OPTION_K_COUNT, m_nOptionKCount);
	DDX_Control(pDX, IDC_EDIT_SCENE_TITLE, m_editSceneTitle);
	DDX_Control(pDX, IDC_LIST_LABELS, m_lstLabels);
	DDX_Control(pDX, IDC_BTN_DELETE_LABEL, m_btnDelLabel);
	DDX_Control(pDX, IDC_BTN_EDIT_LABEL, m_btnEditLabel);
}

BEGIN_MESSAGE_MAP(CDatasetMakerDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &CDatasetMakerDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CDatasetMakerDlg::OnBnClickedCancel)
	ON_WM_CLOSE()
	ON_WM_HSCROLL()
	ON_BN_CLICKED(IDC_RB_SOURCE_VIDEO, &CDatasetMakerDlg::OnBnClickedRbSourceVideo)
	ON_BN_CLICKED(IDC_RB_SOURCE_FOLDER, &CDatasetMakerDlg::OnBnClickedRbSourceFolder)
	ON_BN_CLICKED(IDC_BTN_PATH_SOURCE, &CDatasetMakerDlg::OnBnClickedBtnPathSource)
	ON_BN_CLICKED(IDC_BTN_HEAD_FRAME, &CDatasetMakerDlg::OnBnClickedBtnHeadFrame)
	ON_BN_CLICKED(IDC_BTN_PREV_FRAME, &CDatasetMakerDlg::OnBnClickedBtnPrevFrame)
	ON_BN_CLICKED(IDC_BTN_NEXT_FRAME, &CDatasetMakerDlg::OnBnClickedBtnNextFrame)
	ON_BN_CLICKED(IDC_BTN_TAIL_FRAME, &CDatasetMakerDlg::OnBnClickedBtnTailFrame)
	ON_BN_CLICKED(IDC_CHK_SELECT_ALL_IDX, &CDatasetMakerDlg::OnBnClickedChkSelectAllIdx)
	ON_BN_CLICKED(IDC_BTN_ADD_SOURCE, &CDatasetMakerDlg::OnBnClickedBtnAddSource)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_SOURCE_IMAGES, &CDatasetMakerDlg::OnLvnItemchangedListSourceImages)
	ON_NOTIFY(NM_SETFOCUS, IDC_LIST_SOURCE_IMAGES, &CDatasetMakerDlg::OnNMSetfocusListSourceImages)
	ON_BN_CLICKED(IDC_BTN_ADD_SOURCE_IMAGE, &CDatasetMakerDlg::OnBnClickedBtnAddSourceImage)
	ON_BN_CLICKED(IDC_BTN_DEL_SOURCE_IMAGE, &CDatasetMakerDlg::OnBnClickedBtnDelSourceImage)
	ON_BN_CLICKED(IDC_BTN_CLEAR_SOURCES, &CDatasetMakerDlg::OnBnClickedBtnClearSources)
	ON_BN_CLICKED(IDC_BTN_SELECT_SOURCE_ALL, &CDatasetMakerDlg::OnBnClickedBtnSelectSourceAll)
	ON_BN_CLICKED(IDC_BTN_SELECT_SOURCE_NONE, &CDatasetMakerDlg::OnBnClickedBtnSelectSourceNone)
	ON_BN_CLICKED(IDC_BTN_PREV_PAGE, &CDatasetMakerDlg::OnBnClickedBtnPrevPage)
	ON_BN_CLICKED(IDC_BTN_NEXT_PAGE, &CDatasetMakerDlg::OnBnClickedBtnNextPage)
	ON_CBN_SELCHANGE(IDC_CB_VIEW_LENGTH, &CDatasetMakerDlg::OnCbnSelchangeCbViewLength)
	ON_BN_CLICKED(IDC_BTN_SET_TITLE_CLASS, &CDatasetMakerDlg::OnBnClickedBtnSetSceneTitle)
	ON_LBN_SELCHANGE(IDC_LIST_CLASSES, &CDatasetMakerDlg::OnLbnSelchangeListClasses)
	ON_BN_CLICKED(IDC_BTN_SET_CLASS_NAME, &CDatasetMakerDlg::OnBnClickedBtnSetClassName)
	ON_BN_CLICKED(IDC_BTN_ADD_CLASS, &CDatasetMakerDlg::OnBnClickedBtnAddClass)
	ON_BN_CLICKED(IDC_BTN_DEL_CLASS, &CDatasetMakerDlg::OnBnClickedBtnDelClass)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_LABELS, &CDatasetMakerDlg::OnLvnItemchangedListLabels)
	ON_BN_CLICKED(IDC_BTN_CLASS_COLOR, &CDatasetMakerDlg::OnBnClickedBtnClassColor)
	ON_BN_CLICKED(IDC_CHK_SELECT_MULTI_FRAMES, &CDatasetMakerDlg::OnBnClickedChkSelectMultiFrames)
	ON_BN_CLICKED(IDC_RB_BOX_RATIO_FREE, &CDatasetMakerDlg::OnBnClickedRbBoxRatioFree)
	ON_BN_CLICKED(IDC_RB_BOX_RATIO_XY, &CDatasetMakerDlg::OnBnClickedRbBoxRatioXy)
	ON_BN_CLICKED(IDC_CHK_ADD_BOX_LABEL, &CDatasetMakerDlg::OnBnClickedChkAddBoxLabel)
	ON_BN_CLICKED(IDC_CHK_ADD_POLY_LABEL, &CDatasetMakerDlg::OnBnClickedChkAddPolyLabel)
	ON_BN_CLICKED(IDC_CHK_ADD_SKEL_LABEL, &CDatasetMakerDlg::OnBnClickedChkAddSkelLabel)
	ON_BN_CLICKED(IDC_BTN_DELETE_LABEL, &CDatasetMakerDlg::OnBnClickedBtnDeleteLabel)
	ON_BN_CLICKED(IDC_BTN_EDIT_LABEL, &CDatasetMakerDlg::OnBnClickedBtnEditLabel)
	ON_BN_CLICKED(IDC_BTN_TRACK_LABEL, &CDatasetMakerDlg::OnBnClickedBtnTrackLabel)
	ON_BN_CLICKED(IDC_BTN_CLEAR_LABELS, &CDatasetMakerDlg::OnBnClickedBtnClearLabels)
	ON_COMMAND(ID_MENU_ADDVERTEX, &CDatasetMakerDlg::OnMenuAddvertex)
	ON_COMMAND(ID_MENU_DELETEVERTEX, &CDatasetMakerDlg::OnMenuDeletevertex)
	ON_BN_CLICKED(IDC_BTN_IMPORT_JSON, &CDatasetMakerDlg::OnBnClickedBtnImportJson)
	ON_BN_CLICKED(IDC_BTN_EXPORT_JSON, &CDatasetMakerDlg::OnBnClickedBtnExportJson)
	ON_BN_CLICKED(IDC_BTN_PARSING_MPII, &CDatasetMakerDlg::OnBnClickedBtnParsingMpii)
	ON_BN_CLICKED(IDC_BTN_PARSING_COCO, &CDatasetMakerDlg::OnBnClickedBtnParsingCoco)
	ON_BN_CLICKED(IDC_BTN_TEST, &CDatasetMakerDlg::OnBnClickedBtnTest)
	ON_BN_CLICKED(IDC_BTN_IMPORT_SKELETON, &CDatasetMakerDlg::OnBnClickedBtnImportSkeleton)
	ON_BN_CLICKED(IDC_BTN_EXPORT_SKELETON, &CDatasetMakerDlg::OnBnClickedBtnExportSkeleton)
	ON_BN_CLICKED(IDC_BTN_IMPORT_BBOX, &CDatasetMakerDlg::OnBnClickedBtnImportBbox)
	ON_BN_CLICKED(IDC_BTN_LOAD_POSE_MODEL, &CDatasetMakerDlg::OnBnClickedBtnLoadPoseModel)
END_MESSAGE_MAP()


// CDatasetMakerDlg 메시지 처리기

BOOL CDatasetMakerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	FrSetTraceFileName("DatasetMaker.log");
	
	// Display control 초기화
	m_dispMainImage.SetParent(this);

	// Radio button control 초기화
	m_rbSourceVideo.SetCheck(TRUE);
	m_rbBoxRatioFree.SetCheck(TRUE);

	// View length combobox control 초기화
	m_cbViewLength.SetCurSel(0);

	// Dataset frame list control 초기화
	m_lstSourceImages.InsertColumn(0, _T("No."), LVCFMT_CENTER, 60, -1);
	m_lstSourceImages.InsertColumn(1, _T("Type"), LVCFMT_CENTER, 50, -1);
	m_lstSourceImages.InsertColumn(2, _T("Idx"), LVCFMT_CENTER, 40, -1);
	m_lstSourceImages.InsertColumn(3, _T("Path"), LVCFMT_CENTER, 250, -1);
	m_lstSourceImages.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES | LVS_EX_CHECKBOXES);

	// Class text 관련 control 초기화
	m_editSceneTitle.SetWindowText(_T("default"));
	m_lstClasses.AddString(_T("default"));
	m_vecClassNames.push_back(_T("default"));

	// Label list control 초기화
	m_lstLabels.InsertColumn(0, _T("Idx"), LVCFMT_LEFT, 30, -1);
	m_lstLabels.InsertColumn(1, _T("Style"), LVCFMT_CENTER, 100, -1);
	m_lstLabels.InsertColumn(2, _T("Class"), LVCFMT_CENTER, 100, -1);
	m_lstLabels.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	// Class 관련 control 갱신
	UpdateClassInfo();

	// Draw tool 및 Load button control 갱신
	UpdateToolBtnImg();
	UpdateLoadButtons();

	// Progress bar dialog 생성 및 초기화
	m_dlgProgress = new CProgressDlg(this);
	m_dlgProgress->Create(IDD_DIALOGBAR_PROGRESS);
	m_dlgProgress->CenterWindow(this);
	m_dlgProgress->ShowWindow(SW_HIDE);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CDatasetMakerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CDatasetMakerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CDatasetMakerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CDatasetMakerDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//CDialogEx::OnOK();
}


void CDatasetMakerDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialogEx::OnCancel();
}


void CDatasetMakerDlg::OnClose()
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	DestroyWindow();

	// Video capture 객체 해제
	if (m_vcObj.isOpened())
		m_vcObj.release();

	CDialogEx::OnClose();
}


void CDatasetMakerDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if (pScrollBar)
	{
		if (pScrollBar == (CScrollBar*)&m_sliFrames)
		{
			// 슬라이드 이동시 해당 프레임 로드하여 화면에 출력
			m_nFrameIdx = m_sliFrames.GetPos();

			CString strIdx;
			strIdx.Format(_T("%d"), m_nFrameIdx);
			m_editSourceIdx.SetWindowText(strIdx);

			UpdateFrameLabel();
			DisplaySourceFrame();
		}
	}

	CDialogEx::OnHScroll(nSBCode, nPos, pScrollBar);
}


BOOL CDatasetMakerDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_ESCAPE)
	{
		return TRUE;
	}
	else if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_F5)
	{
		// 박스 라벨 추가
		OnBnClickedChkAddBoxLabel();

		return TRUE;
	}
	else if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_F6)
	{
		// 현재 선택 라벨 삭제
		OnBnClickedBtnDeleteLabel();

		return TRUE;
	}
	else if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_F7)
	{
		// 현재 선택 라벨 편집
		OnBnClickedBtnEditLabel();

		return TRUE;
	}
	else if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_F8)
	{
		// 이전 프레임의 Box label을 현재 프레임에서 Tracking하여 추가
		OnBnClickedBtnTrackLabel();
		return TRUE;
	}
	else if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_PRIOR)
	{
		// 이전 프레임 탐색
		SelectFrame(m_nSourceIdx - 1);

		return TRUE;
	}
	else if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_NEXT)
	{
		// 다음 프레임 탐색
		SelectFrame(m_nSourceIdx + 1);

		return TRUE;
	}
	else if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN)
	{
		if (pMsg->hwnd == GetDlgItem(IDC_EDIT_PAGE_IDX)->m_hWnd)
		{
			// 입력된 Page index의 Dataset frames 로드
			CString strIdx;

			m_editPageIdx.GetWindowText(strIdx);

			m_nPageNum = _ttoi(strIdx) - 1;

			if (m_nPageNum < 0)
				m_nPageNum = 0;
			else if (m_nPageNum > m_vecDataSetItems.size() / m_nCashingSize)
				m_nPageNum = m_vecDataSetItems.size() / m_nCashingSize;

			UpdateDatasetFrameList();
			m_editPageIdx.SetSel(0, -1);
			m_editPageIdx.SetFocus();
		}
		else if (pMsg->hwnd == GetDlgItem(IDC_EDIT_SCENE_TITLE)->m_hWnd)
		{
			// 입력된 Scene title을 해당 프레임에 지정
			OnBnClickedBtnSetSceneTitle();
		}
		else if (pMsg->hwnd == GetDlgItem(IDC_EDIT_SOURCE_IDX)->m_hWnd)
		{
			// 지정된 index의 frame 로드
			CString strIdx;

			m_editSourceIdx.GetWindowText(strIdx);

			m_nFrameIdx = _ttoi(strIdx);

			if (m_nFrameIdx < 0)
				m_nFrameIdx = 0;
			else if (m_nFrameIdx > m_nFrameCnt - 1)
				m_nFrameIdx = m_nFrameCnt - 1;

			strIdx.Format(_T("%d"), m_nFrameIdx);
			m_editSourceIdx.SetWindowText(strIdx);
			m_editSourceIdx.SetSel(0, -1);
			m_editSourceIdx.SetFocus();

			m_sliFrames.SetPos(m_nFrameIdx);

			UpdateFrameLabel();
			DisplaySourceFrame();
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

/// <summary>
/// 로드할 소스를 Video로 설정
/// </summary>
void CDatasetMakerDlg::OnBnClickedRbSourceVideo()
{
	// 로드할 소스 모드를 Video로 지정 (.mp4)
	m_nSourceMode = MODE_SRC_VIDEO;
}

/// <summary>
/// 로드할 소스를 Folder로 설정
/// </summary>
void CDatasetMakerDlg::OnBnClickedRbSourceFolder()
{
	// 로드할 소스 모드를 Folder로 지정 (.jpg|.bmp|.png 등)
	m_nSourceMode = MODE_SRC_FOLDER;
}


/// <summary>
/// 소스 프레임 로드
/// </summary>
void CDatasetMakerDlg::OnBnClickedBtnPathSource()
{
	// 각 소스 모드별 프레임 정보 로드
	if (m_nSourceMode == MODE_SRC_VIDEO)
	{
		static TCHAR BASED_CODE szFilter[] = _T("영상 파일(*.mp4)|*.MP4;*.mp4 |모든파일(*.*)|*.*||");

		CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, szFilter);

		if (dlg.DoModal() == IDOK)
		{
			m_strPathSource = dlg.GetPathName();

			m_editPathSource.SetWindowText(m_strPathSource);

			AddSourceFrame();
			DisplaySourceFrame();
		}
	}
	else if(m_nSourceMode == MODE_SRC_FOLDER)
	{
		CFolderPickerDialog dlg;

		if (dlg.DoModal() == IDOK)
		{
			m_strPathSource = dlg.GetPathName();

			m_editPathSource.SetWindowText(m_strPathSource);

			AddSourceFrame();
			DisplaySourceFrame();
		}
	}

	UpdateFrameLabel();
}

/// <summary>
/// 로드된 영상의 처음 프레임으로 이동
/// </summary>
void CDatasetMakerDlg::OnBnClickedBtnHeadFrame()
{
	m_nFrameIdx = 0;

	CString strIdx;
	strIdx.Format(_T("%d"), m_nFrameIdx);
	m_editSourceIdx.SetWindowText(strIdx);

	m_sliFrames.SetPos(m_nFrameIdx);

	UpdateFrameLabel();
	DisplaySourceFrame();
}

/// <summary>
/// 로드된 영상의 이전 프레임으로 이동
/// </summary>
void CDatasetMakerDlg::OnBnClickedBtnPrevFrame()
{
	if (m_nFrameIdx > 0)
	{
		m_nFrameIdx--;

		CString strIdx;
		strIdx.Format(_T("%d"), m_nFrameIdx);
		m_editSourceIdx.SetWindowText(strIdx);

		m_sliFrames.SetPos(m_nFrameIdx);

		UpdateFrameLabel();
		DisplaySourceFrame();
	}
}

/// <summary>
/// 로드된 영상의 다음 프레임으로 이동
/// </summary>
void CDatasetMakerDlg::OnBnClickedBtnNextFrame()
{
	if (m_nFrameIdx < m_nFrameCnt - 1)
	{
		m_nFrameIdx++;

		CString strIdx;
		strIdx.Format(_T("%d"), m_nFrameIdx);
		m_editSourceIdx.SetWindowText(strIdx);

		m_sliFrames.SetPos(m_nFrameIdx);

		UpdateFrameLabel();
		DisplaySourceFrame();
	}
}

/// <summary>
/// 로드된 영상의 마지막 프레임으로 이동
/// </summary>
void CDatasetMakerDlg::OnBnClickedBtnTailFrame()
{
	m_nFrameIdx = m_nFrameCnt - 1;

	CString strIdx;
	strIdx.Format(_T("%d"), m_nFrameIdx);
	m_editSourceIdx.SetWindowText(strIdx);

	m_sliFrames.SetPos(m_nFrameIdx);

	UpdateFrameLabel();
	DisplaySourceFrame();
}

/// <summary>
/// 로드된 영상의 전체 프레임을 선택
/// </summary>
void CDatasetMakerDlg::OnBnClickedChkSelectAllIdx()
{
	m_bSelectAllIdx = m_chkSelectAllIdx.GetCheck();

	UpdateLoadButtons();
}

/// <summary>
/// 로드된 영상을 Dataset frame list에 추가
/// </summary>
void CDatasetMakerDlg::OnBnClickedBtnAddSource()
{
	int nIdx = m_vecDataSetItems.size();

	if (m_bSelectAllIdx)
	{
		// 전체 프레임 추가
		for (int i = 0; i < m_vecFrameList.size(); i++)
		{
			StDataSetItem item;

			item.bSelected = true;
			item.nId = nIdx;
			item.nSrcType = m_vecFrameList[i].nSrcType == MODE_SRC_VIDEO ? SRC_TYPE_VIDEO : SRC_TYPE_STILL;
			item.strPath = m_vecFrameList[i].strPath;
			item.nWidth = m_vecFrameList[i].nWidth;
			item.nHeight = m_vecFrameList[i].nHeight;

			item.strSceneTitle = _T("default");

			item.nFrmIdx = m_vecFrameList[i].nFrmIdx;

			m_vecDataSetItems.push_back(item);

			nIdx++;
		}
	}
	else
	{
		// 현재 프레임만 추가
		StDataSetItem item;

		item.bSelected = true;
		item.nId = nIdx;
		item.nSrcType = m_vecFrameList[m_nFrameIdx].nSrcType == MODE_SRC_VIDEO ? SRC_TYPE_VIDEO : SRC_TYPE_STILL;
		item.strPath = m_vecFrameList[m_nFrameIdx].strPath;
		item.nWidth = m_vecFrameList[m_nFrameIdx].nWidth;
		item.nHeight = m_vecFrameList[m_nFrameIdx].nHeight;

		item.strSceneTitle = _T("default");
		
		item.nFrmIdx = m_vecFrameList[m_nFrameIdx].nFrmIdx;
		//TODO: inference 결과를 넣어주기...
		m_vecDataSetItems.push_back(item);
	}

	UpdateDatasetFrameList();
}

/// <summary>
/// Dataset frame list control 선택시 해당 frame의 정보 로드
/// </summary>
/// <param name="pNMHDR"></param>
/// <param name="pResult"></param>
void CDatasetMakerDlg::OnLvnItemchangedListSourceImages(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	// 초기화가 안되어 있거나 현재 index와 동일한 경우 미수행
	//if (m_nSourceIdx == pNMLV->iItem || m_bInitializing)
	//	return;

	m_nSourceIdx = pNMLV->iItem + m_nPageNum * m_nCashingSize;

	m_vecDataSetItems[m_nSourceIdx].bSelected = m_lstSourceImages.GetCheck(m_nSourceIdx);

	Mat frame = GetCashingImage(pNMLV->iItem);

	m_dispMainImage.SetImage(frame);

	m_editSceneTitle.SetWindowText(m_vecDataSetItems[m_nSourceIdx].strSceneTitle);

	m_vecCurCoord.clear();

	if (m_vecDataSetItems[m_nSourceIdx].vecLabels.size() > 0)
	{
		for (int i = 0; i < m_vecDataSetItems[m_nSourceIdx].vecLabels.size(); i++)
		{
			StLabelUnit& unit = m_vecDataSetItems[m_nSourceIdx].vecLabels[i];

			if (unit.nStyle == LABEL_UNIT_STYLE_BOX)
			{
				coord_t c;
				c.id = unit.nClass;

				Rect2f rt;
				rt.x = unit.vPts[BOX_LABEL_PT_POS_LT].ptCoord.x;
				rt.y = unit.vPts[BOX_LABEL_PT_POS_LT].ptCoord.y;
				rt.width = unit.vPts[BOX_LABEL_PT_POS_RB].ptCoord.x - unit.vPts[BOX_LABEL_PT_POS_LT].ptCoord.x;
				rt.height = unit.vPts[BOX_LABEL_PT_POS_RB].ptCoord.y - unit.vPts[BOX_LABEL_PT_POS_LT].ptCoord.y;

				c.abs_rect = rt;

				m_vecCurCoord.push_back(c);
			}
		}

		// Tracking 정보 갱신
		m_optFlow.UpdateTrackingFlow(frame, m_vecCurCoord);

		m_bTrackerOn = true;
	}

	// Scene/Label 정보 로드
	m_dispMainImage.SetSceneTitle(m_vecDataSetItems[m_nSourceIdx].strSceneTitle);
	m_dispMainImage.SetLabels(&(m_vecDataSetItems[m_nSourceIdx].vecLabels));

	UpdateLabelList();

	*pResult = 0;
}

/// <summary>
/// Dataset frame list control focus시 해당 frame의 정보 로드
/// </summary>
/// <param name="pNMHDR"></param>
/// <param name="pResult"></param>
void CDatasetMakerDlg::OnNMSetfocusListSourceImages(NMHDR* pNMHDR, LRESULT* pResult)
{
	if (m_vecDataSetItems.size() == 0)
		return;

	Mat frame;

	frame = GetCashingImage(m_nSourceIdx - m_nPageNum * m_nCashingSize);

	m_dispMainImage.SetImage(frame);

	m_editSceneTitle.SetWindowText(m_vecDataSetItems[m_nSourceIdx].strSceneTitle);

	m_vecCurCoord.clear();

	if (m_vecDataSetItems[m_nSourceIdx].vecLabels.size() > 0)
	{
		for (int i = 0; i < m_vecDataSetItems[m_nSourceIdx].vecLabels.size(); i++)
		{
			StLabelUnit& unit = m_vecDataSetItems[m_nSourceIdx].vecLabels[i];

			if (unit.nStyle == LABEL_UNIT_STYLE_BOX)
			{
				coord_t c;
				c.id = unit.nClass;

				Rect2f rt;
				rt.x = unit.vPts[BOX_LABEL_PT_POS_LT].ptCoord.x;
				rt.y = unit.vPts[BOX_LABEL_PT_POS_LT].ptCoord.y;
				rt.width = unit.vPts[BOX_LABEL_PT_POS_RB].ptCoord.x - unit.vPts[BOX_LABEL_PT_POS_LT].ptCoord.x;
				rt.height = unit.vPts[BOX_LABEL_PT_POS_RB].ptCoord.y - unit.vPts[BOX_LABEL_PT_POS_LT].ptCoord.y;

				c.abs_rect = rt;

				m_vecCurCoord.push_back(c);
			}
		}

		// Tracking 정보 갱신
		m_optFlow.UpdateTrackingFlow(frame, m_vecCurCoord);

		m_bTrackerOn = true;
	}

	// Scene/Label 정보 로드
	m_dispMainImage.SetSceneTitle(m_vecDataSetItems[m_nSourceIdx].strSceneTitle);
	m_dispMainImage.SetLabels(&(m_vecDataSetItems[m_nSourceIdx].vecLabels));

	UpdateLabelList();

	*pResult = 0;
}

/// <summary>
/// 영상 파일을 로드하여 Dataset frame list에 추가
/// </summary>
void CDatasetMakerDlg::OnBnClickedBtnAddSourceImage()
{
	static TCHAR BASED_CODE szFilter[] = _T("Image file(*.bmp|*.jpg|*.png)|*.BMP;*.bmp;*.JPG;*.jpg;*.PNG;*.png |모든파일(*.*)|*.*||");

	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, szFilter);

	if (dlg.DoModal() == IDOK)
	{
		CString path;
		path = dlg.GetPathName();


		StDataSetItem item;

		item.bSelected = true;
		item.nId = m_vecDataSetItems.size();
		item.nSrcType = SRC_TYPE_STILL;
		item.nFrmIdx = -1;
		item.strPath = path;

		CT2CA pszConvertedAnsiString(path);
		Mat temp = imread(std::string(pszConvertedAnsiString), 1);

		item.nWidth = temp.cols;
		item.nHeight = temp.rows;

		item.strSceneTitle = _T("default");

		m_vecDataSetItems.push_back(item);

		UpdateDatasetFrameList();
	}
}

/// <summary>
/// Dataset frame list의 선택 항목을 삭제
/// </summary>
void CDatasetMakerDlg::OnBnClickedBtnDelSourceImage()
{
	int nSelected = m_lstSourceImages.GetNextItem(-1, LVNI_SELECTED);

	if (nSelected == -1)
		return;

	int nCount = m_lstSourceImages.GetItemCount();

	for (int i = nCount; i >= 0; i--)
	{
		if (m_lstSourceImages.GetItemState(i, LVIS_SELECTED) != 0)
			m_vecDataSetItems.erase(m_vecDataSetItems.begin() + i + m_nPageNum * m_nCashingSize);
	}

	UpdateDatasetFrameList();

	if (nSelected < m_lstSourceImages.GetItemCount())
	{
		m_lstSourceImages.SetItemState(-1, 0, LVIS_SELECTED | LVIS_FOCUSED);
		m_lstSourceImages.SetItemState(nSelected, LVIS_FOCUSED | LVIS_SELECTED, LVIS_FOCUSED | LVIS_SELECTED);
		m_lstSourceImages.EnsureVisible(nSelected, FALSE);
		m_lstSourceImages.SetFocus();
	}
}

/// <summary>
/// 전체 Dataset frame list를 삭제
/// </summary>
void CDatasetMakerDlg::OnBnClickedBtnClearSources()
{
	m_vecDataSetItems.clear();

	UpdateDatasetFrameList();
}

/// <summary>
/// Export할 대상으로 Dataset frame list를 전체 선택
/// </summary>
void CDatasetMakerDlg::OnBnClickedBtnSelectSourceAll()
{
	for (int i = 0; i < m_lstSourceImages.GetItemCount(); i++)
	{
		m_lstSourceImages.SetCheck(i, TRUE);
		m_vecDataSetItems[i + m_nPageNum * m_nCashingSize].bSelected = true;
	}
}

/// <summary>
/// Export할 대상으로 Dataset frame list를 전체 해제
/// </summary>
void CDatasetMakerDlg::OnBnClickedBtnSelectSourceNone()
{
	for (int i = 0; i < m_lstSourceImages.GetItemCount(); i++)
	{
		m_lstSourceImages.SetCheck(i, FALSE);
		m_vecDataSetItems[i + m_nPageNum * m_nCashingSize].bSelected = false;
	}
}

/// <summary>
/// 이전 페이지로 이동
/// </summary>
void CDatasetMakerDlg::OnBnClickedBtnPrevPage()
{
	if (m_nPageNum == 0)
		return;

	m_nPageNum--;

	UpdateDatasetFrameList();
}

/// <summary>
/// 다음 페이지로 이동
/// </summary>
void CDatasetMakerDlg::OnBnClickedBtnNextPage()
{
	if (m_nPageNum == m_vecDataSetItems.size() / m_nCashingSize)
		return;

	m_nPageNum++;

	UpdateDatasetFrameList();
}

/// <summary>
/// Dataset frame list에 표시할 항목의 개수 지정
/// </summary>
void CDatasetMakerDlg::OnCbnSelchangeCbViewLength()
{
	switch (m_cbViewLength.GetCurSel())
	{
	case 0:
		m_nCashingSize = 30;
		break;
	case 1:
		m_nCashingSize = 40;
		break;
	default:
		m_nCashingSize = 50;
	}
	UpdateDatasetFrameList();
}

/// <summary>
/// 현재 Scene에 대한 Title class name 설정
/// </summary>
void CDatasetMakerDlg::OnBnClickedBtnSetSceneTitle()
{
	if (m_vecDataSetItems.empty())
		return;

	CString str;
	m_editSceneTitle.GetWindowText(str);
	m_vecDataSetItems[m_nSourceIdx].strSceneTitle = str;
	m_editSceneTitle.SetSel(0, -1);
	m_editSceneTitle.SetFocus();

	m_dispMainImage.SetSceneTitle(str);
	m_dispMainImage.ReDrawImage();
}

/// <summary>
/// 선택된 Class list 항목으로 Drawing class를 갱신
/// </summary>
void CDatasetMakerDlg::OnLbnSelchangeListClasses()
{
	int nSelected = m_lstClasses.GetCurSel();
	if (nSelected == -1)
		return;

	m_nSelClass = nSelected;

	CString str;
	str = m_vecClassNames[nSelected];

	m_editClassName.SetWindowText(str);

	UpdateClassInfo();
}


/// <summary>
/// 선택된 Class list 항목을 입력한 Class name 으로 변경
/// </summary>
void CDatasetMakerDlg::OnBnClickedBtnSetClassName()
{
	int nSelected = m_lstClasses.GetCurSel();
	if (nSelected == -1)
		return;


	CString str;
	m_editClassName.GetWindowText(str);

	m_vecClassNames[nSelected] = str;

	UpdateClassCount();
}

/// <summary>
/// Class list에 새로운 Class name 추가
/// </summary>
void CDatasetMakerDlg::OnBnClickedBtnAddClass()
{
	if (m_nClassCount >= MAX_CLASS_COUNT)
		return;

	CString str;
	str.Format(_T("class%d"), m_nClassCount);

	m_vecClassNames.push_back(str);

	UpdateClassCount();
}

/// <summary>
/// Class list 에서 선택된 항목 삭제
/// </summary>
void CDatasetMakerDlg::OnBnClickedBtnDelClass()
{
	int nSelected = m_lstClasses.GetCurSel();

	if (nSelected == -1)
		return;

	if (m_nClassCount <= 1)
	{
		AfxMessageBox(_T("최소 1개의 클래스가 필요합니다!"));
		return;
	}

	m_vecClassNames.erase(m_vecClassNames.begin() + nSelected);

	UpdateClassCount();
}

/// <summary>
/// Label list에서 항목 선택시 해당 Label 강조
/// </summary>
/// <param name="pNMHDR"></param>
/// <param name="pResult"></param>
void CDatasetMakerDlg::OnLvnItemchangedListLabels(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	SelectLabel(pNMLV->iItem);
	m_dispMainImage.SelectLabel(pNMLV->iItem);

	*pResult = 0;
}

// Class color 버튼 클릭시 클래스 선택창 출력
void CDatasetMakerDlg::OnBnClickedBtnClassColor()
{
	CSelectClassDlg dlg;

	dlg.SetClassColors(g_ColorModel);
	dlg.SetClassNames(&m_vecClassNames);
	dlg.m_pSelClass = &m_nSelClass;

	if (dlg.DoModal() == IDOK)
	{
		CString str;
		//m_lstClasses.GetText(nSelected, str);
		str = m_vecClassNames[m_nSelClass];

		m_editClassName.SetWindowText(str);
		m_lstClasses.SetCurSel(m_nSelClass);

		UpdateClassInfo();
	}
}

/// <summary>
/// Multi frame drawing을 위한 설정창 pop-up
/// </summary>
void CDatasetMakerDlg::OnBnClickedChkSelectMultiFrames()
{
	if (!m_chkSelectMultiFrames.GetCheck())
	{
		m_stMultiDrawing.bUse = false;
		UpdateToolBtnImg();
		return;
	}

	if (m_vecDataSetItems.size() < 2)
	{
		AfxMessageBox(_T("Multi frames 선택을 위해선 2개 이상의 frame이 필요합니다."));
		return;
	}

	if (m_vecDataSetItems[m_nSourceIdx].nSrcType != SRC_TYPE_VIDEO)
	{
		AfxMessageBox(_T("Multi frames 선택을 위해선 현재 프레임이 VIDEO 형식이어야 합니다."));
		return;
	}


	CMultiDrawingDlg dlg;

	dlg.m_pDir = &m_stMultiDrawing.nDir;
	dlg.m_pCnt = &m_stMultiDrawing.nCnt;

	if (dlg.DoModal() == IDOK)
	{
		m_stMultiDrawing.bUse = true;
	}
	else
	{
		m_chkSelectMultiFrames.SetCheck(false);
		m_stMultiDrawing.bUse = false;
	}

	if (m_chkAddPolyLabel.GetCheck())
		m_chkAddPolyLabel.SetCheck(false);

	if (m_chkAddSkelLabel.GetCheck())
		m_chkAddSkelLabel.SetCheck(false);

	UpdateToolBtnImg();
}

/// <summary>
/// Box label drawing시 비율 제한을 해제
/// </summary>
void CDatasetMakerDlg::OnBnClickedRbBoxRatioFree()
{
	m_nBoxRatioX = 0;
	m_nBoxRatioY = 0;

	m_dispMainImage.SetBoxRatio(0.);
}

/// <summary>
/// Box label drawing시 비율 제한을 설정
/// </summary>
void CDatasetMakerDlg::OnBnClickedRbBoxRatioXy()
{
	CEditRatioDlg dlg;

	dlg.m_pRatioX = &m_nBoxRatioX;
	dlg.m_pRatioY = &m_nBoxRatioY;

	if (dlg.DoModal() == IDOK)
	{
		CString strRatio;
		strRatio.Format(_T("%d:%d"), m_nBoxRatioX, m_nBoxRatioY);
		m_rbBoxRatioXY.SetWindowText(strRatio);
		m_dispMainImage.SetBoxRatio((double)m_nBoxRatioX / (double)m_nBoxRatioY);
	}
	else
	{
		m_nBoxRatioX = 0;
		m_nBoxRatioY = 0;
		m_dispMainImage.SetBoxRatio(0.);

		m_rbBoxRatioFree.SetCheck(TRUE);
		m_rbBoxRatioXY.SetCheck(FALSE);
	}
}

/// <summary>
/// Box drawing mode로 전환 (Detection에 사용)
/// </summary>
void CDatasetMakerDlg::OnBnClickedChkAddBoxLabel()
{
	if (!m_dispMainImage.IsLoaded() || m_vecDataSetItems.empty())
	{
		AfxMessageBox(_T("Image Load 필요!"));
		return;
	}
	m_dispMainImage.SetCursorMode(DISP_CURSOR_MODE_INSERT_BOX_LABEL);

	if (m_chkAddPolyLabel.GetCheck())
		m_chkAddPolyLabel.SetCheck(false);

	if (m_chkAddSkelLabel.GetCheck())
		m_chkAddSkelLabel.SetCheck(false);

	UpdateToolBtnImg();
}

/// <summary>
/// Polygon area drawing mode로 전환 (Segmentation에 사용)
/// </summary>
void CDatasetMakerDlg::OnBnClickedChkAddPolyLabel()
{
	if (!m_dispMainImage.IsLoaded() || m_vecDataSetItems.empty())
	{
		AfxMessageBox(_T("Image Load 필요!"));
		return;
	}
	m_dispMainImage.SetCursorMode(DISP_CURSOR_MODE_INSERT_POLY_LABEL);

	if (m_chkSelectMultiFrames.GetCheck())
		m_chkSelectMultiFrames.SetCheck(false);

	if (m_chkAddBoxLabel.GetCheck())
		m_chkAddBoxLabel.SetCheck(false);

	if (m_chkAddSkelLabel.GetCheck())
		m_chkAddSkelLabel.SetCheck(false);

	UpdateToolBtnImg();
}

/// <summary>
/// Skeleton drawing mode로 전환 (Pose estimation에 사용)
/// </summary>
void CDatasetMakerDlg::OnBnClickedChkAddSkelLabel()
{
	if (!m_dispMainImage.IsLoaded() || m_vecDataSetItems.empty())
	{
		AfxMessageBox(_T("Image Load 필요!"));
		return;
	}
	m_dispMainImage.SetCursorMode(DISP_CURSOR_MODE_INSERT_SKEL_LABEL);

	if (m_chkSelectMultiFrames.GetCheck())
		m_chkSelectMultiFrames.SetCheck(false);

	if (m_chkAddBoxLabel.GetCheck())
		m_chkAddBoxLabel.SetCheck(false);

	if (m_chkAddPolyLabel.GetCheck())
		m_chkAddPolyLabel.SetCheck(false);

	UpdateToolBtnImg();
}

/// <summary>
/// 선택된 Label을 삭제
/// </summary>
void CDatasetMakerDlg::OnBnClickedBtnDeleteLabel()
{
	if (m_vecDataSetItems.empty())
		return;

	m_vecDataSetItems[m_nSourceIdx].vecLabels.erase(m_vecDataSetItems[m_nSourceIdx].vecLabels.begin() + m_nSelLabel);

	UpdateLabelList();
}

/// <summary>
/// 선택된 Label 정보 수정창 pop-up
/// </summary>
void CDatasetMakerDlg::OnBnClickedBtnEditLabel()
{
	CEditLabelDlg* pDlg = new CEditLabelDlg(this);

	if (m_vecDataSetItems.empty())
		return;

	vector<StLabelUnit>& labels = m_vecDataSetItems[m_nSourceIdx].vecLabels;
	int nSelected = m_lstLabels.GetNextItem(-1, LVNI_SELECTED);

	if (nSelected == -1)
	{
		AfxMessageBox(_T("선택된 Label 없음!"));
		return;
	}

	pDlg->m_pLabel = &(labels[nSelected]);
	pDlg->DoModal();
}

/// <summary>
/// 이전에 선택한 frame에서 box label을 tracking하여 현재 frame의 label list에 추가
/// </summary>
void CDatasetMakerDlg::OnBnClickedBtnTrackLabel()
{
	if (m_bTrackerOn)
	{
		Mat frame = GetDatasetItemImage(m_nSourceIdx);

		m_vecCurCoord = m_optFlow.TrackingFlow(frame, false);

		for (int i = 0; i < m_vecCurCoord.size(); i++)
		{
			StLabelUnit label;
			label.nStyle = LABEL_UNIT_STYLE_BOX;
			label.nClass = m_vecCurCoord[i].id;
			label.vPts.push_back({ true, Point(m_vecCurCoord[i].abs_rect.x, m_vecCurCoord[i].abs_rect.y) });
			label.vPts.push_back({ true, Point(m_vecCurCoord[i].abs_rect.x + m_vecCurCoord[i].abs_rect.width, m_vecCurCoord[i].abs_rect.y + m_vecCurCoord[i].abs_rect.height) });

			m_vecDataSetItems[m_nSourceIdx].vecLabels.push_back(label);
		}
	}

	m_dispMainImage.SetLabels(&(m_vecDataSetItems[m_nSourceIdx].vecLabels));

	UpdateLabelList();
}

/// <summary>
/// 전체 Label list를 삭제
/// </summary>
void CDatasetMakerDlg::OnBnClickedBtnClearLabels()
{
	m_vecDataSetItems[m_nSourceIdx].vecLabels.clear();

	m_dispMainImage.SetLabels(&(m_vecDataSetItems[m_nSourceIdx].vecLabels));

	UpdateLabelList();
}

/// <summary>
/// Polygon drawing시 vertex 추가
/// </summary>
void CDatasetMakerDlg::OnMenuAddvertex()
{
	m_dispMainImage.Addvertex();
}

/// <summary>
/// Polygon drawing시 선택된 vertex 삭제
/// </summary>
void CDatasetMakerDlg::OnMenuDeletevertex()
{
	m_dispMainImage.Deletevertex();
}

/// <summary>
/// 지정한 4D Dataset 형식의 .json 파일을 Dataset frame list로 import
/// </summary>
void CDatasetMakerDlg::OnBnClickedBtnImportJson()
{
	CFolderPickerDialog dlg;

	if (dlg.DoModal() == IDOK)
	{
		m_strPathResult = dlg.GetPathName();

		Invalidate(FALSE);

		if (LoadExistingLabelsJson(m_strPathResult))
			AfxMessageBox(_T("Load 성공"));
	}
}

/// <summary>
/// Check된 dataset frame list의 항목들을 4D Dataset 형식의 .json으로 export
/// </summary>
void CDatasetMakerDlg::OnBnClickedBtnExportJson()
{
	CFolderPickerDialog dlg;

	if (dlg.DoModal() == IDOK)
	{
		m_strPathResult = dlg.GetPathName();

		bool bFlag = false;
		for (int i = 0; i < m_lstSourceImages.GetItemCount(); i++)
		{
			if (m_lstSourceImages.GetCheck(i))
				bFlag = true;
		}
		if (!bFlag)
		{
			AfxMessageBox(_T("선택된 영상이 없습니다."));
			return;
		}

		if (SaveLabelsJson(m_strPathResult))
		{
			if (m_chkOptionUseAnchors.GetCheck())
			{
				vector<vector<Size2f>> anchors = GetAnchorsUsingKmeans();
				SaveAnchorsInfo(anchors, m_strPathResult + _T("\\anchors.txt"));
			}
			AfxMessageBox(_T("Save 성공!"));
		}
		else
			AfxMessageBox(_T("Save 실패!"));
	}
}

/// <summary>
/// [현재 Skeleton만 지원] 지정한 MPII Dataset 형식의 .json 파일을 Dataset frame list로 parsing
/// </summary>
void CDatasetMakerDlg::OnBnClickedBtnParsingMpii()
{
	AfxMessageBox(_T("<Warning> 해당 json 파일과 같은 경로에 [images] 폴더가 포함되어야 합니다."));

	static TCHAR BASED_CODE szFilter[] = _T("Image file(*.json)|*.JSON;*.json |모든파일(*.*)|*.*||");

	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, szFilter);

	if (dlg.DoModal() == IDOK)
	{
		CString pathJson, pathFolder;
		pathJson = dlg.GetPathName();
		pathFolder = dlg.GetFolderPath();

		if (LoadMPII(pathJson, pathFolder + _T("\\images")))
			AfxMessageBox(_T("MPII dataset을 성공적으로 불러왔습니다."));
		else
			AfxMessageBox(_T("MPII dataset을 불러오는 과정에서 오류가 발생하였습니다."));
	}
}


/// <summary>
/// [현재 Skeleton만 지원] 지정한 COCO Dataset 형식의 .json 파일을 Dataset frame list로 parsing
/// </summary>
void CDatasetMakerDlg::OnBnClickedBtnParsingCoco()
{
	AfxMessageBox(_T("<Warning> 해당 json 파일과 같은 경로에 [images] 폴더가 포함되어야 합니다."));

	static TCHAR BASED_CODE szFilter[] = _T("Image file(*.json)|*.JSON;*.json |모든파일(*.*)|*.*||");

	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, szFilter);

	if (dlg.DoModal() == IDOK)
	{
		CString pathJson, pathFolder;
		pathJson = dlg.GetPathName();
		pathFolder = dlg.GetFolderPath();

		if (LoadCOCO(pathJson, pathFolder + _T("\\images")))
			AfxMessageBox(_T("COCO dataset을 성공적으로 불러왔습니다."));
		else
			AfxMessageBox(_T("COCO dataset을 불러오는 과정에서 오류가 발생하였습니다."));
	}
}

/// <summary>
/// [임시] 테스트용 버튼
/// </summary>
void CDatasetMakerDlg::OnBnClickedBtnTest()
{
	CFolderPickerDialog dlg;

	if (dlg.DoModal() == IDOK)
	{
		m_strPathResult = dlg.GetPathName();

		bool bFlag = false;
		for (int i = 0; i < m_lstSourceImages.GetItemCount(); i++)
		{
			if (m_lstSourceImages.GetCheck(i))
				bFlag = true;
		}
		if (!bFlag)
		{
			AfxMessageBox(_T("선택된 영상이 없습니다."));
			return;
		}

		if (SaveSkeletonsCOCOJson(m_strPathResult))
		{
			AfxMessageBox(_T("Save 성공!"));
		}
		else
			AfxMessageBox(_T("Save 실패!"));
	}
}

/// <summary>
/// 로드된 영상에서 선택된 frame을 화면에 display
/// </summary>
void CDatasetMakerDlg::DisplaySourceFrame()
{
	Mat frame;

	if (m_vecFrameList[m_nFrameIdx].nSrcType == MODE_SRC_VIDEO)
	{
		LoadSelectFrame(frame, m_vecFrameList[m_nFrameIdx].strPath, m_nFrameIdx);
	}
	else if (m_vecFrameList[m_nFrameIdx].nSrcType == MODE_SRC_FOLDER)
	{
		LoadStillImage(frame, m_vecFrameList[m_nFrameIdx].strPath);
	}

	m_dispMainImage.SetSceneTitle(m_vecFrameList[m_nFrameIdx].strPath);
	m_dispMainImage.SetLabels(NULL);
	m_dispMainImage.SetImage(frame);
}

/// <summary>
/// 로드된 영상에서 선택된 frame을 Dataset frame list에 추가
/// </summary>
void CDatasetMakerDlg::AddSourceFrame()
{
	if(!m_vecFrameList.empty())
		m_vecFrameList.clear();

	m_nFrameIdx = 0;

	StartProgress();

	if (m_nSourceMode == MODE_SRC_VIDEO)
	{
		SetProgress(50, _T("Checking file size..."));

		VideoCapture videoInput;

		CT2CA pszConvertedAnsiString(m_strPathSource);

		videoInput.open(std::string(pszConvertedAnsiString));

		if (!videoInput.isOpened())
			MessageBox(_T("Video Open Error"));

		int nFrameCnt = (int)videoInput.get(CAP_PROP_FRAME_COUNT);
		int nWidth = (int)videoInput.get(CAP_PROP_FRAME_WIDTH);
		int nHeight = (int)videoInput.get(CAP_PROP_FRAME_HEIGHT);

		videoInput.release();

		m_nFrameCnt = nFrameCnt;

		for (int i = 0; i < nFrameCnt; i++)
		{
			StFrameInfo item;

			item.nSrcType = MODE_SRC_VIDEO;
			item.strPath = m_strPathSource;
			item.nWidth = nWidth;
			item.nHeight = nHeight;
			item.nFrmIdx = i;

			m_vecFrameList.push_back(item);
		}
	}
	else if (m_nSourceMode == MODE_SRC_FOLDER)
	{
		vector<CString> vecFiles = GetImageFilesInDirectory(m_strPathSource);

		for (int i = 0; i < vecFiles.size(); i++)
		{
			CString strMsg;
			strMsg.Format(_T("Loading filelist...(idx=%d)"), i);
			SetProgress(100 * i / vecFiles.size(), strMsg);

			StFrameInfo item;

			item.nSrcType = MODE_SRC_FOLDER;
			item.nFrmIdx = -1;
			item.strPath = vecFiles[i];

			CString strCnt;
			strCnt.Format(_T("%d"), (int) vecFiles.size());
			m_editSourceCnt.SetWindowText(strCnt);
			m_nFrameCnt = vecFiles.size();

			CT2CA pszConvertedAnsiString(vecFiles[i]);
			Mat temp = imread(std::string(pszConvertedAnsiString), 1);

			item.nWidth = temp.cols;
			item.nHeight = temp.rows;

			m_vecFrameList.push_back(item);
		}
	}

	EndProgress();

	UpdateLoadButtons();
}

/// <summary>
/// 로드된 영상 관련 Control의 Enable 상태 갱신
/// </summary>
void CDatasetMakerDlg::UpdateLoadButtons()
{
	if (m_nFrameCnt > 0)
	{
		if (m_bSelectAllIdx)
		{
			m_editSourceIdx.EnableWindow(FALSE);
			m_editSourceCnt.EnableWindow(TRUE);
			m_sliFrames.EnableWindow(FALSE);
			GetDlgItem(IDC_BTN_HEAD_FRAME)->EnableWindow(FALSE);
			GetDlgItem(IDC_BTN_PREV_FRAME)->EnableWindow(FALSE);
			GetDlgItem(IDC_BTN_NEXT_FRAME)->EnableWindow(FALSE);
			GetDlgItem(IDC_BTN_TAIL_FRAME)->EnableWindow(FALSE);
		}
		else
		{
			m_editSourceIdx.EnableWindow(TRUE);
			m_editSourceCnt.EnableWindow(TRUE);
			m_sliFrames.EnableWindow(TRUE);
			GetDlgItem(IDC_BTN_HEAD_FRAME)->EnableWindow(TRUE);
			GetDlgItem(IDC_BTN_PREV_FRAME)->EnableWindow(TRUE);
			GetDlgItem(IDC_BTN_NEXT_FRAME)->EnableWindow(TRUE);
			GetDlgItem(IDC_BTN_TAIL_FRAME)->EnableWindow(TRUE);

			CString strIdx;
			strIdx.Format(_T("%d"), m_nFrameIdx);
			m_editSourceIdx.SetWindowText(strIdx);

			m_sliFrames.SetRange(0, m_nFrameCnt - 1);
			m_sliFrames.SetPos(m_nFrameIdx);
			m_sliFrames.SetLineSize(1);
			m_sliFrames.SetPageSize(30);
		}

		CString strCnt;
		strCnt.Format(_T("%d"), m_nFrameCnt - 1);
		m_editSourceCnt.SetWindowText(strCnt);
	}
	else
	{
		m_editSourceIdx.EnableWindow(FALSE);
		m_editSourceCnt.EnableWindow(FALSE);
		m_sliFrames.EnableWindow(FALSE);
		GetDlgItem(IDC_BTN_HEAD_FRAME)->EnableWindow(FALSE);
		GetDlgItem(IDC_BTN_PREV_FRAME)->EnableWindow(FALSE);
		GetDlgItem(IDC_BTN_NEXT_FRAME)->EnableWindow(FALSE);
		GetDlgItem(IDC_BTN_TAIL_FRAME)->EnableWindow(FALSE);
	}
}

/// <summary>
/// 로드된 영상의 Frame 정보 표시창 갱신
/// </summary>
void CDatasetMakerDlg::UpdateFrameLabel()
{
	if (m_vecFrameList.empty())
		return;

	CString strName;
	if (m_vecFrameList[m_nFrameIdx].nSrcType == MODE_SRC_FOLDER)
	{
		strName = m_vecFrameList[m_nFrameIdx].strPath;
	}
	m_labelFrameName.SetWindowText(strName);
}

/// <summary>
/// Drawing tool buttons의 icon을 갱신
/// </summary>
void CDatasetMakerDlg::UpdateToolBtnImg()
{
	if (m_chkSelectMultiFrames.GetCheck())
		m_chkSelectMultiFrames.SetIcon(::LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_MULTI2)));
	else
		m_chkSelectMultiFrames.SetIcon(::LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_MULTI)));

	if (m_chkAddBoxLabel.GetCheck())
		m_chkAddBoxLabel.SetIcon(::LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_DTT2)));
	else
		m_chkAddBoxLabel.SetIcon(::LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_DTT)));

	if (m_chkAddPolyLabel.GetCheck())
		m_chkAddPolyLabel.SetIcon(::LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_SGMT2)));
	else
		m_chkAddPolyLabel.SetIcon(::LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_SGMT)));

	if (m_chkAddSkelLabel.GetCheck())
		m_chkAddSkelLabel.SetIcon(::LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_SKEL2)));
	else
		m_chkAddSkelLabel.SetIcon(::LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_SKEL)));

}

/// <summary>
/// Dataset frame list의 정보를 갱신
/// </summary>
void CDatasetMakerDlg::UpdateDatasetFrameList()
{
	m_bInitializing = true;

	CString strPageIdx, strPageCount;

	strPageIdx.Format(_T("%d"), m_nPageNum + 1);
	strPageCount.Format(_T("%d"), (int) (m_vecDataSetItems.size() / m_nCashingSize) + 1);

	m_editPageIdx.SetWindowText(strPageIdx);
	m_editPageCount.SetWindowText(strPageCount);

	m_lstSourceImages.DeleteAllItems();

	if (m_nPageNum > m_vecDataSetItems.size() / m_nCashingSize)
		m_nPageNum = m_vecDataSetItems.size() / m_nCashingSize;

	int nStart, nSize;
	nStart = m_nPageNum * m_nCashingSize;
	if ((m_vecDataSetItems.size() - nStart) >= m_nCashingSize)
		nSize = m_nCashingSize;
	else
		nSize = m_vecDataSetItems.size() % m_nCashingSize;

	StartProgress();

	for (int i = 0; i < nSize; i++)
	{
		if (m_vecDataSetItems[nStart + i].nId != nStart + i)
			m_vecDataSetItems[nStart + i].nId = nStart + i;

		CString str, strIdx;
		str.Format(_T("%04d"), m_vecDataSetItems[nStart + i].nId);
		strIdx.Format(_T("%d"), m_vecDataSetItems[nStart + i].nFrmIdx);
		m_lstSourceImages.InsertItem(i, str);
		m_lstSourceImages.SetCheck(i, m_vecDataSetItems[nStart + i].bSelected);
		if (m_vecDataSetItems[nStart + i].nSrcType == SRC_TYPE_STILL)
			m_lstSourceImages.SetItemText(i, 1, _T("STILL"));
		else
			m_lstSourceImages.SetItemText(i, 1, _T("MOVIE"));
		m_lstSourceImages.SetItemText(i, 2, strIdx);
		m_lstSourceImages.SetItemText(i, 3, m_vecDataSetItems[nStart + i].strPath);

		//TODO: 여기서 Inference 할까...?
		m_matCashing[i] = GetDatasetItemImage(nStart + i);
		cv::cuda::GpuMat tmpGpuMath;
		std::vector<std::array<FrPoint, 18>> vecJoint;
		if (_deepInfer != nullptr)
		{
			if (_deepInfer->isPoseInitialize() || m_vecDataSetItems[nStart + i].vecLabels.empty() || !m_matCashing[i].empty())
			{
				if (m_matCashing[i].empty())
					continue;
				if (!m_vecDataSetItems[nStart + i].vecLabels.empty())
					continue;

				cv::Mat tmpMat;
				cv::cvtColor(m_matCashing[i], tmpMat, cv::COLOR_BGR2BGRA);
				tmpGpuMath.upload(tmpMat);
				_deepInfer->doPoseInference(tmpGpuMath.ptr(), tmpGpuMath.step, m_matCashing[i].cols, m_matCashing[i].rows, vecJoint);

				for (auto joint : vecJoint)
				{
					StLabelUnit tmpLabel;
					for (int j = 0; j < joint.size(); ++j)
					{
						StPolyVertex tmpPolyVertex;
						tmpPolyVertex.bVis = false;
						if (joint[j].px > 0 || joint[j].py > 0)
							tmpPolyVertex.bVis = true;
						tmpPolyVertex.ptCoord = { static_cast<int>(joint[j].px), static_cast<int>(joint[j].py) };
						tmpLabel.vPts.push_back(tmpPolyVertex);
					}
					tmpLabel.nStyle = LABEL_UNIT_STYLE_SKEL;
					tmpLabel.nClass = 0;
					m_vecDataSetItems[nStart + i].vecLabels.push_back(tmpLabel);
				}
			}
		}
		CString strMsg;
		strMsg.Format(_T("Loading frames...(idx=%d)"), nStart + i);
		SetProgress(100 * i / nSize, strMsg);
	}
	EndProgress();

	m_bInitializing = false;
}

/// <summary>
/// nIdx의 캐싱된 영상을 가져옴
/// </summary>
/// <param name="nIdx"></param>
/// <returns></returns>
Mat CDatasetMakerDlg::GetCashingImage(int nIdx)
{
	return m_matCashing[nIdx];
}

/// <summary>
/// nIdx의 Dataset image를 가져옴
/// </summary>
/// <param name="nIdx"></param>
/// <returns></returns>
Mat CDatasetMakerDlg::GetDatasetItemImage(int nIdx)
{
	Mat frame;

	if (m_vecDataSetItems[nIdx].nSrcType == SRC_TYPE_STILL || nIdx < 0)
	{
		LoadStillImage(frame, m_vecDataSetItems[nIdx].strPath);
	}
	else
	{
		LoadSelectFrame(frame, m_vecDataSetItems[nIdx].strPath, m_vecDataSetItems[nIdx].nFrmIdx);
	}

	return frame;
}

/// <summary>
/// strFilePath 경로의 영상을 로드하여 nIdx번째 frame을 read
/// </summary>
/// <param name="frame"></param>
/// <param name="strFilePath"></param>
/// <param name="nIdx"></param>
/// <returns></returns>
bool CDatasetMakerDlg::LoadSelectFrame(Mat& frame, CString strFilePath, int nIdx)
{
	bool bSuccess = false;

	if (m_vcObj.isOpened())
		m_vcObj.release();

	CT2CA pszConvertedAnsiString(strFilePath);

	m_vcObj.open(std::string(pszConvertedAnsiString));


	if (!m_vcObj.isOpened())
	{
		MessageBox(_T("Video Open Error"));
		return false;
	}
	m_vcObj.set(CAP_PROP_POS_FRAMES, nIdx);

	bSuccess = m_vcObj.read(frame);

	return bSuccess;
}

/// <summary>
/// strFilePath 경로의 영상을 frame에 로드
/// </summary>
/// <param name="frame"></param>
/// <param name="strFilePath"></param>
/// <returns></returns>
bool CDatasetMakerDlg::LoadStillImage(Mat& frame, CString strFilePath)
{
	CT2CA pszConvertedAnsiString(strFilePath);

	Mat temp;
	temp = imread(std::string(pszConvertedAnsiString), cv::IMREAD_ANYCOLOR);

	// Gray channel일 경우 Color channel로 변환
	if (temp.channels() == 1)
		cvtColor(temp, frame, cv::COLOR_GRAY2BGR);
	else
		frame = temp;

	if (frame.empty())
	{
		MessageBox(_T("Image Open Error"));
		return false;
	}

	return true;
}

/// <summary>
/// idx번째의 dataset frame list의 항목을 선택
/// </summary>
/// <param name="idx"></param>
void CDatasetMakerDlg::SelectFrame(int idx)
{
	if (idx < m_nPageNum * m_nCashingSize)
		return;
	else if (idx >= (m_nPageNum + 1) * m_nCashingSize)
		return;

	m_lstSourceImages.SetItemState(-1, 0, LVIS_SELECTED | LVIS_FOCUSED);
	m_lstSourceImages.SetItemState(idx, LVIS_FOCUSED | LVIS_SELECTED, LVIS_FOCUSED | LVIS_SELECTED);
	m_lstSourceImages.EnsureVisible(idx, FALSE);
	m_lstSourceImages.SetFocus();
}

/// <summary>
/// str 문자열을 delimiters를 기준으로 분리하여 vector 형식으로 반환
/// </summary>
/// <param name="str"></param>
/// <param name="delimiters"></param>
/// <returns></returns>
vector<string> CDatasetMakerDlg::SplitPath(const string& str, const set<char> delimiters)
{
	vector<string> result;

	char const* pch = str.c_str();
	char const* start = pch;
	for (; *pch; ++pch)
	{
		if (delimiters.find(*pch) != delimiters.end())
		{
			if (start != pch)
			{
				string str(start, pch);
				result.push_back(str);
			}
			else
			{
				result.push_back("");
			}
			start = pch + 1;
		}
	}
	result.push_back(start);

	return result;
}

/// <summary>
/// strPath 경로의 폴더에 존재하는 ".bmp", ".jpg", ".png" 확장자의 파일을 vector 형식으로 반환
/// </summary>
/// <param name="strPath"></param>
/// <returns></returns>
vector<CString> CDatasetMakerDlg::GetImageFilesInDirectory(const CString strPath)
{
	vector<CString> strList;

	//검색 클래스
	CFileFind finder;

	//CFileFind는 파일, 디렉터리가 존재하면 TRUE 를 반환함
	BOOL bWorking = finder.FindFile(strPath + _T("\\*.*"));

	while (bWorking)
	{
		//다음 파일 / 폴더 가 존재하면다면 TRUE 반환
		bWorking = finder.FindNextFile();
		//파일 일때
		if (finder.IsArchived())
		{
			//파일의 이름
			CString _fileName = finder.GetFileName();

			// 현재폴더 상위폴더 썸네일파일은 제외
			if (_fileName == _T(".") ||
				_fileName == _T("..") ||
				_fileName == _T("Thumbs.db")) continue;

			if (_fileName.Right(3) != "BMP" && _fileName.Right(3) != "bmp"
				&& _fileName.Right(3) != "JPG" && _fileName.Right(3) != "jpg"
				&& _fileName.Right(3) != "PNG" && _fileName.Right(3) != "png"
				)
				continue;

			strList.push_back(strPath + "\\" + _fileName);
		}
	}

	return strList;
}

/// <summary>
/// 현재 Dataset list를 strPath 경로에 4D Dataset 형식으로 저장
/// </summary>
/// <param name="strPath"></param>
/// <returns></returns>
int CDatasetMakerDlg::SaveLabelsJson(const CString strPath)
{
	UpdateData(TRUE);

	if (m_vecDataSetItems.size() == 0)
	{
		return FALSE;
	}

	StartProgress();

	Document docu;
	docu.SetObject();

	Value valArr(kArrayType);
	Document::AllocatorType& allocator = docu.GetAllocator();

	for (int i = 0; i < m_vecDataSetItems.size(); i++)
	{
		CString strMsg;
		strMsg.Format(_T("Exporting dataset objects...(idx=%d)"), i);
		SetProgress(100 * i / m_vecDataSetItems.size(), strMsg);

		if (!m_vecDataSetItems[i].bSelected)
			continue;

		Mat img = GetDatasetItemImage(i);

		CString strFileName;
		strFileName.Format(_T("%04d.jpg"), i);

		CString pathImg;
		pathImg.Format(m_strPathResult + _T("\\") + strFileName);

		imwrite(string(CT2CA(pathImg)), img);

		Value valItem(kObjectType);
		valItem.AddMember("FileName", Value(string(CT2CA(strFileName)).c_str(), allocator).Move(), allocator);
		valItem.AddMember("Class", Value(string(CT2CA(m_vecDataSetItems[i].strSceneTitle)).c_str(), allocator).Move(), allocator);

		int nWidth, nHeight;
		nWidth = m_vecDataSetItems[i].nWidth;
		nHeight = m_vecDataSetItems[i].nHeight;

		vector<StLabelUnit>& labels = m_vecDataSetItems[i].vecLabels;

		Value valObject(kArrayType);
		Value valSegmentation(kArrayType);
		Value valSkeleton(kArrayType);

		for (int j = 0; j < labels.size(); j++)
		{
			if (labels[j].nStyle == LABEL_UNIT_STYLE_BOX)
			{
				Value valBbox(kObjectType);
				valBbox.AddMember("ID", Value(string(CT2CA(m_vecClassNames[labels[j].nClass])).c_str(), allocator), allocator);

				double rx, ry, rw, rh;
				rx = (double)labels[j].vPts[BOX_LABEL_PT_POS_LT].ptCoord.x / nWidth;
				ry = (double)labels[j].vPts[BOX_LABEL_PT_POS_LT].ptCoord.y / nHeight;
				rw = (double)(labels[j].vPts[BOX_LABEL_PT_POS_RB].ptCoord.x - labels[j].vPts[BOX_LABEL_PT_POS_LT].ptCoord.x) / nWidth;
				rh = (double)(labels[j].vPts[BOX_LABEL_PT_POS_RB].ptCoord.y - labels[j].vPts[BOX_LABEL_PT_POS_LT].ptCoord.y) / nHeight;

				Value valPts(kArrayType);
				valPts.PushBack(rx, allocator);
				valPts.PushBack(ry, allocator);
				valPts.PushBack(rw, allocator);
				valPts.PushBack(rh, allocator);

				valBbox.AddMember("bbox", valPts, allocator);

				valObject.PushBack(valBbox, allocator);
			}
			else if (labels[j].nStyle == LABEL_UNIT_STYLE_POLY)
			{
				Value valPoly(kObjectType);
				valPoly.AddMember("ID", Value(string(CT2CA(m_vecClassNames[labels[j].nClass])).c_str(), allocator), allocator);

				Value valPts(kArrayType);
				for (int k = 0; k < labels[j].vPts.size(); k++)
				{
					Value valCoord(kArrayType);
					valCoord.PushBack((double)labels[j].vPts[k].ptCoord.x / nWidth, allocator);
					valCoord.PushBack((double)labels[j].vPts[k].ptCoord.y / nHeight, allocator);

					valPts.PushBack(valCoord, allocator);
				}
				valPoly.AddMember("Coord", valPts, allocator);

				valSegmentation.PushBack(valPoly, allocator);
			}
			else if (labels[j].nStyle == LABEL_UNIT_STYLE_SKEL)
			{
				Value valSkel(kObjectType);
				valSkel.AddMember("ID", Value(string(CT2CA(m_vecClassNames[labels[j].nClass])).c_str(), allocator), allocator);

				Value valPts(kArrayType);
				for (int k = 0; k < labels[j].vPts.size(); k++)
				{
					Value valCoord(kArrayType);
					if (labels[j].vPts[k].bVis == false)
					{
						valCoord.PushBack(-1, allocator);
						valCoord.PushBack(-1, allocator);
					}
					else
					{
						valCoord.PushBack((double)labels[j].vPts[k].ptCoord.x / nWidth, allocator);
						valCoord.PushBack((double)labels[j].vPts[k].ptCoord.y / nHeight, allocator);
					}

					valPts.PushBack(valCoord, allocator);
				}
				valSkel.AddMember("Edge", valPts, allocator);

				valSkeleton.PushBack(valSkel, allocator);
			}
		}

		valItem.AddMember("Object", valObject, allocator);
		valItem.AddMember("Segmentation", valSegmentation, allocator);
		valItem.AddMember("Skeleton", valSkeleton, allocator);

		valArr.PushBack(valItem, allocator);
	}

	docu.AddMember("Annotations", valArr, allocator);

	CString pathJson;
	pathJson.Format(m_strPathResult + _T("\\") + _T("annotations.json"));

	ofstream outFile;
	outFile.open(string(CT2CA(pathJson)), ios::out);
	OStreamWrapper osw(outFile);

	Writer<OStreamWrapper> writer(osw);
	docu.Accept(writer);

	outFile.close();

	EndProgress();

	return TRUE;
}

/// <summary>
/// 현재 Dataset list를 strPath 경로에 COCO Dataset 형식으로 저장
/// </summary>
/// <param name="strPath"></param>
/// <returns></returns>
int CDatasetMakerDlg::SaveSkeletonsCOCOJson(const CString strPath)
{
	UpdateData(TRUE);

	if (m_vecDataSetItems.size() == 0)
	{
		return FALSE;
	}

	StartProgress();

	Document docu;
	docu.SetObject();

	Document::AllocatorType& allocator = docu.GetAllocator();

	Value valImages(kArrayType);
	for (int i = 0; i < m_vecDataSetItems.size(); i++)
	{
		CString strMsg;
		strMsg.Format(_T("Exporting dataset images...(idx=%d)"), i);
		SetProgress(50 * i / m_vecDataSetItems.size(), strMsg);

		if (!m_vecDataSetItems[i].bSelected)
			continue;

		Mat img = GetDatasetItemImage(i);

		CString strFileName;
		strFileName.Format(_T("%04d.jpg"), i);

		CString pathImg;
		pathImg.Format(m_strPathResult + _T("\\") + strFileName);

		imwrite(string(CT2CA(pathImg)), img);

		Value valItem(kObjectType);
		valItem.AddMember("id", Value(i).Move(), allocator);
		valItem.AddMember("file_name", Value(string(CT2CA(strFileName)).c_str(), allocator).Move(), allocator);
		valItem.AddMember("width", Value(img.cols).Move(), allocator);
		valItem.AddMember("height", Value(img.rows).Move(), allocator);

		valImages.PushBack(valItem, allocator);

	}
	docu.AddMember("images", valImages, allocator);

	int nCntLbl = 0;

	Value valAnn(kArrayType);
	for (int i = 0; i < m_vecDataSetItems.size(); i++)
	{
		CString strMsg;
		strMsg.Format(_T("Exporting dataset objects...(idx=%d)"), i);
		SetProgress(50 + 50 * i / m_vecDataSetItems.size(), strMsg);

		vector<StLabelUnit>& labels = m_vecDataSetItems[i].vecLabels;

		for (int j = 0; j < labels.size(); j++)
		{
			if (labels[j].nStyle == LABEL_UNIT_STYLE_SKEL)
			{
				Value valSkel(kObjectType);

				valSkel.AddMember("image_id", Value(i).Move(), allocator);
				valSkel.AddMember("id", Value(nCntLbl++).Move(), allocator);
				valSkel.AddMember("category_id", Value(labels[j].nClass).Move(), allocator);

				int nCnt = 0;
				Value valPts(kArrayType);
				for (int k = 0; k < labels[j].vPts.size(); k++)
				{
					if (labels[j].vPts[k].bVis == false)
					{
						valPts.PushBack(0, allocator);
						valPts.PushBack(0, allocator);
						valPts.PushBack(0, allocator);
					}
					else
					{
						valPts.PushBack((int) labels[j].vPts[k].ptCoord.x, allocator);
						valPts.PushBack((int) labels[j].vPts[k].ptCoord.y, allocator);
						valPts.PushBack(1, allocator);
						nCnt++;
					}
				}
				valSkel.AddMember("keypoints", valPts, allocator);
				valSkel.AddMember("num_keypoints", Value(nCnt).Move(), allocator);

				valAnn.PushBack(valSkel, allocator);
			}
		}
	}

	docu.AddMember("annotations", valAnn, allocator);

	Value valCat(kArrayType);

	Value valKey(kArrayType);
	for (int i = 0; i < MAX_SKELETON_COUNT; i++)
	{
		valKey.PushBack(Value(g_SkeletonModel[i].c_str(), allocator).Move(), allocator);
	}

	Value valJoints(kArrayType);
	for (int i = 0; i < MAX_LINK_COUNT; i++)
	{
		Value valLink(kArrayType);
		valLink.PushBack(Value(g_Links[i][0] + 1).Move(), allocator);
		valLink.PushBack(Value(g_Links[i][1] + 1).Move(), allocator);

		valJoints.PushBack(valLink, allocator);
	}

	for (int i = 0; i < m_vecClassNames.size(); i++)
	{
		Value valClass(kObjectType);
		Value valKP(kArrayType);
		Value valSK(kArrayType);
		valKP.CopyFrom(valKey, allocator);
		valSK.CopyFrom(valJoints, allocator);
		valClass.AddMember("supercategory", Value("person").Move(), allocator);
		valClass.AddMember("id", Value(i).Move(), allocator);
		valClass.AddMember("name", Value(string(CT2CA(m_vecClassNames[i])).c_str(), allocator), allocator);
		valClass.AddMember("keypoints", valKP, allocator);
		valClass.AddMember("skeleton", valSK, allocator);

		valCat.PushBack(valClass, allocator);
	}

	docu.AddMember("categories", valCat, allocator);


	CString pathJson;
	pathJson.Format(m_strPathResult + _T("\\") + _T("annotations.json"));

	ofstream outFile;
	outFile.open(string(CT2CA(pathJson)), ios::out);
	OStreamWrapper osw(outFile);

	Writer<OStreamWrapper> writer(osw);
	docu.Accept(writer);

	outFile.close();

	EndProgress();

	return TRUE;
}

/// <summary>
/// strPath 경로에 존재하는 4D Dataset 형식의 파일을 로드하여 Dataset list에 추가
/// </summary>
/// <param name="strPath"></param>
/// <returns></returns>
int CDatasetMakerDlg::LoadExistingLabelsJson(const CString strPath)
{
	if (IDNO == AfxMessageBox(_T("Dataset 정보를 불러오시겠습니까? 현재 저장되지 않은 정보는 삭제됩니다."), MB_YESNO))
	{
		return FALSE;
	}

	StartProgress();

	m_vecDataSetItems.clear();
	m_vecClassNames.clear();
	m_vecCurCoord.clear();


	SetProgress(0, _T("Parsing json file..."));

	CString strPathHeader = strPath + _T("\\") + _T("annotations.json");

	std::ifstream fin;
	fin.open(strPathHeader, std::ios::in);

	std::string str;
	str.assign(std::istreambuf_iterator<char>(fin), std::istreambuf_iterator<char>());

	Document docu;

	docu.Parse(str.c_str());

	fin.close();

	int i = 0;

	for (auto& value : docu["Annotations"].GetArray())
	{
		CString strMsg;
		strMsg.Format(_T("Creating dataset object(idx=%d)..."), i);
		SetProgress(100 * i / docu["Annotations"].Size(), strMsg);

		StDataSetItem item;

		item.bSelected = true;
		item.nId = i++;
		item.nSrcType = SRC_TYPE_STILL;
		item.nFrmIdx = -1;
		item.strPath = strPath + _T("\\") + CString(value["FileName"].GetString());
		item.strSceneTitle = value["Class"].GetString();

		Mat temp;
		temp = imread(string(CT2CA(item.strPath)));
		item.nWidth = temp.cols;
		item.nHeight = temp.rows;

		// Detection을 위한 box 정보 가져오기
		for (auto& box : value["Object"].GetArray())
		{
			CString strId = CString(box["ID"].GetString());

			int nFound = -1;

			for (int j = 0; j < m_vecClassNames.size(); j++)
			{
				if (m_vecClassNames[j] == strId)
				{
					nFound = j;
				}
			}

			if (nFound == -1)
			{
				m_vecClassNames.push_back(strId);
				nFound = m_vecClassNames.size() - 1;
			}

			StLabelUnit label;
			label.nClass = nFound;
			label.nStyle = LABEL_UNIT_STYLE_BOX;

			double nLeft, nTop, nRight, nBottom;

			nLeft = box["bbox"][0].GetDouble() * item.nWidth;
			nTop = box["bbox"][1].GetDouble() * item.nHeight;
			nRight = nLeft + box["bbox"][2].GetDouble() * item.nWidth;
			nBottom = nTop + box["bbox"][3].GetDouble() * item.nHeight;

			label.vPts.push_back({ true, Point(nLeft, nTop) });
			label.vPts.push_back({ true, Point(nRight, nBottom) });

			item.vecLabels.push_back(label);
		}

		// Segmentation을 위한 polygon area 정보 가져오기
		for (auto& poly : value["Segmentation"].GetArray())
		{
			CString strId = CString(poly["ID"].GetString());

			int nFound = -1;

			for (int j = 0; j < m_vecClassNames.size(); j++)
			{
				if (m_vecClassNames[j] == strId)
				{
					nFound = j;
				}
			}

			if (nFound == -1)
			{
				m_vecClassNames.push_back(strId);
				nFound = m_vecClassNames.size() - 1;
			}

			StLabelUnit label;
			label.nClass = nFound;
			label.nStyle = LABEL_UNIT_STYLE_POLY;

			for (auto& coord : poly["Coord"].GetArray())
			{
				Point pt;
				pt.x = coord[0].GetDouble() * item.nWidth;
				pt.y = coord[1].GetDouble() * item.nHeight;

				label.vPts.push_back({ true, pt });
			}

			item.vecLabels.push_back(label);
		}

		// Pose estimation을 위한 skeleton 정보 가져오기
		for (auto& skel : value["Skeleton"].GetArray())
		{
			CString strId = CString(skel["ID"].GetString());

			int nFound = -1;

			for (int j = 0; j < m_vecClassNames.size(); j++)
			{
				if (m_vecClassNames[j] == strId)
				{
					nFound = j;
				}
			}

			if (nFound == -1)
			{
				m_vecClassNames.push_back(strId);
				nFound = m_vecClassNames.size() - 1;
			}

			StLabelUnit label;
			label.nClass = nFound;
			label.nStyle = LABEL_UNIT_STYLE_SKEL;

			for (auto& coord : skel["Edge"].GetArray())
			{
				Point pt;
				pt.x = coord[0].GetDouble() * item.nWidth;
				pt.y = coord[1].GetDouble() * item.nHeight;

				if (pt.x < 0 || pt.y < 0)
					label.vPts.push_back({ false, pt });
				else
					label.vPts.push_back({ true, pt });
			}

			item.vecLabels.push_back(label);
		}

		m_vecDataSetItems.push_back(item);
	}

	EndProgress();

	UpdateDatasetFrameList();

	UpdateClassCount();
	UpdateLabelList();

	return TRUE;
}

/// <summary>
/// strJsonPath 경로에 존재하는 BBox 형식의 .json 파일을 Parsing하여 Dataset list에 추가
/// </summary>
/// <param name="strJsonPath"></param>
/// <param name="strImgFolderPath"></param>
/// <returns></returns>
int CDatasetMakerDlg::LoadBBoxTxt(const CString strPath)
{
	if (IDNO == AfxMessageBox(_T("Dataset 정보를 불러오시겠습니까? 현재 저장되지 않은 정보는 삭제됩니다."), MB_YESNO))
	{
		return FALSE;
	}

	StartProgress();

	m_vecDataSetItems.clear();
	m_vecClassNames.clear();
	m_vecCurCoord.clear();


	SetProgress(0, _T("Parsing BBox txt file..."));

	//검색 클래스
	CFileFind finder;

	//CFileFind는 파일, 디렉터리가 존재하면 TRUE 를 반환함
	BOOL bWorking = finder.FindFile(strPath + _T("\\*.*"));


	m_vecClassNames.push_back(_T("red"));
	m_vecClassNames.push_back(_T("blue"));
	m_vecClassNames.push_back(_T("referee"));
	m_vecClassNames.push_back(_T("hug"));

	int nId = 0;

	while (bWorking)
	{
		CString strMsg;
		strMsg.Format(_T("Creating dataset object(idx=%d)..."), nId);
		SetProgress(99, strMsg);

		//다음 파일 / 폴더 가 존재하면다면 TRUE 반환
		bWorking = finder.FindNextFile();
		//파일 일때
		if (finder.IsArchived())
		{
			//파일의 이름
			CString _fileName = finder.GetFileName();

			// 현재폴더 상위폴더 썸네일파일은 제외
			if (_fileName == _T(".") ||
				_fileName == _T("..") ||
				_fileName == _T("Thumbs.db")) continue;

			if (_fileName.Right(3) != "TXT" && _fileName.Right(3) != "txt")
				continue;

			CString strTxtPath = strPath + "\\" + _fileName;

			CString strImgPath = strPath + "\\" + _fileName.Left(_fileName.GetLength() - 3) + "jpg";


			StDataSetItem item;

			item.bSelected = true;
			item.nId = nId;
			item.nSrcType = SRC_TYPE_STILL;
			item.nFrmIdx = nId;
			item.strPath = strImgPath;
			item.strSceneTitle = _T("default");
			CImage cImg;
			cImg.Load(strImgPath);
			item.nWidth = cImg.GetWidth();
			item.nHeight = cImg.GetHeight();

			ifstream ifs;
			ifs.open(string(CT2CA(strTxtPath)));

			while (!ifs.eof())
			{
				StLabelUnit lbl;
				
				lbl.nStyle = LABEL_UNIT_STYLE_BOX;


				string line;
				std::getline(ifs, line);

				if (line.size() < 2)
					continue;

				istringstream fin(line);

				string strClass;
				fin >> strClass;

				std::stringstream ssCls(strClass);
				if (!ssCls.fail())
					ssCls >> lbl.nClass;
				else
					lbl.nClass = 0;

				double dX, dY, dW, dH;
				fin >> dX >> dY >> dW >> dH;

				int nLeft, nTop, nRight, nBottom;
				nLeft = dX * item.nWidth;
				nTop = dY * item.nHeight;
				nRight = nLeft + dW * item.nWidth;
				nBottom = nTop + dH * item.nHeight;

				lbl.vPts.push_back({ true, Point(nLeft, nTop) });
				lbl.vPts.push_back({ true, Point(nRight, nBottom) });
				
				item.vecLabels.push_back(lbl);
			}

			m_vecDataSetItems.push_back(item);

			nId++;
		}
	}

	SetProgress(100, _T("Complete"));

	UpdateDatasetFrameList();

	UpdateClassCount();
	UpdateLabelList();

	EndProgress();

	return TRUE;
}

/// <summary>
/// strJsonPath 경로에 존재하는 COCO Dataset Skeleton 형식의 .json 파일을 Parsing하여 Dataset list에 추가
/// </summary>
/// <param name="strJsonPath"></param>
/// <returns></returns>
int CDatasetMakerDlg::LoadSkeletonsCOCOJson(const CString strJsonPath, const CString strImgFolderPath)
{
	if (IDNO == AfxMessageBox(_T("Dataset 정보를 불러오시겠습니까? 현재 저장되지 않은 정보는 삭제됩니다."), MB_YESNO))
	{
		return FALSE;
	}

	StartProgress();

	m_vecDataSetItems.clear();
	m_vecClassNames.clear();
	m_vecCurCoord.clear();


	SetProgress(0, _T("Parsing COCO json file..."));

	ifstream fin;
	fin.open(strJsonPath, ios::in);

	string str;
	str.assign(istreambuf_iterator<char>(fin), istreambuf_iterator<char>());

	Document docu;
	docu.Parse(str.c_str());

	fin.close();

	int i = 0;

	for (auto& value : docu["images"].GetArray())
	{
		CString strMsg;
		strMsg.Format(_T("Creating dataset image(idx=%d)..."), i);
		SetProgress(33 * i / docu["images"].Size(), strMsg);
		i++;

		StDataSetItem item;

		item.bSelected = true;
		item.nId = value["id"].GetInt();
		item.nSrcType = SRC_TYPE_STILL;
		item.nFrmIdx = value["id"].GetInt();
		item.strPath = strImgFolderPath + _T("\\") + CString(value["file_name"].GetString());
		item.strSceneTitle = _T("default");
		item.nWidth = value["width"].GetInt();
		item.nHeight = value["height"].GetInt();

		m_vecDataSetItems.push_back(item);
	}

	i = 0;

	for (auto& value : docu["categories"].GetArray())
	{
		CString strMsg;
		strMsg.Format(_T("Add dataset categories(idx=%d)..."), i);
		SetProgress(33 + 33 * i / docu["categories"].Size(), strMsg);
		i++;

		int nFound = -1;

		CString name = CString(value["name"].GetString());

		for (int j = 0; j < m_vecClassNames.size(); j++)
		{
			if (m_vecClassNames[j] == name)
			{
				nFound = j;
			}
		}

		if (nFound == -1)
			m_vecClassNames.push_back(name);
	}

	i = 0;

	int nSize = docu["annotations"].Size();

	for (auto& value : docu["annotations"].GetArray())
	{
		CString strMsg;
		strMsg.Format(_T("Load dataset annotations(idx=%d)..."), i);
		SetProgress(66 + 33 * i / docu["annotations"].Size(), strMsg);
		i++;

		int nId = value["image_id"].GetInt();
		int nFound = -1;

		for (int j = 0; j < m_vecDataSetItems.size(); j++)
		{
			if (m_vecDataSetItems[j].nId == nId)
				nFound = j;
		}

		if (nFound == -1)
			continue;

		StLabelUnit label;
		label.nClass = value["category_id"].GetInt();
		label.nStyle = LABEL_UNIT_STYLE_SKEL;

		// get keypoints
		bool vis[17];
		Point keypoints[17];
		bool bExist = false;

		for (int j = 0; j < MAX_SKELETON_COUNT; j++)
		{
			if(value["keypoints"][j * 3 + 2].GetInt() == 0)
				label.vPts.push_back({ false, Point(0, 0) });
			else
				label.vPts.push_back({ true, Point(value["keypoints"][j * 3].GetInt(), value["keypoints"][j * 3 + 1].GetInt()) });
		}
		
		m_vecDataSetItems[nFound].vecLabels.push_back(label);
	}
	SetProgress(100, _T("Complete"));

	UpdateDatasetFrameList();

	UpdateClassCount();
	UpdateLabelList();

	EndProgress();

	return TRUE;

}

/// <summary>
/// strJsonPath 경로에 존재하는 MPII Dataset 형식의 .json 파일과 strImgFolderPath 경로에 존재하는 원본 영상들을 Parsing하여 Dataset list에 추가
/// </summary>
/// <param name="strJsonPath"></param>
/// <param name="strImgFolderPath"></param>
/// <returns></returns>
int CDatasetMakerDlg::LoadMPII(const CString strJsonPath, const CString strImgFolderPath)
{
	if (IDNO == AfxMessageBox(_T("MPII dataset 정보를 불러오시겠습니까? 현재 저장되지 않은 정보는 삭제됩니다."), MB_YESNO))
	{
		return FALSE;
	}

	StartProgress();

	m_vecDataSetItems.clear();
	m_vecClassNames.clear();
	m_vecCurCoord.clear();


	SetProgress(0, _T("Parsing MPII json file..."));

	ifstream fin;
	fin.open(strJsonPath, ios::in);

	string str;
	str.assign(istreambuf_iterator<char>(fin), istreambuf_iterator<char>());

	Document docu;
	docu.Parse(str.c_str());

	fin.close();

	int i = 0;

	for (auto& value : docu.GetArray())
	{
		CString strMsg;
		strMsg.Format(_T("Creating dataset object(idx=%d)..."), i);
		SetProgress(100 * i / docu.Size(), strMsg);

		StDataSetItem item;

		item.bSelected = true;
		item.nId = i;
		item.nSrcType = SRC_TYPE_STILL;
		item.nFrmIdx = -1;
		item.strPath = strImgFolderPath + _T("\\") + CString(value["image"].GetString());
		item.strSceneTitle = _T("default");

		Mat temp;
		temp = imread(string(CT2CA(item.strPath)), cv::IMREAD_ANYDEPTH);

		if (temp.empty())
			continue;

		item.nWidth = temp.cols;
		item.nHeight = temp.rows;

		if (value["joints_vis"].Size() != MAX_SKELETON_COUNT || value["joints"].Size() != MAX_SKELETON_COUNT)
			continue;

		CString strId = _T("skeleton");

		int nFound = -1;

		for (int j = 0; j < m_vecClassNames.size(); j++)
		{
			if (m_vecClassNames[j] == strId)
			{
				nFound = j;
			}
		}

		if (nFound == -1)
		{
			m_vecClassNames.push_back(strId);
			nFound = m_vecClassNames.size() - 1;
		}

		StLabelUnit label;
		label.nClass = nFound;
		label.nStyle = LABEL_UNIT_STYLE_SKEL;

		int arrOrder[] = { 9, 8, 13, 14, 15, 12, 11, 10, 7, 6, 3, 4, 5, 2, 1, 0 };

		auto& vis = value["joints_vis"];
		auto& skel = value["joints"];

		for (int j = 0; j < 16; j++)
		{
			Point ptCoord;
			if (j == 0)
			{
				ptCoord = Point2f((skel[8][0].GetFloat() + skel[9][0].GetFloat()) / 2., (skel[8][1].GetFloat() + skel[9][1].GetFloat()) / 2.);

				label.vPts.push_back({ vis[8].GetInt() > 0 && vis[9].GetInt() ? true : false, ptCoord });
			}
			else if (j == 1)
			{
				ptCoord = Point2f((skel[12][0].GetFloat() + skel[13][0].GetFloat()) / 2., (skel[12][1].GetFloat() + skel[13][1].GetFloat()) / 2.);

				label.vPts.push_back({ vis[12].GetInt() > 0 && vis[13].GetInt() ? true : false, ptCoord });
			}
			else
			{
				int nIdx = arrOrder[j];
				ptCoord = Point2f(skel[nIdx][0].GetFloat(), skel[nIdx][1].GetFloat());

				label.vPts.push_back({ vis[nIdx].GetInt() > 0 ? true : false, ptCoord });
			}
		}

		item.vecLabels.push_back(label);

		m_vecDataSetItems.push_back(item);

		i++;
	}

	EndProgress();

	UpdateDatasetFrameList();

	UpdateClassCount();
	UpdateLabelList();

	return TRUE;
}

/// <summary>
/// strJsonPath 경로에 존재하는 COCO Dataset 형식의 .json 파일과 strImgFolderPath 경로에 존재하는 원본 영상들을 Parsing하여 Dataset list에 추가
/// </summary>
/// <param name="strJsonPath"></param>
/// <param name="strImgFolderPath"></param>
/// <returns></returns>
int CDatasetMakerDlg::LoadCOCO(const CString strJsonPath, const CString strImgFolderPath)
{
	if (IDNO == AfxMessageBox(_T("COCO dataset 정보를 불러오시겠습니까? 현재 저장되지 않은 정보는 삭제됩니다."), MB_YESNO))
	{
		return FALSE;
	}

	StartProgress();

	m_vecDataSetItems.clear();
	m_vecClassNames.clear();
	m_vecCurCoord.clear();


	SetProgress(0, _T("Parsing COCO json file..."));

	ifstream fin;
	fin.open(strJsonPath, ios::in);

	string str;
	str.assign(istreambuf_iterator<char>(fin), istreambuf_iterator<char>());

	Document docu;
	docu.Parse(str.c_str());

	fin.close();

	int i = 0;

	int arrOrder[] = { -1, -1, 5, 7, 9, 6, 8, 10, -1, -1, 11, 13, 15, 12, 14, 16 };

	for (auto& value : docu["images"].GetArray())
	{
		CString strMsg;
		strMsg.Format(_T("Creating dataset object(idx=%d)..."), i);
		SetProgress(100 * i / docu["images"].Size(), strMsg);

		StDataSetItem item;

		item.bSelected = true;
		item.nId = i;
		item.nSrcType = SRC_TYPE_STILL;
		item.nFrmIdx = -1;
		item.strPath = strImgFolderPath + _T("\\") + CString(value["file_name"].GetString());
		item.strSceneTitle = _T("default");

		Mat temp;
		temp = imread(string(CT2CA(item.strPath)), cv::IMREAD_ANYDEPTH);

		if (temp.empty())
			continue;

		item.nWidth = temp.cols;
		item.nHeight = temp.rows;

		CString strId = _T("skeleton");

		int nFound = -1;

		for (int j = 0; j < m_vecClassNames.size(); j++)
		{
			if (m_vecClassNames[j] == strId)
			{
				nFound = j;
			}
		}

		if (nFound == -1)
		{
			m_vecClassNames.push_back(strId);
			nFound = m_vecClassNames.size() - 1;
		}

		int nId = value["id"].GetInt();

		for (auto& obj : docu["annotations"].GetArray())
		{
			if (nId == obj["image_id"].GetInt())
			{
				StLabelUnit label;
				label.nClass = nFound;
				label.nStyle = LABEL_UNIT_STYLE_SKEL;

				// get keypoints
				bool vis[17];
				Point keypoints[17];
				bool bExist = false;

				for (int j = 0; j < 17; j++)
				{
					keypoints[j].x = obj["keypoints"][j * 3].GetInt();
					keypoints[j].y = obj["keypoints"][j * 3 + 1].GetInt();
					vis[j] = obj["keypoints"][j * 3 + 2].GetInt() == 0 ? false : true;
				}

				// Parsing to 4d style
				for (int j = 0; j < 16; j++)
				{
					if (j == 0)
					{
						int nSumX = 0, nSumY = 0, nCnt = 0;
						if (vis[0]) {
							nSumX += keypoints[0].x;	nSumY += keypoints[0].y;	nCnt++;
						}
						if (vis[1]) {
							nSumX += keypoints[1].x;	nSumY += keypoints[1].y;	nCnt++;
						}
						if (vis[2]) {
							nSumX += keypoints[2].x;	nSumY += keypoints[2].y;	nCnt++;
						}
						if (vis[3]) {
							nSumX += keypoints[3].x;	nSumY += keypoints[3].y;	nCnt++;
						}
						if (vis[4]) {
							nSumX += keypoints[4].x;	nSumY += keypoints[4].y;	nCnt++;
						}

						if (nCnt > 0)
						{
							label.vPts.push_back({ true, Point(nSumX / nCnt, nSumY / nCnt) });
							bExist = true;
						}
						else
							label.vPts.push_back({ false, Point(-1, -1) });
					}
					else if (j == 1)
					{
						if (vis[5] && vis[6])
						{
							label.vPts.push_back({ true, Point((keypoints[5].x + keypoints[6].x) / 2, (keypoints[5].y + keypoints[6].y) / 2) });
							bExist = true;
						}
						else
							label.vPts.push_back({ false, Point(-1, -1) });
					}
					else if (j == 8)
					{
						if (vis[5] && vis[6] && vis[11] && vis[12])
						{
							Point pt;
							pt.x = (3 * (keypoints[5].x + keypoints[6].x) + (keypoints[11].x + keypoints[12].x)) / 8;
							pt.y = (3 * (keypoints[5].y + keypoints[6].y) + (keypoints[11].y + keypoints[12].y)) / 8;

							label.vPts.push_back({ true, pt });
							bExist = true;
						}
						else
							label.vPts.push_back({ false, Point(-1, -1) });
					}
					else if (j == 9)
					{
						if (vis[5] && vis[6] && vis[11] && vis[12])
						{
							Point pt;
							pt.x = ((keypoints[5].x + keypoints[6].x) + 3 * (keypoints[11].x + keypoints[12].x)) / 8;
							pt.y = ((keypoints[5].y + keypoints[6].y) + 3 * (keypoints[11].y + keypoints[12].y)) / 8;

							label.vPts.push_back({ true, pt });
							bExist = true;
						}
						else
							label.vPts.push_back({ false, Point(-1, -1) });
					}
					else
					{
						if (vis[arrOrder[j]])
						{
							label.vPts.push_back({ true, keypoints[arrOrder[j]] });
							bExist = true;
						}
						else
							label.vPts.push_back({ false, Point(-1, -1) });
					}
				}

				if (bExist)
				{
					item.vecLabels.push_back(label);
				}
			}
		}

		if (item.vecLabels.size() == 0)
		{
			i++;
			continue;
		}

		m_vecDataSetItems.push_back(item);

		i++;
	}

	EndProgress();

	UpdateDatasetFrameList();

	UpdateClassCount();
	UpdateLabelList();

	return TRUE;
}

/// <summary>
/// Dataset list의 Box 정보들을 k-means clustering 기법을 활용해 anchors 정보로 군집화하여 이중 vector 형식으로 반환
/// </summary>
/// <returns></returns>
vector<vector<Size2f>> CDatasetMakerDlg::GetAnchorsUsingKmeans()
{
	vector<vector<Size2f>> anchors;

	Scalar* colorTab = new Scalar[m_nOptionKCount];

	for (int i = 0; i < m_nOptionKCount; i++)
	{
		colorTab[i] = Scalar(rand() % 200 + 55, rand() % 200 + 55, rand() % 200 + 55);
	}

	vector<Size2f>* vecLabelSizes = new vector<Size2f>[m_nClassCount];


	for (int i = 0; i < m_vecDataSetItems.size(); i++)
	{
		vector<StLabelUnit>& labels = m_vecDataSetItems[i].vecLabels;
		for (int j = 0; j < labels.size(); j++)
		{
			if (labels[j].nStyle == LABEL_UNIT_STYLE_BOX)
			{
				int nWidth, nHeight;
				nWidth = labels[j].vPts[BOX_LABEL_PT_POS_RB].ptCoord.x - labels[j].vPts[BOX_LABEL_PT_POS_LT].ptCoord.x;
				nHeight = labels[j].vPts[BOX_LABEL_PT_POS_RB].ptCoord.y - labels[j].vPts[BOX_LABEL_PT_POS_LT].ptCoord.y;
				vecLabelSizes[labels[j].nClass].push_back(Size(nWidth, nHeight));
			}
		}
	}

	int nViewSize = 500;

	for (int c = 0; c < m_nClassCount; c++)
	{
		vector<Size2f> anchorsClass;

		Mat img(nViewSize, nViewSize, CV_8UC3);

		int nMinX = 99999, nMinY = 99999, nMaxX = 0, nMaxY = 0;
		int nW = 0, nH = 0;

		int k, clusterCount = m_nOptionKCount;
		int i, sampleCount = vecLabelSizes[c].size();
		Mat_<float> points(sampleCount, 2);
		Mat labels;
		clusterCount = MIN(clusterCount, sampleCount);

		Mat centers;

		for (k = 0; k < sampleCount; k++)
		{
			points(k, 0) = vecLabelSizes[c][k].width;
			points(k, 1) = vecLabelSizes[c][k].height;

			nMinX = MIN(nMinX, vecLabelSizes[c][k].width);
			nMinY = MIN(nMinY, vecLabelSizes[c][k].height);
			nMaxX = MAX(nMaxX, vecLabelSizes[c][k].width);
			nMaxY = MAX(nMaxY, vecLabelSizes[c][k].height);
		}
		//Border
		nMinX = MAX(0, nMinX - 20);
		nMinY = MAX(0, nMinY - 20);
		nMaxX = MIN(nViewSize, nMaxX + 20);
		nMaxY = MIN(nViewSize, nMaxY + 20);

		nW = nMaxX - nMinX;
		nH = nMaxY - nMinY;

		while (1)
		{
			kmeans(points, clusterCount, labels, TermCriteria(TermCriteria::EPS + TermCriteria::COUNT, 10, 1.0), 3, KMEANS_PP_CENTERS, centers);
			img = Scalar::all(255);


			for (i = 0; i < sampleCount; i++)
			{
				int clusterIdx = labels.at<int>(i);
				Point2f ipt(
					(points(i, 0) - nMinX) * ((float)nViewSize / nW),
					(points(i, 1) - nMinY) * ((float)nViewSize / nH)
				);

				circle(img, ipt, 2, colorTab[clusterIdx]);
			}

			for (i = 0; i < centers.rows; i++)
			{
				Point2f ipt = Point2f(
					(centers.ptr<float>(i)[0] - nMinX) * ((float)nViewSize / nW),
					(centers.ptr<float>(i)[1] - nMinY) * ((float)nViewSize / nH)
				);
				circle(img, ipt, 2, colorTab[i], 2);

				String strPt = to_string((int)centers.ptr<float>(i)[0]) + "," + to_string((int)centers.ptr<float>(i)[1]);
				putText(img, strPt, Point(ipt.x + 5, ipt.y + 5), 0, 0.5, colorTab[i], 1);

				Size2f sz(centers.ptr<float>(i)[0], centers.ptr<float>(i)[1]);
				anchorsClass.push_back(sz);
			}

			String strK = "k=" + to_string(clusterCount);

			putText(img, "W", Point(nViewSize / 2, 30), 0, 0.5, Scalar(255, 0, 0), 1);
			putText(img, "H", Point(30, nViewSize / 2), 0, 0.5, Scalar(255, 0, 0), 1);

			putText(img, strK, Point(30, 30), 0, 0.5, Scalar(0, 0, 255), 1);

			putText(img, "\'q\' = done", Point(nViewSize - 80, 30), 0, 0.4, Scalar(0, 255, 0), 1);
			putText(img, "\'i\'  = inc.", Point(nViewSize - 80, 45), 0, 0.4, Scalar(0, 255, 0), 1);
			putText(img, "\'d\' = dec.", Point(nViewSize - 80, 60), 0, 0.4, Scalar(0, 255, 0), 1);

			imshow("clusters", img);

			char c = waitKey(0);

			if (c == 'q')
			{
				break;
			}
			else if (c == 'd')
			{
				clusterCount--;
				if (clusterCount < 1)
					clusterCount = 1;
			}
			else if (c == 'i')
			{
				clusterCount++;
				if (clusterCount > sampleCount)
					clusterCount = sampleCount;
			}
		}

		destroyAllWindows();

		anchors.push_back(anchorsClass);
	}

	delete[] vecLabelSizes;

	return anchors;
}

/// <summary>
/// 이중 vector 형식의 anchors 정보를 strPath에 저장
/// </summary>
/// <param name="anchors"></param>
/// <param name="strPath"></param>
/// <returns></returns>
int CDatasetMakerDlg::SaveAnchorsInfo(const vector<vector<Size2f>>& anchors, const CString strPath)
{
	if (anchors.size() == 0)
		return FALSE;

	ofstream ofs(strPath, ios::out);

	for (int i = 0; i < anchors.size(); i++)
	{
		string str;

		str += to_string(i);
		for (int j = 0; j < anchors[i].size(); j++)
		{
			str += " " + to_string((int)anchors[i][j].width) + "," + to_string((int)anchors[i][j].height);
		}
		str += "\n";

		ofs.write(str.c_str(), str.size());
	}

	ofs.close();

	return TRUE;
}

/// <summary>
/// Drawing을 위한 label 관련 button들의 Enable 상태를 갱신
/// </summary>
void CDatasetMakerDlg::UpdateLabelButtons()
{
	m_chkAddBoxLabel.SetCheck(FALSE);
	m_chkAddPolyLabel.SetCheck(FALSE);
	m_chkAddSkelLabel.SetCheck(FALSE);

	if (m_vecDataSetItems[m_nSourceIdx].vecLabels.size() == 0)
	{
		m_btnDelLabel.EnableWindow(FALSE);
		m_btnEditLabel.EnableWindow(FALSE);
	}
	else
	{
		m_btnDelLabel.EnableWindow(TRUE);
		m_btnEditLabel.EnableWindow(TRUE);
	}

	UpdateToolBtnImg();
}

/// <summary>
/// Label list 항목들을 list control과 display에 갱신
/// </summary>
void CDatasetMakerDlg::UpdateLabelList()
{
	m_lstLabels.DeleteAllItems();

	if (m_vecDataSetItems.empty())
		return;

	vector<StLabelUnit>& labels = m_vecDataSetItems[m_nSourceIdx].vecLabels;

	for (int i = 0; i < labels.size(); i++)
	{
		if (labels[i].nStyle == LABEL_UNIT_STYLE_BOX)
		{
			if (labels[i].vPts[BOX_LABEL_PT_POS_LT].ptCoord.x > labels[i].vPts[BOX_LABEL_PT_POS_RB].ptCoord.x)
			{
				int temp;
				temp = labels[i].vPts[BOX_LABEL_PT_POS_LT].ptCoord.x;
				labels[i].vPts[BOX_LABEL_PT_POS_LT].ptCoord.x = labels[i].vPts[BOX_LABEL_PT_POS_RB].ptCoord.x;
				labels[i].vPts[BOX_LABEL_PT_POS_RB].ptCoord.x = temp;
			}
			if (labels[i].vPts[BOX_LABEL_PT_POS_LT].ptCoord.y > labels[i].vPts[BOX_LABEL_PT_POS_RB].ptCoord.y)
			{
				int temp;
				temp = labels[i].vPts[BOX_LABEL_PT_POS_LT].ptCoord.y;
				labels[i].vPts[BOX_LABEL_PT_POS_LT].ptCoord.y = labels[i].vPts[BOX_LABEL_PT_POS_RB].ptCoord.y;
				labels[i].vPts[BOX_LABEL_PT_POS_RB].ptCoord.y = temp;
			}
		}

		CString str[3];
		str[0].Format(_T("%02d"), i);
		if (labels[i].nStyle == LABEL_UNIT_STYLE_BOX)
			str[1] = _T("BOX");
		else if (labels[i].nStyle == LABEL_UNIT_STYLE_BOX)
			str[1] = _T("POLYGON");
		else
			str[1] = _T("SKELETON");
		str[2] = m_vecClassNames[labels[i].nClass];

		m_lstLabels.InsertItem(i, str[0]);
		m_lstLabels.SetItemText(i, 1, str[1]);
		m_lstLabels.SetItemText(i, 2, str[2]);
	}

	m_dispMainImage.ReDrawImage();

	if (m_nSelLabel < 0)
		m_nSelLabel = 0;
	else if (m_nSelLabel > labels.size() - 1)
		m_nSelLabel = labels.size() - 1;

	UpdateLabelButtons();
}

/// <summary>
/// Box 정보 rt를 현재 frame의 label list에 추가
/// </summary>
/// <param name="rt"></param>
void CDatasetMakerDlg::InsertBoxLabel(Rect rt)
{
	int nCnt = m_vecDataSetItems[m_nSourceIdx].vecLabels.size();

	StLabelUnit label;

	label.nStyle = LABEL_UNIT_STYLE_BOX;
	label.nClass = m_nSelClass;
	label.vPts.push_back({ true, Point(rt.x, rt.y) });
	label.vPts.push_back({ true, Point(rt.x + rt.width, rt.y + rt.height) });

	CString str[3];
	str[0].Format(_T("%02d"), nCnt);
	str[1] = _T("BOX");
	str[2] = m_vecClassNames[label.nClass];

	m_lstLabels.InsertItem(nCnt, str[0]);
	m_lstLabels.SetItemText(nCnt, 1, str[1]);
	m_lstLabels.SetItemText(nCnt, 2, str[2]);

	if (m_stMultiDrawing.bUse)
	{
		int nStart, nEnd;
		if (m_stMultiDrawing.nDir == -1)
		{
			nStart = m_nSourceIdx - m_stMultiDrawing.nCnt;
			if (nStart <= 0)	nStart = 0;
			nEnd = m_nSourceIdx;
		}
		else if (m_stMultiDrawing.nDir == 1)
		{
			nStart = m_nSourceIdx;
			nEnd = m_nSourceIdx + m_stMultiDrawing.nCnt;
			if (nEnd > m_vecDataSetItems.size() - 1)	nEnd = m_vecDataSetItems.size() - 1;
		}
		else
		{
			nStart = m_nSourceIdx - m_stMultiDrawing.nCnt;
			if (nStart <= 0)	nStart = 0;
			nEnd = m_nSourceIdx + m_stMultiDrawing.nCnt;
			if (nEnd > m_vecDataSetItems.size() - 1)	nEnd = m_vecDataSetItems.size() - 1;
		}

		for (int i = nStart; i <= nEnd; i++)
		{
			if (i == m_nSourceIdx)
				continue;

			if (m_vecDataSetItems[i].nSrcType != m_vecDataSetItems[m_nSourceIdx].nSrcType || m_vecDataSetItems[i].strPath != m_vecDataSetItems[m_nSourceIdx].strPath)
				continue;

			m_vecDataSetItems[i].vecLabels.push_back(label);
		}
	}

	m_vecDataSetItems[m_nSourceIdx].vecLabels.push_back(label);

	//SelectLabel(nCnt - 1);

	UpdateLabelButtons();
}

/// <summary>
/// Polygon area 정보 poly를 현재 frame의 label list에 추가
/// </summary>
/// <param name="poly"></param>
void CDatasetMakerDlg::InsertPolyLabel(vector<StPolyVertex>& poly)
{
	int nCnt = m_vecDataSetItems[m_nSourceIdx].vecLabels.size();

	StLabelUnit label;

	label.nStyle = LABEL_UNIT_STYLE_POLY;
	label.nClass = m_nSelClass;
	label.vPts = poly;

	CString str[3];
	str[0].Format(_T("%02d"), nCnt);
	str[1] = _T("POLYGON");
	str[2] = m_vecClassNames[label.nClass];

	m_lstLabels.InsertItem(nCnt, str[0]);
	m_lstLabels.SetItemText(nCnt, 1, str[1]);
	m_lstLabels.SetItemText(nCnt, 2, str[2]);

	if (m_stMultiDrawing.bUse)
	{
		int nStart, nEnd;
		if (m_stMultiDrawing.nDir == -1)
		{
			nStart = m_nSourceIdx - m_stMultiDrawing.nCnt;
			if (nStart <= 0)	nStart = 0;
			nEnd = m_nSourceIdx;
		}
		else if (m_stMultiDrawing.nDir == 1)
		{
			nStart = m_nSourceIdx;
			nEnd = m_nSourceIdx + m_stMultiDrawing.nCnt;
			if (nEnd > m_vecDataSetItems.size() - 1)	nEnd = m_vecDataSetItems.size() - 1;
		}
		else
		{
			nStart = m_nSourceIdx - m_stMultiDrawing.nCnt;
			if (nStart <= 0)	nStart = 0;
			nEnd = m_nSourceIdx + m_stMultiDrawing.nCnt;
			if (nEnd > m_vecDataSetItems.size() - 1)	nEnd = m_vecDataSetItems.size() - 1;
		}

		for (int i = nStart; i <= nEnd; i++)
		{
			if (i == m_nSourceIdx)
				continue;

			if (m_vecDataSetItems[i].nSrcType != m_vecDataSetItems[m_nSourceIdx].nSrcType || m_vecDataSetItems[i].strPath != m_vecDataSetItems[m_nSourceIdx].strPath)
				continue;

			m_vecDataSetItems[i].vecLabels.push_back(label);
		}
	}

	m_vecDataSetItems[m_nSourceIdx].vecLabels.push_back(label);

	//SelectLabel(nCnt - 1);

	UpdateLabelButtons();
}

/// <summary>
/// Skeleton 정보 skel를 현재 frame의 label list에 추가
/// </summary>
/// <param name="skel"></param>
void CDatasetMakerDlg::InsertSkelLabel(vector<StPolyVertex>& skel)
{
	int nCnt = m_vecDataSetItems[m_nSourceIdx].vecLabels.size();

	StLabelUnit label;

	label.nStyle = LABEL_UNIT_STYLE_SKEL;
	label.nClass = m_nSelClass;
	label.vPts = skel;

	CString str[3];
	str[0].Format(_T("%02d"), nCnt);
	str[1] = _T("SKELETON");
	str[2] = m_vecClassNames[label.nClass];

	m_lstLabels.InsertItem(nCnt, str[0]);
	m_lstLabels.SetItemText(nCnt, 1, str[1]);
	m_lstLabels.SetItemText(nCnt, 2, str[2]);

	if (m_stMultiDrawing.bUse)
	{
		int nStart, nEnd;
		if (m_stMultiDrawing.nDir == -1)
		{
			nStart = m_nSourceIdx - m_stMultiDrawing.nCnt;
			if (nStart <= 0)	nStart = 0;
			nEnd = m_nSourceIdx;
		}
		else if (m_stMultiDrawing.nDir == 1)
		{
			nStart = m_nSourceIdx;
			nEnd = m_nSourceIdx + m_stMultiDrawing.nCnt;
			if (nEnd > m_vecDataSetItems.size() - 1)	nEnd = m_vecDataSetItems.size() - 1;
		}
		else
		{
			nStart = m_nSourceIdx - m_stMultiDrawing.nCnt;
			if (nStart <= 0)	nStart = 0;
			nEnd = m_nSourceIdx + m_stMultiDrawing.nCnt;
			if (nEnd > m_vecDataSetItems.size() - 1)	nEnd = m_vecDataSetItems.size() - 1;
		}

		for (int i = nStart; i <= nEnd; i++)
		{
			if (i == m_nSourceIdx)
				continue;

			if (m_vecDataSetItems[i].nSrcType != m_vecDataSetItems[m_nSourceIdx].nSrcType || m_vecDataSetItems[i].strPath != m_vecDataSetItems[m_nSourceIdx].strPath)
				continue;

			m_vecDataSetItems[i].vecLabels.push_back(label);
		}
	}

	m_vecDataSetItems[m_nSourceIdx].vecLabels.push_back(label);

	//SelectLabel(nCnt - 1);

	UpdateLabelButtons();
}

/// <summary>
/// nIdx번째 label을 list control에서 선택하고 현재 선택된 label로 지정
/// </summary>
/// <param name="nIdx"></param>
void CDatasetMakerDlg::SelectLabel(int nIdx)
{
	if (m_nSelLabel == nIdx)
	{
		return;
	}

	m_nSelLabel = nIdx;

	m_lstLabels.SetItemState(-1, 0, LVIS_SELECTED | LVIS_FOCUSED);
	m_lstLabels.SetItemState(nIdx, LVIS_FOCUSED | LVIS_SELECTED, LVIS_FOCUSED | LVIS_SELECTED);
	m_lstLabels.EnsureVisible(nIdx, FALSE);
	m_lstLabels.SetFocus();
}

/// <summary>
/// 현재 Class list의 정보를 list control에 갱신
/// </summary>
void CDatasetMakerDlg::UpdateClassCount()
{
	m_nClassCount = m_vecClassNames.size();

	m_lstClasses.ResetContent();

	for (int i = 0; i < m_vecClassNames.size(); i++)
	{
		CString str;
		m_lstClasses.AddString(m_vecClassNames[i]);
	}

	UpdateClassInfo();
	UpdateData(FALSE);
}

/// <summary>
/// 현재 선택된 Class의 정보로 drawing할 class 정보를 갱신
/// </summary>
void CDatasetMakerDlg::UpdateClassInfo()
{
	if (m_vecClassNames.size() == 0)
		return;

	if (m_vecClassNames.size() - 1 < m_nSelClass)
	{
		m_nSelClass = m_vecClassNames.size() - 1;
	}

	m_btnBrushColor.EnableWindowsTheming(FALSE);
	m_btnBrushColor.SetFaceColor(RGB(g_ColorModel[m_nSelClass].r, g_ColorModel[m_nSelClass].g, g_ColorModel[m_nSelClass].b));

	GetDlgItem(IDC_LABEL_CLASS_NAME)->SetWindowText(m_vecClassNames[m_nSelClass]);

	m_dispMainImage.ReDrawImage();
}

/// <summary>
/// 현재 선택된 Label list의 항목을 전달된 매개변수 label로 변경
/// </summary>
/// <param name="label"></param>
void CDatasetMakerDlg::SetCurLabel(StLabelUnit label)
{
	vector<StLabelUnit>& labels = m_vecDataSetItems[m_nSourceIdx].vecLabels;
	
	labels[m_nSelLabel] = label;

	UpdateLabelList();
}

/// <summary>
/// Progress 창의 진행상태를 0으로 초기화하고 창을 표시
/// </summary>
void CDatasetMakerDlg::StartProgress()
{
	m_dlgProgress->SetLoadingStatus(0);
	m_dlgProgress->ShowWindow(SW_SHOW);
}

/// <summary>
/// Pregress 창의 진행상태를 nPos로 설정하고 strMsg를 표시
/// </summary>
/// <param name="nPos"></param>
/// <param name="strMsg"></param>
void CDatasetMakerDlg::SetProgress(int nPos, CString strMsg)
{
	m_dlgProgress->SetLoadingStatus(nPos, strMsg);
}

/// <summary>
/// Pregress 창의 진행상태를 100으로 완료하고 창을 숨김
/// </summary>
void CDatasetMakerDlg::EndProgress()
{
	m_dlgProgress->SetLoadingStatus(100);
	m_dlgProgress->ShowWindow(SW_HIDE);
}



void CDatasetMakerDlg::OnBnClickedBtnImportSkeleton()
{
	static TCHAR BASED_CODE szFilter[] = _T("Image file(*.json)|*.JSON;*.json |모든파일(*.*)|*.*||");

	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, szFilter);

	if (dlg.DoModal() == IDOK)
	{
		CString pathJson, pathFolder;
		pathJson = dlg.GetPathName();
		pathFolder = dlg.GetFolderPath();

		if (LoadSkeletonsCOCOJson(pathJson, pathFolder))
			AfxMessageBox(_T("COCO skeleton dataset을 성공적으로 불러왔습니다."));
		else
			AfxMessageBox(_T("COCO skeleton dataset을 불러오는 과정에서 오류가 발생하였습니다."));
	}
}


void CDatasetMakerDlg::OnBnClickedBtnExportSkeleton()
{
	CFolderPickerDialog dlg;

	if (dlg.DoModal() == IDOK)
	{
		m_strPathResult = dlg.GetPathName();

		bool bFlag = false;
		for (int i = 0; i < m_lstSourceImages.GetItemCount(); i++)
		{
			if (m_lstSourceImages.GetCheck(i))
				bFlag = true;
		}
		if (!bFlag)
		{
			AfxMessageBox(_T("선택된 영상이 없습니다."));
			return;
		}

		if (SaveSkeletonsCOCOJson(m_strPathResult))
		{
			AfxMessageBox(_T("Save 성공!"));
		}
		else
			AfxMessageBox(_T("Save 실패!"));
	}
}


void CDatasetMakerDlg::OnBnClickedBtnImportBbox()
{
	CFolderPickerDialog dlg;

	if (dlg.DoModal() == IDOK)
	{
		m_strPathSource = dlg.GetPathName();

		if (LoadBBoxTxt(m_strPathSource))
			AfxMessageBox(_T("Bbox dataset을 성공적으로 불러왔습니다."));
		else
			AfxMessageBox(_T("Bbox dataset을 불러오는 과정에서 오류가 발생하였습니다."));
	}
}

void CDatasetMakerDlg::OnBnClickedBtnLoadPoseModel()
{
	static TCHAR BASED_CODE szFilter[] = _T("TRT Emgome(*.engine)|*.ENGINE;*.engine |모든파일(*.*)|*.*||");

	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, szFilter);

	if (dlg.DoModal() == IDOK)
	{
		CString pathModel;
		pathModel = dlg.GetPathName();

		if (LoadPoseEngine(pathModel) == 0)
			AfxMessageBox(_T("TensorRT Pose Model을 성공적으로 불러왔습니다."));
		else
			AfxMessageBox(_T("TensorRT Pose Model을 불러오는 과정에서 오류가 발생하였습니다."));
	}
}


int CDatasetMakerDlg::LoadPoseEngine(const CString strModelPath)
{
	int ret = -1;

	//DeepInfer Initailize
	_deepInfer = new DeepInfer();
	_deepInfer_ctx.width = 0;
	_deepInfer_ctx.height = 0;
	_deepInfer_ctx.estimatePath = std::string(CT2CA(strModelPath));
	ret = _deepInfer->poseInitialize(&_deepInfer_ctx);
	
	//TODO: 
	/*
	1) check init - release engine
	2) init engine
	3) load engine
	여기서 구분이 필요할듯 어떤 Network인지의 구분이 필요함... 이걸 자동으로 할 수 있는 방법이 있으려나... key 값이 Load할때 필요할거같은데
	다른거는 다 어느정도 자동으로 가능할거 같은데 후처리하는 부분에서는 어려울듯.
	yoloLoad 혹은 openposeLoad 형태로 가야하나.
	4) inference
	*/
	

	return ret;
}


