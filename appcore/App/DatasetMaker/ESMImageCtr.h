#pragma once
#include <afxwin.h>
#include "ESMCommonDefine.h"

class CESMImageCtr :
	public CStatic
{
public:
	CWnd* m_pParent;
	void SetParent(CWnd* pParent);

public:
	CESMImageCtr();
	~CESMImageCtr();

public:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnPaint();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);

private:
	int m_nCursorMode;			// Display cursor mode (enum DISP_CURSOR_MODE 참고)

	Mat m_matImage;				// 화면에 출력하는 영상 객체
	BITMAPINFO* m_pBitmapInfo;	// 출력을 위한 비트맵 정보

	void CreateBitmapInfo(int w, int h, int bpp); // Bitmap 정보를 생성

	bool m_bLBtnDown;			// Mouse left button down flag
	bool m_bRBtnDown;			// Mouse right button down flag
	bool m_bPolyDrawing;		// Polygon drawing end flag

	int m_nImgWidth;			// 출력 Image의 Width
	int m_nImgHeight;			// 출력 Image의 Height

	double m_dOffsetX;			// Display에 표시할 영상의 현재 Offset X
	double m_dOffsetY;			// Display에 표시할 영상의 현재 Offset Y
	double m_dZoomFactor;		// Display에 표시할 영상의 현재 Zoom factor
	double m_dMinZoomFactor;	// Display에 표시할 영상의 축소가능한 최소 Zoom factor

	double m_dBoxRatio;			// Box drawing시 고정시킬 x/y 비율

	int m_nIdxSkel;				// Drawing 중인 Skeleton 관절 Index
	
	CRect m_rtCtrl;				// Dispaly control의 CRect 정보

	CPoint m_ptCursor;			// 현재 커서의 CPoint 정보

	CPoint m_ptStartROI;		// Box drawing시 mouse down(start) 위치의 CPoint 정보
	cv::Rect m_rtROI;			// Box drawing시 start위치에서 현재 위치까지의 Rect 정보
	vector<StPolyVertex> m_vecPolyLines;	// Polygon drawing시 추가된 좌표 vector

	CString m_strSceneTitle;			// 화면에 표시할 Scene title 정보
	vector<StLabelUnit> *m_pLabels;		// 현재 Label list의 vector pointer
	int m_nSelLabel;					// 현재 선택된 Label의 index 값
	StMouseOnStatus m_stMouseOnStatus;	// id, style, 8mode(0:LT, 1:RT, 2:RB, 3:LB, 4:T, 5:R, 6:B, 7:L, 8:ALL) 
	
public:
	// 영상 로드 여부 확인
	bool IsLoaded() { return !m_matImage.empty(); }
	void SetCursorMode(int nMode);

	void SetImage(Mat img);

	void SetSceneTitle(CString str);
	// vector pointer 설정
	void SetLabels(vector<StLabelUnit>* pLabels) { m_pLabels = pLabels; }
	void SelectLabel(int nIdx);
	StMouseOnStatus FindNearestLabel(Point2f pt);
	void ResizeLabel(int nMode, float dx, float dy);
	void MovePolyLabelVertex(int nIdx, float dx, float dy);

	void ReDrawImage() { Invalidate(FALSE); };
	
	void SetBoxRatio(double dRatio);

	void DrawImage(CPaintDC& dc);
	void DrawImage(CClientDC& dc);
	void DrawROI(CPaintDC &dc);
	void DrawPoly(CPaintDC& dc);
	void DrawSkeleton(CPaintDC& dc);
	void DrawLabels(CPaintDC& dc);
	void DrawSceneTitle(CPaintDC& dc);

	void Addvertex();
	void Deletevertex();

	double dist(Point& p1, Point& p2);
	bool PointInPolygon(Point& point, vector<Point>& vecPoly);
};

