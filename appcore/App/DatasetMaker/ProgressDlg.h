﻿#pragma once


// CProgressDlg 대화 상자

class CProgressDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CProgressDlg)

public:
	CProgressDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CProgressDlg();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOGBAR_PROGRESS };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
	CWnd* m_pParent;

	virtual BOOL OnInitDialog();

	CProgressCtrl m_ctrlProgress;

	void SetLoadingStatus(int nPos, CString strMsg = _T(""));
	afx_msg void OnNcDestroy();
	CStatic m_lblLoadingStatus;
};
