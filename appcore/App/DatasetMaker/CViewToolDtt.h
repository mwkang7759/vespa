﻿#pragma once



// CViewToolDtt 폼 보기

class CViewToolDtt : public CFormView
{
	DECLARE_DYNCREATE(CViewToolDtt)

protected:
	CViewToolDtt();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CViewToolDtt();

public:
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_VIEW_TOOL_DTT };
#endif
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
	CWnd* m_pParent;
	void SetParent(CWnd* pParent);

	CSliderCtrl m_sliClassId;
	CButton m_chkInsertLabel;
	CListCtrl m_lstLabels;

	void SetSliderSize(int nSize);
	void SetSelClassId(int nId);
	int GetSelIdx();
	int GetSelClassId();
	void UpdateLabelList(vector<LabelUnit>& labels);
	void InsertLabel(LabelUnit& label, int nCnt);
	void SelectLabel(int idx);
	void UpdateClassName(CString strName);

	afx_msg void OnLvnItemchangedListLabels(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg	void OnBnClickedChkInsertLabel();
	afx_msg void OnBnClickedBtnDeleteLabel();
	afx_msg void OnBnClickedBtnEditLabel();
	afx_msg void OnBnClickedBtnTrackLabel();
	afx_msg void OnBnClickedBtnClearLabels();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);

	void SetCheckInsertBtn(bool bChk) { m_chkInsertLabel.SetCheck(bChk); }
};


