#pragma once

#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>
#include <vector>
#include <atomic>
#include <io.h>
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/ostreamwrapper.h"
#include <cstdio>
#pragma warning(disable: 4996)

#include "TraceAPI.h"

#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/cudaimgproc.hpp"
#include "opencv2/cudawarping.hpp"
#include "opencv2/cudaarithm.hpp"

#if defined(_DEBUG)
#pragma comment(lib, "opencv_core440d.lib")
#pragma comment(lib, "opencv_highgui440d.lib")
#pragma comment(lib, "opencv_imgproc440d.lib")
#pragma comment(lib, "opencv_imgcodecs440d.lib")
#pragma comment(lib, "opencv_calib3d440d.lib")
#pragma comment(lib, "opencv_video440d.lib")
#pragma comment(lib, "opencv_videoio440d.lib")
#else
#pragma comment(lib, "opencv_core440.lib")
#pragma comment(lib, "opencv_highgui440.lib")
#pragma comment(lib, "opencv_imgproc440.lib")
#pragma comment(lib, "opencv_imgcodecs440.lib")
#pragma comment(lib, "opencv_calib3d440.lib")
#pragma comment(lib, "opencv_video440.lib")
#pragma comment(lib, "opencv_videoio440.lib")
#endif

#define MAX_CLASS_COUNT		100			// 최대 클래스 개수
#define MAX_RESIZE_MODE		9			// Label 수정을 위한 편집 모드의 개수
#define MAX_SKELETON_COUNT	17			// 4D Style skeleton model의 vertex 개수
#define MAX_LINK_COUNT		19			// 4D Style skeleton model의 link 개수

using namespace cv;
using namespace std;
using namespace rapidjson;

// 추가된 소스 영상의 종류
enum SRC_TYPE {
	SRC_TYPE_STILL,	// Still image
	SRC_TYPE_VIDEO	// Video image (frame 정보 포함)
};

// 불러올 소스 path의 종류
enum MODE_SRC {
	MODE_SRC_VIDEO,	// Video file
	MODE_SRC_FOLDER	// Folder images
};

// Display 창의 cursor mode
enum DISP_CURSOR_MODE {
	DISP_CURSOR_MODE_MOVE_LABEL,		// 영상 탐색 및 이동 모드
	DISP_CURSOR_MODE_INSERT_BOX_LABEL,	// Box label 추가 모드
	DISP_CURSOR_MODE_INSERT_POLY_LABEL,	// Polygon label 추가 모드
	DISP_CURSOR_MODE_INSERT_SKEL_LABEL	// Skeleton label 추가 모드
};

// Box label의 편집모드 (9개의 모드, 시계방향)
enum ROI_RESIZE_MODE
{
	ROI_RESIZE_LT,	// 좌상
	ROI_RESIZE_RT,	// 우상
	ROI_RESIZE_RB,	// 우하
	ROI_RESIZE_LB,	// 좌하
	ROI_RESIZE_T,	// 상
	ROI_RESIZE_R,	// 후
	ROI_RESIZE_B,	// 하
	ROI_RESIZE_L,	// 좌
	ROI_RESIZE_ALL	// 박스안
};

// Label의 형태
enum LABEL_UNIT_STYLE
{
	LABEL_UNIT_STYLE_BOX,	// Box style
	LABEL_UNIT_STYLE_POLY,	// Polygon style
	LABEL_UNIT_STYLE_SKEL	// Skeleton style
};

// Box label의 vertex 구성
enum BOX_LABEL_PT_POS
{
	BOX_LABEL_PT_POS_LT,	// 좌상
	BOX_LABEL_PT_POS_RB		// 우하
};

// 현재 커서 위치에서의 라벨 선택 정보
typedef struct {
	int nType;	// LABEL_UNIT_STYLE, 선택된 라벨의 종류
	int nIdx;	// 선택된 Label index
	int nPos;	// 선택된 Label의 편집모드 또는 vertex number
} StMouseOnStatus;

// Label의 각 Vertex 정보
typedef struct {
	bool bVis;		// Visible 여부
	Point ptCoord;	// 좌표
} StPolyVertex;

// Label의 정보
typedef struct {
	int nClass;		// Label의 Class
	int nStyle;		// Label의 Style
	vector<StPolyVertex> vPts;	// Label의 Vertex
} StLabelUnit;

// Frame의 정보
typedef struct {
	MODE_SRC nSrcType;	// 로드된 Frame의 종류
	CString strPath;	// 해당 영상의 경로
	int nWidth;			// 영상 가로 크기
	int nHeight;		// 영상 세로 크기
	int nFrmIdx;		// 영상의 Frame index, Still image일 경우 -1
} StFrameInfo;

// 각 프레임에 대한 데이타셋 정보
typedef struct {
	bool bSelected;			// Export 대상 선택 여부
	int nId;				// Export시 Index
	SRC_TYPE nSrcType;		// 추가된 Frame의 종류 (Still / Video)
	CString strPath;		// 영상 Path
	int nWidth;				// 영상 가로 크기
	int nHeight;			// 영상 세로 크기
	int nFrmIdx;			// 영상의 Frame index, Still image일 경우 -1
	CString strSceneTitle;	// Scene class

	vector<StLabelUnit> vecLabels;	// 프레임의 라벨 정보
} StDataSetItem;

// Class model
typedef struct {
	int r;			// Class color (Red)
	int g;			// Class color (Green)
	int b;			// Class color (Blue)
} StClassModel;

// Multi drawing 정보
typedef struct {
	bool bUse;	// Multi drawing 여부
	int nDir;	// -1 : 전프레임, 0 : 전후프레임, 1 : 후프레임
	int nCnt;	// Multi drawing frame수
} StMultiDrawing;