﻿// CViewToolSegm.cpp: 구현 파일
//

#include "pch.h"
#include "DatasetMaker.h"
#include "DatasetMakerDlg.h"
#include "CViewToolSegm.h"


// CViewToolSegm

IMPLEMENT_DYNCREATE(CViewToolSegm, CFormView)

CViewToolSegm::CViewToolSegm()
	: CFormView(IDD_VIEW_TOOL_SEGM)
{

}

CViewToolSegm::~CViewToolSegm()
{
}

void CViewToolSegm::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BTN_CLASS_COLOR, m_btnBrushColor);
	DDX_Control(pDX, IDC_RB_BRUSH_SIZE_SMALL, m_rbBrushSizeSmall);
	DDX_Control(pDX, IDC_RB_BRUSH_SIZE_MEDIUM, m_rbBrushSizeMedium);
	DDX_Control(pDX, IDC_RB_BRUSH_SIZE_LARGE, m_rbBrushSizeLarge);
	DDX_Control(pDX, IDC_RB_DRAW_TOOL_PEN, m_rbDrawToolPen);
	DDX_Control(pDX, IDC_RB_DRAW_TOOL_WAND, m_rbDrawToolWand);
}

BEGIN_MESSAGE_MAP(CViewToolSegm, CFormView)
	ON_BN_CLICKED(IDC_RB_BRUSH_SIZE_SMALL, &CViewToolSegm::OnBnClickedRbBrushSizeSmall)
	ON_BN_CLICKED(IDC_RB_BRUSH_SIZE_MEDIUM, &CViewToolSegm::OnBnClickedRbBrushSizeMedium)
	ON_BN_CLICKED(IDC_RB_BRUSH_SIZE_LARGE, &CViewToolSegm::OnBnClickedRbBrushSizeLarge)
	ON_BN_CLICKED(IDC_BTN_LABEL_RESET, &CViewToolSegm::OnBnClickedBtnLabelReset)
	ON_BN_CLICKED(IDC_RB_DRAW_TOOL_PEN, &CViewToolSegm::OnBnClickedRbDrawToolPen)
	ON_BN_CLICKED(IDC_RB_DRAW_TOOL_WAND, &CViewToolSegm::OnBnClickedRbDrawToolWand)
END_MESSAGE_MAP()


// CViewToolSegm 진단

#ifdef _DEBUG
void CViewToolSegm::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CViewToolSegm::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CViewToolSegm 메시지 처리기



void CViewToolSegm::SetParent(CWnd* pParent)
{
	m_pParent = pParent;

	m_rbDrawToolPen.SetCheck(TRUE);
	m_rbBrushSizeSmall.SetCheck(TRUE);
}


void CViewToolSegm::OnBnClickedRbBrushSizeSmall()
{
	//((CDatasetMakerDlg*)m_pParent)->SetBrush(1);
}


void CViewToolSegm::OnBnClickedRbBrushSizeMedium()
{
	//((CDatasetMakerDlg*)m_pParent)->SetBrush(3);
}


void CViewToolSegm::OnBnClickedRbBrushSizeLarge()
{
	//((CDatasetMakerDlg*)m_pParent)->SetBrush(5);
}


void CViewToolSegm::OnBnClickedBtnLabelReset()
{
	//((CDatasetMakerDlg*)m_pParent)->ResetLabel();
}


void CViewToolSegm::OnBnClickedRbDrawToolPen()
{
	//((CDatasetMakerDlg*)m_pParent)->SetToolMode(DISP_SGMT_TOOLMODE_PEN);
}


void CViewToolSegm::OnBnClickedRbDrawToolWand()
{
	//((CDatasetMakerDlg*)m_pParent)->SetToolMode(DISP_SGMT_TOOLMODE_WAND);
}

void CViewToolSegm::UpdateClassName(CString strName)
{
	GetDlgItem(IDC_LABEL_CLASS_NAME2)->SetWindowText(strName);
}
