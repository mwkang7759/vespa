﻿// ProgressDlg.cpp: 구현 파일
//

#include "pch.h"
#include "DatasetMaker.h"
#include "ProgressDlg.h"
#include "afxdialogex.h"


// CProgressDlg 대화 상자

IMPLEMENT_DYNAMIC(CProgressDlg, CDialogEx)

CProgressDlg::CProgressDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIALOGBAR_PROGRESS, pParent)
{
	m_pParent = pParent;
}

CProgressDlg::~CProgressDlg()
{
}

void CProgressDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROGRESS_PROC, m_ctrlProgress);
	DDX_Control(pDX, IDC_LABEL_LOADING_STATUS, m_lblLoadingStatus);
}


BEGIN_MESSAGE_MAP(CProgressDlg, CDialogEx)
	ON_WM_NCDESTROY()
END_MESSAGE_MAP()


// CProgressDlg 메시지 처리기



BOOL CProgressDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_ctrlProgress.SetRange(0, 100);
	m_ctrlProgress.SetPos(0);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CProgressDlg::SetLoadingStatus(int nPos, CString strMsg)
{
	m_ctrlProgress.SetPos(nPos);
	m_lblLoadingStatus.SetWindowText(strMsg);

	if (nPos >= 100)
		CDialogEx::OnCancel();
}


void CProgressDlg::OnNcDestroy()
{
	//CDialogEx::OnNcDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	delete this;
}
