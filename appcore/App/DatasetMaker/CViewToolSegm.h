﻿#pragma once



// CViewToolSegm 폼 보기

class CViewToolSegm : public CFormView
{
	DECLARE_DYNCREATE(CViewToolSegm)

protected:
	CViewToolSegm();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CViewToolSegm();

public:
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_VIEW_TOOL_SEGM };
#endif
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
	CWnd* m_pParent;
	void SetParent(CWnd* pParent);

	CMFCButton m_btnBrushColor;
	CButton m_rbBrushSizeSmall;
	CButton m_rbBrushSizeMedium;
	CButton m_rbBrushSizeLarge;
	CButton m_rbDrawToolPen;
	CButton m_rbDrawToolWand;

	afx_msg void OnBnClickedRbBrushSizeSmall();
	afx_msg void OnBnClickedRbBrushSizeMedium();
	afx_msg void OnBnClickedRbBrushSizeLarge();
	afx_msg void OnBnClickedBtnLabelReset();
	afx_msg void OnBnClickedRbDrawToolPen();
	afx_msg void OnBnClickedRbDrawToolWand();

	void UpdateClassName(CString strName);
};


