﻿#pragma once


// CMultiSelectDlg 대화 상자

class CMultiDrawingDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CMultiDrawingDlg)

public:
	CMultiDrawingDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CMultiDrawingDlg();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DLG_MULTI_DRAWING };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
	CWnd* m_pParent;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();

	CComboBox m_cbMultiDirection;
	CEdit m_editMultiCount;

	int* m_pDir;
	int* m_pCnt;
};
