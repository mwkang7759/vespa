﻿#pragma once

#include "ESMCommonDefine.h"

// CSelectLabelDlg 대화 상자

class CSelectClassDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CSelectClassDlg)

public:
	CSelectClassDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CSelectClassDlg();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DLG_SELECT_LABEL };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CWnd* m_pParent;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	
	int m_nSelIdx;
	StClassModel* m_pClassColors;
	vector<CString> *m_pClassNames;
	int* m_pSelClass;
	CMFCButton m_btnColor;
	CListBox m_lstSelectLabel;

	void SetClassColors(StClassModel* pColors);
	void SetClassNames(vector<CString>* pNames);
	afx_msg void OnBnClickedBtnSelectColor();
	afx_msg void OnLbnSelchangeListSelectClass();
};
