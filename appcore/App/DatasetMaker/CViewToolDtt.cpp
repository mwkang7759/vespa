﻿// CViewToolDtt.cpp: 구현 파일
//

#include "pch.h"
#include "DatasetMaker.h"
#include "DatasetMakerDlg.h"
#include "CViewToolDtt.h"

// CViewToolDtt

IMPLEMENT_DYNCREATE(CViewToolDtt, CFormView)

CViewToolDtt::CViewToolDtt()
	: CFormView(IDD_VIEW_TOOL_DTT)
{

}

CViewToolDtt::~CViewToolDtt()
{
}

void CViewToolDtt::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHK_INSERT_LABEL, m_chkInsertLabel);
	DDX_Control(pDX, IDC_LIST_LABELS, m_lstLabels);
	DDX_Control(pDX, IDC_SLIDER_CLASS_ID, m_sliClassId);
}

BEGIN_MESSAGE_MAP(CViewToolDtt, CFormView)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_LABELS, &CViewToolDtt::OnLvnItemchangedListLabels)
	ON_BN_CLICKED(IDC_CHK_INSERT_LABEL, &CViewToolDtt::OnBnClickedChkInsertLabel)
	ON_BN_CLICKED(IDC_BTN_DELETE_LABEL, &CViewToolDtt::OnBnClickedBtnDeleteLabel)
	ON_BN_CLICKED(IDC_BTN_EDIT_LABEL, &CViewToolDtt::OnBnClickedBtnEditLabel)
	ON_BN_CLICKED(IDC_BTN_TRACK_LABEL, &CViewToolDtt::OnBnClickedBtnTrackLabel)
	ON_BN_CLICKED(IDC_BTN_CLEAR_LABELS, &CViewToolDtt::OnBnClickedBtnClearLabels)
	ON_WM_HSCROLL()
END_MESSAGE_MAP()


// CViewToolDtt 진단

#ifdef _DEBUG
void CViewToolDtt::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CViewToolDtt::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CViewToolDtt 메시지 처리기



void CViewToolDtt::SetParent(CWnd* pParent)
{
	m_pParent = pParent;


	m_sliClassId.SetRange(0, 1);
	m_sliClassId.SetTicFreq(1);
	m_sliClassId.SetPos(0);
	m_sliClassId.EnableWindow(FALSE);

	m_lstLabels.InsertColumn(0, _T("Idx"), LVCFMT_LEFT, 30, -1);
	m_lstLabels.InsertColumn(1, _T("Class"), LVCFMT_CENTER, 50, -1);
	m_lstLabels.InsertColumn(2, _T("x"), LVCFMT_CENTER, 50, -1);
	m_lstLabels.InsertColumn(3, _T("y"), LVCFMT_CENTER, 50, -1);
	m_lstLabels.InsertColumn(4, _T("width"), LVCFMT_CENTER, 50, -1);
	m_lstLabels.InsertColumn(5, _T("height"), LVCFMT_CENTER, 50, -1);
	m_lstLabels.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
}

void CViewToolDtt::SetSliderSize(int nSize)
{
	if (nSize == 1)
	{
		m_sliClassId.EnableWindow(FALSE);
	}
	else
	{
		m_sliClassId.EnableWindow(TRUE);
		m_sliClassId.SetRangeMax(nSize - 1);
	}
}

void CViewToolDtt::SetSelClassId(int nId)
{
	m_sliClassId.SetPos(nId);
}

int CViewToolDtt::GetSelIdx()
{
	return m_lstLabels.GetNextItem(-1, LVNI_SELECTED);
}

int CViewToolDtt::GetSelClassId()
{
	return m_sliClassId.GetPos();
}

void CViewToolDtt::UpdateLabelList(vector<LabelUnit>& labels)
{
	m_lstLabels.DeleteAllItems();

	for (int i = 0; i < labels.size(); i++)
	{
		if (labels[i].vPts.width < 0)
		{
			labels[i].vPts.x += labels[i].vPts.width;
			labels[i].vPts.width = -labels[i].vPts.width;
		}
		if (labels[i].vPts.height < 0)
		{
			labels[i].vPts.y += labels[i].vPts.height;
			labels[i].vPts.height = -labels[i].vPts.height;
		}

		CString str[6];
		str[0].Format(_T("%02d"), i);
		str[1].Format(_T("%d"), labels[i].nClass);
		str[2].Format(_T("%d"), labels[i].vPts.x);
		str[3].Format(_T("%d"), labels[i].vPts.y);
		str[4].Format(_T("%d"), labels[i].vPts.width);
		str[5].Format(_T("%d"), labels[i].vPts.height);

		m_lstLabels.InsertItem(i, str[0]);
		m_lstLabels.SetItemText(i, 1, str[1]);
		m_lstLabels.SetItemText(i, 2, str[2]);
		m_lstLabels.SetItemText(i, 3, str[3]);
		m_lstLabels.SetItemText(i, 4, str[4]);
		m_lstLabels.SetItemText(i, 5, str[5]);
	}
}

void CViewToolDtt::InsertLabel(LabelUnit& label, int nCnt)
{
	CString str[6];
	str[0].Format(_T("%02d"), nCnt);
	str[1].Format(_T("%d"), label.nClass);
	str[2].Format(_T("%d"), label.vPts.x);
	str[3].Format(_T("%d"), label.vPts.y);
	str[4].Format(_T("%d"), label.vPts.width);
	str[5].Format(_T("%d"), label.vPts.height);

	m_lstLabels.InsertItem(nCnt, str[0]);
	m_lstLabels.SetItemText(nCnt, 1, str[1]);
	m_lstLabels.SetItemText(nCnt, 2, str[2]);
	m_lstLabels.SetItemText(nCnt, 3, str[3]);
	m_lstLabels.SetItemText(nCnt, 4, str[4]);
	m_lstLabels.SetItemText(nCnt, 5, str[5]);
}


void CViewToolDtt::SelectLabel(int idx)
{
	m_lstLabels.SetItemState(-1, 0, LVIS_SELECTED | LVIS_FOCUSED);
	m_lstLabels.SetItemState(idx, LVIS_FOCUSED | LVIS_SELECTED, LVIS_FOCUSED | LVIS_SELECTED);
	m_lstLabels.EnsureVisible(idx, FALSE);
	m_lstLabels.SetFocus();
}

void CViewToolDtt::UpdateClassName(CString strName)
{
	GetDlgItem(IDC_LABEL_CLASS_NAME)->SetWindowText(strName);
}

void CViewToolDtt::OnBnClickedChkInsertLabel()
{
	((CDatasetMakerDlg*)m_pParent)->OnBnClickedChkAddBoxLabel();
}

void CViewToolDtt::OnBnClickedBtnDeleteLabel()
{
	((CDatasetMakerDlg*)m_pParent)->OnBnClickedBtnDeleteLabel();
}

void CViewToolDtt::OnBnClickedBtnEditLabel()
{
	((CDatasetMakerDlg*)m_pParent)->OnBnClickedBtnEditLabel();
}


void CViewToolDtt::OnBnClickedBtnTrackLabel()
{
	((CDatasetMakerDlg*)m_pParent)->OnBnClickedBtnTrackLabel();
}


void CViewToolDtt::OnBnClickedBtnClearLabels()
{
	((CDatasetMakerDlg*)m_pParent)->OnBnClickedBtnClearLabels();
}

void CViewToolDtt::OnLvnItemchangedListLabels(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	((CDatasetMakerDlg *) m_pParent)->SelectLabel(pNMLV->iItem);

	*pResult = 0;
}


void CViewToolDtt::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	if (pScrollBar)
	{
		if (pScrollBar == (CScrollBar*)&m_sliClassId)
		{
			((CDatasetMakerDlg*)m_pParent)->UpdateClassName();
		}
	}
	else
	{
	}

	CView::OnHScroll(nSBCode, nPos, pScrollBar);
}