﻿#pragma once


// CEditRatioDlg 대화 상자

class CEditRatioDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CEditRatioDlg)

public:
	CEditRatioDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CEditRatioDlg();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DLG_EDIT_RATIO };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
	CWnd* m_pParent;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();

	CComboBox m_cbNewRatioX;
	CComboBox m_cbNewRatioY;

	int* m_pRatioX;
	int* m_pRatioY;
};
