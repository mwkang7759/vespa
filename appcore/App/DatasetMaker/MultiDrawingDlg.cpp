﻿// MultiSelectDlg.cpp: 구현 파일
//

#include "pch.h"
#include "DatasetMaker.h"
#include "MultiDrawingDlg.h"
#include "afxdialogex.h"


// CMultiSelectDlg 대화 상자

IMPLEMENT_DYNAMIC(CMultiDrawingDlg, CDialogEx)

CMultiDrawingDlg::CMultiDrawingDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DLG_MULTI_DRAWING, pParent)
{
	m_pParent = pParent;
}

CMultiDrawingDlg::~CMultiDrawingDlg()
{
}

void CMultiDrawingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CB_MULTI_DIRECTION, m_cbMultiDirection);
	DDX_Control(pDX, IDC_EDIT_MULTI_COUNT, m_editMultiCount);
}


BEGIN_MESSAGE_MAP(CMultiDrawingDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CMultiDrawingDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CMultiDrawingDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// CMultiSelectDlg 메시지 처리기

BOOL CMultiDrawingDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	if (m_pDir != NULL || m_pCnt != NULL)
	{
		m_cbMultiDirection.SetCurSel(0);
		m_editMultiCount.SetWindowText(_T("1"));
	}
	else
	{
		OnCancel();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CMultiDrawingDlg::OnBnClickedOk()
{
	*m_pDir = m_cbMultiDirection.GetCurSel() - 1;

	CString strCnt;
	m_editMultiCount.GetWindowTextW(strCnt);

	*m_pCnt = _ttoi(strCnt);

	CDialogEx::OnOK();
}


void CMultiDrawingDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialogEx::OnCancel();
}
