#pragma once

#include <opencv2/opencv.hpp>
#include <vector>

using namespace cv;
using namespace std;

// label coordinates
typedef struct{
    cv::Rect_<float> abs_rect;
    int id;
} coord_t;

class COptFlowTracker
{

public:
    const int m_nFlowError;

    COptFlowTracker(int nWinSize = 15, int nMaxLevel = 3, int nIters = 8000, int nFlowError = -1);

    // just to avoid extra allocations
    cv::Mat m_matDstGray;
    vector<Point2f> m_ptsFlowPrev, m_ptsFlowCur;
    vector<uchar> m_vecStatus;
    vector<float> m_vecErr;

    cv::Mat m_matSrcGray;    // used in both functions

    std::vector<coord_t> m_vecBboxCur;
    std::vector<bool> m_vecFlagsBboxGood;

    void UpdateBbox(std::vector<coord_t> vecBboxCur);

    void UpdateTrackingFlow(cv::Mat matSrcNew, std::vector<coord_t> vecBboxCur);

    std::vector<coord_t> TrackingFlow(cv::Mat matDstNew, bool bCheckError = true);

};

