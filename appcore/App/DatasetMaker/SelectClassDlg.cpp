﻿// SelectLabelDlg.cpp: 구현 파일
//

#include "pch.h"
#include "DatasetMaker.h"
#include "SelectClassDlg.h"
#include "afxdialogex.h"


// CSelectLabelDlg 대화 상자

IMPLEMENT_DYNAMIC(CSelectClassDlg, CDialogEx)

CSelectClassDlg::CSelectClassDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DLG_SELECT_CLASS, pParent)
{
	m_pParent = pParent;
}

CSelectClassDlg::~CSelectClassDlg()
{
}

void CSelectClassDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BTN_SELECT_COLOR, m_btnColor);
	DDX_Control(pDX, IDC_LIST_SELECT_CLASS, m_lstSelectLabel);
}


BEGIN_MESSAGE_MAP(CSelectClassDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CSelectClassDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CSelectClassDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BTN_SELECT_COLOR, &CSelectClassDlg::OnBnClickedBtnSelectColor)
	ON_LBN_SELCHANGE(IDC_LIST_SELECT_CLASS, &CSelectClassDlg::OnLbnSelchangeListSelectClass)
END_MESSAGE_MAP()


// CSelectLabelDlg 메시지 처리기

BOOL CSelectClassDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	if (m_pClassNames != NULL)
	{
		m_nSelIdx = 0;

		m_btnColor.EnableWindowsTheming(FALSE);
		m_btnColor.SetFaceColor(RGB(m_pClassColors[m_nSelIdx].r, m_pClassColors[m_nSelIdx].g, m_pClassColors[m_nSelIdx].b));

		for (int i = 0; i < (*m_pClassNames).size(); i++)
		{
			m_lstSelectLabel.AddString((*m_pClassNames)[i]);

		}

		if (m_pSelClass != NULL)
		{
			m_lstSelectLabel.SetCurSel(*m_pSelClass);
		}
	}
	else
	{
		OnCancel();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CSelectClassDlg::OnBnClickedOk()
{
	int nCurSel = m_lstSelectLabel.GetCurSel();

	if (m_pSelClass != NULL && nCurSel != -1)
		*m_pSelClass = nCurSel;


	CDialogEx::OnOK();
}


void CSelectClassDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialogEx::OnCancel();
}

void CSelectClassDlg::SetClassColors(StClassModel* pColors)
{
	m_pClassColors = pColors;
}

void CSelectClassDlg::SetClassNames(vector<CString>* pNames)
{
	m_pClassNames = pNames;
}


void CSelectClassDlg::OnBnClickedBtnSelectColor()
{
	CColorDialog dlg;
	if (dlg.DoModal() == IDOK)
	{
		COLORREF color = dlg.GetColor();
		
		m_pClassColors[m_nSelIdx] = StClassModel { GetRValue(color), GetGValue(color), GetBValue(color) };

		m_btnColor.EnableWindowsTheming(FALSE);
		m_btnColor.SetFaceColor(RGB(m_pClassColors[m_nSelIdx].r, m_pClassColors[m_nSelIdx].g, m_pClassColors[m_nSelIdx].b));
	}
}


void CSelectClassDlg::OnLbnSelchangeListSelectClass()
{
	m_nSelIdx = m_lstSelectLabel.GetCurSel();

	m_btnColor.EnableWindowsTheming(FALSE);
	m_btnColor.SetFaceColor(RGB(m_pClassColors[m_nSelIdx].r, m_pClassColors[m_nSelIdx].g, m_pClassColors[m_nSelIdx].b));
}
