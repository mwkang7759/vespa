﻿// EditRatioDlg.cpp: 구현 파일
//

#include "pch.h"
#include "DatasetMaker.h"
#include "EditRatioDlg.h"
#include "afxdialogex.h"


// CEditRatioDlg 대화 상자

IMPLEMENT_DYNAMIC(CEditRatioDlg, CDialogEx)

CEditRatioDlg::CEditRatioDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DLG_EDIT_RATIO, pParent)
{
	m_pParent = pParent;
}

CEditRatioDlg::~CEditRatioDlg()
{
}

void CEditRatioDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CB_NEW_RATIO_X, m_cbNewRatioX);
	DDX_Control(pDX, IDC_CB_NEW_RATIO_Y, m_cbNewRatioY);
}


BEGIN_MESSAGE_MAP(CEditRatioDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CEditRatioDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CEditRatioDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// CEditRatioDlg 메시지 처리기


BOOL CEditRatioDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	if (m_pRatioX != NULL || m_pRatioY != NULL)
	{
		m_cbNewRatioX.InsertString(0, _T("1"));
		m_cbNewRatioX.InsertString(1, _T("2"));
		m_cbNewRatioX.InsertString(2, _T("4"));
		m_cbNewRatioY.InsertString(0, _T("1"));
		m_cbNewRatioY.InsertString(1, _T("2"));
		m_cbNewRatioY.InsertString(2, _T("4"));
		m_cbNewRatioX.SetCurSel(0);
		m_cbNewRatioY.SetCurSel(0);
	}
	else
	{
		OnCancel();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CEditRatioDlg::OnBnClickedOk()
{
	if (m_cbNewRatioX.GetCurSel() == 0)
		*m_pRatioX = 1;
	else if (m_cbNewRatioX.GetCurSel() == 1)
		*m_pRatioX = 2;
	else
		*m_pRatioX = 4;

	if (m_cbNewRatioY.GetCurSel() == 0)
		*m_pRatioY = 1;
	else if (m_cbNewRatioY.GetCurSel() == 1)
		*m_pRatioY = 2;
	else
		*m_pRatioY = 4;

	CDialogEx::OnOK();
}


void CEditRatioDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialogEx::OnCancel();
}