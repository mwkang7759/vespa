﻿#include "Protocol.h"
std::map<int32_t, std::string> mapError;

template <typename T>
int32_t JsonValueValidation(const rapidjson::Value& _value, const std::string _key, T& dstData)
{
	int32_t errorCode = ESMBase::ERR_CODE_T::UNKNOWN;
	if (_value.HasMember(_key))
	{
		const rapidjson::Value::ConstMemberIterator miter = _value.FindMember(_key);
		auto tmp = miter->value.GetType();
		if (typeid(dstData) == typeid(bool))
		{
			if (miter->value.GetType() != rapidjson::Type::kFalseType && miter->value.GetType() != rapidjson::Type::kTrueType)
			{
				errorCode = ESMBase::ERR_CODE_T::JSON_PARSE_TYPE_ERROR;
				mapError.insert(make_pair(errorCode, _key));
				throw mapError;
			}
		}
		else
		{
			if (miter->value.GetType() != rapidjson::Type::kNumberType)
			{

				errorCode = ESMBase::ERR_CODE_T::JSON_PARSE_TYPE_ERROR;
				mapError.insert(make_pair(errorCode, _key));
				throw mapError;
			}
		}

		switch (miter->value.GetType())
		{
		case rapidjson::Type::kFalseType:
			dstData = miter->value.GetBool();
			break;
		case rapidjson::Type::kTrueType:
			dstData = miter->value.GetBool();
			break;
		case rapidjson::Type::kNumberType:
			if (miter->value.IsDouble())
				dstData = miter->value.GetDouble();
			else
				dstData = miter->value.GetInt();
			break;
		default:
			// TODO: anothers...!
			break;
		}
	}
	else
	{
		errorCode = ESMBase::ERR_CODE_T::JSON_PARSE_KEY_NOT_FOUND;
		mapError.insert(make_pair(errorCode, _key));
		throw mapError;
	}

	return ESMBase::ERR_CODE_T::SUCCESS;
}

template<>
int32_t JsonValueValidation(const rapidjson::Value& _value, const std::string _key, std::string& dstData)
{
	int32_t errorCode = ESMBase::ERR_CODE_T::UNKNOWN;
	if (_value.HasMember(_key))
	{
		const rapidjson::Value::ConstMemberIterator miter = _value.FindMember(_key);
		if (miter->value.GetType() != rapidjson::Type::kStringType)
		{
			errorCode = ESMBase::ERR_CODE_T::JSON_PARSE_TYPE_ERROR;
			mapError.insert(make_pair(errorCode, _key));
			throw mapError;
		}
		dstData = miter->value.GetString();
	}
	else
	{
		errorCode = ESMBase::ERR_CODE_T::JSON_PARSE_KEY_NOT_FOUND;
		mapError.insert(make_pair(errorCode, _key));
		throw mapError;
	}

	return ESMBase::ERR_CODE_T::SUCCESS;
}

int32_t JsonValueValidation(const rapidjson::Value::ConstValueIterator& _value, const std::string _key, std::vector<int>& dstData)
{
	int32_t errorCode = ESMBase::ERR_CODE_T::UNKNOWN;
	if (_value->HasMember(_key))
	{
		const rapidjson::Value::ConstMemberIterator miter = _value->FindMember(_key);
		if (miter->value.GetType() != rapidjson::Type::kArrayType)
		{
			errorCode = ESMBase::ERR_CODE_T::JSON_PARSE_TYPE_ERROR;
			mapError.insert(make_pair(errorCode, _key));
			throw mapError;
		}
		for (auto& avalue : miter->value.GetArray())
		{
			if (avalue.GetType() != rapidjson::Type::kNumberType)
			{
				errorCode = ESMBase::ERR_CODE_T::JSON_PARSE_TYPE_ERROR;
				mapError.insert(make_pair(errorCode, _key));
				throw mapError;
			}
			dstData.push_back(avalue.GetInt());
		}
	}
	else
	{
		errorCode = ESMBase::ERR_CODE_T::JSON_PARSE_KEY_NOT_FOUND;
		mapError.insert(make_pair(errorCode, _key));
		throw mapError;
	}

	return ESMBase::ERR_CODE_T::SUCCESS;
}

int32_t JsonValueValidationArray(const rapidjson::Value& _value, const std::string _key, double dstData[])
{
	int32_t errorCode = ESMBase::ERR_CODE_T::UNKNOWN;
	if (_value.HasMember(_key))
	{
		const rapidjson::Value::ConstMemberIterator miter = _value.FindMember(_key);
		if (miter->value.GetType() != rapidjson::Type::kArrayType)
		{
			errorCode = ESMBase::ERR_CODE_T::JSON_PARSE_TYPE_ERROR;
			mapError.insert(make_pair(errorCode, _key));
			throw mapError;
		}
		int32_t index = 0;
		for (auto& avalue : miter->value.GetArray())
		{
			if (avalue.GetType() != rapidjson::Type::kNumberType)
			{
				errorCode = ESMBase::ERR_CODE_T::JSON_PARSE_TYPE_ERROR;
				mapError.insert(make_pair(errorCode, _key));
				throw mapError;
			}
			dstData[index] = avalue.GetDouble();
			index++;
		}
	}
	else
	{
		errorCode = ESMBase::ERR_CODE_T::JSON_PARSE_KEY_NOT_FOUND;
		mapError.insert(make_pair(errorCode, _key));
		throw mapError;
	}

	return ESMBase::ERR_CODE_T::SUCCESS;
}

int32_t Protocol::JsonBasicParse(const std::string body, MTdProtocol& mtdProtocol)
{
	int32_t errorCode = ESMBase::ERR_CODE_T::UNKNOWN;
	m_document.Parse(body);

	try {
		if (m_document.HasParseError())
		{
			errorCode = ESMBase::ERR_CODE_T::JSON_PARSE_DOC_ERROR;
			//SPd_ERROR(ERROR_MSG_FORMAT + "Json Parse error", errorCode);
			return errorCode;
		}

		rapidjson::Value& object(m_document);
		JsonValueValidation(object, MTDPROTOCOL_SECTION1, mtdProtocol.Section1);
		JsonValueValidation(object, MTDPROTOCOL_SECTION2, mtdProtocol.Section2);
		JsonValueValidation(object, MTDPROTOCOL_SECTION3, mtdProtocol.Section3);
		JsonValueValidation(object, MTDPROTOCOL_SENDSTATE, mtdProtocol.SendState);
		JsonValueValidation(object, MTDPROTOCOL_TOKEN, mtdProtocol.Token);
		JsonValueValidation(object, MTDPROTOCOL_FROM, mtdProtocol.From);
		JsonValueValidation(object, MTDPROTOCOL_TO, mtdProtocol.To);
		JsonValueValidation(object, MTDPROTOCOL_ACTION, mtdProtocol.Action);
	}
	catch (std::map<int32_t, std::string> me)
	{
		int32_t expCode = ESMBase::ERR_CODE_T::UNKNOWN;
		for (auto iter : me)
		{
			//SPd_ERROR(ERROR_MSG_FORMAT + iter.second, iter.first);
			expCode = iter.first;
		}

		me.clear();
		return expCode;
	}
	catch (const char* exp)
	{
		//SPd_ERROR(ERROR_MSG_FORMAT + exp, errorCode);
		return errorCode;
	}
	catch (std::exception& e)
	{
		//SPd_ERROR(ERROR_MSG_FORMAT + e.what(), errorCode);
		return errorCode;
	}
	catch (...)
	{
		//SPd_ERROR("Error : Default Catch", errorCode);
		return errorCode;
	}

	return ESMBase::ERR_CODE_T::SUCCESS;
}

int32_t Protocol::ParseConfigSetting(MTdProtocol_4DSDP001& _st4DSDP001)
{
	int32_t errorCode = ESMBase::ERR_CODE_T::UNKNOWN;
	// option
	std::vector<Camera> vCamera;
	std::vector<Path> vPath;
	std::vector<VSPd> vVSpd;
	try
	{
		if (m_document.HasMember("option"))
		{
			rapidjson::Value& object = m_document["option"];
			Option stOption;

			JsonValueValidation(object, "UHDtoFHD", stOption.uhdtofhd);
			JsonValueValidation(object, "camfps", stOption.camfps);
			JsonValueValidation(object, "fps", stOption.fps);
			JsonValueValidation(object, "waittime", stOption.waittime);
			if (!object.HasMember("ReEncoding"))
			{
				//SPd_ERROR(ERROR_MSG_FORMAT + "Parse ReEncoding", errorCode);
				return errorCode;
			}
			rapidjson::Value& oobject = object["ReEncoding"];
			JsonValueValidation(oobject, "use", stOption._reencoding.use);
			JsonValueValidation(oobject, "bitrate", stOption._reencoding.bitrate);
			JsonValueValidation(oobject, "ts", stOption._reencoding.ts);
			_st4DSDP001.option = stOption;
		}
		else
		{
			errorCode = ESMBase::ERR_CODE_T::JSON_PARSE_DOC_ERROR;
			//SPd_ERROR(ERROR_MSG_FORMAT + "Json Parse error", errorCode); 
			return errorCode;
		}

		// cameras
		rapidjson::Value& jcameras = m_document[JSON_CAMERAS];
		for (rapidjson::Value::ConstValueIterator itr = jcameras.Begin(); itr != jcameras.End(); ++itr)
		{
			Camera stCamera;
			const rapidjson::Value& object = itr->GetObjectW();
			JsonValueValidation(object, "index", stCamera.index);
			JsonValueValidation(object, "prefix", stCamera.prefix);
			JsonValueValidation(itr, "pathid", stCamera.pathid);
			//JsonValueValidation(object, "flip", stCamera.flip);
			vCamera.push_back(stCamera);
		}

		// paths
		rapidjson::Value& jpaths = m_document["paths"];
		for (rapidjson::Value::ConstValueIterator itr = jpaths.Begin(); itr != jpaths.End(); ++itr)
		{
			Path stPath;
			const rapidjson::Value& object = itr->GetObjectW();
			JsonValueValidation(object, "id", stPath.id);
			JsonValueValidation(object, "path", stPath.path);
			vPath.push_back(stPath);
		}

		std::string address;
		rapidjson::Value& device = m_document["device"];
		JsonValueValidation(device, "server", address);
		_st4DSDP001.vspdServerAddress = address;

		rapidjson::Value& clients = device["client"];
		for (rapidjson::Value::ConstValueIterator itr = clients.Begin(); itr != clients.End(); ++itr)
		{
			VSPd spd;
			spd.address = itr->GetString();
			//spd.port = VSPD_INTERNAL_WEBSOCKET_PORT;
			vVSpd.push_back(spd);
		}
	}
	catch (std::map<int32_t, std::string> me)
	{
		int32_t expCode = ESMBase::ERR_CODE_T::UNKNOWN;
		for (auto iter : me)
		{
			//SPd_ERROR(ERROR_MSG_FORMAT + iter.second, iter.first);
			expCode = iter.first;
		}

		me.clear();
		return expCode;
	}
	catch (std::exception& e)
	{
		//SPd_ERROR(ERROR_MSG_FORMAT + e.what(), errorCode);
		return errorCode;
	}
	catch (...)
	{
		//SPd_ERROR("Error : Default Catch", errorCode);
		return errorCode;
	}

	_st4DSDP001.cameras = vCamera;
	_st4DSDP001.paths = vPath;
	_st4DSDP001.vspds = vVSpd;
	return ESMBase::ERR_CODE_T::SUCCESS;
}

int32_t Protocol::ParseAdjustSetting(MTdProtocol_4DSDP002& _st4DSDP002)
{
	int32_t errorCode = ESMBase::ERR_CODE_T::UNKNOWN;
	rapidjson::Value& object = m_document;
	try
	{
		JsonValueValidation(object, "AdjustID", _st4DSDP002._AdjustId);

		rapidjson::Value& object_itr = m_document["AdjustData"];
		for (rapidjson::Value::ValueIterator itr = object_itr.Begin(); itr != object_itr.End(); ++itr)
		{
			AdjustData _adjustData;
			Adjust _adjust;
			rtMargin _rtMargin;
			PositionTracking3D _pts3d;
			rapidjson::Value& oobject = itr->GetObjectW();
			JsonValueValidation(oobject, "DscID", _adjustData._DscId);
			JsonValueValidation(oobject, "Mode", _adjustData._Mode);

			if (!oobject.HasMember("Adjust"))
			{
				//SPd_ERROR(ERROR_MSG_FORMAT + "Parse Adjust", errorCode);
				return errorCode;
			}
			rapidjson::Value& ooobject = oobject["Adjust"];
			JsonValueValidation(ooobject, "dAdjustX", _adjust._dAdjuxtX);
			JsonValueValidation(ooobject, "dAdjustY", _adjust._dAdjuxtY);
			JsonValueValidation(ooobject, "dAngle", _adjust._dAngle);
			JsonValueValidation(ooobject, "dRotateX", _adjust._dRotateX);
			JsonValueValidation(ooobject, "dRotateY", _adjust._dRotateY);
			JsonValueValidation(ooobject, "dScale", _adjust._dScale);

			if (!ooobject.HasMember("rtMargin"))
			{
				//SPd_ERROR(ERROR_MSG_FORMAT + "Parse rtMargin", errorCode);
				return errorCode;
			}
			rapidjson::Value& oooobject = ooobject["rtMargin"];
			JsonValueValidation(oooobject, "X", _rtMargin._X);
			JsonValueValidation(oooobject, "Y", _rtMargin._Y);
			JsonValueValidation(oooobject, "Width", _rtMargin._Width);
			JsonValueValidation(oooobject, "Height", _rtMargin._Height);

			JsonValueValidation(oobject, "flip", _adjustData._flip);
			JsonValueValidation(oobject, "UseLogo", _adjustData._UseLogo);

			if (!oobject.HasMember("pts_3d"))
			{
				//SPd_ERROR(ERROR_MSG_FORMAT + "Parse pts_3d", errorCode);
				return errorCode;
			}
			rapidjson::Value& oopts = oobject["pts_3d"];
			JsonValueValidation(oopts, "X1", _pts3d._X1);
			JsonValueValidation(oopts, "X2", _pts3d._X2);
			JsonValueValidation(oopts, "X3", _pts3d._X3);
			JsonValueValidation(oopts, "X4", _pts3d._X4);
			JsonValueValidation(oopts, "Y1", _pts3d._Y1);
			JsonValueValidation(oopts, "Y2", _pts3d._Y2);
			JsonValueValidation(oopts, "Y3", _pts3d._Y3);
			JsonValueValidation(oopts, "Y4", _pts3d._Y4);
			JsonValueValidation(oopts, "CenterX", _pts3d._CenterX);
			JsonValueValidation(oopts, "CenterY", _pts3d._CenterY);

			_adjust._rtMargin = _rtMargin;
			_adjustData._Adjust = _adjust;
			_adjustData._pts3d = _pts3d;

			_st4DSDP002._AdjustDatas.push_back(_adjustData);
		}
	}
	catch (std::map<int32_t, std::string> me)
	{
		int32_t expCode = ESMBase::ERR_CODE_T::UNKNOWN;
		for (auto iter : me)
		{
			//SPd_ERROR(ERROR_MSG_FORMAT + iter.second, iter.first);
			expCode = iter.first;
		}

		me.clear();
		return expCode;
	}
	catch (std::exception& e)
	{
		//SPd_ERROR(ERROR_MSG_FORMAT + e.what(), errorCode);
		return errorCode;
	}
	catch (...)
	{
		//SPd_ERROR("Error : Default Catch", errorCode);
		return errorCode;
	}


	return ESMBase::ERR_CODE_T::SUCCESS;
}

int32_t Protocol::ParseMakeMovie(MTdProtocol_4DSDP003& _st4DSDP003)
{
	int32_t errorCode = ESMBase::ERR_CODE_T::UNKNOWN;
	rapidjson::Value& object = m_document;
	try
	{
		JsonValueValidation(object, "Name", _st4DSDP003.name);
		JsonValueValidation(object, "RecordName", _st4DSDP003.recordname);
		JsonValueValidation(object, "MarkerTime", _st4DSDP003._MarkerTime);
		JsonValueValidation(object, "Template", _st4DSDP003._template);
		if (!object.HasMember("prefix"))
		{
			//SPd_DEBUG(ERROR_MSG_FORMAT + "Parse prefix", errorCode);
		}
		else
		{
			JsonValueValidation(object, "prefix", _st4DSDP003._prefix);
		}
		if (!object.HasMember("gimbal"))
		{
			//SPd_DEBUG(ERROR_MSG_FORMAT + "Parse Gimbal", errorCode);
			_st4DSDP003._gimbal = false;
		}
		else
		{
			JsonValueValidation(object, "gimbal", _st4DSDP003._gimbal);
		}

		if (object.HasMember("debugmode"))
		{
			_st4DSDP003.hasdebugmode = true;
			JsonValueValidation(object, "debugmode", _st4DSDP003.debugmode);
		}
		else
			_st4DSDP003.hasdebugmode = false;
	}
	catch (std::map<int32_t, std::string> me)
	{
		int32_t expCode = ESMBase::ERR_CODE_T::UNKNOWN;
		for (auto iter : me)
		{
			//SPd_ERROR(ERROR_MSG_FORMAT + iter.second, iter.first);
			expCode = iter.first;
		}
		me.clear();
		return expCode;
	}
	try
	{
		if (!object.HasMember("PT"))
		{
			//SPd_DEBUG(ERROR_MSG_FORMAT + "Parse PT", errorCode);

		}
		else
		{
			rapidjson::Value& oobject = object["PT"];
			JsonValueValidation(oobject, "x1", _st4DSDP003._PT.X1);
			JsonValueValidation(oobject, "y1", _st4DSDP003._PT.Y1);
			JsonValueValidation(oobject, "x2", _st4DSDP003._PT.X2);
			JsonValueValidation(oobject, "y2", _st4DSDP003._PT.Y2);
		}
	}
	catch (std::map<int32_t, std::string> me)
	{
		int32_t expCode = ESMBase::ERR_CODE_T::UNKNOWN;
		_st4DSDP003._PT = { 0,0,0,0 };
		return expCode;
	}
	return ESMBase::ERR_CODE_T::SUCCESS;
}

int32_t Protocol::ParseStartSDI(MTdProtocol_4DSDP004& _st4DSDP004)
{
	int32_t errorCode = ESMBase::ERR_CODE_T::UNKNOWN;
	// playlist
	std::vector<PlayList> vPlatList;
	const rapidjson::Value& jPlayLists = m_document["PlayList"];
	try
	{
		for (rapidjson::Value::ConstValueIterator itr = jPlayLists.Begin(); itr != jPlayLists.End(); ++itr)
		{
			PlayList stPlayList;
			const rapidjson::Value& object = itr->GetObjectW();
			JsonValueValidation(object, "id", stPlayList.id);
			JsonValueValidation(object, "name", stPlayList.name);
			JsonValueValidation(object, "made", stPlayList.made);
			JsonValueValidation(object, "path", stPlayList.path);

			vPlatList.push_back(stPlayList);
		}
		_st4DSDP004.playlists = vPlatList;


		rapidjson::Value& dobject = m_document;
		JsonValueValidation(dobject, "StartID", _st4DSDP004.strtid);
		JsonValueValidation(dobject, "UseLogo", _st4DSDP004.uselogo);
		if (_st4DSDP004.uselogo)
			JsonValueValidation(dobject, "LogoPath", _st4DSDP004.logopath);
		JsonValueValidation(dobject, "Repeat", _st4DSDP004.repeat);
		JsonValueValidation(dobject, "count", _st4DSDP004.count);
	}
	catch (std::map<int32_t, std::string> me)
	{
		int32_t expCode = ESMBase::ERR_CODE_T::UNKNOWN;
		for (auto iter : me)
		{
			//SPd_ERROR(ERROR_MSG_FORMAT + iter.second, iter.first);
			expCode = iter.first;
		}

		me.clear();
		return expCode;
	}

	return ESMBase::ERR_CODE_T::SUCCESS;
}

int32_t Protocol::ParseStopSDI(MTdProtocol_4DSDP005& _st4DSDP005)
{
	int32_t errorCode = ESMBase::ERR_CODE_T::UNKNOWN;
	return ESMBase::ERR_CODE_T::SUCCESS;
}

int32_t Protocol::ParseModifiyListSDI(MTdProtocol_4DSDP006& _st4DSDP006)
{
	int32_t errorCode = ESMBase::ERR_CODE_T::UNKNOWN;
	// playlist
	std::vector<PlayList> vPlatList;
	const rapidjson::Value& jPlayLists = m_document["PlayList"];
	try
	{
		for (rapidjson::Value::ConstValueIterator itr = jPlayLists.Begin(); itr != jPlayLists.End(); ++itr)
		{
			PlayList stPlayList;

			const rapidjson::Value& object = itr->GetObjectW();
			JsonValueValidation(object, "id", stPlayList.id);
			JsonValueValidation(object, "name", stPlayList.name);
			JsonValueValidation(object, "made", stPlayList.made);
			JsonValueValidation(object, "path", stPlayList.path);

			vPlatList.push_back(stPlayList);
		}
		_st4DSDP006.playlists = vPlatList;

		rapidjson::Value& dobject = m_document;
		JsonValueValidation(dobject, "StartID", _st4DSDP006.strtid);
		JsonValueValidation(dobject, "UseLogo", _st4DSDP006.uselogo);
		if (_st4DSDP006.uselogo)
			JsonValueValidation(dobject, "LogoPath", _st4DSDP006.logopath);
		JsonValueValidation(dobject, "Repeat", _st4DSDP006.repeat);
		JsonValueValidation(dobject, "count", _st4DSDP006.count);
	}
	catch (std::map<int32_t, std::string> me)
	{
		int32_t expCode = ESMBase::ERR_CODE_T::UNKNOWN;
		for (auto iter : me)
		{
			//SPd_ERROR(ERROR_MSG_FORMAT + iter.second, iter.first);
			expCode = iter.first;
		}

		me.clear();
		return expCode;
	}
	catch (std::exception& e)
	{
		//SPd_ERROR(ERROR_MSG_FORMAT + e.what(), errorCode);
		return errorCode;
	}
	catch (...)
	{
		//SPd_ERROR("Error : Default Catch", errorCode);
		return errorCode;
	}

	return ESMBase::ERR_CODE_T::SUCCESS;
}

int32_t Protocol::ParseDelMovie(MTdProtocol_4DSDP007& _st4DSDP007)
{
	int32_t errorCode = ESMBase::ERR_CODE_T::UNKNOWN;

	// DelList
	std::vector<DelList> vDelList;
	rapidjson::Value& jdellists = m_document["DelList"];
	try
	{
		for (rapidjson::Value::ConstValueIterator itr = jdellists.Begin(); itr < jdellists.End(); ++itr)
		{
			DelList stDelList;

			const rapidjson::Value& object = itr->GetObjectW();
			JsonValueValidation(object, "Name", stDelList.name);
			JsonValueValidation(object, "Path", stDelList.path);

			vDelList.push_back(stDelList);
		}
	}
	catch (std::map<int32_t, std::string> me)
	{
		int32_t expCode = ESMBase::ERR_CODE_T::UNKNOWN;
		for (auto iter : me)
		{
			//SPd_ERROR(ERROR_MSG_FORMAT + iter.second, iter.first);
			expCode = iter.first;
		}

		me.clear();
		return expCode;
	}
	catch (std::exception& e)
	{
		//SPd_ERROR(ERROR_MSG_FORMAT + e.what(), errorCode);
		return errorCode;
	}
	catch (...)
	{
		//SPd_ERROR("Error : Default Catch", errorCode);
		return errorCode;
	}
	_st4DSDP007.dellists = vDelList;
	return ESMBase::ERR_CODE_T::SUCCESS;
}

int32_t Protocol::ParseGetVersion(MTdProtocol_4DSDP008& _st4DSDP008)
{
	int32_t errorCode = ESMBase::ERR_CODE_T::UNKNOWN;
	return ESMBase::ERR_CODE_T::SUCCESS;
}

int32_t Protocol::ParseGetAdjustID(MTdProtocol_4DSDP009& _st4DSDP009)
{
	int32_t errorCode = ESMBase::ERR_CODE_T::UNKNOWN;
	return ESMBase::ERR_CODE_T::SUCCESS;
}

int32_t Protocol::ParseAdjustData(MTdProtocol_4DSDP002& _st4DSDP002)
{
	int32_t errorCode = ESMBase::ERR_CODE_T::UNKNOWN;
	rapidjson::Value& object = m_document;
	try
	{
		JsonValueValidation(object, "AdjustID", _st4DSDP002._AdjustId);

		rapidjson::Value& object_itr = m_document["AdjustData"];
		for (rapidjson::Value::ValueIterator itr = object_itr.Begin(); itr != object_itr.End(); ++itr)
		{
			AdjustData _adjustData;
			Adjust _adjust;
			rtMargin _rtMargin;
			PositionTracking3D _pts3d;
			rapidjson::Value& oobject = itr->GetObjectW();
			// add adjust member..
			JsonValueValidation(oobject, "LiveIndex", _adjustData._liveIdx);
			JsonValueValidation(oobject, "ReplayIndex", _adjustData._replayIdx);
			JsonValueValidation(oobject, "DscID", _adjustData._DscId);
			JsonValueValidation(oobject, "Mode", _adjustData._Mode);

			if (!oobject.HasMember("Adjust"))
			{
				//SPd_ERROR(ERROR_MSG_FORMAT + "Parse Adjust", errorCode);
				return errorCode;
			}
			rapidjson::Value& ooobject = oobject["Adjust"];
			JsonValueValidation(ooobject, "imageWidth", _adjust._imageWidth);
			JsonValueValidation(ooobject, "imageHeight", _adjust._imageHeight);

			// temparal code..
			/*JsonValueValidation(ooobject, "normAdjustX", _adjust._normAdjustX);
			JsonValueValidation(ooobject, "normAdjustY", _adjust._normAdjustY);
			JsonValueValidation(ooobject, "normRotateX", _adjust._normRotateX);
			JsonValueValidation(ooobject, "normRotateY", _adjust._normRotateY);*/

			JsonValueValidation(ooobject, "dAdjustX", _adjust._dAdjuxtX);
			JsonValueValidation(ooobject, "dAdjustY", _adjust._dAdjuxtY);
			JsonValueValidation(ooobject, "dAngle", _adjust._dAngle);
			JsonValueValidation(ooobject, "dRotateX", _adjust._dRotateX);
			JsonValueValidation(ooobject, "dRotateY", _adjust._dRotateY);
			JsonValueValidation(ooobject, "dScale", _adjust._dScale);

			JsonValueValidation(ooobject, "flip", _adjust._flip);
			JsonValueValidation(ooobject, "UseLogo", _adjust._useLogo);

			if (!ooobject.HasMember("rtMargin"))
			{
				//SPd_ERROR(ERROR_MSG_FORMAT + "Parse rtMargin", errorCode);
				return errorCode;
			}
			rapidjson::Value& oooobject = ooobject["rtMargin"];
			JsonValueValidation(oooobject, "X", _rtMargin._X);
			JsonValueValidation(oooobject, "Y", _rtMargin._Y);
			JsonValueValidation(oooobject, "Width", _rtMargin._Width);
			JsonValueValidation(oooobject, "Height", _rtMargin._Height);


			if (!oobject.HasMember("pts_3d"))
			{
				//SPd_ERROR(ERROR_MSG_FORMAT + "Parse pts_3d", errorCode);
				return errorCode;
			}
			rapidjson::Value& oopts = oobject["pts_3d"];
			JsonValueValidation(oopts, "X1", _pts3d._X1);
			JsonValueValidation(oopts, "X2", _pts3d._X2);
			JsonValueValidation(oopts, "X3", _pts3d._X3);
			JsonValueValidation(oopts, "X4", _pts3d._X4);
			JsonValueValidation(oopts, "Y1", _pts3d._Y1);
			JsonValueValidation(oopts, "Y2", _pts3d._Y2);
			JsonValueValidation(oopts, "Y3", _pts3d._Y3);
			JsonValueValidation(oopts, "Y4", _pts3d._Y4);
			JsonValueValidation(oopts, "CenterX", _pts3d._CenterX);
			JsonValueValidation(oopts, "CenterY", _pts3d._CenterY);

			_adjust._rtMargin = _rtMargin;
			_adjustData._Adjust = _adjust;
			_adjustData._pts3d = _pts3d;

			// add homo_t
			/*if (!oobject.HasMember("homo_t"))
			{
				SPd_ERROR(ERROR_MSG_FORMAT + "Parse homo_t", errorCode);
				return errorCode;
			}
			rapidjson::Value& oohomo = oobject["homo_t"];
			JsonValueValidation(oohomo, "r1", _homo_t.r1);
			JsonValueValidation(oohomo, "r2", _homo_t.r1);
			JsonValueValidation(oohomo, "r3", _homo_t.r1);
			_adjustData._homo_t = _homo_t;*/
			


			_st4DSDP002._AdjustDatas.push_back(_adjustData);
		}
	}
	catch (std::map<int32_t, std::string> me)
	{
		int32_t expCode = ESMBase::ERR_CODE_T::UNKNOWN;
		for (auto iter : me)
		{
			//SPd_ERROR(ERROR_MSG_FORMAT + iter.second, iter.first);
			expCode = iter.first;
		}

		me.clear();
		return expCode;
	}
	catch (std::exception& e)
	{
		//SPd_ERROR(ERROR_MSG_FORMAT + e.what(), errorCode);
		return errorCode;
	}
	catch (...)
	{
		//SPd_ERROR("Error : Default Catch", errorCode);
		return errorCode;
	}


	return ESMBase::ERR_CODE_T::SUCCESS;
}

int32_t Protocol::ParseVisualServoing(MTdProtocol_4DSDP010& _st4DSDP010)
{
	int32_t errorCode = ESMBase::ERR_CODE_T::UNKNOWN;

	// test..
	/*MTdProtocol_4DSDP002 st4DSDP002;
	errorCode = protocol.ParseAdjustSetting(st4DSDP002);
	if (errorCode != ESMBase::ERR_CODE_T::SUCCESS)
	{
		mExceptMessage.insert(std::make_pair(errorCode, "Adjust Set(4DSDP002) Parsing Failure"));
		throw mExceptMessage;
	}*/


	std::vector<Preset> vPresetList;
	rapidjson::Value& presetlist = m_document["PresetList"];
	try
	{
		for (rapidjson::Value::ConstValueIterator itr = presetlist.Begin(); itr < presetlist.End(); ++itr)
		{
			Preset stPreset;
			std::string camidx;

			const rapidjson::Value& object = itr->GetObjectW();
			JsonValueValidation(object, "cam_index", stPreset.cam_index);
			//JsonValueValidation(object, "cam_index", camidx);
			JsonValueValidation(object, "preset_image", stPreset.preset_image);

#if defined(WITH_SW_PRESET)
			const rapidjson::Value& oobject = object["gimbal"];
			JsonValueValidation(oobject, "pos_x", stPreset.gimbal.pos_x);
			JsonValueValidation(oobject, "pos_y", stPreset.gimbal.pos_y);
			JsonValueValidation(oobject, "pos_z", stPreset.gimbal.pos_z);
			JsonValueValidation(oobject, "zoom", stPreset.gimbal.zoom);
			JsonValueValidation(oobject, "ip", stPreset.gimbal.ip);
#else
			const rapidjson::Value& oobject = object["gimbal"];
			JsonValueValidation(oobject, "preset", stPreset.gimbal.preset);
			JsonValueValidation(oobject, "pos_x", stPreset.gimbal.pos_x);
			JsonValueValidation(oobject, "pos_y", stPreset.gimbal.pos_y);
			JsonValueValidation(oobject, "pos_z", stPreset.gimbal.pos_z);
			JsonValueValidation(oobject, "zoom", stPreset.gimbal.zoom);
			JsonValueValidation(oobject, "ip", stPreset.gimbal.ip);
#endif
			
			vPresetList.push_back(stPreset);
		}
	}
	catch (std::map<int32_t, std::string> me)
	{
		int32_t expCode = ESMBase::ERR_CODE_T::UNKNOWN;
		for (auto iter : me)
		{
			//SPd_ERROR(ERROR_MSG_FORMAT + iter.second, iter.first);
			expCode = iter.first;
		}

		me.clear();
		return expCode;
	}
	catch (std::exception& e)
	{
		//SPd_ERROR(ERROR_MSG_FORMAT + e.what(), errorCode);
		return errorCode;
	}
	catch (...)
	{
		//SPd_ERROR("Error : Default Catch", errorCode);
		return errorCode;
	}
	_st4DSDP010.presetList = vPresetList;
	return ESMBase::ERR_CODE_T::SUCCESS;
}

int32_t Protocol::JsonToString(rapidjson::Document& _document, std::string& message)
{
	rapidjson::StringBuffer buffer;
	rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
	_document.Accept(writer);

	message = buffer.GetString();

	return ESMBase::ERR_CODE_T::SUCCESS;
}


Protocol::~Protocol()
{

}

Protocol::Protocol()
{
	m_eSPdAPIId = SPdAPIId::NONE;
}

int32_t Protocol::MessageProtocol(const std::string msgBody)
{
	int32_t errorCode = ESMBase::ERR_CODE_T::UNKNOWN;
	MTdProtocol mtdProtocol;

	errorCode = JsonBasicParse(msgBody, mtdProtocol);
	if (errorCode != ESMBase::ERR_CODE_T::SUCCESS)
	{
		return errorCode;
	}

	if (mtdProtocol.Section1.compare(JSON_SECTION_1_4DREPLAY) == 0)
	{
		if (mtdProtocol.Section2.compare(JSON_SECTION_2_CONNECT) == 0)
		{
			m_eSPdAPIId = SPdAPIId::_4DSDP001;
		}
		else if (mtdProtocol.Section2.compare(JSON_SECTION_2_MAKEMOVIE) == 0)
		{
			m_eSPdAPIId = SPdAPIId::_4DSDP003;
		}
		else if (mtdProtocol.Section2.compare(JSON_SECTION_2_STARTSDI) == 0)
		{
			m_eSPdAPIId = SPdAPIId::_4DSDP004;
		}
		else if (mtdProtocol.Section2.compare(JSON_SECTION_2_STOPSDI) == 0)
		{
			m_eSPdAPIId = SPdAPIId::_4DSDP005;
		}
		else if (mtdProtocol.Section2.compare(JSON_SECTION_2_MODIFYLISTSDI) == 0)
		{
			m_eSPdAPIId = SPdAPIId::_4DSDP006;
		}
		else if (mtdProtocol.Section2.compare(JSON_SECTION_2_DELMOVIE) == 0)
		{
			m_eSPdAPIId = SPdAPIId::_4DSDP007;
		}
		else if (mtdProtocol.Section2.compare(JSON_SECTION_2_MOVETOPRESET) == 0)
		{
			m_eSPdAPIId = SPdAPIId::_4DSDP010;
		}
		else if (mtdProtocol.Section2.compare(JSON_SECTION_2_RECTIFICATION) == 0)
		{
			m_eSPdAPIId = SPdAPIId::_4DSDP011;
		}
		else if (mtdProtocol.Section2.compare(JSON_SECTION_2_STOPPRESETMOVE) == 0)
		{
			m_eSPdAPIId = SPdAPIId::_4DSDP012;
		}
		else
		{
			errorCode = ESMBase::ERR_CODE_T::JSON_PARSE_VALUE_NOT_FOUND;	
			//SPd_ERROR(ERROR_MSG_FORMAT + "Parse Section2 Value Not Found", errorCode); 
			return errorCode;
		}
	}
	else if (mtdProtocol.Section1.compare(JSON_SECTION_1_SPD) == 0)
	{
		if (mtdProtocol.Section2.compare(JSON_SECTION_2_ADJUST) == 0)
		{
			if (mtdProtocol.Section3.compare(JSON_SECTION_3_SET) == 0)
			{
				m_eSPdAPIId = SPdAPIId::_4DSDP002;
			}
			else
			{
				errorCode = ESMBase::ERR_CODE_T::JSON_PARSE_VALUE_NOT_FOUND;	
				//SPd_ERROR(ERROR_MSG_FORMAT + "Parse Section3 Value Not Found", errorCode); 
				return errorCode;
			}
		}
		else if (mtdProtocol.Section2.compare(JSON_SECTION_2_GetADJUSTID)==0)
		{
			m_eSPdAPIId = SPdAPIId::_4DSDP009;
		}
		else 
		{
			errorCode = ESMBase::ERR_CODE_T::JSON_PARSE_VALUE_NOT_FOUND;	
			//SPd_ERROR(ERROR_MSG_FORMAT + "Parse Section2 Value Not Found", errorCode); 
			return errorCode;
		}
	}
	else if (mtdProtocol.Section1.compare(JSON_SECTION_1_DAEMON) == 0)
	{
		if (mtdProtocol.Section2.compare(JSON_SECTION_2_INFORMATION) == 0)
		{
			if (mtdProtocol.Section3.compare(JSON_SECTION_3_VERSION) == 0)
			{
				m_eSPdAPIId = SPdAPIId::_4DSDP008;
			}
			else
			{
				errorCode = ESMBase::ERR_CODE_T::JSON_PARSE_VALUE_NOT_FOUND;
				//SPd_ERROR(ERROR_MSG_FORMAT + "Parse Section3 Value Not Found", errorCode); 
				return errorCode;
			}
		}
		else
		{
			errorCode = ESMBase::ERR_CODE_T::JSON_PARSE_VALUE_NOT_FOUND;	
			//SPd_ERROR(ERROR_MSG_FORMAT + "Parse Section2 Value Not Found", errorCode); 
			return errorCode;
		}
	}
	else if (mtdProtocol.Section1.compare(JSON_SECTION_1_CAMERA) == 0)
	{
		//SPd_DEBUG("Parse Section1 Value CAMERA ..");
		errorCode = ESMBase::ERR_CODE_T::JSON_PARSE_VALUE_NOT_FOUND;
		return errorCode;
	}
	else
	{
		errorCode = ESMBase::ERR_CODE_T::JSON_PARSE_VALUE_NOT_FOUND;	
		//SPd_ERROR(ERROR_MSG_FORMAT + "Parse Section1 Value Not Found", errorCode); 
		return errorCode;
	}

	m_stMTdProtocol = mtdProtocol;
	return ESMBase::ERR_CODE_T::SUCCESS;
}


int32_t Protocol::JsonFileWrite(std::string _path)
{
	std::ofstream stream(_path);
	rapidjson::OStreamWrapper oStreamWrapper(stream);
	rapidjson::Writer<rapidjson::OStreamWrapper> writer(oStreamWrapper);
	m_document.Accept(writer);

	return ESMBase::ERR_CODE_T::SUCCESS;
}





int32_t JsonCreate(MTdProtocol& _mtdProtocol, rapidjson::Document& document)
{
	int32_t errorCode = ESMBase::ERR_CODE_T::UNKNOWN;

	document.SetObject();
	document.AddMember(MTDPROTOCOL_SECTION1, _mtdProtocol.Section1, document.GetAllocator());
	document.AddMember(MTDPROTOCOL_SECTION2, _mtdProtocol.Section2, document.GetAllocator());
	document.AddMember(MTDPROTOCOL_SECTION3, _mtdProtocol.Section3, document.GetAllocator());
	document.AddMember(MTDPROTOCOL_SENDSTATE, _mtdProtocol.SendState, document.GetAllocator());
	document.AddMember(MTDPROTOCOL_TOKEN, _mtdProtocol.Token, document.GetAllocator());
	document.AddMember(MTDPROTOCOL_FROM, _mtdProtocol.From, document.GetAllocator());
	document.AddMember(MTDPROTOCOL_TO, _mtdProtocol.To, document.GetAllocator());
	document.AddMember(MTDPROTOCOL_ACTION, _mtdProtocol.Action, document.GetAllocator());


	return ESMBase::ERR_CODE_T::SUCCESS;
}

int32_t ParseSPdAPIId(const rapidjson::Document& _document, SPdAPIId& apiID)
{
	/*
		SPdAPIId Parsing
	*/

	int32_t errorCode = ESMBase::ERR_CODE_T::UNKNOWN;
	if (_document.HasParseError())
	{
		errorCode = ESMBase::ERR_CODE_T::JSON_PARSE_DOC_ERROR;
		//SPd_ERROR("Error Code {} :: Json Parse error", errorCode);
		return errorCode;
	}

	if (_document.HasMember("apiid") == false)
	{
		errorCode = ESMBase::ERR_CODE_T::JSON_PARSE_CODE_NOT_FOUND;	
		//SPd_ERROR(ERROR_MSG_FORMAT + "Parse apiid Not Found", errorCode); 
		return errorCode;
	}
	if (!_document["apiid"].IsInt())
	{
		errorCode = ESMBase::ERR_CODE_T::JSON_PARSE_CODE_NOT_FOUND;	
		//SPd_ERROR(ERROR_MSG_FORMAT + "Parse apiid Not Integer Type", errorCode); 
		return errorCode;
	}

	apiID = SPdAPIId(_document["apiid"].GetInt());

	return ESMBase::ERR_CODE_T::SUCCESS;
}


int32_t JsonSerialize(const rapidjson::Document& _document, std::string& serializeMsg)
{
	int32_t errorCode = ESMBase::ERR_CODE_T::UNKNOWN;
	rapidjson::StringBuffer buffer; buffer.Clear();
	rapidjson::Writer<rapidjson::StringBuffer> jsonWriter(buffer);

	_document.Accept(jsonWriter);
	serializeMsg = buffer.GetString();
	return ESMBase::ERR_CODE_T::SUCCESS;
}


int32_t SetSendProtocol(const SPdAPIId& _apiId, rapidjson::Document& _msgDocument, rapidjson::Document& sendDocument)
{
	int32_t errorCode = ESMBase::ERR_CODE_T::UNKNOWN;
	std::string to = sendDocument[MTDPROTOCOL_TO].GetString();
	std::string from = sendDocument[MTDPROTOCOL_FROM].GetString();
	sendDocument[MTDPROTOCOL_SENDSTATE].SetString("response");
	sendDocument[MTDPROTOCOL_FROM].SetString(rapidjson::StringRef(to), sendDocument.GetAllocator());
	sendDocument[MTDPROTOCOL_TO].SetString(rapidjson::StringRef(from), sendDocument.GetAllocator());
	sendDocument.AddMember(MTDPROTOCOL_RESULTCODE, _msgDocument["ResultCode"].GetInt(), sendDocument.GetAllocator());
	sendDocument.AddMember(MTDPROTOCOL_ERRORMSG, std::string(_msgDocument["ErrorMsg"].GetString()), sendDocument.GetAllocator());


	switch (_apiId)
	{
	case SPdAPIId::_4DSDP001:
		break;
	case SPdAPIId::_4DSDP002:
		break;
	case SPdAPIId::_4DSDP003:
		sendDocument.AddMember("Name", std::string(_msgDocument["Name"].GetString()), sendDocument.GetAllocator());
		sendDocument.AddMember("Status", std::string(_msgDocument["Status"].GetString()), sendDocument.GetAllocator());
		sendDocument.AddMember("Duration", std::string(_msgDocument["Duration"].GetString()), sendDocument.GetAllocator());
		sendDocument.AddMember("SavePath", std::string(_msgDocument["SavePath"].GetString()), sendDocument.GetAllocator());
		break;
	case SPdAPIId::_4DSDP004:
		sendDocument.AddMember("Name", std::string(_msgDocument["Name"].GetString()), sendDocument.GetAllocator());
		sendDocument.AddMember("Status", std::string(_msgDocument["Status"].GetString()), sendDocument.GetAllocator());
		break;
	case SPdAPIId::_4DSDP005:
		break;
	case SPdAPIId::_4DSDP006:
		sendDocument.AddMember("Name", std::string(_msgDocument["Name"].GetString()), sendDocument.GetAllocator());
		sendDocument.AddMember("Status", std::string(_msgDocument["Status"].GetString()), sendDocument.GetAllocator());
		break;
	case SPdAPIId::_4DSDP007:
		sendDocument.AddMember("ResultArray", _msgDocument["ResultArray"].GetArray(), sendDocument.GetAllocator());
		break;
	case SPdAPIId::_4DSDP008:
		//TODO: send message
		sendDocument.AddMember("Version", _msgDocument["Version"].GetObjectW(), sendDocument.GetAllocator());
		break;
	}

	return ESMBase::ERR_CODE_T::SUCCESS;
}

void CreateCommonMessage(rapidjson::Document& _mapDocument, const int& _tokenKey, const SPdAPIId& _apiid, const int& _resultCode, const std::string& _resultMsg)
{
	_mapDocument.SetObject();
	_mapDocument.AddMember("apiid", (int)_apiid, _mapDocument.GetAllocator());
	_mapDocument.AddMember("tokenkey", _tokenKey, _mapDocument.GetAllocator());
	_mapDocument.AddMember("ResultCode", _resultCode, _mapDocument.GetAllocator());
	_mapDocument.AddMember("ErrorMsg", _resultMsg, _mapDocument.GetAllocator());
}

void CreateMakeMovieMessage(rapidjson::Document& _mapDocument, const int _tokenKey, const SPdAPIId _apiId, const MTdProtocol_4DSDP003 _st4DSDP003)
{
	_mapDocument.SetObject();
	_mapDocument.AddMember("apiid", (int)_apiId, _mapDocument.GetAllocator());
	_mapDocument.AddMember("tokenkey", _tokenKey, _mapDocument.GetAllocator());
	_mapDocument.AddMember("Name", _st4DSDP003.name, _mapDocument.GetAllocator());
	_mapDocument.AddMember("RecordName", _st4DSDP003.recordname, _mapDocument.GetAllocator());
	_mapDocument.AddMember("MarkerTime", _st4DSDP003._MarkerTime, _mapDocument.GetAllocator());
	_mapDocument.AddMember("Template", _st4DSDP003._template, _mapDocument.GetAllocator());
	_mapDocument.AddMember("PT_X1", _st4DSDP003._PT.X1, _mapDocument.GetAllocator());
	_mapDocument.AddMember("PT_Y1", _st4DSDP003._PT.Y1, _mapDocument.GetAllocator());
	_mapDocument.AddMember("PT_X2", _st4DSDP003._PT.X2, _mapDocument.GetAllocator());
	_mapDocument.AddMember("PT_Y2", _st4DSDP003._PT.Y2, _mapDocument.GetAllocator());
	_mapDocument.AddMember("prefix", _st4DSDP003._prefix, _mapDocument.GetAllocator());
	_mapDocument.AddMember("gimbal", _st4DSDP003._gimbal, _mapDocument.GetAllocator());
}

int32_t ParseFileAdjust(const std::string _data, std::map<std::string, adjustInfo>& mapAdjustInfo, std::string& id)
{
	rapidjson::Document document;
	document.Parse(_data);
	rapidjson::Value& object = document;
	int32_t errorCode = ESMBase::ERR_CODE_T::UNKNOWN;

	try
	{
		//std::string id;
		JsonValueValidation(object, "AdjustID", id);

		// AdjustData
		if (object.HasMember("AdjustData"))
		{
			rapidjson::Value& object_itr = object["AdjustData"];
			for (rapidjson::Value::ValueIterator itr = object_itr.Begin(); itr != object_itr.End(); ++itr)
			{
				adjustInfo adjustData;

				rapidjson::Value& oobject = itr->GetObjectW();
				JsonValueValidation(oobject, "DscID", adjustData.strDscId);
				JsonValueValidation(oobject, "Mode", adjustData.strMode);

				if (!oobject.HasMember("Adjust"))
				{
					//SPd_ERROR(ERROR_MSG_FORMAT + "Parse Adjust", errorCode);
					return errorCode;
				}
				rapidjson::Value& ooobject = oobject["Adjust"];
				JsonValueValidation(ooobject, "dAdjustX", adjustData.dAdjustX);
				JsonValueValidation(ooobject, "dAdjustY", adjustData.dAdjustY);
				JsonValueValidation(ooobject, "dAngle", adjustData.dAngle);
				JsonValueValidation(ooobject, "dRotateX", adjustData.dRotateX);
				JsonValueValidation(ooobject, "dRotateY", adjustData.dRotateY);
				JsonValueValidation(ooobject, "dScale", adjustData.dScale);

				if (!ooobject.HasMember("rtMargin"))
				{
					//SPd_ERROR(ERROR_MSG_FORMAT + "Parse rtMargin", errorCode);
					return errorCode;
				}
				rapidjson::Value& oooobject = ooobject["rtMargin"];
				JsonValueValidation(oooobject, "X", adjustData.dMarginX);
				JsonValueValidation(oooobject, "Y", adjustData.dMarginY);
				JsonValueValidation(oooobject, "Width", adjustData.dMarginW);
				JsonValueValidation(oooobject, "Height", adjustData.dMarginH);

				JsonValueValidation(oobject, "flip", adjustData.bFlip);
				JsonValueValidation(oobject, "UseLogo", adjustData.bUseLogo);

				if (!oobject.HasMember("pts_3d"))
				{
					//SPd_ERROR(ERROR_MSG_FORMAT + "Parse pts_3d", errorCode);
					return errorCode;
				}

				rapidjson::Value& oopts = oobject["pts_3d"];
				JsonValueValidation(oopts, "X1", adjustData.nX1);
				JsonValueValidation(oopts, "X2", adjustData.nX2);
				JsonValueValidation(oopts, "X3", adjustData.nX3);
				JsonValueValidation(oopts, "X4", adjustData.nX4);
				JsonValueValidation(oopts, "Y1", adjustData.nY1);
				JsonValueValidation(oopts, "Y2", adjustData.nY2);
				JsonValueValidation(oopts, "Y3", adjustData.nY3);
				JsonValueValidation(oopts, "Y4", adjustData.nY4);
				JsonValueValidation(oopts, "CenterX", adjustData.nCenterX);
				JsonValueValidation(oopts, "CenterY", adjustData.nCenterY);

				if (oobject.HasMember("homo_T"))
				{
					rapidjson::Value& homo = oobject["homo_T"];

					if (homo.HasMember("r1"))
						JsonValueValidationArray(homo, "r1", adjustData.r1);
					if (homo.HasMember("r2"))
						JsonValueValidationArray(homo, "r2", adjustData.r2);
					if (homo.HasMember("r3"))
						JsonValueValidationArray(homo, "r3", adjustData.r3);
				}
				mapAdjustInfo.insert(make_pair(adjustData.strDscId, adjustData));
			}
		}

	}
	catch (std::map<int32_t, std::string> me)
	{
		int32_t expCode = ESMBase::ERR_CODE_T::UNKNOWN;
		for (auto iter : me)
		{
			//SPd_ERROR(ERROR_MSG_FORMAT + iter.second, iter.first);
			expCode = iter.first;
		}

		me.clear();
		return expCode;
	}
	return ESMBase::ERR_CODE_T::SUCCESS;
}
