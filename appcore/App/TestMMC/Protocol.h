﻿#pragma once
#define RAPIDJSON_HAS_STDSTRING 1
#include "define.h"
//#include "Logger.h"
//#include <VMSDK.h>
#include <iostream>
#include <fstream>
#include <map>
#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>
#include <rapidjson/ostreamwrapper.h>


enum class SPdAPIId : int
{
    NONE = -1,
    _4DSDP001 = 1,
    _4DSDP002,
    _4DSDP003,
    _4DSDP004,
    _4DSDP005,
    _4DSDP006,
    _4DSDP007,
    _4DSDP008,
    _4DSDP009,
    _4DSDP010,
    _4DSDP011,
    _4DSDP012,
};


enum class MTdProtocolCode : int
{
    _SUCESS = 1000,
    _ERROR
};

struct MTdProtocol
{
    std::string Section1;
    std::string Section2;
    std::string Section3;
    std::string SendState;
    std::string Token;
    std::string From;
    std::string To;
    std::string Action;
};

struct MTdProtocol_Response : MTdProtocol
{
    void Init(MTdProtocol mtdProtocol)
    {
        Section1 = mtdProtocol.Section1;
        Section2 = mtdProtocol.Section2;
        Section3 = mtdProtocol.Section3;
        SendState = mtdProtocol.SendState;
        Token = mtdProtocol.Token;
        From = mtdProtocol.From;
        To = mtdProtocol.To;
        Action = mtdProtocol.Action;
    };
    int resultcode;
    std::string errormsg;
};

struct Camera
{
    int index{};
    std::string prefix;
    std::vector<int> pathid;
    bool flip{};
};
struct Path
{
    int id{};
    std::string path;
};
struct VSPd
{
    int port{};
    std::string address;
};

struct reencoding
{
    bool use{};
    int bitrate{};
    bool ts{};
};
/// <summary>
/// Option object structure obtained from MTdProtocol_4DSDP001
/// </summary>
struct Option
{
    reencoding _reencoding;
    bool uhdtofhd{};
    int camfps{};
    int fps{};
    int waittime{};
};
struct MTdProtocol_4DSDP001 : MTdProtocol
{
    Option option;
    std::vector<Camera> cameras;
    std::vector<Path> paths;
    std::string vspdServerAddress;
    std::vector<VSPd> vspds;
};

struct rtMargin
{
    double _X{};
    double _Y{};
    double _Width{};
    double _Height{};
};
struct Adjust
{
    int    _imageWidth{};
    int    _imageHeight{};
    double _normAdjustX{};
    double _normAdjustY{};
    double _normRotateX{};
    double _normRotateY{};
    bool   _flip{};
    bool   _useLogo{};

    double _dAdjuxtX{};
    double _dAdjuxtY{};
    double _dAngle{};
    double _dRotateX{};
    double _dRotateY{};
    double _dScale{};
    rtMargin _rtMargin;
};

struct PositionTracking3D
{
    int _X1{};
    int _Y1{};
    int _X2{};
    int _Y2{};
    int _X3{};
    int _Y3{};
    int _X4{};
    int _Y4{};
    int _CenterX{};
    int _CenterY{};
};

struct HomoT
{
    double r1[3];
    double r2[3];
    double r3[3];
};

struct AdjustData
{
    int _liveIdx;
    int _replayIdx;
    std::string _DscId;
    std::string _Mode;
    Adjust _Adjust;
    bool _flip{};
    bool _UseLogo{};
    PositionTracking3D _pts3d;
    HomoT _homo_t;
};


#pragma pack(push, 1)
struct ServoData {    
    int nCamCh;
    int nFrameSize;
    char path[2048];
    char gimbalIp[64];
};
#pragma pack(pop)  

struct MTdProtocol_4DSDP002 : MTdProtocol
{
    std::string _AdjustId;
    std::vector<AdjustData> _AdjustDatas;
};


struct PT
{
    int X1{ -1 };
    int Y1{ -1 };
    int X2{ -1 };
    int Y2{ -1 };
    std::string _prefix;
};

/// <summary>
/// Contains data from the MTD protocol '4DSPD003'
/// </summary>
/// <remarks>
/// In this case of the absence of the 'debugmode' member in the JSON protocol,
/// the server process will not save the 'DebugMode' key value in the 'config.ini' file.
/// That's the reason why the 'hasdebugmode' member variable is required.
/// If it is 'false', the server process will not save in the INI file, 
/// so the server process can retain the value in the INI file.
/// </remarks>
struct MTdProtocol_4DSDP003 : MTdProtocol
{
    std::string name;
    std::string recordname;
    int _MarkerTime{};
    std::string _template;
    PT _PT;
    std::string _prefix;
    bool _gimbal;
    bool hasdebugmode{}; // If the JSON protocal has 'debugmode', it is 'true'. Otherwise, it is 'false'.
    bool debugmode{};
};

struct PlayList
{
    int id{};
    std::string name;
    bool made{};
    std::string path;
};
struct MTdProtocol_4DSDP004 : MTdProtocol
{
    std::vector<PlayList> playlists;
    int strtid{};
    bool uselogo{};
    std::string logopath;
    int repeat{};
    int count{};
};

struct MTdProtocol_4DSDP005 : MTdProtocol
{
};

struct MTdProtocol_4DSDP006 : MTdProtocol
{
    std::vector<PlayList> playlists;
    int strtid{};
    bool uselogo{};
    std::string logopath;
    int repeat{};
    int count{};
};

struct DelList
{
    std::string name;
    std::string path;
};
struct MTdProtocol_4DSDP007 : MTdProtocol
{
    std::vector<DelList> dellists;
};

struct MTdProtocol_4DSDP008 : MTdProtocol
{
};

struct MTdProtocol_4DSDP009 : MTdProtocol
{
};

struct Gimbal {
    std::string preset;
    std::string pos_x;
    std::string pos_y;
    std::string pos_z;
    std::string zoom;
    std::string ip;
};

struct Preset
{
    std::string cam_index;
    int32_t cam_preset{};
    std::string preset_image;
    std::string preset_update_frame;
    bool flip;
    Gimbal gimbal;
};

struct MTdProtocol_4DSDP010 : MTdProtocol
{
    bool ptz;
    std::vector<Preset> presetList;
    std::vector<AdjustData> adjustList;
};

typedef struct
{
    std::string strDscId;
    std::string strMode;

    double dAdjustX;
    double dAdjustY;
    double dAngle;
    double dRotateX;
    double dRotateY;
    double dScale;
    double dMarginX;
    double dMarginY;
    double dMarginW;
    double dMarginH;
    int nX1;
    int nY1;
    int nX2;
    int nY2;
    int nX3;
    int nY3;
    int nX4;
    int nY4;

    int nCenterX;
    int nCenterY;

    bool   bFlip;
    bool   bUseLogo;

    double r1[3];
    double r2[3];
    double r3[3];

}adjustInfo;

class Protocol
{
public:
    ~Protocol();
    Protocol();

    rapidjson::Document     m_document;
    SPdAPIId                m_eSPdAPIId;
    MTdProtocol             m_stMTdProtocol;

    int32_t ParseConfigSetting(MTdProtocol_4DSDP001& _st4DSDP001);
    int32_t ParseAdjustSetting(MTdProtocol_4DSDP002& _st4DSDP002);
    int32_t ParseMakeMovie(MTdProtocol_4DSDP003& _st4DSDP003);
    int32_t ParseStartSDI(MTdProtocol_4DSDP004& _st4DSDP004);
    int32_t ParseStopSDI(MTdProtocol_4DSDP005& _st4DSDP005);
    int32_t ParseModifiyListSDI(MTdProtocol_4DSDP006& _st4DSDP006);
    int32_t ParseDelMovie(MTdProtocol_4DSDP007& _st4DSDP007);
    int32_t ParseGetVersion(MTdProtocol_4DSDP008& _st4DSDP008);
    int32_t ParseGetAdjustID(MTdProtocol_4DSDP009& _st4DSDP009);
    int32_t ParseAdjustData(MTdProtocol_4DSDP002& _st4DSDP002);
    int32_t ParseVisualServoing(MTdProtocol_4DSDP010& _st4DSDP010);
    int32_t MessageProtocol(const std::string msgBody);
    int32_t JsonFileWrite(std::string _path);
    int32_t JsonBasicParse(const std::string body, MTdProtocol& mtdProtocol);
    int32_t JsonToString(rapidjson::Document& _document, std::string& message);
    
};


int32_t ParseFileAdjust(const std::string _data, std::map<std::string, adjustInfo>& mapAdjustInfo, std::string & id);
int32_t ParseSPdAPIId(const rapidjson::Document& _document, SPdAPIId& apiID);
int32_t JsonCreate(MTdProtocol& _mtdProtocol, rapidjson::Document& document);
int32_t JsonSerialize(const rapidjson::Document& _document, std::string& serializeMsg);
int32_t SetSendProtocol(const SPdAPIId& _apiId, rapidjson::Document& _msgDocument, rapidjson::Document& sendDocument);
void CreateCommonMessage(rapidjson::Document& _mapDocument, const int& _tokenKey, const SPdAPIId& _apiid, const int& _resultCode, const std::string& _resultMsg);
void CreateMakeMovieMessage(rapidjson::Document& _mapDocument, const int _tokenKey, const SPdAPIId _apiId, const MTdProtocol_4DSDP003 _st4DSDP003);



const std::string ERROR_MSG_FORMAT = "Error Code {} :: ";
// Section 1
const std::string JSON_SECTION_1_4DREPLAY = "4DReplay";
const std::string JSON_SECTION_1_SPD = "SPd";
const std::string JSON_SECTION_1_DAEMON = "Daemon";
const std::string JSON_SECTION_1_CAMERA = "Camera";

// Section 2
const std::string JSON_SECTION_2_CONNECT = "Connect";
const std::string JSON_SECTION_2_ADJUST = "Adjust";
const std::string JSON_SECTION_2_GetADJUSTID = "GetAdjustID";
const std::string JSON_SECTION_2_MAKEMOVIE = "MakeMovie";
const std::string JSON_SECTION_2_STARTSDI = "StartSDI";
const std::string JSON_SECTION_2_STOPSDI = "StopSDI";
const std::string JSON_SECTION_2_MODIFYLISTSDI = "ModifyListSDI";
const std::string JSON_SECTION_2_DELMOVIE = "DelMovie";
const std::string JSON_SECTION_2_INFORMATION = "Information";
const std::string JSON_SECTION_2_MOVETOPRESET = "MoveToPreset";
const std::string JSON_SECTION_2_RECTIFICATION = "Rectification";
const std::string JSON_SECTION_2_STOPPRESETMOVE = "StopPresetMove";

// Section 3
const std::string JSON_SECTION_3_SET = "Set";
const std::string JSON_SECTION_3_VERSION = "Version";

// SendState
const std::string JSON_SENDSTATE_REQUEST = "request";
const std::string JSON_SENDSTATE_RESPONSE = "response";

// Aection                                    
const std::string JSON_ACTION_SET = "set";
const std::string JSON_ACTION_RUN = "run";

const std::string JSON_RESULTCODE = "ResultCode";
const std::string JSON_ERRORMSG = "ErrorMsg";

const std::string JSON_CAMERAS = "cameras";