#pragma once

#include <vector>
#include <map>
#include <string>
#include <filesystem>

//#include "VMError.h"

const std::vector<std::string> mvtmKeyTokens
{
	"command", // Newly added for MPPC (Note: The value of "command" can be added as much as you need.)
	"starttime",
	"endtime",
	"start_dsc",
	"end_dsc",
	"timeframe",
	"VMCC_COUNT",
	"VMCC_0",
	"VMCC_1"
};

// Warning: All command values should be the lowercase text.
const std::vector<std::string> mvtmCommandValueTokens
{
	"recal",
	"recalibration"
};

struct pathInfo
{
	std::string srcMvtmPath;
	std::string srcMvpmPath;
	std::filesystem::path srcCsvPath;
	std::string srcVideoDirPath;
	std::string dstMoviePath;
	std::map<std::string, std::vector<std::filesystem::path>> srcCamPath; // Key:Prefix / Value:PreSD Dir
};

struct cameraInfo
{
	std::map<int, std::string> camPrefix; // Key:ID / Value:Prefix
	std::map<std::string, int> camIdx; // Key:Prefix / Value:ID
};

struct VmVMCC
{
	double time_target{};
	std::pair<double, double> center; // first:x / second:y
	double zoom{};
	double time_scale{};
};

struct VmPeriod
{
	int starttime{};
	int start_cam_idx{};
	int endtime{};
	int end_cam_idx{};
	int timeframe{};
	bool mppc{};
	int vmcc_cnt{};
	std::vector<VmVMCC> vmcc;
	std::string serial_effect;		// Serializing JSON ArrayType to String.
};

struct mvtmInfo
{
	int n_period{};
	std::map<float, int> sectionMap;
	std::map<int, VmPeriod> period; // Key: Period #
};

struct vmccInfo
{
	bool bPosTracking{};
	std::map<int, std::pair<double, double>> frame_center; // Key: Frame Count by Period (Zero-based) / Value: x, y
	std::map<int, double> frame_zoom_ratio; // Key: Frame Count by Period (Zero-based)
};

struct _adjustInfo
{
	std::string strDscId;
	std::string strMode;

	double dAdjustX;
	double dAdjustY;
	double dAngle;
	double dRotateX;
	double dRotateY;
	double dScale;
	double dMarginX;
	double dMarginY;
	double dMarginW;
	double dMarginH;
	int nX1;
	int nY1;
	int nX2;
	int nY2;
	int nX3;
	int nY3;
	int nX4;
	int nY4;

	int nCenterX;
	int nCenterY;

	bool bFlip;
	bool bUseLogo;

	double r1[3];
	double r2[3];
	double r3[3];
};

const std::string WHITESPACE = " \n\r\t\f\v";

struct SourceFrameInfo
{
	std::string camPrefix;	// Camera Prefix
	int time{};				// Elapsed Time (Unit:second)
	int frame{};			// Frame #

	void Modify(int srcFPS) {
		if (srcFPS == 25) {
			int remainder = time % 2;
			if (remainder != 0)	{
				time -= remainder;
				frame += srcFPS;
			}
		}
	};
};

struct MvtmInfo
{
	int period{};	// MVTM Period
	int frame{};	// Frame Index
};

struct DestinationFrameInfo
{
	int count{};		// The frame count of 'srcFrame'
	bool rotation{};	// True if the frame is in rotation period.
	bool mppc{};		// True if the frame is in MPPC period.
	std::string effect;
	ESMBase::FrameType type{ ESMBase::FrameType::Normal };
};

// << About srcFrame >>
// 
// * Key : MVTM Period #
// * Value : Vector of SourceFrameInfo
//   => Vector Index : Frame Index (0-based)
//      - Note: Frame count is stored in the dstFrame.
//              (Practically, the value of the dstFrame is the same as the size of the srcFrame value(as vector).)
struct designatedInfo
{
	int srcFPS{};
	int dstFPS{};
	std::map<int, std::vector<SourceFrameInfo>> srcFrame;
	std::map<int, MvtmInfo> srcMatchingMVTM;				// Key: [MVTM Period] * [Frame Index]
	std::map<int, std::pair<int, int>> srcFrameStartEnd;	// Key: 1-based index / Value: first - start frame#, second - end frame#
	std::map<int, DestinationFrameInfo> dstFrame;			// Key: MVTM Period
	//std::map<int, decodeHW> srcDecodeHW;
};

struct SourceEffectInfo
{
	std::string effect;
	std::pair<int, int> frameRange;
};


struct srcVideoFFmpegInfo
{
	std::map<int, SourceFrameInfo> matchingFrameInfo;	// Key: [MVTM Period] * [Frame Index]
	std::map<int, std::filesystem::path> srcFile;		// Key: [MVTM Period] * [Frame Index] / Value: Specific PreSD's full file name.
	std::map<int, bool> srcIsRotation;					// Key: [MVTM Period] * [Frame Index] / Value: The rotation information. if the value is 'true', this period is the rotation one.
	std::map<int, bool> srcIsMppc;						// Key: [MVTM Period] * [Frame Index] / Value: MPPC information. if the value is 'true', this period is the MPPC period.
	SourceEffectInfo* srcEffect{ nullptr };				// effect data
	std::map<int, int> srcTimeFrame;					// Key: [MVTM Period] * [Frame Index] / Value: 'timeframe' of MVTM by period
	std::map<int, ESMBase::FrameType> srcType; // Key: [MVTM Period] * [Frame Index]
	int srcFPS{};
	~srcVideoFFmpegInfo()
	{
		if (srcEffect)
			delete srcEffect;
	}
};
