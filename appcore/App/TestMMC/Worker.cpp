#pragma warning(disable:26812)
#define __STDC_CONSTANT_MACROS


#include "Worker.h"
#include "ESMBase.h"

#define RAPIDJSON_HAS_STDSTRING 1

#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>
#include <rapidjson/ostreamwrapper.h>

Worker::Worker() {
	_run = true;
	_thread = new std::thread(&Worker::TaskMaking, this, this);

	// temp code
	//_outPath = "c:\\test_contents\\mmc_out.264";
}

Worker::~Worker() {
	_run = false;
	if (_thread != nullptr) {
		_thread->join();
		delete _thread;
		_thread = nullptr;
	}
}

void Worker::SetUseDebugNumber(bool enable) {
	_debug = enable;
}

void Worker::PutMakingMsg(const std::string& msg) {
	_qProcessing.Enqueue(msg);
}

void* Worker::TaskMaking(void* arg) {
	ESMMovieMaker* pMovieMaker = new ESMMovieMaker();
	ESMMovieMaker::CONTEXT_T* tMovieCtx = new ESMMovieMaker::CONTEXT_T();
	ESMMovieMaker::PARAM_T* tMovieParam = new ESMMovieMaker::PARAM_T();
	// add AR Data..
	//tMovieParam->ARData = &_ARData;
	std::string num;
	std::string camid;

	while (_run) {
		if (_qProcessing.IsQueue()) {
			std::string msg = _qProcessing.Dequeue();
			rapidjson::Document document;
			document.Parse(msg);

			tMovieCtx->handler = this;
			tMovieCtx->gimbal = document["gimbal"].GetBool() ? TRUE : FALSE;
			tMovieCtx->width = document["dstwidth"].GetInt();
			tMovieCtx->height = document["dstheight"].GetInt();
			tMovieCtx->fps_num = document["dstfps"].GetInt();

			if (document.HasMember("debug"))
				SetUseDebugNumber(document["debug"].GetBool() ? TRUE : FALSE);

			// TODO: Currently MMC uses "name" attribute to find out
			//       if it needs to call the ESMMovieMaker::Initialize() method or not.
			//       In most cases it works well. However, it is not an ideal solution
			//       because "name" is not for this use.
			//
			//       Therefore, consider to add another attribute ("init" e.g.).

			std::string name{ document["name"].GetString() };
			if (_name != name) {
				_name = name;

				if (pMovieMaker->IsInitialized())
					pMovieMaker->Release();

				pMovieMaker->Initialize(tMovieCtx);
			}
			else if (!pMovieMaker->IsInitialized())
				pMovieMaker->Initialize(tMovieCtx);

			
			tMovieParam->srcFile = document["filepath"].GetString();
			tMovieParam->dstFilePath, document["name"].GetString();
			tMovieParam->nStartFrameNum = document["startframe"].GetInt();
			tMovieParam->nEndFrameNum = document["endframe"].GetInt();
			tMovieParam->fWantedFPS = (float)document["srcfps"].GetInt();
			//tMovieParam->nSrcGopSize = document["srcfps"].GetInt() == 60 ? 12 : 10;
			tMovieParam->bUseExtradata = document["useextradata"].GetBool() ? TRUE : FALSE;
			tMovieParam->bDebugNumber = _debug;
			tMovieParam->frameType = ESMBase::FrameType(document["frametype"].GetInt());

			LOG_I("[Worker] movieParam: startframe(%d), endframe(%d), filepath(%s)", tMovieParam->nStartFrameNum, tMovieParam->nEndFrameNum, tMovieParam->srcFile);
			

			tMovieParam->ARData.Clear();
			auto itEffect = document.FindMember("effect");
			if (itEffect == document.MemberEnd())
				LOG_W("No effect data has been received.");
			else
			{
				JsonObjectToARData(itEffect->value, tMovieParam->ARData);
				tMovieParam->ARData.frame = std::make_pair(document["effectstartframe"].GetInt(), document["effectendframe"].GetInt());

				LOG_I("[Worker] ArData effect startframe(%d), endframe(%d)", tMovieParam->ARData.frame.first, tMovieParam->ARData.frame.second);
			}
			tMovieParam->pointInfo.clear();

			rapidjson::Value::ValueIterator it_frameinfo = document["frameinfo"].Begin();
			for (int i = tMovieParam->nStartFrameNum; i <= tMovieParam->nEndFrameNum; ++i) {
				num = std::to_string(i);

				rapidjson::Value& obj_frameinfo = it_frameinfo->GetObjectW();
				camid = obj_frameinfo[num]["prefix"].GetString();

				tMovieParam->frameInfo[i].x = obj_frameinfo[num]["x"].GetInt();
				tMovieParam->frameInfo[i].y = obj_frameinfo[num]["y"].GetInt();
				tMovieParam->frameInfo[i].zoom = obj_frameinfo[num]["zoom"].GetInt();
				tMovieParam->frameInfo[i].prefix = camid;
				tMovieParam->frameInfo[i].matchingCamPrefix = obj_frameinfo[num]["matchingcamprefix"].GetString();
				tMovieParam->frameInfo[i].matchingTime = obj_frameinfo[num]["matchingtime"].GetInt();
				tMovieParam->frameInfo[i].matchingFrameIdx = obj_frameinfo[num]["matchingframeidx"].GetInt();

				tMovieParam->adjustInfo[i].strDscId = _adjustInfo[camid].strDscId;
				tMovieParam->adjustInfo[i].strMode = _adjustInfo[camid].strMode;
				tMovieParam->adjustInfo[i].bFlip = _adjustInfo[camid].bFlip;
				tMovieParam->adjustInfo[i].dAdjustX = _adjustInfo[camid].dAdjustX;
				tMovieParam->adjustInfo[i].dAdjustY = _adjustInfo[camid].dAdjustY;
				tMovieParam->adjustInfo[i].dAngle = _adjustInfo[camid].dAngle;
				tMovieParam->adjustInfo[i].dMarginH = _adjustInfo[camid].dMarginH;
				tMovieParam->adjustInfo[i].dMarginW = _adjustInfo[camid].dMarginW;
				tMovieParam->adjustInfo[i].dMarginX = _adjustInfo[camid].dMarginX;
				tMovieParam->adjustInfo[i].dMarginY = _adjustInfo[camid].dMarginY;
				tMovieParam->adjustInfo[i].dRotateX = _adjustInfo[camid].dRotateX;
				tMovieParam->adjustInfo[i].dRotateY = _adjustInfo[camid].dRotateY;
				tMovieParam->adjustInfo[i].dScale = _adjustInfo[camid].dScale;
				tMovieParam->adjustInfo[i].nCenterX = _adjustInfo[camid].nCenterX;
				tMovieParam->adjustInfo[i].nCenterY = _adjustInfo[camid].nCenterY;
				tMovieParam->adjustInfo[i].nX1 = _adjustInfo[camid].nX1;
				tMovieParam->adjustInfo[i].nY1 = _adjustInfo[camid].nY1;
				tMovieParam->adjustInfo[i].nX2 = _adjustInfo[camid].nX2;
				tMovieParam->adjustInfo[i].nY2 = _adjustInfo[camid].nY2;
				tMovieParam->adjustInfo[i].nX3 = _adjustInfo[camid].nX3;
				tMovieParam->adjustInfo[i].nY3 = _adjustInfo[camid].nY3;
				tMovieParam->adjustInfo[i].nX4 = _adjustInfo[camid].nX4;
				tMovieParam->adjustInfo[i].nY4 = _adjustInfo[camid].nY4;
				for (int32_t x = 0; x < 3; x++)	{
					tMovieParam->adjustInfo[i].homo.r1[x] = _adjustInfo[camid].r1[x];
					tMovieParam->adjustInfo[i].homo.r2[x] = _adjustInfo[camid].r2[x];
					tMovieParam->adjustInfo[i].homo.r3[x] = _adjustInfo[camid].r3[x];
				}

				// world test..
				tMovieParam->pointInfo[camid] = _pointData.preset[_pointData.currentPresetNumber].points[camid];
				tMovieParam->pointInfo[camid].world_coords = _pointData.preset[_pointData.currentPresetNumber].worlds[camid].world_coords;
				
				
				

				/*if (obj_frameinfo[num]["mppc"].GetBool()) {
					if (tMovieParam->pointInfo.find(camid) == tMovieParam->pointInfo.end())
					{
						auto itPreset = _pointData.preset.find(_pointData.currentPresetNumber);
						if (itPreset == _pointData.preset.end())
							SPd_WARN("VideoMaker has no Preset#{}.", _pointData.currentPresetNumber);
						else
						{
							std::error_code ec;
							auto itPoints = itPreset->second.points.find(camid);
							if (itPoints == itPreset->second.points.end())
								SPd_WARN("Preset#{} in VideoMaker has no points data of camid:{}.",
									_pointData.currentPresetNumber, camid);
							else if (itPoints->second.presd_path.empty())
								SPd_WARN("points(camid:{}).presd_path is empty.", camid);
							else if (std::filesystem::exists(itPoints->second.presd_path, ec))
							{
								SPd_DEBUG("points(camid:{}).presd_path = {}", camid, itPoints->second.presd_path);
								tMovieParam->pointInfo[camid] = itPoints->second;
							}
							else
								SPd_WARN("points(camid:{}).presd_path:{} does not exist. [error_code.message:{}]",
									camid, itPoints->second.presd_path, ec.message());
						}
					}
				}*/
				++it_frameinfo;

				LOG_I("[Worker] frameIdx(%d) matchingframeidx(%d), camPrefix(%s)", i, tMovieParam->frameInfo[i].matchingFrameIdx, tMovieParam->frameInfo[i].prefix);

			}
			pMovieMaker->Process(tMovieParam);
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}

	delete pMovieMaker;
	delete tMovieCtx;
	delete tMovieParam;

	pMovieMaker = nullptr;
	tMovieCtx = nullptr;
	tMovieParam = nullptr;

	return nullptr;
}


void Worker::OnExtraData(const uint8_t* bitstream, int32_t size, const std::string& name) {
	std::shared_ptr<ESMMovieMaker::INTER_MMSC_PACKET_T> pkt = std::shared_ptr<ESMMovieMaker::INTER_MMSC_PACKET_T>(new ESMMovieMaker::INTER_MMSC_PACKET_T);

	ESMMovieMaker::COMMAND_MAKE_T cmd = {};
	cmd.command = ESMMovieMaker::INTER_COMM_MODE_T::MAKE;
	cmd.status = ESMMovieMaker::ERR_CODE_T::SUCCESS;
	cmd.frameNumber = ESMMovieMaker::EXTRA_DATA;
	cmd.length = size;
	strcpy_s(cmd.name, _countof(cmd.name), name.c_str());

	pkt->type = ESMMovieMaker::INTER_MMSC_PACKET_TYPE_T::BINARY;
	pkt->length = sizeof(cmd) + cmd.length;
	pkt->payload = static_cast<char*>(malloc(pkt->length));
	memset(pkt->payload, 0x00, pkt->length);

	memmove(pkt->payload, &cmd, sizeof(cmd));
	memmove(pkt->payload + sizeof(cmd), bitstream, cmd.length);

	//_qCompletedMSG.Enqueue(pkt);
	if (_cbStatus) {
		_cbStatus((void*)bitstream, size, 0);
	}

	/*_file.open(_outPath, std::ios::binary | std::ios::out);
	if (!_file.is_open()) {	
		return;
	}
	_file.write((const char*)bitstream, size);*/
	
}

void Worker::OnEncodingComplete(MppcPoints* points, int32_t status, int32_t frameIdx, const uint8_t* bitstream, int32_t size, const std::string& name) {
	if (points)	{
		//try {
		//	if (points->dsc_id.empty())	{
		//		SPd_WARN("{}() : points->dsc_id is empty.", __func__);
		//		return;
		//	}

		//	// Arrange Result Data
		//	MppcPoints pointsResult = *points;
		//	pointsResult.framenum = frameIdx;

		//	rapidjson::Document doc;
		//	doc.SetObject();

		//	rapidjson::Value obj_points(rapidjson::kObjectType);
		//	MppcPointsToJsonObject(pointsResult, obj_points, doc.GetAllocator());
		//	ApplyMppcPointsResultToJsonObect(pointsResult, obj_points, doc.GetAllocator());

		//	doc.AddMember("points", obj_points, doc.GetAllocator());

		//	// When sending JSON data to MMS or MMC, "mode" member should be added.
		//	doc.AddMember("mode", ESMMovieMaker::INTER_COMM_MODE_T::MPPC, doc.GetAllocator());

		//	std::string msg;
		//	JsonSerialize(doc, msg);

		//	SPd_DEBUG("[MMC->MMS] PointData :\n{}", msg);

		//	// Send PointData to MMS
		//	auto pkt = std::make_shared<ESMMovieMaker::INTER_MMSC_PACKET_T>();
		//	pkt->length = msg.length();
		//	pkt->payload = static_cast<char*>(malloc(pkt->length + 1));
		//	memset(pkt->payload, 0x00, pkt->length + 1);
		//	memmove(pkt->payload, msg.c_str(), pkt->length);
		//	_qCompletedMSG.Enqueue(pkt);
		//}
		//catch (std::exception& e)
		//{
		//	SPd_ERROR("Exception: {}", e.what());
		//}
		//catch (...)
		//{
		//	SPd_CRITICAL("Exception: Unknown");
		//}
	}
	else
	{
		auto pkt = std::make_shared<ESMMovieMaker::INTER_MMSC_PACKET_T>();

		ESMMovieMaker::COMMAND_MAKE_T cmd = {};
		cmd.command = ESMMovieMaker::INTER_COMM_MODE_T::MAKE;
		cmd.status = status;
		cmd.frameNumber = frameIdx + 1;
		cmd.length = size;
		strcpy_s(cmd.name, _countof(cmd.name), name.c_str());

		pkt->type = ESMMovieMaker::INTER_MMSC_PACKET_TYPE_T::BINARY;
		pkt->length = sizeof(cmd) + cmd.length;
		pkt->payload = static_cast<char*>(malloc(pkt->length));
		memset(pkt->payload, 0x00, pkt->length);

		memmove(pkt->payload, &cmd, sizeof(cmd));
		memmove(pkt->payload + sizeof(cmd), bitstream, cmd.length);

		if (_cbStatus) {
			if (bitstream == nullptr)
				_cbStatus((void*)bitstream, size, 2);
			else
				_cbStatus((void*)bitstream, size, 1);
		}

		//_qCompletedMSG.Enqueue(pkt);
		if (_file.is_open()) {	
			_file.write((const char*)bitstream, size);
		}
	}
}

void Worker::onMovieMakerError(int32_t code, const char* error)
{
	char strCode[MAX_PATH] = { 0 };
	_itoa(code, strCode, 10);

	std::string msg = "";
	rapidjson::Document doc;
	doc.SetObject();
	doc.AddMember("mode", ESMMovieMaker::INTER_COMM_MODE_T::MMC_ERROR, doc.GetAllocator());
	doc.AddMember("status", std::string(strCode), doc.GetAllocator());
	doc.AddMember("error", std::string(error), doc.GetAllocator());
	//JsonSerialize(doc, msg);

	std::shared_ptr<ESMMovieMaker::INTER_MMSC_PACKET_T> pkt = std::shared_ptr<ESMMovieMaker::INTER_MMSC_PACKET_T>(new ESMMovieMaker::INTER_MMSC_PACKET_T);
	pkt->type = ESMMovieMaker::INTER_MMSC_PACKET_TYPE_T::TEXT;
	pkt->length = msg.length();
	pkt->payload = static_cast<char*>(malloc(pkt->length + 1));
	memset(pkt->payload, 0x00, pkt->length + 1);
	memmove(pkt->payload, msg.c_str(), pkt->length);
	//_qCompletedMSG.Enqueue(pkt);

	if (_cbStatus) {
		_cbStatus(nullptr, 0, -1);
	}
}

void Worker::SetWorldCoords(int presetNum, MppcPositionPreset& preset) {
	_pointData.currentPresetNumber = presetNum;
	
	//_pointData.preset.insert(std::make_pair(presetNum, preset));
	_pointData.preset[presetNum] = preset;
}


void Worker::SetEffectData(const std::string& msg) {
	try {
		rapidjson::Document doc;
		doc.Parse(msg);
		if (doc.HasParseError())
			throw std::logic_error("JSON_PARSE_DOC_ERROR");
		
		_effectDataSet.Clear();

		auto itEffect = doc.FindMember("Effect");
		if (itEffect != doc.MemberEnd()) {
			for (const auto& objEffect : itEffect->value.GetArray()) {
				auto itName = objEffect.FindMember("name");
				if (itName == objEffect.MemberEnd()) {
					LOG_W("the effect object without 'name' can't be used.");
					continue;
				}

				FdEffect* effect{ nullptr };

				auto itType = objEffect.FindMember("type");
				if (itType != objEffect.MemberEnd()) {
					std::string typeName{ itType->value.GetString() };
					if (typeName == "circle") {
						effect = new FdCircleEffect;
						JsonObjectToCircleEffect(objEffect, *dynamic_cast<FdCircleEffect*>(effect));
					}
					else if (typeName == "arrow") {
						effect = new FdArrowEffect;
						JsonObjectToArrowEffect(objEffect, *dynamic_cast<FdArrowEffect*>(effect));
					}
					else if (typeName == "polygon") {
						effect = new FdPolygonEffect;
						JsonObjectToPolygonEffect(objEffect, *dynamic_cast<FdPolygonEffect*>(effect));
					}
					else
						LOG_W("the effect type(%s) is not defined.", typeName.c_str());
				}
				if (effect) {
					JsonObjectToEffect(objEffect, *effect);
					_effectDataSet.effect.emplace(itName->value.GetString(), effect);
				}
			}
		}
	}
	catch (std::exception& e) {
		LOG_E("[Worker] Exeception: %s", e.what());
	}
	catch (...) {
		LOG_E("[Worker] Error : Default Catch....");
	}
}

void Worker::JsonObjectToARData(const rapidjson::Value& obj, FdARData& outARData)
{
	for (const auto& objEffect : obj.GetArray())
	{
		auto itName = objEffect.FindMember("name");
		if (itName != objEffect.MemberEnd())
		{
			// on test
			//std::string msg = itName->value.GetString();


			auto effect = _effectDataSet.effect.find(itName->value.GetString());
			if (effect != _effectDataSet.effect.end())
			{
				switch (effect->second->type)
				{
				case FdEffect::Type::Arrow:
					outARData.arrow.push_back(*dynamic_cast<FdArrowEffect*>(effect->second));
					SetPointToLineEffect(objEffect, outARData.arrow.back());
					JsonObjectToArrowEffect(objEffect, outARData.arrow.back());
					JsonObjectToEffect(objEffect, outARData.arrow.back());
					SetAdditionalParamToEffect(objEffect, outARData.arrow.back());
					break;
				case FdEffect::Type::Circle:
					outARData.circle.push_back(*dynamic_cast<FdCircleEffect*>(effect->second));
					SetPointToCircleEffect(objEffect, outARData.circle.back());
					JsonObjectToCircleEffect(objEffect, outARData.circle.back());
					JsonObjectToEffect(objEffect, outARData.circle.back());
					SetAdditionalParamToEffect(objEffect, outARData.circle.back());
					break;
				case FdEffect::Type::Polygon:
					outARData.polygon.push_back(*dynamic_cast<FdPolygonEffect*>(effect->second));
					SetPointToPolygonEffect(objEffect, outARData.polygon.back());
					JsonObjectToPolygonEffect(objEffect, outARData.polygon.back());
					JsonObjectToEffect(objEffect, outARData.polygon.back());
					SetAdditionalParamToEffect(objEffect, outARData.polygon.back());
					break;
				default:
					throw std::invalid_argument("Unknown effect type - type:" + (int)effect->second->type);
				}
			}
		}
	}
}

void Worker::SetPointToLineEffect(const rapidjson::Value& objEffect, FdArrowEffect& arrow)
{
	auto itPoint = objEffect.FindMember("point");
	if (itPoint == objEffect.MemberEnd())
		throw std::invalid_argument("No 'point' in 'effect'");

	int i = 0;
	for (const auto& objPoint : itPoint->value.GetArray())
	{
		if (i == 0)
		{
			JsonObjectToPoint(objPoint, arrow.point.first);
			i++;
		}
		else if (i == 1)
		{
			JsonObjectToPoint(objPoint, arrow.point.second);
			break;
		}
	}
}

void Worker::SetPointToCircleEffect(const rapidjson::Value& objEffect, FdCircleEffect& circle)
{
	auto itPoint = objEffect.FindMember("point");
	if (itPoint == objEffect.MemberEnd())
		throw std::invalid_argument("No 'point' in 'effect'");

	for (const auto& objPoint : itPoint->value.GetArray())
	{
		JsonObjectToPoint(objPoint, circle.point);
		break;
	}
}

void Worker::SetPointToPolygonEffect(const rapidjson::Value& objEffect, FdPolygonEffect& polygon)
{
	auto itPoint = objEffect.FindMember("point");
	if (itPoint == objEffect.MemberEnd())
		throw std::invalid_argument("No 'point' in 'effect'");

	if (itPoint->value.GetArray().Size() < FdPolygonEffect::min_point_size)
		throw std::invalid_argument("The array size of 'point' is less than " + std::to_string(FdPolygonEffect::min_point_size));

	if (itPoint->value.GetArray().Size() > FdPolygonEffect::min_point_size)
		LOG_W("The array size of 'point' is {}. The rest of the array data will be ignored.", FdPolygonEffect::min_point_size);

	int i = 0;
	for (const auto& objPoint : itPoint->value.GetArray())
	{
		FdPoint point;
		JsonObjectToPoint(objPoint, point);
		polygon.point[i++] = point;
		if (i >= FdPolygonEffect::min_point_size)
			break;
	}
}

void Worker::SetAdditionalParamToEffect(const rapidjson::Value& objEffect, FdEffect& effect)
{
	auto itFadeIn = objEffect.FindMember("fadein");
	if (itFadeIn != objEffect.MemberEnd())
		effect.fadein = itFadeIn->value.GetBool();

	auto itFadeOut = objEffect.FindMember("fadeout");
	if (itFadeOut != objEffect.MemberEnd())
		effect.fadeout = itFadeOut->value.GetBool();
}

void Worker::JsonObjectToPoint(const rapidjson::Value& objPoint, FdPoint& point)
{
	auto itPrefix = objPoint.FindMember("prefix");
	if (itPrefix != objPoint.MemberEnd())
		point.prefix = itPrefix->value.GetString();

	auto itX = objPoint.FindMember("x");
	if (itX != objPoint.MemberEnd())
		point.x = itX->value.GetInt();

	auto itY = objPoint.FindMember("y");
	if (itY != objPoint.MemberEnd())
		point.y = itY->value.GetInt();
}

void Worker::JsonObjectToArrowEffect(const rapidjson::Value& objEffect, FdArrowEffect& arrow)
{
	auto itStyle = objEffect.FindMember("style");
	if (itStyle != objEffect.MemberEnd())
	{
		std::string styleName{ itStyle->value.GetString() };
		if (styleName == "solid")
			arrow.style = FdArrowEffect::Style::Solid;
		else if (styleName == "dotted")
			arrow.style = FdArrowEffect::Style::Dotted;
	}

	auto itDrawLine = objEffect.FindMember("drawline");
	if (itDrawLine != objEffect.MemberEnd())
	{
		int i = 0;
		for (const auto& drawline : itDrawLine->value.GetArray())
		{
			if (i == 0)
			{
				//arrow.drawline.first = std::stoi(drawline.GetString());
				i++;
			}
			else if (i == 1)
			{
				//arrow.drawline.second = std::stoi(drawline.GetString());
				break;
			}
		}
	}
}

void Worker::JsonObjectToCircleEffect(const rapidjson::Value& objEffect, FdCircleEffect& circle)
{
	auto itStyle = objEffect.FindMember("style");
	if (itStyle != objEffect.MemberEnd())
	{
		std::string styleName{ itStyle->value.GetString() };
		if (styleName == "fill")
			circle.style = FdCircleEffect::Style::Fill;
		else if (styleName == "ring")
			circle.style = FdCircleEffect::Style::Ring;
		else if (styleName == "spin")
			circle.style = FdCircleEffect::Style::Spin;
		else if (styleName == "dotted")
			circle.style = FdCircleEffect::Style::Dotted;
	}

	auto itRadius = objEffect.FindMember("radius");
	if (itRadius != objEffect.MemberEnd())
		circle.radius = std::stod(itRadius->value.GetString());

	auto itSubColor = objEffect.FindMember("sub_color");
	if (itSubColor != objEffect.MemberEnd())
		circle.color_sub.Fill(itSubColor->value.GetString());

	if (auto it = objEffect.FindMember("fadein"); it != objEffect.MemberEnd())
		circle.fadein = it->value.GetBool();
}

void Worker::JsonObjectToPolygonEffect(const rapidjson::Value& objEffect, FdPolygonEffect& polygon)
{
	auto itStyle = objEffect.FindMember("style");
	if (itStyle != objEffect.MemberEnd())
	{
		std::string styleName{ itStyle->value.GetString() };
		if (styleName == "fill")
			polygon.style = FdPolygonEffect::Style::Fill;
	}
}

void Worker::JsonObjectToEffect(const rapidjson::Value& objEffect, FdEffect& effect)
{
	auto itAlpha = objEffect.FindMember("alpha");
	if (itAlpha != objEffect.MemberEnd())
		effect.alpha = std::stoi(itAlpha->value.GetString());

	auto itColor = objEffect.FindMember("color");
	if (itColor != objEffect.MemberEnd())
		effect.color_main.Fill(itColor->value.GetString());

	auto itThickness = objEffect.FindMember("thickness");
	if (itThickness != objEffect.MemberEnd())
		effect.thickness = std::stoi(itThickness->value.GetString());
}

