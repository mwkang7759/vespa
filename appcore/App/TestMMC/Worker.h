#pragma once

#include <thread>
#include <fstream>
#include <functional>

#include "Protocol.h"
#include "MessageQueue.h"
#include "ESMMovieMaker.h"
#include <FdEffectData.h>

#include "TraceAPI.h"


class Worker : public ESMMovieMaker::Handler {
public:
	Worker();
	~Worker();
	void SetUseDebugNumber(bool enable);

	virtual void	OnExtraData(const uint8_t* bitstream, int32_t size, const std::string& name);
	virtual void	OnEncodingComplete(MppcPoints* points, int32_t status, int32_t frameIdx, const uint8_t* bitstream, int32_t size, const std::string& name);
	virtual void	onMovieMakerError(int32_t code, const char* error);


	void PutMakingMsg(const std::string& msg);
	void SetEffectData(const std::string& msg);
	void SetWorldCoords(int presetNum, MppcPositionPreset& presetPos);

	typedef std::function<int(void*, int, int)> callbackStatus;
	callbackStatus _cbStatus{};
	void SetCallback(callbackStatus f) { _cbStatus = std::move(f); }

private:
	bool						_debug{ false };
	bool						_run{ false };
	std::thread*				_thread = nullptr;
	MessageQueue<std::string>	_qProcessing;
	std::string					_name;
	map<std::string, adjustInfo>	_adjustInfo; // Key:DscID
	MppcPointData					_pointData;

	std::string					_outPath{};
	std::ofstream				_file;

	// 
	FdEffectDataSet _effectDataSet; // (Note: Don't modify the data in '_effectDataSet'.)
	//FdARData _ARData;

	void* TaskMaking(void* arg);

	void			JsonObjectToARData(const rapidjson::Value& obj, FdARData& outARData);
	void			SetPointToLineEffect(const rapidjson::Value& objEffect, FdArrowEffect& arrow);
	void			SetPointToCircleEffect(const rapidjson::Value& objEffect, FdCircleEffect& circle);
	void			SetPointToPolygonEffect(const rapidjson::Value& objEffect, FdPolygonEffect& polygon);
	void			SetAdditionalParamToEffect(const rapidjson::Value& objEffect, FdEffect& effect);
	void			JsonObjectToPoint(const rapidjson::Value& objPoint, FdPoint& point);
	void			JsonObjectToArrowEffect(const rapidjson::Value& objEffect, FdArrowEffect& arrow);
	void			JsonObjectToCircleEffect(const rapidjson::Value& objEffect, FdCircleEffect& circle);
	void			JsonObjectToPolygonEffect(const rapidjson::Value& objEffect, FdPolygonEffect& polygon);
	void			JsonObjectToEffect(const rapidjson::Value& objEffect, FdEffect& effect);
};
