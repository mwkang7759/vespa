﻿#pragma once

#include <WinSock2.h>
#include <ESMBase.h>

#define	VSPD_MESSAGE_HEADER_SIZE	5

struct MTdProtocolHeader
{
    int nSize;
    char cSeparator[1];
};

struct ClientSockThreadData
{
    void* pthis;
    int nType;
    int nSocket;
    char* nAddress;
    int nPort;
};

struct SpdMsgHeader
{
    int z;
    char separator;
};

union unSpdMsgHeader
{
    SpdMsgHeader stHeader;
    unsigned char y[VSPD_MESSAGE_HEADER_SIZE];
};

#define MTDPROTOCOL_SECTION1        "Section1"
#define MTDPROTOCOL_SECTION2        "Section2"
#define MTDPROTOCOL_SECTION3        "Section3"
#define MTDPROTOCOL_SENDSTATE       "SendState"
#define MTDPROTOCOL_TOKEN           "Token"
#define MTDPROTOCOL_FROM            "From"
#define MTDPROTOCOL_TO              "To"
#define MTDPROTOCOL_ACTION          "Action"
#define MTDPROTOCOL_RESULTCODE      "ResultCode"
#define MTDPROTOCOL_ERRORMSG        "ErrorMsg"
#define MTDPROTOCOL_CAMIDX          "Camera"
#define MTDPROTOCOL_ADJUSTDATA      "AdjustData"
#define MTDPROTOCOL_STATUS          "Status"
#define MTDPROTOCOL_ADJUSTID        "AdjustID"

