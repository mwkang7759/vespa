﻿
// TestMMCDlg.h: 헤더 파일
//

#pragma once

#include "ESMMovieMaker.h"

#include "ESMMovieGraphic.h"
//#define RAPIDJSON_HAS_STDSTRING 1

#include <string>
#include <thread>
#include <mutex>
#include <vector>
#include <fstream>
#include <iostream>
#include <map>

#include "Protocol.h"

//#include <rapidjson/rapidjson.h>
//#include <rapidjson/document.h>
//#include <rapidjson/stringbuffer.h>
//#include <rapidjson/writer.h>
//#include <rapidjson/ostreamwrapper.h>

#include "TraceAPI.h"
#include "StreamWriterAPI.h"
#include "DeviceOutAPI.h"

// add worker and manager
#include "Worker.h"
#include "Manager.h"

#include "SDL.h" 
#pragma comment(lib, "SDL2main.lib") 
#pragma comment(lib, "SDL2.lib") 



enum class ErrorVM : int
{
	NONE = -1,
	SUCCESS = 0,

	// 1: VMFuncText
	ERROR_LOADTEXT = 10000,
	ERROR_EXTENSION,
	ERROR_PARSETEXT,
	ERROR_PARSEVMCC,
	ERROR_PARSECNT,
	ERROR_FILE_NOT_EXIST,
	ERROR_INVALID_FILE,

	// 2: FuncVMMovie
	ERROR_OPEN_FILE = 20000,
	ERROR_COPY_FRAME_PROPERTIES,
	ERROR_CALCULATE_FRAME,
	ERROR_FIND_STREAM,
	ERROR_CODECPAR_TO_CONTEXT,
	ERROR_OPEN_CODEC,
	ERROR_SEND_PACKET,
	ERROR_RECEIVE_FRAME,
	ERROR_SEEK_FRAME,
	ERROR_FILL_IMAGE,
	ERROR_SCALE_IMAGE,
	ERROR_CONVERT_FRAME,
	ERROR_FLUSH,
	ERROR_CLOSE_DST_FILE,

	// 3: FuncImgProc
	ERROR_IMAGE_ADJUST = 30000,
	ERROR_FRAME2MAT,
	ERROR_MAT2FRAME,
	ERROR_IMAGE_FILL_FRAME2MAT,
	ERROR_IMAGE_SCALE_FRAME2MAT,
	ERROR_IMAGE_FILL_MAT2FRAME,
	ERROR_IMAGE_SCALE_MAT2FRAME,
	ERROR_DO_VMCC,
};
enum class eText : unsigned int
{
	BOTH_ERROR = 0,
	TEXT_MVTM,
	TEXT_4DM,
};

enum class MvtmParseMode : unsigned int
{
	NONE = 0,
	COMMON_SECTION,
	PERIOD_SECTION
};


// CTestMMCDlg 대화 상자
//class CTestMMCDlg : public CDialogEx, public ESMMovieMaker::Handler
class CTestMMCDlg : public CDialogEx
{
// 생성입니다.
public:
	CTestMMCDlg(CWnd* pParent = nullptr);	// 표준 생성자입니다.

	std::map<std::string, adjustInfo>	_adjustInfo;
	
	BOOL							_debug;

	int _dstWidth;
	int _dstHeight;
	int _srcFps;
	int _dstFps;
	int _startFrame;
	int _endFrame;
	std::string _name;
	std::string _framePath{};
	std::string _camPrefix{};
	std::string _srcDir{};

	FrWriterHandle m_hWriter;
	int m_nFrameCnt;
	int m_nSrcFps;

	FILE* mfp;

	std::string outPath{};
	std::string _srcCsvPath{};
	

	// json path
	std::string _adjustPath;
	std::string _frameInfoPath;

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_TESTMMC_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


	// 
	std::vector<cv::Point> _vCaliPoint;


	// add worker and manager 
	//std::unique_ptr<Worker> _worker;
	MakingParam _param{};
	std::unique_ptr<Manager> _manager;
	std::map<std::string, userdata> _mPts;

	userdata _data;

	// add sdl2
	SDL_Window*		m_pScreen;
	SDL_Renderer*	m_pRenderer;
	SDL_Texture* m_pTexture;


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnTest();
	afx_msg void OnBnClickedBtnTest2();
	afx_msg void OnBnClickedBtnMakeJson();
	afx_msg void OnEnChangeEditDstWidth();
	afx_msg void OnBnClickedBtnMaking();
	
	CButton m_btnUseDebug;
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedBtnWarptest();
	afx_msg void OnBnClickedBtnLoadworld();
};
