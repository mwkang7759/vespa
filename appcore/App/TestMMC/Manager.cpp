#include "Manager.h"

#include <sstream>
#include <iomanip>

std::string GetCurrentTimeString()
{
	auto tt = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	tm lt;
	localtime_s(&lt, &tt); // Note: It should be changed to 'localtime' or 'localtime_r' on Linux environment.
	std::ostringstream oss;
	oss << std::put_time(&lt, "%F %T"); // Output Example : 2021-12-01 20:49:52
	return oss.str();
}

#define MWDEBUG

//////////////////////////////////////////////
// Functions for calculating time and frame //
//////////////////////////////////////////////

double TimePerFrame(int srcFPS)
{
	return 1000. / static_cast<double>(srcFPS); // If srcFPS is 25, the return value is 40ms.
}

int GetFixedTime(int time, int srcFPS)
{
	if (srcFPS == 25 || srcFPS == 50) // If source video's FPS(srcFPS) is 25 or 50, no fix will occur.
		return time;

	double tpf = TimePerFrame(srcFPS);
	double remainder = std::fmod(time, tpf);

	if (remainder > tpf * 0.5 && remainder < tpf * 1.5)
		return time;

	return static_cast<int>(static_cast<double>(time) - remainder);
}

void GroupOfFilePath(int period_idx, const SourceFrameInfo& frame_info, const std::map<int, std::vector<SourceFrameInfo>>& m_frame_info,
	int& file_idx, std::map<int, std::pair<int, int>>& m_frame_start_end)
{
	int idx{};
	auto frameStartEnd = std::make_pair(0, 0);

	LOG_I("[Manager] GroupOfFilePath Begin..period_idx(%d)", period_idx);

	if (m_frame_start_end.empty())
		idx = file_idx;
	else
	{
		auto it = m_frame_info.find(period_idx);		
		if (it == m_frame_info.end())
		{
			frameStartEnd.first = m_frame_start_end[file_idx].second + 1;
			frameStartEnd.second = frameStartEnd.first;
			idx = ++file_idx;
			LOG_I("[Manager] it == m_frame_info.end() : idx(%d), first(%d), second(%d)", idx, frameStartEnd.first, frameStartEnd.second);
		}
		/*else if (it->second.back().camPrefix == frame_info.camPrefix && it->second.back().time == frame_info.time
			&& timeframe >= 1000) {
			frameStartEnd.first = m_frame_start_end[file_idx].second + 1;
			frameStartEnd.second = frameStartEnd.first;
			idx = ++file_idx;
		}*/
		else if (it->second.back().camPrefix == frame_info.camPrefix &&
			it->second.back().time == frame_info.time)
		{
			frameStartEnd.first = m_frame_start_end[file_idx].first;
			frameStartEnd.second = m_frame_start_end[file_idx].second + 1;
			idx = file_idx;
			LOG_I("[Manager] it->second.back().camPrefix, time == frame_info : idx(%d), first(%d), second(%d)", idx, frameStartEnd.first, frameStartEnd.second);
		}
		else
		{
			frameStartEnd.first = m_frame_start_end[file_idx].second + 1;
			frameStartEnd.second = frameStartEnd.first;
			idx = ++file_idx;
			LOG_I("[Manager] else..idx(%d), frameStartEnd.first(%d), frameStartEnd.second(%d)", idx, frameStartEnd.first, frameStartEnd.second);
		}
	}

	m_frame_start_end[idx] = frameStartEnd;

	LOG_I("[Manager] GroupOfFilePath End..idx(%d), frameStartEnd.first(%d), frameStartEnd.second(%d)", idx, frameStartEnd.first, frameStartEnd.second);
	//SPd_DEBUG("srcFrameStartEnd[{}] = ({}, {})", idx, frameStartEnd.first, frameStartEnd.second);
}

Manager::Manager() {
	_worker = std::make_unique<Worker>();
	
	_worker->SetCallback(std::bind(&Manager::ManStatusCallback, this, placeholders::_1, placeholders::_2, placeholders::_3));
}

Manager::~Manager() {

}

int Manager::CalcSrcFrames(int markerTime, const cameraInfo& camInfo, const mvtmInfo& mvInfo, designatedInfo& desigInfo) {
	int nFileIdx = 1;
	
	LOG_I("[Manager] CalcSrcFrames Begin..period num(%d), marker time(%d)", mvInfo.n_period, markerTime);
	// calculate source and target
	try {
		desigInfo.srcFrame.clear();
		desigInfo.srcFrameStartEnd.clear();
		desigInfo.dstFrame.clear();
		for (int i = 0; i < mvInfo.n_period; i++) {
			int start_time = markerTime + mvInfo.period.at(i).starttime;
			int start_camIdx = mvInfo.period.at(i).start_cam_idx;
			int end_time = markerTime + mvInfo.period.at(i).endtime;
			int end_camIdx = mvInfo.period.at(i).end_cam_idx;
			int duration = std::abs(end_time - start_time);
			int camCnt = end_camIdx - start_camIdx + 1;

			start_time = GetFixedTime(start_time, desigInfo.srcFPS);
			end_time = GetFixedTime(end_time, desigInfo.dstFPS);

			LOG_I("[Manager] index(%d), start_time(%d), end_time(%d), start_camIdx(%d), end_camIdx(%d), duration(%d), camCnt(%d)", 
				i, start_time, end_time, start_camIdx, end_camIdx, duration, camCnt);

			auto pair_dst_frame_info = desigInfo.dstFrame.emplace(i, DestinationFrameInfo{});
			pair_dst_frame_info.first->second.rotation = camCnt > 1;
			pair_dst_frame_info.first->second.mppc = mvInfo.period.at(i).mppc;
			pair_dst_frame_info.first->second.effect = mvInfo.period.at(i).serial_effect;

			// local test..
			if (duration == 0) {
				rapidjson::Document mapDocument;
				rapidjson::Document::AllocatorType& _allocator = mapDocument.GetAllocator();
				mapDocument.SetObject();

				rapidjson::Value arrEffectData(rapidjson::kArrayType);
				std::string camId = camInfo.camPrefix.find(start_camIdx)->second;
				MakeEffectJson(arrEffectData, _allocator, _pParam->effect, camId);
				mapDocument.AddMember("effect", arrEffectData, _allocator);

				// json serialize..
				std::string msg;
				JsonSerialize(mapDocument, msg);

				pair_dst_frame_info.first->second.effect = msg;
			}

			if (duration == 0) {
				
				// on test..
				if (start_camIdx == end_camIdx) {
					int camIdx = start_camIdx;
					auto it = camInfo.camPrefix.find(camIdx);
					int base_time = start_time;
					int timeframe = mvInfo.period.at(i).timeframe;
					double pause_timeframe = mvInfo.period.at(i).timeframe / 1000.;
					int max_frame = pause_timeframe * desigInfo.srcFPS;
					
					pair_dst_frame_info.first->second.type = ESMBase::FrameType::Still;
					// linear type
					for (double d_time = 0.;
						d_time < static_cast<double>(mvInfo.period.at(i).timeframe) - (TimePerFrame(desigInfo.srcFPS) * .25);
						d_time += TimePerFrame(desigInfo.srcFPS))
					{
						SourceFrameInfo frameInfo;
						frameInfo.camPrefix = it->second;
						frameInfo.time = start_time / 1000;
						frameInfo.frame = static_cast<int>(std::round(std::fmod(start_time, 1000.) / TimePerFrame(desigInfo.srcFPS)));
						frameInfo.Modify(desigInfo.srcFPS);

						LOG_I("=> Frame# %d - camPrefix:%s, time:%ds, frame:%d",
							pair_dst_frame_info.first->second.count, frameInfo.camPrefix.c_str(), frameInfo.time, frameInfo.frame);

						GroupOfFilePath(i, frameInfo, desigInfo.srcFrame, nFileIdx, desigInfo.srcFrameStartEnd);

						desigInfo.srcFrame[i].emplace_back(frameInfo);

						pair_dst_frame_info.first->second.count++;
					}
					// rotation type
					/*for (int cnt = 0; cnt < max_frame; ++cnt) {
						SourceFrameInfo frameInfo;
						frameInfo.camPrefix = it->second;
						frameInfo.time = start_time / 1000;
						frameInfo.frame = static_cast<int>(std::round(std::fmod(start_time, 1000.) / TimePerFrame(desigInfo.srcFPS)));
						frameInfo.Modify(desigInfo.srcFPS);

						LOG_I("[Manager] rotate index(%d), camIdx(%d), frameInfo: camPrefix(%s), time(%d), frame(%d)",
							i, camIdx, frameInfo.camPrefix.c_str(), frameInfo.time, frameInfo.frame);

						GroupOfFilePath(i, frameInfo, desigInfo.srcFrame, nFileIdx, desigInfo.srcFrameStartEnd, timeframe);
						desigInfo.srcFrame[i].emplace_back(frameInfo);

						pair_dst_frame_info.first->second.count++;
					}*/
				}
				else {
					pair_dst_frame_info.first->second.type = ESMBase::FrameType::Rotation;
					int timeframe = mvInfo.period.at(i).timeframe;

					for (int camIdx = start_camIdx;
						end_camIdx > start_camIdx ? camIdx <= end_camIdx : camIdx >= end_camIdx;
						end_camIdx > start_camIdx ? camIdx++ : camIdx--) {
						auto it = camInfo.camPrefix.find(camIdx);
						if (it == camInfo.camPrefix.end()) {
							continue;
						}
						SourceFrameInfo frameInfo;
						frameInfo.camPrefix = it->second;
						frameInfo.time = start_time / 1000;
						frameInfo.frame = static_cast<int>(std::round(std::fmod(start_time, 1000.) / TimePerFrame(desigInfo.srcFPS)));
						frameInfo.Modify(desigInfo.srcFPS);

						LOG_I("[Manager] rotate index(%d), camIdx(%d), frameInfo: camPrefix(%s), time(%d), frame(%d)",
							i, camIdx, frameInfo.camPrefix.c_str(), frameInfo.time, frameInfo.frame);

						GroupOfFilePath(i, frameInfo, desigInfo.srcFrame, nFileIdx, desigInfo.srcFrameStartEnd);
						desigInfo.srcFrame[i].emplace_back(frameInfo);

						pair_dst_frame_info.first->second.count++;
					}
				}

				
			}
			else {
				int timeframe = mvInfo.period.at(i).timeframe;
				for (double d_time = start_time;
					d_time < static_cast<double>(end_time) - (TimePerFrame(desigInfo.srcFPS) * .25);
					d_time += TimePerFrame(desigInfo.srcFPS)) {
					// solve float rounding error
					double d_remainder = std::fmod(d_time, 0.001);
					double t_remainder = std::fmod(d_time, 1000.);
					if (1000. - static_cast<int>(t_remainder) < TimePerFrame(desigInfo.srcFPS)) {
						d_time += 1000. - t_remainder;
						t_remainder = std::fmod(d_time, 1000.);
					}
					if (d_remainder < 0.00001)
						d_time = std::floor(d_time);
					else if (d_remainder > 0.9999)
						d_time = std::ceil(d_time);
					// caculate exact frame by time and camera
					double gNum = (static_cast<double>(camCnt) / static_cast<double>(duration));
					double gDen = (static_cast<double>(d_time) - static_cast<double>(start_time));
					int camIdx = static_cast<int>(gNum * gDen) + start_camIdx;

					auto it = camInfo.camPrefix.find(camIdx);
					if (it == camInfo.camPrefix.end()) {
						continue;
					}

					SourceFrameInfo frameInfo;
					frameInfo.camPrefix = it->second;
					frameInfo.time = static_cast<int>(d_time / 1000.);
					frameInfo.frame = static_cast<int>(std::round(t_remainder / TimePerFrame(desigInfo.srcFPS)));
					frameInfo.Modify(desigInfo.srcFPS);

					LOG_I("[Manager] linear index(%d), d_time(%2.1f), frameInfo: camPrefix(%s), time(%d), frame(%d)",
						i, d_time, frameInfo.camPrefix.c_str(), frameInfo.time, frameInfo.frame);

					GroupOfFilePath(i, frameInfo, desigInfo.srcFrame, nFileIdx, desigInfo.srcFrameStartEnd);
					desigInfo.srcFrame[i].emplace_back(frameInfo);

					pair_dst_frame_info.first->second.count++;
				}
			}
		}
	}
	catch (std::exception& e) {
		//e.what()
		return -1;
	}
	catch (...) {
		return -1;
	}

	LOG_I("[Manager] CalcSrcFrames End..");
	return 1;
}

int Manager::FindSrcFiles(const pathInfo& path, designatedInfo& desigInfo, srcVideoFFmpegInfo& srcFiles) {
	/////////////////////////////
	// 0. Define variables
	int cnt = 0; // [MVTM Period] * [Frame Index]
	//std::string dstMoviePath = _inPathData.dstMoviePath;
	srcFiles.srcFPS = desigInfo.srcFPS;
	
	LOG_I("[Manager] FindSrcFiles Begin..desigInfo.srcFrame.size(): %d", desigInfo.srcFrame.size());
	
	/////////////////////////////
	// 1. Save proper frame number of videos into map
	for (auto i = 0; i < desigInfo.srcFrame.size(); i++) // i : MVTM period
	{
		for (auto j = 0; j < desigInfo.srcFrame[i].size(); j++) // j : Frame Index by MVTM period
		{
			srcFiles.matchingFrameInfo[cnt] = desigInfo.srcFrame[i][j];

			MvtmInfo mvtmInfo;
			mvtmInfo.period = i;
			mvtmInfo.frame = j;
			desigInfo.srcMatchingMVTM[cnt] = mvtmInfo;

			LOG_I("[Manager] mvtmInfo.period (%d), frame (%d)", mvtmInfo.period, mvtmInfo.frame);

			// e.g. : [PreSD Dir]/2021_10_21_13_25_02/[Prefix]_[Elapsed Time(second)].mp4
			srcFiles.srcFile[cnt++] = std::filesystem::path{ path.srcCamPath.find(desigInfo.srcFrame[i][j].camPrefix)->second[0] }
				.append(path.srcVideoDirPath)
				.append(desigInfo.srcFrame[i][j].camPrefix + "_" + std::to_string(desigInfo.srcFrame[i][j].time) + ".mp4");
		}
	}

	LOG_I("[Manager] FindSrcFiles End..");
	return 1;
}
// caution !!!
// need to function rename..
int Manager::FindTimeFrame(const mvtmInfo& mvInfo,	const designatedInfo& desigInfo, srcVideoFFmpegInfo& srcFiles) {
	int cnt = 0;
	if (srcFiles.srcEffect) {
		delete srcFiles.srcEffect;
		srcFiles.srcEffect = nullptr;
	}
	LOG_I("[Manager] FindTimeFrame Begin..desigInfo.srcFrame.size(): %d", desigInfo.srcFrame.size());
	/////////////////////////////
	// 1. Save proper frame number of videos into map
	for (auto i = 0; i < desigInfo.srcFrame.size(); i++) // i: MVTM Period
	{
		for (auto j = 0; j < desigInfo.srcFrame.at(i).size(); j++) // j: Frame Index 
		{
			srcFiles.srcIsRotation[cnt] = desigInfo.dstFrame.at(i).rotation;
			srcFiles.srcIsMppc[cnt] = desigInfo.dstFrame.at(i).mppc;
			//srcFiles.srcTimeFrame[cnt] = mvInfo.period[i].timeframe;
			srcFiles.srcTimeFrame[cnt] = mvInfo.period.at(i).timeframe;
			srcFiles.srcType[cnt] = desigInfo.dstFrame.at(i).type;
			LOG_I("[Manager] srcFiles cnt(%d) : isRotation(%d), isMppc(%d), TimeFrame(%d)", cnt, srcFiles.srcIsRotation[cnt], srcFiles.srcIsMppc[cnt], srcFiles.srcTimeFrame[cnt]);
			
			if (srcFiles.srcEffect)
			{
				if (desigInfo.dstFrame.at(i).effect.empty() && srcFiles.srcEffect->frameRange.second == -1)
					srcFiles.srcEffect->frameRange.second = cnt - 1; // Set the end frame
			}
			else
			{
				if (!desigInfo.dstFrame.at(i).effect.empty())
				{
					srcFiles.srcEffect = new SourceEffectInfo();
					srcFiles.srcEffect->effect = desigInfo.dstFrame.at(i).effect;
					srcFiles.srcEffect->frameRange.first = cnt;
					srcFiles.srcEffect->frameRange.second = -1;
				}
			}

			++cnt;
		}
	}
	if (srcFiles.srcEffect) {
		if (srcFiles.srcEffect->frameRange.second == -1)
			srcFiles.srcEffect->frameRange.second = cnt - 1; // Set the end frame
	}
	LOG_I("[Manager] FindTimeFrame End..");
	return 1;
}
int Manager::CalcVmcc(const mvtmInfo& mvInfo, const designatedInfo& desigInfo, vmccInfo& vcInfo) {
	int frame_cnt = 0;
	
	LOG_I("[Manager] CalcVmcc Begin..mvInfo.period.size(): %d", mvInfo.period.size());

	for (auto i = 0; i < mvInfo.period.size(); i++) // i : Period #
	{
		std::pair<double, double> delta_center{ 1920., 1080. };
		double delta_zoom = 100;

		if (mvInfo.period.at(i).vmcc[0].time_target == 0 || desigInfo.srcFrame.at(i).size() == 1)
		{
			delta_center = mvInfo.period.at(i).vmcc[0].center;
			delta_zoom = mvInfo.period.at(i).vmcc[0].zoom;

			LOG_I("[Manager] period idx(%d) calcvmcc #1 case..delta_center(%2.1f, %2.1f), delta_zoom(%2.1f)",
				i, delta_center.first, delta_center.second, delta_zoom);
		}

		if (desigInfo.srcFrame.at(i).size() == 1)
		{
			vcInfo.frame_center[frame_cnt] = delta_center;
			vcInfo.frame_zoom_ratio[frame_cnt] = delta_zoom;

			++frame_cnt;
			LOG_I("[Manager] period idx(%d) calcvmcc #2 case..delta_center(%2.1f, %2.1f), delta_zoom(%2.1f), frame_cnt(%d)..continue",
				i, delta_center.first, delta_center.second, delta_zoom, frame_cnt);
			continue;
		}

		int start_frame_idx{};
		int mid_frame_idx{};
		int end_frame_idx{};

		for (int j = 0; j < mvInfo.period.at(i).vmcc_cnt; j++) // i : VMCC Index
		{
			start_frame_idx = end_frame_idx;
			mid_frame_idx = static_cast<int>(desigInfo.dstFrame.at(i).count * mvInfo.period.at(i).vmcc[j].time_target * mvInfo.period.at(i).vmcc[j].time_scale);
			end_frame_idx = static_cast<int>(desigInfo.dstFrame.at(i).count * mvInfo.period.at(i).vmcc[j].time_target);

			std::pair<double, double> last_vmcc{ delta_center };
			double last_vmcc_zoom{ delta_zoom };

			LOG_I("[Manager] period idx(%d) vmccIdx(%d), calcvmcc #3 case..start_frame_idx(%d), mid_frame_idx(%d), end_frame_idx(%d)",
				i, j, start_frame_idx, mid_frame_idx, end_frame_idx);
			LOG_I("[Manager] period idx(%d) vmccIdx(%d), calcvmcc #3 case..last_vmcc(%2.1f, %2.1f), last_vmcc_zoom(%2.1f)",
				i, j, last_vmcc.first, last_vmcc.second, last_vmcc_zoom);

			std::pair<double, double> slope_center{ 1., 1. };
			if (mid_frame_idx != 0)
			{
				slope_center.first = (mvInfo.period.at(i).vmcc[j].center.first - last_vmcc.first) /
					static_cast<double>(mid_frame_idx - start_frame_idx - 1);
				slope_center.second = (mvInfo.period.at(i).vmcc[j].center.second - last_vmcc.second) /
					static_cast<double>(mid_frame_idx - start_frame_idx - 1);

				LOG_I("[Manager] period idx(%d) vmccIdx(%d), calcvmcc #4 case..slope_center(%2.1f, %2.1f), mid_frame_idx(%d)",
					i, j, slope_center.first, slope_center.second, mid_frame_idx);
			}
			double slope_zoom = (mvInfo.period.at(i).vmcc[j].zoom - last_vmcc_zoom) /	static_cast<double>(mid_frame_idx - start_frame_idx - 1);
			
			LOG_I("[Manager] period idx(%d) vmccIdx(%d), calcvmcc #5 case..slope_zoom(%2.1f)",	i, j, slope_zoom);
			
			for (int frameIdx = start_frame_idx; frameIdx < mid_frame_idx; frameIdx++)
			{
				delta_center.first = slope_center.first * static_cast<double>(frameIdx - start_frame_idx) + last_vmcc.first;
				delta_center.second = slope_center.second * static_cast<double>(frameIdx - start_frame_idx) + last_vmcc.second;
				delta_zoom = slope_zoom * static_cast<double>(frameIdx - start_frame_idx) + last_vmcc_zoom;

				vcInfo.frame_center[frame_cnt] = delta_center;
				vcInfo.frame_zoom_ratio[frame_cnt] = delta_zoom;

				++frame_cnt;

				LOG_I("[Manager] period idx(%d) vmccIdx(%d) frameIdx(%d), calcvmcc #6 case..delta_center(%2.1f, %2.1f), delta_zoom(%2.1f), frame_cnt(%d)",
					i, j, frameIdx, delta_center.first, delta_center.second, delta_zoom, frame_cnt);
			}

			std::pair<double, double> mid_end{ delta_center };
			double mid_end_zoom{ delta_zoom };

			for (int frameIdx = mid_frame_idx; frameIdx < end_frame_idx; frameIdx++)
			{
				delta_center = mid_end;
				delta_zoom = mid_end_zoom;

				vcInfo.frame_center[frame_cnt] = delta_center;
				vcInfo.frame_zoom_ratio[frame_cnt] = delta_zoom;

				++frame_cnt;

				LOG_I("[Manager] period idx(%d) vmccIdx(%d) frameIdx(%d), calcvmcc #7 case..delta_center(%2.1f, %2.1f), delta_zoom(%2.1f), frame_cnt(%d)",
					i, j, frameIdx, delta_center.first, delta_center.second, delta_zoom, frame_cnt);
			}
		}

		int remain_frame = desigInfo.dstFrame.at(i).count - end_frame_idx;

		std::pair<double, double> last{ delta_center };
		double last_zoom{ delta_zoom };

		LOG_I("[Manager] period idx(%d), calcvmcc #8 case..remain_frame(%d), last_center(%2.1f, %2.1f), last_zoom(%2.1f)",
			i, remain_frame, last.first, last.second, last_zoom);

		std::pair<double, double> r_slope_center{
			(1920. - delta_center.first) / static_cast<double>(remain_frame - 1),
			(1080. - delta_center.second) / static_cast<double>(remain_frame - 1)
		};
		double r_slope_zoom = (100. - delta_zoom) / static_cast<double>(remain_frame - 1);

		LOG_I("[Manager] period idx(%d), calcvmcc #9 case..r_slope_center(%2.1f, %2.1f), r_slope_zoom(%2.1f)",
			i, r_slope_center.first, r_slope_center.second, r_slope_zoom);

		for (int frameIdx = end_frame_idx; frameIdx < desigInfo.dstFrame.at(i).count; frameIdx++)
		{
			delta_center.first = r_slope_center.first * static_cast<double>(frameIdx - end_frame_idx) + last.first;
			delta_center.second = r_slope_center.second * static_cast<double>(frameIdx - end_frame_idx) + last.second;
			delta_zoom = r_slope_zoom * static_cast<double>(frameIdx - end_frame_idx) + last_zoom;

			vcInfo.frame_center[frame_cnt] = delta_center;
			vcInfo.frame_zoom_ratio[frame_cnt] = delta_zoom;

			
			++frame_cnt;

			LOG_I("[Manager] period idx(%d) frameIdx(%d), calcvmcc #10 case..delta_center(%2.1f, %2.1f), delta_zoom(%2.1f), frame_cnt(%d)",
				i, frameIdx, delta_center.first, delta_center.second, delta_zoom, frame_cnt);
		}
	}
	LOG_I("[Manager] CalcVmcc End..");
	return 1;
}



void Manager::BuildVmTask(const designatedInfo& desigVM, const srcVideoFFmpegInfo& srcVideoVM, const vmccInfo& vmccVM, PositionSwipeMgr& posSwipeMgr, const MovieMakeMessage& mmm, bool isDebug)
{
	LOG_I("[Manager] BuildVmTask Begin..desigVM.srcFrameStartEnd.size() : %d", desigVM.srcFrameStartEnd.size());

	std::vector<std::string> cameraId;
	for (auto i = 0; i < desigVM.srcFrame.size(); ++i)
		for (auto j = 0; j < desigVM.srcFrame.at(i).size(); ++j)
			cameraId.emplace_back(desigVM.srcFrame.at(i).at(j).camPrefix);

	//_m_mppcResult.clear();
	//for (const auto& keyVal : srcVideoVM.srcIsMppc)	{
	//	if (keyVal.second && keyVal.first < _cameraId.size()) {
	//		// TODO: Check if the MPPC 'Preset' data is available.
	//		//       4DPD processes the result of 'SetPreset'.
	//		//       Therefore, MMS is OK without the checking 'Preset' because 4DPD will do nothing.

	//		MppcResult result;
	//		result.returned = false;
	//		result.framenum = srcVideoVM.matchingFrameInfo.at(keyVal.first).time * _pointData.preset[_pointData.currentPresetNumber].points[_cameraId[keyVal.first]].camfps +
	//			_srcFFmpegVM.matchingFrameInfo.at(keyVal.first).frame;
	//		_m_mppcResult[_cameraId[keyVal.first]] = MppcResult{};

	//		SPd_DEBUG("result.framenum:{} / camPrefix:{}", result.framenum, _srcFFmpegVM.matchingFrameInfo.at(keyVal.first).camPrefix);
	//	}
	//}

	std::string currentTime = GetCurrentTimeString();

	for (const auto& keyVal : desigVM.srcFrameStartEnd) {
		std::string msg = MakeWorkerJson(keyVal.second, cameraId, desigVM, srcVideoVM, vmccVM, posSwipeMgr, mmm);

		//m_qMovieMakeTaskInfo.Enqueue(msg);
		_worker->PutMakingMsg(msg);
	}

	LOG_I("[Manager] BuildVmTask End..");
}

void Manager::MakeReplayVideo(const MakingParam& param) {

	pathInfo pathVM;
	cameraInfo camVM;
	mvtmInfo mvtmVM;
	adjustInfo adjustVM;
	designatedInfo designatedVM;
	srcVideoFFmpegInfo srcFFmpegVM;
	vmccInfo vmccVM;
	PositionSwipeMgr posSwipeMgr;
	MovieMakeMessage mmm;

	_pParam = &param;
	_frameCnt = 0;


	pathVM = param.path;
	camVM = param.camInfo;
	mvtmVM = param.mvInfo;

	// set fps test..
	designatedVM.srcFPS = param.srcFps;
	designatedVM.dstFPS = param.dstFps;
	_encWidth = param.dstWidth;
	_encHeight = param.dstHeight;
	_encFps = param.dstFps;

	// set writer, enc media info
	_wrInfo.URLType = _MP4_FF_FILE;
	_wrInfo.pUrl = (CHAR*)param.outPath.c_str();

	_encInfo.dwVideoTotal = 1;
	_encInfo.FrVideo.dwFourCC = FOURCC_AVC1;
	_encInfo.FrVideo.dwWidth = param.dstWidth;
	_encInfo.FrVideo.dwHeight = param.dstHeight;
	_encInfo.FrVideo.dwBitRate = param.dstBitrate * 1024 * 1024;
	_encInfo.FrVideo.dwFrameRate = param.dstFps * 1000;

	std::string msg = MakeEffectPresetJson(param);
	_worker->SetEffectData(msg);


	
	LOG_I("[Manager] srcFps(%d), DST: fps(%d), w(%d), h(%d), bitrate(%d)M", param.srcFps, param.dstFps, param.dstWidth, param.dstHeight, param.dstBitrate);

	CalcSrcFrames(param.markertime, camVM, mvtmVM, designatedVM);
	FindSrcFiles(pathVM, designatedVM, srcFFmpegVM);
	FindTimeFrame(mvtmVM, designatedVM, srcFFmpegVM);
	CalcVmcc(mvtmVM, designatedVM, vmccVM);

	_totalFrameCnt = designatedVM.srcMatchingMVTM.size();

	// on test ..pause..
	//_totalFrameCnt--;
	
	MppcPositionPreset preset;
	std::vector<std::string> cameraId;
	for (auto i = 0; i < designatedVM.srcFrame.size(); ++i)
		for (auto j = 0; j < designatedVM.srcFrame.at(i).size(); ++j) {			
			preset.worlds[designatedVM.srcFrame.at(i).at(j).camPrefix].world_coords = param.world_coords;

			// need to ratio each frame..??
			std::map<std::string, userdata>::iterator iterPosFrame;
			iterPosFrame = param.pdata->find(designatedVM.srcFrame.at(i).at(j).camPrefix);
			if (iterPosFrame != param.pdata->end()) {
				userdata udata = iterPosFrame->second;

				MppcPoints points; 
				points.pts_3d.X1 = udata.points[0].x;
				points.pts_3d.Y1 = udata.points[0].y;
				points.pts_3d.X2 = udata.points[1].x;
				points.pts_3d.Y2 = udata.points[1].y;
				points.pts_3d.X3 = udata.points[2].x;
				points.pts_3d.Y3 = udata.points[2].y;
				points.pts_3d.X4 = udata.points[3].x;
				points.pts_3d.Y4 = udata.points[3].y;

				preset.points.insert(std::make_pair(designatedVM.srcFrame.at(i).at(j).camPrefix, points));


				/*preset.points[designatedVM.srcFrame.at(i).at(j).camPrefix].pts_3d.X1 = udata.points[0].x;
				preset.points[designatedVM.srcFrame.at(i).at(j).camPrefix].pts_3d.Y1 = udata.points[0].y;
				preset.points[designatedVM.srcFrame.at(i).at(j).camPrefix].pts_3d.X2 = udata.points[1].x;
				preset.points[designatedVM.srcFrame.at(i).at(j).camPrefix].pts_3d.Y2 = udata.points[1].y;
				preset.points[designatedVM.srcFrame.at(i).at(j).camPrefix].pts_3d.X3 = udata.points[2].x;
				preset.points[designatedVM.srcFrame.at(i).at(j).camPrefix].pts_3d.Y3 = udata.points[2].y;
				preset.points[designatedVM.srcFrame.at(i).at(j).camPrefix].pts_3d.X4 = udata.points[3].x;
				preset.points[designatedVM.srcFrame.at(i).at(j).camPrefix].pts_3d.Y4 = udata.points[3].y;*/
			}
			else {
				preset.points[designatedVM.srcFrame.at(i).at(j).camPrefix].pts_3d.X1 = (3840 * param.world_coords.X1) / 800;
				preset.points[designatedVM.srcFrame.at(i).at(j).camPrefix].pts_3d.Y1 = (2160 * param.world_coords.Y1) / 800;
				preset.points[designatedVM.srcFrame.at(i).at(j).camPrefix].pts_3d.X2 = (3840 * param.world_coords.X2) / 800;
				preset.points[designatedVM.srcFrame.at(i).at(j).camPrefix].pts_3d.Y2 = (2160 * param.world_coords.Y2) / 800;
				preset.points[designatedVM.srcFrame.at(i).at(j).camPrefix].pts_3d.X3 = (3840 * param.world_coords.X3) / 800;
				preset.points[designatedVM.srcFrame.at(i).at(j).camPrefix].pts_3d.Y3 = (2160 * param.world_coords.Y3) / 800;
				preset.points[designatedVM.srcFrame.at(i).at(j).camPrefix].pts_3d.X4 = (3840 * param.world_coords.X4) / 800;
				preset.points[designatedVM.srcFrame.at(i).at(j).camPrefix].pts_3d.Y4 = (2160 * param.world_coords.Y4) / 800;
			}
			

			/*preset.points[designatedVM.srcFrame.at(i).at(j).camPrefix].pts_3d.X1 = (3840 * param.world_coords.X1) / 800;
			preset.points[designatedVM.srcFrame.at(i).at(j).camPrefix].pts_3d.Y1 = (2160 * param.world_coords.Y1) / 800;
			preset.points[designatedVM.srcFrame.at(i).at(j).camPrefix].pts_3d.X2 = (3840 * param.world_coords.X2) / 800;
			preset.points[designatedVM.srcFrame.at(i).at(j).camPrefix].pts_3d.Y2 = (2160 * param.world_coords.Y2) / 800;
			preset.points[designatedVM.srcFrame.at(i).at(j).camPrefix].pts_3d.X3 = (3840 * param.world_coords.X3) / 800;
			preset.points[designatedVM.srcFrame.at(i).at(j).camPrefix].pts_3d.Y3 = (2160 * param.world_coords.Y3) / 800;
			preset.points[designatedVM.srcFrame.at(i).at(j).camPrefix].pts_3d.X4 = (3840 * param.world_coords.X4) / 800;
			preset.points[designatedVM.srcFrame.at(i).at(j).camPrefix].pts_3d.Y4 = (2160 * param.world_coords.Y4) / 800;*/
		}
	_worker->SetWorldCoords(0, preset);

	BuildVmTask(designatedVM, srcFFmpegVM, vmccVM, posSwipeMgr, mmm, false);


}

std::string Manager::MakeEffectPresetJson(const MakingParam& param) {
	rapidjson::Document mapDocument;
	rapidjson::Document::AllocatorType& _allocator = mapDocument.GetAllocator();

	mapDocument.SetObject();

	rapidjson::Value arrPreset(rapidjson::kArrayType);

	for (int i = 0; i < param.effect.circle.size(); i++) {
		rapidjson::Value objEffectData(rapidjson::kObjectType);

		objEffectData.AddMember("name", "SpinCircle", _allocator);
		objEffectData.AddMember("type", "circle", _allocator);
		objEffectData.AddMember("style", "solid", _allocator);
		objEffectData.AddMember("color", "#554466", _allocator);
		objEffectData.AddMember("fadein", true, _allocator);
		objEffectData.AddMember("radius", to_string(param.effect.circle[i].radius), _allocator);
		
		arrPreset.PushBack(objEffectData, _allocator);
	}

	for (int i = 0; i < param.effect.arrow.size(); i++) {
		rapidjson::Value objEffectData(rapidjson::kObjectType);

		objEffectData.AddMember("name", "SolidArrow", _allocator);
		objEffectData.AddMember("type", "arrow", _allocator);
		objEffectData.AddMember("style", "solid", _allocator);
		objEffectData.AddMember("color", "#F44414", _allocator);

		arrPreset.PushBack(objEffectData, _allocator);
	}

	mapDocument.AddMember("Effect", arrPreset, _allocator);

	std::string msg;
	JsonSerialize(mapDocument, msg);

	return msg;
}

void Manager::MakeEffectJson(rapidjson::Value& objEffect, rapidjson::Document::AllocatorType& _allocator, const Effect& effect, std::string& camId) {

	for (int i = 0; i < effect.circle.size(); i++) {
		rapidjson::Value objEffectData(rapidjson::kObjectType);

		objEffectData.AddMember("name", "SpinCircle", _allocator);

		rapidjson::Value arrPointData(rapidjson::kArrayType);
		for (int k = 0; k < 1; k++) {
			rapidjson::Value objPointData(rapidjson::kObjectType);

			objPointData.AddMember("prefix", camId, _allocator);
			objPointData.AddMember("x", effect.circle[i].center_x, _allocator);
			objPointData.AddMember("y", effect.circle[i].center_y, _allocator);

			arrPointData.PushBack(objPointData, _allocator);
		}
		objEffectData.AddMember("point", arrPointData, _allocator);

		objEffect.PushBack(objEffectData, _allocator);
	}

	for (int i = 0; i < effect.arrow.size(); i++) {
		rapidjson::Value objEffectData(rapidjson::kObjectType);

		objEffectData.AddMember("name", "SolidArrow", _allocator);

		rapidjson::Value arrPointData(rapidjson::kArrayType);
		for (int k = 0; k < 2; k++) {
			rapidjson::Value objPointData(rapidjson::kObjectType);

			if (k == 0) {
				objPointData.AddMember("prefix", camId, _allocator);
				objPointData.AddMember("x", effect.arrow[i].start_x, _allocator);
				objPointData.AddMember("y", effect.arrow[i].start_y, _allocator);
			}
			else {
				objPointData.AddMember("prefix", camId, _allocator);
				objPointData.AddMember("x", effect.arrow[i].end_x, _allocator);
				objPointData.AddMember("y", effect.arrow[i].end_y, _allocator);
			}
			arrPointData.PushBack(objPointData, _allocator);
		}
		objEffectData.AddMember("point", arrPointData, _allocator);

		objEffect.PushBack(objEffectData, _allocator);
	}
}

std::string Manager::MakeWorkerJson(const std::pair<int, int>& keyVal, 
									std::vector<std::string>& camId, 
									const designatedInfo& desigVM, 
									const srcVideoFFmpegInfo& srcVideoVM, 
									const vmccInfo& vmccVM, 
									PositionSwipeMgr& posSwipeMgr, 
									const MovieMakeMessage& mmm) {
	int nZoomRatio, nPosX, nPosY, nCentX, nCentY, ret = -1;

	rapidjson::Document mapDocument;
	rapidjson::Document::AllocatorType& _allocator = mapDocument.GetAllocator();

	mapDocument.SetObject();
	mapDocument.AddMember("gimbal", mmm._gimbal, _allocator);
	mapDocument.AddMember("mode", ESMBase::INTER_COMM_MODE_T::MAKE, _allocator);
	mapDocument.AddMember("filepath", srcVideoVM.srcFile.at(keyVal.first).string(), _allocator);
	mapDocument.AddMember("dstwidth", _encWidth, _allocator);
	mapDocument.AddMember("dstheight", _encHeight, _allocator);
	mapDocument.AddMember("startframe", keyVal.first, _allocator);
	mapDocument.AddMember("endframe", keyVal.second, _allocator);
	mapDocument.AddMember("srcfps", desigVM.srcFPS, _allocator);
	mapDocument.AddMember("dstfps", desigVM.dstFPS, _allocator);
	//mapDocument.AddMember("name", UNC(Configurator::Path::MMS::TEMP).append(mmm._name).string(), _allocator);
	mapDocument.AddMember("name", "test_mmc", _allocator);
	mapDocument.AddMember("useextradata", keyVal.first == 0, _allocator);
	//mapDocument.AddMember("debug", isDebug, _allocator);
	mapDocument.AddMember("debug", true, _allocator);
	//mapDocument.AddMember("mmstime", currentTime, _allocator);
	mapDocument.AddMember("mmstime", "123456789", _allocator);
	mapDocument.AddMember("makemoviename", mmm._name, _allocator); // It is the same as the 'Name' parameter in the 'MakeMovie' command.
	mapDocument.AddMember("tokenkey", mmm._tokenKey, _allocator);
	mapDocument.AddMember("frametype", int(srcVideoVM.srcType.at(keyVal.first)), _allocator);


	if (srcVideoVM.srcEffect)
	{
		rapidjson::Document doc;
		doc.Parse(srcVideoVM.srcEffect->effect);
		if (doc.HasParseError())
			LOG_W("Parse error: serial_effect has a parse error");
		else if (!doc.IsArray())
			LOG_W("Parse error: serial_effect is not an array.");
		else
		{
			mapDocument.AddMember("effect", doc.GetArray(), _allocator);
			mapDocument.AddMember("effectstartframe", srcVideoVM.srcEffect->frameRange.first, _allocator);
			mapDocument.AddMember("effectendframe", srcVideoVM.srcEffect->frameRange.second, _allocator);
		}

		// on test..
		rapidjson::Value arrEffectData(rapidjson::kArrayType);
		MakeEffectJson(arrEffectData, _allocator, _pParam->effect, camId[keyVal.first]);
		mapDocument.AddMember("effect", arrEffectData, _allocator);
		mapDocument.AddMember("effectstartframe", srcVideoVM.srcEffect->frameRange.first, _allocator);
		mapDocument.AddMember("effectendframe", srcVideoVM.srcEffect->frameRange.second, _allocator);
	}
	
	/*std::ifstream ist("c:\\Dev\\mmd_test_cfg\\effect_test.json");
	std::string jsonMsg = "";
	for (char p; ist >> p;)
		jsonMsg += p;
	{
		rapidjson::Document doc;
		doc.Parse(jsonMsg);

		
		rapidjson::Value& obj = doc["effect"].GetObjectW();

		if (doc.HasParseError())
			LOG_W("Parse error: serial_effect has a parse error");
		else if (!doc.IsArray())
			LOG_W("Parse error: serial_effect is not an array.");
		else
		{
			mapDocument.AddMember("effect", doc.GetArray(), _allocator);
			mapDocument.AddMember("effectstartframe", srcVideoVM.srcEffect->frameRange.first, _allocator);
			mapDocument.AddMember("effectendframe", srcVideoVM.srcEffect->frameRange.second, _allocator);
		}
	}*/


	// VMCC Frame info
	// Position Tracking Frame info
	rapidjson::Value arrFrameInfo(rapidjson::kArrayType);
	for (int frame = keyVal.first; frame <= keyVal.second; ++frame)
	{
		if (vmccVM.bPosTracking)
		{
			posSwipeMgr.GetMovedAxis(camId[frame], nZoomRatio, nPosX, nPosY, nCentX, nCentY);
		}
		else
		{
			nPosX = static_cast<int>(std::round(vmccVM.frame_center.at(frame).first));
			nPosY = static_cast<int>(std::round(vmccVM.frame_center.at(frame).second));
		}
		nZoomRatio = static_cast<int>(std::round(vmccVM.frame_zoom_ratio.at(frame)));

		// Create Json Message
		rapidjson::Value objFrameInfoData(rapidjson::kObjectType);
		objFrameInfoData.AddMember("x", nPosX, _allocator);
		objFrameInfoData.AddMember("y", nPosY, _allocator);
		objFrameInfoData.AddMember("zoom", nZoomRatio, _allocator);
		objFrameInfoData.AddMember("prefix", camId[frame], _allocator);
		objFrameInfoData.AddMember("matchingcamprefix", srcVideoVM.matchingFrameInfo.at(frame).camPrefix, _allocator);
		objFrameInfoData.AddMember("matchingtime", srcVideoVM.matchingFrameInfo.at(frame).time, _allocator);
		objFrameInfoData.AddMember("matchingframeidx", srcVideoVM.matchingFrameInfo.at(frame).frame, _allocator);
		objFrameInfoData.AddMember("mppc", srcVideoVM.srcIsMppc.at(frame), _allocator);

		LOG_I("[Manager] MakeWorkerJson: frameIdx(%d), x,y,zoom(%d, %d, %d) matchingIdx(%d)", frame, nPosX, nPosY, nZoomRatio, srcVideoVM.matchingFrameInfo.at(frame).frame);

		rapidjson::Value objFrameNumber(std::to_string(frame), _allocator); // frame number

		rapidjson::Value objFrameInfo(rapidjson::kObjectType);
		objFrameInfo.AddMember(objFrameNumber, objFrameInfoData, _allocator);

		arrFrameInfo.PushBack(objFrameInfo, _allocator);
	}
	mapDocument.AddMember("frameinfo", arrFrameInfo, _allocator);

	std::string msg;
	JsonSerialize(mapDocument, msg);

#ifdef MWDEBUG
	std::ofstream file;
	static int count = 0;
	std::string tempName = "c:\\temp\\temp_make_" + to_string(count++) + ".json";
	file.open(tempName, std::ios::out);
	if (!file.is_open()) {
		std::cout << "Saving the engine file " << tempName << " failed" << std::endl;
		return "";
	}
	file.write((const char*)msg.data(), msg.size());
	file.close();
#endif
	
	return msg;
}

int Manager::ManStatusCallback(void* pVoid, int size, int type) {
	LRSLT lRet;
	switch (type) {
	case -1:
		_totalFrameCnt--;
		break;
	case 0:
		_encInfo.FrVideo.dwConfig = size;
		memcpy(_encInfo.FrVideo.pConfig, pVoid, size);

		lRet = FrWriterOpen(&_hWriter, &_wrInfo);
		if (FRFAILED(lRet)) {
			return -1;
		}

		lRet = FrWriterSetInfo(_hWriter, &_encInfo, 0);
		if (FRFAILED(lRet)) {
			return -1;
		}
		break;
	case 1:
		FrMediaStream encStream;
		encStream.tMediaType = VIDEO_MEDIA;
		encStream.pFrame = (BYTE*)pVoid;
		encStream.dwFrameNum = 1;
		encStream.dwFrameLen[0] = size;
		encStream.dwCTS = _frameCnt * 1000. / _encFps;
		//if (frameData.nFrameType == 1)
			encStream.tFrameType[0] = FRAME_I;
		//else
		//	encStream.tFrameType[0] = FRAME_P;
		lRet = FrWriterWriteFrame(_hWriter, &encStream);
		if (FRFAILED(lRet)) {

		}
		_frameCnt++;
		std::cout << "frame write num :  " << _frameCnt << std::endl;

		break;
	case 2:
		// need to error handle
		if (_hWriter != nullptr && _frameCnt == _totalFrameCnt) {
			FrWriterUpdateInfo(_hWriter);
			FrWriterClose(_hWriter);
			_hWriter = nullptr;

			std::cout << "frame write end :  " << _frameCnt << std::endl;
		}
		break;
	
	}
	
	return 1;
}