﻿#include "HomographyMgr.h"

#if _DEBUG
#include<crtdbg.h>
#define new new(_NORMAL_BLOCK, __FILE__, __LINE__)
#define malloc(s) _malloc_dbg(s, _NORMAL_BLOCK, __FILE__, __LINE__)
#endif

HomographyMgr::HomographyMgr()
{

}

HomographyMgr::~HomographyMgr()
{

}


int HomographyMgr::AddHomographyAtPairChannel(int nChannel1, int nChannel2, const Mat& mHomography)
{
	int nRet = 0;
	StChannelPair stChannelPair(nChannel1, nChannel2);

	auto mapDupCheck = m_mapHomography.find(stChannelPair);
	if (m_mapHomography.end() == mapDupCheck)
		m_mapHomography.insert(pair<StChannelPair, Mat>(stChannelPair, mHomography));
	else
		m_mapHomography[stChannelPair] = mHomography;

	return POSITION_SWIPE_HOMOGRAPHY_OK;
}

int HomographyMgr::GetHomographyAtPairChannel(int nChannel1, int nChannel2, Mat& mHomography)
{
	int nRet = 0;
	StChannelPair stChannelPair(nChannel1, nChannel2);

	auto mapDupCheck = m_mapHomography.find(stChannelPair);
	if (m_mapHomography.end() == mapDupCheck)
		nRet = POSITION_SWIPE_HOMOGRAPHY_ERROR_HOMOGRAPHY_IS_NOT_EXIST;
	else
		mHomography = m_mapHomography.at(stChannelPair);

	return POSITION_SWIPE_HOMOGRAPHY_OK;
}

int HomographyMgr::CalcPoint(const Mat& mHomography, Point2d& ptPoint)
{
	if (mHomography.empty())
		return POSITION_SWIPE_HOMOGRAPHY_ERROR_MAP_IS_EMPTY;
	if (mHomography.cols != 3 || mHomography.rows != 3)
		return POSITION_SWIPE_HOMOGRAPHY_ERROR_MAP_IS_NOT_VALID;

	double dArrTempMat_[3] = { ptPoint.x, ptPoint.y, 1. };
	Mat mTempMat(3, 1, CV_64F, dArrTempMat_);
	Mat resultMat = mHomography * mTempMat;

	ptPoint = Point2d(
		resultMat.at<double>(0) / resultMat.at<double>(2),
		resultMat.at<double>(1) / resultMat.at<double>(2)
	);

	return POSITION_SWIPE_HOMOGRAPHY_OK;
}

int HomographyMgr::CalcHomography(
	StFourPoints stFourPoints1,
	StFourPoints stFourPoints2,
	Mat& mHomography)
{
	vector<Point2d> vecFourPoints1;
	vector<Point2d> vecFourPoints2;

	vecFourPoints1.push_back(Point2d(stFourPoints1.nX1, stFourPoints1.nY1));
	vecFourPoints1.push_back(Point2d(stFourPoints1.nX2, stFourPoints1.nY2));
	vecFourPoints1.push_back(Point2d(stFourPoints1.nX3, stFourPoints1.nY3));
	vecFourPoints1.push_back(Point2d(stFourPoints1.nX4, stFourPoints1.nY4));

	vecFourPoints2.push_back(Point2d(stFourPoints2.nX1, stFourPoints2.nY1));
	vecFourPoints2.push_back(Point2d(stFourPoints2.nX2, stFourPoints2.nY2));
	vecFourPoints2.push_back(Point2d(stFourPoints2.nX3, stFourPoints2.nY3));
	vecFourPoints2.push_back(Point2d(stFourPoints2.nX4, stFourPoints2.nY4));

	mHomography = findHomography(vecFourPoints1, vecFourPoints2);

	if (mHomography.empty())
		return POSITION_SWIPE_HOMOGRAPHY_ERROR_MAP_IS_EMPTY;
	else if (mHomography.cols != 3 || mHomography.rows != 3)
		return POSITION_SWIPE_HOMOGRAPHY_ERROR_MAP_IS_NOT_VALID;
	else
		return POSITION_SWIPE_HOMOGRAPHY_OK;
}