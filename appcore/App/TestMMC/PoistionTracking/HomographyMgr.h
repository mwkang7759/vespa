#pragma once

#include <map>
using namespace std;

#include "PositionSwipeDataStruct.h"

class HomographyMgr
{
public:
	HomographyMgr();
	~HomographyMgr();

public:
	int AddHomographyAtPairChannel(int nChannel1, int nChannel2, const Mat& mHomography);
	int GetHomographyAtPairChannel(int nChannel1, int nChannel2, Mat& mHomography);

public:
	int CalcPoint(const Mat& mHomography, Point2d& ptPoint);
	int CalcHomography(
		StFourPoints stFourPoints1,
		StFourPoints stFourPoints2,
		Mat& mHomography);

private:
	struct StChannelPair
	{
		StChannelPair(int nChannel1_, int nChannel2_)
			: nChannel1(nChannel1_), nChannel2(nChannel2_) {}

		bool operator < (const StChannelPair& stChannelPair) const
		{
			if (nChannel1 < stChannelPair.nChannel1)
				return true;
			else if (nChannel1 == stChannelPair.nChannel1)
				if (nChannel2 < stChannelPair.nChannel2)
					return true;
			return false;
		}

		bool operator == (const StChannelPair& stChannelPair) const
		{
			if (nChannel1 == stChannelPair.nChannel1 &&
				nChannel2 == stChannelPair.nChannel2)
				return true;
			else
				return false;
		}

		int nChannel1;
		int nChannel2;
	};
private:
	enum POSITION_SWIPE_HOMOGRAPHY
	{
		POSITION_SWIPE_HOMOGRAPHY_OK = 0x00,

		POSITION_SWIPE_HOMOGRAPHY_ERROR_HOMOGRAPHY_IS_NOT_EXIST = -0x01,
		POSITION_SWIPE_HOMOGRAPHY_ERROR_HOMOGRAPHY_IS_ALREADY_EXIST = -0x02,
		POSITION_SWIPE_HOMOGRAPHY_ERROR_MAP_IS_EMPTY = -0x03,
		POSITION_SWIPE_HOMOGRAPHY_ERROR_MAP_IS_NOT_VALID = -0x04,
	};

private:
	map<StChannelPair, Mat> m_mapHomography;
};

