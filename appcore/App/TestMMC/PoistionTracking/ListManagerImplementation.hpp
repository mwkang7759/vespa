﻿#include "ListManager.h"

template <class TData>
CPP::ListManager<TData>::ListManager(bool bUseMutex)
{
	m_bUseMutex = bUseMutex;
}

template <class TData>
CPP::ListManager<TData>::~ListManager()
{

}

template <class TData>
int CPP::ListManager<TData>::AddData(int nKey, string strKey, const TData& tData)
{
	int nRet = SetData(nKey, strKey, tData);

	if (nRet == LIST_MANAGER_OK)
	{
		return LIST_MANAGER_ERROR_DATA_ALREADY_EXIST;
	}
	else
	{
		StElement stElement;

		stElement.nKey = nKey;
		stElement.strKey = strKey;
		stElement.tData = tData;

		mutexLock();
		m_vecStElement.push_back(stElement);
		mutexUnlock();

		return LIST_MANAGER_OK;
	}
}

template <class TData>
int CPP::ListManager<TData>::SetData(int nKey, string strKey, const TData& tData)
{
	int nRet = 0;
	int nIndex = 0;
	nRet = getIndexBy(nKey, nIndex);

	if (nRet != LIST_MANAGER_OK) return nRet;

	StElement stElement;

	stElement.nKey = nKey;
	stElement.strKey = strKey;
	stElement.tData = tData;

	mutexLock();
	m_vecStElement[nIndex] = stElement;
	mutexUnlock();

	return LIST_MANAGER_OK;
}

template <class TData>
int CPP::ListManager<TData>::SetData(int nKey, const TData& tData)
{
	int nRet = 0;
	int nIndex = 0;
	nRet = getIndexBy(nKey, nIndex);
	
	if (nRet != LIST_MANAGER_OK) return nRet;

	StElement stElement;

	mutexLock();
	stElement.nKey = nKey;
	stElement.strKey = m_vecStElement[nIndex].strKey;
	stElement.tData = tData;

	m_vecStElement[nIndex] = stElement;
	mutexUnlock();

	return LIST_MANAGER_OK;
}

template <class TData>
int CPP::ListManager<TData>::IsExistData(int nKey)
{
	int nRet = CheckValidOfList();
	if (nRet != LIST_MANAGER_OK) return nRet;

	mutexLock();
	for (int i = 0; i < m_vecStElement.size(); i++)
	{
		if (m_vecStElement[i].nKey == nKey)
		{
			mutexUnlock();
			return LIST_MANAGER_OK;
		}
	}
	mutexUnlock();

	return LIST_MANAGER_ERROR_DATA_IS_NOT_EXIST;
}

template <class TData>
int CPP::ListManager<TData>::IsExistData(string strKey)
{
	int nRet = CheckValidOfList();
	if (nRet != LIST_MANAGER_OK) return nRet;

	mutexLock();
	for (int i = 0; i < m_vecStElement.size(); i++)
	{
		if (m_vecStElement[i].strKey.compare(strKey) == 0)
		{
			mutexUnlock();
			return LIST_MANAGER_OK;
		}
	}
	mutexUnlock();

	return LIST_MANAGER_ERROR_DATA_IS_NOT_EXIST;
}

template <class TData>
int CPP::ListManager<TData>::GetKeyByIndex(int nIndex, int& nKey)
{
	int nRet = CheckValidOfIndex(nIndex);
	if (nRet != LIST_MANAGER_OK) return nRet;

	mutexLock();
	nKey = m_vecStElement[nIndex].nKey;
	mutexUnlock();

	return nRet;
}

template <class TData>
int CPP::ListManager<TData>::GetKeyByIndex(int nIndex, string& strKey)
{
	int nRet = CheckValidOfIndex(nIndex);
	if (nRet != LIST_MANAGER_OK) return nRet;

	mutexLock();
	strKey = m_vecStElement[nIndex].strKey;
	mutexUnlock();

	return nRet;
}

template <class TData>
int CPP::ListManager<TData>::GetDataByIndex(int nIndex, TData& tData)
{
	int nRet = CheckValidOfIndex(nIndex);
	if (nRet != LIST_MANAGER_OK) return nRet;

	mutexLock();
	tData = m_vecStElement[nIndex].tData;
	mutexUnlock();

	return nRet;
}

template <class TData>
int CPP::ListManager<TData>::GetDataByKey(int nKey, TData& tData)
{
	int nRet = CheckValidOfList();
	if (nRet != LIST_MANAGER_OK) return nRet;

	mutexLock();
	for (int i = 0; i < m_vecStElement.size(); i++)
	{
		if (m_vecStElement[i].nKey == nKey)
		{
			tData = m_vecStElement[i].tData;
			mutexUnlock();
			return LIST_MANAGER_OK;
		}
	}
	mutexUnlock();

	return LIST_MANAGER_ERROR_DATA_IS_NOT_EXIST;
}

template <class TData>
int CPP::ListManager<TData>::GetDataByKey(string strKey, TData& tData)
{
	int nRet = CheckValidOfList();
	if (nRet != LIST_MANAGER_OK) return nRet;
	
	mutexLock();
	for (int i = 0; i < m_vecStElement.size(); i++)
	{
		if (m_vecStElement[i].strKey.compare(strKey) == 0)
		{
			tData = m_vecStElement[i].tData;
			mutexUnlock();
			return LIST_MANAGER_OK;
		}
	}
	mutexUnlock();

	return LIST_MANAGER_ERROR_DATA_IS_NOT_EXIST;
}

template <class TData>
int CPP::ListManager<TData>::GetListSize()
{
	mutexLock();
	int nDataSize = static_cast<int>(m_vecStElement.size());
	mutexUnlock();

	return nDataSize;
}

template <class TData>
int CPP::ListManager<TData>::AscSortByKey()
{
	mutexLock();
	sort(m_vecStElement.begin(), m_vecStElement.end(),
		[](const auto& stElement1, const auto& stElement2)
		{
			return stElement1.nKey < stElement2.nKey;
		}
	);
	mutexUnlock();

	return LIST_MANAGER_OK;
}

template <class TData>
int CPP::ListManager<TData>::DesSortByKey()
{
	mutexLock();
	sort(m_vecStElement.begin(), m_vecStElement.end(),
		[](const auto& stElement1, const auto& stElement2)
		{
			return stElement1.nKey > stElement2.nKey;
		}
	);
	mutexUnlock();

	return LIST_MANAGER_OK;
}

template <class TData>
int CPP::ListManager<TData>::ShiftKeyToBegin(int nKey)
{
	int nIndex = 0;
	int nRet = getIndexBy(nKey, nIndex);
	if (nRet != LIST_MANAGER_OK) return nRet;

	nRet = shiftIndexToBegin(nIndex);
	if (nRet != LIST_MANAGER_OK) return nRet;

	return LIST_MANAGER_OK;
}

template <class TData>
int CPP::ListManager<TData>::ShiftKeyToBegin(string strKey)
{
	int nIndex = 0;
	int nRet = getIndexBy(strKey, nIndex);
	if (nRet != LIST_MANAGER_OK) return nRet;

	nRet = shiftIndexToBegin(nIndex);
	if (nRet != LIST_MANAGER_OK) return nRet;

	return LIST_MANAGER_OK;
}

template <class TData>
int CPP::ListManager<TData>::ClearList()
{
	mutexLock();
	m_vecStElement.clear();
	mutexUnlock();

	return LIST_MANAGER_OK;
}

template <class TData>
int CPP::ListManager<TData>::getIndexBy(int nKey, int& nIndex)
{
	int nRet = CheckValidOfList();
	if (nRet != LIST_MANAGER_OK) return nRet;

	mutexLock();
	for (int i = 0; i < m_vecStElement.size(); i++)
	{
		if (m_vecStElement[i].nKey == nKey)
		{
			nIndex = i;
			mutexUnlock();
			return LIST_MANAGER_OK;
		}
	}
	mutexUnlock();

	return LIST_MANAGER_ERROR_DATA_IS_NOT_EXIST;
}

template <class TData>
int CPP::ListManager<TData>::getIndexBy(string strKey, int& nIndex)
{
	int nRet = CheckValidOfList();
	if (nRet != LIST_MANAGER_OK) return nRet;

	mutexLock();
	for (int i = 0; i < m_vecStElement.size(); i++)
	{
		if (m_vecStElement[i].strKey.compare(strKey) == 0)
		{
			nIndex = i;
			mutexUnlock();
			return LIST_MANAGER_OK;
		}
	}
	mutexUnlock();

	return LIST_MANAGER_ERROR_DATA_IS_NOT_EXIST;
}

template <class TData>
int CPP::ListManager<TData>::shiftIndexToBegin(int nIndex)
{
	int nRet = CheckValidOfIndex(nIndex);
	if (nRet != LIST_MANAGER_OK) return nRet;

	for (int i = 0; i < nIndex; i++)
	{
		m_vecStElement.push_back(m_vecStElement[0]);
		m_vecStElement.erase(m_vecStElement.begin());
	}

	return nRet;
}

template <class TData>
int CPP::ListManager<TData>::CheckValidOfIndex(int nIndex)
{
	int nRet = CheckValidOfList();
	if (nRet != LIST_MANAGER_OK) return nRet;

	if (nIndex < 0)
		return LIST_MANAGER_ERROR_INDEX_IS_NEGATIVE;
	if (nIndex >= GetListSize())
		return LIST_MANAGER_ERROR_DATA_IS_NOT_EXIST;

	return LIST_MANAGER_OK;
}

template <class TData>
int CPP::ListManager<TData>::CheckValidOfList()
{
	if (GetListSize() == 0)
		return LIST_MANAGER_ERROR_LIST_IS_EMPTY;
	if (GetListSize() < 0)
		return LIST_MANAGER_ERROR_LIST_IS_NOT_VALID;

	return LIST_MANAGER_OK;
}

template <class TData>
int CPP::ListManager<TData>::mutexLock()
{
	if (m_bUseMutex == true)
	{
		m_mDataManagerMutex.lock();
		return LIST_MANAGER_OK;
	}
	else
	{
		return LIST_MANAGER_ERROR_MUTEX_IS_DISABLED;
	}	
}

template <class TData>
int CPP::ListManager<TData>::mutexUnlock()
{
	if (m_bUseMutex == true)
	{
		m_mDataManagerMutex.unlock();
		return LIST_MANAGER_OK;
	}
	else
	{
		return LIST_MANAGER_ERROR_MUTEX_IS_DISABLED;
	}
}

