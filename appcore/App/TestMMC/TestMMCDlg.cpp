﻿
// TestMMCDlg.cpp: 구현 파일
//

#include "pch.h"
#include "framework.h"
#include "TestMMC.h"
#include "TestMMCDlg.h"
#include "afxdialogex.h"

#include <thread>
#include <regex>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// console debugging...

#define NCAA_TEST
#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console")

inline static std::string getLowercase(const std::string& text)
{
	std::string loweredText{ text };
	std::transform(text.begin(), text.end(), loweredText.begin(),
		[](unsigned char c) { return std::tolower(c); });
	return loweredText;
}

inline static std::regex getMvtmKeyValueRegEx()
{
	std::string ret = "^\\s*(";
	for (auto i = 0; i < mvtmKeyTokens.size(); i++)
	{
		ret += mvtmKeyTokens[i];
		if (i + 1 != mvtmKeyTokens.size())
			ret += "|";
	}
	ret += ")\\s*=\\s*([^=\\s]+)\\s*$";
	return std::regex(ret.c_str());
}

inline static std::regex getIniKeyValueRegEx()
{
	return std::regex("^\\s*([^=\\s]+)\\s*=\\s*([^=\\s]+)\\s*$");
}

inline static std::regex getSectionRegEx()
{
	return std::regex("^\\s*\\[(\\S+)\\]\\s*$");
}

inline static std::string getValueFromSymbol(const std::map<std::string, std::string>& mSymbol, const std::string& key)
{
	auto it = mSymbol.find(getLowercase(key));
	return it == mSymbol.end() ? key : it->second;
}

inline static bool checkValidatedMvtmSection(const std::map<std::string, std::string>& mvtmItems)
{
	if (mvtmItems.find("command") == mvtmItems.end())
	{
		// For the backward compatibility, the period without 'command' is permitted.
		if (mvtmItems.size() + 1 == mvtmKeyTokens.size())
		{
			for (auto i = 0; i < mvtmKeyTokens.size(); i++)
				if (mvtmKeyTokens[i] != "command" &&
					mvtmItems.find(mvtmKeyTokens[i]) == mvtmItems.end())
					return false;

			return true;
		}
	}
	else if (mvtmItems.size() == mvtmKeyTokens.size())
	{
		for (auto i = 0; i < mvtmKeyTokens.size(); i++)
		{
			auto it = mvtmItems.find(mvtmKeyTokens[i]);
			if (it == mvtmItems.end())
				return false;

			if (mvtmKeyTokens[i] == "command" &&
				std::find(mvtmCommandValueTokens.begin(), mvtmCommandValueTokens.end(), getLowercase(it->second)) == mvtmCommandValueTokens.end())
				return false;
		}
		return true;
	}
	return false;
}
ErrorVM LoadCameraInfo(
	const std::filesystem::path& _inCsvFile, // (camera_config.csv)
	std::map<std::string, std::vector<std::filesystem::path>>& _outCamPath, // pathInfo::srcCamPath
	cameraInfo& _outCsvInfo)
{
	/////////////////////////////
	// 0. Define variables
	ErrorVM errCode = ErrorVM::ERROR_INVALID_FILE;
	int cnt = -1;
	std::string line;
	std::size_t sIndex;
	std::vector<std::string> token;

	/////////////////////////////
	// 1. Check extension

	std::error_code ec;
	if (!std::filesystem::exists(_inCsvFile, ec))
	{
		//SPd_ERROR("[{}]::({})::File does not exist ({}) [error_code.message:{}]", __func__, errCode, _inCsvFile.string(), ec.message());
		return errCode;
	}

	if (getLowercase(_inCsvFile.extension().string()) != ".csv")
	{
		errCode = ErrorVM::ERROR_INVALID_FILE;
		//SPd_ERROR("[{}]::({})::Invalid file ({})", __func__, errCode, _inCsvFile.string());
		return errCode;
	}

	/////////////////////////////
	// 2. Open file
	std::ifstream textFile{ _inCsvFile };
	if (!textFile.is_open())
	{
		errCode = ErrorVM::ERROR_LOADTEXT;
		//SPd_ERROR("[{}]::({})::Load error", __func__, errCode);
		return errCode;
	}

	/////////////////////////////
	// 3. Put data into proper variables
	int nCase = static_cast<int>(eText::TEXT_4DM);

	try
	{
		switch (nCase)
		{
			case static_cast<int>(eText::TEXT_4DM) :
				//SPd_DEBUG("[{}]::[TEXT_4DM]::Read", __func__);

				while (getline(textFile, line))
				{
					cnt = 0;
					token.clear();
					while (cnt <= 2)
					{
						sIndex = line.find(",");
						token.emplace_back(line.substr(0, sIndex)); // cnt==0:ID / cnt==1:PreSD Dir / cnt==2:Prefix
						line.erase(line.begin(), line.begin() + (sIndex + 1));
						cnt++;
					}
					_outCsvInfo.camPrefix[std::stoi(token[0])] = token[2];
					_outCsvInfo.camIdx[token[2]] = std::stoi(token[0]);
					line = token[1];
					while (1)
					{
						sIndex = line.find(":");
						if (sIndex == std::string::npos)
						{
							if (line.size() > 0)
								_outCamPath[token[2]].emplace_back(line);
							break;
						}
						_outCamPath[token[2]].emplace_back(line.substr(0, sIndex));
						line.erase(line.begin(), line.begin() + (sIndex + 1));
					}
				}
			textFile.close();
			//SPd_DEBUG("[{}]::[TEXT_4DM]::Done", __func__);
			break;
			default:
				errCode = ErrorVM::ERROR_EXTENSION;
				//SPd_DEBUG("[{}]::({})::Case error", __func__, errCode);
				return errCode;
		}
	}
	catch (std::exception& e)
	{
		errCode = ErrorVM::ERROR_PARSETEXT;
		//SPd_ERROR("[{}]::({})::Case exception - {}", __func__, errCode, e.what());
		return errCode;
	}
	catch (...)
	{
		errCode = ErrorVM::ERROR_PARSETEXT;
		//SPd_ERROR("[{}]::({})::Case exception - Unknown", __func__, errCode);
		return errCode;
	}

}
ErrorVM Load3dPositionInfo(const std::filesystem::path& _inCsvFile, std::map<std::string, userdata>& _outCsvInfo)
{
	/////////////////////////////
	// 0. Define variables
	ErrorVM errCode = ErrorVM::ERROR_INVALID_FILE;
	int cnt = -1;
	std::string line;
	std::size_t sIndex;
	std::vector<std::string> token;
	std::string name;
	userdata udata;
	/////////////////////////////
	// 1. Check extension

	std::error_code ec;
	if (!std::filesystem::exists(_inCsvFile, ec))
	{
		//SPd_ERROR("[{}]::({})::File does not exist ({}) [error_code.message:{}]", __func__, errCode, _inCsvFile.string(), ec.message());
		return errCode;
	}

	if (getLowercase(_inCsvFile.extension().string()) != ".csv")
	{
		errCode = ErrorVM::ERROR_INVALID_FILE;
		//SPd_ERROR("[{}]::({})::Invalid file ({})", __func__, errCode, _inCsvFile.string());
		return errCode;
	}

	/////////////////////////////
	// 2. Open file
	std::ifstream textFile{ _inCsvFile };
	if (!textFile.is_open())
	{
		errCode = ErrorVM::ERROR_LOADTEXT;
		//SPd_ERROR("[{}]::({})::Load error", __func__, errCode);
		return errCode;
	}

	/////////////////////////////
	// 3. Put data into proper variables
	int nCase = static_cast<int>(eText::TEXT_4DM);

	try
	{
		switch (nCase)
		{
			case static_cast<int>(eText::TEXT_4DM) :
				//SPd_DEBUG("[{}]::[TEXT_4DM]::Read", __func__);

				while (getline(textFile, line))
				{
					cnt = 0;
					token.clear();
					udata.points.clear();
					//while (cnt <= 2)
					{
						sIndex = line.find(":");
						token.emplace_back(line.substr(0, sIndex)); // cnt==0:ID / cnt==1:PreSD Dir / cnt==2:Prefix
						name = token[0];
						udata.camName = name;
						line.erase(line.begin(), line.begin() + (sIndex + 1));
						cnt++;
					}
					
					//_outCsvInfo.camPrefix[std::stoi(token[0])] = token[2];
					//_outCsvInfo.camIdx[token[2]] = std::stoi(token[0]);
					//line = token[1];
					int idx = 0;
					cv::Point2f pt;
					
					while (1)
					{
						sIndex = line.find(",");
						if (sIndex == std::string::npos)
						{
							if (line.size() > 0)
								token.emplace_back(line);
							break;
						}
						token.emplace_back(line.substr(0, sIndex));
						if (idx%2==0)
							pt.x = std::stof(token[cnt]);
						else {
							pt.y = std::stof(token[cnt]);
							udata.points.push_back(pt);
						}
						cnt++;
						idx++;
						line.erase(line.begin(), line.begin() + (sIndex + 1));
					}
					_outCsvInfo.insert(std::make_pair(name, udata));

					//pt.x = std::stof(val);
					//udata.points[cnt].x = std::stof(val);
					//_outCamPath[token[2]].emplace_back(line.substr(0, sIndex));
				}
			textFile.close();
			//SPd_DEBUG("[{}]::[TEXT_4DM]::Done", __func__);
			break;
			default:
				errCode = ErrorVM::ERROR_EXTENSION;
				//SPd_DEBUG("[{}]::({})::Case error", __func__, errCode);
				return errCode;
		}
	}
	catch (std::exception& e)
	{
		errCode = ErrorVM::ERROR_PARSETEXT;
		//SPd_ERROR("[{}]::({})::Case exception - {}", __func__, errCode, e.what());
		return errCode;
	}
	catch (...)
	{
		errCode = ErrorVM::ERROR_PARSETEXT;
		//SPd_ERROR("[{}]::({})::Case exception - Unknown", __func__, errCode);
		return errCode;
	}

}
ErrorVM ParseVMCC(
	const std::map<float, std::map<std::string, std::string>>& _inMVTMText,
	const std::map<std::string, std::string>& _inMVTMSymbols,
	mvtmInfo& _inoutMVTMData)
{
	/////////////////////////////
	// 0. Define variables
	ErrorVM errCode = ErrorVM::ERROR_INVALID_FILE;
	int cnt = -1;
	int vmcc_case = -1;
	std::string vmcc_text;
	std::string vmcc_array[6];
	size_t vmcc_offset[2];
	auto period_size = _inMVTMText.size();

	/////////////////////////////
	// 1. Convert string to int and parse VMCC according to structure 'MVTM info'
	// std::map is ordered one, so _inMVTMText is sorted by float
	try
	{
		int i = 0;
		for (const auto& keyValMVTM : _inMVTMText)
		{
			float curPeriod = keyValMVTM.first;
			_inoutMVTMData.sectionMap[curPeriod] = i;	// mapping float to index

			VmPeriod period;

			period.starttime = std::stoi(getValueFromSymbol(_inMVTMSymbols, keyValMVTM.second.at("starttime")));
			period.endtime = std::stoi(getValueFromSymbol(_inMVTMSymbols, keyValMVTM.second.at("endtime")));
			period.start_cam_idx = std::stoi(getValueFromSymbol(_inMVTMSymbols, keyValMVTM.second.at("start_dsc")));
			period.end_cam_idx = std::stoi(getValueFromSymbol(_inMVTMSymbols, keyValMVTM.second.at("end_dsc")));
			period.timeframe = std::stoi(getValueFromSymbol(_inMVTMSymbols, keyValMVTM.second.at("timeframe")));

			/*SPd_DEBUG("[{}:{}] starttime = {}({})", curPeriod, i, period.starttime, keyValMVTM.second.at("starttime"));
			SPd_DEBUG("[{}:{}] endtime = {}({})", curPeriod, i, period.endtime, keyValMVTM.second.at("endtime"));
			SPd_DEBUG("[{}:{}] start_dsc = {}({})", curPeriod, i, period.start_cam_idx, keyValMVTM.second.at("start_dsc"));
			SPd_DEBUG("[{}:{}] end_dsc = {}({})", curPeriod, i, period.end_cam_idx, keyValMVTM.second.at("end_dsc"));
			SPd_DEBUG("[{}:{}] timeframe = {}({})", curPeriod, i, period.timeframe, keyValMVTM.second.at("timeframe"));*/

			auto it_mppc = keyValMVTM.second.find("command");
			if (it_mppc != keyValMVTM.second.end())
			{
				std::string loweredValue = getLowercase(getValueFromSymbol(_inMVTMSymbols, it_mppc->second));

				//SPd_DEBUG("[{}:{}] command = {}", curPeriod, i, loweredValue);

				if (loweredValue == "recal" || loweredValue == "recalibration")
					period.mppc = true;
			}

			period.vmcc_cnt = std::stoi(keyValMVTM.second.at("VMCC_COUNT"));

			for (int j = 0; j < period.vmcc_cnt; j++)
			{
				VmVMCC vmcc;

				vmcc_text = keyValMVTM.second.at("VMCC_" + std::to_string(j));

				cnt = 0;
				*vmcc_offset = NULL;
				while (vmcc_offset[0] != std::string::npos)
				{
					if (cnt == 0)
					{
						vmcc_offset[0] = vmcc_text.find(":");
						vmcc_array[cnt] = vmcc_text.substr(0, vmcc_offset[0]);
					}
					else
					{
						vmcc_offset[1] = vmcc_text.find(":", vmcc_offset[0] + 1);
						vmcc_array[cnt] = vmcc_text.substr(vmcc_offset[0] + 1, (vmcc_offset[1] - vmcc_offset[0]) - 1);
						vmcc_offset[0] = vmcc_offset[1];
					}
					vmcc_case = static_cast<int>(std::count(vmcc_text.begin(), vmcc_text.end(), ':'));
					vmcc_array[cnt] = getValueFromSymbol(_inMVTMSymbols, vmcc_array[cnt]);
					switch (cnt)
					{
					case 0:
						vmcc.time_target = std::stod(vmcc_array[cnt]);
						break;
					case 1:
						vmcc.center.first = std::stoi(vmcc_array[cnt]);
						break;
					case 2:
						vmcc.center.second = std::stoi(vmcc_array[cnt]);
						break;
					case 3:
						vmcc.zoom = std::stoi(vmcc_array[cnt]);
						break;
					case 4:
						if (vmcc_case == 4)
							vmcc.time_scale = std::stoi(vmcc_array[cnt]);
						break;
					case 5:
						vmcc.time_scale = std::stoi(vmcc_array[cnt]);
						break;
					default:
						//printf("[%s]::(%d)::Case exception\n", __func__, errCode);
						break;
					}
					cnt++;
				}
				period.vmcc.emplace_back(vmcc);
			}
			_inoutMVTMData.period[i++] = period;
		}
	}
	catch (std::exception& e)
	{
		//errCode = ErrorVM::ERROR_PARSEVMCC;
		//SPd_ERROR("[{}]::({})::Exception error - {}", __func__, errCode, e.what());
		return errCode;
	}
	catch (...)
	{
		//errCode = ErrorVM::ERROR_PARSEVMCC;
		//SPd_ERROR("[{}]::({})::Exception error - Unknown", __func__, errCode);
		return errCode;
	}
	//VM_FINISH;
}
ErrorVM LoadMvtmInfo(const std::string& _inMvtmFile, mvtmInfo& _outMvtmInfo)
{
	/////////////////////////////
	// 0. Define variables
	ErrorVM errCode = ErrorVM::ERROR_INVALID_FILE;
	int cnt = -1;
	int nCase = -1;
	std::string line;
	std::ifstream textFile;
	std::map<std::string, std::string> mvtmBuffer;
	std::map<std::string, std::string> mvtmSymbols;
	std::map<float, std::map<std::string, std::string>> mMVTMText;

	/////////////////////////////
	// 1. Check extension
	if (getLowercase(_inMvtmFile).find(".mvtm") != std::string::npos)
	{
		nCase = static_cast<int>(eText::TEXT_MVTM);
		textFile = std::ifstream(_inMvtmFile);
		_outMvtmInfo.n_period = 0;
	}

	/////////////////////////////
	// 2. Open file
	if (!textFile.is_open())
	{
		errCode = ErrorVM::ERROR_LOADTEXT;
		//SPd_ERROR("[{}]::({})::Load error", __func__, errCode);
		return errCode;
	}

	/////////////////////////////
	// 3. Put data into proper variables
	try
	{
		MvtmParseMode mode = MvtmParseMode::NONE;
		float currentPeriodNo = 0.f;

		switch (nCase)
		{
			case static_cast<int>(eText::TEXT_MVTM) :
				//SPd_DEBUG("[{}]::[TEXT_MVTM]::Read", __func__);

				while (getline(textFile, line))
				{
					std::regex vowels("//*.*|\\*.*\\*");
					line = std::regex_replace(line, vowels, "");
					std::smatch section, keyVal;
					if (std::regex_match(line, section, getSectionRegEx()))
					{
						std::string sectionName = section.str(1);
						std::transform(sectionName.begin(), sectionName.end(), sectionName.begin(), ::toupper);
						std::regex number("^\\d+(\\.\\d*)?$");
						std::smatch numMatch;

						if (mode == MvtmParseMode::PERIOD_SECTION)
						{
							if (checkValidatedMvtmSection(mvtmBuffer))
							{
								mMVTMText.emplace(currentPeriodNo, mvtmBuffer);
								++_outMvtmInfo.n_period;
							}
							mvtmBuffer.clear();
						}

						if (sectionName.compare("COMMON") == 0)
						{
							mode = MvtmParseMode::COMMON_SECTION;
						}
						else if (regex_match(sectionName, numMatch, number))
						{
							currentPeriodNo = std::stof(sectionName);
							mode = MvtmParseMode::PERIOD_SECTION;
						}
						else
						{
							mode = MvtmParseMode::NONE;
						}

						//SPd_DEBUG("section detected : [{}], mode=[{}], currentPeriodNo=[{}]", sectionName, mode, currentPeriodNo);
					}
					else if (mode == MvtmParseMode::COMMON_SECTION)
					{
						if (std::regex_match(line, keyVal, getIniKeyValueRegEx()))
						{
							std::string key = keyVal.str(1);
							std::string val = keyVal.str(2);

							mvtmSymbols[getLowercase(key)] = val;

							//SPd_DEBUG("common section: key=[{}], val=[{}]", getLowercase(key), val);
						}
						else
						{
							//SPd_DEBUG("fail to match key-val : [{}]", line);
						}
					}
					else if (mode == MvtmParseMode::PERIOD_SECTION)
					{
						if (std::regex_match(line, keyVal, getMvtmKeyValueRegEx()))
						{
							std::string key = keyVal.str(1);
							std::string val = keyVal.str(2);

							mvtmBuffer[key] = val;
							//SPd_DEBUG("period({}) section: key=[{}], val=[{}]", currentPeriodNo, key, val);
						}
						else
						{
							//SPd_DEBUG("fail to match key-val : [{}] on period({})", line, currentPeriodNo);
						}
					}
				}

				if (mode == MvtmParseMode::PERIOD_SECTION)
				{
					mMVTMText.emplace(currentPeriodNo, mvtmBuffer);
					++_outMvtmInfo.n_period;
					mvtmBuffer.clear();
				}
				textFile.close();

				ParseVMCC(mMVTMText, mvtmSymbols, _outMvtmInfo);

				//SPd_DEBUG("[{}]::[TEXT_MVTM]::Done", __func__);
				break;
			default:
				errCode = ErrorVM::ERROR_EXTENSION;
				//SPd_DEBUG("[{}]::({})::Case error", __func__, errCode);
				return errCode;
		}
	}
	catch (std::exception& e)
	{
		errCode = ErrorVM::ERROR_PARSETEXT;
		//SPd_ERROR("[{}]::({})::Case exception - {}", __func__, errCode, e.what());
		return errCode;
	}
	catch (...)
	{
		errCode = ErrorVM::ERROR_PARSETEXT;
		//SPd_ERROR("[{}]::({})::Case exception - Unknown", __func__, errCode);
		return errCode;
	}
	//VM_FINISH;
}


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CTestMMCDlg 대화 상자



CTestMMCDlg::CTestMMCDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_TESTMMC_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTestMMCDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHECK_DEBUG, m_btnUseDebug);
}

BEGIN_MESSAGE_MAP(CTestMMCDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BTN_TEST, &CTestMMCDlg::OnBnClickedBtnTest)
	ON_BN_CLICKED(IDC_BTN_MAKE_JSON, &CTestMMCDlg::OnBnClickedBtnMakeJson)
	ON_BN_CLICKED(IDC_BTN_MAKING, &CTestMMCDlg::OnBnClickedBtnMaking)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_BN_CLICKED(IDC_BTN_WARPTEST, &CTestMMCDlg::OnBnClickedBtnWarptest)
	ON_BN_CLICKED(IDC_BTN_LOADWORLD, &CTestMMCDlg::OnBnClickedBtnLoadworld)
END_MESSAGE_MAP()



// CTestMMCDlg 메시지 처리기

BOOL CTestMMCDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	FrSetTraceFileName("test_mmc.log");

	CString strData;
	strData = "C:\\test_contents\\mmc_result.mp4";
	CWnd* pWnd = GetDlgItem(IDC_EDIT_DST_PATH2);
	pWnd->SetWindowText(strData);

#ifdef NCAA_TEST
	strData = "C:\\test_contents\\ncaa_src\\";
#else
	strData = "C:\\test_contents\\MOVIE\\2021_11_13_11_08_07\\";
#endif
	pWnd = GetDlgItem(IDC_EDIT_SRC_FOLDER);
	pWnd->SetWindowText(strData);

	m_btnUseDebug.SetCheck(0);

	// enc param
	strData = "1920";
	pWnd = GetDlgItem(IDC_EDIT_DST_WIDTH);
	pWnd->SetWindowText(strData);

	strData = "1080";
	pWnd = GetDlgItem(IDC_EDIT_DST_HEIGHT);
	pWnd->SetWindowText(strData);

	strData = "50";
	pWnd = GetDlgItem(IDC_EDIT_DST_BIT);
	pWnd->SetWindowText(strData);

	strData = "30";
	pWnd = GetDlgItem(IDC_EDIT_DST_FPS);
	pWnd->SetWindowText(strData);

	// vmcc param
#ifdef NCAA_TEST 
	strData = "C:\\Dev\\mmd_test_cfg\\Test_Ncaa.mvtm";
#else
	strData = "C:\\Dev\\mmd_test_cfg\\Test_Rotate.mvtm";
#endif
	pWnd = GetDlgItem(IDC_EDIT_MVTM_PATH);
	pWnd->SetWindowText(strData);

#ifdef NCAA_TEST
	strData = "0";
#else
	strData = "1000000";
#endif
	pWnd = GetDlgItem(IDC_EDIT_MARKER_TIME);
	pWnd->SetWindowText(strData);



	// effect param
	strData = "716";
	pWnd = GetDlgItem(IDC_EDIT_CIRCLE_X);
	pWnd->SetWindowText(strData);
	
	strData = "948";
	pWnd = GetDlgItem(IDC_EDIT_CIRCLE_Y);
	pWnd->SetWindowText(strData);

	strData = "200";
	pWnd = GetDlgItem(IDC_EDIT_CIRCLE_RADIUS);
	pWnd->SetWindowText(strData);
	
	strData = "2548";
	pWnd = GetDlgItem(IDC_EDIT_CIRCLE_X2);
	pWnd->SetWindowText(strData);
	
	strData = "546";
	pWnd = GetDlgItem(IDC_EDIT_CIRCLE_Y2);
	pWnd->SetWindowText(strData);

	strData = "200";
	pWnd = GetDlgItem(IDC_EDIT_CIRCLE_RADIUS2);
	pWnd->SetWindowText(strData);
	
	strData = "720";
	//pWnd = GetDlgItem(IDC_EDIT_ARROW_START_X);
	//pWnd->SetWindowText(strData);
	
	strData = "940";
	//pWnd = GetDlgItem(IDC_EDIT_ARROW_START_Y);
	//pWnd->SetWindowText(strData);
	
	strData = "2540";
	//pWnd = GetDlgItem(IDC_EDIT_ARROW_END_X);
	//pWnd->SetWindowText(strData);
	
	strData = "540";
	//pWnd = GetDlgItem(IDC_EDIT_ARROW_END_Y);
	//pWnd->SetWindowText(strData);

	// world postion..800x800, ice
	/*strData = "216";
	pWnd = GetDlgItem(IDC_EDIT_WORLD_X1);
	pWnd->SetWindowText(strData);

	strData = "128";
	pWnd = GetDlgItem(IDC_EDIT_WORLD_Y1);
	pWnd->SetWindowText(strData);

	strData = "584";
	pWnd = GetDlgItem(IDC_EDIT_WORLD_X2);
	pWnd->SetWindowText(strData);

	strData = "127";
	pWnd = GetDlgItem(IDC_EDIT_WORLD_Y2);
	pWnd->SetWindowText(strData);

	strData = "619";
	pWnd = GetDlgItem(IDC_EDIT_WORLD_X3);
	pWnd->SetWindowText(strData);

	strData = "566";
	pWnd = GetDlgItem(IDC_EDIT_WORLD_Y3);
	pWnd->SetWindowText(strData);

	strData = "169";
	pWnd = GetDlgItem(IDC_EDIT_WORLD_X4);
	pWnd->SetWindowText(strData);

	strData = "561";
	pWnd = GetDlgItem(IDC_EDIT_WORLD_Y4);
	pWnd->SetWindowText(strData);*/
	
	strData = "380";
	pWnd = GetDlgItem(IDC_EDIT_WORLD_X1);
	pWnd->SetWindowText(strData);

	strData = "108";
	pWnd = GetDlgItem(IDC_EDIT_WORLD_Y1);
	pWnd->SetWindowText(strData);

	strData = "422";
	pWnd = GetDlgItem(IDC_EDIT_WORLD_X2);
	pWnd->SetWindowText(strData);

	strData = "108";
	pWnd = GetDlgItem(IDC_EDIT_WORLD_Y2);
	pWnd->SetWindowText(strData);

	strData = "422";
	pWnd = GetDlgItem(IDC_EDIT_WORLD_X3);
	pWnd->SetWindowText(strData);

	strData = "222";
	pWnd = GetDlgItem(IDC_EDIT_WORLD_Y3);
	pWnd->SetWindowText(strData);

	strData = "380";
	pWnd = GetDlgItem(IDC_EDIT_WORLD_X4);
	pWnd->SetWindowText(strData);

	strData = "222";
	pWnd = GetDlgItem(IDC_EDIT_WORLD_Y4);
	pWnd->SetWindowText(strData);
	

	// set json path
	_adjustPath = "c:\\Dev\\test_json\\adjust_info.json";
	_frameInfoPath = "c:\\Dev\\test_json\\vm_unit_test.json";
#ifdef NCAA_TEST
	_srcCsvPath = "C:\\Dev\\mmd_test_cfg\\camera_config_ncaa.csv";
#else
	_srcCsvPath = "C:\\Dev\\mmd_test_cfg\\camera_config.csv";
#endif

	

	// add worker and manager
	_manager = std::make_unique<Manager>();


	//// add sdl2
	////HANDLE hWnd = GetDlgItem(IDC_STATIC_SRC_PICTURE)->m_hWnd;
	//if (SDL_Init(SDL_INIT_VIDEO) < 0) {
	//	return FALSE;
	//}
	//
	//// create window
	//m_pScreen = SDL_CreateWindowFrom(GetDlgItem(IDC_STATIC_SRC_PICTURE)->GetSafeHwnd());
	//SDL_ShowWindow(m_pScreen);

	//// create render
	//m_pRenderer = SDL_CreateRenderer(m_pScreen, -1, SDL_RENDERER_ACCELERATED);


	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CTestMMCDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 애플리케이션의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CTestMMCDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}

	// display
	//SDL_RenderPresent(m_pRenderer);
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CTestMMCDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CTestMMCDlg::OnBnClickedBtnTest()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strData;
	CWnd* pWnd = GetDlgItem(IDC_EDIT_DST_PATH);
	pWnd->GetWindowText(strData);
	outPath = std::string(CT2CA(strData));

	

	int32_t ret;

	ESMMovieMaker* pMovieMaker = new ESMMovieMaker();
	ESMMovieMaker::CONTEXT_T* tMovieCtx = new ESMMovieMaker::CONTEXT_T();
	ESMMovieMaker::PARAM_T* tMovieParam = new ESMMovieMaker::PARAM_T();

	while (true) {
		// adjust info
		std::ifstream istAd(_adjustPath);
		std::string adjustMsg = "";
		for (char p; istAd >> p;)
			adjustMsg += p;

		std::string id = "20210823 - 231720";
		ParseFileAdjust(adjustMsg, _adjustInfo, id);

		// frame info
		std::ifstream istVm(_frameInfoPath);
		std::string vmMsg = "";
		for (char p; istVm >> p;)
			vmMsg += p;
		std::cout << "json -> " << std::string(vmMsg) << std::endl;


		rapidjson::Document document;
		document.Parse(vmMsg);
		// set adjust info.
		// ..


		break;
	}

	
	AfxMessageBox(_T("Done"));

	delete pMovieMaker;
	delete tMovieCtx;
	delete tMovieParam;

	pMovieMaker = nullptr;
	tMovieCtx = nullptr;
	tMovieParam = nullptr;

	if (mfp) fclose(mfp);
}


void CTestMMCDlg::OnBnClickedBtnTest2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}



void CTestMMCDlg::OnBnClickedBtnMakeJson()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	
}


void CTestMMCDlg::OnEnChangeEditDstWidth()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// __super::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CTestMMCDlg::OnBnClickedBtnMaking()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CString strData;
	CWnd* pWnd = GetDlgItem(IDC_EDIT_DST_PATH2);
	pWnd->GetWindowText(strData);
	_param.outPath = std::string(CT2CA(strData));

	pWnd = GetDlgItem(IDC_EDIT_SRC_FOLDER);
	pWnd->GetWindowText(strData);
	_param.srcDir = std::string(CT2CA(strData));

	int check = m_btnUseDebug.GetCheck();
	if (check)
		_param.debug = true;
	else
		_param.debug = false;

	// enc param
	pWnd = GetDlgItem(IDC_EDIT_DST_WIDTH);
	pWnd->GetWindowText(strData);
	_param.dstWidth = _wtoi(strData);

	pWnd = GetDlgItem(IDC_EDIT_DST_HEIGHT);
	pWnd->GetWindowText(strData);
	_param.dstHeight = _wtoi(strData);

	pWnd = GetDlgItem(IDC_EDIT_DST_BIT);
	pWnd->GetWindowText(strData);
	_param.dstBitrate = _wtoi(strData);

	pWnd = GetDlgItem(IDC_EDIT_DST_FPS);
	pWnd->GetWindowText(strData);
	_param.dstFps = _wtoi(strData);

	// vmcc param
	pWnd = GetDlgItem(IDC_EDIT_MVTM_PATH);
	pWnd->GetWindowText(strData);
	_param.path.srcMvtmPath = std::string(CT2CA(strData));
	// test line..
	//_param.path.srcMvtmPath = "C:\\Dev\\mmd_test_cfg\\Test_Line.mvtm";
	// test mix
	//_param.path.srcMvtmPath = "C:\\Dev\\mmd_test_cfg\\Test_Mix.mvtm";
	// test pause
	//_param.path.srcMvtmPath = "C:\\Dev\\mmd_test_cfg\\Test_Pause.mvtm";
	_param.path.srcCsvPath = _srcCsvPath;
	_param.path.srcVideoDirPath = _param.srcDir;

	pWnd = GetDlgItem(IDC_EDIT_MARKER_TIME);
	pWnd->GetWindowText(strData);
	_param.markertime = _wtoi(strData);

	/*
	
	*/
	if (_data.points.size() > 0) {
		_param.world_coords.X1 = _data.points[0].x;
		_param.world_coords.Y1 = _data.points[0].y;

		_param.world_coords.X2 = _data.points[1].x;
		_param.world_coords.Y2 = _data.points[1].y;

		_param.world_coords.X3 = _data.points[2].x;
		_param.world_coords.Y3 = _data.points[2].y;

		_param.world_coords.X4 = _data.points[3].x;
		_param.world_coords.Y4 = _data.points[3].y;
	}
	else {
		// world postion..800x800
		pWnd = GetDlgItem(IDC_EDIT_WORLD_X1);
		pWnd->GetWindowText(strData);
		_param.world_coords.X1 = _wtoi(strData);

		pWnd = GetDlgItem(IDC_EDIT_WORLD_Y1);
		pWnd->GetWindowText(strData);
		_param.world_coords.Y1 = _wtoi(strData);

		pWnd = GetDlgItem(IDC_EDIT_WORLD_X2);
		pWnd->GetWindowText(strData);
		_param.world_coords.X2 = _wtoi(strData);

		pWnd = GetDlgItem(IDC_EDIT_WORLD_Y2);
		pWnd->GetWindowText(strData);
		_param.world_coords.Y2 = _wtoi(strData);

		pWnd = GetDlgItem(IDC_EDIT_WORLD_X3);
		pWnd->GetWindowText(strData);
		_param.world_coords.X3 = _wtoi(strData);

		pWnd = GetDlgItem(IDC_EDIT_WORLD_Y3);
		pWnd->GetWindowText(strData);
		_param.world_coords.Y3 = _wtoi(strData);

		pWnd = GetDlgItem(IDC_EDIT_WORLD_X4);
		pWnd->GetWindowText(strData);
		_param.world_coords.X4 = _wtoi(strData);

		pWnd = GetDlgItem(IDC_EDIT_WORLD_Y4);
		pWnd->GetWindowText(strData);
		_param.world_coords.Y4 = _wtoi(strData);
	}
	

	// effect
	Circle cl, cl2;
	pWnd = GetDlgItem(IDC_EDIT_CIRCLE_X);
	pWnd->GetWindowText(strData);
	cl.center_x = _wtoi(strData);
	
	pWnd = GetDlgItem(IDC_EDIT_CIRCLE_Y);
	pWnd->GetWindowText(strData);
	cl.center_y = _wtoi(strData);

	pWnd = GetDlgItem(IDC_EDIT_CIRCLE_RADIUS);
	pWnd->GetWindowText(strData);
	cl.radius = _wtoi(strData);

	pWnd = GetDlgItem(IDC_EDIT_CIRCLE_X2);
	pWnd->GetWindowText(strData);
	cl2.center_x = _wtoi(strData);

	pWnd = GetDlgItem(IDC_EDIT_CIRCLE_Y2);
	pWnd->GetWindowText(strData);
	cl2.center_y = _wtoi(strData);

	pWnd = GetDlgItem(IDC_EDIT_CIRCLE_RADIUS2);
	pWnd->GetWindowText(strData);
	cl2.radius = _wtoi(strData);

	_param.effect.circle.push_back(cl);
	_param.effect.circle.push_back(cl2);

	// arrow
	Arrow arr;
	//pWnd = GetDlgItem(IDC_EDIT_ARROW_START_X);
	//pWnd->GetWindowText(strData);
	//arr.start_x = _wtoi(strData);
	
	//pWnd = GetDlgItem(IDC_EDIT_ARROW_START_Y);
	//pWnd->GetWindowText(strData);
	//arr.start_y = _wtoi(strData);
	
	//pWnd = GetDlgItem(IDC_EDIT_ARROW_END_X);
	//pWnd->GetWindowText(strData);
	arr.end_x = _wtoi(strData);
	
	//pWnd = GetDlgItem(IDC_EDIT_ARROW_END_Y);
	//pWnd->GetWindowText(strData);
	//arr.end_y = _wtoi(strData);
	_param.effect.arrow.push_back(arr);

	LoadCameraInfo(_param.path.srcCsvPath, _param.path.srcCamPath, _param.camInfo);
	LoadMvtmInfo(_param.path.srcMvtmPath, _param.mvInfo);
	
	std::filesystem::path srcCsvPath = "C:\\Dev\\mmd_test_cfg\\ncaa_3dposition.csv";
	Load3dPositionInfo(srcCsvPath, _mPts);
	//FindVideoFiles(path, desigInfo, outSrcInfo);
	
	
	// set cam 3d position
	/*for (int i = 0; i < _param.camInfo.camPrefix.size(); i++) {
		for (int k = 0; k < 4; k++) {
			_data.points[k].x = 100;
			_data.points[k].y = 100;
		}
		_mPts.insert(std::make_pair(_param.camInfo.camPrefix, _data));
	}*/



	_param.srcFps = 30;
	_param.pdata = &_mPts;
	_manager->MakeReplayVideo(_param);
}





void CTestMMCDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	//AfxMessageBox(_T("Mouse LButton Donw.."));

	CWnd* pSrcWnd = GetDlgItem(IDC_STATIC_SRC_PICTURE);
	
	pSrcWnd->ScreenToClient(&point);

	
	_vCaliPoint.push_back(cv::Point(point.x, point.y));
	
	CDialogEx::OnLButtonDown(nFlags, point);
}


void CTestMMCDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialogEx::OnLButtonUp(nFlags, point);
}


void CTestMMCDlg::OnBnClickedBtnWarptest()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	FrRawVideo  DecVideo{};
	FrMediaInfo SrcInfo{};
	McVideoOutHandle hDevOut{};

	cv:Mat imgSrc = imread("c:\\test_contents\\ski_test.jpg", IMREAD_COLOR);

	HANDLE hWnd = GetDlgItem(IDC_STATIC_SRC_PICTURE)->m_hWnd;
	SrcInfo.FrVideo.dwBitCount = 24;
	SrcInfo.FrVideo.dwWidth = imgSrc.cols;
	SrcInfo.FrVideo.dwHeight = imgSrc.rows;
	McVideoOutOpen(&hDevOut, &SrcInfo.FrVideo, hWnd, FALSE);

	DecVideo.dwPitch = imgSrc.cols;
	DecVideo.pY = imgSrc.data;
	McVideoOutConversion(hDevOut, &DecVideo);
	McVideoOutPlay(hDevOut);

	McVideoOutClose(hDevOut);
}


 
void mouseHandler(int event, int x, int y, int flags, void* data_ptr) {
	if (event == EVENT_LBUTTONDOWN) {
		userdata* data = ((userdata*)data_ptr);
		cv::circle(data->im, cv::Point(x, y), 3, cv::Scalar(0, 255, 255), 5, cv::LINE_AA);
		cv::imshow("Image", data->im);
		if (data->points.size() < 4) {
			data->points.push_back(cv::Point2f(x, y));
		}
	}
}


void setLabel(cv::Mat& image, std::string str, std::vector<cv::Point> contour) {
	int fontface = FONT_HERSHEY_SIMPLEX;
	double scale = 0.5;
	int thickness = 1;
	int baseline = 0;

	cv::Size text = getTextSize(str, fontface, scale, thickness, &baseline);
	cv::Rect r = boundingRect(contour);

	cv::Point pt(r.x + ((r.width - text.width) / 2), r.y + ((r.height + text.height) / 2));

	rectangle(image, pt + Point(0, baseline), pt + Point(text.width, -text.height), CV_RGB(200, 200, 200), FILLED);
	putText(image, str, pt, fontface, scale, CV_RGB(0, 0, 0), thickness, 8);

}

void CTestMMCDlg::OnBnClickedBtnLoadworld()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//std::string firstPath = "c:\\test_contents\\ncaa_src\\image\\003001_1.jpg";
	//std::string secondPath = "c:\\test_contents\\ncaa_src\\image\\003030_1.jpg";

	cv::Mat imgSrc1 = imread("c:\\test_contents\\ncaa_src\\image\\003011_1.jpg", IMREAD_COLOR);
	cv::Mat imgSrc2 = imread("c:\\test_contents\\ncaa_src\\image\\003030_1.jpg", IMREAD_COLOR);

	m_pTexture = SDL_CreateTexture(m_pRenderer, SDL_PIXELFORMAT_BGR24, SDL_TEXTUREACCESS_STREAMING, imgSrc1.cols, imgSrc1.rows);

	int srcWidth = imgSrc1.cols;
	int srcHeight = imgSrc1.rows;
	SDL_Rect srcRect = { 0, 0, imgSrc1.cols, imgSrc1.rows };
	int dstWidth = 0;
	int dstHeight = 0;
	SDL_GetWindowSize(m_pScreen, &dstWidth, &dstHeight);
	SDL_Rect dstRect;
	// keep ratio
	double srcRatio = srcWidth * 1.0 / srcHeight;
	double dstRatio = dstWidth * 1.0 / dstHeight;
	if (srcRatio > dstRatio)
	{
		dstRect.x = 0;
		dstRect.y = (dstHeight - dstWidth / srcRatio) / 2;
		dstRect.w = dstWidth;
		dstRect.h = dstWidth / srcRatio;
	}
	else
	{
		dstRect.x = (dstWidth - dstHeight * srcRatio) / 2;
		dstRect.y = 0;
		dstRect.w = dstHeight * srcRatio;
		dstRect.h = dstHeight;
	}
	int ret = SDL_UpdateTexture(m_pTexture, nullptr, imgSrc1.data, srcWidth * 3);
	if (ret != 0)
	{
		LOG_I("SDL_UpdateTexture(): %s", SDL_GetError());
	}

	//SDL_UpdateTexture(sdlTexture, NULL, imgSrc1.data, imgSrc1.cols * 3);

	SDL_RenderClear(m_pRenderer);
	SDL_RenderCopy(m_pRenderer, m_pTexture, &srcRect, &dstRect);
	SDL_RenderPresent(m_pRenderer);

	SDL_Delay(40);	// msec


	if (m_pRenderer)
	{
		SDL_DestroyRenderer(m_pRenderer);
		m_pRenderer = nullptr;
	}
	if (m_pTexture)
	{
		SDL_DestroyTexture(m_pTexture);
		m_pTexture = nullptr;
	}
	if (m_pScreen)
	{
		SDL_DestroyWindow(m_pScreen);
		m_pScreen = nullptr;
	}

	SDL_Quit();

	return;

	
#if 0
	ESMMovieGraphic movieGraph;
	ESMMovieGraphic::ARArrow arrow;

	cv::Point2d startPos(720, 940);
	cv::Point2d endPos(2540, 540);
	MppcPoints mpPoint;
	MppcPoints effectPoint;

	mpPoint.world_coords.X1 = 380;
	mpPoint.world_coords.X2 = 422;
	mpPoint.world_coords.X3 = 422;
	mpPoint.world_coords.X4 = 380;
	mpPoint.world_coords.Y1 = 108;
	mpPoint.world_coords.Y2 = 108;
	mpPoint.world_coords.Y3 = 222;
	mpPoint.world_coords.Y4 = 222;

	int alpha = 100;
	ColorRGB color_main;
	color_main.b = 255;
	color_main.g = 0;
	color_main.r = 0;

	for (int i = 0; i < 2; i++) {
		cv::Mat draw = cv::Mat::zeros(imgSrc1.size(), CV_8UC3);
		cv::Point2d warpStart, warpEnd;

		if (i == 0) {
			mpPoint.pts_3d.X1 = 680;
			mpPoint.pts_3d.Y1 = 1036;
			mpPoint.pts_3d.X2 = 126;
			mpPoint.pts_3d.Y2 = 695;
			mpPoint.pts_3d.X3 = 1600;
			mpPoint.pts_3d.Y3 = 412;
			mpPoint.pts_3d.X4 = 2246;
			mpPoint.pts_3d.Y4 = 662;

			effectPoint = mpPoint;
		}
		else {
			mpPoint.pts_3d.X1 = 1490;
			mpPoint.pts_3d.Y1 = 787;
			mpPoint.pts_3d.X2 = 1706;
			mpPoint.pts_3d.Y2 = 489;
			mpPoint.pts_3d.X3 = 3018;
			mpPoint.pts_3d.Y3 = 575;
			mpPoint.pts_3d.X4 = 3026;
			mpPoint.pts_3d.Y4 = 901;
		}
		

		movieGraph.CalcWarpPoint(&effectPoint, startPos, &warpStart);
		movieGraph.CalcWarpPoint(&effectPoint, endPos, &warpEnd);
		//double thick_pixel = movieGraph.ChangeReal2Pixel(&mpPoint, 10);

		//arrow._alpha = alpha;
		//arrow.SetAlphaMax(alpha);
		arrow.SetThickness(thick_pixel);
		arrow.SetArrowType(FdArrowEffect::Style::Solid);
		arrow.SetStartPos(warpStart);
		arrow.SetEndPos(warpEnd);
		arrow._Linedraw_Flag = false;
		arrow.SetColorBGR(color_main);

		//arrow.Draw(draw, 10);


		// color test..
		double objAlpha = 1.0;
		double backAlpha = 1.0 - objAlpha;

		cv::Scalar clr1(255-color_main.b, 255-color_main.g, 255-color_main.r);
		ellipse(draw, cv::Point(warpStart), cv::Size(100, 100), 0, 0, 360, clr1, -1, cv::LINE_AA);

		cv::Scalar clr2(color_main.b* objAlpha, color_main.g* objAlpha, color_main.r* objAlpha);
		ellipse(draw, cv::Point(warpEnd), cv::Size(100, 100), 0, 0, 360, clr2, -1, cv::LINE_AA);

		
		cv::Mat h_inv;
		ESMMovieGraphic::HomoMatrix_inv(&mpPoint, &h_inv);

		cv::Mat blend(imgSrc1.size(), CV_8UC3, cv::Scalar(255,255,255));
		
		if (i == 0) {
			cv::Mat warp = cv::Mat(imgSrc1.size(), CV_8UC3, cv::Scalar(0, 0, 0));
			cv::warpPerspective(draw, warp, h_inv, imgSrc1.size());

			// detect shape
			cv::Mat img_result;
			// convert color to gray
			cv::Mat img_gray;
			cv::cvtColor(warp, img_gray, COLOR_BGR2GRAY);

			// convert gray to binary
			cv::Mat img_bin;
			threshold(img_gray, img_gray, 1, 255, THRESH_BINARY);

			// find contour
			std::vector<std::vector<cv::Point>> contours;
			findContours(img_gray, contours, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
			std::vector<cv::Point2f> approx;
			img_result = warp.clone();

			for (size_t i = 0; i < contours.size(); i++) {
				// approximate contour
				approxPolyDP(Mat(contours[i]), approx, arcLength(Mat(contours[i]), true) * 0.005, true);

				// draw contour shape
				int size = approx.size();
				line(img_result, approx[0], approx[approx.size() - 1], Scalar(0, 255, 0), 3);

				for (int k = 0; k < size - 1;k++) {
					line(img_result, approx[k], approx[k + 1], Scalar(0, 255, 0), 3);
				}

				// determinating shape, put label in contour area
				if (isContourConvex(Mat(approx))) {
					if (size == 3) {
						setLabel(img_result, "triangle", contours[i]);
					}
					else if (size == 4) {
						setLabel(img_result, "rectangle", contours[i]);
					}
					else {
						setLabel(img_result, to_string(approx.size()), contours[i]);
					}
				}
				else {
					setLabel(img_result, to_string(approx.size()), contours[i]);
				}
			}

			for (int j = 0; j < imgSrc1.rows; j++) {
				uchar* dst = imgSrc1.ptr<uchar>(j);
				uchar* src = warp.ptr<uchar>(j);
				for (int i = 0; i < imgSrc1.cols; i++) {
					if (src[3 * i + 0] == 0 && src[3 * i + 1] == 0 && src[3 * i + 2] == 0)
						continue;

					dst[3 * i + 0] = dst[3 * i + 0] * backAlpha + src[3 * i + 0];
					dst[3 * i + 1] = dst[3 * i + 1] * backAlpha + src[3 * i + 1];
					dst[3 * i + 2] = dst[3 * i + 2] * backAlpha + src[3 * i + 2];
				}
			}

			
			// normal alpha blending..
			/*double objAlpha = 0.7;
			double backAlpha = 1.0 - objAlpha;
			for (int j = 0; j < imgSrc1.rows; j++) {
				uchar* dst = imgSrc1.ptr<uchar>(j);
				uchar* src = warp.ptr<uchar>(j);
				for (int i = 0; i < imgSrc1.cols; i++) {
					if (src[3 * i + 0] == 0 && src[3 * i + 1]==0 && src[3 * i + 2]==0)
						continue;

					dst[3 * i + 0] = dst[3 * i + 0] * backAlpha + src[3 * i + 0] * objAlpha;
					dst[3 * i + 1] = dst[3 * i + 1] * backAlpha + src[3 * i + 1] * objAlpha;
					dst[3 * i + 2] = dst[3 * i + 2] * backAlpha + src[3 * i + 2] * objAlpha;
				}
			}*/

			//cv::addWeighted(imgSrc1, 0.7, warp, 0.3, 0.0, blend);
			//cv::subtract(imgSrc1, warp, imgSrc1);
			//cv::add(imgSrc1, warp, imgSrc1);
			

			cv::namedWindow("color", WINDOW_NORMAL);
			cv::resizeWindow("color", cv::Size(1280, 720));
			cv::imshow("color", imgSrc1);
			//cv::imwrite("c:\\test_contents\\ncaa_src\\image\\imgSrc1.jpg", imgSrc1);
		}
		else {
			cv::Mat warp = cv::Mat(imgSrc2.size(), imgSrc2.type());
			cv::warpPerspective(draw, warp, h_inv, imgSrc2.size());
			cv::subtract(imgSrc2, warp, imgSrc2);

			cv::imwrite("c:\\test_contents\\ncaa_src\\image\\imgSrc2.jpg", imgSrc2);
		}
		
	}

#endif

	

#if 0
	std::string imgName;
	{
		static TCHAR BASED_CODE szFilter[] = _T("동영상 파일(*.jpg) | *.jpg;*.jpg |모든파일(*.*)|*.*||");

		CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, szFilter);

		if (dlg.DoModal() == IDOK)
		{
			CString strFilePath = dlg.GetPathName();

			//m_editFilePath.SetWindowText(strFilePath);

			imgName = (CT2CA(strFilePath));

		}
	}
	
	// image
	std::string camName = "";
	size_t pos = imgName.rfind(".");
	if (pos != std::string::npos) {
		size_t pos2 = imgName.rfind("\\");
		camName = imgName.substr(pos2 + 1, pos - pos2 - 3);
	}

	cv::Mat worldImg = cv::imread(imgName);

	////Create a window
	cv::namedWindow("Image", WINDOW_NORMAL);
	cv::resizeWindow("Image", 1920, 1080);
	cv::Mat worldTmp = worldImg.clone();
	_data.points.clear();
	_data.im = worldTmp;
	_data.camName = camName;
	cv::setMouseCallback("Image", mouseHandler, &_data);
	cv::imshow("Image", worldTmp);
	cv::waitKey(0);

	_mPts.insert(std::make_pair(camName, _data));
	LOG_I("[ncaa] cam: %s: (%2.1f, %2.1f) (%2.1f, %2.1f) (%2.1f, %2.1f) (%2.1f, %2.1f)", camName.c_str(), _data.points[0].x, _data.points[0].y,
		_data.points[1].x, _data.points[1].y,
		_data.points[2].x, _data.points[2].y,
		_data.points[3].x, _data.points[3].y);
#endif

#if 0
	// extract thumbnail with c++17
	cv::VideoCapture cap;
	//std::filesystem::path fspath = "c:\\test_contents\\ncaa_src\\";
	std::string fpath = "c:\\test_contents\\ncaa_src\\";
	std::string imgPath = "c:\\test_contents\\ncaa_src\\image\\";
	
	std::vector<std::string> vCamList;
	std::filesystem::create_directory(imgPath);
	for (const auto& file : std::filesystem::directory_iterator(fpath)) {
		std::string tmpName = file.path().u8string();
		LOG_I("[TestMMC] utf8 string filename = (%s)", tmpName.c_str());
		bool findName = false;
		if (vCamList.size() > 0) {
			for (int i = 0; i < vCamList.size(); i++) {
				std::string name = vCamList[i];
				if (tmpName.find(name) != std::string::npos) {
					findName = true;
					break;
				}
			}
			
		}
		if (findName) {
			continue;
		}
		
		cap.open(tmpName);
		if (!cap.isOpened())
			continue;

		cv::Mat frame;
		cap >> frame;
		
		std::string classFilePath = "";
		size_t pos = tmpName.rfind(".");
		if (pos != std::string::npos) {
			size_t pos2 = tmpName.rfind("\\");
			std::string camName = tmpName.substr(pos2 + 1, pos - pos2 - 3);
			vCamList.push_back(camName);
			classFilePath = tmpName.substr(pos2+1, pos-pos2-1) + ".jpg";
		}

		std::string imgName = imgPath + classFilePath;
		if (!frame.empty()) {
			cv::imwrite(imgName, frame);
		}
		
		//std::string tmpPath{ file.u8string() };
		cap.release();
	}
#endif



	//// video
	//cv::VideoCapture cap("C:\\test_contents\\ncaa_src\\003011_1.mp4");
	//if (!cap.isOpened()) {
	//	return;
	//}

	//namedWindow("Image", WINDOW_NORMAL);
	//resizeWindow("Image", 1920, 1080);
	//
	//cv::Mat img;
	//
	//	cap >> img;
	//	if (img.empty()) {
	//		return;
	//	}

	//	cv::Mat ptsTmp = img.clone();
	//	_data.im = ptsTmp;
	//	setMouseCallback("Image", mouseHandler, &_data);
	//	

	//	cv::imshow("Image", ptsTmp);
	//	waitKey(0);
}
