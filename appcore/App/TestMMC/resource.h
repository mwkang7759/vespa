﻿//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// TestMMC.rc에서 사용되고 있습니다.
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_TESTMMC_DIALOG              102
#define IDR_MAINFRAME                   128
#define IDC_BUTTON1                     1000
#define IDC_BTN_TEST                    1000
#define IDC_EDIT1                       1001
#define IDC_EDIT_START_FRAME            1001
#define IDC_EDIT_CIRCLE_X               1001
#define IDC_EDIT_DST_PATH               1002
#define IDC_EDIT_MVTM_PATH              1002
#define IDC_EDIT_SRC_FOLDER             1003
#define IDC_EDIT_START_CAM              1004
#define IDC_EDIT_DST_PATH2              1004
#define IDC_EDIT_END_FRAME              1005
#define IDC_EDIT_CIRCLE_Y               1005
#define IDC_EDIT_END_CAM                1006
#define IDC_EDIT_START_MAKING_FRAME     1007
#define IDC_EDIT_DST_WIDTH              1008
#define IDC_BTN_MAKE_JSON               1009
#define IDC_EDIT_DST_HEIGHT             1010
#define IDC_BTN_MAKING                  1011
#define IDC_CHECK_DEBUG                 1012
#define IDC_EDIT_VMCC_STARTTIME         1013
#define IDC_EDIT_VMCC_STARTDSC          1014
#define IDC_EDIT_VMCC_ENDTIME           1015
#define IDC_EDIT_CIRCLE_X2              1015
#define IDC_EDIT_VMCC_ENDDSC            1016
#define IDC_EDIT_CIRCLE_Y2              1016
#define IDC_EDIT_VMCC_TIMEFRAME         1017
#define IDC_EDIT_CIRCLE_RADIUS          1017
#define IDC_EDIT_VMCC0_TIMETARGET       1018
#define IDC_EDIT_CIRCLE_RADIUS2         1018
#define IDC_EDIT_VMCC0_WIDTH            1019
#define IDC_EDIT_WORLD_X1               1019
#define IDC_EDIT_VMCC0_HEIGHT           1020
#define IDC_EDIT_WORLD_X2               1020
#define IDC_EDIT_VMCC0_SCALE            1021
#define IDC_EDIT_WORLD_X3               1021
#define IDC_EDIT_VMCC0_TIMESCALE        1022
#define IDC_EDIT_WORLD_X4               1022
#define IDC_EDIT_VMCC1_TIMETARGET       1023
#define IDC_EDIT_CIRCLE_X7              1023
#define IDC_EDIT_VMCC1_WIDTH            1024
#define IDC_EDIT_CIRCLE_X8              1024
#define IDC_EDIT_VMCC1_HEIGHT           1025
#define IDC_EDIT_CIRCLE_X9              1025
#define IDC_EDIT_VMCC1_SCALE            1026
#define IDC_EDIT_CIRCLE_X10             1026
#define IDC_EDIT_VMCC1_TIMESCALE        1027
#define IDC_EDIT_WORLD_Y1               1027
#define IDC_EDIT_MARKER_TIME            1028
#define IDC_EDIT_DST_BIT                1029
#define IDC_EDIT_DST_FPS                1030
#define IDC_EDIT_WORLD_Y2               1031
#define IDC_EDIT_WORLD_Y3               1032
#define IDC_EDIT_WORLD_Y4               1033
#define IDC_EDIT_CIRCLE_X15             1034
#define IDC_EDIT_CIRCLE_X16             1035
#define IDC_EDIT_CIRCLE_X17             1036
#define IDC_EDIT_CIRCLE_X18             1037
#define IDC_STATIC_SRC_PICTURE          1038
#define IDC_STATIC_DST_PICTURE          1039
#define IDC_BTN_WARPTEST                1040
#define IDC_BTN_LOADWORLD               1047

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1041
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
