#pragma once

#include <array>

#include "Protocol.h"
#include "MessageQueue.h"

#include "vmcc_info.h"
#include "Worker.h"
#include "PoistionTracking/PositionSwipeMgr.h"

#include "TraceAPI.h"
#include "StreamWriterAPI.h"

struct MovieMakeMessage
{
	int32_t _tokenKey{ -1 };
	int32_t _apiid{ -1 };
	std::string _name;
	std::string _recordeName;
	int32_t _markerTime{ -1 };
	std::string _template;
	bool _gimbal{};
	int32_t _preSetNumber{};
};

typedef struct {
	double time_target;
	int widht;
	int height;
	double ratio;
	double time_scale;

} VmccParam;

typedef struct {
	int center_x;
	int center_y;
	int radius;
} Circle;

typedef struct {
	int start_x;
	int start_y;
	int end_x;
	int end_y;
} Arrow;

typedef struct {
	std::vector<Circle> circle;
	std::vector<Arrow> arrow;
} Effect;

struct userdata {
	cv::Mat im;
	std::string camName;
	std::vector<cv::Point2f> points;
};

typedef struct {
	int dstWidth;
	int dstHeight;
	int srcFps;
	int dstFps;
	int dstBitrate;
	int startFrame;
	int endFrame;
	int tokenKey;
	bool debug;

	std::string name;
	std::string framePath{};
	std::string camPrefix{};
	std::string srcDir{};

	std::string outPath{};

	bool rotate;


	// add
	int markertime;

	cameraInfo camInfo;
	mvtmInfo	mvInfo;
	pathInfo	path;
	
	Effect effect;
	MppcWorldCoords	world_coords;

	std::map<std::string, userdata>* pdata;
} MakingParam;


class Manager {
public:
	Manager();
	~Manager();

	void MakeReplayVideo(const MakingParam& param);

	

private:
	std::unique_ptr<Worker>			_worker;

	MessageQueue<std::string>		_qMovieMakeTaskInfo;

	FrWriterHandle	_hWriter{};
	FrURLInfo		_wrInfo{};
	FrMediaInfo		_encInfo{};

	int _encWidth;
	int _encHeight;
	int _encFps;

	uint32_t _frameCnt{};
	uint32_t _totalFrameCnt{};
	const MakingParam* _pParam;

	int		ManStatusCallback(void* pVoid, int size, int type);

	// vmcc
	int CalcSrcFrames(int markerTime, const cameraInfo& camInfo, const mvtmInfo& mvInfo, designatedInfo& designInfo);
	int FindSrcFiles(const pathInfo& path, designatedInfo& designInfo, srcVideoFFmpegInfo& srcFiles);
	int FindTimeFrame(const mvtmInfo& mvInfo, const designatedInfo& desigInfo, srcVideoFFmpegInfo& srcFiles);
	int CalcVmcc(const mvtmInfo& mvInfo, const designatedInfo& desigInfo, vmccInfo& vmInfo);
	void BuildVmTask(const designatedInfo& desigVM, const srcVideoFFmpegInfo& srcVideoVM, const vmccInfo& vmccVM, PositionSwipeMgr& posSwipeMgr, const MovieMakeMessage& mmm, bool isDebug);
	
	
	// json
	std::string MakeWorkerJson(const std::pair<int, int>& keyVal, std::vector<std::string>& camId, const designatedInfo& desigVM, const srcVideoFFmpegInfo& srcVideoVM, const vmccInfo& vmccVM, PositionSwipeMgr& posSwipeMgr,	const MovieMakeMessage& mmm);
	void MakeEffectJson(rapidjson::Value& objEffect, rapidjson::Document::AllocatorType& _allocator, const Effect& effect, std::string& camId);
	std::string MakeEffectPresetJson(const MakingParam& param);
};
