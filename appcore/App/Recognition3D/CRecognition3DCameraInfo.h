#pragma once

#include <vector>
#include <string>

#include "OpenCV2/core.hpp"
#include "opencv2/calib3d.hpp"

using namespace cv;
using namespace std;

enum RECOGNITION_ERROR
{
	RECOGNITION_ERROR_OK = 1,

	RECOGNITION_ERROR_CAMERA_INFO_ALREADY_EXIST = -1,
	RECOGNITION_ERROR_CAMERA_INFO_NOT_EXIST = -2,
	RECOGNITION_ERROR_CAMERA_INFO_EMPTY = -3,
};

class CRecognition3DCameraInfo
{
public:
	CRecognition3DCameraInfo();
	~CRecognition3DCameraInfo();

private:
	struct StPositionInfo
	{
		Point2d ptImagePlane2DPoint;
		Point3d ptImagePlane3DPoint;
		Point3d ptCameraPos3DPoint;
	};
	struct StCameraInfo
	{
		StCameraInfo()
		{
			nWidth = 0;
			nHeight = 0;
		}
		int nChannel;

		int nWidth;
		int nHeight;

		Mat mIntrinsicMatrix;
		Mat mRotationMatrix;
		Mat mTranslationMatrix;
		
		Mat mExtrinsicMatrix;
		Mat mCameraPoseMatrix;

		vector<vector<StPositionInfo>> vecVecPtPoseAxis;
	};
	vector<StCameraInfo> m_vecCameraInfo;

public:
	int GetCameraInfoIndex(int nChannel);	
	int AddCameraInfo(
		int nChannel,
		int nWidth,
		int nHeight,
		Mat mIntrinsicMatrix,
		Mat mRotationMatrix,
		Mat mTranslationMatrix);
	int GetCameraInfoCount();

	int GetIntrinsicMatrixAs(int nChannel, Mat& mIntrinsicMatrix);
	int GetRotationMatrixAs(int nChannel, Mat& mRotationMatrix);
	int GetTranslationMatrixAs(int nChannel, Mat& mTranslationMatrix);

	int GetImagePlane3DPointAs(int nIndex, int nJointIndex, Point3d& ptImagePlane3DPoint);
	int GetCameraPos3DPointAs(int nIndex, int nJointIndex, Point3d& ptCameraPos3DPoint);

	int GetImagePlane3DPoints(int nChannel, vector<Point3d>& ptImagePlane3DPoint);

	int SetPoseAxis(int nChannel, int nTimeStamp, vector<Point2d> vecPtPoseAxis);

private:
	int translation_Extrinsic4x4_From_Rotation3x3_And_Translation3x1(int nChannel);
	int translation_CameraPose4x4_From_Rotation3x3_And_Translation3x1(int nChannel);

	int calcCameraPointAndImagePlanePoint(
		int nChannel,
		Point2d ptImagePlane2DPoint,
		Point3d& ptImagePlane3DPoint,
		Point3d& ptCameraPos3DPoint);
};

