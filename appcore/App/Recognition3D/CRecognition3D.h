#pragma once

#include <chrono>
#include <iostream>
#include <ctime>
#include <cstdlib>

#include "OpenCV2/core.hpp"
#include "OpenCV2/highgui.hpp"
#include "opencv2/calib3d.hpp"

#include "CVtkViewer.h"
#include "CRecognition3DCameraInfo.h"


class CRecognition3D
{
public:
	CRecognition3D(int nWidth, int nHeight);
	~CRecognition3D();

private:
	CVtkViewer* m_pVtkViewer;
	CRecognition3DCameraInfo m_pCameraInfo;

	int m_nWidth;
	int m_nHeight;

	const int m_nPoseJointCount = 16;

public:
	int Example();

	int Recognition3DPointCloud(vector<vector<Vec3d>>& vVecPointCloud);
	int Recognition3DPoint(
		const vector<vector<Vec3d>>& vVecPointCloud,
		vector<Point3d>& vecPtJoint);

	int ShowData(const vector<vector<Vec3d>>& vVecPointCloud);

public:
	int GetSkewMinCenterPointsFromTwoRays(
		const Point3d& ptLineAPoint1, const Point3d& ptLineAPoint2,
		const Point3d& ptLineBPoint1, const Point3d& ptLineBPoint2,
		Point3d& ptSkewMinimumPoint);

	int Point3dToVec3d(const Point3d& ptPoint, Vec3d& vVec);
	Point3d AverageCloudPoint(const vector<Vec3d>& vecPointCloud);

	int getBasicIntrinsic(cv::Mat& mIntrinsic);

	int getRandomRGBColor(int& nR, int& nG, int& nB);

};

