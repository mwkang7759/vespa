#include <fstream>

#include "CPose3D.h"

#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/viz.hpp>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/ostreamwrapper.h"

#if defined(_DEBUG)
#pragma comment(lib, "opencv_core440d.lib")
#pragma comment(lib, "opencv_highgui440d.lib")
#pragma comment(lib, "opencv_imgproc440d.lib")
#pragma comment(lib, "opencv_imgcodecs440d.lib")
#pragma comment(lib, "opencv_calib3d440d.lib")
#pragma comment(lib, "opencv_cudaimgproc440d.lib")
#pragma comment(lib, "opencv_cudawarping440d.lib")
#pragma comment(lib, "opencv_cudaarithm440d.lib")
#pragma comment(lib, "opencv_videoio440d.lib")
#pragma comment(lib, "opencv_calib3d440d.lib")
#pragma comment(lib, "opencv_viz440d.lib")
#else
#pragma comment(lib, "opencv_core440.lib")
#pragma comment(lib, "opencv_highgui440.lib")
#pragma comment(lib, "opencv_imgproc440.lib")
#pragma comment(lib, "opencv_imgcodecs440.lib")
#pragma comment(lib, "opencv_calib3d440.lib")
#pragma comment(lib, "opencv_cudaimgproc440.lib")
#pragma comment(lib, "opencv_cudawarping440.lib")
#pragma comment(lib, "opencv_cudaarithm440.lib")
#pragma comment(lib, "opencv_videoio440.lib")
#pragma comment(lib, "opencv_calib3d440.lib")
#pragma comment(lib, "opencv_viz440.lib")
#endif

using namespace rapidjson;
using namespace pose3d;

const int g_skeleton[19][2] = { {16, 14} ,{14, 12},{17, 15},{15, 13},{12, 13},{6, 12},{7, 13},{6, 7},{6, 8},{7, 9},{8, 10},{9, 11},{2, 3},{1, 2},{1, 3},{2, 4},{3, 5},{4, 6},{5, 7} };

void LoadCameraInfoJson(std::string path, int nWidth, int nHeight, CRecognition3DCameraInfo& cameraInfo)
{
	//Intrinsic Matrix ȹ��
	Mat mIntrinsicMatrix;
	float fSensorSize = 17.3;
	float fFocalLength = 50.;
	float fFocal = (fFocalLength / fSensorSize) * nWidth;

	mIntrinsicMatrix = (Mat_<double>(3, 3) <<
		(fFocalLength / fSensorSize) * nWidth, 0, nWidth / 2,
		0, (fFocalLength / fSensorSize) * nWidth, nHeight / 2,
		0, 0, 1);


	ifstream fin;
	fin.open(path, ios::in);

	string str;
	str.assign(istreambuf_iterator<char>(fin), istreambuf_iterator<char>());

	Document docu;
	docu.Parse(str.c_str());

	fin.close();

	int nCnt = 1;
	for (auto& value : docu["adjust_list"].GetArray())
	{

		//R|T Matrix ȹ��
		Mat mRotTest = (Mat_<double>(3, 1) <<
			value["extr"]["R1"].GetFloat(),
			value["extr"]["R2"].GetFloat(),
			value["extr"]["R3"].GetFloat()
			);
		Mat mTraTest = (Mat_<double>(3, 1) <<
			value["extr"]["T1"].GetFloat(),
			value["extr"]["T2"].GetFloat(),
			value["extr"]["T3"].GetFloat()
			);

		//Camera ���� �Է�
		cameraInfo.AddCameraInfo(nCnt, nWidth, nHeight, mIntrinsicMatrix, mRotTest, mTraTest);
		nCnt++;
	}

}

void DrawSkeleton(cv::Mat& img, DeepBase::TRTJointPoint_t joint[18])
{

	for (int i = 0; i < 19; i++)
	{
		int x1 = joint[(g_skeleton[i][0] - 1)].px;
		int y1 = joint[(g_skeleton[i][0] - 1)].py;
		int x2 = joint[(g_skeleton[i][1] - 1)].px;
		int y2 = joint[(g_skeleton[i][1] - 1)].py;

		if (x1 > 0 && y1 > 0 && x2 > 0 && y2 > 0)
			cv::line(img, cv::Point2f(x1, y1), cv::Point2f(x2, y2), CV_RGB(255, 0, 0), 3);
	}

	for (int i = 0; i < 18; i++)
	{
		int x = joint[i].px;
		int y = joint[i].py;

		if (x > 0 && y > 0)
			cv::circle(img, cv::Point(x, y), 4, CV_RGB(0, 255, 0), 2);
	}
}


int main()
{
	std::vector<cv::VideoCapture> vecVCList;

	int nWidth, nHeight;

	std::string dirPath = "F:\\RecordSave\\taekwondo";
	std::string listFile = dirPath + "\\file_list.txt";

	ifstream f;
	f.open(listFile, ios::in);

	while (!f.eof())
	{
		std::string strFileName;
		f >> strFileName;

		cv::VideoCapture cap;
		cap.open(dirPath + "\\" + strFileName);

		nWidth = cap.get(cv::CAP_PROP_FRAME_WIDTH);
		nHeight = cap.get(cv::CAP_PROP_FRAME_HEIGHT);

		vecVCList.push_back(cap);
	}

	if (vecVCList.size() == 0)
		return 0;


	CPose3D pose3d;
	pose3d.InitDLEngine(nWidth, nHeight, "c:\\test_contents\\object1650_TK.engine", "c:\\test_contents\\pose.engine");

	CRecognition3DCameraInfo m_pCameraInfo;
	LoadCameraInfoJson("F:\\RecordSave\\taekwondo\\adjust.adj", nWidth, nHeight, m_pCameraInfo);

	cv::cuda::GpuMat gpuImg;

	int nCnt = 0;
	while (1)
	{
		bool bCap = true;
		for (int idx = 0; idx < vecVCList.size(); idx++)
		{
			cv::Mat mSrc;
			vecVCList[idx] >> mSrc;

			if (mSrc.empty())
			{
				bCap = false;
				break;
			}
			gpuImg.release();
			gpuImg.upload(mSrc);

			std::vector<DeepBase::PoseData_t> vecPoses;
			if (pose3d.EstimatePoses(gpuImg, vecPoses) != 0)
				continue;
			else
			{
				int nCh = 1;
				for (auto& joint : vecPoses)
				{
					if (joint.detection_data.object_id == 0)
					{
						DrawSkeleton(mSrc, joint.arrJoint_data);

						vector<Point2d> vecPtPoseAxis;

						for (int j = 0; j < JOINT_COUNT; j++)
						{
							int x = joint.arrJoint_data[j].px;
							int y = joint.arrJoint_data[j].py;

							vecPtPoseAxis.push_back(Point2d(x, y));
						}

						m_pCameraInfo.SetPoseAxis(idx, 0, vecPtPoseAxis);
						nCh++;
					}
				}
				
				//cv::imshow("result", mSrc);
				cv::imwrite(dirPath + "\\result\\" + to_string(idx) + "_" + to_string(nCnt) + ".jpg", mSrc);
			}
		}
		if (!bCap)
			break;

		nCnt++;

		if (m_pCameraInfo.GetCameraInfoCount() < 4)
			continue;

		vector<Point3d> vecPtJoint(JOINT_COUNT);
		pose3d.ReconPose3D(m_pCameraInfo, vecPtJoint, true);
	}

	for (int i = 0; i < vecVCList.size(); i++)
	{
		vecVCList[i].release();
	}

	return 0;
}