#include "CRecognition3D.h"

CRecognition3D::CRecognition3D(int nWidth, int nHeight)
{
	m_nWidth = nWidth;
	m_nHeight = nHeight;

	m_pVtkViewer = new CVtkViewer;

	m_pVtkViewer->VtkInit();

	m_pVtkViewer->DrawBatterBoxCube();
	//m_pVtkViewer->DrawKZone();
	//m_pVtkViewer->DrawBatterBox();
}

CRecognition3D::~CRecognition3D()
{
	delete m_pVtkViewer;
}


int CRecognition3D::Example()
{
	//Intrinsic Matrix ȹ��
	Mat mIntrinsicMatrix;
	getBasicIntrinsic(mIntrinsicMatrix);

	std::chrono::system_clock::time_point start = std::chrono::system_clock::now();

	//R|T Matrix ȹ��
	Mat mRotTest = (Mat_<double>(3, 1) <<
		-1.3729701706840800,
		-0.5951019624838330,
		-0.6334652260221480
		);
	Mat mTraTest = (Mat_<double>(3, 1) <<
		-74.9169503249210000,
		30.6594591018829000,
		841.4183054562470000
		);

	//Camera ���� �Է�
	m_pCameraInfo.AddCameraInfo(1, m_nWidth, m_nHeight, mIntrinsicMatrix, mRotTest, mTraTest);

	//Pose ����Ȯ��
	vector<Point2d> vecPtPoseAxis;
	vecPtPoseAxis.push_back(Point2d(876., 406.));//head
	vecPtPoseAxis.push_back(Point2d(846., 433.));//neck
	vecPtPoseAxis.push_back(Point2d(773., 432.));//left sholder
	vecPtPoseAxis.push_back(Point2d(884., 518.));//right sholder
	vecPtPoseAxis.push_back(Point2d(784., 526.));//left elbow
	vecPtPoseAxis.push_back(Point2d(848., 619.));//right elbow
	vecPtPoseAxis.push_back(Point2d(868., 595.));//left wrisk
	vecPtPoseAxis.push_back(Point2d(883., 612.));//right wrisk
	vecPtPoseAxis.push_back(Point2d(769., 582.));//waist
	vecPtPoseAxis.push_back(Point2d(778., 668.));//hip
	vecPtPoseAxis.push_back(Point2d(732., 666.));//Left hip
	vecPtPoseAxis.push_back(Point2d(818., 666.));//Right hip
	vecPtPoseAxis.push_back(Point2d(714., 766.));//Left knee
	vecPtPoseAxis.push_back(Point2d(852., 768.));//Right knee
	vecPtPoseAxis.push_back(Point2d(712., 894.));//Left ankle
	vecPtPoseAxis.push_back(Point2d(895., 885.));//Right anlke

	//Pose ���� �Է�
	m_pCameraInfo.SetPoseAxis(1, 0, vecPtPoseAxis);
	vecPtPoseAxis.clear();

	mRotTest = (Mat_<double>(3, 1) <<
		-1.3784338335453300,
		-0.5807395559991250,
		-0.6203887914521140
		);
	mTraTest = (Mat_<double>(3, 1) <<
		-75.0241164512941000,
		30.4917987407416000,
		844.0885828470950000
		);

	m_pCameraInfo.AddCameraInfo(2, m_nWidth, m_nHeight, mIntrinsicMatrix, mRotTest, mTraTest);

	vecPtPoseAxis.push_back(Point2d(870., 405.));
	vecPtPoseAxis.push_back(Point2d(842., 435.));//neck
	vecPtPoseAxis.push_back(Point2d(769., 436.));
	vecPtPoseAxis.push_back(Point2d(887., 520.));
	vecPtPoseAxis.push_back(Point2d(793., 532.));//left elbow
	vecPtPoseAxis.push_back(Point2d(849., 620.));//right elbow
	vecPtPoseAxis.push_back(Point2d(864., 593.));//left wrisk
	vecPtPoseAxis.push_back(Point2d(881., 614.));//right wrisk
	vecPtPoseAxis.push_back(Point2d(774., 582.));//waist
	vecPtPoseAxis.push_back(Point2d(776., 661.));//hip
	vecPtPoseAxis.push_back(Point2d(735., 664.));//Left hip
	vecPtPoseAxis.push_back(Point2d(807., 662.));//Right hip
	vecPtPoseAxis.push_back(Point2d(712., 772.));//Left knee
	vecPtPoseAxis.push_back(Point2d(843., 766.));//Right knee
	vecPtPoseAxis.push_back(Point2d(710., 894.));//Left ankle
	vecPtPoseAxis.push_back(Point2d(891., 882.));//Right anlke
	m_pCameraInfo.SetPoseAxis(2, 0, vecPtPoseAxis);
	vecPtPoseAxis.clear();

	mRotTest = (Mat_<double>(3, 1) <<
		-1.3797824542397200,
		-0.5696166652558380,
		-0.6092366004608340
		);
	mTraTest = (Mat_<double>(3, 1) <<
		-75.0025655441668000,
		30.5176026478896000,
		846.7481339786800000
		);

	m_pCameraInfo.AddCameraInfo(3, m_nWidth, m_nHeight, mIntrinsicMatrix, mRotTest, mTraTest);

	vecPtPoseAxis.push_back(Point2d(868., 406.));
	vecPtPoseAxis.push_back(Point2d(838., 435.));//neck
	vecPtPoseAxis.push_back(Point2d(769., 437.));
	vecPtPoseAxis.push_back(Point2d(873., 511.));
	vecPtPoseAxis.push_back(Point2d(784., 540.));//left elbow
	vecPtPoseAxis.push_back(Point2d(853., 628.));//right elbow
	vecPtPoseAxis.push_back(Point2d(882., 588.));//left wrisk
	vecPtPoseAxis.push_back(Point2d(892., 606.));//right wrisk
	vecPtPoseAxis.push_back(Point2d(764., 577.));//waist
	vecPtPoseAxis.push_back(Point2d(768., 660.));//hip
	vecPtPoseAxis.push_back(Point2d(728., 666.));//Left hip
	vecPtPoseAxis.push_back(Point2d(805., 662.));//Right hip
	vecPtPoseAxis.push_back(Point2d(710., 768.));//Left knee
	vecPtPoseAxis.push_back(Point2d(846., 765.));//Right knee
	vecPtPoseAxis.push_back(Point2d(708., 898.));//Left ankle
	vecPtPoseAxis.push_back(Point2d(889., 886.));//Right anlke
	m_pCameraInfo.SetPoseAxis(3, 0, vecPtPoseAxis);
	vecPtPoseAxis.clear();

	mRotTest = (Mat_<double>(3, 1) <<
		-1.3938853345791700,
		-0.5254733423569620,
		-0.5622398088357440
		);
	mTraTest = (Mat_<double>(3, 1) <<
		-74.5136692688982000,
		30.0344856602752000,
		849.2313225445370000
		);

	m_pCameraInfo.AddCameraInfo(4, m_nWidth, m_nHeight, mIntrinsicMatrix, mRotTest, mTraTest);

	vecPtPoseAxis.push_back(Point2d(858., 403.));
	vecPtPoseAxis.push_back(Point2d(829., 436.));//neck
	vecPtPoseAxis.push_back(Point2d(756., 432.));
	vecPtPoseAxis.push_back(Point2d(868., 513.));
	vecPtPoseAxis.push_back(Point2d(783., 546.));//left elbow
	vecPtPoseAxis.push_back(Point2d(835., 616.));//right elbow
	vecPtPoseAxis.push_back(Point2d(874., 585.));//left wrisk
	vecPtPoseAxis.push_back(Point2d(872., 606.));//right wrisk
	vecPtPoseAxis.push_back(Point2d(751., 578.));//waist
	vecPtPoseAxis.push_back(Point2d(753., 657.));//hip
	vecPtPoseAxis.push_back(Point2d(712., 660.));//Left hip
	vecPtPoseAxis.push_back(Point2d(792., 658.));//Right hip
	vecPtPoseAxis.push_back(Point2d(702., 765.));//Left knee
	vecPtPoseAxis.push_back(Point2d(836., 766.));//Right knee
	vecPtPoseAxis.push_back(Point2d(704., 892.));//Left ankle
	vecPtPoseAxis.push_back(Point2d(870., 876.));//Right anlke
	m_pCameraInfo.SetPoseAxis(4, 0, vecPtPoseAxis);
	vecPtPoseAxis.clear();

	mRotTest = (Mat_<double>(3, 1) <<
		-1.3953137839271200,
		-0.5114939673218230,
		-0.5511483061115420
		);
	mTraTest = (Mat_<double>(3, 1) <<
		-74.4020864847346000,
		29.9615597657396000,
		851.9263214069170000
		);

	m_pCameraInfo.AddCameraInfo(5, m_nWidth, m_nHeight, mIntrinsicMatrix, mRotTest, mTraTest);

	vecPtPoseAxis.push_back(Point2d(855., 403.));
	vecPtPoseAxis.push_back(Point2d(826., 435.));//neck
	vecPtPoseAxis.push_back(Point2d(757., 432.));
	vecPtPoseAxis.push_back(Point2d(861., 514.));
	vecPtPoseAxis.push_back(Point2d(777., 546.));//left elbow
	vecPtPoseAxis.push_back(Point2d(836., 614.));//right elbow
	vecPtPoseAxis.push_back(Point2d(873., 585.));//left wrisk
	vecPtPoseAxis.push_back(Point2d(870., 608.));//right wrisk
	vecPtPoseAxis.push_back(Point2d(751., 574.));//waist
	vecPtPoseAxis.push_back(Point2d(747., 660.));//hip
	vecPtPoseAxis.push_back(Point2d(710., 662.));//Left hip
	vecPtPoseAxis.push_back(Point2d(788., 660.));//Right hip
	vecPtPoseAxis.push_back(Point2d(702., 771.));//Left knee
	vecPtPoseAxis.push_back(Point2d(831., 763.));//Right knee
	vecPtPoseAxis.push_back(Point2d(705., 891.));//Left ankle
	vecPtPoseAxis.push_back(Point2d(865., 880.));//Right anlke
	m_pCameraInfo.SetPoseAxis(5, 0, vecPtPoseAxis);
	vecPtPoseAxis.clear();

	mRotTest = (Mat_<double>(3, 1) <<
		-1.3984712029853800,
		-0.4987284424713910,
		-0.5338504603558830
		);
	mTraTest = (Mat_<double>(3, 1) <<
		-74.2619512142004000,
		30.1006241532567000,
		856.3841544603740000
		);

	m_pCameraInfo.AddCameraInfo(6, m_nWidth, m_nHeight, mIntrinsicMatrix, mRotTest, mTraTest);

	vecPtPoseAxis.push_back(Point2d(853., 403.));
	vecPtPoseAxis.push_back(Point2d(824., 435.));//neck
	vecPtPoseAxis.push_back(Point2d(752., 432.));
	vecPtPoseAxis.push_back(Point2d(855., 514.));
	vecPtPoseAxis.push_back(Point2d(794., 542.));//left elbow
	vecPtPoseAxis.push_back(Point2d(834., 614.));//right elbow
	vecPtPoseAxis.push_back(Point2d(870., 589.));//left wrisk
	vecPtPoseAxis.push_back(Point2d(870., 610.));//right wrisk
	vecPtPoseAxis.push_back(Point2d(744., 573.));//waist
	vecPtPoseAxis.push_back(Point2d(742., 660.));//hip
	vecPtPoseAxis.push_back(Point2d(704., 663.));//Left hip
	vecPtPoseAxis.push_back(Point2d(780., 657.));//Right hip
	vecPtPoseAxis.push_back(Point2d(698., 770.));//Left knee
	vecPtPoseAxis.push_back(Point2d(824., 760.));//Right knee
	vecPtPoseAxis.push_back(Point2d(700., 890.));//Left ankle
	vecPtPoseAxis.push_back(Point2d(860., 874.));//Right anlke
	m_pCameraInfo.SetPoseAxis(6, 0, vecPtPoseAxis);
	vecPtPoseAxis.clear();

	mRotTest = (Mat_<double>(3, 1) <<
		-1.4028489423099900,
		-0.4841879846936240,
		-0.5216411427492950
		);
	mTraTest = (Mat_<double>(3, 1) <<
		-74.1351801550424000,
		30.1687985665358000,
		859.5301569627700000
		);

	m_pCameraInfo.AddCameraInfo(7, m_nWidth, m_nHeight, mIntrinsicMatrix, mRotTest, mTraTest);

	vecPtPoseAxis.push_back(Point2d(848., 404.));
	vecPtPoseAxis.push_back(Point2d(816., 436.));//neck
	vecPtPoseAxis.push_back(Point2d(756., 438.));
	vecPtPoseAxis.push_back(Point2d(853., 515.));
	vecPtPoseAxis.push_back(Point2d(792., 540.));//left elbow
	vecPtPoseAxis.push_back(Point2d(832., 614.));//right elbow
	vecPtPoseAxis.push_back(Point2d(866., 582.));//left wrisk
	vecPtPoseAxis.push_back(Point2d(868., 607.));//right wrisk
	vecPtPoseAxis.push_back(Point2d(741., 576.));//waist
	vecPtPoseAxis.push_back(Point2d(738., 661.));//hip
	vecPtPoseAxis.push_back(Point2d(702., 660.));//Left hip
	vecPtPoseAxis.push_back(Point2d(778., 661.));//Right hip
	vecPtPoseAxis.push_back(Point2d(716., 768.));//Left knee
	vecPtPoseAxis.push_back(Point2d(849., 762.));//Right knee
	vecPtPoseAxis.push_back(Point2d(711., 892.));//Left ankle
	vecPtPoseAxis.push_back(Point2d(896., 880.));//Right anlke
	m_pCameraInfo.SetPoseAxis(7, 0, vecPtPoseAxis);
	vecPtPoseAxis.clear();

	mRotTest = (Mat_<double>(3, 1) <<
		-1.4049249051658400,
		-0.4712629543046360,
		-0.5067517621668670
		);
	mTraTest = (Mat_<double>(3, 1) <<
		-73.8259273791567000,
		29.8498605936601000,
		857.1820857457610000
		);

	m_pCameraInfo.AddCameraInfo(8, m_nWidth, m_nHeight, mIntrinsicMatrix, mRotTest, mTraTest);

	vecPtPoseAxis.push_back(Point2d(844., 404.));
	vecPtPoseAxis.push_back(Point2d(817., 435.));//neck
	vecPtPoseAxis.push_back(Point2d(747., 434.));
	vecPtPoseAxis.push_back(Point2d(851., 513.));
	vecPtPoseAxis.push_back(Point2d(783., 531.));//left elbow
	vecPtPoseAxis.push_back(Point2d(828., 613.));//right elbow
	vecPtPoseAxis.push_back(Point2d(865., 589.));//left wrisk
	vecPtPoseAxis.push_back(Point2d(868., 606.));//right wrisk
	vecPtPoseAxis.push_back(Point2d(730., 576.));//waist
	vecPtPoseAxis.push_back(Point2d(734., 657.));//hip
	vecPtPoseAxis.push_back(Point2d(697., 662.));//Left hip
	vecPtPoseAxis.push_back(Point2d(775., 658.));//Right hip
	vecPtPoseAxis.push_back(Point2d(711., 772.));//Left knee
	vecPtPoseAxis.push_back(Point2d(842., 768.));//Right knee
	vecPtPoseAxis.push_back(Point2d(708., 889.));//Left ankle
	vecPtPoseAxis.push_back(Point2d(896., 884.));//Right anlke
	m_pCameraInfo.SetPoseAxis(8, 0, vecPtPoseAxis);
	vecPtPoseAxis.clear();

	mRotTest = (Mat_<double>(3, 1) <<
		-1.4094061696145500,
		-0.4527769622256880,
		-0.4902832483728090
		);
	mTraTest = (Mat_<double>(3, 1) <<
		-73.3764671475015000,
		29.9073782805019000,
		861.1716590996700000
		);

	m_pCameraInfo.AddCameraInfo(9, m_nWidth, m_nHeight, mIntrinsicMatrix, mRotTest, mTraTest);

	vecPtPoseAxis.push_back(Point2d(840., 404.));
	vecPtPoseAxis.push_back(Point2d(811., 435.));//neck
	vecPtPoseAxis.push_back(Point2d(748., 432.));
	vecPtPoseAxis.push_back(Point2d(846., 511.));
	vecPtPoseAxis.push_back(Point2d(776., 532.));//left elbow
	vecPtPoseAxis.push_back(Point2d(822., 612.));//right elbow
	vecPtPoseAxis.push_back(Point2d(864., 579.));//left wrisk
	vecPtPoseAxis.push_back(Point2d(862., 607.));//right wrisk
	vecPtPoseAxis.push_back(Point2d(727., 573.));//waist
	vecPtPoseAxis.push_back(Point2d(728., 662.));//hip
	vecPtPoseAxis.push_back(Point2d(694., 662.));//Left hip
	vecPtPoseAxis.push_back(Point2d(765., 660.));//Right hip
	vecPtPoseAxis.push_back(Point2d(702., 660.));//Left knee
	vecPtPoseAxis.push_back(Point2d(778., 661.));//Right knee
	vecPtPoseAxis.push_back(Point2d(702., 660.));//Left ankle
	vecPtPoseAxis.push_back(Point2d(778., 661.));//Right anlke
	m_pCameraInfo.SetPoseAxis(9, 0, vecPtPoseAxis);
	vecPtPoseAxis.clear();

	mRotTest = (Mat_<double>(3, 1) <<
		-1.4457871222195700,
		0.1482297138503350,
		0.1641979988664890
		);
	mTraTest = (Mat_<double>(3, 1) <<
		-36.8077561882955000,
		24.1737224546279000,
		898.4809354410240000
		);

	m_pCameraInfo.AddCameraInfo(10, m_nWidth, m_nHeight, mIntrinsicMatrix, mRotTest, mTraTest);

	vecPtPoseAxis.push_back(Point2d(742., 396.));
	vecPtPoseAxis.push_back(Point2d(724., 427.));//neck
	vecPtPoseAxis.push_back(Point2d(708., 423.));
	vecPtPoseAxis.push_back(Point2d(729., 500.));
	vecPtPoseAxis.push_back(Point2d(782., 504.));//left elbow
	vecPtPoseAxis.push_back(Point2d(726., 594.));//right elbow
	vecPtPoseAxis.push_back(Point2d(830., 564.));//left wrisk
	vecPtPoseAxis.push_back(Point2d(808., 595.));//right wrisk
	vecPtPoseAxis.push_back(Point2d(660., 560.));//waist
	vecPtPoseAxis.push_back(Point2d(675., 644.));//hip
	vecPtPoseAxis.push_back(Point2d(669., 660.));//Left hip
	vecPtPoseAxis.push_back(Point2d(774., 661.));//Right hip
	vecPtPoseAxis.push_back(Point2d(721., 736.));//Left knee
	vecPtPoseAxis.push_back(Point2d(775., 736.));//Right knee
	vecPtPoseAxis.push_back(Point2d(718., 838.));//Left ankle
	vecPtPoseAxis.push_back(Point2d(688., 853.));//Right anlke
	m_pCameraInfo.SetPoseAxis(10, 0, vecPtPoseAxis);
	vecPtPoseAxis.clear();

	mRotTest = (Mat_<double>(3, 1) <<
		-1.4438121160872500,
		0.1945387340635000,
		0.2145976908653500
		);
	mTraTest = (Mat_<double>(3, 1) <<
		-32.5007830415527000,
		24.2963968892164000,
		905.3346166452080000
		);

	m_pCameraInfo.AddCameraInfo(11, m_nWidth, m_nHeight, mIntrinsicMatrix, mRotTest, mTraTest);

	vecPtPoseAxis.push_back(Point2d(742., 397.));
	vecPtPoseAxis.push_back(Point2d(726., 429.));//neck
	vecPtPoseAxis.push_back(Point2d(711., 424.));
	vecPtPoseAxis.push_back(Point2d(723., 503.));
	vecPtPoseAxis.push_back(Point2d(787., 505.));//left elbow
	vecPtPoseAxis.push_back(Point2d(727., 594.));//right elbow
	vecPtPoseAxis.push_back(Point2d(835., 566.));//left wrisk
	vecPtPoseAxis.push_back(Point2d(811., 596.));//right wrisk
	vecPtPoseAxis.push_back(Point2d(668., 567.));//waist
	vecPtPoseAxis.push_back(Point2d(674., 627.));//hip
	vecPtPoseAxis.push_back(Point2d(680., 638.));//Left hip
	vecPtPoseAxis.push_back(Point2d(685., 636.));//Right hip
	vecPtPoseAxis.push_back(Point2d(739., 729.));//Left knee
	vecPtPoseAxis.push_back(Point2d(752., 728.));//Right knee
	vecPtPoseAxis.push_back(Point2d(715., 818.));//Left ankle
	vecPtPoseAxis.push_back(Point2d(663., 836.));//Right anlke
	m_pCameraInfo.SetPoseAxis(11, 0, vecPtPoseAxis);
	vecPtPoseAxis.clear();

	mRotTest = (Mat_<double>(3, 1) <<
		-1.4386410838220900,
		0.2354471365955030,
		0.2566514428155540
		);
	mTraTest = (Mat_<double>(3, 1) <<
		-29.0580169685853000,
		24.1690555969823000,
		906.8684323883040000
		);

	m_pCameraInfo.AddCameraInfo(12, m_nWidth, m_nHeight, mIntrinsicMatrix, mRotTest, mTraTest);

	vecPtPoseAxis.push_back(Point2d(741., 395.));
	vecPtPoseAxis.push_back(Point2d(724., 427.));//neck
	vecPtPoseAxis.push_back(Point2d(715., 424.));
	vecPtPoseAxis.push_back(Point2d(721., 504.));
	vecPtPoseAxis.push_back(Point2d(790., 506.));//left elbow
	vecPtPoseAxis.push_back(Point2d(724., 590.));//right elbow
	vecPtPoseAxis.push_back(Point2d(831., 561.));//left wrisk
	vecPtPoseAxis.push_back(Point2d(811., 597.));//right wrisk
	vecPtPoseAxis.push_back(Point2d(673., 561.));//waist
	vecPtPoseAxis.push_back(Point2d(666., 621.));//hip
	vecPtPoseAxis.push_back(Point2d(661., 624.));//Left hip
	vecPtPoseAxis.push_back(Point2d(666., 626.));//Right hip
	vecPtPoseAxis.push_back(Point2d(759., 730.));//Left knee
	vecPtPoseAxis.push_back(Point2d(771., 748.));//Right knee
	vecPtPoseAxis.push_back(Point2d(716., 835.));//Left ankle
	vecPtPoseAxis.push_back(Point2d(668., 830.));//Right anlke
	m_pCameraInfo.SetPoseAxis(12, 0, vecPtPoseAxis);
	vecPtPoseAxis.clear();

	mRotTest = (Mat_<double>(3, 1) <<
		-1.4384738225524800,
		0.2672864176874450,
		0.2887585798033730
		);
	mTraTest = (Mat_<double>(3, 1) <<
		-26.0216071241726000,
		24.4054535289305000,
		910.2835396594400000
		);

	m_pCameraInfo.AddCameraInfo(13, m_nWidth, m_nHeight, mIntrinsicMatrix, mRotTest, mTraTest);

	vecPtPoseAxis.push_back(Point2d(740., 395.));
	vecPtPoseAxis.push_back(Point2d(726., 426.));//neck
	vecPtPoseAxis.push_back(Point2d(716., 429.));
	vecPtPoseAxis.push_back(Point2d(726., 504.));
	vecPtPoseAxis.push_back(Point2d(795., 501.));//left elbow
	vecPtPoseAxis.push_back(Point2d(729., 592.));//right elbow
	vecPtPoseAxis.push_back(Point2d(838., 566.));//left wrisk
	vecPtPoseAxis.push_back(Point2d(813., 596.));//right wrisk
	vecPtPoseAxis.push_back(Point2d(678., 561.));//waist
	vecPtPoseAxis.push_back(Point2d(678., 632.));//hip
	vecPtPoseAxis.push_back(Point2d(678., 626.));//Left hip
	vecPtPoseAxis.push_back(Point2d(685., 630.));//Right hip
	vecPtPoseAxis.push_back(Point2d(759., 752.));//Left knee
	vecPtPoseAxis.push_back(Point2d(766., 747.));//Right knee
	vecPtPoseAxis.push_back(Point2d(786., 848.));//Left ankle
	vecPtPoseAxis.push_back(Point2d(658., 841.));//Right anlke
	m_pCameraInfo.SetPoseAxis(13, 0, vecPtPoseAxis);
	vecPtPoseAxis.clear();

	mRotTest = (Mat_<double>(3, 1) <<
		-1.4371322665396700,
		0.2884287003810220,
		0.3111939884572060
		);
	mTraTest = (Mat_<double>(3, 1) <<
		-24.0529680338860000,
		24.4070382637950000,
		909.0154129048960000
		);

	m_pCameraInfo.AddCameraInfo(14, m_nWidth, m_nHeight, mIntrinsicMatrix, mRotTest, mTraTest);

	vecPtPoseAxis.push_back(Point2d(742., 394.));
	vecPtPoseAxis.push_back(Point2d(727., 423.));//neck
	vecPtPoseAxis.push_back(Point2d(722., 423.));
	vecPtPoseAxis.push_back(Point2d(724., 509.));
	vecPtPoseAxis.push_back(Point2d(796., 499.));//left elbow
	vecPtPoseAxis.push_back(Point2d(730., 585.));//right elbow
	vecPtPoseAxis.push_back(Point2d(836., 565.));//left wrisk
	vecPtPoseAxis.push_back(Point2d(805., 590.));//right wrisk
	vecPtPoseAxis.push_back(Point2d(676., 567.));//waist
	vecPtPoseAxis.push_back(Point2d(675., 625.));//hip
	vecPtPoseAxis.push_back(Point2d(679., 622.));//Left hip
	vecPtPoseAxis.push_back(Point2d(676., 625.));//Right hip
	vecPtPoseAxis.push_back(Point2d(754., 718.));//Left knee
	vecPtPoseAxis.push_back(Point2d(766., 750.));//Right knee
	vecPtPoseAxis.push_back(Point2d(771., 848.));//Left ankle
	vecPtPoseAxis.push_back(Point2d(661., 838.));//Right anlke
	m_pCameraInfo.SetPoseAxis(14, 0, vecPtPoseAxis);
	vecPtPoseAxis.clear();

	mRotTest = (Mat_<double>(3, 1) <<
		-1.4341161150937200,
		0.3125790993716450,
		0.3351758287266180
		);
	mTraTest = (Mat_<double>(3, 1) <<
		-21.7532351831083000,
		24.3413828562464000,
		912.6816719213010000
		);

	m_pCameraInfo.AddCameraInfo(15, m_nWidth, m_nHeight, mIntrinsicMatrix, mRotTest, mTraTest);

	vecPtPoseAxis.push_back(Point2d(742., 393.));
	vecPtPoseAxis.push_back(Point2d(726., 423.));//neck
	vecPtPoseAxis.push_back(Point2d(721., 420.));
	vecPtPoseAxis.push_back(Point2d(727., 504.));
	vecPtPoseAxis.push_back(Point2d(799., 499.));//left elbow
	vecPtPoseAxis.push_back(Point2d(734., 585.));//right elbow
	vecPtPoseAxis.push_back(Point2d(835., 565.));//left wrisk
	vecPtPoseAxis.push_back(Point2d(805., 591.));//right wrisk
	vecPtPoseAxis.push_back(Point2d(679., 561.));//waist
	vecPtPoseAxis.push_back(Point2d(681., 619.));//hip
	vecPtPoseAxis.push_back(Point2d(690., 618.));//Left hip
	vecPtPoseAxis.push_back(Point2d(694., 625.));//Right hip
	vecPtPoseAxis.push_back(Point2d(757., 739.));//Left knee
	vecPtPoseAxis.push_back(Point2d(772., 745.));//Right knee
	vecPtPoseAxis.push_back(Point2d(794., 837.));//Left ankle
	vecPtPoseAxis.push_back(Point2d(668., 835.));//Right anlke
	m_pCameraInfo.SetPoseAxis(15, 0, vecPtPoseAxis);
	vecPtPoseAxis.clear();

	mRotTest = (Mat_<double>(3, 1) <<
		-1.4312632777068600,
		0.3325082786143140,
		0.3581528072425260
		);
	mTraTest = (Mat_<double>(3, 1) <<
		-19.5945152411580000,
		24.4580740092456000,
		915.7685735925300000
		);

	m_pCameraInfo.AddCameraInfo(16, m_nWidth, m_nHeight, mIntrinsicMatrix, mRotTest, mTraTest);

	vecPtPoseAxis.push_back(Point2d(744., 392.));
	vecPtPoseAxis.push_back(Point2d(730., 423.));//neck
	vecPtPoseAxis.push_back(Point2d(724., 427.));
	vecPtPoseAxis.push_back(Point2d(725., 502.));
	vecPtPoseAxis.push_back(Point2d(801., 499.));//left elbow
	vecPtPoseAxis.push_back(Point2d(726., 583.));//right elbow
	vecPtPoseAxis.push_back(Point2d(843., 562.));//left wrisk
	vecPtPoseAxis.push_back(Point2d(813., 595.));//right wrisk
	vecPtPoseAxis.push_back(Point2d(690., 560.));//waist
	vecPtPoseAxis.push_back(Point2d(691., 622.));//hip
	vecPtPoseAxis.push_back(Point2d(693., 624.));//Left hip
	vecPtPoseAxis.push_back(Point2d(693., 627.));//Right hip
	vecPtPoseAxis.push_back(Point2d(758., 738.));//Left knee
	vecPtPoseAxis.push_back(Point2d(775., 742.));//Right knee
	vecPtPoseAxis.push_back(Point2d(798., 843.));//Left ankle
	vecPtPoseAxis.push_back(Point2d(682., 838.));//Right anlke
	m_pCameraInfo.SetPoseAxis(16, 0, vecPtPoseAxis);
	vecPtPoseAxis.clear();

	mRotTest = (Mat_<double>(3, 1) <<
		-1.4339520693948100,
		0.3406838269981000,
		0.3710869310765790
		);
	mTraTest = (Mat_<double>(3, 1) <<
		-18.7070121868374000,
		24.6441756737855000,
		915.7651136358870000
		);

	m_pCameraInfo.AddCameraInfo(17, m_nWidth, m_nHeight, mIntrinsicMatrix, mRotTest, mTraTest);

	vecPtPoseAxis.push_back(Point2d(743., 393.));
	vecPtPoseAxis.push_back(Point2d(729., 424.));//neck
	vecPtPoseAxis.push_back(Point2d(729., 426.));
	vecPtPoseAxis.push_back(Point2d(722., 498.));
	vecPtPoseAxis.push_back(Point2d(704., 501.));//left elbow
	vecPtPoseAxis.push_back(Point2d(732., 585.));//right elbow
	vecPtPoseAxis.push_back(Point2d(840., 560.));//left wrisk
	vecPtPoseAxis.push_back(Point2d(811., 596.));//right wrisk
	vecPtPoseAxis.push_back(Point2d(688., 559.));//waist
	vecPtPoseAxis.push_back(Point2d(690., 624.));//hip
	vecPtPoseAxis.push_back(Point2d(698., 625.));//Left hip
	vecPtPoseAxis.push_back(Point2d(690., 626.));//Right hip
	vecPtPoseAxis.push_back(Point2d(766., 723.));//Left knee
	vecPtPoseAxis.push_back(Point2d(778., 742.));//Right knee
	vecPtPoseAxis.push_back(Point2d(802., 847.));//Left ankle
	vecPtPoseAxis.push_back(Point2d(688., 840.));//Right anlke
	m_pCameraInfo.SetPoseAxis(17, 0, vecPtPoseAxis);
	vecPtPoseAxis.clear();

	mRotTest = (Mat_<double>(3, 1) <<
		-1.4298636553499800,
		0.3560190202124510,
		0.3865160122893060
		);
	mTraTest = (Mat_<double>(3, 1) <<
		-17.1706587511078000,
		24.5323673735653000,
		918.6505226964260000
		);

	m_pCameraInfo.AddCameraInfo(18, m_nWidth, m_nHeight, mIntrinsicMatrix, mRotTest, mTraTest);

	vecPtPoseAxis.push_back(Point2d(744., 392.));
	vecPtPoseAxis.push_back(Point2d(732., 423.));//neck
	vecPtPoseAxis.push_back(Point2d(736., 428.));
	vecPtPoseAxis.push_back(Point2d(722., 497.));
	vecPtPoseAxis.push_back(Point2d(806., 499.));//left elbow
	vecPtPoseAxis.push_back(Point2d(736., 585.));//right elbow
	vecPtPoseAxis.push_back(Point2d(843., 561.));//left wrisk
	vecPtPoseAxis.push_back(Point2d(811., 591.));//right wrisk
	vecPtPoseAxis.push_back(Point2d(698., 562.));//waist
	vecPtPoseAxis.push_back(Point2d(696., 628.));//hip
	vecPtPoseAxis.push_back(Point2d(696., 626.));//Left hip
	vecPtPoseAxis.push_back(Point2d(698., 628.));//Right hip
	vecPtPoseAxis.push_back(Point2d(766., 728.));//Left knee
	vecPtPoseAxis.push_back(Point2d(778., 740.));//Right knee
	vecPtPoseAxis.push_back(Point2d(806., 846.));//Left ankle
	vecPtPoseAxis.push_back(Point2d(692., 844.));//Right anlke
	m_pCameraInfo.SetPoseAxis(18, 0, vecPtPoseAxis);
	vecPtPoseAxis.clear();

	//�Էµ� ����������� ���� �� ������ Point Cloud ���� ȹ��
	vector<vector<Vec3d>> vVecPointCloud(m_nPoseJointCount);
	Recognition3DPointCloud(vVecPointCloud);

	//Point Cloud�κ��� �� ������ ��� ��ġ ȹ��
	vector<Point3d> vecPtJoint;
	Recognition3DPoint(vVecPointCloud, vecPtJoint);

	std::chrono::duration<double> sec = std::chrono::system_clock::now() - start;
	std::cout << "Process Time : " << sec.count() << std::endl;

	//Point Cloud �� ���� �����ġ ������ Viewer�� ǥ��
	ShowData(vVecPointCloud);

	return 1;
}

int CRecognition3D::Recognition3DPointCloud(vector<vector<Vec3d>>& vVecPointCloud)
{
	for (int i = 0; i < m_pCameraInfo.GetCameraInfoCount(); i++)
	{
		for (int j = 0; j < m_pCameraInfo.GetCameraInfoCount(); j++)
		{
			if (i == j)
				continue;
			Point3d ptPointSkewMinimumPoint;
			Point3d ptImagePlanePoint1, ptCameraPosPoint1, ptImagePlanePoint2, ptCameraPosPoint2;

			for (int k = 0; k < m_nPoseJointCount; k++)
			{
				m_pCameraInfo.GetImagePlane3DPointAs(i, k, ptImagePlanePoint1);
				m_pCameraInfo.GetCameraPos3DPointAs(i, k, ptCameraPosPoint1);
				m_pCameraInfo.GetImagePlane3DPointAs(j, k, ptImagePlanePoint2);
				m_pCameraInfo.GetCameraPos3DPointAs(j, k, ptCameraPosPoint2);

				GetSkewMinCenterPointsFromTwoRays(
					ptImagePlanePoint1, ptCameraPosPoint1,
					ptImagePlanePoint2, ptCameraPosPoint2,
					ptPointSkewMinimumPoint);

				string strWidgetName1 = "Skew_" + to_string(i) + "_" + to_string(j) + "_" + to_string(k);

				Vec3d pt;
				Point3dToVec3d(ptPointSkewMinimumPoint, pt);
				vVecPointCloud[k].push_back(pt);

			}
		}
	}
	return 1;
}

int CRecognition3D::Recognition3DPoint(
	const vector<vector<Vec3d>>& vVecPointCloud,
	vector<Point3d>& vecPtJoint)
{	
	vecPtJoint.clear();
	for (int i = 0; i < m_nPoseJointCount; i++)
	{
		Point3d ptJoint = AverageCloudPoint(vVecPointCloud[i]);
		vecPtJoint.push_back(ptJoint);
	}

	return 1;
}

int CRecognition3D::ShowData(const vector<vector<Vec3d>>& vVecPointCloud)
{
	vector<Point3d> vecPtJoint;
	for (int i = 0; i < m_nPoseJointCount; i++)
	{
		int nR = 0, nG = 0, nB = 0;
		getRandomRGBColor(nR, nG, nB);

		string strWidgetName = "Cloud_" + to_string(i);
		m_pVtkViewer->ShowCloud(
			strWidgetName, 
			vVecPointCloud[i], 
			nR, nG, nB);

		Point3d ptJoint = AverageCloudPoint(vVecPointCloud[i]);
		m_pVtkViewer->InputPoint(
			strWidgetName + "_avg", 
			ptJoint,
			nR, nG, nB);

		vecPtJoint.push_back(ptJoint);
	}

	m_pVtkViewer->DrawJoint(vecPtJoint);

	m_pVtkViewer->ShowViewer();

	return 1;
}

int CRecognition3D::GetSkewMinCenterPointsFromTwoRays(
	const Point3d& ptLineAPoint1, const Point3d& ptLineAPoint2,
	const Point3d& ptLineBPoint1, const Point3d& ptLineBPoint2,
	Point3d& ptSkewMinimumPoint)
{
	Point3d ptVectorA = ptLineAPoint2 - ptLineAPoint1;
	Point3d ptVectorB = ptLineBPoint2 - ptLineBPoint1;
	Point3d ptVectorW = ptLineBPoint1 - ptLineAPoint1;

	double dNormVectorA = norm(ptVectorA);
	double dNormVectorB = norm(ptVectorB);

	Point3d ptUnitVectorA = ptVectorA / dNormVectorA;
	Point3d ptUnitVectorB = ptVectorB / dNormVectorB;

	double dA = ptUnitVectorA.dot(ptUnitVectorA);
	double dB = ptUnitVectorA.dot(ptUnitVectorB);
	double dC = ptUnitVectorB.dot(ptUnitVectorB);
	double dD = ptUnitVectorA.dot(ptVectorW);
	double dE = ptUnitVectorB.dot(ptVectorW);

	double dSkewMinPointParamA = ((dB * dE) - (dC * dD)) / ((dA * dC) - (dB * dB));
	double dSkewMinPointParamB = ((dA * dE) - (dB * dD)) / ((dA * dC) - (dB * dB));

	Point3d ptSkewAMinimumPoint = ptLineAPoint1 - dSkewMinPointParamA * ptUnitVectorA;
	Point3d ptSkewBMinimumPoint = ptLineBPoint1 - dSkewMinPointParamB * ptUnitVectorB;

	ptSkewMinimumPoint = (ptSkewAMinimumPoint + ptSkewBMinimumPoint) / 2.;

	return 1;
}

int CRecognition3D::Point3dToVec3d(const Point3d& ptPoint, Vec3d& vVec)
{
	vVec = Vec3d(ptPoint.x, ptPoint.y, ptPoint.z);

	return 1;
}

Point3d CRecognition3D::AverageCloudPoint(const vector<Vec3d>& vecPointCloud)
{
	if (vecPointCloud.size() <= 0)
		return Point3d();

	double dSumX = 0;
	double dSumY = 0;
	double dSumZ = 0;

	for (int i = 0; i < vecPointCloud.size(); i++)
	{
		dSumX += vecPointCloud.at(i)[0];
		dSumY += vecPointCloud.at(i)[1];
		dSumZ += vecPointCloud.at(i)[2];
	}

	Point3d ptResult = Point3d(
		dSumX / (vecPointCloud.size()),
		dSumY / (vecPointCloud.size()),
		dSumZ / (vecPointCloud.size()));

	return ptResult;
}

int CRecognition3D::getBasicIntrinsic(cv::Mat& mIntrinsic)
{
	float fSensorSize = 17.3;
	float fFocalLength = 85.;
	float fFocal = (fFocalLength / fSensorSize) * m_nWidth;

	mIntrinsic = (Mat_<double>(3, 3) <<
		(fFocalLength / fSensorSize) * m_nWidth, 0, m_nWidth / 2,
		0, (fFocalLength / fSensorSize) * m_nWidth, m_nHeight / 2,
		0, 0, 1);

	return 0;
}

int CRecognition3D::getRandomRGBColor(int& nR, int& nG, int& nB)
{
	static bool nFlag = true;
	if (nFlag)
	{	
		srand((unsigned int)time(nullptr));
		nFlag = false;
	}

	nR = rand() % 255;
	nG = rand() % 255;
	nB = rand() % 255;

	return 0;
}

//int CRecognition3D::ProjectionTest()
//{
//	cv::Mat mDistCoeffs = Mat::zeros(4, 1, cv::DataType <float>::type);
//
//	cv::Point3d ptPoint;
//	cv::Mat mRotation;
//	cv::Mat mTranslation;
//	cv::Mat mIntrinsic;
//
//	
//	vector<Point3d> vecPtPoint;
//	m_pCameraInfo.GetImagePlane3DPoints(1, vecPtPoint);
//	//vecPtPoint.push_back(ptPoint);
//
//	m_pCameraInfo.GetRotationMatrixAs(1, mRotation);
//	m_pCameraInfo.GetTranslationMatrixAs(1, mTranslation);
//	m_pCameraInfo.GetIntrinsicMatrixAs(1, mIntrinsic);
//
//	vector<cv::Point2d> vecPtResult;
//	projectPoints(
//		vecPtPoint,
//		mRotation,
//		mTranslation,
//		mIntrinsic,
//		mDistCoeffs,
//		vecPtResult);
//
//	return 0;
//}