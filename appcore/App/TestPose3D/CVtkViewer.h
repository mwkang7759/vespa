#pragma once

//#include "OpenCV2/core.hpp"
//#include "OpenCV2/highgui.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/viz.hpp"

using namespace std;
using namespace cv;

class CVtkViewer
{
public:
	CVtkViewer();
	~CVtkViewer();

public:
	int VtkInit();

	int DrawBatterBoxCube();
	int DrawKZone();
	int DrawBatterBox();
	int DrawJoint(const vector<Point3d>& vecPtJoint);

	int InputPoint(
		const string& strCamera, 
		const Point3f& ptPoint, 
		int nBlue, int nGreen, int nRed);
	int ShowCloud(
		const string& strFrame, 
		const vector<Vec3d>& vecCloudPoint, 
		int nBlue, int nGreen, int nRed);
	int DrawLine(
		const string& strLine, 
		const Point3d& ptPoint1, const Point3d& ptPoint2);

	int ShowViewer();

	//Point3f EulerMatrixToAngle(Mat mEulerMatrix);

public:
	viz::Viz3d m_pWindow;
};

