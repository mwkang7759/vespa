#pragma once

#include <DeepBase.h>
#include "..\Object\include\sld_object_detector.h"
#include "..\Pose\include\sld_pose_estimator.h"

#include "CVtkViewer.h"
#include "CRecognition3DCameraInfo.h"

namespace pose3d
{
	const static int JOINT_COUNT = 18;

	class CPose3D
	{
	public:

	public:
		CPose3D(void);

	private:
		deep4d::object::detector* _detector;
		deep4d::object::detector::context_t _detector_ctx;
		deep4d::pose::estimator* _estimator;
		deep4d::pose::estimator::context_t _estimator_ctx;

	private:
		cv::Rect paddingROI(const cv::Rect& _roi, const int& _width, const int& _height);

		cv::Rect findROI(std::vector<DeepBase::BBox_t>& srcBBox, const std::vector<int>& classId, const std::vector<float>& prob,
			std::vector<cv::Rect>& _vecRoi, std::vector<DeepBase::BBoxData_t>& _vecBBoxData, const int& _width, const int& _height);

		DL_RESULT makePoseObjects(std::vector<std::array<int, 36>>& _vecJointPoint, cv::Rect& _roi,
			std::vector<cv::Rect>& _vecBBox, DeepBase::Detection_Data_t& _detectionData, std::vector<DeepBase::PoseData_t>& _vecPoseData);

		int GetSkewMinCenterPointsFromTwoRays(
			const Point3d& ptLineAPoint1, const Point3d& ptLineAPoint2,
			const Point3d& ptLineBPoint1, const Point3d& ptLineBPoint2,
			Point3d& ptSkewMinimumPoint);

		int Point3dToVec3d(const Point3d& ptPoint, Vec3d& vVec);

		Point3d AverageCloudPoint(const vector<Vec3d>& vecPointCloud);

		int getRandomRGBColor(int& nR, int& nG, int& nB);

		double RectangleIoU(int min_x1, int min_y1, int max_x1, int max_y1, int min_x2, int min_y2, int max_x2, int max_y2);

	public:
		DL_RESULT InitDLEngine(int nWidth, int nHeight, std::string strObject, std::string strPose);
		int EstimatePoses(cv::cuda::GpuMat& mGpuImg, std::vector<DeepBase::PoseData_t>& vecPoses);
		int ReconPose3D(CRecognition3DCameraInfo& camInfo, vector<Point3d>& vecPtJoint, bool bDraw);
	};
}

