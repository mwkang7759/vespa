#include "CRecognition3DCameraInfo.h"

CRecognition3DCameraInfo::CRecognition3DCameraInfo()
{

}

CRecognition3DCameraInfo::~CRecognition3DCameraInfo()
{

}

int CRecognition3DCameraInfo::GetCameraInfoIndex(int nChannel)
{
	for (int i = 0; i < m_vecCameraInfo.size(); i++)
	{
		if (m_vecCameraInfo[i].nChannel == nChannel)
			return i;
	}

	return RECOGNITION_ERROR_CAMERA_INFO_NOT_EXIST;
}

int CRecognition3DCameraInfo::AddCameraInfo(
	int nChannel,
	int nWidth,
	int nHeight,
	Mat mIntrinsicMatrix,
	Mat mRotationMatrix,
	Mat mTranslationMatrix)
{
	if (GetCameraInfoIndex(nChannel) != RECOGNITION_ERROR_CAMERA_INFO_NOT_EXIST)
		return RECOGNITION_ERROR_CAMERA_INFO_ALREADY_EXIST;

	StCameraInfo stCameraInfo;

	stCameraInfo.nWidth = nWidth;
	stCameraInfo.nHeight = nHeight;

	stCameraInfo.nChannel = nChannel;
	stCameraInfo.mIntrinsicMatrix = mIntrinsicMatrix;
	stCameraInfo.mRotationMatrix = mRotationMatrix;
	stCameraInfo.mTranslationMatrix = mTranslationMatrix;

	m_vecCameraInfo.push_back(stCameraInfo);

	translation_Extrinsic4x4_From_Rotation3x3_And_Translation3x1(nChannel);
	translation_CameraPose4x4_From_Rotation3x3_And_Translation3x1(nChannel);

	return RECOGNITION_ERROR_OK;
}

int CRecognition3DCameraInfo::GetCameraInfoCount()
{
	if (m_vecCameraInfo.size() < 0)
		return RECOGNITION_ERROR_CAMERA_INFO_EMPTY;

	return m_vecCameraInfo.size();
}

int CRecognition3DCameraInfo::GetIntrinsicMatrixAs(int nChannel, Mat& mIntrinsicMatrix)
{
	int nIndex = GetCameraInfoIndex(nChannel);
	if (m_vecCameraInfo.size() <= nIndex)
		return RECOGNITION_ERROR_CAMERA_INFO_NOT_EXIST;

	mIntrinsicMatrix = m_vecCameraInfo[nIndex].mIntrinsicMatrix;

	return RECOGNITION_ERROR_OK;
}

int CRecognition3DCameraInfo::GetRotationMatrixAs(int nChannel, Mat& mRotationMatrix)
{
	int nIndex = GetCameraInfoIndex(nChannel);
	if (m_vecCameraInfo.size() <= nIndex)
		return RECOGNITION_ERROR_CAMERA_INFO_NOT_EXIST;

	mRotationMatrix = m_vecCameraInfo[nIndex].mRotationMatrix;

	return RECOGNITION_ERROR_OK;
}

int CRecognition3DCameraInfo::GetTranslationMatrixAs(int nChannel, Mat& mTranslationMatrix)
{
	int nIndex = GetCameraInfoIndex(nChannel);
	if (m_vecCameraInfo.size() <= nIndex)
		return RECOGNITION_ERROR_CAMERA_INFO_NOT_EXIST;

	mTranslationMatrix = m_vecCameraInfo[nIndex].mTranslationMatrix;

	return RECOGNITION_ERROR_OK;
}

int CRecognition3DCameraInfo::GetImagePlane3DPointAs(int nIndex, int nJointIndex, Point3d& ptImagePlane3DPoint)
{
	if (m_vecCameraInfo.size() <= nIndex)
		return RECOGNITION_ERROR_CAMERA_INFO_NOT_EXIST;

	if (m_vecCameraInfo[nIndex].vecVecPtPoseAxis.size() == 0)
		return RECOGNITION_ERROR_CAMERA_INFO_NOT_EXIST;

	ptImagePlane3DPoint = m_vecCameraInfo[nIndex].vecVecPtPoseAxis[0][nJointIndex].ptImagePlane3DPoint;

	return RECOGNITION_ERROR_OK;
}

int CRecognition3DCameraInfo::GetCameraPos3DPointAs(int nIndex, int nJointIndex, Point3d& ptCameraPos3DPoint)
{
	if (m_vecCameraInfo.size() <= nIndex)
		return RECOGNITION_ERROR_CAMERA_INFO_NOT_EXIST;

	if (m_vecCameraInfo[nIndex].vecVecPtPoseAxis.size() == 0)
		return RECOGNITION_ERROR_CAMERA_INFO_NOT_EXIST;

	ptCameraPos3DPoint = m_vecCameraInfo[nIndex].vecVecPtPoseAxis[0][nJointIndex].ptCameraPos3DPoint;

	return RECOGNITION_ERROR_OK;
}

int CRecognition3DCameraInfo::GetImagePlane3DPoints(int nChannel, vector<Point3d>& ptImagePlane3DPoint)
{
	int nIndex = GetCameraInfoIndex(nChannel);
	if (m_vecCameraInfo.size() <= nIndex)
		return RECOGNITION_ERROR_CAMERA_INFO_NOT_EXIST;

	//ptImagePlane3DPoint = m_vecCameraInfo[nIndex].vecVecPtPoseAxis[0][nJointIndex].ptImagePlane3DPoint;

	for (int i = 0; i < m_vecCameraInfo[nIndex].vecVecPtPoseAxis[0].size(); i++)
	{
		ptImagePlane3DPoint.push_back(
		m_vecCameraInfo[nIndex].vecVecPtPoseAxis[0][i].ptImagePlane3DPoint);
	}

	return RECOGNITION_ERROR_OK;
}

int CRecognition3DCameraInfo::SetPoseAxis(int nChannel, int nTimeStamp, vector<Point2d> vecPtPoseAxis)
{
	int nIndex = GetCameraInfoIndex(nChannel);
	if (m_vecCameraInfo.size() <= nIndex)
		return RECOGNITION_ERROR_CAMERA_INFO_NOT_EXIST;

	vector<StPositionInfo> vecStPositionInfo;

	for (int i = 0; i < vecPtPoseAxis.size(); i++)
	{
		StPositionInfo stPositionInfo;

		stPositionInfo.ptImagePlane2DPoint = vecPtPoseAxis.at(i);
		
		calcCameraPointAndImagePlanePoint(
			nChannel,
			stPositionInfo.ptImagePlane2DPoint,
			stPositionInfo.ptImagePlane3DPoint,
			stPositionInfo.ptCameraPos3DPoint);

		vecStPositionInfo.push_back(stPositionInfo);		
	}
	m_vecCameraInfo[nIndex].vecVecPtPoseAxis.push_back(vecStPositionInfo);
}

int CRecognition3DCameraInfo::translation_Extrinsic4x4_From_Rotation3x3_And_Translation3x1(int nChannel)
{
	int nIndex = GetCameraInfoIndex(nChannel);
	if (m_vecCameraInfo.size() <= nIndex)
		return RECOGNITION_ERROR_CAMERA_INFO_NOT_EXIST;

	Mat mRotation = m_vecCameraInfo[nIndex].mRotationMatrix;
	Mat mTranslation= m_vecCameraInfo[nIndex].mTranslationMatrix;

	if (mRotation.cols == 3 && mRotation.rows == 3)
	{
		m_vecCameraInfo[nIndex].mExtrinsicMatrix = (Mat_<double>(4, 4) <<
			mRotation.at<double>(0, 0), mRotation.at<double>(0, 1), mRotation.at<double>(0, 2), mTranslation.at<double>(0),
			mRotation.at<double>(1, 0), mRotation.at<double>(1, 1), mRotation.at<double>(1, 2), mTranslation.at<double>(1),
			mRotation.at<double>(2, 0), mRotation.at<double>(2, 1), mRotation.at<double>(2, 2), mTranslation.at<double>(2),
			0, 0, 0, 1);
	}
	else if (mRotation.cols == 1 && mRotation.rows == 3)
	{
		Mat mRodrigues;
		Rodrigues(mRotation, mRodrigues);

		m_vecCameraInfo[nIndex].mExtrinsicMatrix = (Mat_<double>(4, 4) <<
			mRodrigues.at<double>(0, 0), mRodrigues.at<double>(0, 1), mRodrigues.at<double>(0, 2), mTranslation.at<double>(0),
			mRodrigues.at<double>(1, 0), mRodrigues.at<double>(1, 1), mRodrigues.at<double>(1, 2), mTranslation.at<double>(1),
			mRodrigues.at<double>(2, 0), mRodrigues.at<double>(2, 1), mRodrigues.at<double>(2, 2), mTranslation.at<double>(2),
			0, 0, 0, 1);
	}

	return RECOGNITION_ERROR_OK;
}

int CRecognition3DCameraInfo::translation_CameraPose4x4_From_Rotation3x3_And_Translation3x1(int nChannel)
{
	Mat mExtrinsic;

	int nIndex = GetCameraInfoIndex(nChannel);
	if (m_vecCameraInfo.size() <= nIndex)
		return RECOGNITION_ERROR_CAMERA_INFO_NOT_EXIST;

	Mat mRotation = m_vecCameraInfo[nIndex].mRotationMatrix;
	Mat mTranslation = m_vecCameraInfo[nIndex].mTranslationMatrix;

	if (mRotation.cols == 3 && mRotation.rows == 3)
	{
		mRotation = mRotation.t();
		mTranslation = -mRotation * mTranslation;

		m_vecCameraInfo[nIndex].mCameraPoseMatrix = (Mat_<double>(4, 4) <<
			mRotation.at<double>(0, 0), mRotation.at<double>(0, 1), mRotation.at<double>(0, 2), mTranslation.at<double>(0),
			mRotation.at<double>(1, 0), mRotation.at<double>(1, 1), mRotation.at<double>(1, 2), mTranslation.at<double>(1),
			mRotation.at<double>(2, 0), mRotation.at<double>(2, 1), mRotation.at<double>(2, 2), mTranslation.at<double>(2),
			0, 0, 0, 1);
	}
	else if (mRotation.cols == 1 && mRotation.rows == 3)
	{
		Mat mRodrigues;
		Rodrigues(mRotation, mRodrigues);
		mRodrigues = mRodrigues.t();
		mTranslation = -mRodrigues * mTranslation;

		m_vecCameraInfo[nIndex].mCameraPoseMatrix = (Mat_<double>(4, 4) <<
			mRodrigues.at<double>(0, 0), mRodrigues.at<double>(0, 1), mRodrigues.at<double>(0, 2), mTranslation.at<double>(0),
			mRodrigues.at<double>(1, 0), mRodrigues.at<double>(1, 1), mRodrigues.at<double>(1, 2), mTranslation.at<double>(1),
			mRodrigues.at<double>(2, 0), mRodrigues.at<double>(2, 1), mRodrigues.at<double>(2, 2), mTranslation.at<double>(2),
			0, 0, 0, 1);
	}

	return RECOGNITION_ERROR_OK;
}

int CRecognition3DCameraInfo::calcCameraPointAndImagePlanePoint(
	int nChannel,
	Point2d ptImagePlane2DPoint,
	Point3d& ptImagePlane3DPoint,
	Point3d& ptCameraPos3DPoint)
{
	int nIndex = GetCameraInfoIndex(nChannel);
	if (m_vecCameraInfo.size() <= nIndex)
		return RECOGNITION_ERROR_CAMERA_INFO_NOT_EXIST;

	int nWidth = m_vecCameraInfo[nIndex].nWidth;
	int nHeight = m_vecCameraInfo[nIndex].nHeight;
	Mat mIntrinsic = m_vecCameraInfo[nIndex].mIntrinsicMatrix;
	Mat mCameraPoseMatrix = m_vecCameraInfo[nIndex].mCameraPoseMatrix;

	Point3d ptInitIntrinsic3DPoint = Point3d(
		(ptImagePlane2DPoint.x / nWidth) * mIntrinsic.at<double>(0, 2) * 2 -mIntrinsic.at<double>(0, 2),
		(ptImagePlane2DPoint.y / nHeight) * mIntrinsic.at<double>(1, 2) * 2 - mIntrinsic.at<double>(1, 2),
		mIntrinsic.at<double>(0, 0));

	Mat mInitIntrinsic3DPoint = (Mat_<double>(4, 1) <<
		ptInitIntrinsic3DPoint.x,
		ptInitIntrinsic3DPoint.y,
		ptInitIntrinsic3DPoint.z,
		1);
	Mat mImagePlane3DPoint = mCameraPoseMatrix * mInitIntrinsic3DPoint;

	ptImagePlane3DPoint = Point3d(
		mImagePlane3DPoint.at<double>(0),
		mImagePlane3DPoint.at<double>(1),
		mImagePlane3DPoint.at<double>(2));

	Mat mViewPoint = (Mat_<double>(4, 1) << 0, 0, 0, 1);
	mViewPoint = mCameraPoseMatrix * mViewPoint;

	ptCameraPos3DPoint = Point3d(
		mViewPoint.at<double>(0),
		mViewPoint.at<double>(1),
		mViewPoint.at<double>(2));

	return RECOGNITION_ERROR_OK;
}