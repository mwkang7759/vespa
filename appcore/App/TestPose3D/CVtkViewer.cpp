#include "CVtkViewer.h"

CVtkViewer::CVtkViewer()
{

}
CVtkViewer::~CVtkViewer()
{

}

int CVtkViewer::VtkInit()
{
	m_pWindow.setWindowSize(cv::Size(960, 540));
	m_pWindow.setWindowPosition(cv::Point(0, 0));
	m_pWindow.setBackgroundColor(); // black by default

	//Vec3f cam_pos(50.0f, 50.0f, -50.0f);
	//Vec3f cam_focal_point(50.0f, 50.0f, 50.0f);
	//Vec3f cam_y_dir(50.0f, 50.0f, -50.0f);
	//Affine3f cam_pose = viz::makeCameraPose(cam_pos, cam_focal_point, cam_y_dir);
	//Affine3f cam_pose = viz::makeTransformToGlobal(cam_pos, cam_focal_point, cam_y_dir,Vec3f(50, 50, -50));
	//m_pWindow.setViewerPose(cam_pose);
	
	return 1;
}

int CVtkViewer::DrawBatterBoxCube()
{
	viz::WCube cube_widget(
		Point3f(0.0, 0.0, 0.0),
		Point3f(100.0, 100.0, -100.0),
		true,
		viz::Color::Color(128, 128, 0));
	cube_widget.setRenderingProperty(viz::LINE_WIDTH, 2.0);
	m_pWindow.showWidget("Cube Widget", cube_widget);

	return 1;
}

int CVtkViewer::DrawKZone()
{
	//K-Zone
	vector<Point3f> vecKZonePoints;

	vecKZonePoints.push_back(Point3f(44, 43, -13));
	vecKZonePoints.push_back(Point3f(56, 43, -13));
	vecKZonePoints.push_back(Point3f(56, 50, -13));
	vecKZonePoints.push_back(Point3f(50, 57, -13));
	vecKZonePoints.push_back(Point3f(44, 50, -13));
	vecKZonePoints.push_back(Point3f(44, 43, -33.5));
	vecKZonePoints.push_back(Point3f(56, 43, -33.5));
	vecKZonePoints.push_back(Point3f(56, 50, -33.5));
	vecKZonePoints.push_back(Point3f(50, 57, -33.5));
	vecKZonePoints.push_back(Point3f(44, 50, -33.5));

	for (int i = 0; i < 10; i++)
	{
		viz::WSphere wSphere(vecKZonePoints[i], 0.5, 20, viz::Color(192, 192, 192));
		string strSphere = "wSphere" + to_string(i);
		m_pWindow.showWidget(strSphere, wSphere);
	}

	for (int i = 0; i < 5; i++)
	{
		viz::WLine wLine(vecKZonePoints[i % 5], vecKZonePoints[(i + 1) % 5], viz::Color(192, 192, 192));
		string strLine = "wLine" + to_string(i);
		m_pWindow.showWidget(strLine, wLine);

		wLine = viz::WLine(vecKZonePoints[i % 5 + 5], vecKZonePoints[(i + 1) % 5 + 5], viz::Color(192, 192, 192));
		strLine = "wLine" + to_string(i + 5);
		m_pWindow.showWidget(strLine, wLine);

		wLine = viz::WLine(vecKZonePoints[i % 5], vecKZonePoints[i % 5 + 5], viz::Color(192, 192, 192));
		strLine = "wLine" + to_string(i + 10);
		m_pWindow.showWidget(strLine, wLine);
	}
	return 1;
}

int CVtkViewer::DrawBatterBox()
{
	viz::WLine wLine = viz::WLine(Point3f(0, 20.7, 0), Point3f(35, 20.7, 0), viz::Color(255, 255, 255));
	wLine.setRenderingProperty(viz::LINE_WIDTH, 9.0);
	string strLine = "wBatterLine1";// +to_string(i + 10);
	m_pWindow.showWidget(strLine, wLine);

	wLine = viz::WLine(Point3f(65, 20.7, 0), Point3f(100, 20.7, 0), viz::Color(255, 255, 255));
	wLine.setRenderingProperty(viz::LINE_WIDTH, 9.0);
	strLine = "wBatterLine2";// +to_string(i + 10);
	m_pWindow.showWidget(strLine, wLine);

	wLine = viz::WLine(Point3f(100, 20.7, 0), Point3f(100, 80.08, 0), viz::Color(255, 255, 255));
	wLine.setRenderingProperty(viz::LINE_WIDTH, 9.0);
	strLine = "wBatterLine3";// +to_string(i + 10);
	m_pWindow.showWidget(strLine, wLine);

	wLine = viz::WLine(Point3f(100, 80.08, 0), Point3f(65, 80.08, 0), viz::Color(255, 255, 255));
	wLine.setRenderingProperty(viz::LINE_WIDTH, 9.0);
	strLine = "wBatterLine4";// +to_string(i + 10);
	m_pWindow.showWidget(strLine, wLine);

	wLine = viz::WLine(Point3f(35, 80.08, 0), Point3f(0, 80.08, 0), viz::Color(255, 255, 255));
	wLine.setRenderingProperty(viz::LINE_WIDTH, 9.0);
	strLine = "wBatterLine5";// +to_string(i + 10);
	m_pWindow.showWidget(strLine, wLine);

	wLine = viz::WLine(Point3f(0, 80.08, 0), Point3f(0, 20.7, 0), viz::Color(255, 255, 255));
	wLine.setRenderingProperty(viz::LINE_WIDTH, 9.0);
	strLine = "wBatterLine6";// +to_string(i + 10);
	m_pWindow.showWidget(strLine, wLine);

	wLine = viz::WLine(Point3f(65, 20.7, 0), Point3f(65, 80.08, 0), viz::Color(255, 255, 255));
	wLine.setRenderingProperty(viz::LINE_WIDTH, 9.0);
	strLine = "wBatterLine7";// +to_string(i + 10);
	m_pWindow.showWidget(strLine, wLine);

	wLine = viz::WLine(Point3f(35, 20.7, 0), Point3f(35, 80.08, 0), viz::Color(255, 255, 255));
	wLine.setRenderingProperty(viz::LINE_WIDTH, 9.0);
	strLine = "wBatterLine8";// +to_string(i + 10);
	m_pWindow.showWidget(strLine, wLine);

	return 1;
}

int CVtkViewer::DrawJoint(const vector<Point3d>& vecPtJoint)
{
//	viz::WLine wLine = viz::WLine(vecPtJoint[0], vecPtJoint[1], viz::Color(255, 255, 255));
//	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
//	string strLine = "wHeadToNeck";// +to_string(i + 10);
//	m_pWindow.showWidget(strLine, wLine);
//
//	wLine = viz::WLine(vecPtJoint[1], vecPtJoint[2], viz::Color(255, 255, 255));
//	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
//	strLine = "wNeckToLeftSholder";// +to_string(i + 10);
//	m_pWindow.showWidget(strLine, wLine);
//
//	wLine = viz::WLine(vecPtJoint[1], vecPtJoint[3], viz::Color(255, 255, 255));
//	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
//	strLine = "wNeckToRightSholder";// +to_string(i + 10);
//	m_pWindow.showWidget(strLine, wLine);
//
//	wLine = viz::WLine(vecPtJoint[1], vecPtJoint[8], viz::Color(255, 255, 255));
//	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
//	strLine = "wNeckToWaist";// +to_string(i + 10);
//	m_pWindow.showWidget(strLine, wLine);
//
//	wLine = viz::WLine(vecPtJoint[2], vecPtJoint[4], viz::Color(255, 255, 255));
//	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
//	strLine = "wLeftSholderToLeftElbow";// +to_string(i + 10);
//	m_pWindow.showWidget(strLine, wLine);
//
//	wLine = viz::WLine(vecPtJoint[3], vecPtJoint[5], viz::Color(255, 255, 255));
//	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
//	strLine = "wRightSholderToRightElbow";// +to_string(i + 10);
//	m_pWindow.showWidget(strLine, wLine);
//
//	wLine = viz::WLine(vecPtJoint[4], vecPtJoint[6], viz::Color(255, 255, 255));
//	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
//	strLine = "wLeftElbowToLeftWrisk";// +to_string(i + 10);
//	m_pWindow.showWidget(strLine, wLine);
//
//	wLine = viz::WLine(vecPtJoint[5], vecPtJoint[7], viz::Color(255, 255, 255));
//	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
//	strLine = "wRightElbowToRightWrisk";// +to_string(i + 10);
//	m_pWindow.showWidget(strLine, wLine);
//
//	wLine = viz::WLine(vecPtJoint[8], vecPtJoint[9], viz::Color(255, 255, 255));
//	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
//	strLine = "wWaistToHip";// +to_string(i + 10);
//	m_pWindow.showWidget(strLine, wLine);
//
//	wLine = viz::WLine(vecPtJoint[9], vecPtJoint[10], viz::Color(255, 255, 255));
//	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
//	strLine = "wHipToLeftHip";// +to_string(i + 10);
//	m_pWindow.showWidget(strLine, wLine);
//
//	wLine = viz::WLine(vecPtJoint[9], vecPtJoint[11], viz::Color(255, 255, 255));
//	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
//	strLine = "wHipToRightHip";// +to_string(i + 10);
//	m_pWindow.showWidget(strLine, wLine);
//
//	wLine = viz::WLine(vecPtJoint[10], vecPtJoint[12], viz::Color(255, 255, 255));
//	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
//	strLine = "wLeftHipToLeftKnee";// +to_string(i + 10);
//	m_pWindow.showWidget(strLine, wLine);
//
//	wLine = viz::WLine(vecPtJoint[11], vecPtJoint[13], viz::Color(255, 255, 255));
//	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
//	strLine = "wRightHipToRightKnee";// +to_string(i + 10);
//	m_pWindow.showWidget(strLine, wLine);
//
//	wLine = viz::WLine(vecPtJoint[12], vecPtJoint[14], viz::Color(255, 255, 255));
//	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
//	strLine = "wLeftKneeToLeftAnkle";// +to_string(i + 10);
//	m_pWindow.showWidget(strLine, wLine);
//
//	wLine = viz::WLine(vecPtJoint[13], vecPtJoint[15], viz::Color(255, 255, 255));
//	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
//	strLine = "wRightKneeToRightAnkle";// +to_string(i + 10);
//	m_pWindow.showWidget(strLine, wLine);

	viz::WLine wLine = viz::WLine(vecPtJoint[15], vecPtJoint[13], viz::Color(255, 255, 255));
	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
	string strLine = "wLeftAnkleToLeftKnee";// +to_string(i + 10);
	m_pWindow.showWidget(strLine, wLine);

	wLine = viz::WLine(vecPtJoint[13], vecPtJoint[11], viz::Color(255, 255, 255));
	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
	strLine = "wLeftKneeToLeftHip";// +to_string(i + 10);
	m_pWindow.showWidget(strLine, wLine);

	wLine = viz::WLine(vecPtJoint[16], vecPtJoint[14], viz::Color(255, 255, 255));
	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
	strLine = "wRightAnkleToRightKnee";// +to_string(i + 10);
	m_pWindow.showWidget(strLine, wLine);

	wLine = viz::WLine(vecPtJoint[14], vecPtJoint[12], viz::Color(255, 255, 255));
	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
	strLine = "wRightKneeToRightHip";// +to_string(i + 10);
	m_pWindow.showWidget(strLine, wLine);

	wLine = viz::WLine(vecPtJoint[11], vecPtJoint[12], viz::Color(255, 255, 255));
	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
	strLine = "wLeftHipToRightHip";// +to_string(i + 10);
	m_pWindow.showWidget(strLine, wLine);

	wLine = viz::WLine(vecPtJoint[5], vecPtJoint[11], viz::Color(255, 255, 255));
	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
	strLine = "wLeftSholderToLeftHip";// +to_string(i + 10);
	m_pWindow.showWidget(strLine, wLine);

	wLine = viz::WLine(vecPtJoint[6], vecPtJoint[12], viz::Color(255, 255, 255));
	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
	strLine = "wRightSholderToRightHip";// +to_string(i + 10);
	m_pWindow.showWidget(strLine, wLine);

	wLine = viz::WLine(vecPtJoint[5], vecPtJoint[6], viz::Color(255, 255, 255));
	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
	strLine = "wLeftSholderToRightSholder";// +to_string(i + 10);
	m_pWindow.showWidget(strLine, wLine);

	wLine = viz::WLine(vecPtJoint[5], vecPtJoint[7], viz::Color(255, 255, 255));
	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
	strLine = "wLeftSholderToLeftElbow";// +to_string(i + 10);
	m_pWindow.showWidget(strLine, wLine);

	wLine = viz::WLine(vecPtJoint[6], vecPtJoint[8], viz::Color(255, 255, 255));
	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
	strLine = "wRightSholderToRightElbow";// +to_string(i + 10);
	m_pWindow.showWidget(strLine, wLine);

	wLine = viz::WLine(vecPtJoint[7], vecPtJoint[9], viz::Color(255, 255, 255));
	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
	strLine = "wLeftElbowToLeftWrist";// +to_string(i + 10);
	m_pWindow.showWidget(strLine, wLine);

	wLine = viz::WLine(vecPtJoint[8], vecPtJoint[10], viz::Color(255, 255, 255));
	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
	strLine = "wRightElbowToRightWrist";// +to_string(i + 10);
	m_pWindow.showWidget(strLine, wLine);

	wLine = viz::WLine(vecPtJoint[1], vecPtJoint[2], viz::Color(255, 255, 255));
	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
	strLine = "wLeftEyeToRightEye";// +to_string(i + 10);
	m_pWindow.showWidget(strLine, wLine);

	wLine = viz::WLine(vecPtJoint[0], vecPtJoint[1], viz::Color(255, 255, 255));
	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
	strLine = "wNoseToLeftEye";// +to_string(i + 10);
	m_pWindow.showWidget(strLine, wLine);

	wLine = viz::WLine(vecPtJoint[0], vecPtJoint[2], viz::Color(255, 255, 255));
	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
	strLine = "wNoseToRightEye";// +to_string(i + 10);
	m_pWindow.showWidget(strLine, wLine);

	wLine = viz::WLine(vecPtJoint[1], vecPtJoint[3], viz::Color(255, 255, 255));
	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
	strLine = "wLeftEyeToLeftEar";// +to_string(i + 10);
	m_pWindow.showWidget(strLine, wLine);

	wLine = viz::WLine(vecPtJoint[2], vecPtJoint[4], viz::Color(255, 255, 255));
	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
	strLine = "wRightEyeToRightEar";// +to_string(i + 10);
	m_pWindow.showWidget(strLine, wLine);

	wLine = viz::WLine(vecPtJoint[3], vecPtJoint[5], viz::Color(255, 255, 255));
	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
	strLine = "wLeftEarToLeftSholder";// +to_string(i + 10);
	m_pWindow.showWidget(strLine, wLine);

	wLine = viz::WLine(vecPtJoint[4], vecPtJoint[6], viz::Color(255, 255, 255));
	wLine.setRenderingProperty(viz::LINE_WIDTH, 2.0);
	strLine = "wRightEarToRightSholder";// +to_string(i + 10);
	m_pWindow.showWidget(strLine, wLine);

	return 1;
}

int CVtkViewer::InputPoint(
	const string& strCamera,
	const Point3f& ptPoint,
	int nBlue, int nGreen, int nRed)
{
	static int nStaticNum = 0;
	viz::WSphere wSphere(ptPoint, 1.5, 20, viz::Color(nBlue, nGreen, nRed));
	
	m_pWindow.showWidget(strCamera, wSphere);

	return 1;
}

int CVtkViewer::DrawLine(
	const string& strLine,
	const Point3d& ptPoint1, const Point3d& ptPoint2)
{
	viz::WLine wLine = viz::WLine(ptPoint1, ptPoint2, viz::Color(255, 0, 255));

	wLine.setRenderingProperty(viz::LINE_WIDTH, 1.0);
	m_pWindow.showWidget(strLine, wLine);
	//m_pWindow.setWidgetPose("Line", afCamPose);

	return 1;
}

int CVtkViewer::ShowCloud(
	const string& strFrame,
	const vector<Vec3d>& vecCloudPoint,
	int nBlue, int nGreen, int nRed)
{
	viz::WCloud cloudWidget = viz::WCloud(vecCloudPoint, viz::Color(nBlue, nGreen, nRed));
	m_pWindow.showWidget(strFrame, cloudWidget);

	return 1;
}

int CVtkViewer::ShowViewer()
{
	while(!m_pWindow.wasStopped())
		m_pWindow.spinOnce(1, true);	

	return 1;
}

//Point3f CVtkViewer::EulerMatrixToAngle(Mat mEulerMatrix)
//{
//	double sy = sqrt(
//		mEulerMatrix.at<double>(0, 0) * mEulerMatrix.at<double>(0, 0) +
//		mEulerMatrix.at<double>(1, 0) * mEulerMatrix.at<double>(1, 0));
//
//	bool singular = sy < 1e-6; // If
//
//	double x, y, z;
//	if (!singular)
//	{
//		x = atan2(mEulerMatrix.at<double>(2, 1), mEulerMatrix.at<double>(2, 2));
//		y = atan2(-mEulerMatrix.at<double>(2, 0), sy);
//		z = atan2(mEulerMatrix.at<double>(1, 0), mEulerMatrix.at<double>(0, 0));
//	}
//	else
//	{
//		x = atan2(-mEulerMatrix.at<double>(1, 2), mEulerMatrix.at<double>(1, 1));
//		y = atan2(-mEulerMatrix.at<double>(2, 0), sy);
//		z = 0;
//	}
//
//	x = -90. + x * (180.0 / CV_PI);
//	y = y * (180.0 / CV_PI);
//	z = z * (180.0 / CV_PI);
//
//	return Point3f(x, y, z);
//}