#include "CPose3D.h"
#include <iostream>
#include <string>
#include <thread>
#include <mutex>

#include <cuda_runtime.h>

#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/viz.hpp>

#pragma comment(lib, "NVObjectDetector.lib")
#pragma comment(lib, "NVPoseEstimator.lib")

#if defined(_DEBUG)
#pragma comment(lib, "opencv_core440d.lib")
#pragma comment(lib, "opencv_highgui440d.lib")
#pragma comment(lib, "opencv_imgproc440d.lib")
#pragma comment(lib, "opencv_imgcodecs440d.lib")
#pragma comment(lib, "opencv_calib3d440d.lib")
#pragma comment(lib, "opencv_cudaimgproc440d.lib")
#pragma comment(lib, "opencv_cudawarping440d.lib")
#pragma comment(lib, "opencv_cudaarithm440d.lib")
#pragma comment(lib, "opencv_videoio440d.lib")
#pragma comment(lib, "opencv_calib3d440d.lib")
#pragma comment(lib, "opencv_viz440d.lib")
#else
#pragma comment(lib, "opencv_core440.lib")
#pragma comment(lib, "opencv_highgui440.lib")
#pragma comment(lib, "opencv_imgproc440.lib")
#pragma comment(lib, "opencv_imgcodecs440.lib")
#pragma comment(lib, "opencv_calib3d440.lib")
#pragma comment(lib, "opencv_cudaimgproc440.lib")
#pragma comment(lib, "opencv_cudawarping440.lib")
#pragma comment(lib, "opencv_cudaarithm440.lib")
#pragma comment(lib, "opencv_videoio440.lib")
#pragma comment(lib, "opencv_calib3d440.lib")
#pragma comment(lib, "opencv_viz440.lib")
#endif

using namespace pose3d;

CPose3D::CPose3D(void)
	: _detector(nullptr), _estimator(nullptr)
{
}

cv::Rect CPose3D::paddingROI(const cv::Rect& _roi, const int& _width, const int& _height)
{
	// Width and Height Rate
	int roiX1 = _roi.x, roiY1 = _roi.y, roiX2 = _roi.width + _roi.x, roiY2 = _roi.height + _roi.y;
	std::cout << "x1, y1, x2, y2 = " << roiX1 << "," << roiY1 << "," << roiX2 << "," << roiY2 << std::endl;

	int cmpLength = (roiX2 - roiX1) - (roiY2 - roiY1);
	if (cmpLength > 0) // 가로가 클경우
	{
		if (cmpLength / 2 <= (_height - roiY2) && cmpLength / 2 <= roiY1) {
			roiY1 -= cmpLength / 2;
			roiY2 += cmpLength / 2;
		}
		else if (cmpLength <= roiY1) {
			roiY1 -= cmpLength;
		}
		else if (cmpLength <= (_height - roiY2)) {
			roiY2 += cmpLength;
		}
	}
	else if (cmpLength < 0) // 세로가 클경우
	{
		if (abs(cmpLength) / 2 <= (_width - roiX2) && abs(cmpLength) / 2 <= roiX1) {
			roiX1 -= abs(cmpLength) / 2;
			roiX2 += abs(cmpLength) / 2;
		}
		else if (abs(cmpLength) <= roiX1) {
			roiX1 -= abs(cmpLength);
		}
		else if (abs(cmpLength) <= (_width - roiX2)) {
			roiX2 += abs(cmpLength);
		}
	}

	return cv::Rect(roiX1, roiY1, roiX2 - roiX1, roiY2 - roiY1);
}

cv::Rect CPose3D::findROI(std::vector<DeepBase::BBox_t>& srcBBox, const std::vector<int>& classId, const std::vector<float>& prob,
	std::vector<cv::Rect>& _vecRoi, std::vector<DeepBase::BBoxData_t>& _vecBBoxData, const int& _width, const int& _height)
{
	int roiX1 = _width, roiY1 = _height, roiX2 = 0, roiY2 = 0, roiWidth = 0, roiHeight = 0;
	int cnt = 0;
	for (auto& bbox : srcBBox)
	{
		// class가 홍 또는 청 선수일 경우만
		if ((classId[cnt] == 0 || classId[cnt] == 1) && prob[cnt] > 0.7)
		{
			// 좌표의 값이 -가 되는 경우
			if (bbox.x < 0) bbox.x = 0;
			if (bbox.y < 0) bbox.y = 0;

			// image size 넘치는경우
			bbox.w = (bbox.w <= _width) ? bbox.w : _width;
			bbox.h = (bbox.h <= _height) ? bbox.h : _height;
			_vecRoi.push_back({ static_cast<int>(bbox.x)
				, static_cast<int>(bbox.y)
				, static_cast<int>(bbox.w)
				, static_cast<int>(bbox.h) });

			DeepBase::BBoxData_t bboxData;
			bboxData.x = bbox.x; bboxData.y = bbox.y; bboxData.w = bbox.w; bboxData.h = bbox.h;
			bboxData.object_id = classId[cnt];
			bboxData.prob = prob[cnt];
			_vecBBoxData.push_back(bboxData);

			// Compare
			if (bbox.x < roiX1) roiX1 = bbox.x;
			if (bbox.x + bbox.w > roiX2) roiX2 = bbox.x + bbox.w;
			if (bbox.y < roiY1) roiY1 = bbox.y;
			if (bbox.y + bbox.h > roiY2) roiY2 = bbox.y + bbox.h;
		}
		++cnt;
	}
	if (_vecBBoxData.size() <= 0) {
		roiX1 = 0; roiY1 = 0; roiX2 = _width; roiY2 = _height;
	}
	else
	{
		if (roiY2 > _height) roiY2 = _height;
		if (roiX2 > _width) roiX2 = _width;

		// Width and Height Rate
#ifdef __legacy
		int cmpLength = (roiX2 - roiX1) - (roiY2 - roiY1);
		if (cmpLength > 0) // 가로가 클경우
		{
			if (cmpLength / 2 <= (_height - roiY2) && cmpLength / 2 <= roiY1) {
				roiY1 -= cmpLength / 2;
				roiY2 += cmpLength / 2;
			}
			else if (cmpLength <= roiY1) {
				roiY1 -= cmpLength;
			}
			else if (cmpLength <= (_height - roiY2)) {
				roiY2 += cmpLength;
			}
		}
		else if (cmpLength < 0) // 세로가 클경우
		{
			if (abs(cmpLength) / 2 <= (_width - roiY2) && abs(cmpLength) / 2 <= roiY1) {
				roiX1 -= abs(cmpLength) / 2;
				roiX2 += abs(cmpLength) / 2;
			}
			else if (abs(cmpLength) <= roiX1) {
				roiX1 -= abs(cmpLength);
			}
			else if (abs(cmpLength) <= (_width - roiY2)) {
				roiX2 += abs(cmpLength);
			}
		}
#else
		cv::Rect tmpRect = paddingROI(cv::Rect(roiX1, roiY1, roiX2 - roiX1, roiY2 - roiY1), _width, _height);
		roiX1 = tmpRect.x; roiY1 = tmpRect.y; roiX2 = tmpRect.width + tmpRect.x; roiY2 = tmpRect.height + tmpRect.y;

		if (roiX2 >= _width) roiX2 = _width;
		if (roiY2 >= _height) roiY2 = _height;
		if (roiX1 < 0) roiX1 = 0;
		if (roiY1 < 0) roiY1 = 0;
#endif
	}

	return cv::Rect(roiX1, roiY1, roiX2 - roiX1, roiY2 - roiY1);
}

DL_RESULT CPose3D::makePoseObjects(std::vector<std::array<int, 36>>& _vecJointPoint, cv::Rect& _roi,
	std::vector<cv::Rect>& _vecBBox, DeepBase::Detection_Data_t& _detectionData, std::vector<DeepBase::PoseData_t>& _vecPoseData)
{
	for (auto& skeleton : _vecJointPoint)
	{
		int minX = _roi.x + _roi.width, maxX = 0, minY = _roi.y + _roi.height, maxY = 0;
		DeepBase::TRTJointPoint_t arrJoint_data[JOINT_COUNT];
		for (int i = 0; i < skeleton.size(); ++i)
		{
			if (skeleton[i] == 0)
				continue;
			if (i % 2 == 0) // x axis
			{
				if (skeleton[i] < minX) minX = skeleton[i];
				if (skeleton[i] > maxX) maxX = skeleton[i];
				arrJoint_data[i / 2].px = skeleton[i];
			}
			else // y axis
			{
				if (skeleton[i] < minY) minY = skeleton[i];
				if (skeleton[i] > maxY) maxY = skeleton[i];
				arrJoint_data[i / 2].py = skeleton[i];
			}
		}
		//// BBox와 Skeleton min, max좌표값 비교
		//int bboxCnt = 0;
		//for (auto& _bbox : _vecBBox)
		//{
		//	//TODO: if hug 인 경우 pass
		//	//TODO: Pose Data 테스트용...
		//	if (_bbox.x <= minX && _bbox.x + _bbox.width >= maxX &&
		//		_bbox.y <= minY && _bbox.y + _bbox.height >= maxY)
		//	{
		//		DeepBase::PoseData_t poseData_t;
		//		DeepBase::BBoxData_t bbox_t;
		//		bbox_t.x = _bbox.x;
		//		bbox_t.y = _bbox.y;
		//		bbox_t.w = _bbox.width;
		//		bbox_t.h = _bbox.height;
		//		bbox_t.object_id = _detectionData.classId[bboxCnt];
		//		bbox_t.prob = _detectionData.prob[bboxCnt];
		//		poseData_t.detection_data = bbox_t;

		//		memmove(poseData_t.arrJoint_data, arrJoint_data, sizeof(arrJoint_data));
		//		_vecPoseData.push_back(poseData_t);
		//		break;
		//	}
		//	++bboxCnt;
		//}
		
		// Skeleton/bbox간 IoU 방식으로 비교하여 ID 통합
		double maxIoU = 0.0;
		int maxIdx = 0;

		for (int i = 0; i < _vecBBox.size(); i++)
		{
			double iou = RectangleIoU(minX, minY, maxX, maxY, _vecBBox[i].x, _vecBBox[i].y, _vecBBox[i].x + _vecBBox[i].width, _vecBBox[i].y + _vecBBox[i].height);
			if (maxIoU < iou)
			{
				maxIoU = iou;
				maxIdx = i;
			}
		}

		// IoU가 0.6 이상, Detection 결과가 0.8 이상 신뢰할 수 있는 경우 추가
		if (maxIoU > 0.6 && _detectionData.prob[maxIdx] > 0.8)
		{
			DeepBase::PoseData_t poseData_t;
			DeepBase::BBoxData_t bbox_t;
			bbox_t.x = _vecBBox[maxIdx].x;
			bbox_t.y = _vecBBox[maxIdx].y;
			bbox_t.w = _vecBBox[maxIdx].width;
			bbox_t.h = _vecBBox[maxIdx].height;
			bbox_t.object_id = _detectionData.classId[maxIdx];
			bbox_t.prob = _detectionData.prob[maxIdx];
			poseData_t.detection_data = bbox_t;

			memmove(poseData_t.arrJoint_data, arrJoint_data, sizeof(arrJoint_data));
			_vecPoseData.push_back(poseData_t);
			break;
		}
	}
	return DL_RESULT::SUCCESS;
}


int CPose3D::GetSkewMinCenterPointsFromTwoRays(
	const Point3d& ptLineAPoint1, const Point3d& ptLineAPoint2,
	const Point3d& ptLineBPoint1, const Point3d& ptLineBPoint2,
	Point3d& ptSkewMinimumPoint)
{
	Point3d ptVectorA = ptLineAPoint2 - ptLineAPoint1;
	Point3d ptVectorB = ptLineBPoint2 - ptLineBPoint1;
	Point3d ptVectorW = ptLineBPoint1 - ptLineAPoint1;

	double dNormVectorA = norm(ptVectorA);
	double dNormVectorB = norm(ptVectorB);

	Point3d ptUnitVectorA = ptVectorA / dNormVectorA;
	Point3d ptUnitVectorB = ptVectorB / dNormVectorB;

	double dA = ptUnitVectorA.dot(ptUnitVectorA);
	double dB = ptUnitVectorA.dot(ptUnitVectorB);
	double dC = ptUnitVectorB.dot(ptUnitVectorB);
	double dD = ptUnitVectorA.dot(ptVectorW);
	double dE = ptUnitVectorB.dot(ptVectorW);

	double dSkewMinPointParamA = ((dB * dE) - (dC * dD)) / ((dA * dC) - (dB * dB));
	double dSkewMinPointParamB = ((dA * dE) - (dB * dD)) / ((dA * dC) - (dB * dB));

	Point3d ptSkewAMinimumPoint = ptLineAPoint1 - dSkewMinPointParamA * ptUnitVectorA;
	Point3d ptSkewBMinimumPoint = ptLineBPoint1 - dSkewMinPointParamB * ptUnitVectorB;

	ptSkewMinimumPoint = (ptSkewAMinimumPoint + ptSkewBMinimumPoint) / 2.;

	return 1;
}

int CPose3D::Point3dToVec3d(const Point3d& ptPoint, Vec3d& vVec)
{
	vVec = Vec3d(ptPoint.x, ptPoint.y, ptPoint.z);

	return 1;
}

Point3d CPose3D::AverageCloudPoint(const vector<Vec3d>& vecPointCloud)
{
	if (vecPointCloud.size() <= 0)
		return Point3d();

	double dSumX = 0;
	double dSumY = 0;
	double dSumZ = 0;

	for (int i = 0; i < vecPointCloud.size(); i++)
	{
		dSumX += vecPointCloud.at(i)[0];
		dSumY += vecPointCloud.at(i)[1];
		dSumZ += vecPointCloud.at(i)[2];
	}

	Point3d ptResult = Point3d(
		dSumX / (vecPointCloud.size()),
		dSumY / (vecPointCloud.size()),
		dSumZ / (vecPointCloud.size()));

	return ptResult;
}

int CPose3D::getRandomRGBColor(int& nR, int& nG, int& nB)
{
	static bool nFlag = true;
	if (nFlag)
	{
		srand((unsigned int)time(nullptr));
		nFlag = false;
	}

	nR = rand() % 255;
	nG = rand() % 255;
	nB = rand() % 255;

	return 0;
}

double pose3d::CPose3D::RectangleIoU(int min_x1, int min_y1, int max_x1, int max_y1, int min_x2, int min_y2, int max_x2, int max_y2)
{
	double ret;
	double rect1_area, rect2_area;
	double intersection_x_length, intersection_y_length;
	double intersection_area;
	double union_area;

	// 직사각형 A, B의 넓이를 구한다.
	// get area of rectangle A and B
	rect1_area = (max_x1 - min_x1) * (max_y1 - min_y1);
	rect2_area = (max_x2 - min_x2) * (max_y2 - min_y2);

	// Intersection의 가로와 세로 길이를 구한다.
	// get length and width of intersection.
	intersection_x_length = MIN(max_x1, max_x2) - MAX(min_x1, min_x2);
	intersection_y_length = MIN(max_y1, max_y2) - MAX(min_y1, min_y2);

	// width와 length의 길이가 유효하다면 IoU를 구한다.
	// If the width and length are valid, get IoU.
	if (intersection_x_length > 0 && intersection_y_length > 0) {
		intersection_area = intersection_x_length * intersection_y_length;
		union_area = rect1_area + rect2_area - intersection_area;
		ret = intersection_area / union_area;
	}
	else {
		ret = 0;
	}
	return ret;
}

DL_RESULT CPose3D::InitDLEngine(int nWidth, int nHeight, std::string strObject, std::string strPose)
{
	if (_detector != nullptr)
		_detector->release();
	_detector = new deep4d::object::detector();
	_detector_ctx.width = nWidth;
	_detector_ctx.height = nHeight;
	_detector_ctx.enginePath = "c:\\test_contents\\object1650_TK.engine";

	int ret = _detector->initialize(&_detector_ctx);
	if (ret != 0)
		return DL_RESULT::ERR_INIT_DETECT;

	if (_estimator != nullptr)
		_estimator->release();
	_estimator = new deep4d::pose::estimator();
	_estimator_ctx.width = nWidth;
	_estimator_ctx.height = nHeight;
	_estimator_ctx.enginePath = "c:\\test_contents\\pose.engine";

	ret = _estimator->initialize(&_estimator_ctx);
	if (ret != 0)
		return DL_RESULT::ERR_INIT_DETECT;

	return DL_RESULT::SUCCESS;
}

 int CPose3D::EstimatePoses(cv::cuda::GpuMat& mGpuImg, std::vector<DeepBase::PoseData_t>& vecPoses)
{
	cv::cuda::GpuMat cvtGpuImg;
	DeepBase::Detection_Data_t detectionData;
	std::vector<cv::Rect> vecBBox;
	std::vector<DeepBase::BBoxData_t> vecBBoxData;
	
	// common preproc
	cv::cuda::cvtColor(mGpuImg, cvtGpuImg, cv::COLOR_BGR2RGB);

	int ret = _detector->detect((uint8_t*)cvtGpuImg.data, cvtGpuImg.step, detectionData);
	if (ret != 0)
	{
		cvtGpuImg.release();
		return ret;
	}

	//  Grouping object ROI
	cv::Rect roi = findROI(detectionData.bboxes, detectionData.classId, detectionData.prob, vecBBox, vecBBoxData, _detector_ctx.width, _detector_ctx.height);

	std::vector<std::array<int, 36>> vecJointPoint{};

	// pose inference
	ret = _estimator->estimate((uint8_t*)cvtGpuImg.data, cvtGpuImg.step, { roi.x, roi.y, roi.width, roi.height }, vecJointPoint);
	if (ret != 0)
	{
		cvtGpuImg.release();
		return ret;
	}

	makePoseObjects(vecJointPoint, roi, vecBBox, detectionData, vecPoses);

	return 0;
}

 int CPose3D::ReconPose3D(CRecognition3DCameraInfo& camInfo, vector<Point3d>& vecPtJoint, bool bDraw = false)
 {
	 if (camInfo.GetCameraInfoCount() == 0)
		 return -1;

	 if(vecPtJoint.size() > 0)
		vecPtJoint.clear();

	 cv::Point3d ptBound0(0.0, 0.0, -100.0), ptBound1(100.0, 100.0, 0.0);

	 // 3D Reconstruction
	 vector<vector<Vec3d>> vVecPointCloud(JOINT_COUNT);
	 for (int i = 0; i < camInfo.GetCameraInfoCount(); i++)
	 {
		 for (int j = 0; j < camInfo.GetCameraInfoCount(); j++)
		 {
			 if (i == j)
				 continue;
			 Point3d ptPointSkewMinimumPoint;
			 Point3d ptImagePlanePoint1, ptCameraPosPoint1, ptImagePlanePoint2, ptCameraPosPoint2;

			 for (int k = 0; k < JOINT_COUNT; k++)
			 {
				 if (camInfo.GetImagePlane3DPointAs(i, k, ptImagePlanePoint1) == RECOGNITION_ERROR_CAMERA_INFO_NOT_EXIST)
					 continue;
				 if (camInfo.GetCameraPos3DPointAs(i, k, ptCameraPosPoint1) == RECOGNITION_ERROR_CAMERA_INFO_NOT_EXIST)
					 continue;
				 if (camInfo.GetImagePlane3DPointAs(j, k, ptImagePlanePoint2) == RECOGNITION_ERROR_CAMERA_INFO_NOT_EXIST)
					 continue;
				 if (camInfo.GetCameraPos3DPointAs(j, k, ptCameraPosPoint2) == RECOGNITION_ERROR_CAMERA_INFO_NOT_EXIST)
					 continue;

				 if ((ptImagePlanePoint1.x == 0 && ptImagePlanePoint1.y == 0 && ptImagePlanePoint1.z == 0) ||
					 (ptImagePlanePoint2.x == 0 && ptImagePlanePoint2.y == 0 && ptImagePlanePoint2.z == 0) ||
					 (ptCameraPosPoint1.x == 0 && ptCameraPosPoint1.y == 0 && ptCameraPosPoint1.z == 0) ||
					 (ptCameraPosPoint2.x == 0 && ptCameraPosPoint2.y == 0 && ptCameraPosPoint2.z == 0)
					 )
					 continue;

				 GetSkewMinCenterPointsFromTwoRays(
					 ptImagePlanePoint1, ptCameraPosPoint1,
					 ptImagePlanePoint2, ptCameraPosPoint2,
					 ptPointSkewMinimumPoint);

				 string strWidgetName1 = "Skew_" + to_string(i) + "_" + to_string(j) + "_" + to_string(k);

				 if (ptPointSkewMinimumPoint.x >= ptBound0.x && ptPointSkewMinimumPoint.x <= ptBound1.x &&
					 ptPointSkewMinimumPoint.y >= ptBound0.y && ptPointSkewMinimumPoint.y <= ptBound1.y &&
					 ptPointSkewMinimumPoint.z >= ptBound0.z && ptPointSkewMinimumPoint.z <= ptBound1.z)
				 {
					 Vec3d pt;
					 Point3dToVec3d(ptPointSkewMinimumPoint, pt);
					 vVecPointCloud[k].push_back(pt);
				 }

			 }
		 }
	 }

	 CVtkViewer* m_pVtkViewer = new CVtkViewer;

	 m_pVtkViewer->VtkInit();
	 m_pVtkViewer->DrawBatterBoxCube();

	 for (int i = 0; i < JOINT_COUNT; i++)
	 {
		 int nR = 0, nG = 0, nB = 0;
		 getRandomRGBColor(nR, nG, nB);

		 string strWidgetName = "Cloud_" + to_string(i);
		 m_pVtkViewer->ShowCloud(
			 strWidgetName,
			 vVecPointCloud[i],
			 nR, nG, nB);

		 Point3d ptJoint = AverageCloudPoint(vVecPointCloud[i]);
		 m_pVtkViewer->InputPoint(
			 strWidgetName + "_avg",
			 ptJoint,
			 nR, nG, nB);

		 vecPtJoint[i] = ptJoint;
	 }

	 if (bDraw)
	 {
		 m_pVtkViewer->DrawJoint(vecPtJoint);

		 m_pVtkViewer->ShowViewer();
	 }

	 delete m_pVtkViewer;

	 return 0;
 }
