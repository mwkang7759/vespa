﻿
// TestArServerDlg.cpp: 구현 파일
//

#include "pch.h"
#include "framework.h"
#include "TestArServer.h"
#include "TestArServerDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CTestArServerDlg 대화 상자



CTestArServerDlg::CTestArServerDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_TESTARSERVER_DIALOG, pParent)
	, m_iSelect(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTestArServerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	//  DDX_Control(pDX, IDC_CMB_CODECTYPE, m_cmbVCodec);
	DDX_Control(pDX, IDC_CMB_CODECTYPE, m_cmbCodecType);
	DDX_Control(pDX, IDC_CMB_ENC_CODECTYPE, m_cmbEncCodecType);
	DDX_Control(pDX, IDC_CMB_VCODEC, m_cmbVCodec);
	DDX_Control(pDX, IDC_CMB_RESOLUTION, m_cmbResize);
	DDX_Radio(pDX, IDC_RADIO1, m_iSelect);
}

BEGIN_MESSAGE_MAP(CTestArServerDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BTN_START, &CTestArServerDlg::OnBnClickedBtnStart)
	ON_BN_CLICKED(IDC_BTN_STOP, &CTestArServerDlg::OnBnClickedBtnStop)
	ON_CONTROL_RANGE(BN_CLICKED, IDC_RADIO1, IDC_RADIO2, &CTestArServerDlg::OnBnClickedRadio)
	ON_BN_CLICKED(IDC_BTN_BENCH_TEST, &CTestArServerDlg::OnBnClickedBtnBenchTest)
END_MESSAGE_MAP()


// CTestArServerDlg 메시지 처리기

BOOL CTestArServerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	CString strData;


	FrSetTraceFileName((char*)"arserver_test.log");

	m_cmbVCodec.AddString("H.264");
	m_cmbVCodec.AddString("H.265");
	m_cmbVCodec.SetCurSel(0);

	m_cmbCodecType.AddString("HW");
	m_cmbCodecType.AddString("SW");
	m_cmbCodecType.SetCurSel(1);

	m_cmbEncCodecType.AddString("HW");
	m_cmbEncCodecType.AddString("SW");
	m_cmbEncCodecType.SetCurSel(1);
	

	m_cmbResize.AddString("UHD (3840 x 2160)");
	m_cmbResize.AddString("FHD (1920 x 1080)");
	m_cmbResize.AddString("HD (1280 x 720)");
	m_cmbResize.SetCurSel(1);

	CWnd* pWnd = GetDlgItem(IDC_EDIT_URL);
	//strData = "rtsp://192.168.22.123:8554/main";
	//strData = "c:\\test_contents\\test.mp4";
	//strData = "c:\\test_contents\\qos_test\\003055_240.mp4";
	strData = "c:\\test_contents\\ffmpeg\\test_uhd.mp4";
	pWnd->SetWindowText(strData);

	pWnd = GetDlgItem(IDC_EDIT_DSTURL);
	strData = "c:\\test_contents\\001071_1291\\result_001071_1277.mp4";
	pWnd->SetWindowText(strData);
	


	pWnd = GetDlgItem(IDC_EDIT_PORT);
	strData.Format("%d", 9000);
	pWnd->SetWindowText(strData);

	pWnd = GetDlgItem(IDC_EDIT_BITRATE);
	strData.Format("%d", 20);		// Mbps
	pWnd->SetWindowText(strData);

	pWnd = GetDlgItem(IDC_EDIT_GOP);
	strData.Format("%d", 30);		// Mbps
	pWnd->SetWindowText(strData);

	pWnd = GetDlgItem(IDC_EDIT_FF_THREAD_NUM);
	strData.Format("%d", 4);
	pWnd->SetWindowTextA(strData);

	m_iSelect = 1;	// local test

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CTestArServerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 애플리케이션의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CTestArServerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CTestArServerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CTestArServerDlg::OnBnClickedBtnStart()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ArSvrParam svrParam;
	
	CString strData;
	char szCodec[1024] = "";
	char szResolution[1024] = "";
	char szType[1024] = "";
	
	CWnd* pWnd = GetDlgItem(IDC_EDIT_URL);
	pWnd->GetWindowText(strData);
	svrParam.url = strData;

	pWnd = GetDlgItem(IDC_EDIT_DSTURL);
	pWnd->GetWindowText(strData);
	svrParam.dstUrl = strData;

	pWnd = GetDlgItem(IDC_EDIT_PORT);
	pWnd->GetWindowText(strData);
	svrParam.wSvrPort = atoi(strData);

	pWnd = GetDlgItem(IDC_EDIT_FF_THREAD_NUM);
	pWnd->GetWindowText(strData);
	svrParam.dwThreadNum = atoi(strData);
	
	
	m_cmbVCodec.GetLBText(m_cmbVCodec.GetCurSel(), szCodec);
	svrParam.codec = szCodec;

	m_cmbCodecType.GetLBText(m_cmbCodecType.GetCurSel(), szType);
	svrParam.codecType = szType;

	m_cmbEncCodecType.GetLBText(m_cmbEncCodecType.GetCurSel(), szType);
	svrParam.codecEncType = szType;

	m_cmbResize.GetLBText(m_cmbCodecType.GetCurSel(), szResolution);
	if (!strncmp(szResolution, "UHD", 3)) {
		svrParam.dwWidth = 3840;
		svrParam.dwHeight = 2160;
	}
	else if (!strncmp(szResolution, "FHD", 3)) {
		svrParam.dwWidth = 1920;
		svrParam.dwHeight = 1080;
	}
	else if (!strncmp(szResolution, "HD", 2)) {
		svrParam.dwWidth = 1280;
		svrParam.dwHeight = 720;
	}
	else {
		svrParam.dwWidth = 1920;
		svrParam.dwHeight = 1080;
	}
	
	pWnd = GetDlgItem(IDC_EDIT_BITRATE);
	pWnd->GetWindowText(strData);
	svrParam.dwBitrate = atoi(strData) * 1024 * 1024;
	//svrParam.dwBitrate = 20 * 1024 * 1024;

	pWnd = GetDlgItem(IDC_EDIT_GOP);
	pWnd->GetWindowText(strData);
	svrParam.dwGopLength = atoi(strData);

	svrParam.dwFramerate = 30;
	//svrParam.dwWidth = 1920;
	//svrParam.dwHeight = 1080;
	svrParam.dwInterval = 15;
	//svrParam.wSvrPort = 9000;
	svrParam.wPushPort = 9900;
	svrParam.bBenchTest = FALSE;


	switch (m_iSelect) {
	case 0:
		svrParam.bEnableStreaming = TRUE;
		break;
	case 1:
		svrParam.bEnableStreaming = FALSE;
		break;
	default:
		svrParam.bEnableStreaming = FALSE;
		break;
	}
	
	//svrParam.url = "c:\\test_contents\\test.mp4";
	//svrParam.url = "c:\\001021_4.mp4";
	SYSTEM_INFO sysInfo;
	GetSystemInfo(&sysInfo);

		
	m_hArSvr = new FrArServer();
	m_hArSvr->FrSvrOpen(svrParam);
}


void CTestArServerDlg::OnBnClickedBtnStop()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_hArSvr->FrSvrClose();
	delete m_hArSvr;
}

void CTestArServerDlg::OnBnClickedRadio(UINT uiID)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	switch (m_iSelect) {
	case 0:
		AfxMessageBox("streaming..");
		break;
	case 1:
		AfxMessageBox("local..");
		break;
	}
}


void CTestArServerDlg::OnBnClickedBtnBenchTest()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	
	
	// H.264
	// 	   ArSvrParam svrParams[29];
	//svrParams[0] = { "c:\\test_contents\\test_src.mp4", "c:\\test_contents\\test_dst_h264_20m_gop1.mp4", "H.264", "HW", 1920, 1080, 1, 60, 20*1024*1024, 1, 9000, 9900, FALSE, TRUE};
	//svrParams[1] = { "c:\\test_contents\\test_src.mp4", "c:\\test_contents\\test_dst_h264_18m_gop1.mp4", "H.264", "HW", 1920, 1080, 1, 60, 18 * 1024 * 1024, 1, 9000, 9900, FALSE, TRUE };
	//svrParams[2] = { "c:\\test_contents\\test_src.mp4", "c:\\test_contents\\test_dst_h264_15m_gop1.mp4", "H.264", "HW", 1920, 1080, 1, 60, 15 * 1024 * 1024, 1, 9000, 9900, FALSE, TRUE };
	//svrParams[3] = { "c:\\test_contents\\test_src.mp4", "c:\\test_contents\\test_dst_h264_10m_gop1.mp4", "H.264", "HW", 1920, 1080, 1, 60, 10 * 1024 * 1024, 1, 9000, 9900, FALSE, TRUE };
	//svrParams[4] = { "c:\\test_contents\\test_src.mp4", "c:\\test_contents\\test_dst_h264_8m_gop1.mp4", "H.264", "HW", 1920, 1080, 1, 60, 8 * 1024 * 1024, 1, 9000, 9900, FALSE, TRUE };
	//svrParams[5] = { "c:\\test_contents\\test_src.mp4", "c:\\test_contents\\test_dst_h264_5m_gop1.mp4", "H.264", "HW", 1920, 1080, 1, 60, 5 * 1024 * 1024, 1, 9000, 9900, FALSE, TRUE };
	//svrParams[6] = { "c:\\test_contents\\test_src.mp4", "c:\\test_contents\\test_dst_h264_2m_gop1.mp4", "H.264", "HW", 1920, 1080, 1, 60, 2 * 1024 * 1024, 1, 9000, 9900, FALSE, TRUE };

	//svrParams[7] = { "c:\\test_contents\\test_src.mp4", "c:\\test_contents\\test_dst_h264_20m_gop12.mp4", "H.264", "HW", 1920, 1080, 1, 60, 20 * 1024 * 1024, 12, 9000, 9900, FALSE, TRUE };
	//svrParams[8] = { "c:\\test_contents\\test_src.mp4", "c:\\test_contents\\test_dst_h264_18m_gop12.mp4", "H.264", "HW", 1920, 1080, 1, 60, 18 * 1024 * 1024, 12, 9000, 9900, FALSE, TRUE };
	//svrParams[9] = { "c:\\test_contents\\test_src.mp4", "c:\\test_contents\\test_dst_h264_15m_gop12.mp4", "H.264", "HW", 1920, 1080, 1, 60, 15 * 1024 * 1024, 12, 9000, 9900, FALSE, TRUE };
	//svrParams[10] = { "c:\\test_contents\\test_src.mp4", "c:\\test_contents\\test_dst_h264_10m_gop12.mp4", "H.264", "HW", 1920, 1080, 1, 60, 10 * 1024 * 1024, 12, 9000, 9900, FALSE, TRUE };
	//svrParams[11] = { "c:\\test_contents\\test_src.mp4", "c:\\test_contents\\test_dst_h264_8m_gop12.mp4", "H.264", "HW", 1920, 1080, 1, 60, 8 * 1024 * 1024, 12, 9000, 9900, FALSE, TRUE };
	//svrParams[12] = { "c:\\test_contents\\test_src.mp4", "c:\\test_contents\\test_dst_h264_5m_gop12.mp4", "H.264", "HW", 1920, 1080, 1, 60, 5 * 1024 * 1024, 12, 9000, 9900, FALSE, TRUE };
	//svrParams[13] = { "c:\\test_contents\\test_src.mp4", "c:\\test_contents\\test_dst_h264_2m_gop12.mp4", "H.264", "HW", 1920, 1080, 1, 60, 2 * 1024 * 1024, 12, 9000, 9900, FALSE, TRUE };

	//// H.265
	//svrParams[14] = { "c:\\test_contents\\test_src.mp4", "c:\\test_contents\\test_dst_h265_20m_gop1.mp4", "H.265", "HW", 1920, 1080, 1, 60, 20 * 1024 * 1024, 1, 9000, 9900, FALSE, TRUE };
	//svrParams[15] = { "c:\\test_contents\\test_src.mp4", "c:\\test_contents\\test_dst_h265_18m_gop1.mp4", "H.265", "HW", 1920, 1080, 1, 60, 18 * 1024 * 1024, 1, 9000, 9900, FALSE, TRUE };
	//svrParams[16] = { "c:\\test_contents\\test_src.mp4", "c:\\test_contents\\test_dst_h265_15m_gop1.mp4", "H.265", "HW", 1920, 1080, 1, 60, 15 * 1024 * 1024, 1, 9000, 9900, FALSE, TRUE };
	//svrParams[17] = { "c:\\test_contents\\test_src.mp4", "c:\\test_contents\\test_dst_h265_10m_gop1.mp4", "H.265", "HW", 1920, 1080, 1, 60, 10 * 1024 * 1024, 1, 9000, 9900, FALSE, TRUE };
	//svrParams[18] = { "c:\\test_contents\\test_src.mp4", "c:\\test_contents\\test_dst_h265_8m_gop1.mp4", "H.265", "HW", 1920, 1080, 1, 60, 8 * 1024 * 1024, 1, 9000, 9900, FALSE, TRUE };
	//svrParams[19] = { "c:\\test_contents\\test_src.mp4", "c:\\test_contents\\test_dst_h265_5m_gop1.mp4", "H.265", "HW", 1920, 1080, 1, 60, 5 * 1024 * 1024, 1, 9000, 9900, FALSE, TRUE };
	//svrParams[20] = { "c:\\test_contents\\test_src.mp4", "c:\\test_contents\\test_dst_h265_2m_gop1.mp4", "H.265", "HW", 1920, 1080, 1, 60, 2 * 1024 * 1024, 1, 9000, 9900, FALSE, TRUE };

	//svrParams[21] = { "c:\\test_contents\\test_src.mp4", "c:\\test_contents\\test_dst_h265_20m_gop12.mp4", "H.265", "HW", 1920, 1080, 1, 60, 20 * 1024 * 1024, 12, 9000, 9900, FALSE, TRUE };
	//svrParams[22] = { "c:\\test_contents\\test_src.mp4", "c:\\test_contents\\test_dst_h265_18m_gop12.mp4", "H.265", "HW", 1920, 1080, 1, 60, 18 * 1024 * 1024, 12, 9000, 9900, FALSE, TRUE };
	//svrParams[23] = { "c:\\test_contents\\test_src.mp4", "c:\\test_contents\\test_dst_h265_15m_gop12.mp4", "H.265", "HW", 1920, 1080, 1, 60, 15 * 1024 * 1024, 12, 9000, 9900, FALSE, TRUE };
	//svrParams[24] = { "c:\\test_contents\\test_src.mp4", "c:\\test_contents\\test_dst_h265_10m_gop12.mp4", "H.265", "HW", 1920, 1080, 1, 60, 10 * 1024 * 1024, 12, 9000, 9900, FALSE, TRUE };
	//svrParams[25] = { "c:\\test_contents\\test_src.mp4", "c:\\test_contents\\test_dst_h265_8m_gop12.mp4", "H.265", "HW", 1920, 1080, 1, 60, 8 * 1024 * 1024, 12, 9000, 9900, FALSE, TRUE };
	//svrParams[26] = { "c:\\test_contents\\test_src.mp4", "c:\\test_contents\\test_dst_h265_5m_gop12.mp4", "H.265", "HW", 1920, 1080, 1, 60, 5 * 1024 * 1024, 12, 9000, 9900, FALSE, TRUE };
	//svrParams[27] = { "c:\\test_contents\\test_src.mp4", "c:\\test_contents\\test_dst_h265_2m_gop12.mp4", "H.265", "HW", 1920, 1080, 1, 60, 2 * 1024 * 1024, 12, 9000, 9900, FALSE, TRUE };

	//svrParams[28] = { "c:\\test_contents\\00000268.00001961.mp4", "c:\\test_contents\\test_dst_00000268.00001961.mp4", "H.264", "HW", 1920, 1080, 1, 30, 1 * 1024 * 1024, 30, 9000, 9900, FALSE, TRUE };


#if 0
	ArSvrParam svrParams[20];
	svrParams[0] = { "c:\\test_contents\\eSFR.mp4", "c:\\test_contents\\eSFR_dst_h264_5m_gop1.mp4", "H.264", "HW", 1920, 1080, 1, 30, 5 * 1024 * 1024, 1, 9000, 9900, FALSE, TRUE };
	svrParams[1] = { "c:\\test_contents\\eSFR.mp4", "c:\\test_contents\\eSFR_dst_h264_4m_gop1.mp4", "H.264", "HW", 1920, 1080, 1, 30, 4 * 1024 * 1024, 1, 9000, 9900, FALSE, TRUE };
	svrParams[2] = { "c:\\test_contents\\eSFR.mp4", "c:\\test_contents\\eSFR_dst_h264_3m_gop1.mp4", "H.264", "HW", 1920, 1080, 1, 30, 3 * 1024 * 1024, 1, 9000, 9900, FALSE, TRUE };
	svrParams[3] = { "c:\\test_contents\\eSFR.mp4", "c:\\test_contents\\eSFR_dst_h264_2m_gop1.mp4", "H.264", "HW", 1920, 1080, 1, 30, 2 * 1024 * 1024, 1, 9000, 9900, FALSE, TRUE };
	svrParams[4] = { "c:\\test_contents\\eSFR.mp4", "c:\\test_contents\\eSFR_dst_h264_1m_gop1.mp4", "H.264", "HW", 1920, 1080, 1, 30, 1 * 1024 * 1024, 1, 9000, 9900, FALSE, TRUE };

	svrParams[5] = { "c:\\test_contents\\eSFR.mp4", "c:\\test_contents\\eSFR_dst_h264_5m_gop12.mp4", "H.264", "HW", 1920, 1080, 1, 30, 5 * 1024 * 1024, 12, 9000, 9900, FALSE, TRUE };
	svrParams[6] = { "c:\\test_contents\\eSFR.mp4", "c:\\test_contents\\eSFR_dst_h264_4m_gop12.mp4", "H.264", "HW", 1920, 1080, 1, 30, 4 * 1024 * 1024, 12, 9000, 9900, FALSE, TRUE };
	svrParams[7] = { "c:\\test_contents\\eSFR.mp4", "c:\\test_contents\\eSFR_dst_h264_3m_gop12.mp4", "H.264", "HW", 1920, 1080, 1, 30, 3 * 1024 * 1024, 12, 9000, 9900, FALSE, TRUE };
	svrParams[8] = { "c:\\test_contents\\eSFR.mp4", "c:\\test_contents\\eSFR_dst_h264_2m_gop12.mp4", "H.264", "HW", 1920, 1080, 1, 30, 2 * 1024 * 1024, 12, 9000, 9900, FALSE, TRUE };
	svrParams[9] = { "c:\\test_contents\\eSFR.mp4", "c:\\test_contents\\eSFR_dst_h264_1m_gop12.mp4", "H.264", "HW", 1920, 1080, 1, 30, 1 * 1024 * 1024, 12, 9000, 9900, FALSE, TRUE };


	// H.265
	svrParams[10] = { "c:\\test_contents\\eSFR.mp4", "c:\\test_contents\\eSFR_dst_h265_5m_gop1.mp4", "H.265", "HW", 1920, 1080, 1, 30, 5 * 1024 * 1024, 1, 9000, 9900, FALSE, TRUE };
	svrParams[11] = { "c:\\test_contents\\eSFR.mp4", "c:\\test_contents\\eSFR_dst_h265_4m_gop1.mp4", "H.265", "HW", 1920, 1080, 1, 30, 4 * 1024 * 1024, 1, 9000, 9900, FALSE, TRUE };
	svrParams[12] = { "c:\\test_contents\\eSFR.mp4", "c:\\test_contents\\eSFR_dst_h265_3m_gop1.mp4", "H.265", "HW", 1920, 1080, 1, 30, 3 * 1024 * 1024, 1, 9000, 9900, FALSE, TRUE };
	svrParams[13] = { "c:\\test_contents\\eSFR.mp4", "c:\\test_contents\\eSFR_dst_h265_2m_gop1.mp4", "H.265", "HW", 1920, 1080, 1, 30, 2 * 1024 * 1024, 1, 9000, 9900, FALSE, TRUE };
	svrParams[14] = { "c:\\test_contents\\eSFR.mp4", "c:\\test_contents\\eSFR_dst_h265_1m_gop1.mp4", "H.265", "HW", 1920, 1080, 1, 30, 1 * 1024 * 1024, 1, 9000, 9900, FALSE, TRUE };

	svrParams[15] = { "c:\\test_contents\\eSFR.mp4", "c:\\test_contents\\eSFR_dst_h265_5m_gop12.mp4", "H.265", "HW", 1920, 1080, 1, 30, 5 * 1024 * 1024, 12, 9000, 9900, FALSE, TRUE };
	svrParams[16] = { "c:\\test_contents\\eSFR.mp4", "c:\\test_contents\\eSFR_dst_h265_4m_gop12.mp4", "H.265", "HW", 1920, 1080, 1, 30, 4 * 1024 * 1024, 12, 9000, 9900, FALSE, TRUE };
	svrParams[17] = { "c:\\test_contents\\eSFR.mp4", "c:\\test_contents\\eSFR_dst_h265_3m_gop12.mp4", "H.265", "HW", 1920, 1080, 1, 30, 3 * 1024 * 1024, 12, 9000, 9900, FALSE, TRUE };
	svrParams[18] = { "c:\\test_contents\\eSFR.mp4", "c:\\test_contents\\eSFR_dst_h265_2m_gop12.mp4", "H.265", "HW", 1920, 1080, 1, 30, 2 * 1024 * 1024, 12, 9000, 9900, FALSE, TRUE };
	svrParams[19] = { "c:\\test_contents\\eSFR.mp4", "c:\\test_contents\\eSFR_dst_h265_1m_gop12.mp4", "H.265", "HW", 1920, 1080, 1, 30, 1 * 1024 * 1024, 12, 9000, 9900, FALSE, TRUE };


	FrArServer* hArSvr;
	hArSvr = new FrArServer();

	for (int i = 0; i < 20; i++) {
		hArSvr->FrBenchTest(svrParams[i]);
	}
#endif
	
	//hArSvr->FrBenchTest(svrParams[28]);
	
}
