﻿
// PreEmulAppDlg.cpp: 구현 파일
//

#include "pch.h"
#include "framework.h"
#include "PreEmulApp.h"
#include "PreEmulAppDlg.h"
#include "afxdialogex.h"

#include <fstream>
#include <iostream>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CPreEmulAppDlg 대화 상자



CPreEmulAppDlg::CPreEmulAppDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_PREEMULAPP_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CPreEmulAppDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CPreEmulAppDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BTN_TEST, &CPreEmulAppDlg::OnBnClickedBtnTest)
	ON_BN_CLICKED(IDC_BTN_CLOSE, &CPreEmulAppDlg::OnBnClickedBtnClose)
	ON_BN_CLICKED(IDC_BTN_POST_CONN, &CPreEmulAppDlg::OnBnClickedBtnPostConn)
	ON_BN_CLICKED(IDC_BTN_SEND_POST, &CPreEmulAppDlg::OnBnClickedBtnSendPost)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BTN_VPD_CONN, &CPreEmulAppDlg::OnBnClickedBtnVpdConn)
	ON_BN_CLICKED(IDC_BTN_SEND_VPD, &CPreEmulAppDlg::OnBnClickedBtnSendVpd)
	ON_BN_CLICKED(IDC_BTN_CAM_RESTART, &CPreEmulAppDlg::OnBnClickedBtnCamRestart)
END_MESSAGE_MAP()


// CPreEmulAppDlg 메시지 처리기

BOOL CPreEmulAppDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	CString strData;

	SetTraceRollingFileName((char*)"backend_emul.log", 20 * 1024 * 1024, 2);
	SetTraceLogLevel(FDR_LOG_LEVEL_INFO);

	CWnd* pWnd = GetDlgItem(IDC_EDIT_POST_IP);
	strData = "10.82.5.149";
	pWnd->SetWindowText(strData);
	strcpy(m_PcdParam.PostSdIp, strData);

	pWnd = GetDlgItem(IDC_EDIT_VPD_IP);
	strData = "10.82.5.106";
	pWnd->SetWindowText(strData);
	strcpy(m_PcdParam.VpdIp, strData);

	pWnd = GetDlgItem(IDC_EDIT_URL);
	strData = "rtsp://192.168.22.123:8554/main";
	//strData = "c:\\test_contents\\test.mp4";
	pWnd->SetWindowText(strData);
	
	m_PcdParam.postPort = 19729;
	m_PcdParam.vpdPort = 19721;
	

	m_hEmul = FdrEmulOpen(&m_Param);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CPreEmulAppDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 애플리케이션의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CPreEmulAppDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CPreEmulAppDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void TaskPreEmul(void* lpVoid) {
	CPreEmulAppDlg* hEmul = (CPreEmulAppDlg*)lpVoid;
	LONG				lRet;

	/*LOG_I("TaskPreEmul - Start");

	for (int i = 0; i < 100; i++) {
		hEmul->m_hPreSd = FrPreEmulOpen(&hEmul->m_Param);
		FrSleep(1000);
	}*/

	
	LOG_I("TaskPreEmul - End");
	McExitTask();
}


void CPreEmulAppDlg::OnBnClickedBtnTest()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strData;
	
	CWnd* pWnd = GetDlgItem(IDC_EDIT_URL);
	pWnd->GetWindowText(strData);
	strcpy(m_szUrl, strData);
	m_PreParam.pUrl = m_szUrl;

	m_PreParam.port = m_PcdParam.vpdPort;
	strcpy(m_PreParam.VpdIp, m_PcdParam.VpdIp);

	FdrEmulPreStart(m_hEmul, &m_PreParam);
}


void CPreEmulAppDlg::OnBnClickedBtnClose()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	FdrEmulPreStop(m_hEmul);
}


void CPreEmulAppDlg::OnBnClickedButton1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CPreEmulAppDlg::OnBnClickedBtnPostConn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	FdrEmulConnectPost(m_hEmul, &m_PcdParam);
}


void CPreEmulAppDlg::OnBnClickedBtnSendPost()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	std::ifstream ist("backemul.json");
	std::string jsonMsg = "";
	for (char p; ist >> p;)
		jsonMsg += p;
	//std::cout << "json -> " << std::string(jsonMsg) << std::endl;

	//_clientPost.Send(jsonMsg.c_str(), jsonMsg.size());

	FdrEmulSendPost(m_hEmul, jsonMsg.c_str(), jsonMsg.size());
}


void CPreEmulAppDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	FdrEmulClose(m_hEmul);
}


void CPreEmulAppDlg::OnBnClickedBtnVpdConn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CWnd* pWnd = GetDlgItem(IDC_EDIT_VPD_IP);
	CString strData;
	pWnd->GetWindowText(strData);
	strcpy(m_PcdParam.VpdIp, strData);
	FdrEmulConnectVpd(m_hEmul, &m_PcdParam);
}


void CPreEmulAppDlg::OnBnClickedBtnSendVpd()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	std::ifstream ist("4dvpp0003.json");
	std::string jsonMsg = "";
	for (char p; ist >> p;)
		jsonMsg += p;
	
	FdrEmulSendVpd(m_hEmul, jsonMsg.c_str(), jsonMsg.size());
}


void CPreEmulAppDlg::OnBnClickedBtnCamRestart()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	FdrEmulRestartCamera(m_hEmul);
}
