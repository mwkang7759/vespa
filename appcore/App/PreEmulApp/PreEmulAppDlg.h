﻿
// PreEmulAppDlg.h: 헤더 파일
//

#pragma once

#include "FdrEmulatorAPI.h"
#include "SystemAPI.h"
#include "TraceAPI.h"

// CPreEmulAppDlg 대화 상자
class CPreEmulAppDlg : public CDialogEx
{
// 생성입니다.
public:
	CPreEmulAppDlg(CWnd* pParent = nullptr);	// 표준 생성자입니다.

	FdrEmulParam m_Param;
	FdrEmulPreParam m_PreParam;
	FdrEmulPcdParam m_PcdParam;

	FdrEmulHandle m_hEmul;

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PREEMULAPP_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	
	char			m_szUrl[4096] = "";
	char			m_szDstUrl[4096] = "";

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnTest();
	afx_msg void OnBnClickedBtnClose();
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedBtnPostConn();
	afx_msg void OnBnClickedBtnSendPost();
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedBtnVpdConn();
	afx_msg void OnBnClickedBtnSendVpd();
	afx_msg void OnBnClickedBtnCamRestart();
};
