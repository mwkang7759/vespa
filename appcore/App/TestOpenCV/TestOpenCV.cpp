﻿// TestOpenCV.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

//#include "TestCommon.h"

#define USING_TEST_OPENCV
#ifdef USING_TEST_OPENCV

#include <iostream>

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/types_c.h>
#include <bitset>
#include <algorithm>
#include <functional>


using namespace cv;
using namespace std;

#if defined(_DEBUG)
#pragma comment(lib, "opencv_core440d.lib")
#pragma comment(lib, "opencv_highgui440d.lib")
#pragma comment(lib, "opencv_imgproc440d.lib")
#pragma comment(lib, "opencv_imgcodecs440d.lib")
#pragma comment(lib, "opencv_calib3d440d.lib")
#pragma comment(lib, "opencv_cudaimgproc440d.lib")
#pragma comment(lib, "opencv_cudawarping440d.lib")
#pragma comment(lib, "opencv_cudaarithm440d.lib")
#pragma comment(lib, "opencv_video440d.lib")
#pragma comment(lib, "opencv_videoio440d.lib")
#pragma comment(lib, "opencv_cudacodec440d.lib")
#else
#pragma comment(lib, "opencv_core440.lib")
#pragma comment(lib, "opencv_highgui440.lib")
#pragma comment(lib, "opencv_imgproc440.lib")
#pragma comment(lib, "opencv_imgcodecs440.lib")
#pragma comment(lib, "opencv_calib3d440.lib")
#pragma comment(lib, "opencv_cudaimgproc440.lib")
#pragma comment(lib, "opencv_cudawarping440.lib")
#pragma comment(lib, "opencv_cudaarithm440.lib")
#pragma comment(lib, "opencv_video440.lib")
#pragma comment(lib, "opencv_videoio440.lib")
#pragma comment(lib, "opencv_cudacodec440.lib")
#endif


char& find(char s[], int index) {
    return s[index];
}

void print_matInfo(string m_name, Mat m) {
    cout << "[" << m_name << " matrix]" << endl;
    cout << "channel num = " << m.channels() << endl;
    cout << "row x col = " << m.rows << "x" << m.cols << endl << endl;
}

void print_vectorInfo(string v_name, vector<int>v) {
    cout << "[" << v_name << "]";
    if (v.empty()) cout << "Empty" << endl;
    else cout << ((Mat)v).reshape(1, 1) << endl;
    cout << ".size() = " << v.size() << endl;
}

    int x, y;
    int w, h;

    string szTitle = "control window";

void onMouse(int event, int mx, int my, int flag, void* param) {
    Mat* img = (Mat*)param;
    switch (event) {
    case EVENT_LBUTTONDOWN:
        Point pt(mx, my);
        circle(*img, pt, 5, Scalar(0)); // imshow is needed to update display everytime.. 
        imshow(szTitle, *img);
        x = x + 20;
		moveWindow(szTitle, x, y);
        break;
    }
}

void onChange(int value, void* userdata) {
    Mat* m = (Mat*)userdata;
    Mat m2 = *m - 130 + value;
    imshow(szTitle, m2);

}


int caclBrightness(Mat m) {
    int sum = 0;
    for (int i = 0; i < m.rows; i++) {
        for (int j = 0; j < m.cols; i++) {
            sum = sum + (m.at<Vec3b>(i, j)[0] + m.at<Vec3b>(i, j)[1] + m.at<Vec3b>(i, j)[2])/3;
        }
    }

    return sum / (m.rows * m.cols);
}

Mat calc_myHistogram(Mat m, int nBins) {
    Mat hist;
    int channels[] = { 0 };
    int histSize[] = { nBins };
    float range[] = { 0, 256 };
    const float* ranges[] = { range };
    calcHist(&m, 1, channels, Mat(), hist, 1, histSize, ranges);
    return hist;
}

void draw_histo(Mat hist, Mat& hist_img, Size size = Size(256, 200)) {

    hist_img = Mat(size, CV_8U, Scalar(255));
    float bin = (float)hist_img.cols / hist.rows;
    normalize(hist, hist, 0, hist_img.rows, NORM_MINMAX);
    for (int i = 0; i < hist.rows; i++) {
        float start_x = i * bin;
        float end_x = (i + 1) * bin;
        Point2f pt1(start_x, 0);
        Point2f pt2(end_x, hist.at<float>(i));

        if (pt2.y > 0) {
            rectangle(hist_img, pt1, pt2, Scalar(0), -1);
        }
    }
    flip(hist_img, hist_img, 0);
}

Mat readImage() {
    Mat I = imread("d:\\test_image\\lenna.bmp");
    if (I.empty()) {
        namedWindow("source");
        imshow("source", I);
    }

    waitKey(10);
    return I;
}

void getZeroPaddedImage(Mat& src, Mat& dst) {

}
template <typename T>
class PacketBuffer {
public:
    PacketBuffer(size_t maxSize = 0);
    virtual ~PacketBuffer() = default;

    bool bufferPacket(const T& packet);
    T getNextPacket();
private:
    std::queue<T> mPacket;
    size_t mMaxSize;
};
template <typename T> PacketBuffer<T>::PacketBuffer(size_t maxSize) : mMaxSize(maxSize) {

}
template <typename T> bool PacketBuffer<T>::bufferPacket(const T& packet) {
    if (mMaxSize > 0 && mPacket.size() == mMaxSize) {
        return false;
    }
    mPacket.push(packet);
    return true;
}
template <typename T> T PacketBuffer<T>::getNextPacket() {
    if (mPacket.empty()) {
        throw std::out_of_range("buffer is empty");
    }
    T temp = mPacket.front();
    mPacket.pop();
    return temp;
}
class Data final {
public:
    explicit Data(int value = 0) : mValue(value) {

    }
    int getValue() const { return mValue; }
    void setValue(int value) { mValue = value; }
private:
    int mValue;
};
// add multi gop
struct PackInfo {
    int firstFrameIdx = 0;
    int prevCamCh = -1;
    int prevFrameIdx = 0;
    int prevRecNo = 0;
    int prevGopIdx = 0;
    int lossFrame = false;
    int fpsRatio = 1;
    int totalCount = 0;
};
bool perfectScore(int num) {
    return (num >= 100);
}
void process(const vector<int>& vec, function<void(int)> f) {
    for (auto& i : vec) {
        f(i);
    }
}
void print(int num) {
    cout << num << " ";
    return;
}
// 함수 포인터로 스레드 만들기
void counter(int id, int numIterations) {
    for (int i = 0; i < numIterations; i++) {
        cout << "Counter " << id << " has value " << i << endl;
    }
}

// 함수 객체로 스레드 만들기
class Counter {
public:
    Counter(int id, int numIterations) : mId(id), mNumIterations(numIterations) {}
    void operator()() const {
        for (int i = 0; i < mNumIterations; i++) {
            cout << "Counter " << mId << " has value " << i << endl;
        }
    }
private:
    int mId;
    int mNumIterations;
};

// 멤버 함수로 스레드 만들기
class Request {
public:
    Request(int id) : mId(id) {}
    void process() {
        cout << "Processing request " << mId << endl;
    }
private:
    int mId;
};

/*
int main()
{
#if 0
    ///////////////////////////////////////////////////////////////////////////
    // 1. 함수 포인터로 스레드 호출
    thread t1(counter, 1, 6);
    thread t2(counter, 2, 4);
    
    t1.join();
    t2.join();
    ///////////////////////////////////////////////////////////////////////////
#endif
#if 0
    ///////////////////////////////////////////////////////////////////////////
    // 2. 함수 객체 스레드 호출
    // 유니폼 초기화를 사용하는 방법
    thread t1{ Counter{1, 20}};
    // 일반 변수처럼 네임드 인스턴스로 초기화하는 방법
    Counter c(2, 12);
    thread t2(c);
    // 임시 객체를 사용하는 방법
    thread t3(Counter(3, 10));

    // 세 스레드가 모두 마칠 때까지 기다린다.
    t1.join();
    t2.join();
    t3.join();
    ///////////////////////////////////////////////////////////////////////////
#endif
#if 0
    ///////////////////////////////////////////////////////////////////////////
    // 3. 람다 표현식으로 스레드 만들기 & 호출
    int id = 1;
    int numIterations = 5;
    thread t1([id, numIterations] {
        for (int i = 0; i < numIterations; ++i) {
            cout << "Counter " << id << " has value " << i << endl;
        }
        });
    t1.join();
    ///////////////////////////////////////////////////////////////////////////
#endif
#if 0
    ///////////////////////////////////////////////////////////////////////////
    // 1. 함수 포인터로 스레드 호출
    Request req(100);
    thread t1{ &Request::process, &req };

    t1.join();
    
    ///////////////////////////////////////////////////////////////////////////
#endif
    cout << "Close main()..";

    return 0;
}
*/

#include <opencv2/core/cuda.hpp>
#include <opencv2/cudawarping.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/cudaarithm.hpp>
#include <opencv2/cudafilters.hpp>
#include <opencv2/cudacodec.hpp>

#include <cuda_runtime.h>
#include <Simd/SimdLib.h>

static __global__ void blending() {

}

int main() {
    /*std::string _in = "c:\\test_contents\\003027_20.mp4";
    cv::Ptr<cudacodec::VideoReader> in = cudacodec::createVideoReader(_in);

    cout << "cudacodec::opencv videoreader create().." << endl;*/


#if 0
    cv::Mat draw = cv::Mat::zeros(img.size(), CV_8UC3);

    DrawArCircle(draw, 0);
    DrawArPolygon(draw);
    DrawArArrow(draw);

    cv::cuda::GpuMat draw_gpu;

    draw_gpu.upload(draw);

    cv::Mat h_inv;
    ESMMovieGraphic::HomoMatrix_inv(&_param->pointInfo[_param->frameInfo[_globalCnt].prefix], &h_inv);

    //cv::cuda::GpuMat warp = cv::cuda::GpuMat(img.size(), img.type());
    cv::Mat warp = cv::Mat(img.size(), img.type());

    // need to handle h_inv exception
    if (h_inv.rows >= 3 && h_inv.cols >= 3) {
        cv::warpPerspective(draw, warp, h_inv, img.size(), 1, 0);
    }

    // color test..
    double objAlpha = 1.0;
    double backAlpha = 1.0 - objAlpha;

    cv::Mat back;
    img.download(back);

    //cv::Mat color_mat = cv::Mat(cv::Size(3840, 2160), CV_8UC3, cv::Scalar(100, 100, 100));
    for (int j = 0; j < back.rows; j++) {
        uchar* dst = back.ptr<uchar>(j);
        uchar* src = warp.ptr<uchar>(j);
        for (int i = 0; i < back.cols; i++) {
            if (src[3 * i + 0] == 0 && src[3 * i + 1] == 0 && src[3 * i + 2] == 0)
                continue;

            dst[3 * i + 0] = dst[3 * i + 0] * backAlpha + src[3 * i + 0] * objAlpha;
            dst[3 * i + 1] = dst[3 * i + 1] * backAlpha + src[3 * i + 1] * objAlpha;
            dst[3 * i + 2] = dst[3 * i + 2] * backAlpha + src[3 * i + 2] * objAlpha;
        }
    }
    img.upload(back);
    return;
#endif
    cv::Mat imgSrc1 = imread("c:\\test_contents\\ncaa_src\\image\\003011_1.jpg", IMREAD_COLOR);
    cv::Mat imgSrc2 = imread("c:\\test_contents\\ncaa_src\\image\\003030_1.jpg", IMREAD_COLOR);

    cv::Point2d startPos(720, 940);
    cv::Point2d endPos(2540, 540);

    int alpha = 100;
    
    // color test..
    double objAlpha = 1.0;
    double backAlpha = 1.0 - objAlpha;

    cv::Mat draw = cv::Mat::zeros(imgSrc1.size(), CV_8UC3);

    cv::Scalar clr1(255 - 0, 255 - 255, 255 - 0);
    ellipse(draw, cv::Point(startPos), cv::Size(100, 100), 0, 0, 360, clr1, -1, cv::LINE_AA);

    cv::Scalar clr2(255 * objAlpha, 0 * objAlpha, 0 * objAlpha);
    ellipse(draw, cv::Point(endPos), cv::Size(100, 100), 0, 0, 360, clr2, -1, cv::LINE_AA);

    auto start = std::chrono::system_clock::now();

    //cv::Mat color_mat = cv::Mat(cv::Size(3840, 2160), CV_8UC3, cv::Scalar(100, 100, 100));
    for (int j = 0; j < imgSrc1.rows; j++) {
        uchar* dst = imgSrc1.ptr<uchar>(j);
        uchar* src = draw.ptr<uchar>(j);
        for (int i = 0; i < imgSrc1.cols; i++) {
            if (src[3 * i + 0] == 0 && src[3 * i + 1] == 0 && src[3 * i + 2] == 0)
                continue;

            dst[3 * i + 0] = dst[3 * i + 0] * backAlpha + src[3 * i + 0];
            dst[3 * i + 1] = dst[3 * i + 1] * backAlpha + src[3 * i + 1];
            dst[3 * i + 2] = dst[3 * i + 2] * backAlpha + src[3 * i + 2];
        }
    }

    auto end = std::chrono::system_clock::now();
    std::chrono::milliseconds delta = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    cout << "alpha blending,  chrono Tick=" << delta.count();

    //if (_is_icon_loaded && drawLogo)
    //SimdAlphaBlending(_logo, _context->width * 3, _context->width, _context->height, 3, _alpha, _context->width, _rgb_frame->data[0], _context->width * 3);

    

    return 1;
}

#endif
