
#include "SystemAPI.h"
#include "TraceAPI.h"
#include "StreamFFWriter.h"
#include "UtilAPI.h"


LRSLT FFWriterOpen(void** phWriter, FrURLInfo* hUrlInfo) {
	FFWriterHandle	hWriter;
	int				iRet;

	hWriter = (FFWriterHandle)MALLOCZ(sizeof(FFWriterStruct));
	if (hWriter) {
		LOG_I("FFWriterOpen - url=%s Begin..", hUrlInfo->pUrl);
		
		// copy src path..
		strcpy(hWriter->m_szSrcName, hUrlInfo->pUrl);

		hWriter->format = av_guess_format(NULL, hUrlInfo->pUrl, NULL);
		if (!hWriter->format) {
			return FR_FAIL;
		}


		iRet = avformat_alloc_output_context2(&hWriter->format_ctx, hWriter->format, NULL, hUrlInfo->pUrl);
		if (iRet < 0) {
			LOG_E("FFWriterOpen avformat_alloc_output_context2() fail..(%d)", iRet);
			return FR_FAIL;
		}

		/*hWriter->dstCodec = avcodec_find_encoder(AV_CODEC_ID_H264);
		if (!hWriter->dstCodec) {
			return FR_FAIL;
		}*/
		
		hWriter->video_stream = avformat_new_stream(hWriter->format_ctx, NULL);
		if (!hWriter->video_stream) {
			return FR_FAIL;
		}

		/*hWriter->dstCodecCtx = avcodec_alloc_context3(hWriter->dstCodec);
		if (!hWriter->dstCodecCtx) {
			return FR_FAIL;
		}*/

		*phWriter = (void*)hWriter;
	}
	else
		return	COMMON_ERR_MEM;
	
	LOG_I("FFWriterOpen - End..");
	return	FR_OK;
}

void FFWriterClose(void* pVoid) {
	FFWriterHandle	hWriter = (FFWriterHandle)pVoid;
	LOG_I("FFWriterClose Begin..");

	if (hWriter) {
		// 0. Define variables
		if (hWriter->video_stream && hWriter->video_stream->codecpar->extradata_size > 0) {
			av_free(hWriter->video_stream->codecpar->extradata);
			hWriter->video_stream->codecpar->extradata = NULL;
		}

		//int ret = -1;
		av_write_trailer(hWriter->format_ctx);
		if (!(hWriter->format->flags & AVFMT_NOFILE)) {
			if (avio_close(hWriter->format_ctx->pb) < 0) {
				//return ErrorVM::ERROR_CLOSE_DST_FILE;
			}
		}

		if (hWriter->dstCodecCtx) {
			avcodec_free_context(&hWriter->dstCodecCtx);
		}
		if (hWriter->format_ctx) {
			avformat_free_context(hWriter->format_ctx);
		}

		FREE(hWriter);
		LOG_I("FFWriterClose End..");
	}
}


LRSLT FFWriterSetInfo(void* pVoid, FrMediaInfo* hInfo, DWORD32 dwStartCTS) {
	FFWriterHandle	hWriter = (FFWriterHandle)pVoid;
	FrVideoInfo* hVideo = &hInfo->FrVideo;
	//double bitrate = -1.;
	int ret = -1;

	LOG_I("FFWriterSetInfo Begin..");

	//_inoutFFMPEG.dstFormatCtx->max_delay = (int)(0.7 * AV_TIME_BASE);
	switch (hInfo->FrVideo.dwFourCC) {
	case FOURCC_HEVC:
	case FOURCC_H265:
		hWriter->video_stream->codecpar->codec_id = AV_CODEC_ID_H265;
		break;
	case FOURCC_h264:
	case FOURCC_AVC1:
	case FOURCC_H264:
		hWriter->video_stream->codecpar->codec_id = AV_CODEC_ID_H264;
		break;
	default:
		hWriter->video_stream->codecpar->codec_id = AV_CODEC_ID_H264;
		break;
	}
	

	hWriter->video_stream->codecpar->codec_type = AVMEDIA_TYPE_VIDEO;
	hWriter->video_stream->codecpar->format = hWriter->dstPixelFmt;
	hWriter->video_stream->codecpar->width = hVideo->dwWidth;
	hWriter->video_stream->codecpar->height = hVideo->dwHeight;
	hWriter->video_stream->codecpar->bit_rate = hVideo->dwBitRate;
	/*hWriter->video_stream->time_base.num = 1;
	hWriter->video_stream->time_base.den = hVideo->dwFrameRate / 1000;*/
	hWriter->video_stream->r_frame_rate.num = 1001;
	hWriter->video_stream->r_frame_rate.den = hVideo->dwFrameRate; //_context->videoFps * 1000;
	hWriter->video_stream->codecpar->codec_tag = 0;
	
	hWriter->video_stream->codecpar->extradata_size = hVideo->dwConfig;
	hWriter->video_stream->codecpar->extradata = (BYTE*)(av_malloc(hVideo->dwConfig));
	memcpy(hWriter->video_stream->codecpar->extradata, hVideo->pConfig, hVideo->dwConfig);
	

	/*avcodec_parameters_to_context(hWriter->dstCodecCtx, hWriter->video_stream->codecpar);
	hWriter->dstCodecCtx->width = hVideo->dwWidth;
	hWriter->dstCodecCtx->height = hVideo->dwHeight;
	hWriter->dstCodecCtx->time_base.num = 1;
	hWriter->dstCodecCtx->time_base.den = hVideo->dwFrameRate/1000;
	hWriter->dstCodecCtx->max_b_frames = 1;
	hWriter->dstCodecCtx->gop_size = 1;
	hWriter->dstCodecCtx->bit_rate = hVideo->dwBitRate;

	if (hWriter->format_ctx->oformat->flags & AVFMT_GLOBALHEADER) {
		hWriter->dstCodecCtx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
	}

	ret = avcodec_open2(hWriter->dstCodecCtx, hWriter->dstCodec, NULL);
	if (ret < 0) {
		return FR_FAIL;;
	}*/

	
	if ((ret = avio_open(&hWriter->format_ctx->pb, hWriter->m_szSrcName, AVIO_FLAG_WRITE)) < 0) {
		char err_string[MAX_PATH] = { 0 };
		av_make_error_string(err_string, MAX_PATH, ret);
		return FR_FAIL;;
	}
	

	if ((ret = avformat_write_header(hWriter->format_ctx, NULL)) < 0) {
		return FR_FAIL;;
	}

	av_dump_format(hWriter->format_ctx, 0, hWriter->m_szSrcName, 1);

//#ifndef NOT_MOOV_ESTIMATE
//    /// calculate the size of moov
//    moov_position = (DWORD32)pFile->moov_position;
//    pFile->moov_size = MW_mp4_write_moov_size(pFile, &(pFile->moov));
//    pFile->moov_position = moov_position;
//    TRACE("[Writer]	MP4FWriterSetInfo - moov initial size (%d)!!!", pFile->moov_size);
//#endif
	LOG_I("FFWriterSetInfo End..");

	return	FR_OK;
}

LRSLT FFWriterUpdateInfo(void* pVoid) {
	FFWriterHandle	hWriter = (FFWriterHandle)pVoid;
	//DWORD32			dwVideoAvgBitRate = 0, dwAudioAvgBitRate = 0;
	//DWORD32			dwTime;

	LOG_I("FFWriterUpdateInfo Begin..");

	if (hWriter) {
	}
	
	LOG_I("FFWriterUpdateInfo End..");

	return	FR_OK;
}

LRSLT FFWriterWriteFrame(void* pVoid, FrMediaStream* pMedia) {
	FFWriterHandle	hWriter = (FFWriterHandle)pVoid;
	BYTE*			pFrame;
	//BOOL			bIntra;
	//DWORD32			i, dwNalNum = 0, dwDur;
    //DWORD32         dwCurrentSize = 0;
	AVPacket pkt;
	int ret;

	pFrame = pMedia->pFrame;

	if (pMedia->tMediaType == AUDIO_MEDIA) {
	}
	else if (pMedia->tMediaType == VIDEO_MEDIA) {
		LOG_D("FFWriteWriteFrame frame num(%d), start cts(%d), type(%d)", pMedia->dwFrameNum, pMedia->dwCTS, pMedia->tFrameType[0]);
		
		// msec base
		DWORD32 dwCTS = pMedia->dwCTS;
		DWORD32 dwDelta = 1000*1000/hWriter->video_stream->r_frame_rate.den;
		AVRational	bq;
		bq.num = 1;
		bq.den = 1000;
		
		for (int i = 0; i < pMedia->dwFrameNum; i++) {
			av_init_packet(&pkt);
			pkt.pts = av_rescale_q(dwCTS, bq, hWriter->video_stream->time_base);
			pkt.dts = pkt.pts;
			pkt.duration = av_rescale_q(1, bq, hWriter->video_stream->time_base);

			pkt.pos = -1;
			pkt.stream_index = hWriter->video_stream->index;
			pkt.data = pFrame;
			pkt.size = pMedia->dwFrameLen[i];
			
			ret = av_write_frame(hWriter->format_ctx, &pkt);
			av_packet_unref(&pkt);

			pFrame += pMedia->dwFrameLen[i];
			dwCTS += dwDelta;
		}
	}

	return	FR_OK;
}
