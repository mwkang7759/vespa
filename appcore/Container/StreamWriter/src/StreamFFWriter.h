
#ifndef	_STREAMFFWRITER_H_
#define	_STREAMFFWRITER_H_

#include "SystemAPI.h"
#include "TraceAPI.h"
#include "StreamWriter.h"

#define	DEF_BUFFER_SIZE_IN_SEC			10000
#define	MAX_NAL							1000
#define	DEF_AUDIO_GATHER_BUFFER			5		// must be 0 or 2 or higher

#ifdef __cplusplus
extern "C"
{
#endif

#include "libavutil/timestamp.h"
#include "libavformat/avformat.h"
#include "libavcodec/avcodec.h"
#include "libavutil/opt.h"
#include "libavdevice/avdevice.h"
#include "libswscale/swscale.h"
#include "libavutil/avutil.h"
#include "libavutil/imgutils.h"
#include "libavutil/mathematics.h"



#ifdef __cplusplus
}
#endif


typedef	struct {
	DWORD32*		m_pWrittenSize;

	DWORD32			m_dwAudioFourCC;
	DWORD32			m_dwVideoFourCC;
	DWORD32			m_dwTextFourCC;

	DWORD32			m_dwSampleRate;
	DWORD32			m_dwVideoFrameRate;
	DWORD32			m_dwTextBitRate;

	char	m_szSrcName[2048];	// need to modify..
	
	AVFormatContext* format_ctx;
	AVOutputFormat* format;
	AVStream* video_stream;
	AVStream* audio_stream;
	AVCodecContext* dstCodecCtx;
	AVCodec* dstCodec;
	AVStream* dstStream;
	enum AVPixelFormat dstPixelFmt;
	
	
} FFWriterStruct, *FFWriterHandle;

LRSLT FFWriterOpen(void** phWriter, FrURLInfo* hUrlInfo);
void FFWriterClose(void* pVoid);
LRSLT FFWriterSetInfo(void* pVoid, FrMediaInfo* hInfo, DWORD32 dwStartCTS);
LRSLT FFWriterUpdateInfo(void* pVoid);
LRSLT FFWriterWriteFrame(void* pVoid, FrMediaStream* pMedia);

#endif	// _STREAMFFWRITER_H_
