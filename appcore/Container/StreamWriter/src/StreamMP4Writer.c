
#include "SystemAPI.h"
#include "TraceAPI.h"
#include "StreamMP4Writer.h"
#include "UtilAPI.h"

#include "mov-format.h"
#include "mov-file-buffer.h"


//const struct mov_buffer_t* mov_file_buffer(void);

LRSLT MP4WriterOpen(void** phWriter, FrURLInfo* hUrlInfo) {
	MP4WriterHandle	hWriter;
	//int				iRet;

	hWriter = (MP4WriterHandle)MALLOCZ(sizeof(MP4WriterStruct));
	if (hWriter) {
		LOG_I("FFWriterOpen - url=%s Begin..", hUrlInfo->pUrl);
		
		// copy src path..
		strcpy(hWriter->m_szSrcName, hUrlInfo->pUrl);
		hWriter->m_fp = fopen(hWriter->m_szSrcName, "wb+");

		//hWriter->m_hFile = FrOpenFile(hUrlInfo->pUrl, FILE_CREATE | FILE_WRITE);
		hWriter->m_hMP4Writer = mov_writer_create(mov_file_buffer(), hWriter->m_fp, MOV_FLAG_FASTSTART);
		
		

		*phWriter = (void*)hWriter;
	}
	else
		return	COMMON_ERR_MEM;
	
	LOG_I("FFWriterOpen - End..");
	return	FR_OK;
}

void MP4WriterClose(void* pVoid) {
	MP4WriterHandle	hWriter = (MP4WriterHandle)pVoid;
	LOG_I("MP4WriterClose Begin..");

	if (hWriter) {
		mov_writer_destroy(hWriter->m_hMP4Writer);
		fclose(hWriter->m_fp);
		FREE(hWriter);
		LOG_I("MP4WriterClose End..");
	}
}


LRSLT MP4WriterSetInfo(void* pVoid, FrMediaInfo* hInfo, DWORD32 dwStartCTS) {
	MP4WriterHandle	hWriter = (MP4WriterHandle)pVoid;
	//FrVideoInfo* hVideo = &hInfo->FrVideo;
	//double bitrate = -1.;
	//int ret = -1;

	LOG_I("MP4WriterSetInfo Begin..");

	//_inoutFFMPEG.dstFormatCtx->max_delay = (int)(0.7 * AV_TIME_BASE);
	switch (hInfo->FrVideo.dwFourCC) {
	case FOURCC_HEVC:
	case FOURCC_H265:
		//hWriter->video_stream->codecpar->codec_id = AV_CODEC_ID_H265;
		hWriter->m_vtrack = mov_writer_add_video(hWriter->m_hMP4Writer, MOV_OBJECT_HEVC, hInfo->FrVideo.dwWidth, hInfo->FrVideo.dwHeight, hInfo->FrVideo.pConfig, hInfo->FrVideo.dwConfig);
		break;
	case FOURCC_h264:
	case FOURCC_AVC1:
	case FOURCC_H264:
		//hWriter->video_stream->codecpar->codec_id = AV_CODEC_ID_H264;
		break;
	default:
		//hWriter->video_stream->codecpar->codec_id = AV_CODEC_ID_H264;
		break;
	}
	

	
	LOG_I("FFWriterSetInfo End..");

	return	FR_OK;
}

LRSLT MP4WriterUpdateInfo(void* pVoid) {
	//MP4WriterHandle	hWriter = (MP4WriterHandle)pVoid;
	//DWORD32			dwVideoAvgBitRate = 0, dwAudioAvgBitRate = 0;
	//DWORD32			dwTime;

	LOG_I("FFWriterUpdateInfo Begin..");

	//if (hWriter) {
	//}
	
	LOG_I("FFWriterUpdateInfo End..");

	return	FR_OK;
}

LRSLT MP4WriterWriteFrame(void* pVoid, FrMediaStream* pMedia) {
	MP4WriterHandle	hWriter = (MP4WriterHandle)pVoid;
	BYTE*			pFrame;
	//BOOL			bIntra;
	//DWORD32			i, dwNalNum = 0, dwDur;
    //DWORD32         dwCurrentSize = 0;
	//AVPacket pkt;
	//int ret;

	pFrame = pMedia->pFrame;

	if (pMedia->tMediaType == AUDIO_MEDIA) {
	}
	else if (pMedia->tMediaType == VIDEO_MEDIA) {
		LOG_D("FFWriteWriteFrame frame num(%d), start cts(%d), type(%d)", pMedia->dwFrameNum, pMedia->dwCTS, pMedia->tFrameType[0]);
		//TRACE("[MP4 Writer] %3d-th [len %5d] [Frm %d]", hWriter->m_dwVFrmCnt, pMedia->m_dwFrameLen[0], pMedia->m_dwFrameNum);
		
		for (int i = 0; i < pMedia->dwFrameNum; i++) {			
			//ret = av_write_frame(hWriter->format_ctx, &pkt);
			int flag;
			if (pMedia->tFrameType[i] == FRAME_I)
				flag = MOV_AV_FLAG_KEYFREAME;
			else flag = 0;
			mov_writer_write(hWriter->m_hMP4Writer, hWriter->m_vtrack, pFrame, pMedia->dwFrameLen[i], pMedia->dwCTS, pMedia->dwCTS, flag);
			
			pFrame += pMedia->dwFrameLen[i];
		}
	}

	return	FR_OK;
}
