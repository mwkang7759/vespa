
#ifndef	_STREAMWRITER_H_
#define	_STREAMWRITER_H_

#include "SocketAPI.h"
#include "SystemAPI.h"
#include "TraceAPI.h"
#include "mediainfo.h"
#include "mediafourcc.h"

//#define	PARSE_RESULT

typedef	struct {
	MUTEX_HANDLE	m_hLock;
	void*			m_hWriter;
	BOOL			m_bMem;
	DWORD32*			m_pWrittenSize;
	

	LRSLT			(*WriterOpen)(void**, FrURLInfo*);
	void			(*WriterClose)(void*);
	LRSLT			(*WriterSetInfo)(void*, FrMediaInfo*, DWORD32);
	LRSLT			(*WriterUpdateInfo)(void*);
	LRSLT			(*WriterWriteFrame)(void*, FrMediaStream*);
	LRSLT			(*WriterUpdateData)(void*, FrMediaStream*);
	LRSLT			(*WriterGetHeaderInfo)(void*, FrMediaStream*);
	LRSLT			(*WriterClearBuffer)(void*, DWORD32, DWORD32);
	LRSLT			(*WriterEstimateMoov)(void*, FrMediaStream*, DWORD32, DWORD32, DWORD32, DWORD32);
	LRSLT			(*WriterWriteEstimateMoov)(void*);
	LRSLT			(*WriterGetSeekTime)(void*, DWORD32*, DWORD32*, DWORD32*, DWORD32*, DWORD32*, DWORD32*);
	DWORD32			(*WriterGetFileSize)(void *pVoid);
#ifdef	PARSE_RESULT
	FILE_HANDLE		m_hFile;
#endif
} FrWriterStruct;


#endif	// _STREAMWRITER_H_
