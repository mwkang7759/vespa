
#ifndef	_STREAMMP4WRITER_H_
#define	_STREAMMP4WRITER_H_

#include "SystemAPI.h"
#include "TraceAPI.h"
#include "StreamWriter.h"

#define	DEF_BUFFER_SIZE_IN_SEC			10000
#define	MAX_NAL							1000
#define	DEF_AUDIO_GATHER_BUFFER			5		// must be 0 or 2 or higher

#ifdef __cplusplus
extern "C"
{
#endif

#include "mov-writer.h"


#ifdef __cplusplus
}
#endif


typedef	struct {
	DWORD32*		m_pWrittenSize;

	DWORD32			m_dwAudioFourCC;
	DWORD32			m_dwVideoFourCC;
	DWORD32			m_dwTextFourCC;

	DWORD32			m_dwSampleRate;
	DWORD32			m_dwVideoFrameRate;
	DWORD32			m_dwTextBitRate;

	char	m_szSrcName[2048];	// need to modify..
	
	mov_writer_t* m_hMP4Writer;
	int				m_vtrack;
	FILE* m_fp;
	
	FILE_HANDLE		m_hFile;

	
} MP4WriterStruct, *MP4WriterHandle;

LRSLT MP4WriterOpen(void** phWriter, FrURLInfo* hUrlInfo);
void MP4WriterClose(void* pVoid);
LRSLT MP4WriterSetInfo(void* pVoid, FrMediaInfo* hInfo, DWORD32 dwStartCTS);
LRSLT MP4WriterUpdateInfo(void* pVoid);
LRSLT MP4WriterWriteFrame(void* pVoid, FrMediaStream* pMedia);

#endif	// _STREAMFFWRITER_H_
