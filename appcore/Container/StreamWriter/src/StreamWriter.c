
#include "SystemAPI.h"
#include "TraceAPI.h"
#include "StreamWriter.h"
#include "StreamWriterAPI.h"
#include "StreamFFWriter.h"
#include "StreamMP4Writer.h"

BOOL SelectWriter(FrWriterStruct* hWriter, int eType) {
	if (eType == _MP4_FF_FILE) {
		hWriter->WriterOpen = FFWriterOpen;
		hWriter->WriterClose = FFWriterClose;
		hWriter->WriterSetInfo = FFWriterSetInfo;
		hWriter->WriterUpdateInfo = FFWriterUpdateInfo;
		hWriter->WriterWriteFrame = FFWriterWriteFrame;
		hWriter->WriterUpdateData = NULL;
		hWriter->WriterGetHeaderInfo = NULL;
		hWriter->WriterClearBuffer = NULL;
		hWriter->WriterGetFileSize = NULL;
	}
	else if (eType == _MP4_FILE) {
		hWriter->WriterOpen = MP4WriterOpen;
		hWriter->WriterClose = MP4WriterClose;
		hWriter->WriterSetInfo = MP4WriterSetInfo;
		hWriter->WriterUpdateInfo = MP4WriterUpdateInfo;
		hWriter->WriterWriteFrame = MP4WriterWriteFrame;
		hWriter->WriterUpdateData = NULL;
		hWriter->WriterGetHeaderInfo = NULL;
		hWriter->WriterClearBuffer = NULL;
		hWriter->WriterGetFileSize = NULL;
	}
	else
		return	FALSE;

	return	TRUE;
}

LRSLT FrWriterOpen(FrWriterHandle* phWriter, FrURLInfo *pURL) {
	FrWriterStruct*	hWriter;
	LRSLT			lRet;

	hWriter = (FrWriterStruct*)MALLOCZ(sizeof(FrWriterStruct));
	if (hWriter) {
		// memory file
		//hWriter->m_bMem = pURL->m_bMem;
		//hWriter->m_pWrittenSize = pURL->m_pWrittenSize;		// size of memory file

		FrCreateMutex(&hWriter->m_hLock);

		if (!SelectWriter(hWriter, pURL->URLType)) {
			FrWriterClose(hWriter);
			return	COMMON_ERR_FILETYPE;
		}

		
		lRet = hWriter->WriterOpen(&hWriter->m_hWriter, pURL);
		if (FRFAILED(lRet))	{
			FrWriterClose(hWriter);
			return	lRet;
		}

		*phWriter = hWriter;

#ifdef	PARSE_RESULT
		hWriter->m_hFile = McOpenFile("writer.txt", FILE_CREATE|FILE_WRITE);
#endif
	}
	else
		return COMMON_ERR_MEM;

	return	FR_OK;
}

void FrWriterClose(FrWriterHandle hHandle) {
	FrWriterStruct*	hWriter = (FrWriterStruct*)hHandle;

	if (hWriter) {
		TRACE("FrWriterClose Begin..");

		// Get the file size
		//if( hWriter->m_pWrittenSize)
		//	*(hWriter->m_pWrittenSize) = hWriter->WriterGetFileSize(hWriter->m_hWriter);
		hWriter->WriterClose(hWriter->m_hWriter);

 		FrDeleteMutex(&hWriter->m_hLock);
#ifdef	PARSE_RESULT
		McCloseFile(hWriter->m_hFile);
#endif
		FREE(hWriter);

		TRACE("FrWriterClose End..");
	}
}

LRSLT FrWriterSetInfo(FrWriterHandle hHandle, FrMediaInfo* hInfo, DWORD32 dwStartCTS) {
	FrWriterStruct*	hWriter = (FrWriterStruct*)hHandle;
	LRSLT			lRet;

	if (hWriter->WriterSetInfo) {
		//FrVideoInfo*	hVideo = &hInfo->FrVideo;
		//DWORD32			dwHeight, dwWidth;

		//// Change Height and Width in case of Rotate
		//if (hVideo && (hVideo->m_eFlip == MIRROR_ROTATE_90 || hVideo->m_eFlip == MIRROR_ROTATE_270))
		//{
		//	dwHeight = hVideo->m_dwHeight;
		//	dwWidth = hVideo->m_dwWidth;
		//	hVideo->m_dwWidth = dwHeight;
		//	hVideo->m_dwHeight = dwWidth;
		//}

		lRet = hWriter->WriterSetInfo(hWriter->m_hWriter, hInfo, dwStartCTS);

		// Return Height and Width to original value in case of Rotate
		//if (hVideo && (hVideo->m_eFlip == MIRROR_ROTATE_90 || hVideo->m_eFlip == MIRROR_ROTATE_270))
		//{
		//	hVideo->m_dwWidth = dwWidth;
		//	hVideo->m_dwHeight = dwHeight;
		//}

		if (FRFAILED(lRet))
			return	lRet;
	}

	return	FR_OK;
}

LRSLT FrWriterUpdateInfo(FrWriterHandle hHandle) {
	FrWriterStruct*	hWriter = (FrWriterStruct*)hHandle;
	LRSLT			lRet;

	if (hWriter->WriterUpdateInfo) {
		lRet = hWriter->WriterUpdateInfo(hWriter->m_hWriter);
		if (FRFAILED(lRet))
			return	lRet;
	}

	//if (hWriter->m_hLock);

	return	FR_OK;
}

LRSLT FrWriterWriteFrame(FrWriterHandle hHandle, FrMediaStream* pMedia) {
	FrWriterStruct*	hWriter = (FrWriterStruct*)hHandle;
	LRSLT			lRet;

	if (!pMedia->dwFrameNum) {
		LOG_E("FrWriterWriteFrame empty frame counter..(%d)", pMedia->dwFrameNum);
		return FR_FAIL;
	}

 	FrEnterMutex(&hWriter->m_hLock);

	if (hWriter->WriterWriteFrame) {
		lRet = hWriter->WriterWriteFrame(hWriter->m_hWriter, pMedia);
		if (FRFAILED(lRet))	{
			FrLeaveMutex(&hWriter->m_hLock);
			return lRet;
		}
	}

#ifdef	PARSE_RESULT
	if (pMedia->m_iMediaType == AUDIO_MEDIA)
		FPRINTF(hWriter->m_hFile, "[AUDIO FRAME] size=%d\n", pMedia->m_dwFrameLen);
	else if (pMedia->m_iMediaType == VIDEO_MEDIA)
		FPRINTF(hWriter->m_hFile, "[VIDEO FRAME] size=%d\n", pMedia->m_dwFrameLen);
	else if (pMedia->m_iMediaType == TEXT_MEDIA)
		FPRINTF(hWriter->m_hFile, "[TEXT FRAME] size=%d\n", pMedia->m_dwFrameLen);
	FILEDUMP(hWriter->m_hFile, pMedia->m_pFrame[0], pMedia->m_dwFrameLen);
#endif

 	FrLeaveMutex(&hWriter->m_hLock);

	return	FR_OK;
}

LRSLT FrWriterUpdateData(FrWriterHandle hHandle, FrMediaStream* pMedia) {
	FrWriterStruct*	hWriter = (FrWriterStruct*)hHandle;
	LRSLT			lRet;

	if (hWriter->WriterUpdateData) {
		lRet = hWriter->WriterUpdateData(hWriter->m_hWriter, pMedia);
		if (FRFAILED(lRet))
			return	lRet;
	}

	return	FR_OK;
}

//LRSLT McWriterGetHeaderInfo(McWriterHandle hHandle, McMediaData* pMedia)
//{
//	McWriterStruct*	hWriter = (McWriterStruct*)hHandle;
//	BYTE*			pHeader = NULL;
//	LRSLT			lRet;
//
//	if (hWriter->WriterGetHeaderInfo)
//	{
//		lRet = hWriter->WriterGetHeaderInfo(hWriter->m_hWriter, pMedia);
//		if (FRFAILED(lRet))
//			return	lRet;
//	}
//
//	return	FR_OK;
//}
//
//
//LRSLT McWriterClearBuffer(McWriterHandle hHandle, DWORD32	dwVideoID, DWORD32 dwAudioID)
//{
//	McWriterStruct*	hWriter = (McWriterStruct*)hHandle;
//	LRSLT			lRet;
//
//	if (hWriter->WriterClearBuffer)
//	{
//		lRet = hWriter->WriterClearBuffer(hWriter->m_hWriter, dwVideoID, dwAudioID);
//		if (FRFAILED(lRet))
//			return	lRet;
//	}
//
//	return	FR_OK;
//}
//
//LRSLT McWriterEstimateMoov(McWriterHandle hHandle, McMediaData* pMedia, DWORD32 dwTotalVideoFrame, DWORD32 dwTotalAudioSample, DWORD32 dwIDRCount, DWORD32 dwVideoConfigSize)
//{
//	McWriterStruct*	hWriter = (McWriterStruct*)hHandle;
//	LRSLT			lRet;
//
//	if (hWriter->WriterEstimateMoov)
//	{
//		lRet = hWriter->WriterEstimateMoov(hWriter->m_hWriter, pMedia, dwTotalVideoFrame, dwTotalAudioSample, dwIDRCount, dwVideoConfigSize);
//		if (FRFAILED(lRet))
//			return	lRet;
//	}
//
//	return	FR_OK;
//}
//
//LRSLT McWriterWriteEstimateMoov(McWriterHandle hHandle)
//{
//	McWriterStruct*	hWriter = (McWriterStruct*)hHandle;
//	LRSLT			lRet;
//
//	if (hWriter->WriterWriteEstimateMoov)
//	{
//		lRet = hWriter->WriterWriteEstimateMoov(hWriter->m_hWriter);
//		if (FRFAILED(lRet))
//			return	lRet;
//	}
//
//	return	lRet;
//}
//
//LRSLT McWriterGetSeekTime(McWriterHandle hHandle, DWORD32* dwOffset, DWORD32* dwFrameType, DWORD32* dwTime, DWORD32* dwAudioNum, DWORD32* dwVideoNum, DWORD32* dwNextIFrame)
//{
//	McWriterStruct*	hWriter = (McWriterStruct*)hHandle;
//	LRSLT			lRet;
//
//	if (hWriter->WriterGetSeekTime)
//	{
//		lRet = hWriter->WriterGetSeekTime(hWriter->m_hWriter, dwOffset, dwFrameType, dwTime, dwAudioNum, dwVideoNum, dwNextIFrame);
//		if (FRFAILED(lRet))
//			return	lRet;
//	}
//
//	return	lRet;
//}
//
//LRSLT McWriterGetFileSize(McWriterHandle hHandle)
//{
//	McWriterStruct*	hWriter = (McWriterStruct*)hHandle;
//
//	if (hWriter->WriterGetFileSize)
//		return	hWriter->WriterGetFileSize(hWriter->m_hWriter);
//
//	return	0;
//}
//
