/*****************************************************************************
*                                                                            *
*                            StreamReader Library							 *
*                                                                            *
*   Copyright (c) 2014 by DreamToBe, Incoporated. All Rights Reserved.      *
******************************************************************************

    File Name       : StreamM3UReader.h
    Author(s)       : CHANG, JoonHo
    Created         : 1 Aug 2014

    Description     : HLS Stream Reader API
    Notes           :

==============================================================================
                      Modification History (Reverse Order)
==============================================================================
    Date        Author      Location (variables) / Description
------------------------------------------------------------------------------

------------------------------------------------------------------------------*/

#ifndef	_STREAMM3UREADER_H_
#define	_STREAMM3UREADER_H_

#include "SocketAPI.h"
#include "SystemAPI.h"
#include "TraceAPI.h"
#include "mediainfo.h"
#include "UtilAPI.h"
//#include "SdpAPI.h"
//#include "RtspAPI.h"
//#include "RtpAPI.h"
//#include "DepacketAPI.h"
#include "StreamReader.h"
#include "StreamReaderAPI.h"

#define		HTTP_TASK_PRIORITY				THREAD_PRIORITY_NORMAL
#define		HTTP_TASK_STACKSIZE				0x2000
#define		HTTP_RECV_TIMEOUT				10000
#define		HTTP_DEFAULT_TIMEOUT			3000
#define		HTTP_DEFAULT_RETRY_COUNT		20				// 2 seconds. Maybe sleep 100 msec for each retry
#define		HTTP_CONNECT_RETRY_COUNT		5				//
#define		TS_INIT_BUFFER_TIME				1000
#define		TS_MAX_BUFFER_TIME				30000
#define		TS_MAX_UPDATE_SLEEP_TIME		1000			// sleep time when there is no segment url
#define		TS_MAX_ALLOWED_TS_DURATION		3000			// 

#define		MAX_HTTP_MSG_SIZE				1000
#define		MAX_SEG_URL_LENGTH				4096

#define		MAX_M3U_BUFFER_NUM				50
#define		MAX_M3U_LIVE_SEGMENT			20
#define		MIN_M3U_BUFFER_TIME				4000			//12000			// 12 sec
#define		MAX_M3U_BUFFER_TIME				60000			// 60 sec
#define		MAX_WAIT_TIME					10000			// 10 sec
#define		MIN_ALLOWED_SEQUENCE_NUM_DIFF	100				// trust the m3u when the sequence nuber is lower by MIN_ALLOWED_SEQUENCE_NUM_DIFF
#define		MAX_ALLOWED_WAIT_SEQ_NUM		2

#define		TS_MAX_TIMESTAMP_InMsec			95443718LL		// 8589934592(2^33) / 90 in Msec
#define		TS_MIN_TIMESTAMP_DIFF			1000			// 1 sec

#define		BASE_BUFFER_TIME				10				// 10 seconds
#define		BUFFER_RATIO_BOTTOM				100 //30
#define		BUFFER_RATIO_ONE_STEP_DOWN		150 //50
#define		BUFFER_RATIO_NO_CHANGE			200 //80
#define		BUFFER_RATIO_ONE_STEP_UP		250 //100

#define		M3U_NOT_COPY_FRAME_DATA

#ifdef		_DEBUG
#define		FILE_DUMP
//#define		FRAME_DUMP
#endif

typedef enum {
	SEG_TASK_IDLE = 0,
	SEG_TASK_RUN,
	SEG_TASK_PAUSED,
	SEG_TASK_SEEK,
	SEG_TASK_PAUSE,
	SEG_TASK_END
} SEG_TASK_STATUS;

typedef enum {
	READ_NOT_YET = 0,		// Not read a frame
	READ_DOING,				// During read
	READ_END				// EOF
} BUFFER_READ_STATUS;

typedef	struct {
	BYTE*				m_pszUrl;						// Url of segment
	FrReaderHandle		m_hSegReader;					// Reader handle
	BOOL				m_bGetInfo;						// Indicator of McReaderGetInfo() being called or not
	BOOL				m_bVideoConfigChanged;			// Indicate whether video config is changed or not
	BOOL				m_bBadSegment;					// Indicator of segment being corrupt 
	BOOL				m_bLastSegment;					// Indicator of segment being the last when vod
	DWORD32				m_dwSeqNumber;					// Sequence number
	DWORD32				m_dwStartCTS;					// CTS of 1st frame 
	DWORD32				m_dwStartCTSInfo;				// Start CTS from Segment info
	DWORD32				m_dwDuration;					// Duration of segment
	DWORD32				m_dwCurCTS;						// CTS of last read frame. used to calculate buffer fullness
	BOOL				m_bAudio;						// Audio exists in segment
	BOOL				m_bVideo;						// Video exists in segment
	BUFFER_READ_STATUS	m_eAudioRead;					// Audio Read Status
	BUFFER_READ_STATUS	m_eVideoRead;					// Video Read Status
	DWORD32				m_dwBuffer;						// m_pBuffer size
	DWORD32				m_dwBufferPos;					// Position where the data is written
	BYTE*				m_pBuffer;						// Pointer to current segment buffer
} M3UBuffer, *M3UBufferHandle;

typedef	struct {
	DWORD32				m_dwDuration;					// segment duration
	DWORD32				m_dwStartCTS;					// start CTS of segment. accumulated duration
	DWORD32				m_dwSeqNumber;					// Sequence number of the segment
	BYTE*				m_pszUrl;						// Segment url
} M3USegmentStruct, *M3USegmentHandle;

typedef	struct {
	BYTE				m_szUrl[MAX_SEG_URL_LENGTH];	// m3u url
	DWORD32				m_dwUrl;						// directory position
	BOOL				m_bLive;						// Vod or live
	DWORD32				m_dwStartSeqNumber;				// sequence number of the 1st m3u
	DWORD32				m_dwBandwidth;					// bandwidth of the stream
	DWORD32				m_dwAvgSegment;					// Average segment size : AR model
	DWORD32				m_dwLastUpdateTick;				// Tick count of m3u being updated last
	DWORD32				m_dwTargetDuration;				// Target duration of the current m3u
	DWORD32				m_dwSegNumInLiveService;		// Number of Segments in live m3u. 
														// m_dwTargetDuration * m_dwSegNumInLiveService will be max buffering
	DWORD32				m_dwCurSeqNumber;				// start sequence number of the m3u read last
	DWORD32				m_dwLastSeqNumber;				// last sequence number of the m3u read last
														// which equals to sequence number expected to add
	DWORD32				m_dwSegmentAlloced;				// size of m_phSegment
	DWORD32				m_dwSegmentUsedEnd;				// Segment written last. fixed for VoD. increased periodically for Live
	DWORD32				m_dwCurSegment;					// Segment to be read
	M3USegmentStruct**	m_phSegment;					// reference to an array of segments
	VOID*				m_hParentReader;				// referecne to the parent m3u reader
	TASK_HANDLE			m_hUpdateM3UTask;				// reference to the task of m3u update for live
	SOCK_HANDLE			m_hSocket;						// tcp socket handle to be reused when Keep-alive is supported by server
	BOOL				m_bM3UUpdatePaused;				// Paused or Running
} M3UStreamStruct, *M3UStreamHandle;

typedef	struct {
	TCHAR*				m_pszURL;						// m3u url. master or simple
	char*				m_pszUserAgent;
	FrURLInfo			m_URL;							// Url info

	BOOL				m_bSeek;						// Set when seeked
	BOOL				m_bPaused;						// Set when paused
	BOOL				m_bPaused2Change;				// Set when the stream is going to change
	LRSLT				m_lError;
	DWORD32				m_dwMedia;
	BOOL				m_bAudioExist;
	BOOL				m_bVideoExist;
	BOOL				m_bTextExist;
	MUTEX_HANDLE		m_hMutex;

	// Stream and Segment Info and control
	BOOL				m_bLive;						// Live
	DWORD32				m_dwDuration;					// Total duration in case of vod
	BOOL				m_bEnd;							// set when reader gets closed
	DWORD32				m_dwConnectTimeOut;				// tcp connect timeout in msecs
	DWORD32				m_dwRecvTimeOut;				// tcp recv timeout in msecs
	DWORD32				m_dwRetryCount;					// tcp reconnect tries

	DWORD32				m_dwTargetDuration;				// Target duration in case of live
	DWORD32				m_dwSeqeunce;					// Sequence number temporarily stored
	BOOL				m_bSegmentFound;				// Indicator of at least a segment being found
	DWORD32				m_dwStartTime;					// which is the 1st PES time stamp in the 1st segment
	DWORD32				m_dwCurTimeAudio;				// Audio CTS read last
	DWORD32				m_dwCurTimeVideo;				// Video CTS read last
	DWORD32				m_dwCurSeqNumber;				// Sequence number of the current segment
	DWORD32				m_dwDummyVideo;					// Counter of NoMedia frames when the stream falls down to audio-only stream

	// Segment Read Task
	BOOL				m_bProgressive;					// Progressive Download mode
	SEG_TASK_STATUS		m_eTaskStatus;					// Status : IDLE, RUN, PAUSE, SEEK etc
	SOCK_HANDLE			m_hSegSock;						// Tcp socket used by m_hGetOneSegmentTask
	TASK_HANDLE			m_hGetOneSegmentTask;			// Segment Read Task
	LRSLT				m_lGetOneSegmentError;			// returned error from m_hGetOneSegmentTask
	TASK_HANDLE			m_hGetSegmentTask;				// Segment Read Control Task
	LRSLT				m_lGetSegmentError;				// returned error from m_hGetSegmentTask
	BOOL				m_bFunction_GetOneSegment;		// Function call or Task invoke
	BOOL				m_bFirstRead;					// 1st segment read
	BOOL				m_bReaderStop;					// Stop the reader from download the segment fully

	// Buffer Settings
	BOOL				m_bDisableNoData;
	DWORD32				m_dwInitBuffTime;
	DWORD32				m_dwSeekBuffTime;
	DWORD32				m_dwBuffTime;
	BOOL				m_bLiveBuffTime;			// set by 1 when different buffer times need to be set for VoD and Live streams
	DWORD32				m_dwInitBuffTimeLive;
	DWORD32				m_dwSeekBuffTimeLive;
	DWORD32				m_dwBuffTimeLive;

	// Read Buffer Control
	BOOL				m_bMemOptimize;					// Reader is used for relay. Need to minimize memory
	DWORD32				m_dwMinBufferTime;				// Minimum required buffering. MIN_M3U_BUFFER_TIME when unset.
														// Somewhere between MIN_M3U_BUFFER_TIME (10sec) and MAX_M3U_BUFFER_TIME (60sec)
	DWORD32				m_dwRemainTime;					// time to the end of the contents from the current segment
	DWORD32				m_dwBufferRatio;				// Current buffer occupancy normalized by 100
	DWORD32				m_dwReadBuffer;					// Current read buffer index
	DWORD32				m_dwWriteBuffer;				// Current write buffer index
	M3UBuffer			m_Buffer[MAX_M3U_BUFFER_NUM];	// An array of buffers where segments are stored
	DWORD32				m_dwCurVideoReadBuffer;			// Current Audio read buffer index
	DWORD32				m_dwCurAudioReadBuffer;			// Current Video read buffer index
	DWORD32				m_dwAudioLostDuration;			// Lost Audio duration
	DWORD32				m_dwVideoLostDuration;			// Lost Video duration
	DWORD32				m_BandwithEstimated[MAX_M3U_BUFFER_NUM];			
	DWORD32				m_dwLastBWEstimation;			// Last Bandwidth Estimation
														// bandwidth estimated from download speed
	DWORD32				m_dwBENumber;					// # of valid values in m_BandwithEstimated
	DWORD32				m_dwBEIndex;					// # of current updated
	DWORD32				m_dwLastStreamChangeTick;		// Tick of stream changed last

	// Adaptive Stream
	DWORD32				m_dwStream;						// Number of streams in the session. 
	DWORD32				m_dwCurStream;					// Currently read stream
	M3UStreamStruct**	m_phStream;						// Reference to an array of streams

	// M3U or Segment Buffer
	DWORD32				m_dwM3UBuffer;					// Size of the temporary buffer
	BYTE*				m_pM3UBuffer;					// pointer to the buffer
	// Video Decoder Info
	DWORD32				m_dwVideoConfig;				// Size of video config
	BYTE				m_VideoConfig[MAX_CONFIG_SIZE];	// Pointer to video config
	FrMediaInfo*		m_pInfo;						// To store media info from get_info to be used for reopen video decoder
	BOOL				m_bVideo;						// Video exists in current segment
	BOOL				m_bAudio;						// Audio exists in current segment

	// Frame Buffer
	DWORD32				m_dwVideoFrame;
	BYTE*				m_pVideoFrame;
	DWORD32				m_dwAudioFrame;
	BYTE*				m_pAudioFrame;

	// Segment file format
	eM3UFileFormat		m_eM3UFormat;					// M3U Segment format
	BOOL				m_bH264TsFormat;				// Allows start code in H264 frames
	M3UStartTime		m_eM3UStartTime;				// M3U Start Time mode
	DWORD32				m_dwTsPacketSize;				// Store TS packet size
	DWORD32				m_dwPrevTsCTSVideo;				// Timestamp of previous frame
	DWORD32				m_dwPrevOutCTSVideo;			// Timestamp of previous frame put out from M3UReader
	DWORD32				m_dwWrapCountVideo;				// How many times PTS has wrapped down to 0
	DWORD32				m_dwPrevTsCTSAudio;				// Timestamp of previous frame from TS Reader
	DWORD32				m_dwPrevOutCTSAudio;			// Timestamp of previous frame put out from M3UReader
	DWORD32				m_dwWrapCountAudio;				// How many times PTS has wrapped down to 0

	DWORD32				m_dwPrevTsDTSVideo;				// Timestamp of previous frame
	DWORD32				m_dwWrapCountVideoDTS;			// How many times PTS has wrapped down to 0

#ifdef	FILE_DUMP
	FILE_HANDLE		m_hFile;
#endif

} M3UReaderStruct, *M3UReaderHandle;


LRSLT	M3UReaderOpen(void** phReader, FrURLInfo* pURL);
void	M3UReaderClose(void* pVoid);
LRSLT	M3UReaderGetInfo(void* pVoid, FrMediaInfo* hInfo);
LRSLT	M3UReaderStart(void* pVoid, DWORD32* pdwStart);
LRSLT	M3UReaderReadFrame(void* pVoid, FrMediaStream* pMedia);
BOOL	M3UReaderSeekByTime(void* pVoid, DWORD32 *pdwCTS, BOOL bKeyFrame);
//BOOL	M3UReaderSeek(void* pVoid, DWORD32* dwCTS, BOOL bKeyFrame);
//LRSLT	M3UReaderSeekByClock(void* pVoid, DWORD32 dwDate, DWORD32 dwTime);
//BOOL	M3UReaderSetTrackID(void* pVoid, DWORD32 dwTrackID, DWORD32 dwCurCTS, INT32 nMediaType);
BOOL	M3UReaderGetStatus(void* pVoid, void* p);
LRSLT	M3UReaderSetStatus(void* pVoid, INT32 iStatus, INT32 nScale, QWORD qwCurFileSize);


#endif	// _STREAMM3UREADER_H_
