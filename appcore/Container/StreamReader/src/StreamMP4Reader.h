#pragma once

#include "SystemAPI.h"
#include "TraceAPI.h"
#include "StreamReader.h"

#define	DEF_BUFFER_SIZE_IN_SEC			10000
#define	MAX_NAL							1000
#define	DEF_AUDIO_GATHER_BUFFER			5		// must be 0 or 2 or higher

#ifdef __cplusplus
extern "C"
{
#endif

#include "mov-reader.h"

#ifdef __cplusplus
}
#endif


typedef	struct {
	//DWORD32* m_pWrittenSize;

	DWORD32			m_dwAudioFourCC;
	DWORD32			m_dwVideoFourCC;
	DWORD32			m_dwTextFourCC;

	DWORD32			m_dwSampleRate;
	DWORD32			m_dwVideoFrameRate;
	DWORD32			m_dwTextBitRate;

	char	m_szSrcName[2048];	// need to modify..

	mov_reader_t* m_hMP4Reader;
	int				m_vtrack;
	FILE* m_fp;

	FILE_HANDLE		m_hFile;


} MP4ReaderStruct, * MP4ReaderHandle;

LRSLT MP4ReaderOpen(void** phWriter, FrURLInfo* hUrlInfo);
void MP4ReaderClose(void* pVoid);
LRSLT MP4ReaderGetInfo(void* pVoid, FrMediaInfo* hInfo, DWORD32 dwStartCTS);
LRSLT MP4ReaderReadFrame(void* pVoid, FrMediaStream* pMedia);
