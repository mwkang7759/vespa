#include "StreamMP4Reader.h"

#include "mov-format.h"
#include "mov-file-buffer.h"


LRSLT MP4ReaderOpen(void** phReader, FrURLInfo* hUrlInfo) {
	MP4ReaderHandle	hReader;
	//int				iRet;

	hReader = (MP4ReaderHandle)MALLOCZ(sizeof(MP4ReaderStruct));
	if (hReader) {
		LOG_I("MP4ReaderOpen - url=%s Begin..", hUrlInfo->pUrl);

		// copy src path..
		strcpy(hReader->m_szSrcName, hUrlInfo->pUrl);
		hReader->m_fp = fopen(hReader->m_szSrcName, "rb");

		//hWriter->m_hFile = FrOpenFile(hUrlInfo->pUrl, FILE_CREATE | FILE_WRITE);
		hReader->m_hMP4Reader = mov_reader_create(mov_file_buffer(), hReader->m_fp);



		*phReader = (void*)hReader;
	}
	else
		return	COMMON_ERR_MEM;

	LOG_I("MP4ReaderOpen - End..");
	return	FR_OK;
}

void MP4ReaderClose(void* pVoid) {
	MP4ReaderHandle	hReader = (MP4ReaderHandle)pVoid;
	LOG_I("MP4ReaderClose Begin..");

	if (hReader) {
		mov_reader_destroy(hReader->m_hMP4Reader);
		fclose(hReader->m_fp);
		FREE(hReader);
		LOG_I("MP4ReaderClose End..");
	}
}


LRSLT MP4ReaderGetInfo(void* pVoid, FrMediaInfo* hInfo, DWORD32 dwStartCTS) {
	MP4ReaderHandle	hReader = (MP4ReaderHandle)pVoid;
	//FrVideoInfo* hVideo = &hInfo->FrVideo;
	//double bitrate = -1.;
	//int ret = -1;

	LOG_I("MP4ReaderGetInfo Begin..");

	struct mov_reader_trackinfo_t info = {};
	//mov_reader_getinfo(mov, &info, flv);
	mov_reader_getinfo(hReader->m_hMP4Reader, &info, NULL);


	LOG_I("MP4ReaderGetInfo End..");

	return	FR_OK;
}


LRSLT MP4ReaderReadFrame(void* pVoid, FrMediaStream* pMedia) {
	MP4ReaderHandle	hReader = (MP4ReaderHandle)pVoid;
	BYTE* pFrame;
	//BOOL			bIntra;
	//DWORD32			i, dwNalNum = 0, dwDur;
	//DWORD32         dwCurrentSize = 0;
	//AVPacket pkt;
	//int ret;

	pFrame = pMedia->pFrame;

	if (pMedia->tMediaType == AUDIO_MEDIA) {
	}
	else if (pMedia->tMediaType == VIDEO_MEDIA) {
		//mov_reader_read(hReader->m_hMP4Reader, hReader->m_vtrack, pFrame, pMedia->dwFrameLen[i], pMedia->dwCTS, pMedia->dwCTS, flag);

		LOG_D("MP4ReaderReadFrame frame num(%d), start cts(%d), type(%d)", pMedia->dwFrameNum, pMedia->dwCTS, pMedia->tFrameType[0]);
		//TRACE("[MP4 Writer] %3d-th [len %5d] [Frm %d]", hWriter->m_dwVFrmCnt, pMedia->m_dwFrameLen[0], pMedia->m_dwFrameNum);

		for (int i = 0; i < pMedia->dwFrameNum; i++) {
			//ret = av_write_frame(hWriter->format_ctx, &pkt);
			int flag;
			if (pMedia->tFrameType[i] == FRAME_I)
				flag = MOV_AV_FLAG_KEYFREAME;
			else flag = 0;
			//mov_reader_read(hReader->m_hMP4Reader, hReader->m_vtrack, pFrame, pMedia->dwFrameLen[i], pMedia->dwCTS, pMedia->dwCTS, flag);

			pFrame += pMedia->dwFrameLen[i];
		}
	}

	return	FR_OK;
}
