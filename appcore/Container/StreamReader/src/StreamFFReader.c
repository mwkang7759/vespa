
#include "SystemAPI.h"
#include "TraceAPI.h"
#include "StreamFFReader.h"

//#pragma comment(lib, "avcodec.lib")
//#pragma comment(lib, "avformat.lib")
//#pragma comment(lib, "swscale.lib")
//#pragma comment(lib, "avutil.lib")


LRSLT FFReaderOpen(void** phReader, FrURLInfo* pURL) {
	FFReaderHandle	hReader;

	LOG_I("[Reader]	Open url(%s) Begin..", pURL->pUrl);
	hReader = (FFReaderHandle)MALLOCZ(sizeof(FFReaderStruct));
	if (hReader) {
		int ret = avformat_open_input(&hReader->_format_ctx, pURL->pUrl, NULL, NULL);
		if (ret) {
			char err[1024];
			av_strerror(ret, err, sizeof(err));
			LOG_E("[Reader]	Open - avformat_open_input error..ret(%d: %s)", ret, err);
			return COMMON_ERR_FOPEN;
		}

		//hReader->m_pUrl = pURL;
		*phReader = (void*)hReader;
	}
	else
		return	COMMON_ERR_MEM;

	LOG_I("[Reader]	Open End..");

	return	FR_OK;
}

void FFReaderClose(void* pVoid) {
	FFReaderHandle	hReader = (FFReaderHandle)pVoid;
	LOG_I("[Reader]	Close() Begin..");
	if (hReader) {
		if (hReader->m_pBuffer)
			FREE(hReader->m_pBuffer);
		
		if (hReader->_packet.data)
			av_packet_unref(&hReader->_packet);

		if (hReader->_packet_filtered.data)
			av_packet_unref(&hReader->_packet_filtered);

		avformat_close_input(&hReader->_format_ctx);
		//if (!hReader->m_bOutInFileHandle && hReader->m_hInFile)
		//	McCloseFile(hReader->m_hInFile);
		//if (hReader->m_pReader)
		//	hReader->m_hDtbReader->ReaderClose(hReader->m_pReader);
		FREE(hReader);
	}
	LOG_I("[Reader]	Close() End..");
}

BOOL FFReaderIsValidInfo(void* pVoid, QWORD qwCurFileSize) {
	//FFReaderHandle	hReader = (FFReaderHandle)pVoid;
	/*DWORD32			dwTag, dwSize, dwRemain;
	INT32			iRet, iRet2;

	TRACE("[Reader]	WAVReaderIsValidInfo %d", qwCurFileSize);*/

	return	TRUE;
}



LRSLT FFReaderGetInfo(void* pVoid, FrMediaInfo* hInfo) {
	FFReaderHandle	hReader = (FFReaderHandle)pVoid;
	//FrAudioInfo*	hAudio = &hInfo->FrAudio;
	FrVideoInfo* hVideo = &hInfo->FrVideo;
	/*INT32			iReadByte;
	DWORD32			dwTag, dwSize = 0;
	BOOL			bFmtFound = FALSE;
	*/
	INT32			iRet;
	
	LOG_I("[Reader]	GetInfo() Begin..");

	iRet = avformat_find_stream_info(hReader->_format_ctx, NULL);
	if (iRet) {
		LOG_E("[Reader]	GetInfo - avformat_find_stream_info error..ret(%d)", iRet);
		return COMMON_ERR_FOPEN;
	}
	
	hReader->_stream_index = av_find_best_stream(hReader->_format_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
	if (hReader->_stream_index == -1) {
		LOG_E("[Reader]	GetInfo - avformat_find_stream_info error..ret(%d)", iRet);
		return COMMON_ERR_FOPEN;
	}

	hReader->_bMP4 = FALSE;
	if (hReader->_stream_index >= 0) {
		enum AVCodecID videoCodec = hReader->_format_ctx->streams[hReader->_stream_index]->codecpar->codec_id;
		hVideo->dwConfig = hReader->_format_ctx->streams[hReader->_stream_index]->codecpar->extradata_size;
		memcpy(hVideo->pConfig, hReader->_format_ctx->streams[hReader->_stream_index]->codecpar->extradata, hReader->_format_ctx->streams[hReader->_stream_index]->codecpar->extradata_size);
		hVideo->dwWidth = hReader->_format_ctx->streams[hReader->_stream_index]->codecpar->width;
		hVideo->dwHeight = hReader->_format_ctx->streams[hReader->_stream_index]->codecpar->height;
		int32_t videoFPSNum = hReader->_format_ctx->streams[hReader->_stream_index]->r_frame_rate.num;
		int32_t videoFPSDen = hReader->_format_ctx->streams[hReader->_stream_index]->r_frame_rate.den;
		hVideo->dwFrameRate = (int)(((double)videoFPSNum / videoFPSDen) * 1000);		
		hVideo->dwDuration = ((double)hReader->_format_ctx->streams[hReader->_stream_index]->duration / hReader->_format_ctx->streams[hReader->_stream_index]->time_base.den) * 1000;
		hVideo->dwBitRate = hReader->_format_ctx->streams[hReader->_stream_index]->codec->bit_rate;

		//int32_t videoCodec2 = ESMFFDemuxer::VIDEO_CODEC_T::AVC; // use fourcc
		hReader->_bMP4 = ((videoCodec == AV_CODEC_ID_H264) || (videoCodec == AV_CODEC_ID_HEVC)) && ((!strcmp(hReader->_format_ctx->iformat->long_name, "QuickTime / MOV") ||
			!strcmp(hReader->_format_ctx->iformat->long_name, "FLV (Flash Video)") ||
			!strcmp(hReader->_format_ctx->iformat->long_name, "Matroska / WebM")));
		if (hReader->_bMP4)	{
			const AVBitStreamFilter* bsf = NULL;
			if (videoCodec == AV_CODEC_ID_H264)
				bsf = av_bsf_get_by_name("h264_mp4toannexb");
			else if (videoCodec == AV_CODEC_ID_HEVC)
				bsf = av_bsf_get_by_name("hevc_mp4toannexb");
			if (!bsf) {
				LOG_E("[Reader]	GetInfo - av_bsf_get_by_name() error..");
				return COMMON_ERR_FOPEN;
			}
			av_bsf_alloc(bsf, &hReader->_bsfc);
			hReader->_bsfc->par_in = hReader->_format_ctx->streams[hReader->_stream_index]->codecpar;
			av_bsf_init(hReader->_bsfc);
		}

		switch (videoCodec)
		{
		case AV_CODEC_ID_H264:
			//videoCodec2 = ESMFFDemuxer::VIDEO_CODEC_T::AVC;
			hInfo->FrVideo.dwFourCC = FOURCC_AVC1;
			break;
		case AV_CODEC_ID_HEVC:
			//videoCodec2 = ESMFFDemuxer::VIDEO_CODEC_T::HEVC;
			hInfo->FrVideo.dwFourCC = FOURCC_HEVC;
			break;
		}

		hInfo->dwTotalRange = (DWORD32)hReader->_format_ctx->duration;
		hInfo->dwVideoTotal = 1;
		
		//strcpy(hInfo->m_szFileFormat, "mp4");*/
		
		//if (_front)
		//	_front->OnVideoBegin(videoCodec2, videoExtradata, videoExtradataSize, videoWidth, videoHeight, videoFPS);
	}

	LOG_I("[Reader]	GetInfo() End..");

	return	FR_OK;
}

LRSLT FFReaderReadFrame(void* pVoid, FrMediaStream* pMedia) {
	FFReaderHandle		hReader = (FFReaderHandle)pVoid;
	/*INT32				iReadByte;
	DWORD32				dwFrameSize;*/
	
	int32_t e = 0;
	if (e = av_read_frame(hReader->_format_ctx, &hReader->_packet) >= 0) {
		if (hReader->_packet.stream_index == hReader->_stream_index) {
			if (hReader->_bMP4) {
				if (hReader->_packet_filtered.data)
					av_packet_unref(&hReader->_packet_filtered);
				av_bsf_send_packet(hReader->_bsfc, &hReader->_packet);
				av_bsf_receive_packet(hReader->_bsfc, &hReader->_packet_filtered);
				if (hReader->_packet_filtered.size > 0)	{
					//memcpy(hReader->m_pBuffer, hReader->_packet_filtered.data, hReader->_packet_filtered.size);
					//pMedia->m_pFrame		= hReader->m_pBuffer;
					
					/*double pts = (hReader->_packet_filtered.pts * 90000) * av_q2d(hReader->_format_ctx->streams[hReader->_stream_index]->time_base);
					pts /= 90;*/
					//double pts = (hReader->_packet_filtered.pts * 1000) * av_q2d(hReader->_format_ctx->streams[hReader->_stream_index]->time_base);
					double pts = (hReader->_packet_filtered.pts * 90000) * av_q2d(hReader->_format_ctx->streams[hReader->_stream_index]->time_base);

					pMedia->pFrame = hReader->_packet_filtered.data;
					pMedia->dwFrameNum	= 1;
					pMedia->dwFrameLen[0] = hReader->_packet_filtered.size;
					pMedia->dwCTS = (DWORD32)pts; //hReader->_packet_filtered.pts;
					//pMedia->tFrameType[0] = hReader->_packet_filtered.flags?FRAME_I:FRAME_P;
					if (hReader->_packet_filtered.flags & AV_PKT_FLAG_KEY)
						pMedia->tFrameType[0] = FRAME_I;
					else
						pMedia->tFrameType[0] = FRAME_P;


					LOG_D("ReadFrame() frame length(%d), pts(%d), type(%d), dur(%d), cts(%d)", 
						hReader->_packet_filtered.size, hReader->_packet_filtered.pts, hReader->_packet_filtered.flags, hReader->_packet_filtered.duration, pMedia->dwCTS);
					
					return FR_OK;
				}
			}
			else {
				if (hReader->_packet.size > 0) {
					pMedia->pFrame = hReader->_packet_filtered.data;
					pMedia->dwFrameNum = 1;
					pMedia->dwFrameLen[0] = hReader->_packet_filtered.size;
					pMedia->dwCTS = hReader->_packet_filtered.pts;
					pMedia->tFrameType[0] = hReader->_packet_filtered.flags ? FRAME_I : FRAME_P;

					return FR_OK;
				}
			}
		}
		else {
			return COMMON_ERR_NODATA;
		}
	}

	return	COMMON_ERR_ENDOFDATA;
}

BOOL FFReaderSeekByTime(void* pVoid, DWORD32* dwCTS, BOOL bKeyFrame) {
	//FFReaderHandle	hReader = (FFReaderHandle)pVoid;
	
	return	TRUE;
}

LRSLT FFReaderSetStatus(void* pVoid, INT32 iStatus, INT32 nScale, QWORD qwCurFileSize) {
	//FFReaderHandle	hReader = (FFReaderHandle)pVoid;

	/*if (iStatus == READ_STOP) {
		hReader->m_bStop = TRUE;
		McSeekFile(hReader->m_hInFile, 0, SEEK_SET);
	}
	else if (iStatus == READ_SCALE)
		return	COMMON_ERR_FUNCTION;

	hReader->m_dwCurFileSize = (DWORD32)qwCurFileSize;*/

	return	FR_OK;
}

const FrReader FrFFReader = {
	"FFReader",			// pReaderName;
	FFReaderOpen,			// ReaderOpen
	FFReaderClose,			// ReaderClose
	FFReaderIsValidInfo,	// ReaderIsValidInfo
	FFReaderGetInfo,		// ReaderGetInfo
	NULL,					// ReaderStart
	NULL,					// ReaderMakeIndex
	FFReaderReadFrame,		// ReaderReadFrame
	NULL,	// ReaderSeekByTime
	NULL,					// ReaderSeekByKeyFrame
	NULL,					// ReaderSeekBySubtitle
	NULL,					// ReaderSeekByMedia
	NULL,					// ReaderSeekByClock
	NULL,					// ReaderSetTrackID
	NULL,		// ReaderSetStatus
	NULL,					// ReaderGetStatus
	NULL,					// ReaderGetCurTime
	NULL,					// ReaderSetTotalRange
	NULL,					// ReaderSeekByVideo
	NULL,					// ReaderSetFeedback
	NULL,                   // ReaderGetFeedback
	NULL,					// ReaderSetFactor
	NULL,					// ReaderGetBitrate
	NULL,					// ReaderOpen2
	NULL,					// ReaderSeekPrevFrame
	NULL,					// ReaderSeekNextFrame
	NULL,					// ReaderSetDFSReadSize
};
