#pragma once

#ifdef REMOTE_RTSP_READER
#include "SocketAPI.h"
#include "SystemAPI.h"
#include "TraceAPI.h"
#include "mediainfo.h"
#include "UtilAPI.h"
#include "SdpAPI.h"
#include "RtspAPI.h"
#include "RtpAPI.h"
#include "StreamUtilAPI.h"
#include "DepacketAPI.h"
#include "StreamReader.h"


#define		RTSP_TASK_PRIORITY			THREAD_PRIORITY_NORMAL
//#define		RTP_TASK_PRIORITY			THREAD_PRIORITY_TIME_CRITICAL
#define		RTP_TASK_PRIORITY			THREAD_PRIORITY_HIGHEST
#define		RTCP_TASK_PRIORITY			THREAD_PRIORITY_LOWEST

#define		RTSP_TASK_STACKSIZE			0x2000
#define		RTP_TASK_STACKSIZE			0x1000
#define		RTCP_TASK_STACKSIZE			0x400

#define		RTSP_RECV_TIMEOUT			5000 //10000	// 5 sec => 10 sec
#define		RTP_NO_PRECV_TIMEOUT		5000*10
//#define		RTP_INIT_BUFFER_TIME		2000
#define		RTP_NO_END_PRECV_TIMEOUT	1000
#define		DEFAULT_RTP_INIT_BUFFER_TIME 2000
#define		RTP_MAX_BUFFER_TIME			30000

#define		UDP_FIXED	// mobile youtube..

// on test..
//#define		FILE_DUMP

#ifdef		_DEBUG
//#define		FILE_DUMP
//#define		FRAME_DUMP
#endif

#if	defined(WIN32)
//# define		USE_WAIT_EVENT
#endif

typedef	struct {
	DWORD32				m_dwRange;
	BOOL				m_bComplete;
	WORD				m_wSeq;
	DWORD32				m_dwTime;
} PlayInfo;

typedef	struct {
	DWORD32				m_dwFourCC;
	BOOL				m_bPCM;						// When audio is PCM, the audio is not used to measure bufferfullness
	INT32				m_iStreamType;
	DWORD32				m_dwTrackID;
	BOOL				m_bEnd;
	BOOL				m_bReset;
	LRSLT				m_lError;

	//DtbFECPacketHandle	m_hFEC;                     // When video use FEC

	TASK_HANDLE			m_hRTPRecv;
	BYTE				m_pRTCPBuf[MAX_RTP_PKT_SIZE];

	FrRTPHandle			m_hRTP;
	SOCK_HANDLE			m_hRTPSocket;
	SOCK_HANDLE			m_hRTCPSocket;
	WORD				m_wRTPPort;
	WORD				m_wRTCPPort;
	WORD				m_wRTPRemotePort;
	WORD				m_wRTCPRemotePort;

	FrDepacketHandle	m_hDepacket;

	McBufHandle			m_hRTPBuf;
	DWORD32				m_dwInitBufSize;
	DWORD32				m_dwMaxBufSize;

	BOOL				m_bFirst;
	DWORD32				m_dwBaseCTSCor;
	DWORD32				m_dwCurCTS;
	DWORD32				m_dwBaseCTS;
	DWORD32				m_dwDeltaCTS;
	DWORD32				m_dwRange;
	DWORD32				m_dwLastRtpTick;

	McQueHandle			m_hPlayInfoQue;
	PlayInfo* m_pPlayInfo;
	WORD				m_wSeekSeq;
	BOOL				m_bFirstRTP;
	DWORD32				m_dwSeekTime;

	// jitter
	DWORD32				m_dwPrevTick;
	DWORD32				m_dwJitterDelay;
	DWORD32				m_dwSeqCnt;			// only tcp
	WORD				m_wPrevSeq;			// only tcp
	DWORD32				m_dwPrevRecvTS;		// Timestamp of the previous RTP packet
	BOOL				m_bCheck;
	BOOL				m_bCTS;

	// set to 1 when want No-mutex buffer to be on
	BOOL				m_bRTPBuffer;
	//RTPBufferHandle		m_hRTPBuffer;

	// VoD end condition
	DWORD32* m_pdwReaderTick;
} PSStreamStruct, * PSStreamHandle;

typedef	struct {
	TCHAR* m_szURL;
	char* m_szUserAgent;

	PSStreamStruct		m_Stream[MAX_TRACKS];
	DWORD32				m_dwStream;
	DWORD32				m_dwAudioIdx;
	DWORD32				m_dwVideoIdx;
	DWORD32				m_dwTextIdx;
	DWORD32				m_dwAppIdx;
	BOOL				m_bAudioExist;
	BOOL				m_bVideoExist;
	BOOL				m_bTextExist;
	BOOL				m_bAppExist;
	BOOL				m_bLive;

	// Handle
	FrRTSPHandle		m_hRTSP;
	FrSDPHandle			m_hSDP;

	RTSP_INFO			m_RtspInfo;
	SOCK_HANDLE			m_hRTSPSocket;
	char* m_szRTSPAddr;
	WORD				m_wRTSPPort;
	BOOL				m_bWebSocket;
	BOOL				m_bP2P;		// P2P
	BOOL				m_bP2PTCP;
	BOOL				m_bRelay;
	char* m_pszTrueRelayUrl;

	// buffer
	char				m_pSendBuffer[MAX_RTSP_MSG_SIZE];
	char				m_pRecvBuffer[MAX_RTSP_MSG_SIZE + 1];

	TASK_HANDLE			m_hRTSPRecv;
	TASK_HANDLE			m_hTaskRTCP;

	EVENT_HANDLE        m_hAReadEvent;
	EVENT_HANDLE        m_hVReadEvent;
	EVENT_HANDLE		m_hRecvEvent;
	BOOL				m_bWaitEvent;
	LRSLT				m_lError;
	BOOL				m_bEnd;
	BOOL* m_pbForceEnd;
	BOOL				m_bResumeInTrickMode;

	DWORD32				m_dwTotalBufSize;
	BOOL				m_bSetStatus;
	BOOL				m_bPaused;
	BOOL				m_bSeeked;
	BOOL				m_bDisableNoData;
	DWORD32				m_dwInitBuffTime;
	DWORD32				m_dwSeekBuffTime;
	DWORD32				m_dwBuffTime;
	BOOL				m_bLiveBuffTime;			// set by 1 when different buffer times need to be set for VoD and Live streams
	DWORD32				m_dwInitBuffTimeLive;
	DWORD32				m_dwSeekBuffTimeLive;
	DWORD32				m_dwBuffTimeLive;
	DWORD32				m_dwTick;

	// authentication
	char* m_pszID;
	char* m_pszPasswd;

	
	// set to 1 when want Reorder buffer to be on
	BOOL				m_bReorderBuffer;
	//PSBufferHandle		m_hReorderBuffer;

#ifdef	FILE_DUMP
	FILE_HANDLE		m_hFile;
#endif

#ifdef WIN32
	INT64				m_n64FreqTime;
#endif

} RTSPReaderStruct, * RTSPReaderHandle;

BOOL IS_RTSP_URL(const TCHAR* pURL);

LRSLT RTSPReaderOpen(void** phReader, FrURLInfo* pURL);
void RTSPReaderClose(void* pVoid);
LRSLT RTSPReaderGetInfo(void* pVoid, FrMediaInfo* hInfo);
LRSLT RTSPReaderStart(void* pVoid, DWORD32* pdwStart);
LRSLT RTSPReaderReadFrame(void* pVoid, FrMediaStream* pMedia);
BOOL RTSPReaderSeek(void* pVoid, DWORD32* dwCTS, BOOL bKeyFrame);
//LRSLT PSReaderSeekByClock(void* pVoid, DWORD32 dwDate, DWORD32 dwTime);
//BOOL PSReaderSetTrackID(void* pVoid, DWORD32 dwTrackID, DWORD32 dwCurCTS, INT32 nMediaType);
BOOL RTSPReaderGetStatus(void* pVoid, void* p);
LRSLT RTSPReaderSetStatus(void* pVoid, INT32 iStatus, INT32 nScale, QWORD qwCurFileSize);
//LRSLT PSReaderGetCurTime(void* pVoid, DWORD32* pdwDate, DWORD32* pdwTime);
//BOOL PSReaderGetFeedback(void* pVoid, EVENT_HANDLE* phAEvent, EVENT_HANDLE* phVEvent);
#endif