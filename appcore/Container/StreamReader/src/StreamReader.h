
#ifndef	_STREAMREADER_H_
#define	_STREAMREADER_H_

#include "SystemAPI.h"
#include "TraceAPI.h"
#include "mediainfo.h"
#include "mediafourcc.h"
#include "UtilAPI.h"

#ifdef _DEBUG
//#define	VIDEO_FRAME_DUMP
//#define	AUDIO_FRAME_DUMP
#endif

#ifndef		DTB_MEM_OPTIMIZE
# define		MAX_HEADER_SIZE			1000000			// 0x20000
#else
# define		MAX_HEADER_SIZE			100000			// 0x20000
#endif

typedef	struct {
	void*				m_hReader;
	const char*			m_pReaderName;

	CHAR*				m_pURL;
	FrURLInfo*			m_hURLInfo;
	FILE_HANDLE			m_hInFile;
	QWORD				m_qwFileSize;
	DWORD32				m_dwEnd;
	BYTE*				m_pBuffer;
	DWORD32				m_dwBuffer;
	BOOL				m_bStreaming;
	INT32				m_nStatus;
	MUTEX_HANDLE		m_hLock;
	BOOL				m_bAudioExist;
	BOOL				m_bVideoExist;

#ifdef	AUDIO_FRAME_DUMP
	FILE_HANDLE			m_hAudioFile;
#endif
#ifdef	VIDEO_FRAME_DUMP
	FILE_HANDLE			m_hVideoFile;
#endif

	LRSLT				(*ReaderOpen)(void**, FrURLInfo*);
	void				(*ReaderClose)(void*);
	BOOL				(*ReaderIsValidInfo)(void*,QWORD);
	LRSLT				(*ReaderGetInfo)(void*, FrMediaInfo*);
	LRSLT				(*ReaderStart)(void*, DWORD32*);
	LRSLT				(*ReaderMakeIndex)(void*, QWORD, FrMediaInfo*);
	LRSLT				(*ReaderReadFrame)(void*, FrMediaStream*);
	BOOL				(*ReaderSeekByTime)(void*, DWORD32*, BOOL);
	BOOL				(*ReaderSeekByKeyFrame)(void*, DWORD32*, BOOL);
	BOOL				(*ReaderSeekBySubtitle)(void*, DWORD32*, BOOL);
	BOOL				(*ReaderSeekByMedia)(void*, DWORD32*, FrMediaType);
	LRSLT				(*ReaderSeekByClock)(void*, DWORD32, DWORD32);
	BOOL				(*ReaderSetTrackID)(void*, DWORD32, DWORD32, INT32);
	LRSLT				(*ReaderSetStatus)(void*, INT32, INT32, QWORD);
	BOOL				(*ReaderGetStatus)(void*, void*);
	LRSLT				(*ReaderGetCurTime)(void*, DWORD32*, DWORD32*);
	BOOL				(*ReaderSetTotalRange)(void*, DWORD32);
	BOOL				(*ReaderSeekByVideo)(void*, DWORD32*);
	BOOL				(*ReaderSetFeedback)(void*, WORD, BOOL);
    BOOL                (*ReaderGetFeedback)(void*, EVENT_HANDLE*, EVENT_HANDLE*);
	void				(*ReaderSetFactor)(void*, float, float);
	INT32				(*ReaderGetBitrate)(void*);
	LRSLT				(*ReaderOpen2)(void**, CHAR* pFilename, INT32 nSmiMode);
	BOOL				(*ReaderSeekPrevFrame)(void*, DWORD32*);
	BOOL				(*ReaderSeekNextFrame)(void*, DWORD32*);
	BOOL				(*ReaderSetDFSReadSize)(void*, DWORD32, DWORD32);
} ReaderStruct, *ReaderHandle;

void PrintURLInfo(FrURLInfo* pURL);

typedef struct {
	const char			*pReaderName;
	LRSLT				(*ReaderOpen)(void**, FrURLInfo*);
	void				(*ReaderClose)(void*);
	BOOL				(*ReaderIsValidInfo)(void*,QWORD);
	LRSLT				(*ReaderGetInfo)(void*, FrMediaInfo*);
	LRSLT				(*ReaderStart)(void*, DWORD32*);
	LRSLT				(*ReaderMakeIndex)(void*, QWORD, FrMediaInfo*);
	LRSLT				(*ReaderReadFrame)(void*, FrMediaStream*);
	BOOL				(*ReaderSeekByTime)(void*, DWORD32*, BOOL);
	BOOL				(*ReaderSeekByKeyFrame)(void*, DWORD32*, BOOL);
	BOOL				(*ReaderSeekBySubtitle)(void*, DWORD32*, BOOL);
	BOOL				(*ReaderSeekByMedia)(void*, DWORD32*, FrMediaType);
	LRSLT				(*ReaderSeekByClock)(void*, DWORD32, DWORD32);
	BOOL				(*ReaderSetTrackID)(void*, DWORD32, DWORD32, INT32);
	LRSLT				(*ReaderSetStatus)(void*, INT32, INT32, QWORD);
	BOOL				(*ReaderGetStatus)(void*, void*);
	LRSLT				(*ReaderGetCurTime)(void*, DWORD32*, DWORD32*);
	BOOL				(*ReaderSetTotalRange)(void*, DWORD32);
	BOOL				(*ReaderSeekByVideo)(void*, DWORD32*);
	BOOL				(*ReaderSetFeedback)(void*, WORD, BOOL);
    BOOL                (*ReaderGetFeedback)(void*, EVENT_HANDLE*, EVENT_HANDLE*);
	void				(*ReaderSetFactor)(void*, float, float);
	INT32				(*ReaderGetBitrate)(void*);
	LRSLT				(*ReaderOpen2)(void**, CHAR* pFilename, INT32 nSmiMode);		// SMI Reader open
	BOOL				(*ReaderSeekPrevFrame)(void*, DWORD32*);
	BOOL				(*ReaderSeekNextFrame)(void*, DWORD32*);
	BOOL				(*ReaderSetDFSReadSize)(void*, DWORD32, DWORD32);
} FrReader;


#ifdef __cplusplus
extern "C"
{
#endif

extern BOOL IS_RTSP_URL(const TCHAR* pURL);
extern BOOL IS_M3U_URL(const TCHAR* pUrl, DWORD32 dwUrl);

//////////////////////////////////////////////////////////////////
const extern FrReader FrFFReader;
const extern FrReader FrRTSPReader;
const extern FrReader FrM3UReader;

#ifdef __cplusplus
}
#endif

#endif	// _STREAMREADER_H_
