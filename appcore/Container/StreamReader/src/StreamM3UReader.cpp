/*****************************************************************************
*                                                                            *
*                            StreamReader Library							 *
*                                                                            *
*   Copyright (c) 2014 by DreamToBe, Incoporated. All Rights Reserved.      *
******************************************************************************

    File Name       : StreamM3UReader.c
    Author(s)       : CHANG, Joonho
    Created         : 1 Aug 2014

    Description     : HLS Stream Reader API
    Notes           :

==============================================================================
                      Modification History (Reverse Order)
==============================================================================
    Date        Author      Location (variables) / Description
------------------------------------------------------------------------------

------------------------------------------------------------------------------*/
#include "StreamM3UReader.h"
#include "FrSystem.h"

#if 1

#define	ISSTARTSTRING(c)		( ((c[0]) == _T('#')) && ((c[1]) == _T('E')) && ((c[2]) == _T('X')) && ((c[3]) == _T('T')) )
#define	ISUPPERLETTER(c)		( _T('A') <= (c) && (c) <= _T('Z') )
#define	ISALLOWABLEINM3U(c)		( ISUPPERLETTER(c) || (c) == _T('-') )
#define	ISNOTALLOWEDINURL(c)	( (c[0]) )

static LRSLT GetHttpFile(
	SOCK_HANDLE *hInSocket, const TCHAR *pszUrl, BYTE **ppbFile,
	DWORD32 *pdwFile, BOOL bSegment, M3UReaderHandle hReader);

static VOID UpdateM3U(M3UStreamHandle hStream);

BOOL IS_M3U_URL(const TCHAR *pUrl, DWORD32 dwUrl)
{
	DWORD32		dwPos = 0;
	BOOL		bStreaming = FALSE;
	BYTE		*pDot;

	if (!pUrl || dwUrl<8)
		return	FALSE;
	if (!_tcsncmp(pUrl, _T("http://"), 7))
	{
		bStreaming = TRUE;
		pUrl += 7;
		dwPos += 7;
	}

	while (dwPos + 3 < dwUrl)
	{
		pDot = (BYTE*)_tcschr((char*)pUrl, '.');

		if (pDot)
		{
			pDot ++;
			dwPos += pDot - (BYTE*)pUrl;
			pUrl = (char*)pDot;
			if (!_tcsncmp(pUrl, _T("m3u"), 3))
				return TRUE;
		}
		else
			return FALSE;
	}

	return	FALSE;
}

BOOL IS_M3U(BYTE *pBuf, DWORD32 dwBuf)
{
	if (!strncmp((char*)pBuf, "#EXTM3U",7))
		return TRUE;
	if (strstr((char*)pBuf, "#EXT-X-STREAM-INF:")     ||
		strstr((char*)pBuf, "#EXT-X-TARGETDURATION:") ||
		strstr((char*)pBuf, "#EXT-X-MEDIA-SEQUENCE:"))
		return TRUE;

	return FALSE;
}

#ifndef	STREAM_INPUT_UNSUPPORTED

static BOOL StringStart(const char *pStr, const char *pPfx, const char **pNextstr)
{
    while (*pPfx && *pPfx == *pStr) {
        pPfx++;
        pStr++;
    }
    if (!*pPfx && pNextstr && *pNextstr)
        *pNextstr = pStr;
    return !*pPfx;
}

#define	ISSHARP(c)		( (unsigned) ((c) == _T('#')) )

static char* GetUrl(char* pBuffer, DWORD32 dwBuffer, DWORD32* dwLen)
{
	DWORD32		i, bHttp;

	if (!pBuffer)
		return	NULL;

	bHttp = !_tcsncmp(pBuffer, _T("http://"), 7);

	if (bHttp)
	{
		for (i=0; i<dwBuffer; i++, pBuffer++)
			if ( ISLINEFEED(*pBuffer) || ISRETURN(*pBuffer) || ISSPACE(*pBuffer) || ISCOMMA(*pBuffer) || ISSHARP(*pBuffer) || !*pBuffer)
				break;
	}
	else
	{
		for (i=0; i<dwBuffer; i++, pBuffer++)
			if ( ISLINEFEED(*pBuffer) || ISRETURN(*pBuffer) || ISCOMMA(*pBuffer) ||  ISSHARP(*pBuffer) || !*pBuffer)
				break;
	}

	if (dwLen)
		*dwLen = i;

	return	pBuffer;
}

static char* GetString(char* pBuffer, DWORD32 dwBuffer, DWORD32* dwLen)
{
	DWORD32		i;

	if (!pBuffer)
		return	NULL;

	for (i=0; i<dwBuffer; i++, pBuffer++)
	{
		if ( !ISCHAR(*pBuffer) )		// ISSAFE ???. To Fix
			break;
	}

	if (dwLen)
		*dwLen = i;

	return	pBuffer;
}

// Search LF or RETURN up to non-LF and non-return character after LFs or Returns
static char* GetLine(char* pBuffer, DWORD32 dwBuffer, DWORD32* dwLen)
{
	DWORD32		i;
	BOOL		bStartWidthLine = FALSE;

	if (!pBuffer)
		return	NULL;

	for (i=0; i<dwBuffer; i++, pBuffer++)
	{
		if ( ISLINEFEED(*pBuffer) || ISRETURN(*pBuffer) )
			break;
	}

	if (i==0)
		bStartWidthLine = TRUE;

	for (; i<dwBuffer; i++, pBuffer++)
	{
		if ( ISLINEFEED(*pBuffer) || ISRETURN(*pBuffer) )
			continue;
		else
			break;
	}

	if (dwLen)
		*dwLen = i;

	if (bStartWidthLine)
		return NULL;
	else
		return	pBuffer;
}

static char* GetM3UAttribute(char* pBuffer, DWORD32 dwBuffer, DWORD32* dwLen)
{
	DWORD32	i;

	*dwLen = 0;

	if (!pBuffer)
		return	NULL;

	for (i=0; i<dwBuffer; i++, pBuffer++)
		if (!ISALLOWABLEINM3U(*pBuffer))
			break;

	*dwLen =i;

	return	pBuffer;
}

char* GetM3UDecNumber(char* pBuffer, DWORD32 dwBuffer, DWORD32 *dwDecLen, DWORD32* dwNum)
{
	char	*pStr=pBuffer;
	char	strNum[20];
	DWORD32	i;

	*dwDecLen = 0;
	if (!pStr)
		return	NULL;

	for (i=0; i<dwBuffer; i++, pStr++)
	{
		if (!ISDIGIT(*pStr))
			break;
	}

	if (i==0)
	{
		TRACE("[M3UReader] GetM3UDecNumber - Decimal number does not start with digit (%c)", *pBuffer);
		return	NULL;
	}

	if (dwDecLen)
		*dwDecLen = i;
	memcpy(strNum, pBuffer, i);
	strNum[i] = '\0';

	if (dwNum)
		*dwNum = ATOI(strNum);

	return	pStr;
}

static DWORD32 ReverseSearch(BYTE *pszBuffer, DWORD32 dwData, BYTE bC)
{
	BYTE	*pPtr = pszBuffer + dwData;

	if (!pszBuffer)
		return 0;

	while(dwData--)
		if (*--pPtr== bC)
			return dwData;

	return 0;
}

#define	SEGMENT_INIT_NUMBER		4			// 3 is most common for Live service
static VOID MakeAbsoluteUrl(BYTE **ppDestUrl, BYTE *pszUpperUrl, DWORD32 dwUpperUrl, BYTE *pszLowerUrl, DWORD32 dwLowerUrl)
{
	DWORD32		dwDirectory = 0, dwPos;
	BYTE		*pszFound=NULL, *pszEnd=NULL;

	if (pszLowerUrl[0] != '.')
	{
		if ( !strncmp((char*)pszLowerUrl, "http", 4) ||			// Absoulute url of http download
			  pszLowerUrl[1] == ':')							// Absoulute url of local files C:xxx
		{
			*ppDestUrl = (BYTE*)MALLOCZ(dwLowerUrl+1);
			if (*ppDestUrl)
				strncpy((char*)*ppDestUrl, (char*)pszLowerUrl, dwLowerUrl);
			return;
		}
		else if (pszLowerUrl[0] == '/')							// root relative url
		{
			DWORD32		dwLen;

			pszFound = (BYTE*)UtilStrFindString((char*)pszUpperUrl+10, (char*)"/");
			if (pszFound)
			{
				dwLen = (DWORD32)(pszFound - pszUpperUrl);
				*ppDestUrl = (BYTE*)MALLOCZ(dwLen + dwLowerUrl+1);
				if (*ppDestUrl)
				{
					strncpy((char*)*ppDestUrl, (char*)pszUpperUrl, dwLen);
					strncpy((char*)*ppDestUrl+dwLen, (char*)pszLowerUrl, dwLowerUrl);
				}
			}
			else
			{
				if (*ppDestUrl)
					*ppDestUrl = NULL;
			}
			return;
		}
	}

	pszFound = pszEnd = pszLowerUrl;
	if (!strncmp((char*)pszFound, "./", 2) ||
		!strncmp((char*)pszFound, ".\\", 2))
	{
		pszFound += 2;
		dwLowerUrl -= 2;
	}

	while(1)
	{
		pszFound = (BYTE*)UtilStrFindString((char*)pszFound, (char*)"..");
		if (!pszFound)
			break;
		pszFound += 3;
		dwLowerUrl -= pszFound - pszEnd;
		pszEnd = pszFound;
		dwDirectory++;
	}

	dwPos = dwUpperUrl;

	while(dwDirectory--)
	{
		dwPos = ReverseSearch(pszUpperUrl, dwUpperUrl, '/');
		if (!dwPos)
			dwPos = ReverseSearch(pszUpperUrl, dwUpperUrl, '\\');
		if (!dwPos)
			break;
		dwUpperUrl=dwPos;
	}

	if (!dwPos && dwDirectory != (DWORD32)-1)
	{
		*ppDestUrl = (BYTE*)MALLOCZ(dwLowerUrl+1);
		if (*ppDestUrl)
			strncpy((char*)*ppDestUrl, (char*)pszLowerUrl, dwLowerUrl);
		return;
	}
	else
	{
		*ppDestUrl = (BYTE*)MALLOCZ(dwUpperUrl + dwLowerUrl+2);
		strncpy((char*)*ppDestUrl, (char*)pszUpperUrl, dwUpperUrl + 1);
		strncpy((char*)*ppDestUrl+dwUpperUrl + 1, (char*)pszEnd, dwLowerUrl);
	}
}

static BOOL addSegment(M3UStreamHandle hStream, BYTE *pUrl, DWORD32 dwSeqNumber, DWORD32 dwDuration)
{
	M3USegmentHandle	hSegment;
	DWORD32				dwStartCTS = 0;

	// new
	if (!hStream->m_bLive)
	{
		if (!hStream->m_dwSegmentAlloced)
		{
			hStream->m_phSegment = (M3USegmentStruct **)MALLOCZ(sizeof(M3USegmentStruct*) * SEGMENT_INIT_NUMBER);
			if (hStream->m_phSegment)
				hStream->m_dwSegmentAlloced = SEGMENT_INIT_NUMBER;
		}
		else if (hStream->m_dwSegmentAlloced <=  hStream->m_dwSegmentUsedEnd)
		{
			DWORD32			i;
			hStream->m_phSegment = (M3USegmentStruct **)REALLOC(hStream->m_phSegment, sizeof(M3USegmentStruct*) * hStream->m_dwSegmentAlloced * 2);
			if (hStream->m_phSegment)
				hStream->m_dwSegmentAlloced *= 2;

			// Init
			for(i=hStream->m_dwSegmentUsedEnd; i<hStream->m_dwSegmentAlloced; i++)
				hStream->m_phSegment[i] = NULL;
		}

		if (!hStream->m_dwSegmentUsedEnd)
			dwStartCTS = 0;
		else
			dwStartCTS = hStream->m_phSegment[hStream->m_dwSegmentUsedEnd-1]->m_dwStartCTS
				+ hStream->m_phSegment[hStream->m_dwSegmentUsedEnd-1]->m_dwDuration;

		hStream->m_phSegment[hStream->m_dwSegmentUsedEnd] = (M3USegmentStruct *)MALLOCZ(sizeof(M3USegmentStruct));
		hSegment = hStream->m_phSegment[hStream->m_dwSegmentUsedEnd];
		if (hSegment && pUrl)
		{
			DWORD32			dwLineLen;
			CHAR			*pEndLine;

			pEndLine = GetUrl((char*)pUrl, strnlen((char*)pUrl, MAX_SEG_URL_LENGTH), &dwLineLen);
			MakeAbsoluteUrl(&hSegment->m_pszUrl, &hStream->m_szUrl[0], hStream->m_dwUrl, pUrl, dwLineLen);
		}

		if (!hSegment || !hSegment->m_pszUrl)
			return FALSE;

		// we have to make a full url using Baseurl ... FIXME Later
		hSegment->m_dwSeqNumber = dwSeqNumber;
		hSegment->m_dwDuration = dwDuration;
		hSegment->m_dwStartCTS = dwStartCTS;

		hStream->m_dwLastSeqNumber = dwSeqNumber;
		hStream->m_dwSegmentUsedEnd++;
	}
	// Segment buffer expanded already at get_info() in case of live
	else
	{
		DWORD32		dwNext;

		if (dwSeqNumber <= hStream->m_dwLastSeqNumber)
			return TRUE;

		dwNext = hStream->m_dwSegmentUsedEnd + 1;
		if (hStream->m_dwSegmentAlloced <= dwNext)
			dwNext = 0;

		if (dwNext == hStream->m_dwCurSegment)
			return FALSE;

		hSegment = hStream->m_phSegment[hStream->m_dwSegmentUsedEnd];
		if (hSegment && pUrl)
		{
			DWORD32			dwLineLen;
			CHAR			*pEndLine;

			// FREE
			if (hSegment->m_pszUrl)
				FREE(hSegment->m_pszUrl);
			hSegment->m_pszUrl = NULL;

			// Make absolute url
			pEndLine = GetUrl((char*)pUrl, strnlen((char*)pUrl, MAX_SEG_URL_LENGTH), &dwLineLen);
			MakeAbsoluteUrl(&hSegment->m_pszUrl, &hStream->m_szUrl[0], hStream->m_dwUrl, pUrl, dwLineLen);
			if (!hSegment->m_pszUrl)
				return FALSE;
		}

		// we have to make a full url using Baseurl ... FIXME Later
		hSegment->m_dwSeqNumber = dwSeqNumber;
		hSegment->m_dwDuration = dwDuration;
		hSegment->m_dwStartCTS = dwStartCTS;

		hStream->m_dwLastSeqNumber = dwSeqNumber;
		hStream->m_dwSegmentUsedEnd = dwNext;

		// TRACE("[M3UReader] hStream 0x%x addSegment UsedEnd %d", hStream, hStream->m_dwSegmentUsedEnd);
	}

	return TRUE;
}

static BOOL addStream(M3UReaderHandle hReader, M3UStreamHandle *phStream, BYTE *pUrl, BYTE *pBase)
{
	M3UStreamStruct *hStream;

	// new
	if (!hReader->m_dwStream)
		hReader->m_phStream = (M3UStreamStruct **)MALLOCZ(sizeof(M3UStreamStruct*));
	else
		hReader->m_phStream = (M3UStreamStruct **)REALLOC(hReader->m_phStream, sizeof(M3UStreamStruct*) * (hReader->m_dwStream+1));

	// alloc a new stream
	hReader->m_phStream[hReader->m_dwStream] = (M3UStreamStruct *)MALLOCZ(sizeof(M3UStreamStruct));
	if (!hReader->m_phStream || !hReader->m_phStream[hReader->m_dwStream])
		return FALSE;

	hStream = hReader->m_phStream[hReader->m_dwStream];
	if (pUrl)
	{
		DWORD32			dwLineLen;
		CHAR			*pEndLine, *pNewUrl;

		// Add base url for stream url if pUrl is relative
		if (pBase)
		{
			DWORD32		dwDirectoryEndPos = ReverseSearch(pBase, strlen((char*)pBase), '/');
			MakeAbsoluteUrl((BYTE**)&pNewUrl, pBase, dwDirectoryEndPos, pUrl, strlen((char*)pUrl));
			if (!pNewUrl)
				pNewUrl = (char*)pUrl;
		}
		else
			pNewUrl = (char*)pUrl;

		pEndLine = GetUrl((char*)pNewUrl, strnlen((char*)pNewUrl, MAX_SEG_URL_LENGTH), &dwLineLen);

		strncpy((char*)&hStream->m_szUrl[0], (char*)pNewUrl, dwLineLen);

		hStream->m_dwUrl = ReverseSearch(&hStream->m_szUrl[0], dwLineLen, '/');
		if (!hStream->m_dwUrl)
			hStream->m_dwUrl = ReverseSearch(&hStream->m_szUrl[0], dwLineLen, '\\');

		if (pNewUrl != (char*)pUrl)
			FREE(pNewUrl);
	}

	// we have to make a full url using Baseurl ... FIXME Later
	hStream->m_hParentReader = (VOID*)hReader;
	hStream->m_hUpdateM3UTask = INVALID_TASK;
	hStream->m_dwCurSeqNumber = (DWORD32)-1;
	if (phStream)
		*phStream = hStream;
	hReader->m_dwStream++;

	return TRUE;
}

static LRSLT expandSegmentList(M3UReaderHandle hReader)
{
	M3UStreamHandle		hStream;
	DWORD32				i, j, dwList;

	if (!hReader->m_bLive)
		return FR_OK;

	for (i=0; i<hReader->m_dwStream; i++)
	{
		hStream = hReader->m_phStream[i];
		dwList =  ((INT32)hStream->m_dwSegmentUsedEnd < MAX_M3U_LIVE_SEGMENT/2) ? MAX_M3U_LIVE_SEGMENT : hStream->m_dwSegmentUsedEnd*2;
		hStream->m_phSegment = (M3USegmentStruct **)REALLOC(hStream->m_phSegment, sizeof(M3USegmentStruct*) * dwList);
		if (!hStream->m_phSegment)
		{
			TRACE("[M3UReader] expandSegmentList memory alloc failed (%d)", dwList);
			return COMMON_ERR_MEM;
		}
		hStream->m_dwSegmentAlloced = dwList;

		for (j=hStream->m_dwSegmentUsedEnd; j<hStream->m_dwSegmentAlloced; j++)
		{
			hStream->m_phSegment[j] = (M3USegmentStruct *)MALLOCZ(sizeof(M3USegmentStruct));
			if (!hStream->m_phSegment[j])
			{
				TRACE("[M3UReader] expandSegmentList memory alloc failed");
				return COMMON_ERR_MEM;
			}
		}
	}

	return FR_OK;
}

static BOOL extractKeyValue(const BYTE *pString, DWORD32 dwString, const BYTE **ppKey, const BYTE **ppValue, const BYTE **ppEnd)
{
	const BYTE	*pPtr = pString;

	*ppKey = NULL;
	*ppValue = NULL;
	*ppEnd = NULL;

	// End of Line
	if (ISRETURN(*pPtr) || ISLINEFEED(*pPtr) )
		return FALSE;

	// Search key after stripping Spaces or a Comma
	while(*pPtr && (ISSPACE(*pPtr)) || ISCOMMA(*pPtr))
		pPtr++;
	if (!*pPtr)		// end of string
		return FALSE;
	*ppKey = pPtr;
	// Search value
	if (!(pPtr = (BYTE*)strchr((char*)*ppKey, '=')))
        return FALSE;
	// Search Next key
	*ppValue = ++pPtr;

	while(*pPtr)
	{
		// New Line
		if ( ISRETURN(*pPtr) || ISLINEFEED(*pPtr) )
			break;
		// New
		if ( ISSPACE(*pPtr) || ISCOMMA(*pPtr) )
		{
			pPtr++;
			break;
		}
		pPtr++;
		continue;
	}
	*ppEnd = pPtr;

	return (*ppKey && *ppValue);
}

static VOID UpdateSeqInfo(M3UStreamHandle hCurStream, DWORD32 dwSequence, DWORD32 dwBandwidth, DWORD32 dwTargetDuration)
{
	if (dwTargetDuration)
		hCurStream->m_dwTargetDuration = dwTargetDuration;
	if (dwBandwidth)
		hCurStream->m_dwBandwidth = dwBandwidth;
	if (!hCurStream->m_dwAvgSegment && hCurStream->m_dwTargetDuration && hCurStream->m_dwBandwidth)
	{
		// Initially estimated target size
		hCurStream->m_dwAvgSegment = (DWORD32)(hCurStream->m_dwBandwidth * 1.3)  /8 * hCurStream->m_dwTargetDuration;
		hCurStream->m_dwAvgSegment /= 1000;			// in seconds
	}
}

static LRSLT parseM3U(M3UReaderHandle hReader, M3UStreamHandle hCurStream, BYTE *pM3UBuffer, DWORD32 dwM3UBuffer, TCHAR *pBaseUrl)
{
	DWORD32			dwContent, dwLineLen;
	BYTE			*pNewLine=(BYTE*)NULL,
					*pNextLine=(BYTE*)NULL,
					*pContent=(BYTE*)NULL,
					*pKey=(BYTE*)NULL,
					*pValue=(BYTE*)NULL;
	BOOL			bSegment=FALSE, bStream=FALSE, bSequence=FALSE, bFirst=TRUE;
	DWORD32			dwDuration=0, dwBandwidth=0, dwTargetDuration=0, dwSequence=-1, dwSegment=0;
	M3UStreamHandle hStream=NULL;
	BOOL			bLive = TRUE;

	dwContent = dwM3UBuffer;
	pContent = pM3UBuffer;

	if (!pContent || !dwM3UBuffer)
		return FR_OK;

	if (hCurStream)
		TRACE("[M3UReader] parseM3U Starts Seq %d Seg %d", hCurStream->m_dwCurSeqNumber, hCurStream->m_dwCurSegment);

	// Parse 1st line
	if(!StringStart((char*)pContent, "#EXTM3U", (const char**)&pNewLine))
	{
		TRACE("[M3UReader] parseM3U - do not start with %s", "#EXTM3U");
		return	COMMON_ERR_STREAMSYNTAX;
	}

	pNewLine = pContent;
	pNextLine = pNewLine;
	while (0 < (INT32)dwContent)
	{
		// Get a line
		pNewLine = (BYTE*)GetLine((char*)pNextLine, dwContent, &dwLineLen);
		if (!pNewLine)
			break;
		dwContent -= dwLineLen;			// Remaining
		pNextLine = pNewLine;

		if ( StringStart((char*)pNewLine, "#EXT-X-STREAM-INF:", (const char**)&pNewLine) )
		{
			BYTE	*pPtr = pNewLine;

			if (hCurStream)
			{
				TRACE("[M3UReader] parseM3U - The current m3u must not be a master");
				return COMMON_ERR_STREAMSYNTAX;
			}

			// master m3u
			bStream = TRUE;
			bLive = FALSE;
			while(pPtr)
			{
				if (!extractKeyValue(pPtr, dwContent, (const BYTE**)&pKey, (const BYTE**)&pValue, (const BYTE**)&pPtr))
					break;
				if (!strncmp((char*)pKey, "BANDWIDTH", 9))
					dwBandwidth = atoi((char*)pValue);
			}
		}
		else if ( StringStart((char*)pNewLine, "#EXT-X-TARGETDURATION:", (const char**)&pNewLine) )
		{
			dwTargetDuration = (DWORD32)(atoi((char*)pNewLine)*1000);
		}
		else if ( StringStart((char*)pNewLine, "#EXT-X-MEDIA-SEQUENCE:", (const char**)&pNewLine) )
		{
			bSequence = TRUE;
			dwSequence = (DWORD32)(atoi((char*)pNewLine));

			// No update
			if (hCurStream && hCurStream->m_bLive)
			{
				// TRACE("[M3UReader] parseM3U - Sequence Number %d (Last SeqNum %d)", dwSequence, hCurStream->m_dwCurSeqNumber);

				// Sequence number of the m3u playlist read last time in live service
				if(hCurStream->m_dwCurSeqNumber != (DWORD32)-1 &&
				   dwSequence <= hCurStream->m_dwCurSeqNumber &&
				   hCurStream->m_dwCurSeqNumber <= dwSequence + MIN_ALLOWED_SEQUENCE_NUM_DIFF)
					return COMMON_ERR_PRECONDITION;
				hCurStream->m_dwCurSeqNumber = dwSequence;
				bSequence = FALSE;
				//hCurStream->m_dwLastUpdateTick = McGetTickCount();
				//TRACE("[Reader] parseM3U - update CurSeqNumber %d", dwSequence);
			}
			// To set StartSeqNumber of the stream
			else if (hCurStream)
				hCurStream->m_dwCurSeqNumber = dwSequence;
		}
		else if ( StringStart((char*)pNewLine, "#EXT-X-KEY:", (const char**)&pNewLine) )
		{
		}
		else if ( StringStart((char*)pNewLine, "#EXT-X-MEDIA:", (const char**)&pNewLine) )
		{
		}
		else if ( StringStart((char*)pNewLine, "#EXT-X-PLAYLIST-TYPE:", (const char**)&pNewLine) )
		{
		}
		else if ( StringStart((char*)pNewLine, "#EXT-X-ENDLIST", (const char**)&pNewLine) )
		{
			bLive = FALSE;
		}
		else if ( StringStart((char*)pNewLine, "#EXTINF:", (const char**)&pNewLine) )
		{
			// Get Duration
			bSegment = TRUE;
			dwDuration = (DWORD32)(atof((char*)pNewLine)*1000);
			if (hStream)
			{
				TRACE("[M3UReader] parseM3U - The current m3u must not have a segment since it's a master");
				return COMMON_ERR_STREAMSYNTAX;
			}
		}
		else if ( StringStart((char*)pNewLine, "#", NULL) )
			continue;
		else if (pNewLine[0])
		{
			if (bStream)
			{
				// when parsing a leaf m3u
				if (hCurStream)
					continue;
				if (!addStream(hReader, &hStream, pNewLine, (BYTE*)pBaseUrl))
					return COMMON_ERR_MEM;
				UpdateSeqInfo(hStream, dwSequence, dwBandwidth, dwTargetDuration);
				bStream = FALSE;
			}
			// This should be ahead of bSegment, because dwSequence increases by 1 for each segment
			if (bSequence)
				bSequence = FALSE;
			if (bSegment)
			{
				if (!hCurStream)
				{
					if (!addStream(hReader, &hCurStream, (BYTE*)hReader->m_pszURL, NULL))
						return COMMON_ERR_MEM;
				}
				// First Segment for VoD
				if (bFirst && !hCurStream->m_bLive)
				{
					bFirst = FALSE;
					UpdateSeqInfo(hCurStream, dwSequence, dwBandwidth, dwTargetDuration);
				}

				//TRACE("[M3UReader] addStream - Cur Seg %d Used End Seg %d", hCurStream->m_dwCurSegment, hCurStream->m_dwSegmentUsedEnd);
				addSegment(hCurStream, pNewLine, dwSequence, dwDuration);
				// First Segment for Live update
				if (bFirst && hCurStream->m_bLive)
				{
					bFirst = FALSE;
					UpdateSeqInfo(hCurStream, dwSequence, dwBandwidth, dwTargetDuration);
				}
				dwSequence++;				// automatic increase
				bSegment = FALSE;
				dwSegment++;				// get the number of segment in the m3u to guess max buffering time for live
			}
		}
	}

	hReader->m_bLive = bLive;
	if (hStream)
		hStream->m_bLive = bLive;
	if (hCurStream)
	{
		hCurStream->m_bLive = bLive;
		if (!hCurStream->m_dwSegNumInLiveService)
			hCurStream->m_dwSegNumInLiveService = dwSegment;
		if (!bLive)
		{
			// No EXT-X-MEDIA-SEQUENCE
			if (hCurStream->m_phSegment[0]->m_dwSeqNumber != 0)
			{
				DWORD32		i;
				for (i=0; i<hCurStream->m_dwSegmentUsedEnd;i++)
					hCurStream->m_phSegment[i]->m_dwSeqNumber = i;
				hCurStream->m_dwCurSeqNumber = 0;
				hCurStream->m_dwLastSeqNumber = hCurStream->m_dwSegmentUsedEnd?hCurStream->m_dwSegmentUsedEnd-1:0;
			}
		}
	}

	if (hCurStream)
		TRACE("[M3UReader] parseM3U End Seq %d Seg %d", hCurStream->m_dwCurSeqNumber, hCurStream->m_dwCurSegment);

	return	FR_OK;
}

#define	GET_FILE_ERROR_CHECK		100

static LRSLT GetFile(M3UReaderHandle hReader, const TCHAR *pUrl, FrURLInfo *pURL, BYTE **ppBuffer, DWORD32 *pdwBuffer, BOOL bSegment)
{
	LRSLT			lRet;
	DWORD32			dwFileSize, dwErrorCheck=0, dwPrevCurSize=0;
	FILE_HANDLE		hM3UFile;

	if (!hReader || !pUrl || !pURL || !ppBuffer || !pdwBuffer)
		return COMMON_ERR_NULLPOINTER;

	// http
	if ( (!_tcsncmp(pUrl, _T("http://"), 7)) || (!_tcsncmp(pUrl, _T("https://"), 8)) )
	{
		if (bSegment)
			lRet = GetHttpFile(&hReader->m_hSegSock, pUrl, ppBuffer, pdwBuffer, bSegment, hReader);
		else
			lRet = GetHttpFile(NULL, pUrl, ppBuffer, pdwBuffer, bSegment, hReader);

		if (FRFAILED(lRet))
		{
			TRACE("[M3UReader] GetFile - Http open failed 0x%x (url %s)", lRet, pUrl);
			return lRet;
		}

#if 0
	if (1)
	{
		char localname[3000];
		FILE *fp;
		char *fname = (BYTE*)UtilURLGetFilename((char*)pUrl);
		sprintf(localname, "C:\\Temp\\%s", fname);

		fp = fopen(localname, "wb");
		fwrite(*ppBuffer, 1, *pdwBuffer, fp);
		fclose(fp);
	}
#endif

	}
	// local file
	else
	{
		std::string name(pUrl);
		fr::File f;
		f.Open(name, fr::File::READ_MODE);
		
		dwFileSize = fr::GetSizeOfFile(name);
		*ppBuffer = (BYTE *)MALLOCZ(dwFileSize+188);
		if (*ppBuffer==NULL)
		{
			TRACE("[M3UReader] M3U - m3u file memory alloc failed");
			f.Close();
			*pdwBuffer = 0;
			return COMMON_ERR_MEM;
		}

		//McReadFile(hM3UFile, *ppBuffer, dwFileSize);
		//McCloseFile(hM3UFile);
		f.Read((char*)*ppBuffer, dwFileSize);
		f.Close();
		*pdwBuffer = dwFileSize;
	}

	TRACE("[M3UReader] GetFile ended");
	return FR_OK;
}


static VOID EmptyOneBuffer(M3UBufferHandle hBuffer, M3UReaderHandle hReader)
{
	if (hBuffer->m_pBuffer)
		FREE(hBuffer->m_pBuffer);
	hBuffer->m_pBuffer = NULL;
	FrEnterMutex(&hReader->m_hMutex);
	if (hBuffer->m_hSegReader)
		FrReaderClose(hBuffer->m_hSegReader);
	hBuffer->m_hSegReader = NULL;
	FrLeaveMutex(&hReader->m_hMutex);
	memset(hBuffer, 0, sizeof(M3UBuffer));
}

static VOID UpdateBandwidthEstimated(M3UReaderHandle hReader, DWORD32 dwBE)
{
	// estimated bandwidth
	hReader->m_dwLastBWEstimation = dwBE;
	hReader->m_BandwithEstimated[hReader->m_dwBEIndex] = dwBE;

	hReader->m_dwBENumber++;
	if (MAX_M3U_BUFFER_NUM <= hReader->m_dwBENumber)
		hReader->m_dwBENumber = MAX_M3U_BUFFER_NUM;
	hReader->m_dwBEIndex++;
	if (MAX_M3U_BUFFER_NUM <= hReader->m_dwBEIndex)
		hReader->m_dwBEIndex = 0;
}

static VOID EmptyOneBufferAfterBeingUsed(M3UBufferHandle hBuffer, M3UReaderHandle hReader)
{
	// When both audio and video are read
	if ( (!hBuffer->m_bVideo || hBuffer->m_eVideoRead == READ_END) &&
		 (!hBuffer->m_bAudio || hBuffer->m_eAudioRead == READ_END) )
		EmptyOneBuffer(hBuffer, hReader);
}

static VOID EmptyBuffers(M3UReaderHandle hReader)
{
	DWORD32		i;

	for(i=0; i<MAX_M3U_BUFFER_NUM; i++)
	{
		FrEnterMutex(&hReader->m_hMutex);
		EmptyOneBuffer(&hReader->m_Buffer[i], hReader);
		FrLeaveMutex(&hReader->m_hMutex);
		hReader->m_BandwithEstimated[i] = 0;
	}
	// bandwidth buffer
	hReader->m_dwBENumber = 0;
	hReader->m_dwBEIndex = 0;

	hReader->m_dwBufferRatio = 0;
	hReader->m_dwCurAudioReadBuffer = hReader->m_dwCurVideoReadBuffer = 0;
	hReader->m_dwReadBuffer = hReader->m_dwWriteBuffer = 0;
}

// Keep the current Buffer to make CTS continous
static VOID EmptyBuffersExceptCurrentOne(M3UReaderHandle hReader)
{
	DWORD32		i, dwEndBuffer = (DWORD32)-1;

	for(i=0; i<MAX_M3U_BUFFER_NUM; i++)
	{
		if (i == hReader->m_dwCurAudioReadBuffer ||
			i == hReader->m_dwCurVideoReadBuffer )
		{
			if (dwEndBuffer == (DWORD32)-1)
				dwEndBuffer = i;
			else
				if (i != MAX_M3U_BUFFER_NUM-1)
					dwEndBuffer = i;
			continue;
		}
		EmptyOneBuffer(&hReader->m_Buffer[i], hReader);
		hReader->m_BandwithEstimated[i] = 0;
	}
	// bandwidth buffer
	hReader->m_dwBENumber = 0;
	hReader->m_dwBEIndex = 0;

	hReader->m_dwBufferRatio = 0;
	dwEndBuffer++;
	hReader->m_dwWriteBuffer = (dwEndBuffer == MAX_M3U_BUFFER_NUM) ? 0 : dwEndBuffer;
}

static VOID TaskGetOneSegment(VOID* lpVoid)
{
	M3UReaderHandle		hReader = (M3UReaderHandle)lpVoid;
	M3UStreamHandle		hStream = hReader->m_phStream[hReader->m_dwCurStream];
	M3USegmentHandle	hSegment = hStream->m_phSegment[hStream->m_dwCurSegment];
	M3UBufferHandle		hBuffer = &hReader->m_Buffer[hReader->m_dwWriteBuffer];
	BYTE				*pFile=NULL;
	DWORD32				dwFile=0, dwNext=0, dwTick1, dwTick2;
	BOOL				bSuccess = FALSE;
	//TASK_HANDLE			hMyTask;

	//TRACE("[M3UReader] TaskGetOneSegment Starts Stream %d", hReader->m_dwCurStream);

	//TRACE("[M3UReader] TaskGetOneSegment Starts Stack Address %x => %x", &hReader, &bSuccess);

#ifdef	TaskGetOneSegment
	if (!hReader->m_bFunction_GetOneSegment)
	{
		while(hReader->m_hGetOneSegmentTask == INVALID_TASK)
			McSleep(10);
		hMyTask = hReader->m_hGetOneSegmentTask;
	}
#endif

	if (hStream->m_bLive)
	{
		// No segment unread
		if (hStream->m_dwCurSegment == hStream->m_dwSegmentUsedEnd)
		{
			//TRACE("[M3UReader] TaskGetOneSegment - no more segment left unread Cur %d End %d",hStream->m_dwCurSegment, hStream->m_dwSegmentUsedEnd );
			goto end;
		}

		dwNext = hStream->m_dwCurSegment + 1;
		if (hStream->m_dwSegmentAlloced <= dwNext)
			dwNext = 0;
	}
	else
	{
		if ((INT32)hStream->m_dwSegmentUsedEnd <= (INT32)hStream->m_dwCurSegment)
		{
			TRACE("[M3UReader] TaskGetOneSegment - no more segment left unread");
			FrSleep(100);
			goto end;
		}
	}

	dwTick1 = FrGetTickCount();
	hReader->m_lGetOneSegmentError = GetFile(hReader, (char*)hSegment->m_pszUrl, &hReader->m_URL, &pFile, &dwFile, TRUE);
	if (hReader->m_dwDuration)
	{
		hReader->m_dwRemainTime =
			(hSegment->m_dwStartCTS < hReader->m_dwDuration) ? hReader->m_dwDuration - hSegment->m_dwStartCTS : 1;
	}
	else
		hReader->m_dwRemainTime = MAX(hSegment->m_dwDuration, MIN_M3U_BUFFER_TIME);
	TRACE("[M3UReader] TaskGetOneSegment - Duration %d Start CTS %d", hReader->m_dwDuration, hSegment->m_dwStartCTS);

	dwTick2 = FrGetTickCount();
	dwTick2 = ABS(dwTick2 - dwTick1) + 1;

	if (FRFAILED(hReader->m_lGetOneSegmentError))
	{
		// forced to change to bottom
		if (hReader->m_lGetOneSegmentError == COMMON_ERR_CANCEL)
		{
			TRACE("[M3UReader] TaskGetOneSegment stopped externally");
			return;
		}

		if (hReader->m_lGetOneSegmentError == FR_FAIL)
			return;

		hReader->m_dwCurSeqNumber = hSegment->m_dwSeqNumber;
		if (hReader->m_lGetOneSegmentError == COMMON_ERR_BADREQUEST ||
			hReader->m_lGetOneSegmentError == COMMON_ERR_FOPEN)
		{
			// move to next segment
			if (hReader->m_bLive)
				hStream->m_dwCurSegment = dwNext;
			else
			{
				hStream->m_dwCurSegment++;
				hBuffer->m_bLastSegment = hStream->m_dwSegmentUsedEnd <= hStream->m_dwCurSegment;
			}
		}
		return;
	}

	hReader->m_dwCurSeqNumber = hSegment->m_dwSeqNumber;
	// move to next segment
	if (hReader->m_bLive)
		hStream->m_dwCurSegment = dwNext;
	else
	{
		hStream->m_dwCurSegment++;
		hBuffer->m_bLastSegment = hStream->m_dwSegmentUsedEnd <= hStream->m_dwCurSegment;
	}

	// Skip error segment
	if (hReader->m_lGetOneSegmentError != FR_OK)
	{
		TRACE("[M3UReader] TaskGetOneSegment - GetFile Failed (%x)", hReader->m_lGetOneSegmentError);
		goto end;
	}

	TRACE("[M3UReader] TaskGetOneSegment - WriteBuf %d GetFile size %d buffer address %x", hReader->m_dwWriteBuffer, dwFile, hBuffer);

	// when download is complete properly
	if (dwFile)
	{
		// store segment file which is just read or downloaded via http
		hBuffer->m_dwBuffer = dwFile;
		hBuffer->m_dwBufferPos = dwFile;
		if (hReader->m_bProgressive)
		{
			FrEnterMutex(&hReader->m_hMutex);
			if (hBuffer->m_hSegReader)
			{
				ReaderHandle hReader2 = (ReaderHandle)hBuffer->m_hSegReader;
				if (hReader2->ReaderSetStatus)
				{
					//TRACE("SetStatus 0 : Buffer %5d  Len %5d", hBuffer->m_dwSeqNumber, hBuffer->m_dwBufferPos);
					hReader2->ReaderSetStatus(hReader2->m_hReader, READ_INFO, 0, hBuffer->m_dwBufferPos);
				}
			}
			FrLeaveMutex(&hReader->m_hMutex);
		}
		else
		{
			// To double check if the buffer is freed
			FREE(hBuffer->m_pBuffer);
			hBuffer->m_pBuffer = pFile;
		}
		hBuffer->m_dwStartCTSInfo = hSegment->m_dwStartCTS;				// Segment start CTS in case of vod. for live, must be updated right after get_info()
		hBuffer->m_dwDuration = hSegment->m_dwDuration;					// duration
		hBuffer->m_dwSeqNumber = hSegment->m_dwSeqNumber;				// relevant sequence number in case of live
		UpdateBandwidthEstimated(hReader, dwFile*1000/(dwTick2+200));


#if 0
		{
			char file_name[1000];
			FILE *fp;
			sprintf(file_name, "C:\\Temp\\test_00%02d.ts", hReader->m_dwWriteBuffer);

			fp = fopen(file_name, "wb");
			fwrite(pFile, 1, dwFile, fp);
			fclose(fp);
		}
#endif

		if(hReader->m_bProgressive==FALSE && hBuffer->m_hSegReader)
		{
			FrEnterMutex(&hReader->m_hMutex);
			FrReaderClose(hBuffer->m_hSegReader);
			hBuffer->m_hSegReader = NULL;
			FrLeaveMutex(&hReader->m_hMutex);
		}

		// Increase Buffer
		hReader->m_dwWriteBuffer++;
		if (MAX_M3U_BUFFER_NUM <= hReader->m_dwWriteBuffer)
			hReader->m_dwWriteBuffer = 0;

		if (!hStream->m_dwAvgSegment)
			hStream->m_dwAvgSegment = dwFile;
		else
			hStream->m_dwAvgSegment = (hStream->m_dwAvgSegment*9 + dwFile)/10;	// AR average
		bSuccess = TRUE;
	}
end:
	if (bSuccess)
		TRACE("[M3UReader]	TaskGetOneSegment - End (WriteBuffer %d) Succeeded", hReader->m_dwWriteBuffer);
	else if (hReader->m_bLive==FALSE)
		TRACE("[M3UReader]	TaskGetOneSegment - End (WriteBuffer %d)", hReader->m_dwWriteBuffer);

#ifdef	TaskGetOneSegment
	if (!hReader->m_bFunction_GetOneSegment)
	{
		McCloseTask(hMyTask);

		TRACE("[M3UReader] TaskGetOneSegment - Task %x closed", hMyTask);

		hReader->m_hGetOneSegmentTask = INVALID_TASK;
		McExitTask();
	}
#endif
}

static DWORD32	GetBufferedSegments(M3UReaderHandle hReader)
{
	if (!hReader)
		return 0;

	return (hReader->m_dwWriteBuffer + MAX_M3U_BUFFER_NUM - hReader->m_dwReadBuffer) % MAX_M3U_BUFFER_NUM;
}

static DWORD32	GetBufferedDuration(M3UReaderHandle hReader, BOOL bTrace)
{
	DWORD32		dwRead = hReader->m_dwCurAudioReadBuffer,	//hReader->m_dwReadBuffer
				dwWrite = hReader->m_dwWriteBuffer,
				dwDuration = 0, dwEndTime,
				i;

	// Buffer Empty
	if (dwWrite == dwRead)
	{
		if (hReader->m_bProgressive)
		{
			M3UBufferHandle	hBuffer = &hReader->m_Buffer[dwRead];
			DWORD32			dwRatio;

			if (hBuffer->m_dwBuffer)
			{
				dwRatio = hBuffer->m_dwBufferPos*100/hBuffer->m_dwBuffer;
				dwDuration = hBuffer->m_dwDuration * dwRatio/100;
				dwDuration = hBuffer->m_dwStartCTSInfo + dwDuration;
				if (hBuffer->m_dwCurCTS < dwDuration)
					return dwDuration - hBuffer->m_dwCurCTS;
				else
					return 0;
			}

			return 0;
		}
		else
			return 0;
	}

	if (hReader->m_bLive)
	{
		if (hReader->m_Buffer[dwRead].m_dwCurCTS <
			hReader->m_Buffer[dwRead].m_dwStartCTS + hReader->m_Buffer[dwRead].m_dwDuration)
			dwDuration = hReader->m_Buffer[dwRead].m_dwStartCTS + hReader->m_Buffer[dwRead].m_dwDuration - hReader->m_Buffer[dwRead].m_dwCurCTS;

		dwRead++;
		if (MAX_M3U_BUFFER_NUM <= dwRead)
			dwRead = 0;
		if (dwWrite < dwRead)
			dwWrite += MAX_M3U_BUFFER_NUM;

		for(i= dwRead; i<dwWrite; i++)
		{
			DWORD32 dwIdx = i%MAX_M3U_BUFFER_NUM;
			dwDuration += hReader->m_Buffer[dwIdx].m_dwDuration;
		}

		if (bTrace)
			TRACE("[M3UReader] GetBufferedDuration : Buffer Duration %d [Start CTS %d Cur CTS %d In Read Buffer %d] [Write Buffer %d] [Audio Read Buffer %d, Video Read Buffer %d]",
				dwDuration,
				hReader->m_Buffer[hReader->m_dwCurAudioReadBuffer].m_dwStartCTS,
				hReader->m_Buffer[dwRead].m_dwCurCTS,
				hReader->m_dwCurAudioReadBuffer, dwWrite,
				hReader->m_dwCurAudioReadBuffer, hReader->m_dwCurVideoReadBuffer);
	}
	else
	{
		if (hReader->m_bProgressive)
		{
			M3UBufferHandle	hBuffer;
			DWORD32			dwRatio, dwTime = 0;

			hBuffer = &hReader->m_Buffer[dwWrite];
			if (hBuffer->m_dwBufferPos == 0)
			{
				// Not initialized yet
				dwWrite = (dwWrite) ? dwWrite-1 : MAX_M3U_BUFFER_NUM-1;
				hBuffer = &hReader->m_Buffer[dwWrite];
			}

			if (hBuffer->m_dwBuffer)
			{
				dwRatio = hBuffer->m_dwBufferPos*100/hBuffer->m_dwBuffer;
				dwTime = hBuffer->m_dwDuration * dwRatio/100;
			}
			dwEndTime = hBuffer->m_dwStartCTSInfo + dwTime;

			if (hReader->m_dwCurTimeAudio < dwEndTime)
				dwDuration = dwEndTime - hReader->m_dwCurTimeAudio;
			else
				dwDuration = 0;

			//TRACE("GetStatus duration %d receving %d", dwDuration, dwTime);

		}
		else
		{
			dwWrite = (dwWrite) ? dwWrite-1 : MAX_M3U_BUFFER_NUM-1;

			//TRACE("[M3UReader] GetBufferedDuration - WriteBuffer-1 %d Start CTS %d Duration %d",
			//	dwWrite, hReader->m_Buffer[dwWrite].m_dwStartCTS, hReader->m_Buffer[dwWrite].m_dwDuration);
			dwEndTime =  hReader->m_Buffer[dwWrite].m_dwStartCTSInfo + hReader->m_Buffer[dwWrite].m_dwDuration;

			if (hReader->m_dwCurTimeAudio < dwEndTime)
				dwDuration = dwEndTime - hReader->m_dwCurTimeAudio;
			else
				dwDuration = 0;
		}
	}

	return dwDuration;
}

// return : Buffer time in msec
static DWORD32	UpdateBufferRatio(M3UReaderHandle hReader, BOOL bTrace)
{
	DWORD32		dwBufferTime = GetBufferedDuration(hReader, FALSE);
	DWORD32		dwMinDuration = MIN(hReader->m_dwMinBufferTime, TS_MAX_BUFFER_TIME);		// reduce buffer time for the few last segments

	//TRACE("[M3UReader] GetMoreSegment - Buffer time %d min buffer time %d remain time %d",
	//	dwBufferTime, hReader->m_dwMinBufferTime, hReader->m_dwRemainTime);

	hReader->m_dwBufferRatio = dwBufferTime * 100 / MAX(1, dwMinDuration);
	if (100 < hReader->m_dwBufferRatio)
		hReader->m_dwBufferRatio = 100;

	return dwBufferTime;
}

static BOOL	Need2ChangeStream(M3UReaderHandle hReader, DWORD32 dwBufferedTime, DWORD32 *pdwNewStream, BOOL bForced2Bottom)
{
	DWORD32				dwTick = FrGetTickCount();
	DWORD32				dwDiff;
	M3UStreamHandle		hStream = hReader->m_phStream[hReader->m_dwCurStream];
	BOOL				bChanged = FALSE;
	DWORD32				dwBase = MIN(hReader->m_dwMinBufferTime, hStream->m_dwTargetDuration);
	DWORD32				dwEstBandwidth;
	INT32				nBufferRatio, nEstBufInc=0;

	// Forced
	if (bForced2Bottom)
	{
		*pdwNewStream = 0;
		hReader->m_dwLastStreamChangeTick = dwTick;
		return TRUE;
	}

	// Single stream. Nothing to do.
	if (hReader->m_dwStream < 2)
		return FALSE;

	// have to get at least one segment after the stream changed last time
	dwDiff = ABS(dwTick - hReader->m_dwLastStreamChangeTick);
	if (dwDiff < hStream->m_dwTargetDuration * 3/2)
		return FALSE;

	// estimate how much full the buffer will be after receiving the current segment
	nEstBufInc = hStream->m_dwTargetDuration;
	nEstBufInc = nEstBufInc - hStream->m_dwAvgSegment * 1000 / hReader->m_dwLastBWEstimation;		// Target Duration - Estimated recv time

	//TRACEX(DTB_LOG_DEBUG, "[M3UReader] Need2ChangeStream - Estimated Buf %d : Inc %d", dwBufferedTime + nEstBufInc, nEstBufInc);

// BUFFER_RATIO_BOTTOM				30
// BUFFER_RATIO_ONE_STEP_DOWN		50
// BUFFER_RATIO_NO_CHANGE			80
// BUFFER_RATIO_ONE_STEP_UP		100
	if (nEstBufInc < 0)
		nBufferRatio = ((INT32)dwBufferedTime + nEstBufInc) * 100 / (INT32)MAX(dwBase, BASE_BUFFER_TIME);
	else
		nBufferRatio = ((INT32)dwBufferedTime + 0) * 100 / (INT32)MAX(dwBase, BASE_BUFFER_TIME);;

	// go to Bottom
	if (nBufferRatio < BUFFER_RATIO_BOTTOM)
	{
		// If the current stream is not 0 (lowest bandwidth)
		TRACE("[M3UReader] Need2ChangeStream - Move to bottom rate");
		if (hReader->m_dwCurStream)
		{
			bChanged = TRUE;
			*pdwNewStream = 0;
		}
	}
	// Step down to the stream just below the current stream
	else if(nBufferRatio < BUFFER_RATIO_ONE_STEP_DOWN )
	{
		if (0 < hReader->m_dwCurStream)
		{
			bChanged = TRUE;
			*pdwNewStream = hReader->m_dwCurStream-1;
			TRACE("[M3UReader] Need2ChangeStream - One step down to %d", *pdwNewStream);
		}
	}
	// Just keep the current stream
	else if(nBufferRatio < BUFFER_RATIO_NO_CHANGE )
	{
	}
	// Check
	else if(nBufferRatio < BUFFER_RATIO_ONE_STEP_UP )
	{
		// Not the highest stream
		if ( (hReader->m_dwCurStream+1) != hReader->m_dwStream)
		{
			M3UStreamHandle		hNewStream = hReader->m_phStream[hReader->m_dwCurStream+1];
			dwEstBandwidth = hNewStream->m_dwAvgSegment * 1000 / hNewStream->m_dwTargetDuration ;	// in Bytes

			// We have enough bandwidth
			if (dwEstBandwidth < hReader->m_dwLastBWEstimation)
			{
				bChanged = TRUE;
				*pdwNewStream = hReader->m_dwCurStream+1;
				TRACE("[M3UReader] Need2ChangeStream - One step up to %d", *pdwNewStream);
			}
		}
	}
	else
	{
		// Not the highest stream
		if ( (hReader->m_dwCurStream+1) != hReader->m_dwStream)
		{
			INT32		i;

			// Find the highest stream available with the current estimated bandwidth
			for(i=hReader->m_dwStream-1; (INT32)hReader->m_dwCurStream <= i; i--)
			{
				M3UStreamHandle		hNewStream = hReader->m_phStream[i];
				dwEstBandwidth = hNewStream->m_dwAvgSegment * 1000 / hNewStream->m_dwTargetDuration ;		// in bytes

				// We have enough bandwidth
				if (dwEstBandwidth  < hReader->m_dwLastBWEstimation)
				{
					bChanged = TRUE;
					*pdwNewStream = i;
					TRACE("[M3UReader] Need2ChangeStream - Move up to highest %d", *pdwNewStream);
					break;
				}
			}
		}
	}

	if (bChanged)
		hReader->m_dwLastStreamChangeTick = dwTick;

	return bChanged;
}

static BOOL ToggleM3UUpdate(M3UReaderHandle hReader, BOOL bTrue)
{
	DWORD32		i, j=0;

	hReader->m_bPaused2Change = bTrue;
	while(1)
	{
		BOOL		bNotChanged = FALSE;

		for(i=0; i<hReader->m_dwStream; i++)
		{
			if (hReader->m_phStream[i]->m_bM3UUpdatePaused != hReader->m_bPaused2Change)
				bNotChanged = TRUE;
		}

		if (bNotChanged)
		{
			FrSleep(10);
			j += 10;
			if (MAX_WAIT_TIME < j)
				return FALSE;
			continue;
		}
		break;
	}
	return TRUE;
}

static BOOL UpdateWithNewStream(M3UReaderHandle hReader, DWORD32 dwNewStream)
{
	M3UStreamHandle		hCurStream = hReader->m_phStream[hReader->m_dwCurStream],
						hNewStream = hReader->m_phStream[dwNewStream];
	DWORD32				dwCurSeqNum = hReader->m_dwCurSeqNumber + 1;
	DWORD32				i, dwStart, dwEnd, bLarge=FALSE, bWait=FALSE;

	dwStart = hNewStream->m_dwCurSegment;
	dwEnd = hNewStream->m_dwSegmentUsedEnd;
	if (dwEnd<dwStart)
		dwEnd += hNewStream->m_dwSegmentAlloced;

	if (dwEnd == dwStart)
		return FALSE;

	for (i=dwStart; i<dwEnd; i++)
	{
		DWORD32		dwSegment = i%hNewStream->m_dwSegmentAlloced;
		DWORD32		dwSeqNumber = hNewStream->m_phSegment[dwSegment]->m_dwSeqNumber;

		// Found : same sequenc number. assumed the sequence numbers in the streams are all related
		if (dwSeqNumber == dwCurSeqNum)
		{
			hReader->m_dwCurStream = dwNewStream;
			hNewStream->m_dwCurSegment = dwSegment;
			return TRUE;
		}
		if (dwSeqNumber < dwCurSeqNum)
		{
			bLarge = TRUE;
			if (ABS(dwSeqNumber-dwCurSeqNum) < MAX_ALLOWED_WAIT_SEQ_NUM)
				bWait = TRUE;
		}
	}

	if (bWait)
		return FALSE;

	if (bLarge)
		hNewStream->m_dwCurSegment = hNewStream->m_dwSegmentAlloced;

	// Only change stream
	hReader->m_dwCurStream = dwNewStream;
	if (McSocketIsValid(hReader->m_hSegSock))
	{
		TRACE("[M3UReader] UpdateWithNewStream - socket closing - stream changed");
		McSocketClose(hReader->m_hSegSock);
		hReader->m_hSegSock = NULL;
	}
	return TRUE;
}

static VOID ChangeStream(M3UReaderHandle hReader, DWORD32 dwBufferTime, BOOL bForced2Bottom)
{
	DWORD32		dwNewStream=0;

	// Check to change stream
	if (Need2ChangeStream(hReader, dwBufferTime, &dwNewStream, bForced2Bottom))
	{
		//BOOL	bPause = TRUE;

		// Get m3u update paused
		//if (!ToggleM3UUpdate(hReader, bPause))
		//	return;

		// Keep time stamp continuous over the streams
		TRACE("[M3UReader] Change Stream to %d", dwNewStream);
		while(UpdateWithNewStream(hReader, dwNewStream) == FALSE)
		{
			TRACE("[M3UReader] UpdateWithNewStream failed - wait ");
			FrSleep(100);
			if (hReader->m_eTaskStatus == SEG_TASK_END)
				break;
			if (hReader->m_bReaderStop == TRUE)
				break;
		}

		// resume m3u update
		//bPause = FALSE;
		//if (!ToggleM3UUpdate(hReader, bPause))
		//	return;
	}
}

static BOOL GetMoreSegment(M3UReaderHandle hReader)
{
	DWORD32		dwBufferTime = UpdateBufferRatio(hReader, TRUE);
	BOOL		bForced = FALSE;

	// Live
	if (hReader->m_bLive)
	{
		DWORD32		dwNextWriteBuffer = hReader->m_dwWriteBuffer+1;

		if (MAX_M3U_BUFFER_NUM <= dwNextWriteBuffer)
			dwNextWriteBuffer = 0;

		// Buffer is full
		if (hReader->m_dwReadBuffer == dwNextWriteBuffer)
			return FALSE;

		// In memory optimize mode, hold at most 2 segments
		if (hReader->m_bMemOptimize)
		{
			if (2 < GetBufferedSegments(hReader))
				return FALSE;
		}
		// No read when bufferirng is more than 60
		else if (hReader->m_dwMinBufferTime <= dwBufferTime)
		{
			M3UStreamHandle		hStream = hReader->m_phStream[hReader->m_dwCurStream];

			if (hStream->m_dwCurSegment != hStream->m_dwSegmentUsedEnd)
				TRACE("[M3UReader] GetMoreSegment - Buffer time %d is bigger than Max Buffer Time %d. Discard the segments",
					dwBufferTime, hReader->m_dwMinBufferTime);

			hStream->m_dwCurSegment = hStream->m_dwSegmentUsedEnd;
			return FALSE;
		}
	}
	// VoD
	else
	{
		// In memory optimize mode, hold at most 2 segments
		if (hReader->m_bMemOptimize)
		{
			if (2 < GetBufferedSegments(hReader))
				return FALSE;
		}
		// No read when bufferirng is more than 100
		else if (hReader->m_dwMinBufferTime+hReader->m_dwTargetDuration <= dwBufferTime)
			return FALSE;
	}

retry_after_change:
	// Check if stream change is necessary
	ChangeStream(hReader, dwBufferTime, bForced);

	// Make Reading Task
#if 0
	if (hReader->m_hGetOneSegmentTask == INVALID_TASK)
	{
		hReader->m_hGetOneSegmentTask = McCreateTask(NULL, (PTASK_START_ROUTINE)TaskGetOneSegment, hReader,
				HTTP_TASK_PRIORITY, HTTP_TASK_STACKSIZE, 0);

		TRACE("[M3UReader] TaskGetOneSegment - Task %x Opened", hReader->m_hGetOneSegmentTask);
	}
	else
	{
		TRACE("[M3UReader] TaskGetOneSegment - Task %x Opened already", hReader->m_hGetOneSegmentTask);
	}
#else
	hReader->m_bFunction_GetOneSegment = TRUE;

	//TRACE("[M3UReader] TaskGetOneSegment Start : WriteBuffer %d", hReader->m_dwWriteBuffer);
	TaskGetOneSegment(hReader);
	if (hReader->m_lGetOneSegmentError == FR_FAIL)
	{
		bForced = TRUE;
		goto retry_after_change;
	}
	else if (hReader->m_lGetOneSegmentError == COMMON_ERR_CANCEL)
	{
		return FALSE;
	}

	//TRACE("[M3UReader] TaskGetOneSegment End : WriteBuffer %d", hReader->m_dwWriteBuffer);
#endif

	return TRUE;
}

static VOID GetLiveStreamPaused(M3UReaderHandle hReader)
{
	DWORD32 i;

	// Nothing to do for Vod
	if (!hReader->m_bLive)
		return;

	// Empty buffers
	hReader->m_dwBufferRatio = 0;
	hReader->m_dwCurAudioReadBuffer = hReader->m_dwCurVideoReadBuffer = 0;
	hReader->m_dwReadBuffer = hReader->m_dwWriteBuffer = 0;

	for(i=0; i<hReader->m_dwStream; i++)
	{
		M3UStreamHandle		hStream = hReader->m_phStream[i];
		DWORD32				j;

		hStream->m_dwCurSeqNumber = (DWORD32)-1;
		hStream->m_dwLastSeqNumber = 0;
		hStream->m_dwCurSegment = 0;
		hStream->m_dwSegmentUsedEnd = 0;
		for(j=0; j<hStream->m_dwSegmentAlloced; j++)
		{
			M3USegmentHandle	hSeg = hStream->m_phSegment[i];
			hSeg->m_dwDuration = hSeg->m_dwSeqNumber = hSeg->m_dwStartCTS = 0;
			if (hSeg->m_pszUrl)
				FREE(hSeg->m_pszUrl);
			hSeg->m_pszUrl = NULL;
		}
	}
}

static VOID TaskGetSegments(VOID* lpVoid)
{
	M3UReaderHandle	hReader = (M3UReaderHandle)lpVoid;

#ifdef EMBEDDED_M3U_UPDATE
	M3UStreamHandle		hStream = hReader->m_phStream[0];

	// Updated Time
	hStream->m_dwLastUpdateTick = McGetTickCount() - hStream->m_dwTargetDuration;
#endif

	hReader->m_dwLastStreamChangeTick = FrGetTickCount();

	while (hReader->m_eTaskStatus != SEG_TASK_END)
	{
		DWORD32	i;

		switch(hReader->m_eTaskStatus)
		{
		case	SEG_TASK_IDLE:
			FrSleep(10);
			break;
		case	SEG_TASK_RUN:
			if (hReader->m_bLive)
				hReader->m_bPaused = FALSE;
			// if no segment in the current list of segments unread
			if (!GetMoreSegment(hReader))
				FrSleep(100);
			// have enough data in the buffer
			//if (hReader->m_phStream[hReader->m_dwCurStream]->m_dwTargetDuration < UpdateBufferRatio(hReader, FALSE))
#ifdef EMBEDDED_M3U_UPDATE
			if(hReader->m_bPaused || hReader->m_bPaused2Change)
			{
				hStream->m_bM3UUpdatePaused = TRUE;
			}
			else
			{
				hStream->m_bM3UUpdatePaused = FALSE;
				UpdateM3U(hStream);
			}
#endif
			break;
		case	SEG_TASK_SEEK:
			// do things to carry out seek
			hReader->m_eTaskStatus = SEG_TASK_PAUSED;		// to wait for
			break;
		case	SEG_TASK_PAUSE:
			if (hReader->m_bLive)
			{
				hReader->m_bPaused = TRUE;
				FrSleep(10);
				for(i=0; i<hReader->m_dwStream; i++)
					if (!hReader->m_phStream[i]->m_bM3UUpdatePaused)
						continue;
				// discard segment info
				for(i=0; i<hReader->m_dwStream; i++)
					hReader->m_phStream[i]->m_dwCurSegment = hReader->m_phStream[i]->m_dwSegmentUsedEnd = 0;
				FrSleep(100);
				//EmptyBuffersExceptCurrentOne(hReader);
			}
			//GetLiveStreamPaused(hReader);
			hReader->m_eTaskStatus = SEG_TASK_PAUSED;		// to wait for other request
			break;
		case	SEG_TASK_PAUSED:
			hReader->m_bReaderStop = FALSE;
			FrSleep(10);
			break;
		default:
			FrSleep(10);
		}
	}
	hReader->m_bReaderStop = TRUE;
	McCloseTask(hReader->m_hGetSegmentTask);
	hReader->m_hGetSegmentTask = INVALID_TASK;
	TRACE("[M3UReader]	TaskGetSegments - End");
	McExitTask();

	return;
}

static VOID UpdateM3U(M3UStreamHandle hStream)
{
	M3UReaderHandle		hReader = (M3UReaderHandle)hStream->m_hParentReader;
	DWORD32				dwTick, dwDiff;
	LRSLT				lRet;
	BYTE				*pFile=NULL;
	DWORD32				dwFile=0, dwSleep=TS_MAX_UPDATE_SLEEP_TIME;
	BOOL				bSleep=FALSE;

	//TRACE("[M3UReader] UpdateM3U Tick");

	if (hReader->m_bLive==FALSE)
		return;

	dwTick = FrGetTickCount();
	if (hStream->m_dwLastUpdateTick < dwTick)
		dwDiff = dwTick - hStream->m_dwLastUpdateTick;
	else
		dwDiff = MAX_DWORD + dwTick - hStream->m_dwLastUpdateTick;

	if (dwDiff < hStream->m_dwTargetDuration)
	//if (dwDiff < hStream->m_dwTargetDuration)
	{
		DWORD32		dwSegment = (hStream->m_dwSegmentUsedEnd - hStream->m_dwCurSegment) % hStream->m_dwSegmentAlloced;

		if (dwSegment || dwDiff < hStream->m_dwTargetDuration*4/5)
		{
			//TRACE("[M3UReader] UpdateM3U - Stored Segment url %d (%d msec after the last update", dwSegment, dwDiff);
			FrSleep(100);
			return;
		}
		else
		{
			bSleep=TRUE;
			dwSleep = MIN(dwSleep, hStream->m_dwTargetDuration - dwDiff);
		}
	}

	if ((lRet = GetFile(hReader, (char*)hStream->m_szUrl, &hReader->m_URL, &pFile, &dwFile, FALSE)) != FR_OK)
	{
		//TRACE("[M3UReader] UpdateM3U GetFile failed");
		goto end;
	}
	else if (pFile && dwFile)
	{
		lRet = parseM3U(hReader, hStream, pFile, dwFile, NULL);
		if (pFile)
			FREE(pFile);
		if (FRFAILED(lRet))
			;

		// Move to next TargetDuration
		if (dwDiff < hStream->m_dwTargetDuration + TS_MAX_ALLOWED_TS_DURATION)
		{
			// keep the Target duration
			if (hStream->m_dwTargetDuration - dwDiff < 1000)
				hStream->m_dwLastUpdateTick += hStream->m_dwTargetDuration;
			else
				hStream->m_dwLastUpdateTick += dwDiff;
		}
		// Probably paused for some time
		else
			hStream->m_dwLastUpdateTick = dwTick;

		if (bSleep)
			FrSleep(dwSleep);
	}

end:
	;//TRACE("[M3UReader] UpdateM3U Tick End %d", McGetTickCount()-dwTick);
}

static VOID UpdateSegmentsList(M3UStreamHandle hStream, M3UStreamHandle	hCurStream)
{
	M3USegmentHandle		hSegment;
	DWORD32					dwStartSeqNum = hCurStream->m_phSegment[hCurStream->m_dwCurSegment]->m_dwSeqNumber;

	//if (hCurStream->m_dwCurSeqNumber <= hStream->m_dwCurSeqNumber)
	//	return;

	// if sequence numbers are not able to trust, jusk keep the last segment
	if ( (INT32)hCurStream->m_dwSegmentAlloced/2 < ABS(hStream->m_dwCurSeqNumber - hCurStream->m_dwCurSeqNumber) )
	{
		// No segment
		if (hStream->m_dwCurSegment == hStream->m_dwSegmentUsedEnd)
			return;
		hStream->m_dwCurSegment = hStream->m_dwSegmentUsedEnd ?
			hStream->m_dwSegmentUsedEnd-1 : hCurStream->m_dwSegmentAlloced-1;
		return;
	}

	hSegment = hStream->m_phSegment[hStream->m_dwCurSegment];
	while(hSegment->m_dwSeqNumber < dwStartSeqNum)
	{
		FREE(hSegment->m_pszUrl);
		hSegment->m_pszUrl = 0;

		//TRACE("[Reader] UpdateSegmentsList - update CurSeqNumber %d", hStream->m_dwCurSeqNumber);

		hStream->m_dwCurSegment++;
		if (hStream->m_dwSegmentAlloced <= hStream->m_dwCurSegment)
			hStream->m_dwCurSegment = 0;

		hSegment = hStream->m_phSegment[hStream->m_dwCurSegment];
		hStream->m_dwCurSeqNumber = hSegment->m_dwSeqNumber;

		// There is no segment
		if (hStream->m_dwCurSegment == hStream->m_dwSegmentUsedEnd)
			break;
	}
}

static VOID TaskUpdateM3U(VOID* lpVoid)
{
	M3UStreamHandle		hStream = (M3UStreamHandle)lpVoid,
						hStreamCur;
	M3UReaderHandle		hReader = (M3UReaderHandle)hStream->m_hParentReader;
	DWORD32				dwTick = FrGetTickCount();

	// Nothing to do for VoD
	if (!hStream->m_bLive || !hStream || !hReader)
		return;

	TRACE("[M3UReader]	TaskUpdateM3U Starts");

	// Updated Time
	hStream->m_dwLastUpdateTick = FrGetTickCount() - hStream->m_dwTargetDuration;

	while (hReader->m_eTaskStatus != SEG_TASK_END)
	{
		//TRACE("[M3UReader]	TaskUpdateM3U Tick %d", McGetTickCount() - dwTick);
		hStreamCur = hReader->m_phStream[hReader->m_dwCurStream];

		if (hReader->m_bReaderStop == TRUE)
		{
			FrSleep(10);
			continue;
		}

		if (hStream != hStreamCur)
			UpdateSegmentsList(hStream, hStreamCur);

		if(hReader->m_bPaused || hReader->m_bPaused2Change)
		{
			hStream->m_bM3UUpdatePaused = TRUE;
		}
		else
		{
			hStream->m_bM3UUpdatePaused = FALSE;
			UpdateM3U(hStream);
		}
		FrSleep(10);
	}

	McCloseTask(hStream->m_hUpdateM3UTask);
	hStream->m_hUpdateM3UTask = INVALID_TASK;
	TRACE("[M3UReader]	TaskUpdateM3U Ends");
	McExitTask();

	return;
}

static VOID SortStreamByBandwidth(M3UReaderHandle hReader)
{
	INT32				i, nMax, nStreamNum;
	DWORD32				dwStreamIdx;
	M3UStreamHandle		hStream, hStreamTemp;

	nStreamNum = hReader->m_dwStream;
	while(0 < nStreamNum)
	{
		nMax = 0;
		dwStreamIdx = 0;
		for (i=0; i<nStreamNum; i++)
		{
			hStream = hReader->m_phStream[i];
			if (nMax < (INT32)hStream->m_dwBandwidth)
			{
				dwStreamIdx = i;
				nMax = hStream->m_dwBandwidth;
			}
		}
		nStreamNum--;
		// if the stream with the max bitrate is the last one
		// nothing to do
		if (dwStreamIdx == nStreamNum)
			continue;
		// move the default stream to the current stream
		if (hReader->m_dwCurStream == dwStreamIdx)
			hReader->m_dwCurStream = nStreamNum;

		// Swap index to sort
		hStreamTemp = hReader->m_phStream[nStreamNum];
		hReader->m_phStream[nStreamNum] = hReader->m_phStream[dwStreamIdx];
		hReader->m_phStream[dwStreamIdx] = hStreamTemp;
	}
}

LRSLT M3UReaderOpen(VOID** phReader, FrURLInfo* pURL)
{
	M3UReaderHandle	hReader;
	LRSLT			lRet;

	hReader = (M3UReaderHandle)MALLOCZ(sizeof(M3UReaderStruct));
	if (hReader)
	{
		TRACE("[M3UReader]	M3UReaderOpen - url=%s", pURL->pUrl);
		memcpy(&hReader->m_URL, pURL, sizeof(FrURLInfo));

		FrCreateMutex(&hReader->m_hMutex);

		hReader->m_hGetSegmentTask = INVALID_TASK;
		hReader->m_hGetOneSegmentTask = INVALID_TASK;
		hReader->m_pszURL = UtilStrStrdupEx(pURL->pUrl, 100);

		// Set connection time out
		hReader->m_dwConnectTimeOut = pURL->m_dwConnTimeOut ? pURL->m_dwConnTimeOut : HTTP_DEFAULT_TIMEOUT;
		hReader->m_dwRecvTimeOut = pURL->m_dwRecvTimeOut ? pURL->m_dwRecvTimeOut : HTTP_DEFAULT_TIMEOUT;
		hReader->m_dwRetryCount = pURL->m_dwRetryCnt ? pURL->m_dwRetryCnt : HTTP_DEFAULT_RETRY_COUNT;
		//TRACE("[M3UReader]	M3UReaderOpen - RetryCount %d", hReader->m_dwRetryCount);
		hReader->m_hSegSock = NULL;
		hReader->m_bProgressive = TRUE;

		// set initial buffering time
		hReader->m_bDisableNoData = pURL->m_bDisableNoData;
		hReader->m_dwInitBuffTime = pURL->m_dwInitBufferTime;
		hReader->m_dwSeekBuffTime = pURL->m_dwSeekBufferTime;
		hReader->m_dwBuffTime = pURL->m_dwBufferTime;
		if (pURL->m_bLiveBufferTime)
		{
			hReader->m_bLiveBuffTime = pURL->m_bLiveBufferTime;
			hReader->m_dwInitBuffTimeLive = pURL->m_dwInitBufferTimeLive;
			hReader->m_dwSeekBuffTimeLive = pURL->m_dwSeekBufferTimeLive;
			hReader->m_dwBuffTimeLive = pURL->m_dwBufferTimeLive;
		}

		// start code in H264 frames is allowed or not
		hReader->m_bH264TsFormat = pURL->m_eM3UFormat == M3U_FORMAT_IS_TS_AND_TS_H264;
		if (hReader->m_pszURL)
		{
#ifdef	UNICODE
			char			*pszTemp;

			TRACE("[M3UReader]	M3UReaderOpen - Unicode Conversion");
			pszTemp = (char *)MALLOCZ(_tcslen(hReader->m_pszURL) * sizeof(UTF16));
			UtilUniUTF16toANSI((UTF16*)phReader->m_szURL, sizeof(*hReader->m_szURL), pszTemp, sizeof(*pszTemp), DTB_CP_SYS);
			FREE(pszTemp);
			pszTemp = NULL;
#endif
		}

		// Use M3U Buffer for debug
		lRet = GetFile(hReader, pURL->pUrl, pURL, &hReader->m_pM3UBuffer, &hReader->m_dwM3UBuffer, FALSE);
		if (lRet != FR_OK)
			goto failed;

		if (FRFAILED(lRet = parseM3U(hReader, NULL, hReader->m_pM3UBuffer, hReader->m_dwM3UBuffer, pURL->pUrl)))
		{
			M3UReaderClose(hReader);
			return	lRet;
		}

		// Memory optimized
		hReader->m_bMemOptimize = pURL->m_bM3UMemOptimize;
		FREE((VOID*)hReader->m_pM3UBuffer);
		hReader->m_pM3UBuffer = NULL;
		if (hReader->m_bMemOptimize==FALSE)
			hReader->m_pInfo = (FrMediaInfo*)MALLOCZ(sizeof(FrMediaInfo));

		*phReader = (VOID*)hReader;
	}
	else
		return	COMMON_ERR_MEM;

	TRACE("[M3UReader]	M3UReaderOpen - succeeded");
	return	FR_OK;

failed:
	TRACE("[M3UReader]	M3UReaderOpen - failed");
	M3UReaderClose(hReader);
	return lRet;
}

VOID M3UReaderClose(VOID* pVoid)
{
	M3UReaderHandle	hReader = (M3UReaderHandle)pVoid;
	DWORD32			i, j;

	if (hReader)
	{
		TRACE("[M3UReader]	M3UReaderClose - starts");
		// Wait for the Tasks to stop
		hReader->m_bReaderStop = TRUE;
		hReader->m_eTaskStatus = SEG_TASK_END;
		j = 0;
		while(hReader->m_hGetOneSegmentTask != INVALID_TASK)
		{
			FrSleep(10);
			if (++j < MAX_WAIT_TIME/10)
				continue;
			else
				break;
		}
		j = 0;
		while(hReader->m_hGetSegmentTask != INVALID_TASK)
		{
			FrSleep(10);
			if (++j < MAX_WAIT_TIME/10)
				continue;
			else
				break;
		}
		TRACE("[M3UReader]	M3UReaderClose - SegmentTask ended (0x%x)", hReader->m_hGetSegmentTask);

		for(i=0; i<hReader->m_dwStream; i++)
		{
			j = 0;
			if (hReader->m_phStream[i] && hReader->m_phStream[i]->m_bLive)
			{
				hReader->m_bPaused = TRUE;
				while(hReader->m_phStream[i]->m_hUpdateM3UTask != INVALID_TASK)
				{
					FrSleep(10);
					if (++j < MAX_WAIT_TIME/10)
						continue;
					else
						break;
				}
			}
		}
		if (hReader->m_phStream && hReader->m_phStream[0])
			TRACE("[M3UReader]	M3UReaderClose - UpdateM3UTask ended (0x%x)", hReader->m_phStream[0]->m_hUpdateM3UTask);

		FREE((VOID*)hReader->m_pM3UBuffer);
		if (hReader->m_phStream)
		{
			for(i=0; i<hReader->m_dwStream; i++)
			{
				if (hReader->m_phStream[i])
				{
					if (hReader->m_phStream[i]->m_phSegment)
					{
						for(j=0; j<hReader->m_phStream[i]->m_dwSegmentAlloced; j++)
							if (hReader->m_phStream[i]->m_phSegment[j])
							{
								FREE((VOID*)hReader->m_phStream[i]->m_phSegment[j]->m_pszUrl);
								FREE((VOID*)hReader->m_phStream[i]->m_phSegment[j]);
							}
						FREE((VOID*)hReader->m_phStream[i]->m_phSegment);
					}
					FREE((VOID*)hReader->m_phStream[i]);
				}
			}
			FREE((VOID*)hReader->m_phStream);
		}
		TRACE("[M3UReader]	M3UReaderClose - Stream Buffer freed");

		for(i=0; i<MAX_M3U_BUFFER_NUM; i++)
		{
			M3UBufferHandle	hBuffer = &hReader->m_Buffer[i];
			FREE(hBuffer->m_pBuffer);
			FrEnterMutex(&hReader->m_hMutex);
			if (hBuffer->m_hSegReader)
			{
				TRACE("[M3UReader]	M3UReaderClose - Reader of %d-th Buffer closing", i);
				FrReaderClose(hBuffer->m_hSegReader);
				TRACE("[M3UReader]	M3UReaderClose - Reader of %d-th Buffer closed", i);
			}
			FrLeaveMutex(&hReader->m_hMutex);
		}

		if(1 || McSocketIsValid(hReader->m_hSegSock))
		{
			TRACE("[M3UReader] M3UReaderClose - socket closing");
			McSocketClose(hReader->m_hSegSock);
		}

		hReader->m_bEnd = TRUE;

		FREE(hReader->m_pVideoFrame);
		FREE(hReader->m_pAudioFrame);
		TRACE("[M3UReader]	M3UReaderClose - Frame Buffer Freed (V 0x%x, A 0x%x)", hReader->m_pVideoFrame, hReader->m_pAudioFrame);

#ifdef	FILE_DUMP
		FrCloseFile(hReader->m_hFile);
#endif
		FREE(hReader->m_pszURL);
		FREE(hReader->m_pInfo);

		FrDeleteMutex(&hReader->m_hMutex);

		FREE(hReader);
		TRACE("[M3UReader]	M3UReaderClose Ends");
	}
}

// already proved in ReaderOpen
// dummy functon. Not to cause chaos
BOOL M3UReaderIsValidInfo(void* pVoid, QWORD qwCurFileSize)
{
	M3UReaderHandle	hReader = (M3UReaderHandle)pVoid;

	return TRUE;
}

BOOL CompareAndStoreVideoConfig(M3UReaderHandle	hReader, FrMediaInfo *hInfo)
{
	FrVideoInfo	 *hVideo = &hInfo->FrVideo;

	// when there's no config
	if (!hVideo->dwConfig)
		return FALSE;

	// when meet the cofing first
	if (!hReader->m_dwVideoConfig)
	{
		hReader->m_dwVideoConfig = MIN(MAX_CONFIG_SIZE, hVideo->dwConfig);
		if (hReader->m_dwVideoConfig)
			memcpy(hReader->m_VideoConfig,  hVideo->pConfig, hReader->m_dwVideoConfig);
		return FALSE;
	}

	// same config
	if ( (hReader->m_dwVideoConfig == hVideo->dwConfig) &&
		 (!memcmp(hReader->m_VideoConfig, hVideo->pConfig, hReader->m_dwVideoConfig)) )
		return FALSE;

	// coyp config
	hReader->m_dwVideoConfig = MIN(MAX_CONFIG_SIZE, hVideo->dwConfig);
	if (hReader->m_dwVideoConfig)
		memcpy(&hReader->m_VideoConfig[0],  hVideo->pConfig, hReader->m_dwVideoConfig);

	return TRUE;
}

static LRSLT AllocFrameMemory(M3UReaderHandle	hReader)
{
	//DWORD32		dwVideoFrame;

	// do nothing. because we use queue for omx decoder
	return FR_OK;

#if 0
	if (hReader->m_Buffer[0].m_dwDuration)
	{
		// 3 times of 1 sec of 1 st segment
		dwVideoFrame = hReader->m_Buffer[0].m_dwBuffer / (hReader->m_Buffer[0].m_dwDuration/1000) * 3;
		hReader->m_dwVideoFrame = dwVideoFrame;
		hReader->m_pVideoFrame = (BYTE*)MALLOCZ(hReader->m_dwVideoFrame);
	}
	else
	{
		hReader->m_dwVideoFrame = 1000;
		hReader->m_pVideoFrame = (BYTE*)MALLOCZ(hReader->m_dwVideoFrame);
	}
	hReader->m_dwAudioFrame = 1000;
	hReader->m_pAudioFrame = (BYTE*)MALLOCZ(hReader->m_dwAudioFrame);

	if (!hReader->m_pVideoFrame || !hReader->m_pAudioFrame)
	{
		TRACE("[M3UReader] - AllocFrameMemory failed");
		return COMMON_ERR_MEM;
	}
#endif

	return FR_OK;
}

static LRSLT SegmentReaderOpen(M3UReaderHandle hReader, M3UBufferHandle hBuffer, FrMediaInfo *hInfo)
{
	FrURLInfo			Url = {(FrURLType)0, };
	FrURLInfo			*hUrl = &Url;
	FrMediaInfo			Info = { 0, }, *hSegInfo = NULL;

	//TRACE("[M3UReader] SegmentReaderOpen - Start");

	TRACE("[M3UReader] SegmentReaderOpen - Buffer size %d Buffer address %x", hBuffer->m_dwBuffer, hBuffer);

	while(1)
	{
		if (hReader->m_bEnd || hReader->m_bReaderStop)
			break;

		if (188*20 < hBuffer->m_dwBufferPos)
			break;
		FrSleep(10);
	}

	if (hReader->m_bEnd || hReader->m_bReaderStop)
	{
		return COMMON_ERR_CANCEL;
	}

	// Reader Open with MemFile
	hUrl->m_bMem = TRUE;
	hUrl->m_pMem = hBuffer->m_pBuffer;
	if (hReader->m_bProgressive)
	{
		hUrl->m_qwFileSize = hBuffer->m_dwBuffer;
		hUrl->m_qwSize = hBuffer->m_dwBufferPos;
	}
	else
		hUrl->m_qwSize = hBuffer->m_dwBuffer;
	// Progressive Download
	hUrl->m_bProgressive = hReader->m_bProgressive;
	/*hUrl->m_dwRTSPSupported = 0xFFFFFFFF;
	hUrl->m_dwMMSSupported = 0xFFFFFFFF;
	hUrl->m_dwHTTPSupported = 0xFFFFFFFF;
	hUrl->m_dwLocalSupported = 0xFFFFFFFF;*/
	hUrl->m_eM3UFormat = hReader->m_eM3UFormat;

	if (!hReader->m_bFirstRead)
		hReader->m_bFirstRead = TRUE;
	else
		hUrl->m_bNotPrintUrlInfo = TRUE;

	// if this is not to open m3u reader
	if (!hInfo)
		hUrl->m_eM3UFormat = hReader->m_eM3UFormat;
	hSegInfo = hReader->m_pInfo?hReader->m_pInfo:&Info;
	hSegInfo->eM3UFormat = hReader->m_eM3UFormat;
	hSegInfo->eM3UStartTime = M3U_START_TIME_SET;
	hSegInfo->dwStartTime = hReader->m_dwStartTime;
	hSegInfo->dwTotalRange = hBuffer->m_dwDuration;
	if (hInfo)
		hInfo->dwTotalRange = hBuffer->m_dwDuration;

	hReader->m_lGetOneSegmentError = FrReaderOpen(&hBuffer->m_hSegReader, hUrl);
	if (hReader->m_lGetOneSegmentError != FR_OK)
	{
		TRACE("[M3UReader] SegmentReaderOpen - McReaderOpen Failed (%x)", hReader->m_lGetOneSegmentError);
		memset(hBuffer, 0, sizeof(M3UBuffer));
		hBuffer->m_bBadSegment = TRUE;
		return hReader->m_lGetOneSegmentError;
	}
#ifdef EMBEDDED_M3U_UPDATE
	if (hInfo)		// copy Ts packet size
	{
		hReader->m_dwTsPacketSize = hInfo->m_dwTsPacketSize;
		hSegInfo->m_dwTsPacketSize = hReader->m_dwTsPacketSize;
	}
#endif
	hReader->m_lGetOneSegmentError = FrReaderGetInfo(hBuffer->m_hSegReader, hInfo ? hInfo : hSegInfo);

	if (hReader->m_lGetOneSegmentError != FR_OK)
	{
		FrEnterMutex(&hReader->m_hMutex);
		FrReaderClose(hBuffer->m_hSegReader);
		hBuffer->m_hSegReader = NULL;
		FrLeaveMutex(&hReader->m_hMutex);

		if (hBuffer->m_dwBuffer == hBuffer->m_dwBufferPos)
		{
			TRACE("[M3UReader] SegmentReaderOpen - McReaderGetInfo Failed (%x) - bad segment", hReader->m_lGetOneSegmentError);
			hBuffer->m_bBadSegment = TRUE;
			return hReader->m_lGetOneSegmentError;
		}
		else
		{
			TRACE("[M3UReader] SegmentReaderOpen - McReaderGetInfo Failed (%x)", COMMON_ERR_NODATA);
			hReader->m_lGetOneSegmentError = FR_OK;
			FrEnterMutex(&hReader->m_hMutex);
			FrReaderClose(hBuffer->m_hSegReader);
			hBuffer->m_hSegReader = NULL;
			FrLeaveMutex(&hReader->m_hMutex);
			return COMMON_ERR_NODATA;
		}
	}

	if (hInfo)
	{
		if (hInfo->URLType == _TS_FILE)
		{
			hReader->m_eM3UFormat = hReader->m_bH264TsFormat ? M3U_FORMAT_IS_TS_AND_TS_H264 : M3U_FORMAT_IS_TS;
		}
		else if (hInfo->URLType == _MP4_FILE)
			hReader->m_eM3UFormat = M3U_FORMAT_IS_MP4;
		else
			hReader->m_eM3UFormat = M3U_FORMAT_NONE;
		hSegInfo->eM3UFormat = hReader->m_eM3UFormat;
		hReader->m_bAudio = hInfo->dwAudioTotal ? TRUE: FALSE;
		hReader->m_bVideo = hInfo->dwVideoTotal ? TRUE: FALSE;
	}
	else
	{
		hReader->m_bAudio = hSegInfo->dwAudioTotal ? TRUE: FALSE;
		hReader->m_bVideo = hSegInfo->dwVideoTotal ? TRUE: FALSE;
	}
	hBuffer->m_bGetInfo = TRUE;

	TRACE("[M3UReader] SegmentReaderOpen - Ends");

	return FR_OK;
}

LRSLT M3UReaderGetInfo(VOID* pVoid, FrMediaInfo* hInfo)
{
	M3UReaderHandle	hReader = (M3UReaderHandle)pVoid;
	DWORD32			i, dwAudioNum = 0, dwVideoNum = 0, dwTextNum = 0;
	LRSLT			lRet;
	M3UBufferHandle	hBuffer;

	hInfo->URLType = _HLS_URL;
	strcpy(hInfo->szFileFormat, "m3u");

	TRACE("[Reader] M3UReaderGetInfo starts");

	// When the M3U is a master playlist file
	if ( (hReader->m_dwStream == 1 && !hReader->m_phStream[0]->m_phSegment) ||
		 (1 < hReader->m_dwStream) )
	{
		for (i=0; i<hReader->m_dwStream; i++)
		{
			BYTE		*pFile=NULL;
			DWORD32		dwFile=0;

			lRet = GetFile(hReader, (char*)hReader->m_phStream[i]->m_szUrl, &hReader->m_URL, &pFile, &dwFile, FALSE);
			if (lRet != FR_OK)
				return COMMON_ERR_STREAMSYNTAX;

			if (FRFAILED(lRet = parseM3U(hReader, hReader->m_phStream[i], pFile, dwFile, NULL)))
			{
				TRACE("[Reader] M3UReaderGetInfo - parsM3U () error %d", lRet);
				FREE(pFile);
				M3UReaderClose(hReader);
				return	lRet;
			}
			FREE(pFile);

			if (hReader->m_bLive)
				hReader->m_phStream[i]->m_dwStartSeqNumber = hReader->m_phStream[i]->m_dwCurSeqNumber;
		}
	}
	// check live mode
	if (hReader->m_phStream[0]->m_bLive && !hReader->m_bLive)
		hReader->m_bLive = TRUE;


	// extend a list of segment in case of live
	if (FRFAILED(lRet = expandSegmentList(hReader)))
		return lRet;


	// When adaptive stream
	SortStreamByBandwidth(hReader);

	// start task which updates the m3u files at an interval of target duration for live service
	if (hReader->m_bLive)
	{
		DWORD32		dwDur = hReader->m_phStream[0]->m_dwTargetDuration * (hReader->m_phStream[0]->m_dwSegNumInLiveService+1);
		hReader->m_dwMinBufferTime = MAX(MAX_M3U_BUFFER_TIME, dwDur);
	}
	else
	{
		if (MAX_M3U_BUFFER_TIME < hInfo->dwBufferTime)
			hReader->m_dwMinBufferTime = MAX_M3U_BUFFER_TIME;
		else if (hInfo->dwBufferTime)
			hReader->m_dwMinBufferTime = hInfo->dwBufferTime ;
		else
			hReader->m_dwMinBufferTime = MIN_M3U_BUFFER_TIME;

		hReader->m_dwMinBufferTime = MAX(MAX_M3U_BUFFER_TIME, hInfo->dwBufferTime);
	}

	// Just get to at least a segment file to validate urls
	if (hReader->m_bProgressive == FALSE)
	{
		TaskGetOneSegment((VOID*)hReader);
		if (FRFAILED(hReader->m_lGetOneSegmentError))
		{
			TRACE("[M3UReader]	M3UReaderGetInfo - Failed (0x%x). Cannot open segment file", hReader->m_lGetOneSegmentError);
			return hReader->m_lGetOneSegmentError;
		}
	}

	// Make Reading Task
	if (hReader->m_hGetSegmentTask == INVALID_TASK)
		hReader->m_hGetSegmentTask = McCreateTask(NULL, (PTASK_START_ROUTINE)TaskGetSegments, hReader,
				HTTP_TASK_PRIORITY, 0/*HTTP_TASK_STACKSIZE*/, 0);

	//TRACE("[M3UReader] M3UReaderGetInfo - TaskGetSegments %x", hReader->m_hGetSegmentTask);
	hReader->m_eTaskStatus = SEG_TASK_RUN;

	/////////////////////
	if (hReader->m_bProgressive)
	{
		M3UBufferHandle hBuffer = &hReader->m_Buffer[hReader->m_dwReadBuffer];

		i = 0;
		while(hBuffer->m_dwBufferPos < 188*20)
		{
			if (hReader->m_bReaderStop)
				break;
			//TRACE("[M3UReader] M3UReaderGetInfo - Buffer pos %d", hBuffer->m_dwBufferPos);
			FrSleep(10);
			if (++i < MAX_WAIT_TIME/10)
				continue;
			else
				break;
		}

		if (!hBuffer->m_dwBufferPos)
			return COMMON_ERR_FOPEN;

		TRACE("[M3UReader] M3UReaderGetInfo - Buffer pos %d received", hBuffer->m_dwBufferPos);
	}
	else
	{
		i = 0;
		while(!hReader->m_dwWriteBuffer)
		{
			if (hReader->m_bReaderStop)
				break;
			FrSleep(10);
			if (++i < MAX_WAIT_TIME/10)
				continue;
			else
				break;
		}

		if (!hReader->m_dwWriteBuffer)
			return COMMON_ERR_FOPEN;

		TRACE("[M3UReader] M3UReaderGetInfo - Buffers %d received", hReader->m_dwWriteBuffer);
	}

	if (hReader->m_bReaderStop)
	{
		TRACE("[M3UReader]	M3UReaderGetInfo Ends - externally stopped");
		return	COMMON_ERR_CANCEL;
	}

	// Get Info
	hBuffer = &hReader->m_Buffer[hReader->m_dwReadBuffer];
	hInfo->eM3UStartTime = M3U_START_TIME_GET;

	TRACE("[M3UReader] M3UReaderGetInfo start : 1st Segment Size %d Buffer num %d", hReader->m_Buffer[hReader->m_dwReadBuffer].m_dwBuffer, hReader->m_dwReadBuffer);
	hReader->m_eM3UFormat = hReader->m_bH264TsFormat ? M3U_FORMAT_IS_TS_AND_TS_H264 : M3U_FORMAT_IS_TS;
	hReader->m_lError =  SegmentReaderOpen(hReader, &hReader->m_Buffer[hReader->m_dwReadBuffer], hInfo);
	if (hReader->m_lError != FR_OK)
	{
		TRACE("[M3UReader] M3UReaderGetInfo Failed (%x)", hReader->m_lError);
		return hReader->m_lError;
	}
	TRACE("[M3UReader] M3UReaderGetInfo end");

	CompareAndStoreVideoConfig(hReader, hInfo);
	//if (AllocFrameMemory(hReader) != MC_OK)
	//	return COMMON_ERR_MEM;

	hInfo->URLType = _HLS_URL;
	strcpy(hInfo->szFileFormat, "m3u");

	hReader->m_dwStartTime = hInfo->dwStartTime;
	hInfo->eM3UStartTime = M3U_START_TIME_NO_OP;
	hBuffer->m_bGetInfo = TRUE;
	hBuffer->m_bAudio = hInfo->dwAudioTotal ? TRUE: FALSE;
	hBuffer->m_bVideo = hInfo->dwVideoTotal ? TRUE: FALSE;

	hReader->m_bAudioExist = hBuffer->m_bAudio;
	hReader->m_bVideoExist = hBuffer->m_bVideo;

	// Total Range Update
	if (!hReader->m_bLive)
	{
		hInfo->dwTotalRange = hReader->m_phStream[0]->m_phSegment[hReader->m_phStream[0]->m_dwSegmentUsedEnd-1]->m_dwStartCTS +
			hReader->m_phStream[0]->m_phSegment[hReader->m_phStream[0]->m_dwSegmentUsedEnd-1]->m_dwDuration;
		hReader->m_dwDuration = hInfo->dwTotalRange;

		TRACE("[M3UReader]	M3UReaderGetInfo - duration %d", hReader->m_dwDuration);
	}
	else
		hInfo->dwTotalRange = 0;

	// Make Update M3U Task
	if (hReader->m_bLive)
		for (i=0; i<hReader->m_dwStream; i++)
		{
			hReader->m_bPaused = FALSE;
#ifndef EMBEDDED_M3U_UPDATE
			hReader->m_phStream[i]->m_hUpdateM3UTask = McCreateTask(NULL, (PTASK_START_ROUTINE)TaskUpdateM3U, hReader->m_phStream[i],
				HTTP_TASK_PRIORITY, 0/*HTTP_TASK_STACKSIZE*/, 0);
#endif
		}

	// to seamless come over the boundary of segments
	hInfo->FrVideo.bLowDelayDecoding = TRUE;
	// Set maximum wait time in player engine until the player buffer gets full
	hInfo->dwBufferTime = MAX_M3U_BUFFER_TIME;
	// Bitrate
	if (hReader->m_phStream[0]->m_dwBandwidth)
		hInfo->FrVideo.dwBitRate = hReader->m_phStream[0]->m_dwBandwidth;
	else
	{
		// Not accurate But needed
		if (hReader->m_Buffer[0].m_dwDuration)
		{
			QWORD		qwTemp = (QWORD)hReader->m_Buffer[0].m_dwBuffer*8*1000;
			hInfo->FrVideo.dwBitRate = (DWORD32)(qwTemp/hReader->m_Buffer[0].m_dwDuration);
		}
	}

	TRACE("[M3UReader]	M3UReaderGetInfo Ends");

	return	FR_OK;
}

LRSLT M3UReaderStart(VOID* pVoid, DWORD32* pdwStart)
{
	M3UReaderHandle	hReader = (M3UReaderHandle)pVoid;

#if 0
	lRet = RTSPPlay(hReader, FALSE, *pdwStart);
	if (MCFAILED(lRet))
		return	lRet;

	*pdwStart = McRTSPGetPlayFromRange(hReader->m_hRTSP);
#endif

	return	FR_OK;
}

#if 0
INT32	M3UReaderGetBitrate(VOID* pVoid)
{
	M3UReaderHandle		hReader = (M3UReaderHandle)pVoid;
	return hReader->m_dwAvgBitrate;
}
#endif

VOID M3UReaderSetFactor(VOID* pVoid, float fa, float fb)
{
#if 0
	M3UReaderHandle		hReader = (M3UReaderHandle)pVoid;
	TRACE("[M3UReader] factor a = %2.1f, b = %2.1f", fa, fb);
	hReader->m_fA = fa;
	hReader->m_fB = fb;
#endif
}

static VOID UpdateReadBuffer(M3UReaderHandle hReader, BOOL bAudio, DWORD32 dwNextReadBuffer)
{
	DWORD32		i,
				dwPrevReadBuffer = hReader->m_dwReadBuffer,
				dwCurReadBuffer;

	if (!hReader->m_bVideoExist || !hReader->m_bAudioExist)
		hReader->m_dwReadBuffer
			= hReader->m_dwCurAudioReadBuffer = hReader->m_dwCurVideoReadBuffer
				= dwNextReadBuffer;
	else
	{
		if (bAudio)
		{
			TRACE("[M3UReader] UpdateReadBuffer - audio read Buffer %d", dwNextReadBuffer);
			hReader->m_dwCurAudioReadBuffer = dwNextReadBuffer;
		}
		else
		{
			TRACE("[M3UReader] UpdateReadBuffer - video read Buffer %d", dwNextReadBuffer);
			hReader->m_dwCurVideoReadBuffer = dwNextReadBuffer;
		}

		if (ABS(hReader->m_dwCurAudioReadBuffer - hReader->m_dwCurVideoReadBuffer) < MAX_M3U_BUFFER_NUM/2)
			hReader->m_dwReadBuffer = MIN(hReader->m_dwCurAudioReadBuffer, hReader->m_dwCurVideoReadBuffer);
		else
			hReader->m_dwReadBuffer = MAX(hReader->m_dwCurAudioReadBuffer, hReader->m_dwCurVideoReadBuffer);
	}

	// TRACE("[M3UReader] UpdateReadBuffer Prev Read Buffer %d => Cur Read Buffer %d", dwPrevReadBuffer, hReader->m_dwReadBuffer);

	// Nothing to do
	if (hReader->m_dwReadBuffer == dwPrevReadBuffer)
		return;

	TRACE("[M3UReader] UpdateReadBuffer - Remove Buffer [Start %d, End %d]", dwPrevReadBuffer, hReader->m_dwReadBuffer);

	dwCurReadBuffer = hReader->m_dwReadBuffer;
	if (hReader->m_dwReadBuffer < dwPrevReadBuffer)
		dwCurReadBuffer += MAX_M3U_BUFFER_NUM;

	for (i=dwPrevReadBuffer; i<dwCurReadBuffer; i++)
	{
		DWORD32	dwBuffer = i % MAX_M3U_BUFFER_NUM;
		EmptyOneBufferAfterBeingUsed(&hReader->m_Buffer[i], hReader);
	}
}

static VOID CheckEndOfSegment(M3UBufferHandle hBuffer, BOOL bAudio)
{
	if (hBuffer->m_dwBuffer == hBuffer->m_dwBufferPos)
	{
		if (bAudio)
			hBuffer->m_eAudioRead = READ_END;
		else
			hBuffer->m_eVideoRead = READ_END;
	}
}

static LRSLT CopyMediaFrame(M3UReaderHandle hReader, FrMediaStream *pMedia, M3UBufferHandle hBuffer, BOOL bAudio)
{
#ifdef	M3U_NOT_COPY_FRAME_DATA
	// Must not change the order below
	if (bAudio)
	{
		pMedia->dwCTS -= hReader->m_dwAudioLostDuration;
		hReader->m_dwCurTimeAudio = pMedia->dwCTS;
	}
	else
	{
		pMedia->dwCTS -= hReader->m_dwVideoLostDuration;
		hReader->m_dwCurTimeVideo = pMedia->dwCTS;
	}

	return FR_OK;
#else
	DWORD32		*pdwFrame;
	BYTE		**ppFrame;

	// Must not change the order below
	if (bAudio)
	{
		pMedia->m_dwCTS -= hReader->m_dwAudioLostDuration;
		hReader->m_dwCurTimeAudio = pMedia->m_dwCTS;
		//hBuffer->m_eAudioRead = READ_DOING;
		pdwFrame = &hReader->m_dwAudioFrame;
		ppFrame = &hReader->m_pAudioFrame;
	}
	else
	{
		pMedia->m_dwCTS -= hReader->m_dwVideoLostDuration;
		hReader->m_dwCurTimeVideo = pMedia->m_dwCTS;
		//hBuffer->m_eVideoRead = READ_DOING;
		pdwFrame = &hReader->m_dwVideoFrame;
		ppFrame = &hReader->m_pVideoFrame;
	}

	if (*pdwFrame < pMedia->m_dwFrameLen[0])
	{
		TRACE("[M3UReader] CopyMediaFrame - increase frame memory from %d to %d", *pdwFrame, pMedia->m_dwFrameLen[0]);
		if (*ppFrame)
			FREE(*ppFrame);
		*ppFrame = NULL;
		*ppFrame = (BYTE*)MALLOCZ(pMedia->m_dwFrameLen[0]+FF_EXTRA_READ_BYTES);
		if (!*ppFrame)
			return COMMON_ERR_MEM;
		*pdwFrame = pMedia->m_dwFrameLen[0];
	}
	memcpy(*ppFrame, pMedia->m_pFrame, pMedia->m_dwFrameLen[0]);
	pMedia->m_pFrame = *ppFrame;

	return MC_OK;
#endif
}

// Update Time related info
static VOID UpdateTimeInfo(M3UReaderHandle hReader, M3UBufferHandle hBuffer, BOOL bAudio, BOOL bFirstFrame, DWORD32 dwTargetDuration, FrMediaStream* pMedia)
{
	pMedia->bDiscontinuity = FALSE;
	if (bFirstFrame)
	{
		if (bAudio && hReader->m_dwPrevOutCTSAudio)
		{
			DWORD32		dwDiff = ABS(pMedia->dwCTS - hReader->m_dwPrevOutCTSAudio);

			if (TS_MIN_TIMESTAMP_DIFF < dwDiff)
			{
				TRACE("[Reader] M3UReader - UpdateTimeInfo - big audio cts jump %d (cur %d, prev %d)",
					dwDiff, pMedia->dwCTS, hReader->m_dwPrevOutCTSAudio);
				pMedia->bDiscontinuity = TRUE;
			}
		}
	}

	if (bAudio)
	{
		hReader->m_dwPrevOutCTSAudio = pMedia->dwCTS;
		hBuffer->m_eAudioRead = READ_DOING;
	}
	else
	{
		hReader->m_dwPrevOutCTSVideo = pMedia->dwCTS;
		hBuffer->m_eVideoRead = READ_DOING;;
	}
}

//static dwSegNum;

#define		DUMMY_VIDEO_FRAME_DURATION	100
static VOID MakeDummyVideoFrame(M3UReaderHandle hReader, FrMediaStream *pMedia)
{
	pMedia->dwFrameNum = 0;
	pMedia->dwFrameLen[0] = 0;
	pMedia->dwCTS = hReader->m_dwCurTimeVideo + DUMMY_VIDEO_FRAME_DURATION;
	hReader->m_dwCurTimeVideo = pMedia->dwCTS;
	hReader->m_dwDummyVideo++;
}

LRSLT M3UReaderReadFrame(VOID* pVoid, FrMediaStream* pMedia)
{
	M3UReaderHandle		hReader = (M3UReaderHandle)pVoid;
	M3UStreamHandle		hStream = hReader->m_phStream[hReader->m_dwCurStream];
	M3USegmentHandle	hSegment = hStream->m_phSegment[hStream->m_dwCurSegment];
	M3UBufferHandle		hBuffer = &hReader->m_Buffer[hReader->m_dwReadBuffer];
	LRSLT				lRet;
	DWORD32				dwReadBuffer=0, dwNextReadBuffer = 0;
	BOOL				bAudio, bGetInfo=FALSE, bFirstFrame=FALSE;

	//TRACE("[Reader] M3UReaderReadFrame Start Type %d", pMedia->m_tMediaType);

retry:
	if (pMedia->tMediaType == AUDIO_MEDIA)
	{
		bAudio = TRUE;
		hBuffer = &hReader->m_Buffer[hReader->m_dwCurAudioReadBuffer];
		dwReadBuffer = hReader->m_dwCurAudioReadBuffer;
		if (hBuffer->m_eAudioRead == READ_NOT_YET)
			bFirstFrame = TRUE;
	}
	else if (pMedia->tMediaType == VIDEO_MEDIA)
	{
		bAudio = FALSE;
		hBuffer = &hReader->m_Buffer[hReader->m_dwCurVideoReadBuffer];
		dwReadBuffer = hReader->m_dwCurVideoReadBuffer;
		if (hBuffer->m_eVideoRead == READ_NOT_YET)
			bFirstFrame = TRUE;
	}
	else
	{
		//TRACE("[Reader] M3UReaderReadFrame - Unknown media (%d) requested", pMedia->m_tMediaType);
		return COMMON_ERR_NOMEDIA;
	}
	//else if (pMedia->m_tMediaType == TEXT_MEDIA)
	//	hStream = &hReader->m_Stream[hReader->m_dwTextIdx];

	// No Buffer unread
	if (dwReadBuffer == hReader->m_dwWriteBuffer)
	{
		if (hReader->m_bProgressive)
		{
			DWORD32	dwBufferTime = GetBufferedDuration(hReader, FALSE);
			if (dwBufferTime == 0)
			{
				TRACE("[Reader] M3UReaderReadFrame End - no data - progressive");
				return COMMON_ERR_NODATA;
			}
		}
		else
		{
			TRACE("[Reader] M3UReaderReadFrame End - no data");
			return COMMON_ERR_NODATA;
		}
	}

	dwNextReadBuffer = dwReadBuffer + 1;
	if (MAX_M3U_BUFFER_NUM <= dwNextReadBuffer)
		dwNextReadBuffer = 0;

	// When reading a frame in the buffer first time
	// need to do get_info
	pMedia->pVideoInfoVideoConfig = NULL;
	if (!hBuffer->m_bGetInfo)
	{
		LRSLT		lRet;

//TRACE("[M3UReader]	ReaderReadFrame - GetInfo Starts (Read Buffer %d)", hReader->m_dwReadBuffer);
		lRet = SegmentReaderOpen(hReader, hBuffer, NULL);
//TRACE("[M3UReader]	ReaderReadFrame - GetInfo Ends (Read Buffer %d)", hReader->m_dwReadBuffer);
		if (FRFAILED(lRet))
		{
			TRACE("[Reader] M3UReaderReadFrame End - no data - getinfo failed");
			return COMMON_ERR_NODATA;
		}

		hBuffer->m_bGetInfo = TRUE;
		hBuffer->m_bAudio = hReader->m_bAudio;
		hBuffer->m_bVideo = hReader->m_bVideo;

		//if (hReader->m_dwCurAudioReadBuffer == 4)
		//	hBuffer->m_bVideo = FALSE;

		if (hReader->m_bAudioExist && !hBuffer->m_bAudio)
		{
			hBuffer->m_eAudioRead = READ_END;
			TRACE("[M3UReader] Segment has no audio");
		}
		if (hReader->m_bVideoExist && !hBuffer->m_bVideo)
		{
			 hBuffer->m_eVideoRead = READ_END;
			 TRACE("[M3UReader] Segment has no video");
		}

		bGetInfo = TRUE;
		hReader->m_dwDummyVideo = 0;
		// Player mode
		if (hReader->m_pInfo)
		{
			hReader->m_pInfo->FrVideo.bLowDelayDecoding = TRUE;	// to seamless come over the boundary of segments
			strcpy(hReader->m_pInfo->szFileFormat, "m3u");
			if (hReader->m_bSeek)
			{
				TRACE("[M3UReader]	m_bVideoConfigChanged set forcedly after seeking");
				CompareAndStoreVideoConfig(hReader, hReader->m_pInfo);
				hBuffer->m_bVideoConfigChanged = TRUE;
				hReader->m_bSeek = FALSE;
			}
			else
			if (CompareAndStoreVideoConfig(hReader, hReader->m_pInfo))
			{
				TRACE("[M3UReader]	Read Video Stream Changed");
				hBuffer->m_bVideoConfigChanged = TRUE;
			}
		}

		//TRACE("[M3UReader] config size %d", hReader->m_pInfo->m_Video[0].m_dwConfig);

		// always reopen video decoder when meeting a new segment
		//hBuffer->m_bVideoConfigChanged = TRUE;
	}

	// Handle with the case ts segment has only one stream
	if ( (bAudio && hBuffer->m_eAudioRead == READ_END) ||
		 (!bAudio && hBuffer->m_eVideoRead == READ_END) )
	{
		CheckEndOfSegment(hBuffer, bAudio);
		if (hBuffer->m_bLastSegment)
			return COMMON_ERR_ENDOFDATA;
		TRACE("[M3UReader] ReadFrame - end of segment - due to No audio or video");
		UpdateReadBuffer(hReader, bAudio, dwNextReadBuffer);

		TRACE("[M3UReader] ReadFrame Set start CTS %5d", pMedia->dwCTS);
		hBuffer->m_dwStartCTS = pMedia->dwCTS;

		goto retry;
	}

	// Segment is not valid
	if (hBuffer->m_bBadSegment || !hBuffer->m_hSegReader)
	{
		CheckEndOfSegment(hBuffer, TRUE);
		CheckEndOfSegment(hBuffer, FALSE);
		if (hBuffer->m_bLastSegment)
			return COMMON_ERR_ENDOFDATA;
		UpdateReadBuffer(hReader, bAudio, dwNextReadBuffer);
		goto retry;
	}
	else
	{
		// Make dummy video frame to keep the last decoded frame displaying
		if (hReader->m_bVideoExist && !bAudio && !hBuffer->m_bVideo)
		{
			MakeDummyVideoFrame(hReader, pMedia);
			hBuffer->m_eVideoRead = READ_DOING;
			if (hStream->m_dwTargetDuration/DUMMY_VIDEO_FRAME_DURATION <= hReader->m_dwDummyVideo)
			{
				hBuffer->m_eVideoRead = READ_END;
				if (hBuffer->m_bLastSegment)
					return COMMON_ERR_ENDOFDATA;
				UpdateReadBuffer(hReader, bAudio, dwNextReadBuffer);
				goto retry;
			}

			TRACE("[M3UReader]	Bad Segment is %d or Segment Reader handle is 0x%x", hBuffer->m_bBadSegment, hBuffer->m_hSegReader);
			return COMMON_ERR_NOMEDIA;
		}
		else
			lRet = FrReaderReadFrame(hBuffer->m_hSegReader, pMedia);
	}

	// Move next segment
	if (FRFAILED(lRet))
	{
		if (lRet == COMMON_ERR_ENDOFDATA)
		{
			if (hBuffer->m_dwBuffer == hBuffer->m_dwBufferPos)
			{
				CheckEndOfSegment(hBuffer, bAudio);
				if (hBuffer->m_bLastSegment)
					return COMMON_ERR_ENDOFDATA;

				TRACE("[M3UReader] ReadFrame - end of segment");
				UpdateReadBuffer(hReader, bAudio, dwNextReadBuffer);
				goto retry;
			}
			else
			{
				TRACE("[M3UReader] ReadFrame - NoData");
				FrSleep(10);
				return COMMON_ERR_NODATA;
			}
		}

		TRACE("[M3UReader] ReadFrame - error 0x%x", lRet);
		return lRet;
	}
	else
	{
		if (pMedia->tMediaType == VIDEO_MEDIA)
		{
			// Check wrap-around of TimeStamp
			if (pMedia->dwCTS < hReader->m_dwPrevTsCTSVideo)
				if (TS_MAX_TIMESTAMP_InMsec/2 < ABS(pMedia->dwCTS - hReader->m_dwPrevTsCTSVideo))
				{
					TRACE("[Reader] M3UReaderReadFrame - meet a video CTS wrap around (%d) - cur %d prev %d",
						hReader->m_dwWrapCountVideo, pMedia->dwCTS, hReader->m_dwPrevTsCTSVideo);
					hReader->m_dwWrapCountVideo++;
				}
			hReader->m_dwPrevTsCTSVideo = pMedia->dwCTS;
			// Wrap around taken into consideration
			pMedia->dwCTS += TS_MAX_TIMESTAMP_InMsec*hReader->m_dwWrapCountVideo;

			/*if (pMedia->eTimeStamp == TIMESTAMP_PTS_DTS)
			{
				if (pMedia->m_dwDTS[0] < hReader->m_dwPrevTsDTSVideo)
					if (TS_MAX_TIMESTAMP_InMsec/2 < ABS(pMedia->m_dwDTS[0] - hReader->m_dwPrevTsDTSVideo))
					{
						TRACE("[Reader] M3UReaderReadFrame - meet a video DTS wrap around (%d) - cur %d prev %d",
							hReader->m_dwWrapCountVideoDTS, pMedia->m_dwDTS[0], hReader->m_dwPrevTsDTSVideo);
						hReader->m_dwWrapCountVideoDTS++;
					}

				hReader->m_dwPrevTsDTSVideo = pMedia->m_dwDTS[0];
				pMedia->m_dwDTS[0] +=  TS_MAX_TIMESTAMP_InMsec*hReader->m_dwWrapCountVideoDTS;;
			}*/
		}
		else if (pMedia->tMediaType == AUDIO_MEDIA)
		{
			// Check wrap-around of TimeStamp
			if (pMedia->dwCTS < hReader->m_dwPrevTsCTSAudio)
				if (TS_MAX_TIMESTAMP_InMsec/2 < ABS(pMedia->dwCTS - hReader->m_dwPrevTsCTSAudio))
				{
					TRACE("[Reader] M3UReaderReadFrame - meet a Audio CTS wrap around (%d) - cur %d prev %d",
						hReader->m_dwWrapCountAudio, pMedia->dwCTS, hReader->m_dwPrevTsCTSAudio);
					hReader->m_dwWrapCountAudio++;
				}
			hReader->m_dwPrevTsCTSAudio = pMedia->dwCTS;
			// Wrap around taken into consideration
			pMedia->dwCTS += TS_MAX_TIMESTAMP_InMsec*hReader->m_dwWrapCountAudio;
		}

		if (!bAudio)
		{
			// Player mode
			if (hReader->m_pInfo && hBuffer->m_bVideoConfigChanged)
			{
				pMedia->pVideoInfoVideoConfig = &hReader->m_pInfo->FrVideo;
				hBuffer->m_bVideoConfigChanged = FALSE;
				TRACE("[M3UReader]	Video Stream Changed Applied Width %d Height %d",
					pMedia->pVideoInfoVideoConfig->dwWidth, pMedia->pVideoInfoVideoConfig->dwHeight);
				hReader->m_pInfo->FrVideo.dwDuration = hReader->m_dwDuration;
			}
		}

		// Make CTS increase from 0
		if (hReader->m_dwStartTime <= pMedia->dwCTS)
			pMedia->dwCTS -= hReader->m_dwStartTime;
		else
			pMedia->dwCTS = 0;

		/*if (pMedia->m_eTimeStamp == TIMESTAMP_PTS_DTS)
		{
			if (hReader->m_dwStartTime <= pMedia->m_dwDTS[0])
				pMedia->m_dwDTS[0] -= hReader->m_dwStartTime;
			else
				pMedia->m_dwDTS[0] = 0;
		}*/
	}

	UpdateTimeInfo(hReader, hBuffer, bAudio, bFirstFrame, hStream->m_dwTargetDuration, pMedia);
	CopyMediaFrame(hReader, pMedia, hBuffer, bAudio);

	//TRACE("[M3UReader]	CTS %d from Reader", pMedia->m_dwCTS);
	hBuffer->m_dwCurCTS = pMedia->dwCTS;

	// To store CTS for calculation of buffer fullness.
	// Should be after UpdateTimeInfo() and CopyMediaFrame()
	if (bGetInfo)
	{
		bGetInfo = FALSE;
		TRACE("[M3UReader] ReadFrame Set start CTS %5d", pMedia->dwCTS);
		hBuffer->m_dwStartCTS = pMedia->dwCTS;
	}

#if 1
	//TRACE("[Reader] M3UReaderReadFrame End");

	if (bFirstFrame || 0)
	{
	if (pMedia->tMediaType == VIDEO_MEDIA && 0)
		TRACE("[M3UReader]	Read Video Size=%5d CTS=%7d", pMedia->dwFrameLen[0], pMedia->dwCTS);
	if (pMedia->tMediaType == AUDIO_MEDIA && 0)
		TRACE("[M3UReader]	Read Audio Size=%5d CTS=%7d", pMedia->dwFrameLen[0], pMedia->dwCTS);
	}
#endif
	return	FR_OK;
}

BOOL M3UReaderSeek(VOID* pVoid, DWORD32* pdwCTS, BOOL bKeyFrame)
{
	return	!M3UReaderSeekByTime(pVoid, pdwCTS, bKeyFrame);
}

BOOL M3UReaderSeekByTime(VOID* pVoid, DWORD32 *pdwCTS, BOOL bKeyFrame)
{
	M3UReaderHandle	hReader = (M3UReaderHandle)pVoid;
	M3UStreamHandle	hStream = hReader->m_phStream[hReader->m_dwCurStream];
	LRSLT			lRet = FR_OK;
	DWORD32			i, dwCurSegment;

	TRACE("[M3UReader]	M3UReaderSeekByTime - start with CTS %d", *pdwCTS);
	//*pdwCTS = 2492000;

	if (hReader->m_bLive)
	{
		TRACE("[M3UReader]	M3UReaderSeekByTime - Seek is not allowed, since source is live.");
		//return COMMON_ERR_FUNCTION;
		return FALSE;
	}

	if (hReader->m_dwDuration <= *pdwCTS)
	{
		TRACE("[M3UReader]	M3UReaderSeekByTime - Seek failed. Out of range.");
		//return COMMON_ERR_INVALID_RANGE;
		return FALSE;
	}

	// Wait for the Tasks to stop
	hReader->m_bReaderStop = TRUE;
	hReader->m_eTaskStatus = SEG_TASK_SEEK;
	i = 0;
	while(hReader->m_eTaskStatus != SEG_TASK_PAUSED)
	{
		FrSleep(10);
		if (++i < MAX_WAIT_TIME/10)
			continue;
		else
			break;
	}
	//TRACE("[M3UReader]	M3UReaderSeekByTime - SEG_TASK_PAUSED %d", hReader->m_eTaskStatus);
	i = 0;
	while(hReader->m_hGetOneSegmentTask != INVALID_TASK)
	{
		FrSleep(10);
		if (++i < MAX_WAIT_TIME/10)
			continue;
		else
			break;
	}
	//TRACE("[M3UReader]	M3UReaderSeekByTime - GetOneSegmentTask - deleted (0x%x)", hReader->m_hGetOneSegmentTask);

	for (i=0; i<hStream->m_dwSegmentUsedEnd; i++)
		if (*pdwCTS <= hStream->m_phSegment[i]->m_dwStartCTS+hStream->m_phSegment[i]->m_dwDuration)
		{
			*pdwCTS = hStream->m_phSegment[i]->m_dwStartCTS;
			break;
		}

	// move down by 1. which will be increased by GetOneMoreSegment()
	dwCurSegment = i;
	for (i=0; i<hReader->m_dwStream; i++)
	{
		hStream = hReader->m_phStream[i];
		hStream->m_dwCurSegment = dwCurSegment;
	}

	EmptyBuffers(hReader);

	hReader->m_dwCurTimeAudio = hReader->m_dwCurTimeVideo = *pdwCTS;
	hReader->m_dwAudioLostDuration = hReader->m_dwVideoLostDuration = 0;

	// Resume
	hReader->m_eTaskStatus = SEG_TASK_RUN;

	// Wait for a segment to be downloaded
	i = 0;
	while(!hReader->m_dwWriteBuffer)
	{
		FrSleep(10);
		if (++i < MAX_WAIT_TIME/100)
			continue;
		else
			break;
	}
	//TRACE("[M3UReader]	M3UReaderSeekByTime - Get a segment download - (buffer num %d)", hReader->m_dwWriteBuffer);
	if(!hReader->m_dwWriteBuffer)
	{
		TRACE("[M3UReader]	M3UReaderSeekByTime - failed");
		//return COMMON_ERR_FOPEN;
	}
	hReader->m_bSeek = TRUE;
	hReader->m_dwPrevTsCTSAudio = hReader->m_dwPrevTsCTSVideo =
		hReader->m_dwPrevOutCTSAudio = hReader->m_dwPrevOutCTSVideo =
		hReader->m_dwPrevTsDTSVideo = 0;
	hReader->m_dwWrapCountAudio = hReader->m_dwWrapCountVideo =
		hReader->m_dwWrapCountVideoDTS = 0;

	TRACE("[M3UReader]	M3UReaderSeekByTime - ends with CTS %d", *pdwCTS);

	return	TRUE;
}

static DWORD32 GetRemainingDuration(M3UReaderHandle hReader)
{
	M3UStreamHandle		hStream;
	M3UBufferHandle		hBuffer;
	M3USegmentHandle	hSegment;
	DWORD32				dwStartCTS;

	if (hReader->m_bLive)
		return MAX_DWORD/2;
	else
	{
		hStream = hReader->m_phStream[hReader->m_dwCurStream];
		hBuffer = &hReader->m_Buffer[hReader->m_dwReadBuffer];
		hSegment = hStream->m_phSegment[hBuffer->m_dwSeqNumber];

		dwStartCTS = MAX(hReader->m_dwCurTimeAudio, hReader->m_dwCurTimeVideo);

		if (dwStartCTS < hReader->m_dwDuration)
			return hReader->m_dwDuration - dwStartCTS;
		else
			return 1;
	}
}

BOOL M3UReaderGetStatus(VOID* pVoid, VOID* p)
{
	M3UReaderHandle		hReader = (M3UReaderHandle)pVoid;
	M3UStreamHandle		hStream;
	FrReaderStatus*		pStatus = (FrReaderStatus*)p;
	DWORD32				dwBufferingRatio = 100;
	DWORD32				dwRemainDur, dwDur;
	DWORD32				dwBufferedTime = MAX_DWORD,
						dwBufferingTime;

	// When in relay mode
	if (hReader->m_bMemOptimize)
	{
		 hReader->m_dwBufferRatio = GetBufferedSegments(hReader)/2*100;
		 if (100 < hReader->m_dwBufferRatio)
			 hReader->m_dwBufferRatio = 100;

	 	pStatus->m_dwBufferingRatio = pStatus->m_dwAudioBuffRatio = pStatus->m_dwVideoBuffRatio = hReader->m_dwBufferRatio;
		return TRUE;

	}

	// Set : Necessary buffering time
	//pStatus->m_dwCurFSizeRatio = 100;
	if (hReader->m_bLive && hReader->m_bLiveBuffTime)
	{
		if (pStatus->m_eMode == READER_BUFFERING_INIT)
			dwBufferingTime = hReader->m_dwInitBuffTimeLive;
		else if (pStatus->m_eMode == READER_BUFFERING_SEEK)
			dwBufferingTime = hReader->m_dwSeekBuffTimeLive;
		else
			dwBufferingTime = hReader->m_dwBuffTimeLive;
	}
	else
	{
		if (pStatus->m_eMode == READER_BUFFERING_INIT)
			dwBufferingTime = hReader->m_dwInitBuffTime;
		else if (pStatus->m_eMode == READER_BUFFERING_SEEK)
			dwBufferingTime = hReader->m_dwSeekBuffTime;
		else
			dwBufferingTime = hReader->m_dwBuffTime;
	}

	// Check Duration
	dwRemainDur = GetRemainingDuration(hReader);
	if (hReader->m_bAudioExist)
	{
		hStream = hReader->m_phStream[0];
		dwDur =  GetBufferedDuration(hReader, FALSE);

		// Keep the amount of data in the buffer strictly
		//dwDur += pStatus->m_dwBufferBaseAudio;

		// No buffering
		if (dwBufferingTime == 0)
			pStatus->m_dwAudioBuffRatio = 100;
		// At the end of stream
		else if (dwRemainDur <= 2000 && 100 <= dwDur)
			pStatus->m_dwAudioBuffRatio = 100;
		else if (dwBufferingTime <= dwRemainDur)
			pStatus->m_dwAudioBuffRatio = dwDur*100/dwBufferingTime;
		else
			pStatus->m_dwAudioBuffRatio = dwDur*100/dwRemainDur;

		if (pStatus->m_dwAudioBuffRatio < dwBufferingRatio)
			dwBufferingRatio = pStatus->m_dwAudioBuffRatio;
		//TRACE("[PSReader]			AudioBuff = %d%%", pStatus->m_dwAudioBuffRatio);

		dwBufferedTime = MIN(dwBufferedTime, dwDur);
		pStatus->m_dwAudioBufferedTime = dwDur;
	}
	else
	{
		pStatus->m_dwAudioBuffRatio = 0;
		pStatus->m_dwAudioBufferedTime = 0;
	}

	if (hReader->m_bVideoExist)
	{
		hStream = hReader->m_phStream[0];
		dwDur =  GetBufferedDuration(hReader, FALSE);

		// Keep the amount of data in the buffer strictly
		//dwDur += pStatus->m_dwBufferBaseVideo;

		// No buffering
		if (dwBufferingTime == 0)
			pStatus->m_dwVideoBuffRatio = 100;
		// At the end of stream
		else if (dwRemainDur <= 2000 && 100 <= dwDur)
			pStatus->m_dwVideoBuffRatio = 100;
		else if (dwBufferingTime <= dwRemainDur)
			pStatus->m_dwVideoBuffRatio = dwDur*100/dwBufferingTime;
		else
			pStatus->m_dwVideoBuffRatio = dwDur*100/dwRemainDur;

		if (pStatus->m_dwVideoBuffRatio < dwBufferingRatio)
			dwBufferingRatio = pStatus->m_dwVideoBuffRatio;
		//TRACE("[PSReader]			VideoBuff = %d%%", pStatus->m_dwVideoBuffRatio);

		dwBufferedTime = MIN(dwBufferedTime, dwDur);
		pStatus->m_dwVideoBufferedTime = dwDur;
	}
	else
	{
		pStatus->m_dwVideoBuffRatio = 0;
		pStatus->m_dwVideoBufferedTime = 0;
	}

	pStatus->m_lError = hReader->m_lError;
	pStatus->m_dwBufferingRatio = dwBufferingRatio;
	pStatus->m_dwBufferedTime = dwBufferedTime;

	if (hReader->m_dwBufferRatio < 10)
	{
		GetBufferedDuration(hReader, FALSE);
	}

	return	TRUE;
}

LRSLT M3UReaderSetStatus(VOID* pVoid, INT32 iStatus, INT32 nScale, QWORD qwCurFileSize)
{
	M3UReaderHandle	hReader = (M3UReaderHandle)pVoid;

	if (iStatus == READ_PLAY)
	{
		if (hReader->m_eTaskStatus == SEG_TASK_PAUSED ||
			hReader->m_eTaskStatus == SEG_TASK_IDLE)
			hReader->m_eTaskStatus = SEG_TASK_RUN;
	}
	else if (iStatus == READ_PAUSE)
	{
		if (hReader->m_eTaskStatus == SEG_TASK_RUN)
		{
			hReader->m_eTaskStatus = SEG_TASK_PAUSE;
		}
	}
	else if (iStatus == READ_SCALE)
	{
	}
	else if (iStatus == READ_STOP)
	{
		hReader->m_bReaderStop = TRUE;
	}

	return	FR_OK;
}

static _INLINE_ DWORD32 estimateRemainTime(DWORD32 dwTickStart, DWORD32 dwDuration, DWORD32 dwContent, DWORD32 dwRecv, DWORD32 dwLeast)
{
	DWORD32		dwTime = FrGetTickCount();
	DWORD32		dwRemainSize = dwContent - dwRecv;
	DWORD32		dwBandwidth=0, dwFinishTime=0;

	if (dwRemainSize < dwLeast)
		return 0;

	if (dwTickStart<dwTime)
		dwTime = dwTime - dwTickStart;
	else
		return 0;

	// hReader->m_dwTargetDuration/2 goes since the function starts
	if (dwDuration/2 < dwTime)
	{
		dwBandwidth = dwRecv*1000/dwTime+1;						// in seconds
		dwFinishTime = (dwContent - dwRecv)*1000/dwBandwidth;	// in miliseconds
	}

	return dwFinishTime;
}

static VOID closeSocketInSync(SOCK_HANDLE* hCurSock, SOCK_HANDLE* hInSock)
{
	if (hCurSock)
	{
		McSocketClose(*hCurSock);
		*hCurSock = NULL;
	}
	if (hInSock)
		*hInSock = NULL;

	return;
}

#define		GetHttpMsgLen		2048
static LRSLT GetHttpFile(
	SOCK_HANDLE		*hInSocket,			// [in, Out] Socket to be used or Socekt opened
	const TCHAR		*pszUrl,			// [in]      Url name
	BYTE			**ppbFile,			// [Out]	 Receive data pointer
	DWORD32			*pdwFile,			// [Out]	 Receive data size
	BOOL			bSegment,			// [In]		 Segment or M3U
	M3UReaderHandle hReader)			// [In]		 Reader handle
{
    LRSLT			lRet;
    INT32			nMsg;
    BYTE			*strMsg;
    BYTE			szResponseCode[32] = "";
    BYTE			*pHTTPAddr=NULL, *pHTTPFile=NULL, *pHeaderEnd=NULL, *pFind=NULL;
    WORD			wHTTPPort;
    DWORD32			dwContentLength=0, dwBodySize, dwHeaderSize, dwRetryCnt=0;
    DWORD32			dwBufferedTime=0;
	SOCK_HANDLE		hSocket = NULL;
	BOOL			bServerKeepAlive = FALSE, bSocketOpened = FALSE, bClientKeepAlive = FALSE;
	BOOL			bBufferFull=FALSE;
    BOOL            bSecureConnect=FALSE;
	M3UBufferHandle		hBuffer = &hReader->m_Buffer[hReader->m_dwWriteBuffer];

	// get a start tick
	DWORD32			dwTickStart = FrGetTickCount();

    TRACE("[M3UReader] GetHttpFile enter [%s]", pszUrl);

	// Turn on Keep-Alive for segments
	if (hInSocket)
		bClientKeepAlive = TRUE;

	*ppbFile = NULL;
	*pdwFile = 0;
	strMsg = (BYTE*)MALLOCZ(GetHttpMsgLen+2);

retry_after_reopen:
	if (hReader->m_bReaderStop || hReader->m_bEnd)
	{
		TRACE("[M3UReader] GetHttpFile stopped externally");
		lRet = COMMON_ERR_CANCEL;
		goto failed;
	}

	// Update buffer ratio
	UpdateBufferRatio(hReader, FALSE);
	bBufferFull	= hReader->m_dwBufferRatio == 100;

	pHTTPAddr = (BYTE*)UtilURLGetAddr((char*)pszUrl);
    pHTTPFile = (BYTE*)UtilURLGetContentLocation((char*)pszUrl);
    wHTTPPort = UtilURLGetPort(pszUrl);
    if (!wHTTPPort && !UtilStrStrnicmp(pszUrl, "HTTP://", 7)) {
        wHTTPPort = 80;
        bSecureConnect = FALSE;
    }
    else if (!wHTTPPort && !UtilStrStrnicmp(pszUrl, "HTTPS://", 8)) {
        wHTTPPort = 443;
        bSecureConnect = TRUE;
    }
    else if (wHTTPPort != 0 && !UtilStrStrnicmp(pszUrl, "HTTPS://", 8)) {
        bSecureConnect = TRUE;
    }
    else {
        bSecureConnect = FALSE;
    }

    if(!pHTTPAddr || !pHTTPFile || !wHTTPPort) {
        TRACE("[M3UReader] GetHttpFile - Invalid url(%s)", pszUrl);
        lRet = COMMON_ERR_NULLPOINTER;
		goto failed;
    }
    TRACE("Addr(%s), Port(%d), File(%s) - File Mem 0x%x", pHTTPAddr, wHTTPPort, pHTTPFile, *ppbFile);

    if(hInSocket && McSocketIsValid(*hInSocket))
	{
		hSocket = *hInSocket;
		TRACE("[M3UReader] GetHttpFile - input socket %d", (LPPTR)hSocket);
	}
	else {
        //lRet = McSocketTcpOpen(&hSocket, (char*)pHTTPAddr, wHTTPPort, FALSE, 0);
		//lRet = McSocketTcpOpen(&hSocket, (char*)pHTTPAddr, wHTTPPort, FALSE, hReader->m_dwConnectTimeOut);
		lRet = McSocketTcpOpen2(&hSocket, (char*)pHTTPAddr, wHTTPPort, FALSE, hReader->m_dwConnectTimeOut, bSecureConnect);
        if (FRFAILED(lRet)) {
            TRACE("[M3UReader] GetHttpFile - Connect Error");
            goto failed;
        }
		if (hInSocket)
			*hInSocket = hSocket;
        TRACE("[M3UReader] GetHttpFile - Connect(%s:%d)", pHTTPAddr, wHTTPPort);
    }

    nMsg = 0;
    //memset(strMsg, 0x00, sizeof(strMsg));
	memset(strMsg, 0x00, GetHttpMsgLen+2);
	nMsg  =  sprintf((char*)strMsg, "GET %s HTTP/1.1\r\n", pHTTPFile);
    nMsg += sprintf((char*)strMsg+nMsg, "Host: %s\r\n", pHTTPAddr);

	// Partial request :: Range
	if (*ppbFile)
	{
		TRACE("[M3UReader] GetHttpFile - resume Connect(%s:%d)", pHTTPAddr, wHTTPPort);

		if (dwBodySize && dwContentLength && (dwBodySize < dwContentLength) )
			nMsg += sprintf((char*)strMsg+nMsg, "Range: bytes=%u-%u\r\n", dwBodySize, dwContentLength);
		else
		{
			TRACE("[M3UReader] GetHttpFile - resume with 0 start position ");
			dwBodySize = 0;
		}
	}

	nMsg += sprintf((char*)strMsg+nMsg, bClientKeepAlive ? "Connection: Keep-Alive\r\n" : "Connection: close\r\n");
    nMsg += sprintf((char*)strMsg+nMsg, "\r\n");
	TRACE("[M3UReader] GetHttpFile - nMsg %d", nMsg);
    FREE(pHTTPAddr);	pHTTPAddr = NULL;
    FREE(pHTTPFile);	pHTTPFile = NULL;

    lRet = McSocketSend(hSocket, (char*)strMsg, nMsg);
	//lRet = McSocketSendNonBlock(hSocket, (char*)strMsg, nMsg, 0);
    if (FRFAILED(lRet))
	{
        TRACE("[M3UReader] GetHttpFile - Send Request Error");

		TRACE("[M3UReader] GetHttpFile - socket closing - error 0x%x", lRet);
		closeSocketInSync(&hSocket, hInSocket);

		if (hReader->m_dwRetryCount < dwRetryCnt++)
		{
			TRACE("[M3UReader] GetHttpFile - Retry to McSocketSend %d times. But failed", dwRetryCnt);
			goto failed;
		}

		TRACE("[M3UReader] GetHttpFile - McSocketSend");
		FrSleep(100);
		goto retry_after_reopen;
    }
    //TRACEX(DTB_LOG_INFO, "[M3UReader] GetHttpFile - Send Request(%d)\n%s", lRet, strMsg);

	// Wait
	lRet = McSocketWait(hSocket, hReader->m_dwRecvTimeOut);
	if (FRFAILED(lRet))
	{
		TRACE("[M3UReader] GetHttpFile - It's failed to get a response. (0x%x). Msg Timeout : %d msec!",
			lRet, hReader->m_dwRecvTimeOut);
		closeSocketInSync(&hSocket, hInSocket);

		if (lRet == (LRSLT)COMMON_ERR_TIMEOUT)
			lRet = (LRSLT)COMMON_ERR_SVR_NORESPONSE;
		else
			lRet = (LRSLT)COMMON_ERR_SVR_DISCONNECT;

		if (hReader->m_dwRetryCount < dwRetryCnt++)
		{
			TRACE("[M3UReader] GetHttpFile - Retry to McSocketWait 5 times. But failed");
			goto failed;
		}

		TRACE("[M3UReader] GetHttpFile - McSocketWait");
		FrSleep(100);
		goto retry_after_reopen;
	}

    //memset(strMsg, 0x00, sizeof(strMsg));
    //lRet = McSocketRecv(hSocket, (char*)strMsg, sizeof(strMsg)-1);
	memset(strMsg, 0x00, GetHttpMsgLen+2);
    lRet = McSocketRecv(hSocket, (char*)strMsg, GetHttpMsgLen);
    if (FRFAILED(lRet)) {
        TRACE("[M3UReader] GetHttpFile - Recv Response Error 1");
		closeSocketInSync(&hSocket, hInSocket);

        if (hReader->m_dwRetryCount < dwRetryCnt++)
		{
			TRACE("[M3UReader] GetHttpFile - Retry to McSocketRecv 5 times. But failed");
			goto failed;
		}

		TRACE("[M3UReader] GetHttpFile - McSocketRecv");
		FrSleep(100);
		goto retry_after_reopen;
	}
    //TRACEX(DTB_LOG_INFO, "[M3UReader] GetHttpFile - Recv Response(%d)\n%s", lRet, strMsg);

    pHeaderEnd = (BYTE*)strstr((char*)strMsg, "\r\n\r\n");
    if(pHeaderEnd == NULL) {
        TRACE("[M3UReader] GetHttpFile - Invaild message type");
        lRet = COMMON_ERR_BADRESPONSE;
		goto failed;
    }

    if(!sscanf((char*)strMsg, "%*s %10s", szResponseCode)) {
        TRACE("[M3UReader] GetHttpFile - Missing Object (code) in HTTP response");
        lRet = COMMON_ERR_BADRESPONSE;
		goto failed;
    }
	else
	{
		DWORD32		dwStatusCode = atoi((char*)szResponseCode);
		if (dwStatusCode != 200 && dwStatusCode != 206)
		{
			if (500 <= dwStatusCode)
				lRet = COMMON_ERR_INTERNALSVR;
			else
				lRet = COMMON_ERR_BADREQUEST;
			TRACE("[M3UReader] GetHttpFile - http response error StatusCode(%d)", dwStatusCode);
			goto failed;
		}
	}

	pFind = (BYTE*)UtilStrIFindString((char*)strMsg, (char*)"Keep-Alive");
    if(pFind)
        bServerKeepAlive = TRUE;
	//bServerKeepAlive = FALSE;

    pFind = (BYTE*)UtilStrIFindString((char*)strMsg, (char*)"Content-Length");
    if(pFind == NULL) {
        TRACE("[M3UReader] GetHttpFile - Invaild message type");
        lRet = COMMON_ERR_BADRESPONSE;
		goto failed;
    }

	// get header size to copy data in the 1st reception
	dwHeaderSize = pHeaderEnd+4 - strMsg;
	dwRetryCnt = 0;

	// Come here 1st time
	if (*ppbFile == NULL)
	{
		// Get ContentLength
		dwContentLength = 0;
	    UtilStrGetDecNumber((char*)pFind, NULL, &dwContentLength);
		TRACE("ContentLength(%d)", dwContentLength);


		if (dwContentLength==0)
		{
			TRACE("[M3UReader] GetHttpFile - not support 0 conent length");
			lRet = COMMON_ERR_STREAMSYNTAX;
			goto failed;
		}

		if (bSegment && hReader->m_bProgressive)
		{
			M3UStreamHandle		hStream = hReader->m_phStream[hReader->m_dwCurStream];
			M3USegmentHandle	hSegment = hStream->m_phSegment[hStream->m_dwCurSegment];

			// To double check if the buffer is freed
			FREE(hBuffer->m_pBuffer);
			hBuffer->m_pBuffer = (BYTE *)MALLOC(dwContentLength+188);

			// store segment file which is just read or downloaded via http
			hBuffer->m_dwBuffer = dwContentLength;
			hBuffer->m_dwBufferPos = 0;
			hBuffer->m_dwStartCTS = hSegment->m_dwStartCTS;					// Segment start CTS in case of vod. for live, must be updated right after get_info()
			hBuffer->m_dwDuration = hSegment->m_dwDuration;					// duration
			hBuffer->m_dwSeqNumber = hSegment->m_dwSeqNumber;				// relevant sequence number in case of live
			hBuffer->m_dwStartCTSInfo = hSegment->m_dwStartCTS;				// Segment start CTS in case of vod. for live, must be updated right after get_info()
			hBuffer->m_dwCurCTS = hSegment->m_dwStartCTS;					// Start cts

			// copy
			*ppbFile = hBuffer->m_pBuffer;
		}
		else
		{
			*ppbFile = (BYTE *)MALLOC(dwContentLength+188);
			if (bSegment)
				hBuffer->m_dwBufferPos = 0;
		}

		if(*ppbFile == NULL)
		{
			TRACE("[M3UReader] GetHttpFile - Memory allocation error");
			lRet = COMMON_ERR_MEM;
			goto failed;
		}

		*pdwFile = dwContentLength;
	    memset(*ppbFile, 0x00, dwContentLength+188);
		dwBodySize = 0;
		if ((INT32)dwHeaderSize < lRet)
		{
			dwBodySize = lRet - dwHeaderSize;
			memcpy(*ppbFile, pHeaderEnd+4, dwBodySize);
			if (bSegment && hReader->m_bProgressive)
			{
				hBuffer->m_dwBufferPos += dwBodySize;
				FrEnterMutex(&hReader->m_hMutex);
				if (hBuffer->m_hSegReader)
				{
					ReaderHandle hReader2 = (ReaderHandle)hBuffer->m_hSegReader;
					if (hReader2->ReaderSetStatus)
					{
						//TRACE("SetStatus 1 : Buffer %5d  Len %5d", hBuffer->m_dwSeqNumber, hBuffer->m_dwBufferPos);
						hReader2->ReaderSetStatus(hReader2->m_hReader, READ_INFO, 0, hBuffer->m_dwBufferPos);
					}
				}
				FrLeaveMutex(&hReader->m_hMutex);
			}
		}
		TRACE("[M3UReader] GetHttpFile - start to recv new file - HeaderSize(%d), BodySize(%d)",
			dwHeaderSize, dwBodySize);

	}
	// Reconnect during recving
	else
	{
		DWORD32		dwData;

		TRACE("[M3UReader] GetHttpFile - resume to recv - current HeaderSize(%d), BodySize(%d)/ContentLength(%d)",
			dwHeaderSize, dwBodySize, dwContentLength);

		if ((INT32)dwHeaderSize < lRet)
		{
			dwData = lRet - dwHeaderSize;
			memcpy(*ppbFile + dwBodySize, pHeaderEnd+4, dwData);
			dwBodySize += dwData;
			if (bSegment && hReader->m_bProgressive)
			{
				hBuffer->m_dwBufferPos += dwData;
				FrEnterMutex(&hReader->m_hMutex);
				if (hBuffer->m_hSegReader)
				{
					ReaderHandle hReader2 = (ReaderHandle)hBuffer->m_hSegReader;
					if (hReader2->ReaderSetStatus)
					{
						//TRACE("SetStatus 2 : Buffer %5d  Len %5d", hBuffer->m_dwSeqNumber, hBuffer->m_dwBufferPos);
						hReader2->ReaderSetStatus(hReader2->m_hReader, READ_INFO, 0, hBuffer->m_dwBufferPos);
					}
				}
				FrLeaveMutex(&hReader->m_hMutex);
			}

		}
	}

    while(dwBodySize < dwContentLength)
    {
		//TRACE("Content len %d", dwBodySize);

		// Update buffer ratio
		dwBufferedTime = UpdateBufferRatio(hReader, FALSE);

		// when a segment is not the stream with lowest bitrate
		if (bBufferFull && bSegment && hReader->m_dwCurStream)
		{
			DWORD32		dwRemainTime;

			dwRemainTime = estimateRemainTime(dwTickStart, hReader->m_phStream[hReader->m_dwCurStream]->m_dwTargetDuration,
				dwContentLength, dwBodySize, hReader->m_phStream[0]->m_dwAvgSegment);

			if (dwBufferedTime < dwRemainTime)
			{
				TRACE("[M3UReader] GetHttpFile - estimated that it is going to drop to BUFFERING. Finish receiving");
				lRet = FR_FAIL;
				goto failed;
			}
		}

		if (hReader->m_eTaskStatus == SEG_TASK_END ||
			hReader->m_bReaderStop == TRUE)
		{
			TRACE("[M3UReader] GetHttpFile - socket closing - external command");
			lRet = FR_OK;
			goto failed;
		}

		lRet = McSocketWait(hSocket, hReader->m_dwRecvTimeOut);
		if (FRFAILED(lRet))
		{
			if (lRet == (LRSLT)COMMON_ERR_TIMEOUT)
			{
				TRACE("[M3UReader] GetHttpFile (%s) - Time Out error", pszUrl);
				FrSleep(10);
				continue;
			}

			lRet = (LRSLT)COMMON_ERR_SVR_DISCONNECT;
            TRACE("[M3UReader] GetHttpFile - Recv Response Error - wait");
			goto failed;
		}

        lRet = McSocketRecv(hSocket, (char*)*ppbFile+dwBodySize, dwContentLength-dwBodySize);
		if (FRFAILED(lRet))
		{
			lRet = (LRSLT)COMMON_ERR_SVR_DISCONNECT;
            TRACE("[M3UReader] GetHttpFile - Recv Response Error - recv");
			TRACE("[M3UReader] GetHttpFile - Recv Response(%d/%d)", dwBodySize, dwContentLength);
			closeSocketInSync(&hSocket, hInSocket);

			if (hReader->m_dwRetryCount < dwRetryCnt++)
			{
				TRACE("[M3UReader] GetHttpFile - Retry to McSocketRecv %d times. But failed", dwRetryCnt);
				goto failed;
			}
			FrSleep(100);
			goto retry_after_reopen;
		}
        dwBodySize += lRet;
		if (bSegment && hReader->m_bProgressive)
		{
			hBuffer->m_dwBufferPos += lRet;
			FrEnterMutex(&hReader->m_hMutex);
			if (hBuffer->m_hSegReader)
			{
				ReaderHandle hReader2 = (ReaderHandle)hBuffer->m_hSegReader;
				if (hReader2->ReaderSetStatus)
				{
					//TRACE("SetStatus 3 : Buffer %5d  Len %5d", hBuffer->m_dwSeqNumber, hBuffer->m_dwBufferPos);
					hReader2->ReaderSetStatus(hReader2->m_hReader, READ_INFO, 0, hBuffer->m_dwBufferPos);
				}
			}
			FrLeaveMutex(&hReader->m_hMutex);
		}

        //TRACEX(DTB_LOG_TRACE, "[M3UReader] GetHttpFile - Recv Response(%d/%d)", lRet, dwBodySize);

		FrSleep(0);
    }
    TRACE("[M3UReader] GetHttpFile - Recv all (body size %d/Content length %d)", dwBodySize, dwContentLength);

	// handle with socket properly
	if (hInSocket && bServerKeepAlive)
	{
		TRACE("[M3UReader] GetHttpFile - store socket %d. Keep-alive mode", (LPPTR)hSocket);
		//*hInSocket = hSocket;
	}
	else
	{
		TRACE("[M3UReader] GetHttpFile - socket (%d) closing", hSocket);
		closeSocketInSync(&hSocket, hInSocket);
	}

	FREE(strMsg);
	TRACE("[M3UReader] GetHttpFile exit %s", pszUrl);
	return FR_OK;

failed:
	//if (bSocketOpened && hSocket)
	closeSocketInSync(&hSocket, hInSocket);

	// release
	FREE(pHTTPAddr);
    FREE(pHTTPFile);
	if (hReader->m_bProgressive)
	{
		FREE(hBuffer->m_pBuffer);
		hBuffer->m_pBuffer = NULL;
		hBuffer->m_dwBuffer = 0;
		hBuffer->m_dwBufferPos = 0;
	}
	else if (*ppbFile)
		FREE(*ppbFile);
	*ppbFile = NULL;
	*pdwFile = 0;
	FREE(strMsg);
	TRACE("[M3UReader] GetHttpFile failed %s (Err 0x%x)", pszUrl, lRet);

	return lRet;
}

const FrReader FrM3UReader =
{
	"MU3Reader",			// pReaderName;
	M3UReaderOpen,			// ReaderOpen
	M3UReaderClose,			// ReaderClose
	M3UReaderIsValidInfo,	// ReaderIsValidInfo
	M3UReaderGetInfo,		// ReaderGetInfo
	NULL,					// ReaderStart
	NULL,					// ReaderMakeIndex
	M3UReaderReadFrame,		// ReaderReadFrame
	M3UReaderSeekByTime,	// ReaderSeekByTime
	NULL,					// ReaderSeekByKeyFrame
	NULL,					// ReaderSeekBySubtitle
	NULL,					// ReaderSeekByMedia
	NULL,					// ReaderSeekByClock
	NULL,					// ReaderSetTrackID
	M3UReaderSetStatus,		// ReaderSetStatus
	M3UReaderGetStatus,		// ReaderGetStatus
	NULL,					// ReaderGetCurTime
	NULL,					// ReaderSetTotalRange
	NULL,					// ReaderSeekByVideo
	NULL,					// ReaderSetFeedback
	NULL,                   // ReaderGetFeedback
	NULL,					// ReaderSetFactor
	NULL,					// ReaderGetBitrate
	NULL,					// ReaderOpen2
	NULL,					// ReaderSeekPrevFrame
	NULL,					// ReaderSeekNextFrame
	NULL,					// ReaderSetDFSReadSize
};

#endif	// #ifndef	STREAM_INPUT_UNSUPPORTED


#endif