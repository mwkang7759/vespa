
#include "StreamReader.h"
#include "StreamReaderAPI.h"

static BOOL CopyReader(void* pReader, const FrReader *pReaderInfo) {
	ReaderStruct* hReader = (ReaderStruct*)pReader;
	
	if (!hReader || !pReaderInfo)
		return TRUE;

	hReader->m_pReaderName = pReaderInfo->pReaderName;
	hReader->ReaderOpen = pReaderInfo->ReaderOpen;
	hReader->ReaderClose = pReaderInfo->ReaderClose;
	hReader->ReaderIsValidInfo = pReaderInfo->ReaderIsValidInfo;
	hReader->ReaderGetInfo = pReaderInfo->ReaderGetInfo;
	hReader->ReaderStart = pReaderInfo->ReaderStart;
	hReader->ReaderMakeIndex = pReaderInfo->ReaderMakeIndex;
	hReader->ReaderReadFrame = pReaderInfo->ReaderReadFrame;
	hReader->ReaderSeekByTime = pReaderInfo->ReaderSeekByTime;
	hReader->ReaderSeekByKeyFrame = pReaderInfo->ReaderSeekByKeyFrame;
	hReader->ReaderSeekBySubtitle = pReaderInfo->ReaderSeekBySubtitle;
	hReader->ReaderSeekByMedia = pReaderInfo->ReaderSeekByMedia;
	hReader->ReaderSeekByClock = pReaderInfo->ReaderSeekByClock;
	hReader->ReaderSetTrackID = pReaderInfo->ReaderSetTrackID;
	hReader->ReaderSetStatus = pReaderInfo->ReaderSetStatus;
	hReader->ReaderGetStatus = pReaderInfo->ReaderGetStatus;
	hReader->ReaderGetCurTime = pReaderInfo->ReaderGetCurTime;
	hReader->ReaderSetTotalRange = pReaderInfo->ReaderSetTotalRange;
	hReader->ReaderSeekByVideo = pReaderInfo->ReaderSeekByVideo;
	hReader->ReaderSetFeedback = pReaderInfo->ReaderSetFeedback;
    hReader->ReaderGetFeedback = pReaderInfo->ReaderGetFeedback;
	hReader->ReaderSetFactor = pReaderInfo->ReaderSetFactor;
	hReader->ReaderGetBitrate = pReaderInfo->ReaderGetBitrate;
	hReader->ReaderOpen2 = pReaderInfo->ReaderOpen2;
	hReader->ReaderSeekPrevFrame = pReaderInfo->ReaderSeekPrevFrame;
	hReader->ReaderSeekNextFrame = pReaderInfo->ReaderSeekNextFrame;
	hReader->ReaderSetDFSReadSize = pReaderInfo->ReaderSetDFSReadSize;

	return TRUE;
}

static BOOL SelectReader(void* pReader, BYTE* pBuffer, DWORD32 dwBuffer) {
	ReaderStruct* hReader = (ReaderStruct*)pReader;

	CopyReader(hReader, &FrFFReader);

	return	TRUE;
}

LRSLT FrReaderOpen(void** phReader, FrURLInfo* pURL) {
	ReaderStruct*	hReader;
	LRSLT			lRet			= 0;
	DWORD32			nTotalRange		= 0;
	DWORD32			dwStartTick		= 0;
	DWORD32			dwMaxTimeout	= 0;
	INT32			iReadByte		= 0;

	hReader = (ReaderStruct*)MALLOCZ(sizeof(ReaderStruct));
	if (hReader) {
		PrintURLInfo(pURL);
		
		hReader->m_pBuffer = (BYTE*)MALLOCZ(MAX_HEADER_SIZE);
		if (!hReader->m_pBuffer) {
			FREE(hReader);
			LOG_E("ReaderOpen: memory allocation fail: MAX_HEADER_SIZE(%d)", MAX_HEADER_SIZE);
			return	COMMON_ERR_MEM;
		}

		FrCreateMutex(&hReader->m_hLock);

		// save the URLInfo
		//hReader->m_hURLInfo = (McURLInfo*)MALLOCZ(sizeof(McURLInfo));
		//memcpy(hReader->m_hURLInfo, pURL, sizeof(McURLInfo));
		
		// save the pURL Info
		if (pURL->pUrl) {
			hReader->m_pURL = (CHAR*)MALLOCZ(MAX(strnlen(pURL->pUrl, 2*MAX_URL_LENGTH)+1,2*MAX_URL_LENGTH));
			strcpy(hReader->m_pURL, pURL->pUrl);
		}

		/*hReader->m_hTask = INVALID_TASK;
		hReader->m_hAudioReadTask = INVALID_TASK;
		hReader->m_hVideoReadTask = INVALID_TASK;*/

		// remote
		if (IS_RTSP_URL((const TCHAR*)pURL->pUrl)) {
#ifdef REMOTE_RTSP_READER
			lRet = FrRTSPReader.ReaderOpen(&hReader->m_hReader, pURL);
			if (FRFAILED(lRet)) {
				FrReaderClose(hReader);
				return	lRet;
			}
			CopyReader(hReader, &FrRTSPReader);
			hReader->m_bStreaming = TRUE;
#endif
			
		}
		else if (IS_M3U_URL((const TCHAR*)pURL->pUrl, MAX_URL_LENGTH)) {
			lRet = FrM3UReader.ReaderOpen(&hReader->m_hReader, pURL);
			if (FRFAILED(lRet)) {
				FrReaderClose(hReader);
				return	lRet;
			}
			CopyReader(hReader, &FrM3UReader);
			hReader->m_bStreaming = TRUE;
		}
		else {		// local
			
			/*hReader->m_pTempURL = (CHAR*)MALLOCZ(MAX_URL_LENGTH);
			pUrl = hReader->m_pTempURL;*/

			// open file
			hReader->m_hInFile = FrOpenFile(pURL->pUrl, FILE_READ);
			if (hReader->m_hInFile == NULL)	{
				LOG_E("FrReaderOpen - File open error!");
				FrReaderClose(hReader);
				return	COMMON_ERR_FOPEN;
			}
			//hReader->m_qwFileSize = McSizeOfFile(hReader->m_hInFile);
			/*if (hReader->m_qwFileSize == 0)	{
				TRACE("[Reader]	McReaderOpen - File Size is Zero!!!");
				FrReaderClose(hReader);
				return	COMMON_ERR_STREAMSYNTAX;
			}*/
			//TRACE("[Reader] McReaderOpen - ReadBufferEnable [%d]", hReader->m_bReadBufferEnable);
			//pURL->m_qwSize = hReader->m_qwFileSize;

			FrCloseFile(hReader->m_hInFile);
			hReader->m_hInFile = NULL;
			
			if (!SelectReader(hReader, hReader->m_pBuffer, hReader->m_dwBuffer)) {
				LOG_E("FrReaderOpen - File format is not supported!");
				FrReaderClose(hReader);
				return	COMMON_ERR_FILETYPE;
			}
			
			lRet = hReader->ReaderOpen(&hReader->m_hReader, pURL);
			if (FRFAILED(lRet))	{
				FrReaderClose(hReader);
				return	lRet;
			}
		}
		
		*phReader = (void*)hReader;

		TRACE("FrReaderOpen() - Ends..");
	}
	else
		return	COMMON_ERR_MEM;

#ifdef	AUDIO_FRAME_DUMP
	hReader->m_hAudioFile = FILEDUMPOPEN("c:\\audio_frame", "dmp", (DWORD32)hReader);
#endif
#ifdef	VIDEO_FRAME_DUMP
	hReader->m_hVideoFile = FILEDUMPOPEN("c:\\video_frame", "dmp", (DWORD32)hReader);
#endif
	
	return	FR_OK;
}

void FrReaderClose(void* pReader) {
	ReaderStruct* hReader = (ReaderStruct*)pReader;
	
	TRACE("FrReaderClose - Start (0x%x)", hReader);
	if (hReader) {
		//TRACE("[Reader]	FrReaderClose - Task close is started");
		//hReader->m_nStatus = READ_STOP;
		//while (hReader->m_hTask != INVALID_TASK)
		//	FrSleep(10);
		//TRACE("[Reader]	McReaderClose - hTask is closed");

		if (hReader->m_hReader)
			hReader->ReaderClose(hReader->m_hReader);

		TRACE("FrReaderClose - ReaderClose() Success.");

#ifdef	AUDIO_FRAME_DUMP
		McCloseFile(hReader->m_hAudioFile);
#endif
#ifdef	VIDEO_FRAME_DUMP
		McCloseFile(hReader->m_hVideoFile);
#endif
		if (hReader->m_hInFile)
			FrCloseFile(hReader->m_hInFile);

		//FREE(hReader->m_szFilename);
		//FREE(hReader->m_hURLInfo);
		FREE(hReader->m_pURL);
		FREE(hReader->m_pBuffer);
		
		FrDeleteMutex(&hReader->m_hLock);

		FREE(hReader);
	}

	TRACE("FrReaderClose End");
}

LRSLT FrReaderGetInfo(void* pReader, FrMediaInfo* hInfo) {
	ReaderStruct* hReader = (ReaderStruct*)pReader;
	//QWORD	qwCurSize;
	DWORD32	lRet;
	DOUBLE	lTotalTime = 0.0;
	
	if (hReader->ReaderGetInfo)	{
		while (1) {
			lRet = hReader->ReaderGetInfo(hReader->m_hReader, hInfo);
			if (FRFAILED(lRet))	{
				if (lRet == COMMON_ERR_NODATA) {
					TRACE("Getinfo - No Data");
					continue;
				}
				else
					return	lRet;
			}
			else
			{
				TRACE("FrReaderGetInfo - Total Range %d", hInfo->dwTotalRange);
				break;
			}
		}

		if (hInfo->dwVideoTotal) {
			hReader->m_bVideoExist = TRUE;
			
			//GetVideoCodec();
			FrVideoInfo* hVideo = &hInfo->FrVideo;
			TRACE("VIDEO MEDIA");
			TRACE("	Codec=%d (%s)", hVideo->dwFourCC, hVideo->szCodec);
			TRACE("	Bitrate=%d", hVideo->dwBitRate);
			TRACE("	FrameRate=%d", hVideo->dwFrameRate);
			TRACE("	Width=%d", hVideo->dwWidth);
			TRACE("	Height=%d", hVideo->dwHeight);
		}
	}
	else
		return	COMMON_ERR_FUNCTION;

	return	lRet;
}

LRSLT FrReaderStart(void* pReader, DWORD32* pdwStart) {
	ReaderStruct* hReader = (ReaderStruct*)pReader;
	LRSLT	lRet = FR_OK;
	DWORD32	dwStartTime = 0;

	TRACE("FrReaderStart() Begin..");

	if (pdwStart)
		dwStartTime = *pdwStart;

	if (hReader->ReaderStart) {
		lRet = hReader->ReaderStart(hReader->m_hReader, &dwStartTime);
		if (pdwStart)
			*pdwStart = dwStartTime;
	}

	TRACE("FrReaderStart() StartTime(%d) End..", dwStartTime);

	return	lRet;
}

LRSLT FrReaderReadFrame(void* pReader, FrMediaStream* pMedia) {
	ReaderStruct* hReader = (ReaderStruct*)pReader;

	LRSLT	lRet = FR_OK;

	FrEnterMutex(&hReader->m_hLock);

	if (pMedia->tMediaType == AUDIO_MEDIA) {
		if (!hReader->m_bAudioExist) {
			FrLeaveMutex(&hReader->m_hLock);
			return	COMMON_ERR_NOMEDIA;
		}
		
		lRet = hReader->ReaderReadFrame(hReader->m_hReader, pMedia);
		if (FRFAILED(lRet))	{
			FrLeaveMutex(&hReader->m_hLock);
			if (lRet==COMMON_ERR_NODATA && !hReader->m_bStreaming)
				return	COMMON_ERR_ENDOFDATA;

			return	lRet;
		}

		//hReader->m_dwAudioCTS = pMedia->m_dwCTS;
		//pMedia->m_dwCTS += hReader->m_nAudioCTSDelta;
	}
	else if (pMedia->tMediaType == VIDEO_MEDIA) {
		if (!hReader->m_bVideoExist) {
			FrLeaveMutex(&hReader->m_hLock);
			return	COMMON_ERR_NOMEDIA;
		}

		lRet = hReader->ReaderReadFrame(hReader->m_hReader, pMedia);
		if (FRFAILED(lRet)) {
			FrLeaveMutex(&hReader->m_hLock);
			// TEMP
			//if (lRet==COMMON_ERR_NODATA && !hReader->m_bStreaming)	{
			//	TRACE("FrReaderReadFrame : ReaderReadFrame return 0x%x, so the return value will be COMMON_ERR_ENDOFDATA.", lRet);
			//	return	COMMON_ERR_ENDOFDATA;
			//}
			
			return	lRet;
		}
        
#ifdef	VIDEO_FRAME_DUMP
		{
			DWORD32 i, dwFrameLen = 0;
			for (i = 0; i < pMedia->m_dwFrameNum; i++)
				dwFrameLen += pMedia->m_dwFrameLen[i];
			FrWriteFile(hReader->m_hVideoFile, pMedia->m_pFrame, dwFrameLen);
		}
#endif
		//hReader->m_dwVideoCTS = pMedia->m_dwCTS;
		//pMedia->m_dwCTS += hReader->m_nVideoCTSDelta;
		//TRACE("[Reader] McReaderReadFrame: Video Read... Type(%d), CTS=%d, Len(%d)",
		//	      pMedia->tFrameType[0], pMedia->dwCTS, pMedia->dwFrameLen[0]);
	}
	
	FrLeaveMutex(&hReader->m_hLock);
	
	if (hReader->m_dwEnd && hReader->m_dwEnd < pMedia->dwCTS) {
		TRACE("FrReaderReadFrame() End Time = [%d], Cur CTS = [%d]", hReader->m_dwEnd, pMedia->dwCTS);
		return	COMMON_ERR_ENDOFDATA;
	}

	//if (FRSUCCEEDED(lRet)) {
	//	// except smi
	//	if (pMedia->m_tMediaType != TEXT_MEDIA)
	//		if(pMedia->m_dwFrameLen[0] == 0 && !pMedia->m_bAudioNullPad)
	//			lRet = COMMON_ERR_NODATA;
	//}
	
	return	lRet;
}

BOOL FrReaderGetStatus(void* pReader, FrReaderStatus* pStatus, ReaderStatusInfo* hInfo) {
	ReaderStruct* hReader = (ReaderStruct*)pReader;

	//QWORD	qwSize;
	QWORD	qwCurSize=0;
	//QWORD	qwFileOffset;
	DWORD32	dwSeekCTS = 0;

	memset(pStatus, 0, sizeof(FrReaderStatus));
	
	// Streaming or Progressive Download
	if (hReader->ReaderGetStatus)
		hReader->ReaderGetStatus(hReader->m_hReader, pStatus);
	else {
		pStatus->m_dwBufferingRatio = 100;
		pStatus->m_dwCurFSizeRatio = 100;
		pStatus->m_qwCurSize = hReader->m_qwFileSize;
		pStatus->m_qwFileSize = hReader->m_qwFileSize;
	}

	return	TRUE;
}

LRSLT FrReaderSetStatus(void* pReader, INT32 nStatus, INT32 nScale) {
	ReaderStruct* hReader = (ReaderStruct*)pReader;
	LRSLT	lRet = FR_OK;

	if (!hReader)
		return	FR_OK;

	if (hReader->ReaderSetStatus) {
		FrEnterMutex(&hReader->m_hLock);
		lRet = hReader->ReaderSetStatus(hReader->m_hReader, nStatus, nScale, hReader->m_qwFileSize);
		if (FRSUCCEEDED(lRet))
			hReader->m_nStatus = nStatus;
		FrLeaveMutex(&hReader->m_hLock);
	}
	else
		hReader->m_nStatus = nStatus;

	return	lRet;
}



void PrintURLInfo(FrURLInfo* pURL) {
	/*if (pURL->m_bNotPrintUrlInfo)
		return;*/

	TRACE("[Reader]	====================< URL Information >====================");
	TRACE("[Reader]	m_URLType           : %d", pURL->URLType);

	
	TRACE("[Reader]	m_pURL              : %s", pURL->pUrl);

	//TRACE("[Reader]	m_pCaptionURL       : %s", pURL->m_pCaptionURL);

	/*TRACE("[Reader]	m_pUserAgent        : %s", pURL->m_pUserAgent);
	TRACE("[Reader]	m_pUserName         : %s", pURL->m_pUserName);*/
	/*TRACE("[Reader]	m_pReferer          : %s", pURL->m_pReferer);
	TRACE("[Reader]	m_pCookie           : %s", pURL->m_pCookie);
	TRACE("[Reader]	m_pToken            : %s", pURL->m_pToken);
	TRACE("[Reader]	m_dwStartDate       : %d", pURL->m_dwStartDate);
	TRACE("[Reader]	m_dwEndDate         : %d", pURL->m_dwEndDate);
	TRACE("[Reader]	m_dwStartTime       : %d", pURL->m_dwStartTime);
	TRACE("[Reader]	m_dwEndTime         : %d", pURL->m_dwEndTime);
	TRACE("[Reader]	m_dwConnTimeOut     : %d", pURL->m_dwConnTimeOut);
	TRACE("[Reader]	m_dwMResTimeOut     : %d", pURL->m_dwMResTimeOut);
	TRACE("[Reader]	m_dwRecvTimeOut     : %d", pURL->m_dwRecvTimeOut);*/
	/*TRACE("[Reader]	m_dwRetryCnt        : %d", pURL->m_dwRetryCnt);*/
	/*TRACE("[Reader]	m_dwReCntCnt        : %d", pURL->m_dwReCntCnt);
	TRACE("[Reader]	m_dwRecvDelay       : %d", pURL->m_dwRecvDelay);
	TRACE("[Reader]	m_dwRecvMaxDelay    : %d", pURL->m_dwRecvMaxDelay);
	TRACE("[Reader]	m_dwRecvMaxSize     : %d", pURL->m_dwRecvMaxSize);
	TRACE("[Reader]	m_dwRecvBufLeng     : %d", pURL->m_dwRecvBufLeng);*/
	TRACE("[Reader]	-----------------------------------------------------------");
	/*TRACE("[Reader]	m_pMem              : 0x%X", pURL->m_pMem);
	TRACE("[Reader]	m_bMem              : %d",	 pURL->m_bMem);
	TRACE("[Reader]	m_qwSize            : %lld", pURL->m_qwSize);
	TRACE("[Reader]	m_dwStart           : %d", pURL->m_dwStart);
	TRACE("[Reader]	m_dwEnd             : %d", pURL->m_dwEnd);*/
	/*TRACE("[Reader]	m_bMgosp            : %d", pURL->m_bMgosp);
	TRACE("[Reader]	m_bMInfoSvr         : %d", pURL->m_bMInfoSvr);
	TRACE("[Reader]	m_dwMMSSupported    : %d", pURL->m_dwMMSSupported);
	TRACE("[Reader]	m_dwRTSPSupported   : %d", pURL->m_dwRTSPSupported);
	TRACE("[Reader]	m_dwHTTPSupported   : %d", pURL->m_dwHTTPSupported);
	TRACE("[Reader]	m_dwLocalSupported  : %d", pURL->m_dwLocalSupported);
	TRACE("[Reader]	m_bLV               : %d", pURL->m_bLV);
	TRACE("[Reader]	m_bNoSeek           : %d", pURL->m_bNoSeek);*/
    //TRACE("[Reader]	-----------------------------------------------------------");
    //TRACE("[Reader]	EnableReadBuffer    : %d", pURL->m_bEnableReadBuffer);
	TRACE("[Reader]	===========================================================");
}