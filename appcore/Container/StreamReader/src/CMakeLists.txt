cmake_minimum_required(VERSION 3.16)

# add all current files to SRC_FILES
file(GLOB_RECURSE SRC_FILES CONFIGURE_DEPENDS
	${CMAKE_CURRENT_SOURCE_DIR}/*.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/*.c
	)
# make static library
add_library(StreamReader STATIC ${SRC_FILES})

# header path for using compile library
target_include_directories(StreamReader PUBLIC ${CMAKE_SOURCE_DIR}/include
	${CMAKE_SOURCE_DIR}/_3rdparty_/ffmpeg-4.2.1/include)

# compile options
target_compile_options(StreamReader PRIVATE -Wall -Werror -Wno-error=switch -Wno-error=parentheses -Wno-error=unused-variable -Wno-error=unused-function -Wno-error=unused-but-set-variable -Wno-error=deprecated-declarations -DLinux)
