
#ifndef	_STREAMFFREADER_H_
#define	_STREAMFFREADER_H_

#include "SystemAPI.h"
#include "TraceAPI.h"
#include "StreamReader.h"

#ifdef __cplusplus
extern "C"
{
#endif

#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libavutil/mem.h"
#include "libavutil/opt.h"
#include "libavutil/imgutils.h"
#include "libswscale/swscale.h"
#include "libavutil/mathematics.h"
#include "libavutil/time.h"

#ifdef __cplusplus
}
#endif

typedef	struct {
	AVFormatContext* _format_ctx;
	int32_t				_stream_index;
	AVPacket			_packet;
	AVPacket			_packet_filtered;
	AVBSFContext* _bsfc;
	BOOL				_bMP4;
	int32_t				_nCnt;

	BYTE*			m_pBuffer;
	DWORD32			m_dwBuffSize;

} FFReaderStruct, *FFReaderHandle;

LRSLT FFReaderOpen(void** phReader, FrURLInfo* pURL);
void FFReaderClose(void* pVoid);
BOOL FFReaderIsValidInfo(void* pVoid, QWORD qwCurFileSize);
LRSLT FFReaderGetInfo(void* pVoid, FrMediaInfo* hInfo);
LRSLT FFReaderReadFrame(void* pVoid, FrMediaStream* pMedia);
//BOOL FFReaderSeekByTime(void* pVoid, DWORD32* dwCTS, BOOL bKeyFrame);
//LRSLT FFReaderSetStatus(void* pVoid, INT32 iStatus, INT32 nScale, QWORD qwCurFileSize);

#endif	// _STREAMFFREADER_H_
