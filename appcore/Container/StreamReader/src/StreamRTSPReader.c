﻿
#include "StreamRTSPReader.h"
#include "StreamReaderAPI.h"

BOOL IS_RTSP_URL(const TCHAR* pURL) {
	if (!pURL)
		return	FALSE;
	if (!_tcsncmp(pURL, _T("rtsp://"), 7) ||
		!_tcsncmp(pURL, _T("rtspt://"), 8) ||
		!_tcsncmp(pURL, _T("rtspu://"), 8))
		return	TRUE;

	return	FALSE;
}

#ifdef REMOTE_RTSP_READER

static LRSLT RTSPSend(RTSPReaderHandle hReader, BOOL bWait) {
	LRSLT	lRet = FR_OK;

#ifdef	FILE_DUMP
	FrWriteFile(hReader->m_hFile, "C => S\r\n[", strlen("C => S\r\n["));
	FrWriteFile(hReader->m_hFile, hReader->m_pSendBuffer, strlen(hReader->m_pSendBuffer));
	FrWriteFile(hReader->m_hFile, "]\r\n", strlen("]\r\n"));
#endif

	TRACE("[PSReader] RTSPSend start");
	if (bWait) {
		hReader->m_bWaitEvent = TRUE;
#ifdef	USE_WAIT_EVENT
		FrResetEvent(hReader->m_hRecvEvent);
#else
		FrRTSPUnsetResponse(hReader->m_hRTSP);
#endif
	}

	lRet = McSocketSend(hReader->m_hRTSPSocket, hReader->m_pSendBuffer, strlen(hReader->m_pSendBuffer));
	if (FRFAILED(lRet))	{
		TRACE("[PSReader] RTSPSend - SocketSend error");
		return	COMMON_ERR_SVR_DISCONNECT;
	}

	if (bWait) {
		DWORD32 	dwStart = FrGetTickCount();
		DWORD32		dwSleepTime = 10;
		DWORD32		nCount = 0;

#ifdef USE_WAIT_EVENT
		if (!McWaitEvent(hReader->m_hRecvEvent, RTSP_RECV_TIMEOUT)) {
			TRACEX(DTB_LOG_ERROR, "[RTSP]	RTSPSend - Not recv event.");
			return	COMMON_ERR_SVR_NORESPONSE;
		}
#else
		while (1) {
			if (hReader->m_bEnd) {
				TRACE("RTSPSend - stopped externally");
				return COMMON_ERR_CANCEL;
			}

			if (hReader->m_pbForceEnd && *hReader->m_pbForceEnd) {
				TRACE("RTSPSend - Force stopped externally");
				return COMMON_ERR_CANCEL;
			}

			if (FrRTSPGetResponse(hReader->m_hRTSP) == TRUE) {
				TRACE("RTSPSend - wait - get response");
				break;
			}

			if (RTSP_RECV_TIMEOUT / dwSleepTime < nCount++) {
				hReader->m_bWaitEvent = FALSE;
				TRACE("RTSPSend - Not recv event - timeout (%d)", FrGetTickCount() - dwStart);
				return	COMMON_ERR_SVR_NORESPONSE;
			}

			FrSleep(dwSleepTime);
		}
#endif

		hReader->m_bWaitEvent = FALSE;

		lRet = FrRTSPGetErrorCode(hReader->m_hRTSP);
	}

	return	lRet;
}

static LRSLT RTSPDescribe(RTSPReaderHandle hReader) {
	LRSLT	lRet;

	if (hReader->m_RtspInfo.m_bHTTP) {
		// Send Http GET message
		lRet = FrRTSPHttpGetMethod(hReader->m_hRTSP, hReader->m_pSendBuffer, MAX_RTSP_MSG_SIZE);
		if (FRFAILED(lRet))
			return	lRet;

		lRet = RTSPSend(hReader, TRUE);
		if (FRFAILED(lRet))
			return	lRet;
	}

	// Send DESCRIBE message
	lRet = FrRTSPDescribeMethod(hReader->m_hRTSP, hReader->m_pSendBuffer, MAX_RTSP_MSG_SIZE);
	if (FRFAILED(lRet))
		return	lRet;

	LOG_I("RTSPDescribe - Describe request send");
	LOG_I("	%s", hReader->m_pSendBuffer);

	lRet = RTSPSend(hReader, TRUE);
	if (FRFAILED(lRet))
		return	lRet;

	LOG_I("RTSPDescribe - Describe response recv");

	return	FR_OK;
}

static LRSLT RTSPSetup(RTSPReaderHandle hReader, DWORD32 dwIndex) {
	LRSLT	lRet;

	lRet = FrRTSPSetupMethod(hReader->m_hRTSP, hReader->m_pSendBuffer, MAX_RTSP_MSG_SIZE, dwIndex);
	if (FRFAILED(lRet))
		return	lRet;

	LOG_I("RTSPSetup - Setup request send");
	LOG_I("	%s", hReader->m_pSendBuffer);

	lRet = RTSPSend(hReader, TRUE);
	if (FRFAILED(lRet))
		return	lRet;

	LOG_I("RTSPSetup - Setup response recv");

	return	FR_OK;
}

static LRSLT RTSPPlay(RTSPReaderHandle hReader, BOOL bResume, DWORD32 dwPos, BOOL bStart) {
	PSStreamHandle	hStream;
	DWORD32			dwIndex;
	LRSLT			lRet;
	PlayInfo* pPlayInfo;
	//

	lRet = FrRTSPPlayMethod(hReader->m_hRTSP, hReader->m_pSendBuffer, MAX_RTSP_MSG_SIZE, bResume, dwPos);
	if (FRFAILED(lRet)) {
		LOG_E("RTSPPlay - FrRTSPPlayMethod err(0x%x)", lRet);		
		return	lRet;
	}

	LOG_I("RTSPPlay - Play request send");
	LOG_I("	%s", hReader->m_pSendBuffer);

	lRet = RTSPSend(hReader, TRUE);
	if (FRFAILED(lRet)) {
		LOG_E("[PSReader]	RTSPPlay - RTSPSend err(0x%x)", lRet);
		return	lRet;
	}

	LOG_I("RTSPPlay - Play response recv Start %d Resume %d", bResume, bStart);

	
	for (dwIndex = 0; dwIndex < hReader->m_dwStream; dwIndex++)	{
		hStream = &hReader->m_Stream[dwIndex];
		hStream->m_dwLastRtpTick = FrGetTickCount();				// assume rtp packet is received at the moment

		pPlayInfo = (PlayInfo*)MALLOC(sizeof(PlayInfo));
		pPlayInfo->m_bComplete = FrRTSPGetRTPInfoComplete(hReader->m_hRTSP, dwIndex);
		pPlayInfo->m_dwRange = FrRTSPGetPlayFromRange(hReader->m_hRTSP);
		pPlayInfo->m_wSeq = FrRTSPGetRTPInfoSeq(hReader->m_hRTSP, dwIndex);
		pPlayInfo->m_dwTime = FrRTSPGetRTPInfoTime(hReader->m_hRTSP, dwIndex);

		if (!hStream->m_hPlayInfoQue)
			continue;

		if (hStream->m_iStreamType == VIDEO_MEDIA)
			LOG_I("RTSPPlay - Add Video BaseCTS %d (bResume %d)(TS %u)", pPlayInfo->m_dwRange, bResume, pPlayInfo->m_dwTime);
		if (hStream->m_iStreamType == AUDIO_MEDIA)
			LOG_I("RTSPPlay - Add Audio BaseCTS %d (bResume %d)", pPlayInfo->m_dwRange, bResume);

		UtilQuePutData(hStream->m_hPlayInfoQue, pPlayInfo, sizeof(PlayInfo));
		hStream->m_wSeekSeq = pPlayInfo->m_wSeq;

		LOG_I("RTSPPlay - seek seq %d seq %d", hStream->m_wSeekSeq, hStream->m_wPrevSeq);
		hStream->m_dwSeekTime = pPlayInfo->m_dwTime;
	}

	return	FR_OK;
}


static LRSLT RTSPPause(RTSPReaderHandle hReader) {
	LRSLT	lRet;

	lRet = FrRTSPPauseMethod(hReader->m_hRTSP, hReader->m_pSendBuffer, MAX_RTSP_MSG_SIZE);
	if (FRFAILED(lRet))
		return	lRet;

	LOG_I("RTSPPause - Pause request send");

	lRet = RTSPSend(hReader, TRUE);
	if (FRFAILED(lRet))
		return	lRet;

	LOG_I("RTSPPause - Pause response recv");

	return	FR_OK;
}

static LRSLT RTSPTeardown(RTSPReaderHandle hReader) {
	LRSLT	lRet;

	lRet = FrRTSPTeardownMethod(hReader->m_hRTSP, hReader->m_pSendBuffer, MAX_RTSP_MSG_SIZE);
	if (FRFAILED(lRet))
		return	lRet;

	LOG_I("RTSPTeardown - Teardown request send");

	lRet = RTSPSend(hReader, FALSE);
	if (FRFAILED(lRet))
		return	lRet;

	LOG_I("RTSPTeardown - Teardown response recv");

	return	FR_OK;
}

static LRSLT RTSPGetParam(RTSPReaderHandle hReader)
{
	char	szContent[64];
	LRSLT	lRet;

	sprintf(szContent, "current_viewing_time\r\noldest_time\r\nlatest_time\r\n");

	lRet = FrRTSPGetParamMethod(hReader->m_hRTSP, hReader->m_pSendBuffer, MAX_RTSP_MSG_SIZE, szContent);
	if (FRFAILED(lRet))
		return	lRet;

	LOG_I("RTSPGetParam - GetParam request send");

	lRet = RTSPSend(hReader, TRUE);
	if (FRFAILED(lRet))
		return	lRet;

	LOG_I("RTSPGetParam - GetParam response recv");

	return	FR_OK;
}

static LRSLT RTSPGetParam2(RTSPReaderHandle hReader)
{
	LRSLT	lRet;

	lRet = FrRTSPGetParamMethod(hReader->m_hRTSP, hReader->m_pSendBuffer, MAX_RTSP_MSG_SIZE, NULL);
	if (FRFAILED(lRet))
		return	lRet;

	LOG_I("RTSPGetParam2 - GetParam request send");

	lRet = RTSPSend(hReader, TRUE);
	if (FRFAILED(lRet))
		return	lRet;

	LOG_I("RTSPGetParam2 - GetParam response recv");

	return	FR_OK;
}

static LRSLT RTSPOption(RTSPReaderHandle hReader) {
	LRSLT	lRet;

	// Send OPTION message
	lRet = FrRTSPOptionsMethod(hReader->m_hRTSP, hReader->m_pSendBuffer, MAX_RTSP_MSG_SIZE);
	if (FRFAILED(lRet))
		return	lRet;

	LOG_I("RTSPOption - Option request send");

	lRet = RTSPSend(hReader, TRUE);

	LOG_I("RTSPOption - Option response recv (0x%x)", lRet);

	if (FRFAILED(lRet))
		return	lRet;

	return	FR_OK;
}

static VOID PSStreamSetReset(RTSPReaderHandle hReader, BOOL bReset) {
	DWORD32		i;

	for (i = 0; i < hReader->m_dwStream; i++)
		hReader->m_Stream[i].m_bReset = bReset;
}

static char* RTSPParseRTPMessage(RTSPReaderHandle hReader, char* pBuffer, DWORD32* pdwBufLen) {
	PSStreamHandle		hStream = NULL;
	DWORD32				dwIndex;
	WORD				wSize, wSeq;
	BYTE* pData;

	//hjchoi
	DWORD32				dwSeq;
	//DWORD32				dwSeqCnt = 0;
	//WORD			wPrevSeq = 0;
	int				nDiff = 0;
	///////////

	if (*pdwBufLen < RTP_TCP_HEADER_LEN)
		return	pBuffer;

	memcpy(&wSize, &pBuffer[2], 2);
	wSize = ntohs(wSize);

	if (*pdwBufLen < (DWORD32)(wSize + RTP_TCP_HEADER_LEN))
		return	pBuffer;

	for (dwIndex = 0;dwIndex < hReader->m_dwStream;dwIndex++) {
		hStream = &hReader->m_Stream[dwIndex];
		if (hStream->m_wRTPPort == (WORD)(pBuffer[1] & 0xfe))
			break;
	}
	if (dwIndex == hReader->m_dwStream)
		hStream = NULL;

	if (hStream && hStream->m_hRTP)	{
		if (!(pBuffer[1] & 0x1)) {
			if (FrRTPParseRTPPacket(hStream->m_hRTP, (BYTE*)(pBuffer + RTP_TCP_HEADER_LEN), (DWORD32)wSize, &wSeq, hReader->m_dwTick, &hStream->m_dwSeekTime) == FR_OK) {
				if (hStream->m_bFirstRTP)
					hStream->m_bFirstRTP = FALSE;

				pData = (BYTE*)MALLOC((DWORD32)wSize);
				memcpy(pData, pBuffer + RTP_TCP_HEADER_LEN, wSize);

				// RTP receiving tick
				hStream->m_dwLastRtpTick = FrGetTickCount();

				nDiff = wSeq - (hStream->m_wPrevSeq);
				if (nDiff < 0 && -nDiff >= MAX_WORD / 2) {
					hStream->m_dwSeqCnt++;
					LOG_I("------------- Wrap around : wSeq[%d] wPrevSeq[%d] nDiff[%d] ----------", wSeq, hStream->m_wPrevSeq, nDiff);
				}

				if (nDiff >= MAX_WORD - 5) {
					//dwSeq = wSeq;
					dwSeq = wSeq + ((MAX_WORD + 1) * (hStream->m_dwSeqCnt - 1));
					LOG_I("----Already Wrap around: nDiff[%d] dwSeq[%d] --", nDiff, dwSeq);
				}
				else
					dwSeq = wSeq + ((MAX_WORD + 1) * hStream->m_dwSeqCnt);
				if (dwSeq >= MAX_DWORD)	{
					dwSeq = 0;
					LOG_I("-------------MAX_WORD*2 dwSeq = 0--------------");
				}

				//if (hStream->m_iStreamType == VIDEO_MEDIA)
				//	TRACEX(DTB_LOG_TRACE, "-- PUT Video : dwSeq[%u] wSeq[%u] PrevSeq[%d] SeqDiff[%d] SeqCnt[%d] Len(%d) Buf(%d) --",
				//		dwSeq, wSeq, hStream->m_wPrevSeq, nDiff, hStream->m_dwSeqCnt, wSize, UtilBufGetBufferedCnt(hStream->m_hRTPBuf));
				//if (hStream->m_iStreamType == AUDIO_MEDIA)
				//	TRACEX(DTB_LOG_TRACE, "-- PUT Audio : dwSeq[%u] wSeq[%u] PrevSeq[%d] SeqDiff[%d] SeqCnt[%d] Len(%d) Buf(%d) --",
				//		dwSeq, wSeq, hStream->m_wPrevSeq, nDiff, hStream->m_dwSeqCnt, wSize, UtilBufGetBufferedCnt(hStream->m_hRTPBuf));

				UtilBufPutData(hStream->m_hRTPBuf, pData, (DWORD32)wSize, dwSeq);

				// set event for marker bit
				if (hStream->m_iStreamType == AUDIO_MEDIA) {
					if (UtilBufGetBufferedCnt(hStream->m_hRTPBuf) == 1 && hReader->m_hAReadEvent != INVALID_HANDLE)
						FrSendEvent(hReader->m_hAReadEvent);
				}
				else if (hStream->m_iStreamType == VIDEO_MEDIA) {
					BOOL bMarker;
					bMarker = (pData[1] & 0x80) >> 7;
					if (bMarker) {
						if (hReader->m_hVReadEvent != INVALID_HANDLE)
							FrSendEvent(hReader->m_hVReadEvent);
					}
				}

				if (nDiff >= MAX_WORD - 5) {
					hStream->m_wPrevSeq = 0;	// no wrap
					LOG_W("----Already Wrap around: No Wrap PreSeq Zero..--");
				}
				else
					hStream->m_wPrevSeq = wSeq;

				if (hStream->m_bCheck == FALSE && hStream->m_iStreamType == VIDEO_MEDIA) {
					if (hStream->m_dwPrevRecvTS) {
						INT32	nDiff = FrRTPGetLastRecvTS(hStream->m_hRTP) - hStream->m_dwPrevRecvTS;
						if (nDiff < 0)
							hStream->m_bCTS = TRUE;
					}
					hStream->m_dwPrevRecvTS = FrRTPGetLastRecvTS(hStream->m_hRTP);
				}
			}
		}
		else
		{
			if (hStream->m_hRTP)
				FrRTPParseRTCPPacket(hStream->m_hRTP, (BYTE*)(pBuffer + RTP_TCP_HEADER_LEN), wSize);
		}
	}
	else
	{
		if (!hStream)
			LOG_I("[RTSPParseRTPMessage] Need to skip the RTP Data : hStream(0x%x)", hStream);
		else if (!hStream->m_hRTP)
			LOG_I("[RTSPParseRTPMessage] Need to skip the RTP Data : hStream->m_hRTP(0x%x)", hStream->m_hRTP);
	}

	*pdwBufLen -= (wSize + RTP_TCP_HEADER_LEN);
	pBuffer += (wSize + RTP_TCP_HEADER_LEN);

	return	pBuffer;
}

static void FuncRTCP(void* lpVoid) {
	RTSPReaderHandle	hReader = (RTSPReaderHandle)lpVoid;
	PSStreamHandle	hStream;
	FD_SET_TYPE		rSet;
	DWORD32			dwIndex;
	DWORD32			dwOverflow, dwUpperlimit;
	BOOL			bEnd = FALSE;
	LRSLT			lRet;

	//TRACEX(DTB_LOG_INFO, "[PSReader]	FuncRTCP - Start");

	dwOverflow = (DWORD32)((float)hReader->m_dwTotalBufSize * 95 / 100);
	dwUpperlimit = (DWORD32)((float)hReader->m_dwTotalBufSize * 80 / 100);

	if (!hReader->m_bEnd) {
		if (!bEnd) {
			if (hReader->m_RtspInfo.m_bUDP) {
				FD_ZERO(&rSet);
				for (dwIndex = 0; dwIndex < hReader->m_dwStream; dwIndex++) {
					hStream = &hReader->m_Stream[dwIndex];
					FD_SET(hStream->m_hRTCPSocket->m_hSocket, &rSet);
				}

				lRet = McSocketSelect(&rSet, NULL, NULL);
				if (FRFAILED(lRet)) {
					if (lRet != (LRSLT)COMMON_ERR_TIMEOUT)
						bEnd = TRUE;
				}

				for (dwIndex = 0; dwIndex < hReader->m_dwStream; dwIndex++)	{
					hStream = &hReader->m_Stream[dwIndex];
					if (!hStream || !hStream->m_hRTP)
						continue;

					// RTCP Send
					lRet = FrRTPMakeRTCPPacket(hStream->m_hRTP, hStream->m_pRTCPBuf, 0, 0, FALSE, FALSE);
					if (lRet) {
						lRet = McSocketSendTo(hStream->m_hRTCPSocket, (char*)hStream->m_pRTCPBuf, lRet,	hReader->m_szRTSPAddr, hStream->m_wRTCPRemotePort);
						if (FRFAILED(lRet))	{
							bEnd = TRUE;
							break;
						}
					}

					// RTCP Recv
					if (FD_ISSET(hStream->m_hRTCPSocket->m_hSocket, &rSet))	{
						lRet = McSocketRecvFrom(hStream->m_hRTCPSocket, (char*)hStream->m_pRTCPBuf, MAX_RTP_PKT_SIZE, NULL, NULL);
						if (FRFAILED(lRet))	{
							bEnd = TRUE;
							break;
						}

						FrRTPParseRTCPPacket(hStream->m_hRTP, hStream->m_pRTCPBuf, lRet);
					}
				}
			}
			else
			{
				// RTCP Send
				for (dwIndex = 0; dwIndex < hReader->m_dwStream; dwIndex++)	{
					hStream = &hReader->m_Stream[dwIndex];
					if (!hStream->m_hRTP)
						continue;

					lRet = FrRTPMakeRTCPPacket(hStream->m_hRTP, (BYTE*)(hStream->m_pRTCPBuf + RTP_TCP_HEADER_LEN), 0, 0, FALSE, FALSE);
					if (lRet) {
						WORD	wSize;

						hStream->m_pRTCPBuf[0] = '$';
						hStream->m_pRTCPBuf[1] = (BYTE)(hStream->m_wRTCPRemotePort);
						wSize = htons((WORD)lRet);
						memcpy(&hStream->m_pRTCPBuf[2], &wSize, 2);
						lRet += RTP_TCP_HEADER_LEN;
						lRet = McSocketSend(hReader->m_hRTSPSocket, (char*)hStream->m_pRTCPBuf, lRet);
						if (FRFAILED(lRet))	{
							bEnd = TRUE;
							break;
						}
					}
				}
			}
		}

		// Buffer Control
		if (!hReader->m_bSetStatus)	{
			BOOL	bUnderflow;
			DWORD32	dwAudioSize, dwVideoSize, dwTextSize;
			DWORD32	dwBufferSize;
			LRSLT	lRet;

			dwBufferSize = 0;
			bUnderflow = FALSE;

			if (hReader->m_bAudioExist)	{
				hStream = &hReader->m_Stream[hReader->m_dwAudioIdx];
				dwAudioSize = UtilBufGetBufferedSize(hStream->m_hRTPBuf);
				if (dwAudioSize < hStream->m_dwInitBufSize)
					bUnderflow = TRUE;
				dwBufferSize += dwAudioSize;
			}
			if (hReader->m_bVideoExist)	{
				hStream = &hReader->m_Stream[hReader->m_dwVideoIdx];
				dwVideoSize = UtilBufGetBufferedSize(hStream->m_hRTPBuf);
				if (dwVideoSize < hStream->m_dwInitBufSize)
					bUnderflow = TRUE;
				dwBufferSize += dwVideoSize;
			}
			if (hReader->m_bTextExist) {
				hStream = &hReader->m_Stream[hReader->m_dwTextIdx];
				dwTextSize = UtilBufGetBufferedSize(hStream->m_hRTPBuf);
				if (dwTextSize < hStream->m_dwInitBufSize)
					bUnderflow = TRUE;
				dwBufferSize += dwTextSize;
			}

			if (dwBufferSize > dwOverflow && !bUnderflow && hReader->m_bPaused == FALSE) {
				LOG_I("[PSReader] Buffer overflow (%d < %d)", dwOverflow, dwBufferSize);
				lRet = RTSPPause(hReader);
				if (FRSUCCEEDED(lRet))
					hReader->m_bPaused = TRUE;
			}
			if (dwBufferSize < dwUpperlimit && hReader->m_bPaused == TRUE) {
				lRet = RTSPPlay(hReader, TRUE, 0, FALSE);
				if (FRSUCCEEDED(lRet))
					hReader->m_bPaused = FALSE;
			}
		}
	}

	LOG_I("[PSReader]	FuncRTCP - End");
}

static void TaskRTSPRecv(void* lpVoid) {
	RTSPReaderHandle	hReader = (RTSPReaderHandle)lpVoid;
	char* pBuffer, * pRet;
	DWORD           dwTimeout;
	DWORD32			dwTick = FrGetTickCount(), dwDiff;
	DWORD32			dwLen = 0;
	LRSLT			lRet = FR_OK;
	BOOL			bRet;

	//DWORD32			dwSeqCnt = 0;
	//WORD			wPrevSeq = 0;

	LOG_I("TaskRTSPRecv - Start");

	hReader->m_dwTick = FrGetTickCount();
	pBuffer = hReader->m_pRecvBuffer;
	dwTimeout = 1000;
	if (hReader->m_hTaskRTCP == INVALID_TASK) {
		// for LVA PSReader burst TCP read
		dwTimeout = 100;
	}
	LOG_I("TaskRTSPRecv - dwTimeout(%d), hTaskRTCP(%d)", dwTimeout, hReader->m_hTaskRTCP);
	while (!hReader->m_bEnd) {
		lRet = McSocketWait3(hReader->m_hRTSPSocket, dwTimeout);
		if (FRFAILED(lRet))	{
			if (lRet == (LRSLT)COMMON_ERR_TIMEOUT) {
				if (hReader->m_hTaskRTCP == INVALID_TASK) {
					dwDiff = McGetSubtract(FrGetTickCount(), dwTick);
					if (dwDiff >= 2000) { // FrRTPOpen()::m_dwRTCPPeriod = 2000
						FuncRTCP(lpVoid);
						dwTick = FrGetTickCount();
					}
				}
				lRet = FR_OK;
				continue;
			}
			LOG_E("TaskRTSPRecv - wait error (0x%x)", lRet);
			lRet = (LRSLT)COMMON_ERR_SVR_DISCONNECT;
			break;
		}

		lRet = McSocketRecv(hReader->m_hRTSPSocket, pBuffer, MAX_RTSP_MSG_SIZE - dwLen);
		if (FRFAILED(lRet)) {
			LOG_E("TaskRTSPRecv - recv error (0x%x)", lRet);
			lRet = (LRSLT)COMMON_ERR_SVR_DISCONNECT;
			break;
		}
		pBuffer[lRet] = 0;		// Add null character to avoid getting strings wrong

		/*LOG_D("%p    TaskRTSPRecv - RTSP Recv %5d", hReader, lRet);
		LOG_D("%s", pBuffer);*/
			

		dwLen += lRet;

		hReader->m_dwTick = FrGetTickCount();
		pBuffer = hReader->m_pRecvBuffer;
		while (dwLen) {
			if (pBuffer[0] == '$') {
				pRet = RTSPParseRTPMessage(hReader, pBuffer, &dwLen);
				if (pBuffer == pRet)
					break;
			}
			else
			{
				pRet = FrRTSPParseMessage(hReader->m_hRTSP, pBuffer, &dwLen);
				if (!pRet)
					break;

				if (pBuffer == pRet)
					break;

				// When seeked
				if (hReader->m_bSeeked)	{
					DWORD32			i;
					PSStreamStruct* hStream;

					LOG_I("TaskRTSPRecv - Buffer reset after seek");

					// reset
					for (i = 0; i < hReader->m_dwStream; i++) {
						hStream = &hReader->m_Stream[i];

						// Reset
						hStream->m_dwLastRtpTick = FrGetTickCount();
						UtilBufReset(hStream->m_hRTPBuf);
						UtilQueReset(hStream->m_hPlayInfoQue);
						FrDepacketInit(hStream->m_hDepacket);
						if (hStream->m_pPlayInfo) {
							FREE(hStream->m_pPlayInfo);
							hStream->m_pPlayInfo = NULL;
						}
					}
					// inform the stream of being seeked
					PSStreamSetReset(hReader, TRUE);

					hReader->m_bSeeked = FALSE;
				}

#ifdef	FILE_DUMP
				FrWriteFile(hReader->m_hFile, "S => C\r\n[", strlen("S => C\r\n["));
				FrWriteFile(hReader->m_hFile, pBuffer, pRet - pBuffer);
				FrWriteFile(hReader->m_hFile, "]\r\n", strlen("]\r\n"));
#endif
				bRet = FrRTSPMakeResponse(hReader->m_hRTSP, hReader->m_pSendBuffer, MAX_RTSP_MSG_SIZE);
				if (bRet) {
#ifdef	FILE_DUMP
					FrWriteFile(hReader->m_hFile, "C => S\r\n[", strlen("C => S\r\n["));
					FrWriteFile(hReader->m_hFile, hReader->m_pSendBuffer, strlen(hReader->m_pSendBuffer));
					FrWriteFile(hReader->m_hFile, "]\r\n", strlen("]\r\n"));
#endif
					lRet = McSocketSend(hReader->m_hRTSPSocket, hReader->m_pSendBuffer, strlen(hReader->m_pSendBuffer));
					if (FRFAILED(lRet))	{
						LOG_I("[RTSP]	RTSPParsingMessage - Send Response Error");
						lRet = (LRSLT)COMMON_ERR_SVR_DISCONNECT;
						dwLen = 0;
					}
				}

#ifdef	USE_WAIT_EVENT
				if (hReader->m_bWaitEvent)
					FrSendEvent(hReader->m_hRecvEvent);
#else
				if (hReader->m_bWaitEvent)
					FrRTSPSetResponse(hReader->m_hRTSP);
#endif
			}
			pBuffer = pRet;
		}

		if (FRFAILED(lRet))
			break;

		if (pBuffer != hReader->m_pRecvBuffer && dwLen)
			//memcpy(hReader->m_pRecvBuffer, pBuffer, dwLen);
			memmove(hReader->m_pRecvBuffer, pBuffer, dwLen);
		pBuffer = hReader->m_pRecvBuffer + dwLen;
	}

	hReader->m_lError = lRet;
	McCloseTask(hReader->m_hRTSPRecv);
	hReader->m_hRTSPRecv = INVALID_TASK;
	LOG_I("TaskRTSPRecv - End (Error 0x%x Reader %p)", hReader->m_lError, hReader);
	McExitTask();
}

static void TaskRTPRecv(void* lpVoid) {
	PSStreamStruct* hStream = (PSStreamStruct*)lpVoid;
	BYTE* pPacket;
	LRSLT			lRet = FR_OK;
	WORD			wSeq;
	DWORD32			dwDiff;
	//hjchoi
	DWORD32			dwSeq;
	DWORD32			dwSeqCnt = 0;
	WORD			wPrevSeq = 0;
	int				nDiff = 0;

	//INT64			n64FreqTime = 0;
	//INT64			n64BeginTime = 0;
	//INT64			n64EndTime = 0;
	//INT64			n64CurTime = 0;

	//DWORD32			dwBeginTime = 0;
	//DWORD32			dwEndTime = 0;
	//DWORD32			dwCurTime = 0;

	WORD			wLastSeq = 0;
	DWORD32			dwSeqGap = 0;
	INT64			n64Gap = 0;
	INT64			n64GapEachFrame = 0;
	BOOL			bMarkBit = FALSE;
	BOOL			bPrevMarkBit = TRUE;

	//////////
	LOG_I("TaskRTPRecv - Start");


	* hStream->m_pdwReaderTick = FrGetTickCount();
	while (!hStream->m_bEnd) {
		lRet = McSocketWait(hStream->m_hRTPSocket, 100);
		if (FRFAILED(lRet)) {
			dwDiff = McGetSubtract(FrGetTickCount(), *hStream->m_pdwReaderTick);
			if (lRet != COMMON_ERR_TIMEOUT || dwDiff >= 10000) {
				lRet = (LRSLT)COMMON_ERR_SVR_DISCONNECT;
				break;
			}

			continue;
		}
		*hStream->m_pdwReaderTick = FrGetTickCount();

		if (hStream->m_iStreamType == VIDEO_MEDIA) {
			if (bPrevMarkBit) {
				;
			}
		}

		pPacket = (BYTE*)MALLOC(MAX_RTP_PKT_SIZE);
		lRet = McSocketRecvFrom(hStream->m_hRTPSocket, (char*)pPacket, MAX_RTP_PKT_SIZE, NULL, NULL);
		if (FRFAILED(lRet))	{
			FREE(pPacket);
			lRet = (LRSLT)COMMON_ERR_SVR_DISCONNECT;
			break;
		}

		//TRACE("[PSReader] receive udp data = %d", lRet);
		//if (hStream->m_iStreamType == APPL_MEDIA)
		//	TRACE("[PSReader] receive udp data = %d", lRet);

		if (FrRTPParseRTPPacket(hStream->m_hRTP, pPacket, (DWORD32)lRet, &wSeq, 0, &hStream->m_dwSeekTime) == FR_OK) {
			bMarkBit = FrRTPGetLastRecvMBit(hStream->m_hRTP);

			if (hStream->m_iStreamType == VIDEO_MEDIA) {
				if (bMarkBit) {
					dwSeqGap = wSeq - wLastSeq;
					wLastSeq = wSeq;
				}

				bPrevMarkBit = bMarkBit;
			}

			if (hStream->m_bFirstRTP)
				hStream->m_bFirstRTP = FALSE;

			nDiff = wSeq - wPrevSeq;
			if (nDiff < 0 && -nDiff >= MAX_WORD / 2) {
				dwSeqCnt++;
				LOG_I("------------- Wrap around : wSeq[%d] wPrevSeq[%d] nDiff[%d] ----------", wSeq, wPrevSeq, nDiff);
			}

			if (nDiff >= MAX_WORD - 5) {
				//dwSeq = wSeq;
				dwSeq = wSeq + ((MAX_WORD + 1) * (dwSeqCnt - 1));
				LOG_I("----Already Wrap around: nDiff[%d] dwSeq[%d] --", nDiff, dwSeq);
			}
			else
				dwSeq = wSeq + ((MAX_WORD + 1) * dwSeqCnt);
			if (dwSeq >= MAX_DWORD) {
				dwSeq = 0;
				TRACE("-------------MAX_WORD*2 dwSeq = 0--------------");
			}

			if (1 && hStream->m_iStreamType == VIDEO_MEDIA) {
				if (bMarkBit)
					LOG_D("-- PUT Video :  G(%0.2f : %0.2f) SeqGap(%d) dwSeq[%u] wSeq[%u] PrevSeq[%d] SeqDiff[%d] SeqCnt[%d] Len(%d) M(%d) --", n64GapEachFrame / 1000., n64Gap / 1000., dwSeqGap, dwSeq, wSeq, wPrevSeq, nDiff, dwSeqCnt, lRet, bMarkBit);
				//else
				//	TRACEX(DTB_LOG_TRACE, "-- PUT Video : dwSeq[%u] wSeq[%u] PrevSeq[%d] SeqDiff[%d] SeqCnt[%d] Len(%d) M(%d) --", dwSeq, wSeq, wPrevSeq, nDiff, dwSeqCnt, lRet, bMarkBit);
			}
			//if (hStream->m_iStreamType == AUDIO_MEDIA)
			//	TRACEX(DTB_LOG_TRACE, "-- PUT Audio : dwSeq[%u] wSeq[%u] PrevSeq[%d] SeqDiff[%d] SeqCnt[%d] Len(%d)--", dwSeq, wSeq, wPrevSeq, nDiff, dwSeqCnt, lRet);
			
			UtilBufPutData(hStream->m_hRTPBuf, pPacket, (DWORD32)lRet, dwSeq);
			
			if (nDiff >= MAX_WORD - 5) {
				wPrevSeq = 0;				// no wrap
				hStream->m_wPrevSeq = 0;	// no wrap
				LOG_W("----Already Wrap around: No Wrap PreSeq Zero..--");
			}
			else {
				hStream->m_wPrevSeq = wSeq;
				wPrevSeq = wSeq;
			}

			//if (hStream->m_iStreamType == VIDEO_MEDIA)
			//	TRACEX(DTB_LOG_DEBUG, "-- PUT Video: recv complete tick[%u] --", FrGetTickCount()-hStream->m_pdwReaderTick );
			//if (hStream->m_iStreamType == AUDIO_MEDIA)
			//	TRACEX(DTB_LOG_DEBUG, "-- PUT Audio: recv complete tick[%u] --", FrGetTickCount()-hStream->m_pdwReaderTick );
		}

		//FrSleep(20);
	}

	hStream->m_lError = lRet;
	McCloseTask(hStream->m_hRTPRecv);
	hStream->m_hRTPRecv = INVALID_TASK;
	LOG_I("TaskRTPRecv - End");
	McExitTask();
}

static void TaskRTCP(void* lpVoid) {
	RTSPReaderHandle	hReader = (RTSPReaderHandle)lpVoid;
	PSStreamHandle	hStream;
	FD_SET_TYPE		rSet;
	DWORD32			dwIndex;
	DWORD32			dwOverflow, dwUpperlimit;
	BOOL			bEnd = FALSE;
	LRSLT			lRet;

	LOG_I("TaskRTCP - Start");

	dwOverflow = (DWORD32)((float)hReader->m_dwTotalBufSize * 95 / 100);
	dwUpperlimit = (DWORD32)((float)hReader->m_dwTotalBufSize * 80 / 100);

	while (!hReader->m_bEnd) {
		if (!bEnd) {
			if (hReader->m_RtspInfo.m_bUDP) {
				FD_ZERO(&rSet);
				for (dwIndex = 0; dwIndex < hReader->m_dwStream; dwIndex++)	{
					hStream = &hReader->m_Stream[dwIndex];
					FD_SET(hStream->m_hRTCPSocket->m_hSocket, &rSet);
				}

				lRet = McSocketSelect(&rSet, NULL, NULL);
				if (FRFAILED(lRet))	{
					if (lRet != (LRSLT)COMMON_ERR_TIMEOUT)
						bEnd = TRUE;
				}

				for (dwIndex = 0; dwIndex < hReader->m_dwStream; dwIndex++)	{
					hStream = &hReader->m_Stream[dwIndex];
					if (!hStream || !hStream->m_hRTP)
						continue;

					// RTCP Send
					lRet = FrRTPMakeRTCPPacket(hStream->m_hRTP, hStream->m_pRTCPBuf, 0, 0, FALSE, FALSE);
					if (lRet) {
						lRet = McSocketSendTo(hStream->m_hRTCPSocket, (char*)hStream->m_pRTCPBuf, lRet,	hReader->m_szRTSPAddr, hStream->m_wRTCPRemotePort);
						if (FRFAILED(lRet))	{
							bEnd = TRUE;
							break;
						}
					}

					// RTCP Recv
					if (FD_ISSET(hStream->m_hRTCPSocket->m_hSocket, &rSet))	{
						lRet = McSocketRecvFrom(hStream->m_hRTCPSocket, (char*)hStream->m_pRTCPBuf, MAX_RTP_PKT_SIZE, NULL, NULL);
						if (FRFAILED(lRet))	{
							bEnd = TRUE;
							break;
						}

						FrRTPParseRTCPPacket(hStream->m_hRTP, hStream->m_pRTCPBuf, lRet);
					}
				}
			}
			else {
				// RTCP Send
				for (dwIndex = 0; dwIndex < hReader->m_dwStream; dwIndex++)	{
					hStream = &hReader->m_Stream[dwIndex];
					if (!hStream->m_hRTP)
						continue;

					lRet = FrRTPMakeRTCPPacket(hStream->m_hRTP, (BYTE*)(hStream->m_pRTCPBuf + RTP_TCP_HEADER_LEN), 0, 0, FALSE, FALSE);
					if (lRet) {
						WORD	wSize;

						hStream->m_pRTCPBuf[0] = '$';
						hStream->m_pRTCPBuf[1] = (BYTE)(hStream->m_wRTCPRemotePort);
						wSize = htons((WORD)lRet);
						memcpy(&hStream->m_pRTCPBuf[2], &wSize, 2);
						lRet += RTP_TCP_HEADER_LEN;

						lRet = McSocketSend(hReader->m_hRTSPSocket, (char*)hStream->m_pRTCPBuf, lRet);
						if (FRFAILED(lRet))	{
							bEnd = TRUE;
							break;
						}

						if (dwIndex == 0 && FrRTSPGetNeedToGetParameter(hReader->m_hRTSP)) {
							RTSPGetParam2(hReader);
						}
					}
				}
			}
		}

		// Buffer Control
		if (!hReader->m_bSetStatus)	{
			BOOL	bUnderflow;
			DWORD32	dwAudioSize, dwVideoSize, dwTextSize;
			DWORD32	dwBufferSize;
			LRSLT	lRet;

			dwBufferSize = 0;
			bUnderflow = FALSE;

			if (hReader->m_bAudioExist) {
				hStream = &hReader->m_Stream[hReader->m_dwAudioIdx];
				dwAudioSize = UtilBufGetBufferedSize(hStream->m_hRTPBuf);
				if (dwAudioSize < hStream->m_dwInitBufSize)
					bUnderflow = TRUE;
				dwBufferSize += dwAudioSize;
			}
			if (hReader->m_bVideoExist)	{
				hStream = &hReader->m_Stream[hReader->m_dwVideoIdx];
				dwVideoSize = UtilBufGetBufferedSize(hStream->m_hRTPBuf);
				if (dwVideoSize < hStream->m_dwInitBufSize)
					bUnderflow = TRUE;
				dwBufferSize += dwVideoSize;
			}
			if (hReader->m_bTextExist) {
				hStream = &hReader->m_Stream[hReader->m_dwTextIdx];
				dwTextSize = UtilBufGetBufferedSize(hStream->m_hRTPBuf);
				if (dwTextSize < hStream->m_dwInitBufSize)
					bUnderflow = TRUE;
				dwBufferSize += dwTextSize;
			}

			if (dwBufferSize > dwOverflow && !bUnderflow && hReader->m_bPaused == FALSE) {
				LOG_I("[PSReader] Buffer overflow (%d < %d)", dwOverflow, dwBufferSize);
				lRet = RTSPPause(hReader);
				if (FRSUCCEEDED(lRet))
					hReader->m_bPaused = TRUE;
			}
			if (dwBufferSize < dwUpperlimit && hReader->m_bPaused == TRUE) {
				lRet = RTSPPlay(hReader, TRUE, 0, FALSE);
				if (FRSUCCEEDED(lRet))
					hReader->m_bPaused = FALSE;
			}
		}
		FrSleep(200);
	}

	McCloseTask(hReader->m_hTaskRTCP);
	hReader->m_hTaskRTCP = INVALID_TASK;
	LOG_I("[PSReader]	TaskRTCP - End");
	McExitTask();
}

static LRSLT parseUrl(RTSPReaderHandle hReader)
{
	char* pos;
	char* pszTemp = (char*)MALLOCZ(_tcslen(hReader->m_szURL) * sizeof(UTF16));
	char* pSrcUrl = hReader->m_szURL;

	strcpy(pszTemp, pSrcUrl);

	if (hReader->m_bRelay)
		pos = NULL;
	else
		pos = UtilStrIFindString(pszTemp, "@");
	if (pos) {
		char* id = UtilStrIFindString(pszTemp, "//");
		if (id) {
			char* pStr = id + strlen("//");

			char* pass = strchr(pStr, (INT32)(':'));
			if (pass) {
				hReader->m_pszID = (char*)MALLOCZ(pass - pStr + 1);
				strncpy(hReader->m_pszID, pStr, pass - pStr);
				hReader->m_RtspInfo.m_pUserName = hReader->m_pszID;		// remapping

				pStr = pass + 1;
				hReader->m_pszPasswd = (char*)MALLOCZ(pos - pStr + 1);
				strncpy(hReader->m_pszPasswd, pStr, pos - pStr);
				hReader->m_RtspInfo.m_pUserPasswd = hReader->m_pszPasswd;
			}
		}

		hReader->m_szRTSPAddr = UtilURLGetAddr(pos);		// Server Address
		if (!hReader->m_szRTSPAddr) {
			FREE(pszTemp);
			//FREE(hReader);
			return	COMMON_ERR_URL;
		}

		hReader->m_wRTSPPort = UtilURLGetPort(pos);		// Port
		if (!hReader->m_wRTSPPort) {
			if (hReader->m_RtspInfo.m_bHTTP)
				hReader->m_wRTSPPort = HTTP_DEFAULT_PORT;
			else
				hReader->m_wRTSPPort = RTSP_DEFAULT_PORT;
		}

		// option request..
		sprintf(hReader->m_szURL, "rtsp://%s", pos + 1);
		hReader->m_pszTrueRelayUrl = hReader->m_szURL;
	}
	else {
		hReader->m_szRTSPAddr = UtilURLGetAddr(pszTemp);		// Server Address
		if (!hReader->m_szRTSPAddr) {
			FREE(pszTemp);
			//FREE(hReader);
			return	COMMON_ERR_URL;
		}

		hReader->m_wRTSPPort = UtilURLGetPort(pszTemp);		// Port
		if (!hReader->m_wRTSPPort) {
			if (hReader->m_RtspInfo.m_bHTTP)
				hReader->m_wRTSPPort = HTTP_DEFAULT_PORT;
			else
				hReader->m_wRTSPPort = RTSP_DEFAULT_PORT;
		}
	}

	FREE(pszTemp);

	return FR_OK;
}

static LRSLT parseUIDnPASSWORD(RTSPReaderHandle hReader, CHAR* pSrcUrl) {
	char* pos;
	char* pszTemp = (char*)MALLOCZ(_tcslen(hReader->m_szURL) * sizeof(UTF16));

	strcpy(pszTemp, pSrcUrl);

	pos = UtilStrIFindString(pszTemp, "@");
	if (pos) {
		char* id = UtilStrIFindString(pszTemp, "//");
		if (id)	{
			char* pStr = id + strlen("//");

			char* pass = strchr(pStr, (INT32)(':'));
			if (pass) {
				hReader->m_pszID = (char*)MALLOCZ(pass - pStr + 1);
				strncpy(hReader->m_pszID, pStr, pass - pStr);
				hReader->m_RtspInfo.m_pUserName = hReader->m_pszID;		// remapping

				pStr = pass + 1;
				hReader->m_pszPasswd = (char*)MALLOCZ(pos - pStr + 1);
				strncpy(hReader->m_pszPasswd, pStr, pos - pStr);
				hReader->m_RtspInfo.m_pUserPasswd = hReader->m_pszPasswd;
			}
		}

		// option request..
		sprintf(hReader->m_szURL, "rtsp://%s", pos + 1);
		hReader->m_pszTrueRelayUrl = hReader->m_szURL;
	}

	FREE(pszTemp);

	return FR_OK;
}

LRSLT RTSPReaderOpen(void** phReader, FrURLInfo* pURL) {
	RTSPReaderHandle	hReader;
	PSStreamHandle	hStream;
	FrMediaHandle	hMedia;
	DWORD32			dwIndex;
	LRSLT			lRet;
	WORD			wRTPPort;
	BOOL			bSSL = FALSE;

	// on test
//	char			pBuffer[MAX_URL_LENGTH];

//	memset (pBuffer, 0, MAX_URL_LENGTH);
	hReader = (RTSPReaderHandle)MALLOCZ(sizeof(RTSPReaderStruct));
	if (hReader) {
		LOG_I("RTSPReaderOpen - url=%s, init buffer time(%d)", pURL->pUrl, pURL->dwInitBufferTime);

		hReader->m_bSetStatus = TRUE;
		hReader->m_hTaskRTCP = INVALID_TASK;
		hReader->m_hRTSPRecv = INVALID_TASK;
		hReader->m_hRecvEvent = INVALID_HANDLE;
		hReader->m_hAReadEvent = INVALID_HANDLE;
		hReader->m_hVReadEvent = INVALID_HANDLE;
		hReader->m_szURL = UtilStrStrdupEx(pURL->pUrl, 100);
		hReader->m_RtspInfo.m_bServer = FALSE;
		hReader->m_RtspInfo.m_pURL = hReader->m_szURL;
		hReader->m_RtspInfo.m_bTS = FALSE;

		hReader->m_RtspInfo.m_dwBufferTime = pURL->dwInitBufferTime;
		if (hReader->m_RtspInfo.m_dwBufferTime <= 2000)
			hReader->m_RtspInfo.m_dwBufferTime = 2000;

		LOG_I("RTSPReaderOpen - set default rtsp initial buffering time(%d)", hReader->m_RtspInfo.m_dwBufferTime);

		
		if (!_tcsncmp(pURL->pUrl, _T("rtspt"), 5)) {
			//_tcscpy(hReader->m_szURL+4, hReader->m_szURL+5);
			memmove(hReader->m_szURL + 4, hReader->m_szURL + 5, strlen(hReader->m_szURL) - 4);
			hReader->m_RtspInfo.m_bHTTP = FALSE;
			hReader->m_RtspInfo.m_bUDP = FALSE;
			hReader->m_RtspInfo.m_bMulti = FALSE;
		}
		else if (!_tcsncmp(pURL->pUrl, _T("rtspu"), 5))
		{
			//_tcscpy(hReader->m_szURL+4, hReader->m_szURL+5);
			memmove(hReader->m_szURL + 4, hReader->m_szURL + 5, strlen(hReader->m_szURL) - 4);
			hReader->m_RtspInfo.m_bHTTP = FALSE;
			hReader->m_RtspInfo.m_bUDP = TRUE;
			hReader->m_RtspInfo.m_bUDPfec = FALSE;
			hReader->m_RtspInfo.m_bMulti = FALSE;
		}
		//////////////////////////////////////////////////////////////////////////////////////////////////
		else if (!_tcsncmp(pURL->pUrl, _T("rtsp"), 4))				// default. Hence must be the one which comes last
		{
			hReader->m_RtspInfo.m_bHTTP = FALSE;
			hReader->m_RtspInfo.m_bUDP = FALSE;
			hReader->m_RtspInfo.m_bMulti = FALSE;
		}
		else {
			FREE(hReader);
			return	COMMON_ERR_URL;
		}

		if ((lRet = parseUrl(hReader)) != FR_OK) {
			LOG_E("RTSPReaderOpen parseUrl - error 0x%x", lRet);
			FREE(hReader);
			return lRet;
		}

		// socket info
		//lRet = McSocketTcpOpen(&hReader->m_hRTSPSocket, hReader->m_szRTSPAddr, hReader->m_wRTSPPort,
		//				FALSE, pURL->m_dwConnTimeOut);
		lRet = McSocketTcpOpen2(&hReader->m_hRTSPSocket, hReader->m_szRTSPAddr, hReader->m_wRTSPPort,
				FALSE, pURL->dwConnTimeOut, bSSL);
		if (FRFAILED(lRet))	{
			RTSPReaderClose(hReader);
			return	lRet;
		}

		// RTSP open
		lRet = FrRTSPOpen(&hReader->m_hRTSP, &hReader->m_RtspInfo);
		if (FRFAILED(lRet))	{
			LOG_E("[PSReader] FrRTSPOpen() error !!");
			RTSPReaderClose(hReader);
			return	lRet;
		}

#ifdef	FILE_DUMP
		hReader->m_hFile = FrOpenFile("rtsp_log.txt", FILE_CREATE | FILE_WRITE);
#endif
		hReader->m_hAReadEvent = FrCreateEvent(NULL);
		hReader->m_hVReadEvent = FrCreateEvent(NULL);
		hReader->m_hRecvEvent = FrCreateEvent(NULL);

		hReader->m_hRTSPRecv = McCreateTask(NULL, (PTASK_START_ROUTINE)TaskRTSPRecv, hReader, RTSP_TASK_PRIORITY, 0, 0);

		// option request..
		lRet = RTSPOption(hReader);
		if (FRFAILED(lRet))	{
			if (lRet == COMMON_ERR_UNAUTHORIZED) {
				lRet = RTSPOption(hReader);
				if (FRFAILED(lRet))	{
					LOG_E("[PSReader] RTSPOption() error !!");
					RTSPReaderClose(hReader);
					return	lRet;
				}
			}
			else {
				LOG_E("[PSReader] RTSPOption() error !!");
				RTSPReaderClose(hReader);
				return	lRet;
			}
		}

		// describe request..
		lRet = RTSPDescribe(hReader);
		if (FRFAILED(lRet)) {
			if (lRet == COMMON_ERR_UNAUTHORIZED) {
				lRet = RTSPDescribe(hReader);
				if (FRFAILED(lRet)) {
					LOG_E("[PSReader] RTSPDescribe() error !!");
					RTSPReaderClose(hReader);
					return	lRet;
				}
			}
			else {
				LOG_E("[PSReader] RTSPDescribe() error !!");
				RTSPReaderClose(hReader);
				return	lRet;
			}
		}

		if (hReader->m_bEnd) {
			LOG_D("[PSReader] RTSPDescribe() - stopped externally");
			RTSPReaderClose(hReader);
			return	COMMON_ERR_CANCEL;
		}

		// SDP open
		lRet = FrSDPOpen(&hReader->m_hSDP, FrRTSPGetSDP(hReader->m_hRTSP), FrRTSPGetSDPLength(hReader->m_hRTSP), 0);
		if (FRFAILED(lRet)) {
			LOG_E("[PSReader] FrSDPOpen() error !!");
			RTSPReaderClose(hReader);
			return	lRet;
		}

		hReader->m_dwStream = FrSDPGetMediaNum(hReader->m_hSDP);
		for (dwIndex = 0; dwIndex < hReader->m_dwStream;dwIndex++) {
			hStream = &hReader->m_Stream[dwIndex];
			hStream->m_hRTPRecv = INVALID_TASK;
			hStream->m_bFirstRTP = TRUE;
		}
		FrRTSPSetControl(hReader->m_hRTSP, FrSDPGetSDPControl(hReader->m_hSDP));

		//lRet = FrRTSPInit(hReader->m_hRTSP, hReader->m_dwStream);
		lRet = FrRTSPInit(hReader->m_hRTSP, FrSDPGetMediaNumOrg(hReader->m_hSDP));
		if (FRFAILED(lRet)) {
			LOG_E("[PSReader] FrRTSPInit() error !!");
			RTSPReaderClose(hReader);
			return	lRet;
		}

		if (hReader->m_RtspInfo.m_bUDP) {
			//TRACE("[PSReader] UDP port : %d ~ %d", pURL->m_wUDPStartPort, pURL->m_wUDPEndPort);
			wRTPPort = RTP_UDP_PORT;
		}
		else
			wRTPPort = RTP_TCP_PORT;

		//for (dwIndex = 0; dwIndex < hReader->m_dwStream;dwIndex++)
		for (dwIndex = 0; dwIndex < FrSDPGetMediaNumOrg(hReader->m_hSDP);dwIndex++) {
			char* pMultiAddr = NULL;

			hStream = &hReader->m_Stream[dwIndex];
			hStream->m_pdwReaderTick = &hReader->m_dwTick;				// for update
			hMedia = FrSDPGetMediaHandle(hReader->m_hSDP, dwIndex);
			if (!hMedia)
				continue;

			if (FrSDPGetMediaType(hMedia) == MI_AUDIO) {
				if (FrSDPGetSendOnly(hMedia)) {
					TRACE("[PSReader] Not setup an audio back channel (send-only audio stream)");
					wRTPPort += 2;
					continue;
				}
			}

			// s1 ip camera, ignore audio rtp stream.
			//if ( (FrSDPGetMediaType(hMedia) == MI_AUDIO) && !UtilStrStrnicmp(FrSDPGetSessionFieldName(hReader->m_hSDP), "Media Presentation", strlen("Media Presentation"))
			//		&& !UtilStrStrnicmp(FrSDPGetInfoFieldName(hReader->m_hSDP), "samsung", strlen("samsung")) )
			//	continue;
			//hStream->m_dwJitterDelay = 100;

			FrRTSPSetTrackControl(hReader->m_hRTSP, dwIndex, FrSDPGetMediaControl(hMedia));

			if (hReader->m_dwStream <= dwIndex)	{
				TRACE("Just add original channel info %d/%d", dwIndex, FrSDPGetMediaNumOrg(hReader->m_hSDP));
				continue;
			}

			// Send SETUP message
			if (hReader->m_RtspInfo.m_bMulti) {
				lRet = RTSPSetup(hReader, dwIndex);
				if (FRFAILED(lRet))	{
					RTSPReaderClose(hReader);
					return	lRet;
				}

				pMultiAddr = FrRTSPGetMultiAddress(hReader->m_hRTSP);
				wRTPPort = FrRTSPGetRTPMultiPort(hReader->m_hRTSP, dwIndex);
			}

			// RTP socket open
			if (hReader->m_RtspInfo.m_bUDP)	{
#ifdef UDP_FIXED
				hStream->m_wRTPPort = wRTPPort;
				lRet = McSocketUdpOpenFixed(&hStream->m_hRTPSocket, pMultiAddr, NULL, &hStream->m_wRTPPort, hReader->m_RtspInfo.m_bMulti);
				if (FRFAILED(lRet))	{
					RTSPReaderClose(hReader);
					return	lRet;
				}
				hStream->m_wRTCPPort = hStream->m_wRTPPort + 1;
				lRet = McSocketUdpOpenFixed(&hStream->m_hRTCPSocket, pMultiAddr, NULL, &hStream->m_wRTCPPort, hReader->m_RtspInfo.m_bMulti);
				if (FRFAILED(lRet))	{
					RTSPReaderClose(hReader);
					return	lRet;
				}
				wRTPPort = hStream->m_wRTPPort;
#else
				hStream->m_wRTPPort = wRTPPort;
				lRet = McSocketUdpOpen(&hStream->m_hRTPSocket, pMultiAddr, NULL, &hStream->m_wRTPPort, hReader->m_RtspInfo.m_bMulti);
				if (FRFAILED(lRet))	{
					RTSPReaderClose(hReader);
					return	lRet;
				}
				hStream->m_wRTCPPort = hStream->m_wRTPPort + 1;
				lRet = McSocketUdpOpen(&hStream->m_hRTCPSocket, pMultiAddr, NULL, &hStream->m_wRTCPPort, hReader->m_RtspInfo.m_bMulti);
				if (FRFAILED(lRet))	{
					RTSPReaderClose(hReader);
					return	lRet;
				}
				wRTPPort = hStream->m_wRTPPort;
#endif
			}
			else {
				hStream->m_wRTPPort = wRTPPort;
				hStream->m_wRTCPPort = hStream->m_wRTPPort + 1;
			}
			wRTPPort += 2;

			// RTSP set
			FrRTSPSetRTPLocalPort(hReader->m_hRTSP, dwIndex, hStream->m_wRTPPort);
			FrRTSPSetRTCPLocalPort(hReader->m_hRTSP, dwIndex, hStream->m_wRTCPPort);
			LOG_I("RTSPReaderOpen - Local RTP port=%d, RTCP port=%d", hStream->m_wRTPPort, hStream->m_wRTCPPort);

			if (!hReader->m_RtspInfo.m_bMulti) {
				lRet = RTSPSetup(hReader, dwIndex);
				if (FRFAILED(lRet))	{
					// request transport that server can support or not..because of the youtube
					if (lRet == (LRSLT)COMMON_ERR_UNSUPPORT_TRASNPORT) {
						hReader->m_RtspInfo.m_bUDP = !hReader->m_RtspInfo.m_bUDP;
						FrRTSPUpdateTransport(hReader->m_hRTSP, hReader->m_RtspInfo.m_bUDP);
						// set rtp, rtcp port with udp
						//LOG_I("[PSReader] youtube UDP port : %d ~ %d", pURL->m_wUDPStartPort, pURL->m_wUDPEndPort);
						if (!dwIndex) {
							wRTPPort = RTP_UDP_PORT;
						}
#ifdef UDP_FIXED
						hStream->m_wRTPPort = wRTPPort;
						lRet = McSocketUdpOpenFixed(&hStream->m_hRTPSocket, pMultiAddr, NULL, &hStream->m_wRTPPort, hReader->m_RtspInfo.m_bMulti);
						if (FRFAILED(lRet))	{
							RTSPReaderClose(hReader);
							return	lRet;
						}
						hStream->m_wRTCPPort = hStream->m_wRTPPort + 1;
						lRet = McSocketUdpOpenFixed(&hStream->m_hRTCPSocket, pMultiAddr, NULL, &hStream->m_wRTCPPort, hReader->m_RtspInfo.m_bMulti);
						if (FRFAILED(lRet))	{
							RTSPReaderClose(hReader);
							return	lRet;
						}
						wRTPPort = hStream->m_wRTPPort;
#else
						hStream->m_wRTPPort = wRTPPort;
						lRet = McSocketUdpOpen(&hStream->m_hRTPSocket, pMultiAddr, NULL, &hStream->m_wRTPPort, hReader->m_RtspInfo.m_bMulti);
						if (FRFAILED(lRet))
						{
							RTSPReaderClose(hReader);
							return	lRet;
						}
						hStream->m_wRTCPPort = hStream->m_wRTPPort + 1;
						lRet = McSocketUdpOpen(&hStream->m_hRTCPSocket, pMultiAddr, NULL, &hStream->m_wRTCPPort, hReader->m_RtspInfo.m_bMulti);
						if (FRFAILED(lRet))
						{
							RTSPReaderClose(hReader);
							return	lRet;
						}
						wRTPPort = hStream->m_wRTPPort;
#endif
						wRTPPort += 2;
						// RTSP set
						FrRTSPSetRTPLocalPort(hReader->m_hRTSP, dwIndex, hStream->m_wRTPPort);
						FrRTSPSetRTCPLocalPort(hReader->m_hRTSP, dwIndex, hStream->m_wRTCPPort);

						lRet = RTSPSetup(hReader, dwIndex);
						if (FRFAILED(lRet))	{
							RTSPReaderClose(hReader);
							return	lRet;
						}
					}
					else {
						RTSPReaderClose(hReader);
						return	lRet;
					}
				}

				hStream->m_wRTPRemotePort = FrRTSPGetRTPRemotePort(hReader->m_hRTSP, dwIndex);
				hStream->m_wRTCPRemotePort = FrRTSPGetRTCPRemotePort(hReader->m_hRTSP, dwIndex);
				LOG_I("RTSPReaderOpen - Remote RTP port=%d, RTCP port=%d", hStream->m_wRTPRemotePort, hStream->m_wRTCPRemotePort);
			}

			// RTP open
			lRet = FrRTPOpen(&hStream->m_hRTP, hMedia);
			if (FRFAILED(lRet))	{
				LOG_E("RTSPReaderOpen: FrRTPOpen error !!");
				RTSPReaderClose(hReader);
				return	lRet;
			}

			//hStream->m_dwInitBufSize = (DWORD32)SCALE(FrSDPGetMediaAvgBitrate(hMedia), RTP_INIT_BUFFER_TIME, 8000);
			hStream->m_dwInitBufSize = (DWORD32)SCALE(FrSDPGetMediaAvgBitrate(hMedia), hReader->m_dwInitBuffTime, 8000);
			hStream->m_dwMaxBufSize = (DWORD32)SCALE(FrSDPGetMediaAvgBitrate(hMedia), RTP_MAX_BUFFER_TIME, 8000);
			hReader->m_dwTotalBufSize += hStream->m_dwMaxBufSize;

			hStream->m_hRTPBuf = UtilBufOpen(1);
			hStream->m_hPlayInfoQue = UtilQueOpen();
			hStream->m_hDepacket = FrDepacketOpen(hMedia);
			hStream->m_dwTrackID = dwIndex;

			switch (FrSDPGetMediaType(hMedia))
			{
			case MI_AUDIO:
				hStream->m_iStreamType = AUDIO_MEDIA;
				if (!hStream->m_dwInitBufSize)
					//hStream->m_dwInitBufSize = 48000 * RTP_INIT_BUFFER_TIME / 8000;
					hStream->m_dwInitBufSize = 48000 * hReader->m_dwInitBuffTime / 8000;
				if (!hStream->m_dwMaxBufSize)
					hStream->m_dwMaxBufSize = 48000 * RTP_MAX_BUFFER_TIME / 8000;
				LOG_I("[PSReader]	PSReaderOpen - Audio Channel Open initbufsize=%d", hStream->m_dwInitBufSize);
				break;
			case MI_VIDEO:
				hStream->m_iStreamType = VIDEO_MEDIA;
				if (!hStream->m_dwInitBufSize)
					//hStream->m_dwInitBufSize = (QWORD)512000 * RTP_INIT_BUFFER_TIME / 90000;
					hStream->m_dwInitBufSize = (QWORD)512000 * hReader->m_dwInitBuffTime / 90000;
				if (!hStream->m_dwMaxBufSize)
					hStream->m_dwMaxBufSize = (QWORD)512000 * RTP_MAX_BUFFER_TIME / 90000;
				LOG_I("RTSPReaderOpen - Video Channel Open initbufsize=%d", hStream->m_dwInitBufSize);
				break;
			default:
				continue;
			}

			if (hReader->m_RtspInfo.m_bUDP)
				hStream->m_hRTPRecv = McCreateTask(NULL, (PTASK_START_ROUTINE)TaskRTPRecv, hStream,
					RTP_TASK_PRIORITY, 0 /*RTP_TASK_STACKSIZE*/, 0);
		}

		//hReader->m_hTaskRTCP = McCreateTask(NULL, (PTASK_START_ROUTINE)TaskRTCP, hReader, RTCP_TASK_PRIORITY, 0, 0);

		* phReader = (void*)hReader;
	}
	else
		return	COMMON_ERR_MEM;

	return	FR_OK;
}

void RTSPReaderClose(void* pVoid) {
	RTSPReaderHandle	hReader = (RTSPReaderHandle)pVoid;
	PSStreamHandle	hStream;
	DWORD32			dwIndex;

	LOG_I("RTSPReaderClose Begin..");
	if (hReader) {
		if (hReader->m_hRecvEvent != INVALID_HANDLE)
			FrResetEvent(hReader->m_hRecvEvent);

		if (hReader->m_hAReadEvent != INVALID_HANDLE)
			FrResetEvent(hReader->m_hAReadEvent);

		if (hReader->m_hVReadEvent != INVALID_HANDLE)
			FrResetEvent(hReader->m_hVReadEvent);

		if (hReader->m_hRTSP)
			RTSPTeardown(hReader);

		for (dwIndex = 0;dwIndex < hReader->m_dwStream;dwIndex++) {
			hStream = &hReader->m_Stream[dwIndex];

			hStream->m_bEnd = TRUE;
			while (hStream->m_hRTPRecv != INVALID_TASK)
				FrSleep(10);
		}

		hReader->m_bEnd = TRUE;
		while (hReader->m_hTaskRTCP != INVALID_TASK)
			FrSleep(10);
		while (hReader->m_hRTSPRecv != INVALID_TASK)
			FrSleep(10);

		for (dwIndex = 0;dwIndex < hReader->m_dwStream;dwIndex++) {
			hStream = &hReader->m_Stream[dwIndex];

			McSocketClose(hStream->m_hRTPSocket);
			McSocketClose(hStream->m_hRTCPSocket);

			//StreamRTPBufferClose(hStream->m_hRTPBuffer);

			UtilBufClose(hStream->m_hRTPBuf);
			UtilQueClose(hStream->m_hPlayInfoQue);
			FrRTPClose(hStream->m_hRTP);
			FrDepacketClose(hStream->m_hDepacket);
		}

		if (hReader->m_hRecvEvent != INVALID_HANDLE)
			FrDeleteEvent(hReader->m_hRecvEvent);

		if (hReader->m_hAReadEvent != INVALID_HANDLE)
			FrDeleteEvent(hReader->m_hAReadEvent);

		if (hReader->m_hVReadEvent != INVALID_HANDLE)
			FrDeleteEvent(hReader->m_hVReadEvent);

		McSocketClose(hReader->m_hRTSPSocket);
		FrRTSPClose(hReader->m_hRTSP);
		FrSDPClose(hReader->m_hSDP);

		//StreamPSBufferClose(hReader->m_hReorderBuffer);

		FREE(hReader->m_szRTSPAddr);
		FREE(hReader->m_szUserAgent);
		FREE(hReader->m_szURL);

		FREE(hReader->m_pszID);
		FREE(hReader->m_pszPasswd);

#ifdef	FILE_DUMP
		FrCloseFile(hReader->m_hFile);
#endif
		FREE(hReader);
		LOG_I("RTSPReaderClose End..");
	}
}

LRSLT RTSPReaderGetInfo(void* pVoid, FrMediaInfo* hInfo) {
	RTSPReaderHandle	hReader = (RTSPReaderHandle)pVoid;
	PSStreamHandle	hStream;
	FrMediaHandle	hMedia;
	OBJECT_TYPE		eObject;
	DWORD32			i, dwAudioNum = 0, dwVideoNum = 0, dwTextNum = 0;

	//hInfo->URLType = _RTSP_URL;
	hInfo->dwTotalRange = FrSDPGetSDPRange(hReader->m_hSDP);
	//hInfo->dwBufferTime = hReader->m_dwBuffTime;

	LOG_I("RTSPReaderGetInfo Begin..");
	
	// Audio
	for (i = 0; i < hReader->m_dwStream; i++) {
		hStream = &hReader->m_Stream[i];

		if (hStream->m_iStreamType == AUDIO_MEDIA)	{
			FrAudioInfo* hAudio = &hInfo->FrAudio;
		}
		else if (hStream->m_iStreamType == VIDEO_MEDIA)
		{
			FrVideoInfo* hVideo = &hInfo->FrVideo;

			if (!hReader->m_bVideoExist) {
				hReader->m_dwVideoIdx = i;
				hReader->m_bVideoExist = TRUE;
			}

			//if (hReader->m_bDisableNoData)
			//	hStream->m_dwJitterDelay = 200;

			hMedia = FrSDPGetMediaHandle(hReader->m_hSDP, hStream->m_dwTrackID);
			eObject = FrSDPGetMediaObjectType(hMedia);

			hVideo->dwFourCC = ConvObjectToCodec(eObject);
			hStream->m_dwFourCC = hVideo->dwFourCC;
			hVideo->dwBitRate = FrSDPGetMediaAvgBitrate(hMedia);
			hVideo->dwWidth = FrSDPGetMediaVideoWidth(hMedia);
			hVideo->dwHeight = FrSDPGetMediaVideoHeight(hMedia);
			hVideo->nProfile = FrSDPGetMediaProfile(hMedia);
			hVideo->nLevel = FrSDPGetMediaLevel(hMedia);
			hVideo->nCompatibility = FrSDPGetMediaCompatibility(hMedia);
			hVideo->dwFrameRate = FrSDPGetMediaVideoFrameRate(hMedia);
			hVideo->dwConfig = FrSDPGetMediaConfigLen(hMedia);
			
			if (eObject == H264_OBJECT && FrSDPGetMediaH264ParamSet(hMedia)) {
				hVideo->dwConfig = make_h264_config_data_from_skt(hVideo->pConfig,
					hVideo->dwConfig, FrSDPGetMediaConfig(hMedia), hVideo->dwConfig);
			}
			else if (eObject == H265_OBJECT) {
				hVideo->dwConfig = make_hvcC_config(hVideo->pConfig, sizeof(hVideo->pConfig), FrSDPGetH265hvcCInfo(hMedia));
			}
			else
				memcpy(hVideo->pConfig, FrSDPGetMediaConfig(hMedia), hVideo->dwConfig);

			hReader->m_bReorderBuffer = FALSE;

			hInfo->dwVideoTotal++;
			FrVideoGetInfo(hVideo->pConfig, hVideo->dwConfig, hVideo);
			hVideo->dwMaxBuffSize = hVideo->dwWidth * hVideo->dwHeight * 3;
			hVideo->dwDuration = FrSDPGetMediaRange(hMedia);
			hStream->m_dwRange = hVideo->dwDuration;
			if (!hStream->m_dwRange)	// assume to live case of range is zero.
				hStream->m_dwRange = INFINITE;

			hStream->m_dwDeltaCTS = (DWORD32)CalVideoCTSByFrameRate(1, hVideo->dwFrameRate);
		}
	}

	// Set the Live stream
	if (FrSDPGetMediaIsLive(hReader->m_hSDP)) {
		hInfo->dwTotalRange = 0;
		hReader->m_bLive = TRUE;
	}

	//hInfo->dwAudioTotal = dwAudioNum;
	//hInfo->dwVideoTotal = dwVideoNum;
	
	LOG_I("RTSPReaderGetInfo End..");

	return	FR_OK;
}

LRSLT RTSPReaderStart(void* pVoid, DWORD32* pdwStart) {
	RTSPReaderHandle	hReader = (RTSPReaderHandle)pVoid;
	LRSLT			lRet;

	lRet = RTSPPlay(hReader, FALSE, *pdwStart, TRUE);
	if (FRFAILED(lRet))
		return	lRet;

	*pdwStart = FrRTSPGetPlayFromRange(hReader->m_hRTSP);

	// s1
	if (!FrRTSPIsFindRTPInfo(hReader->m_hRTSP) && !FrRTSPIsFindRange(hReader->m_hRTSP))
		*pdwStart = -1;

	return	FR_OK;
}


static void RTSPReaderUpdatePlayInfo(PSStreamHandle hStream) {
	PlayInfo* hNextPlayInfo = NULL;

	hStream->m_pPlayInfo = (PlayInfo*)UtilQueGetData(hStream->m_hPlayInfoQue, NULL);
	while (1) {
		hNextPlayInfo = (PlayInfo*)UtilQueShowHeadData(hStream->m_hPlayInfoQue, NULL);
		if (hNextPlayInfo == NULL)
			break;

		// No rtp packets are sent between the last 2 play infos
		if (hStream->m_pPlayInfo->m_wSeq == hNextPlayInfo->m_wSeq) {
			FREE(hStream->m_pPlayInfo);
			hStream->m_pPlayInfo = (PlayInfo*)UtilQueGetData(hStream->m_hPlayInfoQue, NULL);
		}
		else
		{
			break;
		}
	}
}

LRSLT RTSPReaderReadFrame(void* pVoid, FrMediaStream* pMedia) {
	RTSPReaderHandle		hReader = (RTSPReaderHandle)pVoid;
	PSStreamHandle		hStream = NULL;
	FrMediaHandle		hMedia = NULL;
	DWORD32				dwCTS = 0;
	BOOL				bVideoReorder = FALSE;

	if (pMedia->tMediaType == AUDIO_MEDIA) {
		hStream = &hReader->m_Stream[hReader->m_dwAudioIdx];
		//TRACE("[PSReader]	Read Audio ....");
	}
	else if (pMedia->tMediaType == VIDEO_MEDIA) {
		hStream = &hReader->m_Stream[hReader->m_dwVideoIdx];
		bVideoReorder = hReader->m_bReorderBuffer;
		//LOG_D("ReaderReadFrame read video begin..reoder(%d)", bVideoReorder);
	}
	
	// Set the check
	if (hStream)
		hStream->m_bCheck = TRUE;

	
	hMedia = FrSDPGetMediaHandle(hReader->m_hSDP, hStream->m_dwTrackID);

//get_a_frame:
	// Get RTP BaseTime
	if (!hStream->m_pPlayInfo)
		RTSPReaderUpdatePlayInfo(hStream);

	while (1) {
		BYTE* pPacket;
		DWORD32	dwPacket;
		WORD	wSeq = 0;

		if (hReader->m_lError)
			return hReader->m_lError;

		// End
		if (hReader->m_bEnd) {
			TRACE("ReaderReadFrame stopped externally");
			pMedia->dwFrameLen[0] = 0;
			pMedia->dwFrameNum = 0;
			FrSleep(10);

			return COMMON_ERR_ENDOFDATA;
		}

		pMedia->pFrame = FrDepacketGetData(hStream->m_hDepacket, &(pMedia->dwFrameLen[0]), &dwCTS);
		if (pMedia->pFrame)	{
			if (hStream->m_dwFourCC == FOURCC_AVC1 || hStream->m_dwFourCC == FOURCC_H264)
				if (pMedia->dwFrameNum == 1 && pMedia->dwFrameLen[0] == 6 && pMedia->pFrame[4] == 0x9)
					continue;
			// help drop out RTP packets on the previous timeline when seek
			if (hStream->m_bReset && hStream->m_pPlayInfo) {
				LOG_D("ReaderReadFrame continue");
				continue;
				//TRACE("[PSReader]	Read CTS=%d", dwCTS);
			}
			if (hStream->m_dwFourCC == FOURCC_H264)	{
				if (check_h264_config_only(pMedia->pFrame, pMedia->dwFrameLen[0])) {
					LOG_D("ReaderReadFrame H264 Config data only %d", pMedia->dwFrameLen[0]);
					pMedia->pFrame = NULL;
					pMedia->dwFrameLen[0] = 0;
					continue;
				}
			}
			break;
		}

		if (UtilBufCheckLostData(hStream->m_hRTPBuf)) {
			if (hReader->m_bDisableNoData) {
				if (hStream->m_iStreamType == VIDEO_MEDIA) {
					if (UtilBufGetBufferedCnt(hStream->m_hRTPBuf) < 10)	{
						LOG_D("ReaderReadFrame video packet lost ....(buffered rtp count %d)", UtilBufGetBufferedCnt(hStream->m_hRTPBuf));
						return	COMMON_ERR_NODATA;
					}
					else
						LOG_D("ReaderReadFrame video packet lost ....(buffered rtp count %d) - keep processing. So this frame will be thrown away", UtilBufGetBufferedCnt(hStream->m_hRTPBuf));
				}
				else if (hStream->m_iStreamType == AUDIO_MEDIA)	{
					if (UtilBufGetBufferedCnt(hStream->m_hRTPBuf) < 2) {
						LOG_D("ReaderReadFrame audio packet lost ....(buffered rtp count %d)", UtilBufGetBufferedCnt(hStream->m_hRTPBuf));
						return	COMMON_ERR_NODATA;
					}
					else
						LOG_D("ReaderReadFrame audio packet lost ....(buffered rtp count %d) - keep processing. So this frame will be thrown away", UtilBufGetBufferedCnt(hStream->m_hRTPBuf));
				}
			}
		}
		else
			hStream->m_dwPrevTick = 0;

		//TRACEX(DTB_LOG_INFO, "[PSReader] Get Buf(%d)", UtilBufGetBufferedCnt(hStream->m_hRTPBuf));
		pPacket = UtilBufGetData(hStream->m_hRTPBuf, &dwPacket, 0);
		if (pPacket) {
			if (hStream->m_pPlayInfo) {
				memcpy((BYTE*)&wSeq, (BYTE*)&pPacket[2], 2);
				wSeq = ntohs(wSeq);

				// when play method requested, time line must be in sync
				if (hStream->m_pPlayInfo->m_wSeq <= wSeq) {
					if (hReader->m_bLive && !hStream->m_pPlayInfo->m_dwRange) {
						LOG_I("ReaderReadFrame %d type - source is live and resume response has no range info", pMedia->tMediaType);
					}
					else {
						hStream->m_dwBaseCTS = hStream->m_pPlayInfo->m_dwRange;

						if (pMedia->tMediaType == AUDIO_MEDIA)
							LOG_I("ReaderReadFrame Read Audio BaseCTS=%d", hStream->m_dwBaseCTS);
						else if (pMedia->tMediaType == VIDEO_MEDIA)
							LOG_I("ReaderReadFrame Read Video BaseCTS=%d", hStream->m_dwBaseCTS);
						
						FrRTPSetStartTS(hStream->m_hRTP, hStream->m_pPlayInfo->m_dwTime);
						hStream->m_dwSeekTime = hStream->m_pPlayInfo->m_dwTime;
					}

					FREE(hStream->m_pPlayInfo);
					hStream->m_pPlayInfo = NULL;
					hStream->m_bReset = FALSE;
				}
			}

			dwCTS = FrRTPGetCTS(hStream->m_hRTP, pPacket, 0);
			FrDepacketPutData(hStream->m_hDepacket, pPacket, dwPacket, dwCTS);
			if (hReader->m_bLive)
				if (hStream->m_bFirst == FALSE) {
					hStream->m_bFirst = TRUE;
					if (3000 < dwCTS) {
						LOG_D("ReaderReadFrame BaseCTS Cor %d", dwCTS);
						hStream->m_dwBaseCTS = 0;
						hStream->m_dwBaseCTSCor = dwCTS;
					}
				}
		}
		else
		{
			if (!hReader->m_bDisableNoData && !hReader->m_bLive) {
				LOG_D("ReaderReadFrame	-*- %s Packet is NULL!", (pMedia->tMediaType == AUDIO_MEDIA) ? "Audio" : "Video");
				LOG_D("ReaderReadFrame	-*- hStream->m_dwRange : [%8d], hStream->m_dwCurCTS : [%8d]",
					hStream->m_dwRange, hStream->m_dwCurCTS);
			}

			//TRACEX(DTB_LOG_INFO, "[PSReader] Base CTS %7d CTS %7d", hStream->m_dwBaseCTS, hStream->m_dwCurCTS);

			// Check for Vod end condition
			if (!hReader->m_bLive && hStream->m_dwRange <= hStream->m_dwCurCTS + 2000) {
				// No rtp packets received for RTP_NO_END_PRECV_TIMEOUT around the end of streaming
				if (RTP_NO_END_PRECV_TIMEOUT < McGetSubtract(FrGetTickCount(), hStream->m_dwLastRtpTick)) {
					LOG_I("ReaderReadFrame %p	-*- ABS(%d - %d) <= 2000 Return is COMMON_ERR_ENDOFDATA", hReader, hStream->m_dwRange, hStream->m_dwCurCTS);
					return	COMMON_ERR_ENDOFDATA;
				}
				else {
					//TRACE("[PSReader] %p	-*- TrickPlay (Media %d). Not received rtp data during %8d (cur %8d, last %8d).",
					//		hReader, hStream->m_iStreamType, FrGetTickCount() - hStream->m_dwLastRtpTick, FrGetTickCount(), hStream->m_dwLastRtpTick);
					return COMMON_ERR_NODATA;
				}
			}
			// when RTP data has not been received longer than at least RTP_NO_PRECV_TIMEOUT
			else if (RTP_NO_PRECV_TIMEOUT < (DWORD32)ABS(FrGetTickCount() - hStream->m_dwLastRtpTick)) {
				if (hReader->m_bLive == FALSE) {
					// NoSyncMode for VoD
					if (hReader->m_bDisableNoData) {
						if (hStream->m_iStreamType == AUDIO_MEDIA) {
							if (hReader->m_bVideoExist) {
								PSStreamHandle	hVideoStream = &hReader->m_Stream[hReader->m_dwVideoIdx];

								if (RTP_NO_PRECV_TIMEOUT < (DWORD32)ABS(FrGetTickCount() - hVideoStream->m_dwLastRtpTick)) {
									LOG_E("ReaderReadFrame %p	-*- Not received rtp audio data during %d, %d for VoD stream (and rpt timeout for video). return COMMON_ERR_SVR_NODATA",
										hReader, FrGetTickCount(), hStream->m_dwLastRtpTick);
									return	COMMON_ERR_SVR_NODATA;
								}
								else {
									return	COMMON_ERR_NODATA;
								}
							}
							else {
								LOG_E("ReaderReadFrame %p	-*- Not received rtp audio data during %d, %d for VoD stream. return COMMON_ERR_SVR_NODATA",
									hReader, FrGetTickCount(), hStream->m_dwLastRtpTick);
								return	COMMON_ERR_SVR_NODATA;
							}
						}
						else {
							LOG_E("ReaderReadFrame %p	-*- Not received rtp video data during %d, %d for VoD stream. return COMMON_ERR_SVR_NODATA",
								hReader, FrGetTickCount(), hStream->m_dwLastRtpTick);
							return	COMMON_ERR_SVR_NODATA;
						}
					}
					else {
						LOG_E("ReaderReadFrame %p	-*- Not received rtp data during %d, %d for VoD %d stream. return COMMON_ERR_SVR_NODATA",
							hReader, FrGetTickCount(), hStream->m_dwLastRtpTick, hStream->m_iStreamType);
						return	COMMON_ERR_SVR_NODATA;
					}
				}
				else {
					if (RTP_NO_PRECV_TIMEOUT < FrGetTickCount() - hStream->m_dwLastRtpTick) {
						LOG_D("ReaderReadFrame %p	-*- Not received rtp data during %d, %d for Live stream", hReader, FrGetTickCount(), hStream->m_dwLastRtpTick);
					}
				}
			}

			if (FrRTPIsRTCPBye(hStream->m_hRTP)) {
				LOG_I("ReaderReadFrame	-*- RTP is RTCP Bye - Return is COMMON_ERR_ENDOFDATA");
				return	COMMON_ERR_ENDOFDATA;
			}
			if (FRFAILED(hReader->m_lError)) {
				LOG_I("ReaderReadFrame	-*- hReader Error - Return is hStream->m_lError [0x%X]", hStream->m_lError);
				return	COMMON_ERR_ENDOFDATA;
			}
			if (FRFAILED(hStream->m_lError)) {
				LOG_I("ReaderReadFrame	-*- hStream Error - Return is hStream->m_lError [0x%X]", hStream->m_lError);
				return	COMMON_ERR_ENDOFDATA;
			}

			if (!hReader->m_bDisableNoData && !hReader->m_bLive)
				LOG_D("ReaderReadFrame	-*- The other Error - Return is COMMON_ERR_NODATA, bVideoReorder(%d)", bVideoReorder);
			if (bVideoReorder) {
				return COMMON_ERR_NODATA;
			}
			else
				return	COMMON_ERR_NODATA;
		}
	} // end of while

	pMedia->dwCTS = hStream->m_dwCurCTS = hStream->m_dwBaseCTS + dwCTS - hStream->m_dwBaseCTSCor;
	pMedia->dwFrameNum = 1;
	//pMedia->dwDeltaCTS = hStream->m_dwDeltaCTS;
	//pMedia->eTimeStamp = TIMESTAMP_PTS;
	hStream->m_dwPrevTick = 0;

	switch (hStream->m_dwFourCC)
	{
	case	FOURCC_AVC1:
	case	FOURCC_H264:
		if (is_a_h264_frame_intra(pMedia->pFrame, pMedia->dwFrameLen[0])) {
			pMedia->tFrameType[0] = FRAME_I;
			//TRACEX(DTB_LOG_TRACE, "[PSReader]	-*- FRAME_P");
		}
		else
			pMedia->tFrameType[0] = FRAME_P;
		
		{
			INT32		iLen = pMedia->dwFrameLen[0];
			DWORD32	dwNalLen;
			BYTE* pFrame = pMedia->pFrame;

			while (iLen > 0)
			{
				dwNalLen = ConvByteToDWORD(pFrame) + 4;
				pFrame[0] = 0; pFrame[1] = 0; pFrame[2] = 0; pFrame[3] = 1;
				if ((INT32)dwNalLen > iLen)
					return	FALSE;
				iLen -= dwNalLen;
				if (iLen < 0)
					return	FALSE;
				pFrame += dwNalLen;
				if (pFrame >= pMedia->pFrame + pMedia->dwFrameLen[0])
					break;
			}
		}
		
		break;
	case	FOURCC_HEVC:
	case	FOURCC_H265:
		if (is_a_h265_frame_intra(pMedia->pFrame, pMedia->dwFrameLen[0]))
			pMedia->tFrameType[0] = FRAME_I;
		else
			pMedia->tFrameType[0] = FRAME_P;

		if (pMedia->dwFrameLen[0])
			pMedia->dwFrameLen[0] = check_h2645_rtp_nal_stream(pMedia->pFrame, pMedia->dwFrameLen[0]);

		break;
	default:
		pMedia->tFrameType[0] = FRAME_UNKNOWN;
		break;

	}


	if (pMedia->tMediaType == VIDEO_MEDIA)
		LOG_D("Read Video Type(%d), Size=%6d Base=%8d CTS=%8d [%5d]", pMedia->tFrameType[0], pMedia->dwFrameLen[0], hStream->m_dwBaseCTS, pMedia->dwCTS, dwCTS);
	
	//if (pMedia->m_tMediaType == AUDIO_MEDIA)
	//	TRACEX(DTB_LOG_TRACE, "[PSReader] Read Audio Size=%6d Base=%8d CTS=%8d", pMedia->m_dwFrameLen[0], hStream->m_dwBaseCTS, pMedia->m_dwCTS);
	
	return	FR_OK;
}

BOOL RTSPReaderSeek(void* pVoid, DWORD32* pdwCTS, BOOL bKeyFrame) {
	RTSPReaderHandle	hReader = (RTSPReaderHandle)pVoid;
	PSStreamHandle	hStream = NULL;
	LRSLT			lRet;

	//*pdwCTS = 25966;

	LOG_I("[PSReader] Seek Starts (CTS %d)", *pdwCTS);

	if (!hReader->m_bPaused) {
		lRet = RTSPPause(hReader);
		if (FRFAILED(lRet))
			return	FALSE;
		hReader->m_bPaused = TRUE;
	}
	// empty reoder buffer
	//StreamPSBufferReset(hReader->m_hReorderBuffer);

	lRet = RTSPPlay(hReader, FALSE, *pdwCTS, TRUE);
	if (FRFAILED(lRet))
		return	FALSE;

	*pdwCTS = FrRTSPGetPlayFromRange(hReader->m_hRTSP);

	hReader->m_bPaused = FALSE;

	LOG_I("[PSReader] Seek Ends (seeked CTS %d)", *pdwCTS);

	return	TRUE;
}

BOOL RTSPReaderGetStatus(void* pVoid, void* p) {
	RTSPReaderHandle	hReader = (RTSPReaderHandle)pVoid;
	PSStreamHandle	hStream = NULL;
	FrReaderStatus* pStatus = (FrReaderStatus*)p;
	DWORD32			dwBufferingRatio = 100;
	DWORD32			dwRemainDur, dwDur;

	DWORD32			dwRet, dwCurBuffSize;

	DWORD32         dwBufferedTime = MAX_DWORD,	dwBufferingTime;

	if (hReader->m_bAudioExist)	{
		hStream = &hReader->m_Stream[hReader->m_dwAudioIdx];
		dwCurBuffSize = UtilBufGetBufferedSize(hStream->m_hRTPBuf);
		pStatus->m_dwAudioBuffRatio = dwCurBuffSize * 100 / hStream->m_dwMaxBufSize;
		dwRet = dwCurBuffSize * 100 / hStream->m_dwInitBufSize;
		if (dwRet < dwBufferingRatio)
			dwBufferingRatio = dwRet;
		//TRACE("[NOAReader]			AudioBuff = %d%%", pStatus->m_dwAudioBuffRatio);
	}
	else {
		pStatus->m_dwAudioBuffRatio = 0;
	}

	if (hReader->m_bVideoExist)	{
		hStream = &hReader->m_Stream[hReader->m_dwVideoIdx];
		dwCurBuffSize = UtilBufGetBufferedSize(hStream->m_hRTPBuf);
		pStatus->m_dwVideoBuffRatio = dwCurBuffSize * 100 / hStream->m_dwMaxBufSize;
		dwRet = dwCurBuffSize * 100 / hStream->m_dwInitBufSize;
		if (dwRet < dwBufferingRatio)
			dwBufferingRatio = dwRet;
		//TRACE("[NOAReader]			VideoBuff = %d%%", pStatus->m_dwVideoBuffRatio);
	}
	else {
		pStatus->m_dwVideoBuffRatio = 0;
	}

	pStatus->m_dwBufferingRatio = dwBufferingRatio;

	return	TRUE;
}

LRSLT RTSPReaderSetStatus(void* pVoid, INT32 iStatus, INT32 nScale, QWORD qwCurFileSize) {
	RTSPReaderHandle	hReader = (RTSPReaderHandle)pVoid;
	LRSLT			lRet;

	if (iStatus == READ_PLAY) {
		if (hReader->m_bPaused) {
			lRet = RTSPPlay(hReader, TRUE, 0, 0);
			if (FRFAILED(lRet))
				return	lRet;
			hReader->m_bPaused = FALSE;
		}
	}
	else if (iStatus == READ_PAUSE) {
		if (!hReader->m_bPaused) {
			LOG_I("[NOAReader] Status Change to ReadPause Starts");
			lRet = RTSPPause(hReader);
			if (FRFAILED(lRet))
				return	lRet;
			hReader->m_bPaused = TRUE;
			LOG_I("[NOAReader] Status Change to ReadPause Ends");
		}
	}
	else if (iStatus == READ_STOP) {
	}

	return	FR_OK;
}

const FrReader FrRTSPReader =
{
	"RTSPReader",			// pReaderName;
	RTSPReaderOpen,			// ReaderOpen
	RTSPReaderClose,			// ReaderClose
	NULL,					// ReaderIsValidInfo
	RTSPReaderGetInfo,		// ReaderGetInfo
	RTSPReaderStart,			// ReaderStart
	NULL,					// ReaderMakeIndex
	RTSPReaderReadFrame,		// ReaderReadFrame
	RTSPReaderSeek,			// ReaderSeekByTime
	NULL,					// ReaderSeekByKeyFrame
	NULL,					// ReaderSeekBySubtitle
	NULL,					// ReaderSeekByMedia
	NULL,	// ReaderSeekByClock
	NULL,					// ReaderSetTrackID
	RTSPReaderSetStatus,		// ReaderSetStatus
	RTSPReaderGetStatus,		// ReaderGetStatus
	NULL,		// ReaderGetCurTime
	NULL,					// ReaderSetTotalRange
	NULL,					// ReaderSeekByVideo
	NULL,					// ReaderSetFeedback
	NULL,    // ReaderGetFeedback
	NULL,		// ReaderSetFactor
	NULL,		// ReaderGetBitrate
	NULL,					// ReaderOpen2
	NULL,					// ReaderSeekPrevFrame
	NULL,					// ReaderSeekNextFrame
	NULL,					// ReaderSetDFSReadSize
};
#endif