vespa 폴더 설명

실제 사용할때 필요한 폴더만 적었습니다.

App : 각종 모듈 검증을 위한 test program 폴더 (ex. TestMMC)
Bin : 최종 바이너리(.exe)파일이 생성되는 폴더 (ex. TestMMC.exe)
Engine : 어플팀 전달을 위한 라이브러리를 생성하는 폴더 (ex. ESMMovieMaker)
SubModule : 내부 라이브러리에서 사용하는 모듈을 생성하는 폴더 (ex. ESMFFDemuxer)
include : 모든 폴더에서 사용하는 전역 헤더 파일
