#include "SeiGen.h"

unsigned char* FdrSeiGen::rbsp2ebsp(int* ebsp_size, unsigned char* rbsp, int rbsp_size) {
	unsigned char* ebsp = (unsigned char*)malloc(sizeof(unsigned char) * rbsp_size);
	int j = 0;
	int count = 0;

	for (int i = 0; i < rbsp_size; i++) {
		if (count >= 2 && !(rbsp[i] & 0xFC)) {
			j++;
			//realloc(ebsp, rbsp_size + j - i);
			ebsp = (unsigned char*)realloc(ebsp, rbsp_size + j - i);
			ebsp[j - 1] = 0x03;



			count = 0;
		}
		ebsp[j] = rbsp[i];



		if (rbsp[i] == 0x00)
			count++;
		else
			count = 0;
		j++;
	}
	*ebsp_size = j;



	return ebsp;
}

void FdrSeiGen::SetPoseReconFrame(int nByteSize, unsigned char* pPoseData) {
	if ((pPoseData != NULL) && (nByteSize > 0)) {

		int nPoseDataByteSize = nByteSize;

		ptrPoseReconSeiMessage = std::shared_ptr<SEI_MESSAGE_T>(new SEI_MESSAGE_T());
		ptrPoseReconSeiMessage->seiType = ARGUMENTED_REALITY;

		char* poseReconMessage = NULL;
		char	startCode[4] = { 0x00, 0x00, 0x00, 0x01 };
		int		seiHeaderSize; //nalu + seitype

		//if (_ctx._videoInfo.codecId == CodecH265)
		//	seiHeaderSize = 4;
		//else
			seiHeaderSize = 3;

		int poseReconHeaderSize = sizeof(ISO_IEC_11578_AR);
		

		int poseReconSize = sizeof(startCode) + seiHeaderSize + poseReconHeaderSize + nPoseDataByteSize + 1;//+ positionSwipeElemCount * positionSwipeElemSize;
		int seiSizeByteCount = (poseReconHeaderSize + nPoseDataByteSize) / 255;
		int seiSizeByteRemain = (poseReconHeaderSize + nPoseDataByteSize) % 255;

		//seiHeaderSize++;				 
		poseReconSize += seiSizeByteCount;

		poseReconMessage = (char*)malloc(poseReconSize);//(startCode) + seiHeaderSize + positionSwipeSize + 1);
		int poseReconIndex = 0;
		::memmove(&poseReconMessage[poseReconIndex], &startCode, sizeof(startCode));
		poseReconIndex += 4;

		//if (_ctx._videoInfo.codecId == CodecH265) {
		//	positionSwipeMessage[positionSwipeIndex] = 0x4e;//nalu type  4e01
		//	positionSwipeIndex++;
		//	positionSwipeMessage[positionSwipeIndex] = 0x01;//nalu type
		//	positionSwipeIndex++;
		//}
		//else {
			poseReconMessage[poseReconIndex] = 0x06;//nalu type
			poseReconIndex++;
		//}

		poseReconMessage[poseReconIndex] = 0x05;//user data unregistered sei message
		poseReconIndex++;

		for (int i = 0; i < seiSizeByteCount; i++)
		{
			poseReconMessage[poseReconIndex] = 0xFF;
			poseReconIndex++;
		}
		poseReconMessage[poseReconIndex] = seiSizeByteRemain;
		poseReconIndex++;

		::memmove(&poseReconMessage[poseReconIndex], ISO_IEC_11578_AR, sizeof(ISO_IEC_11578_AR));
		poseReconIndex += sizeof(ISO_IEC_11578_AR);

		::memmove(&poseReconMessage[poseReconIndex], pPoseData, nPoseDataByteSize);
		poseReconIndex += nPoseDataByteSize;

		poseReconMessage[poseReconIndex] = 0x80; // rbsp stop one bit and trailing bits
		poseReconIndex += 1;

		ptrPoseReconSeiMessage->seiMessage = poseReconMessage;
		ptrPoseReconSeiMessage->seiMessageLength = poseReconSize;
	}
	else {
		ptrPoseReconSeiMessage.reset();
	}
}

FdrSeiGen::FdrSeiGen() {

}

FdrSeiGen::~FdrSeiGen() {

}


