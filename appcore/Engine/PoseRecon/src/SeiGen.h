#pragma once

//#include "SystemAPI.h"
//#include "mediainfo.h"
//#include "StreamReaderAPI.h"
//#include "TraceAPI.h"
//#include "SocketAPI.h"
//#include "PoseReconAPI.h"

#include <string>
#include <thread>
#include <mutex>
#include <vector>

struct bbox_t {
	unsigned int x, y, w, h;
	float prob;
	unsigned int obj_id;
	unsigned int track_id;
	unsigned int frames_counter;
	float x_3d, y_3d, z_3d;
};
struct image_t {
	int h;
	int w;
	int c;
	float* data;
};
struct TRTJointPoint {
	int px;
	int py;
};

struct stPose_data_t {
	bbox_t stDetection_data;
	TRTJointPoint arrJointset[18];
};

struct detection_data_CMS {
	std::vector<bbox_t> result_vec;	// detection result data about current frame
	std::vector<stPose_data_t> PoseResult_vec; // Pose Estimation result data about current frame
	uint64_t frame_id; // current frame number
	int trt_camera_number; // camera number of current frame
	bool slow_motion_trigger;
	TRTJointPoint hit_coord;

	///////////// use when tracking function is operated ////////////////////
	bool new_detection;
	bool exit_flag;
	/////////////////////////////////////////////////////////////////////////

	// initialization structure
	detection_data_CMS() : new_detection(false), exit_flag(false) {}
};


enum SEI_TYPE_T
{
	POSITION_SWIPE = 0,
	ARGUMENTED_REALITY = 1,
	MULTIVIEW = 2,
	POSE_RECON = 3,
};

typedef struct _SEI_MESSAGE_T
{
	int		seiType;
	char* seiMessage;
	int		seiMessageLength;
	_SEI_MESSAGE_T()
		: seiType(POSITION_SWIPE)
		, seiMessage(NULL)
		, seiMessageLength(0)
	{
	}

	~_SEI_MESSAGE_T()
	{
		if (seiMessage && seiMessageLength > 0)
		{
			free(seiMessage);
			seiMessageLength = 0;
		}
	}
} SEI_MESSAGE_T;


class FdrSeiGen {
public:
	FdrSeiGen();
	~FdrSeiGen();

	int Open();
	int Close();

	void SetPoseReconFrame(int nByteSize, unsigned char* pPoseData);

private:
	
	std::mutex	m_xStatus;
	//std::thread* m_hTask;
	//void* TaskMgPlayer(void* arg);
	
	void MakeDetectData(rapidjson::Value& arrDetectData, rapidjson::Document::AllocatorType& _allocator);

	const unsigned char ISO_IEC_11578_AR[16] = { 0xbc, 0x97, 0xb8, 0x4d, 0x96, 0x9f, 0x48, 0xb9, 0xbc, 0xe4, 0x7c, 0x1c, 0x1a, 0x39, 0x2f, 0x37 };
	const unsigned char ISO_IEC_11578_POSITION_SWIPE[16] = { 0x62, 0x63, 0x00, 0xe0, 0x97, 0x87, 0x46, 0x1d, 0xa1, 0x25, 0x4d, 0x5c, 0x12, 0x49, 0xbc, 0x99 };
	const unsigned char ISO_IEC_11578_MULTIVIEW[16] = { 0xe1, 0x3b, 0x76, 0x75, 0x6d, 0xb0, 0x46, 0x1b, 0xa3, 0xe7, 0xea, 0xf8, 0x01, 0xc9, 0x38, 0xc1 };

	unsigned char* rbsp2ebsp(int* ebsp_size, unsigned char* rbsp, int rbsp_size);

	std::shared_ptr<SEI_MESSAGE_T> ptrPoseReconSeiMessage;
};

