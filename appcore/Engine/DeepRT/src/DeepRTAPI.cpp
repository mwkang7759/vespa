
#include "DeepRTAPI.h"
#include "DeepRT.h"

FrDeepRT::FrDeepRT() {
	m_hDeep = new DeepRT();
}

FrDeepRT::~FrDeepRT() {
	delete m_hDeep;
}

int FrDeepRT::Run(Param& param) {
	m_hDeep->Run(param);
	
	return 1;
}

int FrDeepRT::Close() {
	m_hDeep->Close();
	
	return 1;
}