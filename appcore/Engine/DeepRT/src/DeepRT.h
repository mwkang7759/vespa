#pragma once

#include "SystemAPI.h"
#include "mediainfo.h"
#include "DecoderAPI.h"
#include "EncoderAPI.h"
#include "ConverterAPI.h"
#include "StreamReaderAPI.h"
#include "StreamWriterAPI.h"
#include "TraceAPI.h"
#include "SocketAPI.h"
#include "DeviceOutAPI.h"
#include "DeepRTAPI.h"

#include "SeiGen.h"

#include <string>
#include <thread>
#include <mutex>
#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/cudaimgproc.hpp>

#include <cuda_runtime.h>

#include <fstream>
#include <DeepInfer.h>
//#include <sld_object_detector.h>
//#include <sld_pose_estimator.h>

#define		MAX_TIME_DIFF				2000

#define		AV_SYNC_LOWER_LIMIT		0
#define		AV_SYNC_UPPER_LIMIT		(AV_SYNC_LOWER_LIMIT + 200)
#define		AV_SYNC_HIGH_LIMIT		(AV_SYNC_LOWER_LIMIT + 600)


typedef enum {
	CODEC_NVIDIA,
	CODEC_FFMPEG
};

typedef enum {
	STOP,
	READY,
	BUFFERING,
	PAUSE,
	PLAYING,
	STEP,
} PLAYER_STATUS;

typedef struct {
	std::string url;
	int codecType;
	HANDLE	wnd;

} PLAYER_PARAM;

class FrDeepRT::DeepRT {
public:
	DeepRT();
	~DeepRT();

	int Open(FrDeepRT::Param& param);
	int Close();

	void Run(FrDeepRT::Param& param);

	// set callback..
	// ..


private:
	BOOL	m_bEnd;
	FrReaderHandle	m_hReader;
	
	FrVideoDecHandle m_hDecVideo;
	FrVideoEncHandle m_hEncVideo;
	McVideoOutHandle m_hDevOut;
	FrWriterHandle	m_hWriter;
	FrVideoConvHandle	m_hConv;
	

	FrURLInfo	m_UrlInfo;
	FrMediaInfo m_SrcInfo;
	FrMediaInfo m_EncInfo;

	FrMediaStream m_VideoStream;
	FrRawVideo m_RawVideo;
	
	FrTimeHandle		m_hTime;

	///////////////////////////////////////////////////////////////////////////
	// SeiGen
	FdrSeiGen m_Sei;
	///////////////////////////////////////////////////////////////////////////

	
	int					m_iStatus;

	BOOL				m_bVideoRead;			// read video
	BOOL				m_bVideoDecode;			// decode video
	BOOL				m_bVideoRender;			// render video 
	BOOL				m_bSync;
	BOOL				m_bScale;

	DeepInfer* _deepInfer;
	DeepInfer::context_t _deepInfer_ctx;

	std::mutex	m_xStatus;
	std::thread* m_hTask;
	void* TaskDeepSession(void* arg);
};