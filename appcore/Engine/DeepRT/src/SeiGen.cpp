#include "SeiGen.h"

void FdrSeiGen::MakeDetectData(rapidjson::Value& objDetect, rapidjson::Document::AllocatorType& _allocator) {
	rapidjson::Value arrDetectData(rapidjson::kArrayType);

	for (int i = 0; i<m_vDetectData.size(); i++) {

		rapidjson::Value objBoxData(rapidjson::kObjectType);

		objBoxData.AddMember("x", m_vDetectData.at(i).x, _allocator);
		objBoxData.AddMember("y", m_vDetectData.at(i).x, _allocator);
		objBoxData.AddMember("w", m_vDetectData.at(i).x, _allocator);
		objBoxData.AddMember("h", m_vDetectData.at(i).x, _allocator);

		objBoxData.AddMember("prob", m_vDetectData.at(i).x, _allocator);
		objBoxData.AddMember("objId", m_vDetectData.at(i).x, _allocator);
		objBoxData.AddMember("trackId", m_vDetectData.at(i).x, _allocator);
		objBoxData.AddMember("frameCnt", m_vDetectData.at(i).x, _allocator);
		objBoxData.AddMember("x_3d", m_vDetectData.at(i).x, _allocator);
		objBoxData.AddMember("y_3d", m_vDetectData.at(i).x, _allocator);
		objBoxData.AddMember("z_3d", m_vDetectData.at(i).x, _allocator);

		arrDetectData.PushBack(objBoxData, _allocator);
	}

	rapidjson::Value objBox(rapidjson::kObjectType);
	objBox.AddMember("box", arrDetectData, _allocator);

	objDetect.AddMember("detect", objBox, _allocator);
}

void FdrSeiGen::MakePoseData(rapidjson::Value& objPose, rapidjson::Document::AllocatorType& _allocator) {
	rapidjson::Value arrPoseData(rapidjson::kArrayType);

	for (int i = 0; i < m_vPoseData.size(); i++) {
		rapidjson::Value objBoxData(rapidjson::kObjectType);

		objBoxData.AddMember("x", m_vPoseData.at(i).stDetection_data.x, _allocator);
		objBoxData.AddMember("y", m_vPoseData.at(i).stDetection_data.x, _allocator);
		objBoxData.AddMember("w", m_vPoseData.at(i).stDetection_data.x, _allocator);
		objBoxData.AddMember("h", m_vPoseData.at(i).stDetection_data.x, _allocator);

		objBoxData.AddMember("prob", m_vPoseData.at(i).stDetection_data.x, _allocator);
		objBoxData.AddMember("objId", m_vPoseData.at(i).stDetection_data.x, _allocator);
		objBoxData.AddMember("trackId", m_vPoseData.at(i).stDetection_data.x, _allocator);
		objBoxData.AddMember("frameCnt", m_vPoseData.at(i).stDetection_data.x, _allocator);
		objBoxData.AddMember("x_3d", m_vPoseData.at(i).stDetection_data.x, _allocator);
		objBoxData.AddMember("y_3d", m_vPoseData.at(i).stDetection_data.x, _allocator);
		objBoxData.AddMember("z_3d", m_vPoseData.at(i).stDetection_data.x, _allocator);

		rapidjson::Value arrJointData(rapidjson::kArrayType);
		for (int j = 0; j < 18; j++) {
			rapidjson::Value objJointData(rapidjson::kObjectType);
			
			objJointData.AddMember("px", m_vPoseData.at(i).arrJointset[j].px, _allocator);
			objJointData.AddMember("py", m_vPoseData.at(i).arrJointset[j].py, _allocator);			
			
			arrJointData.PushBack(objJointData, _allocator);
		}
		objBoxData.AddMember("joint", arrJointData, _allocator);
		
		arrPoseData.PushBack(objBoxData, _allocator);
	}

	rapidjson::Value objBox(rapidjson::kObjectType);
	objBox.AddMember("box", arrPoseData, _allocator);

	objPose.AddMember("pose", objBox, _allocator);
}

void FdrSeiGen::MakeHitData(rapidjson::Value& objHit, rapidjson::Document::AllocatorType& _allocator) {
	rapidjson::Value arrHitData(rapidjson::kArrayType);
	for (int i = 0; i < m_vHitData.size(); i++) {
		rapidjson::Value objJointData(rapidjson::kObjectType);

		objJointData.AddMember("px", m_vHitData.at(i).px, _allocator);
		objJointData.AddMember("py", m_vHitData.at(i).py, _allocator);
		
		arrHitData.PushBack(objJointData, _allocator);
	}
	objHit.AddMember("joint", arrHitData, _allocator);
}

void FdrSeiGen::MakeAuraData(rapidjson::Value& objAura, rapidjson::Document::AllocatorType& _allocator) {
	rapidjson::Value arrAuraData(rapidjson::kArrayType);
	for (int i = 0; i < m_vAuraData.size(); i++) {
		rapidjson::Value objJointData(rapidjson::kObjectType);

		objJointData.AddMember("px", m_vAuraData.at(i).px, _allocator);
		objJointData.AddMember("py", m_vAuraData.at(i).py, _allocator);

		arrAuraData.PushBack(objJointData, _allocator);
	}
	objAura.AddMember("joint", arrAuraData, _allocator);
}

void FdrSeiGen::MakeActionData(rapidjson::Value& objAction, rapidjson::Document::AllocatorType& _allocator) {
	
	objAction.AddMember("key", "1234", _allocator);
	objAction.AddMember("matchNumber", "2", _allocator);
	objAction.AddMember("phase", "final", _allocator);
	objAction.AddMember("action", "HOME_KICK", _allocator);
	objAction.AddMember("round", 1, _allocator);
	objAction.AddMember("roundTime", "1:27", _allocator);
	objAction.AddMember("homeBonusTime", "5", _allocator);
	objAction.AddMember("awayBonusTime", "0", _allocator);
	
	rapidjson::Value objScore(rapidjson::kObjectType);
	objScore.AddMember("home", 87, _allocator);
	objScore.AddMember("away", 60, _allocator);
	objScore.AddMember("homeHit", 7, _allocator);
	objScore.AddMember("awayHit", 0, _allocator);
	objScore.AddMember("homeApply", 14, _allocator);
	objScore.AddMember("awayApply", 0, _allocator);
	objAction.AddMember("score", objScore, _allocator);


	rapidjson::Value objPenalties(rapidjson::kObjectType);
	objPenalties.AddMember("homeAll", 3, _allocator);
	objPenalties.AddMember("awayAll", 3, _allocator);
	objPenalties.AddMember("home", 3, _allocator);
	objPenalties.AddMember("away", 3, _allocator);
	objAction.AddMember("penalties", objPenalties, _allocator);


	rapidjson::Value objResult(rapidjson::kObjectType);
	objResult.AddMember("home", 2, _allocator);
	objResult.AddMember("away", 2, _allocator);
	objResult.AddMember("winner", "blue", _allocator);
	objResult.AddMember("winType", "PTF", _allocator);
	objAction.AddMember("result", objResult, _allocator);

}

std::string FdrSeiGen::MakeJson() {
	std::string mapSerialize = "";

	rapidjson::Document doc;
	rapidjson::Document::AllocatorType& _allocator = doc.GetAllocator();

	doc.SetObject();

	doc.AddMember("frameId", 1, doc.GetAllocator());
	doc.AddMember("camNumber", 2, doc.GetAllocator());


	rapidjson::Value objDetect(rapidjson::kObjectType);
	MakeDetectData(objDetect, _allocator);
	doc.AddMember("detect", objDetect, _allocator);

	rapidjson::Value objPose(rapidjson::kObjectType);
	MakePoseData(objPose, _allocator);
	doc.AddMember("pose", objPose, _allocator);

	rapidjson::Value objHit(rapidjson::kObjectType);
	MakeHitData(objHit, _allocator);
	doc.AddMember("hit", objHit, _allocator);

	rapidjson::Value objAura(rapidjson::kObjectType);
	MakeAuraData(objAura, _allocator);
	doc.AddMember("aura", objAura, _allocator);

	rapidjson::Value objAction(rapidjson::kObjectType);
	MakeActionData(objAction, _allocator);
	doc.AddMember("matchAction", objAction, _allocator);




	
	rapidjson::StringBuffer buffer; buffer.Clear();
	rapidjson::Writer<rapidjson::StringBuffer> jsonWriter(buffer);

	doc.Accept(jsonWriter);
	mapSerialize = buffer.GetString();

	return mapSerialize;
}

unsigned char* FdrSeiGen::rbsp2ebsp(int* ebsp_size, unsigned char* rbsp, int rbsp_size) {
	unsigned char* ebsp = (unsigned char*)malloc(sizeof(unsigned char) * rbsp_size);
	int j = 0;
	int count = 0;

	for (int i = 0; i < rbsp_size; i++) {
		if (count >= 2 && !(rbsp[i] & 0xFC)) {
			j++;
			//realloc(ebsp, rbsp_size + j - i);
			ebsp = (unsigned char*)realloc(ebsp, rbsp_size + j - i);
			ebsp[j - 1] = 0x03;



			count = 0;
		}
		ebsp[j] = rbsp[i];



		if (rbsp[i] == 0x00)
			count++;
		else
			count = 0;
		j++;
	}
	*ebsp_size = j;



	return ebsp;
}

void FdrSeiGen::SetPoseReconFrame(int nByteSize, unsigned char* pPoseData) {
	if ((pPoseData != NULL) && (nByteSize > 0)) {

		int nPoseDataByteSize = nByteSize;

		m_ptrPoseSeiMessage = std::shared_ptr<SEI_MESSAGE_T>(new SEI_MESSAGE_T());
		m_ptrPoseSeiMessage->seiType = ARGUMENTED_REALITY;

		char* poseReconMessage = NULL;
		char	startCode[4] = { 0x00, 0x00, 0x00, 0x01 };
		int		seiHeaderSize; //nalu + seitype

		//if (_ctx._videoInfo.codecId == CodecH265)
		//	seiHeaderSize = 4;
		//else
			seiHeaderSize = 3;

		int poseReconHeaderSize = sizeof(ISO_IEC_11578_AR);
		

		int poseReconSize = sizeof(startCode) + seiHeaderSize + poseReconHeaderSize + nPoseDataByteSize + 1;//+ positionSwipeElemCount * positionSwipeElemSize;
		int seiSizeByteCount = (poseReconHeaderSize + nPoseDataByteSize) / 255;
		int seiSizeByteRemain = (poseReconHeaderSize + nPoseDataByteSize) % 255;

		//seiHeaderSize++;				 
		poseReconSize += seiSizeByteCount;

		poseReconMessage = (char*)malloc(poseReconSize);//(startCode) + seiHeaderSize + positionSwipeSize + 1);
		int poseReconIndex = 0;
		::memmove(&poseReconMessage[poseReconIndex], &startCode, sizeof(startCode));
		poseReconIndex += 4;

		//if (_ctx._videoInfo.codecId == CodecH265) {
		//	positionSwipeMessage[positionSwipeIndex] = 0x4e;//nalu type  4e01
		//	positionSwipeIndex++;
		//	positionSwipeMessage[positionSwipeIndex] = 0x01;//nalu type
		//	positionSwipeIndex++;
		//}
		//else {
			poseReconMessage[poseReconIndex] = 0x06;//nalu type
			poseReconIndex++;
		//}

		poseReconMessage[poseReconIndex] = 0x05;//user data unregistered sei message
		poseReconIndex++;

		for (int i = 0; i < seiSizeByteCount; i++)
		{
			poseReconMessage[poseReconIndex] = 0xFF;
			poseReconIndex++;
		}
		poseReconMessage[poseReconIndex] = seiSizeByteRemain;
		poseReconIndex++;

		::memmove(&poseReconMessage[poseReconIndex], ISO_IEC_11578_AR, sizeof(ISO_IEC_11578_AR));
		poseReconIndex += sizeof(ISO_IEC_11578_AR);

		::memmove(&poseReconMessage[poseReconIndex], pPoseData, nPoseDataByteSize);
		poseReconIndex += nPoseDataByteSize;

		poseReconMessage[poseReconIndex] = 0x80; // rbsp stop one bit and trailing bits
		poseReconIndex += 1;

		m_ptrPoseSeiMessage->seiMessage = poseReconMessage;
		m_ptrPoseSeiMessage->seiMessageLength = poseReconSize;
	}
	else {
		m_ptrPoseSeiMessage.reset();
	}
}

FdrSeiGen::FdrSeiGen() {

}

FdrSeiGen::~FdrSeiGen() {

}


