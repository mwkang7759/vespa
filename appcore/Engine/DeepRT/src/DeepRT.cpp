#include "DeepRT.h"
#include "DeepBase.h"

#if defined(_DEBUG)
#pragma comment(lib, "opencv_core440d.lib")
#pragma comment(lib, "opencv_highgui440d.lib")
#pragma comment(lib, "opencv_imgproc440d.lib")
#pragma comment(lib, "opencv_imgcodecs440d.lib")
#pragma comment(lib, "opencv_calib3d440d.lib")
#pragma comment(lib, "opencv_cudaimgproc440d.lib")
#pragma comment(lib, "opencv_cudawarping440d.lib")
#pragma comment(lib, "opencv_cudaarithm440d.lib")
#else
#pragma comment(lib, "opencv_core440.lib")
#pragma comment(lib, "opencv_highgui440.lib")
#pragma comment(lib, "opencv_imgproc440.lib")
#pragma comment(lib, "opencv_imgcodecs440.lib")
#pragma comment(lib, "opencv_calib3d440.lib")
#pragma comment(lib, "opencv_cudaimgproc440.lib")
#pragma comment(lib, "opencv_cudawarping440.lib")
#pragma comment(lib, "opencv_cudaarithm440.lib")
#endif

//static cv::Scalar obj_id_to_color(int obj_id) {
//    int const colors[6][3] = { { 1,0,1 },{ 0,0,1 },{ 0,1,1 },{ 0,1,0 },{ 1,1,0 },{ 1,0,0 } };
//    int const offset = obj_id * 123457 % 6;
//    int const color_scale = 150 + (obj_id * 123457) % 100;
//    cv::Scalar color(colors[offset][0], colors[offset][1], colors[offset][2]);
//    color *= color_scale;
//    return color;
//}


FrDeepRT::DeepRT::DeepRT() {

}

FrDeepRT::DeepRT::~DeepRT() {

}

//std::vector<std::string> classesName;

int FrDeepRT::DeepRT::Open(FrDeepRT::Param& param) {
    DWORD32 dwStart = 0;

    //SetTraceRollingFileName((char*)"deep_rt.log", 20 * 1024 * 1024, 2);
    m_hTask = nullptr;
    m_bEnd = FALSE;

    LOG_I("============ DeepRT::Open() ================");
    LOG_I("DeepRT::Open() Begin..");

    FrSysInit();

    /*std::string path = param.detectRTEnginePath;
    std::string classFilePath;
    size_t pos = path.rfind("\\");
    if (pos != std::string::npos)
        classFilePath = path.substr(0, pos) + "\\" + "coco.names";
    
    std::string line;
    std::ifstream ifs(classFilePath.c_str());
    while (std::getline(ifs, line)) classesName.push_back(line);
    ifs.close();*/
    
    // reader
    //m_UrlInfo.pUrl = (char*)"c:\\test_contents\\test_2021_06_30_450.mp4";
    m_UrlInfo.pUrl = (char*)param.srcPath.c_str();
    LRSLT lRet = FrReaderOpen(&m_hReader, &m_UrlInfo);
    if (FRFAILED(lRet)) {
        return -1;
    }

    lRet = FrReaderGetInfo(m_hReader, &m_SrcInfo);
    if (FRFAILED(lRet)) {
        return -1;
    }

    // decoder
    FrVideoInfo* pSrcVideoInfo = &m_SrcInfo.FrVideo;
    // reset video resolution
    pSrcVideoInfo->dwWidth /= 2;
    pSrcVideoInfo->dwHeight /= 2;

    pSrcVideoInfo->eCodecType = CODEC_HW;
    pSrcVideoInfo->eColorFormat = VideoFmtBGRA;     // output color format, //VideoFmtBGRA
    pSrcVideoInfo->bLowDelayDecoding = TRUE;
    pSrcVideoInfo->memDir = MEM_GPU;   // or MEM_GPU
    lRet = FrVideoDecOpen(&m_hDecVideo, NULL, pSrcVideoInfo);
    if (FRFAILED(lRet)) {
        return -1;
    }

    // color conv
    lRet = FrVideoConvOpen(&m_hConv, pSrcVideoInfo, VideoFmtYV12);
    if (FRFAILED(lRet)) {
        return -1;
    }

    // encoder
    m_EncInfo.dwVideoTotal = 1;
    m_EncInfo.FrVideo.dwFourCC = FOURCC_H264;
    m_EncInfo.FrVideo.eCodecType = CODEC_HW;    //CODEC_HW;
    m_EncInfo.FrVideo.dwWidth = m_SrcInfo.FrVideo.dwWidth;
    m_EncInfo.FrVideo.dwHeight = m_SrcInfo.FrVideo.dwHeight;
    m_EncInfo.FrVideo.dwBitRate = 20 * 1024 * 1024;
    m_EncInfo.FrVideo.dwFrameRate = 30 * 1000;
    m_EncInfo.FrVideo.fIntraFrameRefresh = 1;
    m_EncInfo.FrVideo.eColorFormat = VideoFmtYV12;

    lRet = FrVideoEncOpen(&m_hEncVideo, &m_EncInfo.FrVideo);
    if (FRFAILED(lRet)) {
        return -1;
    }

    // writer
    FrURLInfo	DstUrl = {};
    //DstUrl.pUrl = (CHAR*)"c:\\test_contents\\deep_rt_out.mp4";
    DstUrl.pUrl = (char*)param.dstPath.c_str();
    DstUrl.URLType = _MP4_FF_FILE; //_MP4_FILE;
    lRet = FrWriterOpen(&m_hWriter, &DstUrl);
    if (FRFAILED(lRet)) {
        return -1;
    }

    lRet = FrWriterSetInfo(m_hWriter, &m_EncInfo, 0);
    if (FRFAILED(lRet)) {
        return -1;
    }

    LOG_I("DeepRT::Open() detector initialize() before..");
    _deepInfer = new DeepInfer();
    _deepInfer_ctx.width = m_SrcInfo.FrVideo.dwWidth;
    _deepInfer_ctx.height = m_SrcInfo.FrVideo.dwHeight;
    _deepInfer_ctx.detectPath = param.detectRTEnginePath;
    _deepInfer_ctx.estimatePath = param.poseRTEnginePath;
    _deepInfer->initialize(&_deepInfer_ctx);
    //_deepInfer->release();
    //bool bDLInit = _deepInfer->isInitialized();
    //delete[] _deepInfer; _deepInfer = nullptr;

    LOG_I("DeepRT::Open() estimator initialize() after..");

    lRet = FrReaderStart(m_hReader, &dwStart);
    if (FRFAILED(lRet)) {
        return -1;
    }


    LOG_I("DeepRT::Open() End..");

	return 1;
}

int FrDeepRT::DeepRT::Close() {
    LOG_I("DeepRT::Close() Begin..");

    m_bEnd = TRUE;    
    while (m_hTask != nullptr) {
        FrSleep(10);
    }

    if (m_hWriter != nullptr) {
        FrWriterUpdateInfo(m_hWriter);
        FrWriterClose(m_hWriter);
        m_hWriter = nullptr;
    }

    //_detector->release();
    //_estimator->release();

    if (m_hEncVideo != nullptr) {
        FrVideoEncClose(m_hEncVideo);
        m_hEncVideo = nullptr;
    }

    if (m_hDecVideo != nullptr) {
        FrVideoDecClose(m_hDecVideo);
        m_hDecVideo = nullptr;
    }

    if (m_hConv != nullptr) {
        FrVideoConvClose(m_hConv);
        m_hConv = nullptr;
    }

    if (m_hReader != nullptr) {
        FrReaderClose(m_hReader);
        m_hReader = nullptr;
    }

    if (_deepInfer != nullptr) {
        _deepInfer->release();
        delete[] _deepInfer;
    }

    FrSysClose();

    LOG_I("DeepRT::Close() End..");
    
	return 1;
}

void* FrDeepRT::DeepRT::TaskDeepSession(void* arg) {
    LRSLT lRet;
    DWORD32 dwTime;
    int iDiff;
    FrMediaStream VideoData;
    FrMediaStream EncStream;
    FrRawVideo  DecVideo;
    FrRawVideo  ConvVideo;
    FrRawVideo* pCbVideo;
    BOOL bFirstDecoded = FALSE;
    bool bUseMoviePlay = true;

    memset(&EncStream, 0, sizeof(FrMediaStream));
    memset(&VideoData, 0, sizeof(FrMediaStream));
    VideoData.tMediaType = VIDEO_MEDIA;
    EncStream.tMediaType = VIDEO_MEDIA;

    TRACE("DeepRT::TaskDeepSession() Begin..");

    while (!m_bEnd) {
        lRet = FrReaderReadFrame(m_hReader, &VideoData);
        if (FRFAILED(lRet)) {
            if (lRet == COMMON_ERR_ENDOFDATA)
                break;

            FrSleep(10);
            continue;
        }

        // output format: bgra
        lRet = FrVideoDecDecode(m_hDecVideo, &VideoData, &DecVideo);
        if (FRFAILED(lRet)) {
            continue;
        }
        for (int i = 0; i < DecVideo.nDecoded; i++) {
            
            // infer
            uint8_t* _output = nullptr;
            size_t _outputSize = 0;
            
            cv::Mat tmpImg, cvtimg;
            cv::cuda::GpuMat gpuImg = cv::cuda::GpuMat(DecVideo.dwDecodedHeight, DecVideo.dwDecodedWidth, CV_8UC4, DecVideo.pY, DecVideo.dwPitch);
            //cv::cuda::cvtColor(gpuImg, gpuImg, cv::COLOR_BGRA2RGBA);
            
            //_deepInfer->doInference(gpuImg.data, gpuImg.step, &_output, _outputSize);
            DeepBase::Detection_Data_t detectData;
            _deepInfer->doDetectInference(gpuImg.data, gpuImg.step, DecVideo.dwDecodedWidth, DecVideo.dwDecodedHeight, detectData);

            // Step5
            gpuImg.download(tmpImg);
            _deepInfer->drawDetect(detectData, tmpImg);
            
            //int const colors[6][3] = { { 1,0,1 },{ 0,0,1 },{ 0,1,1 },{ 0,1,0 },{ 1,1,0 },{ 1,0,0 } };
            //for (int i = 0; i < detectData.bboxes.size(); ++i) {
            //    DeepBase::BBox_t bbox = detectData.bboxes[i];
            //    cv::Rect rcBox(bbox.x, bbox.y, bbox.w, bbox.h);

            //    //std::cout << "idx : " << idx << ", box info : " << box.x << ", " << box.y << ", " << box.width << ", " << box.height << ", confidence : " << scores[idx]
            //    //    << ", label : " << classes[idx] << std::endl;

            //    if (bUseMoviePlay) {
            //        cv::Scalar color = obj_id_to_color(detectData.classId[i]);
            //        
            //        std::string outLabel = classesName[detectData.classId[i]] + " : " + std::to_string(detectData.prob[i]);
            //        cv::rectangle(tmpImg, rcBox, color, 2);
            //        cv::putText(tmpImg, outLabel, cv::Point(rcBox.x, rcBox.y), cv::FONT_HERSHEY_SIMPLEX, 0.50, cv::Scalar(255, 255, 51), 2);
            //    }

            //}
            
            //if (bUseMoviePlay) {
            //    cv::imshow("object detect window", tmpImg);
            //    //cv::imwrite("D:\\Download\\testResult2.jpg", frame);
            //    int key = cv::waitKey(33);    // 3 or 16ms
            //}
            gpuImg.upload(tmpImg);
            
            if (_output){
                delete _output;
            }

            // bgra -> yv12
            lRet = FrVideoConvColor(m_hConv, &DecVideo, &ConvVideo);
            if (FRFAILED(lRet)) {
                TRACE("DeepRT::TaskDeepSession() FrVideoConvColor() fail..ret(0x%x).", lRet);
                continue;
            }
            
            lRet = FrVideoEncEncode(m_hEncVideo, &ConvVideo, &EncStream);
            if (FRFAILED(lRet)) {
                continue;
            }

            lRet = FrWriterWriteFrame(m_hWriter, &EncStream);
            if (FRFAILED(lRet)) {
                continue;
            }
        }
    }

    // flushing..
    while (TRUE) {
        lRet = FrVideoDecDecode(m_hDecVideo, NULL, &DecVideo);
        if (FRFAILED(lRet)) {
            break;
        }
        for (int i = 0; i < DecVideo.nDecoded; i++) {
            
            // infer
            uint8_t* _output = nullptr;
            size_t _outputSize = 0;

            cv::Mat tmpImg, cvtimg;
            cv::cuda::GpuMat gpuImg = cv::cuda::GpuMat(DecVideo.dwDecodedHeight, DecVideo.dwDecodedWidth, CV_8UC4, DecVideo.pY, DecVideo.dwPitch);
            //cv::cuda::cvtColor(gpuImg, gpuImg, cv::COLOR_BGRA2RGBA);

            //_deepInfer->doInference(gpuImg.data, gpuImg.step, &_output, _outputSize);
            DeepBase::Detection_Data_t detectData;
            _deepInfer->doDetectInference(gpuImg.data, gpuImg.step, DecVideo.dwDecodedWidth, DecVideo.dwDecodedHeight, detectData);

            // Step5
            gpuImg.download(tmpImg);
            _deepInfer->drawDetect(detectData, tmpImg);

            //int const colors[6][3] = { { 1,0,1 },{ 0,0,1 },{ 0,1,1 },{ 0,1,0 },{ 1,1,0 },{ 1,0,0 } };
            //for (int i = 0; i < detectData.bboxes.size(); ++i) {
            //    DeepBase::BBox_t bbox = detectData.bboxes[i];
            //    cv::Rect rcBox(bbox.x, bbox.y, bbox.w, bbox.h);

            //    //std::cout << "idx : " << idx << ", box info : " << box.x << ", " << box.y << ", " << box.width << ", " << box.height << ", confidence : " << scores[idx]
            //    //    << ", label : " << classes[idx] << std::endl;

            //    if (bUseMoviePlay) {
            //        cv::Scalar color = obj_id_to_color(detectData.classId[i]);

            //        //std::string outLabel = classesName[classes[idx]] + " : " + std::to_string(scores[idx]);
            //        cv::rectangle(tmpImg, rcBox, color, 2);
            //        //cv::putText(tmpImg, outLabel, cv::Point(box.x, box.y), cv::FONT_HERSHEY_SIMPLEX, 0.50, cv::Scalar(255, 255, 51), 2);
            //    }

            //}
            //if (bUseMoviePlay) {
            //    cv::imshow("object detect window", tmpImg);
            //    //cv::imwrite("D:\\Download\\testResult2.jpg", frame);
            //    int key = cv::waitKey(33);    // 3 or 16ms
            //}
            gpuImg.upload(tmpImg);
            
            lRet = FrVideoConvColor(m_hConv, &DecVideo, &ConvVideo);
            if (FRFAILED(lRet)) {
                TRACE("DeepRT::TaskDeepSession() FrVideoConvColor() fail..ret(0x%x).", lRet);
                continue;
            }

            lRet = FrVideoEncEncode(m_hEncVideo, &ConvVideo, &EncStream);
            if (FRFAILED(lRet)) {
                continue;
            }

            lRet = FrWriterWriteFrame(m_hWriter, &EncStream);
            if (FRFAILED(lRet)) {
                continue;
            }
        }
    }

    TRACE("DeepRT::TaskDeepSession() End..");

    m_hTask = nullptr;    
    return nullptr;
}

void FrDeepRT::DeepRT::Run(FrDeepRT::Param& param) {
    Open(param);
    TaskDeepSession(this);
    Close();
}