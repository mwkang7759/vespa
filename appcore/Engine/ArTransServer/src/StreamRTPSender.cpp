#include "StreamRTPSender.h"


// RtspSession
RtspSession::RtspSession() {
	memset(&m_RtspInfo, 0, sizeof(RTSP_INFO));
}

RtspSession::RtspSession(SOCK_HANDLE hRTSPSocket, DWORD32 dwAddr, char* pSvrAddr, DWORD32 dwStream) {
	
	m_hRTSPSocket = hRTSPSocket;
	m_dwRemoteAddr = dwAddr;
	m_pSvrAddr = pSvrAddr;
	m_dwChannel = dwStream;

	memset(&m_RtspInfo, 0, sizeof(RTSP_INFO));
}

RtspSession::~RtspSession() {

}

LRSLT RtspSession::Open(const char* pURL, FrSDPHandle hSDP, const char* pSDP) {
	FrMediaHandle		hMedia;
	DWORD				i;
	LONG				lRet;

	LOG_I("[GameSender] SessionOpen() - begin..");

	m_szRemoteAddr = (char*)MALLOCZ(MAX_ADDR_LEN);
	INET_NTOA(m_szRemoteAddr, (DWORD32)m_dwRemoteAddr);

	m_RtspInfo.m_bServer = TRUE;
	m_RtspInfo.m_bUDP = TRUE;		// need to modify ...
	m_RtspInfo.m_pURL = (TCHAR*)pURL;
	m_RtspInfo.m_dwSession = (DWORD32)(RAND());
	if (m_RtspInfo.m_dwSession < 0x10000000)
		m_RtspInfo.m_dwSession = MAX_DWORD - m_RtspInfo.m_dwSession;

	lRet = FrRTSPOpen(&m_hRTSP, &m_RtspInfo);
	if (FRFAILED(lRet)) {
		LOG_E("[GameSender]	FrRTSPOpen() failed!");
		return	lRet;
	}

	lRet = FrRTSPInit(m_hRTSP, (DWORD32)m_dwChannel);
	if (FRFAILED(lRet)) {
		LOG_E("[GameSender]	FrRTSPInit() failed!");
		return	lRet;
	}

	FrRTSPSetLocalAddress(m_hRTSP, m_pSvrAddr);
	FrRTSPSetPlayRange(m_hRTSP, INFINITE);
	FrRTSPSetSDP(m_hRTSP, (char*)pSDP);
	FrRTSPSetControl(m_hRTSP, FrSDPGetSDPControl(hSDP));
	for (i = 0; i < m_dwChannel; i++) {
		//hChannel = &hSession->m_Channel[i];

		hMedia = FrSDPGetMediaHandle(hSDP, i);
		FrRTSPSetTrackControl(m_hRTSP, i, FrSDPGetMediaControl(hMedia));
		FrRTSPSetCheckSetup(m_hRTSP, i);

		ChannelCtrl* hChannel = new ChannelCtrl();
		hChannel->m_hRTSPSocket = m_hRTSPSocket;
		hChannel->m_szRemoteAddr = m_szRemoteAddr;

		m_vChCtrl.push_back(hChannel);

		LOG_I("[GameSender]	SessionOpen() ChannelCtrl push_back(%p)", hChannel);
	}

#ifdef	FILE_DUMP
	sprintf(szDumpFile, "c:\\rtsp_log_%lu.txt", hSession->m_RtspInfo.m_dwSession);
	hSession->m_hFile = FrOpenFile(szDumpFile, FILE_CREATE | FILE_WRITE);
#endif

	m_hRecvEvent = FrCreateEvent(NULL);

	LOG_I("[GameSender]	SessionOpen() end..Session(%lu) Addr(%s)", m_RtspInfo.m_dwSession, m_szRemoteAddr);

	return FR_OK;
}


void RtspSession::Close() {
	int i;
	for (i = 0; i < m_dwChannel; i++) {
		ChannelCtrl* hChannel = m_vChCtrl[i];

		LOG_I("[GameSender]	SessionClose() ChannelCtrl pop_back(%p), rtp sock(0x%x)", hChannel, hChannel->m_hRTPSocket);

		if (hChannel->m_hRTPSocket != INVALID_SOCK) {
			McSocketClose(hChannel->m_hRTPSocket);
			hChannel->m_hRTPSocket = INVALID_SOCK;
		}
		if (hChannel->m_hRTCPSocket != INVALID_SOCK) {
			McSocketClose(hChannel->m_hRTCPSocket);
			hChannel->m_hRTCPSocket = INVALID_SOCK;
		}
	}
	for (i = 0; i < m_dwChannel; i++) {
		m_vChCtrl.pop_back();
	}

	FrRTSPClose(m_hRTSP);

	/*for (i = 0; i < m_dwStream; i++) {
	FrEnterMutex(&m_Stream[i].m_xChannel);
	}*/

	McSocketClose(m_hRTSPSocket);
	m_hRTSPSocket = INVALID_SOCK;

	/*for (i = 0; i < m_dwStream; i++)
	{
	hChannel = &hSession->m_Channel[i];
	hChannel->m_hRTSPSocket = INVALID_SOCKET;

	McLeaveMutex(&m_Stream[i].m_xChannel);
	}*/

	FREE(m_szRemoteAddr);

#ifdef	FILE_DUMP
	FrCloseFile(hSession->m_hFile);
#endif

	if (m_hRecvEvent != INVALID_HANDLE)
		FrDeleteEvent(m_hRecvEvent);
}

LRSLT RtspSession::Setup(WORD* pUDPPort, std::vector<RtpTaskStream *>& rtpStream) {
	int i;
	LRSLT lRet;

	FrRTSPGetConfigInfo(m_hRTSP, &m_RtspInfo);
	i = FrRTSPGetReqIndex(m_hRTSP);
	ChannelCtrl* hChannel = m_vChCtrl[i];
	hChannel->m_bUDP = m_RtspInfo.m_bUDP;

	LOG_I("Setup() ChannelCtrl hChannel(%p)", hChannel);

	if (m_RtspInfo.m_bUDP) {
		lRet = McSocketUdpOpen(&hChannel->m_hRTPSocket, NULL, NULL, pUDPPort, m_RtspInfo.m_bMulti);
		if (FRFAILED(lRet))
			return	lRet;

		hChannel->m_wRTPLocalPort = *pUDPPort;
		hChannel->m_wRTCPLocalPort = *pUDPPort + 1;

		lRet = McSocketUdpOpen(&hChannel->m_hRTCPSocket, NULL, NULL, &hChannel->m_wRTCPLocalPort, m_RtspInfo.m_bMulti);
		if (FRFAILED(lRet))
			return	lRet;

		*pUDPPort += 2;
		if (*pUDPPort >= MAX_WORD / 2)
			*pUDPPort = RTP_UDP_PORT;

		LOG_I("SessionSetup() ChannelCtrl hChannel rtp sock(0x%x)", hChannel->m_hRTPSocket);
	}
	/*else {
		hChannel->m_wRTPLocalPort = m_wTCPPort;
		hChannel->m_wRTCPLocalPort = m_wTCPPort + 1;
		m_wTCPPort += 2;
		if (m_wTCPPort >= MAX_BYTE)
			m_wTCPPort = RTP_TCP_PORT;
	}*/

	FrRTSPSetRTPInfoSSRC(m_hRTSP, (DWORD32)i, FrRTPGetSSRC(rtpStream[i]->m_hRTP));

	FrRTSPSetRTPLocalPort(m_hRTSP, (DWORD32)i, hChannel->m_wRTPLocalPort);
	FrRTSPSetRTCPLocalPort(m_hRTSP, (DWORD32)i, hChannel->m_wRTCPLocalPort);
	hChannel->m_wRTPRemotePort = FrRTSPGetRTPRemotePort(m_hRTSP, (DWORD32)i);
	hChannel->m_wRTCPRemotePort = FrRTSPGetRTCPRemotePort(m_hRTSP, (DWORD32)i);

	LOG_I("SessionSetup() end..");

	return FR_OK;
}

LRSLT RtspSession::Play(void *pData) {
	for (int i = 0; i < m_dwChannel; i++) {
		ChannelCtrl* hChannel = m_vChCtrl[i];
		hChannel->m_bPlay = TRUE;

		LOG_I("Play() ChannelCtrl hChannel(%p)", hChannel);
	}


	if (pData) {
		FrStreamRTPSender *hSender = (FrStreamRTPSender*)(pData);
		if (hSender->m_cbStart) {
			hSender->m_cbStart(NULL, hSender->m_ctx);
			LOG_I("Play() sender callback start");
		}
	}

	return FR_OK;
}

char* RtspSession::RTSPParseRTPMessage(char* pBuffer, DWORD32* pdwBufLen) {
	DWORD					i;
	WORD					wSize;
	ChannelCtrl* hChannel = NULL;
	RtpTaskStream* hStream = NULL;

	if (*pdwBufLen < RTP_TCP_HEADER_LEN)
		return	pBuffer;

	memcpy(&wSize, &pBuffer[2], 2);
	wSize = ntohs(wSize);

	if (*pdwBufLen < (DWORD)(wSize + RTP_TCP_HEADER_LEN))
		return	pBuffer;

	for (i = 0; i < m_dwChannel; i++) {		
		hChannel = m_vChCtrl[i];
		if (hChannel->m_wRTPLocalPort == (WORD)(pBuffer[1] & 0xfe))
			break;
	}
	if (i == m_dwChannel)
		hStream = NULL;

	if (hStream) {
		if ((pBuffer[1] & 0x1))
			FrRTPParseRTCPPacket(hStream->m_hRTP, (BYTE*)(pBuffer + RTP_TCP_HEADER_LEN), wSize);
	}
	*pdwBufLen -= (wSize + RTP_TCP_HEADER_LEN);
	pBuffer += (wSize + RTP_TCP_HEADER_LEN);

	return	pBuffer;
}


// RtpTaskStream
RtpTaskStream::RtpTaskStream(FrMediaType eType) {
	m_eStreamType = eType;
	m_hCirBuffer = new CircularBuffer(sizeof(RtpPacket) * 100);
	//m_dw264BuffSize = 2 * 1024 * 1024;	// 2M
	//m_p264Buffer = (BYTE *)MALLOC(m_dw264BuffSize);
}
RtpTaskStream::~RtpTaskStream() {
	/*if (m_p264Buffer) {
		FREE(m_p264Buffer);
		m_p264Buffer = nullptr;
	}*/
	if (m_hCirBuffer) delete m_hCirBuffer;
}
LRSLT RtpTaskStream::Open() {
	return FR_OK;
}
void RtpTaskStream::Close() {

}



// GameSender
FrStreamRTPSender::FrStreamRTPSender() {
	m_hSvrTask = INVALID_TASK;
	m_hRTSPRecv = INVALID_TASK;
	m_hRTCPTask = INVALID_TASK;
}
FrStreamRTPSender::FrStreamRTPSender(void* start, void* stop, void* ctx) {
	m_hSvrTask = INVALID_TASK;
	m_hRTSPRecv = INVALID_TASK;
	m_hRTCPTask = INVALID_TASK;
	m_cbStart = (cbSessionStart)start;
	m_cbStop = (cbSessionStop)stop;
	m_ctx = ctx;
}
FrStreamRTPSender::~FrStreamRTPSender() {
	
}

LRSLT FrStreamRTPSender::Open(FrMediaInfo* hInfo, short port) {
	LRSLT lRet;
	FrMediaHandle		hMedia;
	OBJECT_TYPE			eObject;
	DWORD32				i, dwStream = 0;

	lRet = McSocketTcpOpen(&m_hSvrSocket, NULL, port, TRUE, 0);
	if (FRFAILED(lRet))	{
		LOG_E("SenderOpen() McSocketTcpOpen error (%x)", lRet);
		return	lRet;
	}

	m_dwStream = hInfo->dwAudioTotal + hInfo->dwVideoTotal;

	LOG_I("SenderOpen() begin..dwStream(%d), listen port(%d)", m_dwStream, port);

	lRet = FrSDPOpen(&m_hSDP, NULL, 0, (DWORD32)m_dwStream);
	if (FRFAILED(lRet)) {
		LOG_E("SenderOpen() FrSDPOpen error (%x)", lRet);
		return	lRet;
	}
	FrSDPSetSDPRange(m_hSDP, hInfo->dwTotalRange);
	
	/*for (i = 0; i < hInfo->m_dwAudioTotal; i++)	{
		FrAudioInfo*	hAudio = &hInfo->m_Audio[i];

		m_vStream.push_back(new RtpTaskStream(AUDIO_MEDIA));
		
		hMedia = FrSDPGetMediaHandle(m_hSDP, dwStream);		
		FrSDPSetMediaType(hMedia, MI_AUDIO);
		FrSDPSetMediaRange(hMedia, hAudio->m_dwDuration);
		FrSDPSetMediaAvgBitrate(hMedia, hAudio->m_dwBitRate);
		eObject = ConvCodecToObject(hAudio->m_dwFourCC);
		if (eObject == UNKNOWN_OBJECT)	{
			TRACE("[GameSender]	Open() - Unknown Audio Codec!!!\n");
			return	COMMON_ERR_AUDIOCODEC;
		}
		
		FrSDPSetMediaObjectType(hMedia, eObject);
		FrSDPSetMediaSampleRate(hMedia, hAudio->m_dwSampleRate);
		FrSDPSetMediaConfig(hMedia, hAudio->m_pConfig, hAudio->m_dwConfig);
		FrSDPSetMediaAudioChannelNum(hMedia, hAudio->m_dwChannelNum);
		if (!m_bAudioExist) {
			m_dwAudioIdx = dwStream;
			m_bAudioExist = TRUE;
		}
		dwStream++;
	}*/

	{
		FrVideoInfo*	hVideo = &hInfo->FrVideo;

		m_vStream.push_back(new RtpTaskStream(VIDEO_MEDIA));

		hMedia = FrSDPGetMediaHandle(m_hSDP, dwStream);
		FrSDPSetMediaType(hMedia, MI_VIDEO);
		FrSDPSetMediaRange(hMedia, hVideo->dwDuration);
		FrSDPSetMediaAvgBitrate(hMedia, hVideo->dwBitRate);
		eObject = ConvCodecToObject(hVideo->dwFourCC);
		if (eObject == UNKNOWN_OBJECT)	{
			TRACE("SenderOpen() - Unknown Video Codec!!!\n");
			return	COMMON_ERR_VIDEOCODEC;
		}
		FrSDPSetMediaObjectType(hMedia, eObject);
		FrSDPSetMediaSampleRate(hMedia, 90000);
		FrSDPSetMediaConfig(hMedia, hVideo->pConfig, hVideo->dwConfig);
		FrSDPSetMediaVideoWidth(hMedia, hVideo->dwWidth);
		FrSDPSetMediaVideoHeight(hMedia, hVideo->dwHeight);

		FrSDPSetMediaProfile(hMedia, hVideo->nProfile);
		FrSDPSetMediaLevel(hMedia, hVideo->nLevel);
		FrSDPSetMediaCompatibility(hMedia, hVideo->nCompatibility);
		LOG_I("SenderOpen() Video bit(%d), w(%d),h(%d)", hVideo->dwBitRate, hVideo->dwWidth, hVideo->dwHeight);

		if (!m_bVideoExist) {
			m_dwVideoIdx = dwStream;
			m_bVideoExist = TRUE;
		}
		dwStream++;
	}

	for (i = 0; i < m_dwStream; i++) {
		RtpTaskStream *hStream = m_vStream[i];

		LOG_I("SenderOpen() RtpTaskStream hstream(%p)", hStream);
		
		hMedia = FrSDPGetMediaHandle(m_hSDP, i);

		hStream->m_hBuf = UtilQueOpen();
		hStream->m_hInfoBuf = UtilQueOpen();
		hStream->m_hPacket = FrPacketOpen(hMedia, TRUE);
		if (!hStream->m_hPacket)
			return	COMMON_ERR_MEM;
		FrPacketInit(hStream->m_hPacket, FALSE);

		// RTP open
		lRet = FrRTPOpen(&hStream->m_hRTP, hMedia);
		if (FRFAILED(lRet))
			return	lRet;

		FrCreateMutex(&hStream->m_xChannel);

		//hStream->m_hRTPSend = FrCreateTask(NULL, (PTASK_START_ROUTINE)TaskRTPSender, hStream,
	//		RTP_TASK_PRIORITY, RTP_TASK_STACKSIZE, 0);
	}


	m_p264Buffer = (BYTE*)MALLOC(m_dw264BuffSize);

	m_hSvrTask = McCreateTask(NULL, (PTASK_START_ROUTINE)TaskListener, this,
		SERVER_TASK_PRIORITY, SERVER_TASK_STACKSIZE, 0);
	
	m_hRTSPRecv = McCreateTask(NULL, (PTASK_START_ROUTINE)TaskRTSPServer, this,
			RTSP_TASK_PRIORITY, RTSP_TASK_STACKSIZE, 0);

	LOG_I("SenderOpen() end..");

	return FR_OK;
}

void FrStreamRTPSender::Close() {
	DWORD32				i;

	TRACE("SenderClose() - begin");
	/*vector<Time *>::iterator it;
	for (it = vt.begin(); it != vt.end(); it++) {
	delete *it;
	}*/
	for (i = 0; i < m_dwStream; i++)	{
		RtpTaskStream *hStream = m_vStream[i];
		hStream->m_bEnd = TRUE;
		while (hStream->m_hRTPSend != INVALID_TASK)
			FrSleep(10);

		TRACE("SenderClose() - TaskRTPSender() task close");
	}
	m_bEnd = TRUE;
	
	/*while (hSender->m_hRTCPTask != INVALID_TASK)
		FrSleep(10);
	TRACE("[Sender]	MwSenderClose - RTCP task close\n");*/

	while (m_hRTSPRecv != INVALID_TASK)
		FrSleep(10);
	TRACE("SenderClose() - TaskRTSPRecv task close");

	while (m_hSvrTask != INVALID_TASK)
		FrSleep(10);
	TRACE("SenderClose() - TaskListener task close");

	//for (i = 0; i < MAX_SESSION; i++)
	//	SenderSessionClose(hSender, i);
	//SessionClose();
	for (i = 0; i < m_vSession.size(); i++) {
		RtspSession *hSession = m_vSession[i];
		SessionClose(hSession);
	}

	for (i = 0; i < m_dwStream; i++)	{
		RtpTaskStream *hStream = m_vStream[i];

		UtilQueClose(hStream->m_hBuf);
		UtilQueClose(hStream->m_hInfoBuf);
		FrPacketClose(hStream->m_hPacket);
		FrRTPClose(hStream->m_hRTP);
		FrDeleteMutex(&hStream->m_xChannel);

		delete hStream;
	}

	FREE(m_p264Buffer);

	FrSDPClose(m_hSDP);
	McSocketClose(m_hSvrSocket);

	FREE(m_szSvrAddr);
	FREE(m_szURL);

	TRACE("SenderClose() - end");
}

LONG FrStreamRTPSender::SendFrame(FrMediaStream* pMedia, void *extra) {
	RtpTaskStream*		hStream = NULL;
	BYTE*				pFrame;
	BYTE*				pPacket;
	DWORD32				i, dwFrameCTS, dwPacket, dwRtpCTS;
	BOOL				bMBit;
	PacketInfo*			pInfo = NULL;
	LONG				lRet = FR_OK;
	QWORD				qwTemp;
	BOOL				bAudioAlign = FALSE;
	int                 nQueueCnt = 0;
	int                 size = 0;
	
	
	if (pMedia->tMediaType == AUDIO_MEDIA && m_bAudioExist) {
		hStream = m_vStream[m_dwAudioIdx];
	}
	else if (pMedia->tMediaType == VIDEO_MEDIA && m_bVideoExist) {
		hStream = m_vStream[m_dwVideoIdx];

		// need to linking..
		size = make_h264stream_rtp_format_from_ts_format(m_p264Buffer, m_dw264BuffSize, pMedia->pFrame, pMedia->dwFrameLen[0]);
		pMedia->pFrame = m_p264Buffer;
		pMedia->dwFrameLen[0] = size;
	}
		
	
	if (!hStream)
		return	FR_OK;

	pFrame = pMedia->pFrame;
	dwFrameCTS = pMedia->dwCTS;

	for (i = 0; i < pMedia->dwFrameNum; i++)	{
		LOG_D("SendFrame() - PacketPutData num(%d), len(%d)", pMedia->dwFrameNum, pMedia->dwFrameLen[0]);

		//static FILE_HANDLE gPktFile = NULL;
		//if (!gPktFile) {
		//	gPktFile = (FILE_HANDLE)FILEDUMPOPEN("d:\\video_rtp", "pkt", 0);
		//}
		//if (gPktFile) {
		//	
		//	FPRINTF(gPktFile, "[PACKET] Size=%d\n", pMedia->m_dwFrameLen[i]);
		//	FPRINTF(gPktFile, "	[Payload]\n");
		//	FILEDUMP(gPktFile, pFrame, pMedia->m_dwFrameLen[i]);
		//}

		FrPacketPutData(hStream->m_hPacket, pFrame, pMedia->dwFrameLen[i], (DWORD32)dwFrameCTS);
		while (1) {
			pPacket = FrPacketGetData(hStream->m_hPacket, &dwPacket, &dwRtpCTS, &bMBit);
			if (!pPacket)
				break;

			FrRTPMakeRTPPacket(hStream->m_hRTP, pPacket + RTP_TCP_HEADER_LEN, (DWORD32)dwRtpCTS, bMBit);

			pInfo = (PacketInfo*)MALLOC(sizeof(PacketInfo));
			
			pInfo->m_dwCTS = dwRtpCTS;
			pInfo->m_dwTS = FrRTPGetSendTS(hStream->m_hRTP);
			pInfo->m_dwSeq = FrRTPGetSendSeq(hStream->m_hRTP);
			UtilQuePutData(hStream->m_hBuf, pPacket, (DWORD32)dwPacket);
			UtilQuePutData(hStream->m_hInfoBuf, (void*)pInfo, sizeof(PacketInfo));
		}

		pFrame += pMedia->dwFrameLen[i];
		//dwFrameCTS += pMedia->dwDeltaCTS;
	}

	//if (pInfo != NULL)
	//TRACEX(DTB_LOG_DEBUG, "[GameSender]	PutVideoData - CTS(%d), TS(%d), Seq(%d) MBit(%d)", pInfo->m_dwCTS, pInfo->m_dwTS, pInfo->m_dwSeq, bMBit);


	funcRTPSender(hStream);


	if (FRFAILED(m_lError))	{
		lRet = m_lError;
		m_lError = FR_OK;
	}
	
	//TRACEX(DTB_LOG_TRACE, "[GameSender]	SendFrame() end..");
	//if (pMedia->m_tMediaType == AUDIO_MEDIA)
	//	TRACEX(DTB_LOG_TRACE, "[GameSender][a] SendFrame - CTS(%d)", pMedia->m_dwCTS);
	//if (pMedia->m_tMediaType == VIDEO_MEDIA) {

	//	TRACEX(DTB_LOG_DEBUG, "[GameSender][v] McSocketSendTo wSeq=%d CTS=%d dwTS=%d, MBit(%d), diff(%d)", 
	//		dwSeq, dwCTS, dwTS, bMbit, McGetTickCount() - hStream->m_dwVideoTick);
	//	hStream->m_dwVideoTick = FrGetTickCount();
	//}
	
	return	lRet;
}

LRSLT FrStreamRTPSender::SessionOpen() {
	FrMediaHandle	hMedia;
	DWORD			i;
	LONG			lRet;
	SOCK_HANDLE		hRTSPSocket;
	DWORD32			dwRemoteAddr;
	WORD			wRTSPRemotePort;
	
	LOG_I("SessionOpen() - begin..");

	lRet = McSocketAccept(&hRTSPSocket, m_hSvrSocket, &dwRemoteAddr, &wRTSPRemotePort);
	if (FRFAILED(lRet)) {
		LOG_E("SessionOpen() - Connection is not accepted\n");
		return	lRet;
	}
	
	if (!m_szSvrAddr) {
		m_szSvrAddr = (char*)MALLOCZ(MAX_ADDR_LEN);
		INET_NTOA(m_szSvrAddr, McSocketGetSockAddr(hRTSPSocket));

		m_szURL = (char*)MALLOCZ(MAX_STRING_LENGTH);

		FrSDPSetLocalAddr(m_hSDP, m_szSvrAddr);
		lRet = FrSDPGetSDP(m_hSDP, m_pSDP, MAX_SDP_LEN);
		if (FRFAILED(lRet)) {
			McSocketClose(hRTSPSocket);
			return	lRet;
		}
	}

	//////////////////////////////////////////////////////////////////////////////
	// on test..
	if (!m_vSession.empty()) {
		McSocketClose(hRTSPSocket);
		return FR_FAIL;
	}
	//////////////////////////////////////////////////////////////////////////////


	// create new session..
	RtspSession* hSession = new RtspSession(hRTSPSocket, dwRemoteAddr, m_szSvrAddr, m_dwStream);
	lRet = hSession->Open(m_szURL, m_hSDP, m_pSDP);
	if (FRFAILED(lRet)) {
		return	lRet;
	}

	// need to mutex ???
	for (i = 0; i < m_dwStream; i++) {
		m_vStream[i]->m_Channel.push_back(hSession->m_vChCtrl[i]);
		//m_Stream[i]->m_pvChannel = &(hSession->m_Channel[i]);
	}

	m_vSession.push_back(hSession);

	//if (m_cbStart)
	//	m_cbStart(NULL, m_ctx);
	
	LOG_I("[GameSender]	SessionOpen() end..hSession(%p), cnt(%d)", hSession, m_vSession.size());
	
	return FR_OK;
}

void FrStreamRTPSender::SessionClose(RtspSession *hSession) {
	int i;
	
	LOG_I("[GameSender] SessionClose() begin.. session cnt(%d), err(%x)", m_vSession.size(), m_lError);
	if (m_cbStop)
		m_cbStop(NULL, m_ctx);

	if (m_vSession.empty())
		return;

	for (i = 0; i < m_dwStream; i++) {
		//FrEnterMutex(&hSender->m_Stream[i].m_xChannel);
		for (int j = 0; j < m_vStream[i]->m_Channel.size(); j++) {
			ChannelCtrl* hChannel = m_vStream[i]->m_Channel[j];
			if (hChannel == hSession->m_vChCtrl[i]) {
				LOG_I("[GameSender] SessionClose() - m_dwStream(%d), hChannel(%p), (%d-%d) ", 
					m_dwStream, hChannel, i, j);
				
				m_vStream[i]->m_Channel.erase(m_vStream[i]->m_Channel.begin() + j);
				break;
			}
		}
		//m_Stream[i]->m_Channel.pop_back();
		//FrLeaveMutex(&hSender->m_Stream[i].m_xChannel);
	}

	if (hSession) {
		hSession->Close();
		delete hSession;
		for (i = 0; i < m_vSession.size(); i++) {
			if (hSession == m_vSession[i]) {
				LOG_I("[GameSender] SessionClose() - m_dwSession(%d), pSession(%p), (%d)",
					m_vSession.size(), hSession, i);
				m_vSession.erase(m_vSession.begin() + i);
				break;
			}
		}
	}
}


///////////////////////////////////////////////////////////////////////////////
// util 
CircularBuffer::CircularBuffer(size_t capacity)
	: beg_index_(0)
	, end_index_(0)
	, size_(0)
	, capacity_(capacity)
{
	data_ = new char[capacity];
}

CircularBuffer::~CircularBuffer()
{
	delete[] data_;
}

size_t CircularBuffer::write(const char *data, size_t bytes)
{
	if (bytes == 0) return 0;

	size_t capacity = capacity_;
	size_t bytes_to_write = std::min(bytes, capacity - size_);

	// Write in a single step
	if (bytes_to_write <= capacity - end_index_)
	{
		memcpy(data_ + end_index_, data, bytes_to_write);
		end_index_ += bytes_to_write;
		if (end_index_ == capacity) end_index_ = 0;
	}
	// Write in two steps
	else
	{
		size_t size_1 = capacity - end_index_;
		memcpy(data_ + end_index_, data, size_1);
		size_t size_2 = bytes_to_write - size_1;
		memcpy(data_, data + size_1, size_2);
		end_index_ = size_2;
	}

	size_ += bytes_to_write;
	return bytes_to_write;
}

size_t CircularBuffer::read(char *data, size_t bytes)
{
	if (bytes == 0) return 0;

	size_t capacity = capacity_;
	size_t bytes_to_read = std::min(bytes, size_);

	// Read in a single step
	if (bytes_to_read <= capacity - beg_index_)
	{
		memcpy(data, data_ + beg_index_, bytes_to_read);
		beg_index_ += bytes_to_read;
		if (beg_index_ == capacity) beg_index_ = 0;
	}
	// Read in two steps
	else
	{
		size_t size_1 = capacity - beg_index_;
		memcpy(data, data_ + beg_index_, size_1);
		size_t size_2 = bytes_to_read - size_1;
		memcpy(data + size_1, data_, size_2);
		beg_index_ = size_2;
	}

	size_ -= bytes_to_read;
	return bytes_to_read;
}



#if 0
LRSLT GameSender::SessionOpen() {
	FrMediaHandle		hMedia;
	DWORD				i;
	LONG				lRet;

	TRACEX(DTB_LOG_INFO, "[GameSender] SessionOpen() - begin..");

#ifndef RTSP_SESESSION_MODEL

	RtspSession* hSession = new RtspSession();

	lRet = McSocketAccept(&hSession->m_hRTSPSocket, m_hSvrSocket, &hSession->m_dwRemoteAddr, &hSession->m_wRTSPRemotePort);
	if (FRFAILED(lRet)) {
		TRACEX(DTB_LOG_INFO, "[GameSender] SessionOpen() - Connection is not accepted\n");
		//FREE(hSession->m_szRemoteAddr);
		return	lRet;
	}

	// on test..
	//hSession->Open();

	hSession->m_szRemoteAddr = (char*)MALLOCZ(MAX_ADDR_LEN);
	INET_NTOA(hSession->m_szRemoteAddr, (DWORD32)hSession->m_dwRemoteAddr);

	if (!m_szSvrAddr) {
		m_szSvrAddr = (char*)MALLOCZ(MAX_ADDR_LEN);
		INET_NTOA(m_szSvrAddr, McSocketGetSockAddr(hSession->m_hRTSPSocket));

		m_szURL = (char*)MALLOCZ(MAX_STRING_LENGTH);

		FrSDPSetLocalAddr(m_hSDP, m_szSvrAddr);
		lRet = FrSDPGetSDP(m_hSDP, m_pSDP, MAX_SDP_LEN);
		if (FRFAILED(lRet)) {
			//FREE(hSession->m_szRemoteAddr);
			//FREE(hSession);
			return	lRet;
		}
	}

	hSession->m_RtspInfo.m_bServer = TRUE;
	hSession->m_RtspInfo.m_bUDP = TRUE;		// need to modify ...
	hSession->m_RtspInfo.m_pURL = m_szURL;
	hSession->m_RtspInfo.m_dwSession = (DWORD32)(RAND());
	if (hSession->m_RtspInfo.m_dwSession < 0x10000000)
		hSession->m_RtspInfo.m_dwSession = MAX_DWORD - hSession->m_RtspInfo.m_dwSession;

	hSession->m_dwChannel = m_dwStream;

	lRet = FrRTSPOpen(&hSession->m_hRTSP, &hSession->m_RtspInfo);
	if (FRFAILED(lRet)) {
		//McSocketClose(hSession->m_hRTSPSocket);
		//hSession->m_hRTSPSocket = INVALID_SOCKET;
		//FREE(hSession->m_szRemoteAddr);
		//FREE(hSession);
		TRACEX(DTB_LOG_ERROR, "[GameSender]	FrRTSPOpen() failed!\n");
		return	lRet;
	}

	lRet = FrRTSPInit(hSession->m_hRTSP, (DWORD32)hSession->m_dwChannel);
	if (FRFAILED(lRet)) {
		//FrRTSPClose(hSession->m_hRTSP);
		//McSocketClose(hSession->m_hRTSPSocket);
		//hSession->m_hRTSPSocket = INVALID_SOCKET;

		//FREE(hSession->m_szRemoteAddr);
		//FREE(hSession);
		TRACEX(DTB_LOG_ERROR, "[GameSender]	FrRTSPInit() failed!\n");
		return	lRet;
	}

	FrRTSPSetLocalAddress(hSession->m_hRTSP, m_szSvrAddr);
	//FrRTSPSetMultiAddress(hSession->m_hRTSP, m_szMultiAddr);
	FrRTSPSetPlayRange(hSession->m_hRTSP, INFINITE);
	FrRTSPSetSDP(hSession->m_hRTSP, m_pSDP);
	FrRTSPSetControl(hSession->m_hRTSP, FrSDPGetSDPControl(m_hSDP));
	for (i = 0; i < hSession->m_dwChannel; i++) {
		//hChannel = &hSession->m_Channel[i];

		hMedia = FrSDPGetMediaHandle(m_hSDP, i);
		FrRTSPSetTrackControl(hSession->m_hRTSP, i, FrSDPGetMediaControl(hMedia));
		FrRTSPSetCheckSetup(hSession->m_hRTSP, i);

		ChannelCtrl* hChannel = new ChannelCtrl();
		hChannel->m_hRTSPSocket = hSession->m_hRTSPSocket;
		hChannel->m_szRemoteAddr = hSession->m_szRemoteAddr;

		hSession->m_vChCtrl.push_back(hChannel);

		TRACEX(DTB_LOG_INFO, "[GameSender]	SessionOpen() ChannelCtrl push_back(%p)", hChannel);

		//hChannel->m_hRTPSocket = INVALID_SOCKET;
		//hChannel->m_hRTCPSocket = INVALID_SOCKET;

		//hChannel->m_hRTSPSocket = hSession->m_hRTSPSocket;
		//hChannel->m_szRemoteAddr = hSession->m_szRemoteAddr;
	}

#ifdef	FILE_DUMP
	sprintf(szDumpFile, "c:\\rtsp_log_%lu.txt", hSession->m_RtspInfo.m_dwSession);
	hSession->m_hFile = FrOpenFile(szDumpFile, FILE_CREATE | FILE_WRITE);
#endif

	//for (i = 0; i < m_dwStream; i++) {
	//	FrEnterMutex(&hSender->m_Stream[i].m_xChannel);
	//	hSender->m_Stream[i].m_hChannel[dwSession] = &hSession->m_Channel[i];
	//	FrLeaveMutex(&hSender->m_Stream[i].m_xChannel);
	//}
	for (i = 0; i < m_dwStream; i++) {
		m_vStream[i]->m_Channel.push_back(hSession->m_vChCtrl[i]);
		//m_Stream[i]->m_Channel[m_dwSession] = hSession->m_Channel[i];
	}

	hSession->m_hRecvEvent = FrCreateEvent(NULL);

	//m_Session[dwSession] = hSession;
	//m_dwSession++;

	TRACEX(DTB_LOG_INFO, "[GameSender]	SessionOpen() end..Session(%lu) Addr(%s)", hSession->m_RtspInfo.m_dwSession, hSession->m_szRemoteAddr);

	m_vSession.push_back(hSession);
	//m_dwSession++;

	return MC_OK;
#else
	SOCK_HANDLE hRTSPSocket;
	DWORD32				dwRemoteAddr;
	WORD				wRTSPRemotePort;

	lRet = McSocketAccept(&hRTSPSocket, m_hSvrSocket, &dwRemoteAddr, &wRTSPRemotePort);
	if (FRFAILED(lRet)) {
		TRACEX(DTB_LOG_INFO, "[GameSender] SessionOpen() - Connection is not accepted\n");
		return	lRet;
	}

	if (!m_szSvrAddr) {
		m_szSvrAddr = (char*)MALLOCZ(MAX_ADDR_LEN);
		INET_NTOA(m_szSvrAddr, McSocketGetSockAddr(hRTSPSocket));

		m_szURL = (char*)MALLOCZ(MAX_STRING_LENGTH);

		FrSDPSetLocalAddr(m_hSDP, m_szSvrAddr);
		lRet = FrSDPGetSDP(m_hSDP, m_pSDP, MAX_SDP_LEN);
		if (FRFAILED(lRet)) {
			McSocketClose(hRTSPSocket);
			return	lRet;
		}
	}

	RtspSession* hSession = new RtspSession(hRTSPSocket, dwRemoteAddr, m_szSvrAddr, m_dwStream);
	lRet = hSession->Open(m_szURL, m_hSDP, m_pSDP);
	if (FRFAILED(lRet)) {
		return	lRet;
	}

	for (i = 0; i < m_dwStream; i++) {
		m_vStream[i]->m_Channel.push_back(hSession->m_vChCtrl[i]);
		//m_Stream[i]->m_pvChannel = &(hSession->m_Channel[i]);
	}

	m_vSession.push_back(hSession);

	TRACEX(DTB_LOG_INFO, "[GameSender]	SessionOpen() end..hSession(%p), cnt(%d)", hSession, m_vSession.size());

	return MC_OK;
#endif
}

void GameSender::SessionClose(RtspSession *pSession) {
	int i;
	TRACEX(DTB_LOG_INFO, "[GameSender] SessionClose() begin.. session cnt(%d), err(%x)", m_vSession.size(), m_lError);

#ifndef RTSP_SESESSION_MODEL
	if (m_vSession.empty())
		return;

	RtspSession	*hSession = m_vSession.front();

	for (i = 0; i < m_dwStream; i++) {
		//McEnterMutex(&hSender->m_Stream[i].m_xChannel);
		m_vStream[i]->m_Channel.pop_back();
		//FrLeaveMutex(&hSender->m_Stream[i].m_xChannel);
	}

	for (i = 0; i < hSession->m_dwChannel; i++) {
		ChannelCtrl* hChannel = hSession->m_vChCtrl[i];

		TRACEX(DTB_LOG_INFO, "[GameSender]	SessionClose() ChannelCtrl pop_back(%p), rtp sock(0x%x)", hChannel, hChannel->m_hRTPSocket);

		if (hChannel->m_hRTPSocket != INVALID_SOCK) {
			McSocketClose(hChannel->m_hRTPSocket);
			hChannel->m_hRTPSocket = INVALID_SOCK;
		}
		if (hChannel->m_hRTCPSocket != INVALID_SOCK) {
			McSocketClose(hChannel->m_hRTCPSocket);
			hChannel->m_hRTCPSocket = INVALID_SOCK;
		}

		//hSession->m_Channel.pop_back();
		//hSession->m_Channel.erase(hSession->m_Channel.begin() + i);
	}
	for (i = 0; i < hSession->m_dwChannel; i++) {
		hSession->m_vChCtrl.pop_back();
	}

	FrRTSPClose(hSession->m_hRTSP);

	/*for (i = 0; i < m_dwStream; i++) {
	FrEnterMutex(&m_Stream[i].m_xChannel);
	}*/

	McSocketClose(hSession->m_hRTSPSocket);
	hSession->m_hRTSPSocket = INVALID_SOCK;

	/*for (i = 0; i < m_dwStream; i++)
	{
	hChannel = &hSession->m_Channel[i];
	hChannel->m_hRTSPSocket = INVALID_SOCKET;

	FrLeaveMutex(&m_Stream[i].m_xChannel);
	}*/

	FREE(hSession->m_szRemoteAddr);

#ifdef	FILE_DUMP
	FrCloseFile(hSession->m_hFile);
#endif

	if (hSession->m_hRecvEvent != INVALID_HANDLE)
		FrDeleteEvent(hSession->m_hRecvEvent);

	TRACEX(DTB_LOG_INFO, "[GameSender] SessionClose() - end..Session(%lu)", hSession->m_RtspInfo.m_dwSession);

	delete hSession;
	m_vSession.pop_back();

	/*hSender->m_Session[dwSession] = NULL;
	hSender->m_dwSession--;*/
#else
	if (m_vSession.empty())
		return;

	for (i = 0; i < m_dwStream; i++) {
		//FrEnterMutex(&hSender->m_Stream[i].m_xChannel);
		for (int j = 0; j < m_vStream[i]->m_Channel.size(); j++) {
			ChannelCtrl* hChannel = m_vStream[i]->m_Channel[j];
			if (hChannel == pSession->m_vChCtrl[i]) {
				TRACEX(DTB_LOG_INFO, "[GameSender] SessionClose() - m_dwStream(%d), hChannel(%p), (%d-%d) ",
					m_dwStream, hChannel, i, j);
				m_vStream[i]->m_Channel.erase(m_vStream[i]->m_Channel.begin() + j);
				break;
			}
		}

		//m_Stream[i]->m_Channel.pop_back();
		//FrLeaveMutex(&hSender->m_Stream[i].m_xChannel);
	}

	pSession->Close();
	delete pSession;
	for (i = 0; i < m_vSession.size(); i++) {
		if (pSession == m_vSession[i]) {
			TRACEX(DTB_LOG_INFO, "[GameSender] SessionClose() - m_dwSession(%d), pSession(%p), (%d)",
				m_vSession.size(), pSession, i);
			m_vSession.erase(m_vSession.begin() + i);
			break;
		}
	}
	//m_Session.pop_back();
#endif
}
LONG GameSender::SessionPlay() {
	TRACEX(DTB_LOG_INFO, "[GameSender] SessionPlay() begin..cnt(%d)", m_vSession.size());

#ifndef RTSP_SESESSION_MODEL
	if (m_vSession.empty())
		return MC_FAIL;

	RtspSession	*&hSession = m_vSession.front();

	for (int i = 0; i < hSession->m_dwChannel; i++) {
		ChannelCtrl* hChannel = hSession->m_vChCtrl[i];
		hChannel->m_bPlay = TRUE;

		TRACEX(DTB_LOG_INFO, "[GameSender]	SessionPlay() ChannelCtrl hChannel(%p)", hChannel);
	}

	TRACEX(DTB_LOG_INFO, "[GameSender] SessionPlay() end.. ");

	return MC_OK;
#else
	LRSLT lRet;
	RtspSession	*&hSession = m_vSession.front();
	lRet = hSession->Play();
	return lRet;
#endif
}
LONG GameSender::SessionSetup() {
#ifndef RTSP_SESESSION_MODEL
	DWORD				i;
	LONG				lRet;

	TRACEX(DTB_LOG_INFO, "[GameSender] SessionSetup() begin..cnt(%d)", m_vSession.size());

	if (m_vSession.empty())
		return MC_FAIL;

	RtspSession	*&hSession = m_vSession.front();
	//RtspSession	*hSession2 = m_Session.front();

	FrRTSPGetConfigInfo(hSession->m_hRTSP, &hSession->m_RtspInfo);
	i = FrRTSPGetReqIndex(hSession->m_hRTSP);
	ChannelCtrl* hChannel = hSession->m_vChCtrl[i];
	hChannel->m_bUDP = hSession->m_RtspInfo.m_bUDP;

	TRACEX(DTB_LOG_INFO, "[GameSender]	SessionSetup() ChannelCtrl hChannel(%p)", hChannel);

	if (hSession->m_RtspInfo.m_bUDP) {
		lRet = McSocketUdpOpen(&hChannel->m_hRTPSocket, NULL, NULL, &m_wUDPPort, hSession->m_RtspInfo.m_bMulti);
		if (FRFAILED(lRet))
			return	lRet;

		hChannel->m_wRTPLocalPort = m_wUDPPort;
		hChannel->m_wRTCPLocalPort = m_wUDPPort + 1;

		lRet = McSocketUdpOpen(&hChannel->m_hRTCPSocket, NULL, NULL, &hChannel->m_wRTCPLocalPort, hSession->m_RtspInfo.m_bMulti);
		if (FRFAILED(lRet))
			return	lRet;

		m_wUDPPort += 2;
		if (m_wUDPPort >= MAX_WORD / 2)
			m_wUDPPort = RTP_UDP_PORT;

		TRACEX(DTB_LOG_INFO, "[GameSender]	SessionSetup() ChannelCtrl hChannel rtp sock(0x%x)", hChannel->m_hRTPSocket);

	}
	else {
		hChannel->m_wRTPLocalPort = m_wTCPPort;
		hChannel->m_wRTCPLocalPort = m_wTCPPort + 1;
		m_wTCPPort += 2;
		if (m_wTCPPort >= MAX_BYTE)
			m_wTCPPort = RTP_TCP_PORT;
	}

	FrRTSPSetRTPInfoSSRC(hSession->m_hRTSP, (DWORD32)i, FrRTPGetSSRC(m_vStream[i]->m_hRTP));

	FrRTSPSetRTPLocalPort(hSession->m_hRTSP, (DWORD32)i, hChannel->m_wRTPLocalPort);
	FrRTSPSetRTCPLocalPort(hSession->m_hRTSP, (DWORD32)i, hChannel->m_wRTCPLocalPort);
	hChannel->m_wRTPRemotePort = FrRTSPGetRTPRemotePort(hSession->m_hRTSP, (DWORD32)i);
	hChannel->m_wRTCPRemotePort = FrRTSPGetRTCPRemotePort(hSession->m_hRTSP, (DWORD32)i);

	TRACEX(DTB_LOG_INFO, "[GameSender] SessionSetup() end..");

	return	MC_OK;
#else
	LRSLT lRet;
	RtspSession	*&hSession = m_vSession.front();
	lRet = hSession->Setup(&m_wUDPPort, m_vStream);
	return lRet;
#endif
}
#endif