#include "StreamRTPSender.h"
//#include "FECPacket.h"




void TaskListener(void* lpVoid) {
	FrStreamRTPSender*		hSender = (FrStreamRTPSender*)lpVoid;
	FD_SET_TYPE			rSet;
	LONG				lRet;

	LOG_I("[GameSender]	TaskListener - Start");

	while (!hSender->m_bEnd) {
		FD_ZERO(&rSet);
		FD_SET(hSender->m_hSvrSocket->m_hSocket, &rSet);

		lRet = McSocketSelect(&rSet, NULL, NULL);
		if (FRFAILED(lRet)) {
			continue;
		}

		if (FD_ISSET(hSender->m_hSvrSocket->m_hSocket, &rSet)) {
			lRet = hSender->SessionOpen();
			if (FRFAILED(lRet)) {
				hSender->m_lError = lRet;
				LOG_E("[GameSender]	TaskListener - SessionOpen() failed!");
				continue;
			}
		}
		//McSleep(10);
	}

	McCloseTask(hSender->m_hSvrTask);
	hSender->m_hSvrTask = INVALID_TASK;
	LOG_I("[GameSender]	TaskListener - End");
	McExitTask();
}

void funcRTPSender(void* lpVoid) {
	RtpTaskStream*	hStream = (RtpTaskStream *)lpVoid;

	BYTE*				pPacket;
	DWORD32				dwPacket = 0;
	PacketInfo*			pInfo = NULL;
	LONG				lRet = FR_OK;

	DWORD32             dwSeq = 0;
	DWORD32             dwCTS = 0;
	DWORD32             dwTS = 0;

	DWORD32             dwSendCurTime = 0;
	DWORD32             dwPrevCTS = 0;
	ChannelCtrl*		hChannel = NULL;
	int i;

	//TRACEX(DTB_LOG_INFO, "[GameSender (%d)]	begin", hStream->m_eStreamType);

	//dwWaitBaseTime = McGetTickCount();

	while (1) {
		pPacket = (BYTE*)UtilQueGetData(hStream->m_hBuf, &dwPacket);
		if (!pPacket) {
			break;
		}
		pInfo = (PacketInfo*)UtilQueGetData(hStream->m_hInfoBuf, NULL);
		if (pInfo) {
			dwSeq = pInfo->m_dwSeq;
			dwCTS = pInfo->m_dwCTS;
			dwTS = pInfo->m_dwTS;
		} else {
			dwSeq = 0;
			dwCTS = 0;
			dwTS = 0;

			LOG_W("[funcRTPSender (%d)]    Warning, PacketInfo is NULL", hStream->m_eStreamType);
		}

		//if (hStream->m_Channel.empty()) {
		//	FREE(pPacket);
		//	FREE(pInfo);
		//	pInfo = NULL;
		//	continue;
		//}

		for (i = 0; i < hStream->m_Channel.size(); i++) {
			hChannel = hStream->m_Channel[i];			
			if (!hChannel->m_bPlay) {
				LOG_W("[funcRTPSender (%d)]	Channel is not play mode", hStream->m_eStreamType);
				continue;
			}

			if (hChannel->m_bUDP) {
				if (hStream->m_eStreamType == VIDEO_MEDIA) {
					lRet = McSocketSendTo(hChannel->m_hRTPSocket, (char*)(pPacket + RTP_TCP_HEADER_LEN), (DWORD32)(dwPacket - RTP_TCP_HEADER_LEN),
							hChannel->m_szRemoteAddr, hChannel->m_wRTPRemotePort);
					
					LOG_T("[funcRTPSender][v] McSocketSendTo dwPacket=%d addr=%s:%d", dwPacket, hChannel->m_szRemoteAddr, hChannel->m_wRTPRemotePort);
				}
				else {
					;
				}
				
				//if (hStream->m_eStreamType == VIDEO_MEDIA) {
				//	RTP_HEADER*		pRtpHdr = (RTP_HEADER*)(pPacket + RTP_TCP_HEADER_LEN);
				//	char*		pos = (char*)(pPacket + RTP_TCP_HEADER_LEN);
				//	int bMbit = (pos[1] & 0x80) >> 7;
				//	
				//	// RTP Header parsing
				//	DWORD32 wSeq = ntohs(pRtpHdr->wSeq);
				//	DWORD32 dwTS = ntohl(pRtpHdr->dwTimestamp);
				//	DWORD32 dwSSRC = ntohl(pRtpHdr->dwSSRC);
				//	
				//	if (bMbit) {
				//		//TRACEX(DTB_LOG_DEBUG, "[GameSender][v] McSocketSendTo wSeq=%d CTS=%d dwTS=%d, MBit(%d), diff(%d)", 
				//		//	dwSeq, dwCTS, dwTS, bMbit, McGetTickCount() - hStream->m_dwVideoTick);
				//		//hStream->m_dwVideoTick = McGetTickCount();
				//	}
				//	//TRACEX(DTB_LOG_DEBUG, "[GameSender][v] McSocketSendTo Seq=%d CTS=%d TS=%d", dwSeq, dwCTS, dwTS);
				//}

				//dwSendCurTime = McGetTickCount();
			}
			else {
				// TCP
				WORD	wSize;
				pPacket[0] = '$';
				pPacket[1] = (BYTE)(hChannel->m_wRTPRemotePort);
				wSize = htons((WORD)(dwPacket - RTP_TCP_HEADER_LEN));
				memcpy(&pPacket[2], &wSize, 2);

				lRet = McSocketSend(hChannel->m_hRTSPSocket, (char*)pPacket, (DWORD32)dwPacket);
				if (FRFAILED(lRet)) {
					LOG_E("funcRTPSender: McSocketSend Error lRet(0x%x)", lRet);
				}
				//LOG_T("[funcRTPSender][v] McSocketSend dwPacket=%d", dwPacket);
			}
		} // end for ()

		FREE(pPacket);
		FREE(pInfo);
		pInfo = NULL;
	}
	//TRACEX(DTB_LOG_DEBUG, "[GameSender][v] funcRTPSender end.. ");
}

void TaskRTPSender(void* lpVoid) {
	RtpTaskStream*	hStream = (RtpTaskStream *)lpVoid;

	BYTE*				pPacket;
	DWORD32				dwPacket = 0;
	PacketInfo*			pInfo = NULL;
	LONG				lRet = FR_OK;

	DWORD32             dwSeq = 0;
	DWORD32             dwCTS = 0;
	DWORD32             dwTS = 0;

	//DWORD32             dwWaitTime = 0;
	//DWORD32             dwWaitBaseTime = 0;
	//DWORD32             dwWaitCurTime = 0;

	//DWORD32             dwSendTime = 0;
	//DWORD32             dwSendBaseTime = 0;
	DWORD32             dwSendCurTime = 0;
	DWORD32             dwPrevCTS = 0;
	ChannelCtrl*		hChannel = NULL;
	int i;

	LOG_I("[TaskRTPSender (%d)]	begin", hStream->m_eStreamType);

	//dwWaitBaseTime = McGetTickCount();

	while (!hStream->m_bEnd) {
		pPacket = (BYTE*)UtilQueGetData(hStream->m_hBuf, &dwPacket);
		if (!pPacket) {
			//TRACEX(DTB_LOG_TRACE, "[GameSender (%d)]	Packet is NULL", hStream->m_eStreamType);
			FrSleep(1);
			continue;
		}
		pInfo = (PacketInfo*)UtilQueGetData(hStream->m_hInfoBuf, NULL);
		if (pInfo) {
			dwSeq = pInfo->m_dwSeq;
			dwCTS = pInfo->m_dwCTS;
			dwTS = pInfo->m_dwTS;
		} else {
			dwSeq = 0;
			dwCTS = 0;
			dwTS = 0;

			LOG_W("[GameSender (%d)]    Warning, PacketInfo is NULL", hStream->m_eStreamType);
		}

		/*if (!hStream->m_qPacket.empty()) {
			RtpPacket *&pSample = hStream->m_qPacket.front();
			hStream->m_qPacket.pop();

			pPacket = (BYTE*)pSample->pPacket;
			dwPacket = pSample->dwSize;
			dwSeq = pSample->dwSeq;
			dwCTS = pSample->dwCTS;
			dwTS = pSample->dwTS;

			FREE(pPacket);
		}*/

		/*if (hStream->m_hCirBuffer->size()) {
			RtpPacket *pSample = nullptr;
			hStream->m_hCirBuffer->read((char *)pSample, sizeof(RtpPacket));
			
			pPacket = (BYTE*)pSample->pPacket;
			dwPacket = pSample->dwSize;
			dwSeq = pSample->dwSeq;
			dwCTS = pSample->dwCTS;
			dwTS = pSample->dwTS;

			FREE(pPacket);
		}*/

		/*if (hStream->m_eStreamType == AUDIO_MEDIA)
			TRACEX(DTB_LOG_TRACE, "[GameSender (%d)]	GetAudioData Seq=%d CTS=%d TS=%d", hStream->m_eStreamType, dwSeq, dwCTS, dwTS);
		else if (hStream->m_eStreamType == VIDEO_MEDIA)
			TRACEX(DTB_LOG_TRACE, "[GameSender (%d)]	GetVideoData Seq=%d CTS=%d TS=%d", hStream->m_eStreamType, dwSeq, dwCTS, dwTS);*/

		if (hStream->m_Channel.empty()) {
			FREE(pPacket);
			FREE(pInfo);
			pInfo = NULL;
			continue;
		}

		for (i = 0; i < hStream->m_Channel.size(); i++) {
			hChannel = hStream->m_Channel[i];
			//TRACEX(DTB_LOG_TRACE, "[GameSender (%d)] hStream size(%d), channel(%p), udp(%d), rtp socket(0x%x)", 
			//	hStream->m_eStreamType, hStream->m_Channel.size(), hChannel, hChannel->m_bUDP, hChannel->m_hRTPSocket);
			if (!hChannel->m_bPlay) {
				LOG_W("[GameSender (%d)]	Channel is not play mode", hStream->m_eStreamType);
				continue;
			}

			if (hChannel->m_bUDP) {
				if (hStream->m_eStreamType == VIDEO_MEDIA) {
				//lRet = McSocketSendTo(hChannel->m_hRTPSocket, (char*)(pPacket + RTP_TCP_HEADER_LEN), (DWORD32)(dwPacket - RTP_TCP_HEADER_LEN),
				//	hChannel->m_szRemoteAddr, hChannel->m_wRTPRemotePort);
				//lRet = FECPacketizer(hChannel->m_hRTPSocket, (char*)(pPacket + RTP_TCP_HEADER_LEN), (DWORD32)(dwPacket - RTP_TCP_HEADER_LEN),
				//	hChannel->m_szRemoteAddr, hChannel->m_wRTPRemotePort);
				}
				else {

				lRet = McSocketSendTo(hChannel->m_hRTPSocket, (char*)(pPacket + RTP_TCP_HEADER_LEN), (DWORD32)(dwPacket - RTP_TCP_HEADER_LEN),
					hChannel->m_szRemoteAddr, hChannel->m_wRTPRemotePort);
				}
				//lRet = McSocketSendTo(hChannel->m_hRTPSocket, (char*)(pPacket + RTP_TCP_HEADER_LEN), (DWORD32)(dwPacket - RTP_TCP_HEADER_LEN),
				//	hChannel->m_szRemoteAddr, hChannel->m_wRTPRemotePort);

				if (hStream->m_eStreamType == AUDIO_MEDIA) {
					DWORD32 dwDiff = FrGetTickCount() - dwSendCurTime;
					LOG_T("[GameSender][a]	McSocketSendTo Seq=%d CTS=%d TS=%d, tickdiff(%d)", dwSeq, dwCTS, dwTS, dwDiff);
					if (dwDiff >= 20)
						LOG_T("[GameSender][a]	McSocketSendTo **********************************");
				}
				else if (hStream->m_eStreamType == VIDEO_MEDIA) {
					RTP_HEADER*		pRtpHdr = (RTP_HEADER*)(pPacket + RTP_TCP_HEADER_LEN);
					char*		pos = (char*)(pPacket + RTP_TCP_HEADER_LEN);
					int bMbit = (pos[1] & 0x80) >> 7;
					
					// RTP Header parsing
					DWORD32 wSeq = ntohs(pRtpHdr->wSeq);
					DWORD32 dwTS = ntohl(pRtpHdr->dwTimestamp);
					DWORD32 dwSSRC = ntohl(pRtpHdr->dwSSRC);
					
					if (bMbit)
						LOG_D("[GameSender][v] McSocketSendTo wSeq=%d CTS=%d dwTS=%d, MBit(%d)", dwSeq, dwCTS, dwTS, bMbit);
					//TRACEX(DTB_LOG_DEBUG, "[GameSender][v] McSocketSendTo Seq=%d CTS=%d TS=%d", dwSeq, dwCTS, dwTS);
				}

				dwSendCurTime = FrGetTickCount();
			}
			else {
				/*WORD	wSize;
				pPacket[0] = '$';
				pPacket[1] = (BYTE)(hChannel->m_wRTPRemotePort);
				wSize = htons((WORD)(dwPacket - RTP_TCP_HEADER_LEN));
				memcpy(&pPacket[2], &wSize, 2);

				dwWaitCurTime = McGetTickCount();
				lRet = McSocketSend(hChannel->m_hRTSPSocket, (char*)pPacket, (DWORD32)dwPacket);
				dwSendCurTime = McGetTickCount();*/
			}
		} // end for ()

		FREE(pPacket);
		FREE(pInfo);
		pInfo = NULL;
	}

	McCloseTask(hStream->m_hRTPSend);
	hStream->m_hRTPSend = INVALID_TASK;
	LOG_I("[GameSender]	end");
	McExitTask();
}

void TaskRTSPServer(void* lpVoid) {
	FrStreamRTPSender*		hSender = (FrStreamRTPSender*)lpVoid;
	RtspSession*	hSession = nullptr;

	int i;
	char				*pBuffer, *pRet;
	LONG				lRet;
	DWORD32 dwTimeout = 10;
	BOOL				bClose = FALSE;

	LOG_I("[GameSender]	TaskRTSPServer - begin");

	while (!hSender->m_bEnd) {
		if (hSender->m_vSession.empty()) {
			FrSleep(10);
			continue;
		}

		for (i = 0; i < hSender->m_vSession.size(); i++) {
			hSession = hSender->m_vSession[i];

			lRet = McSocketWait3(hSession->m_hRTSPSocket, dwTimeout);
			if (FRFAILED(lRet)) {
				if (lRet == (LRSLT)COMMON_ERR_TIMEOUT) {
					continue;
				}
				lRet = (LRSLT)COMMON_ERR_SVR_DISCONNECT;
				break;
			}

			bClose = FALSE;
			pBuffer = hSession->m_pRecvBuffer + hSession->m_dwRecvLen;
			lRet = McSocketRecv(hSession->m_hRTSPSocket, pBuffer, (DWORD32)(MAX_RTSP_MSG_SIZE - hSession->m_dwRecvLen));
			if (FRFAILED(lRet)) {
				LOG_E("[GameSender]	TaskRTSPServer: McSocketRecv Error (%d)", hSession->m_hRTSPSocket->m_hSocket);
				lRet = (LONG)COMMON_ERR_CLI_DISCONNECT;
				hSender->m_lError = lRet;

				//break;
				hSender->SessionClose(hSession);
				continue;
			}
			LOG_D("[GameSender]	hSession(%p) TaskRTSPServer: RTSP Recv = %d (lRet:%d), session size(%d)", 
				hSession, MAX_RTSP_MSG_SIZE - hSession->m_dwRecvLen, lRet, hSender->m_vSession.size());

			hSession->m_dwRecvLen += lRet;

			pBuffer = hSession->m_pRecvBuffer;
			while (hSession->m_dwRecvLen) {
				if (pBuffer[0] == '$') {
					pRet = hSession->RTSPParseRTPMessage(pBuffer, &hSession->m_dwRecvLen);
					if (pBuffer == pRet) {
						LOG_I("[GameSender] TaskRTSPServer: pBuffer == pRet");
						break;
					}
				}
				else {
					METHOD_TYPE	eMethod;
					BOOL		bRet;

					pRet = FrRTSPParseMessage(hSession->m_hRTSP, pBuffer, &hSession->m_dwRecvLen);
					if (!pRet) {
						LOG_I("[GameSender] TaskRTSPServer: FrRTSPParseMessage Error");
						break;
					}
					if (pBuffer == pRet) {
						LOG_I("[GameSender] TaskRTSPServer: pBuffer == pRet");
						break;
					}

					LOG_I("[GameSender C => S]");
					LOG_I("[GameSender] %s ", pBuffer);

#ifdef	FILE_DUMP
					McWriteFile(hSession->m_hFile, "C => S\r\n[", strlen("C => S\r\n["));
					McWriteFile(hSession->m_hFile, pBuffer, pRet - pBuffer);
					McWriteFile(hSession->m_hFile, "]\r\n", strlen("]\r\n"));
#endif
					// pre-process
					switch (eMethod = FrRTSPGetReqMethod(hSession->m_hRTSP))
					{
					case DESCRIBE_METHOD:
						break;
					case SETUP_METHOD:
						hSession->Setup(&hSender->m_wUDPPort, hSender->m_vStream);
						break;
					case PLAY_METHOD:
						hSession->Play(hSender);
						break;
					case PAUSE_METHOD:
						break;
					case TEARDOWN_METHOD:
						break;
					default:

						break;
					}

					bRet = FrRTSPMakeResponse(hSession->m_hRTSP, hSession->m_pSendBuffer, MAX_RTSP_MSG_SIZE);
					if (bRet) {
						LOG_I("[GameSender S => C]");
						LOG_I("[GameSender] %s ", hSession->m_pSendBuffer);
#ifdef	FILE_DUMP
						McWriteFile(hSession->m_hFile, "S => C\r\n[", strlen("S => C\r\n["));
						McWriteFile(hSession->m_hFile, hSession->m_pSendBuffer, strlen(hSession->m_pSendBuffer));
						McWriteFile(hSession->m_hFile, "]\r\n", strlen("]\r\n"));
#endif
						lRet = McSocketSend(hSession->m_hRTSPSocket, hSession->m_pSendBuffer, (DWORD32)strlen(hSession->m_pSendBuffer));
						if (FRFAILED(lRet)) {
							LOG_E("[GameSender] TaskRTSPServer: McSocketSend Error");
							hSession->m_dwRecvLen = 0;
							lRet = (LONG)COMMON_ERR_CLI_DISCONNECT;
						}
					}

					// post-process
					switch (eMethod)
					{
					case DESCRIBE_METHOD:
						break;
					case SETUP_METHOD:
						break;
					case PLAY_METHOD:
						break;
					case PAUSE_METHOD:
						break;
					case TEARDOWN_METHOD:
						bClose = TRUE;
						break;
					default:
						break;
					}

					//if (hSession->m_bWaitEvent)
					//	McSendEvent(hSession->m_hRecvEvent);
				}
				pBuffer = pRet;
			}

			if (bClose) {
				hSender->SessionClose(hSession);
				continue;
			}

			if (pBuffer != hSession->m_pRecvBuffer && hSession->m_dwRecvLen)
				memcpy(hSession->m_pRecvBuffer, pBuffer, hSession->m_dwRecvLen);
			memset(hSession->m_pRecvBuffer + hSession->m_dwRecvLen, 0, MAX_RTSP_MSG_SIZE - hSession->m_dwRecvLen);
		} // end for()
	}

	//TRACE("[TEST] end while : hSender->m_bEnd = %d\n", hSender->m_bEnd);		// on test..

	McCloseTask(hSender->m_hRTSPRecv);
	hSender->m_hRTSPRecv = INVALID_TASK;
	LOG_I("[GameSender] TaskRTSPServer - End");
	McExitTask();
}


#if 0
void TaskRTPSender(void* lpVoid) {
	RtpTaskStream*	hStream = (RtpTaskStream *)lpVoid;

	BYTE*				pPacket;
	DWORD32				dwPacket = 0;
	PacketInfo*			pInfo = NULL;
	LONG				lRet = MC_OK;

	DWORD32             dwSeq = 0;
	DWORD32             dwCTS = 0;
	DWORD32             dwTS = 0;

	DWORD32             dwWaitTime = 0;
	DWORD32             dwWaitBaseTime = 0;
	DWORD32             dwWaitCurTime = 0;

	DWORD32             dwSendTime = 0;
	DWORD32             dwSendBaseTime = 0;
	DWORD32             dwSendCurTime = 0;
	ChannelCtrl*		hChannel = NULL;
	int i;

	TRACEX(DTB_LOG_INFO, "[GameSender (%d)]	begin", hStream->m_eStreamType);

	dwWaitBaseTime = McGetTickCount();

	while (!hStream->m_bEnd) {
		pPacket = (BYTE*)UtilQueGetData(hStream->m_hBuf, &dwPacket);
		if (!pPacket) {
			//TRACEX(DTB_LOG_TRACE, "[GameSender (%d)]	Packet is NULL", hStream->m_eStreamType);
			McSleep(1);
			continue;
		}
		pInfo = (PacketInfo*)UtilQueGetData(hStream->m_hInfoBuf, NULL);
		if (pInfo) {
			dwSeq = pInfo->m_dwSeq;
			dwCTS = pInfo->m_dwCTS;
			dwTS = pInfo->m_dwTS;
		}
		else {
			dwSeq = 0;
			dwCTS = 0;
			dwTS = 0;

			TRACEX(DTB_LOG_WARN, "[GameSender (%d)]    Warning, PacketInfo is NULL", hStream->m_eStreamType);
		}


		/*if (!hStream->m_qPacket.empty()) {
		RtpPacket *&pSample = hStream->m_qPacket.front();
		hStream->m_qPacket.pop();

		pPacket = (BYTE*)pSample->pPacket;
		dwPacket = pSample->dwSize;
		dwSeq = pSample->dwSeq;
		dwCTS = pSample->dwCTS;
		dwTS = pSample->dwTS;

		FREE(pPacket);
		}*/

		/*if (hStream->m_hCirBuffer->size()) {
		RtpPacket *pSample = nullptr;
		hStream->m_hCirBuffer->read((char *)pSample, sizeof(RtpPacket));

		pPacket = (BYTE*)pSample->pPacket;
		dwPacket = pSample->dwSize;
		dwSeq = pSample->dwSeq;
		dwCTS = pSample->dwCTS;
		dwTS = pSample->dwTS;

		FREE(pPacket);
		}*/

		/*if (hStream->m_eStreamType == AUDIO_MEDIA)
		TRACEX(DTB_LOG_TRACE, "[GameSender (%d)]	GetAudioData Seq=%d CTS=%d TS=%d", hStream->m_eStreamType, dwSeq, dwCTS, dwTS);
		else if (hStream->m_eStreamType == VIDEO_MEDIA)
		TRACEX(DTB_LOG_TRACE, "[GameSender (%d)]	GetVideoData Seq=%d CTS=%d TS=%d", hStream->m_eStreamType, dwSeq, dwCTS, dwTS);*/

		if (hStream->m_Channel.empty()) {
			FREE(pPacket);
			FREE(pInfo);
			pInfo = NULL;
			continue;
		}

#ifdef RTSP_SESESSION_MODEL
		for (i = 0; i < hStream->m_Channel.size(); i++) {
			hChannel = hStream->m_Channel[i];
			//TRACEX(DTB_LOG_TRACE, "[GameSender (%d)] hStream size(%d), channel(%p), udp(%d), rtp socket(0x%x)", 
			//	hStream->m_eStreamType, hStream->m_Channel.size(), hChannel, hChannel->m_bUDP, hChannel->m_hRTPSocket);
			if (!hChannel->m_bPlay) {
				TRACEX(DTB_LOG_WARN, "[GameSender (%d)]	Channel is not play mode", hStream->m_eStreamType);
				/*FREE(pPacket);
				FREE(pInfo);
				pInfo = NULL;*/
				continue;
			}

			dwSendBaseTime = McGetTickCount();
			if (hChannel->m_bUDP) {
				dwWaitCurTime = McGetTickCount();

				lRet = McSocketSendTo(hChannel->m_hRTPSocket, (char*)(pPacket + RTP_TCP_HEADER_LEN), (DWORD32)(dwPacket - RTP_TCP_HEADER_LEN),
					hChannel->m_szRemoteAddr, hChannel->m_wRTPRemotePort);

				if (hStream->m_eStreamType == AUDIO_MEDIA)
					LOG_D("[GameSender][a]	McSocketSendTo Seq=%d CTS=%d TS=%d", dwSeq, dwCTS, dwTS);
				//else if (hStream->m_eStreamType == VIDEO_MEDIA)
				//	TRACEX(DTB_LOG_TRACE, "[GameSender (%d)]	McSocketSendTo Seq=%d CTS=%d TS=%d", hStream->m_eStreamType, dwSeq, dwCTS, dwTS);


				dwSendCurTime = McGetTickCount();
			}
			else {
				/*WORD	wSize;
				pPacket[0] = '$';
				pPacket[1] = (BYTE)(hChannel->m_wRTPRemotePort);
				wSize = htons((WORD)(dwPacket - RTP_TCP_HEADER_LEN));
				memcpy(&pPacket[2], &wSize, 2);

				dwWaitCurTime = McGetTickCount();
				lRet = McSocketSend(hChannel->m_hRTSPSocket, (char*)pPacket, (DWORD32)dwPacket);
				dwSendCurTime = McGetTickCount();*/
			}
		} // end for ()

		FREE(pPacket);
		FREE(pInfo);
		pInfo = NULL;
#else
		hChannel = hStream->m_Channel[0];
		//TRACEX(DTB_LOG_TRACE, "[GameSender (%d)] hStream size(%d), channel(%p), udp(%d), rtp socket(0x%x)", 
		//	hStream->m_eStreamType, hStream->m_Channel.size(), hChannel, hChannel->m_bUDP, hChannel->m_hRTPSocket);
		if (!hChannel->m_bPlay) {
			TRACEX(DTB_LOG_WARN, "[GameSender (%d)]	Channel is not play mode", hStream->m_eStreamType);
			FREE(pPacket);
			FREE(pInfo);
			pInfo = NULL;
			continue;
		}

		//dwSendBaseTime = McGetTickCount();
		if (hChannel->m_bUDP) {
			//dwWaitCurTime = McGetTickCount();

			lRet = McSocketSendTo(hChannel->m_hRTPSocket, (char*)(pPacket + RTP_TCP_HEADER_LEN), (DWORD32)(dwPacket - RTP_TCP_HEADER_LEN),
				hChannel->m_szRemoteAddr, hChannel->m_wRTPRemotePort);

			if (hStream->m_eStreamType == AUDIO_MEDIA) {
				DWORD32 dwDiff = McGetTickCount() - dwSendCurTime;
				LOG_D("[GameSender][a]	McSocketSendTo Seq=%d CTS=%d TS=%d, tickdiff(%d)", dwSeq, dwCTS, dwTS, dwDiff);
				if (dwDiff >= 20)
					LOG_D("[GameSender][a]	McSocketSendTo **********************************");
			}

			//else if (hStream->m_eStreamType == VIDEO_MEDIA)
			//	TRACEX(DTB_LOG_TRACE, "[GameSender (%d)]	McSocketSendTo Seq=%d CTS=%d TS=%d", hStream->m_eStreamType, dwSeq, dwCTS, dwTS);


			dwSendCurTime = McGetTickCount();
		}
		else {
			/*WORD	wSize;
			pPacket[0] = '$';
			pPacket[1] = (BYTE)(hChannel->m_wRTPRemotePort);
			wSize = htons((WORD)(dwPacket - RTP_TCP_HEADER_LEN));
			memcpy(&pPacket[2], &wSize, 2);

			dwWaitCurTime = McGetTickCount();
			lRet = McSocketSend(hChannel->m_hRTSPSocket, (char*)pPacket, (DWORD32)dwPacket);
			dwSendCurTime = McGetTickCount();*/
		}


		FREE(pPacket);
		FREE(pInfo);
		pInfo = NULL;
#endif
	}

	McCloseTask(hStream->m_hRTPSend);
	hStream->m_hRTPSend = INVALID_TASK;
	TRACEX(DTB_LOG_INFO, "[GameSender]	end");
	McExitTask();
}

void TaskRTSPServer(void* lpVoid) {
	GameSender*		hSender = (GameSender*)lpVoid;
	//RtspSession*	hSession = hSender->m_Session[0];
	//RtspSession*	hSession = hSender->m_Session.front();
	RtspSession*	hSession = nullptr;

	int i;
	char				*pBuffer, *pRet;
	LONG				lRet;
	DWORD32 dwTimeout = 10;
	BOOL				bClose = FALSE;

	TRACEX(DTB_LOG_INFO, "[GameSender]	TaskRTSPServer - begin");

	while (!hSender->m_bEnd) {
		/*if (!hSession) {
		McSleep(10);
		continue;
		}*/
		if (hSender->m_vSession.empty()) {
			McSleep(10);
			continue;
		}
#ifdef RTSP_SESESSION_MODEL
		for (i = 0; i < hSender->m_vSession.size(); i++) {
			hSession = hSender->m_vSession[i];

			lRet = McSocketWait3(hSession->m_hRTSPSocket, dwTimeout);
			if (FRFAILED(lRet)) {
				if (lRet == (LRSLT)COMMON_ERR_TIMEOUT) {
					continue;
				}
				lRet = (LRSLT)COMMON_ERR_SVR_DISCONNECT;
				break;
			}

			bClose = FALSE;
			pBuffer = hSession->m_pRecvBuffer + hSession->m_dwRecvLen;
			lRet = McSocketRecv(hSession->m_hRTSPSocket, pBuffer, (DWORD32)(MAX_RTSP_MSG_SIZE - hSession->m_dwRecvLen));
			if (FRFAILED(lRet)) {
				TRACEX(DTB_LOG_DEBUG, "[GameSender]	TaskRTSPServer: McSocketRecv Error (%d)", hSession->m_hRTSPSocket->m_hSocket);
				lRet = (LONG)COMMON_ERR_CLI_DISCONNECT;
				hSender->m_lError = lRet;

				//break;
				hSender->SessionClose(hSession);
				continue;
			}
			TRACEX(DTB_LOG_DEBUG, "[GameSender]	TaskRTSPServer: RTSP Recv = %d (lRet:%d), session size(%d)", MAX_RTSP_MSG_SIZE - hSession->m_dwRecvLen, lRet, hSender->m_vSession.size());

			hSession->m_dwRecvLen += lRet;

			pBuffer = hSession->m_pRecvBuffer;
			while (hSession->m_dwRecvLen) {
				if (pBuffer[0] == '$') {
					pRet = hSession->RTSPParseRTPMessage(pBuffer, &hSession->m_dwRecvLen);
					if (pBuffer == pRet)
					{
						TRACEX(DTB_LOG_INFO, "[GameSender] TaskRTSPServer: pBuffer == pRet");
						break;
					}
				}
				else {
					METHOD_TYPE	eMethod;
					BOOL		bRet;

					pRet = FrRTSPParseMessage(hSession->m_hRTSP, pBuffer, &hSession->m_dwRecvLen);
					if (!pRet) {
						TRACEX(DTB_LOG_INFO, "[GameSender] TaskRTSPServer: FrRTSPParseMessage Error");
						break;
					}
					if (pBuffer == pRet) {
						TRACEX(DTB_LOG_INFO, "[GameSender] TaskRTSPServer: pBuffer == pRet");
						break;
					}

					TRACEX(DTB_LOG_INFO, "[GameSender C => S]");
					TRACEX(DTB_LOG_INFO, "[GameSender] %s ", pBuffer);

#ifdef	FILE_DUMP
					McWriteFile(hSession->m_hFile, "C => S\r\n[", strlen("C => S\r\n["));
					McWriteFile(hSession->m_hFile, pBuffer, pRet - pBuffer);
					McWriteFile(hSession->m_hFile, "]\r\n", strlen("]\r\n"));
#endif
					// pre-process
					switch (eMethod = FrRTSPGetReqMethod(hSession->m_hRTSP))
					{
					case DESCRIBE_METHOD:
						break;
					case SETUP_METHOD:
						hSession->Setup(&hSender->m_wUDPPort, hSender->m_vStream);
						break;
					case PLAY_METHOD:
						hSession->Play();
						break;
					case PAUSE_METHOD:
						break;
					case TEARDOWN_METHOD:
						break;
					default:

						break;
					}

					bRet = FrRTSPMakeResponse(hSession->m_hRTSP, hSession->m_pSendBuffer, MAX_RTSP_MSG_SIZE);
					if (bRet) {
						TRACEX(DTB_LOG_INFO, "[GameSender S => C]");
						TRACEX(DTB_LOG_INFO, "[GameSender] %s ", hSession->m_pSendBuffer);
#ifdef	FILE_DUMP
						McWriteFile(hSession->m_hFile, "S => C\r\n[", strlen("S => C\r\n["));
						McWriteFile(hSession->m_hFile, hSession->m_pSendBuffer, strlen(hSession->m_pSendBuffer));
						McWriteFile(hSession->m_hFile, "]\r\n", strlen("]\r\n"));
#endif
						lRet = McSocketSend(hSession->m_hRTSPSocket, hSession->m_pSendBuffer, (DWORD32)strlen(hSession->m_pSendBuffer));
						if (FRFAILED(lRet)) {
							TRACEX(DTB_LOG_ERROR, "[GameSender] TaskRTSPServer: McSocketSend Error");
							hSession->m_dwRecvLen = 0;
							lRet = (LONG)COMMON_ERR_CLI_DISCONNECT;
						}
					}

					// post-process
					switch (eMethod)
					{
					case DESCRIBE_METHOD:
						break;
					case SETUP_METHOD:
						break;
					case PLAY_METHOD:
						break;
					case PAUSE_METHOD:
						break;
					case TEARDOWN_METHOD:
						bClose = TRUE;
						break;
					default:
						break;
					}

					//if (hSession->m_bWaitEvent)
					//	McSendEvent(hSession->m_hRecvEvent);
				}
				pBuffer = pRet;
			}

			if (bClose) {
				hSender->SessionClose(hSession);
				continue;
			}

			if (pBuffer != hSession->m_pRecvBuffer && hSession->m_dwRecvLen)
				memcpy(hSession->m_pRecvBuffer, pBuffer, hSession->m_dwRecvLen);
			memset(hSession->m_pRecvBuffer + hSession->m_dwRecvLen, 0, MAX_RTSP_MSG_SIZE - hSession->m_dwRecvLen);
		} // end for()

#else
		hSession = hSender->m_vSession[0];
		lRet = McSocketWait3(hSession->m_hRTSPSocket, dwTimeout);
		if (FRFAILED(lRet)) {
			if (lRet == (LRSLT)COMMON_ERR_TIMEOUT) {
				continue;
			}
			lRet = (LRSLT)COMMON_ERR_SVR_DISCONNECT;
			break;
		}

		bClose = FALSE;
		pBuffer = hSession->m_pRecvBuffer + hSession->m_dwRecvLen;
		lRet = McSocketRecv(hSession->m_hRTSPSocket, pBuffer, (DWORD32)(MAX_RTSP_MSG_SIZE - hSession->m_dwRecvLen));
		if (FRFAILED(lRet)) {
			TRACEX(DTB_LOG_DEBUG, "[GameSender]	TaskRTSPServer: McSocketRecv Error (%d)", hSession->m_hRTSPSocket->m_hSocket);
			lRet = (LONG)COMMON_ERR_CLI_DISCONNECT;
			hSender->m_lError = lRet;

			//break;
			hSender->SessionClose(hSession);
			continue;
		}
		TRACEX(DTB_LOG_DEBUG, "[GameSender]	TaskRTSPServer: RTSP Recv = %d (lRet:%d)", MAX_RTSP_MSG_SIZE - hSession->m_dwRecvLen, lRet);

		hSession->m_dwRecvLen += lRet;

		pBuffer = hSession->m_pRecvBuffer;
		while (hSession->m_dwRecvLen) {
			if (pBuffer[0] == '$') {
				pRet = hSession->RTSPParseRTPMessage(pBuffer, &hSession->m_dwRecvLen);
				if (pBuffer == pRet)
				{
					TRACEX(DTB_LOG_INFO, "[GameSender] TaskRTSPServer: pBuffer == pRet");
					break;
				}
			}
			else {
				METHOD_TYPE	eMethod;
				BOOL		bRet;

				pRet = FrRTSPParseMessage(hSession->m_hRTSP, pBuffer, &hSession->m_dwRecvLen);
				if (!pRet) {
					TRACEX(DTB_LOG_INFO, "[GameSender] TaskRTSPServer: FrRTSPParseMessage Error");
					break;
				}
				if (pBuffer == pRet) {
					TRACEX(DTB_LOG_INFO, "[GameSender] TaskRTSPServer: pBuffer == pRet");
					break;
				}

				TRACEX(DTB_LOG_INFO, "[GameSender C => S]");
				TRACEX(DTB_LOG_INFO, "[GameSender] %s ", pBuffer);

#ifdef	FILE_DUMP
				McWriteFile(hSession->m_hFile, "C => S\r\n[", strlen("C => S\r\n["));
				McWriteFile(hSession->m_hFile, pBuffer, pRet - pBuffer);
				McWriteFile(hSession->m_hFile, "]\r\n", strlen("]\r\n"));
#endif
				// pre-process
				switch (eMethod = FrRTSPGetReqMethod(hSession->m_hRTSP))
				{
				case DESCRIBE_METHOD:
					break;
				case SETUP_METHOD:
					hSender->SessionSetup();
					break;
				case PLAY_METHOD:
					hSender->SessionPlay();
					break;
				case PAUSE_METHOD:
					break;
				case TEARDOWN_METHOD:
					break;
				default:

					break;
				}

				bRet = FrRTSPMakeResponse(hSession->m_hRTSP, hSession->m_pSendBuffer, MAX_RTSP_MSG_SIZE);
				if (bRet) {
					TRACEX(DTB_LOG_INFO, "[GameSender S => C]");
					TRACEX(DTB_LOG_INFO, "[GameSender] %s ", hSession->m_pSendBuffer);
#ifdef	FILE_DUMP
					McWriteFile(hSession->m_hFile, "S => C\r\n[", strlen("S => C\r\n["));
					McWriteFile(hSession->m_hFile, hSession->m_pSendBuffer, strlen(hSession->m_pSendBuffer));
					McWriteFile(hSession->m_hFile, "]\r\n", strlen("]\r\n"));
#endif
					lRet = McSocketSend(hSession->m_hRTSPSocket, hSession->m_pSendBuffer, (DWORD32)strlen(hSession->m_pSendBuffer));
					if (FRFAILED(lRet)) {
						TRACEX(DTB_LOG_ERROR, "[GameSender] TaskRTSPServer: McSocketSend Error");
						hSession->m_dwRecvLen = 0;
						lRet = (LONG)COMMON_ERR_CLI_DISCONNECT;
					}
				}

				// post-process
				switch (eMethod)
				{
				case DESCRIBE_METHOD:
					break;
				case SETUP_METHOD:
					break;
				case PLAY_METHOD:
					break;
				case PAUSE_METHOD:
					break;
				case TEARDOWN_METHOD:
					bClose = TRUE;
					break;
				default:
					break;
				}

				//if (hSession->m_bWaitEvent)
				//	McSendEvent(hSession->m_hRecvEvent);
			}
			pBuffer = pRet;
		}

		if (bClose) {
			hSender->SessionClose(hSession);
			continue;
		}

		if (pBuffer != hSession->m_pRecvBuffer && hSession->m_dwRecvLen)
			memcpy(hSession->m_pRecvBuffer, pBuffer, hSession->m_dwRecvLen);
		memset(hSession->m_pRecvBuffer + hSession->m_dwRecvLen, 0, MAX_RTSP_MSG_SIZE - hSession->m_dwRecvLen);
#endif
		//McSleep(10);
	}

	//TRACE("[TEST] end while : hSender->m_bEnd = %d\n", hSender->m_bEnd);		// on test..

	McCloseTask(hSender->m_hRTSPRecv);
	hSender->m_hRTSPRecv = INVALID_TASK;
	TRACEX(DTB_LOG_INFO, "[GameSender] TaskRTSPServer - End");
	McExitTask();
}
#endif