#include "DecoderAPI.h"
#include "ArTransServer.h"


#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/cudaimgproc.hpp"
#include "opencv2/cudawarping.hpp"
#include "opencv2/cudaarithm.hpp"

#ifdef WIN32
#if defined(_DEBUG)
#pragma comment(lib, "opencv_core440d.lib")
#pragma comment(lib, "opencv_highgui440d.lib")
#pragma comment(lib, "opencv_imgproc440d.lib")
#pragma comment(lib, "opencv_imgcodecs440d.lib")
#pragma comment(lib, "opencv_calib3d440d.lib")
#pragma comment(lib, "opencv_cudaimgproc440d.lib")
#pragma comment(lib, "opencv_cudawarping440d.lib")
#pragma comment(lib, "opencv_cudaarithm440d.lib")
#else
#pragma comment(lib, "opencv_core440.lib")
#pragma comment(lib, "opencv_highgui440.lib")
#pragma comment(lib, "opencv_imgproc440.lib")
#pragma comment(lib, "opencv_imgcodecs440.lib")
#pragma comment(lib, "opencv_calib3d440.lib")
#pragma comment(lib, "opencv_cudaimgproc440.lib")
#pragma comment(lib, "opencv_cudawarping440.lib")
#pragma comment(lib, "opencv_cudaarithm440.lib")
#endif
#endif

//void TaskArServer(void* lpVoid) {
//	FrArServer* hServer = (FrArServer*)lpVoid;
//	LRSLT lRet;
//	FrMediaStream VideoData;
//	memset(&VideoData, 0, sizeof(FrMediaStream));
//    VideoData.tMediaType = VIDEO_MEDIA;
//
//	
//	TRACE("[Streamer] TaskSound() start..");
//
//	while (!hServer->m_bEnd) {
//
//        FrSleep(10);
//	}
//
//	McCloseTask(hServer->m_hArSvrTask);
//    hServer->m_hArSvrTask = INVALID_TASK;
//
//	TRACE("[Streamer] TaskSound() end..");
//
//	McExitTask();
//}


int FrArServer::FrSvrOpen(ArSvrParam& param) {
    int ret;
    m_bBenchTest = false;

    if (param.codec == "H.264")
        m_EncInfo.FrVideo.dwFourCC = FOURCC_H264;
    else if (param.codec == "H.265")
        m_EncInfo.FrVideo.dwFourCC = FOURCC_HEVC;
    else
        m_EncInfo.FrVideo.dwFourCC = FOURCC_H264;
    
    if (param.codecType == "SW") {
        m_SrcInfo.FrVideo.eCodecType = CODEC_SW_ESM;
        //m_EncInfo.FrVideo.eCodecType = CODEC_SW;
    }
    else {
        m_SrcInfo.FrVideo.eCodecType = CODEC_HW;
    }

    if (param.codecEncType == "SW") {
        m_EncInfo.FrVideo.eCodecType = CODEC_SW_ESM;
        //m_EncInfo.FrVideo.eCodecType = CODEC_SW;
    }
    else {
        m_EncInfo.FrVideo.eCodecType = CODEC_HW;
    }

        
	m_EncInfo.FrVideo.dwWidth = param.dwWidth;
	m_EncInfo.FrVideo.dwHeight = param.dwHeight;
	m_EncInfo.FrVideo.dwBitRate = param.dwBitrate;
	m_EncInfo.FrVideo.dwFrameRate = param.dwFramerate * 1000;
    m_EncInfo.FrVideo.fIntraFrameRefresh = param.dwInterval;
    m_EncInfo.FrVideo.dwGopLength = param.dwGopLength;
    if (param.bEnableStreaming)
        m_bServer = TRUE;
    else
        m_bServer = FALSE;
    m_bBenchTest = param.bBenchTest;

    FrSocketInitNetwork();

	// open rtp sender..
	m_UrlInfo.pUrl = (char *)param.url.c_str();
    m_UrlInfo.URLType = _MP4_FILE;

    m_DstUrlInfo = {};
    strcpy(m_szDstName, (char*)param.dstUrl.c_str());
    m_DstUrlInfo.pUrl = m_szDstName;
    m_DstUrlInfo.URLType = _MP4_FF_FILE; //_MP4_FF_FILE; //_MP4_FILE;
    ret = SvrInit(m_UrlInfo, param.wSvrPort, m_DstUrlInfo);
    if (ret < 0) {
        FrSvrClose();
        return -1;
    }

    SvrStart();

	return 1;
}

void FrArServer::FrSvrClose() {
    SvrStop();

    FrSocketCloseNetwork();
}

FrArServer::FrArServer() {
    memset(&m_UrlInfo, 0, sizeof(FrURLInfo));
    memset(&m_EncInfo, 0, sizeof(FrMediaInfo));
    memset(&m_SrcInfo, 0, sizeof(FrMediaInfo));

    FrSysInit();
    
}

FrArServer::~FrArServer() {

}

void FrArServer::ArServerThread() {
    LRSLT lRet;
    //DWORD32 dwTime;
    //int diff;
    FrMediaStream VideoData;
    FrMediaStream EncStream;
    FrRawVideo  DecVideo;
    FrRawVideo  ScaleVideo;
    FrRawVideo  ConvVideo;

    DWORD dwTotalVideoFrameLenth = 0;

    memset(&EncStream, 0, sizeof(FrMediaStream));
    memset(&VideoData, 0, sizeof(FrMediaStream));
    VideoData.tMediaType = VIDEO_MEDIA;
    EncStream.tMediaType = VIDEO_MEDIA;

    m_bEncoding = true;

    TRACE("ArServerThread() Begin..");
    
    FrTimeStart(m_hTime);

    while (!m_bEnd) {
        lRet = FrReaderReadFrame(m_hReader, &VideoData);
        if (FRFAILED(lRet)) {
            if (lRet == COMMON_ERR_ENDOFDATA)
                break;

            FrSleep(10);
            continue;
        }

        dwTotalVideoFrameLenth += VideoData.dwFrameLen[0];
        TRACE("ArServerThread() : total video frame len(%d), cur len(%d), type(%d).", dwTotalVideoFrameLenth, VideoData.dwFrameLen[0], VideoData.tFrameType[0]);
        
        lRet = FrVideoDecDecode(m_hDecVideo, &VideoData, &DecVideo);
        if (FRFAILED(lRet)) {
            continue;
        }
        if (m_bEncoding == true) {
            for (int i = 0; i < DecVideo.nDecoded; i++) {
                // repos
                DecVideo.pY = DecVideo.ppDecoded[i];
                DecVideo.pU = DecVideo.pY + DecVideo.dwPitch * DecVideo.dwDecodedHeight;
                DecVideo.pV = DecVideo.pU + DecVideo.dwPitch * DecVideo.dwDecodedHeight / 4;


                // direct
                lRet = FrVideoEncEncode(m_hEncVideo, &DecVideo, &EncStream);
                if (FRFAILED(lRet)) {
                    continue;
                }


                if (m_bServer) {
                    // send rtp        
                    m_hSender->SendFrame(&EncStream, NULL);
                }
                else {
                    lRet = FrWriterWriteFrame(m_hWriter, &EncStream);
                    if (FRFAILED(lRet)) {
                        continue;
                    }
                }
            }
        }        
        //FrSleep(10);
    }

    if (m_bEncoding == true) {
        if (m_hWriter != nullptr && !m_bServer) {
            FrWriterUpdateInfo(m_hWriter);
            FrWriterClose(m_hWriter);
            m_hWriter = nullptr;
        }
    }
    

    TRACE("ArServerThread() End..");

    m_hArSvrThread = nullptr;
}


int FrArServer::SvrStart() {
    DWORD32 dwStart = 0;
    LRSLT lRet;
    

    if (m_SrcInfo.dwVideoTotal && !m_bBenchTest) {
        m_hArSvrThread = new std::thread(&FrArServer::ArServerThread, this);
    }
        
    lRet = FrReaderStart(m_hReader, &dwStart);
    if (FRFAILED(lRet)) {
        return -1;
    }
    

    return 1;
}

int FrArServer::SvrStop() {
    LOG_I("SvrStop Begin..");

    m_bEnd = TRUE;
    
    while (m_hArSvrThread != nullptr) {
        FrSleep(10);
    }

    if (m_hSender) {
        m_hSender->Close();
        m_hSender = nullptr;
    }
     
    
    if (m_hEncVideo != nullptr) {
        FrVideoEncClose(m_hEncVideo);
        m_hEncVideo = nullptr;
    }
        
    if (m_hScaler != nullptr) {
        FrVideoResizeClose(m_hScaler);
        m_hScaler = nullptr;
    }

    if (m_hConv != nullptr) {
        FrVideoConvClose(m_hConv);
        m_hConv = nullptr;
    }

    if (m_hDecVideo != nullptr) {
        FrVideoDecClose(m_hDecVideo);
        m_hDecVideo = nullptr;
    }

    if (m_hWriter != nullptr) {
        FrWriterUpdateInfo(m_hWriter);
        FrWriterClose(m_hWriter);
    }
    
    
    
    if (m_hReader != nullptr) {
        FrReaderClose(m_hReader);
        m_hReader = nullptr;
    }
    
    if (m_hTime != nullptr) {
        FrTimeStop(m_hTime);
        FrTimeClose(m_hTime);
        m_hTime = nullptr;
    }

    LOG_I("SvrStop End..");

    return 1;
}

int FrArServer::SvrInit(FrURLInfo& URLInfo, short listenPort, FrURLInfo& WriteInfo) {
    LOG_I("SvrInit Begin..");

    m_bEnd = FALSE;
    //m_bServer = FALSE;

    m_hTime = FrTimeOpen(0, 1.0);

    
    // reader
    LRSLT lRet = FrReaderOpen(&m_hReader, &URLInfo);
    if (FRFAILED(lRet)) {
        return -1;
    }

    lRet = FrReaderGetInfo(m_hReader, &m_SrcInfo);
    if (FRFAILED(lRet)) {
        return -1;
    }
        
    // decoder
    FrVideoInfo* pVideoInfo = &m_SrcInfo.FrVideo;
    pVideoInfo->eColorFormat = VideoFmtIYUV; //VideoFmtBGRA; //VideoFmtNV12;                    //VideoFmtYUV420P;     // VideoFmtIYUV //output color format
    pVideoInfo->bLowDelayDecoding = TRUE;

    if (m_EncInfo.FrVideo.eCodecType == CODEC_HW) {
        pVideoInfo->memDir = MEM_GPU;
    }
    else {
        pVideoInfo->memDir = MEM_CPU;
    }
    pVideoInfo->dwWidth = m_EncInfo.FrVideo.dwWidth;
    pVideoInfo->dwHeight = m_EncInfo.FrVideo.dwHeight;

    lRet = FrVideoDecOpen(&m_hDecVideo, NULL, pVideoInfo);
    if (FRFAILED(lRet)) {
        return -1;
    }
        
    // converter(scale, color, fps)
    lRet = FrVideoResizeOpen(&m_hScaler, pVideoInfo, &m_EncInfo.FrVideo);
    if (FRFAILED(lRet)) {
        return -1;
    }
        
    // color conv
    lRet = FrVideoConvOpen(&m_hConv, pVideoInfo, VideoFmtYV12);
    if (FRFAILED(lRet)) {
        return -1;
    }
        
    // need to open image processor
    // ..


    // encoder
    m_EncInfo.dwVideoTotal = 1;
    //m_EncInfo.FrVideo.dwFourCC = FOURCC_AVC1;
    //m_EncInfo.FrVideo.eCodecType = CODEC_SW;    //CODEC_HW;
    // need to set others..
    // ..
    m_EncInfo.FrVideo.eColorFormat = VideoFmtIYUV;
    lRet = FrVideoEncOpen(&m_hEncVideo, &m_EncInfo.FrVideo);
    if (FRFAILED(lRet)) {
        return -1;
    }
    
    // stream sender
    if (m_bServer) {
        m_hSender = new FrStreamRTPSender();
        lRet = m_hSender->Open(&m_EncInfo, listenPort);
        if (FRFAILED(lRet)) {
            return -1;
        }
    }
    else {
        //// writer
        lRet = FrWriterOpen(&m_hWriter, &WriteInfo);
        if (FRFAILED(lRet)) {
            return -1;
        }

        lRet = FrWriterSetInfo(m_hWriter, &m_EncInfo, 0);
        if (FRFAILED(lRet)) {
            return -1;
        }
    }
    

    LOG_I("SvrInit End..");

    return 1;
}

void FrArServer::FrBenchTest(ArSvrParam& param) {
    FrSvrOpen(param);
    ArServerThread();
    /*while (m_vhReadThread[0] != nullptr) {
        FrSleep(10);
    }
    while (m_vhDecThread[0] != nullptr) {
        FrSleep(10);
    }*/
    FrSvrClose();
}














#if 0
void FrArServer::ReadThread(int idx) {
    LRSLT lRet;
    //DWORD32 dwTime;
    //int diff;
    FrMediaStream VideoData;
    FrMediaStream EncStream;
    FrRawVideo  DecVideo;
    FrRawVideo  ScaleVideo;
    FrRawVideo  ConvVideo;

    DWORD dwTotalVideoFrameLenth = 0;

    memset(&EncStream, 0, sizeof(FrMediaStream));
    memset(&VideoData, 0, sizeof(FrMediaStream));
    VideoData.tMediaType = VIDEO_MEDIA;
    EncStream.tMediaType = VIDEO_MEDIA;

    TRACE("ArServerThread() Begin..idx(%d)", idx);

    FrTimeStart(m_hTime);



    while (!m_bEnd) {
        lRet = FrReaderReadFrame(m_vhReader[idx], &VideoData);
        if (FRFAILED(lRet)) {
            if (lRet == COMMON_ERR_ENDOFDATA)
                break;

            FrSleep(10);
            continue;
        }

        dwTotalVideoFrameLenth += VideoData.dwFrameLen[0];
        TRACE("ArServerThread() : total video frame len(%d), cur len(%d), type(%d).", dwTotalVideoFrameLenth, VideoData.dwFrameLen[0], VideoData.tFrameType[0]);

        //for (int i = 0; i < m_sizeOfCh; i++) {
        m_xFrame.lock();
        FrMediaStream* pStream = &VideoData;
        uchar* pFrame = new uchar[VideoData.dwFrameLen[0]];
        memcpy(pFrame, VideoData.pFrame, VideoData.dwFrameLen[0]);
        pStream->pFrame = pFrame;

        //m_vStreams[i].push_back(VideoData);
        m_vStreams[idx].push_back(*pStream);
        m_xFrame.unlock();
        //}    
    }


    TRACE("ArServerThread() End..");

    m_vhReadThread[idx] = nullptr;
}

void FrArServer::DecThread(int idx) {
    LRSLT lRet;
    //DWORD32 dwTime;
    //int diff;
    FrMediaStream VideoData;
    FrMediaStream EncStream;
    FrRawVideo  DecVideo;
    FrRawVideo  ScaleVideo;
    FrRawVideo  ConvVideo;

    FrMediaStream* pVideoData;

    DWORD dwTotalVideoFrameLenth = 0;

    memset(&EncStream, 0, sizeof(FrMediaStream));
    memset(&VideoData, 0, sizeof(FrMediaStream));
    VideoData.tMediaType = VIDEO_MEDIA;
    EncStream.tMediaType = VIDEO_MEDIA;

    int index = idx;

    TRACE("DecThread() Begin..index: (%d)", index);

    FrTimeStart(m_hTime);

    while (!m_bEnd) {
        if (!m_vStreams[index].size()) {
            FrSleep(1);
            continue;
        }
        m_xFrame.lock();
        for (int i = 0; i < m_vStreams[index].size(); i++) {
            pVideoData = &m_vStreams[index][i];

            //lRet = FrVideoDecDecode(m_hDecVideo, &VideoData, &DecVideo);
            lRet = FrVideoDecDecode(m_vhDecVideo[index], pVideoData, &DecVideo);

            delete[] pVideoData->pFrame;

            if (FRFAILED(lRet)) {
                continue;
            }
            for (int i = 0; i < DecVideo.nDecoded; i++) {
                // repos
                DecVideo.pY = DecVideo.ppDecoded[i];

                // push raw data..
                // ..
                // ..

            }
        }
        m_vStreams[index].clear();
        m_xFrame.unlock();


        //FrSleep(10);
    }

    TRACE("DecThread() End..");
    m_vhDecThread[index] = nullptr;
}

void FrArServer::EncThread() {
    LRSLT lRet;
    //DWORD32 dwTime;
    //int diff;
    FrMediaStream VideoData;
    FrMediaStream EncStream;
    FrRawVideo  DecVideo;
    FrRawVideo  ScaleVideo;
    FrRawVideo  ConvVideo;

    DWORD dwTotalVideoFrameLenth = 0;

    memset(&EncStream, 0, sizeof(FrMediaStream));
    memset(&VideoData, 0, sizeof(FrMediaStream));
    VideoData.tMediaType = VIDEO_MEDIA;
    EncStream.tMediaType = VIDEO_MEDIA;

    TRACE("ArServerThread() Begin..");

    FrTimeStart(m_hTime);

    while (!m_bEnd) {
        lRet = FrReaderReadFrame(m_hReader, &VideoData);
        if (FRFAILED(lRet)) {
            if (lRet == COMMON_ERR_ENDOFDATA)
                break;

            FrSleep(10);
            continue;
        }

        dwTotalVideoFrameLenth += VideoData.dwFrameLen[0];
        TRACE("ArServerThread() : total video frame len(%d), cur len(%d), type(%d).", dwTotalVideoFrameLenth, VideoData.dwFrameLen[0], VideoData.tFrameType[0]);

        lRet = FrVideoDecDecode(m_hDecVideo, &VideoData, &DecVideo);
        if (FRFAILED(lRet)) {
            continue;
        }
        for (int i = 0; i < DecVideo.nDecoded; i++) {
            // repos
            DecVideo.pY = DecVideo.ppDecoded[i];


            // direct
            lRet = FrVideoEncEncode(m_hEncVideo, &DecVideo, &EncStream);
            if (FRFAILED(lRet)) {
                continue;
            }


            if (m_bServer) {
                // send rtp        
                m_hSender->SendFrame(&EncStream, NULL);
            }
            else {
                lRet = FrWriterWriteFrame(m_hWriter, &EncStream);
                if (FRFAILED(lRet)) {
                    continue;
                }
            }
        }

        //FrSleep(10);
    }


    // on test..
    if (m_hWriter != nullptr && !m_bServer) {
        FrWriterUpdateInfo(m_hWriter);
        FrWriterClose(m_hWriter);
        m_hWriter = nullptr;
    }

    TRACE("ArServerThread() End..");
    m_hEncThread = nullptr;
}
#endif