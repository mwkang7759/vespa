/*
Thanks Nghia Ho for his excellent code.
And,I modified the smooth step using a simple kalman filter .
So,It can processes live video streaming.
modified by chen jia.
email:chenjia2013@foxmail.com
*/

//#include <opencv2/opencv.hpp>
//#include <iostream>
//#include <cassert>
//#include <cmath>
//#include <fstream>
//#include "DefData.hpp"

#include "LiveStabil.h"

#include "TraceAPI.h"

#if defined(_DEBUG)
#pragma comment(lib, "opencv_core440d.lib")
#pragma comment(lib, "opencv_highgui440d.lib")
#pragma comment(lib, "opencv_imgproc440d.lib")
#pragma comment(lib, "opencv_imgcodecs440d.lib")
#pragma comment(lib, "opencv_calib3d440d.lib")
#pragma comment(lib, "opencv_cudaimgproc440d.lib")
#pragma comment(lib, "opencv_cudawarping440d.lib")
#pragma comment(lib, "opencv_cudaarithm440d.lib")
#pragma comment(lib, "opencv_video440d.lib")
#pragma comment(lib, "opencv_videoio440d.lib")
#pragma comment(lib, "opencv_cudaoptflow440d.lib")

#else
#pragma comment(lib, "opencv_core440.lib")
#pragma comment(lib, "opencv_highgui440.lib")
#pragma comment(lib, "opencv_imgproc440.lib")
#pragma comment(lib, "opencv_imgcodecs440.lib")
#pragma comment(lib, "opencv_calib3d440.lib")
#pragma comment(lib, "opencv_cudaimgproc440.lib")
#pragma comment(lib, "opencv_cudawarping440.lib")
#pragma comment(lib, "opencv_cudaarithm440.lib")
#pragma comment(lib, "opencv_video440.lib")
#pragma comment(lib, "opencv_videoio440.lib")
#pragma comment(lib, "opencv_cudaoptflow440.lib")
#endif

using namespace std;
using namespace cv;
using namespace dove;
// This video stablisation smooths the global trajectory using a sliding average window

//const int SMOOTHING_RADIUS = 15; // In frames. The larger the more stable the video, but less reactive to sudden panning
const int HORIZONTAL_BORDER_CROP = 20; // In pixels. Crops the border to reduce the black borders from stabilisation being too noticeable.


// 1. Get previous to current frame transformation (dx, dy, da) for all frames
// 2. Accumulate the transformations to get the image trajectory
// 3. Smooth out the trajectory using an averaging window
// 4. Generate new set of previous to current transform, such that the trajectory ends up being the same as the smoothed trajectory
// 5. Apply the new transformation to the video


LiveStabil::LiveStabil() {
	out_transform.open("prev_to_cur_transformation.txt");
	out_trajectory.open("trajectory.txt");
	out_smoothed_trajectory.open("smoothed_trajectory.txt");
	out_new_transform.open("new_prev_to_cur_transformation.txt");

	_imgHeight = 1080;
	_imgWidth = 1920;
	_firstImg = false;
}

LiveStabil::LiveStabil(int h, int w) {
	LiveStabil();
	_imgHeight = h;
	_imgWidth = w;
	_firstImg = false;
}

LiveStabil::~LiveStabil() {

}

int LiveStabil::StabilImage(uchar* image, int pitch, cv::Mat& cur, cv::Mat& cur2)
{
	Mat cur_grey;
	
	auto start = std::chrono::system_clock::now();

	cv::cuda::GpuMat img;
	FrCvClrConv::YUV2BGR(img, (uint8_t*)image, _imgWidth, _imgHeight, pitch);
	//cv::cuda::GpuMat img(_imgHeight, _imgWidth, CV_8UC1, image, _imgPitch);

	auto mid1 = std::chrono::system_clock::now();
	std::chrono::milliseconds delta = std::chrono::duration_cast<std::chrono::milliseconds>(mid1 - start);
	LOG_D("live stabil.. FrCvClrConv::YUV2BGR Tick=%d", delta.count());

	img.download(cur);

	auto mid2 = std::chrono::system_clock::now();
	delta = std::chrono::duration_cast<std::chrono::milliseconds>(mid2 - mid1);
	LOG_D("live stabil.. img.download(cur) Tick=%d", delta.count());


	if (!_firstImg) {
		_prevImage = cur.clone();
		cvtColor(_prevImage, _prevGray, COLOR_BGR2GRAY);
		
		_firstImg = true;

		// Step 1 - Get previous to current frame transformation (dx, dy, da) for all frames
		vector <TransformParam> prev_to_cur_transform; // previous to current
		// Accumulated frame to frame transform
		//double a = 0;
		//double x = 0;
		//double y = 0;
		
		// Step 2 - Accumulate the transformations to get the image trajectory
		vector <Trajectory> trajectory; // trajectory at all frames
		//
		// Step 3 - Smooth out the trajectory using an averaging window
		vector <Trajectory> smoothed_trajectory; // trajectory at all frames
		//Trajectory X;	//posteriori state estimate
		//Trajectory X_;	//priori estimate
		//Trajectory P;	// posteriori estimate error covariance
		//Trajectory P_;	// priori estimate error covariance
		//Trajectory K;	//gain
		//Trajectory	z;	//actual measurement
		double pstd = 4e-3;//can be changed
		double cstd = 0.25;//can be changed
		Q.set(pstd, pstd, pstd);// process noise covariance
		R.set(cstd, cstd, cstd);// measurement noise covariance 
		// Step 4 - Generate new set of previous to current transform, such that the trajectory ends up being the same as the smoothed trajectory
		vector <TransformParam> new_prev_to_cur_transform;
		//
		// Step 5 - Apply the new transformation to the video
		//cap.set(CV_CAP_PROP_POS_FRAMES, 0);
		Mat T(2, 3, CV_64F);

		vert_border = HORIZONTAL_BORDER_CROP * _prevImage.rows / _prevImage.cols; // get the aspect ratio correct
		//int vert_border = HORIZONTAL_BORDER_CROP * _imgHeight / _imgWidth; // get the aspect ratio correct

		//int k = 1;

		//Mat last_T;
		Mat prev_grey_, cur_grey_;

		return 0;
	}
	cvtColor(cur, cur_grey, COLOR_BGR2GRAY);

	auto mid3 = std::chrono::system_clock::now();
	delta = std::chrono::duration_cast<std::chrono::milliseconds>(mid3 - mid2);
	LOG_D("live stabil.. cvtColor(gray) Tick=%d", delta.count());

	// vector from prev to cur
	vector <Point2f> prev_corner, cur_corner;
	vector <Point2f> prev_corner2, cur_corner2;
	vector <uchar> status;
	vector <float> err;

	//goodFeaturesToTrack(prev_grey, prev_corner, 200, 0.01, 30);
	//calcOpticalFlowPyrLK(prev_grey, cur_grey, prev_corner, cur_corner, status, err);
	goodFeaturesToTrack(_prevGray, prev_corner, 200, 0.01, 30);
	
	auto mid3_1 = std::chrono::system_clock::now();
	delta = std::chrono::duration_cast<std::chrono::milliseconds>(mid3_1 - mid3);
	LOG_D("live stabil.. good function Tick=%d", delta.count());
	
	calcOpticalFlowPyrLK(_prevGray, cur_grey, prev_corner, cur_corner, status, err);

	auto mid4 = std::chrono::system_clock::now();
	delta = std::chrono::duration_cast<std::chrono::milliseconds>(mid4 - mid3_1);
	LOG_D("live stabil.. calc function Tick=%d", delta.count());

	// weed out bad matches
	for (size_t i = 0; i < status.size(); i++) {
		if (status[i]) {
			prev_corner2.push_back(prev_corner[i]);
			cur_corner2.push_back(cur_corner[i]);
		}
	}

	// translation + rotation only
	//Mat T = estimateRigidTransform(prev_corner2, cur_corner2, false); // false = rigid transform, no scaling/shearing
	Mat T = estimateAffine2D(prev_corner2, cur_corner2); // false = rigid transform, no scaling/shearing		

	// in rare cases no transform is found. We'll just use the last known good transform.
	if (T.data == NULL) {
		last_T.copyTo(T);
	}

	T.copyTo(last_T);

	// decompose T
	double dx = T.at<double>(0, 2);
	double dy = T.at<double>(1, 2);
	double da = atan2(T.at<double>(1, 0), T.at<double>(0, 0));
	//
	//prev_to_cur_transform.push_back(TransformParam(dx, dy, da));

	auto mid5 = std::chrono::system_clock::now();
	delta = std::chrono::duration_cast<std::chrono::milliseconds>(mid5 - mid4);
	LOG_D("live stabil.. mid5 - mid4 Tick=%d", delta.count());

	//out_transform << k << " " << dx << " " << dy << " " << da << endl;
	//
	// Accumulated frame to frame transform
	x += dx;
	y += dy;
	a += da;
	//trajectory.push_back(Trajectory(x,y,a));
	//
	//out_trajectory << k << " " << x << " " << y << " " << a << endl;
	//
	z = Trajectory(x, y, a);
	//
	if (k == 1) {
		// intial guesses
		X = Trajectory(0, 0, 0); //Initial estimate,  set 0
		P = Trajectory(1, 1, 1); //set error variance,set 1
	}
	else
	{
		//time update��prediction��
		X_ = X; //X_(k) = X(k-1);
		P_ = P + Q; //P_(k) = P(k-1)+Q;
		// measurement update��correction��
		K = P_ / (P_ + R); //gain;K(k) = P_(k)/( P_(k)+R );
		X = X_ + K * (z - X_); //z-X_ is residual,X(k) = X_(k)+K(k)*(z(k)-X_(k)); 
		P = (Trajectory(1, 1, 1) - K) * P_; //P(k) = (1-K(k))*P_(k);
	}
	//smoothed_trajectory.push_back(X);
	//out_smoothed_trajectory << k << " " << X.x << " " << X.y << " " << X.a << endl;
	//-
	// target - current
	double diff_x = X.x - x;//
	double diff_y = X.y - y;
	double diff_a = X.a - a;

	dx = dx + diff_x;
	dy = dy + diff_y;
	da = da + diff_a;

	//new_prev_to_cur_transform.push_back(TransformParam(dx, dy, da));
	//
	//out_new_transform << k << " " << dx << " " << dy << " " << da << endl;
	//
	T.at<double>(0, 0) = cos(da);
	T.at<double>(0, 1) = -sin(da);
	T.at<double>(1, 0) = sin(da);
	T.at<double>(1, 1) = cos(da);

	T.at<double>(0, 2) = dx;
	T.at<double>(1, 2) = dy;

	//Mat cur2;

	warpAffine(_prevImage, cur2, T, cur.size());

	auto mid6 = std::chrono::system_clock::now();
	delta = std::chrono::duration_cast<std::chrono::milliseconds>(mid6 - mid5);
	LOG_D("live stabil.. mid6 - mid5 Tick=%d", delta.count());

	cur2 = cur2(Range(vert_border, cur2.rows - vert_border), Range(HORIZONTAL_BORDER_CROP, cur2.cols - HORIZONTAL_BORDER_CROP));

	// Resize cur2 back to cur size, for better side by side comparison
	resize(cur2, cur2, cur.size());

	// Now draw the original and stablised side by side for coolness
	//Mat canvas = Mat::zeros(cur.rows, cur.cols * 2 + 10, cur.type());

	//_prevImage.copyTo(canvas(Range::all(), Range(0, cur2.cols)));
	//cur2.copyTo(canvas(Range::all(), Range(cur2.cols + 10, cur2.cols * 2 + 10)));

	// If too big to fit on the screen, then scale it down by 2, hopefully it'll fit :)
	//if (canvas.cols > 1920) {
	//	resize(canvas, canvas, Size(1920, 540));
	//}
	// char filename[30];
	// sprintf(filename, "saved/%d_cur.png", k);
	// imwrite(filename, cur2);

	//outputVideo << canvas;
		
	_prevImage = cur.clone();//cur.copyTo(prev);
	cur_grey.copyTo(_prevGray);

	auto mid7 = std::chrono::system_clock::now();
	delta = std::chrono::duration_cast<std::chrono::milliseconds>(mid7 - mid6);
	LOG_D("live stabil.. mid7 - mid6 Tick=%d", delta.count());


	cv::cuda::GpuMat resImg;
	resImg.upload(cur2);

	auto end = std::chrono::system_clock::now();
	delta = std::chrono::duration_cast<std::chrono::milliseconds>(end - mid7);
	LOG_D("live stabil.. end upload Tick=%d", delta.count());

	//cout << "Frame: " << k << "/" << max_frames << " - good optical flow: " << prev_corner2.size() << endl;
	k++;
		
	return 1;
}


int LiveStabil::StabilImageCuda(uchar* image, int pitch, cv::cuda::GpuMat& img, cv::cuda::GpuMat& cur2)
{
	cuda::GpuMat cur_grey;

	FrCvClrConv::YUV2BGR(img, (uint8_t*)image, _imgWidth, _imgHeight, pitch);
	//cv::cuda::GpuMat img(_imgHeight, _imgWidth, CV_8UC1, image, _imgPitch);

	//img.download(cur);
	if (!_firstImg) {
		_prevImageCuda = img.clone();
		cuda::cvtColor(_prevImageCuda, _prevGrayCuda, COLOR_BGR2GRAY);

		_firstImg = true;

		// Step 1 - Get previous to current frame transformation (dx, dy, da) for all frames
		
		// Step 2 - Accumulate the transformations to get the image trajectory
		//
		// Step 3 - Smooth out the trajectory using an averaging window
		
		double pstd = 4e-3;//can be changed
		double cstd = 0.25;//can be changed
		Q.set(pstd, pstd, pstd);// process noise covariance
		R.set(cstd, cstd, cstd);// measurement noise covariance 
		//
		// Step 5 - Apply the new transformation to the video
		
		vert_border = HORIZONTAL_BORDER_CROP * _prevImageCuda.rows / _prevImageCuda.cols; // get the aspect ratio correct
		//int vert_border = HORIZONTAL_BORDER_CROP * _imgHeight / _imgWidth; // get the aspect ratio correct

		detector = cuda::createGoodFeaturesToTrackDetector(_prevGrayCuda.type(), 200, 0.01, 30);
		d_pyrLK_sparse = cuda::SparsePyrLKOpticalFlow::create(Size(21, 21), 3, 30);
		
		return 0;
	}
	cuda::cvtColor(img, cur_grey, COLOR_BGR2GRAY);


	// vector from prev to cur
	//vector <Point2f> prev_corner, cur_corner;
	cuda::GpuMat prev_corner, cur_corner;
	vector <Point2f> prev_corner2, cur_corner2;
	//vector <uchar> status;
	cuda::GpuMat status;
	vector <float> err;

	//goodFeaturesToTrack(prev_grey, prev_corner, 200, 0.01, 30);
	//calcOpticalFlowPyrLK(prev_grey, cur_grey, prev_corner, cur_corner, status, err);
	//cuda::GpuMat d_pts;
	//Ptr<cuda::CornersDetector> detector = cuda::createGoodFeaturesToTrackDetector(_prevGrayCuda.type(), 200, 0.01, 30);
	detector->detect(_prevGrayCuda, prev_corner);
	d_pyrLK_sparse->calc(_prevGrayCuda, cur_grey, prev_corner, cur_corner, status);
	
	
	// weed out bad matches
	/*for (size_t i = 0; i < status.size(); i++) {
		if (status[i]) {
			prev_corner2.push_back(prev_corner[i]);
			cur_corner2.push_back(cur_corner[i]);
		}
	}*/

	// translation + rotation only
	Mat prev_corner_c, cur_corner_c;
	prev_corner.download(prev_corner_c);
	cur_corner.download(cur_corner_c);

	//Mat T = estimateRigidTransform(prev_corner2, cur_corner2, false); // false = rigid transform, no scaling/shearing
	//Mat T = estimateAffine2D(prev_corner2, cur_corner2); // false = rigid transform, no scaling/shearing		
	Mat T = estimateAffine2D(prev_corner_c, cur_corner_c); // false = rigid transform, no scaling/shearing		

	// in rare cases no transform is found. We'll just use the last known good transform.
	if (T.data == NULL) {
		last_T.copyTo(T);
	}

	T.copyTo(last_T);

	// decompose T
	double dx = T.at<double>(0, 2);
	double dy = T.at<double>(1, 2);
	double da = atan2(T.at<double>(1, 0), T.at<double>(0, 0));
	//
	//prev_to_cur_transform.push_back(TransformParam(dx, dy, da));

	
	//out_transform << k << " " << dx << " " << dy << " " << da << endl;
	//
	// Accumulated frame to frame transform
	x += dx;
	y += dy;
	a += da;
	//trajectory.push_back(Trajectory(x,y,a));
	//
	//out_trajectory << k << " " << x << " " << y << " " << a << endl;
	//
	z = Trajectory(x, y, a);
	//
	if (k == 1) {
		// intial guesses
		X = Trajectory(0, 0, 0); //Initial estimate,  set 0
		P = Trajectory(1, 1, 1); //set error variance,set 1
	}
	else
	{
		//time update��prediction��
		X_ = X; //X_(k) = X(k-1);
		P_ = P + Q; //P_(k) = P(k-1)+Q;
		// measurement update��correction��
		K = P_ / (P_ + R); //gain;K(k) = P_(k)/( P_(k)+R );
		X = X_ + K * (z - X_); //z-X_ is residual,X(k) = X_(k)+K(k)*(z(k)-X_(k)); 
		P = (Trajectory(1, 1, 1) - K) * P_; //P(k) = (1-K(k))*P_(k);
	}
	//smoothed_trajectory.push_back(X);
	//out_smoothed_trajectory << k << " " << X.x << " " << X.y << " " << X.a << endl;
	//-
	// target - current
	double diff_x = X.x - x;//
	double diff_y = X.y - y;
	double diff_a = X.a - a;

	dx = dx + diff_x;
	dy = dy + diff_y;
	da = da + diff_a;

	//new_prev_to_cur_transform.push_back(TransformParam(dx, dy, da));
	//
	//out_new_transform << k << " " << dx << " " << dy << " " << da << endl;
	//
	T.at<double>(0, 0) = cos(da);
	T.at<double>(0, 1) = -sin(da);
	T.at<double>(1, 0) = sin(da);
	T.at<double>(1, 1) = cos(da);

	T.at<double>(0, 2) = dx;
	T.at<double>(1, 2) = dy;

	//Mat cur2;

	cuda::warpAffine(_prevImageCuda, cur2, T, img.size());

	
	cur2 = cur2(Range(vert_border, cur2.rows - vert_border), Range(HORIZONTAL_BORDER_CROP, cur2.cols - HORIZONTAL_BORDER_CROP));

	// Resize cur2 back to cur size, for better side by side comparison
	cuda::resize(cur2, cur2, img.size());

	// Now draw the original and stablised side by side for coolness
	//Mat canvas = Mat::zeros(cur.rows, cur.cols * 2 + 10, cur.type());

	//_prevImage.copyTo(canvas(Range::all(), Range(0, cur2.cols)));
	//cur2.copyTo(canvas(Range::all(), Range(cur2.cols + 10, cur2.cols * 2 + 10)));

	// If too big to fit on the screen, then scale it down by 2, hopefully it'll fit :)
	//if (canvas.cols > 1920) {
	//	resize(canvas, canvas, Size(1920, 540));
	//}
	// char filename[30];
	// sprintf(filename, "saved/%d_cur.png", k);
	// imwrite(filename, cur2);

	//outputVideo << canvas;

	_prevImageCuda = img.clone();//cur.copyTo(prev);
	cur_grey.copyTo(_prevGrayCuda);

	
	//cv::cuda::GpuMat resImg;
	//resImg.upload(cur2);

	
	//cout << "Frame: " << k << "/" << max_frames << " - good optical flow: " << prev_corner2.size() << endl;
	k++;

	return 1;
}
