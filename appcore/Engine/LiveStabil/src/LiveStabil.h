#pragma once

#include <opencv2/opencv.hpp>
#include <opencv2/cudaoptflow.hpp>
#include <iostream>
#include <cassert>
#include <cmath>
#include <fstream>
#include "DefData.hpp"
#include "FrCvClrConv.h"

class LiveStabil {
public:
	LiveStabil();
	LiveStabil(int h, int w);
	~LiveStabil();

	cv::Mat stabImg;

	cv::cuda::GpuMat stabImgCuda;

	int StabilImage(uchar* pImage, int pitch, cv::Mat& cur, cv::Mat& stabImg);
	int StabilImageCuda(uchar* pImage, int pitch, cv::cuda::GpuMat& src, cv::cuda::GpuMat& stabImg);

private:
	ofstream out_transform;
	ofstream out_trajectory;
	ofstream out_smoothed_trajectory;
	ofstream out_new_transform;

	int _imgHeight;
	int _imgWidth;
	//int _imgPitch;
	bool _firstImg;

	cv::Mat _prevGray;
	cv::Mat _prevImage;

	cv::cuda::GpuMat _prevGrayCuda;
	cv::cuda::GpuMat _prevImageCuda;

	cv::Mat last_T;


	Ptr<cuda::CornersDetector> detector;
	Ptr<cuda::SparsePyrLKOpticalFlow> d_pyrLK_sparse;
	
	
	// Accumulated frame to frame transform
	double a = 0;
	double x = 0;
	double y = 0;
	int k = 1;

	dove::Trajectory X;	//posteriori state estimate
	dove::Trajectory X_;	//priori estimate
	dove::Trajectory P;	// posteriori estimate error covariance
	dove::Trajectory P_;	// priori estimate error covariance
	dove::Trajectory K;	//gain
	
	//double pstd = 4e-3;	//can be changed
	//double cstd = 0.25;	//can be changed

	dove::Trajectory Q;	// process noise covariance
	dove::Trajectory R;	// measurement noise covariance 

	dove::Trajectory	z;	//actual measurement

	int vert_border; // get the aspect ratio correct
};
