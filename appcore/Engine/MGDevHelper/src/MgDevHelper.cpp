#include "mediafourcc.h"
#include "MgDevHelper.h"


#include <vector>
#include <opencv2/opencv.hpp>
#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/cudaimgproc.hpp"
#include "opencv2/cudawarping.hpp"
#include "opencv2/cudaarithm.hpp"

#if defined(_DEBUG)
#pragma comment(lib, "opencv_core440d.lib")
#pragma comment(lib, "opencv_highgui440d.lib")
#pragma comment(lib, "opencv_imgproc440d.lib")
#pragma comment(lib, "opencv_imgcodecs440d.lib")
#pragma comment(lib, "opencv_calib3d440d.lib")
#pragma comment(lib, "opencv_cudaimgproc440d.lib")
#pragma comment(lib, "opencv_cudawarping440d.lib")
#pragma comment(lib, "opencv_cudaarithm440d.lib")
#else
#pragma comment(lib, "opencv_core440.lib")
#pragma comment(lib, "opencv_highgui440.lib")
#pragma comment(lib, "opencv_imgproc440.lib")
#pragma comment(lib, "opencv_imgcodecs440.lib")
#pragma comment(lib, "opencv_calib3d440.lib")
#pragma comment(lib, "opencv_cudaimgproc440.lib")
#pragma comment(lib, "opencv_cudawarping440.lib")
#pragma comment(lib, "opencv_cudaarithm440.lib")
#endif


int FrMgHelper::FrMgHelperOpen(HelperParam& param) {
    int ret;
    if (param.codec == "H.264")
        m_EncInfo.FrVideo.dwFourCC = FOURCC_H264;
    else
        m_EncInfo.FrVideo.dwFourCC = FOURCC_H264;
    if (param.codecType == "SW")
        m_EncInfo.FrVideo.eCodecType = CODEC_SW;
    else
        m_EncInfo.FrVideo.eCodecType = CODEC_HW;

	m_EncInfo.FrVideo.dwWidth = param.dwWidth;
	m_EncInfo.FrVideo.dwHeight = param.dwHeight;
	m_EncInfo.FrVideo.dwBitRate = param.dwBitrate;
	m_EncInfo.FrVideo.dwFrameRate = param.dwFramerate * 1000;
    m_EncInfo.FrVideo.fIntraFrameRefresh = param.dwInterval;

    FrSocketInitNetwork();

	// open rtp sender..
	m_UrlInfo.pUrl = (char *)param.url.c_str();
	
    ret = HelperInit(m_UrlInfo, param.wSvrPort);
    if (ret < 0) {
        FrMgHelperClose();
        return -1;
    }

    HelperStart();

	return 1;
}

void FrMgHelper::FrMgHelperClose() {
    HelperStop();

    FrSocketCloseNetwork();
}

FrMgHelper::FrMgHelper() {
    memset(&m_UrlInfo, 0, sizeof(FrURLInfo));
    memset(&m_EncInfo, 0, sizeof(FrMediaInfo));
    memset(&m_SrcInfo, 0, sizeof(FrMediaInfo));

    FrSysInit();
    
}

FrMgHelper::~FrMgHelper() {

}

void FrMgHelper::HelperServerThread() {
    LRSLT lRet;
    DWORD32 dwTime;
    int diff;
    FrMediaStream VideoData;
    FrMediaStream EncStream;
    FrRawVideo  DecVideo;
    FrRawVideo  ScaleVideo;

    memset(&EncStream, 0, sizeof(FrMediaStream));
    memset(&VideoData, 0, sizeof(FrMediaStream));
    VideoData.tMediaType = VIDEO_MEDIA;
    EncStream.tMediaType = VIDEO_MEDIA;
    

    TRACE("HelperServerThread() Begin..");
    
    FrTimeStart(m_hTime);

    while (!m_bEnd) {
        lRet = FrReaderReadFrame(m_hReader, &VideoData);
        if (FRFAILED(lRet)) {
            if (lRet == COMMON_ERR_ENDOFDATA)
                break;

            FrSleep(10);
            continue;
        }
        
        /*dwTime = FrTimeGetTime(m_hTime);
        diff = dwTime - VideoData.dwCTS;
        if (diff >= 0) {
            if (diff > MAX_TIME_DIFF) {
                FrTimeSetRefTime(m_hTime, VideoData.dwCTS);
                TRACE("ArServerThread: McTimeSetRefTime=%d cur=%d\n", VideoData.dwCTS, dwTime);
            }
        }*/

        lRet = FrVideoDecDecode(m_hDecVideo, &VideoData, &DecVideo);
        if (FRFAILED(lRet)) {
            continue;
        }
        if (DecVideo.dwSrcCnt) {
            // callback..
            // // ..
            // ..
            // ..




            //std::vector<cv::cuda::GpuMat> vecGpuImage, vecGpuOutput(3);

            //int lumaWidth = m_SrcInfo.FrVideo.dwWidth;
            //int lumaHeight = m_SrcInfo.FrVideo.dwHeight;
            //int chromaWidth = lumaWidth >> 1;
            //int chromaHeight = lumaHeight >> 1;
            //int lumaPitch = DecVideo.dwPitch;
            //int chromaPitch = lumaPitch >> 1;
            //int dpYUVPitch = lumaPitch;

            //unsigned char* yDeviceptr = DecVideo.pY;
            //unsigned char* uDeviceptr = yDeviceptr + lumaPitch * lumaHeight;
            //unsigned char* vDeviceptr = uDeviceptr + chromaPitch * chromaHeight;

            //cv::cuda::GpuMat gpuY(lumaHeight, lumaWidth, CV_8UC1, yDeviceptr, lumaPitch);
            //cv::cuda::GpuMat gpuU(chromaHeight, chromaWidth, CV_8UC1, uDeviceptr, chromaPitch);
            //cv::cuda::GpuMat gpuV(chromaHeight, chromaWidth, CV_8UC1, vDeviceptr, chromaPitch);

            //vecGpuImage.push_back(gpuY);
            //vecGpuImage.push_back(gpuU);
            //vecGpuImage.push_back(gpuV);

            //vecGpuImage[0].copyTo(vecGpuOutput[0]);
            //vecGpuImage[1].copyTo(vecGpuOutput[1]);
            //vecGpuImage[2].copyTo(vecGpuOutput[2]);

            //cv::cuda::resize(vecGpuOutput[1], vecGpuOutput[1], cv::Size(vecGpuOutput[1].cols * 2, vecGpuOutput[1].rows * 2), cv::InterpolationFlags::INTER_CUBIC);
            //cv::cuda::resize(vecGpuOutput[2], vecGpuOutput[2], cv::Size(vecGpuOutput[2].cols * 2, vecGpuOutput[2].rows * 2), cv::InterpolationFlags::INTER_CUBIC);

            //cv::cuda::GpuMat gResult;
            //cv::cuda::merge(vecGpuOutput, gResult);
            //cv::cuda::cvtColor(gResult, gResult, cv::ColorConversionCodes::COLOR_YUV2RGB);
            //cv::Mat mResult;
            //gResult.download(mResult);

            //try
            //{
            //    cv::imwrite("c:\\cv_out.bmp", mResult);
            //}
            //catch (cv::Exception& e)
            //{
            //    std::string err_msg = e.what();
            //    std::cout << "StillImageEncoder::StillImageSaveYUV420toBGR : " << err_msg << std::endl;
            //}
        }

        //FrSleep(10);
        // test..
        if (m_SrcInfo.dwTotalRange > 0) {
            FrSleep(30);
        }
    }

    TRACE("HelperServerThread() End..");

    m_hHelperThread = nullptr;
}

int FrMgHelper::HelperStart() {
    DWORD32 dwStart = 0;
    LRSLT lRet;

    if (m_SrcInfo.dwVideoTotal) {
        m_hHelperThread = new std::thread(&FrMgHelper::HelperServerThread, this);
    }
        
    lRet = FrReaderStart(m_hReader, &dwStart);
    if (FRFAILED(lRet)) {
        return -1;
    }

    return 1;
}

int FrMgHelper::HelperStop() {
    LOG_I("HelperStop Begin..");

    m_bEnd = TRUE;
    
    while (m_hHelperThread != nullptr) {
        FrSleep(10);
    }

    /*if (m_hWriter != nullptr) {
        FrWriterUpdateInfo(m_hWriter);
        FrWriterClose(m_hWriter);
    }
     
    if (m_hEncVideo != nullptr) {
        FrVideoEncClose(m_hEncVideo);
        m_hEncVideo = nullptr;
    }
    
    if (m_hScaler != nullptr) {
        FrVideoResizeClose(m_hScaler);
        m_hScaler = nullptr;
    }*/
    
    if (m_hDecVideo != nullptr) {
        FrVideoDecClose(m_hDecVideo);
        m_hDecVideo = nullptr;
    }
    
    if (m_hReader != nullptr) {
        FrReaderClose(m_hReader);
        m_hReader = nullptr;
    }
    
    if (m_hTime != nullptr) {
        FrTimeStop(m_hTime);
        FrTimeClose(m_hTime);
        m_hTime = nullptr;
    }

    LOG_I("HelperStop End..");

    return 1;
}

int FrMgHelper::HelperInit(FrURLInfo& URLInfo, short listenPort) {
    LOG_I("SvrInit Begin..");

    m_bEnd = FALSE;
    
    m_hTime = FrTimeOpen(0, 1.0);

    // reader
	LRSLT lRet = FrReaderOpen(&m_hReader, &URLInfo);
	if (FRFAILED(lRet)) {
		return -1;
	}

    lRet = FrReaderGetInfo(m_hReader, &m_SrcInfo);
    if (FRFAILED(lRet)) {
        return -1;
    }

    // decoder
    FrVideoInfo* pVideoInfo = &m_SrcInfo.FrVideo;
    pVideoInfo->eColorFormat = VideoFmtYUV420P;     // output color format
    pVideoInfo->eCodecType = m_EncInfo.FrVideo.eCodecType; 
    pVideoInfo->bLowDelayDecoding = TRUE;
    lRet = FrVideoDecOpen(&m_hDecVideo, NULL, pVideoInfo);
    if (FRFAILED(lRet)) {
        return -1;
    }


    //// converter(scale, color, fps)
    //lRet = FrVideoResizeOpen(&m_hScaler, pVideoInfo, &m_EncInfo.FrVideo);
    //if (FRFAILED(lRet)) {
    //    return -1;
    //}

    //// encoder
    //m_EncInfo.dwVideoTotal = 1;
    //lRet = FrVideoEncOpen(&m_hEncVideo, &m_EncInfo.FrVideo);
    //if (FRFAILED(lRet)) {
    //    return -1;
    //}

    //// writer
    //FrURLInfo	DstUrl = {};
    //DstUrl.pUrl = (CHAR*)"c:\\arserver_write_test.mp4";
    //DstUrl.URLType = _MP4_FILE;
    //lRet = FrWriterOpen(&m_hWriter, &DstUrl);
    //if (FRFAILED(lRet)) {
    //    return -1;
    //}

    //lRet = FrWriterSetInfo(m_hWriter, &m_EncInfo, 0);
    //if (FRFAILED(lRet)) {
    //    return -1;
    //}


   
    LOG_I("SvrInit End..");

	return 1;
}