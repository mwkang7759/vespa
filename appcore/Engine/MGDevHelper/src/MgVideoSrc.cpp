#include "MgVideoSrc.h"
#include "MgVideoSrcAPI.h"


 void TaskMgVideo(void* lpVoid) {
	FrMgVideoStruct* hMgVideo = (FrMgVideoStruct*)lpVoid;

    LRSLT lRet;
    DWORD32 dwTime;
    int iDiff;
    FrMediaStream VideoData;
    FrMediaStream EncStream;
    FrRawVideo  DecVideo;
    FrRawVideo  ConvVideo;
    FrRawVideo*  pCbVideo;
    BOOL bFirstDecoded = FALSE;

    memset(&EncStream, 0, sizeof(FrMediaStream));
    memset(&VideoData, 0, sizeof(FrMediaStream));
    VideoData.tMediaType = VIDEO_MEDIA;
    EncStream.tMediaType = VIDEO_MEDIA;

    hMgVideo->m_bVideoDecode = FALSE;
    hMgVideo->m_bVideoRead = FALSE;
    hMgVideo->m_bVideoRender = FALSE;


    TRACE("TaskMgVideo() Begin..");

    //FrTimeStart(hMgVideo->m_hTime);

    while (!hMgVideo->m_bEnd) {
        switch (hMgVideo->m_iStatus) {
        case PLAYING:
            TRACE("TaskMgVideo() PLAYING bVideoRender(%d), bVideoDecoded(%d), bVideoRead(%d)", hMgVideo->m_bVideoRender, hMgVideo->m_bVideoDecode, hMgVideo->m_bVideoRead);
            if (hMgVideo->m_bVideoRender) {
                if (hMgVideo->m_bSync) {
                    dwTime = FrTimeGetTime(hMgVideo->m_hTime);
                    iDiff = dwTime - DecVideo.dwCTS;

                    if (iDiff >= AV_SYNC_LOWER_LIMIT && iDiff <= AV_SYNC_UPPER_LIMIT) {
                        CbData cbData;
                        if (hMgVideo->m_SrcInfo.FrVideo.eCodecType == CODEC_HW)
                            cbData.codec_type = CODEC_NVIDIA;
                        else
                            cbData.codec_type = CODEC_FFMPEG;
                        cbData.pData = DecVideo.pY;
                        cbData.width = hMgVideo->m_SrcInfo.FrVideo.dwWidth;
                        cbData.height = hMgVideo->m_SrcInfo.FrVideo.dwHeight;
                        cbData.pitch = DecVideo.dwPitch;

                        cbData.pY = DecVideo.pY;
                        cbData.pU = DecVideo.pU;
                        cbData.pV = DecVideo.pV;
                        hMgVideo->m_fVideoCallback(hMgVideo->m_phCallBackHandle, &cbData);

                        hMgVideo->m_bVideoRender = FALSE;
                        hMgVideo->m_bVideoDecode = FALSE;
                        TRACE("TaskMgVideo	Video Rendering... CTS=%d CurTime=%d, diff = %d", DecVideo.dwCTS, dwTime, iDiff);
                    }
                    else if (iDiff >= AV_SYNC_UPPER_LIMIT && iDiff <= AV_SYNC_HIGH_LIMIT) {
                        TRACE("TaskMgVideo	Video doesn't display.. CurTime=%d VideoCTS=%d Diff=%d", dwTime, DecVideo.dwCTS, iDiff);
                        hMgVideo->m_bVideoRender = FALSE;
                        hMgVideo->m_bVideoDecode = FALSE;
                    }
                    else if (iDiff > AV_SYNC_HIGH_LIMIT) {	// The video frame is late. It's skipped.
                        LOG_D("TaskMgVideo Video Skipped to IVOP... CurTime=%d VideoCTS=%d Diff=%d", dwTime, DecVideo.dwCTS, iDiff);
                        hMgVideo->m_bVideoRender = FALSE;
                        hMgVideo->m_bVideoDecode = FALSE;
                    }
                    else {
                        TRACE("TaskMgVideo	VideoCTS=%d CTS=%d Diff=%d", DecVideo.dwCTS, dwTime, iDiff);
                        //McSleep(ABS(iDiff));
                        FrSleep(10);
                        break;
                    }
                }
                else {
                    //LOG_D("TaskMgVideo No Sync Video Callback... VideoCTS=%d Decoded Num=%d", DecVideo.dwCTS, DecVideo.nDecoded);
                    for (int i = 0; i < DecVideo.nDecoded; i++) {
                        BOOL bSuccess = TRUE;
                        CbData cbData;

                        if (hMgVideo->m_bScale) {
                            // need to resize
                            if (hMgVideo->m_SrcInfo.FrVideo.eCodecType == CODEC_HW) {
                                DecVideo.pY = DecVideo.ppDecoded[i];
                            }
                            
                            if (FrVideoResizeVideo(hMgVideo->m_hScaler, &DecVideo, &ConvVideo) < 0) {
                                bSuccess = FALSE;
                            }
                            pCbVideo = &ConvVideo;
                            cbData.pData = pCbVideo->pY;
                            cbData.width = hMgVideo->m_EncInfo.FrVideo.dwWidth;
                            cbData.height = hMgVideo->m_EncInfo.FrVideo.dwHeight;
                        }
                        else {
                            pCbVideo = &DecVideo;
                            if (hMgVideo->m_SrcInfo.FrVideo.eCodecType == CODEC_HW) {
                                cbData.pData = pCbVideo->ppDecoded[i];
                            }
                            
                            cbData.width = hMgVideo->m_SrcInfo.FrVideo.dwWidth;
                            cbData.height = hMgVideo->m_SrcInfo.FrVideo.dwHeight;
                        }
                        
                        if (hMgVideo->m_SrcInfo.FrVideo.eCodecType == CODEC_HW)
                            cbData.codec_type = CODEC_NVIDIA;
                        else
                            cbData.codec_type = CODEC_FFMPEG;
                        //cbData.pData = pCbVideo->ppDecoded[i];
                        //cbData.width = hMgVideo->m_SrcInfo.FrVideo.dwWidth;
                        //cbData.height = hMgVideo->m_SrcInfo.FrVideo.dwHeight;
                        cbData.pitch = pCbVideo->dwPitch;

                        cbData.pY = pCbVideo->pY;
                        cbData.pU = pCbVideo->pU;
                        cbData.pV = pCbVideo->pV;


                        /*CbData cbData;
                        if (hMgVideo->m_SrcInfo.FrVideo.eCodecType == CODEC_HW)
                            cbData.codec_type = CODEC_NVIDIA;
                        else
                            cbData.codec_type = CODEC_FFMPEG;
                        cbData.pData = DecVideo.ppDecoded[i];
                        cbData.width = hMgVideo->m_SrcInfo.FrVideo.dwWidth;
                        cbData.height = hMgVideo->m_SrcInfo.FrVideo.dwHeight;
                        cbData.pitch = DecVideo.dwPitch;

                        cbData.pY = DecVideo.pY;
                        cbData.pU = DecVideo.pU;
                        cbData.pV = DecVideo.pV;*/
                        
                        //LOG_D("TaskMgVideo No Sync Video Callback... codec_type=%d pitch=%d, height(%d)", cbData.codec_type, cbData.pitch, cbData.height);
                        if (bSuccess)
                            hMgVideo->m_fVideoCallback(hMgVideo->m_phCallBackHandle, &cbData);
                        //LOG_D("TaskMgVideo No Sync Video Callback... VideoCTS=%d Decoded Num=%d, curNum(%d)", DecVideo.dwCTS, DecVideo.nDecoded, i);
                        /*if (DecVideo.nDecoded > 1)
                            FrSleep(10);*/
                    }
                    hMgVideo->m_bVideoRender = FALSE;
                    hMgVideo->m_bVideoDecode = FALSE;
                }
            }

            if (hMgVideo->m_bVideoRead && !hMgVideo->m_bVideoDecode) {
                lRet = FrVideoDecDecode(hMgVideo->m_hDecVideo, &VideoData, &DecVideo);
                if (FRFAILED(lRet)) {
                    //continue;
                }
                else {
                    if (!bFirstDecoded) {
                        FrTimeStart(hMgVideo->m_hTime);
                        bFirstDecoded = TRUE;
                        LOG_D("TaskMgVideo Fist Video Decoded... VideoCTS=%d", DecVideo.dwCTS);
                    }
                    hMgVideo->m_bVideoDecode = TRUE;
                    hMgVideo->m_bVideoRender = TRUE;
                }
                
                hMgVideo->m_bVideoRead = FALSE;
            }

            if (!hMgVideo->m_bVideoRead) {
                lRet = FrReaderReadFrame(hMgVideo->m_hReader, &VideoData);
                if (FRFAILED(lRet)) {
                    if (lRet == COMMON_ERR_ENDOFDATA) {
                        LOG_D("TaskMgVideo EndOfData.. VideoCTS=%d", VideoData.dwCTS);
                        break;
                    }

                    FrSleep(10);        // assume to wait data..
                    //continue;
                }
                else {
                    hMgVideo->m_bVideoRead = TRUE;
                }
            }

            break;
        case STOP:
            break;
        case PAUSE:
            break;
        case STEP:
            break;
        default:
            break;
        }






        //lRet = FrReaderReadFrame(hMgVideo->m_hReader, &VideoData);
        //if (FRFAILED(lRet)) {
        //    if (lRet == COMMON_ERR_ENDOFDATA)
        //        break;

        //    FrSleep(10);
        //    continue;
        //}

        //lRet = FrVideoDecDecode(hMgVideo->m_hDecVideo, &VideoData, &DecVideo);
        //if (FRFAILED(lRet)) {
        //    continue;
        //}
        //if (DecVideo.dwSrcCnt) {
        //    CbData cbData;
        //    if (hMgVideo->m_SrcInfo.FrVideo.eCodecType == CODEC_HW)
        //        cbData.codec_type = CODEC_NVIDIA;
        //    else
        //        cbData.codec_type = CODEC_FFMPEG;
        //    cbData.pData = DecVideo.pY;
        //    cbData.width = hMgVideo->m_SrcInfo.FrVideo.dwWidth;
        //    cbData.height = hMgVideo->m_SrcInfo.FrVideo.dwHeight;
        //    cbData.pitch = DecVideo.dwPitch;
        //    
        //    cbData.pY = DecVideo.pY;
        //    cbData.pU = DecVideo.pU;
        //    cbData.pV = DecVideo.pV;
        //    hMgVideo->m_fVideoCallback(hMgVideo->m_phCallBackHandle, &cbData);  
        //}

        //if (hMgVideo->m_SrcInfo.dwTotalRange > 0) {
        //    FrSleep(30);
        //}
    }

	McCloseTask(hMgVideo->m_hTask);
	hMgVideo->m_hTask = INVALID_TASK;

	TRACE("TaskMgVideo end..");

	McExitTask();
}

 int MgVideoChangeStatus(FrMgVideoHandle hHandle, int iStatus) {
     FrMgVideoStruct* hMgVideo = (FrMgVideoStruct*)hHandle;

	 LRSLT	lRet;
	 BOOL	bRet = FALSE;

	 FrEnterMutex(&hMgVideo->m_xStatus);
	 switch (iStatus)
	 {
	 case STOP:
         if (hMgVideo->m_iStatus != STOP) {
			 //McReaderSetStatus(hPlayer->m_hReader, READ_STOP, 0);
             hMgVideo->m_iStatus = STOP;
			 bRet = TRUE;
			 TRACE("MgVideoChangeStatus	Status is STOP!!!!");
		 }
		 break;
	 case READY:
		 if (hMgVideo->m_iStatus == STOP) {
             hMgVideo->m_iStatus = READY;
			 bRet = TRUE;
			 TRACE("MgVideoChangeStatus	Status is READY!!!!");
		 }
		 break;
	 case BUFFERING:
        break;
	 case PLAYING:
		 if (hMgVideo->m_iStatus == PLAYING || hMgVideo->m_iStatus == READY) {
			 //McReaderSetStatus(hPlayer->m_hReader, READ_PLAY, 0);
			 FrTimeStart(hMgVideo->m_hTime);
             hMgVideo->m_iStatus = PLAYING;
			 bRet = TRUE;
			 TRACE("MgVideoChangeStatus	Status is PLAYING!!!!");
		 }
		 break;
	 case STEP:
		 if (hMgVideo->m_iStatus == PAUSE) {
			 //McReaderSetStatus(hPlayer->m_hReader, READ_STEP, 0);
             hMgVideo->m_iStatus = STEP;
			 bRet = TRUE;
			 TRACE("MgVideoChangeStatus	Status is STEP!!!!");
		 }
		 break;
     case PAUSE:
         if (hMgVideo->m_iStatus == PLAYING) {
             //McReaderSetStatus(hPlayer->m_hReader, READ_STEP, 0);
             hMgVideo->m_iStatus = PAUSE;
             bRet = TRUE;
             TRACE("MgVideoChangeStatus	Status is PAUSE!!!!");
         }
         break;
	 }
	 FrLeaveMutex(&hMgVideo->m_xStatus);

	 return	bRet;
 }

FrMgVideoHandle FrMgVideoOpen(MgVideoParam* pParam) {
	FrMgVideoStruct* hMgVideo = (FrMgVideoStruct*)MALLOCZ(sizeof(FrMgVideoStruct));
	if (hMgVideo) {
		//FrSetTraceFileName((char*)"mg_video.log");
        SetTraceRollingFileName((char*)"mg_video.log", 20 * 1024 * 1024, 2);

        hMgVideo->m_hTask = INVALID_TASK;

        LOG_I("FrMgVideoOpen Begin..");

		FrSysInit();
		FrSocketInitNetwork();
        FrCreateMutex(&hMgVideo->m_xStatus);

        hMgVideo->m_UrlInfo.pUrl = (char*)pParam->pUrl;
        if (pParam->codec_type == CODEC_NVIDIA)
            hMgVideo->m_SrcInfo.FrVideo.eCodecType = CODEC_HW;
        else
            hMgVideo->m_SrcInfo.FrVideo.eCodecType = CODEC_SW;
        hMgVideo->m_EncInfo.FrVideo.dwWidth = pParam->newWidth;
        hMgVideo->m_EncInfo.FrVideo.dwHeight = pParam->newHeight;

        MgVideoChangeStatus(hMgVideo, STOP);

        LOG_I("FrMgVideoOpen End, codec type(%d), New W(%d), H(%d)", hMgVideo->m_SrcInfo.FrVideo.eCodecType, pParam->newWidth, pParam->newHeight);
	}

	return hMgVideo;
}

void FrMgVideoClose(FrMgVideoHandle hHandle) {
    FrMgVideoStruct* hMgVideo = (FrMgVideoStruct*)hHandle;
	if (hMgVideo) {

		FrSocketCloseNetwork();
        FrDeleteMutex(&hMgVideo->m_xStatus);

		FREE(hMgVideo);
	}
}

int FrMgVideoStart(FrMgVideoHandle hHandle) {
    FrMgVideoStruct* hMgVideo = (FrMgVideoStruct*)hHandle;

	if (hMgVideo) {
        LOG_I("FrMgVideoStart Begin..");

        hMgVideo->m_bEnd = FALSE;
        hMgVideo->m_bSync = FALSE;

        hMgVideo->m_hTime = FrTimeOpen(0, 1.0);

        // reader
        LRSLT lRet = FrReaderOpen(&hMgVideo->m_hReader, &hMgVideo->m_UrlInfo);
        if (FRFAILED(lRet)) {
            return -1;
        }

        lRet = FrReaderGetInfo(hMgVideo->m_hReader, &hMgVideo->m_SrcInfo);
        if (FRFAILED(lRet)) {
            return -1;
        }

        MgVideoChangeStatus(hMgVideo, READY);

        // decoder
        FrVideoInfo* pSrcVideoInfo = &hMgVideo->m_SrcInfo.FrVideo;
        pSrcVideoInfo->eColorFormat = VideoFmtYUV420P;     // output color format
        //pVideoInfo->eCodecType = CODEC_SW;
        pSrcVideoInfo->bLowDelayDecoding = TRUE;
        pSrcVideoInfo->memDir = MEM_GPU;   // or MEM_GPU
        lRet = FrVideoDecOpen(&hMgVideo->m_hDecVideo, NULL, pSrcVideoInfo);
        if (FRFAILED(lRet)) {
            return -1;
        }

        // converter(scale, color, fps)
        FrVideoInfo* pDstVideoInfo = &hMgVideo->m_EncInfo.FrVideo;
        if (pSrcVideoInfo->dwWidth != pDstVideoInfo->dwWidth || pSrcVideoInfo->dwHeight != pDstVideoInfo->dwHeight) {
            lRet = FrVideoResizeOpen(&hMgVideo->m_hScaler, pSrcVideoInfo, pDstVideoInfo);
            if (FRFAILED(lRet)) {
                return -1;
            }
            hMgVideo->m_bScale = TRUE;
        }
        
        DWORD32 dwStart = 0;

        MgVideoChangeStatus(hMgVideo, PLAYING);
        if (hMgVideo->m_SrcInfo.dwVideoTotal) {
			hMgVideo->m_hTask = McCreateTask((char*)"MgVideoTask", (PTASK_START_ROUTINE)TaskMgVideo, hMgVideo, THREAD_PRIORITY_HIGHEST, NULL, 0);
        }

        lRet = FrReaderStart(hMgVideo->m_hReader, &dwStart);
        if (FRFAILED(lRet)) {
            return -1;
        }

        LOG_I("FrMgVideoStart End..");
	}

	return 1;
}

int FrMgVideoStop(FrMgVideoHandle hHandle) {
    FrMgVideoStruct* hMgVideo = (FrMgVideoStruct*)hHandle;
	if (hMgVideo) {

		LOG_I("FrMgVideoStop Begin..");

        hMgVideo->m_bEnd = TRUE;
        MgVideoChangeStatus(hMgVideo, STOP);
		
		while (hMgVideo->m_hTask != INVALID_TASK) {
			FrSleep(10);
		}

		if (hMgVideo->m_hDecVideo != nullptr) {
			FrVideoDecClose(hMgVideo->m_hDecVideo);
			hMgVideo->m_hDecVideo = nullptr;
		}
        if (hMgVideo->m_hScaler != nullptr) {
            FrVideoResizeClose(hMgVideo->m_hScaler);
            hMgVideo->m_hScaler = nullptr;
        }
		
		if (hMgVideo->m_hReader != nullptr) {
			FrReaderClose(hMgVideo->m_hReader);
			hMgVideo->m_hReader = nullptr;
		}
		
		if (hMgVideo->m_hTime != nullptr) {
			FrTimeStop(hMgVideo->m_hTime);
			FrTimeClose(hMgVideo->m_hTime);
			hMgVideo->m_hTime = nullptr;
		}

		LOG_I("FrMgVideoStop End..");

	}

	return 1;
}


void FrMgVideoRegCallback(FrMgVideoHandle hHandle, void* fVideoCallback, void* phCallBackHandle) {
    FrMgVideoStruct* hMgVideo = (FrMgVideoStruct*)hHandle;
    if (!hMgVideo)
        return;

    hMgVideo->m_fVideoCallback = (PCALLBACK_ROUTINE_EX2)fVideoCallback;
    hMgVideo->m_phCallBackHandle = phCallBackHandle;
}


int FrMgVideoPause(FrMgVideoHandle hHandle) {
    FrMgVideoStruct* hMgVideo = (FrMgVideoStruct*)hHandle;
    if (hMgVideo) {
    }

    return 1;
}

int FrMgVideoSetp(FrMgVideoHandle hHandle) {
    FrMgVideoStruct* hMgVideo = (FrMgVideoStruct*)hHandle;
    if (hMgVideo) {
    }

    return 1;
}
