
#include "FdrPlayer.h"
#include "Player.h"

FdrPlayer::FdrPlayer() {
	m_hPlayer = new FdrPlayer::Player();

	mImplPlayer = std::make_unique<Player>();
}

FdrPlayer::~FdrPlayer() {
	delete m_hPlayer;
}

int FdrPlayer::Open(Param& param) {
	_param.hWnd = param.hWnd;

	m_hPlayer->Open();

	// on test..
	//mImplPlayer->Open();
	
	return 1;
}

int FdrPlayer::Close() {
	m_hPlayer->Close();
	
	return 1;
}

int FdrPlayer::Start() {
	PLAYER_PARAM param;
	param.codecType = CODEC_FFMPEG;
	param.url = "c:\\test_contents\\test.mp4";
	param.wnd = _param.hWnd;
	
	m_hPlayer->Prepare(param);
	m_hPlayer->Play();
	
	return 1;
}

int FdrPlayer::Stop() {
	m_hPlayer->Stop();

	return 1;
}