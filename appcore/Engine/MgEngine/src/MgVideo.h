#pragma once

#include "SystemAPI.h"
#include "TraceAPI.h"
#include "SocketAPI.h"
#include "StreamReaderAPI.h"
#include "StreamWriterAPI.h"
#include "DecoderAPI.h"
#include "EncoderAPI.h"
#include "ConverterAPI.h"


#define		MAX_TIME_DIFF				2000

#define		AV_SYNC_LOWER_LIMIT		0
#define		AV_SYNC_UPPER_LIMIT		(AV_SYNC_LOWER_LIMIT + 200)
#define		AV_SYNC_HIGH_LIMIT		(AV_SYNC_LOWER_LIMIT + 600)

typedef enum {
	STOP,
	READY,
	BUFFERING,
	PAUSE,
	PLAYING,
	STEP,
} MGVIDEO_STATUS;

class MgVideo {
public:
	MgVideo();
	~MgVideo();
private:
	BOOL	m_bEnd;
	FrReaderHandle	m_hReader;
	FrWriterHandle	m_hWriter;
	FrVideoDecHandle m_hDecVideo;
	FrVideoEncHandle m_hEncVideo;
	FrVideoResizeHandle	m_hScaler;
	FrVideoConvHandle	m_hConv;

	FrURLInfo	m_UrlInfo;
	FrMediaInfo m_SrcInfo;
	FrMediaInfo m_EncInfo;

	FrMediaStream m_VideoStream;
	FrRawVideo m_RawVideo;
	TASK_HANDLE m_hTask; // or
	FrTimeHandle		m_hTime;

	MUTEX_HANDLE		m_xStatus;
	int					m_iStatus;

	BOOL				m_bVideoRead;			// read video
	BOOL				m_bVideoDecode;			// decode video
	BOOL				m_bVideoRender;			// render video 
	BOOL				m_bSync;
	BOOL				m_bScale;
};

void TaskMgVideo(void* lpVoid);


