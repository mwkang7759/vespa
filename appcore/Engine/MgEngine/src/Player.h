#pragma once

#include "SystemAPI.h"
#include "mediainfo.h"
#include "DecoderAPI.h"
#include "StreamReaderAPI.h"
#include "TraceAPI.h"
#include "SocketAPI.h"
#include "DeviceOutAPI.h"
#include "FdrPlayer.h"

#include <string>
#include <thread>
#include <mutex>


#define		MAX_TIME_DIFF				2000

#define		AV_SYNC_LOWER_LIMIT		0
#define		AV_SYNC_UPPER_LIMIT		(AV_SYNC_LOWER_LIMIT + 200)
#define		AV_SYNC_HIGH_LIMIT		(AV_SYNC_LOWER_LIMIT + 600)


typedef enum {
	CODEC_NVIDIA,
	CODEC_FFMPEG
};

typedef enum {
	STOP,
	READY,
	BUFFERING,
	PAUSE,
	PLAYING,
	STEP,
} PLAYER_STATUS;

typedef struct {
	std::string url;
	int codecType;
	HANDLE	wnd;

} PLAYER_PARAM;

class FdrPlayer::Player {
public:
	Player();
	~Player();

	int Open();
	int Close();

	int Play();
	int Stop();
	int Prepare(PLAYER_PARAM& param);	// get stream information

	int ChangeStatus(int status);

	// set callback..
	// ..


private:
	BOOL	m_bEnd;
	FrReaderHandle	m_hReader;
	
	FrVideoDecHandle m_hDecVideo;
	McVideoOutHandle m_hDevOut;
	

	FrURLInfo	m_UrlInfo;
	FrMediaInfo m_SrcInfo;
	FrMediaInfo m_EncInfo;

	FrMediaStream m_VideoStream;
	FrRawVideo m_RawVideo;
	
	FrTimeHandle		m_hTime;

	
	int					m_iStatus;

	BOOL				m_bVideoRead;			// read video
	BOOL				m_bVideoDecode;			// decode video
	BOOL				m_bVideoRender;			// render video 
	BOOL				m_bSync;
	BOOL				m_bScale;

	std::mutex	m_xStatus;
	std::thread* m_hTask;
	void* TaskPlayback(void* arg);
};