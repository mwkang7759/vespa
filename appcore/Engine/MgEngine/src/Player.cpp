#include "Player.h"
#include "FrUtil.h"

FdrPlayer::Player::Player() {

}

FdrPlayer::Player::~Player() {

}

BOOL FdrPlayer::Player::ChangeStatus(int status) {
    LRSLT	lRet;
    BOOL	bRet = FALSE;

    m_xStatus.lock();
    switch (status)
    {
    case STOP:
        if (m_iStatus != STOP) {
            //McReaderSetStatus(hPlayer->m_hReader, READ_STOP, 0);
            m_iStatus = STOP;
            bRet = TRUE;
            TRACE("MgVideoChangeStatus	Status is STOP!!!!");
        }
        break;
    case READY:
        if (m_iStatus == STOP) {
            m_iStatus = READY;
            bRet = TRUE;
            TRACE("MgVideoChangeStatus	Status is READY!!!!");
        }
        break;
    case BUFFERING:
        break;
    case PLAYING:
        if (m_iStatus == PLAYING || m_iStatus == READY) {
            //McReaderSetStatus(hPlayer->m_hReader, READ_PLAY, 0);
            FrTimeStart(m_hTime);
            m_iStatus = PLAYING;
            bRet = TRUE;
            TRACE("MgVideoChangeStatus	Status is PLAYING!!!!");
        }
        break;
    case STEP:
        if (m_iStatus == PAUSE) {
            //McReaderSetStatus(hPlayer->m_hReader, READ_STEP, 0);
            m_iStatus = STEP;
            bRet = TRUE;
            TRACE("MgVideoChangeStatus	Status is STEP!!!!");
        }
        break;
    case PAUSE:
        if (m_iStatus == PLAYING) {
            //McReaderSetStatus(hPlayer->m_hReader, READ_STEP, 0);
            m_iStatus = PAUSE;
            bRet = TRUE;
            TRACE("MgVideoChangeStatus	Status is PAUSE!!!!");
        }
        break;
    }
    m_xStatus.unlock();

    return	bRet;
}

int FdrPlayer::Player::Open() {
    
    SetTraceRollingFileName((char*)"mg_player.log", 20 * 1024 * 1024, 2);

    m_hTask = nullptr;

    LOG_I("FrMgVideoOpen Begin..");

    FrSysInit();
    FrSocketInitNetwork();
    //FrCreateMutex(&hMgVideo->m_xStatus);

    ChangeStatus(STOP);

    LOG_I("Open() End..");
	return 1;
}

int FdrPlayer::Player::Close() {
    FrSocketCloseNetwork();
    FrSysClose();
    
	return 1;
}

int FdrPlayer::Player::Play() {
    LRSLT lRet;
    DWORD32 dwStart = 0;

    ChangeStatus(PLAYING);

    
    m_hTask = new std::thread(&Player::TaskPlayback, this, this);

    /*if (m_SrcInfo.dwVideoTotal) {
        hMgVideo->m_hTask = McCreateTask((char*)"MgVideoTask", (PTASK_START_ROUTINE)TaskMgVideo, hMgVideo, THREAD_PRIORITY_HIGHEST, NULL, 0);
    }*/

    lRet = FrReaderStart(m_hReader, &dwStart);
    if (FRFAILED(lRet)) {
        return -1;
    }

	return 1;
}

int FdrPlayer::Player::Stop() {
    LOG_I("Stop Begin..");

    m_bEnd = TRUE;
    ChangeStatus(STOP);

    while (m_hTask != nullptr) {
        FrSleep(10);
    }

    if (m_hDecVideo != nullptr) {
        FrVideoDecClose(m_hDecVideo);
        m_hDecVideo = nullptr;
    }
    
    if (m_hReader != nullptr) {
        FrReaderClose(m_hReader);
        m_hReader = nullptr;
    }

    if (m_hDevOut != nullptr) {
        McVideoOutClose(m_hDevOut);
        m_hDevOut = nullptr;
    }

    if (m_hTime != nullptr) {
        FrTimeStop(m_hTime);
        FrTimeClose(m_hTime);
        m_hTime = nullptr;
    }

    LOG_I("Stop End..");

	return 1;
}

int FdrPlayer::Player::Prepare(PLAYER_PARAM& param) {
    m_UrlInfo.pUrl = (char*)param.url.c_str();
    if (param.codecType == CODEC_NVIDIA)
        m_SrcInfo.FrVideo.eCodecType = CODEC_HW;
    else
        m_SrcInfo.FrVideo.eCodecType = CODEC_SW;

    LOG_I("Prepare() Begin..");

    m_bEnd = FALSE;
    m_bSync = FALSE;
    m_hTime = FrTimeOpen(0, 1.0);

    // reader
    LRSLT lRet = FrReaderOpen(&m_hReader, &m_UrlInfo);
    if (FRFAILED(lRet)) {
        return -1;
    }

    lRet = FrReaderGetInfo(m_hReader, &m_SrcInfo);
    if (FRFAILED(lRet)) {
        return -1;
    }

    // decoder
    FrVideoInfo* pSrcVideoInfo = &m_SrcInfo.FrVideo;
    pSrcVideoInfo->eColorFormat = VideoFmtYUV420P;     // output color format
    pSrcVideoInfo->bLowDelayDecoding = TRUE;
    pSrcVideoInfo->memDir = MEM_CPU;   // or MEM_GPU
    lRet = FrVideoDecOpen(&m_hDecVideo, NULL, pSrcVideoInfo);
    if (FRFAILED(lRet)) {
        return -1;
    }

    // device
    //HANDLE hWnd;
    m_SrcInfo.FrVideo.dwBitCount = 16;
    McVideoOutOpen(&m_hDevOut, &m_SrcInfo.FrVideo, param.wnd, FALSE);

    ChangeStatus(READY);

    LOG_I("Prepare() End..");
    
	return 1;
}

void* FdrPlayer::Player::TaskPlayback(void* arg) {
    LRSLT lRet;
    DWORD32 dwTime;
    int iDiff;
    FrMediaStream VideoData;
    FrRawVideo  DecVideo;
    FrRawVideo* pCbVideo;
    BOOL bFirstDecoded = FALSE;

    memset(&VideoData, 0, sizeof(FrMediaStream));
    VideoData.tMediaType = VIDEO_MEDIA;

    m_bVideoDecode = FALSE;
    m_bVideoRead = FALSE;
    m_bVideoRender = FALSE;

    TRACE("TaskMgVideo() Begin..");

    fr::Tick tick;

    //FrTimeStart(hMgVideo->m_hTime);

    while (!m_bEnd) {
        switch (m_iStatus) {
        case PLAYING:
            TRACE("TaskMgVideo() PLAYING bVideoRender(%d), bVideoDecoded(%d), bVideoRead(%d)", m_bVideoRender, m_bVideoDecode, m_bVideoRead);
            if (m_bVideoRender) {
                if (m_bSync) {
                    dwTime = FrTimeGetTime(m_hTime);
                    iDiff = dwTime - DecVideo.dwCTS;

                    if (iDiff >= AV_SYNC_LOWER_LIMIT && iDiff <= AV_SYNC_UPPER_LIMIT) {
                        for (int i = 0; i < DecVideo.nDecoded; i++) {
                            McVideoOutConversion(m_hDevOut, &DecVideo);
                            McVideoOutPlay(m_hDevOut);
                        }

                        m_bVideoRender = FALSE;
                        m_bVideoDecode = FALSE;
                        TRACE("TaskMgVideo	Video Rendering... CTS=%d CurTime=%d, diff = %d", DecVideo.dwCTS, dwTime, iDiff);
                    }
                    else if (iDiff >= AV_SYNC_UPPER_LIMIT && iDiff <= AV_SYNC_HIGH_LIMIT) {
                        TRACE("TaskMgVideo	Video doesn't display.. CurTime=%d VideoCTS=%d Diff=%d", dwTime, DecVideo.dwCTS, iDiff);
                        m_bVideoRender = FALSE;
                        m_bVideoDecode = FALSE;
                    }
                    else if (iDiff > AV_SYNC_HIGH_LIMIT) {	// The video frame is late. It's skipped.
                        LOG_D("TaskMgVideo Video Skipped to IVOP... CurTime=%d VideoCTS=%d Diff=%d", dwTime, DecVideo.dwCTS, iDiff);
                        m_bVideoRender = FALSE;
                        m_bVideoDecode = FALSE;
                    }
                    else {
                        TRACE("TaskMgVideo	VideoCTS=%d CTS=%d Diff=%d", DecVideo.dwCTS, dwTime, iDiff);
                        //McSleep(ABS(iDiff));
                        FrSleep(10);
                        break;
                    }
                }
                else {
                    //LOG_D("TaskMgVideo No Sync Video Callback... VideoCTS=%d Decoded Num=%d", DecVideo.dwCTS, DecVideo.nDecoded);
                    tick.Start();
                    for (int i = 0; i < DecVideo.nDecoded; i++) {
                        McVideoOutConversion(m_hDevOut, &DecVideo);
                        McVideoOutPlay(m_hDevOut);
                    }
                    long diff = tick.Stop();
                    LOG_D("TaskMgVideo Tick test..tick(%d)", diff);

                    m_bVideoRender = FALSE;
                    m_bVideoDecode = FALSE;
                }
            }

            if (m_bVideoRead && !m_bVideoDecode) {
                lRet = FrVideoDecDecode(m_hDecVideo, &VideoData, &DecVideo);
                if (SUCCEEDED(lRet)) {
                    if (!bFirstDecoded) {
                        FrTimeStart(m_hTime);
                        bFirstDecoded = TRUE;
                        LOG_D("TaskMgVideo Fist Video Decoded... VideoCTS=%d", DecVideo.dwCTS);
                    }
                    m_bVideoDecode = TRUE;
                    m_bVideoRender = TRUE;
                }

                m_bVideoRead = FALSE;
            }

            if (!m_bVideoRead) {
                lRet = FrReaderReadFrame(m_hReader, &VideoData);
                if (FRFAILED(lRet)) {
                    if (lRet == COMMON_ERR_ENDOFDATA) {
                        LOG_D("TaskMgVideo EndOfData.. VideoCTS=%d", VideoData.dwCTS);
                        break;
                    }

                    FrSleep(10);        // assume to wait data..
                    //continue;
                }
                else {
                    m_bVideoRead = TRUE;
                }
            }

            break;
        case STOP:
            break;
        case PAUSE:
            break;
        case STEP:
            break;
        default:
            break;
        }

    }

    m_hTask = nullptr;    
    return nullptr;
}