#pragma once

#include "DecoderAPI.h"
#include "EncoderAPI.h"
#include "ConverterAPI.h"

typedef struct {
	FrVideoDecHandle m_hVideoDec;
	FrVideoEncHandle m_hVideoEnc;
	
	FrMediaInfo m_SrcInfo;
	FrMediaInfo m_DstInfo;

	FrMediaStream m_VideoStream;
	FrRawVideo m_RawVideo;
} FrTransStruct;
