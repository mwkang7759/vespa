﻿/*****************************************************************************
*                                                                            *
*					AR Graphics Module          							 *
*                                                                            *
*   Copyright (c) 2004 by 4dreplay, Incoporated. All Rights Reserved.        *
******************************************************************************

	File Name       : ESMMovieGraphic
	Author(s)       : Ma EunKyoung
	Created         : 09 Dec 2021

	Description     : 
	Notes           :

==============================================================================*/

#pragma once

#include <opencv2/opencv.hpp>
#include <opencv2/cudaimgproc.hpp>

#include "ESMLogger.h"

#include "MatrixCalculator.h"
#include "ESMMovieGraphic.h"

#include "FdEffectData.h"	//add
#include "ESMMovieMaker.h"	//add

class ESMMovieGraphic
{
public:
	class ARShape
	{
	public:
		ARShape();
		virtual ~ARShape();

		int mAlpha_{ 100 };
		int mAlpha_max_{ 100 };		
		int mTime_Ani_;
		Scalar _bgr;

		enum eCOLOR {
			CUSTOM, RED, ORANGE, YELLOW, GREEN, BLUE, CYAN, BLACK,
			GOLD, OLIVE, AQUAMARIN, SKYBLUE, VYOLET, PINK, MINT, BRIKRED, WHITE,
		};
		eCOLOR mColor_;
		eCOLOR mColor_Spin_;

		int GetAbsValue(int cm);
		void SetColorBGR(const ColorRGB& bgr);
		void SetColor(eCOLOR c, FdEffect* effect, Scalar* color);
		void Appear(int t) { mAlpha_ += (mAlpha_max_ * 255.0 / 100.0) / t; }
		void Appear() { mAlpha_ = mAlpha_max_; }
		void Diappear(int t) { mAlpha_ -= (mAlpha_max_ * 255.0 / 100.0) / t; }
	};

	class ARCircle:public ARShape 
	{
	public:
		Scalar mSpin_rgb_;
		Point mCenter_;
		int mSpin_;
		double mRadius_;
		double mRadius_start_;
		double mThickness_;
		bool mCir_Bigger_Flag_;
	
		enum eCirType { CIRCLE_FILL, CIRCLE_RING, CIRCLE_DOTTED, CIRCLE_SPIN };
		eCirType mCircle_type_;

		void Draw(Mat plane);
		void ExpandRad(int t, int r) { mRadius_ = ((1.0 - mRadius_start_) * t / mTime_Ani_ + mRadius_start_) * r; }
	};

	class ARArrow : public ARShape
	{
	private:
		double triEx, triEy, trip1x, trip1y, trip2x, trip2y;
		double angle;
		double thickness;

	public:
		Point mStart_, mEnd_;
		Point mSetStart_, mSetEnd_;
		bool mSetStart_Flag_, mSetEnd_Flag_;
		bool mLinedraw_Flag_;

		double mThick_{ 10. };
		enum eArrType { SOLID, DOTTED };
		eArrType mArrow_type;

		void SetStartPos(Point& start);
		void SetEndPos(Point& end);
		void Draw(Mat plane, int t);
	

	};


	class ARTriangle :public ARShape {

	public:

		vector<Point> mPoint_;
		vector<Point> mWarpPoint_;

		void Draw(Mat plane);

	};


	static void CalcWarpPoint(Point2d point, Point2d* h_point, int idx);
	void CalcWarpPoint_T(Point2d point, Point2d* h_point, int idx);


	ESMMovieGraphic();
	~ESMMovieGraphic();

};