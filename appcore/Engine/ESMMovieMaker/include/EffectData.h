﻿#pragma once
#define _NO_LINE_TYPE_
#define _NO_POLYGON_TYPE_
#include <array>
#ifndef _NO_POLYGON_TYPE_
#include <vector>
#endif

namespace Effect
{
	enum class EffectType
	{
		// 0: Invalid
#ifndef _NO_LINE_TYPE_
		FXT_LINE = 1,
#else
		FXT_ARROW = 2,
#endif
		FXT_CIRCLE = 3,
		FXT_TRIANGLE = 4,
#ifndef _NO_POLYGON_TYPE_
		FXT_POLYGON = 5,
#endif
	};

#ifndef _NO_LINE_TYPE_
	enum class LineStyle
	{
		// 0: Invalid
		FXST_LINE_SOLID = 1,
		FXST_LINE_DOTTED,
	};
#endif

	enum class ArrowStyle
	{
		// 0: Invalid
		FXST_ARROW_SOLID = 1,
		FXST_ARROW_DOTTED,
	};

	enum class CircleStyle
	{
		// 0: Invalid
		FXST_CIR_FILL = 1,
		FXST_CIR_RING,
		FXST_CIR_SPIN,
		FXST_CIR_DOTTED,
	};

	enum class PolygonStyle
	{
		// 0: Invalid
		FXST_POLY_FILL = 1,
		//FXST_POLY_EDGE,		// 현재 Fill Style만 적용
	};

	typedef unsigned char color8;
	struct ColorRGB
	{
		color8 r{};
		color8 g{};
		color8 b{};
	};

	// pre-define color
	static const ColorRGB COLOR_RED =		{ 255, 0,   0   };
	static const ColorRGB COLOR_GREEN =		{ 0,   255, 0   };
	static const ColorRGB COLOR_BLUE =		{ 0,   0,   255 };
	// ...

	struct Point
	{
		int cam_idx{};
		double x{};
		double y{};
	};

	struct BaseEffect
	{
		EffectType type{};
		int style{};
		//int thickness{};
		ColorRGB color;
		int alpha_max{ 100 };
		bool fadein{ false };
		bool fadeout{ false };

		virtual BaseEffect* Get() = 0;
	};

#ifndef _NO_LINE_TYPE_
	struct LineEffect : public BaseEffect
	{
		Point pt_from;
		Point pt_to;

		LineEffect(LineStyle st)
		{
			type = EffectType::FXT_LINE;
			style = static_cast<int>(st);
		}
		virtual LineEffect* Get() override { return this; }
	};
#endif

	struct ArrowEffect : public BaseEffect
	{
		std::array<Point, 2> pts;

		ArrowEffect(LineStyle st)
		{
			type = EffectType::FXT_ARROW;
			style = static_cast<int>(st);
		}
		virtual ArrowEffect* Get() override { return this; }
	};

	struct CircleEffect : public BaseEffect
	{
		Point center;
		std::array<int,2> radius;	// size 0: mmc default size, 1: 고정 size, 2: effect 구간 size 변화
		ColorRGB sub_color;

		CircleEffect(CircleStyle st)
		{
			type = EffectType::FXT_CIRCLE;
			style = static_cast<int>(st);
		}
		virtual CircleEffect* Get() override { return this; }
	};

	struct TriangleEffect : public BaseEffect
	{
		std::array<Point, 3> pts;

		TriangleEffect(PolygonStyle st)
		{
			type = EffectType::FXT_TRIANGLE;
			style = static_cast<int>(st);
		}
		virtual TriangleEffect* Get() override { return this; }
	}

#ifndef _NO_POLYGON_TYPE_
	struct PolygonEffect : public BaseEffect
	{
		std::vector<Point> pts;

		PolygonEffect(PolygonStyle st)
		{
			type = EffectType::FXT_POLYGON;
			style = static_cast<int>(st);
		}
		virtual PolygonEffect* Get() override { return this; }
	};
#endif
}