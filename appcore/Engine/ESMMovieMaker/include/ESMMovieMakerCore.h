﻿#pragma once

#include <ESMMovieMaker.h>
#include <ESMFFDemuxer.h>
#include <ESMNvdec.h>
#include <ESMNvenc.h>
#include <ESMNvconverter.h>
#include <opencv2/opencv.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/cudafilters.hpp>

#include "ESMLogger.h"
#include "ESMConnector.h" //20211005 Kelly add for Recal
#include "ESMMovieGraphic.h" //20211209 Kelly add for Graphic

// add pose
#include "DeepInfer.h"

enum CALL_TYPE {
	VIDEO_RECV = 0,
	VIDEO_END = 1
};

class ESMMovieMaker::Core: public ESMFFDemuxer::Handler
{
public:
	Core(VOID);
	virtual ~Core(VOID);

	BOOL	IsInitialized(VOID);
	int		Initialize(ESMMovieMaker::CONTEXT_T* ctx);
	int		Release(VOID);
	int32_t	Process(ESMMovieMaker::PARAM_T* param);

	virtual int32_t OnVideoBegin(ESMFFDemuxer::CONTEXT_T* ctx, int32_t codec, const uint8_t* extradata, int32_t extradataSize, int32_t width, int32_t height, double fps, int32_t framecount, int32_t seektime);
	virtual int32_t OnVideoRecv(ESMFFDemuxer::CONTEXT_T* ctx, uint8_t* bytes, int32_t nbytes, int32_t& nFrameIdx);
	virtual int32_t OnVideoEnd(ESMFFDemuxer::CONTEXT_T* ctx, int32_t& nFrameIdx);
private:
	Core(const ESMMovieMaker::Core& clone);

	bool CheckValidH(HOMO_T& t);
	int32_t DoAdjust(cv::cuda::GpuMat& srcMat, ESMMovieMaker::ADJUST_INFO_T& adjustInfo, const int32_t dstWidth, const int32_t dstHeight, cv::cuda::GpuMat& dstMat);
	int32_t DoAdjustH(cv::cuda::GpuMat& srcMat, ESMMovieMaker::ADJUST_INFO_T& adjustInfo, const int32_t dstWidth, const int32_t dstHeight, cv::cuda::GpuMat& dstMat);
	int32_t DoVMCC(cv::cuda::GpuMat& srcMat, std::array<int32_t, 2> inCenterPt, const int32_t inZoom, const int32_t inDstWidth, const int32_t inDstHeight, cv::cuda::GpuMat& dstMat);
	int32_t DebugNumberOnMat(cv::cuda::GpuMat& srcMat, const int32_t idx, const std::string prefix);
	int32_t WriteFile(const char* filename, uint8_t* data, int32_t size);
	void ProceedDemuxError(int32_t status);
	int32_t TransFrames(ESMFFDemuxer::CONTEXT_T* ctx, const int callType, int& nFrameIdx, const int nDecoded, const uint8_t** ppDecoded);
	void DrawArEffect(cv::cuda::GpuMat& img);

	long long					MillisecondsNow(void);

private:
	ESMMovieMaker::CONTEXT_T* _context;
	BOOL _isInitialized;

	ESMFFDemuxer::CONTEXT_T _demuxerCtx;
	ESMFFDemuxer _demuxer;

	ESMNvdec::CONTEXT_T _decoderCtx;
	ESMNvdec _decoder;

	ESMNvenc::CONTEXT_T _encoderCtx;
	ESMNvenc _encoder;

	ESMNvconverter::CONTEXT_T _converterCtx;
	ESMNvconverter _converter;

	uint8_t* _bitstream;
	size_t _bitstreamCapacity;

	uint8_t* _encodeInput;
	size_t _encodeInputPitch;

	ESMMovieMaker::PARAM_T* _param;
	int32_t _frameCount;
	int32_t _seekTime;
	int32_t _gopSize;

	int32_t _startGopNum;
	int32_t _globalCnt;
	int32_t _localStartCnt;
	int32_t _localEndCnt;
	BOOL _bReverse;

	BOOL _encodeIndex;

	//20211005 Kelly add for Recal
	BOOL _recal; 
	ESMConnector* _conn;
	bool _recal_mode;
	cv::Mat ref_img;
	cv::cuda::GpuMat gref_img;
	string _recal_dscid;

	// add pose
	bool _pose_mode;
	DeepInfer* _infer{ nullptr };
	DeepInfer::context_t _infer_ctx;

	//20211209 Kelly add for graphic
	ESMMovieGraphic* _effect;
	
	FdPoint _fdPoint[10];
	FdEffect* _fdEffect;
	vector<ESMMovieGraphic::ARArrow> _arrow;
	vector<ESMMovieGraphic::ARCircle> _circle;
	vector<ESMMovieGraphic::ARTriangle> _triangle;
	vector<cv::Point2d> selPoint;
	vector<cv::Point2d> wPoint;

#if defined(WITH_SMOOTH_FILTER)
	cv::Ptr<cv::cuda::Filter> _smooth;
#endif

	LARGE_INTEGER	_fw_frequency;
	BOOL			_fw_use_qpc;


	//ESMLogger _logger;
};