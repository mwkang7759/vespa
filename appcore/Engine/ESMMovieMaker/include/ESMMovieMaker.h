﻿#pragma once

#if defined(EXPORT_ESM_MOVIEMAKER)
#define ESM_MOVIEMAKER_CLASS __declspec(dllexport)
#else
#define ESM_MOVIEMAKER_CLASS __declspec(dllimport)
#endif

#include <ESMBase.h>
#include <MppcPointData.h>
#include <FdEffectData.h>
#include <map>

class ESMMovieMaker : public ESMBase
{
public:
	class Core;
public:
	class Handler
	{
	public:
		virtual void OnExtraData(const uint8_t* bitstream, int32_t size, const std::string& name) = 0;
		virtual void OnEncodingComplete(MppcPoints* points, int32_t status, int32_t frameIdx, const uint8_t * bitstream, int32_t size, const std::string& name) = 0;
		virtual void onMovieMakerError(int32_t code, const char* error) = 0;
	};

	typedef struct _CONTEXT_T
	{
		int32_t deviceIndex = 0;
		int32_t width = 1920;
		int32_t height = 1080;
		int32_t codec = ESMMovieMaker::VIDEO_CODEC_T::AVC;
		int32_t profile = ESMMovieMaker::AVC_PROFILE_T::HP;
		int32_t gop = 1;
		int32_t bitrate = 70 * 1024 * 1024;
		int32_t fps_num = 30;
		int32_t fps_den = 1;
		BOOL	gimbal = FALSE;
		ESMMovieMaker::Handler* handler = NULL;
		BOOL operator==(const _CONTEXT_T & rhs) 
		{
			if (this->deviceIndex != rhs.deviceIndex ||
				this->codec != rhs.codec ||
				this->profile != rhs.profile ||
				this->gop != rhs.gop ||
				this->bitrate != rhs.bitrate ||
				this->fps_num != rhs.fps_num ||
				this->fps_den != rhs.fps_den)
				return false;
			return true;
		}

		BOOL operator!=(const _CONTEXT_T & rhs)
		{
			return !(*this == rhs);
		}
	} CONTEXT_T;

	typedef struct _HOMO_T
	{
		double r1[3];
		double r2[3];
		double r3[3];
	} HOMO_T;

	typedef struct _ADJUST_INFO_T
	{
		std::string strDscId;
		std::string strMode;

		double dAdjustX;
		double dAdjustY;
		double dAngle;
		double dRotateX;
		double dRotateY;
		double dScale;
		double dMarginX;
		double dMarginY;
		double dMarginW;
		double dMarginH;
		int32_t nX1;
		int32_t nY1;
		int32_t nX2;
		int32_t nY2;
		int32_t nX3;
		int32_t nY3;
		int32_t nX4;
		int32_t nY4;

		int32_t nCenterX;
		int32_t nCenterY;

		BOOL   bFlip;
		//bool   bUseLogo;
		HOMO_T homo;
	} ADJUST_INFO_T;

	struct FRAME_INFO_T
	{
		int32_t x{};
		int32_t y{};
		int32_t zoom{};
		char prefix[MAX_PATH];
		std::string matchingCamPrefix;
		int32_t matchingTime{};
		int32_t matchingFrameIdx{};

		FRAME_INFO_T(VOID)
			: prefix{ 0 }
		{
		}
	};

	typedef struct _PARAM_T
	{
		char srcFile[MAX_PATH];
		char dstFilePath[MAX_PATH];
		int32_t nStartFrameNum;
		int32_t nEndFrameNum;
		int32_t nSrcGopSize;
		float fWantedFPS;
		BOOL bUseExtradata;
		std::string recordName;
		std::map<int32_t, ADJUST_INFO_T> adjustInfo;
		std::map<int32_t, FRAME_INFO_T> frameInfo;
		std::map<std::string, MppcPoints> pointInfo; // Key: dsc_id
		BOOL bDebugNumber;
		FdARData* ARData{ nullptr }; // Warning: Don't delete the object this pointer points.
		_PARAM_T(VOID)
			: srcFile{ 0 }
			, dstFilePath{ 0 }
			, nStartFrameNum(0)
			, nEndFrameNum(0)
			, nSrcGopSize(0)
			, fWantedFPS(0.f)
			, bUseExtradata(TRUE)
			, bDebugNumber(FALSE)
		{
		}
	} PARAM_T;
	
	ESMMovieMaker(VOID);
	virtual ~ESMMovieMaker(VOID);

	BOOL		IsInitialized(VOID);
	int32_t		Initialize(ESMMovieMaker::CONTEXT_T * ctx);
	int32_t		Process(ESMMovieMaker::PARAM_T* param);
	int32_t		Release(VOID);

private:
	ESMMovieMaker(const ESMMovieMaker& clone);

private:
	ESMMovieMaker::Core* _core;
};