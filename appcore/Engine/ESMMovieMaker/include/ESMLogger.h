﻿#pragma once
#define SPDLOG_ACTIVE_LEVEL SPDLOG_LEVEL_TRACE
#include <spdlog/spdlog.h>
#include <spdlog/async.h>
#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/sinks/rotating_file_sink.h>
#include <spdlog/sinks/dist_sink.h>
#include <spdlog/sinks/msvc_sink.h>
#include <iostream>

class ESMLogger
{
private:
	static std::shared_ptr<spdlog::logger> _logger;

public:
	ESMLogger(VOID);
	~ESMLogger(VOID);

	static void Init(VOID);
	static int32_t GetLogLevel(VOID);
};

//#define SPd_INFO(...)	 ::Logger::GetConsoleLogger()->info(__VA_ARGS__);SPDLOG_INFO(__VA_ARGS__); ::Logger::GetFileLogger()->info(__VA_ARGS__);
#define SPd_ERROR(...)		SPDLOG_ERROR(__VA_ARGS__)
#define SPd_WARN(...)		SPDLOG_WARN(__VA_ARGS__)
#define SPd_INFO(...)		SPDLOG_INFO(__VA_ARGS__)
#define SPd_DEBUG(...)		SPDLOG_DEBUG(__VA_ARGS__)
#define SPd_CRITICAL(...)	SPDLOG_CRITICAL(__VA_ARGS__)
#define SPd_TRACE(...)		SPDLOG_TRACE(__VA_ARGS__)

