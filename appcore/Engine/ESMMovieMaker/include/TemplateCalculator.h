﻿#pragma once

#include <opencv2/opencv.hpp>
#include <vector>

using namespace std;
using namespace cv;

#define FD_MIN(a,b) (((a)<(b))?(a):(b))
#define FD_MAX(a,b) (((a)>(b))?(a):(b))

typedef struct CvLine {
    Point2f p1;
    Point2f p2;
} Line;

class TemplateCalculator
{
private:
    static double dist(const Point2f& p1, const Point2f& p2);
    static double euclideanDistance(const Point2f& p1, const Point2f& p2);
    static int ccw(const Point2f& p1, const Point2f& p2, const Point2f& p3);
    static int SortComparator(const Point2f& pt, const Point2f& left, const Point2f& right);
    static void QuickSort(Point2f* a, int lo, int hi);
    static int LineComparator(Point2f left, Point2f right);
    static void swap(Point2f& p1, Point2f& p2);
    static int LineIntersection(CvLine l1, CvLine l2);
    static int PolygonInOut(Point2f p, int num_vertex, Point2f* vertices);
    static Point2f IntersectionPoint(const Point2f& p1, const Point2f& p2, const Point2f& p3, const Point2f& p4);
    static double GetPolygonArea(vector<Point2f>& points);
    static bool checkInteriorExterior(const cv::Mat& mask, const cv::Rect& interiorBB, int& top, int& bottom, int& left, int& right);
    static bool sortX(cv::Point2f a, cv::Point2f b);
    static bool sortY(cv::Point2f a, cv::Point2f b);
    static Point2f GetCentroidPolygon(const std::vector<cv::Point2f>& poly);
    static Point2f GetFarthestPoint(const std::vector<cv::Point2f>& poly, const Point2f pt);
public:
    static bool isInside(Point2f B, const vector<Point2f>& p);
    static vector<Point2f> GetIntersection(vector<Point2f> points1, vector<Point2f> points2);
    static Rect2f GetRectangleInsidePolygon(int nWidth, int nHeight, vector<Point2f> poly);
    static Rect2f GetCentroidRectangleInsidePolygon(const vector<Point2f>& ptsPoly, const double fRatioWH);
};

