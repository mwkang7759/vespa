﻿// ***********************************************************************
// Assembly         : FdEffectData.h
// Author           : Changhwan Bang (chbang@4dreplay.com)
// Created          : 2021-11-23
// 
// Copyright (c) 4DREPLAY Korea, Inc. All rights reserved.
// ***********************************************************************

////////////////////
// AR Effect Data //
////////////////////

#pragma once

#include <string>
#include <map>
#include <vector>
#include <array>

typedef unsigned char color8;
struct ColorRGB
{
	color8 r{};
	color8 g{};
	color8 b{};

	void Fill(const std::string& value)
	{
		// Convert string to RGB

		if (value.empty())
			throw std::invalid_argument("value is empty.");

		if (value.size() < 2)
			throw std::invalid_argument("value size should be at lease 2.");

		if (value[0] == '#')
		{
			if (value.size() < 7)
				throw std::invalid_argument("#-value size should be at least 7 including '#'.");

			r = std::stoi(value.substr(1, 2), nullptr, 16);
			g = std::stoi(value.substr(3, 2), nullptr, 16);
			b = std::stoi(value.substr(5, 2), nullptr, 16);
		}
	}
};

struct FdPoint
{
	std::string prefix; // Camera Prefix (e.g. "011013")
	double x{};
	double y{};
};

struct FdEffect
{
	enum class Type
	{
		Arrow = 0,
		Circle,
		Polygon
	};

	Type type; // You can use it especially to find out what kind of FdEffect it is.

	int alpha{ 100 };
	int thickness{ -1 }; // Default Value: -1 (Auto)
	bool fadein{};
	bool fadeout{};

	ColorRGB color_main;

	virtual ~FdEffect() {}
};

struct FdArrowEffect : public FdEffect
{
	enum class Style
	{
		Solid = 0,
		Dotted,
	};

	Style style;
	std::pair<double, double> drawline{ 0, 100 }; // first:'from', second:'to'
	std::pair<FdPoint, FdPoint> point;

	FdArrowEffect() : FdEffect() { type = Type::Arrow; }
};

struct FdCircleEffect : public FdEffect
{
	enum class Style
	{
		Fill = 0,
		Ring,
		Spin,
		Dotted
	};

	Style style;
	double radius{};
	ColorRGB color_sub;
	FdPoint point;

	FdCircleEffect() : FdEffect() { type = Type::Circle; }
};

struct FdPolygonEffect : public FdEffect
{
	enum class Style
	{
		Fill = 0,
	};

	Style style;
	std::vector<FdPoint> point{ FdPoint{}, FdPoint{}, FdPoint{} };

	static constexpr int min_point_size{ 3 }; // Minimum size of 'point' vector. 

	FdPolygonEffect() : FdEffect()
	{
		type = Type::Polygon;
		for (int i = 0; i < min_point_size; i++)
			point.push_back(FdPoint{});
	}
};

struct FdARData
{
	std::pair<int, int> frame; // first: start frame / second: end frame
	std::vector<FdArrowEffect> arrow;
	std::vector<FdCircleEffect> circle;
	std::vector<FdPolygonEffect> polygon;

	bool IsAvailable() // If it returns false, it means that AR data is not ready yet.
	{
		return !(arrow.empty() && circle.empty() && polygon.empty());
	}

	void Clear()
	{
		frame = std::make_pair(0, 0);
		arrow.clear();
		circle.clear();
		polygon.clear();
	}
};

/////////
// MMC //
/////////

// Note: Do not handle this data directly. 'VideoMaker' in MMC handles it.
struct FdEffectDataSet
{
	std::map<std::string, FdEffect*> effect; // Key:'CustomCircle,Arrow,Polygon,...'

	void Clear()
	{
		for (auto& pair : effect)
			delete pair.second;
		effect.clear();
	}

	~FdEffectDataSet()
	{
		Clear();
	}
};
