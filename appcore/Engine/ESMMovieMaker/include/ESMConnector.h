﻿/*****************************************************************************
*                                                                            *
*                            ESMonnector.h        			     *
*                                                                            *
*   Copyright (C) 2021 By 4dreplay, Incoporated. All Rights Reserved.        *
******************************************************************************

    File Name       : ESMConnector.h
    Author(S)       : Me Eunkyung
    Created         : 05 Nov 2021

    Description     : Data Coverter from both direction : ESM <-> Genesis
    Notes           : 
*/

#include "../Recalibration/src/DefData.hpp"
#include "../Recalibration/src/Extractor.hpp"
#include "MppcPointData.h"

#if defined _WIN_ || _WINDOWS


#endif

using namespace std;
using namespace cv;

#define _GPU

class ESMConnector {

    public:
        ESMConnector();
        ~ESMConnector();
        
        int Start(int width = 3840);
        int CheckDataValidity(MppcPoints* mpp, std::string dsc_id, int _scale = 1);
#if defined GPU
        int Recalibration(cuda::GpuMat& gref, cuda::GpuMat& gcur, MppcPoints* mpp, std::string dsc_id);
#else        
        int Recalibration(Mat& ref, Mat& cur, MppcPoints* mpp, string dsc_id);
#endif        
        int Finish();        

    private:
        int scale;
        Dlog dl;
        Extractor *genesis;
        Mat ref;
        Mat cur;
        FPt in_pt[4]; 
        FPt out_pt[4];        
#if defined GPU
        cuda::GpuMat gref;
        cuda::GpuMat gcur;
#endif

        int ConvertFromESMtoGenesis(MppcPoints* mpp, FPt* in);
        int ConvertFromGenesistoESM(FPt* out, MppcPoints* mpp);
        
};