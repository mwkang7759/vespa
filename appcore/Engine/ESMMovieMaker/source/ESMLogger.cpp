﻿#include "ESMLogger.h"
#include <sstream>
#include <iomanip>

enum class SPdLogLevel : int
{
	NONE = -1,
	SPD_LOG_LEVEL_TRACE = 0,
	SPD_LOG_LEVEL_DEBUG,
	SPD_LOG_LEVEL_INFO,
	SPD_LOG_LEVEL_WARN,
	SPD_LOG_LEVEL_ERROR,
	SPD_LOG_LEVEL_CRITICAL,
	SPD_LOG_LEVEL_OFF
};

const std::string CONFIG_INI_NAME = "./config.ini";

std::shared_ptr<spdlog::logger> ESMLogger::_logger;

ESMLogger::ESMLogger(VOID)
{
	Init();
}

ESMLogger::~ESMLogger(VOID)
{
	SPd_INFO("Logger End!");
}

void ESMLogger::Init(VOID)
{
	spdlog::flush_every(std::chrono::seconds(1));
	auto console_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
	console_sink->set_level(spdlog::level::trace);
	auto console_link = std::make_shared<spdlog::sinks::dist_sink_mt>();
	console_link->add_sink(console_link);

	std::string fileName("logs/movie_maker-");

	// Put date and time to the log file name.
	auto tt = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	tm lt;
	localtime_s(&lt, &tt);

	std::ostringstream oss;
	oss << std::put_time(&lt, "%y%m%d_%H%M%S");
	fileName += oss.str();
	fileName += "-";

	// Note: Writing PID to the log file is mandatory because MMd(MMC, esp.) can execute multiple processes in one PC.
	//       You can't get proper log files without it because spdlog doesn't support the multiple process.
	fileName += std::to_string(GetCurrentProcessId());
	fileName += ".log";

	auto file_sink = std::make_shared<spdlog::sinks::rotating_file_sink_mt>(fileName, 1024 * 1000 * 10, 10);
	file_sink->set_level(spdlog::level::trace);
	auto file_link = std::make_shared < spdlog::sinks::dist_sink_mt>();
	file_link->add_sink(file_sink);

	spdlog::sinks_init_list sink_list = { console_sink, file_sink };


	_logger = std::make_shared<spdlog::logger>("MMd", sink_list);
	int get = GetLogLevel();
	switch (get)
	{
	case 0:
		_logger->set_level(spdlog::level::trace);
		break;
	case 1:
		_logger->set_level(spdlog::level::debug);
		break;
	case 2:
		_logger->set_level(spdlog::level::info);
		break;
	case 3:
		_logger->set_level(spdlog::level::warn);
		break;
	case 4:
		_logger->set_level(spdlog::level::err);
		break;
	case 5:
		_logger->set_level(spdlog::level::critical);
		break;
	}

	// Note: Writing PID to the log file is mandatory because MMd(MMC, esp.) can execute multiple processes in one PC.
	//       You can't get proper log files without it because spdlog doesn't support the multiple process.

	spdlog::set_default_logger(_logger);
	spdlog::set_pattern("[%Y-%m-%d %X.%e] [PID:%P] [thread %t] [%^%l%$] [%s:%#] - %v");
}

int32_t ESMLogger::GetLogLevel(VOID)
{
	char iniInfo[100];
	GetPrivateProfileStringA("Log", "Level", NULL, iniInfo, 100, CONFIG_INI_NAME.c_str());
	std::string strLevel = std::string(iniInfo);
	SPdLogLevel eLevel = SPdLogLevel::NONE;
	if (strLevel.compare("trace") == 0)
		eLevel = SPdLogLevel::SPD_LOG_LEVEL_TRACE;
	else if (strLevel.compare("debug") == 0)
		eLevel = SPdLogLevel::SPD_LOG_LEVEL_DEBUG;
	else if (strLevel.compare("info") == 0)
		eLevel = SPdLogLevel::SPD_LOG_LEVEL_INFO;
	else if (strLevel.compare("warning") == 0)
		eLevel = SPdLogLevel::SPD_LOG_LEVEL_WARN;
	else if (strLevel.compare("error") == 0)
		eLevel = SPdLogLevel::SPD_LOG_LEVEL_ERROR;
	else if (strLevel.compare("critical") == 0)
		eLevel = SPdLogLevel::SPD_LOG_LEVEL_CRITICAL;
	else
		eLevel = SPdLogLevel::SPD_LOG_LEVEL_INFO;

	return static_cast<int>(eLevel);
}