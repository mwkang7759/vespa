﻿/*****************************************************************************
*                                                                            *
*					AR Graphics Module          							 *
*                                                                            *
*   Copyright (c) 2004 by 4dreplay, Incoporated. All Rights Reserved.        *
******************************************************************************

	File Name       : ESMMovieGraphic.cpp
	Author(s)       : Ma EunKyoung
	Created         : 09 Dec 2021

	Description     : 
	Notes           :

==============================================================================*/

#include "ESMMovieMakerCore.h"
#include <ESMLocks.h>
#include "cuda_runtime.h"

#include "opencv2/cudawarping.hpp"
#include "opencv2/cudaarithm.hpp"




#if defined(_DEBUG)
#pragma comment(lib, "opencv_core440d.lib")
#pragma comment(lib, "opencv_highgui440d.lib")
#pragma comment(lib, "opencv_imgproc440d.lib")
#pragma comment(lib, "opencv_imgcodecs440d.lib")
#pragma comment(lib, "opencv_calib3d440d.lib")
#pragma comment(lib, "opencv_cudaimgproc440d.lib")
#pragma comment(lib, "opencv_cudawarping440d.lib")
#pragma comment(lib, "opencv_cudaarithm440d.lib")
#else
#pragma comment(lib, "opencv_core440.lib")
#pragma comment(lib, "opencv_highgui440.lib")
#pragma comment(lib, "opencv_imgproc440.lib")
#pragma comment(lib, "opencv_imgcodecs440.lib")
#pragma comment(lib, "opencv_calib3d440.lib")
#pragma comment(lib, "opencv_cudaimgproc440.lib")
#pragma comment(lib, "opencv_cudawarping440.lib")
#pragma comment(lib, "opencv_cudaarithm440.lib")
#endif


void ESMMovieGraphic::CalcWarpPoint(Point2d point, Point2d* h_point, int idx)
{

	double h11, h12, h13, h21, h22, h23, h31, h32, h33;
	Mat h;
	//h = Hmg[idx];
	h11 = h.at<double>(0, 0);
	h12 = h.at<double>(0, 1);
	h13 = h.at<double>(0, 2);
	h21 = h.at<double>(1, 0);
	h22 = h.at<double>(1, 1);
	h23 = h.at<double>(1, 2);
	h31 = h.at<double>(2, 0);
	h32 = h.at<double>(2, 1);
	
	double dx = (h11 * point.x + h12 * point.y + h13) / (h31 * point.x + h32 * point.y + 1);
	double dy = (h21 * point.x + h22 * point.y + h23) / (h31 * point.x + h32 * point.y + 1);

	*h_point = Point2d(dx, dy);
}

void ESMMovieGraphic::CalcWarpPoint_T(Point2d point, Point2d* h_point, int idx)
{

	double th11, th12, th13, th21, th22, th23, th31, th32, th33;
	Mat h_T;
	//h_T = t.Hmg[idx];
	th11 = h_T.at<double>(0, 0);
	th12 = h_T.at<double>(0, 1);
	th13 = h_T.at<double>(0, 2);
	th21 = h_T.at<double>(1, 0);
	th22 = h_T.at<double>(1, 1);
	th23 = h_T.at<double>(1, 2);
	th31 = h_T.at<double>(2, 0);
	th32 = h_T.at<double>(2, 1);

	double dx = (th11 * point.x + th12 * point.y + th13) / (th31 * point.x + th32 * point.y + 1);
	double dy = (th21 * point.x + th22 * point.y + th23) / (th31 * point.x + th32 * point.y + 1);

	*h_point = Point2d(dx, dy);
}

ESMMovieGraphic::ESMMovieGraphic() {

}

ESMMovieGraphic::~ESMMovieGraphic() {

}

ESMMovieGraphic::ARShape::ARShape() {
	mAlpha_ = 100;
	mAlpha_max_ = 100;
	mTime_Ani_ = 1.0;
}
ESMMovieGraphic::ARShape::~ARShape() {

}

int ESMMovieGraphic::ARShape::GetAbsValue(int cm)
{

	double wX[4] = {};
	double wY[4] = {};

	// 값 받아오기 수정
	MppcWorlds world;
	wX[0] = world.world_coords.X1;
	wX[1] = world.world_coords.X2;
	wX[2] = world.world_coords.X3;
	wX[3] = world.world_coords.X4;
	wY[0] = world.world_coords.Y1;
	wY[1] = world.world_coords.Y2;
	wY[2] = world.world_coords.Y3;
	wY[3] = world.world_coords.Y4;

	double widPixel = (abs(wX[1] - wX[0]) + abs(wX[2] - wX[3])) * 0.5;
	//double heiPixel = (abs(wY[2] - wY[1]) + abs(wY[3] - wY[0])) * 0.5;

	const int widReal = 13500;	//	(millimeter)
	//const int heiReal = 3300;
	const int radiusReal = 600; // 0.6M

	int radius1, radius2;
	radius1 = double(radiusReal * widPixel) / widReal;
	//radius2 = double(radiusReal * heiPixel) / heiReal;

	return radius1 * 2;
}


void ESMMovieGraphic::ARShape::SetColorBGR(const ColorRGB& rgb) {
	_bgr = Scalar((255 - rgb.b) / 255.0 * mAlpha_, (255 - rgb.g) / 255.0 * mAlpha_, (255 - rgb.r) / 255.0 * mAlpha_);
}

void ESMMovieGraphic::ARShape::SetColor(eCOLOR c, FdEffect* effect, Scalar* color) {
	int alpha = mAlpha_;
	int alpha_max = mAlpha_max_;
	
	uchar r = effect->color_main.r;
	uchar g = effect->color_main.g;
	uchar b = effect->color_main.b;

	switch(c)
	{
	case CUSTOM:
		//rgb = Scalar((255 - b) * alpha / alpha_max, (255 - g) * alpha / alpha_max, (255 - r) * alpha / alpha_max);				//custom
		*color = Scalar((255 - b) / 255.0 * alpha, (255 - g) / 255.0 * alpha, (255 - r) / 255.0 * alpha);
		break;
	case RED:
		r = 255, g = 50, b = 55;
		break;
	case ORANGE:
		r = 255, g = 150, b = 50;
		break;
	case YELLOW:
		r = 255, g = 250, b = 50;
		break;
	case GREEN:
		r = 55, g = 250, b = 50;
		break;
	case BLUE:
		r = 55, g = 50, b = 250;
		break;
	case CYAN:
		r = 55, g = 250, b = 250;
		break;
	case BLACK:
		r = 55, g = 55, b = 55;
		break;
	case AQUAMARIN:
		r = 127, g = 255, b = 212;
		break;
	case GOLD:
		r = 255, g = 215, b = 0;
		break;
	case OLIVE:
		r = 85, g = 107, b = 47;
		break;
	case SKYBLUE:
		r = 135, g = 206, b = 250;
		break;
	case MINT:
		r = 0, g = 206, b = 209;
		break;
	case VYOLET:
		r = 152, g = 85, b = 211;
		break;
	case PINK:
		r = 255, g = 182, b = 203;
		break;
	case WHITE:
		r = 255, g = 255, b = 255;
		break;
}
	*color = Scalar((255 - b) / 255.0 * alpha, (255 - g) / 255.0 * alpha, (255 - r) / 255.0 * alpha);
}


void ESMMovieGraphic::ARCircle::Draw(Mat plane)
{
	static int beta = 15;
	//SetColor(mColor_Spin_, &mSpin_rgb_);
	
	mThickness_ = 4;
	Point center = mCenter_;
	static int spin = mSpin_;
	static int rad;
	static int rad_percent=0.5;
	//static Scalar rgb = rgb_;
	static Scalar spin_rgb = mSpin_rgb_;

	//if (mCir_Bigger_Flag_)
	//	rad = mRadius_ * (rad_percent * t / mTime_Ani_ + rad_percent);
	////rad = double((1.0 - mRadius_start) * (t / mTime_ani_) + mRadius_start) * mRadius_;
	//else if (mCir_Smaller_Flag_)
	//	rad = ((mRadius_end_ - 1.0) * t / mTime_Ani_ + 1.0) * mRadius_;
	//else
		rad = mRadius_;
	static double hole = rad * (10 - mThickness_) / 10.0;
	
	switch (mCircle_type_)
	{
	case CIRCLE_SPIN:
		ellipse(plane, Point(center), Size(rad, rad), 0, 0, 360, _bgr, -1, LINE_AA);				//테두리
		ellipse(plane, Point(center), Size(rad + 2, rad + 2), spin - 30, 0, 60, Scalar(0, 0, 0), -1, LINE_AA);	//피자모양으로 자르기
		ellipse(plane, Point(center), Size(rad, rad), spin - 30, 0, 60, spin_rgb, -1, LINE_AA);		// 자른 영역에 다른색으로 채우기
		ellipse(plane, Point(center), Size(hole, hole), 0, 0, 360, Scalar(0, 0, 0), -1, LINE_AA);	//가운데 뚫어주기
		break;

	case CIRCLE_RING:
		ellipse(plane, Point(center), Size(rad, rad), 0, 0, 360, _bgr, -1, LINE_AA);
		ellipse(plane, Point(center), Size(hole, hole), 0, 0, 360, Scalar(0, 0, 0), -1, LINE_AA);
		break;

	case CIRCLE_DOTTED:
		ellipse(plane, Point(center), Size(rad, rad), 0, 0, 360, _bgr, -1, LINE_AA);
		for (int i = 0; i < 360 / beta; i++)
		{
			if (i % 2 == 0)
			{
				ellipse(plane, Point(center), Size(rad + 2, rad + 2), spin + beta * i, 0, beta * 0.6, Scalar(0, 0, 0), -1, LINE_AA);
			}
		}
		ellipse(plane, Point(center), Size(rad * 0.7, rad * 0.7), 0, 0, 360, Scalar(0, 0, 0), -1, LINE_AA);
		break;

	case CIRCLE_FILL:
		ellipse(plane, Point(center), Size(rad, rad), 0, 0, 360, _bgr, -1, LINE_AA);
		break;

	default:
		break;
	}
}

void ESMMovieGraphic::ARArrow::SetStartPos(Point& start) {
	mStart_ = start;
}

void ESMMovieGraphic::ARArrow::SetEndPos(Point& end) {
	mEnd_ = end;
}

void ESMMovieGraphic::ARArrow::Draw(Mat plane, int t)
{
	double time = mTime_Ani_;
	double d = 0;

	Point aniPoint;
	Point tip_start, tip_end;
	Point line_start, line_end;

	double radius = GetAbsValue(50);
	static double p = 25.0;
	static double dot = 20.0;
	thickness = mThick_ * 0.4;

	// Calcurate for angle (for drawing arrow tip) 
	if (mStart_.x == mEnd_.x)
	{
		if (mEnd_.y > mStart_.y)
			angle = CV_PI / 2.0;
		else
			angle = CV_PI * (3.0 / 2.0);
	}
	else {
		angle = atan(double(abs(mEnd_.y - mStart_.y)) / double(abs(mEnd_.x - mStart_.x)));
		if (mEnd_.x > mStart_.x && mEnd_.y >= mStart_.y)
		{
		}
		else if (mStart_.x > mEnd_.x && mEnd_.y >= mStart_.y)
		{
			angle = CV_PI - angle;
		}
		else if (mStart_.x > mEnd_.x && mStart_.y > mEnd_.y)
		{
			angle = CV_PI + angle;
		}
		else if (mEnd_.x > mStart_.x && mStart_.y > mEnd_.y)
		{
			angle = 2 * CV_PI - angle;
		}
	}

	//SetColor(mColor_, &rgb_);
	//static Scalar rgb = rgb_;

	//If SetStart flag true, Arrow and Circle set Pos Auto Calc
	if (mSetStart_Flag_)
	{
		tip_start.x = mStart_.x + radius * (sqrt(3) / 2.0) * cos(angle) + radius * cos(angle);
		tip_start.y = mStart_.y + radius * (sqrt(3) / 2.0) * sin(angle) + radius * sin(angle);
		line_start.x = mStart_.x + radius*1.3 * cos(angle);
		line_start.y = mStart_.y + radius*1.3 * sin(angle);
	}
	else
	{
		tip_start.x = mStart_.x + radius * (sqrt(3) / 2.0) * cos(angle);
		tip_start.y = mStart_.y + radius * (sqrt(3) / 2.0) * sin(angle);
		line_start.x = mStart_.x;
		line_start.y = mStart_.y;
	}

	if (mSetEnd_Flag_)
	{
		if (mLinedraw_Flag_ == true)
		{
			tip_end.x = mEnd_.x - radius * cos(angle);
			tip_end.y = mEnd_.y - radius * sin(angle);
			aniPoint.x = double((tip_start.x + (tip_end.x - tip_start.x) * ((1.0 - d) * pow((t + 1) / time, 2) + d)));
			aniPoint.y = double((tip_start.y + (tip_end.y - tip_start.y) * ((1.0 - d) * pow((t + 1) / time, 2) + d)));
			tip_end = aniPoint;
		}
		else
		{
			tip_end.x = mEnd_.x - radius * cos(angle);
			tip_end.y = mEnd_.y - radius * sin(angle);
		}
		line_end.x = tip_end.x - radius * (sqrt(3) / 2.0) * cos(angle);
		line_end.y = tip_end.y - radius * (sqrt(3) / 2.0) * sin(angle);
	}
	else
	{
		if (mLinedraw_Flag_ == true)
		{
			aniPoint.x = double((tip_start.x + (mEnd_.x - tip_start.x) * ((1.0 - d) * pow((t + 1) / time, 2) + d)));
			aniPoint.y = double((tip_start.y + (mEnd_.y - tip_start.y) * ((1.0 - d) * pow((t + 1) / time, 2) + d)));
			tip_end = aniPoint;
		}
		else
		{
			tip_end.x = mEnd_.x;
			tip_end.y = mEnd_.y;

		}
		line_end.x = tip_end.x - radius * (sqrt(3) / 2.0) * cos(angle);
		line_end.y = tip_end.y - radius * (sqrt(3) / 2.0) * sin(angle);
	}


	Point pts[3];
	double len;
	len = sqrt(pow(line_end.x - line_start.x, 2) + pow(line_end.y - line_start.y, 2));

	int tip_angle = 30;
	//Arrow tip position
	triEx = tip_end.x;
	triEy = tip_end.y;
	trip1x = triEx - radius * cos(angle + tip_angle * CV_PI / 180);
	trip1y = triEy - radius * sin(angle + tip_angle * CV_PI / 180);
	trip2x = triEx - radius * cos(angle - tip_angle * CV_PI / 180);
	trip2y = triEy - radius * sin(angle - tip_angle * CV_PI / 180);
	pts[0] = Point(triEx, triEy);
	pts[1] = Point(trip1x, trip1y);
	pts[2] = Point(trip2x, trip2y);
	const Point* ppt[1] = { pts };
	int npt[] = { 3 };
	double move_end_X = (GetAbsValue(50) / 1.5) * cos(angle);
	double move_end_Y = (GetAbsValue(50) / 1.5) * sin(angle);



	switch (mArrow_type)
	{
	case SOLID:
		line(plane, line_start, line_end, _bgr, mThick_, LINE_AA);
		fillPoly(plane, ppt, npt, 1, _bgr, LINE_AA);
		break;

	case DOTTED:
		fillPoly(plane, ppt, npt, 1, _bgr, LINE_AA);
		for (int i = 1; i < len / dot; i++)
			circle(plane, Point(line_start.x + (i * dot * cos(angle)), line_start.y + (i * dot * sin(angle))), radius / 5.0, _bgr, -1, LINE_AA);
		break;

	default:
		break;
	}
}

void ESMMovieGraphic::ARTriangle::Draw(Mat plane)
{
	//SetColor(mColor_, &rgb_);
	//static Scalar rgb = rgb_;
	Point pts[3];
	pts[0] = mWarpPoint_[0];
	pts[1] = mWarpPoint_[1];
	pts[2] = mWarpPoint_[2];

	const Point* ppt[1] = { pts };
	int npt[] = { 3 };

	fillPoly(plane, ppt, npt, 1, _bgr, 8);
}



