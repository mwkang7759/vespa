﻿#include "TemplateCalculator.h"



double TemplateCalculator::dist(const Point2f& p1, const Point2f& p2) {
    return (p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y);
}

double TemplateCalculator::euclideanDistance(const Point2f& p1, const Point2f& p2) {
    return sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
}

int TemplateCalculator::ccw(const Point2f& p1, const Point2f& p2, const Point2f& p3)
{
    int cross_product = (p2.x - p1.x) * (p3.y - p1.y) - (p3.x - p1.x) * (p2.y - p1.y);

    if (cross_product > 0) {
        return 1;
    }
    else if (cross_product < 0) {
        return -1;
    }
    else {
        return 0;
    }
}

// right가 left의 반시계 방향에 있으면 true이다.
// true if right is counterclockwise to left.
int TemplateCalculator::SortComparator(const Point2f& pt, const Point2f& left, const Point2f& right)
{
    int ret;
    int direction = ccw(pt, left, right);
    if (direction == 0) {
        ret = (dist(pt, left) < dist(pt, right));
    }
    else if (direction == 1) {
        ret = 1;
    }
    else {
        ret = 0;
    }
    return ret;
}

void TemplateCalculator::QuickSort(Point2f* a, int lo, int hi)
{
    if (hi - lo <= 0) {
        return;
    }

    // 현재 배열 범위의 중앙값을 피벗으로 선택한다.
    // Select the median as pivot in the current array range.
    Point2f pivot = a[lo + (hi - lo + 1) / 2];
    int i = lo, j = hi;

    // 정복 과정
    // Conquer process
    while (i <= j) {
        // 피벗의 왼쪽에는 comparator(타겟, "피벗")을 만족하지 않는 인덱스를 선택 (i)
        // On the left side of the pivot, select an index that doesn't satisfy the comparator(target, "pivot"). (i)
        while (SortComparator(a[0], a[i], pivot)) i++;

        // 피벗의 오른쪽에는 comparator("피벗", 타겟)을 만족하지 않는 인덱스를 선택 (j)
        // On the right side of the pivot, select an index that doesn't satisfy the comparator("pivot", target). (j)
        while (SortComparator(a[0], pivot, a[j])) j--;
        // (i > j) 피벗의 왼쪽에는 모든 값이 피벗보다 작고 피벗의 오른쪽에는 모든 값이 피벗보다 큰 상태가 되었음.
        // (i > j) On the left side of the pivot, all values are smaller than the pivot, and on the right side of the pivot, all values are larger than the pivot.
        if (i > j) {
            break;
        }

        // i번째 값은 피벗 보다 크고 j번째 값은 피벗보다 작으므로 두 값을 스왑한다.
        // The i-th value is larger than the pivot and the j-th value is smaller than the pivot, so swap the two values.
        Point2f temp = a[i];
        a[i] = a[j];
        a[j] = temp;

        // 인덱스 i를 1증가 시키고 인덱스 j를 1 감소 시켜서 탐색 범위를 안쪽으로 좁힌다.
        // Narrow the search inward by increasing index i by one and decreasing index j by one.
        i++;
        j--;
    }

    // 분할 과정
    // Divide process
    QuickSort(a, lo, j);
    QuickSort(a, i, hi);
}



// right가 left 보다 크면 true를 반환합니다.
// if right is greater than left, it is true
int TemplateCalculator::LineComparator(Point2f left, Point2f right)
{
    int ret;
    if (left.x == right.x) {
        ret = (left.y <= right.y);
    }
    else {
        ret = (left.x <= right.x);
    }
    return ret;
}

// 입력받은 Point 구조체의 값을 교환합니다.
// Exchanges the value of the input Point structure.
void TemplateCalculator::swap(Point2f& p1, Point2f& p2)
{
    Point2f temp;
    temp = p1;
    p1 = p2;
    p2 = temp;
}

// 두 선분이 교차하면 1을 교차하지 않으면 0을 반환합니다.
// If the two segments intersect, they will return 1 if they do not intersect 0.
int TemplateCalculator::LineIntersection(CvLine l1, CvLine l2)
{
    int ret;
    // l1을 기준으로 l2가 교차하는 지 확인한다.
    // Check if l2 intersects based on l1.
    int l1_l2 = ccw(l1.p1, l1.p2, l2.p1) * ccw(l1.p1, l1.p2, l2.p2);
    // l2를 기준으로 l1이 교차하는 지 확인한다.
    // Check if l1 intersects based on l2.
    int l2_l1 = ccw(l2.p1, l2.p2, l1.p1) * ccw(l2.p1, l2.p2, l1.p2);

    ret = (l1_l2 < 0) && (l2_l1 < 0);

    return ret;
}

int TemplateCalculator::PolygonInOut(Point2f p, int num_vertex, Point2f* vertices)
{

    int ret = 0;

    // 마지막 꼭지점과 첫번째 꼭지점이 연결되어 있지 않다면 오류를 반환한다.
    // If the last vertex and the first vertex are not connected, an error is returned.
    if (vertices[0].x != vertices[num_vertex].x || vertices[0].y != vertices[num_vertex].y) {
        printf("Last vertex and first vertex are not connected.");
        return -1;
    }

    for (int i = 0; i < num_vertex; ++i) {

        // 점 p가 i번째 꼭지점과 i+1번째 꼭지점을 이은 선분 위에 있는 경우
        // If point p is on the line connecting the i and i + 1 vertices
        if (ccw(vertices[i], vertices[i + 1], p) == 0) {
            int min_x = FD_MIN(vertices[i].x, vertices[i + 1].x);
            int max_x = FD_MAX(vertices[i].x, vertices[i + 1].x);
            int min_y = FD_MIN(vertices[i].y, vertices[i + 1].y);
            int max_y = FD_MAX(vertices[i].y, vertices[i + 1].y);

            // 점 p가 선분 내부의 범위에 있는 지 확인
            // Determine if point p is in range within line segment
            if (min_x <= p.x && p.x <= max_x && min_y <= p.y && p.y <= max_y) {
                return 1;
            }
        }
        else { ; }
    }

    // 다각형 외부에 임의의 점과 점 p를 연결한 선분을 만든다.
    // Create a line segment connecting a random point outside the polygon and point p.
    Point2f outside_point;
    outside_point.x = 1; outside_point.y = 1234567;
    CvLine l1;
    l1.p1 = outside_point;
    l1.p2 = p;

    // 앞에서 만든 선분과 다각형을 이루는 선분들이 교차되는 갯수가 센다.
    // Count the number of intersections between the previously created line segments and the polygonal segments.
    for (int i = 0; i < num_vertex; ++i) {
        CvLine l2;
        l2.p1 = vertices[i];
        l2.p2 = vertices[i + 1];
        ret += LineIntersection(l1, l2);
    }

    // 교차한 갯수가 짝수인지 홀수인지 확인한다.
    // Check if the number of crossings is even or odd.
    ret = ret % 2;
    return ret;
}

bool TemplateCalculator::isInside(Point2f B, const vector<Point2f>& p)
{
    //crosses는 점q와 오른쪽 반직선과 다각형과의 교점의 개수
    int crosses = 0;
    for (int i = 0; i < p.size(); i++) {
        int j = (i + 1) % p.size();
        //점 B가 선분 (p[i], p[j])의 y좌표 사이에 있음
        if ((p[i].y > B.y) != (p[j].y > B.y)) {
            //atX는 점 B를 지나는 수평선과 선분 (p[i], p[j])의 교점
            double atX = (p[j].x - p[i].x) * (B.y - p[i].y) / (p[j].y - p[i].y) + p[i].x;
            //atX가 오른쪽 반직선과의 교점이 맞으면 교점의 개수를 증가시킨다.
            if (B.x < atX)
                crosses++;
        }
    }
    return crosses % 2 > 0;
}

// (p1, p2)를 이은 직선과 (p3, p4)를 이은 직선의 교차점을 구하는 함수
// Function to get intersection point with line connecting points (p1, p2) and another line (p3, p4).
Point2f TemplateCalculator::IntersectionPoint(const Point2f& p1, const Point2f& p2, const Point2f& p3, const Point2f& p4)
{
    Point2f ret;
    ret.x = ((p1.x * p2.y - p1.y * p2.x) * (p3.x - p4.x) - (p1.x - p2.x) * (p3.x * p4.y - p3.y * p4.x)) / ((p1.x - p2.x) * (p3.y - p4.y) - (p1.y - p2.y) * (p3.x - p4.x));
    ret.y = ((p1.x * p2.y - p1.y * p2.x) * (p3.y - p4.y) - (p1.y - p2.y) * (p3.x * p4.y - p3.y * p4.x)) / ((p1.x - p2.x) * (p3.y - p4.y) - (p1.y - p2.y) * (p3.x - p4.x));

    return ret;
}

// 다각형의 넓이를 구한다.
// find the area of a polygon
double TemplateCalculator::GetPolygonArea(vector<Point2f> &points)
{
    double ret = 0;
    int i, j;
    i = points.size() - 1;
    for (j = 0; j < points.size(); ++j) {
        ret += points[i].x * points[j].y - points[j].x * points[i].y;
        i = j;
    }
    ret = ret < 0 ? -ret : ret;
    ret /= 2;

    return ret;
}

vector<Point2f> TemplateCalculator::GetIntersection(vector<Point2f> points1, vector<Point2f> points2)
{
    vector<Point2f> vInterPts;
    double ret;
    CvLine l1, l2;

    // points1과 points2 각각을 반시계방향으로 정렬한다.
    // sort by counter clockwise point1 and points2.
    QuickSort(points1.data(), 1, points1.size() - 1);

    QuickSort(points2.data(), 1, points2.size() - 1);

    // 차례대로 점들을 이었을 때, 다각형이 만들어질 수 있도록 시작점을 마지막에 추가한다.
    // Add the starting point to the last in order to make polygon when connect points in order.
    points1.push_back(points1[0]);
    points2.push_back(points2[0]);

    // points1의 다각형 선분들과 points2의 다각형 선분들의 교차점을 구한다.
    // Find the intersection of the polygon segments of points1 and the polygon segments of points2.
    for (int i = 0; i < points1.size() - 1; ++i) {
        l1.p1 = points1[i];
        l1.p2 = points1[i + 1];
        for (int j = 0; j < points2.size() - 1; ++j) {
            l2.p1 = points2[j];
            l2.p2 = points2[j + 1];

            // 선분 l1과 l2가 교차한다면 교차점을 intersection_point에 저장한다.
            // If line segments l1 and l2 intersect, store the intersection at intersection_point.
            if (LineIntersection(l1, l2)) {
                vInterPts.push_back(IntersectionPoint(l1.p1, l1.p2, l2.p1, l2.p2));
            }
        }
    }

    for (int i = 0; i < points1.size() - 1; i++)
    {
        if (isInside(points1[i], points2))
        {
            vInterPts.push_back(points1[i]);
        }
    }
    for (int i = 0; i < points2.size() - 1; i++)
    {
        if (isInside(points2[i], points1))
        {
            vInterPts.push_back(points2[i]);
        }
    }

    QuickSort(vInterPts.data(), 1, vInterPts.size() - 1);

    return vInterPts;
}

bool TemplateCalculator::checkInteriorExterior(const cv::Mat& mask, const cv::Rect& interiorBB, int& top, int& bottom, int& left, int& right)
{
    // return true if the rectangle is fine as it is!
    bool returnVal = true;

    cv::Mat sub = mask(interiorBB);

    unsigned int x = 0;
    unsigned int y = 0;

    // count how many exterior pixels are at the
    unsigned int cTop = 0; // top row
    unsigned int cBottom = 0; // bottom row
    unsigned int cLeft = 0; // left column
    unsigned int cRight = 0; // right column
    // and choose that side for reduction where mose exterior pixels occured (that's the heuristic)

    for (y = 0, x = 0; x < sub.cols; ++x)
    {
        // if there is an exterior part in the interior we have to move the top side of the rect a bit to the bottom
        if (sub.at<unsigned char>(y, x) == 0)
        {
            returnVal = false;
            ++cTop;
        }
    }

    for (y = sub.rows - 1, x = 0; x < sub.cols; ++x)
    {
        // if there is an exterior part in the interior we have to move the bottom side of the rect a bit to the top
        if (sub.at<unsigned char>(y, x) == 0)
        {
            returnVal = false;
            ++cBottom;
        }
    }

    for (y = 0, x = 0; y < sub.rows; ++y)
    {
        // if there is an exterior part in the interior
        if (sub.at<unsigned char>(y, x) == 0)
        {
            returnVal = false;
            ++cLeft;
        }
    }

    for (x = sub.cols - 1, y = 0; y < sub.rows; ++y)
    {
        // if there is an exterior part in the interior
        if (sub.at<unsigned char>(y, x) == 0)
        {
            returnVal = false;
            ++cRight;
        }
    }

    // that part is ugly and maybe not correct, didn't check whether all possible combinations are handled. Check that one please. The idea is to set `top = 1` iff it's better to reduce the rect at the top than anywhere else.
    if (cTop > cBottom)
    {
        if (cTop > cLeft)
            if (cTop > cRight)
                top = 1;
    }
    else
        if (cBottom > cLeft)
            if (cBottom > cRight)
                bottom = 1;

    if (cLeft >= cRight)
    {
        if (cLeft >= cBottom)
            if (cLeft >= cTop)
                left = 1;
    }
    else
        if (cRight >= cTop)
            if (cRight >= cBottom)
                right = 1;



    return returnVal;
}

bool TemplateCalculator::sortX(cv::Point2f a, cv::Point2f b)
{
    return a.x < b.x;
}

bool TemplateCalculator::sortY(cv::Point2f a, cv::Point2f b)
{
    return a.y < b.y;
}

Point2f TemplateCalculator::GetCentroidPolygon(const std::vector<cv::Point2f>& poly)
{
    if (poly.size() == 0)
        return Point2f(-1.f, -1.f);

    float cx(0), cy(0), area(0);
    for (int i = 0; i < poly.size(); i++)
    {
        Point2f pt1 = poly[i];
        Point2f pt2 = poly[(i + 1) % poly.size()];

        area += pt1.x * pt2.y;
        area -= pt1.y * pt2.x;

        cx += ((pt1.x + pt2.x) * ((pt1.x * pt2.y) - pt2.x * pt1.y));
        cy += ((pt1.y + pt2.y) * ((pt1.x * pt2.y) - pt2.x * pt1.y));
    }

    area /= 2.0;
    area = fabs(area);

    return Point2f(cx / (6.0 * area), cy / (6.0 * area));
}

Point2f TemplateCalculator::GetFarthestPoint(const std::vector<cv::Point2f>& poly, const Point2f pt)
{
    double dLongest(0);
    Point2f ptResult;

    for (int i = 0; i < poly.size(); i++)
    {
        double _dist = euclideanDistance(poly[i], pt);
        if (dLongest < _dist)
        {
            dLongest = _dist;
            ptResult = poly[i];
        }
    }

    return ptResult;
}

Rect2f TemplateCalculator::GetRectangleInsidePolygon(int nWidth, int nHeight, vector<Point2f> poly)
{

    vector<vector<Point>> contour(1, vector<Point>());

    for (int i = 0; i < poly.size(); i++)
    {
        contour[0].push_back(Point(poly[i].x, poly[i].y));
    }

    const cv::Point *pts = (const cv::Point*)Mat(contour[0]).data;
    int nptsI = poly.size();

    cv::Mat contourMask = cv::Mat::zeros(Size(nWidth, nHeight), CV_8UC1);
    fillPoly(contourMask, &pts, &nptsI, 1, Scalar(255));

    // sort contour in x/y directions to easily find min/max and next
    std::vector<cv::Point2f> cSortedX = poly;
    std::sort(cSortedX.begin(), cSortedX.end(), TemplateCalculator::sortX);

    std::vector<cv::Point2f> cSortedY = poly;
    std::sort(cSortedY.begin(), cSortedY.end(), TemplateCalculator::sortY);


    unsigned int minXId = 0;
    unsigned int maxXId = cSortedX.size() - 1;

    unsigned int minYId = 0;
    unsigned int maxYId = cSortedY.size() - 1;

    cv::Rect interiorBB;

    while ((minXId < maxXId) && (minYId < maxYId))
    {
        cv::Point min(cSortedX[minXId].x, cSortedY[minYId].y);
        cv::Point max(cSortedX[maxXId].x, cSortedY[maxYId].y);

        interiorBB = cv::Rect(min.x, min.y, max.x - min.x, max.y - min.y);


        if (interiorBB.x < 0)
            interiorBB.x = 0;
        if (interiorBB.x + interiorBB.width > nWidth - 1)
            interiorBB.width = nWidth - interiorBB.x - 1;
        if (interiorBB.y < 0)
            interiorBB.y = 0;
        if (interiorBB.y + interiorBB.height > nHeight - 1)
            interiorBB.height = nHeight - interiorBB.y - 1;

        // out-codes: if one of them is set, the rectangle size has to be reduced at that border
        int ocTop = 0;
        int ocBottom = 0;
        int ocLeft = 0;
        int ocRight = 0;

        bool finished = checkInteriorExterior(contourMask, interiorBB, ocTop, ocBottom, ocLeft, ocRight);
        if (finished)
        {
            break;
        }

        // reduce rectangle at border if necessary
        if (ocLeft)++minXId;
        if (ocRight) --maxXId;

        if (ocTop) ++minYId;
        if (ocBottom)--maxYId;
    }

    Rect2f rtResult = interiorBB;

    if ((double)rtResult.width / rtResult.height > (double)nWidth / nHeight)
    {
        double centerX = (rtResult.x + rtResult.width) / 2;
        rtResult.width = rtResult.height * ((double) nWidth / nHeight);
        rtResult.x = centerX - rtResult.width / 2;
    }
    else
    {
        double centerY = (rtResult.y + rtResult.height) / 2;
        rtResult.height = rtResult.width / ((double) nWidth / nHeight);
        rtResult.y = centerY - rtResult.height / 2;
    }

    return rtResult;
}

Rect2f TemplateCalculator::GetCentroidRectangleInsidePolygon(const vector<Point2f>& ptsPoly, const double fRatioWH)
{
    Point2f ptCenter = GetCentroidPolygon(ptsPoly);

    Point2f ptFarthest = GetFarthestPoint(ptsPoly, ptCenter);

    double dIncW, dIncH;
    if (fRatioWH > 1.) { dIncW = 1.;  dIncH = 1. / fRatioWH; }
    else { dIncW = fRatioWH;  dIncH = 1.; }

    double dX, dY;
    if (abs(ptCenter.x - ptFarthest.x) / abs(ptCenter.y - ptFarthest.y) > fRatioWH)
    {
        dX = abs(ptCenter.x - ptFarthest.x) - dIncW;
        dY = abs(ptCenter.x - ptFarthest.x) / fRatioWH - dIncH;
    }
    else
    {
        dX = abs(ptCenter.y - ptFarthest.y) * fRatioWH - dIncW;
        dY = abs(ptCenter.y - ptFarthest.y) - dIncH;
    }

    while (dX > 1. || dY > 1.)
    {
        vector<Point2f> corners;
        corners.push_back(Point2f(ptCenter.x - dX, ptCenter.y - dY));
        corners.push_back(Point2f(ptCenter.x + dX, ptCenter.y - dY));
        corners.push_back(Point2f(ptCenter.x + dX, ptCenter.y + dY));
        corners.push_back(Point2f(ptCenter.x - dX, ptCenter.y + dY));

        bool bInt = false;
        for (int i = 0; i < corners.size(); i++)
        {
            CvLine sideR = { corners[i], corners[(i + 1) % corners.size()] };

            for (int j = 0; j < ptsPoly.size(); j++)
            {
                CvLine sideP = { ptsPoly[j], ptsPoly[(j + 1) % ptsPoly.size()] };

                if (LineIntersection(sideR, sideP) == 1)
                {
                    bInt = true;
                    break;
                }
            }
            if (bInt)    break;
        }

        if (!bInt)
        {
            Rect2f rtInside;
            rtInside.x = ptCenter.x - dX;
            rtInside.y = ptCenter.y - dY;
            rtInside.width = dX * 2 + 1;
            rtInside.height = dY * 2 + 1;

            return rtInside;
        }

        dX -= dIncW;
        dY -= dIncH;
    }
    

    return Rect2f();
}
