﻿#include "ESMMovieMaker.h"
#include "ESMMovieMakerCore.h"

ESMMovieMaker::ESMMovieMaker(VOID)
{
	_core = new ESMMovieMaker::Core();
}

ESMMovieMaker::~ESMMovieMaker(VOID)
{
	if (_core) 	{
		if (_core->IsInitialized())
			_core->Release();
		delete _core;
		_core = NULL;
	}
}

BOOL ESMMovieMaker::IsInitialized(VOID)
{
	return _core->IsInitialized();
}

int32_t ESMMovieMaker::Initialize(ESMMovieMaker::CONTEXT_T* ctx)
{
	return _core->Initialize(ctx);
}

int32_t ESMMovieMaker::Release(VOID)
{
	return _core->Release();
}

int32_t	ESMMovieMaker::Process(ESMMovieMaker::PARAM_T* param)
{
	return _core->Process(param);
}