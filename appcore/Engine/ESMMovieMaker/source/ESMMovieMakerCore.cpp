﻿#include "ESMMovieMakerCore.h"
#include <ESMLocks.h>
#include "cuda_runtime.h"

#include "opencv2/cudawarping.hpp"
#include "opencv2/cudaarithm.hpp"
#include "MatrixCalculator.h"

#define CONV_TEST
//#define PNG_DUMP
#include "FrCvClrConv.h"
#include "TraceAPI.h"

#if defined(_DEBUG)
#pragma comment(lib, "opencv_core440d.lib")
#pragma comment(lib, "opencv_highgui440d.lib")
#pragma comment(lib, "opencv_imgproc440d.lib")
#pragma comment(lib, "opencv_imgcodecs440d.lib")
#pragma comment(lib, "opencv_calib3d440d.lib")
#pragma comment(lib, "opencv_cudaimgproc440d.lib")
#pragma comment(lib, "opencv_cudawarping440d.lib")
#pragma comment(lib, "opencv_cudaarithm440d.lib")
#else
#pragma comment(lib, "opencv_core440.lib")
#pragma comment(lib, "opencv_highgui440.lib")
#pragma comment(lib, "opencv_imgproc440.lib")
#pragma comment(lib, "opencv_imgcodecs440.lib")
#pragma comment(lib, "opencv_calib3d440.lib")
#pragma comment(lib, "opencv_cudaimgproc440.lib")
#pragma comment(lib, "opencv_cudawarping440.lib")
#pragma comment(lib, "opencv_cudaarithm440.lib")
#endif


ESMMovieMaker::Core::Core(VOID)
	: _encodeInput(nullptr)
	, _encodeInputPitch(0)
	, _encodeIndex(0)
{
	_isInitialized = FALSE;

	_bitstream = nullptr;
	_bitstreamCapacity = 0;

	_context = nullptr;
	_param = nullptr;

	_frameCount = 0;
	_seekTime = 0;
	_gopSize = 0;
	_startGopNum = 0;
	_globalCnt = 0;
	_localStartCnt = 0;
	_localEndCnt = 0;

#if defined(WITH_SMOOTH_FILTER)
	_smooth = nullptr;
#endif

	_bReverse = false;

	_recal = false;
	_conn = nullptr; //20211005 Kelly add for Recal

	_fw_use_qpc = QueryPerformanceFrequency(&_fw_frequency);
}

ESMMovieMaker::Core::~Core(VOID)
{

}

BOOL ESMMovieMaker::Core::IsInitialized(VOID)
{
	return _isInitialized;
}

int32_t ESMMovieMaker::Core::Initialize(ESMMovieMaker::CONTEXT_T* ctx)
{
	int32_t status = ESMMovieMaker::ERR_CODE_T::GENERIC_FAIL;
	if (ctx == NULL)
		return ESMMovieMaker::ERR_CODE_T::INVALID_PARAMETER;

	_context = ctx;
	_isInitialized = TRUE;

	//20211005 Kelly add for Recal
	_conn = new ESMConnector();
	_recal_mode = FALSE;

	//20211217 Hyena add for AR

	double abs_circle, abs_arr;
	//abs_circle = ESMMovieGraphic::ARShape::GetAbsValue(50);
	//abs_arr = ESMMovieGraphic::ARShape::GetAbsValue(50);

#if 0
	for (int p = 0; p < 10; p++)
	{
		//selPoint[p] = Point2d(_fdPoint[p].x, _fdPoint[p].y);
		selPoint.push_back(Point2d(_fdPoint[p].x, _fdPoint[p].y));
		//ESMMovieGraphic::CalcWarpPoint(selPoint[p], &wPoint[p], _fdPoint[p].prefix);
	}
	//ESMMovieGraphic::ARShape::mAbs_val_() = 30;
	//_arrow[0].mStart_ = wPoint[1];
	_arrow[0].mStart_ = Point(299, 399);
	_arrow[0].mEnd_ = Point(500, 600);
	_arrow[0].mColor_ = ESMMovieGraphic::ARShape::AQUAMARIN;
	_arrow[0].mArrow_type = ESMMovieGraphic::ARArrow::SOLID;
	_arrow[0].mAlpha_max_ = 80;
	_arrow[0].mThick_ = abs_arr * 0.4;

	_circle[0].mCenter_ = Point(600, 400);
	_circle[0].mColor_ = ESMMovieGraphic::ARShape::GOLD;
	_circle[0].mColor_Spin_ = ESMMovieGraphic::ARShape::YELLOW;
	_circle[0].mCircle_type_ = ESMMovieGraphic::ARCircle::CIRCLE_FILL;
	_circle[0].mAlpha_max_ = 70;
	_circle[0].mRadius_ = abs_circle;
	_circle[0].mRadius_start_ = 20;

	_triangle[0].mWarpPoint_ = { Point(111,222),Point(222,222),Point(333,444) };
	_triangle[0].mAlpha_max_ = 100;
	_triangle[0].mColor_ = ESMMovieGraphic::ARShape::GOLD;
#endif

	// add pose
	_infer = nullptr;
	_pose_mode = false;

	status = ESMMovieMaker::ERR_CODE_T::SUCCESS;
	return status;
}

int32_t ESMMovieMaker::Core::Release(VOID)
{
	int res;
	
	// add pose
	if (_infer && _infer->isInitialized()) {
		_infer->release();
		_infer = nullptr;
	}

	if (_encoder.IsInitialized())
	{
		if (_encodeInput)
		{
			cudaFree(_encodeInput);
			_encodeInput = nullptr;
		}
		if (_bitstream)
		{
			free(_bitstream);
			_bitstream = nullptr;
		}

		res = _encoder.Release();
		if (res)
			SPd_ERROR("_encoder.Release()={}", res);
	}

	if (_converter.IsInitialized())
	{
		res = _converter.Release();
		if (res)
			SPd_ERROR("_converter.Release()={}", res);
	}
	if (_decoder.IsInitialized())
	{
		res = _decoder.Release();
		if (res)
			SPd_ERROR("_decoder.Release()={}", res);
	}

	delete _conn; //20211005 Kelly add for Recal
	_recal_mode = FALSE;

	_isInitialized = FALSE;
	return ESMMovieMaker::ERR_CODE_T::SUCCESS;
}

int32_t ESMMovieMaker::Core::Process(ESMMovieMaker::PARAM_T* param)
{
	int32_t status = ESMMovieMaker::ERR_CODE_T::GENERIC_FAIL;
	if (!_isInitialized || !param)
		return ESMMovieMaker::ERR_CODE_T::UNINITIALIZED_ERROR;

	_param = param;

	SPd_DEBUG("pointInfo.size = {}", param->pointInfo.size());

	for (auto& points : param->pointInfo)
	{
		SPd_DEBUG("pointInfo[camid:{}] - presd_path:{}/dsc_id:{}/framenum:{}/camfps:{}",
			points.first,
			points.second.presd_path,
			points.second.dsc_id,
			points.second.framenum,
			points.second.camfps);

		_demuxerCtx.mppc = true;
		_demuxerCtx.dsc_id = points.second.dsc_id;
		strncpy_s(_demuxerCtx.path, points.second.presd_path.c_str(), MAX_PATH);
		_demuxerCtx.nSrcGopSize = points.second.camfps == 60 ? 12 : 10;
		_demuxerCtx.handler = this;
		_recal_dscid.clear();
		_startGopNum = 1;
		_localStartCnt = 1;
		_localEndCnt = 2;

		//_startGopNum = 0;
		//_localStartCnt = 1;
		//_localEndCnt = 1;

		_demuxerCtx.fWantedFPS = points.second.camfps;
		_demuxerCtx.nStartGopNum = _startGopNum;
		_demuxerCtx.nStartFrameNum = _localStartCnt;
		_demuxerCtx.nEndFrameNum = _localEndCnt;

		SPd_DEBUG("_demuxerCtx - nSrcGopSize:{}, fWantedFPS:{}, nStartGopNum:{}, nStartFrameNum:{}, nEndFrameNum:{}",
			_demuxerCtx.nSrcGopSize,
			_demuxerCtx.fWantedFPS,
			_demuxerCtx.nStartGopNum,
			_demuxerCtx.nStartFrameNum,
			_demuxerCtx.nEndFrameNum);

		status = _demuxer.Demux(&_demuxerCtx);

		SPd_DEBUG("Demux={}", status);

		ProceedDemuxError(status);
	}

    _demuxerCtx.mppc = false;
	strncpy_s(_demuxerCtx.path, param->srcFile, MAX_PATH);
	//_demuxerCtx.nStartFrameNum = param->nStartFrameNum;
	//_demuxerCtx.nEndFrameNum = param->nEndFrameNum;
	_demuxerCtx.nSrcGopSize = param->nSrcGopSize;
	_demuxerCtx.handler = this;

	if (param->nStartFrameNum > param->nEndFrameNum)
	{
		// TODO: Time reverse
		//_bReverse = true;
	}
	else
	{
		_startGopNum = param->frameInfo[param->nStartFrameNum].matchingFrameIdx / param->nSrcGopSize;
		_localStartCnt = param->frameInfo[param->nStartFrameNum].matchingFrameIdx;
		_localEndCnt = param->frameInfo[param->nEndFrameNum].matchingFrameIdx;
		_globalCnt = param->nStartFrameNum - 1;
	}

	_demuxerCtx.fWantedFPS = param->fWantedFPS;
	_demuxerCtx.nStartGopNum = _startGopNum;
	_demuxerCtx.nStartFrameNum = _localStartCnt;
	_demuxerCtx.nEndFrameNum = _localEndCnt;

	LOG_I("[Worker] Demux() srcGopSize(%d), wantedFps(%d), startGopNum(%d), startFrameNum(%d), endFrameNum(%d), _globalCnt(%d)",
		_demuxerCtx.nSrcGopSize, _demuxerCtx.fWantedFPS, _demuxerCtx.nStartGopNum, _demuxerCtx.nStartFrameNum, _demuxerCtx.nEndFrameNum, _globalCnt);

	SPd_DEBUG("_demuxerCtx - nSrcGopSize:{}, fWantedFPS:{}, nStartGopNum:{}, nStartFrameNum:{}, nEndFrameNum:{}",
		_demuxerCtx.nSrcGopSize,
		_demuxerCtx.fWantedFPS,
		_demuxerCtx.nStartGopNum,
		_demuxerCtx.nStartFrameNum,
		_demuxerCtx.nEndFrameNum);

	status = _demuxer.Demux(&_demuxerCtx);

	SPd_DEBUG("Demux={}", status);

	ProceedDemuxError(status);
	return status;
}

void ESMMovieMaker::Core::ProceedDemuxError(int32_t status)
{	
	char err[MAX_PATH] = { 0 };
	switch (status)
	{
	case ESMMovieMaker::ERR_CODE_T::SUCCESS:
		break;
	case ESMMovieMaker::ERR_CODE_T::OPEN_INPUT_ERROR:
		//SPd_ERROR("[ESMMovieMaker] Media File is not Exist[{}]", _demuxerCtx.path);
		if (_context && _context->handler)
		{
			_snprintf_s(err, sizeof(err), "[ESMMovieMaker] Media File is not Exist[%s]", _demuxerCtx.path);
			_context->handler->onMovieMakerError(status, err);
		}
		break;
	case ESMMovieMaker::ERR_CODE_T::FIND_STREAM_INFO_ERROR:
		//SPd_ERROR("[ESMMovieMaker] Media File Does Not Contain Video[{}]", _demuxerCtx.path);
		if (_context && _context->handler)
		{
			_snprintf_s(err, sizeof(err), "[ESMMovieMaker] Media File Does Not Contain Video[%s]", _demuxerCtx.path);
			_context->handler->onMovieMakerError(status, err);
		}
		break;
	case ESMMovieMaker::ERR_CODE_T::STREAM_INDEX_ERROR:
		//SPd_ERROR("[ESMMovieMaker] Media File Does Not Contain Video[{}]", _demuxerCtx.path);
		if (_context && _context->handler)
		{
			_snprintf_s(err, sizeof(err), "[ESMMovieMaker] Media File Does Not Contain Video[%s]", _demuxerCtx.path);
			_context->handler->onMovieMakerError(status, err);
		}
		break;
	case ESMMovieMaker::ERR_CODE_T::GOPSIZE_IS_NOT_EQUAL:
		//SPd_ERROR("[ESMMovieMaker] Media File FPS IS Not Same Value Required{}", _param->nSrcGopSize);
		if (_context && _context->handler)
		{
			_snprintf_s(err, sizeof(err), "[ESMMovieMaker] Media File FPS IS Not Same Value Required %d", _param->nSrcGopSize);
			_context->handler->onMovieMakerError(status, err);
		}
		break;
	case ESMMovieMaker::ERR_CODE_T::FRAMECOUNT_DIFFER_FROM_FPS:
		//SPd_ERROR("[ESMMovieMaker] Media File FrameCount Differ From FPS{}", (int32_t)ceil(_param->fWantedFPS));
		if (_context && _context->handler)
		{
			_snprintf_s(err, sizeof(err), "[ESMMovieMaker] Media File FrameCount Differ From FPS %d (videoFrameCount=%d)",
				(int32_t)ceil(_param->fWantedFPS), _demuxerCtx.videoFrameCount);
			_context->handler->onMovieMakerError(status, err);
		}
		break;
	case ESMMovieMaker::ERR_CODE_T::FILEWRITE_ERROR:
		//SPd_ERROR("[ESMMovieMaker] Making File Source can not be Created, Check MMd Server Disk Space");
		if (_context && _context->handler)
		{
			_snprintf_s(err, sizeof(err), "[ESMMovieMaker] Making File Source can not be Created, Check MMd Server Disk Space");
			_context->handler->onMovieMakerError(status, err);
		}
		break;
	case ESMMovieMaker::ERR_CODE_T::GENERIC_FAIL:
		//SPd_ERROR("[ESMMovieMaker] Media File Is Not MP4 Format[{}]", _demuxerCtx.path);
		if (_context && _context->handler)
		{
			_snprintf_s(err, sizeof(err), "[ESMMovieMaker] Media File Is Not MP4 Format[%s]", _demuxerCtx.path);
			_context->handler->onMovieMakerError(status, err);
		}
		break;
	default:
		if (_context && _context->handler)
		{
			_snprintf_s(err, sizeof(err), "[ESMMovieMaker] Unknown Error (Check av_seek_frame())");
			_context->handler->onMovieMakerError(status, err);
	}
		break;
	}

}

int32_t ESMMovieMaker::Core::OnVideoBegin(ESMFFDemuxer::CONTEXT_T* ctx, int32_t codec, const uint8_t* extradata, int32_t extradataSize, int32_t width, int32_t height, double fps, int32_t framecount, int32_t seektime)
{
	int res;

	if (!_decoder.IsInitialized())
	{
		_decoderCtx.deviceIndex = _context->deviceIndex;
		_decoderCtx.codec = codec;
		if (ctx->mppc == true) {

			_decoderCtx.colorspace = ESMNvdec::COLORSPACE_T::BGRA;
		}
		else {
			// add opencv color conv
			_decoderCtx.colorspace = ESMNvdec::COLORSPACE_T::YV12;
		}
		_decoderCtx.width = width;
		_decoderCtx.height = height;

#ifdef CONV_DIRECT
		_decoderCtx.width = width/2;
		_decoderCtx.height = height/2;
#endif
		
		SPd_DEBUG("_decoder.Initialize(ctx - codec:{}/width:{}/height:{})",
			_decoderCtx.codec, _decoderCtx.width, _decoderCtx.height);

		res = _decoder.Initialize(&_decoderCtx);
		if (res)
			SPd_ERROR("_decoder.Initialize()={}", res);
	}

	if (ctx->mppc)
	{
		_frameCount = 0;
		//_encodeIndex = 0;
		_seekTime = seektime;
		_gopSize = (int32_t(ceil(fps)) == 60 ? 12 : 10);
		if (_gopSize != _param->nSrcGopSize)
			return ESMMovieMaker::ERR_CODE_T::GOPSIZE_IS_NOT_EQUAL;

		return ESMMovieMaker::ERR_CODE_T::SUCCESS;
	}

	if (!_converter.IsInitialized()) 
	{
		_converterCtx.deviceIndex = _context->deviceIndex;
		_converterCtx.width = _context->width;
		_converterCtx.height = _context->height;
		_converterCtx.inputColorspace = ESMNvconverter::COLORSPACE_T::BGRA;
		_converterCtx.outputColorspace = ESMNvconverter::COLORSPACE_T::NV12;
		
		SPd_DEBUG("_converter.Initialize(ctx - width:{}/height:{})", _converterCtx.width, _converterCtx.height);

		res = _converter.Initialize(&_converterCtx);
		if (res)
			SPd_ERROR("_converter.Initialize()={}", res);
	}
	if (!_encoder.IsInitialized()) 
	{
		_encoderCtx.deviceIndex = _context->deviceIndex;
		_encoderCtx.bitrate = _context->bitrate;
		_encoderCtx.codec = _context->codec;
#if defined(CONV_TEST) || defined(CONV_DIRECT)		
		_encoderCtx.colorspace = ESMNvenc::COLORSPACE_T::YV12;
#else
		_encoderCtx.colorspace = _converterCtx.outputColorspace;
#endif
		_encoderCtx.fps_den = _context->fps_den;
		_encoderCtx.fps_num = _context->fps_num;
		_encoderCtx.gop = _context->gop;
		_encoderCtx.width = _context->width;
		_encoderCtx.height = _context->height;
		_encoderCtx.profile = _context->profile;

		SPd_DEBUG("_encoder.Initialize(ctx - bitrate:{}/codec:{}/colorspace:{}/fps_den:{}/fps_num:{}/gop:{}/width:{}/height:{}/profile:{})",
			_encoderCtx.bitrate, _encoderCtx.codec, _encoderCtx.colorspace, _encoderCtx.fps_den, _encoderCtx.fps_num,
			_encoderCtx.gop, _encoderCtx.width, _encoderCtx.height, _encoderCtx.profile);

		res = _encoder.Initialize(&_encoderCtx);
		if (res)
			SPd_ERROR("_encoder.Initialize()={}", res);

		cudaError_t cerr = cudaMallocPitch((void**)&_encodeInput, &_encodeInputPitch, _encoderCtx.width, (_encoderCtx.height >> 1) * 3);
		if (cerr != cudaSuccess)
			SPd_ERROR("cudaMallocPitch(width:{}/height:{})={}",
				_encoderCtx.width, _encoderCtx.height, cerr);

		_bitstreamCapacity = _context->bitrate << 1;
		_bitstream = (uint8_t*)malloc(_bitstreamCapacity);

#if defined(WITH_SMOOTH_FILTER)
		if(!_smooth)
			_smooth = cv::cuda::createGaussianFilter(CV_8UC4, CV_8UC4, cv::Size(3, 3), 3);
#endif
	}
	int32_t status = ESMMovieMaker::ERR_CODE_T::SUCCESS;
	if (_param->bUseExtradata) 
	{
#if 0
		long long begin = MillisecondsNow();
		long long end = MillisecondsNow();

		int32_t extradataSize = 0;
		uint8_t* extradata = _encoder.GetExtradata(extradataSize);
		std::string filename = _param->dstFilePath;
		filename += "\\extraData.ext";
		status = WriteFile(filename.c_str(), extradata, extradataSize);

		end = MillisecondsNow();
		//SPd_INFO("{}.h264 File Writing Duration is {} msec", (_making_end - _making_begin));
		SPd_INFO("extraData File Writing Duration is {} msec", (end - begin));
#else
		int32_t extradataSize = 0;
		uint8_t* extradata = _encoder.GetExtradata(extradataSize);
		if (_context != NULL && _context->handler != NULL)
			_context->handler->OnExtraData(extradata, extradataSize, _param->dstFilePath);
#endif
	}
	_frameCount = 0;
	//_encodeIndex = 0;
	_seekTime = seektime;
	_gopSize = (int32_t(ceil(fps)) == 60 ? 12 : 10);
	
	if (status != ESMMovieMaker::ERR_CODE_T::SUCCESS)
		return ESMMovieMaker::ERR_CODE_T::FILEWRITE_ERROR;

	if (_gopSize != _param->nSrcGopSize)
		return ESMMovieMaker::ERR_CODE_T::GOPSIZE_IS_NOT_EQUAL;
	return ESMMovieMaker::ERR_CODE_T::SUCCESS;
}

void ESMMovieMaker::Core::DrawArEffect(cv::cuda::GpuMat& img) {
	cv::Mat frame, result;
	img.download(frame);
	cv::Mat draw = cv::Mat::zeros(frame.size(), frame.type());
	cv::Mat warp = cv::Mat::zeros(frame.size(), frame.type());

	// on test..
	static int frame_cnt = 0;

	LOG_I("[Worker] DrawArEffect, Frame start(%d), end(%d), Size arrow(%d), circle(%d), polygon(%d)", 
		_param->ARData->frame.first, _param->ARData->frame.second, _param->ARData->arrow.size(), _param->ARData->circle.size(), _param->ARData->polygon.size());

	for (const auto& arw : _param->ARData->arrow) {
		ESMMovieGraphic::ARArrow arrow;

		Point startPos(arw.point.first.x, arw.point.first.y);
		Point endPos(arw.point.second.x, arw.point.second.y);
		
		arrow.mArrow_type = ESMMovieGraphic::ARArrow::SOLID;
		arrow.mAlpha_max_ = 80;
		//arrow.mThick_ = abs_arr * 0.4;

		// on test..recal end pos
		arrow.mThick_ = 100;
		if (_param->nStartFrameNum == _param->ARData->frame.first) {
			endPos = Point(0, 0);
		}
		else {
			int width = abs(arw.point.second.x - arw.point.first.x);
			int height = abs(arw.point.second.y - arw.point.first.y);
			

			int delta_x = (width) / (_param->ARData->frame.second - _param->ARData->frame.first);
			int delta_y = (height) / (_param->ARData->frame.second - _param->ARData->frame.first);

			/*if (!width) {
				width = arw.point.second.x;
			}
			if (!height) {
				height = arw.point.second.y;
			}*/

			endPos = Point(startPos.x + delta_x * frame_cnt, startPos.y + delta_y * frame_cnt);
			frame_cnt++;
		}
		
		
		arrow.SetStartPos(startPos);
		arrow.SetEndPos(endPos);
		arrow.SetColorBGR(arw.color_main);
		arrow.Appear();
		arrow.Draw(draw, 30);
	}
	for (const auto& crc : _param->ARData->circle) {
		ESMMovieGraphic::ARCircle circle;

		circle.mCenter_ = Point(crc.point.x, crc.point.y);
		circle.mRadius_ = crc.radius;
		
		circle.mColor_ = ESMMovieGraphic::ARShape::WHITE;
		circle.mColor_Spin_ = ESMMovieGraphic::ARShape::YELLOW;
		circle.mCircle_type_ = ESMMovieGraphic::ARCircle::CIRCLE_FILL;
		circle.mAlpha_max_ = 70;
		circle.mRadius_start_ = 20;

		circle.SetColorBGR(crc.color_main);
		circle.Appear();
		circle.Draw(draw);
	}
	for (const auto& polygon : _param->ARData->polygon) {

	}


	//warpPerspective(draw, warp, h_T, frame.size());
	subtract(frame, draw, result);

	img.upload(result);
}

int32_t ESMMovieMaker::Core::TransFrames(ESMFFDemuxer::CONTEXT_T* ctx, const int callType, int& nFrameIdx, const int nDecoded, const uint8_t** ppDecoded) {
	int32_t status = ESMMovieMaker::ERR_CODE_T::GENERIC_FAIL;
	LOG_I("[Worker] TransFrames Begin..nDecoded(%d)", nDecoded);

	for (int32_t idx = 0; idx < nDecoded; idx++)
	{
		LOG_I("[Worker] idx(%d) before, nFrameIdx(%d), _localStartCnt(%d), _localEndCnt(%d), _globalCnt(%d)", 
			idx, nFrameIdx, _localStartCnt, _localEndCnt, _globalCnt);

		nFrameIdx++;
		if (_localStartCnt <= nFrameIdx && nFrameIdx <= _localEndCnt)
		{
			_globalCnt++;

			ctx->dsc_id = _param->frameInfo[_globalCnt].prefix;
			//20211005 Kelly add for Recal			
			SPd_DEBUG("onVideoReceive recalmode {} recaldscid {} current dsc id {}  ", _recal_mode, _recal_dscid, ctx->dsc_id);
			if (_recal_mode == TRUE && _recal_dscid == ctx->dsc_id)
			{
				SPd_DEBUG("cur_img insert in onVideoRecv {} ", idx);
				cv::cuda::GpuMat img = cv::cuda::GpuMat(_decoderCtx.height, _decoderCtx.width, CV_8UC4, (uint8_t*)ppDecoded[idx], _decoder.GetPitch2()).clone();
				//if (_param->pointInfo[_globalCnt].flip > 0) {
				//	cv::cuda::GpuMat fimg = img;
				//	cv::cuda::flip(fimg, img, 0);
				//	fimg.release();
				//}
				if (_param->pointInfo[ctx->dsc_id].Width == 3840) {
					cv::cuda::resize(img, img, cv::Size(_param->pointInfo[ctx->dsc_id].Width / 2, _param->pointInfo[ctx->dsc_id].Height / 2), 0, 0, cv::INTER_CUBIC);
				}

				cv::cuda::GpuMat gcur_img;
				if (_param->pointInfo[ctx->dsc_id].flip > 0) {
					cv::cuda::flip(img, gcur_img, -1);
				}
				else
					gcur_img = img;

				Mat cur_img;
				gcur_img.download(cur_img);
				//debug
				char curname[50];
				sprintf_s(curname, "recalibration\\saved\\cur_img_%s.png", ctx->dsc_id.c_str());
				cv::imwrite(curname, cur_img);

				SPd_DEBUG("cur_img saved. width {} height {} ", _param->pointInfo[ctx->dsc_id].Width / 2, _param->pointInfo[ctx->dsc_id].Height / 2);
				int32_t result = _conn->CheckDataValidity(&_param->pointInfo[ctx->dsc_id], ctx->dsc_id, 2);

				SPd_DEBUG("cur_img insert in CheckDataValidity result {} ", result);

				if (result == ERR_NONE)
				{
					_conn->Start(_param->pointInfo[ctx->dsc_id].Width / 2);
					double* score = 0;
					int32_t cal_result = _conn->Recalibration(gref_img, gcur_img, &_param->pointInfo[ctx->dsc_id], _recal_dscid);
					SPd_DEBUG("cal_result after Recalibration{} ", cal_result);
					_conn->Finish();
					SPd_DEBUG("cal_result after Recalibration{} ", cal_result);
					//_param->pointInfo[ctx->dsc_id].recalibration_status = 1;
					//_param->pointInfo[ctx->dsc_id].recalibration_score = cal_result;
				}
				else {
					_param->pointInfo[ctx->dsc_id].recalibration_status = -1;
					_param->pointInfo[ctx->dsc_id].recalibration_score = -1;
				}

				img.release();
				gref_img.release();
				gcur_img.release();
				_recal_mode = FALSE;

				// Send Recalibration Data to VideoMaker instance in MMS
				// (Note: When 'points' parameter is not null, 'name' parameter is considered as 'recordName' of '_param'.
				if (_context != NULL && _context->handler != NULL)
					_context->handler->OnEncodingComplete(&(_param->pointInfo[ctx->dsc_id]), 0,
						_param->pointInfo[ctx->dsc_id].camfps * _param->frameInfo[_globalCnt].matchingTime + _param->frameInfo[_globalCnt].matchingFrameIdx,
						nullptr, 0, _param->recordName);

				return ESMMovieMaker::ERR_CODE_T::FINISH_DECODE;
			}


			//uint8_t* src = pDecoded;
			//uint8_t* dst = _pauseFrame;

			//cudaMemcpy2D(dst, _pauseFramePitch, src, _decoder.GetPitch2(), _decoderCtx.width, _decoderCtx.height, cudaMemcpyDeviceToDevice);
			//src += _decoder.GetPitch2() * _decoderCtx.height;
			//dst += _pauseFramePitch * _decoderCtx.height;
			//cudaMemcpy2D(dst, _pauseFramePitch / 2, src, _decoder.GetPitch2() / 2, _decoderCtx.width / 2, _decoderCtx.height / 2, cudaMemcpyDeviceToDevice);
			//src += _decoder.GetPitch2() * _decoderCtx.height / 4;
			//dst += _pauseFramePitch * _decoderCtx.height / 4;
			//cudaMemcpy2D(dst, _pauseFramePitch / 2, src, _decoder.GetPitch2() / 2, _decoderCtx.width / 2, _decoderCtx.height / 2, cudaMemcpyDeviceToDevice);

			///*cudaError_t err = cudaMemcpy(_pauseFrame, pDecoded, _decoder.GetPitch2() * _decoderCtx.height * 3 / 2, cudaMemcpyDeviceToDevice);
			//if (err != cudaSuccess) {
			//	return -1;
			//}*/

			// TODO: Idx for frameInfo should be changed to frame number which is counting
			//cv::cuda::GpuMat img = cv::cuda::GpuMat(_decoderCtx.height, _decoderCtx.width, CV_8UC4, ppDecoded[idx], _decoder.GetPitch2()).clone();
			cuda::GpuMat img;
			FrCvClrConv::YUV2BGR(img, (uint8_t*)ppDecoded[idx], _decoderCtx.width, _decoderCtx.height, _decoder.GetPitch2());
			// add animation..
			// 211220 Hyena add AR
			if (_param->ARData->IsAvailable()) {
				if (_param->nStartFrameNum >= _param->ARData->frame.first && _param->nStartFrameNum <= _param->ARData->frame.second) {
					DrawArEffect(img);
				}
			}

			if (_context && _context->gimbal)
				status = DoAdjustH(img, _param->adjustInfo[_globalCnt], _encoderCtx.width, _encoderCtx.height, img);
			else
				status = DoAdjust(img, _param->adjustInfo[_globalCnt], _encoderCtx.width, _encoderCtx.height, img);

			if (status != ESMMovieMaker::ERR_CODE_T::SUCCESS)
				break;

			status = DoVMCC(img, { _param->frameInfo[_globalCnt].x, _param->frameInfo[_globalCnt].y }, _param->frameInfo[_globalCnt].zoom, _encoderCtx.width, _encoderCtx.height, img);
			if (status != ESMMovieMaker::ERR_CODE_T::SUCCESS)
				break;

			// add pose
			//std::vector<std::array<FrPoint, 18>> vecJoint;
			//if (_pose_mode == true && _infer != nullptr) {
			//	_infer->doPoseInference(img.ptr(), img.step, img.cols, img.rows, vecJoint);

			//	if (vecJoint.size()) {
			//		// download
			//		cv::Mat tmpImg;
			//		img.download(tmpImg);

			//		_infer->drawPose(vecJoint, tmpImg);

			//		// upload
			//		img.upload(tmpImg);
			//	}
			//}
			//////////////////////////////////////////////////////////////////////////////

			if (_param->bDebugNumber)
			{
				status = DebugNumberOnMat(img, _globalCnt, _param->frameInfo[_globalCnt].prefix);
				if (status != ESMMovieMaker::ERR_CODE_T::SUCCESS)
					break;
			}

			FrCvClrConv::BGR2YUV(_encodeInput, _encodeInputPitch, img);
			/*status = _converter.ConvertBGRA2YUV(img.data, img.step, _encodeInput, _encodeInputPitch, _context->width, _context->height);
			if (status != ESMMovieMaker::ERR_CODE_T::SUCCESS)
			{
				SPd_ERROR("_converter.ConvertBGRA2YUV()={}", status);
				break;
			}*/

			int32_t bitstreamSize = 0;
			long long bitstreamTimestamp = 0;
			status = _encoder.Encode(_encodeInput, _encodeInputPitch, 0, _bitstream, _bitstreamCapacity, bitstreamSize, bitstreamTimestamp);
			if (status != ESMMovieMaker::ERR_CODE_T::SUCCESS)
			{
				SPd_ERROR("_encoder.Encode()={}", status);
				break;
			}

			SPd_DEBUG("_encoder.Encode(inputPitch:{}/frameCount:{})=0 (bitstreamSize={}/bitstreamTimestamp={})",
				_encodeInputPitch, _frameCount, bitstreamSize, bitstreamTimestamp);
			
			_encodeIndex++;
			img.release();

			LOG_I("[Worker] idx(%d) after, nFrameIdx(%d), _localStartCnt(%d), _localEndCnt(%d), _globalCnt(%d), _encodeIndex(%d)",
				idx, nFrameIdx, _localStartCnt, _localEndCnt, _globalCnt, _encodeIndex);

#if 0
			long long begin = MillisecondsNow();
			long long end = MillisecondsNow();

			std::string filename = _param->dstFilePath;
			filename += "\\";
			filename += std::to_string(_globalCnt);
			filename += ".h264";
			status = WriteFile(filename.c_str(), _bitstream, bitstreamSize);

			end = MillisecondsNow();
			SPd_INFO("{}.h264 File Writing Duration is {} msec", _globalCnt, (end - begin));

			if (status != ESMMovieMaker::ERR_CODE_T::SUCCESS)
				return status;

			if (_context != NULL && _context->handler != NULL)
				_context->handler->OnEncodingComplete(status, _globalCnt);
#else
			SPd_INFO("{}.h264 File is Sended", _globalCnt);
			if (_context != NULL && _context->handler != NULL)
				_context->handler->OnEncodingComplete(nullptr,
					status, _globalCnt, _bitstream, bitstreamSize, std::string(_param->dstFilePath));
#endif

			if (nFrameIdx >= _localEndCnt)
				return ESMMovieMaker::ERR_CODE_T::FINISH_DECODE;
		}
	}

	LOG_I("[Worker] TransFrames End..nDecoded(%d)", nDecoded);

	if (callType == VIDEO_RECV)
		return  ESMMovieMaker::ERR_CODE_T::ERR_AGAIN;
	else if (callType == VIDEO_END) {
		if (_context != NULL && _context->handler != NULL)
			_context->handler->OnEncodingComplete(nullptr, status, _globalCnt, nullptr, 0, std::string(_param->dstFilePath));
		return  ESMMovieMaker::ERR_CODE_T::SUCCESS;
	}
		
}

int32_t ESMMovieMaker::Core::OnVideoRecv(ESMFFDemuxer::CONTEXT_T* ctx, uint8_t* bytes, int32_t nbytes, int32_t& nFrameIdx)
{
	int32_t nDecoded = -1;
	uint8_t** ppDecoded = NULL;
	long long* tsDecoded = 0;

	if (ctx->mppc)
	{
		SPd_DEBUG("ctx->mppc true ctx->dsc_id {} flip {} ", ctx->dsc_id, _param->pointInfo[ctx->dsc_id].flip);
		if (nbytes > 0) {
			if (_recal_dscid == ctx->dsc_id) {
				SPd_DEBUG("same camera data received {} {})", _recal_dscid, ctx->dsc_id);
				return ESMMovieMaker::ERR_CODE_T::FINISH_DECODE;
			}

			SPd_DEBUG(" _param->pointinfo _globalCnt {} width {} height {} ", _globalCnt, _param->pointInfo[ctx->dsc_id].Width, _param->pointInfo[ctx->dsc_id].Height);
			_recal_mode = TRUE;
			_recal_dscid = ctx->dsc_id;

			static long long uid = 0;
			int res = _decoder.Decode(bytes, nbytes, uid++, &ppDecoded, &nDecoded, &tsDecoded, NULL, NULL);
			if (res)
				SPd_ERROR("_decoder.Decode(nbytes:{}/frameCount:{})={} nDecoded {}",
					nbytes, _frameCount, res, nDecoded);
			else
				SPd_DEBUG("_decoder.Decode(nbytes:{}/frameCount:{})=0 (nDecoded={})",
					nbytes, _frameCount, nDecoded);

			if (!nDecoded) {
				res = _decoder.Decode(nullptr, 0, 0, &ppDecoded, &nDecoded, &tsDecoded, NULL, NULL);
				if (res != ESMNvdec::ERR_CODE_T::SUCCESS) {
					SPd_DEBUG("Decode() flush fail ret: {}", res);
					return false;
				}
			}
			SPd_DEBUG("nDecoded {} {} ", nDecoded, uid);
			for (int32_t idx = 0; idx < nDecoded; idx++) {

				cv::cuda::GpuMat img = cv::cuda::GpuMat(_param->pointInfo[ctx->dsc_id].Height, _param->pointInfo
				[ctx->dsc_id].Width, CV_8UC4, ppDecoded[0], _decoder.GetPitch2()).clone();
				//if (_param->pointInfo[_globalCnt].flip > 0) {
				//	cv::cuda::GpuMat fimg = img;
				//	cv::cuda::flip(fimg, img, 0);
				//	fimg.release();
				//}
				if (_param->pointInfo[ctx->dsc_id].Width == 3840) {
					cv::cuda::resize(img, img, cv::Size(_param->pointInfo[ctx->dsc_id].Width/2, _param->pointInfo[ctx->dsc_id].Height/2), 0, 0, cv::INTER_CUBIC);
				}

				if (_param->pointInfo[ctx->dsc_id].flip > 0) {
					cv::cuda::flip(img, gref_img, -1);
				}
				else
					gref_img = img;

				Mat ref_img;
				gref_img.download(ref_img);
				char refname[50];
				sprintf_s(refname, "recalibration\\saved\\ori_img_%s.png", ctx->dsc_id.c_str());
				cv::imwrite(refname, ref_img);

				SPd_DEBUG("idx {} ", idx);
				//debug
				//sprintf(refname, "recalibration\\saved\\ref_img_%s.png", ctx->dsc_id.c_str());
				//cv::imwrite(refname, ref_img);
				SPd_DEBUG("ctx->mppc ref image save done {}", ctx->dsc_id);
			}
		}
		return ESMMovieMaker::ERR_CODE_T::FINISH_DECODE;
	}


	// add pose
	if (_pose_mode == true && _infer == nullptr) {
		int32_t status = ESMMovieMaker::ERR_CODE_T::SUCCESS;
		
		_infer = new DeepInfer();
		_infer_ctx.width = _context->width;
		_infer_ctx.height = _context->height;
		_infer_ctx.detectPath = "detector.engine";
		_infer_ctx.estimatePath = "pose.engine";
		status = _infer->initialize(&_infer_ctx);
		if (status != static_cast<int>(DL_RESULT::SUCCESS)) {
			status = ESMMovieMaker::ERR_CODE_T::INFER_INIT_ERROR;
		}
	}


	int res = _decoder.Decode(bytes, nbytes, _frameCount++, &ppDecoded, &nDecoded, &tsDecoded, NULL, NULL);
	if (res)
		SPd_ERROR("_decoder.Decode(nbytes:{}/frameCount:{})={}",
			nbytes, _frameCount, res);
	else
		SPd_DEBUG("_decoder.Decode(nbytes:{}/frameCount:{})=0 (nDecoded={}) nFrameIdx:{}, _localStartCnt:{}, _localEndCnt:{}",
			nbytes, _frameCount, nDecoded, nFrameIdx, _localStartCnt, _localEndCnt);

	return TransFrames(ctx, VIDEO_RECV, nFrameIdx, nDecoded, (const uint8_t**)ppDecoded);
}

int32_t ESMMovieMaker::Core::OnVideoEnd(ESMFFDemuxer::CONTEXT_T* ctx, int32_t& nFrameIdx)
{
	if (ctx->mppc)
	{
		if (_decoder.IsInitialized())
		{
			int res = _decoder.Release();
			if (res)
				SPd_ERROR("_decoder.Release()={}", res);
		}

		return ESMMovieMaker::ERR_CODE_T::SUCCESS;
	}

	int32_t nDecoded = -1;
	uint8_t** ppDecoded = NULL;
	long long* tsDecoded = 0;

	int res = _decoder.Decode(NULL, 0, _frameCount++, &ppDecoded, &nDecoded, &tsDecoded, NULL, NULL);
	if (res)
		SPd_ERROR("_decoder.Decode(frameCount:{})={}", _frameCount, res);
	else
		SPd_DEBUG("_decoder.Decode(frameCount:{})=0 (nDecoded={}) nFrameIdx:{}, _localStartCnt:{}, _localEndCnt:{}",
			_frameCount, nDecoded, nFrameIdx, _localStartCnt, _localEndCnt);

	return TransFrames(ctx, VIDEO_END, nFrameIdx, nDecoded, (const uint8_t**)ppDecoded);
}

bool ESMMovieMaker::Core::CheckValidH(HOMO_T& t)
{
	if (t.r1[0] == 1.0 && t.r1[1] == 0.0 && t.r1[2] == 0.0
		&& t.r2[0] == 0.0 && t.r2[1] == 1.0 && t.r2[2] == 0.0
		&& t.r3[0] == 0.0 && t.r3[1] == 0.0 && t.r3[2] == 1.0)
		return false;

	return true;
}

int32_t ESMMovieMaker::Core::DoAdjust(cv::cuda::GpuMat& srcMat, ESMMovieMaker::ADJUST_INFO_T& adjustInfo, const int32_t dstWidth, const int32_t dstHeight, cv::cuda::GpuMat& dstMat)
{
#ifdef CONV_TEST
	adjustInfo.dMarginW = srcMat.cols;
	adjustInfo.dMarginH = srcMat.rows;
	adjustInfo.dScale = 1.0;

	cv::Rect2d rtMargin(adjustInfo.dMarginX, adjustInfo.dMarginY, adjustInfo.dMarginW, adjustInfo.dMarginH);

	double dMoveX, dMoveY;
	dMoveX = adjustInfo.dAdjustX;
	dMoveY = adjustInfo.dAdjustY;

	double dRotateX, dRotateY;
	dRotateX = adjustInfo.dRotateX;
	dRotateY = adjustInfo.dRotateY;

	double dAngle = (adjustInfo.dAngle + 90.0);
	double dRad = dAngle * CV_PI / 180.0;

	double dScale = adjustInfo.dScale;

	cv::Mat m, mTfm, mRot, mTrn, mScale, mFlip, mMargin, mScaleOutput;
	/*MatrixCalculator::GetFlipMatrix(srcMat.cols, srcMat.rows, adjustInfo.bFlip, adjustInfo.bFlip, mFlip);
	MatrixCalculator::GetRotationMatrix(dRad, dRotateX, dRotateY, mRot);
	MatrixCalculator::GetScaleMarix(dScale, dScale, dRotateX, dRotateY, mScale);
	MatrixCalculator::GetTranslationMatrix(dMoveX, dMoveY, mTrn);
	MatrixCalculator::GetMarginMatrix(srcMat.cols, srcMat.rows, rtMargin.x, rtMargin.y, rtMargin.width, rtMargin.height, mMargin);*/


	MatrixCalculator::GetScaleMarix((float)dstWidth / srcMat.cols, (float)dstHeight / srcMat.rows, mScaleOutput);


	mTfm = mScaleOutput;
	MatrixCalculator::ParseAffineMatrix(mTfm, m);

	
	cv::cuda::GpuMat temp;
	srcMat.copyTo(temp);
	cv::cuda::warpAffine(temp, dstMat, m, cv::Size(dstWidth, dstHeight), cv::INTER_CUBIC, cv::BORDER_CONSTANT);

	return ESMMovieMaker::ERR_CODE_T::SUCCESS;
#endif

	try 
	{
		if (adjustInfo.dAdjustX == 0.0 && adjustInfo.dAdjustY == 0.0) 
		{
			printf("***************************************** Adjust null: %s *****************************************\n", adjustInfo.strDscId.c_str());
		}

		//cv::Size dstSize(srcMat.cols, srcMat.rows);
		//if (dstSize.width == 0 || dstSize.height == 0)
		//	dstSize = srcMat.size();

		cv::Rect2d rtMargin(adjustInfo.dMarginX, adjustInfo.dMarginY, adjustInfo.dMarginW, adjustInfo.dMarginH);

		double dMoveX, dMoveY;
		dMoveX = adjustInfo.dAdjustX;
		dMoveY = adjustInfo.dAdjustY;

		double dRotateX, dRotateY;
		dRotateX = adjustInfo.dRotateX;
		dRotateY = adjustInfo.dRotateY;

		double dAngle = (adjustInfo.dAngle + 90.0);
		double dRad = dAngle * CV_PI / 180.0;

		double dScale = adjustInfo.dScale;

		cv::Mat m, mTfm, mRot, mTrn, mScale, mFlip, mMargin, mScaleOutput;
		MatrixCalculator::GetFlipMatrix(srcMat.cols, srcMat.rows, adjustInfo.bFlip, adjustInfo.bFlip, mFlip);
		MatrixCalculator::GetRotationMatrix(dRad, dRotateX, dRotateY, mRot);
		MatrixCalculator::GetScaleMarix(dScale, dScale, dRotateX, dRotateY, mScale);
		MatrixCalculator::GetTranslationMatrix(dMoveX, dMoveY, mTrn);
		MatrixCalculator::GetMarginMatrix(srcMat.cols, srcMat.rows, rtMargin.x, rtMargin.y, rtMargin.width, rtMargin.height, mMargin);
		MatrixCalculator::GetScaleMarix((float)dstWidth / srcMat.cols, (float)dstHeight / srcMat.rows, mScaleOutput);


		 
		 

		mTfm = mScaleOutput * mMargin * mTrn * mScale * mRot * mFlip;
		MatrixCalculator::ParseAffineMatrix(mTfm, m);

		//if (g_VMdebugMode) {
		//	cv::Mat debugMat;
		//	_dstMat.download(debugMat);
		//	cv::line(debugMat, cv::Point(static_cast<int>(rtMargin.x), static_cast<int>(rtMargin.y)),
		//		cv::Point(static_cast<int>(rtMargin.x + rtMargin.width), static_cast<int>(rtMargin.y + rtMargin.height)),
		//		cv::Scalar(25, 75, 225), 5);
		//	cv::rectangle(debugMat, cv::Point(static_cast<int>(rtMargin.x), static_cast<int>(rtMargin.y)),
		//		cv::Point(static_cast<int>(rtMargin.x + rtMargin.width), static_cast<int>(rtMargin.y + rtMargin.height)),
		//		cv::Scalar(25, 75, 225), 5);
		//	cv::circle(debugMat, cv::Point(static_cast<int>(rtMargin.x), static_cast<int>(rtMargin.y)), 20, cv::Scalar(0, 0, 255), 4);
		//	cv::circle(debugMat, cv::Point(static_cast<int>(rtMargin.x + rtMargin.width), static_cast<int>(rtMargin.y + rtMargin.height)), 20, cv::Scalar(0, 0, 255), 4);
		//	_dstMat.upload(debugMat);
		//}

		//cv::cuda::GpuMat res;
		cv::cuda::GpuMat temp;
		srcMat.copyTo(temp);
		cv::cuda::warpAffine(temp, dstMat, m, cv::Size(dstWidth, dstHeight), cv::INTER_CUBIC, cv::BORDER_CONSTANT);

		return ESMMovieMaker::ERR_CODE_T::SUCCESS;
	}
	catch (std::exception e) 
	{
		DWORD err = ::GetLastError();
		return ESMMovieMaker::ERR_CODE_T::IMAGE_ADJUST_ERROR;
	}
}

int32_t ESMMovieMaker::Core::DoAdjustH(cv::cuda::GpuMat& srcMat, ESMMovieMaker::ADJUST_INFO_T& adjustInfo, const int32_t dstWidth, const int32_t dstHeight, cv::cuda::GpuMat& dstMat)
{
	try
	{
		if (!CheckValidH(adjustInfo.homo))
		{
			//SPd_ERROR("AdjustH Error : {}");
			if (_context && _context->handler)
			{
				_context->handler->onMovieMakerError(ESMMovieMaker::ERR_CODE_T::VMCC_ERROR, "[ESMMovieMaker] Invalid Homograph Data");
			}
			return ESMMovieMaker::ERR_CODE_T::VMCC_ERROR;
		}
		cv::Mat mH = cv::Mat::eye(3, 3, CV_32FC1);

		for (int i = 0; i < 3; i++)
		{
			mH.at<float>(0, i) = adjustInfo.homo.r1[i];
			mH.at<float>(1, i) = adjustInfo.homo.r2[i];
			mH.at<float>(2, i) = adjustInfo.homo.r3[i];
		}

		cv::Mat mFlip, mS, mTfm;
		MatrixCalculator::GetFlipMatrix(srcMat.cols, srcMat.rows, adjustInfo.bFlip, adjustInfo.bFlip, mFlip);
		MatrixCalculator::GetScaleMarix((float)dstWidth / srcMat.cols, (float)dstHeight / srcMat.rows, mS);

		mTfm = mS * mH * mFlip;

		//cv::Mat mAfn;
		//MatrixCalculator::ParseAffineMatrix(mTfm, mAfn);
		//cv::cuda::warpAffine(srcMat, dstMat, mAfn, cv::Size(dstWidth, dstHeight));

		cv::cuda::warpPerspective(srcMat, dstMat, mTfm, cv::Size(dstWidth, dstHeight));

		return ESMMovieMaker::ERR_CODE_T::SUCCESS;
	}
	catch (std::exception& e)
	{
		//SPd_ERROR("AdjustH Error : {}", e.what());
		if (_context && _context->handler)
		{
			char err[MAX_PATH] = { 0 };
			_snprintf_s(err, sizeof(err), "[ESMMovieMaker] AdjustH Error[%s]", e.what());
			_context->handler->onMovieMakerError(ESMMovieMaker::ERR_CODE_T::VMCC_ERROR, err);
		}
		return ESMMovieMaker::ERR_CODE_T::VMCC_ERROR;
	}
}

int32_t ESMMovieMaker::Core::DoVMCC(cv::cuda::GpuMat& srcMat, std::array<int32_t, 2> inCenterPt, const int32_t inZoom, const int32_t inDstWidth, const int32_t inDstHeight, cv::cuda::GpuMat& dstMat)
{
	try 
	{
		cv::cuda::GpuMat cropMat;
		auto dScale = inZoom / 100.;
		auto nLeft = static_cast<int>(inCenterPt[0] * 0.5 - (static_cast<int>(srcMat.cols / dScale) >> 1));
		auto nTop = static_cast<int>(inCenterPt[1] * 0.5 - (static_cast<int>(srcMat.rows / dScale) >> 1));
		auto nWidth = static_cast<int32_t>(srcMat.cols / dScale - 1);
		if ((nLeft + nWidth) > srcMat.cols)
			nLeft = nLeft - ((nLeft + nWidth) - srcMat.cols);
		else if (nLeft < 0)
			nLeft = 0;

		auto nHeight = static_cast<int32_t>(srcMat.rows / dScale - 1);
		if ((nTop + nHeight) > srcMat.rows)
			nTop = nTop - ((nTop + nHeight) - srcMat.rows);
		else if (nTop < 0)
			nTop = 0;

		if (srcMat.cols >= (nLeft + nWidth) && srcMat.rows >= (nTop + nHeight))
			cropMat = srcMat(cv::Rect(nLeft, nTop, nWidth, nHeight)).clone();
		else
			cropMat = srcMat.clone();

		cv::cuda::resize(cropMat, dstMat, cv::Size(inDstWidth, inDstHeight), 0, 0, cv::INTER_CUBIC);
#if defined(WITH_SMOOTH_FILTER)
		_smooth->apply(dstMat, dstMat);
#endif
		return ESMMovieMaker::ERR_CODE_T::SUCCESS;
	}
	catch (std::exception) 
	{
		return ESMMovieMaker::ERR_CODE_T::VMCC_ERROR;
	}
}

int32_t ESMMovieMaker::Core::DebugNumberOnMat(cv::cuda::GpuMat& srcMat, const int32_t idx, const std::string prefix)
{
	cv::Mat tmpSrcMat;
	srcMat.download(tmpSrcMat);
	cv::putText(tmpSrcMat, prefix + ", " + std::to_string(idx), cv::Point(100, 100), cv::QT_FONT_NORMAL, 3.0, cv::Scalar(255, 125, 125), 5);
	srcMat.upload(tmpSrcMat);

	return ESMMovieMaker::ERR_CODE_T::SUCCESS;
}

int32_t ESMMovieMaker::Core::WriteFile(const char* filename, uint8_t* data, int32_t size)
{
	BOOL bWritten = FALSE;
	HANDLE pFile = ::CreateFileA(filename, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (pFile != NULL && pFile != INVALID_HANDLE_VALUE) 
	{

		uint32_t bytes_written = 0;
		for(int32_t index=0; index<10; index++)
		{
			uint32_t nb_write = 0;
			::WriteFile(pFile, data, size, (LPDWORD)&nb_write, 0);
			bytes_written += nb_write;
			if (size == bytes_written)
			{
				bWritten = TRUE;
				break;
			}
			::Sleep(10);
		}

		::CloseHandle(pFile);
		pFile = INVALID_HANDLE_VALUE;
	}
	if(bWritten)
		return ESMMovieMaker::ERR_CODE_T::SUCCESS;
	else
		return ESMMovieMaker::ERR_CODE_T::FILEWRITE_ERROR;
}

long long ESMMovieMaker::Core::MillisecondsNow(void)
{
	if (_fw_use_qpc)
	{
		LARGE_INTEGER now;
		QueryPerformanceCounter(&now);
		return (1000LL * now.QuadPart) / _fw_frequency.QuadPart;
	}
	else
	{
		return GetTickCount();
	}
}