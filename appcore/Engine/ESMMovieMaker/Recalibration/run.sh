if [ -f "genesis" ];then
    rm genesis 
fi
if [ -f "generated/CMakeFiles" ];then
    rm -rf generated/CMakeFiles
fi
if [ -f "generated/cmake_install.cmake" ];then
    rm generated/cmake_install.cmake
fi
if [ -f "generated/CMakeCache.txt" ];then
    rm generated/CMakeCache.txt
fi

make -j4

FILE=genesis
if [ -f "$FILE" ];then
    mv CMakeFiles generated/
    mv cmake_install.cmake generated/
    mv CMakeCache.txt generated/

    echo "Output File Created."
    ./genesis 
#    cd streamlit
#    streamlit run app.py
fi

if [ ! -d "generated" ];then
    mkdir generated
fi

if [ ! -d "log" ];then
    mkdir log
fi

if [ ! -d "saved" ];then
    mkdir log
fi

cp -rf /Users/4dreplay/work/v4_mmd/Libs/ESMMovieMaker/Recalibration/src /Users/4dreplay/work/genesis/ 
