﻿
/*****************************************************************************
*                                                                            *
*                            Genesis          								 *
*                                                                            *
*   Copyright (C) 2021 By 4dreplay, Incoporated. All Rights Reserved.        *
******************************************************************************

    File Name       : genesis.pp
    Author(S)       : Me Eunkyung
    Created         : 21 Sep 2021

    Description     : genesis.cpp
    Notes           : Python - C library connector.
*/
#define _CRT_SECURE_NO_WARNINGS
#include <filesystem>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <vector>
#include "src/DefData.hpp"
#include "src/Extractor.hpp"
#include "../../../Base/include/MppcPointData.h"

using namespace std;

#define VER "0.1.0"

void Finish();
void GetVerion() {
    cout<< "Cur Version : " << VER << endl;        
}

/* std::map<int32_t, MppcPoints> pointInfo; // int32_t = frameid
struct MppcPoints
{
    std::string dsc_id;
    int framenum{};
    int camfps{};
    int flip{};
    std::string Group;
    int Width{};
    int Height{};
    int infection_point{};
    double swipe_base_length{};
    int ManualOffsetY{};
    double FocalLength{};
    MppcPts2D pts_2d;
    MppcPts3D pts_3d;
    MppcPtsSwipe pts_swipe;
};
struct MppcPts3D
{
    double X1{};
    double Y1{};
    double X2{};
    double Y2{};
    double X3{};
    double Y3{};
    double X4{};
    double Y4{};
    double CenterX{};
    double CenterY{};
    MppcPoint Point1;
    MppcPoint Point2;
    MppcPoint Point3;
    MppcPoint Point4;
    MppcPoint Center;
};
struct MppcPoint
{
    bool IsEmpty{};
    double X{};
    double Y{};
};
*/

Extractor* ext;
int main(void) {
        Logger("main start");

        int cnt;
        int* region;
        char* img_path;
        int result = 0;
        FPt in_pt[4]; 
        FPt out_pt[4];        
        MppcPoints pts;
        char file_1[50];
        char file_2[50]; 
        double score;       
#if defined _WIN_        
        sprintf(file_1,"D:\\genesis_test\\new\\1.png");
        sprintf(file_2,"D:\\genesis_test\\new\\2.png");
#endif
#if defined _MAC_
        sprintf(file_1,"new/1.png");
        sprintf(file_2,"new/2.png");
#endif

        pts.pts_3d.Point1.X = 1340.22;
        pts.pts_3d.Point1.Y = 763.36;
        pts.pts_3d.Point2.X = 817.860;
        pts.pts_3d.Point2.Y = 867.640;
        pts.pts_3d.Point3.X = 623.622;
        pts.pts_3d.Point3.Y = 370.51;
        pts.pts_3d.Point4.X = 1056.026;
        pts.pts_3d.Point4.Y = 296.089;        

        in_pt[0].x = pts.pts_3d.Point1.X;
        in_pt[0].y = pts.pts_3d.Point1.Y;
        in_pt[1].x = pts.pts_3d.Point2.X;
        in_pt[1].y = pts.pts_3d.Point2.Y;
        in_pt[2].x = pts.pts_3d.Point3.X;
        in_pt[2].y = pts.pts_3d.Point3.Y;
        in_pt[3].x = pts.pts_3d.Point4.X;
        in_pt[3].y = pts.pts_3d.Point4.Y;

        ext = new Extractor();
#if defined _WIN_ || _WINDOWS
#if defined GPU
        cv::cuda::GpuMat ref;
        cv::cuda::GpuMat cur;
        result = ext->ExecuteClient(ref, cur, in_pt, out_pt, "TEST");
#endif
#else
        Mat ref = imread(file_1);
        Mat cur = imread(file_2);
        //resize(ref, ref, Size(ref.cols/2, ref.rows/2), 0, 0, 1);
        //resize(cur, cur, Size(cur.cols/2, cur.rows/2), 0, 0, 1);

        result = ext->ExecuteClient(ref, cur, in_pt, out_pt, "TEST");
#endif
        if(result != ERR_NONE) {
            Logger("Execuete Client ERR  %d ", result);
        }
        Logger("Execuete Client returned score = %d", result);
        Finish();
        Logger("Main finish well .. ");
}

void Finish() {    
    ext->~Extractor();
}