#pragma once

#include "UdpSync.h"
//#include "FD_Singleton.h"

#include <string>
#include <memory>
#include <mutex>
#include <map>
#include <functional>
#include <filesystem>	// c++17
namespace fs = std::filesystem;


using namespace FDLive;

#define	CAMERA_INFO_IPADDRESS						"IPAddress"
#define	CAMERA_INFO_SUBNETMASK						"SubnetMask"
#define	CAMERA_INFO_GATEWAYADDRESS					"GatewayAddress"
#define	CAMERA_INFO_MANUFACTURER					"Manufacturer"			// onvif new
#define	CAMERA_INFO_MODELNAME						"ModelName"
#define	CAMERA_INFO_UUID							"UUID"
#define	CAMERA_INFO_FIRMWARENXP						"FirmwareNXP"
#define	CAMERA_INFO_FIRMWAREXZ01					"FirmwareXZ01"
#define	CAMERA_INFO_FIRMWAREVER						"FirmwareVersion"		// onvif new
#define	CAMERA_INFO_LOGMODE							"LogMode"
#define	CAMERA_INFO_NTPSERVER						"NtpServer"
#define	CAMERA_INFO_TIMEDIFF						"Time Diff"
#define	CAMERA_INFO_MANUFALFOCUSPOSITION			"ManualFocusPosition"
#define	CAMERA_INFO_ZOOMPOSSITION					"ZoomPosition"
#define	CAMERA_INFO_FOCUSMODE						"FocusMode"
#define	CAMERA_INFO_AUTO_FOCUS_POINT				"AutoFocusPoint"
#define	CAMERA_INFO_FOCUSLOCX0						"FocusLocation_x0"
#define	CAMERA_INFO_FOCUSLOCY0						"FocusLocation_y0"
#define	CAMERA_INFO_FOCUSLOCX1						"FocusLocation_x1"
#define	CAMERA_INFO_FOCUSLOCY1						"FocusLocation_y1"
#define	CAMERA_INFO_EXPOSUERMODE					"ExposureMode"
#define	CAMERA_INFO_AEAREA							"AeArea"
#define	CAMERA_INFO_APERTURE						"Aperture"
#define	CAMERA_INFO_SHUTTERSPEED					"ShutterSpeed"
#define	CAMERA_INFO_ISO								"ISO"
#define	CAMERA_INFO_WHITEBALANCE					"WhiteBalance"
#define	CAMERA_INFO_EXPOSURECOMPENSATION   			"ExposureCompensation"
#define	CAMERA_INFO_IMAGESSTABILIZATIONMODE			"ImageStabilizationMode"
#define	CAMERA_INFO_CONTRAST						"Contrast"
#define	CAMERA_INFO_SHARPNESS						"Sharpness"
#define	CAMERA_INFO_NOISEREDUCTION					"NoiseReduction"
#define	CAMERA_INFO_SATURATION						"Saturation"
#define	CAMERA_INFO_HUE								"Hue"
#define	CAMERA_INFO_RGB_R							"RGB_r"
#define	CAMERA_INFO_RGB_G							"RGB_g"
#define	CAMERA_INFO_RGB_B							"RGB_b"
#define	CAMERA_INFO_COLORTEMPERATURE				"ColorTemperature"
#define	CAMERA_INFO_FOCUSPEAKINGLV					"FocusPeakingLv"
#define	CAMERA_INFO_FOCUSPEAKINGCOLOR				"FocusPeakingColor"
#define	CAMERA_INFO_LUMINANCELEVEL					"LuminanceLevel"
#define	CAMERA_INFO_DIGITALZOOM						"DigitalZoom"
#define	CAMERA_INFO_CMOSOFFSETCORRECTLEVEL			"CMOSOffsetCorrectLevel"
#define	CAMERA_INFO_LEDCONTROLMODE					"LedControlMode"
#define	CAMERA_INFO_MICLEVEL						"MicLevel"
#define	CAMERA_INFO_WINDNOISECANCELLATION			"WindNoiseCancellation"
#define	CAMERA_INFO_LENSPOWER						"LensPower"
#define	CAMERA_INFO_STREAMTYPE						"StreamType"

#define	CAMERA_INFO_VIDEOFORMAT_MAIN				"VideoFormatMain"
#define CAMERA_INFO_VIDEOENCODING_MAIN				"VideoEncodingMain"			// onvif new
#define CAMERA_INFO_VIDEORESOLUTION_MAIN			"VideoResolutionMain"		// onvif new
#define CAMERA_INFO_VIDEOFRAMERATE_MAIN				"VideoFrameRateMain"		// onvif new
#define CAMERA_INFO_VIDEOQUALITY_MAIN				"VideoQualityMain"			// onvif new
#define	CAMERA_INFO_VIDEOBITRATE_MAIN				"VideoBitrateMain"
#define	CAMERA_INFO_VIDEOGOP_MAIN					"VideoGopMain"
#define	CAMERA_INFO_VIDEOGOPVALUE_MAIN				"VideoGopValueMain"			// onvif new
#define CAMERA_INFO_STREAMPORT_MAIN					"StreamPortMain"			// onvif new
#define CAMERA_INFO_STREAMURL_MAIN					"StreamUrlMain"				// onvif new

#define	CAMERA_INFO_VIDEOFORMAT_SUB					"VideoFormatSub"
#define CAMERA_INFO_VIDEOENCODING_SUB				"VideoEncodingSub"			// onvif new
#define CAMERA_INFO_VIDEORESOLUTION_SUB				"VideoResolutionSub"		// onvif new
#define CAMERA_INFO_VIDEOFRAMERATE_SUB				"VideoFrameRateSub"			// onvif new
#define CAMERA_INFO_VIDEOQUALITY_SUB				"VideoQualitySub"			// onvif new
#define	CAMERA_INFO_VIDEOBITRATE_SUB				"VideoBitrateSub"
#define	CAMERA_INFO_VIDEOGOP_SUB					"VideoGopSub"
#define	CAMERA_INFO_VIDEOGOPVALUE_SUB				"VideoGopValueSub"			// onvif new
#define CAMERA_INFO_STREAMPORT_SUB					"StreamPortSub"				// onvif new
#define CAMERA_INFO_STREAMURL_SUB					"StreamUrlSub"				// onvif new

struct CmdResult
{
	bool result;
	std::string reason;
	CmdResult(bool _result = true, const std::string& _reason = "")
		: result(_result), reason(_reason)
	{}
};
typedef std::map<std::string, CmdResult> CmdResultMap;

enum SyncMode
{
	SYNC_MUST,
	SYNC_SKIP,
	SYNC_IGNORE,
};

class Camera
{
public:
	enum CameraState
	{
		STOPED = 0,
		SYNCED,
		PREPARED,	// Only FD10c
		STARTED,
	};

public:
	Camera(const std::string& strIP, const std::string& strType, const unsigned short syncPort);
	virtual ~Camera();

public:
	const std::string& GetCameraType() const;

	void SetConnected(bool bConnected);
	bool IsConnected();

	void SetState(const CameraState& state);
	CameraState	GetState();

	bool RunSync(const unsigned int uiSyncTime);
	void GetSyncResult(SyncResult& result) const;

	void SetRecordDelayTime(const unsigned int uiDelayTime) { m_uiRecordDelay = uiDelayTime; }
	void SetStatusNGCommand(int shift, bool b);
	unsigned int GetStatusNGCommandValue();

	virtual bool Connect(const std::string& strHostIP) = 0;
	virtual bool Disconnect() = 0;
	virtual bool Prepare();
	virtual bool Start(const unsigned int uiRecordTime, const unsigned int uiWaitTime, const int nSyncMode);
	virtual bool Stop();
	virtual bool SetCameraInfo(const std::map<std::string, std::map<std::string, std::string>>& mpCommand) = 0;
	virtual void GetCameraInfo(std::map<std::string, std::string>& mpCameraInfo, bool bForceUpdate);
	virtual void ClearCameraInfo();
	virtual bool UpdateFirmware(const std::string& ver, const std::string& subVer) { return false; }
	virtual void GetCommandResult(CmdResultMap& mpCmdResult) = 0;
	virtual int  GetGop() { return 0; }
	virtual unsigned int GetTemperature() { return 0; }
	virtual std::string GetStatusNGCommand(bool bStarted) = 0;
	virtual std::string GetStatusNGCommandAll() = 0;
	virtual int GetSyncMask() const = 0;

protected:
	std::mutex m_mutex;

	bool m_bConnected;
	CameraState m_state;

	std::string m_strIP;
	std::string m_strType;

	std::map<std::string, std::string> m_mpCameraInfo;

	UdpSync m_udpSync;
	unsigned int m_uiRecordDelay;	// ������ȭ��� (ms)

	unsigned int m_uiStatusNGCmd;
};