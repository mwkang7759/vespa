﻿#include "AsyncSession.h"

AsyncSession::AsyncSession(AsyncSession::Handler* handler, asio::ip::tcp::socket fd)
	: _handler(handler)
	, _fd(std::move(fd))
	, _recv_buffer(nullptr)
	, _recv_buffer_size(VSPD_MESSAGE_HEADER_SIZE)
	, _recv_buffer_index(0)
{
	_recv_buffer = static_cast<char*>(malloc(_recv_buffer_size));
	PostRecvHeader();
}

AsyncSession::~AsyncSession(void)
{
	
	while(!_wqueue.empty())
		_wqueue.pop_front();
}

int32_t AsyncSession::Send(const char* payload, int32_t length)
{
	AsyncSession::PACKET_HEADER_T h = { 0 };
	int32_t hl = VSPD_MESSAGE_HEADER_SIZE;
	h.z = length;

	std::shared_ptr<AsyncSession::PACKET_T> pkt = std::shared_ptr<AsyncSession::PACKET_T>(new AsyncSession::PACKET_T(reinterpret_cast<const char*>(&h), payload, hl, length));
	{
		std::lock_guard<std::mutex> lock(_lock);
		bool writeInProgress = !_wqueue.empty();
		_wqueue.push_back(pkt);
		if (!writeInProgress)
		{
			PostSend();
		}
	}
	return 0; // AsyncSession::ERR_CODE_T::SUCCESS;
}

void AsyncSession::PostSend(void)
{
	std::shared_ptr<AsyncSession::PACKET_T> pkt = nullptr;
	{
		std::lock_guard<std::mutex> lock(_lock);
		pkt = _wqueue.front();
	}
	if (pkt != nullptr)
	{
		auto self(shared_from_this());
		asio::async_write(_fd, asio::buffer(pkt->payload, pkt->length),
			[this, self](std::error_code ec, std::size_t)
			{
				if (!ec)
				{
					bool needWriteInProcess = false;
					{
						std::lock_guard<std::mutex> lock(_lock);
						_wqueue.pop_front();
					}

					{
						std::lock_guard<std::mutex> lock(_lock);
						needWriteInProcess = !_wqueue.empty();
					}

					if (needWriteInProcess)
					{
						PostSend();
					}
				}
				else
				{
					if (_handler)
						_handler->OnConnectionLost(shared_from_this());
				}
			});
	}
}

void AsyncSession::PostRecvHeader(void)
{
	asio::async_read(_fd,
		asio::buffer(_recv_buffer + _recv_buffer_index, VSPD_MESSAGE_HEADER_SIZE),
		[this](std::error_code ec, std::size_t /*length*/)
		{
			if (!ec)
			{
				AsyncSession::PACKET_HEADER_T pkt = { 0 };
				memmove(&pkt, _recv_buffer, VSPD_MESSAGE_HEADER_SIZE);
				_recv_buffer_index += VSPD_MESSAGE_HEADER_SIZE;

				BOOL bPost = TRUE;
				int32_t needed_buffer_count = pkt.z;
				if (needed_buffer_count > (_recv_buffer_size - _recv_buffer_index))
				{
					int32_t added_buffer_count = needed_buffer_count - (_recv_buffer_size - _recv_buffer_index);
					char * recv_buffer = static_cast<char*>(realloc(_recv_buffer, size_t(_recv_buffer_size + added_buffer_count)));
					if (recv_buffer != nullptr)
					{
						_recv_buffer = recv_buffer;
						_recv_buffer_size = _recv_buffer_size + added_buffer_count;
					}
					else
					{
						if (_handler)
							_handler->OnConnectionLost(shared_from_this());
						bPost = FALSE;
					}
				}
				if (bPost)
				{
					_recv_body_size = needed_buffer_count;
					if (_recv_buffer != nullptr && _recv_body_size > 0)
						PostRecvBody();
				}
			}
			else
			{
				if (_handler)
					_handler->OnConnectionLost(shared_from_this());
			}
		});
}

void AsyncSession::PostRecvBody(void)
{
	asio::async_read(_fd,
		asio::buffer(_recv_buffer + _recv_buffer_index, _recv_body_size),
		[this](std::error_code ec, std::size_t /*length*/)
		{
			if (!ec)
			{
				if (_handler)
					_handler->OnRecv(_recv_buffer + _recv_buffer_index, _recv_body_size);

				_recv_buffer_index = 0;
				_recv_body_size = 0;

				PostRecvHeader();
			}
			else
			{
				if (_handler)
					_handler->OnConnectionLost(shared_from_this());
			}
		});
}

