#pragma once

#include "AsyncClient.h"

#include "SystemAPI.h"
#include "TraceAPI.h"
#include "SocketAPI.h"
#include "StreamReaderAPI.h"
#include "StreamWriterAPI.h"



#define		MAX_TIME_DIFF				2000

#define		AV_SYNC_LOWER_LIMIT		0
#define		AV_SYNC_UPPER_LIMIT		(AV_SYNC_LOWER_LIMIT + 200)
#define		AV_SYNC_HIGH_LIMIT		(AV_SYNC_LOWER_LIMIT + 600)

#define _FRAME_STRUCT_EXTEND


struct SpdMsgHeader
{
    int z;
    char separator;
};

union unSpdMsgHeader
{
    SpdMsgHeader stHeader;
    unsigned char y[VSPD_MESSAGE_HEADER_SIZE];
};

class MTdClient
{
public:
    friend class OnConnectLostWorker;

    int32_t	Connect(const char* address, int32_t port);
    int32_t	Disconnect(void);

    int32_t Send(const char* payload, int32_t size);

private:
    void	OnConnectionLost(void);
    void	OnRecv(const char* payload, int32_t size);

private:
    AsyncClient _client;
};

typedef struct {
    char* pUrl;

    char szIp[1024];
    short port;
    int sessionNum;

} PreParam;

typedef struct {
    char* pSave;

    char szIp[1024];
    short port;
    
} PostParam;

enum {
    FRAME_TYPE_None,
    FRAME_TYPE_IFRAME,
    FRAME_TYPE_PFRAME,
    FRAME_TYPE_END,
};

enum {
    GOP_POS_MIDDLE,
    GOP_POS_START,
    GOP_POS_END,
    GOP_POS_NONE,
};

#pragma pack(push, 1)
struct stFrameData {
    stFrameData()
    {
        nTimeStamp = 0;
        nPostSdIP = 0;
        nFrameNum = -1;
        nCamCh = -1;
        nFrameType = FRAME_TYPE_None;
        nFrameSize = 0;
        nGopPos = -1;
        nGopIndex = 0;
        nRecordFrameNo = 0;
        lastInput = 0;
        pFrame = nullptr;
        pVPdMetaData = nullptr;
        pEtcMetaData = nullptr;
        nVPdMetaSize = 0;
        nEtcMetaSize = 0;
#ifdef _FRAME_STRUCT_EXTEND
        ulPostSdIP = 0;
        ulVPdIP = 0;
        memset(cRecordName, 0, sizeof(cRecordName));
#endif
    }
    long long nTimeStamp;
    int nPostSdIP;       // PostSd IP 의 4번째 자리
    int nFrameNum;
    int nCamCh;
    int nGopIndex;   // Gop 순번
    short nFrameType; // 1 : IFrame, 2 : PFrame
    short nGopPos;	//  0: Gop의 중간 Frame , 1: Gop 단위 전송 완료. 
    int nRecordFrameNo;
    uint64_t/*clock_t*/ lastInput;
#ifdef _FRAME_STRUCT_EXTEND
    unsigned long long ulPostSdIP;             //PostSdIP
    unsigned long long ulVPdIP;                //VPdIP
    unsigned char cRecordName[20];        //RecordName
#endif
    int nFrameSize;
    int nVPdMetaSize;
    int nEtcMetaSize;
    char* pFrame;
    char* pVPdMetaData;
    char* pEtcMetaData;
};
#pragma pack(pop)  

typedef struct {
    SOCK_HANDLE		m_hSocket;
    char* m_pIp;
    short           m_wPort;

} MTdSession;

typedef struct {
    SOCK_HANDLE		m_hSocket;
    char            *m_pIp;
    short           m_wPort;

    stFrameData     m_FrameData;
} PreChannel;

typedef struct {
    BOOL	        m_bEnd;
    FrReaderHandle	m_hReader;
    
    FrURLInfo	    m_UrlInfo;
    FrMediaInfo     m_SrcInfo;

    FrMediaStream   m_VideoStream;
    SOCK_HANDLE		m_hVpdSocket;
    TASK_HANDLE     m_hPreTask;

    MUTEX_HANDLE	m_xStatus;
    int				m_iStatus;

    char            m_szVpdIp[1024];
    char            m_szPostIp[1024];
    short           m_wVpdPort;
    
} PreSession;

typedef struct {
    BOOL	m_bEnd;
    FrWriterHandle	m_hWriter;

    FrURLInfo	m_UrlInfo;
    FrMediaInfo m_EncInfo;

    FrMediaStream m_VideoStream;
    SOCK_HANDLE		m_hSocket;
    TASK_HANDLE m_hPostTask;

    MUTEX_HANDLE		m_xStatus;
    int					m_iStatus;

    char            m_szVpdIp[1024];
    short           m_wPort;
    
} PostSession;


typedef struct {
	BOOL	m_bEnd;
	PreParam m_PreParam;
    PostParam m_PostParam;

    PreSession m_Pre;
    PostSession m_Post;
    
    //MTdClient m_Pcd;
    AsyncClient _client;

    // Pcd Emul
    SOCK_HANDLE		m_hPostSock;
    SOCK_HANDLE		m_hVpdSock;

} FdrEmulStruct;




int PreClose(PreSession* hPre);
int PreOpen(PreSession* hPre);
int PostClose(PostSession* hPost);
int PostOpen(PostSession* hPost);


void TaskPreSession(void* lpVoid);
void TaskPostSession(void* lpVoid);

void TaskEmulSession(void* lpVoid);
void TaskDmConnect(void* lpVoid);

PreSession* PreSessionOpen(PreParam* pParam);
void PreSessionClose(PreSession* hSession);
int PreSessionStart(PreSession* hSession);
int PreSessionStop(PreSession* hSession);