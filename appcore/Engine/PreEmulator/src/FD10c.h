#pragma once

#include "Camera.h"
#include "Markup.h"

#include <array>
#include <vector>
#include <list>
#include <thread>

template <class T>
void endianswap(T* objp)
{
	unsigned char* memp = reinterpret_cast<unsigned char*>(objp);
	std::reverse(memp, memp + sizeof(T));
}

#pragma pack(push, 1)
struct FD10Status
{
	// Status Header
	unsigned char HeaderLength;
	unsigned char BitField;
	unsigned char CommandID;
	unsigned char Reserved_h;
	unsigned int  DataLength;

	// Statys Paylod							// Offset(bytes)	| FIeld						| Size	| Value		| Description
	unsigned char	CameraStatus;				// 0				| Camera Status				| 1		| UINT8		| For setting values, refer to the state number in the state transition table in Appendix Chapter.
												// 0:Wait			1:Open			2:Pan / Tilt			3:Prepare			4:Streaming			5:Shutdown
	unsigned char	XZ01Status;					// 1				| XZ01 Status				| 1		| UINT8		| Power 1: On , 2: Off
	unsigned char	LensPowerStatus;			// 2				| Lens power status			| 1		| UINT8		| Power 1: On , 2: Off
	unsigned char	Reserved;					// 3				| Reserved					| 1		| -			| -
	unsigned short	CameraStatusInterval;		// 4				| Camera Status Interval	| 2		| UINT16	| Refer to OpenSession parameter
	unsigned short	CameraErrorCode;			// 6				| Camera Error Code			| 2		| UINT16	| Refer to Logging and OpenSession
	unsigned int	CalculatedAperture;			// 8				| Calculated Aperture		| 4		| UINT32	| magnified by 10 times
	unsigned int	CalculatedShutterSpeed;		// 12				| Calculated Shutter Speed	| 4		| UINT32	| in microseconds.
	unsigned int	CalculatedISO;				// 16				| Calculated Iso			| 4		| UINT32	| ISO speed
	unsigned int	RunningTime;				// 20				| Running Time				| 4		| UINT32	| Elapsed time since the camera started up. (in milliseconds)
	unsigned short	StreamBitrate_Main;			// 24				| Stream Bitrate(Main)		| 2		| UINT16	| Main stream bitrate
	unsigned short	StreamBitrate_Sub;			// 26				| Stream Bitrate(Sub)		| 2		| UINT16	| Sub stream bitrate
	float			Attitude_Pitch;				// 28				| Attitude (Pitch)			| 4		| FLOAT		| Pitch (in degrees, -180.0～180.0)
	float			Attitude_Roll;				// 32				| Attitude (Roll)			| 4		| FLOAT		| Roll (in degrees, -180.0～180.0)
	float			Attitude_Yaw;				// 36				| Attitude (Yaw)			| 4		| FLOAT		| Yaw (in degrees, -180.0～180.0)
	signed char		Temperature_Imagesensor;	// 40				| Temperature (Image sensor)| 1		| INT8		| Temperature (in degrees C, -128～127) ※1
	signed char		Temperature_Powersupply;	// 41				| Temperature (Power supply)| 1		| INT8		| Temperature (in degrees C, -128～127) ※1
	signed char		Temperature_CameraASIC;		// 42				| Temperature (Camera ASIC) | 1		| INT8		| Temperature (in degrees C, -128～127) ※1
	signed char		Temperature_NetworkSoC;		// 43				| Temperature (Network SoC) | 1		| INT8		| Temperature (in degrees C, -128～127) ※1
};
#pragma pack(pop)

class StatusCtx
{
	struct FD10StatusEx
	{
		FD10StatusEx()
		{
			memset(this, 0x0, sizeof(FD10StatusEx));
		}

		FD10Status status;
		unsigned long ulLastTime;
	};

public:
	StatusCtx() : m_bStop(false), m_pThread(nullptr) {}
	~StatusCtx();

	void DeleteAllInstance();

	void AddInstance(const std::string& strIP);
	void DeleteInstance(const std::string& strIP);

	typedef std::function<void(const std::string& strIP, bool bConnected, unsigned char lastStatus)> status_callback;
	void SetStatusCallback(status_callback f) { m_callback = std::move(f); }

	unsigned int GetTemperature_NetworkSoC(const std::string& strIP);

private:
	void StatusThread();

private:
	std::mutex m_mutex;
	std::map<std::string, FD10StatusEx> m_mpStatus;
	bool m_bStop;
	std::thread* m_pThread;
	status_callback m_callback;
};

class FD10c : public Camera
{
public:
	enum ComPort
	{
		TCP_PORT		= 50000,
		UDP_PORT_SYNC	= 50001,
		UDP_PORT_BEGIN	= 50002,
		UDP_PORT_END	= 52000,
	};

	enum ZoomPos
	{
		ZOOM_POS_UPPER = 0,
		ZOOM_POS_LOWER,
		ZOOM_POS_STEP,

		ZOOM_POS_SIZE,
	};

	enum StatusNGCommand
	{
		// 순서 변경 x (우선순위 NG 커맨드 1개만 메시지로 전달함)
		CMD_NG_UDPSTATUS = 0,
		CMD_NG_START,
		CMD_NG_PREPARE,
		CMD_NG_SYNC,
		CMD_NG_STOP,
		CMD_NG_OPENSESSION,
		CMD_NG_GET_CAMERAINFO,
		CMD_NG_GET_APERTURE,
		CMD_NG_GET_ZOOMPOS,
		CMD_NG_TURN_ON_LENS,
		CMD_NG_SET_FOCUSMODE,
		CMD_NG_CLOSESESSION,

		CMD_NG_MAX,	// Invalid 
	};

public:
	static StatusCtx s_StatusCtx;
	static bool GetFirmwareList(std::vector<std::string>& vecFirmware);

public: 
	FD10c(const std::string& strIP, const std::string& strModel);
	~FD10c();

public:
	// Derived Camera 
	virtual bool Connect(const std::string& strHostIP);
	virtual bool Disconnect();
	virtual bool Prepare();
	virtual bool Start(const unsigned int uiRecordTime, const unsigned int uiWaitTime, const int nSyncMode);
	virtual bool Stop();
	virtual bool SetCameraInfo(const std::map<std::string, std::map<std::string, std::string>>& mpCommand);
	virtual void ClearCameraInfo();
	virtual bool UpdateFirmware(const std::string& _strNXP, const std::string& _strXZ01);
	virtual void GetCommandResult(CmdResultMap& mpCmdResult);
	virtual int  GetGop();
	virtual unsigned int GetTemperature();
	virtual std::string GetStatusNGCommand(bool bStarted);
	virtual std::string GetStatusNGCommandAll();
	virtual int GetSyncMask() const { return CMD_NG_SYNC; }

	void GetApertureList(std::vector<int>& vecAperture);
	void GetZoomPosRange(int& nUpper, int& nLower, int& nStep);

private:
	bool PrepareStreaming();
	bool StartStreaming(const unsigned int uiRecordTime, const unsigned int uiWaitTime, const int nSyncMode);
	void SetCommandResult(const std::string& strCmd, bool bResult, const std::string& strReason = "");

	bool SendCommand(const std::string& strCmd, const std::map<std::string, std::string>& mpArg = std::map<std::string, std::string>());
	void OnResponse(const std::string& strCmd, const int nResponseCode, const std::string& strContents);

	void SetCameraProperty(CMarkup& xml, const std::string& strProperty);
	void SetStatusNG_FD10Command(const std::string& strCmd);

	void GetFirmwareFilePath(const std::string& strCmd, const std::string& strVer, std::string& strPath);

private:
	std::vector<int> m_vecAperture;
	std::array<int, ZOOM_POS_SIZE> m_arrZoomPos;

	// SetCameraInfo 호출시 각 Command 응답을 보관 (Command와 InfoName이 일치하지 않음)
	// ex1) Command: SetXXX, InfoName: XXX
	// ex2) Command: SetRGB, InfoName: RGB_R, RGB_G, RGB_B
	CmdResultMap m_mpSetCmdResult;
};