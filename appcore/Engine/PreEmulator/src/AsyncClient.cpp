﻿#include "AsyncClient.h"

AsyncClient::AsyncClient(void)
	: _fd(_context)
	, _thread(INVALID_HANDLE_VALUE)
	, _session(nullptr)
	, _handler(nullptr)
{

}

AsyncClient::~AsyncClient(void)
{

}

int32_t AsyncClient::Connect(AsyncClient::Handler* handler, const char* address, int32_t portnumber)
{
	char sportnumber[MAX_PATH] = { 0 };
	_itoa_s(portnumber, sportnumber, 10);
	_handler = handler;

	asio::ip::tcp::resolver resolver(_context);
	auto endpoint = resolver.resolve(address, sportnumber);
	PostConnect(endpoint);

	_thread = (HANDLE)_beginthreadex(NULL, 0, AsyncClient::ProcessCB, this, 0, NULL);
	return 0; //AsyncClient::ERR_CODE_T::SUCCESS;
}

int32_t AsyncClient::Disconnect(void)
{
	asio::post(_context, [this]()
		{
			_fd.close();
		});

	if (::WaitForSingleObject(_thread, INFINITE) == WAIT_OBJECT_0)
	{
		::CloseHandle(_thread);
		_thread = INVALID_HANDLE_VALUE;
	}
	return 0; //AsyncClient::ERR_CODE_T::SUCCESS;
}

int32_t AsyncClient::Send(const char* payload, int32_t length)
{
	if (_session)
		return _session->Send(payload, length);
	return 1; //AsyncClient::ERR_CODE_T::GENERIC_FAIL;
}

int32_t AsyncClient::PostConnect(const asio::ip::tcp::resolver::results_type& endpoints)
{
	asio::async_connect(_fd, endpoints,
		[this](std::error_code ec, asio::ip::tcp::endpoint)
		{
			if (!ec)
			{
				_session = std::make_shared<AsyncSession>(this, std::move(_fd));
			}
		});
	return 0; //AsyncClient::ERR_CODE_T::SUCCESS;
}

void AsyncClient::OnConnectionLost(std::shared_ptr<AsyncSession> session)
{
	_fd.close();
	if (_handler)
		_handler->OnConnectionLost();
}

void AsyncClient::OnRecv(const char* payload, int32_t size)
{
	if (_handler)
		_handler->OnRecv(payload, size);
}

void AsyncClient::Process(void)
{
	_context.run();
}

unsigned AsyncClient::ProcessCB(void* param)
{
	AsyncClient* self = static_cast<AsyncClient*>(param);
	self->Process();
	return 0;
}