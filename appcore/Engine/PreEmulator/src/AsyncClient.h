﻿#pragma once

#include <cstdlib>
#include <asio.hpp>
#include "AsyncSession.h"


class AsyncClient : public AsyncSession::Handler
{
public:
	class Handler
	{
	public:
		virtual void OnConnectionLost(void) = 0;
		virtual void OnRecv(const char* payload, int32_t size) = 0;
	};

	AsyncClient(void);
	~AsyncClient(void);

	int32_t Connect(AsyncClient::Handler * handler, const char* address, int32_t portnumber);
	int32_t Disconnect(void);
	int32_t Send(const char* payload, int32_t length);

private:
	int32_t PostConnect(const asio::ip::tcp::resolver::results_type& endpoints);

	void	OnConnectionLost(std::shared_ptr<AsyncSession> session);
	void	OnRecv(const char* payload, int32_t size);

	static unsigned __stdcall ProcessCB(void* param);
	void Process(void);

	asio::io_context		_context;
	asio::ip::tcp::socket	_fd;

	HANDLE					_thread;

	std::shared_ptr<AsyncSession> _session;

	AsyncClient::Handler* _handler;
};
