#include "FdrEmulator.h"
#include "FdrEmulatorAPI.h"

void TaskPreSession(void* lpVoid) {
    PreSession* hPre = (PreSession*)lpVoid;

    LRSLT lRet;
    DWORD32 dwTime;
    int iDiff, frameCnt = 0;
    BOOL bReadFrame = FALSE;
    FrMediaStream VideoData;
    FrMediaStream EncStream;

    memset(&EncStream, 0, sizeof(FrMediaStream));
    memset(&VideoData, 0, sizeof(FrMediaStream));
    VideoData.tMediaType = VIDEO_MEDIA;
    EncStream.tMediaType = VIDEO_MEDIA;

    TRACE("TaskPreSession() Begin..");

    while (!hPre->m_bEnd) {
        if (!bReadFrame) {
            lRet = FrReaderReadFrame(hPre->m_hReader, &VideoData);
            if (FRFAILED(lRet)) {
                if (lRet == COMMON_ERR_ENDOFDATA)
                    break;

                FrSleep(10);        // assume to wait data..
                continue;
            }


            // set frameinfo
            stFrameData frameData;
            memset(&frameData, 0, sizeof(stFrameData));
            frameData.nPostSdIP = 106;
            frameData.nCamCh = 1;
            if (VideoData.tFrameType[0] == FRAME_I)
                frameData.nFrameType = 1;   // 1: I, 2: P
            else
                frameData.nFrameType = 2;   // 1: I, 2: P
            frameData.nFrameNum = ++frameCnt;
            frameData.nFrameSize = VideoData.dwFrameLen[0];
            // post send ip
             frameData.ulPostSdIP = inet_addr(hPre->m_szPostIp);
            //frameData.ulPostSdIP = inet_addr("10.82.5.106");
            //frameData.pFrame = (char*)MALLOC(VideoData.dwFrameLen[0]);
            //memcpy(frameData.pFrame, VideoData.pFrame, VideoData.dwFrameLen[0]);
            

            McSocketSend(hPre->m_hVpdSocket, (char*)&frameData, sizeof(stFrameData));
            McSocketSend(hPre->m_hVpdSocket, (char*)VideoData.pFrame, VideoData.dwFrameLen[0]);

        }
        else {
            FrSleep(10);
        }
    }

    McCloseTask(hPre->m_hPreTask);
    hPre->m_hPreTask = INVALID_TASK;

    TRACE("TaskPreSession end..");

    McExitTask();
}

void TaskPostSession(void* lpVoid) {
    PostSession* hPost = (PostSession*)lpVoid;

    LRSLT lRet;
    DWORD32 dwTime;
    int iDiff, frameCnt = 0;
    BOOL bAccept = FALSE;
    
    FrMediaStream EncStream;
    SOCK_HANDLE		hSocket = INVALID_SOCK;
    DWORD32			dwRemoteAddr;
    WORD			wRemotePort;
    stFrameData     frameData;
    char* pBuffer;

    memset(&EncStream, 0, sizeof(FrMediaStream));
    
    
    EncStream.tMediaType = VIDEO_MEDIA;

    TRACE("TaskPostSession() Begin..");

    while (!hPost->m_bEnd) {
        /*if (!bAccept) {
            lRet = McSocketAccept(&hSocket, hPost->m_hSocket, &dwRemoteAddr, &wRemotePort);
            if (FRFAILED(lRet)) {
                LOG_E("SessionOpen() - Connection is not accepted\n");
                break;
            }
            bAccept = TRUE;
        }*/

        // stream info
        
        DtbSocketRecv(hSocket, (char*)&frameData, sizeof(stFrameData));
        pBuffer = (char*)MALLOC(frameData.nFrameSize);
        DtbSocketRecv(hSocket, pBuffer, frameData.nFrameSize);

        TRACE("TaskPostSession recv data: frame size: %d, time: %d", frameData.nFrameSize, frameData.nTimeStamp);

        EncStream.pFrame = (BYTE*)pBuffer;
        EncStream.dwFrameNum = 1;
        EncStream.dwFrameLen[0] = frameData.nFrameSize;
        EncStream.dwCTS = frameData.nTimeStamp;
        if (frameData.nFrameType == 1)
            EncStream.tFrameType[0] = FRAME_I;
        else
            EncStream.tFrameType[0] = FRAME_P;
        lRet = FrWriterWriteFrame(hPost->m_hWriter, &EncStream);
        if (FRFAILED(lRet)) {
            
        }

        FREE(pBuffer);
    }

    McCloseTask(hPost->m_hPostTask);
    hPost->m_hPostTask = INVALID_TASK;

    TRACE("TaskPostSession end..");

    McExitTask();
}
#if 0
void TaskEmulListener(void* lpVoid) {
    FrEmulStruct* hEmul = (FrEmulStruct*)lpVoid;
    FD_SET_TYPE			rSet;
    LONG				lRet;

    LOG_I("TaskEmulListener - Start");

    while (!hEmul->m_bEnd) {
        FD_ZERO(&rSet);
        FD_SET(hEmul->m_hDmSocket->m_hSocket, &rSet);

        lRet = McSocketSelect(&rSet, NULL, NULL);
        if (FRFAILED(lRet)) {
            continue;
        }

        if (FD_ISSET(hEmul->m_hDmSocket->m_hSocket, &rSet)) {
            lRet = CreateEmulSession(hEmul);
            if (FRFAILED(lRet)) {
                hEmul->m_lError = lRet;
                LOG_E("[GameSender]	TaskListener - SessionOpen() failed!");
                continue;
            }
        }
    }

    McCloseTask(hEmul->m_hListenTask);
    hEmul->m_hListenTask = INVALID_TASK;
    LOG_I("TaskEmulListener - End");
    McExitTask();
}

void TaskDmConnect(void* lpVoid) {
    FrEmulStruct* hEmul = (FrEmulStruct*)lpVoid;

    LRSLT lRet;
    DWORD32 dwTime;
    int iDiff;
    BOOL bReadFrame = FALSE;
    FrMediaStream VideoData;
    FrMediaStream EncStream;
    FD_SET_TYPE			rSet;

    memset(&EncStream, 0, sizeof(FrMediaStream));
    memset(&VideoData, 0, sizeof(FrMediaStream));
    VideoData.tMediaType = VIDEO_MEDIA;
    EncStream.tMediaType = VIDEO_MEDIA;

    TRACE("TaskDmConnect() Begin..");

    while (!hEmul->m_bEnd) {
        FD_ZERO(&rSet);
        FD_SET(hEmul->m_hDmSocket->m_hSocket, &rSet);

        lRet = McSocketSelect(&rSet, NULL, NULL);
        if (FRFAILED(lRet)) {
            if (lRet != COMMON_ERR_TIMEOUT) {
                TRACE("TaskDmConnect - Error (%d) is not time out...", lRet);
                goto CLEAR;

            }
            continue;
        }

        if (FD_ISSET(hEmul->m_hDmSocket->m_hSocket, &rSet)) {
            // receive data and parse
            // ..
            TRACE("TaskDmConnect() Receive data..");
            
        }
    }

    
CLEAR:
    //FREE(hSession);
    /*McCloseTask(hEmul->m_hTask);
    hEmul->m_hTask = INVALID_TASK;*/

    TRACE("TaskDmConnect end..");

    McExitTask();
}
#endif
#if 0
void TaskEmulSession(void* lpVoid) {
    EmulSession* hSession = (EmulSession*)lpVoid;

    LRSLT lRet;
    DWORD32 dwTime;
    int iDiff;
    BOOL bReadFrame = FALSE;
    FrMediaStream VideoData;
    FrMediaStream EncStream;
    FD_SET_TYPE			rSet;
    
    memset(&EncStream, 0, sizeof(FrMediaStream));
    memset(&VideoData, 0, sizeof(FrMediaStream));
    VideoData.tMediaType = VIDEO_MEDIA;
    EncStream.tMediaType = VIDEO_MEDIA;

    TRACE("TaskEmulSession() Begin..");

    while (1) {
        FD_ZERO(&rSet);
        FD_SET(hSession->m_hSocket->m_hSocket, &rSet);

        lRet = McSocketSelect(&rSet, NULL, NULL);
        if (FRFAILED(lRet)) {
            if (lRet != COMMON_ERR_TIMEOUT) {
                TRACE("TaskEmulSession - Error (%d) is not time out...", lRet);
                goto CLEAR;
                
            }
            continue;
        }

        if (FD_ISSET(hSession->m_hSocket->m_hSocket, &rSet)) {
            /*if (!bHeader) {

            }
            if (bHeader) {

            }*/

            break;
        }
    }

    // 1. parse json
    // 2. command pre
    //if (cmd == presd) {
        //TaskPreSession();
    {
        PreSession* hPreSession = PreSessionOpen(hSession->m_pPreParam);
        PreSessionStart(hPreSession);
        /*while (hPreSession->m_bEnd) {
            FrSleep(10);
        }*/
        PreSessionStop(hPreSession);
        PreSessionClose(hPreSession);
    }
        
    //}

CLEAR:
    FREE(hSession);
    /*McCloseTask(hEmul->m_hTask);
    hEmul->m_hTask = INVALID_TASK;*/

    TRACE("TaskEmulSession end..");

    McExitTask();
}
#endif

