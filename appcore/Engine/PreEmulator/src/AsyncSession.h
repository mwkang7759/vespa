﻿#pragma once

#include <cstdlib>
#include <deque>
#include <asio.hpp>

#define	VSPD_MESSAGE_HEADER_SIZE	5

class AsyncSession : public std::enable_shared_from_this<AsyncSession>
{
public:
	class Handler
	{
	public:
		virtual void OnConnectionLost(std::shared_ptr<AsyncSession> session) = 0;
		virtual void OnRecv(const char* payload, int32_t size) = 0;
	};
	typedef struct _PACKET_HEADER_T
	{
		int32_t z;
		char	separator;
	} PACKET_HEADER_T;

	typedef struct _PACKET_T
	{
		char* payload;
		int32_t length;
		_PACKET_T(const char * h, const char* b, int32_t hl, int32_t bl)
			: length(hl+bl)
		{
			payload = static_cast<char*>(malloc(length));
			memmove(payload, h, hl);
			memmove(payload + hl, b, bl);
		}

		~_PACKET_T(void)
		{
			if (payload)
				free(payload);
			payload = NULL;
			length = 0;
		}
	} PACKET_T;

	AsyncSession(AsyncSession::Handler * handler, asio::ip::tcp::socket fd);
	~AsyncSession(void);

	int32_t Send(const char* payload, int32_t length);

private:
	void PostSend(void);

	void PostRecvHeader(void);
	void PostRecvBody(void);

private:
	AsyncSession::Handler* _handler;
	asio::ip::tcp::socket _fd;
	//CRITICAL_SECTION _lock;
	std::mutex _lock;
	std::deque<std::shared_ptr<AsyncSession::PACKET_T>> _wqueue;

	char* _recv_buffer;
	int32_t _recv_buffer_size;
	int32_t _recv_buffer_index;
	int32_t _recv_body_size;
};
