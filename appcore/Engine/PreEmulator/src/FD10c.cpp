#include "FD10c.h"
#include "HttpClient.h"
#include "Network.h"
#include "ErrorList.h"

#include <vector>
#include <thread>

#define URL_CGI_XACTI						"xaccma.cgi"
#define URL_LOG_XACTI						"xaccma.log"

#define CMD_ERR_LOG							"errorLog"
#define CMD_FW_UPDATE_NXP					"FirmwareNXP"		// network
#define CMD_FW_UPDATE_XZ01					"FirmwareXZ01"		// camera
#define CMD_OPEN_SESSION				    "OpenSession"
#define CMD_CLOSE_SESSION					"CloseSession"
#define CMD_SET_PREPARE_STREAMING			"SetPrepareStreaming"
#define CMD_SET_START_STREAMTIME			"SetStartStreamTime"
#define CMD_SET_STOP_STREAMING				"SetStopStreaming"
#define CMD_RESET_VSYNC						"ResetVsync"
#define CMD_SET_PPM_FOR_VSYNC				"SetPpmForVsync"
#define CMD_ONE_SHOT_AF						"OneShotAF"
#define CMD_STORE_LENS_POS					"StoreLensPosition"
#define CMD_REBOOT							"Reboot"
#define CMD_SET_LED_CONTROL_MODE			"SetLedControlMode"
#define CMD_SET_FOCUS_MODE					"SetFocusMode"
#define CMD_SET_AUTO_FOCUS_POINT			"SetAutoFocusPoint"
#define CMD_GET_AUTO_FUCOS_POINT			"GetAutoFocusPoint"
#define CMD_SET_AE_AREA						"SetAeArea"
#define CMD_SET_IMAGE_STABILIZATION_MODE	"SetImageStabilizationMode"
#define CMD_SET_FOCUS_PEAKING				"SetFocusPeaking"
#define CMD_SET_VIDEO_LUMINANCE_LEVEL		"SetVideoLuminanceLevel"
#define CMD_SET_ISO							"SetIso"
#define CMD_GET_ISO							"GetIso"
#define CMD_SET_MANUAL_FOCUS_POS			"SetManualFocusPosition"
#define CMD_GET_MANUAL_FOCUS_POS			"GetManualFocusPosition"
#define CMD_SET_ZOOM_POS					"SetZoomPosition"
#define CMD_GET_ZOOM_POS					"GetZoomPosition"
#define CMD_GET_CAMERA_INFO					"GetCameraInfo"
#define CMD_UPDATE_NTP_DATE					"UpdateNtpDate"
#define CMD_TURN_OFF_LENS					"TurnOffLens"
#define CMD_TURN_ON_LENS					"TurnOnLens"
#define CMD_CALIBATE_CUSTOM_WHITE_BALANCE	"CalibrateCustomWhiteBalance"
#define CMD_SHUTDOWN						"Shutdown"
#define CMD_SET_EXPOSURE_COMPENSATION		"SetExposureCompensation"
#define CMD_SET_SHUTTER_SPEED				"SetShutterSpeed"
#define CMD_GET_SHUTTER_SPEED				"GetShutterSpeed"
#define CMD_SET_VIDEO_FORMAT				"SetVideoFormat"
#define CMD_GET_VIDEO_FORMAT				"GetVideoFormat"
#define CMD_SET_FOCUS_LOCATION				"SetFocusLocation"
#define CMD_GET_FOCUS_LOCATION				"GetFocusLocation"
#define CMD_SET_VIDEO_DIGITAL_ZOOM			"SetVideoDigitalZoom"
#define CMD_GET_VIDEO_DIGITAL_ZOOM			"GetVideoDigitalZoom"
#define CMD_SET_SATURATION					"SetSaturation"
#define CMD_GET_SATURATION					"GetSaturation"
#define CMD_SET_HUE							"SetHue"
#define CMD_GET_HUE							"GetHue"
#define CMD_SET_SHARPNESS					"SetSharpness"
#define CMD_GET_SHARPNESS					"GetSharpness"
#define CMD_SET_CONTRAST					"SetContrast"
#define CMD_GET_CONTRAST					"GetContrast"
#define CMD_SET_NOISE_REDUCTION				"SetNoiseReduction"
#define CMD_GET_NOISE_REDUCTION				"GetNoiseReduction"
#define CMD_SET_VIDEO_BITRATE				"SetVideoBitrate"
#define CMD_GET_VIDEO_BITRATE				"GetVideoBitrate"
#define CMD_SET_WHITE_BALANCE_MODE			"SetWhiteBalanceMode"
#define CMD_GET_WHITE_BALANCE_MODE			"GetWhiteBalanceMode"
#define CMD_SET_COLOR_TEMPERATURE			"SetColorTemperature"
#define CMD_GET_COLOR_TEMPERATURE			"GetColorTemperature"
#define CMD_SET_APERTURE					"SetAperture"
#define CMD_GET_APERTURE					"GetAperture"
#define CMD_GET_FOCUS_MODE					"GetFocusMode"
#define CMD_SET_EXPOSURE_MODE				"SetExposureMode"
#define CMD_GET_EXPOSURE_MODE				"GetExposureMode"
#define CMD_SET_CMOS_OFFSET_CORRECT_LEVEL	"SetCMOSOffsetCorrectLevel"
#define CMD_SET_RGB							"SetRGB"
#define CMD_NOT_OPEN_SESSION			    "NotOpenSession"

#define RESULT_OK							"OK"
#define RESULT_NG							"NG"

#define FIRMWARE_DIR						"./firmware/FD10"
#define FIRMWARE_NXP_DIR					"NXP"
#define FIRMWARE_NXP_FILE					"fwupdate.tar.gz"
#define FIRMWARE_XZ01_DIR					"ASIC"
#define FIRMWARE_XZ01_FILE					"firmware.bin"

#define STATUS_INTERVAL		3				// 3 sec
#define _ASYNC_CMD_

StatusCtx::~StatusCtx()
{
	DeleteAllInstance();
}

void StatusCtx::DeleteAllInstance()
{
	m_bStop = true;
	if (m_pThread != nullptr)
	{
		if (m_pThread->joinable())
		{
			m_pThread->join();
		}
		delete m_pThread;
		m_pThread = nullptr;
	}

	m_mpStatus.clear();
}

void StatusCtx::AddInstance(const std::string& strIP)
{
	bool bEmpty = false;
	FD10StatusEx statusEx;
	statusEx.ulLastTime = GetTickTime();

	std::unique_lock<std::mutex> locker(m_mutex);
	bEmpty = m_mpStatus.empty();
	m_mpStatus[strIP] = statusEx;
	locker.unlock();

	if (bEmpty && m_pThread == nullptr)
	{
		m_bStop = false;
		m_pThread = new std::thread(&StatusCtx::StatusThread, this);
	}
}

void StatusCtx::DeleteInstance(const std::string& strIP)
{
	bool bEmpty = false;
	std::unique_lock<std::mutex> locker(m_mutex);
	const auto itr = m_mpStatus.find(strIP);
	if (itr != m_mpStatus.end())
	{
		m_mpStatus.erase(itr);
	}
	bEmpty = m_mpStatus.empty();
	locker.unlock();

	if (bEmpty && m_pThread != nullptr)
	{
		m_bStop = true;
		if (m_pThread->joinable())
		{
			m_pThread->join();
		}
		delete m_pThread;
		m_pThread = nullptr;
	}
}

unsigned int StatusCtx::GetTemperature_NetworkSoC(const std::string& strIP)
{
	unsigned int uiTemperature = 0;
	std::lock_guard<std::mutex> locker(m_mutex);
	const auto itr = m_mpStatus.find(strIP);
	if (itr != m_mpStatus.end())
	{
		uiTemperature = (itr->second).status.Temperature_NetworkSoC;
	}
	return uiTemperature;
}

void StatusCtx::StatusThread()
{
	TraceL << "[FD10] Status Thread Start";

	UDPSocket socket;
	socket.Bind(FD10c::UDP_PORT_BEGIN);

	const unsigned int bufSize = 56 + 1;	// RecvData(56) + \n(1)
	char buff[bufSize] = { 0, };

	while (!m_bStop) {
		sockaddr_in addr_in;
		memset(buff, 0x0, bufSize);

		bool bUpdate = socket.RecvFrom(buff, bufSize, addr_in);
		const unsigned long ulCurTickTime = GetTickTime();

		FD10Status status = { 0, };
		std::string strIP;

		if (bUpdate) {
			memcpy(&status, buff, sizeof(status));

			// Endian Convert
			status.DataLength = ntohl(status.DataLength);
			status.CameraStatusInterval = ntohs(status.CameraStatusInterval);
			status.CameraErrorCode = ntohs(status.CameraErrorCode);
			status.CalculatedAperture = ntohl(status.CalculatedAperture);
			status.CalculatedShutterSpeed = ntohl(status.CalculatedShutterSpeed);
			status.CalculatedISO = ntohl(status.CalculatedISO);
			status.RunningTime = ntohl(status.RunningTime);
			status.StreamBitrate_Main = ntohs(status.StreamBitrate_Main);
			status.StreamBitrate_Sub = ntohs(status.StreamBitrate_Sub);
			endianswap(&status.Attitude_Pitch);
			endianswap(&status.Attitude_Roll);
			endianswap(&status.Attitude_Yaw);

			strIP = inet_ntoa(addr_in.sin_addr);
		}

		std::unique_lock<std::mutex> lock(m_mutex);
		for (auto& v : m_mpStatus) {
			FD10StatusEx& statusEx = v.second;

			if (v.first == strIP) {
				memcpy(&statusEx.status, &status, sizeof(status));
				statusEx.ulLastTime = ulCurTickTime;
			}

			if (m_callback != nullptr) {
				// CurTime - LastTime > 10 sec -> Disconnected
				bool bConnected = (ulCurTickTime - statusEx.ulLastTime < 10000);
				m_callback(v.first, bConnected, v.second.status.CameraStatus);
			}
		}
		lock.unlock();

		if (bUpdate && status.Temperature_NetworkSoC >= 103) {
			FD_ErrorL(FDPrinter << "[FD10] " << strIP << " NetworkSoc Temperature is high.  PoE+hub off (FD10 Off).");
		}

		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}

	socket.CloseSocket();

	TraceL << "[FD10] Status Thread End";
}

StatusCtx FD10c::s_StatusCtx;

bool FD10c::GetFirmwareList(std::vector<std::string>& vecFirmware)
{
	const std::string strFirmDir = FIRMWARE_DIR;
	fs::path path(strFirmDir);
	if (fs::exists(path) == false || fs::is_directory(path) == false) {
		return false;
	}

	for (const auto& v : fs::directory_iterator(path)) {
		bool bAsic = false;
		bool bNXP = false;
		if (fs::is_directory(v)) {
			fs::path path_asic(v.path() / FIRMWARE_XZ01_DIR);
			if (fs::exists(path_asic) && fs::is_directory(path_asic)) {
				path_asic /= FIRMWARE_XZ01_FILE;
				bAsic = (fs::exists(path_asic) && fs::is_regular_file(path_asic));
			}

			fs::path path_nxp(v.path() / FIRMWARE_NXP_DIR);
			if (fs::exists(path_nxp) && fs::is_directory(path_nxp)) {
				path_nxp /= FIRMWARE_NXP_FILE;
				bNXP = (fs::exists(path_nxp) && fs::is_regular_file(path_nxp));
			}

			if (bAsic && bNXP) {
				std::string strVersion = v.path().u8string();
				strVersion = strVersion.substr(strFirmDir.length() + 1);

				vecFirmware.push_back(strVersion);
			}
		}
	}

	return true;
}

FD10c::FD10c(const std::string& strIP, const std::string& strModel)
	: Camera(strIP, strModel, UDP_PORT_SYNC)
{
	m_arrZoomPos.fill(0);
}

FD10c::~FD10c()
{
	FD10c::s_StatusCtx.DeleteInstance(m_strIP);
}

bool FD10c::Connect(const std::string& strHostIP)
{
	std::map<std::string, std::string> mpArg;

	mpArg["arg_ip"] = strHostIP;
	mpArg["arg_udp_port"] = std::to_string(UDP_PORT_BEGIN);
	mpArg["arg_camera_status_interval"] = std::to_string(STATUS_INTERVAL);	// 3sec [1,3600]
	mpArg["arg_debug"] = "";	// For debug ([0,99] Not required)

	bool bRet = SendCommand(CMD_OPEN_SESSION, mpArg);

	if (IsConnected()) {
		std::map<std::string, std::string> args;
		std::vector<std::thread> vec;
		// LensPower가 Off일 경우
		// 1. [ManualFocusPosition, ZoomPosition, FocusMode, Aperture] 정보를 CameraInfo에서 받아올 수 없음
		// 2. ZoomPosition, ManualFocusPosition을 변경할 수 없음
		vec.push_back(std::thread(&FD10c::SendCommand, this, CMD_TURN_ON_LENS, args));
		std::this_thread::sleep_for(std::chrono::milliseconds(50));
		vec.push_back(std::thread(&FD10c::SendCommand, this, CMD_GET_CAMERA_INFO, args));
		std::this_thread::sleep_for(std::chrono::milliseconds(50));
		vec.push_back(std::thread(&FD10c::SendCommand, this, CMD_GET_APERTURE, args));
		std::this_thread::sleep_for(std::chrono::milliseconds(50));
		vec.push_back(std::thread(&FD10c::SendCommand, this, CMD_GET_ZOOM_POS, args));

		for (auto& v : vec) {
			if(v.joinable())
				v.join();
		}

		// FocusMode: Manual만 사용
		const auto itr = m_mpCameraInfo.find(CAMERA_INFO_FOCUSMODE);
		if (itr != m_mpCameraInfo.end() && itr->second != "Manual") {
			args["arg"] = "Manual";
			std::thread th = std::thread(&FD10c::SendCommand, this, CMD_SET_FOCUS_MODE, args);
			if(th.joinable())
				th.join();
		}
	}

	return bRet;
}

bool FD10c::Disconnect()
{
	SendCommand(CMD_CLOSE_SESSION);
	return true;
}

bool FD10c::Prepare()
{
	return PrepareStreaming();
}

bool FD10c::Start(const unsigned int uiRecordTime, const unsigned int uiWaitTime, const int nSyncMode)
{
	if (GetState() != PREPARED) {
		WarnL << "[FD10] " << m_strIP << " Reject <Run> - PrepareStreaming Fail";
		return false;
	}

	return StartStreaming(uiRecordTime, uiWaitTime, nSyncMode);
}

bool FD10c::Stop()
{
	SendCommand(CMD_SET_STOP_STREAMING);
	return (GetState() == STOPED);
}

bool FD10c::SetCameraInfo(const std::map<std::string, std::map<std::string, std::string>>& mpCommand)
{
	std::vector<std::thread> vec;

	std::unique_lock<std::mutex> lock(m_mutex);
	m_mpSetCmdResult.clear();
	for (const auto& v : mpCommand) {
		// Reboot 명령은 응답을 받지 않음 -> true 상태로 시작하여 실패시 false로 변경
		if (v.first == CMD_REBOOT) {
			m_mpSetCmdResult[v.first] = CmdResult(true);
		}
		// LensPower Off 상태에서는 동작할수 없음
		else if (v.first == CMD_SET_MANUAL_FOCUS_POS || v.first == CMD_SET_ZOOM_POS) {
			bool bLensOn = false;
			auto itr = m_mpCameraInfo.find(CAMERA_INFO_LENSPOWER);
			if (itr != m_mpCameraInfo.end()) {
				bLensOn = (itr->second == "On");
			}

			if (bLensOn == false) {
				m_mpSetCmdResult[v.first] = CmdResult(false, "LensPower Off");
				continue;
			}
			else {
				m_mpSetCmdResult[v.first] = CmdResult(false);
			}
		}
		else {
			m_mpSetCmdResult[v.first] = CmdResult(false);
		}
		vec.push_back(std::thread(&FD10c::SendCommand, this, v.first, v.second));
		std::this_thread::sleep_for(std::chrono::milliseconds(50));
	}
	lock.unlock();

	for (auto& v : vec) {
		v.join();
	}

	return true;
}

void FD10c::ClearCameraInfo()
{
	std::lock_guard<std::mutex> lock(m_mutex);
	m_mpCameraInfo.clear();
	m_vecAperture.clear();
	for (int i = 0; i < ZOOM_POS_SIZE; ++i) {
		m_arrZoomPos[i] = 0;
	}
}

bool FD10c::UpdateFirmware(const std::string& _strNXP, const std::string& _strXZ01)
{
	// ex
	//http://192.168.17.195:50000/xaccma.firm/XZ01  // firmware.bin
	//http://192.168.17.195:50000/xaccma.firm/NXP   // fwupdate.tar.gz

	if (IsConnected() == false) {
		m_mpSetCmdResult[CMD_FW_UPDATE_XZ01] = CmdResult(false, "Not Connected");
		m_mpSetCmdResult[CMD_FW_UPDATE_NXP] = CmdResult(false, "Not Connected");
		WarnL << "[FD10]" << m_strIP  << " Reject <Update Firmware> - Disconnected";
		return false;
	}

	std::string strNXP;
	std::string strXZ01;

	std::unique_lock<std::mutex> lock(m_mutex);
	auto itr = m_mpCameraInfo.find("FirmwareNXP");
	if (itr != m_mpCameraInfo.end()) {
		strNXP = m_mpCameraInfo["FirmwareNXP"];
	}

	itr = m_mpCameraInfo.find("FirmwareXZ01");
	if (itr != m_mpCameraInfo.end()) {
		strXZ01 = m_mpCameraInfo["FirmwareXZ01"];
	}

	//m_mpSetCmdResult.clear();
	m_mpSetCmdResult[CMD_FW_UPDATE_XZ01] = CmdResult(true);
	m_mpSetCmdResult[CMD_FW_UPDATE_NXP] = CmdResult(true);
	lock.unlock();

	HttpClient http;
	std::string strUrl;
	std::string strFilePath;

	// 업데이트 후 카메라 재부팅 없음
	if (_strXZ01 != strXZ01) {
		strUrl = "http://" + m_strIP + ":" + std::to_string(TCP_PORT) + "/xaccma.firm/XZ01";
		GetFirmwareFilePath(CMD_FW_UPDATE_XZ01, _strXZ01, strFilePath);
		if (strFilePath.empty() == false) {
			if (http.PostFileUpload(strUrl, strFilePath, m_strIP, CMD_FW_UPDATE_XZ01) == false) {
				SetCommandResult(CMD_FW_UPDATE_XZ01, false);
				FD_ErrorL(FDPrinter << "[FD10] " << m_strIP << " XZ01 Firmware Update Fail");
			}
		}
	}

	bool bReboot = false;
	// 업데이트 후 카메라 리부팅 (후순위에 배치)
	if (_strNXP != strNXP) {
		strUrl = "http://" + m_strIP + ":" + std::to_string(TCP_PORT) + "/xaccma.firm/NXP";
		GetFirmwareFilePath(CMD_FW_UPDATE_NXP, _strNXP, strFilePath);
		if (strFilePath.empty() == false) {
			bool bReboot = http.PostFileUpload(strUrl, strFilePath, m_strIP, CMD_FW_UPDATE_NXP);
			if (bReboot) {
				ClearCameraInfo();
				std::lock_guard<std::mutex> lock(m_mutex);
				m_bConnected = false;
				m_state = STOPED;
				m_uiStatusNGCmd = 0;
			}
			else {
				SetCommandResult(CMD_FW_UPDATE_NXP, false);
				FD_ErrorL(FDPrinter << "[FD10] " << m_strIP << " NXP Firmware Update Fail");
			}
		}
	}
#if 0	// 펌웨어 업데이트 후 GetCameraInfo 명령 API 거부, Reconnect 방식으로 다시 얻어오는 방법 가이드
	// 리부팅하지 않을경우 카메라 정보 갱신(펌웨어 버전 정보 갱신)
	if (bReboot == false) {
		// 펌웨어 업데이트 후 여유시간 부여
		std::this_thread::sleep_for(std::chrono::milliseconds(3000));
		std::map<std::string, std::string> dummy;
		SendCommand(CMD_GET_CAMERA_INFO, dummy);
	}
#endif

	return true;
}

void FD10c::GetCommandResult(CmdResultMap& mpCmdResult)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	mpCmdResult = m_mpSetCmdResult;
}

int FD10c::GetGop()
{
	std::string strVideoGopMain = "Unknown";
	std::string strVideoFormatMain = "Unknown";

	std::unique_lock<std::mutex> lock(m_mutex);
	if (m_mpCameraInfo.find("VideoGopMain") != m_mpCameraInfo.end())
	{
		strVideoGopMain = m_mpCameraInfo["VideoGopMain"];

		if (m_mpCameraInfo.find("VideoFormatMain") != m_mpCameraInfo.end())
		{
			strVideoFormatMain = m_mpCameraInfo["VideoFormatMain"];
		}
	}
	lock.unlock();

	bool bDefault = true;
	int nGop = 0;

	if (strVideoGopMain == "Large")
	{
		if (strVideoFormatMain == "UHD-50" || strVideoFormatMain == "UHD-60" || strVideoFormatMain == "FHD-180" || strVideoFormatMain == "FHD-150")
		{
			nGop = 12;
			bDefault = false;
		}
		else
		{
			nGop = 10;
			bDefault = false;
		}
	}
	else if (strVideoGopMain == "Small")
	{
		nGop = 1;
		bDefault = false;
	}

	if (bDefault)
	{
		FD_ErrorL(FDPrinter << "[FD10] " << m_strIP << " GoP Set Default Value: " << nGop
			<< ", VideoGopMain: " << strVideoGopMain
			<< ", VideoFormatMain: " << strVideoFormatMain);
	}

	return nGop;
}

unsigned int FD10c::GetTemperature()
{
	return FD10c::s_StatusCtx.GetTemperature_NetworkSoC(m_strIP);
}

std::string FD10c::GetStatusNGCommand(bool bStarted)
{
	unsigned int status = 0;
	bool bConnected = false;

	std::unique_lock<std::mutex> lock(m_mutex);
	status = m_uiStatusNGCmd;
	bConnected = m_bConnected;
	lock.unlock();

	std::string strNGCmd;
	if ((bConnected == false) && (status & (1 << CMD_NG_UDPSTATUS))) {
		strNGCmd = "UDP Status";
	}
	else {
		int start = 0, end = 0;
		if (bStarted) {
			start = CMD_NG_START;
			end = CMD_NG_SYNC;
		}
		else {
			start = CMD_NG_STOP;
			end = CMD_NG_CLOSESESSION;
		}

		for (int i = start; i <= end; ++i) {
			if (status & (1 << i)) {
				switch (i) {
				case CMD_NG_UDPSTATUS:		strNGCmd = "UDP Status";				break;
				case CMD_NG_SYNC:			strNGCmd = "Sync";						break;
				case CMD_NG_PREPARE:		strNGCmd = CMD_SET_PREPARE_STREAMING;	break;
				case CMD_NG_START:			strNGCmd = CMD_SET_START_STREAMTIME;	break;
				case CMD_NG_STOP:			strNGCmd = CMD_SET_STOP_STREAMING;		break;
				case CMD_NG_OPENSESSION:	strNGCmd = CMD_OPEN_SESSION;			break;
				case CMD_NG_GET_CAMERAINFO:	strNGCmd = CMD_GET_CAMERA_INFO;			break;
				case CMD_NG_GET_APERTURE:	strNGCmd = CMD_GET_APERTURE;			break;
				case CMD_NG_GET_ZOOMPOS:	strNGCmd = CMD_GET_ZOOM_POS;			break;
				case CMD_NG_TURN_ON_LENS:	strNGCmd = CMD_TURN_ON_LENS;			break;
				case CMD_NG_SET_FOCUSMODE:	strNGCmd = CMD_SET_FOCUS_MODE;			break;
				case CMD_NG_CLOSESESSION:	strNGCmd = CMD_CLOSE_SESSION;			break;
				default: break;
				}
				break;
			}
		}
	}

	if (strNGCmd.empty() == false)
		strNGCmd.append(" : NG");

	return strNGCmd;
}

std::string FD10c::GetStatusNGCommandAll()
{
	unsigned int status = 0;

	std::unique_lock<std::mutex> lock(m_mutex);
	status = m_uiStatusNGCmd;
	lock.unlock();

	std::string strNGCmd = "NG Command {";
	int nNGCnt = 0;
	for (int i = 0; i < CMD_NG_MAX; ++i) {
		if (status & (1 << i)) {
			if (nNGCnt > 0)
				strNGCmd += ",";

			switch (i) {
			case CMD_NG_UDPSTATUS:		strNGCmd += "UDP Status";				break;
			case CMD_NG_SYNC:			strNGCmd += "Sync";						break;
			case CMD_NG_PREPARE:		strNGCmd += CMD_SET_PREPARE_STREAMING;	break;
			case CMD_NG_START:			strNGCmd += CMD_SET_START_STREAMTIME;	break;
			case CMD_NG_STOP:			strNGCmd += CMD_SET_STOP_STREAMING;		break;
			case CMD_NG_OPENSESSION:	strNGCmd += CMD_OPEN_SESSION;			break;
			case CMD_NG_GET_CAMERAINFO:	strNGCmd += CMD_GET_CAMERA_INFO;		break;
			case CMD_NG_GET_APERTURE:	strNGCmd += CMD_GET_APERTURE;			break;
			case CMD_NG_GET_ZOOMPOS:	strNGCmd += CMD_GET_ZOOM_POS;		break;
			case CMD_NG_CLOSESESSION:	strNGCmd += CMD_CLOSE_SESSION;			break;
			default: break;
			}
			++nNGCnt;
		}
	}
	strNGCmd += "}";

	return strNGCmd;
}

void FD10c::GetApertureList(std::vector<int>& vecAperture)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	vecAperture = m_vecAperture;
}

void FD10c::GetZoomPosRange(int& nUpper, int& nLower, int& nStep)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	nUpper = m_arrZoomPos.at(ZOOM_POS_UPPER);
	nLower = m_arrZoomPos.at(ZOOM_POS_LOWER);
	nStep = m_arrZoomPos.at(ZOOM_POS_STEP);
}

bool FD10c::PrepareStreaming()
{
	std::map<std::string, std::string> mpArg;
	mpArg["arg_status"] = "0";		// For debug
	mpArg["arg_size"] = "125";		// [50,125] Buffer clear size. Unit is MB. (default value is 125)

	return SendCommand(CMD_SET_PREPARE_STREAMING, mpArg);
}

bool FD10c::StartStreaming(const unsigned int uiRecordTime, const unsigned int uiWaitTime, const int nSyncMode)
{
	unsigned int uiTime = 0;
	if (nSyncMode != SYNC_SKIP) {
		uiTime = (uiRecordTime - m_udpSync.GetLastRecvTime()) + m_udpSync.GetSyncDelay() + (uiWaitTime * 100) + (m_uiRecordDelay * 100);
		TraceL << "[FD10] " << m_strIP << " StartStrmingTime: " << uiTime << " (delay:" << m_uiRecordDelay << "ms)";
	}

	std::map<std::string, std::string> mpArg;
	mpArg["arg_type"] = "Ticktime";
	mpArg["arg_time"] = std::to_string(uiTime);

	return SendCommand(CMD_SET_START_STREAMTIME, mpArg);
}

void FD10c::SetCommandResult(const std::string& strCmd, bool bResult, const std::string& strReason /*= ""*/)
{
	std::lock_guard<std::mutex> lock(m_mutex);

	auto itr = m_mpSetCmdResult.find(strCmd);
	if (itr != m_mpSetCmdResult.end()) {
		(itr->second).result = bResult;
		(itr->second).reason = strReason;
	}
}

bool FD10c::SendCommand(const std::string& strCmd, const std::map<std::string, std::string>& mpArg /*= std::map<std::string, std::string>()*/)
{
	if (m_strIP.empty() || strCmd.empty())
		return false;

	std::string strUrl = string_format("http://%s:%s/%s?cmd=%s", m_strIP.c_str(), std::to_string(TCP_PORT).c_str(), URL_CGI_XACTI, strCmd.c_str());

#if 1	// UI 에서 보내지 말아야함
	std::map<std::string, std::string>& mpTemp = const_cast<std::map<std::string, std::string>&>(mpArg);

	if (mpTemp.find("arg") != mpTemp.end() && mpTemp.at("arg") == "none")
	{
		mpTemp.erase("arg");
	}
	
	if (strCmd == CMD_SET_FOCUS_PEAKING)
	{
		if (mpTemp.find("arg_lv") != mpTemp.end() && mpTemp.at("arg_lv") == "Off")
		{
			mpTemp.erase("arg_color");
		}
	}
#endif

	for (const auto& v : mpArg)
	{
		strUrl += "&" + v.first + "=" + v.second;
	}

	TraceL << "[FD10] " << m_strIP << " HTTP Request : " << strCmd;

	HttpClient http(std::bind(&FD10c::OnResponse, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
	bool ret = http.SendRequest(strUrl, m_strIP, strCmd);
	if (ret == false) {
		if (strCmd == CMD_CLOSE_SESSION) {
			// 카메라가 Recording 상태에서 Stop 명령을 실패할 경우에, Reconnect(CloseSession->OpenSession) 루틴에서 레코딩 상태를 강제로 해제함 (2021.06.16)
			std::lock_guard<std::mutex> lock(m_mutex);
			m_bConnected = false;
			m_state = STOPED;
		}
		else if (strCmd == CMD_REBOOT) {
			SetCommandResult(CMD_REBOOT, false, "HTTP Fail");
		}

		SetStatusNG_FD10Command(strCmd);
	}
	return ret;
}

void FD10c::OnResponse(const std::string& strCmd, const int nResponseCode, const std::string& strContents)
{
	if (nResponseCode == 200) {	// 200 : HTTP OK
		std::string strResult;
		std::string strOpName;

		CMarkup xml;
		if (xml.SetDoc(strContents)) {
			if (xml.FindElem("ccma") && xml.IntoElem()) {
				if (xml.FindElem("output") && xml.IntoElem()) {
					if (xml.FindElem("result")) {
						strResult = xml.GetData();
					}
					if (xml.FindElem("operation")) {
						strOpName = xml.GetAttrib("name");
					}
				}
			}
		}

		if (strResult == RESULT_OK) {
			DebugL << "[FD10] " << m_strIP << " HTTP Response OK - Cmd:" << strCmd << ", Response Code:" << nResponseCode;
			//DebugL << strContents;
			SetCommandResult(strOpName, true);
			// 모든 명령에 OK 응답은 OpenSession NG 상태를 리셋함
			SetStatusNGCommand(CMD_NG_OPENSESSION, false);

			if (strOpName == CMD_OPEN_SESSION) {
				FD10c::s_StatusCtx.AddInstance(m_strIP);
				std::lock_guard<std::mutex> lock(m_mutex);
				m_bConnected = true;
				m_state = STOPED;
				m_uiStatusNGCmd = 0;
			}
			else if (strOpName == CMD_CLOSE_SESSION) {
				FD10c::s_StatusCtx.DeleteInstance(m_strIP);
				ClearCameraInfo();
				std::lock_guard<std::mutex> lock(m_mutex);
				m_bConnected = false;
				m_state = STOPED;
				m_uiStatusNGCmd = 0;
			}
			else if (strOpName == CMD_SET_PREPARE_STREAMING) {
				SetStatusNGCommand(CMD_NG_PREPARE, false);
				std::lock_guard<std::mutex> lock(m_mutex);
				m_state = PREPARED;
			}
			else if (strOpName == CMD_SET_START_STREAMTIME) {
				SetStatusNGCommand(CMD_NG_START, false);
				std::lock_guard<std::mutex> lock(m_mutex);
				m_state = STARTED;
			}
			else if (strOpName == CMD_SET_STOP_STREAMING) {
				SetStatusNGCommand(CMD_NG_STOP, false);
				std::lock_guard<std::mutex> lock(m_mutex);
				m_state = STOPED;
			}
			else if (strOpName == CMD_GET_CAMERA_INFO) {
				SetStatusNGCommand(CMD_NG_GET_CAMERAINFO, false);
				if (xml.IntoElem() && xml.FindElem("argument") && xml.IntoElem()) {
					std::map<std::string, std::string> mpCameraInfo;
					while (xml.FindElem("value")) {
						mpCameraInfo[xml.GetAttrib("name")] = xml.GetData();
					}

					std::lock_guard<std::mutex> lock(m_mutex);
					m_mpCameraInfo = mpCameraInfo;
				}
			}
			else if (strOpName == CMD_SET_APERTURE) {
				SetCameraProperty(xml, CAMERA_INFO_APERTURE);
			}
			else if (strOpName == CMD_GET_APERTURE) {
				SetStatusNGCommand(CMD_NG_GET_APERTURE, false);
				if (xml.IntoElem()) {
					std::string strAperture;
					std::vector<int> vecAperture;

					while (xml.FindElem("argument")) {
						if (xml.GetAttrib("name").compare("Argument") == 0 && xml.IntoElem()) {
							if (xml.FindElem("value")) {
								strAperture = xml.GetData();
							}
							xml.OutOfElem();
						}
						else if (xml.GetAttrib("name").compare("ArgumentList") == 0 && xml.IntoElem()) {
							while (xml.FindElem("value")) {
								vecAperture.push_back(std::stoi(xml.GetData()));
							}
							xml.OutOfElem();
						}
					}

					std::lock_guard<std::mutex> lock(m_mutex);
					m_mpCameraInfo[CAMERA_INFO_APERTURE] = strAperture;
					m_vecAperture = vecAperture;
				}
			}
			else if (strOpName == CMD_SET_ZOOM_POS) {
				SetCameraProperty(xml, CAMERA_INFO_ZOOMPOSSITION);
				SendCommand(CMD_GET_APERTURE);
			}
			else if (strOpName == CMD_GET_ZOOM_POS) {
				SetStatusNGCommand(CMD_NG_GET_ZOOMPOS, false);
				if (xml.IntoElem()) {
					std::string strZoomPos;
					std::array<int, ZOOM_POS_SIZE> arrZoomPos = { 0, };

					while (xml.FindElem("argument")) {
						if (xml.GetAttrib("name").compare("Argument") == 0 && xml.IntoElem()) {
							if (xml.FindElem("value")) {
								strZoomPos = xml.GetData();
							}
							xml.OutOfElem();
						}
						else if (xml.GetAttrib("name").compare("ArgumentRange") == 0 && xml.IntoElem()) {
							if (xml.FindElem("upper"))		arrZoomPos[ZOOM_POS_UPPER] = std::stoi(xml.GetData());
							if (xml.FindElem("lower"))		arrZoomPos[ZOOM_POS_LOWER] = std::stoi(xml.GetData());
							if (xml.FindElem("step"))		arrZoomPos[ZOOM_POS_STEP] = std::stoi(xml.GetData());
							xml.OutOfElem();
						}
					}

					std::lock_guard<std::mutex> lock(m_mutex);
					m_mpCameraInfo[CAMERA_INFO_ZOOMPOSSITION] = strZoomPos;
					m_arrZoomPos = arrZoomPos;
				}
			}
			else if (strOpName == CMD_SET_VIDEO_FORMAT) {
				if (xml.IntoElem()) {
					std::string strFormat;
					std::string strGop;
					std::string strTarget;

					while (xml.FindElem("argument")) {
						if (xml.GetAttrib("name").compare("arg_format") == 0 && xml.IntoElem()) {
							if (xml.FindElem("value")) {
								strFormat = xml.GetData();
							}
							xml.OutOfElem();
						}
						else if (xml.GetAttrib("name").compare("arg_gop") == 0 && xml.IntoElem()) {
							if (xml.FindElem("value")) {
								strGop = xml.GetData();
							}
							xml.OutOfElem();
						}
						else if (xml.GetAttrib("name").compare("arg_target") == 0 && xml.IntoElem()) {
							if (xml.FindElem("value"))
							{
								strTarget = xml.GetData();
							}
							xml.OutOfElem();
						}
					}

					if (strTarget == "Main") {
						std::lock_guard<std::mutex> lock(m_mutex);
						m_mpCameraInfo[CAMERA_INFO_VIDEOFORMAT_MAIN] = strFormat;
						m_mpCameraInfo[CAMERA_INFO_VIDEOGOP_MAIN] = strGop;
					}
					else if (strTarget == "Sub") {
						std::lock_guard<std::mutex> lock(m_mutex);
						m_mpCameraInfo[CAMERA_INFO_VIDEOFORMAT_SUB] = strFormat;
						m_mpCameraInfo[CAMERA_INFO_VIDEOGOP_SUB] = strGop;
					}
					else {
						FD_ErrorL(FDPrinter << "[FD10] " << m_strIP << " Unexpected Result - Cmd:" << strCmd << ", arg_target:" << strTarget);
					}
				}
			}
			else if (strOpName == CMD_GET_VIDEO_FORMAT) {
				if (xml.IntoElem()) {
					std::string strStreamType;
					std::string strMainStream;
					std::string strSubStream;
					std::string strGop;

					while (xml.FindElem("argument")) {
						if (xml.GetAttrib("name").compare("Argument") == 0 && xml.IntoElem()) {
							while (xml.FindElem("value")) {
								const std::string strName = xml.GetAttrib("name");
								if (strName == "StreamType")			strStreamType = xml.GetData();
								else if (strName == "MainStream")		strMainStream = xml.GetData();
								else if (strName == "SubStream")		strSubStream = xml.GetData();
								else if (strName == "GOP")				strGop = xml.GetData();
							}

							if (strStreamType == "Main") {
								std::lock_guard<std::mutex> lock(m_mutex);
								m_mpCameraInfo[CAMERA_INFO_VIDEOFORMAT_MAIN] = strMainStream;
								m_mpCameraInfo[CAMERA_INFO_VIDEOGOP_MAIN] = strGop;
							}
							else if (strStreamType == "Sub") {
								std::lock_guard<std::mutex> lock(m_mutex);
								m_mpCameraInfo[CAMERA_INFO_VIDEOFORMAT_SUB] = strSubStream;
								m_mpCameraInfo[CAMERA_INFO_VIDEOGOP_SUB] = strGop;
							}
							else {
								FD_ErrorL(FDPrinter << "[FD10] " << m_strIP << " Unexpected Result - Cmd:" << strCmd << ", StreamType:" << strStreamType);
							}

							xml.OutOfElem();
						}
#if 0	// Not Use
						else if (xml.GetAttrib("name").compare("ArgumentList") == 0 && xml.IntoElem()) {
							// list
							while (xml.FindElem("value")) {
								const std::string strName = xml.GetAttrib("name");
								if (strName == "MainList") {
									xml.GetData();
								}
								else if (strName == "SubList") {
									xml.GetData();
								}
								else if (strName == "TargetStreamList") {
									xml.GetData();
								}
								else if (strName == "GOPList") {
									xml.GetData();
								}
							}
							xml.OutOfElem();
						}
#endif
					}
				}
			}
			else if (strOpName == CMD_SET_VIDEO_BITRATE) {
				if (xml.IntoElem()) {
					std::string strBitrate;
					std::string strTarget;

					while (xml.FindElem("argument")) {
						if (xml.GetAttrib("name").compare("arg_bitrate") == 0 && xml.IntoElem()) {
							if (xml.FindElem("value")) {
								strBitrate = xml.GetData();
							}
							xml.OutOfElem();
						}
						else if (xml.GetAttrib("name").compare("arg_target") == 0 && xml.IntoElem()) {
							if (xml.FindElem("value")) {
								strTarget = xml.GetData();
							}
							xml.OutOfElem();
						}
					}

					if (strTarget == "Main") {
						std::lock_guard<std::mutex> lock(m_mutex);
						m_mpCameraInfo[CAMERA_INFO_VIDEOBITRATE_MAIN] = strBitrate;
					}
					else if (strTarget == "Sub") {
						std::lock_guard<std::mutex> lock(m_mutex);
						m_mpCameraInfo[CAMERA_INFO_VIDEOBITRATE_SUB] = strBitrate;
					}
					else {
						FD_ErrorL(FDPrinter << "[FD10] " << m_strIP << " Unexpected Result - Cmd:" << strCmd << ", arg_target:" << strTarget);
					}
				}
			}
			else if (strOpName == CMD_SET_ISO) {
				SetCameraProperty(xml, CAMERA_INFO_ISO);
			}
			else if (strOpName == CMD_SET_SHUTTER_SPEED) {
				SetCameraProperty(xml, CAMERA_INFO_SHUTTERSPEED);
			}
			else if (strOpName == CMD_SET_WHITE_BALANCE_MODE) {
				SetCameraProperty(xml, CAMERA_INFO_WHITEBALANCE);
			}
			else if (strOpName == CMD_SET_COLOR_TEMPERATURE) {
				SetCameraProperty(xml, CAMERA_INFO_COLORTEMPERATURE);
			}
			else if (strOpName == CMD_SET_FOCUS_MODE) {
				SetStatusNGCommand(CMD_NG_SET_FOCUSMODE, false);
				SetCameraProperty(xml, CAMERA_INFO_FOCUSMODE);
			}
			else if (strOpName == CMD_SET_AUTO_FOCUS_POINT) {
				SetCameraProperty(xml, CAMERA_INFO_AUTO_FOCUS_POINT);
			}
			else if (strOpName == CMD_SET_AE_AREA) {
				SetCameraProperty(xml, CAMERA_INFO_AEAREA);
			}
			else if (strOpName == CMD_GET_MANUAL_FOCUS_POS || strOpName == CMD_SET_MANUAL_FOCUS_POS) {
				SetCameraProperty(xml, CAMERA_INFO_MANUFALFOCUSPOSITION);
			}
			else if (strOpName == CMD_SET_IMAGE_STABILIZATION_MODE) {
				SetCameraProperty(xml, CAMERA_INFO_IMAGESSTABILIZATIONMODE);
			}
			else if (strOpName == CMD_SET_FOCUS_PEAKING) {
				if (xml.IntoElem()) {
					std::string strLv;
					std::string strColor;

					while (xml.FindElem("argument")) {
						if (xml.GetAttrib("name").compare("arg_lv") == 0 && xml.IntoElem()) {
							if (xml.FindElem("value")) {
								strLv = xml.GetData();
							}
							xml.OutOfElem();
						}
						else if (xml.GetAttrib("name").compare("arg_color") == 0 && xml.IntoElem()) {
							if (xml.FindElem("value")) {
								strColor = xml.GetData();
							}
							xml.OutOfElem();
						}
					}

					std::lock_guard<std::mutex> lock(m_mutex);
					m_mpCameraInfo[CAMERA_INFO_FOCUSPEAKINGLV] = strLv;
					m_mpCameraInfo[CAMERA_INFO_FOCUSPEAKINGCOLOR] = strColor;
				}
			}
			else if (strOpName == CMD_SET_LED_CONTROL_MODE) {
				SetCameraProperty(xml, CAMERA_INFO_LEDCONTROLMODE);
			}
			else if (strOpName == CMD_SET_VIDEO_LUMINANCE_LEVEL) {
				SetCameraProperty(xml, CAMERA_INFO_LUMINANCELEVEL);
			}
			else if (strOpName == CMD_SET_EXPOSURE_MODE) {
				SetCameraProperty(xml, CAMERA_INFO_EXPOSUERMODE);
			}
			else if (strOpName == CMD_SET_EXPOSURE_COMPENSATION) {
				SetCameraProperty(xml, CAMERA_INFO_EXPOSURECOMPENSATION);
			}
			else if (strOpName == CMD_SET_SATURATION) {
				SetCameraProperty(xml, CAMERA_INFO_SATURATION);
			}
			else if (strOpName == CMD_SET_HUE) {
				SetCameraProperty(xml, CAMERA_INFO_HUE);
			}
			else if (strOpName == CMD_SET_SHARPNESS) {
				SetCameraProperty(xml, CAMERA_INFO_SHARPNESS);
			}
			else if (strOpName == CMD_SET_CONTRAST) {
				SetCameraProperty(xml, CAMERA_INFO_CONTRAST);
			}
			else if (strOpName == CMD_SET_NOISE_REDUCTION) {
				SetCameraProperty(xml, CAMERA_INFO_NOISEREDUCTION);
			}
			else if (strOpName == CMD_SET_CMOS_OFFSET_CORRECT_LEVEL) {
				SetCameraProperty(xml, CAMERA_INFO_CMOSOFFSETCORRECTLEVEL);
			}
			else if (strOpName == CMD_SET_RGB) {
				if (xml.IntoElem()) {
					std::string strR, strG, strB;

					while (xml.FindElem("argument")) {
						if (xml.GetAttrib("name").compare("arg_R") == 0 && xml.IntoElem()) {
							if (xml.FindElem("value")) {
								strR = xml.GetData();
							}
							xml.OutOfElem();
						}
						else if (xml.GetAttrib("name").compare("arg_G") == 0 && xml.IntoElem()) {
							if (xml.FindElem("value")) {
								strG = xml.GetData();
							}
							xml.OutOfElem();
						}
						else if (xml.GetAttrib("name").compare("arg_B") == 0 && xml.IntoElem()) {
							if (xml.FindElem("value"))
							{
								strB = xml.GetData();
							}
							xml.OutOfElem();
						}
					}

					std::lock_guard<std::mutex> lock(m_mutex);
					m_mpCameraInfo[CAMERA_INFO_RGB_R] = strR;
					m_mpCameraInfo[CAMERA_INFO_RGB_G] = strG;
					m_mpCameraInfo[CAMERA_INFO_RGB_B] = strB;
				}
			}
			else if (strOpName == CMD_ONE_SHOT_AF) {
				SendCommand(CMD_GET_MANUAL_FOCUS_POS);
			}
			else if (strOpName == CMD_TURN_ON_LENS) {
				SetStatusNGCommand(CMD_NG_TURN_ON_LENS, false);
				std::lock_guard<std::mutex> lock(m_mutex);
				m_mpCameraInfo[CAMERA_INFO_LENSPOWER] = "On";
			}
			else if (strOpName == CMD_TURN_OFF_LENS) {
				std::lock_guard<std::mutex> lock(m_mutex);
				m_mpCameraInfo[CAMERA_INFO_LENSPOWER] = "Off";
			}
		}
		else { // NG
			SetStatusNG_FD10Command(strCmd);

			std::string strReason;
			if (xml.IntoElem()) {
				while (xml.FindElem("argument")) {
					if (xml.GetAttrib("name").compare("Reason") == 0 && xml.IntoElem()) {
						if (xml.FindElem("value")) {
							strReason = xml.GetData();
						}
						break;
					}
				}
			}

			if (strOpName == CMD_CLOSE_SESSION) {
				// 카메라가 Recording 상태에서 Stop 명령을 실패할 경우에, Reconnect(CloseSession->OpenSession) 루틴에서 레코딩 상태를 강제로 해제함 (2021.06.16)
				std::lock_guard<std::mutex> lock(m_mutex);
				m_bConnected = false;
				m_state = STOPED;
			}
			else {
				SetCommandResult(strOpName, false, strReason);
			}

			ErrorL << "[FD10] " << m_strIP << " NG Response - Cmd:" << strCmd << ", Reason: " << strReason;
			//ErrorL << strContents;
		}
	}
	else {
		SetStatusNG_FD10Command(strCmd);
		SetCommandResult(strCmd, false, "HTTP NOK");
		ErrorL << "[FD10] " << m_strIP << " HTTP Response NOK - Cmd:" << strCmd << ", Response Code:" << nResponseCode;
	}
}

void FD10c::SetCameraProperty(CMarkup& xml, const std::string& strProperty)
{
	if (xml.IntoElem())
	{
		if (xml.FindElem("argument") && xml.IntoElem())
		{
			if (xml.FindElem("value"))
			{
				std::lock_guard<std::mutex> lock(m_mutex);
				m_mpCameraInfo[strProperty] = xml.GetData();
			}
		}
	}
}

void FD10c::SetStatusNG_FD10Command(const std::string& strCmd)
{
	if (strCmd == CMD_OPEN_SESSION) {
		SetStatusNGCommand(CMD_NG_OPENSESSION, true);
	}
	else if (strCmd == CMD_CLOSE_SESSION) {
		SetStatusNGCommand(CMD_NG_CLOSESESSION, true);
	}
	else if (strCmd == CMD_SET_PREPARE_STREAMING) {
		SetStatusNGCommand(CMD_NG_PREPARE, true);
	}
	else if (strCmd == CMD_SET_START_STREAMTIME) {
		SetStatusNGCommand(CMD_NG_START, true);
	}
	else if (strCmd == CMD_SET_STOP_STREAMING) {
		SetStatusNGCommand(CMD_NG_STOP, true);
	}
	else if (strCmd == CMD_GET_CAMERA_INFO) {
		SetStatusNGCommand(CMD_NG_GET_CAMERAINFO, true);
	}
	else if (strCmd == CMD_GET_APERTURE) {
		SetStatusNGCommand(CMD_NG_GET_APERTURE, true);
	}
	else if (strCmd == CMD_GET_ZOOM_POS) {
		SetStatusNGCommand(CMD_NG_GET_ZOOMPOS, true);
	}
	else if (strCmd == CMD_TURN_ON_LENS) {
		SetStatusNGCommand(CMD_NG_TURN_ON_LENS, true);
	}
	else if (strCmd == CMD_SET_FOCUS_MODE) {
		SetStatusNGCommand(CMD_NG_SET_FOCUSMODE, true);
	}
}

void FD10c::GetFirmwareFilePath(const std::string& strCmd, const std::string& strVer, std::string& strPath)
{
	strPath.clear();

	fs::path path(FIRMWARE_DIR);
	if (fs::exists(path) && fs::is_directory(path)) {
		path /= strVer;
		if (fs::exists(path) && fs::is_directory(path)) {
			if (strCmd == CMD_FW_UPDATE_NXP) {
				path /= FIRMWARE_NXP_DIR;
				path /= FIRMWARE_NXP_FILE;
			}
			else if (strCmd == CMD_FW_UPDATE_XZ01) {
				path /= FIRMWARE_XZ01_DIR;
				path /= FIRMWARE_XZ01_FILE;
			}

			if (fs::exists(path) && fs::is_regular_file(path)) {
				strPath = path.u8string();
			}
		}
	}
}