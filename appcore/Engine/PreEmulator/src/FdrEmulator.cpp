#include <asio.hpp>
#include "FdrEmulator.h"
#include "FdrEmulatorAPI.h"

#include <string>
#include <fstream>
#include <iostream>




FdrEmulHandle FdrEmulOpen(FdrEmulParam* pParam) {
    FdrEmulStruct* hEmul = (FdrEmulStruct*)MALLOCZ(sizeof(FdrEmulStruct));
	if (hEmul) {
        LRSLT lRet;
        LOG_I("FdrEmulOpen Begin..listenPort(%d)", pParam->wListenPort);

		FrSysInit();
		FrSocketInitNetwork();
        
        // PreSd param
        hEmul->m_PreParam.port = pParam->port;
        hEmul->m_PreParam.pUrl = (char*)pParam->pUrl;
        hEmul->m_PreParam.sessionNum = pParam->sessionNum;
        strcpy(hEmul->m_PreParam.szIp, pParam->szIp);

        strcpy(hEmul->m_Pre.m_szVpdIp, hEmul->m_PreParam.szIp);
        hEmul->m_Pre.m_wVpdPort = hEmul->m_PreParam.port;
        hEmul->m_Pre.m_UrlInfo.pUrl = hEmul->m_PreParam.pUrl;
        
        // PostSd param
        hEmul->m_PostParam.port = pParam->wListenPort;
        hEmul->m_PostParam.pSave = pParam->pSave;
        
        strcpy(hEmul->m_Post.m_szVpdIp, hEmul->m_Pre.m_szVpdIp);
        hEmul->m_Post.m_wPort = hEmul->m_PostParam.port;
        hEmul->m_Post.m_UrlInfo.pUrl = hEmul->m_PostParam.pSave;


        
        LOG_I("FrPreEmulOpen End..");
	}

	return hEmul;
}

void FdrEmulClose(FdrEmulHandle hHandle) {
    FdrEmulStruct* hEmul = (FdrEmulStruct*)hHandle;
    LOG_I("FdrEmulClose Begin..");
    if (hEmul) {
        hEmul->m_bEnd = TRUE;
        
        //FdrEmulStop(hEmul);

        FREE(hEmul);
    }
    LOG_I("FdrEmulClose End..");
}


int FdrEmulPreStart(FdrEmulHandle hHandle, FdrEmulPreParam* param) {
    FdrEmulStruct* hEmul = (FdrEmulStruct*)hHandle;
    LRSLT lRet;

    LOG_I("FdrEmulPreStart Begin..");
    if (!hEmul)
        return -1;
    
    strcpy(hEmul->m_Pre.m_szVpdIp, param->VpdIp);
    hEmul->m_Pre.m_wVpdPort = 19720;
    hEmul->m_Pre.m_UrlInfo.pUrl = param->pUrl;
    strcpy(hEmul->m_Pre.m_szPostIp, param->PostIp);
    PreOpen(&hEmul->m_Pre);

    LOG_I("FdrEmulPreStart End..");
    
    return 1;
}

int FdrEmulPreStop(FdrEmulHandle hHandle) {
    FdrEmulStruct* hEmul = (FdrEmulStruct*)hHandle;
    LOG_I("FdrEmulPreStop Begin..");

    if (hEmul) {
        PreClose(&hEmul->m_Pre);
    }

    LOG_I("FdrEmulPreStop End..");

    return 1;
}


int PreOpen(PreSession* hPre) {
    LRSLT lRet;
    DWORD32 dwStart = 0;
    hPre->m_bEnd = FALSE;

    LOG_I("PreOpen() Begin..");

    lRet = McSocketTcpOpen(&hPre->m_hVpdSocket, hPre->m_szVpdIp, hPre->m_wVpdPort, FALSE, 0);
    if (FRFAILED(lRet)) {
        LOG_E("PreOpen() McSocketTcpOpen error (%x)", lRet);
        return -1;
    }

    // reader
    lRet = FrReaderOpen(&hPre->m_hReader, &hPre->m_UrlInfo);
    if (FRFAILED(lRet)) {
        return -1;
    }

    lRet = FrReaderGetInfo(hPre->m_hReader, &hPre->m_SrcInfo);
    if (FRFAILED(lRet)) {
        return -1;
    }

    hPre->m_hPreTask = McCreateTask((char*)"PreTask", (PTASK_START_ROUTINE)TaskPreSession, hPre, THREAD_PRIORITY_HIGHEST, NULL, 0);

    lRet = FrReaderStart(hPre->m_hReader, &dwStart);
    if (FRFAILED(lRet)) {
        return -1;
    }

    LOG_I("PreOpen() End..");

    return 1;
}

int PreClose(PreSession* hPre) {
    LOG_I("PreClose() Begin..");
    if (hPre) {
        hPre->m_bEnd = TRUE;
        while (hPre->m_hPreTask != INVALID_TASK)
            FrSleep(10);
        
        if (hPre->m_hReader != nullptr) {
            FrReaderClose(hPre->m_hReader);
            hPre->m_hReader = nullptr;
        }

        McSocketClose(hPre->m_hVpdSocket);
    }
    LOG_I("PreClose() End..");
    return 1;
}

//AsyncClient _client;

int FdrEmulConnectPost(FdrEmulHandle hHandle, FdrEmulPcdParam* param) {
    FdrEmulStruct* hEmul = (FdrEmulStruct*)hHandle;
    LRSLT lRet;
    
    //hEmul->m_Pcd.Connect(param->PostSdIp, param->postPort);
    //hEmul->_client.Connect(nullptr, param->PostSdIp, param->postPort);

    //AsyncClient _client;
    //_client.Connect(nullptr, param->PostSdIp, param->postPort);

    lRet = McSocketTcpOpen(&hEmul->m_hPostSock, param->PostSdIp, param->postPort, FALSE, 0);
    if (FRFAILED(lRet)) {
        LOG_E("PostOpen() McSocketTcpOpen error (%x)", lRet);
        return -1;
    }


    return 1;
}

int FdrEmulSendPost(FdrEmulHandle hHandle, const char* payload, int32_t size) {
    FdrEmulStruct* hEmul = (FdrEmulStruct*)hHandle;

    int ret = -1;
    union unSpdMsgHeader uniSpdMsgHeader;
    uniSpdMsgHeader.stHeader.separator = 0;
    uniSpdMsgHeader.stHeader.z = (int)size;
    int nTotalSize = uniSpdMsgHeader.stHeader.z + VSPD_MESSAGE_HEADER_SIZE;
    char* sendMsg = new char[nTotalSize];

    memcpy(sendMsg, &uniSpdMsgHeader, sizeof(uniSpdMsgHeader.y));
    memcpy(sendMsg + VSPD_MESSAGE_HEADER_SIZE, payload, uniSpdMsgHeader.stHeader.z);

    McSocketSend(hEmul->m_hPostSock, (char*)sendMsg, nTotalSize);
    delete[] sendMsg;

    //SPd_DEBUG("Send Message : {}", message);
    //ret = m_Server.Send(sendMsg, nTotalSize); delete[] sendMsg; if (ret != 0) return false;

    //return _client.Send(payload, size);
    return 1;
}

int FdrEmulConnectVpd(FdrEmulHandle hHandle, FdrEmulPcdParam* param) {
    FdrEmulStruct* hEmul = (FdrEmulStruct*)hHandle;
    LRSLT lRet;

    lRet = McSocketTcpOpen(&hEmul->m_hVpdSock, param->VpdIp, param->vpdPort, FALSE, 0);
    if (FRFAILED(lRet)) {
        LOG_E("PostOpen() McSocketTcpOpen error (%x)", lRet);
        return -1;
    }


    return 1;
}

int FdrEmulSendVpd(FdrEmulHandle hHandle, const char* payload, int32_t size) {
    FdrEmulStruct* hEmul = (FdrEmulStruct*)hHandle;

    int ret = -1;
    union unSpdMsgHeader uniSpdMsgHeader;
    uniSpdMsgHeader.stHeader.separator = 0;
    uniSpdMsgHeader.stHeader.z = (int)size;
    int nTotalSize = uniSpdMsgHeader.stHeader.z + VSPD_MESSAGE_HEADER_SIZE;
    char* sendMsg = new char[nTotalSize];

    memcpy(sendMsg, &uniSpdMsgHeader, sizeof(uniSpdMsgHeader.y));
    memcpy(sendMsg + VSPD_MESSAGE_HEADER_SIZE, payload, uniSpdMsgHeader.stHeader.z);

    McSocketSend(hEmul->m_hVpdSock, (char*)sendMsg, nTotalSize);
    delete[] sendMsg;

    //SPd_DEBUG("Send Message : {}", message);
    //ret = m_Server.Send(sendMsg, nTotalSize); delete[] sendMsg; if (ret != 0) return false;

    //return _client.Send(payload, size);
    return 1;
}

#if 0
int PostOpen(PostSession* hPost) {
    LRSLT lRet;
    DWORD32 dwStart = 0;
    hPost->m_bEnd = FALSE;

    LOG_I("PostOpen Begin..");

    lRet = McSocketTcpOpen(&hPost->m_hSocket, hPost->m_szVpdIp, hPost->m_wPort, FALSE, 0);
    if (FRFAILED(lRet)) {
        LOG_E("PostOpen() McSocketTcpOpen error (%x)", lRet);
        return -1;
    }

    // reader
    
    hPost->m_UrlInfo.URLType = _MP4_FILE;
    lRet = FrWriterOpen(&hPost->m_hWriter, &hPost->m_UrlInfo);
    if (FRFAILED(lRet)) {
        return -1;
    }

    memset(&hPost->m_EncInfo, 0, sizeof(FrMediaInfo));
    FrVideoInfo* pEncVideoInfo = &hPost->m_EncInfo.FrVideo;
    pEncVideoInfo->dwWidth = 1920;
    pEncVideoInfo->dwHeight = 1080;
    pEncVideoInfo->dwBitRate = 20 * 1024 * 1024;
    lRet = FrWriterSetInfo(hPost->m_hWriter, &hPost->m_EncInfo, 0);
    if (FRFAILED(lRet)) {
        return -1;
    }

    hPost->m_hPostTask = McCreateTask((char*)"PostTask", (PTASK_START_ROUTINE)TaskPostSession, hPost, THREAD_PRIORITY_HIGHEST, NULL, 0);

    /*lRet = FrWriterStart(hPre->m_hReader, &dwStart);
    if (FRFAILED(lRet)) {
        return -1;
    }*/

    LOG_I("PostOpen End..");

    return 1;
}

int PostClose(PostSession* hPost) {
    if (hPost) {
        hPost->m_bEnd = TRUE;
        while (hPost->m_hPostTask != INVALID_TASK)
            FrSleep(10);

        if (hPost->m_hWriter != nullptr) {
            FrWriterUpdateInfo(hPost->m_hWriter);
            FrWriterClose(hPost->m_hWriter);
            hPost->m_hWriter = nullptr;
        }

        McSocketClose(hPost->m_hSocket);
    }

    return 1;
}



#endif
