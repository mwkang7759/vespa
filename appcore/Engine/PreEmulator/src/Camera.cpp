#include "Camera.h"


Camera::Camera(const std::string& strIP, const std::string& strType, const unsigned short syncPort)
	: m_udpSync(strIP, syncPort)
{
	m_bConnected = false;
	m_state = STOPED;
	m_strIP = strIP;
	m_strType = strType;
	m_uiRecordDelay = 0;
	m_uiStatusNGCmd = 0;
}

Camera::~Camera()
{
	m_udpSync.StopSync();
}

const std::string& Camera::GetCameraType() const
{
	return m_strType;
}

void Camera::GetCameraInfo(std::map<std::string, std::string>& mpCameraInfo, bool bForceUpdate)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	mpCameraInfo = m_mpCameraInfo;
}

void Camera::SetConnected(bool bConnected)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	m_bConnected = bConnected;
}

bool Camera::IsConnected()
{
	std::lock_guard<std::mutex> lock(m_mutex);
	return m_bConnected;
}

void Camera::SetState(const CameraState& state)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	m_state = state;
}

Camera::CameraState	Camera::GetState()
{
	std::lock_guard<std::mutex> lock(m_mutex);
	return m_state;
}

bool Camera::RunSync(const unsigned int uiSyncTime)
{
	int nShift = GetSyncMask();
	bool bSynced = m_udpSync.RunSync(uiSyncTime);
	if (bSynced) {
		SetState(SYNCED);
		SetStatusNGCommand(nShift, false);
	} else {
		SetStatusNGCommand(nShift, true);
	}

	return bSynced;
}

void Camera::GetSyncResult(SyncResult& result) const
{
	return m_udpSync.GetSyncResult(result);
}

void Camera::SetStatusNGCommand(int shift, bool b)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	if (b) {
		m_uiStatusNGCmd |= (1 << shift);
	}
	else {
		m_uiStatusNGCmd &= ~(1 << shift);
	}
}

unsigned int Camera::GetStatusNGCommandValue()
{
	std::lock_guard<std::mutex> lock(m_mutex);
	return m_uiStatusNGCmd;
}

bool Camera::Prepare() 
{ 
	std::lock_guard<std::mutex> lock(m_mutex);
	if (m_state == SYNCED) {
		m_state = PREPARED;
	}
	return true; 
}

bool Camera::Start(const unsigned int uiRecordTime, const unsigned int uiWaitTime, const int nSyncMode)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	if (m_state == PREPARED) {
		m_state = STARTED;
	}
	return true;
}

bool Camera::Stop()
{
	SetState(STOPED);
	return true;
}

void Camera::ClearCameraInfo()
{
	std::lock_guard<std::mutex> lock(m_mutex);
	m_mpCameraInfo.clear();
}