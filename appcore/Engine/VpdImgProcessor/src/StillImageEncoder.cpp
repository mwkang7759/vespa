#include "StillImageEncoder.h"
#include "ImageProcessorUtil.h"

StillImageEncoder::StillImageEncoder()
{

}

StillImageEncoder::~StillImageEncoder()
{

}

int StillImageEncoder::StillImageSaveYUV420toBGR(
	unsigned char* gszYUVFrame, int nInputWidth, int nInputHeight, int nPitch,
	int nOutputWidth, int nOutputHeight, string strOutputFileName)
{
	vector<cuda::GpuMat> vecGpuImage, vecGpuOutput(3);
	ImageProcessorUtil imageProcessorUtil;
	imageProcessorUtil.CudaToGpuMatYUV420(
		gszYUVFrame, nInputWidth, nInputHeight, nPitch,
		vecGpuImage
	);

	vecGpuImage[0].copyTo(vecGpuOutput[0]);
	vecGpuImage[1].copyTo(vecGpuOutput[1]);
	vecGpuImage[2].copyTo(vecGpuOutput[2]);

	cuda::resize(vecGpuOutput[1], vecGpuOutput[1], cv::Size(vecGpuOutput[1].cols * 2, vecGpuOutput[1].rows * 2), INTER_CUBIC);
	cuda::resize(vecGpuOutput[2], vecGpuOutput[2], cv::Size(vecGpuOutput[2].cols * 2, vecGpuOutput[2].rows * 2), INTER_CUBIC);

	cuda::GpuMat gResult;
	cuda::merge(vecGpuOutput, gResult);
	cuda::resize(gResult, gResult, cv::Size(nOutputWidth, nOutputHeight));
	cuda::cvtColor(gResult, gResult, COLOR_YUV2RGB);

	Mat mResult;
	gResult.download(mResult);
	
	try 
	{
		imwrite(strOutputFileName, mResult);
	}
	catch (cv::Exception& e)
	{
		string err_msg = e.what();
		strErrorMessage = "StillImageEncoder::StillImageSaveYUV420toBGR : ";
		strErrorMessage.append(err_msg);
		std::cout << "StillImageEncoder::StillImageSaveYUV420toBGR : " << err_msg << std::endl;
	}
	return 1;
}