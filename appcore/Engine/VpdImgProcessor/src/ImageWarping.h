#pragma once

#include "define_IP.h"

//#include "ListManager.h"
//using namespace listmanager::impl_vec;

#include "ListManagerAsMap.h"
using namespace listmanager::impl_map;
//using namespace listmanager::custom_key;

class ImageWarping : public ListManager<int, StYUV420AdjustData>
{
public:
	ImageWarping();
	~ImageWarping();

public:
	IP_RESULT AddAdjustData(
		int nChannel, string strDscID,
		int nMoveX, int nMoveY,	int nRotateX, int nRotateY,	
		float fAngle, float fScale,

		int nMarginX, int nMarginY,
		int nMarginWidth, int nMarginHeight,

		bool bFlip, 
		int nWidth, int nHeight);

	IP_RESULT RigidTransformForYUV420(int nChannel,
		unsigned char* gszYUVFrame, int nWidth, int nHeight, int nPitch);

	IP_RESULT ErrorReturn(LM_RESULT lmErrCode);

private:
	IP_RESULT rigidTransform(
		const cuda::GpuMat& gInputImage, cuda::GpuMat& gOutputImage,
		int nOutputWidth, int nOutputHeight,
		StAdjustData stAdjustData);

	IP_RESULT rigidTransform(
		const Mat& mInputImage, Mat& mOutputImage,
		int nOutputWidth, int nOutputHeight,
		StAdjustData stAdjustData);

	IP_RESULT getAffineMatrix(int nInputWidth, int nInputHeight,
		StAdjustData stAdjustData, Mat& mWarpingMat);

private:
	std::mutex m_adjustMutex;
};

