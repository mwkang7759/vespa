#pragma once

#include "define_IP.h"

class ImageProcessorUtil
{
public:
	ImageProcessorUtil();
	~ImageProcessorUtil();

	IP_RESULT CudaToGpuMatYUV420(
		unsigned char* dpYUVFrame, int nWidth, int nHeight, int nPitch,
		vector<cuda::GpuMat> &vecGpuMatYUV420);
};

