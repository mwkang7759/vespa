#pragma once
#include "define_IP.h"

#include <iostream>

class StillImageEncoder
{
public:
	StillImageEncoder();
	~StillImageEncoder();

private:
	string strErrorMessage;

public:
	int StillImageSaveYUV420toBGR(
		unsigned char* gszYUVFrame, int nInputWidth, int nInputHeight, int nPitch,
		int nOutputWidth, int nOutputHeight, string strOutputFileName);
};

