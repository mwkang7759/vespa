#include "ImageWarping.h"
#include "ImageProcessorUtil.h"
#include "MatrixCalculator.h"

ImageWarping::ImageWarping()
{

}

ImageWarping::~ImageWarping()
{

}

IP_RESULT ImageWarping::AddAdjustData(
	int nChannel, string strDscID,
	int nMoveX, int nMoveY, int nRotateX, int nRotateY,
	float fAngle, float fScale,

	int nMarginX, int nMarginY,
	int nMarginWidth,  int nMarginHeight,

	bool bFlip,
	int nWidth, int nHeight)
{
	StYUV420AdjustData stAdjustData;

	stAdjustData.Full.nMoveX = nMoveX;
	stAdjustData.Full.nMoveY = nMoveY;
	stAdjustData.Full.nRotateX = nRotateX;
	stAdjustData.Full.nRotateY = nRotateY;
	stAdjustData.Full.fAngle = fAngle;
	stAdjustData.Full.fScale = fScale;

	stAdjustData.Full.nMarginX = nMarginX;
	stAdjustData.Full.nMarginY = nMarginY;
	stAdjustData.Full.nMarginWidth = nMarginWidth;
	stAdjustData.Full.nMarginHeight = nMarginHeight;
	
	stAdjustData.Full.bFlip = bFlip;
	stAdjustData.Full.nWidth = nWidth;
	stAdjustData.Full.nHeight = nHeight;

	stAdjustData.Half.nMoveX = nMoveX >> 1;
	stAdjustData.Half.nMoveY = nMoveY >> 1;
	stAdjustData.Half.nRotateX = nRotateX >> 1;
	stAdjustData.Half.nRotateY = nRotateY >> 1;
	stAdjustData.Half.fAngle = fAngle;
	stAdjustData.Half.fScale = fScale;

	stAdjustData.Half.nMarginX = nMarginX >> 1;
	stAdjustData.Half.nMarginY = nMarginY >> 1;
	stAdjustData.Half.nMarginWidth = nMarginWidth >> 1;
	stAdjustData.Half.nMarginHeight = nMarginHeight >> 1;

	stAdjustData.Half.bFlip = bFlip;
	stAdjustData.Half.nWidth = nWidth >> 1;
	stAdjustData.Half.nHeight = nHeight >> 1;

	LM_RESULT lmRet = AddData(nChannel, stAdjustData);

	return ErrorReturn(lmRet);
}

IP_RESULT ImageWarping::RigidTransformForYUV420(int nChannel,
	unsigned char* gszYUVFrame, int nWidth, int nHeight, int nPitch)
{
	m_adjustMutex.lock();

	IP_RESULT iwRet;

	vector<cuda::GpuMat> vecGpuImage, vecGpuOutput;
	ImageProcessorUtil imageProcessorUtil;
	iwRet = imageProcessorUtil.CudaToGpuMatYUV420(gszYUVFrame, nWidth, nHeight, nPitch, vecGpuImage);	

	if (iwRet != IP_RESULT::OK)
	{
		m_adjustMutex.unlock();
		return iwRet;
	}

	vecGpuOutput = vector<cuda::GpuMat>(vecGpuImage.size());

	StYUV420AdjustData stAdjustData;
	LM_RESULT lmRet = GetDataByKey(nChannel, stAdjustData);

	if (lmRet == LM_RESULT::OK)
	{		
		rigidTransform(vecGpuImage[0], vecGpuOutput[0], vecGpuImage[0].cols, vecGpuImage[0].rows, stAdjustData.Full);
		rigidTransform(vecGpuImage[1], vecGpuOutput[1], vecGpuImage[1].cols, vecGpuImage[1].rows, stAdjustData.Half);
		rigidTransform(vecGpuImage[2], vecGpuOutput[2], vecGpuImage[2].cols, vecGpuImage[2].rows, stAdjustData.Half);
	}

	if (!vecGpuOutput[0].empty() && !vecGpuOutput[1].empty() && !vecGpuOutput[2].empty())
	{
		vecGpuOutput[0].copyTo(vecGpuImage[0]);
		vecGpuOutput[1].copyTo(vecGpuImage[1]);
		vecGpuOutput[2].copyTo(vecGpuImage[2]);
	}

	m_adjustMutex.unlock();

	return ErrorReturn(lmRet);
}

IP_RESULT ImageWarping::getAffineMatrix(int nInputWidth, int nInputHeight,
	StAdjustData stAdjustData, Mat& mWarpingMat)
{
	double dWidthRatio = static_cast<double>(nInputWidth) / static_cast<double>(stAdjustData.nWidth);
	double dHeightRatio = static_cast<double>(nInputHeight) / static_cast<double>(stAdjustData.nHeight);

	float fMoveX = static_cast<float>(stAdjustData.nMoveX * dWidthRatio);
	float fMoveY = static_cast<float>(stAdjustData.nMoveY * dHeightRatio);
	float fRotateX = static_cast<float>(stAdjustData.nRotateX * dWidthRatio);
	float fRotateY = static_cast<float>(stAdjustData.nRotateY * dHeightRatio);
	int nMarginX = static_cast<int>(stAdjustData.nMarginX * dWidthRatio);
	int nMarginY = static_cast<int>(stAdjustData.nMarginY * dHeightRatio);
	int nMarginWidth = static_cast<int>(stAdjustData.nMarginWidth * dWidthRatio);
	int nMarginHeight = static_cast<int>(stAdjustData.nMarginHeight * dHeightRatio);

	float fScale = stAdjustData.fScale;

	float fAngle = (stAdjustData.fAngle + 90.f);
	float fRad = fAngle * static_cast<float>(CV_PI) / 180.f;

	cv::Mat m, mTfm, mRot, mTrn, mScale, mFlip, mMargin, mScaleOutput;

	MatrixCalculator::GetFlipMatrix(
		nInputWidth, nInputHeight, 
		stAdjustData.bFlip, stAdjustData.bFlip, mFlip);
	MatrixCalculator::GetRotationMatrix(fRad, fRotateX, fRotateY, mRot);
	MatrixCalculator::GetScaleMarix(fScale, fScale, fRotateX, fRotateY, mScale);
	MatrixCalculator::GetTranslationMatrix(fMoveX, fMoveY, mTrn);
	MatrixCalculator::GetMarginMatrix(nInputWidth, nInputHeight, 
		nMarginX, nMarginY, nMarginWidth, nMarginHeight,
		mMargin);
	MatrixCalculator::GetScaleMarix(1.f, 1.f, mScaleOutput); //(OutputSize / InputSize)

	mTfm = /*mScaleOutput * */mMargin * mTrn * mScale * mRot * mFlip;
	MatrixCalculator::ParseAffineMatrix(mTfm, m);

	mWarpingMat = m;

	return IP_RESULT::OK;
}

IP_RESULT ImageWarping::rigidTransform(
	const cuda::GpuMat& gInputImage, cuda::GpuMat& gOutputImage,
	int nOutputWidth, int nOutputHeight,
	StAdjustData stAdjustData)
{
	if (nOutputWidth > gInputImage.cols || nOutputHeight > gInputImage.rows)
	{
		return IP_RESULT::ERROR_OUTPUT_IS_BIGGER_THAN_INPUT;
	}

	if (nOutputWidth == 0 || nOutputHeight == 0)
	{
		nOutputWidth = gInputImage.cols;
		nOutputHeight = gInputImage.rows;
	}
	if (stAdjustData.fAngle == 0.f || stAdjustData.fScale == 0.f ||
		stAdjustData.nMarginWidth <= 0 || stAdjustData.nMarginHeight <= 0)
		return IP_RESULT::ERROR_INVALID_ADJUST_DATA;

	int nInputWidth = gInputImage.cols;
	int nInputHeight = gInputImage.rows;

	Mat mWarpingMat;
	getAffineMatrix(nInputWidth, nInputHeight, stAdjustData, mWarpingMat);

	//gOutputImage = cuda::GpuMat(gInputImage.size(), gInputImage.type());
	gInputImage.copyTo(gOutputImage); //���� �� chroma, luma
	cuda::warpAffine(gInputImage, gOutputImage, mWarpingMat, gOutputImage.size(), INTER_CUBIC, BORDER_CONSTANT);

	return IP_RESULT::OK;
}

IP_RESULT ImageWarping::rigidTransform(
	const Mat& mInputImage, Mat& mOutputImage,
	int nOutputWidth, int nOutputHeight,
	StAdjustData stAdjustData)
{
	if (nOutputWidth > mInputImage.cols || nOutputHeight > mInputImage.rows)
	{
		return IP_RESULT::ERROR_OUTPUT_IS_BIGGER_THAN_INPUT;
	}

	if (nOutputWidth == 0 || nOutputHeight == 0)
	{
		nOutputWidth = mInputImage.cols;
		nOutputHeight = mInputImage.rows;
	}

	if (stAdjustData.fAngle == 0.f || stAdjustData.fScale == 0.f ||
		stAdjustData.nMarginWidth <= 0 || stAdjustData.nMarginHeight <= 0)
		return IP_RESULT::ERROR_INVALID_ADJUST_DATA;

	int nInputWidth = mOutputImage.cols;
	int nInputHeight = mOutputImage.rows;

	Mat mWarpingMat;
	getAffineMatrix(nInputWidth, nInputHeight, stAdjustData, mWarpingMat);

	//gOutputImage = cuda::GpuMat(gInputImage.size(), gInputImage.type());
	mInputImage.copyTo(mOutputImage);
	warpAffine(mInputImage, mOutputImage, mWarpingMat, mOutputImage.size(), INTER_CUBIC, BORDER_CONSTANT);

	return IP_RESULT::OK;
}

IP_RESULT ImageWarping::ErrorReturn(LM_RESULT lmErrCode)
{
	if (lmErrCode == LM_RESULT::ERROR_DATA_IS_NOT_EXIST)
		return IP_RESULT::ERROR_CAMERA_ADJUST_DATA_IS_NOT_EXIST;
	else if (lmErrCode == LM_RESULT::ERROR_DATA_ALREADY_EXIST)
		return IP_RESULT::ERROR_CAMERA_ADJUST_DATA_ALREADY_EXIST;
	else if (lmErrCode == LM_RESULT::ERROR_LIST_IS_EMPTY)
		return IP_RESULT::ERROR_CAMERA_ADJUST_DATA_IS_EMPTY;
	else if (lmErrCode == LM_RESULT::ERROR_LIST_IS_NOT_VALID)
		return IP_RESULT::ERROR_CAMERA_ADJUST_DATA_IS_NOT_VALID;
	else if (lmErrCode == LM_RESULT::OK)
		return IP_RESULT::OK;
	else
		return IP_RESULT::OK;
}