#include "MatrixCalculator.h"

/// <summary>
/// Tranlation Matrix 계산
/// </summary>
/// <param name="tx"></param>
/// <param name="ty"></param>
/// <param name="m"></param>
/// <returns></returns>
bool MatrixCalculator::GetTranslationMatrix(const float tx, const float ty, cv::Mat& m)
{
    m = cv::Mat::eye(3, 3, CV_32FC1);

    if (tx == 0 && ty == 0)
        return false;

    m.at<float>(0, 2) = (float)tx;
    m.at<float>(1, 2) = (float)ty;

    return true;
}
/// <summary>
/// Rotation Matrix 계산 (원점 중심)
/// </summary>
/// <param name="rad"></param>
/// <param name="m"></param>
/// <returns></returns>
bool MatrixCalculator::GetRotationMatrix(const float rad, cv::Mat& m)
{
    m = cv::Mat::eye(3, 3, CV_32FC1);

    if (rad == 0)
        return false;

    m.at<float>(0, 0) = (float)cos(rad);
    m.at<float>(0, 1) = (float)-sin(rad);
    m.at<float>(1, 0) = (float)sin(rad);
    m.at<float>(1, 1) = (float)cos(rad);

    return true;
}
/// <summary>
/// Rotation Matrix 계산 (특정좌표 중심)
/// </summary>
/// <param name="rad"></param>
/// <param name="cx"></param>
/// <param name="cy"></param>
/// <param name="m"></param>
/// <returns></returns>
bool MatrixCalculator::GetRotationMatrix(const float rad, const float cx, const float cy, cv::Mat& m)
{
    m = cv::Mat::eye(3, 3, CV_32FC1);

    if (rad == 0)
        return false;

    cv::Mat mTrnA, mTrnB, mRot;

    GetTranslationMatrix(-cx, -cy, mTrnA);
    GetRotationMatrix(rad, mRot);
    GetTranslationMatrix(cx, cy, mTrnB);

    m = mTrnB * mRot * mTrnA;

    return true;
}
/// <summary>
/// Scaling Matrix 계산 (원점 중심)
/// </summary>
/// <param name="scaleX"></param>
/// <param name="scaleY"></param>
/// <param name="m"></param>
/// <returns></returns>
bool MatrixCalculator::GetScaleMarix(const float scaleX, const float scaleY, cv::Mat& m)
{
    m = cv::Mat::eye(3, 3, CV_32FC1);

    if (scaleX == 0.0 || scaleY == 0.0)
        return false;

    m.at<float>(0, 0) = (float)scaleX;
    m.at<float>(1, 1) = (float)scaleY;

    return true;
}
/// <summary>
/// Scaling Matrix 계산 (특정좌표 중심)
/// </summary>
/// <param name="scaleX"></param>
/// <param name="scaleY"></param>
/// <param name="cx"></param>
/// <param name="cy"></param>
/// <param name="m"></param>
/// <returns></returns>
bool MatrixCalculator::GetScaleMarix(const float scaleX, const float scaleY, const float cx, const float cy, cv::Mat& m)
{
    m = cv::Mat::eye(3, 3, CV_32FC1);

    if (scaleX == 0.0 || scaleY == 0.0)
        return false;

    cv::Mat mTrnA, mTrnB, mScale;

    GetTranslationMatrix(-cx, -cy, mTrnA);
    GetScaleMarix(scaleX, scaleY, mScale);
    GetTranslationMatrix(cx, cy, mTrnB);

    m = mTrnB * mScale * mTrnA;

    return true;
}
/// <summary>
/// Flip Matrix 계산
/// </summary>
/// <param name="width"></param>
/// <param name="height"></param>
/// <param name="isFlipX"></param>
/// <param name="isFlipY"></param>
/// <param name="m"></param>
/// <returns></returns>
bool MatrixCalculator::GetFlipMatrix(const int width, const int height, const bool isFlipX, const bool isFlipY, cv::Mat& m)
{
    m = cv::Mat::eye(3, 3, CV_32FC1);

    if (!isFlipX && !isFlipY)
        return false;

    if (isFlipX)
    {
        m.at<float>(0, 0) = (float)-1.0F;
        m.at<float>(0, 2) = (float)width;
    }
    if (isFlipY)
    {
        m.at<float>(1, 1) = (float)-1.0F;
        m.at<float>(1, 2) = (float)height;
    }

    return true;
}
/// <summary>
/// Margin Crop을 위한 Translation/Scaling Matrix 계산
/// </summary>
/// <param name="width"></param>
/// <param name="height"></param>
/// <param name="marginX"></param>
/// <param name="marginY"></param>
/// <param name="marginW"></param>
/// <param name="marginH"></param>
/// <param name="m"></param>
/// <returns></returns>
bool MatrixCalculator::GetMarginMatrix(const int width, const int height, const int marginX, const int marginY, const int marginW, const int marginH, cv::Mat& m)
{
    m = cv::Mat::eye(3, 3, CV_32FC1);

    if (width <= 0 || height <= 0 || marginX < 0 || marginY < 0 || marginW <= 0 || marginH <= 0)
        return false;

    float fCenterX = marginX + marginW / 2.0F;
    float fCenterY = marginY + marginH / 2.0F;
    float fScaleX = (float)width / (float)marginW;
    float fScaleY = (float)height / (float)marginH;

    cv::Mat mTrnA, mTrnB, mScale;

    GetTranslationMatrix(-fCenterX, -fCenterY, mTrnA);
    GetScaleMarix(fScaleX, fScaleY, mScale);
    GetTranslationMatrix(width / 2.0F, height / 2.0F, mTrnB);

    m = mTrnB * mScale * mTrnA;

    return true;
}
/// <summary>
/// 3x3 Matrix을 Affine transform을 위한 2x3 Matrix로 변환
/// </summary>
/// <param name="m33"></param>
/// <param name="m23"></param>
/// <returns></returns>
bool MatrixCalculator::ParseAffineMatrix(const cv::Mat& m33, cv::Mat& m23)
{
    m23 = cv::Mat::eye(2, 3, CV_32FC1);

    if (m33.cols != 3 || m33.rows != 3)
        return false;

    m23 = m33(cv::Rect(0, 0, 3, 2));

    return true;
}
