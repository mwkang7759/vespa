#pragma once

#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/cudaimgproc.hpp"
#include "opencv2/cudawarping.hpp"
#include "opencv2/cudaarithm.hpp"
using namespace cv;

#include <vector>
#include <string>
using namespace std;

const int IP_RESULT_MIN_COUNT = -0x09;
enum class IP_RESULT
{	
	ERROR_INPUT_OR_OUPUT_IS_NOT_3CHANNEL = IP_RESULT_MIN_COUNT,
	ERROR_INPUT_IS_NOT_YUV420,
	ERROR_OUTPUT_IS_BIGGER_THAN_INPUT,
	ERROR_CAMERA_ADJUST_DATA_IS_NOT_EXIST,
	ERROR_CAMERA_ADJUST_DATA_ALREADY_EXIST,
	ERROR_MARGIN_IS_NOT_EXIST,
	ERROR_CAMERA_ADJUST_DATA_IS_EMPTY,
	ERROR_CAMERA_ADJUST_DATA_IS_NOT_VALID,
	ERROR_INVALID_ADJUST_DATA,

	OK,
};


struct StAdjustData
{
	StAdjustData()
	{
		fNormMoveX = 0;
		fNormMoveY = 0;
		fNormRotateX = 0;
		fNormRotateY = 0;

		nMoveX = 0;
		nMoveY = 0;
		nRotateX = 0;
		nRotateY = 0;
		fAngle = 0.f;
		fScale = 0.f;

		nMarginX = 0;
		nMarginY = 0;
		nMarginWidth = 0;
		nMarginHeight = 0;

		bFlip = false;
		nWidth = 0;
		nHeight = 0;
	}

	int nMoveX;
	int nMoveY;
	float fNormMoveX;
	float fNormMoveY;
	int nRotateX;
	int nRotateY;
	float fNormRotateX;
	float fNormRotateY;
	float fAngle;
	float fScale;

	int nMarginX;
	int nMarginY;
	int nMarginWidth;
	int nMarginHeight;

	bool bFlip;
	int nWidth;
	int nHeight;
};

struct StYUV420AdjustData
{
	StAdjustData Full;
	StAdjustData Half;
};
