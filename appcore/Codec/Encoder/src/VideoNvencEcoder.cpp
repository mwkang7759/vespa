#include "VideoEncoder.h"


LRSLT VideoNvencOpen(FrVideoEncStruct* hEncoder, FrVideoInfo* hInfo) {
	LOG_I("NvencOpen Begin..");

	hEncoder->m_pEnc = new ESMNvenc();
	hEncoder->m_EncCtx.bitrate = hEncoder->m_dwEncBuffSize;
	switch (hInfo->dwFourCC) {
	case FOURCC_H264:
	case FOURCC_AVC1:
	case FOURCC_h264:
		hEncoder->m_EncCtx.codec = ESMNvenc::VIDEO_CODEC_T::AVC;
		hEncoder->m_EncCtx.profile = ESMNvenc::AVC_PROFILE_T::HP;
		break;
	case FOURCC_H265:
	case FOURCC_HEVC:
		hEncoder->m_EncCtx.codec = ESMNvenc::VIDEO_CODEC_T::HEVC;
		hEncoder->m_EncCtx.profile = ESMNvenc::HEVC_PROFILE_T::DP;
		break;
	}
	
	switch (hInfo->eColorFormat) {
	case VideoFmtNV12:
		hEncoder->m_EncCtx.colorspace = ESMBase::COLORSPACE_T::NV12;
		break;
	case VideoFmtYV12:
		hEncoder->m_EncCtx.colorspace = ESMBase::COLORSPACE_T::YV12;
		break;
	default:
		hEncoder->m_EncCtx.colorspace = ESMBase::COLORSPACE_T::YV12;
		break;

	}
	//hEncoder->m_EncCtx.colorspace = ESMNvenc::COLORSPACE_T::YV12;
	hEncoder->m_EncCtx.deviceIndex = 0;
	//hEncoder->m_EncCtx.gop = 1;
	hEncoder->m_EncCtx.gop = hInfo->dwGopLength;
	hEncoder->m_EncCtx.fps_num = hInfo->dwFrameRate;
	hEncoder->m_EncCtx.fps_den = 1000;
	hEncoder->m_EncCtx.height = hInfo->dwHeight;
	hEncoder->m_EncCtx.width = hInfo->dwWidth;
	
	hEncoder->m_pEnc->Initialize(&hEncoder->m_EncCtx);

	INT extraSize = 0;
	BYTE* extraData;
	extraData = hEncoder->m_pEnc->GetExtradata(extraSize);

	hInfo->dwConfig = extraSize;
	memcpy(hInfo->pConfig, extraData, extraSize);

	LOG_I("NvencOpen dwConfig(%d) End..", hInfo->dwConfig);

	return FR_OK;
}

void VideoNvencClose(FrVideoEncStruct* hEncoder) {
	LOG_I("NvencClose Begin..");

	if (hEncoder) {
		hEncoder->m_pEnc->Release();
		delete hEncoder->m_pEnc;
		hEncoder->m_pEnc = nullptr;
	}
	
	LOG_I("NvencClose End..");
}

int VideoNvencEncode(FrVideoEncStruct* hEncoder, FrRawVideo* pSrc, FrMediaStream* pDst) {
	int ret;
	
	//unsigned char* encodeInput = NULL;
	//size_t encodeInputPitch = 0;
	int32_t bitstreamSize = 0;
	long long bitstreamTimestamp = 0;
	BYTE* pBitstream = hEncoder->m_pEncFrame;
	UINT dwCnt = 0;
	int frameType = 0;
	
	LOG_D("NvencEncode Src: decoded num(%d), cts(%d), pitch(%d)", pSrc->dwSrcCnt, pSrc->dwCTS, pSrc->dwPitch);

	/*for (int i = 0; i < pSrc->nDecoded; i++) {
		ret = hEncoder->m_pEnc->Encode(pSrc->ppDecoded[i], pSrc->dwPitch, pSrc->dwCTS, pBitstream, hEncoder->m_dwEncBuffSize, bitstreamSize, bitstreamTimestamp);
		if (!ret) {
			pDst->dwFrameLen[i] = bitstreamSize;
			pBitstream += bitstreamSize;
			dwCnt++;
		}
	}*/
	//typedef enum _NV_ENC_PIC_TYPE
	//{
	//	NV_ENC_PIC_TYPE_P = 0x0,     /**< Forward predicted */
	//	NV_ENC_PIC_TYPE_B = 0x01,    /**< Bi-directionally predicted picture */
	//	NV_ENC_PIC_TYPE_I = 0x02,    /**< Intra predicted picture */
	//	NV_ENC_PIC_TYPE_IDR = 0x03,    /**< IDR picture */
	//	NV_ENC_PIC_TYPE_BI = 0x04,    /**< Bi-directionally predicted with only Intra MBs */
	//	NV_ENC_PIC_TYPE_SKIPPED = 0x05,    /**< Picture is skipped */
	//	NV_ENC_PIC_TYPE_INTRA_REFRESH = 0x06,    /**< First picture in intra refresh cycle */
	//	NV_ENC_PIC_TYPE_NONREF_P = 0x07,    /**< Non reference P picture */
	//	NV_ENC_PIC_TYPE_UNKNOWN = 0xFF     /**< Picture type unknown */
	//} NV_ENC_PIC_TYPE;
	//ret = hEncoder->m_pEnc->Encode(pSrc->pY, pSrc->dwPitch, pSrc->dwCTS, pBitstream, hEncoder->m_dwEncBuffSize, bitstreamSize, bitstreamTimestamp);
	ret = hEncoder->m_pEnc->Encode(pSrc->pY, pSrc->dwPitch, pSrc->dwCTS, pBitstream, hEncoder->m_dwEncBuffSize, bitstreamSize, bitstreamTimestamp, false, frameType);
	if (!ret) {
		pDst->dwFrameLen[0] = bitstreamSize;
		pBitstream += bitstreamSize;
		dwCnt++;
	}
	switch (frameType) {
	case 2:
	case 3:
		pDst->tFrameType[0] = FRAME_I;
		break;
	default:
		pDst->tFrameType[0] = FRAME_P;
		break;
	}
	pDst->pFrame = hEncoder->m_pEncFrame;
	pDst->dwCTS = (DWORD32)bitstreamTimestamp;
	pDst->dwFrameNum = dwCnt;

	LOG_D("NvencEncode Dst: encoded num(%d), cts(%d), frame len(%d)", pDst->dwFrameNum, pDst->dwCTS, pDst->dwFrameLen[0]);

	return 1;
}

