
#include "VideoEncoder.h"
#include "EncoderAPI.h"

#include <chrono>


LRSLT FrVideoEncOpen(void** phEncoder, FrVideoInfo* hInfo) {
	FrVideoEncStruct* hEncoder;
	LRSLT lRet = FR_FAIL;

	LOG_I("VideoEncOpen - w(%d), h(%d), bitrate(%d) Begin..", hInfo->dwWidth, hInfo->dwHeight, hInfo->dwBitRate);

	hEncoder = (FrVideoEncStruct*)MALLOCZ(sizeof(FrVideoEncStruct));
	if (hEncoder) {
		hEncoder->m_dwFourCC = hInfo->dwFourCC;
		hEncoder->m_dwWidth = hInfo->dwWidth;
		hEncoder->m_dwHeight = hInfo->dwHeight;
		hEncoder->m_eCodecType = hInfo->eCodecType;
		
		// Allocates bitstream buffers
		hEncoder->m_dwEncBuffSize = hInfo->dwBitRate;
		hEncoder->m_pEncFrame = (BYTE *)MALLOC(hEncoder->m_dwEncBuffSize);

		switch(hEncoder->m_dwFourCC) {
		case FOURCC_AVC1:
		case FOURCC_H264:
		case FOURCC_h264:
			if (hEncoder->m_eCodecType == CODEC_HW) {
				lRet = VideoNvencOpen(hEncoder, hInfo);
				if (FRFAILED(lRet)) {
					FrVideoEncClose(hEncoder);
					return lRet;
				}
			}
			else {
				hEncoder->m_pvFFMpeg = FFVideoEncOpen((UINT32)hEncoder->m_dwFourCC, hInfo);
				if (!hEncoder->m_pvFFMpeg) {
					FrVideoEncClose(hEncoder);
					return COMMON_ERR_VIDEOENC;
				}
				lRet = FR_OK;
			}
			
			LOG_I("VideoEncOpen H264 %d %d", hInfo->dwWidth, hInfo->dwHeight);
			break;
		case FOURCC_H265:
		case FOURCC_HEVC:
			if (hEncoder->m_eCodecType == CODEC_HW) {
				lRet = VideoNvencOpen(hEncoder, hInfo);
				if (FRFAILED(lRet)) {
					FrVideoEncClose(hEncoder);
					return lRet;
				}
			}
			break;
		default:
			break;
		} // end switch

		if (FRFAILED(lRet))	{
			LOG_E("VideoEncOpen() Don't select video encoder.. (FourCC : %d, CodecType : %d)", hEncoder->m_dwFourCC, hEncoder->m_eCodecType);
			FrVideoEncClose(hEncoder);
			return COMMON_ERR_VIDEOENC;
		}

		// for debugging..
		//QueryPerformanceFrequency(&hEncoder->m_qFreq);

		*phEncoder = hEncoder;

#ifdef	VIDEO_DUMP
		hEncoder->m_hFile = FILEDUMPOPEN("c:\\video_enc", "dmp", (DWORD32)hEncoder);
#endif
#ifdef VIDEO_YUV_DUMP
		hEncoder->m_hFileYUV = FILEDUMPOPEN("c:\\video_enc", "yuv", hEncoder);
#endif
	}
	else
		return COMMON_ERR_MEM;

	LOG_I("VideoEncOpen End..");

	return FR_OK;
}


void FrVideoEncClose(void* hEnc) {
	FrVideoEncStruct	*hEncoder = (FrVideoEncStruct*)hEnc;

	LOG_I("VideoEncClose Begin (0x%x)", hEncoder);

	if (hEncoder) {
		switch (hEncoder->m_dwFourCC) {
		case FOURCC_AVC1:
		case FOURCC_H264:
		case FOURCC_h264:
			if (hEncoder->m_eCodecType == CODEC_HW) {
				VideoNvencClose(hEncoder);
				/*if(hEncoder->m_iCodecSubType == H264_CUDA)
					cudaEncClose(hEncoder->m_pCuda);*/
			}
			else {
				if (hEncoder->m_pvFFMpeg) {
					FFVideoEncClose(hEncoder->m_pvFFMpeg);
					hEncoder->m_pvFFMpeg = NULL;
				}
			}
			break;
		case FOURCC_H265:
		case FOURCC_HEVC:
			if (hEncoder->m_eCodecType == CODEC_HW) {
				VideoNvencClose(hEncoder);
				/*if(hEncoder->m_iCodecSubType == H264_CUDA)
					cudaEncClose(hEncoder->m_pCuda);*/
			}
			break;
		default:
			break;
		} // end switch


#ifdef	VIDEO_DUMP
		McCloseFile(hEncoder->m_hFile);
#endif
#ifdef VIDEO_YUV_DUMP
		McCloseFile(hEncoder->m_hFileYUV);
#endif
		FREE(hEncoder->m_pEncFrame);

		FREE(hEncoder);
	}

	LOG_I("VideoEncClose End");
}

LRSLT FrVideoEncInit(void* hHandle, FrVideoInfo* hInfo, DWORD32 dwPass) {
	FrVideoEncStruct	*hEncoder = (FrVideoEncStruct*)hHandle;
	//LRSLT			lRet;

	hEncoder->m_dwFrameNum = 0;
	hEncoder->m_bIntra = TRUE;
    
    LOG_I("McVideoEncInit begin (0x%x)", hEncoder->m_dwFourCC);

	switch (hEncoder->m_dwFourCC) {
	case FOURCC_h264:
	case FOURCC_H264:
	case FOURCC_AVC1:
		if (hEncoder->m_eCodecType == CODEC_HW) {
			;
		}
		else {
			;
		}

#ifdef	VIDEO_DUMP
		McWriteFile(hEncoder->m_hFile, (char*)&hInfo->m_dwConfig, 4);
		McWriteFile(hEncoder->m_hFile, hInfo->m_pConfig, hInfo->m_dwConfig);
#endif
		break;
	default:
		break;
	} // end switch

    LOG_I("McVideoEncInit end");
           
	return	FR_OK;
}

BOOL FrVideoEncDestroy(void* hHandle, BOOL bEncCancel) { 
	FrVideoEncStruct	*hEncoder = (FrVideoEncStruct*)hHandle;
	BOOL	bRet = TRUE;

	if (hEncoder) {
		switch (hEncoder->m_dwFourCC) {
		case FOURCC_H264:
		case FOURCC_AVC1:
			if (hEncoder->m_eCodecType == CODEC_HW) {
				;
			}
			else {
				;
			}
			break;
		default:
			break;
		} // end switch
	}

	return	bRet;
}

BOOL FrVideoEncReset(void* hHandle, int nStartCTS) {
	FrVideoEncStruct	*hEncoder = (FrVideoEncStruct*)hHandle;
	if (!hEncoder)
		return FALSE;

	//hEncoder->m_nStartCTS = nStartCTS;
	//hEncoder->m_dwSrcCTS = 0;
	hEncoder->m_bIntra = TRUE;
	
	//LOG_I("[EncVideo]	McVideoEncReset - StartCTS = %d", hEncoder->m_nStartCTS);

	return TRUE;
}

LRSLT FrVideoEncEncode(void* hHandle, FrRawVideo* pSrc, FrMediaStream* pDst) {
	FrVideoEncStruct	*hEncoder = (FrVideoEncStruct*)hHandle;
	//INT64	n64CTS;
	DWORD32	dwRet = 0;

	if (hEncoder == NULL)
		return FR_FAIL;

	if (!pSrc) {		
		//LOG_I("[EncVideo] source is NULL StartCTS = %d, srcCTS = %d", hEncoder->m_nStartCTS, pSrc->m_dwCTS);
		return FR_FAIL;
	}

#ifdef VIDEO_YUV_DUMP
	//if (hEncoder->m_nPassCnt == hEncoder->m_nPassMode-1)
	{
		if (pDec)
		{
			McWriteFile(hEncoder->m_hFileDec, pDec->m_pY, hEncoder->m_dwWidth * hEncoder->m_dwHeight);
			McWriteFile(hEncoder->m_hFileDec, pDec->m_pU, hEncoder->m_dwWidth * hEncoder->m_dwHeight / 4);
			McWriteFile(hEncoder->m_hFileDec, pDec->m_pV, hEncoder->m_dwWidth * hEncoder->m_dwHeight / 4);
		}
	}
#endif

	auto start = std::chrono::system_clock::now();
	// LARGE_INTEGER qStart = { 0 };
	// LARGE_INTEGER qEnd = { 0 };
	// QueryPerformanceCounter(&qStart);

	switch (hEncoder->m_dwFourCC) {
	case FOURCC_H264:
	case FOURCC_AVC1:
		if (hEncoder->m_eCodecType == CODEC_HW) {
			dwRet = VideoNvencEncode(hEncoder, pSrc, pDst);
		}
		else {
			/*if ((INT32)hEncoder->m_dwEncCnt <= 0)
				hEncoder->m_dwEncCnt = pSrc->m_dwEncCnt;
			if (hEncoder->m_bIntra)
			{
				pSrc->m_tFrameType = FRAME_I;
				hEncoder->m_bIntra = FALSE;
			}
			else
				pSrc->m_tFrameType = FRAME_P;*/

			dwRet = FFVideoEncEncode(hEncoder->m_pvFFMpeg, pSrc, pDst, hEncoder->m_pEncFrame, hEncoder->m_dwEncBuffSize);
			if (FRFAILED(dwRet)) {
				LOG_E("FFVideoEncEncode Failed");
				return dwRet;
			}
			dwRet = 0;
		}
		break;
	case FOURCC_H265:
	case FOURCC_HEVC:
		if (hEncoder->m_eCodecType == CODEC_HW) {
			dwRet = VideoNvencEncode(hEncoder, pSrc, pDst);
		}
		break;
	default:
		break;
	}

	auto end = std::chrono::system_clock::now();
	std::chrono::milliseconds delta = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	//std::cout << delta.count() << ",  ";
	// QueryPerformanceCounter(&qEnd);
	// int diff = (int)(((qEnd.QuadPart - qStart.QuadPart) * 1000) / hEncoder->m_qFreq.QuadPart);
	
	//LOG_D("EncEncode FrameLen = %d CTS=%d chrono Tick=%d, Query Tick(%d)", pDst->dwFrameLen[0], pDst->dwCTS, delta.count(), diff);
	LOG_D("EncEncode FrameLen = %d CTS=%d chrono Tick=%d", pDst->dwFrameLen[0], pDst->dwCTS, delta.count());



	//LOG_T("[EncVideo]	StartCTS = %d Src=%d CTS=%d End CTS=%d FrameLen = %d",
	//	hEncoder->m_nStartCTS, pSrc->m_dwCTS, pDst->m_dwCTS, pDst->m_dwEndCTS, pDst->m_dwFrameLen[0]);

#ifdef	VIDEO_DUMP
	{
		int i;
		for(i=0; i<pDst->m_dwFrameNum; i++) {
			McWriteFile(hEncoder->m_hFile, (char*)&pDst->m_dwFrameLen[i], 4);
			McWriteFile(hEncoder->m_hFile, pDst->m_pFrame, pDst->m_dwFrameLen[i]);
		}
	}
#endif

	return dwRet;
}

BOOL FrVideoEncFlush(void* hHandle, FrMediaStream* pDst, FrVideoInfo *hVideo) {
	FrVideoEncStruct	*hEncoder = (FrVideoEncStruct*)hHandle;
	BOOL	bRet = FALSE;

	switch (hEncoder->m_dwFourCC)
	{
	case FOURCC_AVC1:
	case FOURCC_H264:
	case FOURCC_h264:
		if (hEncoder->m_eCodecType == CODEC_HW) {
			;
		}
		else {
			;
		}
		break;
	case FOURCC_H265:
	case FOURCC_HEVC:
		break;
	default:
		return FALSE;
	}

	// Need to change CTS based on Encoding Start CTS. Do not change CTS when flushing is done
	// The value in pDst may be referenced by the other funtions
	//if (bRet)
	//	pDst->m_dwCTS += hEncoder->m_nStartCTS;

	return bRet;
}
