#include "VideoEncoder.h"


#ifdef H264_CODEC_X264

int X264DLLOpen(FrVideoEncStruct* hEncoder)
{

#if defined _X264_STATIC_LIB_
	// Pthread init
	//pthread_win32_process_attach_np();
    //pthread_win32_thread_attach_np();
    //x264_encoder_close();

	// copy function pointers
    hEncoder->x_open = (x264_t *(*)(x264_param_t*))x264_encoder_open_148;
	hEncoder->x_close = (void (*)(x264_t*))x264_encoder_close;
	hEncoder->x_encode = (int (*)(x264_t*, x264_nal_t **, int *, x264_picture_t *, x264_picture_t *))x264_encoder_encode;
	hEncoder->x_headers = (int (*)(x264_t*, x264_nal_t**, int *))x264_encoder_headers;
	hEncoder->x_delay = (int (*)(x264_t*))x264_encoder_delayed_frames;
	hEncoder->x_parameters = (void (*)(x264_t*, x264_param_t*))x264_encoder_parameters;
	hEncoder->x_reconfig = (int (*)(x264_t*, x264_param_t*))x264_encoder_reconfig;
	hEncoder->x_default = (void (*)(x264_param_t*))x264_param_default;
	hEncoder->x_clean = (void (*)(x264_picture_t *))x264_picture_clean;
	hEncoder->x_alloc = (int (*)(x264_picture_t *, int, int, int))x264_picture_alloc;

	hEncoder->x_fastfirstpass = (void (*)(x264_param_t *))x264_param_apply_fastfirstpass;
	hEncoder->x_apply_profile = (int (*)(x264_param_t *, const char *))x264_param_apply_profile;
	hEncoder->x_preset = (int (*)(x264_param_t *, const char *, const char *))x264_param_default_preset;
	hEncoder->x_param_parse = (int (*)(x264_param_t *, const char *, const char *))x264_param_parse;

    LOG_I("[EncVideo VideoD264Open]	D264 : X264DLLOpen - static linking..");

#elif defined _X264_DTB_DLL_
#if 0
	hEncoder->m_hModule = LoadLibrary(MC_T("C:\\WINDOWS\\system32\\dve-s-100.dll"));
	if (!hEncoder->m_hModule)
	{
		hEncoder->m_hModule = LoadLibrary(MC_T("dve-s-100.dll"));
		if (!hEncoder->m_hModule)
		{
			return -1;
		}
	}
	hEncoder->x_open = (x264_t *(*)(x264_param_t*))GetProcAddress(hEncoder->m_hModule, MC_T("sub_init"));
	hEncoder->x_close = (void (*)(x264_t*))GetProcAddress(hEncoder->m_hModule, MC_T("VEncClose"));
	hEncoder->x_encode = (int (*)(x264_t*, x264_nal_t **, int *, x264_picture_t *, x264_picture_t *))GetProcAddress(hEncoder->m_hModule, MC_T("VEncEncode"));
	hEncoder->x_headers = (int (*)(x264_t*, x264_nal_t**, int *))GetProcAddress(hEncoder->m_hModule, MC_T("AddEncF1"));
	hEncoder->x_delay = (int (*)(x264_t*))GetProcAddress(hEncoder->m_hModule, MC_T("VEncUpdate"));
	hEncoder->x_parameters = (void (*)(x264_t*, x264_param_t*))GetProcAddress(hEncoder->m_hModule, MC_T("VEncP"));
	hEncoder->x_reconfig = (int (*)(x264_t*, x264_param_t*))GetProcAddress(hEncoder->m_hModule, MC_T("VEncReset"));
	hEncoder->x_default = (void (*)(x264_param_t*))GetProcAddress(hEncoder->m_hModule, MC_T("VEncCfgDef"));
	hEncoder->x_clean = (void (*)(x264_picture_t *))GetProcAddress(hEncoder->m_hModule, MC_T("VEncFrameInit"));
	hEncoder->x_alloc = (int (*)(x264_picture_t *, int, int, int))GetProcAddress(hEncoder->m_hModule, MC_T("VEncFrameAlloc"));

	hEncoder->x_fastfirstpass = (void (*)(x264_param_t *))GetProcAddress(hEncoder->m_hModule, MC_T("VEncCfgInit"));
	hEncoder->x_apply_profile = (int (*)(x264_param_t *, const char *))GetProcAddress(hEncoder->m_hModule, MC_T("set_prof"));
	hEncoder->x_preset = (int (*)(x264_param_t *, const char *, const char *))GetProcAddress(hEncoder->m_hModule, MC_T("preset_para"));
	hEncoder->x_param_parse = (int (*)(x264_param_t *, const char *, const char *))GetProcAddress(hEncoder->m_hModule, MC_T("parse_para"));
#else
	hEncoder->m_hModule = LoadLibrary(MC_T("C:\\WINDOWS\\system32\\dve_s-200.dll"));
	if (!hEncoder->m_hModule)
	{
		//hEncoder->m_hModule = LoadLibrary(MC_T("dve_s-200.dll"));
		//hEncoder->m_hModule = LoadLibrary("C:\\Dev\\4dreplay\\4d_project\\x64\\Debug\\dve_s-200.dll");
		hEncoder->m_hModule = LoadLibrary("C:\\dve_s-200.dll");
		if (!hEncoder->m_hModule)
		{
			LOG_E("[x264] error code(%d)", GetLastError());
			return -1;
		}
	}
	hEncoder->x_open = (x264_t *(*)(x264_param_t*))GetProcAddress(hEncoder->m_hModule, MC_T("dtb_h264_encoder_open_200"));
	hEncoder->x_close = (void (*)(x264_t*))GetProcAddress(hEncoder->m_hModule, MC_T("dtb_h264_encoder_close"));
	hEncoder->x_encode = (int (*)(x264_t*, x264_nal_t **, int *, x264_picture_t *, x264_picture_t *))GetProcAddress(hEncoder->m_hModule, MC_T("dtb_h264_encoder_encode"));
	hEncoder->x_headers = (int (*)(x264_t*, x264_nal_t**, int *))GetProcAddress(hEncoder->m_hModule, MC_T("dtb_h264_encoder_headers"));
	hEncoder->x_delay = (int (*)(x264_t*))GetProcAddress(hEncoder->m_hModule, MC_T("dtb_h264_encoder_delayed_frames"));
	hEncoder->x_parameters = (void (*)(x264_t*, x264_param_t*))GetProcAddress(hEncoder->m_hModule, MC_T("dtb_h264_encoder_parameters"));
	hEncoder->x_reconfig = (int (*)(x264_t*, x264_param_t*))GetProcAddress(hEncoder->m_hModule, MC_T("dtb_h264_encoder_reconfig"));
	hEncoder->x_default = (void (*)(x264_param_t*))GetProcAddress(hEncoder->m_hModule, MC_T("dtb_h264_param_default"));
	hEncoder->x_clean = (void (*)(x264_picture_t *))GetProcAddress(hEncoder->m_hModule, MC_T("dtb_h264_picture_clean"));
	hEncoder->x_alloc = (int (*)(x264_picture_t *, int, int, int))GetProcAddress(hEncoder->m_hModule, MC_T("dtb_h264_picture_alloc"));

	hEncoder->x_fastfirstpass = (void (*)(x264_param_t *))GetProcAddress(hEncoder->m_hModule, MC_T("dtb_h264_param_apply_fastfirstpass"));
	hEncoder->x_apply_profile = (int (*)(x264_param_t *, const char *))GetProcAddress(hEncoder->m_hModule, MC_T("dtb_h264_param_apply_profile"));
	hEncoder->x_preset = (int (*)(x264_param_t *, const char *, const char *))GetProcAddress(hEncoder->m_hModule, MC_T("dtb_h264_param_default_preset"));
	hEncoder->x_param_parse = (int (*)(x264_param_t *, const char *, const char *))GetProcAddress(hEncoder->m_hModule, MC_T("dtb_h264_param_parse"));
#endif

#else
	// 1. load dll
	hEncoder->m_hModule = LoadLibrary(MC_T("C:\\WINDOWS\\system32\\dtbvide.dll"));
	if (!hEncoder->m_hModule)
	{
		hEncoder->m_hModule = LoadLibrary(MC_T("dtbvide.dll"));
		if (!hEncoder->m_hModule)
		{
			return -1;
		}
	}

	// current opeating version..92
	hEncoder->x_open = (x264_t *(*)(x264_param_t*))GetProcAddress(hEncoder->m_hModule, MC_T("x264_encoder_open_92"));
	hEncoder->x_close = (void (*)(x264_t*))GetProcAddress(hEncoder->m_hModule, MC_T("x264_encoder_close"));
	hEncoder->x_encode = (int (*)(x264_t*, x264_nal_t **, int *, x264_picture_t *, x264_picture_t *))GetProcAddress(hEncoder->m_hModule, MC_T("x264_encoder_encode"));
	hEncoder->x_headers = (int (*)(x264_t*, x264_nal_t**, int *))GetProcAddress(hEncoder->m_hModule, MC_T("x264_encoder_headers"));
	hEncoder->x_delay = (int (*)(x264_t*))GetProcAddress(hEncoder->m_hModule, MC_T("x264_encoder_delayed_frames"));
	hEncoder->x_parameters = (void (*)(x264_t*, x264_param_t*))GetProcAddress(hEncoder->m_hModule, MC_T("x264_encoder_parameters"));
	hEncoder->x_reconfig = (int (*)(x264_t*, x264_param_t*))GetProcAddress(hEncoder->m_hModule, MC_T("x264_encoder_reconfig"));
	hEncoder->x_default = (void (*)(x264_param_t*))GetProcAddress(hEncoder->m_hModule, MC_T("x264_param_default"));
	hEncoder->x_clean = (void (*)(x264_picture_t *))GetProcAddress(hEncoder->m_hModule, MC_T("x264_picture_clean"));
	hEncoder->x_alloc = (int (*)(x264_picture_t *, int, int, int))GetProcAddress(hEncoder->m_hModule, MC_T("x264_picture_alloc"));

	hEncoder->x_fastfirstpass = (void (*)(x264_param_t *))GetProcAddress(hEncoder->m_hModule, MC_T("x264_param_apply_fastfirstpass"));
	hEncoder->x_apply_profile = (int (*)(x264_param_t *, const char *))GetProcAddress(hEncoder->m_hModule, MC_T("x264_param_apply_profile"));
	hEncoder->x_preset = (int (*)(x264_param_t *, const char *, const char *))GetProcAddress(hEncoder->m_hModule, MC_T("x264_param_default_preset"));
	hEncoder->x_param_parse = (int (*)(x264_param_t *, const char *, const char *))GetProcAddress(hEncoder->m_hModule, MC_T("x264_param_parse"));
#endif

	if (!hEncoder->x_open || !hEncoder->x_close || !hEncoder->x_encode || !hEncoder->x_headers || !hEncoder->x_delay ||
		!hEncoder->x_parameters || !hEncoder->x_reconfig || !hEncoder->x_default || !hEncoder->x_clean || !hEncoder->x_alloc ||
		!hEncoder->x_fastfirstpass || !hEncoder->x_apply_profile || !hEncoder->x_preset)
	{
		return -1;
	}

	return 1;
}

// watch out,  sps positon..!!
// as update x264 library, change sps position.
static void GetParamSetNoStart(FrVideoInfo *hInfo, FrVideoEncStruct* hEncoder)
{
	unsigned char pBuff[4]="";
	unsigned char pHdr[3]="";
	unsigned char *pos = hInfo->pConfig;
	char *pSeq = (char*)hEncoder->m_pNal[0].p_payload;	// 1 -> 0
	int i_sps_len, i_pps_len;

	// get profile and level..
	hInfo->nProfile		= pSeq[5];
	hInfo->nCompatibility = pSeq[6]; //yhoh �߰�
	hInfo->nLevel			= pSeq[7];

	i_sps_len = hEncoder->m_pNal[0].i_payload - 4;
	i_pps_len = hEncoder->m_pNal[1].i_payload - 4;

	// eliminate start code and add three byte with followed..
	// first 1 byte = number of sps, pps
	// other 2 bytes = length of sps, pps
	pHdr[0] = 0x01;		// fixed value : 1
	pHdr[1] = (unsigned char)(i_sps_len >> 8);
	pHdr[2] = (unsigned char)i_sps_len;

	memcpy(pos, pHdr, 3);
	pos += 3;
	memcpy(pos, hEncoder->m_pNal[0].p_payload+4, i_sps_len);
	pos += i_sps_len;

	pHdr[0] = 0x01;		// fixed value : 1
	pHdr[1] = (unsigned char)(i_pps_len >> 8);
	pHdr[2] = (unsigned char)(i_pps_len);

	memcpy(pos, pHdr, 3);
	pos += 3;
	memcpy(pos, hEncoder->m_pNal[1].p_payload+4, i_pps_len);
	hInfo->dwConfig = i_sps_len + i_pps_len +6;
}

LRSLT SetAdaptiveParam(FrVideoEncStruct* hEncoder, FrVideoInfo *hInfo) {
    int		nRet, nPreset, nTune, nProfile;
	char	szOpt[5120]="";


    // get default parameters..
    hEncoder->x_default(&hEncoder->m_tParam);

    // "ultrafast", "superfast, "veryfast", "faster", "fast", "medium", "slow", "slower", "veryslow", "placebo", 0
    // "film", "animation", "grain", "stillimage", "psnr", "ssim", "fastdecode", "zerolatency", 0
    if ( (0<=hInfo->nPreset) && (hInfo->nPreset<10) )
        nPreset = hInfo->nPreset;
    else
        nPreset = 3;        // faster
    if ( (0<=hInfo->nTune) && (hInfo->nTune<8) )
        nTune = hInfo->nTune;
    else
        nTune = 0;

    hEncoder->x_preset(&hEncoder->m_tParam, x264_preset_names[nPreset], x264_tune_names[nTune]);

    // set timebase by 1/1000 (msec)
	hEncoder->m_tParam.i_timebase_den = 1000;
	hEncoder->m_tParam.i_timebase_num = 1;


    // Other default settings
    hEncoder->m_tParam.b_aud = 0;
    hEncoder->m_tParam.b_repeat_headers = 0;
    hEncoder->m_tParam.b_annexb = 0;
    hEncoder->m_bEnableStartCode = FALSE;

    // to support both mp4 type and ts type for nhn cache..mwkang 2010.09.02
	if (hInfo->BSType == TS_TYPE)
		hEncoder->m_bEnableStartCode = TRUE;

	// video size
	hEncoder->m_tParam.i_width = hInfo->dwWidth;
	hEncoder->m_tParam.i_height = hInfo->dwHeight;

    hEncoder->m_tParam.i_keyint_max = 24;
    hEncoder->m_tParam.i_keyint_min = 24;

    hEncoder->m_tParam.i_scenecut_threshold = -1;

    nProfile = hInfo->nProfileType;

    nRet = hEncoder->x_param_parse(&hEncoder->m_tParam, "no-scenecut", NULL);
    if (nRet != 0)
    {
        LOG_E("[EncVideo VideoD264Open] D264 [error]: D264_param_parse failed  - no-scenecut");
        return COMMON_ERR_VIDEOENC;
    }

    nRet = hEncoder->x_param_parse(&hEncoder->m_tParam, "force-cfr", NULL);
    if (nRet != 0)
    {
        LOG_E("[EncVideo VideoD264Open] D264 [error]: D264_param_parse failed  - force-cfr");
        return COMMON_ERR_VIDEOENC;
    }

    // set abr
    sprintf_s(szOpt, sizeof(szOpt), "%d", hInfo->dwBitRate/1000);
    nRet = hEncoder->x_param_parse( &hEncoder->m_tParam, "bitrate", szOpt );
    if (nRet != 0)
    {
        LOG_E("[EncVideo VideoD264Open] D264 [error]: D264_param_parse failed  - bitrate");
        return COMMON_ERR_VIDEOENC;
    }

    sprintf_s(szOpt, sizeof(szOpt), "%d", hInfo->dwBitRate/1000);
    nRet = hEncoder->x_param_parse(&hEncoder->m_tParam, "vbv-maxrate", szOpt);
    if (nRet != 0)
    {
        LOG_E("[EncVideo VideoD264Open] D264 [error]: D264_param_parse failed  - vbv-maxrate");
        return COMMON_ERR_VIDEOENC;
    }

    // set frame rate..
    if (hInfo->dwFrameRate) {
    	sprintf_s(szOpt, sizeof(szOpt), "%2.2f", hInfo->dwFrameRate / 1000.);
    	nRet = hEncoder->x_param_parse( &hEncoder->m_tParam, "fps", szOpt );
    	if (nRet != 0)
    	{
    		LOG_E("[EncVideo VideoD264Open]	D264 [error]: D264_param_parse failed  - frame rate");
    		return COMMON_ERR_VIDEOENC;
    	}
    }

    if( hEncoder->x_apply_profile( &hEncoder->m_tParam, x264_profile_names[nProfile] ) < 0 )
	{
		LOG_E("[EncVideo VideoD264Open]	D264 [error]: x_apply_profile failed");
		return COMMON_ERR_VIDEOENC;
	}

	// Open
	if( ( hEncoder->m_hX264 = hEncoder->x_open( &hEncoder->m_tParam ) ) == NULL )
    {
        LOG_E("[EncVideo VideoD264Open]	D264 [error]: D264_encoder_open failed");
        return COMMON_ERR_VIDEOENC;
    }

	// Get current params
	hEncoder->x_parameters(hEncoder->m_hX264, &hEncoder->m_tParam);

	// Create a new pic
	hEncoder->m_Pic.i_type = X264_TYPE_AUTO;
    hEncoder->m_Pic.i_qpplus1 = 0;
    hEncoder->m_Pic.img.i_csp = X264_CSP_I420;
    hEncoder->m_Pic.img.i_plane = 3;
    hEncoder->m_Pic.img.i_stride[0] = hEncoder->m_tParam.i_width;
    hEncoder->m_Pic.img.i_stride[1] = hEncoder->m_tParam.i_width / 2;
    hEncoder->m_Pic.img.i_stride[2] = hEncoder->m_tParam.i_width / 2;
    hEncoder->m_Pic.param = NULL;

    return FR_OK;
}


LRSLT VideoX264Open(FrVideoEncStruct* hEncoder, FrVideoInfo *hInfo)
{
	int		nRet, nPreset, nTune, nProfile;
	char	szOpt[5120]="";

	// load 264 library..
	if (X264DLLOpen(hEncoder) < 0)
	{
		LOG_E("[EncVideo VideoD264Open]	D264 [error]: D264_encoder_open failed - DLL open failed");
		return COMMON_ERR_DLLOPEN;
	}
    if (hInfo->bAdaptive == TRUE) {
        SetAdaptiveParam(hEncoder, hInfo);
    } else {

        // get default parameters..
    	hEncoder->x_default(&hEncoder->m_tParam);

    	// "ultrafast", "superfast, "veryfast", "faster", "fast", "medium", "slow", "slower", "veryslow", "placebo", 0
    	// "film", "animation", "grain", "stillimage", "psnr", "ssim", "fastdecode", "zerolatency", 0
    	if ( (0<=hInfo->nPreset) && (hInfo->nPreset<10) )
    		nPreset = hInfo->nPreset;
    	else
    		nPreset = 3;		// faster
    	if ( (0<=hInfo->nTune) && (hInfo->nTune<8) )
    		nTune = hInfo->nTune;
    	else
    		nTune = 0;

    	// zero latency
    	if (hInfo->nTune==7)
    		hInfo->dwBFrame = 0;

    	// preset
    	hEncoder->x_preset(&hEncoder->m_tParam, x264_preset_names[nPreset], x264_tune_names[nTune]);

    	// set timebase by 1/1000 (msec)
    	hEncoder->m_tParam.i_timebase_den = 1000;
    	hEncoder->m_tParam.i_timebase_num = 1;

    	// psnr/ssim report
    	//hEncoder->m_fPsnr = 0.0;
    	//hEncoder->m_fSsim = 0.0;

    	hEncoder->m_tParam.analyse.b_psnr = 1;
    	hEncoder->m_tParam.analyse.b_ssim = 1;

#ifndef SUPPORT_IOS
    	//hEncoder->m_tParam.analyse.p_psnr = &hEncoder->m_fPsnr;
    	//hEncoder->m_tParam.analyse.p_ssim = &hEncoder->m_fSsim;
#endif

    	// Other default settings
    	hEncoder->m_tParam.b_aud = 0;
    	hEncoder->m_tParam.b_repeat_headers = 0;
    	hEncoder->m_tParam.b_annexb = 0;
    	hEncoder->m_bEnableStartCode = FALSE;

    	// to support both mp4 type and ts type for nhn cache..mwkang 2010.09.02
    	if (hInfo->BSType == TS_TYPE)
    		hEncoder->m_bEnableStartCode = TRUE;

    	// video size
    	hEncoder->m_tParam.i_width = hInfo->dwWidth;
    	hEncoder->m_tParam.i_height = hInfo->dwHeight;

    	// key frame interval
    	hEncoder->m_tParam.i_keyint_max = (int)(hInfo->fIntraFrameRefresh * hInfo->dwFrameRate / 1000 );
    	//hEncoder->m_tParam.i_keyint_min = (int)(hInfo->m_fIntraFrameRefresh * hInfo->m_dwFrameRate / 1000 );
    	hEncoder->m_tParam.i_scenecut_threshold = -1;

    	// "X264_RC_CQP", "X264_RC_CRF", "X264_RC_ABR"
    	nProfile = hInfo->nProfileType;
    	if (nProfile == 0)
    		hInfo->dwRCMode = 2;

    	switch(hInfo->dwRCMode)
    	{
    	case	0:		// CQP
    		// set qp value
    		sprintf_s(szOpt, sizeof(szOpt), "%2.2f", (float)hInfo->dwQualityFactor/1000.);
    		nRet = hEncoder->x_param_parse( &hEncoder->m_tParam, "qp", szOpt );
    		if (nRet != 0)
    		{
    			LOG_E("[EncVideo VideoD264Open]	D264 [error]: D264_param_parse failed  - frame rate");
    			return COMMON_ERR_VIDEOENC;
    		}
            LOG_I("[EncVideo VideoD264Open]	D264_param_parse : qp : QualityFactor (%s)", szOpt);

    		break;
    	case	1:		// CRF
    		// set rf value
    		sprintf_s(szOpt, sizeof(szOpt), "%2.2f", (float)hInfo->dwQualityFactor/1000.);
    		nRet = hEncoder->x_param_parse( &hEncoder->m_tParam, "crf", szOpt );
    		if (nRet != 0)
    		{
    			LOG_E("[EncVideo VideoD264Open]	D264 [error]: D264_param_parse failed  - frame rate");
    			return COMMON_ERR_VIDEOENC;
    		}
            LOG_I("[EncVideo VideoD264Open]	D264_param_parse : crf : QualityFactor (%s)", szOpt);

    		break;
    	default:		// ABR
    		// set abr
    		sprintf_s(szOpt, sizeof(szOpt), "%d", hInfo->dwBitRate/1000);
    		nRet = hEncoder->x_param_parse( &hEncoder->m_tParam, "bitrate", szOpt );
    		if (nRet != 0)
    		{
    			LOG_E("[EncVideo VideoD264Open]	D264 [error]: D264_param_parse failed  - bitrate");
    			return COMMON_ERR_VIDEOENC;
    		}

            LOG_I("[EncVideo VideoD264Open]	D264_param_parse : ABR : Bitrate (%s)", szOpt);
    	}

    	// set frame rate..
    	sprintf_s(szOpt, sizeof(szOpt), "%2.2f", hInfo->dwFrameRate / 1000.);
    	nRet = hEncoder->x_param_parse( &hEncoder->m_tParam, "fps", szOpt );
    	if (nRet != 0)
    	{
    		LOG_E("[EncVideo VideoD264Open]	D264 [error]: D264_param_parse failed  - frame rate");
    		return COMMON_ERR_VIDEOENC;
    	}
        LOG_I("[EncVideo VideoD264Open]	D264_param_parse : FPS : FrameRate (%s)", szOpt);

    	// Profile
    	if(hInfo->nProfileType==0 )		// Baseline
    	{
    		// NHN
    		if (hInfo->dwWidth <= 480 && hInfo->dwHeight <= 360)
    		{
    			hEncoder->m_tParam.i_threads = 1;
    			hEncoder->m_tParam.b_sliced_threads = TRUE;
    		}
    		hEncoder->m_tParam.i_frame_reference = 1;
    		LOG_I("[EncVideo VideoD264Open]	D264 : It's working as baseline profile with single nal");
    	}
    	else
    	{
    		if (hInfo->dwVidEncThread <= 12)
    			hEncoder->m_tParam.i_threads = hInfo->dwVidEncThread;
    		else
    			hEncoder->m_tParam.i_threads = 4;
    		hEncoder->m_tParam.i_bframe = (int)hInfo->dwBFrame;
    		// Probably for RTS
    		if (hInfo->bSpeedOptimization == FALSE)
    		{
    			hEncoder->m_tParam.i_bframe = 0;
    			hEncoder->m_tParam.i_frame_reference = 1;
    			hEncoder->m_tParam.b_sliced_threads = TRUE;
    		}

    		LOG_I("[EncVideo VideoD264Open]	D264 : It's working with (%d) enc thread..", hEncoder->m_tParam.i_threads);
    	}

    	if (0 && hInfo->dwNumPasses==1)
    	{
    		sprintf_s(szOpt, sizeof(szOpt), "%s", hInfo->pConfig);
    		//nRet = hEncoder->x_param_parse( &hEncoder->m_tParam, "stats", szOpt );
    		nRet = hEncoder->x_param_parse( &hEncoder->m_tParam, "stats", "C:\\Temp\\stats.log" );
    		if (nRet != 0)
    		{
    			LOG_E("[EncVideo VideoD264Open]	D264 [error]: D264_param_parse failed  - frame rate");
    			return COMMON_ERR_VIDEOENC;
    		}

    		// set 1st pass
    		nRet = hEncoder->x_param_parse( &hEncoder->m_tParam, "pass", "1" );
    		if (nRet != 0)
    		{
    			LOG_E("[EncVideo VideoD264Open]	D264 [error]: D264_param_parse failed  - frame rate");
    			return COMMON_ERR_VIDEOENC;
    		}
    	}

    	// If first pass mode is used, apply faster settings.
    	hEncoder->x_fastfirstpass( &hEncoder->m_tParam );

    	if( hEncoder->x_apply_profile( &hEncoder->m_tParam, x264_profile_names[nProfile] ) < 0 )
    	{
    		LOG_E("[EncVideo VideoD264Open]	D264 [error]: x_apply_profile failed");
    		return COMMON_ERR_VIDEOENC;
    	}

    	// Open
    	if( ( hEncoder->m_hX264 = hEncoder->x_open( &hEncoder->m_tParam ) ) == NULL )
        {
            LOG_E("[EncVideo VideoD264Open]	D264 [error]: D264_encoder_open failed");
            return COMMON_ERR_VIDEOENC;
        }

    	// Get current params
    	hEncoder->x_parameters(hEncoder->m_hX264, &hEncoder->m_tParam);

    	// Create a new pic
    	hEncoder->m_Pic.i_type = X264_TYPE_AUTO;
        hEncoder->m_Pic.i_qpplus1 = 0;

        /*
        if (hInfo->m_eColorFormat == VideoFmtNV12)
        {
            hEncoder->m_Pic.img.i_csp = X264_CSP_NV12;
            hEncoder->m_Pic.img.i_plane = 2;
            hEncoder->m_Pic.img.i_stride[0] = hEncoder->m_tParam.i_width;
            hEncoder->m_Pic.img.i_stride[1] = hEncoder->m_tParam.i_width;
        }
        else
        {
            hEncoder->m_Pic.img.i_csp = X264_CSP_I420;
            hEncoder->m_Pic.img.i_plane = 3;
            hEncoder->m_Pic.img.i_stride[0] = hEncoder->m_tParam.i_width;
            hEncoder->m_Pic.img.i_stride[1] = hEncoder->m_tParam.i_width / 2;
            hEncoder->m_Pic.img.i_stride[2] = hEncoder->m_tParam.i_width / 2;
        }
         */

        hEncoder->m_Pic.param = NULL;

    	LOG_I("[EncVideo] VideoD264Open - preset=%d, tune=%d, profile=%d, rcmode=%d, bitrate=%d, quailityfactor=%d", nPreset, nTune, nProfile, hInfo->dwRCMode, hInfo->dwBitRate, hInfo->dwQualityFactor);
    	LOG_I("[EncVideo] VideoD264Open - Success !!");
    }

	return FR_OK;
}

LRSLT VideoX264Init(FrVideoEncStruct* hEncoder, FrVideoInfo* hInfo)
{
	INT32	iRet, iNal;
	INT32	iSize=0;
	BYTE *pos = hEncoder->m_pConfig;

	iRet = hEncoder->x_headers(hEncoder->m_hX264, &hEncoder->m_pNal, &iNal);
	if(iRet < 0)
		return COMMON_ERR_VIDEOENC;

	// config info with start code
	memcpy(pos, hEncoder->m_pNal[0].p_payload, hEncoder->m_pNal[0].i_payload);
	pos[0] = pos[1] = pos[2] = 0; pos[3] = 1;			// replace Bytes of size with Bytes of start code
	pos += hEncoder->m_pNal[0].i_payload;
	memcpy(pos, hEncoder->m_pNal[1].p_payload, hEncoder->m_pNal[1].i_payload);
	pos[0] = pos[1] = pos[2] = 0; pos[3] = 1;			// replace Bytes of size with Bytes of start code
	hEncoder->m_dwConfig = hEncoder->m_pNal[0].i_payload + hEncoder->m_pNal[1].i_payload;

	// this is to be used later for IPhone RTS
	//hInfo->m_dwOrgConfig = hEncoder->m_dwConfig;
	//memcpy(hInfo->m_pOrgConfig, hEncoder->m_pConfig, hEncoder->m_dwConfig);

	// config info for MP4
	GetParamSetNoStart(hInfo, hEncoder);

	return FR_OK;
}

BOOL VideoX264Destroy(FrVideoEncStruct* hEncoder)
{
	x264_picture_t	pic_out;
    x264_nal_t		*nal;
    INT32			iLen, i_nal, i_delay, iCount=0;

	if (hEncoder->m_bFlushed == TRUE)
	{
		LOG_D("D264 : already flushed");
		return TRUE;
	}

	// flush delayed frames
	while( iCount++ < 1000 )
	{
		i_delay = hEncoder->x_delay(hEncoder->m_hX264);
		if (!i_delay)
			break;

		iLen = hEncoder->x_encode(hEncoder->m_hX264, &nal, &i_nal, NULL, &pic_out);
		if (iLen < 0)
		{
			LOG_E("D264 [error]: D264_encoder_encode failed" );
			return FALSE;
		}
		else if (iLen == 0)
		{
			LOG_D("D264 : all the frames flushed" );
			return TRUE;
		}
	}

	return TRUE;
}

void VideoX264Close(FrVideoEncStruct* hEncoder)
{
	FREE(hEncoder->m_pEncFrame);
	//FREE(hEncoder->m_pRefFrame);

	if (hEncoder->m_hX264)
		hEncoder->x_close( hEncoder->m_hX264 );

#if !defined _X264_STATIC_LIB_
	if (hEncoder->m_hModule)
		FreeLibrary(hEncoder->m_hModule);
#endif

#if defined _X264_STATIC_LIB_
	{
	//extern void pthread_globals_init();
    //pthread_win32_thread_detach_np();
    //pthread_win32_process_detach_np();
	//pthread_globals_init();
	}
#endif
}


#ifdef	KT_INSPRIT
	static frame_cnt___;
#endif

int VideoX264Encode(FrVideoEncStruct* hEncoder, FrRawVideo *pSrc, FrMediaStream* pDst)
{
	x264_picture_t	pic_out;
    x264_nal_t		*nal;
    int				i_nal, i_nalu_size=0, i;
	int				i_frame_size = 0;
	BYTE*			pFrame = hEncoder->m_pEncFrame;
	DWORD32			total_size = 0;

    if (pSrc->eColorFormat == VideoFmtNV12)
    {
        hEncoder->m_Pic.img.i_csp = X264_CSP_NV12;
        hEncoder->m_Pic.img.i_plane = 2;

        hEncoder->m_Pic.img.plane[0]    = pSrc->pY;
        hEncoder->m_Pic.img.plane[1]    = pSrc->pU;
        hEncoder->m_Pic.img.i_stride[0] = pSrc->dwPitch;
        hEncoder->m_Pic.img.i_stride[1] = pSrc->dwPitch;
    }
    else
    {
        hEncoder->m_Pic.img.i_csp = X264_CSP_I420;
        hEncoder->m_Pic.img.i_plane = 3;

        hEncoder->m_Pic.img.plane[0]    = pSrc->pY;
        hEncoder->m_Pic.img.plane[1]    = pSrc->pU;
        hEncoder->m_Pic.img.plane[2]    = pSrc->pV;
        hEncoder->m_Pic.img.i_stride[0] = pSrc->dwPitch;
        hEncoder->m_Pic.img.i_stride[1] = pSrc->dwPitch/2;
        hEncoder->m_Pic.img.i_stride[2] = pSrc->dwPitch/2;
    }

	hEncoder->m_Pic.i_pts			= hEncoder->m_dwPtsCnt;
	hEncoder->m_dwPtsCnt++;

	/* Do not force any parameters */
//#if 1
#ifdef	KT_INSPRIT
    if( (frame_cnt___++ % hEncoder->m_tParam.i_keyint_max) == 0)
		hEncoder->m_Pic.i_type = X264_TYPE_IDR;
	else
		hEncoder->m_Pic.i_type = X264_TYPE_P;
#else
	hEncoder->m_Pic.i_type = X264_TYPE_AUTO;
#endif

	// Copy CTS of 1st video frame
	if (hEncoder->m_n64DTSBase == MIN_INT64) {
		if (pSrc->dwCTS)
			LOG_D("[VideoEnc] D264 Encode - 1st Frame has non-zero cts %d", pSrc->dwCTS);
		hEncoder->m_n64DTSBase = ((INT64)0-pSrc->dwCTS);
	}

	// Set some info
	if (hEncoder->m_bIntra) {
		hEncoder->m_bIntra = FALSE;
		hEncoder->m_Pic.i_type = X264_TYPE_IDR;
		hEncoder->m_dwEncCnt = 0;
	}
    hEncoder->m_Pic.i_qpplus1 = 0;
	if(hEncoder->m_dwEncCnt <= 0) {
		hEncoder->m_dwEncCnt = pSrc->dwEncCnt;
	}

	// use Pts of Source
	hEncoder->m_Pic.i_pts = (INT64)pSrc->dwCTS;

	i_frame_size = hEncoder->x_encode( hEncoder->m_hX264, &nal, &i_nal, &hEncoder->m_Pic, &pic_out );
	if( i_frame_size < 0 )
    {
        LOG_E("D264 [error]: D264_encoder_encode failed" );
		return -1;
    }

    //LOG_I( "D264 : D264_encoder_encode : f-type (%d) size (%d)", pic_out.i_type, i_frame_size);

	// zero frame
	if( i_frame_size == 0) {
		pDst->pFrame = hEncoder->m_pEncFrame;
		pDst->dwFrameNum = 1;
		pDst->dwFrameLen[0] = 0;

		//LOG_I( "SRC DTS %8d", pSrc->m_dwCTS);

		return --hEncoder->m_dwEncCnt;
	}

	// Get the CTS and DTS bias from timestamp of 1st frame
	if (!hEncoder->m_bDTS) {
		hEncoder->m_bDTS = TRUE;

		// Nothing
		if (pic_out.i_dts < 0) {
			LOG_D("[VideoEnc] D264 Encode - 1st Frame has dts %d", pic_out.i_dts);
			hEncoder->m_n64DTSBase += pic_out.i_dts;
		}
		else
			hEncoder->m_n64DTSBase = 0;
	}

	if (hEncoder->m_tParam.i_bframe == 0) {	
		pDst->dwCTS = (DWORD32)(pic_out.i_dts - hEncoder->m_n64DTSBase);
		/*pDst->m_dbCTS = (DOUBLE)pDst->m_dwCTS;
		pDst->m_dwDTS[0] = pDst->m_dwCTS;
		pDst->m_eTimeStamp = TIMESTAMP_DTS;*/
	}
	else
	{
		//LOG_I( "SRC DTS %8d DTS %8d CTS %8d", pSrc->m_dwCTS, (DWORD32)pic_out.i_dts, (DWORD32)pic_out.i_pts);
		pDst->dwCTS = (DWORD32)(pic_out.i_pts - hEncoder->m_n64DTSBase);
		/*pDst->m_dbCTS = (DOUBLE)pDst->m_dwCTS;
		pDst->m_dwDTS[0] = (DWORD32)(pic_out.i_dts - hEncoder->m_n64DTSBase);
		pDst->m_eTimeStamp = TIMESTAMP_PTS_DTS;*/
	}

#if 0
#define X264_TYPE_AUTO          0x0000  /* Let x264 choose the right type */
#define X264_TYPE_IDR           0x0001
#define X264_TYPE_I             0x0002
#define X264_TYPE_P             0x0003
#define X264_TYPE_BREF          0x0004  /* Non-disposable B-frame */
#define X264_TYPE_B             0x0005
#define X264_TYPE_KEYFRAME      0x0006  /* IDR or I depending on b_open_gop option */
#endif

	switch(pic_out.i_type)
	{
	case	X264_TYPE_IDR:	pDst->tFrameType[0] = FRAME_I;	break;
	case	X264_TYPE_B:
	case	X264_TYPE_BREF:	pDst->tFrameType[0] = FRAME_B;	break;
	default:				pDst->tFrameType[0] = FRAME_P;	break;
	}

#if 0
	for(i=0;i<i_nal;i++)
	{
		switch(nal[i].i_type)
		{
		case 0:		// NAL_UNKNOWN
			pDst->m_tFrameType[0] = FRAME_UNKNOWN;
			break;
		case 1:		// NAL_SLICE
			pDst->m_tFrameType[0] = FRAME_P;
			break;
		case 2:		// NAL_SLICE_DPA
			pDst->m_tFrameType[0] = FRAME_P;		// ???
			break;
		case 3:		// NAL_SLICE_DPB
			pDst->m_tFrameType[0] = FRAME_P;
			break;
		case 4:		// NAL_SLICE_DPC
			pDst->m_tFrameType[0] = FRAME_P;
			break;
		case 5:		// NAL_SLICE_IDR
			pDst->m_tFrameType[0] = FRAME_I;
			break;
		case 6:		// NAL_SEI
			pDst->m_tFrameType[0] = FRAME_UNKNOWN;
			break;
		case 7:		// NAL_SPS
			pDst->m_tFrameType[0] = FRAME_UNKNOWN;
			break;
		case 8:		// NAL_PPS
			pDst->m_tFrameType[0] = FRAME_UNKNOWN;
			break;
		case 9:		// NAL_AUD
			pDst->m_tFrameType[0] = FRAME_UNKNOWN;
			break;
		default:
			pDst->m_tFrameType[0] = FRAME_UNKNOWN;
			LOG_I( "D264 : nal type is %d",  nal[0].i_type);
			break;
		}
		if(pDst->m_tFrameType[0] == FRAME_I ||
			pDst->m_tFrameType[0] == FRAME_P ||
			pDst->m_tFrameType[0] == FRAME_B)
			break;
	}
#endif

	if (hEncoder->m_bEnableStartCode) {
		if(pDst->tFrameType[0] == FRAME_I) {
			// add access unit delimeter..
			*pFrame++ = 0x00;
			*pFrame++ = 0x00;
			*pFrame++ = 0x00;
			*pFrame++ = 0x01;
			*pFrame++ = 0x09;
			*pFrame++ = 0xF0;

			memcpy(pFrame, hEncoder->m_pConfig, hEncoder->m_dwConfig);
			pFrame += hEncoder->m_dwConfig;

			//memcpy(pFrame, nal[0].p_payload, i_frame_size);
			pDst->dwFrameLen[0] = i_frame_size + hEncoder->m_dwConfig + 6;
		}
		else
		{
			*pFrame++ = 0x00;
			*pFrame++ = 0x00;
			*pFrame++ = 0x00;
			*pFrame++ = 0x01;
			*pFrame++ = 0x09;
			*pFrame++ = 0xF0;

			//memcpy(pFrame, nal[0].p_payload, i_frame_size);
			pDst->dwFrameLen[0] = i_frame_size + 6;
		}

		for(i=0;i<i_nal;i++, nal++) {
			DWORD32	size;

			BYTE *st = nal->p_payload;
			size = nal->i_payload;
			// start code
			st[0] = st[1] = st[2] = 0;
			st[3] = 1;

			memcpy(pFrame, st, size);
			pFrame += size;
			total_size += size;
		}
	}
	else
	{
		//memcpy(pFrame, nal[0].p_payload, i_frame_size);
		//pDst->m_dwFrameLen[0] = i_frame_size;
		//pDst->m_ucTag[0] = 0;

		//Only write idr slice or inter slice
		for(i=0;i<i_nal;i++, nal++)	{
			if ( nal->i_type==5 || nal->i_type==1 ) {
				memcpy(pFrame, nal->p_payload, nal->i_payload);
                pFrame += nal->i_payload;
				total_size += nal->i_payload;
			}
		}
		pDst->dwFrameLen[0] = total_size;
		//pDst->m_ucTag[0] = 0;
	}
	//if (hEncoder->m_bEnableStartCode)
	//{
		// force to fix access unit delimeter..
	//	pFrame[5] = 0xE0;
	//}

	pDst->pFrame = hEncoder->m_pEncFrame;
	pDst->dwFrameNum = 1;

	// No flush
	hEncoder->m_bFlushed = FALSE;

	return --hEncoder->m_dwEncCnt;
}

BOOL VideoX264Flush(FrVideoEncStruct* hEncoder, FrMediaStream* pDst)
{
	x264_picture_t	pic_out;
    x264_nal_t		*nal;
    int				i_nal, i_nalu_size=0, i_delay, i;
	int				i_frame_size = 0;
	BYTE*			pFrame = hEncoder->m_pEncFrame;
	DWORD32			total_size = 0;

	i_delay = hEncoder->x_delay(hEncoder->m_hX264);
	if (!i_delay)
	{
		LOG_E("[VideoEnc] D264 Flush ended");
		hEncoder->m_bFlushed = TRUE;
		return FALSE;
	}

	i_frame_size = hEncoder->x_encode( hEncoder->m_hX264, &nal, &i_nal, NULL, &pic_out );
	if( i_frame_size <= 0 ) {
        LOG_E("D264 [error]: D264_encoder_encode failed" );
		hEncoder->m_bFlushed = TRUE;
        return FALSE;
    }

	if (hEncoder->m_tParam.i_bframe == 0) {
		pDst->dwCTS = (DWORD32)(pic_out.i_dts - hEncoder->m_n64DTSBase);
		/*pDst->m_dbCTS = (DOUBLE)pDst->m_dwCTS;
		pDst->m_dwDTS[0] = pDst->m_dwCTS;
		pDst->m_eTimeStamp = TIMESTAMP_DTS;*/
	}
	else
	{
		pDst->dwCTS = (DWORD32)(pic_out.i_pts - hEncoder->m_n64DTSBase);
		/*pDst->m_dbCTS = (DOUBLE)pDst->m_dwCTS;
		pDst->m_dwDTS[0] = (DWORD32)(pic_out.i_dts - hEncoder->m_n64DTSBase);
		pDst->m_eTimeStamp = TIMESTAMP_PTS_DTS;*/
	}

	for(i=0;i<i_nal;i++)
	{
		switch(nal[i].i_type)
		{
		case 0:		// NAL_UNKNOWN
			pDst->tFrameType[0] = FRAME_UNKNOWN;
			break;
		case 1:		// NAL_SLICE
			pDst->tFrameType[0] = FRAME_P;
			break;
		case 2:		// NAL_SLICE_DPA
			pDst->tFrameType[0] = FRAME_P;		// ???
			break;
		case 3:		// NAL_SLICE_DPB
			pDst->tFrameType[0] = FRAME_P;
			break;
		case 4:		// NAL_SLICE_DPC
			pDst->tFrameType[0] = FRAME_P;
			break;
		case 5:		// NAL_SLICE_IDR
			pDst->tFrameType[0] = FRAME_I;
			break;
		case 6:		// NAL_SEI
			pDst->tFrameType[0] = FRAME_UNKNOWN;
			break;
		case 7:		// NAL_SPS
			pDst->tFrameType[0] = FRAME_UNKNOWN;
			break;
		case 8:		// NAL_PPS
			pDst->tFrameType[0] = FRAME_UNKNOWN;
			break;
		case 9:		// NAL_AUD
			pDst->tFrameType[0] = FRAME_UNKNOWN;
			break;
		default:
			pDst->tFrameType[0] = FRAME_UNKNOWN;
			LOG_I("D264 : nal type is %d",  nal[0].i_type);
			break;
		}
		if(pDst->tFrameType[0] == FRAME_I ||
			pDst->tFrameType[0] == FRAME_P ||
			pDst->tFrameType[0] == FRAME_B)
			break;
	}

	if (hEncoder->m_bEnableStartCode) {
		if(pDst->tFrameType[0] == FRAME_I) {
			// add access unit delimeter..
			*pFrame++ = 0x00;
			*pFrame++ = 0x00;
			*pFrame++ = 0x00;
			*pFrame++ = 0x01;
			*pFrame++ = 0x09;
			*pFrame++ = 0xF0;

			memcpy(pFrame, hEncoder->m_pConfig, hEncoder->m_dwConfig);
			pFrame += hEncoder->m_dwConfig;

			//memcpy(pFrame, nal[0].p_payload, i_frame_size);
			pDst->dwFrameLen[0] = i_frame_size + hEncoder->m_dwConfig + 6;
		}
		else
		{
			*pFrame++ = 0x00;
			*pFrame++ = 0x00;
			*pFrame++ = 0x00;
			*pFrame++ = 0x01;
			*pFrame++ = 0x09;
			*pFrame++ = 0xF0;

			//memcpy(pFrame, nal[0].p_payload, i_frame_size);
			pDst->dwFrameLen[0] = i_frame_size + 6;
		}

		for(i=0;i<i_nal;i++, nal++)	{
			DWORD32	size;

			BYTE *st = nal->p_payload;
			size = nal->i_payload;
			// start code
			st[0] = st[1] = st[2] = 0;
			st[3] = 1;

			memcpy(pFrame, st, size);
			pFrame += size;
			total_size += size;
		}
	}
	else
	{
		//memcpy(pFrame, nal[0].p_payload, i_frame_size);
		//pDst->m_dwFrameLen[0] = i_frame_size;
		//pDst->m_ucTag[0] = 0;

		//Only write idr slice or inter slice
		for(i=0;i<i_nal;i++, nal++)	{
			if ( nal->i_type==5 || nal->i_type==1 )	{
				memcpy(pFrame, nal->p_payload, nal->i_payload);
				pFrame += nal->i_payload;
				total_size += nal->i_payload;
			}
		}
		pDst->dwFrameLen[0] = total_size;
		//pDst->m_ucTag[0] = 0;
	}
	//if (hEncoder->m_bEnableStartCode)
	//{
		// force to fix access unit delimeter..
	//	pFrame[5] = 0xE0;
	//}

	pDst->pFrame = hEncoder->m_pEncFrame;
	pDst->dwFrameNum = 1;

	return TRUE;
}


#endif
