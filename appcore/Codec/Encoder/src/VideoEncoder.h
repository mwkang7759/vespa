
#ifndef	_VIDEOENCODER_H_
#define	_VIDEOENCODER_H_

#include "SystemAPI.h"
#include "TraceAPI.h"
#include "mediainfo.h"
#include "mediafourcc.h"


#include "FFCodecAPI.h"

// GPU
#include "ESMNvenc.h"


#ifdef _DEBUG
//#define		VIDEO_DUMP
//#define		VIDEO_YUV_DUMP
#endif


#define H264_X264 1
#define H264_FFMPEG 2

//#define H264_CODEC_X264
#ifdef H264_CODEC_X264
#if defined(WIN32)
#define _X264_DTB_DLL_
#include "stdint_win.h"
//#include "x264.h"
#include "dtb_h264_redef.h"
#include "dtb_h264.h"
#elif defined(Linux) || defined(MINGW) || defined(SUPPORT_ANDROID) || defined(SUPPORT_IOS)
#define _X264_STATIC_LIB_
#ifdef __cplusplus
extern "C"
{
#endif    /* __cplusplus */

#include "dtb_h264_redef.h"
#include "dtb_h264.h"

#ifdef __cplusplus
}
#endif    /* __cplusplus */
#endif
#endif

#ifdef FFMPEG_CODEC_VIDEO
#include "FFMpegCodecAPI.h"
#endif




typedef	struct {
	INT32				m_iCodecSubType;
	FrCodecType			m_eCodecType;

	DWORD32				m_dwFourCC;
	DWORD32				m_dwWidth;
	DWORD32				m_dwHeight;
	BYTE*				m_pEncFrame;
	DWORD32				m_dwEncBuffSize;
	void*				m_pvFFMpeg;
	DWORD32			    m_dwFrameNum;
	
	DWORD32				m_dwEncCnt;
    BOOL				m_bIntra;
	BOOL						m_bEnableStartCode;
	BYTE						m_pConfig[MAX_CONFIG_SIZE];
	DWORD32						m_dwConfig;
#ifdef WIN32
	HMODULE						m_hModule;
	LARGE_INTEGER m_qFreq;
#endif
	DWORD32						m_dwPtsCnt;

	double				m_dbFrameDuration;
	BOOL						m_bDTS;
	INT64						m_n64DTSBase;

	BOOL						m_bFlushed;						// Set when all the frames in the buffer are flushed
																// Workaround for not returning from EncoderDestory after flushing all the frames

	


	ESMNvenc* m_pEnc;
	ESMNvenc::CONTEXT_T m_EncCtx;


#ifdef H264_CODEC_X264
	x264_param_t				m_tParam;
	x264_t* m_hX264;
	x264_nal_t* m_pNal;
	x264_picture_t				m_Pic;

	x264_t *(*x_open)( x264_param_t * );
	void    (*x_close)( x264_t * );
	INT32     (*x_delay)( x264_t * );
	void    (*x_parameters)( x264_t *, x264_param_t * );
	INT32     (*x_reconfig)( x264_t *, x264_param_t * );
	INT32     (*x_headers)( x264_t *, x264_nal_t **, INT32 * );
	INT32     (*x_encode)( x264_t *, x264_nal_t **, INT32 *, x264_picture_t *, x264_picture_t * );

	INT32		(*x_alloc)( x264_picture_t *pic, INT32, INT32, INT32 );
	void	(*x_clean)( x264_picture_t *pic );
	void	(*x_default)(x264_param_t *);
	INT32     (*x_preset)(x264_param_t *, const char *, const char *);
	void	(*x_fastfirstpass)( x264_param_t * );
	INT32		(*x_apply_profile)( x264_param_t *, const char *profile );
	INT32		(*x_param_parse)( x264_param_t *, const char *name, const char *value );

#endif

#ifdef CUDA_CODEC
	void*					m_pCuda;
#endif

#ifdef	VIDEO_DUMP
	FILE_HANDLE		m_hFile;
#endif

#ifdef	VIDEO_YUV_DUMP
	FILE_HANDLE		m_hFileDec;
	FILE_HANDLE		m_hFileYUV;
#endif
	
} FrVideoEncStruct;

#ifdef __cplusplus
extern "C"
{
#endif

// X264 encoder
LRSLT VideoX264Open(FrVideoEncStruct* hEncoder, FrVideoInfo *hInfo);
LRSLT VideoX264Init(FrVideoEncStruct* hEncoder, FrVideoInfo* hInfo);
INT32 VideoX264Encode(FrVideoEncStruct* hEncoder, FrRawVideo *pSrc, FrMediaStream* pDst);
BOOL VideoX264Destroy(FrVideoEncStruct* hEncoder);
BOOL VideoX264Flush(FrVideoEncStruct* hEncoder, FrMediaStream* pDst);
void VideoX264Close(FrVideoEncStruct* hEncoder);

LRSLT VideoNvencOpen(FrVideoEncStruct* hEncoder, FrVideoInfo* hInfo);
LRSLT VideoNvencInit(FrVideoEncStruct* hEncoder, FrVideoInfo* hInfo);
INT32 VideoNvencEncode(FrVideoEncStruct* hEncoder, FrRawVideo* pSrc, FrMediaStream* pDst);
BOOL VideoNvencDestroy(FrVideoEncStruct* hEncoder);
BOOL VideoNvencFlush(FrVideoEncStruct* hEncoder, FrMediaStream* pDst);
void VideoNvencClose(FrVideoEncStruct* hEncoder);

//LRSLT VideoFFEncOpen(FrVideoEncStruct* hEncoder, FrVideoInfo* hInfo);
//LRSLT VideoFFEncInit(FrVideoEncStruct* hEncoder, FrVideoInfo* hInfo);
//INT32 VideoFFEncEncode(FrVideoEncStruct* hEncoder, FrRawVideo* pSrc, FrMediaStream* pDst);
//BOOL VideoFFEncDestroy(FrVideoEncStruct* hEncoder);
//BOOL VideoFFEncFlush(FrVideoEncStruct* hEncoder, FrMediaStream* pDst);
//void VideoFFEncClose(FrVideoEncStruct* hEncoder);


#ifdef __cplusplus
}
#endif

#endif // _VIDEOENCODER_H_

