
#include "VideoDecoder.h"
#include "DecoderAPI.h"
#include "TraceAPI.h"


BOOL McVideoDecGetInfo(BYTE* pBuffer, DWORD32 dwBuffLen, FrVideoInfo* hInfo) {
	//switch(hInfo->dwFourCC)
	//{
	//case FOURCC_AVC1:		// baseline profile
	//	GetH264HeaderInfo(pBuffer, dwBuffLen, hInfo, FALSE);
	//	break;
	//case FOURCC_h264:		// main profile
	//case FOURCC_H264:
	//case FOURCC_OMX264:
	//case FOURCC_CUDA:
	//case FOURCC_INTEL:
	//	GetH264HeaderInfo(pBuffer, dwBuffLen, hInfo, TYPE_TS);
	//	break;
	//case FOURCC_H265:
	//case FOURCC_HEVC:
	//case FOURCC_OMX265:
	//	GetH265HeaderInfo(pBuffer, dwBuffLen, hInfo, TYPE_TS);
	//	break;
	//default:
	//	return TRUE;
	//}

	return	TRUE;
}

LRSLT VideoDecInit(void* hHandle, FrMediaStream* pData, FrVideoInfo* hInfo) {
	FrVideoDecStruct* hDecoder = (FrVideoDecStruct*)hHandle;
	LRSLT lRet;

	switch (hDecoder->m_dwFourCC) {
	case FOURCC_AVC1:
	case FOURCC_H264:
	case FOURCC_h264:
		if (hDecoder->m_eCodecType == CODEC_HW) {
			lRet = VideoNvdecOpen(hDecoder, pData, hInfo);
			if (FRFAILED(lRet)) {
				//McVideoDecClose(hDecoder);
				return    lRet;
			}
		}
		else if (hDecoder->m_eCodecType == CODEC_HW_QSV) {
			lRet = VideoQsvOpen(hDecoder, pData, hInfo);
			if (FRFAILED(lRet)) {
				//McVideoDecClose(hDecoder);
				return    lRet;
			}
		}
		else if (hDecoder->m_eCodecType == CODEC_SW){
			lRet = VideoFFInit(hDecoder, pData, hInfo);
			if (FRFAILED(lRet)) {
				//McVideoDecClose(hDecoder);
				return    lRet;
			}
		}
		else if (hDecoder->m_eCodecType == CODEC_SW_ESM) {
			lRet = VideoEsmFFInit(hDecoder, pData, hInfo);
			if (FRFAILED(lRet)) {
				//McVideoDecClose(hDecoder);
				return    lRet;
			}
		}
		
		break;
	case FOURCC_H265:
	case FOURCC_HEVC:
		break;
	default:
		break;
	}

	return FR_OK;
}
void VideoDecClose(void* hHandle) {
	FrVideoDecStruct* hDecoder = (FrVideoDecStruct*)hHandle;
	if (hDecoder) {
		switch (hDecoder->m_dwFourCC) {
		case FOURCC_AVC1:
		case FOURCC_H264:
		case FOURCC_h264:
			if (hDecoder->m_eCodecType == CODEC_HW) {
				VideoNvdecClose(hDecoder);
			}
			else if (hDecoder->m_eCodecType == CODEC_HW_QSV) {
				VideoQsvClose(hDecoder);
			}
			else if (hDecoder->m_eCodecType == CODEC_SW){
				VideoFFClose(hDecoder);
			}
			else if (hDecoder->m_eCodecType == CODEC_SW_ESM) {
				VideoEsmFFClose(hDecoder);
			}
			break;		
		case FOURCC_H265:			
		case FOURCC_HEVC:
			break;
		default:
			break;
		}
	}
}

LRSLT VideoDecoding(void* hHandle, FrMediaStream* pData, FrRawVideo* pMedia) {
	FrVideoDecStruct* hDecoder = (FrVideoDecStruct*)hHandle;
	//LRSLT lRet;
	int ret;

	switch (hDecoder->m_dwFourCC) {
	case FOURCC_AVC1:
	case FOURCC_H264:
	case FOURCC_h264:
		if (hDecoder->m_eCodecType == CODEC_HW) {
			ret = VideoNvdecDecode(hDecoder, pData, pMedia);
			if (ret < 1) {
				// ret 0 is need more data..
				return FR_FAIL;
			}
		}
		else if (hDecoder->m_eCodecType == CODEC_HW_QSV) {
			ret = VideoQsvDecode(hDecoder, pData, pMedia);
			if (ret < 1) {
				// ret 0 is need more data..
				return FR_FAIL;
			}
		}
		else if (hDecoder->m_eCodecType == CODEC_SW) {
			ret = VideoFFDecode(hDecoder, pData, pMedia);
			if (ret != 1)
				return FR_FAIL;
		}
		else if (hDecoder->m_eCodecType == CODEC_SW_ESM) {
			ret = VideoEsmFFDecode(hDecoder, pData, pMedia);
			if (ret != 1)
				return FR_FAIL;
		}
		break;
	case FOURCC_H265:
	case FOURCC_HEVC:
		break;
	default:
		break;
	}

	return FR_OK;
}

LRSLT FrVideoDecOpen(void** phDecoder, FrMediaStream *pData, FrVideoInfo *hInfo) {
	FrVideoDecStruct*	hDecoder;
	LRSLT				lRet = FR_FAIL;

	LOG_I("FrVideoDecOpen Begin..");

	hDecoder = (FrVideoDecStruct*)MALLOCZ(sizeof(FrVideoDecStruct));
	if (hDecoder) {
		hDecoder->m_bReset = TRUE;
		hDecoder->m_dwFourCC = hInfo->dwFourCC;
		hDecoder->m_eCodecType = hInfo->eCodecType;
		hDecoder->m_dwWidth = hInfo->dwWidth;
		hDecoder->m_dwHeight = hInfo->dwHeight;
		hDecoder->m_memDir = hInfo->memDir;

		hDecoder->m_dwTotalTick = 0;
		hDecoder->m_dwTotalCnt = 0;

	    lRet = VideoDecInit(hDecoder, pData, hInfo);
		if (FRFAILED(lRet))	{
			FrVideoDecClose(hDecoder);
			return lRet;
		}

		hDecoder->m_pHostBuffer = (BYTE *)MALLOC(hInfo->dwWidth * hInfo->dwHeight * 4);	// enough size
		//LOG_I("[DecVideo] McVideoDecOpen - Dump flag (%d)..", hInfo->m_bWriteDump);
		//hDecoder->m_bDump = hInfo->m_bWriteDump;
			
#ifdef	VIDEO_DUMP
			hDecoder->m_hFile = (FILE_HANDLE)FILEDUMPOPEN("video_dec", "dmp", (DWORD32)hDecoder);
			if (hInfo->m_dwConfig) {
				LOG_I("[DecVideo] McVideoDecOpen - Config Length (%d)", hInfo->m_dwConfig);
				FrWriteFile(hDecoder->m_hFile, (char*)&hInfo->m_dwConfig, 4);
				FrWriteFile(hDecoder->m_hFile, (char*)hInfo->m_pConfig, hInfo->m_dwConfig);
			}
#endif

#ifdef	VIDEO_YUV_DUMP
		hDecoder->m_hYUVFile = (FILE_HANDLE)FILEDUMPOPEN("video_dec_conv", "yuv", (DWORD32)hDecoder);
#endif

		*phDecoder = hDecoder;

		LOG_I("FrVideoDecOpen handle(0x%x) End..", hDecoder);
	}
	else
		return	COMMON_ERR_MEM;

	return	lRet;
}

void FrVideoDecClose(void* hHandle) {
	FrVideoDecStruct*	hDecoder = (FrVideoDecStruct*)hHandle;

	LOG_I("FrVideoDecClose() Begin (0x%x)", hDecoder);
	LOG_D("FrVideoDecClose() average Tick=%d, total tick(%d), total frame(%d)", hDecoder->m_dwTotalTick/hDecoder->m_dwTotalCnt, hDecoder->m_dwTotalTick, hDecoder->m_dwTotalCnt);

	if (hDecoder) {
		VideoDecClose(hDecoder);

		FREE(hDecoder->m_pHostBuffer);
		hDecoder->m_pHostBuffer = NULL;

		//FREE(hDecoder->m_pFrameBuffer);
		//hDecoder->m_pFrameBuffer = NULL;
		//FREE(hDecoder->m_pYUVBuffer);
		//hDecoder->m_pYUVBuffer = NULL;
		
#ifdef VIDEO_DECODER_DUMP
		if (hDecoder->m_bDump == TRUE)
			McCloseFile(hDecoder->m_hDumpFile);
#endif
        
#ifdef	VIDEO_DUMP
		McCloseFile(hDecoder->m_hFile);
#endif
#ifdef	VIDEO_YUV_DUMP
		McCloseFile(hDecoder->m_hYUVFile);
#endif

		FREE(hDecoder);
	}

	LOG_I("FrVideoDecClose End");
}



LRSLT FrVideoDecDecode(void* hHandle, FrMediaStream* pData, FrRawVideo* pMedia) {
	FrVideoDecStruct*	hDecoder = (FrVideoDecStruct*)hHandle;
	//FrRawVideo		Media = {0, };
	//DWORD32				dwTick, dwTick2;
	LRSLT lRet;
    
	if (!hDecoder)
		return FALSE;

    hDecoder->m_lError = FR_OK;
    
	//dwTick = FrGetTickCount();
	auto start = std::chrono::system_clock::now();

	lRet = VideoDecoding(hDecoder, pData, pMedia);
	if (FRFAILED(lRet))
		return lRet;
	
	auto end = std::chrono::system_clock::now();
	std::chrono::milliseconds delta = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	hDecoder->m_dwTotalCnt += pMedia->nDecoded;
	hDecoder->m_dwTotalTick += delta.count();
	LOG_D("DecDecode CTS=%d chrono Tick=%d", pMedia->dwCTS, delta.count());

#ifdef	VIDEO_YUV_DUMP
// 	McWriteFile(hDecoder->m_hYUVFile, (char*)Media.m_pY, hDecoder->m_dwSrcWidth * hDecoder->m_dwSrcHeight);
// 	McWriteFile(hDecoder->m_hYUVFile, (char*)Media.m_pU, hDecoder->m_dwSrcWidth * hDecoder->m_dwSrcHeight / 4);
// 	McWriteFile(hDecoder->m_hYUVFile, (char*)Media.m_pV, hDecoder->m_dwSrcWidth * hDecoder->m_dwSrcHeight / 4);
#endif

	/*dwTick2 = McGetTickCount();
	hDecoder->m_dwDecTime += (dwTick2 - dwTick);
	dwTick = dwTick2;
	*/

	return	FR_OK;
}


BOOL FrVideoDecReset(void* hHandle) {
	FrVideoDecStruct*	hDecoder = (FrVideoDecStruct*)hHandle;

	if(!hDecoder)
		return FALSE;

	hDecoder->m_bReset = TRUE;
	//hDecoder->m_dwDstFrameNum = 0;
	//hDecoder->m_dwDecCnt = 0;
	/*hDecoder->m_bMergeCaption = FALSE;
	hDecoder->m_dwLastSrcCTS = hDecoder->m_dwLastDecodedCTS = (DWORD32)-1;*/
	FrVideoDecFlush(hDecoder);				// Set 4 frames to be skipped & in some decoders, flush the bitstream buffer

	return	TRUE;
}


void FrVideoDecFlush(void* hHandle) {
	FrVideoDecStruct*	hDecoder = (FrVideoDecStruct*)hHandle;

	if (!hDecoder)
		return;

	/*if ((hDecoder->m_dwFourCC != FOURCC_OMX264) && (hDecoder->m_dwFourCC != FOURCC_OMX265))
		FFMpegVideoDecoderFlush(hDecoder->m_hDecoder);*/
}
