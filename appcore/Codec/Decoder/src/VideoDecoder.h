
#ifndef	_VIDEODECODER_H_
#define	_VIDEODECODER_H_

#include "SystemAPI.h"
#include "TraceAPI.h"
#include "mediainfo.h"
#include "mediafourcc.h"
#include "mediaerror.h"
#include "UtilAPI.h"


#ifdef _DEBUG
//#define	VIDEO_DUMP
//#define	VIDEO_YUV_DUMP
#endif

// CPU
#include "FFCodecAPI.h"

// GPU
#include "ESMNvdec.h"
#include <chrono>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include "ESMFFQSVdec.h"
#include "ESMFFDecoder.h"

#define	TYPE_RTP	0
#define	TYPE_TS		1

typedef struct {
	// Handle
	void*				m_hDecoder;
	DWORD32				m_dwWidth;
	DWORD32				m_dwHeight;
	
    LRSLT               m_lError;
	DWORD32				m_dwFourCC;			// Decoder Type
	FrCodecType			m_eCodecType;
	
	BOOL				m_bReset;
	BOOL				m_bLowDelayMode;
	BYTE*				m_pHostBuffer;

	// nvidia
	ESMNvdec* m_pDec;
	ESMNvdec::CONTEXT_T m_DecCtx;
	unsigned char** m_ppDecoded;
	int m_nDecoded;
	int m_nRemainDecoded;
	long long* m_pDecodedUids;
	int			m_memDir;

	// intel
	ESMFFQsvdec* m_pQsv;
	ESMFFQsvdec::CONTEXT_T m_QsvCtx;

	// esm ff
	ESMFFDecoder* m_pFF;
	ESMFFDecoder::CONTEXT_T m_FFCtx;
	
	// debug
	DWORD32 m_dwTotalTick;
	DWORD32 m_dwTotalCnt;
	


#ifdef	VIDEO_DUMP
	FILE_HANDLE			m_hFile;
#endif
#ifdef VIDEO_YUV_DUMP
	FILE_HANDLE			m_hYUVFile;
#endif

} FrVideoDecStruct;

DWORD32 vlc_Golomb(McBitHandle hBit);
BOOL GetH264HeaderInfo(BYTE* pBuffer, DWORD32 dwLen, FrVideoInfo* hInfo, DWORD32 dwType);
BOOL GetH265HeaderInfo(BYTE* pBuffer, DWORD32 dwLen, FrVideoInfo* hInfo, DWORD32 dwType);


#ifdef __cplusplus
extern "C"
{
#endif

// decoder
LRSLT VideoFFInit(FrVideoDecStruct* hDecoder, FrMediaStream* pSrc, FrVideoInfo* hInfo);
void VideoFFClose(FrVideoDecStruct* hDecoder);
INT32 VideoFFDecode(FrVideoDecStruct* hDecoder, FrMediaStream* pSrc, FrRawVideo* pDst);

LRSLT VideoEsmFFInit(FrVideoDecStruct* hDecoder, FrMediaStream* pSrc, FrVideoInfo* hInfo);
void VideoEsmFFClose(FrVideoDecStruct* hDecoder);
INT32 VideoEsmFFDecode(FrVideoDecStruct* hDecoder, FrMediaStream* pSrc, FrRawVideo* pDst);

// nvidia
LRSLT VideoNvdecOpen(FrVideoDecStruct* hDecoder, FrMediaStream* pSrc, FrVideoInfo* hInfo);
void VideoNvdecClose(FrVideoDecStruct* hDecoder);
int VideoNvdecDecode(FrVideoDecStruct* hDecoder, FrMediaStream* pSrc, FrRawVideo* pDst);
//LRSLT VideoNvDecInit();
//void VideoNvDecClose();
//BOOL VideoNvDecDecode();

// qsv
LRSLT VideoQsvOpen(FrVideoDecStruct* hDecoder, FrMediaStream* pSrc, FrVideoInfo* hInfo);
void VideoQsvClose(FrVideoDecStruct* hDecoder);
int VideoQsvDecode(FrVideoDecStruct* hDecoder, FrMediaStream* pSrc, FrRawVideo* pDst);

#ifdef __cplusplus
}
#endif


#endif	// _VIDEODECODER_H_
