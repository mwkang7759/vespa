#include "VideoDecoder.h"

LRSLT VideoFFInit(FrVideoDecStruct* hDecoder, FrMediaStream *pData, FrVideoInfo *hInfo) {
	LRSLT					lRet = FR_OK;
	VideoOutFormat			eVideoFormat = VideoFmtUnknown;

	hDecoder->m_hDecoder = FFVideoDecOpen(
		hDecoder->m_dwFourCC,
		hInfo->pConfig,
		hInfo->dwConfig,
		hInfo->dwHeight,
		hInfo->dwWidth,
		hInfo->dwBitCount,
		&eVideoFormat,
		TIMESTAMP_PTS,
		hInfo);

	if (!hDecoder->m_hDecoder) {
		//FrVideoDecClose(hDecoder);
		TRACEX(FDR_LOG_ERROR, "VideoFFInit - FFVideoDecOpen failed! (%s)", hInfo->szCodec);
		return	COMMON_ERR_VIDEODEC;
	}
    else
        TRACEX(FDR_LOG_INFO, "VideoFFInit - FFVideoDecOpen Succeed! (%s)", hInfo->szCodec);

	hDecoder->m_bLowDelayMode = hInfo->bLowDelayDecoding;
	TRACEX(FDR_LOG_INFO, "VideoFFInit- %s FFMPEG %d %d", hInfo->szCodec, hInfo->dwWidth, hInfo->dwHeight);

	return lRet;
}

void VideoFFClose(FrVideoDecStruct* hDecoder) {
	if (hDecoder->m_hDecoder) {
		FFVideoDecClose(hDecoder->m_hDecoder);
		hDecoder->m_hDecoder = NULL;
	}
}
INT32 VideoFFDecode(FrVideoDecStruct* hDecoder, FrMediaStream* pSrc, FrRawVideo* pDst) {
	INT32	ret = 0;

	PsVideoCodecOutput*		pOut = NULL;

	if (pSrc) {
		pDst->dwCTS = pSrc->dwCTS;
		//hDecoder->m_dwLastSrcCTS = pSrc->m_dwCTS;

		ret = FFVideoDecDecode(hDecoder->m_hDecoder, pSrc->pFrame, pSrc->dwFrameLen[0],
			pSrc->dwCTS, 0, TIMESTAMP_PTS, &pOut);
	}
	else {
		BOOL bStartFlush;
		if (!hDecoder->m_nRemainDecoded) {
			hDecoder->m_nRemainDecoded = 1;
			bStartFlush = TRUE;
		} else
			bStartFlush = FALSE;
			
		ret = FFVideoDecGetBufferedFrame(hDecoder->m_hDecoder, &pOut, bStartFlush);
	}
	
	if (ret ==1) {
		pDst->pY = pOut->y;
		pDst->pU = pOut->u;
		pDst->pV = pOut->v;
		pDst->dwPitch = pOut->y_pitch;
		pDst->dwCTS = pOut->cts;
		//hDecoder->m_dwLastDecodedCTS = pOut->cts;
		//pDst->dwSrcCnt = 0;
		pDst->dwSrcCnt = 1;
		pDst->nDecoded = 1;

		if (pOut->eVideoFormat == VideoFmtNV12)
			pDst->eColorFormat = VideoFmtNV12;
		else if (pOut->eVideoFormat == VideoFmtQSV)
		{
			pDst->eColorFormat = VideoFmtQSV;
			//pDst->pDevPtr = pOut->surface;
		}
		else
		{
			pDst->eColorFormat = pOut->eVideoFormat;
			//pDst->pDevPtr = NULL;
		}
		LOG_D("VideoFFDecode bRet = %d, Out pitch = %d, CTS = %d", ret, pOut->y_pitch, pDst->dwCTS);
	}
	else {
		pDst->dwSrcCnt = 0;
		ret = FALSE;
	}

	return ret;
}

LRSLT VideoEsmFFInit(FrVideoDecStruct* hDecoder, FrMediaStream* pData, FrVideoInfo* hInfo) {
	LRSLT					lRet = FR_OK;
	VideoOutFormat			eVideoFormat = VideoFmtUnknown;

	hDecoder->m_pFF = new ESMFFDecoder();
	
	hDecoder->m_FFCtx.codec = ESMFFDecoder::VIDEO_CODEC_T::AVC;
	hDecoder->m_FFCtx.extradata_size = hInfo->dwConfig;
	hDecoder->m_FFCtx.extradata = (uint8_t*)malloc(hInfo->dwConfig);
	::memmove(hDecoder->m_FFCtx.extradata, hInfo->pConfig, hInfo->dwConfig);
	hDecoder->m_FFCtx.height = hInfo->dwHeight;
	hDecoder->m_FFCtx.width = hInfo->dwWidth;
	//_uyvy_size = hDecoder->m_FFCtx.height * hDecoder->m_FFCtx.width * 2;
	hDecoder->m_pFF->Initialize(&hDecoder->m_FFCtx);
	

	hDecoder->m_bLowDelayMode = hInfo->bLowDelayDecoding;
	TRACEX(FDR_LOG_INFO, "VideoFFInit- %s FFMPEG %d %d", hInfo->szCodec, hInfo->dwWidth, hInfo->dwHeight);

	return lRet;
}

void VideoEsmFFClose(FrVideoDecStruct* hDecoder) {
	if (hDecoder->m_pFF) {
		hDecoder->m_pFF->Release();
		
		delete hDecoder->m_pFF;
	}
}
INT32 VideoEsmFFDecode(FrVideoDecStruct* hDecoder, FrMediaStream* pSrc, FrRawVideo* pDst) {
	int ret;
	int nDecoded = -1;
	unsigned char** ppDecoded = NULL;

	if (pSrc) {
		LOG_D("VideoEsmFFDecode Src: frame len(%d), cts(%d), type(%d)", pSrc->dwFrameLen[0], pSrc->dwCTS, pSrc->tFrameType[0]);
		//ret = hDecoder->m_pFF->Decode(pSrc->pFrame, pSrc->dwFrameLen[0], &pDst->ppDecoded, &pDst->nDecoded, FALSE, pSrc->dwCTS);
		
		ESMFFDecoder::INPUT_STREAM src{};
		ESMFFDecoder::OUTPUT_RAW dst{};
		src.bitstream = pSrc->pFrame;
		src.bitstreamSize = pSrc->dwFrameLen[0];
		src.cts = pSrc->dwCTS;
		ret = hDecoder->m_pFF->Decode(&src, &dst);


		if (ret != ESMBase::ERR_CODE_T::SUCCESS) {
			return -1;
		}

		if (dst.numberOfDecoded) {
			pDst->nDecoded = dst.numberOfDecoded;
			pDst->dwCTS = dst.cts;
			pDst->dwPitch = hDecoder->m_FFCtx.width;
			pDst->ppDecoded = dst.decoded;
			pDst->pY = pDst->ppDecoded[0];
			pDst->dwSrcCnt = pDst->nDecoded;
			pDst->dwDecodedWidth = hDecoder->m_FFCtx.width;
			pDst->dwDecodedHeight = hDecoder->m_FFCtx.height;

			LOG_D("VideoEsmFFDecode Dst: cts(%d), decoded num(%d), pitch(%d)", pDst->dwCTS, pDst->nDecoded, pDst->dwPitch);
		}
		else {
			pDst->dwSrcCnt = 0;
			LOG_D("VideoEsmFFDecode Dst: no decoded frame, decoded num(%d)", pDst->nDecoded);
		}

		//if (pDst->nDecoded) {
		//	pDst->dwCTS = pSrc->dwCTS;
		//	pDst->dwPitch = hDecoder->m_FFCtx.width;
		//	pDst->pY = pDst->ppDecoded[0];
		//	pDst->dwSrcCnt = pDst->nDecoded;
		//	pDst->dwDecodedWidth = hDecoder->m_FFCtx.width;
		//	pDst->dwDecodedHeight = hDecoder->m_FFCtx.height;

		//	/*if (pDst->nDecoded > 1)
		//		pDst->dwSrcCnt = pDst->nDecoded;*/

		//	LOG_D("VideoEsmFFDecode Dst: cts(%d), decoded num(%d), pitch(%d)", pDst->dwCTS, pDst->nDecoded, pDst->dwPitch);
		//}
		//else {
		//	pDst->dwSrcCnt = 0;
		//	LOG_D("VideoEsmFFDecode Dst: no decoded frame, decoded num(%d)", pDst->nDecoded);
		//}
	}
	else {
		LOG_D("VideoEsmFFDecode Src: decoder to flush video, m_nDecoded(%d)...", hDecoder->m_nDecoded);
		if (!hDecoder->m_nDecoded) {
			ret = hDecoder->m_pFF->Decode(NULL, 0, &hDecoder->m_ppDecoded, &hDecoder->m_nDecoded, FALSE, 0);
			if (ret != ESMBase::ERR_CODE_T::SUCCESS) {
				return -1;
			}
			hDecoder->m_nRemainDecoded = hDecoder->m_nDecoded;
		}
		int flushIdx = hDecoder->m_nRemainDecoded - hDecoder->m_nDecoded;
		if (flushIdx >= hDecoder->m_nRemainDecoded) {
			LOG_D("VideoEsmFFDecode Dst: decoder to flush video done..flush idx(%d)..remain(%d)", flushIdx, hDecoder->m_nRemainDecoded);
			return -1;
		}
		//pDst->dwCTS = hDecoder->m_pDecodedUids[flushIdx];
		pDst->dwPitch = hDecoder->m_FFCtx.width;
		pDst->pY = hDecoder->m_ppDecoded[flushIdx];
		pDst->dwSrcCnt = 1;

		LOG_D("VideoEsmFFDecode Dst: decoder to flush video flush idx(%d), dec num(%d), remain(%d), cts(%d)", flushIdx, hDecoder->m_nDecoded, hDecoder->m_nRemainDecoded, pDst->dwCTS);
		hDecoder->m_nDecoded--;
	}

	return pDst->dwSrcCnt;
}
