#include "VideoDecoder.h"
#include "DecoderAPI.h"


LRSLT VideoQsvOpen(FrVideoDecStruct* hDecoder, FrMediaStream* pStream, FrVideoInfo* hInfo) {
	LOG_I("QsvOpen Begin..w(%d), h(%d)", hInfo->dwWidth, hInfo->dwHeight);

	if (hDecoder->m_pQsv == nullptr) {
		hDecoder->m_pQsv = new ESMFFQsvdec();
		//hDecoder->m_QsvCtx.deviceIndex = 0;
		hDecoder->m_QsvCtx.codec = ESMBase::VIDEO_CODEC_T::AVC;
		
		hDecoder->m_QsvCtx.width = hInfo->dwWidth;
		hDecoder->m_QsvCtx.height = hInfo->dwHeight;
		/*switch (hInfo->eColorFormat) {
		case VideoFmtYUV420P:
		case VideoFmtIYUV:
			hDecoder->m_QsvCtx.colorspace = ESMBase::COLORSPACE_T::I420;
			break;
		case VideoFmtBGRA:
			hDecoder->m_DecCtx.colorspace = ESMBase::COLORSPACE_T::BGRA;
			break;
		case VideoFmtYV12:
			hDecoder->m_DecCtx.colorspace = ESMBase::COLORSPACE_T::YV12;
			break;
		case VideoFmtNV12:
			hDecoder->m_DecCtx.colorspace = ESMBase::COLORSPACE_T::NV12;
			break;
		default:
			hDecoder->m_DecCtx.colorspace = ESMBase::COLORSPACE_T::YV12;
			break;
		}*/

		if (hDecoder->m_pQsv->Initialize(&hDecoder->m_QsvCtx) != ESMBase::ERR_CODE_T::SUCCESS)
			return FR_FAIL;
	}

	LOG_I("QsvOpen() End..logo(%d)", hDecoder->m_QsvCtx.logo);
	return FR_OK;
}

void VideoQsvClose(FrVideoDecStruct* hDecoder) {
	LOG_I("QsvClose Begin..");

	if (hDecoder) {
		hDecoder->m_pQsv->Release();
		delete hDecoder->m_pQsv;
		hDecoder->m_pQsv = nullptr;
	}

	LOG_I("QsvClose End..");
}

int VideoQsvDecode(FrVideoDecStruct* hDecoder, FrMediaStream* pSrc, FrRawVideo* pDst) {
	int ret;

	if (hDecoder->m_pQsv == nullptr)
		return -1;

	//LOG_D("[DecVideo] VideoNvdecDecode() Begin..");

	if (pSrc) {
		LOG_D("QsvDecode Src: frame len(%d), cts(%d), type(%d)", pSrc->dwFrameLen[0], pSrc->dwCTS, pSrc->tFrameType[0]);
		ret = hDecoder->m_pQsv->Decode(pSrc->pFrame, pSrc->dwFrameLen[0], &pDst->ppDecoded, &pDst->nDecoded, FALSE);
		if (ret != ESMBase::ERR_CODE_T::SUCCESS) {
			return -1;
		}

		if (pDst->nDecoded) {			
			pDst->dwCTS = pDst->pDecodedUids[0];
			//pDst->dwPitch = hDecoder->m_pQsv->GetPitch2();
			pDst->pY = pDst->ppDecoded[0];
			
			pDst->dwSrcCnt = pDst->nDecoded;
			pDst->dwDecodedWidth = hDecoder->m_QsvCtx.width;
			pDst->dwDecodedHeight = hDecoder->m_QsvCtx.height;

			/*if (pDst->nDecoded > 1)
				pDst->dwSrcCnt = pDst->nDecoded;*/

			LOG_D("QsvDecode Dst: cts(%d), decoded num(%d), pitch(%d)", pDst->pDecodedUids[0], pDst->nDecoded, pDst->dwPitch);
		}
		else {
			pDst->dwSrcCnt = 0;
			LOG_D("QsvDecode Dst: no decoded frame, decoded num(%d)", pDst->nDecoded);
		}
	}
	else {
		LOG_D("QsvDecode Src: decoder to flush video, m_nDecoded(%d)...", hDecoder->m_nDecoded);
		if (!hDecoder->m_nDecoded) {
			ret = hDecoder->m_pQsv->Decode(NULL, 0, &hDecoder->m_ppDecoded, &hDecoder->m_nDecoded, FALSE);
			if (ret != ESMBase::ERR_CODE_T::SUCCESS) {
				return -1;
			}
			hDecoder->m_nRemainDecoded = hDecoder->m_nDecoded;
		}
		int flushIdx = hDecoder->m_nRemainDecoded - hDecoder->m_nDecoded;
		if (flushIdx >= hDecoder->m_nRemainDecoded) {
			LOG_D("QsvDecode Dst: decoder to flush video done..flush idx(%d)..remain(%d)", flushIdx, hDecoder->m_nRemainDecoded);
			return -1;
		}
		pDst->dwCTS = hDecoder->m_pDecodedUids[flushIdx];
		//pDst->dwPitch = hDecoder->m_pQsv->GetPitch2();
		pDst->pY = hDecoder->m_ppDecoded[flushIdx];
		pDst->dwSrcCnt = 1;

		LOG_D("QsvDecode Dst: decoder to flush video flush idx(%d), dec num(%d), remain(%d), cts(%d)", flushIdx, hDecoder->m_nDecoded, hDecoder->m_nRemainDecoded, pDst->dwCTS);
		hDecoder->m_nDecoded--;
	}

	//LOG_D("VideoNvdecDecode End..");
	if (pDst->dwSrcCnt && (hDecoder->m_memDir == MEM_CPU)) {
		auto start = std::chrono::system_clock::now();

		cudaError_t err = cudaMemcpy(hDecoder->m_pHostBuffer, pDst->pY, pDst->dwPitch * hDecoder->m_dwHeight * 3 / 2, cudaMemcpyDeviceToHost);
		if (err != cudaSuccess) {
			return -1;
		}

		auto end = std::chrono::system_clock::now();
		std::chrono::milliseconds delta = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
		LOG_D("QsvDecode cudaMemcpy() chrono Tick=%d", delta.count());

		
	}





	return pDst->dwSrcCnt;
}
