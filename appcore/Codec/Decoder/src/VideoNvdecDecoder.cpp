#include "VideoDecoder.h"
#include "DecoderAPI.h"


LRSLT VideoNvdecOpen(FrVideoDecStruct* hDecoder, FrMediaStream* pStream, FrVideoInfo* hInfo) {
	LOG_I("NvdecOpen Begin..w(%d), h(%d)", hInfo->dwWidth, hInfo->dwHeight);

	if (hDecoder->m_pDec == nullptr) {
		hDecoder->m_pDec = new ESMNvdec();
		hDecoder->m_DecCtx.deviceIndex = 0;
		hDecoder->m_DecCtx.codec = ESMBase::VIDEO_CODEC_T::AVC;
		// Nvdec을 이용해 scaling하는 것이 CUDA resizer보다 성능이 더 좋음
		hDecoder->m_DecCtx.width = hInfo->dwWidth;
		hDecoder->m_DecCtx.height = hInfo->dwHeight;
		switch (hInfo->eColorFormat) {
		case VideoFmtYUV420P:
		case VideoFmtIYUV:
			hDecoder->m_DecCtx.colorspace = ESMBase::COLORSPACE_T::I420;
			break;
		case VideoFmtBGRA:
			hDecoder->m_DecCtx.colorspace = ESMBase::COLORSPACE_T::BGRA;
			break;
		case VideoFmtYV12:
			hDecoder->m_DecCtx.colorspace = ESMBase::COLORSPACE_T::YV12;
			break;
		case VideoFmtNV12:
			hDecoder->m_DecCtx.colorspace = ESMBase::COLORSPACE_T::NV12;
			break;
		default:
			hDecoder->m_DecCtx.colorspace = ESMBase::COLORSPACE_T::YV12;
			break;
		}
		
		if (hDecoder->m_pDec->Initialize(&hDecoder->m_DecCtx) != ESMBase::ERR_CODE_T::SUCCESS)
			return FR_FAIL;
	}

	LOG_I("NvdecOpen() End..colorspace(%d)", hDecoder->m_DecCtx.colorspace);
	return FR_OK;
}

void VideoNvdecClose(FrVideoDecStruct* hDecoder) {
	LOG_I("NvdecClose Begin..");

	if (hDecoder) {
		hDecoder->m_pDec->Release();
		delete hDecoder->m_pDec;
		hDecoder->m_pDec = nullptr;
	}

	LOG_I("NvdecClose End..");
}

int VideoNvdecDecode(FrVideoDecStruct* hDecoder, FrMediaStream *pSrc, FrRawVideo* pDst) {	
	int ret;

	if (hDecoder->m_pDec == nullptr)
		return -1;

	//LOG_D("[DecVideo] VideoNvdecDecode() Begin..");
	
	if (pSrc) {
		LOG_D("NvdecDecode Src: frame len(%d), cts(%d), type(%d)", pSrc->dwFrameLen[0], pSrc->dwCTS, pSrc->tFrameType[0]);
		ret = hDecoder->m_pDec->Decode(pSrc->pFrame, pSrc->dwFrameLen[0], pSrc->dwCTS, &pDst->ppDecoded, &pDst->nDecoded, (long long**)&pDst->pDecodedUids, NULL, NULL);
		if (ret != ESMBase::ERR_CODE_T::SUCCESS) {
			return -1;
		}

		if (pDst->nDecoded) {
			//pDst->dwCTS = pDecodedUids[0];
			pDst->dwCTS = pDst->pDecodedUids[0];
			pDst->dwPitch = hDecoder->m_pDec->GetPitch2();
			//pDst->pY = ppDecoded[0];
			pDst->pY = pDst->ppDecoded[0];
			//pDst->dwSrcCnt = 1;
			pDst->dwSrcCnt = pDst->nDecoded;
			pDst->dwDecodedWidth = hDecoder->m_DecCtx.width;
			pDst->dwDecodedHeight = hDecoder->m_DecCtx.height;

			/*if (pDst->nDecoded > 1)
				pDst->dwSrcCnt = pDst->nDecoded;*/
			
			LOG_D("NvdecDecode Dst: cts(%d), decoded num(%d), pitch(%d)", pDst->pDecodedUids[0], pDst->nDecoded, pDst->dwPitch);
		}
		else {
			pDst->dwSrcCnt = 0;
			LOG_D("NvdecDecode Dst: no decoded frame, decoded num(%d)", pDst->nDecoded);
		}
	}
	else {
		LOG_D("NvdecDecode Src: decoder to flush video, m_nDecoded(%d)...", hDecoder->m_nDecoded);
		if (!hDecoder->m_nDecoded) {
			ret = hDecoder->m_pDec->Decode(NULL, 0, 0, &hDecoder->m_ppDecoded, &hDecoder->m_nDecoded, &hDecoder->m_pDecodedUids, NULL, NULL);
			if (ret != ESMBase::ERR_CODE_T::SUCCESS) {
				return -1;
			}
			hDecoder->m_nRemainDecoded = hDecoder->m_nDecoded;
		}
		int flushIdx = hDecoder->m_nRemainDecoded - hDecoder->m_nDecoded;
		if (flushIdx >= hDecoder->m_nRemainDecoded) {
			LOG_D("NvdecDecode Dst: decoder to flush video done..flush idx(%d)..remain(%d)", flushIdx, hDecoder->m_nRemainDecoded);
			return -1;
		}		
		pDst->dwCTS = hDecoder->m_pDecodedUids[flushIdx];
		pDst->dwPitch = hDecoder->m_pDec->GetPitch2();
		pDst->pY = hDecoder->m_ppDecoded[flushIdx];
		pDst->dwSrcCnt = 1;
		
		LOG_D("NvdecDecode Dst: decoder to flush video flush idx(%d), dec num(%d), remain(%d), cts(%d)", flushIdx, hDecoder->m_nDecoded, hDecoder->m_nRemainDecoded, pDst->dwCTS);
		hDecoder->m_nDecoded--;
	}
	
	//LOG_D("VideoNvdecDecode End..");
	if (pDst->dwSrcCnt && (hDecoder->m_memDir == MEM_CPU)) {
		auto start = std::chrono::system_clock::now();

		cudaError_t err = cudaMemcpy(hDecoder->m_pHostBuffer, pDst->pY, pDst->dwPitch * hDecoder->m_dwHeight * 3 / 2, cudaMemcpyDeviceToHost);
		if (err != cudaSuccess) {
			return -1;
		}

		auto end = std::chrono::system_clock::now();
		std::chrono::milliseconds delta = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
		LOG_D("NvdecDecode cudaMemcpy() chrono Tick=%d", delta.count());

		//static FILE* fp2 = NULL;
		//if (!fp2) {
		//	fp2 = fopen("c:\\cuda_copy_pitch.raw", "wb");
		//	if (fp2) {
		//		BYTE* pos = hDecoder->m_pHostBuffer;
		//		BYTE* pos_v = hDecoder->m_pHostBuffer + pDst->dwPitch * hDecoder->m_dwHeight;
		//		BYTE* pos_u = pos_v + pDst->dwPitch * hDecoder->m_dwHeight / 4;
		//		
		//		// Copy Y
		//		int i;
		//		for (i = 0; i < hDecoder->m_dwHeight; i++) {
		//			fwrite(pos, 1, hDecoder->m_dwWidth, fp2);
		//			pos += pDst->dwPitch;
		//		}

		//		pos = pos_v;
		//		for (i = 0; i < hDecoder->m_dwHeight / 2; i++) {
		//			fwrite(pos, 1, hDecoder->m_dwWidth / 2, fp2);
		//			pos += pDst->dwPitch / 2;
		//		}

		//		pos = pos_u;
		//		for (i = 0; i < hDecoder->m_dwHeight/2; i++) {
		//			fwrite(pos, 1, hDecoder->m_dwWidth/2, fp2);
		//			pos += pDst->dwPitch/2;
		//		}
		//	}
		//}
	}


	
	
	
	return pDst->dwSrcCnt;
}
