﻿#pragma once

#if defined(EXPORT_ESM_FF_DECODER_LIB)
#  define EXP_ESM_FF_DECODER_CLASS __declspec(dllexport)
#else
#  define EXP_ESM_FF_DECODER_CLASS __declspec(dllimport)
#endif

#include <ESMBase.h>

class ESMFFDecoder : public ESMBase
{
	class Core;
public:
	typedef struct _CONTEXT_T
	{
		int32_t	codec;
		int32_t	width;
		int32_t	height;
		int32_t dstWidth;
		int32_t dstHeight;
		uint8_t* extradata;
		int32_t	extradata_size;
		char logo[MAX_PATH];
		_CONTEXT_T(void)
			: codec(ESMFFDecoder::VIDEO_CODEC_T::AVC)
			, width(1920)
			, height(1080)
			, dstWidth(1920)
			, dstHeight(1080)
			, extradata(nullptr)
			, extradata_size(0)
			, logo{}
		{
		}
	} CONTEXT_T;

	typedef struct _INPUT_STREAM {
		unsigned char* bitstream;
		int bitstreamSize;
		unsigned int cts;

	} INPUT_STREAM;

	typedef struct _OUTPUT_RAW {
		unsigned char** decoded;
		int numberOfDecoded;
		unsigned int cts;

	} OUTPUT_RAW;

	ESMFFDecoder(void);
	virtual ~ESMFFDecoder(void);

	BOOL	IsInitialized(void);
	
	int		Initialize(ESMFFDecoder::CONTEXT_T * context);
	int		Release(void);

	int		Decode(unsigned char* bitstream, int bitstreamSize, unsigned char*** decoded, int* numberOfDecoded, BOOL drawLogo, unsigned int cts);
	int		Decode(const INPUT_STREAM* pSrc, OUTPUT_RAW* pDsts);


private:
	ESMFFDecoder::Core * _Core;
};