﻿#pragma once

#include "ESMFFDecoder.h"
#include <cstdio>
#include <vector>
#include <memory>

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/opt.h>
#include <libavutil/mathematics.h>
#include <libavfilter/buffersrc.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/avfilter.h>
#include <libavutil/imgutils.h>
}
#include <ppl.h>
#include <concurrent_queue.h>

class ESMFFDecoder::Core
{
public:
	Core(void);
	~Core(void);

	BOOL	IsInitialized(void);

	int		Initialize(ESMFFDecoder::CONTEXT_T * context);
	int		Release(void);

	int		Decode(unsigned char* bitstream, int bitstreamSize, unsigned char*** decoded, int* numberOfDecoded, BOOL drawLogo, unsigned int cts);
	int		Decode(const INPUT_STREAM* pSrc, OUTPUT_RAW* pDst);

private:
	BOOL FileExists(LPSTR szPath);

private:
	BOOL _isInitialized;

	ESMFFDecoder::CONTEXT_T* _context;
	AVPacket* _av_packet;
	AVCodecContext* _av_codec_ctx;
	AVCodec* _av_codec;
	AVFrame* _yuv_frame;
	AVFrame* _rgb_frame;
	AVFrame* _uyvy_frame;
	
	int32_t _uyvy_buffer_size;
	uint8_t* _uyvy_buffer;
	int32_t _rgb_buffer_size;
	uint8_t* _rgb_buffer;

	int32_t _yuv_buffer_size;
	uint8_t* _yuv_buffer;

	SwsContext* _sws_yuv_ctx;
	SwsContext* _sws_rgb_ctx;
	SwsContext* _sws_yuv2uyvy_ctx;

	// resize
	AVFrame* _yuv_resize_frame;
	SwsContext* _sws_resize_yuv_ctx;
	//int32_t _yuv_buffer_size;
	//uint8_t* _yuv_buffer;

	std::vector<uint8_t*> _queue;

	BOOL _is_icon_loaded;

	//FILE* _fp_yuv;

	uint8_t* _logo;
	uint8_t* _alpha;
};