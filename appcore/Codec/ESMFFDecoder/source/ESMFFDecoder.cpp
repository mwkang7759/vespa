﻿#include "ESMFFDecoder.h"	
#include "ESMFFDecoderCore.h"

#if _DEBUG
#include<crtdbg.h>
#define new new(_NORMAL_BLOCK, __FILE__, __LINE__)
#define malloc(s) _malloc_dbg(s, _NORMAL_BLOCK, __FILE__, __LINE__)
#endif

ESMFFDecoder::ESMFFDecoder(void)
{
	_Core = new ESMFFDecoder::Core();
}

ESMFFDecoder::~ESMFFDecoder(void)
{
	if(_Core)
	{
		if(_Core->IsInitialized())
			_Core->Release();
		delete _Core;
		_Core = nullptr;
	}
}

BOOL ESMFFDecoder::IsInitialized(void)
{
	return _Core->IsInitialized();
}

int ESMFFDecoder::Initialize(ESMFFDecoder::CONTEXT_T * context)
{
	return _Core->Initialize(context);
}

int ESMFFDecoder::Release(void)
{
	return _Core->Release();
}

int	ESMFFDecoder::Decode(unsigned char* bitstream, int bitstreamSize, unsigned char*** decoded, int* numberOfDecoded, BOOL drawLogo, unsigned int cts)
{
	return _Core->Decode(bitstream, bitstreamSize, decoded, numberOfDecoded, drawLogo, cts);
}

int	ESMFFDecoder::Decode(const INPUT_STREAM* pSrc, OUTPUT_RAW* pDst)
{	
	return _Core->Decode(pSrc, pDst);
}