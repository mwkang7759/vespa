﻿#include "ESMFFDecoderCore.h"
#include <process.h>
#include "lodepng.h"
#include <Simd/SimdLib.h>

#include "TraceAPI.h"
//#pragma comment(lib, "avcodec.lib")
//#pragma comment(lib, "avformat.lib")
//#pragma comment(lib, "swscale.lib")
//#pragma comment(lib, "avutil.lib")


#if _DEBUG
#include<crtdbg.h>
#define new new(_NORMAL_BLOCK, __FILE__, __LINE__)
#define malloc(s) _malloc_dbg(s, _NORMAL_BLOCK, __FILE__, __LINE__)
#endif

ESMFFDecoder::Core::Core(void)
	: _context(nullptr)
	, _isInitialized(FALSE)
	, _uyvy_buffer_size(0)
	, _sws_yuv_ctx(nullptr)
	, _sws_rgb_ctx(nullptr)
	, _sws_yuv2uyvy_ctx(nullptr)
	, _uyvy_buffer(nullptr)
	, _rgb_buffer(nullptr)
	, _logo(nullptr)
	, _alpha(nullptr)
	, _is_icon_loaded(FALSE)
{


}

ESMFFDecoder::Core::~Core(void)
{

}

BOOL ESMFFDecoder::Core::IsInitialized(void)
{
	return _isInitialized;
}

int ESMFFDecoder::Core::Initialize(ESMFFDecoder::CONTEXT_T * context)
{
	_context = context;
	//_fp_yuv = fopen(_context->yuv, "wb + ");
	_is_icon_loaded = FALSE;
	if (strlen(_context->logo) > 0 && FileExists(_context->logo))
	{
		std::vector<unsigned char> png;
		std::vector<unsigned char> img;
		unsigned int width, height;
		lodepng::State state;
		unsigned error = lodepng::load_file(png, _context->logo);
		if (!error)
		{
			error = lodepng::decode(img, width, height, png);
			if (!error && _context->width == width && _context->height == height)
			{
				_logo = (uint8_t*)malloc(width * height * 3);
				_alpha = (uint8_t*)malloc(width * height);
				if (_logo != NULL && _alpha != NULL)
				{
					::memset(_logo, 0x00, width * height * 3);
					::memset(_alpha, 0x00, width * height);

					int logoIndex = 0;
					int alphaIndex = 0;
					for (int i = 0; i < img.size(); i++)
					{
						if ((i + 1) % 4 == 0)
						{
							_alpha[alphaIndex] = img.at(i);
							alphaIndex++;
						}
						else
						{
							_logo[logoIndex] = img.at(i);
							logoIndex++;
						}
					}
					_is_icon_loaded = TRUE;
				}
			}
		}
	}

	switch (_context->codec)
	{
		case ESMFFDecoder::VIDEO_CODEC_T::AVC:
		{
			_av_codec = avcodec_find_decoder(AV_CODEC_ID_H264);
			break;
		}
		case ESMFFDecoder::VIDEO_CODEC_T::HEVC:
		{
			_av_codec = avcodec_find_decoder(AV_CODEC_ID_HEVC);
			break;
		}
	};

	if (!_av_codec)
	{
		Release();
		return ESMFFDecoder::ERR_CODE_T::GENERIC_FAIL;
	}

	_av_codec_ctx = avcodec_alloc_context3(_av_codec);
	if (!_av_codec_ctx)
	{
		Release();
		return ESMFFDecoder::ERR_CODE_T::GENERIC_FAIL;
	}

	if (_av_codec->capabilities & AV_CODEC_CAP_TRUNCATED)
		_av_codec_ctx->flags |= AV_CODEC_CAP_TRUNCATED;

	_av_codec_ctx->delay = 0;
	// on test..
	_av_codec_ctx->thread_count = 12;
	if (_context->extradata && (_context->extradata_size > 0))
	{
		_av_codec_ctx->extradata = static_cast<uint8_t*>(av_malloc(_context->extradata_size));
		_av_codec_ctx->extradata_size = _context->extradata_size;
		memcpy(_av_codec_ctx->extradata, _context->extradata, _av_codec_ctx->extradata_size);
		//memset(_av_codec_ctx->extradata, 0x00, _context->extra_data_size);
	}
	if (avcodec_open2(_av_codec_ctx, _av_codec, NULL) < 0)
	{
		Release();
		return ESMFFDecoder::ERR_CODE_T::GENERIC_FAIL;
	}
	//_av_codec_ctx->slices = 4;
	//_av_codec_ctx->slice_flags = SLICE_FLAG_ALLOW_FIELD;
	//_av_codec_ctx->slice_count = 4;
	//_av_codec_ctx->thread_type = FF_THREAD_SLICE;
	_yuv_frame = av_frame_alloc();
	if (!_yuv_frame)
	{
		Release();
		return ESMFFDecoder::ERR_CODE_T::GENERIC_FAIL;
	}

	// add resize frame
	_yuv_resize_frame = av_frame_alloc();
	if (!_yuv_resize_frame) {
		Release();
		return ESMFFDecoder::ERR_CODE_T::GENERIC_FAIL;
	}

	_uyvy_frame = av_frame_alloc();
	if (!_uyvy_frame)
	{
		Release();
		return ESMFFDecoder::ERR_CODE_T::GENERIC_FAIL;
	}

	_rgb_frame = av_frame_alloc();
	if (!_rgb_frame)
	{
		Release();
		return ESMFFDecoder::ERR_CODE_T::GENERIC_FAIL;
	}

	_av_packet = av_packet_alloc();
	if (!_av_packet)
	{
		Release();
		return ESMFFDecoder::ERR_CODE_T::GENERIC_FAIL;
	}

	_yuv_buffer_size = av_image_get_buffer_size(AV_PIX_FMT_YUV420P, 1920, 1080, 1);
	_yuv_buffer = (uint8_t*)av_malloc(_yuv_buffer_size);
	av_image_fill_arrays(_yuv_resize_frame->data, _yuv_resize_frame->linesize, _yuv_buffer, AV_PIX_FMT_YUV420P, 1920, 1080, 1);

	_uyvy_buffer_size = av_image_get_buffer_size(AV_PIX_FMT_UYVY422, _context->width, _context->height, 1);
	_uyvy_buffer = (uint8_t*)av_malloc(_uyvy_buffer_size);
	av_image_fill_arrays(_uyvy_frame->data, _uyvy_frame->linesize, _uyvy_buffer, AV_PIX_FMT_UYVY422, _context->width, _context->height, 1);

	_rgb_buffer_size = av_image_get_buffer_size(AV_PIX_FMT_RGB24, _context->width, _context->height, 1);
	_rgb_buffer = (uint8_t*)av_malloc(_rgb_buffer_size);
	av_image_fill_arrays(_rgb_frame->data, _rgb_frame->linesize, _rgb_buffer, AV_PIX_FMT_RGB24, _context->width, _context->height, 1);

	_sws_yuv_ctx = sws_getContext(_context->width, _context->height, AV_PIX_FMT_RGB24, _context->width, _context->height, AV_PIX_FMT_UYVY422, SWS_FAST_BILINEAR, NULL, NULL, NULL);
	_sws_rgb_ctx = sws_getContext(_context->width, _context->height, AV_PIX_FMT_YUV420P, _context->width, _context->height, AV_PIX_FMT_RGB24, SWS_FAST_BILINEAR, NULL, NULL, NULL);
	_sws_yuv2uyvy_ctx = sws_getContext(_context->width, _context->height, AV_PIX_FMT_YUV420P, _context->width, _context->height, AV_PIX_FMT_UYVY422, SWS_FAST_BILINEAR, NULL, NULL, NULL);

	// add yuv resize
	_sws_resize_yuv_ctx = sws_getContext(3840, 2160, AV_PIX_FMT_YUV420P, 1920, 1080, AV_PIX_FMT_YUV420P, SWS_FAST_BILINEAR, NULL, NULL, NULL);

	_isInitialized = TRUE;
	return ESMFFDecoder::ERR_CODE_T::SUCCESS;
}

int ESMFFDecoder::Core::Release(void)
{
	sws_freeContext(_sws_yuv2uyvy_ctx);
	sws_freeContext(_sws_rgb_ctx);
	sws_freeContext(_sws_yuv_ctx);

	// add yuv resize
	sws_freeContext(_sws_resize_yuv_ctx);

	_uyvy_buffer_size = 0;
	_rgb_buffer_size = 0;
	_yuv_buffer_size = 0;

	if (_yuv_buffer) {
		av_free(_yuv_buffer);
		_yuv_buffer = nullptr;
	}

	if (_uyvy_buffer)
	{
		av_free(_uyvy_buffer);
		_uyvy_buffer = nullptr;
	}
	
	if (_rgb_buffer)
	{
		av_free(_rgb_buffer);
		_rgb_buffer = nullptr;
	}

	if (_av_packet)
	{
		av_packet_free(&_av_packet);
		_av_packet = nullptr;
	}

	if (_uyvy_frame)
	{
		av_frame_free(&_uyvy_frame);
		_uyvy_frame = nullptr;
	}

	if (_yuv_frame)
	{
		av_frame_free(&_yuv_frame);
		_yuv_frame = nullptr;
	}

	// add resize frame
	if (_yuv_resize_frame) {
		av_frame_free(&_yuv_resize_frame);
		_yuv_resize_frame = nullptr;
	}

	/*
	if (_av_format_ctx)
	{
		av_free(_av_format_ctx);
		_av_format_ctx = nullptr;
	}
	*/

	if (_av_codec_ctx)
	{
		avcodec_free_context(&_av_codec_ctx);
		_av_codec_ctx = nullptr;
	}

	if (_alpha)
	{
		free(_alpha);
		_alpha = nullptr;
	}

	if (_logo)
	{
		free(_logo);
		_logo = nullptr;
	}

	_isInitialized = FALSE;
	return ESMFFDecoder::ERR_CODE_T::SUCCESS;
}

int ESMFFDecoder::Core::Decode(unsigned char* bitstream, int bitstreamSize, unsigned char*** decoded, int* numberOfDecoded, BOOL drawLogo, unsigned int cts)
{
	std::vector<uint8_t*>::iterator iter;
	for (iter = _queue.begin(); iter < _queue.end(); iter++)
		if (*(iter))
			free(*iter);
	_queue.clear();

	int32_t value = ESMFFDecoder::ERR_CODE_T::GENERIC_FAIL;
	int32_t result = -1;

	if (bitstream && bitstreamSize > 0)
	{
		_av_packet->data = reinterpret_cast<uint8_t*>(bitstream);
		_av_packet->size = bitstreamSize;
		_av_packet->pts = cts;
		result = avcodec_send_packet(_av_codec_ctx, _av_packet);
	}
	else 
	{
		result = avcodec_send_packet(_av_codec_ctx, NULL);
	}
	if (result < 0)
		return ESMFFDecoder::ERR_CODE_T::SUCCESS;

	while (result >= 0)
	{
		result = avcodec_receive_frame(_av_codec_ctx, _yuv_frame);
		if (result == AVERROR(EAGAIN) || result == AVERROR_EOF)
			break;
		else if (result < 0)
			break;

		// The following codes are commented out because they are useless.

		/*if (_is_icon_loaded && drawLogo)
		{
			_sws_rgb_ctx = sws_getCachedContext(_sws_rgb_ctx, _context->width, _context->height, AV_PIX_FMT_YUV420P, _context->width, _context->height, AV_PIX_FMT_RGB24, SWS_FAST_BILINEAR, NULL, NULL, NULL);
			sws_scale(_sws_rgb_ctx, _yuv_frame->data, _yuv_frame->linesize, 0, _context->height, _rgb_frame->data, _rgb_frame->linesize);

			SimdAlphaBlending(_logo, _context->width * 3, _context->width, _context->height, 3, _alpha, _context->width, _rgb_frame->data[0], _context->width * 3);

			_sws_yuv_ctx = sws_getCachedContext(_sws_yuv_ctx, _context->width, _context->height, AV_PIX_FMT_RGB24, _context->width, _context->height, AV_PIX_FMT_UYVY422, SWS_FAST_BILINEAR, NULL, NULL, NULL);
			sws_scale(_sws_yuv_ctx, _rgb_frame->data, _rgb_frame->linesize, 0, _context->height, _uyvy_frame->data, _uyvy_frame->linesize);
		}
		else*/
		{
			//_sws_yuv2uyvy_ctx = sws_getCachedContext(_sws_yuv2uyvy_ctx, _context->width, _context->height, AV_PIX_FMT_YUV420P, _context->width, _context->height, AV_PIX_FMT_UYVY422, SWS_FAST_BILINEAR, NULL, NULL, NULL);
			//sws_scale(_sws_yuv2uyvy_ctx, _yuv_frame->data, _yuv_frame->linesize, 0, _context->height, _uyvy_frame->data, _uyvy_frame->linesize);

			//_sws_rgb_ctx = sws_getCachedContext(_sws_rgb_ctx, _context->width, _context->height, AV_PIX_FMT_YUV420P, _context->width, _context->height, AV_PIX_FMT_RGB24, SWS_FAST_BILINEAR, NULL, NULL, NULL);
			//sws_scale(_sws_rgb_ctx, _yuv_frame->data, _yuv_frame->linesize, 0, _context->height, _rgb_frame->data, _rgb_frame->linesize);
		}

		//fwrite(_uyvy_frame->data[0], size_t(_context->width) * size_t(_context->height), 2, _fp_yuv);

		//uint8_t* buffer = (uint8_t*)malloc(_uyvy_buffer_size);
		//memmove(buffer, _uyvy_frame->data[0], _uyvy_buffer_size);
		/*uint8_t* buffer = (uint8_t*)malloc(_yuv_buffer_size);
		int pos = 0;
		memcpy(buffer, _yuv_frame->data[0], _context->width * _context->height);
		pos += _context->width * _context->height;
		memcpy(buffer + pos, _yuv_frame->data[1], _context->width * _context->height / 4);
		pos += _context->width * _context->height / 4;
		memcpy(buffer + pos, _yuv_frame->data[2], _context->width * _context->height / 4);*/

		// pts
		int cts = _yuv_frame->pkt_pts;

		// resize
		sws_scale(_sws_resize_yuv_ctx, _yuv_frame->data, _yuv_frame->linesize, 0, 2160, _yuv_resize_frame->data, _yuv_resize_frame->linesize);

		uint8_t* buffer = (uint8_t*)malloc(_yuv_buffer_size);
		int pos = 0;
		memcpy(buffer, _yuv_resize_frame->data[0], 1920 * 1080);
		pos += 1920 * 1080;
		memcpy(buffer + pos, _yuv_resize_frame->data[1], 1920 * 1080 / 4);
		pos += 1920 * 1080 / 4;
		memcpy(buffer + pos, _yuv_resize_frame->data[2], 1920 * 1080 / 4);

		TRACE("frame %c", av_get_picture_type_char(_yuv_resize_frame->pict_type));

		_queue.push_back(buffer);
	}

	if(_queue.size()>0)
	{
		*decoded = &_queue[0];
		*numberOfDecoded = _queue.size();
	}
	else
	{
		*numberOfDecoded = 0;
	}
	return ESMFFDecoder::ERR_CODE_T::SUCCESS;
}

BOOL ESMFFDecoder::Core::FileExists(LPSTR szPath)
{
	DWORD dwAttrib = GetFileAttributesA(szPath);
	return (dwAttrib != INVALID_FILE_ATTRIBUTES &&
		!(dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
}



int ESMFFDecoder::Core::Decode(const INPUT_STREAM* pSrc, OUTPUT_RAW* pDst)
{
	std::vector<uint8_t*>::iterator iter;
	for (iter = _queue.begin(); iter < _queue.end(); iter++)
		if (*(iter))
			free(*iter);
	_queue.clear();

	int32_t value = ESMFFDecoder::ERR_CODE_T::GENERIC_FAIL;
	int32_t result = -1;

	if (pSrc->bitstream && pSrc->bitstreamSize > 0)
	{
		_av_packet->data = reinterpret_cast<uint8_t*>(pSrc->bitstream);
		_av_packet->size = pSrc->bitstreamSize;
		_av_packet->pts = pSrc->cts;
		result = avcodec_send_packet(_av_codec_ctx, _av_packet);
	}
	else
	{
		result = avcodec_send_packet(_av_codec_ctx, NULL);
	}
	if (result < 0)
		return ESMFFDecoder::ERR_CODE_T::SUCCESS;

	while (result >= 0)
	{
		result = avcodec_receive_frame(_av_codec_ctx, _yuv_frame);
		if (result == AVERROR(EAGAIN) || result == AVERROR_EOF)
			break;
		else if (result < 0)
			break;
		
		// pts
		//int cts = _yuv_frame->pkt_pts;
		pDst->cts = _yuv_frame->pkt_pts;

		// resize
		sws_scale(_sws_resize_yuv_ctx, _yuv_frame->data, _yuv_frame->linesize, 0, 2160, _yuv_resize_frame->data, _yuv_resize_frame->linesize);

		uint8_t* buffer = (uint8_t*)malloc(_yuv_buffer_size);
		int pos = 0;
		memcpy(buffer, _yuv_resize_frame->data[0], 1920 * 1080);
		pos += 1920 * 1080;
		memcpy(buffer + pos, _yuv_resize_frame->data[1], 1920 * 1080 / 4);
		pos += 1920 * 1080 / 4;
		memcpy(buffer + pos, _yuv_resize_frame->data[2], 1920 * 1080 / 4);

		TRACE("frame %c", av_get_picture_type_char(_yuv_resize_frame->pict_type));

		_queue.push_back(buffer);
	}

	if (_queue.size() > 0)
	{
		pDst->decoded = &_queue[0];
		pDst->numberOfDecoded = _queue.size();
	}
	else
	{
		pDst->numberOfDecoded = 0;
	}
	return ESMFFDecoder::ERR_CODE_T::SUCCESS;
}