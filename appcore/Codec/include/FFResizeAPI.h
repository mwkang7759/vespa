#ifndef __FFRESIZEAPI_H__
#define __FFRESIZEAPI_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef	void*	FFResizeHandle;

FFResizeHandle FFResizeOpen(
	DWORD32				dwSrcFourCC,		// Source color space 
	DWORD32				dwSrcBitCount,		// Source # of Bits per Pixel
	VideoOutFormat		eSrcSubFormat,		// Source Color SubFormat for 16 bit RGB or YUV
	BOOL				bInterlace,			// Source is interlace (2) or progressive (1)
	DWORD32				dwSrcWidth,			// Source Width
	DWORD32				dwSrcHeight,		// Source Height
	DWORD32				dwSrcStartW,		// Source Croppong Vertical Start Position
	DWORD32				dwSrcStartH,		// Source Croppong Horizontal Start Position
	DWORD32				dwDstFourCC,		// Output color space 
	DWORD32				dwDstBitCount,		// Output # of Bits per Pixel
	VideoOutFormat		eDstSubFormat,		// Output Color SubFormat for 16 bit RGB or YUV
	DWORD32				dwDstWidth,			// Output Width
	DWORD32				dwDstHeight,		// Output Height
	DWORD32				dwOutStartW,		// Output Padding Vertical Start Position
	DWORD32				dwOutStartH			// Output Padding Horizontal Start Position
);

FFResizeHandle FFColorConvOpen(
	FFResizeHandle		hCur,
	DWORD32				dwSrcFourCC,		// Source color space 
	DWORD32				dwSrcBitCount,		// # of Bits per Pixel
	VideoOutFormat		eSrcSubFormat,		// Source Color SubFormat for RGB or YUV
	DWORD32				dwSrcWidth,			// Source Width
	DWORD32				dwSrcHeight,		// Source Height
	DWORD32				dwDstFourCC,		// Output color space 
	DWORD32				dwDstBitCount,		// Output # of Bits per Pixel
	VideoOutFormat		eDstSubFormat		// Output Color SubFormat for RGB or YUV
);

VOID FFResizeClose(FFResizeHandle p);

VOID FFResizeFrame(
	FFResizeHandle h,						// Handle of FFFilter
	const BYTE* const pSrc[],				// Pointers to Source Image
	const DWORD32 pSrcPitch[],				// Pitchs of Source Image
	BYTE* const pDst[],						// Pointers to Output Image
	const DWORD32	pDstPitch[]					// Pitchs of Output Image
);

#ifdef __cplusplus
}
#endif

#endif // __FFRESIZEAPI_H__ 
