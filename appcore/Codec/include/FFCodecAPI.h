#ifndef _FFMPEG_CODECAPI_H
#define _FFMPEG_CODECAPI_H

#include "mediainfo.h"
#ifdef FFMPEG2_DLL_EXPORTS
 #define TRACE(A, ...)
 #define DLL_EXPORT_DEFINE	__declspec(dllexport)
#else
 #include "TraceAPI.h"
 #define DLL_EXPORT_DEFINE
#endif

#define		CodecMPEG4				0
#define		CodecH264				1
#define		FLAC_FRAME_SIZE			1024
#define		ALAC_FRAME_SIZE			4096

#ifdef __cplusplus
extern "C" {
#endif

typedef struct PsVideoCodecOutput {
	BYTE*		y;
	BYTE*		u;
	BYTE*		v;
	INT32		height;
	INT32		width;
	INT32		y_pitch;
	INT32		u_pitch;
	INT32		v_pitch;
	INT32		format;
	INT32		usedBytes;
	INT32		got_picture;
	INT32		interlace;
	UINT32		cts;
	VOID*		surface;

	BYTE*		frame;
	INT32		out_size;
	INT32		out_buffer_size;
	VideoOutFormat	eVideoFormat;
} PsVideoCodecOutput;

// init
DLL_EXPORT_DEFINE
VOID   FFMpegInit();

// video decode
DLL_EXPORT_DEFINE
VOID * FFVideoDecOpen(
	UINT32 codec_type,
	BYTE *config_data,
	UINT32 config_size,
	UINT32 frame_height,
	UINT32 frame_width,
	UINT32 bits_count,
	VideoOutFormat	*eVideoFormat,
	FrTimeStamp eMode,
	FrVideoInfo *hVideo
);
DLL_EXPORT_DEFINE
VOID   FFVideoDecClose(VOID *p);
DLL_EXPORT_DEFINE
INT32  FFVideoDecDecode(VOID *p, BYTE *bitstream, INT32 size, INT32 cts, INT32 dts, FrTimeStamp time_mode, PsVideoCodecOutput **pout);
DLL_EXPORT_DEFINE
INT32  FFVideoDecGetBufferedFrame(VOID *p, PsVideoCodecOutput **pout, BOOL bStartFlush);
DLL_EXPORT_DEFINE
VOID   FFVideoDecFlush(VOID *p);

//// video encode
DLL_EXPORT_DEFINE
VOID*  FFVideoEncOpen(UINT32 codec_type, FrVideoInfo* hVideo);
DLL_EXPORT_DEFINE
VOID   FFVideoEncClose(VOID *p);
DLL_EXPORT_DEFINE
BOOL   FFVideoEncEncode(VOID *p, FrRawVideo *pVideo, FrMediaStream *pData, BYTE *pBitstream, INT32 nBitstream);
DLL_EXPORT_DEFINE
BOOL  FFVideoEncFlush(VOID *p, FrMediaStream *pData, BYTE *pBitstream, INT32 nBitstream);
DLL_EXPORT_DEFINE
BOOL  FFVideoEncGetInfo(VOID *p, FrVideoInfo* hVideo);


#ifdef __cplusplus
}
#endif

#endif /* AVCODEC_H */
