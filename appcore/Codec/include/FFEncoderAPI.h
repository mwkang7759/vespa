#ifndef CODECAPI_H
#define CODECAPI_H

/**
 * @file avcodec.h
 * external api header.
 */

#define		CodecMPEG4	0
#define		CodecH264	1

#ifdef __cplusplus
extern "C" {
#endif

typedef struct PsVideoEncOutput 
{
	unsigned char	*frame;
	int				out_size;
	int				out_buffer_size;
} PsVideoEncOutput;

typedef struct
{
	unsigned char	*frame;
	int				buffer_size;
	int				width;
	int				height;
	int				bitrate;
	int				framerate;
}FFMPEG_ENC_INFO;

extern void * FFMpegCodecEncOpen(unsigned int codec_type, FFMPEG_ENC_INFO *pInfo);
extern void FFMpegCodecEncClose(void *p);
extern int  FFMpegCodecEncode(void *p, unsigned char *y, unsigned char* u, unsigned char*v, PsVideoEncOutput **pout);

#ifdef __cplusplus
}
#endif

#endif /* AVCODEC_H */
