#ifndef __FFFILTERAPI_H__
#define __FFFILTERAPI_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef	void*	FFFilterHandle;

typedef	enum {
	FFFilterNone = 0,
	FFFilterRotate90,
	FFFilterRotate180,
	FFFilterRotate270,
	FFFilterMirrorHorizontal,
	FFFilterMirrorVertical,
} eFFFilterMode;

FFFilterHandle FFFilterOpen(
	DWORD32			dwSrcFourCC,		// Source color space
	DWORD32			dwSrcWidth,			// Source width
	DWORD32			dwSrcHeight,		// Source height
	DWORD32			dwSrcTick,			// Source timestamp ticks in seconds
	eFFFilterMode	eMode				// Filter mode
);

FFFilterHandle FFFilterOpenEx(
	DWORD32			dwSrcFourCC,		// Source color space
	DWORD32			dwSrcWidth,			// Source width
	DWORD32			dwSrcHeight,		// Source height
	DWORD32			dwSrcTick,			// Source timestamp ticks in seconds
	DWORD32			dwBlur				// Blur level
);

VOID FFFilterClose(FFFilterHandle p);

LRSLT FFFilterFrame(
	FFFilterHandle	h,				// Handle of FFFilter
	DWORD32			dwSrcFourCC,	// Source color space. Valid when h is null
	DWORD32			dwSrcWidth,		// Source width
	DWORD32			dwSrcHeight,	// Source height
	DWORD32			dwSrcTick,		// Source timestamp ticks in seconds
	eFFFilterMode	eMode,			// Filter mode. End of Valid
	BYTE			*ppSrc[],		// Pointers to Source Image
	DWORD32			pSrcPitch[],	// Pitches of Source Image
	BYTE			*ppDst[],		// Pointers to Output Image
	DWORD32			pDstPitch[]		// Pitches of Output Image
);

#ifdef __cplusplus
}
#endif

#endif // FFFILTERAPI_H 
