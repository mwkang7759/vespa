/*****************************************************************************
*                                                                            *
*                            DTB H264 Encoder Redefine Header							 	 *
*                                                                            *
*   Copyright (c) 2016 by DreamToBe, Incoporated. All Rights Reserved.       *
******************************************************************************

    File Name       : dtb_h264_redef.h
    Author(s)       : CHANG, Joonho
    Created         : 29 Dec 2016

    Description     : File API
    Notes           :

==============================================================================
                      Modification History (Reverse Order)
==============================================================================
    Date        Author      		Location (variables) / Description
------------------------------------------------------------------------------
	29/12/2016		CHANG, Joonho		draft
------------------------------------------------------------------------------*/

#ifndef DTB_H264_REDEFINE_H

#define DTB_H264_REDEFINE_H

// Structures
#define		x264_param_t					dtb_h264_param_t
#define		x264_t							dtb_h264_t
#define		x264_nal_t						dtb_h264_nal_t
#define		x264_picture_t					dtb_h264_picture_t

// Constant varialbes
#define		x264_preset_names				dtb_h264_preset_names
#define		x264_tune_names					dtb_h264_tune_names
#define		x264_profile_names				dtb_h264_profile_names

// Macros
#define		X264_TYPE_AUTO					DTB_H264_TYPE_AUTO
#define		X264_CSP_NV12					DTB_H264_CSP_NV12
#define		X264_CSP_I420					DTB_H264_CSP_I420
#define		X264_TYPE_IDR					DTB_H264_TYPE_IDR
#define		X264_TYPE_P						DTB_H264_TYPE_P
#define		X264_TYPE_BREF					DTB_H264_TYPE_BREF
#define		X264_TYPE_B						DTB_H264_TYPE_B
#define		X264_TYPE_KEYFRAME				DTB_H264_TYPE_KEYFRAME

// Functions
#define		x264_encoder_open_148			dtb_h264_encoder_open_200
#define		x264_encoder_close				dtb_h264_encoder_close
#define		x264_encoder_encode				dtb_h264_encoder_encode
#define		x264_encoder_headers			dtb_h264_encoder_headers
#define		x264_encoder_delayed_frames		dtb_h264_encoder_delayed_frames
#define		x264_encoder_parameters			dtb_h264_encoder_parameters
#define		x264_encoder_reconfig			dtb_h264_encoder_reconfig
#define		x264_param_default				dtb_h264_param_default
#define		x264_picture_clean				dtb_h264_picture_clean
#define		x264_picture_alloc				dtb_h264_picture_alloc
#define		x264_param_apply_fastfirstpass	dtb_h264_param_apply_fastfirstpass
#define		x264_param_apply_profile		dtb_h264_param_apply_profile
#define		x264_param_default_preset		dtb_h264_param_default_preset
#define		x264_param_parse			dtb_h264_param_parse

#endif
