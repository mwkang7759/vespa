
#include "mediainfo.h"
//#include "FFDecoderAPI.h"
#include "FFCommon.h"


#define		NoMemory	-1

// ultrafast	superfast	veryfast	faster	fast	medium	slow	slower	veryslow	placebo
VOID * FFVideoEncOpen(UINT32 codec_type, FrVideoInfo * hVideo)
{
	NewAVCodecContext* n = NULL;
	AVCodec* codec = NULL;
	//AVCodecContext *c=NULL;
	enum AVCodecID		CodecID;
	//AVDictionary* dict = NULL;
	INT32				lRet;
	char				szOption[2000] = { 0, },
		* pszOpt = &szOption[0];
	BOOL				bThreadSliceSupported = FALSE,
		bThreadFrameSupported = FALSE;

	switch (codec_type)
	{
	case FOURCC_JPEG:
		CodecID = AV_CODEC_ID_MJPEG;
		break;
	case FOURCC_AVC1:
	case FOURCC_H264:
	case FOURCC_h264:
		CodecID = AV_CODEC_ID_H264;
		break;
	case FOURCC_H265:
	case FOURCC_HEVC:
		CodecID = AV_CODEC_ID_H265;
		break;
	default: return	NULL;
	}

	// allocation
	n = (NewAVCodecContext*)av_mallocz(sizeof(NewAVCodecContext));

	n->nFirstCTS = MAX_DWORD / 2;
	n->bFirstFrame = TRUE;

	// register all the codecs && find the encoder
	//avcodec_register_all();
	codec = avcodec_find_encoder(CodecID);
	if (!codec) {
		return	(VOID*)NoMemory;
	}
	n->c = avcodec_alloc_context3(codec);

	// allco failed
	if (!n || !n->c)
	{
		// free alloced memory
		if (n->c)	avcodec_close((AVCodecContext*)n->c);
		if (n)		av_free((VOID*)n);

		return	(VOID*)NoMemory;
	}

	// put sample parameters
	n->c->bit_rate = hVideo->dwBitRate;
	// resolution must be a multiple of two
	n->c->width = hVideo->dwWidth;
	n->c->height = hVideo->dwHeight;

	n->c->max_b_frames = hVideo->dwBFrame;

	// frames per second
	if (hVideo->dwFrameRate)
	{
		n->c->time_base.num = 1;
		n->c->time_base.den = 1000;
		n->c->gop_size = (int)(hVideo->dwFrameRate * hVideo->fIntraFrameRefresh / 1000);
		n->c->framerate.num = hVideo->dwFrameRate;
		n->c->framerate.den = 1000;
	}
	else
	{
		n->c->time_base.num = 1;
		n->c->time_base.den = 1000;
		n->c->gop_size = (int)(25000 * hVideo->fIntraFrameRefresh / 1000);
		n->c->framerate.num = 1;
		n->c->framerate.den = 25;
	}

	//n->c->max_b_frames = 0;
	n->c->pix_fmt = AV_PIX_FMT_YUV420P;
	n->c->colorspace = AVCOL_SPC_SMPTE170M;
	n->c->chroma_sample_location = AVCHROMA_LOC_LEFT;
	n->c->bits_per_raw_sample = 16;
	n->c->codec_id = codec->id;
	//n->c->sample_aspect_ratio.den = 1;			// square
	//n->c->sample_aspect_ratio.num = 1;

	if (codec->capabilities & AV_CODEC_CAP_SLICE_THREADS)
		bThreadSliceSupported = TRUE;
	else if (codec->capabilities & AV_CODEC_CAP_AUTO_THREADS)
		bThreadSliceSupported = TRUE;

	if (codec->capabilities & AV_CODEC_CAP_FRAME_THREADS)
		bThreadFrameSupported = TRUE;
	else if (codec->capabilities & AV_CODEC_CAP_AUTO_THREADS)
		bThreadFrameSupported = TRUE;

	if (CodecID == AV_CODEC_ID_H264)
	{
		const char* h264_preset[] = { "ultrafast", "superfast", "veryfast", "faster", "fast",
									  "medium",
									  "slow", "slower", "veryslow", "placebo" };
		const char* h264_tune[] = { "film", "animation", "grain", "stillimage", "psnr", "ssim", "fastdecode", "zerolatency", };

		// "ultrafast", "superfaset", "veryfast", "faster", "fast", "medium", "slow", "slower", "veryslow", "placebo", 0
		// "film", "animation", "grain", "stillimage", "psnr", "ssim", "fastdecode", "zerolatency", 0
		if ((0 <= hVideo->nPreset) && (hVideo->nPreset < 10))
			hVideo->nPreset = hVideo->nPreset;
		else
			hVideo->nPreset = 3;		// faster
		if ((0 <= hVideo->nTune) && (hVideo->nTune < 8))
			hVideo->nTune = hVideo->nTune;
		else
			hVideo->nTune = 0;		// film

		// zero latency
		hVideo->nTune = 7;		// force set..
		if (hVideo->nTune == 7)
		{
			hVideo->dwBFrame = 0;
			n->c->max_b_frames = 0;
			n->c->thread_count = 1;
		}
		// Set multi threads
		else if (hVideo->dwVidEncThread <= 12)
			n->c->thread_count = hVideo->dwVidEncThread;
		else
			n->c->thread_count = 4;


		// x264-params
		pszOpt = szOption;
		lRet = 0;

		lRet = av_opt_set(n->c->priv_data, "preset", h264_preset[hVideo->nPreset], 0);

		//hVideo->m_nTune = 7;			// zerolatency
		lRet = av_opt_set(n->c->priv_data, "tune", h264_tune[hVideo->nTune], 0);

		if (hVideo->nProfileType == 0)
		{
			n->c->profile = FF_PROFILE_H264_BASELINE;
			hVideo->dwRCMode = 2;		// ABR
		}
		else if (hVideo->nProfileType == 1)
			n->c->profile = FF_PROFILE_H264_MAIN;
		else if (hVideo->nProfileType == 2)
			n->c->profile = FF_PROFILE_H264_HIGH;
		else
			n->c->profile = FF_PROFILE_H264_MAIN;

		switch (hVideo->dwRCMode)
		{
		case	0:		// CQP
			// set qp value
			n->c->bit_rate = 0;
			pszOpt += sprintf_s(pszOpt, sizeof(szOption) / 2, "qp=%2.3f:", (float)hVideo->dwQualityFactor / 1000.);
			//lRet = av_opt_set(n->c->priv_data, "x264-params", szOpt, 0);
			if (lRet != 0)
			{
				LOG_E("[FFMpegVideoEncoderOpen]	FFMPEG H264 [error]: H264_param_parse failed  - qp");
				return NULL;
			}
			break;
		case	1:		// CRF
			// set rf value
			n->c->bit_rate = 0;
			pszOpt += sprintf_s(pszOpt, sizeof(szOption) / 2, "crf=%2.3f:", (float)hVideo->dwQualityFactor / 1000.);
			//lRet = av_opt_set(n->c->priv_data, "x264-params", szOpt, 0);
			if (lRet != 0)
			{
				LOG_E("[FFMpegVideoEncoderOpen]	FFMPEG H264 [error]: H264_param_parse failed  - crf");
				return NULL;
			}
			break;
		default:		// ABR
			// Nothing tho do
			break;
		}

		if (hVideo->nProfileType == 0)		// Baseline
		{
			// No BFrames
			n->c->max_b_frames = 0;

			// NHN. Optimize delay
			if (hVideo->dwWidth <= 480 && hVideo->dwHeight <= 360)
				n->c->thread_count = 1;

			if (bThreadSliceSupported)
				n->c->thread_type = FF_THREAD_FRAME;
			else if (bThreadFrameSupported)
				n->c->thread_type = FF_THREAD_SLICE;

			// set ref frame number by 1
			pszOpt += sprintf_s(pszOpt, sizeof(szOption) / 2, "frameref=1:");

			LOG_E("[FFMpegVideoEncoderOpen]	FFMPEG H264 : It's working as baseline profile with single nal");
		}
		else
		{
			// Probably for RTS
			if (hVideo->bSpeedOptimization == FALSE)
			{
				if (bThreadSliceSupported)
					n->c->thread_type = FF_THREAD_SLICE;
				else if (bThreadFrameSupported)
					n->c->thread_type = FF_THREAD_FRAME;

				// set ref frame number by 1
				//pszOpt += sprintf_s(pszOpt, sizeof(szOption)/2, "ref=1:");
				LOG_E("[FFMpegVideoEncoderOpen]	FFMPEG H264 : It's working in delay optimization mode");
			}
			else
			{
				if (bThreadFrameSupported)
					n->c->thread_type = FF_THREAD_FRAME;
				else if (bThreadSliceSupported)
					n->c->thread_type = FF_THREAD_SLICE;
			}
		}

		n->c->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;			// get global decoder config info

		if (hVideo->BSType == MP4_TYPE)
		{
			n->bIsMP4 = TRUE;
			lRet = av_opt_set(n->c->priv_data, "aud", "0", 0);

			pszOpt += sprintf_s(pszOpt, sizeof(szOption) / 2, "repeat-headers=0:");
			pszOpt += sprintf_s(pszOpt, sizeof(szOption) / 2, "annexb=0:");

			// no sei info
			//lRet = av_opt_set(n->c->priv_data, "x265-params", "no-info=1:", 0);
			//lRet = av_opt_set(n->c->priv_data, "x264-params", "info=0:", 0);
			//lRet = av_opt_set(n->c->priv_data, "x264-params", "rc_lookahead=2:", 0);
			//sprintf_s(temp, sizeof(temp), "sliced-threads=%d:", hVideo->m_dwVidEncThread);
			//lRet = av_opt_set(n->c->priv_data, "x264-params", temp, 0);
			pszOpt += sprintf_s(pszOpt, sizeof(szOption) / 2, "keyint=%d:", (int)(hVideo->fIntraFrameRefresh * hVideo->dwFrameRate / 1000));
			pszOpt += sprintf_s(pszOpt, sizeof(szOption) / 2, "bframes=%d:", hVideo->dwBFrame);
			pszOpt += sprintf_s(pszOpt, sizeof(szOption) / 2, "scenecut=%d:", -1);
		}
		else
		{
			// let each frame start with AUD
			// repeat vps, sps and pps at the start of each key frame
			// no sei info
			lRet = av_opt_set(n->c->priv_data, "aud", "1", 0);
			
			pszOpt += sprintf_s(pszOpt, sizeof(szOption) / 2, "repeat-headers=1:");
			pszOpt += sprintf_s(pszOpt, sizeof(szOption) / 2, "annexb=1:");

			pszOpt += sprintf_s(pszOpt, sizeof(szOption) / 2, "keyint=%d:", (int)(hVideo->fIntraFrameRefresh * hVideo->dwFrameRate / 1000));
			pszOpt += sprintf_s(pszOpt, sizeof(szOption) / 2, "bframes=%d:", hVideo->dwBFrame);
			pszOpt += sprintf_s(pszOpt, sizeof(szOption) / 2, "scenecut=%d:", -1);
		}
		pszOpt += sprintf_s(pszOpt, sizeof(szOption) / 2, "threads=%d:", n->c->thread_count);

		lRet = av_opt_set(n->c->priv_data, "x264-params", &szOption[0], 0);
		if (lRet != 0)
		{
			LOG_E("[FFMpegVideoEncoderOpen]	FFMPEG H264 [error]: H264_param_parse failed  - crf");
			return NULL;
		}
	}
	else
		if (CodecID == AV_CODEC_ID_H265)
		{
			INT32	lRet;

			const char* hevc_preset[] = { "ultrafast", "superfast", "veryfast", "faster", "fast",
										  "medium",
										  "slow", "slower", "veryslow", "placebo" };
			const char* hevc_tune[] = { "psnr", "ssim", "fastdecode" , "zero-latency", "grain" };

			// x265-params
			pszOpt = szOption;
			lRet = 0;

			if (10 <= hVideo->nPreset)
				hVideo->nPreset = 10 - 1;
			lRet = av_opt_set(n->c->priv_data, "preset", hevc_preset[hVideo->nPreset], 0);
			if (4 <= hVideo->nTune)
				hVideo->nTune = 4 - 1;
			lRet = av_opt_set(n->c->priv_data, "tune", hevc_tune[hVideo->nTune], 0);

			if (hVideo->nProfile == 0)
				n->c->profile = FF_PROFILE_HEVC_MAIN;
			else if (hVideo->nProfile == 1)
				n->c->profile = FF_PROFILE_HEVC_MAIN_10;
			else if (hVideo->nProfile == 2)
				n->c->profile = FF_PROFILE_HEVC_MAIN_STILL_PICTURE;
			else if (hVideo->nProfile == 3)
				n->c->profile = FF_PROFILE_HEVC_REXT;
			else
				n->c->profile = FF_PROFILE_HEVC_MAIN;

#define	HEVC_LEVEL(a)	((DWORD32)((a)*30))
			if (hVideo->dwHeight <= 320 && hVideo->dwWidth <= 240)
				n->c->level = HEVC_LEVEL(2);
			else if (hVideo->dwHeight <= 720 && hVideo->dwWidth <= 480)
				n->c->level = HEVC_LEVEL(3);
			else if (hVideo->dwHeight <= 1280 && hVideo->dwWidth <= 720)
				n->c->level = HEVC_LEVEL(3.1);
			else if (hVideo->dwHeight <= 1920 && hVideo->dwWidth <= 1080)
				n->c->level = HEVC_LEVEL(4.1);
			else if (hVideo->dwHeight <= 4096 && hVideo->dwWidth <= 2160)
				n->c->level = HEVC_LEVEL(5.1);
			else
				n->c->level = HEVC_LEVEL(6.1);

			// zero latency
			if (hVideo->nTune == 3)
			{
				hVideo->dwBFrame = 0;
				n->c->max_b_frames = 0;
				n->c->thread_count = 1;
			}
			// Set multi threads
			else if (hVideo->dwVidEncThread <= 12)
				n->c->thread_count = hVideo->dwVidEncThread;
			else
				n->c->thread_count = 4;

			switch (hVideo->dwRCMode)
			{
			case	0:		// CQP
				// set qp value
				n->c->bit_rate = 0;
				pszOpt += sprintf_s(pszOpt, sizeof(szOption) / 2, "qp=%d:", (int)(hVideo->dwQualityFactor / 1000.));
				break;
			case	1:		// CRF
				// set rf value
				n->c->bit_rate = 0;
				pszOpt += sprintf_s(pszOpt, sizeof(szOption) / 2, "crf=%5.3f:", (float)hVideo->dwQualityFactor / 1000.);
				break;
			default:		// ABR
				// bitrate implies ABR mode
				//pszOpt += sprintf_s(pszOpt, sizeof(szOption)/2, "bitrate=%d:", (int)(hVideo->m_dwBitRate/1000) );
				break;
			}

			if (hVideo->BSType == MP4_TYPE)
			{
				n->bIsMP4 = TRUE;
				// no sei info
				//lRet = av_opt_set(n->c->priv_data, "x265-params", "no-info=1:", 0);
				pszOpt += sprintf_s(pszOpt, sizeof(szOption) / 2, "aud=0:repeat-headers=0:info=0:annexb=1:");
				n->c->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;			// get global decoder config info
			}
			else
			{
				// let each frame start with AUD
				// repeat vps, sps and pps at the start of each key frame
				// no sei info
				pszOpt += sprintf_s(pszOpt, sizeof(szOption) / 2, "aud=1:repeat-headers=1:info=0:annexb=1:");
			}

			pszOpt += sprintf_s(pszOpt, sizeof(szOption) / 2, "profile=%d:", (int)n->c->profile);
			pszOpt += sprintf_s(pszOpt, sizeof(szOption) / 2, "keyint=%d:", (int)(hVideo->fIntraFrameRefresh * hVideo->dwFrameRate / 1000));
			pszOpt += sprintf_s(pszOpt, sizeof(szOption) / 2, "bframes=%d:", hVideo->dwBFrame);
			pszOpt += sprintf_s(pszOpt, sizeof(szOption) / 2, "no-scenecut:");
			//pszOpt += sprintf_s(pszOpt, sizeof(szOption)/2, "fps=%d/%d:", (int)hVideo->m_dwFrameRate, 1000);

			// Probably for RTS
			if (hVideo->bSpeedOptimization == FALSE)
			{
				if (bThreadSliceSupported)
					n->c->thread_type = FF_THREAD_SLICE;
				else if (bThreadFrameSupported)
					n->c->thread_type = FF_THREAD_FRAME;

				LOG_E("[FFMpegVideoEncoderOpen]	FFMPEG H265 : It's working in delay optimization mode");
			}
			else
			{
				if (bThreadFrameSupported)
					n->c->thread_type = FF_THREAD_FRAME;
				else if (bThreadSliceSupported)
					n->c->thread_type = FF_THREAD_SLICE;
			}
			pszOpt += sprintf_s(pszOpt, sizeof(szOption) / 2, "threads=%d:", n->c->thread_count);


			n->c->time_base.num = 1000;
			n->c->time_base.den = hVideo->dwFrameRate;

			lRet = av_opt_set(n->c->priv_data, "x265-params", szOption, 0);
			if (lRet != 0)
			{
				LOG_E("[FFMpegVideoEncoderOpen]	FFMPEG H265 [error]: H265_param_parse failed  - crf");
				return NULL;
			}
		}

	//if (avcodec_open(n->c, codec) < 0) {
	if (avcodec_open2(n->c, codec, NULL) < 0) {
		FFVideoEncClose((VOID*)n);
		return	NULL;
	}

	n->input_frame = av_frame_alloc();
	n->input_frame->format = n->c->pix_fmt;
	n->input_frame->width = n->c->width;
	n->input_frame->height = n->c->height;

	// the image can be allocated by any means and av_image_alloc() is
	// just the most convenient way if av_malloc() is to be used
	//ret = (INT32)av_image_alloc(n->output_frame->data, n->output_frame->linesize, n->c->width, n->c->height, n->c->pix_fmt, 32);
	//if (ret < 0)
	//{
	//	FFMpegAudioEncoderClose((VOID *)n);
	//	return	NULL;
	//}

	FFVideoEncGetInfo(n, hVideo);

	/*if (!logfp)
	{
#ifdef __FFMPEG_DEBUG__
		logfp = fopen(log_file_name, "w+t");
#endif
		if (!bSetLogCallBack)
		{
			av_log_set_callback(log_callback_help);
			bSetLogCallBack = 1;
		}
	}*/

	return	(VOID*)n;
}

VOID FFVideoEncClose(VOID* p)
{
	NewAVCodecContext* n = (NewAVCodecContext*)p;

#ifdef __FFMPEG_DEBUG__
	if (logfp)
	{
		fclose(logfp);
		logfp = NULL;
	}
#endif

	if (n == NULL)
		return;

	avcodec_close(n->c);
	av_free(n->c);
	if (n->output_temp)
		av_free((VOID*)n->output_temp);
	if (n->new_config_data)
		av_free((VOID*)n->new_config_data);
	if (n->input_frame)
	{
		//av_freep(&n->input_frame->data[0]);
		av_frame_free(&n->input_frame);
	}
	av_free(n);

	return;
}

// static DWORD32 search_aud(BYTE* pData, DWORD32 dwData)
// {
// 	DWORD32		dwPos = 0;

// 	while (dwPos + 3 <= dwData)
// 	{
// 		if (pData[0] == 0 && pData[1] == 0 && pData[2] == 1)
// 		{
// 			if ((pData[3] & 0x1F) == 9)			// Aud
// 			{
// 				if (dwPos && pData[-1] == 0)
// 					dwPos--;
// 				return dwPos;
// 			}
// 		}
// 		dwPos++;
// 		pData++;
// 	}

// 	return (DWORD32)0;
// }

BOOL FFVideoEncEncode(VOID* p, FrRawVideo* pVideo, FrMediaStream* pData, BYTE* pBitstream, INT32 nBitstream)
{
	NewAVCodecContext* n = (NewAVCodecContext*)p;
	AVPacket* pkt = &n->avpkt;
	AVFrame* frame = n->input_frame;
	INT32				got_packet;
	BOOL				bRet = FALSE;

	av_init_packet(pkt);
	pkt->data = NULL;
	pkt->size = 0;

	if (p && pVideo && pData && pBitstream && nBitstream) {
		// memcpy pImag into n->output_frame
		frame->data[0] = pVideo->pY;
		frame->data[1] = pVideo->pU;
		frame->data[2] = pVideo->pV;
		frame->linesize[0] = pVideo->dwPitch;
		frame->linesize[1] = pVideo->dwPitch / 2;
		frame->linesize[2] = pVideo->dwPitch / 2;
		//frame.colorspace = AVCOL_SPC_SMPTE170M;
		frame->pts = pVideo->dwCTS;
		// key frame
		//frame->key_frame = pVideo->m_tFrameType == FRAME_I;
		frame->pict_type = pVideo->tFrameType == FRAME_I ? AV_PICTURE_TYPE_I : AV_PICTURE_TYPE_NONE;

		int ret;
		ret = avcodec_send_frame(n->c, frame);
		if (ret < 0) {
			LOG_E("Error sending a frame for encoding");
			return FALSE;
		}
		while (ret >= 0) {
			// ���ڵ��� ����� �޾ƿ�
			ret = avcodec_receive_packet(n->c, pkt);
			// mp2������ �ʿ���� ���� üũ������, h264������ �ʿ���
			// �ش� ������ ���μ��� ���ᰡ �ƴ϶�, �ѹ� �� ó���ؾ� ���� ��Ÿ��
			if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
				return FALSE;
			else if (ret < 0) {
				LOG_E("Error during encoding");
				return FALSE;
			}
			//printf("Write packet %3"PRId64" (size=%5d)\n", pkt->pts, pkt->size);
			
			//pkt�� �޸𸮸� ������
			//av_packet_unref(pkt);

			// add 0604
			got_packet = 1;
			break;
		}

		// deprecated..
		/*if ((nRet = avcodec_encode_video2(n->c, pkt, frame, &got_packet)) < 0)
		{
			TRACE("[FFMPEG] FFMpegVideoEncoderEncode - video encoding failed in avcodec_encode_video2");
			return -1;
		}*/

		// To avoid DTS being negative
		if (n->nFirstCTS == MAX_DWORD / 2 && got_packet) {
			if (pkt->dts <= 0)
				n->nFirstCTS = (INT32)pkt->dts;
			else
				n->nFirstCTS = 0;
		}
			

		if (got_packet) {
			DWORD32		dwSkip = 0;

			bRet = TRUE;

			if (pkt->flags & AV_PKT_FLAG_KEY)
			{
				pData->tFrameType[0] = FRAME_I;
				if (n->bFirstFrame)	{
					//dwSkip = search_aud(pkt->data, pkt->size);
					n->bFirstFrame = FALSE;
				}
			}
			else
				pData->tFrameType[0] = FRAME_P;


			if (pkt->size) {
				if (n->bIsMP4)
					;//pData->m_dwFrameLen[0] = make_h264stream_rtp_format(pBitstream, nBitstream, pkt->data + dwSkip, pkt->size - dwSkip);
				else
				{
					pData->dwFrameLen[0] = pkt->size - dwSkip;
					memcpy(pBitstream, pkt->data + dwSkip, pkt->size - dwSkip);
				}
			}
				

			pData->dwFrameNum = 1;

			pData->pFrame = pBitstream;
			if (0 < n->c->max_b_frames) {
				pData->dwCTS = (DWORD32)pkt->pts - n->nFirstCTS;
				/*pData->m_dbCTS = (DOUBLE)pData->m_dwCTS;
				pData->m_dwDTS[0] = (DWORD32)pkt->dts - n->nFirstCTS;
				pData->m_eTimeStamp = TIMESTAMP_PTS_DTS;*/
			}
			else
			{
				pData->dwCTS = (DWORD32)pkt->dts - n->nFirstCTS;
				/*pData->m_dbCTS = (DOUBLE)pData->m_dwCTS;
				pData->m_eTimeStamp = TIMESTAMP_DTS;*/
			}

			//pData->m_ucTag[0] = 0x1;									// PTS notify
			//pData->m_dwDTS[0] = (DWORD32)pkt->pts - n->nFirstCTS;		// PTS

			LOG_D("FFVideoEncEncode: Len(%d) type(%d) pts %lld, dwCTS %d", pData->dwFrameLen[0], pData->tFrameType[0], pkt->pts, pData->dwCTS);

			//TRACE("[FFMPEG] FFMpegVideoEncoderEncode :: pts %lld dts %lld dwCTS %d", pkt->pts, pkt->dts, pData->m_dwCTS);
			//if (frame->pict_type == AV_PICTURE_TYPE_I)
			//	TRACE("[FFMPEG] FFMpegVideoEncoderEncode [INTRA] :: pts %lld dts %lld dwCTS %d", pkt->pts, pkt->dts, pData->m_dwCTS);
			//else
			//	TRACE("[FFMPEG] FFMpegVideoEncoderEncode [IN___] :: pts %lld dts %lld dwCTS %d", pkt->pts, pkt->dts, pData->m_dwCTS);;


			// deprecated..
			//av_free_packet(pkt);
		}
		else
		{
			TRACE("[FFMPEG] FFMpegVideoEncoderEncode failed (src cts %08d)", pVideo->dwCTS);
			bRet = FALSE;
			pData->dwFrameNum = 0;
			pData->dwFrameLen[0] = 0;
			if ((INT32)pData->dwCTS < 0) {
				pData->dwCTS = 0;
				//pData->m_dbCTS = 0.0;
			}
			//pData->m_eTimeStamp = TIMESTAMP_DTS;
		}
	}

	return	bRet;
}

BOOL FFVideoEncFlush(VOID* p, FrMediaStream* pData, BYTE* pBitstream, INT32 nBitstream)
{
	NewAVCodecContext* n = (NewAVCodecContext*)p;
	AVPacket* pkt = &n->avpkt;
	INT32	got_packet;

	av_init_packet(pkt);
	pkt->data = NULL;
	pkt->size = 0;

	got_packet = 0;

	if (p && pData && pBitstream && nBitstream)
	{
		// deprecated..
		//if (avcodec_encode_video2(n->c, pkt, NULL, &got_packet) < 0)
		{
			TRACE("[FFMPEG] FFMpegVideoEncoderFlush - video encoding failed (avcodec_encode_video2) ");
			return FALSE;
		}

		if (got_packet)
		{
			DWORD32		dwSkip = 0;

			//nRet = 0;
			if (pkt->size)
				memcpy(pBitstream, pkt->data, pkt->size);

			if (pkt->flags & AV_PKT_FLAG_KEY)
				pData->tFrameType[0] = FRAME_I;
			else
				pData->tFrameType[0] = FRAME_P;

			if (pkt->size) {
				if (n->bIsMP4)
					; //pData->m_dwFrameLen[0] = make_h264stream_rtp_format(pBitstream, nBitstream, pkt->data + dwSkip, pkt->size - dwSkip);
				else
				{
					pData->dwFrameLen[0] = pkt->size - dwSkip;
					memcpy(pBitstream, pkt->data + dwSkip, pkt->size - dwSkip);
				}
			}
				

			pData->dwFrameNum = 1;
			pData->pFrame = pBitstream;
			if (0 < n->c->max_b_frames)	{
				pData->dwCTS = (DWORD32)pkt->pts - n->nFirstCTS;
				/*pData->m_dbCTS = (DOUBLE)pData->m_dwCTS;
				pData->m_dwDTS[0] = (DWORD32)pkt->dts - n->nFirstCTS;
				pData->m_eTimeStamp = TIMESTAMP_PTS_DTS;*/
			}
			else {
				pData->dwCTS = (DWORD32)pkt->dts - n->nFirstCTS;
				/*pData->m_dbCTS = (DOUBLE)pData->m_dwCTS;
				pData->m_eTimeStamp = TIMESTAMP_DTS;*/
			}

			// deprecated..
			//av_free_packet(pkt);
		}
		else
		{
			//nRet = 1;
			pData->dwFrameNum = 0;
			pData->dwFrameLen[0] = 0;
			//pData->m_eTimeStamp = TIMESTAMP_DTS;

			return FALSE;
		}
	}

	//TRACE("[FFmpeg Audio] encoding succeeded (output bytes %d)", nRet);

	return	TRUE;
}

// static DWORD32 MakeH264ConfigForMP4(BYTE* pOut, BYTE* pIn, DWORD32 dwIn)
// {
// 	DWORD32		dwLen, dwTotalLen = 0;

// 	// eliminate start code and add three byte with followed..
// 	// first 1 byte = number of sps, pps
// 	// other 2 bytes = length of sps, pps
// 	dwLen = ((DWORD32)pIn[2] << 8) + ((DWORD32)pIn[3]);
// 	pOut[0] = 0x01;		// fixed value : 1
// 	pOut[1] = (unsigned char)(dwLen >> 8);
// 	pOut[2] = (unsigned char)dwLen;
// 	memcpy(pOut + 3, pIn + 4, dwLen);

// 	pIn += dwLen + 4;
// 	pOut += dwLen + 3;
// 	dwTotalLen = dwLen + 3;

// 	dwLen = ((DWORD32)pIn[2] << 8) + ((DWORD32)pIn[3]);
// 	pOut[0] = 0x01;		// fixed value : 1
// 	pOut[1] = (unsigned char)(dwLen >> 8);
// 	pOut[2] = (unsigned char)dwLen;
// 	memcpy(pOut + 3, pIn + 4, dwLen);
// 	dwTotalLen += dwLen + 3;

// 	return dwTotalLen;
// }

BOOL FFVideoEncGetInfo(VOID* p, FrVideoInfo* hVideo)
{
	NewAVCodecContext* n = (NewAVCodecContext*)p;
	//BYTE				pTemp[1000];

	switch (hVideo->dwFourCC)
	{
	case FOURCC_AVC1:
	case FOURCC_H264:
	case FOURCC_h264:

		// this is to be used later for IPhone RTS ?
		/*hVideo->m_dwOrgConfig = n->c->extradata_size;
		memcpy(hVideo->m_pOrgConfig, n->c->extradata, n->c->extradata_size);*/

		// get profile and level..
		hVideo->nProfile = n->c->extradata[5];
		hVideo->nCompatibility = n->c->extradata[6];
		hVideo->nLevel = n->c->extradata[7];
		// config info for MP4
		//hVideo->dwConfig = make_h264stream_rtp_format(pTemp, sizeof(hVideo->pConfig), n->c->extradata, n->c->extradata_size);
		/*if (hVideo->dwConfig == 0)
			hVideo->dwConfig = n->c->extradata_size;
		hVideo->dwConfig = MakeH264ConfigForMP4(hVideo->pConfig, pTemp, hVideo->dwConfig);*/
		hVideo->dwConfig = n->c->extradata_size;
		memcpy(hVideo->pConfig, n->c->extradata, hVideo->dwConfig);
		break;
	case FOURCC_H265:
	case FOURCC_HEVC:
		if (n->c->extradata_size)
		{
			//hVideo->dwConfig = get_hevc_config_size(n->c->extradata, n->c->extradata_size);
			if (hVideo->dwConfig)
				memcpy(hVideo->pConfig, n->c->extradata, hVideo->dwConfig);
		}
		hVideo->nProfile = n->c->profile;
		hVideo->nLevel = n->c->level;
		break;
	default:
		hVideo->dwConfig = n->c->extradata_size;
		if (n->c->extradata_size)
			memcpy(hVideo->pConfig, n->c->extradata, n->c->extradata_size);
		hVideo->nProfile = n->c->profile;
		hVideo->nLevel = n->c->level;
	}
	return	TRUE;
}

