#pragma once

#include "FFCodecAPI.h"

#ifdef WIN32

#define	inline		__inline

#ifdef __cplusplus
extern "C" {
#endif

#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libavutil/mem.h"
#include "libavutil/opt.h"
#include "libavutil/imgutils.h"
#include "libswscale/swscale.h"
#include "libavutil/mathematics.h"
#include "libavutil/time.h"
#include "libavfilter/avfilter.h"
#include "libavfilter/buffersrc.h"

	// FFMPEG Libray
//#pragma comment(lib, "avcodec.lib")
//#pragma comment(lib, "avformat.lib")
//#pragma comment(lib, "swscale.lib")
//#pragma comment(lib, "avutil.lib")

#ifdef __cplusplus
}
#endif


#else   // current only linux

#if __cplusplus
extern "C" {
#endif

#include "./libavcodec/avcodec.h"
#include "./libavutil/avutil.h"
#include "./libavutil/imgutils.h"
#include "./libavutil/opt.h"
#include "./libavutil/dict.h"
#include "./libavformat/avformat.h"
#include "./libswscale/swscale.h"
#include "./libavutil/buffer.h"
#include "./libavutil/error.h"
#include "./libavutil/hwcontext.h"
	//#include "./libavutil/hwcontext_qsv.h"				// Currently intel qsv not supported
#include "./libavutil/mem.h"
#include "./libavfilter/avfilter.h"
#include "./libavfilter/buffersrc.h"

#if __cplusplus
}
#endif // SUPPORT_IOS
#endif


// The different ways of decoding and managing data memory. You are not
// supposed to support all the modes in your application but pick the one most
// appropriate to your needs. Look for the use of api_mode in this example to
// see what are the differences of API usage between them
enum {
	API_MODE_OLD = 0, /* old method, deprecated */
	API_MODE_NEW_API_REF_COUNT = 1, /* new method, using the frame reference counting */
	API_MODE_NEW_API_NO_REF_COUNT = 2, /* new method, without reference counting */
};

static int api_mode = API_MODE_NEW_API_NO_REF_COUNT;

#ifndef MAKEFOURCC
#define MAKEFOURCC(ch0, ch1, ch2, ch3)  ((ch0) | ((ch1) << 8) | ((ch2) << 16) | ((ch3) << 24 ))
#endif

typedef struct DecodeContext {
	AVBufferRef* hw_device_ref;
} DecodeContext;

typedef struct NewAVCodecContext {
	AVCodecContext* c;
	AVFrame* picture;
	BYTE* cfgdata;
	BYTE* cfgdataForFree;
	UINT32				cfgdata_size;
	BYTE* new_config_data;				// for Vorbis Audio decoder :: a buffer to store a new config data
	VOID* output_temp;
	PsVideoCodecOutput	pout;
	AVPacket			avpkt;
	UINT32				isAudioDTS;
	UINT32				hasPTS;							// set to 1 when source media has pts.
	INT32				nFirstCTS;
	BOOL				bFirstFrame;					// set to 1 to identify the 1st encoded frame

	BOOL				bIsMP4;							// H265 stream is mp4 format
	UINT32				bUnknowColorFmt;
	BOOL				bSeekCheck;						// when seeked, check if the output video is keyframe or not
	BOOL				bSeek;
	AVFrame* input_frame;
	AVFrame* output_frame;
	INT64				sync_opts;						// frame number
	BYTE* pallete_decoding_frame;
	DWORD32			channels_in_info;				// # of audio channels externally input
	FLOAT* fAudioFrame;					// primary for AC3 encoding
	SHORT* wAudioFrame;					// primary for ALAC encoding to interleave planary pcm

	enum AVSampleFormat
		audio_sample_format;			// audio sample format : S16/S32 only supported currently
	UINT32				bits_per_raw_sample;			// audio sample bit suze : must be 16, 24 or 32
	UINT32				bytes_per_raw_sample;			// audio sample byte size : must be 2 or 4

	BOOL				bHWAccel;						// video : use hw decoder
	BOOL				bUseGPUMomory;					// when not set, copy a decoded frame in hw memory to system memory
	DecodeContext		decode;							// decode context for hw decoder

	AVFrame* picture_sw;					// sw frame from hw picture

	BOOL				bUseOldAPI;						// use deprecated APIs

	// flush
	BOOL			bStartFlush;

} NewAVCodecContext;

