#include "mediainfo.h"
#include "FFCodecAPI.h"
#include "FFCommon.h"

#include "FFFilterAPI.h"

#ifndef MAKEFOURCC
#define MAKEFOURCC(ch0, ch1, ch2, ch3)  ((ch0) | ((ch1) << 8) | ((ch2) << 16) | ((ch3) << 24 ))
#endif

#ifdef __FFFILTER_DEBUG__
FILE *logfp;
extern void av_log_set_callback(void (*)(void*, int, const char*, va_list));

static void log_callback_help(void* ptr, int level, const char* fmt, va_list vl)
{
    if(logfp)
	{
		vfprintf(logfp, fmt, vl);
		fflush(logfp);
	}
}
#endif

typedef struct {
    AVFilter			*hBufferSrc;		// Buffer source
    AVFilter			*hBufferSink;		// Buffer output
    AVFilterInOut		*pOutputs;			// Filter input
	AVFilterInOut		*pInputs;			// Filter output
    enum AVPixelFormat	pPixFmts[2];			// Pixel Format
	AVFilterGraph		*hFilterGraph;		// Filter graph
	AVFilterContext		*hBufferSinkCtx;	// Source Filter handle
	AVFilterContext		*hBufferSrcCtx;		// Sink Filter handle
	eFFFilterMode		eMode;				// Filter mode
	AVFrame				*pSrcFrame;			// Source frame
	AVFrame				*pFilterFrame;		// Filtered frame
}	FFFilterStruct;

FFFilterHandle FFFilterOpen(
	DWORD32			dwSrcFourCC,		// Source color space
	DWORD32			dwSrcWidth,			// Source width
	DWORD32			dwSrcHeight,		// Source height
	DWORD32			dwSrcTick,			// Source timestamp ticks in seconds
	eFFFilterMode	eMode				// Filter mode
)
{
	FFFilterStruct		*hFilter;
    char				szArgs[512];
    int					iRet = 0;
	enum AVPixelFormat	pPixFmt[] = { AV_PIX_FMT_YUV420P, AV_PIX_FMT_NONE };
	char				szFilterDescr[256] = "";

	hFilter = (FFFilterStruct *)MALLOCZ(sizeof (FFFilterStruct));
	if (!hFilter)
	{
		TRACE("[FFFilter] FFFilterOpen Faile - memory alloc failed");
		return NULL;
	}

	// Set Pixel Format
	switch(dwSrcFourCC)
	{
	case	FOURCC_I420:
		hFilter->pPixFmts[0] = AV_PIX_FMT_YUV420P;
		break;
	case	FOURCC_NV12:
		hFilter->pPixFmts[0] = AV_PIX_FMT_NV12;
		break;
	default:
		hFilter->pPixFmts[0] = AV_PIX_FMT_YUV420P;
	}
	hFilter->pPixFmts[1] = AV_PIX_FMT_NONE;

	// Set Filter Mode
    //avfilter_register_all();
	switch(eMode)
	{
	case	FFFilterMirrorHorizontal:
		sprintf_s(szFilterDescr, sizeof(szFilterDescr), "hflip");
		break;
	case	FFFilterMirrorVertical:
		sprintf_s(szFilterDescr, sizeof(szFilterDescr), "vflip");
		break;
	case	FFFilterRotate90:
		sprintf_s(szFilterDescr, sizeof(szFilterDescr), "transpose=1");
		break;
	case	FFFilterRotate180:
		sprintf_s(szFilterDescr, sizeof(szFilterDescr), "transpose=2,transpose=2");
		break;
	case	FFFilterRotate270:
		sprintf_s(szFilterDescr, sizeof(szFilterDescr), "transpose=2");
		break;
	default:
		TRACE("[FFFilter] FFFilterOpen Failed - unsupported file mode %d", eMode);
		goto end;
	}

	hFilter->hBufferSrc  = (AVFilter *)avfilter_get_by_name("buffer");
	hFilter->hBufferSink = (AVFilter *)avfilter_get_by_name("buffersink");
	hFilter->pInputs  = avfilter_inout_alloc();
	hFilter->pOutputs = avfilter_inout_alloc();

	hFilter->hFilterGraph = avfilter_graph_alloc();
    if (!hFilter->pInputs || !hFilter->pOutputs || !hFilter->hFilterGraph)
	{
        TRACE("[FFFilter] FFFilterOpen Failed - filter alloc failed");
        goto end;
    }

	// filter setting
    sprintf_s(szArgs, sizeof(szArgs),
            "video_size=%dx%d:pix_fmt=%d:time_base=%d/%d:pixel_aspect=%d/%d",
            dwSrcWidth, dwSrcHeight, hFilter->pPixFmts[0],
            1, dwSrcTick,
            1, 1);

    // video source buffer
	iRet = avfilter_graph_create_filter(&hFilter->hBufferSrcCtx, hFilter->hBufferSrc, "in", szArgs, NULL, hFilter->hFilterGraph);
    if (iRet < 0)
	{
        TRACE("[FFFilter] FFFilterOpen Failed - Cannot create buffer for source");
        goto end;
    }

    // video sink buffer to terminate the filter chain
	iRet = avfilter_graph_create_filter(&hFilter->hBufferSinkCtx, hFilter->hBufferSink, "out", NULL, NULL, hFilter->hFilterGraph);
    if (iRet < 0)
	{
        TRACE("[FFFilter] FFFilterOpen Failed - Cannot create buffer for sink");
        goto end;
    }

    iRet = av_opt_set_int_list(hFilter->hBufferSinkCtx, "pix_fmts", hFilter->pPixFmts, AV_PIX_FMT_NONE, AV_OPT_SEARCH_CHILDREN);
    if (iRet < 0)
	{
        TRACE("[FFFilter] FFFilterOpen Failed - Cannot set output pixel format");
        goto end;
    }

    // Set the endpoints for the filter graph. The filter_graph will be linked to the graph described by filters_descr.
    // The buffer source output must be connected to the input pad of
    // the first filter described by filters_descr; since the first
    // filter input label is not specified, it is set to "in" by
    // default.
	hFilter->pOutputs->name       = av_strdup("in");
	hFilter->pOutputs->filter_ctx = hFilter->hBufferSrcCtx;
	hFilter->pOutputs->pad_idx   = 0;
    hFilter->pOutputs->next       = NULL;

    // The buffer sink input must be connected to the output pad of
    // the last filter described by filters_descr; since the last
    // filter output label is not specified, it is set to "out" by
    // default.
	hFilter->pInputs->name       = av_strdup("out");
	hFilter->pInputs->filter_ctx = hFilter->hBufferSinkCtx;
    hFilter->pInputs->pad_idx    = 0;
    hFilter->pInputs->next       = NULL;

	if ((iRet = avfilter_graph_parse_ptr(hFilter->hFilterGraph, szFilterDescr, &hFilter->pInputs, &hFilter->pOutputs, NULL)) < 0)
        goto end;

    if ((iRet = avfilter_graph_config(hFilter->hFilterGraph, NULL)) < 0) {
		goto end;
	}
        

	if (!(hFilter->pSrcFrame = av_frame_alloc()) )
	{
        TRACE("[FFFilter] FFFilterOpen Failed - Failed to alloc src frame");
        goto end;
    }

	hFilter->pSrcFrame->width = dwSrcWidth;
	hFilter->pSrcFrame->height = dwSrcHeight;
	hFilter->pSrcFrame->format = hFilter->pPixFmts[0];

	if (!(hFilter->pFilterFrame = av_frame_alloc()) )
	{
        TRACE("[FFFilter] FFFilterOpen Failed - Failed to alloc src frame");
        goto end;
    }

	return	(FFFilterHandle)hFilter;

end:
	FFFilterClose((FFFilterHandle)hFilter);

	return	(FFFilterHandle)NULL;
}

VOID FFFilterClose(FFFilterHandle h)
{
	FFFilterStruct		*hFilter = (FFFilterStruct*)h;

	if (hFilter)
	{
		if (hFilter->pInputs)
			avfilter_inout_free(&hFilter->pInputs);
		if (hFilter->pOutputs)
			avfilter_inout_free(&hFilter->pOutputs);
		if (hFilter->hFilterGraph)
			avfilter_graph_free(&hFilter->hFilterGraph);
		if (hFilter->pSrcFrame)
			av_frame_free(&hFilter->pSrcFrame);
		if (hFilter->pFilterFrame)
			av_frame_free(&hFilter->pFilterFrame);
		FREE(hFilter);
	}

#ifdef __FFFILTER_DEBUG__
	fclose(logfp);
#endif
}

LRSLT FFFilterFrame(
	FFFilterHandle	h,				// Handle of FFFilter
	DWORD32			dwSrcFourCC,	// Source color space. Valid when h is null
	DWORD32			dwSrcWidth,		// Source width
	DWORD32			dwSrcHeight,	// Source height
	DWORD32			dwSrcTick,		// Source timestamp ticks in seconds
	eFFFilterMode	eMode,			// Filter mode. End of Valid

	BYTE			*ppSrc[],		// Pointers to Source Image
	DWORD32			pSrcPitch[],	// Pitches of Source Image
	BYTE			*ppDst[],		// Pointers to Output Image
	DWORD32			pDstPitch[]		// Pitchs of Output Image
)
{
	FFFilterStruct		*hFilter = (FFFilterStruct*)h;
	LRSLT				lRet = FR_OK;

	if (hFilter==NULL)
	{
		TRACE("[FFFilter] FFFilterFrame Failed - null filter handle");
		return COMMON_ERR_NULLPOINTER;
	}

    // push a frame into the filtergraph
	hFilter->pSrcFrame->data[0] = ppSrc[0];
	hFilter->pSrcFrame->data[1] = ppSrc[1];
	hFilter->pSrcFrame->data[2] = ppSrc[2];
	hFilter->pSrcFrame->data[3] = NULL;

	hFilter->pSrcFrame->linesize[0] = pSrcPitch[0];
	hFilter->pSrcFrame->linesize[1] = pSrcPitch[1];
	hFilter->pSrcFrame->linesize[2] = pSrcPitch[2];
	hFilter->pSrcFrame->linesize[3] = 0;

	lRet = av_buffersrc_add_frame_flags(hFilter->hBufferSrcCtx, hFilter->pSrcFrame, AV_BUFFERSRC_FLAG_KEEP_REF);
    if (lRet < 0)
	{
        TRACE("[FFFilter] FFFilterFrame Failed - Error while feeding the filtergraph");
        goto end;
    }

    // pull filtered frames from the filtergraph
	lRet = av_buffersink_get_frame(hFilter->hBufferSinkCtx, hFilter->pFilterFrame);
    if (lRet < 0)
	{
		TRACE("[FFFilter] FFFilterFrame Failed - Error while feeding the filtergraph");
		goto end;
	}

	av_image_copy(ppDst, (int*)pDstPitch, (const uint8_t **)hFilter->pFilterFrame->data, hFilter->pFilterFrame->linesize,
		hFilter->pPixFmts[0], hFilter->pFilterFrame->width, hFilter->pFilterFrame->height);

    av_frame_unref(hFilter->pFilterFrame);
    //av_frame_unref(hFilter->pSrcFrame);
#if 0
	{
		FILE	*fp = fopen("D:\\source.yuv", "ab");
		fwrite(ppSrc[0], 1, pSrcPitch[0]*hFilter->pSrcFrame->height, fp);
		fwrite(ppSrc[1], 1, pSrcPitch[1]*hFilter->pSrcFrame->height/2, fp);
		fwrite(ppSrc[2], 1, pSrcPitch[2]*hFilter->pSrcFrame->height/2, fp);
		fclose(fp);
	}
#endif
#if 0
	{
		FILE	*fp = fopen("D:\\filter.yuv", "ab");
		fwrite(ppDst[0], 1, pDstPitch[0]*hFilter->pFilterFrame->height, fp);
		fwrite(ppDst[1], 1, pDstPitch[1]*hFilter->pFilterFrame->height/2, fp);
		fwrite(ppDst[2], 1, pDstPitch[2]*hFilter->pFilterFrame->height/2, fp);
		fclose(fp);
	}
#endif

end:
	return lRet;
}

LRSLT FFFilterFrame2(
	FFFilterHandle	h,				// Handle of FFFilter
	DWORD32			dwSrcFourCC,	// Source color space. Valid when h is null
	DWORD32			dwSrcWidth,		// Source width
	DWORD32			dwSrcHeight,	// Source height
	DWORD32			dwSrcTick,		// Source timestamp ticks in seconds
	eFFFilterMode	eMode,			// Filter mode. End of Valid
	BYTE			*ppSrc[],		// Pointers to Source Image
	DWORD32			pSrcPitch[],	// Pitches of Source Image
	BYTE			*ppDst[],		// Pointers to Output Image
	DWORD32			pDstPitch[]		// Pitchs of Output Image
)
{
	FFFilterStruct		*hFilter = (FFFilterStruct*)h;
	LRSLT				lRet = FR_OK;

	if (h==NULL)
		//hFilter = FFFilterOpen(dwSrcFourCC, dwSrcWidth, dwSrcHeight, dwSrcTick, eMode);

	// Alloc
	if (hFilter==NULL)
	{
		TRACE("[FFFilter] FFFilterFrame Failed - Failed to alloc Filter handle");
		return COMMON_ERR_MEDIAPARAM;
	}

    // push a frame into the filtergraph
	hFilter->pSrcFrame->data[0] = ppSrc[0];
	hFilter->pSrcFrame->data[1] = ppSrc[1];
	hFilter->pSrcFrame->data[2] = ppSrc[2];
	hFilter->pSrcFrame->data[3] = NULL;

	hFilter->pSrcFrame->linesize[0] = pSrcPitch[0];
	hFilter->pSrcFrame->linesize[1] = pSrcPitch[1];
	hFilter->pSrcFrame->linesize[2] = pSrcPitch[2];
	hFilter->pSrcFrame->linesize[3] = 0;

	lRet = av_buffersrc_add_frame_flags(hFilter->hBufferSrcCtx, hFilter->pSrcFrame, AV_BUFFERSRC_FLAG_KEEP_REF);
    if (lRet < 0)
	{
        TRACE("[FFFilter] FFFilterFrame Failed - Error while feeding the filtergraph");
        goto end;
    }

    // pull filtered frames from the filtergraph
	lRet = av_buffersink_get_frame(hFilter->hBufferSinkCtx, hFilter->pFilterFrame);
    if (lRet < 0)
	{
		TRACE("[FFFilter] FFFilterFrame Failed - Error while feeding the filtergraph");
		goto end;
	}
end:
	// Free
	if (h==NULL)
	{
		FFFilterClose((FFFilterHandle)hFilter);
	}
	return lRet;
}

FFFilterHandle FFFilterOpenEx(
	DWORD32			dwSrcFourCC,		// Source color space
	DWORD32			dwSrcWidth,			// Source width
	DWORD32			dwSrcHeight,		// Source height
	DWORD32			dwSrcTick,			// Source timestamp ticks in seconds
	DWORD32			dwBlur				// Blur level
)
{
	FFFilterStruct		*hFilter;
    char				szArgs[512];
    int					iRet = 0;
	enum AVPixelFormat	pPixFmt[] = { AV_PIX_FMT_YUV420P, AV_PIX_FMT_NONE };
	char				szFilterDescr[256] = "";

	// blur level..
	DWORD32	dwMaxLuma, dwMaxChroma, dwBoxBlur, dwBoxChroma;

	TRACE("[FFFilter] FFFilterOpenEx - W(%d), H(%d), Blur(%d)", dwSrcWidth, dwSrcHeight, dwBlur);


	hFilter = (FFFilterStruct *)MALLOCZ(sizeof (FFFilterStruct));
	if (!hFilter)
	{
		TRACE("[FFFilter] FFFilterOpen Faile - memory alloc failed");
		return NULL;
	}

	// Set Pixel Format
	switch(dwSrcFourCC)
	{
	case	FOURCC_I420:
		hFilter->pPixFmts[0] = AV_PIX_FMT_YUV420P;
		break;
	case	FOURCC_NV12:
		hFilter->pPixFmts[0] = AV_PIX_FMT_NV12;
		break;
	default:
		hFilter->pPixFmts[0] = AV_PIX_FMT_YUV420P;
	}
	hFilter->pPixFmts[1] = AV_PIX_FMT_NONE;

	// Set Filter Mode
    //avfilter_register_all();

	TRACE("[FFFilter] FFFilterOpenEx - avfilter_register_all() success..");

	dwMaxLuma = MIN(dwSrcWidth, dwSrcHeight) / 2;
	TRACE("[FFFilter] FFFilterOpenEx - dwMaxLuma(%d)", dwMaxLuma);
	if (dwMaxLuma  > 0) {
		dwMaxChroma = dwMaxLuma / 2;
		TRACE("[FFFilter] FFFilterOpenEx - dwMaxChroma(%d)", dwMaxChroma);
		dwBoxBlur = (DWORD32)(((float)dwBlur / 100.) * dwMaxLuma);
		TRACE("[FFFilter] FFFilterOpenEx - dwMaxLuma(%d), dwMaxChroma(%d), Blur(%d)", dwMaxLuma, dwMaxChroma, dwBoxBlur);
		dwBoxChroma = dwBoxBlur / 2;
		if (dwBoxChroma < 5)
			dwBoxChroma = 5; //default..

		TRACE("[FFFilter] FFFilterOpenEx - dwBoxChroma(%d)", dwBoxChroma);
	}

	//sprintf_s(szFilterDescr, sizeof(szFilterDescr), "boxblur=200:1:cr=100:ar=100");
	sprintf_s(szFilterDescr, sizeof(szFilterDescr), "boxblur=%d:1:cr=%d:ar=%d", dwBoxBlur, dwBoxChroma, dwBoxBlur);
	TRACE("[FFFilter] FFFilterOpenEx - Filter Desc(%s)", szFilterDescr);
/*
	switch(eMode)
	{
	case	FFFilterMirrorHorizontal:
		sprintf_s(szFilterDescr, sizeof(szFilterDescr), "hflip");
		break;
	case	FFFilterMirrorVertical:
		sprintf_s(szFilterDescr, sizeof(szFilterDescr), "vflip");
		break;
	case	FFFilterRotate90:
		sprintf_s(szFilterDescr, sizeof(szFilterDescr), "transpose=1");
		break;
	case	FFFilterRotate180:
		sprintf_s(szFilterDescr, sizeof(szFilterDescr), "transpose=2,transpose=2");
		break;
	case	FFFilterRotate270:
		sprintf_s(szFilterDescr, sizeof(szFilterDescr), "transpose=2");
		break;
	case FFFilterBlur:
		sprintf_s(szFilterDescr, sizeof(szFilterDescr), "boxblur=200:1:cr=100:ar=100");
		break;
	default:
		TRACE("[FFFilter] FFFilterOpen Failed - unsupported file mode %d", eMode);
		goto end;
	}
*/
	hFilter->hBufferSrc  = (AVFilter *)avfilter_get_by_name("buffer");
	hFilter->hBufferSink = (AVFilter *)avfilter_get_by_name("buffersink");
	hFilter->pInputs  = avfilter_inout_alloc();
	hFilter->pOutputs = avfilter_inout_alloc();

	hFilter->hFilterGraph = avfilter_graph_alloc();
    if (!hFilter->pInputs || !hFilter->pOutputs || !hFilter->hFilterGraph)
	{
        TRACE("[FFFilter] FFFilterOpen Failed - filter alloc failed");
        goto end;
    }

	// filter setting
    sprintf_s(szArgs, sizeof(szArgs),
            "video_size=%dx%d:pix_fmt=%d:time_base=%d/%d:pixel_aspect=%d/%d",
            dwSrcWidth, dwSrcHeight, hFilter->pPixFmts[0],
            1, dwSrcTick,
            1, 1);

    // video source buffer
	iRet = avfilter_graph_create_filter(&hFilter->hBufferSrcCtx, hFilter->hBufferSrc, "in", szArgs, NULL, hFilter->hFilterGraph);
    if (iRet < 0)
	{
        TRACE("[FFFilter] FFFilterOpen Failed - Cannot create buffer for source");
        goto end;
    }

    // video sink buffer to terminate the filter chain
	iRet = avfilter_graph_create_filter(&hFilter->hBufferSinkCtx, hFilter->hBufferSink, "out", NULL, NULL, hFilter->hFilterGraph);
    if (iRet < 0)
	{
        TRACE("[FFFilter] FFFilterOpen Failed - Cannot create buffer for sink");
        goto end;
    }

    iRet = av_opt_set_int_list(hFilter->hBufferSinkCtx, "pix_fmts", hFilter->pPixFmts, AV_PIX_FMT_NONE, AV_OPT_SEARCH_CHILDREN);
    if (iRet < 0)
	{
        TRACE("[FFFilter] FFFilterOpen Failed - Cannot set output pixel format");
        goto end;
    }

    // Set the endpoints for the filter graph. The filter_graph will be linked to the graph described by filters_descr.
    // The buffer source output must be connected to the input pad of
    // the first filter described by filters_descr; since the first
    // filter input label is not specified, it is set to "in" by
    // default.
	hFilter->pOutputs->name       = av_strdup("in");
	hFilter->pOutputs->filter_ctx = hFilter->hBufferSrcCtx;
	hFilter->pOutputs->pad_idx   = 0;
    hFilter->pOutputs->next       = NULL;

    // The buffer sink input must be connected to the output pad of
    // the last filter described by filters_descr; since the last
    // filter output label is not specified, it is set to "out" by
    // default.
	hFilter->pInputs->name       = av_strdup("out");
	hFilter->pInputs->filter_ctx = hFilter->hBufferSinkCtx;
    hFilter->pInputs->pad_idx    = 0;
    hFilter->pInputs->next       = NULL;

	if ((iRet = avfilter_graph_parse_ptr(hFilter->hFilterGraph, szFilterDescr, &hFilter->pInputs, &hFilter->pOutputs, NULL)) < 0)
        goto end;

    if ((iRet = avfilter_graph_config(hFilter->hFilterGraph, NULL)) < 0) {
		goto end;
	}

	if (!(hFilter->pSrcFrame = av_frame_alloc()) )
	{
        TRACE("[FFFilter] FFFilterOpen Failed - Failed to alloc src frame");
        goto end;
    }

	hFilter->pSrcFrame->width = dwSrcWidth;
	hFilter->pSrcFrame->height = dwSrcHeight;
	hFilter->pSrcFrame->format = hFilter->pPixFmts[0];

	if (!(hFilter->pFilterFrame = av_frame_alloc()) )
	{
        TRACE("[FFFilter] FFFilterOpen Failed - Failed to alloc src frame");
        goto end;
    }

	TRACE("[FFFilter] FFFilterOpenEx - end..");

	return	(FFFilterHandle)hFilter;

end:
	FFFilterClose((FFFilterHandle)hFilter);

	return	(FFFilterHandle)NULL;
}
