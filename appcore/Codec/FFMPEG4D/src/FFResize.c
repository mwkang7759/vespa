#include "mediainfo.h"
#include "FFCodecAPI.h"
#include "FFCommon.h"

#include "FFResizeAPI.h"

#ifndef MAKEFOURCC
#define MAKEFOURCC(ch0, ch1, ch2, ch3)  ((ch0) | ((ch1) << 8) | ((ch2) << 16) | ((ch3) << 24 ))
#endif

#ifdef __FFRESIZE_DEBUG__
FILE *logfp;
extern void av_log_set_callback(void (*)(void*, int, const char*, va_list));

static void log_callback_help(void* ptr, int level, const char* fmt, va_list vl)
{
    if(logfp)
	{
		vfprintf(logfp, fmt, vl);
		fflush(logfp);
	}
}
#endif

typedef struct {
	INT32				srcW;				// Source Width
	INT32				srcH;				// Source Height
	DWORD32				srcStartW;			// Where resize starts horizontally in the source image buffer, in case of source cropping
	DWORD32				srcStartH;			// Where resize starts vertically in the source image buffer, in case of source cropping
	enum AVPixelFormat	srcFormat;			// Source Color & Pixel Format
	BOOL				interlace;			// Source is interlace (true) or progressive (false)
	INT32				dstW;				// Output Width
	INT32				dstH;				// Output Height
	DWORD32				dstStartW;			// Where resized image starts to store horizontally in the output image buffer, in case of output padding
	DWORD32				dstStartH;			// Where resized image starts to store vertically in the source image buffer, in case of output padding
	enum AVPixelFormat	dstFormat;			// Output Color & Pixel Format
	INT32				flags;				// Flags for resize. Not used currently
	struct SwsContext*	sws;				// Handle to Resize
	BOOL				bCopy;				// Copy RGB data instead of convertion. This is walkaround for RGB 1:1 resizing problem
	DWORD32				ColorBytes;			// Bytes for RGB Color
}	FFResizeStruct;

	//av_image_fill_linesizes()
	//sws_getContext()
	//sws_scale()
	//sws_freeContext()

// video
static enum AVPixelFormat FFGetPixelFormat(DWORD32 dwFourCC, DWORD32 dwBitCount, VideoOutFormat eSubFormat)
{
	switch(dwFourCC)
	{
	case	FOURCC_RGB:
		switch(dwBitCount)
		{
		case	32:
			switch(eSubFormat)
			{
			case	VideoFmtBGRA:	return	AV_PIX_FMT_BGRA;
			case	VideoFmtABGR:	return	AV_PIX_FMT_ABGR;
			case	VideoFmtARGB:	return	AV_PIX_FMT_ARGB;
			default:				return	AV_PIX_FMT_RGBA;
			}
		case	24:
			switch(eSubFormat)
			{
			case	VideoFmtBGR24:	return	AV_PIX_FMT_BGR24;
			default:				return	AV_PIX_FMT_RGB24;
			}
		case	16:
			switch(eSubFormat)
			{
			case	VideoFmtRGB555LE:	return	AV_PIX_FMT_RGB555LE;
			case	VideoFmtBGR555LE:	return	AV_PIX_FMT_BGR555LE;
			case	VideoFmtRGB565LE:	return	AV_PIX_FMT_RGB565LE;
			case	VideoFmtBGR565LE:	return	AV_PIX_FMT_BGR565LE;
			case	VideoFmtRGB555BE:	return	AV_PIX_FMT_RGB555BE;
			case	VideoFmtBGR555BE:	return	AV_PIX_FMT_BGR555BE;
			case	VideoFmtRGB565BE:	return	AV_PIX_FMT_RGB565BE;
			case	VideoFmtBGR565BE:	return	AV_PIX_FMT_BGR565BE;
			default:					return	AV_PIX_FMT_RGB555LE;
			}
		}
		return	AV_PIX_FMT_NONE;
	case	FOURCC_RGB16:			// This is not permitted. FOURCC_RGB, dwBitCount and eRGBSubFormat should be used instead.
		return	AV_PIX_FMT_NONE;
	case	FOURCC_IYUV:
		return	AV_PIX_FMT_YUV420P;
	case	FOURCC_YUV9:
		return	AV_PIX_FMT_YUV410P;
	case	FOURCC_YUV422P:
		return	AV_PIX_FMT_YUV422P;
	case	FOURCC_I420P10:
		return	AV_PIX_FMT_YUV420P10LE;
	case	FOURCC_YUV411P:
		return AV_PIX_FMT_YUV411P;
    case   FOURCC_NV12:
        return AV_PIX_FMT_NV12;
	default:
		return	AV_PIX_FMT_NONE;		// Not Supported Pixel Format
	}

	return	AV_PIX_FMT_NONE;			// Not Supported Pixel Foramt
}

static enum AVPixelFormat FFPutPixelFormat(DWORD32 dwFourCC, DWORD32 dwBitCount, VideoOutFormat eSubFormat)
{
	switch(dwFourCC)
	{
	case	FOURCC_RGB:
		switch(dwBitCount)
		{
		case	32:
			switch(eSubFormat)
			{
			case	VideoFmtBGRA:	return	AV_PIX_FMT_BGRA;
			case	VideoFmtABGR:	return	AV_PIX_FMT_ABGR;
			case	VideoFmtARGB:	return	AV_PIX_FMT_ARGB;
			case	VideoFmtRGBA:	return	AV_PIX_FMT_RGBA;
			default:				return	AV_PIX_FMT_RGBA;
			}
		case	24:
			switch(eSubFormat)
			{
			case	VideoFmtBGR24:	return	AV_PIX_FMT_BGR24;
			default:				return	AV_PIX_FMT_RGB24;
			}
		case	16:
			switch(eSubFormat)
			{
			case	VideoFmtRGB555LE:	return	AV_PIX_FMT_RGB555LE;
			case	VideoFmtBGR555LE:	return	AV_PIX_FMT_BGR555LE;
			case	VideoFmtRGB565LE:	return	AV_PIX_FMT_RGB565LE;
			case	VideoFmtBGR565LE:	return	AV_PIX_FMT_BGR565LE;
			case	VideoFmtRGB555BE:	return	AV_PIX_FMT_RGB555BE;
			case	VideoFmtBGR555BE:	return	AV_PIX_FMT_BGR555BE;
			case	VideoFmtRGB565BE:	return	AV_PIX_FMT_RGB565BE;
			case	VideoFmtBGR565BE:	return	AV_PIX_FMT_BGR565BE;
			default:					return	AV_PIX_FMT_RGB555LE;
			}
		default:					return	AV_PIX_FMT_NONE;
		}
	case	FOURCC_RGB16:
									return	AV_PIX_FMT_RGB555LE;
	case	FOURCC_IYUV:
	case	FOURCC_YV12:
									return	AV_PIX_FMT_YUV420P;
    case    FOURCC_NV12:
                                    return AV_PIX_FMT_NV12;
	default:		// Not Supported Pixel Format
									return	AV_PIX_FMT_NONE;
	}

	return	AV_PIX_FMT_NONE;			// Not Supported Pixel Foramt
}

FFResizeHandle FFResizeOpen(
	DWORD32				dwSrcFourCC,		// Source color space
	DWORD32				dwSrcBitCount,		// # of Bits per Pixel
	VideoOutFormat		eSrcSubFormat,		// Source Color SubFormat for RGB or YUV
	BOOL				bInterlace,			// Source is interlace (true) or progressive (false)
	DWORD32				dwSrcWidth,			// Source Width
	DWORD32				dwSrcHeight,		// Source Height
	DWORD32				dwSrcStartW,		// Source Croppong Horizontal Start Position
	DWORD32				dwSrcStartH,		// Source Croppong Vertical Start Position
	DWORD32				dwDstFourCC,		// Output color space
	DWORD32				dwDstBitCount,		// Output # of Bits per Pixel
	VideoOutFormat		eDstSubFormat,		// Output Color SubFormat for RGB or YUV
	DWORD32				dwDstWidth,			// Output Width
	DWORD32				dwDstHeight,		// Output Height
	DWORD32				dwDstStartW,		// Output Padding Horizontal Start Position
	DWORD32				dwDstStartH			// Output Padding Vertical Start Position
)
{
	FFResizeStruct		*hResize;
	struct SwsContext	*hSws = NULL;
	enum AVPixelFormat	SrcFormat, DstFormat;

	// Check if Source format is supported or not
	SrcFormat = FFGetPixelFormat(dwSrcFourCC, dwSrcBitCount, eSrcSubFormat);
	if (SrcFormat == AV_PIX_FMT_NONE)
	{
		TRACE("[FFFilter] FFResize Open Failed. Unsupported Source Color Format FourCC = 0x%0x, Color Bit Depth = %d, Sub Format = %d",
			dwSrcFourCC, dwSrcBitCount, (DWORD32)SrcFormat);
		return	(FFResizeHandle)NULL;
	}

	DstFormat = FFPutPixelFormat(dwDstFourCC, dwDstBitCount, eDstSubFormat);
	if (DstFormat == AV_PIX_FMT_NONE)
	{
		TRACE("[FFFilter] FFResize Open Failed. Unsupported Output Color Format FourCC = 0x%0x, Color Bit Depth = %d, Sub Format = %d",
			dwDstFourCC, dwDstBitCount, (DWORD32)DstFormat);
		return	(FFResizeHandle)NULL;
	}

	hResize = (FFResizeStruct*)MALLOCZ(sizeof(FFResizeStruct));
	if (!hResize)
	{
		TRACE("[FFFilter] FFResize Open Failed. Alloced Memory Size is zero");
		return	(FFResizeHandle)NULL;
	}

	// Store parameters
	hResize->srcFormat = SrcFormat;
	hResize->srcW = (INT32)dwSrcWidth;
	hResize->srcH = bInterlace ? (INT32)dwSrcHeight/2 : (INT32)dwSrcHeight;

	hResize->srcStartW = dwSrcStartW;
	hResize->srcStartH = dwSrcStartH;
	hResize->dstW = (INT32)dwDstWidth;
	hResize->dstH = (INT32)dwDstHeight;
	hResize->dstStartW = dwDstStartH;
	hResize->dstStartH = dwDstStartW;
	hResize->dstFormat = DstFormat;
	hResize->flags = SWS_BILINEAR;

#if 1
	//hSws = sws_getCachedContext((struct SwsContext*)NULL,
	hSws = sws_getContext(
		hResize->srcW, hResize->srcH, hResize->srcFormat,
		hResize->dstW, hResize->dstH, hResize->dstFormat,
		hResize->flags,
		NULL, NULL, NULL);
#endif

	if (!hSws)
	{
		TRACE("[FFFilter] FFResize Open Failed. getContext() Failed");
		FREE(hResize);
		return	(FFResizeHandle)NULL;
	}

	hResize->sws = hSws;

	// No use of converting. Input and output share same color space
	hResize->bCopy = (!dwSrcFourCC) &&
		(hResize->srcW==hResize->dstW) && (hResize->srcH==hResize->dstH) &&
		(hResize->srcFormat==hResize->dstFormat);
	hResize->ColorBytes = MAX(dwSrcFourCC ? 1 : dwSrcBitCount>>3, 1);

	return	(VOID*)hResize;
}

// We confine the function by color space conversion, no resize.
FFResizeHandle FFColorConvOpen(
	FFResizeHandle		hCur,
	DWORD32				dwSrcFourCC,		// Source color space
	DWORD32				dwSrcBitCount,		// # of Bits per Pixel
	VideoOutFormat		eSrcSubFormat,		// Source Color SubFormat for RGB or YUV
	DWORD32				dwSrcWidth,			// Source Width
	DWORD32				dwSrcHeight,		// Source Height
	DWORD32				dwDstFourCC,		// Output color space
	DWORD32				dwDstBitCount,		// Output # of Bits per Pixel
	VideoOutFormat		eDstSubFormat		// Output Color SubFormat for RGB or YUV
)
{
	FFResizeStruct		*hColorConv;
	struct SwsContext	*hSws, *hCurSws=NULL;
	enum AVPixelFormat	SrcFormat, DstFormat;

	// Check if Source format is supported or not
	SrcFormat = FFGetPixelFormat(dwSrcFourCC, dwSrcBitCount, eSrcSubFormat);
	if (SrcFormat == AV_PIX_FMT_NONE)
	{
		TRACE("[FFFilter] FFColorConvOpen Open Failed. Unsupported Source Color Format FourCC = 0x%0x, Color Bit Depth = %d, Sub Format = %d",
			dwSrcFourCC, dwSrcBitCount, (DWORD32)SrcFormat);
		return	(FFResizeHandle)NULL;
	}

	DstFormat = FFPutPixelFormat(dwDstFourCC, dwDstBitCount, eDstSubFormat);
	if (DstFormat == AV_PIX_FMT_NONE)
	{
		TRACE("[FFFilter] FFColorConvOpen Open Failed. Unsupported Output Color Format FourCC = 0x%0x, Color Bit Depth = %d, Sub Format = %d",
			dwDstFourCC, dwDstBitCount, (DWORD32)DstFormat);
		return	(FFResizeHandle)NULL;
	}

	if (!hCur)
	{
		hColorConv = (FFResizeStruct*)MALLOCZ(sizeof(FFResizeStruct));
		if (!hColorConv)
		{
			TRACE("[FFFilter] FFColorConvOpen Open Failed. Alloced Memory Size is zero");
			return	(FFResizeHandle)NULL;
		}
	}
	else
	{
		hColorConv = (FFResizeStruct*)hCur;
		hCurSws = hColorConv->sws;
	}

	// Store parameters
	hColorConv->srcFormat = SrcFormat;
	hColorConv->srcW = (INT32)dwSrcWidth;
	hColorConv->srcH = (INT32)dwSrcHeight;
	hColorConv->srcStartW = 0;
	hColorConv->srcStartH = 0;
	hColorConv->dstW = (INT32)dwSrcWidth;
	hColorConv->dstH = (INT32)dwSrcHeight;
	hColorConv->dstStartW = 0;
	hColorConv->dstStartH = 0;
	hColorConv->dstFormat = DstFormat;
	hColorConv->flags = SWS_BILINEAR;

	hSws = sws_getCachedContext(hCurSws,
		hColorConv->srcW, hColorConv->srcH, hColorConv->srcFormat,
		hColorConv->dstW, hColorConv->dstH, hColorConv->dstFormat,
		hColorConv->flags,
		NULL, NULL, NULL);

	if (!hSws)
	{
		TRACE("[FFFilter] FFResize Open Failed. getContext() Failed");
		FREE(hColorConv);
		return	(FFResizeHandle)NULL;
	}

	hColorConv->sws = hSws;

	return	(VOID*)hColorConv;
}

VOID FFResizeClose(FFResizeHandle h)
{
	FFResizeStruct		*hResize = (FFResizeStruct*)h;

	if (hResize)
	{
		if (hResize->sws)
			sws_freeContext(hResize->sws);
		FREE(hResize);
	}

#ifdef __FFFILTER_DEBUG__
	fclose(logfp);
#endif
}

static VOID CopyRGB(BYTE* pchOut, DWORD32 dwOutPitch, BYTE* pchIn, DWORD32 dwWidth, DWORD32 dwHeight, DWORD32 dwInPitch, DWORD32 dwRGBBytes)
{
	DWORD32	i;

	for (i = 0;i < dwHeight;i++)
	{
		memcpy(pchOut, pchIn, dwWidth*dwRGBBytes);
		pchOut += dwOutPitch;
		pchIn += dwInPitch;
	}
}

VOID FFResizeFrame(
	FFResizeHandle h,				// Handle of FFFilter
	const BYTE* const ppSrc[],		// Pointers to Source Image
	const DWORD32 pSrcPitch[],		// Pitches of Source Image
	BYTE* const ppDst[],			// Pointers to Output Image
	const DWORD32	pDstPitch[]		// Pitches of Output Image
)
{
	INT32	iHeight;

	FFResizeStruct		*hResize = (FFResizeStruct*)h;
	if (hResize->bCopy)
		CopyRGB(ppDst[0], pDstPitch[0], (BYTE*)ppSrc[0], hResize->srcW, hResize->srcH, pSrcPitch[0], hResize->ColorBytes);
	else
		iHeight = sws_scale(hResize->sws, ppSrc, (int*)pSrcPitch, 0, hResize->srcH, ppDst, (int*)pDstPitch);

	return;
}
