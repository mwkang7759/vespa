#include "mediainfo.h"
#include "FFCodecAPI.h"
#include "FFCommon.h"


#define		NoMemory	0

# ifdef WIN32
	char log_file_name[] = "D:\\ffmpeg.log";
# else
	char log_file_name[] = "./ffmpeg.log";
# endif

#ifdef	_DEBUG
# ifdef WIN32
#  define		__FFMPEG_DEBUG__
# endif
#endif

BOOL	bSetLogCallBack;
FILE	*logfp;
extern VOID av_log_set_callback(VOID (*)(VOID*, INT32, const char*, va_list));

static VOID log_callback_help(VOID* ptr, INT32 level, const char* fmt, va_list vl)
{
#ifdef	_DEBUG
    if(logfp)
	{
		vfprintf(logfp, fmt, vl);
		fflush(logfp);
	}
#endif
	// do nothing
}

static VOID get_video_color_format(NewAVCodecContext *n, VideoOutFormat	*eVideoFormat) {
	n->bUnknowColorFmt = FALSE;
	switch(n->c->pix_fmt) {
	case	AV_PIX_FMT_YUV420P:			*eVideoFormat = VideoFmtYUV420P;	break;
	case	AV_PIX_FMT_RGB555LE:		*eVideoFormat = VideoFmtRGB555LE;	break;
	case	AV_PIX_FMT_BGR555LE:		*eVideoFormat = VideoFmtBGR555LE;	break;
	case	AV_PIX_FMT_RGB565LE:		*eVideoFormat = VideoFmtRGB565LE;	break;
	case	AV_PIX_FMT_BGR565LE:		*eVideoFormat = VideoFmtBGR565LE;	break;

	case	AV_PIX_FMT_RGB555BE:		*eVideoFormat = VideoFmtRGB555BE;	break;
	case	AV_PIX_FMT_BGR555BE:		*eVideoFormat = VideoFmtBGR555BE;	break;
	case	AV_PIX_FMT_RGB565BE:		*eVideoFormat = VideoFmtRGB565BE;	break;
	case	AV_PIX_FMT_BGR565BE:		*eVideoFormat = VideoFmtBGR565BE;	break;

	case	AV_PIX_FMT_RGBA:			*eVideoFormat = VideoFmtRGBA;		break;
	case	AV_PIX_FMT_BGRA:			*eVideoFormat = VideoFmtBGRA;		break;
	case	AV_PIX_FMT_ABGR:			*eVideoFormat = VideoFmtABGR;		break;
	case	AV_PIX_FMT_ARGB:			*eVideoFormat = VideoFmtARGB;		break;

	case	AV_PIX_FMT_RGB24:			*eVideoFormat = VideoFmtRGB24;		break;
	case	AV_PIX_FMT_BGR24:			*eVideoFormat = VideoFmtBGR24;		break;

	case	AV_PIX_FMT_YUV410P:			*eVideoFormat = VideoFmtYUV410P;	break;
	case	AV_PIX_FMT_YUV411P:			*eVideoFormat = VideoFmtYUV411P;	break;
	case	AV_PIX_FMT_YUVJ422P:		*eVideoFormat = VideoFmtYUV422P;	break;
	case	AV_PIX_FMT_YUV420P10LE:		*eVideoFormat = VideoFmtYUV420P10LE;	break;
	case	AV_PIX_FMT_YUV422P10LE:		*eVideoFormat = VideoFmtYUV422P10LE;	break;
	case	AV_PIX_FMT_PAL8:			*eVideoFormat = VideoFmtBGR24;			break;			// RGBQuad is an order of GBRA
	case	AV_PIX_FMT_NONE:			*eVideoFormat = VideoFmtUnknown;
			n->bUnknowColorFmt = TRUE;
			break;
	case	AV_PIX_FMT_BGR0:			*eVideoFormat = VideoFmtBGRA;		break;
	case	AV_PIX_FMT_NV12:			*eVideoFormat = VideoFmtNV12;		break;
	case	AV_PIX_FMT_NV21:			*eVideoFormat = VideoFmtNV21;		break;
	case	AV_PIX_FMT_QSV:				// QSV
			if (n->bUseGPUMomory)
				*eVideoFormat = VideoFmtQSV;
			else						
				*eVideoFormat = VideoFmtNV12;		
			break;
	default:	*eVideoFormat = VideoFmtYUV420P;	break;
	}
}

// video
VOID * FFVideoDecOpen(
	UINT32 codec_type,
	BYTE *config_data,
	UINT32 config_size,
	UINT32 frame_height,
	UINT32 frame_width,
	UINT32 color_bits,
	VideoOutFormat	*eVideoFormat,
	FrTimeStamp eMode,
	FrVideoInfo *hVideo
)
{
	NewAVCodecContext	*n=NULL;
    AVCodec				*codec=NULL;
	enum				AVCodecID CodecID;
	//UINT32				codec_tag = 0;
	AVDictionary		*opts = NULL;
	BOOL				bSeekCheck = FALSE;
	//int					ret=0;


//    AVPacket avpkt;
	switch(codec_type)
	{
	case	FOURCC_H264:
	case	FOURCC_h264:
	case 	FOURCC_AVC1:
		CodecID = AV_CODEC_ID_H264;
		break;
	case FOURCC_PNG:
		CodecID = AV_CODEC_ID_PNG;
		break;
	case FOURCC_H265:
	case FOURCC_HEVC:
		CodecID = AV_CODEC_ID_HEVC;
		break;
	default : return	NULL;
	}

	// allocation
	n = (NewAVCodecContext *)av_mallocz(sizeof(NewAVCodecContext));

	//n->bHWAccel = TRUE;
	//n->bUseGPUMomory = TRUE;

	// register all the codecs
    //avcodec_register_all();	// deprecated..

	// find the video decoder
	codec = avcodec_find_decoder(CodecID);
	n->bHWAccel = FALSE;
	if (!codec) {
		TRACEX(FDR_LOG_ERROR, "Cannot find codec %d", CodecID);
		goto clean_up;
	}

	n->c = avcodec_alloc_context3(codec);

	av_init_packet(&(n->avpkt));

	//n->picture = avcodec_alloc_frame();
	n->picture = av_frame_alloc();

	if (n->bHWAccel)
		n->picture_sw = av_frame_alloc();

	// allco failed
	if( !n || !n->c || !n->picture)
	{
		TRACEX(FDR_LOG_ERROR, "Memory alloc failed");
		goto clean_up;
	}


	/* init values
	static INT32 seek_by_bytes=-1;
	static INT32 display_disable;
	static INT32 show_status = 1;
	static INT32 debug = 0;
	static INT32 debug_mv = 0;
	static INT32 step = 0;
	static INT32 thread_count = 1;
	static INT32 workaround_bugs = 1;
	static INT32 fast = 0;
	static INT32 genpts = 0;
	static INT32 lowres = 0;
	static INT32 idct = FF_IDCT_AUTO; This is 0
	static enum AVDiscard skip_frame= AVDISCARD_DEFAULT;
	static enum AVDiscard skip_idct= AVDISCARD_DEFAULT;
	static enum AVDiscard skip_loop_filter= AVDISCARD_DEFAULT;
	static INT32 error_recognition = AV_EF_CAREFUL;
	static INT32 error_concealment = 3;
	static INT32 decoder_reorder_pts= -1;
	static INT32 autoexit;
	static INT32 loop=1;
	static INT32 framedrop=1;
	static INT32 rdftspeed=20;
	*/

	n->bSeekCheck = bSeekCheck;						// codec is a mpeg4 variant
	
	//if(fast) n->c->flags2 |= CODEC_FLAG2_FAST;
	n->c->debug_mv = 0;
    n->c->debug = 0; //FF_DEBUG_BUGS;
    n->c->workaround_bugs = FF_BUG_AUTODETECT;
    n->c->lowres = 0;

    n->c->idct_algo = FF_IDCT_AUTO;
    n->c->skip_frame = AVDISCARD_DEFAULT;
    n->c->skip_idct = AVDISCARD_DEFAULT;
    n->c->skip_loop_filter= AVDISCARD_DEFAULT;
    //n->c->err_recognition= AV_EF_CRCCHECK; //AV_EF_CAREFUL;
    n->c->error_concealment= FF_EC_GUESS_MVS | FF_EC_DEBLOCK;

	// config info
	n->c->extradata = n->cfgdata = config_data;
	n->c->extradata_size = n->cfgdata_size = config_size;
	// copy sizes into ....
	n->c->coded_height = frame_height;
	n->c->coded_width = frame_width;
	n->c->height = frame_height;
	n->c->width = frame_width;
	// color depth
	n->c->bits_per_coded_sample = color_bits;
	// codec tag
	n->c->codec_tag = codec_type;


	if (CodecID == AV_CODEC_ID_H264) {
		if (0 < (INT32)config_size) {
			//INT32	i, num, size, wr_pos, sps_size=0, loc;
			//BYTE	*p, *pCfg;

			// make config struct

			// all config data
			if (config_data[0] == 0 && config_data[1] == 0 && config_data[2] == 0 && config_data[3] == 1) {
				n->cfgdata_size = config_size;
				n->cfgdata = (BYTE *)av_mallocz(n->cfgdata_size+AV_INPUT_BUFFER_PADDING_SIZE);
				n->cfgdataForFree = n->cfgdata;
				memcpy(n->cfgdata, config_data, config_size);
			}
			else
			if (config_data[0] == 1 && ((config_data[5] & 0x1F) == 0x1)) {
				n->cfgdata_size = config_size;
				n->cfgdata = (BYTE *)av_mallocz(n->cfgdata_size+AV_INPUT_BUFFER_PADDING_SIZE);
				n->cfgdataForFree = n->cfgdata;
				memcpy(n->cfgdata, config_data, config_size);
			}
			else
			{
				//n->cfgdata_size = config_size+7;		// 7 bytes added for FFMPEG H.264
				//n->cfgdata = (BYTE *)av_mallocz(n->cfgdata_size+AV_INPUT_BUFFER_PADDING_SIZE);
				//n->cfgdataForFree = n->cfgdata;
				//pCfg = config_data;

				//// find # of sps
				//num = 0; p = config_data; loc=0; size=0;
				//while(loc<(INT32)config_size) {
				//	if ( (p[2] & 0xF) == 7)					// sps
				//		num++;
				//	else
				//		break;
				//	size = (p[0] << 8) | (p[1]) + 2;
				//	p += size;
				//	loc += size;
				//}

				//if (num && loc <= config_size) {
				//	// header for ffmpeg config (6B)
				//	n->cfgdata[0] = 1;		// start byte should be 1. This is configurationVersion in avcC atom
				//	if (0 < hVideo->m_dwAVCNalLengthType && hVideo->m_dwAVCNalLengthType <= 4)
				//		n->cfgdata[4] = hVideo->m_dwAVCNalLengthType - 1;
				//	else
				//		n->cfgdata[4] = 3;		// 3+1 is the size of nal size in bytes except for sps and pps (2 bytes)

				//	// filling sps
				//	n->cfgdata[5] = num;	// # of sps
				//	wr_pos = 6;
				//	for(i=0, loc=6; i<num; i++)	{
				//		size = (pCfg[0] << 8) | (pCfg[1]) + 2;
				//		// copy sps first inclusive of 2 bytes nal size
				//		memcpy(n->cfgdata + wr_pos, pCfg, size);
				//		pCfg += size;
				//		sps_size += size;
				//		wr_pos += size;
				//	}

				//	// find # of pps
				//	num = 0; p = pCfg; loc = 0;
				//	while(loc<(INT32)config_size-sps_size) {
				//		if ( (p[2] & 0xF) == 8)					// sps
				//			num++;
				//		else
				//			break;
				//		size = (p[0] << 8) | (p[1]) + 2;
				//		p += size;
				//		loc += size;
				//	}

				//	// filling pps
				//	n->cfgdata[wr_pos++] = num;	// # of sps
				//	for(i=0, loc=6; i<num; i++)	{
				//		size = (pCfg[0] << 8) | (pCfg[1]) + 2;
				//		// copy sps first inclusive of 2 bytes nal size
				//		memcpy(n->cfgdata + wr_pos, pCfg, size);
				//		pCfg += size;
				//		wr_pos += size;
				//	}
				//}
			}

			// config info
			n->c->extradata = n->cfgdata;
			n->c->extradata_size = n->cfgdata_size;
		}
		else {
			// config info
			n->c->extradata = NULL;
			n->c->extradata_size = n->cfgdata_size = 0;
		}

		// on test..
		/*if (codec->capabilities & AV_CODEC_CAP_SLICE_THREADS) {
			if (hVideo->bLowDelayDecoding)
				n->c->thread_type = FF_THREAD_SLICE;
			n->c->thread_count = 4;
		}*/
		n->c->thread_count = 10;

		if (codec->capabilities & AV_CODEC_CAP_FRAME_THREADS) {
			if (!hVideo->bLowDelayDecoding && 0) {
				n->c->thread_type |= FF_THREAD_FRAME;
				n->c->thread_count = 4;
			}
		}
	}
	else if (CodecID == AV_CODEC_ID_H265) {
		if (codec->capabilities & AV_CODEC_CAP_SLICE_THREADS) {
			if (hVideo->bLowDelayDecoding)
				n->c->thread_type = FF_THREAD_SLICE;
			n->c->thread_count = 4;
		}
		if (codec->capabilities & AV_CODEC_CAP_FRAME_THREADS) {
			if (!hVideo->bLowDelayDecoding)	{
				n->c->thread_type |= FF_THREAD_FRAME;
				n->c->thread_count = 4;
			}
		}

		// always no delay
		n->c->flags |= AV_CODEC_FLAG_LOW_DELAY;

		// config info
		n->c->extradata = n->cfgdata;
		n->c->extradata_size = n->cfgdata_size;
	}
	
	TRACEX(FDR_LOG_INFO, "Low delay mode %s", (hVideo->bLowDelayDecoding ? "enable" : "disable"));

	if (!logfp)	{
#ifdef __FFMPEG_DEBUG__
		logfp = fopen(log_file_name, "w+t");
#endif
		if (!bSetLogCallBack) {
			av_log_set_callback(log_callback_help);
			bSetLogCallBack = 1;
		}
	}

	if (api_mode == API_MODE_NEW_API_REF_COUNT)
		av_dict_set(&opts, "refcounted_frames", "1", 0);

	//if (hVideo->bLowDelayDecoding)	{
	//	n->c->flags |= AV_CODEC_FLAG_LOW_DELAY;
	//}

	//n->c->refcounted_frames = 10;
	//n->c->codec_tag = codec_tag;
	
	if (avcodec_open2(n->c, codec, &opts) < 0) {
		FFVideoDecClose((VOID *)n);
		return	NULL;
	}
	

	n->hasPTS = (eMode == TIMESTAMP_PTS) || (eMode == TIMESTAMP_PTS_DTS);
	get_video_color_format(n, eVideoFormat);
	if (n->bUnknowColorFmt == FALSE)
		n->pout.eVideoFormat = *eVideoFormat;

	if (n->c->pix_fmt == AV_PIX_FMT_PAL8) {
		n->pallete_decoding_frame = (BYTE*)av_mallocz(n->c->height * n->c->width * 3);
		n->cfgdata = (BYTE*)av_mallocz(256*4);
		if (!n->pallete_decoding_frame || !n->pallete_decoding_frame)
		{
			FFVideoDecClose((VOID *)n);
			return	NULL;
		}
		memcpy(n->cfgdata, config_data, config_size);
	}

#ifdef USE_OLD_API
	if (n->bHWAccel==FALSE)
		n->bUseOldAPI = TRUE;
#endif

	return	(VOID *)n;

clean_up:
	if( !n || !n->c || !n->picture)	{
		if (n->decode.hw_device_ref)
			av_buffer_unref(&n->decode.hw_device_ref);

		// free alloced memory
		if(n->c)		av_free( (VOID *)n->c);
		if(n->picture)	av_free( (VOID *)n->picture);
		if(n)			av_free( (VOID *)n);
	}

	return	(VOID *)NoMemory;
}

VOID FFVideoDecClose(VOID *p) {
	NewAVCodecContext *n=(NewAVCodecContext *)p;

	if (n) {
		if (n->c->pix_fmt == AV_PIX_FMT_PAL8)
			av_free(n->cfgdata);

		if (n->cfgdataForFree)
			av_free(n->cfgdataForFree);

		avcodec_close(n->c);
		av_free((VOID *)n->c);
		//av_free(n->picture);
		av_frame_free(&n->picture);

		if (n->picture_sw)
			av_frame_free(&n->picture_sw);

		if (n->decode.hw_device_ref)
			av_buffer_unref(&n->decode.hw_device_ref);

		if (n->pallete_decoding_frame)
			av_free((VOID*)n->pallete_decoding_frame);

		// on test..mwkang, 2005.12.14
		av_free((VOID *)n);
		//av_free_static();

#ifdef __FFMPEG_DEBUG__
		if (logfp) {
			fclose(logfp);
			logfp = NULL;
		}
#endif
	}

	return;
}


VOID FFVideoDecFlush(VOID *p) {
	NewAVCodecContext *n=(NewAVCodecContext *)p;

	if (!p)
		return;

	avcodec_flush_buffers(n->c);
	if (n->bSeekCheck)
		n->bSeek = TRUE;
}

static VOID decode_pallete(BYTE *pOutput, BYTE *pInput, BYTE *pPallete, DWORD32 dwHeight, DWORD32 dwWidth, DWORD32 dwPitch)
{
	DWORD32		i, j;
	BYTE		*pIn, *pOut;

	for (i=0; i<dwHeight; i++)
	{
		DWORD32		dwLoc;

		pIn = pInput + dwPitch * i;
		pOut = pOutput + 3 * dwWidth * i;
		for (j=0; j<dwWidth; j++, pIn++, pOut += 3)
		{
			dwLoc = *pIn * 4;
			pOut[0] = pPallete[dwLoc];			// R
			pOut[1] = pPallete[dwLoc+1];		// G
			pOut[2] = pPallete[dwLoc+2];		// B
		}
	}
}

static VOID copy_output_picture(NewAVCodecContext *n)
{
	// QSV
	if (n->c->pix_fmt == AV_PIX_FMT_QSV)
	{
		n->pout.height = n->c->height;
		n->pout.width = n->c->width;
		n->pout.y = n->picture->data[0];
		n->pout.u = n->picture->data[1];
		n->pout.v = n->picture->data[2];
		n->pout.surface = n->picture->data[3];
		n->pout.y_pitch = n->picture->linesize[0];
		n->pout.u_pitch = n->picture->linesize[1];
		n->pout.v_pitch = n->picture->linesize[2];
		n->pout.format = n->picture->format;

		if (n->bUnknowColorFmt)
			get_video_color_format(n, &n->pout.eVideoFormat);
	}
	else if (n->c->pix_fmt == AV_PIX_FMT_PAL8)
	{
		// decode a frame with palette info on data[1]
		if (n->picture->palette_has_changed && n->picture->data[1])
		{
			TRACE("[Codec] palette changed");
			memcpy(n->cfgdata, n->picture->data[1], 256 * 4);
		}
		decode_pallete(n->pallete_decoding_frame, n->picture->data[0], n->cfgdata,
			n->c->height, n->c->width, n->picture->linesize[0]);

		n->pout.height = n->c->height;
		n->pout.width = n->c->width;
		n->pout.y = n->pallete_decoding_frame;
		n->pout.u = NULL;
		n->pout.v = NULL;
		n->pout.y_pitch = n->c->width * 3;
		n->pout.u_pitch = 0;
		n->pout.v_pitch = 0;
	}
	else
	{
		n->pout.height = n->c->height;
		n->pout.width = n->c->width;
		n->pout.y = n->picture->data[0];
		n->pout.u = n->picture->data[1];
		n->pout.v = n->picture->data[2];
		n->pout.y_pitch = n->picture->linesize[0];
		n->pout.u_pitch = n->picture->linesize[1];
		n->pout.v_pitch = n->picture->linesize[2];
		n->pout.format = n->picture->format;
		if (n->bUnknowColorFmt)
			get_video_color_format(n, &n->pout.eVideoFormat);
	}

	return;
}

INT32 FFVideoDecDecode(VOID *p, BYTE *bitstream, INT32 size, INT32 cts, INT32 dts, FrTimeStamp time_mode, PsVideoCodecOutput **pout) {
	NewAVCodecContext *n=(NewAVCodecContext *)p;
	INT32	complete_picture = 0;
	//INT32	zero_bytes_used = 0;

	INT32	ret = -1, ret2 = -1;

	n->avpkt.data = bitstream;
	n->avpkt.size = size;

	if(bitstream==NULL || !size) {
		return	0;
	}

    while (n->avpkt.size > 0) {
		switch(time_mode) {
		case	TIMESTAMP_PTS_DTS:
			n->avpkt.pts = cts;
			n->avpkt.dts = dts;
			break;
		case	TIMESTAMP_PTS:
			n->avpkt.pts = cts;
			break;
		default	:
			n->avpkt.dts = cts;
		}

		{
			n->pout.got_picture = 0;
			ret = avcodec_send_packet(n->c, &(n->avpkt));
			if( ret < 0 ) {
				TRACE("[FFDec] send packet error %d", ret);

				if (complete_picture)
					goto fail;
				else
					return	(INT32) 0;
			}

			ret2 = 0;
			while (0 <= ret2) {
				ret2 = avcodec_receive_frame(n->c, n->picture);
				if (ret2 < 0) {
					if (ret2 == AVERROR(EAGAIN)) {
						TRACE("[FFDec] receive frame error eagain(%d)", ret2);
					}
					char err[1024];
					av_strerror(ret2, err, sizeof(err));
					TRACE("[FFDec] receive frame error (%d-%s)", ret2, err);
					if (complete_picture)
						goto fail;
					else
						return	(INT32) 0;
				}

				n->pout.got_picture = 1;

				if (ret <0)
					TRACE("[FFMpegDec] receive frame succeeded as send packet returned with error");

				if (n->pout.got_picture == 1)
					break;

				//TRACE("[FFMpegDec] receive frame ret %d", ret2);
			}
			n->avpkt.size = 0;

			// get decoded picture
			if (n->pout.got_picture) {
				copy_output_picture(n);

				if (n->bSeek) {
					if (n->picture->key_frame)
						n->bSeek = FALSE;
					else
						return (INT32) 2;				// need more data to meet an I-frame
				}

				*pout = &n->pout;
				complete_picture = 1;

				if (n->hasPTS)
					(*pout)->cts = (UINT32)n->picture->pkt_pts;
				else
					(*pout)->cts = (UINT32)n->picture->pkt_dts;

				// get picture format
				if (n->bUnknowColorFmt)	{
					n->bUnknowColorFmt = FALSE;
					get_video_color_format(n, &(n->pout.eVideoFormat));
				}
				n->pout.interlace = n->picture->interlaced_frame;

				// discard the remaining video frame for jpeg decoding
				if (n->c->codec_id == AV_CODEC_ID_JPEGLS)
					goto fail;
			}
			else
			{
				//TRACE("[FFMpegDec] no got_picture. remaining data %d!!!", n->avpkt.size);
				// get picture format

				if (n->bUnknowColorFmt)	{
					*pout = &n->pout;
					n->bUnknowColorFmt = FALSE;
					get_video_color_format(n, &(n->pout.eVideoFormat));
					(*pout)->got_picture = 0;

				}
			}

			// unref
			//av_frame_unref(n->picture);
			//if (n->picture_sw)
			//	av_frame_unref(n->picture_sw);
		}
	}

fail:

	if(!complete_picture) {
		//TRACE("[FFMpegDec] no got_picture remaining data %d!!!\n", n->avpkt.size);
		copy_output_picture(n);

		if (n->avpkt.size)
			return	0;
		else
			return	(INT32) 2;				// decoding successed but needs more data
	}

	//TRACE("[FFMPEG2] cts %d => pts %d", cts, (*pout)->cts);

#if 0
	{
		static INT32 count;
		FILE *fp;

		fp = fopen("D:\\Temp\\h264.yuv","a+b");
		if (fp && count++ < 10)
		{
			fwrite(n->pout.y, 1, n->c->height* n->pout.y_pitch , fp);
			fwrite(n->pout.u, 1, n->c->height* n->pout.u_pitch/2 , fp);
			fwrite(n->pout.v, 1, n->c->height* n->pout.v_pitch/2 , fp);
		}

		if (fp)
			fclose(fp);
	}
#endif

	return	1;
}

INT32 FFVideoDecGetBufferedFrame(VOID *p, PsVideoCodecOutput **pout, BOOL bStartFlush) {
	NewAVCodecContext	*n=(NewAVCodecContext *)p;
	int					ret = 0;

	// retrieve
	n->avpkt.size = 0;
	n->avpkt.data = NULL;
	n->pout.got_picture = 0;

	if (bStartFlush) {
		ret = avcodec_send_packet(n->c, NULL);
		if (ret < 0) {
			TRACE("FFVideoDecGetBufferedFrame flushing: send packet error %d - end of file", ret);
			return 0;
		}
	}
		
	ret = avcodec_receive_frame(n->c, n->picture);
	if (ret < 0) {
		TRACE("FFVideoDecGetBufferedFrame flushing: receive frame error 0x%x, (eof 0x%x)", ret, AVERROR_EOF);
		return 0;
	}
	n->pout.got_picture = 1;

	/*while (ret >= 0) {
		ret = avcodec_receive_frame(n->c, n->picture);
		if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
			break;
		else if (ret < 0)
			break;

		n->pout.got_picture = 1;

		copy_output_picture(n);
		*pout = &n->pout;
		if (n->hasPTS)
			(*pout)->cts = (UINT32)n->picture->pkt_pts;
		else
			(*pout)->cts = (UINT32)n->picture->pkt_dts;

		TRACE("FFVideoDecGetBufferedFrame read video frames cts(%d)", (UINT32)n->picture->pkt_pts);
	}*/
	

	// get decoded picture
    if (n->pout.got_picture) {
		copy_output_picture(n);
		*pout = &n->pout;
		if (n->hasPTS)
			(*pout)->cts = (UINT32)n->picture->pkt_pts;
		else
			(*pout)->cts = (UINT32)n->picture->pkt_dts;

		TRACE("FFVideoDecGetBufferedFrame read video frames");

		return 1;
	}

	TRACE("FFVideoDecGetBufferedFrame There is no remaining video frames");

	return 0;
}

// static DWORD32 search_aud(BYTE *pData, DWORD32 dwData) {
// 	DWORD32		dwPos = 0;

// 	while(dwPos+3 <= dwData) {
// 		if (pData[0]==0 && pData[1] == 0 && pData[2] == 1)	{
// 			if ((pData[3] & 0x1F)==9)			// Aud
// 			{
// 				if (dwPos && pData[-1] == 0)
// 					dwPos --;
// 				return dwPos;
// 			}
// 		}
// 		dwPos++;
// 		pData++;
// 	}

// 	return (DWORD32)0;
// }