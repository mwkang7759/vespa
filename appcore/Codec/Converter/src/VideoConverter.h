
#ifndef	_VIDEOCONVERTER_H_
#define	_VIDEOCONVERTER_H_

#include "SystemAPI.h"
#include "TraceAPI.h"
#include "mediainfo.h"
#include "mediafourcc.h"
#include "mediaerror.h"
#include "UtilAPI.h"


#define	USE_FFFILTER
#ifdef	USE_FFFILTER
#define	USE_FFFILTER_RESIZE					// Enable Resize
#define	USE_FFFILTER_COLOR_CONV				// Enable Color Space Conversion
#include "FFFilterAPI.h"
#include "FFResizeAPI.h"
#endif

#ifdef _DEBUG
//#define VIDEO_DECODER_DUMP
//#define	VIDEO_DUMP
//#define	VIDEO_YUV_DUMP
//#define	VIDEO_PARSE_DUMP
#endif

#include "FFCodecAPI.h"

// GPU
#include "ESMNvconverter.h"
#include "ESMNvresizer.h"
#include <cuda.h>
//#include <cuda_runtime_api.h>
#include <cuda_runtime.h>

#ifdef CUDA_CODEC
#include "CudaDecoderAPI.h"
#endif

#define	TYPE_RTP	0
#define	TYPE_TS		1


typedef struct {
	LRSLT               m_lError;
	FrCodecType			m_eCodecType;
	DWORD32				m_dwSrcWidth;
	DWORD32				m_dwSrcHeight;
	DWORD32				m_dwDstWidth;
	DWORD32				m_dwDstHeight;
	BYTE*				m_pResizeBuffer;

	ESMNvresizer* m_pResizer;
	ESMNvresizer::CONTEXT_T m_ResizeCtx;

#ifdef	USE_FFFILTER
	FFFilterHandle		m_hResizeFilter;
#endif
} FrVideoResizeStruct;



typedef struct {
	LRSLT               m_lError;
	FrCodecType			m_eCodecType;

	ESMNvconverter* m_pCvt;
	ESMNvconverter::CONTEXT_T m_CvtCtx;
	void* m_pBuffer;
	BYTE* m_pHostBuffer;

	BYTE* m_pConvDstBuffer;
	size_t m_nConvDstPitch;

#ifdef	USE_FFFILTER
	FFFilterHandle		m_hColorFilter;
#endif
} FrVideoConvStruct;

#ifdef __cplusplus
extern "C"
{
#endif

// decoder
//LRSLT VideoNvconvOpen(FrVideoConvStruct* hConverter, FrMediaStream* pSrc, FrVideoInfo* hInfo);
//void VideoNvconvClose(FrVideoConvStruct* hConverter);
//
//BOOL VideoNvconvResize(FrVideoConvStruct* hConverter, FrRawVideo* pSrc, FrRawVideo* pDst);
//BOOL VideoNvconvColor(FrVideoConvStruct* hConverter, FrRawVideo* pSrc, FrRawVideo* pDst);


#ifdef __cplusplus
}
#endif


#endif	// _VIDEODECODER_H_
