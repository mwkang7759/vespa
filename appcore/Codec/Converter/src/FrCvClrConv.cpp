#include "FrCvClrConv.h"

void FrCvClrConv::YUV2BGR(cv::cuda::GpuMat& img, uint8_t* pVideo, int width, int height, int pitch) {
	
	std::vector<cv::cuda::GpuMat> vecGpuImage, vecGpuOutput(3);

	int lumaWidth = width;
	int lumaHeight = height;
	int chromaWidth = lumaWidth >> 1;
	int chromaHeight = lumaHeight >> 1;
	int lumaPitch = pitch;
	int chromaPitch = lumaPitch >> 1;

	uint8_t* yDeviceptr = pVideo;
	uint8_t* uDeviceptr = yDeviceptr + lumaPitch * lumaHeight;
	uint8_t* vDeviceptr = uDeviceptr + chromaPitch * chromaHeight;

	cv::cuda::GpuMat gpuY(lumaHeight, lumaWidth, CV_8UC1, yDeviceptr, lumaPitch);
	cv::cuda::GpuMat gpuU(chromaHeight, chromaWidth, CV_8UC1, uDeviceptr, chromaPitch);
	cv::cuda::GpuMat gpuV(chromaHeight, chromaWidth, CV_8UC1, vDeviceptr, chromaPitch);

	vecGpuImage.push_back(gpuY);
	vecGpuImage.push_back(gpuU);
	vecGpuImage.push_back(gpuV);

	vecGpuImage[0].copyTo(vecGpuOutput[0]);
	vecGpuImage[1].copyTo(vecGpuOutput[1]);
	vecGpuImage[2].copyTo(vecGpuOutput[2]);

	cv::cuda::resize(vecGpuOutput[1], vecGpuOutput[1], cv::Size(vecGpuOutput[1].cols * 2, vecGpuOutput[1].rows * 2), cv::INTER_CUBIC);
	cv::cuda::resize(vecGpuOutput[2], vecGpuOutput[2], cv::Size(vecGpuOutput[2].cols * 2, vecGpuOutput[2].rows * 2), cv::INTER_CUBIC);

	cv::cuda::merge(vecGpuOutput, img);
	cv::cuda::cvtColor(img, img, cv::ColorConversionCodes::COLOR_YUV2BGR);	// COLOR_YUV2BGR_YV12; //COLOR_YUV2BGR; //COLOR_YUV2RGB

	//Mat mResult;
	//img.download(mResult);
}

void FrCvClrConv::BGR2YUV(uint8_t* pYUV, int pitch, cv::cuda::GpuMat& img) {
	cv::cuda::GpuMat splitImg[3];
	cv::cuda::cvtColor(img, img, cv::COLOR_BGR2YUV);

	cv::cuda::split(img, splitImg);
	cv::cuda::resize(splitImg[1], splitImg[1], cv::Size(splitImg[1].cols * 0.5, splitImg[1].rows * 0.5), cv::INTER_CUBIC);
	cv::cuda::resize(splitImg[2], splitImg[2], cv::Size(splitImg[2].cols * 0.5, splitImg[2].rows * 0.5), cv::INTER_CUBIC);

	uint8_t* pos = pYUV;
	cudaMemcpy2D(pos, pitch, splitImg[0].data, splitImg[0].step, splitImg[0].cols, splitImg[0].rows, cudaMemcpyDeviceToDevice);
	pos += pitch * img.rows;
	cudaMemcpy2D(pos, pitch / 2, splitImg[1].data, splitImg[1].step, splitImg[1].cols, splitImg[1].rows, cudaMemcpyDeviceToDevice);
	pos += pitch * img.rows / 4;
	cudaMemcpy2D(pos, pitch / 2, splitImg[2].data, splitImg[2].step, splitImg[2].cols, splitImg[2].rows, cudaMemcpyDeviceToDevice);
}
