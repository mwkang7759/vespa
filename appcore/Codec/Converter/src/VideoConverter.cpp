
#include "VideoConverter.h"
#include "ConverterAPI.h"
#include "TraceAPI.h"

#include <cuda.h>


// add modify

int FrVideoCudaCopy(void* pDst, void* pSrc, int size, int dir) {
	/*CUdeviceptr dpSrcFrame = 0;
	unsigned int srcPitch = 0;
	::cuCtxPushCurrent(_cuContext);

	::cuMemAllocPitch((CUdeviceptr*)&pFrame, &_cuPitchConverted, 4 * _context->width, _context->height, 16);


	CUDA_MEMCPY2D m = { 0 };
	m.srcMemoryType = CU_MEMORYTYPE_DEVICE;
	m.srcDevice = dpSrcFrame;
	m.srcPitch = srcPitch;
	m.dstMemoryType = CU_MEMORYTYPE_DEVICE;
	m.dstDevice = (CUdeviceptr)(pDecodedFrame);
	m.dstPitch = _cuPitch ? _cuPitch : _cuWidth * (_cuBitdepthMinus8 ? 2 : 1);
	m.WidthInBytes = _cuWidth * (_cuBitdepthMinus8 ? 2 : 1);
	m.Height = _cuHeight;
	::cuMemcpy2DAsync(&m, _cuvidStream);

	m.srcDevice = (CUdeviceptr)((unsigned char*)dpSrcFrame + m.srcPitch * _cuSurfaceHeight);
	m.dstDevice = (CUdeviceptr)(m.dstHost = pDecodedFrame + m.dstPitch * _cuHeight);
	m.Height = _cuHeight >> 1;
	::cuMemcpy2DAsync(&m, _cuvidStream);
	::cuStreamSynchronize(_cuvidStream);
	
	
	::cuCtxPopCurrent(NULL);*/

	return 1;
}


#if 0
LRSLT FrVideoConvOpen(void** phConverter, FrVideoInfo *hSrcInfo, FrVideoInfo *hDstInfo) {
	FrVideoConvStruct*	hConverter;
	LRSLT				lRet = FR_FAIL;

	hConverter = (FrVideoConvStruct*)MALLOCZ(sizeof(FrVideoConvStruct));
	if (hConverter) {

#ifdef	VIDEO_YUV_DUMP
		hDecoder->m_hYUVFile = (FILE_HANDLE)FILEDUMPOPEN("video_dec_conv", "yuv", (DWORD32)hDecoder);
#endif

		*phConverter = hConverter;

		TRACEX(FDR_LOG_INFO, "FrVideoConvOpen Success -- handle(0x%x)", hConverter);
	}
	else
		return	COMMON_ERR_MEM;

    TRACEX(FDR_LOG_INFO, "FrVideoConvOpen : end");

	return	FR_OK;
}

void FrVideoConvClose(void* hHandle) {
	FrVideoConvStruct*	hConverter = (FrVideoConvStruct*)hHandle;

	TRACEX(FDR_LOG_DEBUG, "FrVideoConvClose Begin (0x%x)", hConverter);

	if (hConverter) {

		//FREE(hDecoder->m_pFrameBuffer);
		//hDecoder->m_pFrameBuffer = NULL;
		//FREE(hDecoder->m_pYUVBuffer);
		//hDecoder->m_pYUVBuffer = NULL;
		
#ifdef VIDEO_DECODER_DUMP
		if (hDecoder->m_bDump == TRUE)
			McCloseFile(hDecoder->m_hDumpFile);
#endif
        
#ifdef	USE_FFFILTER
		if (hConverter->m_hColorFilter)
			FFFilterClose(hConverter->m_hColorFilter);
#endif

#ifdef	VIDEO_DUMP
		McCloseFile(hDecoder->m_hFile);
#endif
#ifdef	VIDEO_YUV_DUMP
		McCloseFile(hDecoder->m_hYUVFile);
#endif
#ifdef	VIDEO_PARSE_DUMP
		McCloseFile(hDecoder->m_hParseFile);
#endif

		TRACEX(FDR_LOG_INFO, "FrVideoConvClose Success -- handle(0x%x)", hConverter);

		FREE(hConverter);
	}

	TRACEX(FDR_LOG_DEBUG, "FrVideoConvClose End");
}



LRSLT FrVideoConvResizee(void* hHandle, FrRawVideo* pSrc, FrRawVideo* pDst) {
	FrVideoConvStruct*	hConverter = (FrVideoConvStruct*)hHandle;
	FrRawVideo		Media = {0, };
	DWORD32				dwTick, dwTick2;
	BOOL				bIsDecoderBuffer = TRUE;	// This is to prevent decoder buffer from being used to merge subtitle or rotate
	LRSLT lRet;
    
	if (!hConverter)
		return FALSE;

    
	dwTick = FrGetTickCount();

#ifdef	VIDEO_YUV_DUMP
// 	McWriteFile(hDecoder->m_hYUVFile, (char*)Media.m_pY, hDecoder->m_dwSrcWidth * hDecoder->m_dwSrcHeight);
// 	McWriteFile(hDecoder->m_hYUVFile, (char*)Media.m_pU, hDecoder->m_dwSrcWidth * hDecoder->m_dwSrcHeight / 4);
// 	McWriteFile(hDecoder->m_hYUVFile, (char*)Media.m_pV, hDecoder->m_dwSrcWidth * hDecoder->m_dwSrcHeight / 4);
#endif

	/*dwTick2 = McGetTickCount();
	hDecoder->m_dwDecTime += (dwTick2 - dwTick);
	dwTick = dwTick2;
	*/

	return	FR_OK;
}
#endif