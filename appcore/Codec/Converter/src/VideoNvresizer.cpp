#include "VideoConverter.h"
#include "ConverterAPI.h"

#include <chrono>

LRSLT FrVideoResizeOpen(FrVideoResizeHandle* phResizer, FrVideoInfo* hSrcInfo, FrVideoInfo* hDstInfo) {
	LOG_I("ResizeOpen Begin..Src: Codec Type(%d), w(%d), h(%d) ==> Dst: w(%d), h(%d)", hSrcInfo->eCodecType, hSrcInfo->dwWidth, hSrcInfo->dwHeight, hDstInfo->dwWidth, hDstInfo->dwHeight);

	FrVideoResizeStruct* hResizer = (FrVideoResizeStruct*)MALLOCZ(sizeof(FrVideoResizeStruct));
	if (hResizer) {
		hResizer->m_eCodecType = hSrcInfo->eCodecType;
		if (hResizer->m_eCodecType == CODEC_HW) {
			hResizer->m_pResizer = new ESMNvresizer();
			if (!hResizer->m_pResizer->IsInitialized()) {
				hResizer->m_ResizeCtx.deviceIndex = 0;
				hResizer->m_ResizeCtx.inputWidth = hSrcInfo->dwWidth;
				hResizer->m_ResizeCtx.inputHeight = hSrcInfo->dwHeight;
				hResizer->m_ResizeCtx.outputWidth = hDstInfo->dwWidth;
				hResizer->m_ResizeCtx.outputHeight = hDstInfo->dwHeight;
				hResizer->m_ResizeCtx.colorspace = ESMNvresizer::COLORSPACE_T::YV12;

				hResizer->m_pResizer->Initialize(&hResizer->m_ResizeCtx);
			}
		}
		else {
			hResizer->m_dwSrcWidth = hSrcInfo->dwWidth;
			hResizer->m_dwSrcHeight = hSrcInfo->dwHeight;
			hResizer->m_dwDstWidth = hDstInfo->dwWidth;
			hResizer->m_dwDstHeight = hDstInfo->dwHeight;

			hResizer->m_pResizeBuffer = (BYTE*)MALLOCZ(hResizer->m_dwDstWidth * hResizer->m_dwDstHeight * 3 / 2);

			hResizer->m_hResizeFilter = FFResizeOpen(FOURCC_IYUV, 16, VideoFmtYUV420P, FALSE, hSrcInfo->dwWidth, hSrcInfo->dwHeight, 0, 0,
				FOURCC_IYUV, 16, VideoFmtYUV420P, hDstInfo->dwWidth, hDstInfo->dwHeight, 0, 0);
		}
		
		LOG_I("ResizeOpen End..");

		*phResizer = hResizer;
	}
	else
		return COMMON_ERR_MEM;

	return FR_OK;
}

void FrVideoResizeClose(FrVideoResizeHandle hHandle) {
	FrVideoResizeStruct* hResizer = (FrVideoResizeStruct*)hHandle;
	LOG_D("ResizeClose Begin..");
	if (hResizer) {
		if (hResizer->m_eCodecType == CODEC_HW) {
			hResizer->m_pResizer->Release();
			delete hResizer->m_pResizer;
		}
		else {
			FFResizeClose(hResizer->m_hResizeFilter);
			FREE(hResizer->m_pResizeBuffer);
		}
		
		FREE(hResizer);
	}
	LOG_D("ResizeClose End..");
}

int FrVideoResizeVideo(FrVideoResizeHandle hHandle, FrRawVideo* pSrc, FrRawVideo* pDst) {
	FrVideoResizeStruct* hResizer = (FrVideoResizeStruct*)hHandle;
	
	unsigned char* dpResizedYUVFrame = NULL;
	int dpResizedYUVPitch = 0;

	LOG_D("Resize Src: pitch(%d), decoded num(%d) cts(%d)", pSrc->dwPitch, pSrc->dwSrcCnt, pSrc->dwCTS);
	if (!pSrc->dwPitch) {
		LOG_E("Resize Src: pitch(%d) is null..", pSrc->dwPitch);
		return -1;
	}

	auto start = std::chrono::system_clock::now();

	if (hResizer->m_eCodecType == CODEC_HW) {
		int ret;
		ret = hResizer->m_pResizer->Resize(pSrc->pY, pSrc->dwPitch, &dpResizedYUVFrame, dpResizedYUVPitch);
		if (ret != ESMNvresizer::ERR_CODE_T::SUCCESS) {
			return -1;
		}

		pDst->pY = dpResizedYUVFrame;
		pDst->dwCTS = pSrc->dwCTS;
		pDst->dwPitch = dpResizedYUVPitch;
		pDst->dwSrcCnt = pSrc->dwSrcCnt;
	}
	else {
		BYTE* ppSrc[4] = { 0, }, * ppDst[4] = { 0, };
		DWORD32			pdwSrcPitch[4] = { 0, }, pdwDstPitch[4] = { 0, };

		// YUV
		ppSrc[0] = pSrc->pY;
		ppSrc[1] = pSrc->pU;
		ppSrc[2] = pSrc->pV;
		pdwSrcPitch[0] = pSrc->dwPitch;
		pdwSrcPitch[1] = pSrc->dwPitch / 2;
		pdwSrcPitch[2] = pSrc->dwPitch / 2;
		
		// YUV
		ppDst[0] = hResizer->m_pResizeBuffer;
		ppDst[1] = ppDst[0] + hResizer->m_dwDstHeight * hResizer->m_dwDstWidth;
		ppDst[2] = ppDst[1] + hResizer->m_dwDstHeight * hResizer->m_dwDstWidth / 4;
		pdwDstPitch[0] = hResizer->m_dwDstWidth;
		pdwDstPitch[1] = hResizer->m_dwDstWidth / 2;
		pdwDstPitch[2] = hResizer->m_dwDstWidth / 2;
		
		FFResizeFrame(hResizer->m_hResizeFilter, ppSrc, pdwSrcPitch, ppDst, pdwDstPitch);

		pDst->pY = hResizer->m_pResizeBuffer;
		pDst->pU = pDst->pY + hResizer->m_dwDstWidth * hResizer->m_dwDstHeight;
		pDst->pV = pDst->pU + hResizer->m_dwDstWidth * hResizer->m_dwDstHeight / 4;
		pDst->dwCTS = pSrc->dwCTS;
		pDst->dwPitch = hResizer->m_dwDstWidth;
		pDst->dwSrcCnt = pSrc->dwSrcCnt;
		
	}
	
	auto end = std::chrono::system_clock::now();
	std::chrono::milliseconds delta = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

	
	LOG_D("Resize Dst: pitch(%d), decoded num(%d) cts(%d), Tick(%d)", pDst->dwPitch, pDst->dwSrcCnt, pDst->dwCTS, delta.count());


	return 1;
}
