﻿#include "VideoConverter.h"
#include "ConverterAPI.h"

//#include <cuda.h>
#include <chrono>

LRSLT FrVideoConvOpen(FrVideoConvHandle* phResizer, FrVideoInfo* hInfo, int color) {
	LOG_I("ConvOpen Begin..");
	FrVideoConvStruct* hConv = (FrVideoConvStruct*)MALLOCZ(sizeof(FrVideoConvStruct));
	if (hConv) {
		hConv->m_eCodecType = hInfo->eCodecType;
		if (hConv->m_eCodecType == CODEC_HW) {
			hConv->m_pCvt = new ESMNvconverter();
			if (!hConv->m_pCvt->IsInitialized()) {
				hConv->m_CvtCtx.deviceIndex = 0;

				hConv->m_CvtCtx.height = hInfo->dwHeight;
				hConv->m_CvtCtx.width = hInfo->dwWidth; 

				cudaMalloc(&hConv->m_pBuffer, hInfo->dwWidth * hInfo->dwHeight * 3);	// sufficient memory..
				cudaError_t cerr = cudaMallocPitch((void**)&hConv->m_pConvDstBuffer, &hConv->m_nConvDstPitch, hInfo->dwWidth, (hInfo->dwHeight >> 1) * 3);
				if (cerr != cudaSuccess)
					LOG_E("cudaMallocPitch width:(%d) / height:(%d)", hConv->m_CvtCtx.width, hConv->m_CvtCtx.height, cerr);
				
				switch (hInfo->eColorFormat) {
				case VideoFmtYUV420P:
				case VideoFmtIYUV:
					hConv->m_CvtCtx.inputColorspace = ESMNvconverter::COLORSPACE_T::I420;
					break;
				case VideoFmtNV12:
					hConv->m_CvtCtx.inputColorspace = ESMNvconverter::COLORSPACE_T::NV12;
					break;
				case VideoFmtYV12:
					hConv->m_CvtCtx.inputColorspace = ESMNvconverter::COLORSPACE_T::YV12;
					break;
				case VideoFmtBGRA:
					hConv->m_CvtCtx.inputColorspace = ESMNvconverter::COLORSPACE_T::BGRA;
					break;
				default:
					hConv->m_CvtCtx.inputColorspace = ESMNvconverter::COLORSPACE_T::YV12;
					break;
				}
				switch (color) {
				case VideoFmtBGRA:
					hConv->m_CvtCtx.outputColorspace = ESMNvconverter::COLORSPACE_T::BGRA;
					break;
				case VideoFmtYUV420P:
				case VideoFmtIYUV:
					hConv->m_CvtCtx.outputColorspace = ESMNvconverter::COLORSPACE_T::I420;
					break;
				case VideoFmtNV12:
					hConv->m_CvtCtx.outputColorspace = ESMNvconverter::COLORSPACE_T::NV12;
					break;
				case VideoFmtYV12:
					hConv->m_CvtCtx.inputColorspace = ESMNvconverter::COLORSPACE_T::YV12;
					break;
				default:
					hConv->m_CvtCtx.outputColorspace = ESMNvconverter::COLORSPACE_T::YV12;
					break;
				}

				hConv->m_pCvt->Initialize(&hConv->m_CvtCtx);
			}
		}
		else {
			;
		}
		

		*phResizer = hConv;
	}
	else
		return COMMON_ERR_MEM;
	
	LOG_I("ConvOpen End..");

	return FR_OK;
}

void FrVideoConvClose(FrVideoConvHandle hHandle) {
	FrVideoConvStruct* hConv = (FrVideoConvStruct*)hHandle;
	LOG_I("ConvClose Begin..");

	if (hConv) {
		if (hConv->m_eCodecType == CODEC_HW) {
			cudaFree(hConv->m_pBuffer);

			if (hConv->m_pConvDstBuffer) {
				cudaFree(hConv->m_pConvDstBuffer);
				hConv->m_pConvDstBuffer = nullptr;
			}
			hConv->m_pCvt->Release();
			delete hConv->m_pCvt;
		}
		else {
			;
		}
		
		FREE(hConv);
	}

	LOG_I("ConvClose End..");
}

int FrVideoConvColor(FrVideoConvHandle hHandle, FrRawVideo* pSrc, FrRawVideo* pDst) {
	FrVideoConvStruct* hConv = (FrVideoConvStruct*)hHandle;

	int ret;
	LOG_D("Conversion Src:");
	
	auto start = std::chrono::system_clock::now();
	//unsigned char* ppFrame = nullptr;
	//size_t	ppPitchSize;
	if (hConv->m_eCodecType == CODEC_HW) {
		if (hConv->m_CvtCtx.outputColorspace == ESMNvconverter::COLORSPACE_T::BGRA) 
			ret = hConv->m_pCvt->ConvertYUV2BGRA(pSrc->pY, pSrc->dwPitch, &pDst->pY, (int&)pDst->dwPitch);
		else {
			//ret = hConv->m_pCvt->ConvertBGRA2YUV(pSrc->pY, pSrc->dwPitch, pDst->pY, pDst->dwPitch, hConv->m_CvtCtx.width, hConv->m_CvtCtx.height);
			ret = hConv->m_pCvt->ConvertBGRA2YUV(pSrc->pY, pSrc->dwPitch, hConv->m_pConvDstBuffer, hConv->m_nConvDstPitch, hConv->m_CvtCtx.width, hConv->m_CvtCtx.height);
			pDst->pY = hConv->m_pConvDstBuffer;
			pDst->dwPitch = hConv->m_nConvDstPitch;
		}
		
		pDst->dwDecodedWidth = hConv->m_CvtCtx.width;
		pDst->dwDecodedHeight = hConv->m_CvtCtx.height;
		pDst->dwCTS = pSrc->dwCTS;
	}
	else {
		;
	}
	
	auto end = std::chrono::system_clock::now();
	std::chrono::milliseconds delta = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	
	LOG_D("Conversion Dst CTS=%d chrono Tick=%d", pDst->dwCTS, delta.count());

	
	
	return ret;
}

