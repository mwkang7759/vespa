#pragma once

#if defined(WIN32) && defined(_USRDLL)
#if defined(EXPORT_ESM_NVROTATOR)
#define ESM_NVROTATOR_CLASS __declspec(dllexport)
#else
#define ESM_NVROTATOR_CLASS __declspec(dllimport)
#endif
#else
#define ESM_NVROTATOR_CLASS
#endif

#include <ESMBase.h>

//class ESM_NVROTATOR_CLASS ESMNvrotator
class ESMNvrotator
	: public ESMBase
{
public:
	class Rotator;
	class Core;
public:
	typedef struct _CONTEXT_T
	{
		void *	context;
		int		deviceIndex;
		int		width;
		int		height;
		int		colorspace;
		_CONTEXT_T()
			: context(NULL)
			, deviceIndex(0)
			, width(0)
			, height(0)
			, colorspace(ESMNvrotator::COLORSPACE_T::YV12)
		{
		}
	} CONTEXT_T;

	ESMNvrotator(void);
	virtual ~ESMNvrotator(void);

	bool	IsInitialized(void);

	int		Initialize(ESMNvrotator::CONTEXT_T * ctx);
	int		Release(void);
	int		Rotate(unsigned char * src, int srcPitch, unsigned char ** dst, int & dstPitch, float degree);

private:
	ESMNvrotator::Core * _core;
};