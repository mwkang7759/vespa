#include "ESMNvrotator.h"
#include <cuda.h>
#include <vector>

class ESMNvrotator::Core
{
public:
	Core(void);
	virtual ~Core(void);

	bool	IsInitialized(void);

	int		Initialize(ESMNvrotator::CONTEXT_T * ctx);
	int		Release(void);
	int		Rotate(unsigned char * src, int srcPitch, unsigned char ** dst, int & dstPitch, float degree);

private:
	bool						_initialized;
	CUcontext					_cuContext;
	ESMNvrotator::CONTEXT_T *	_context;
	size_t						_cuPitch;
	unsigned char *				_cuFrame;
};