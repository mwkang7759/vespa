#include "rotator.h"

template<class T>
__device__ static T clamp(T x, T lower, T upper)
{
	return x < lower ? lower : (x > upper ? upper : x);
}

static __global__ void rotateYV12(cudaTextureObject_t tex, unsigned char *dst, int pitch, int width, int height, float radian)
{
	int x = blockIdx.x * blockDim.x + threadIdx.x;
	int y = blockIdx.y * blockDim.y + threadIdx.y;

#if 0
    float u = x / (float)width; 
    float v = y / (float)height; 

	u -= 0.5f;
	v -= 0.5f;
    float tu = u * cosf(radian) - v * sinf(radian) + 0.5f; 
    float tv = v * cosf(radian) + u * sinf(radian) + 0.5f; 
#else
    float u = (float)x - (float)width/2; 
    float v = (float)y - (float)height/2; 
    float tu = u*cosf(radian) - v*sinf(radian); 
    float tv = v*cosf(radian) + u*sinf(radian); 

    tu /= (float)width; 
    tv /= (float)height; 
	tu += 0.5f;
	tv += 0.5f;
#endif
	
	*(dst + x + y * pitch) = (unsigned char)clamp((float)tex2D<float>(tex, tu, tv) * 255, 0.0f, 255.0f);//(y % 255);
}

static __global__ void rotateNV12Luma(cudaTextureObject_t tex, unsigned char *dst, int pitch, int width, int height, float radian)
{
	int x = blockIdx.x * blockDim.x + threadIdx.x;
	int y = blockIdx.y * blockDim.y + threadIdx.y;

    float u = (float)x - (float)width/2; 
    float v = (float)y - (float)height/2; 
    float tu = u*cosf(radian) - v*sinf(radian); 
    float tv = v*cosf(radian) + u*sinf(radian); 

    tu /= (float)width; 
    tv /= (float)height; 
	tu += 0.5f;
	tv += 0.5f;
	*(dst + x + y * pitch) = (unsigned char)clamp((float)tex2D<float>(tex, tu, tv) * 255, 0.0f, 255.0f);//(y % 255);
}

template<typename yuv>
static __global__ void rotateNV12Chroma(cudaTextureObject_t texUV, unsigned char *dst, unsigned char *dstUV, int pitch, int width, int height, float radian)
{
	int x = blockIdx.x * blockDim.x + threadIdx.x;
	int y = blockIdx.y * blockDim.y + threadIdx.y;

	if (x >= width / 2 || y >= height / 2)
		return;

	typedef decltype(yuv::x) YuvUnit;
	const int MAX = 1 << (sizeof(YuvUnit) * 8);
	
	float u = (float)x - (float)(width>>1)/2;
	float v = (float)y - (float)(height>>1)/2;
    float tu = u * cosf(radian) - v * sinf(radian); 
    float tv = v * cosf(radian) + u * sinf(radian); 

    tu /= (float)(width>>1); 
    tv /= (float)(height>>1); 
	tu += 0.5f;
	tv += 0.5f;

	yuv data;
	float2 uv = tex2D<float2>(texUV, tu, tv);
	data.x = (YuvUnit)clamp((float)(uv.x * MAX), 0.0f, 255.0f);
	data.y = (YuvUnit)clamp((float)(uv.y * MAX), 0.0f, 255.0f);
	*(yuv *)(dstUV + y * pitch + x * 2 * sizeof(YuvUnit)) = data;
}

static void rotateYV12(unsigned char * dst, int dstPitch, unsigned char * src, int srcPitch, int width, int height, float radian)
{
	cudaResourceDesc resDesc = {};
	resDesc.resType = cudaResourceTypePitch2D;
	resDesc.res.pitch2D.devPtr = src;
	resDesc.res.pitch2D.desc = cudaCreateChannelDesc<unsigned char>();
	resDesc.res.pitch2D.width = width;
	resDesc.res.pitch2D.height = height;
	resDesc.res.pitch2D.pitchInBytes = srcPitch;

	cudaTextureDesc texDesc = {};
	texDesc.addressMode[0]	= cudaAddressModeClamp;
	texDesc.addressMode[1]	= cudaAddressModeClamp;
	texDesc.filterMode		= cudaFilterModeLinear;
	texDesc.readMode		= cudaReadModeNormalizedFloat;
	texDesc.normalizedCoords= 1;
	
	cudaTextureObject_t texObj = 0;
	cudaError_t err = cudaCreateTextureObject(&texObj, &resDesc, &texDesc, NULL);

	dim3 threadPerBlock(8, 8);
	dim3 blocks((width + threadPerBlock.x - 1)/ threadPerBlock.x, (height + threadPerBlock.y - 1)/ threadPerBlock.y);
	rotateYV12<<<blocks, threadPerBlock>>>(texObj, dst, dstPitch, width, height, radian);

	cudaDestroyTextureObject(texObj);
}

static void rotateNV12Luma(unsigned char * dst, int dstPitch, unsigned char * src, int srcPitch, int width, int height, float radian)
{
	cudaResourceDesc resDesc = {};
	resDesc.resType = cudaResourceTypePitch2D;
	resDesc.res.pitch2D.devPtr = src;
	resDesc.res.pitch2D.desc = cudaCreateChannelDesc<unsigned char>();
	resDesc.res.pitch2D.width = width;
	resDesc.res.pitch2D.height = height;
	resDesc.res.pitch2D.pitchInBytes = srcPitch;

	cudaTextureDesc texDesc = {};
	texDesc.addressMode[0]	= cudaAddressModeClamp;
	texDesc.addressMode[1]	= cudaAddressModeClamp;
	texDesc.filterMode		= cudaFilterModeLinear;
	texDesc.readMode		= cudaReadModeNormalizedFloat;
	texDesc.normalizedCoords= 1;
	
	cudaTextureObject_t texObj = 0;
	cudaError_t err = cudaCreateTextureObject(&texObj, &resDesc, &texDesc, NULL);

	dim3 threadPerBlock(8, 8);
	dim3 blocks((width + threadPerBlock.x - 1)/ threadPerBlock.x, (height + threadPerBlock.y - 1)/ threadPerBlock.y);
	rotateNV12Luma<<<blocks, threadPerBlock>>>(texObj, dst, dstPitch, width, height, radian);

	cudaDestroyTextureObject(texObj);
}

static void rotateNV12Chroma(unsigned char * dst, int dstPitch, unsigned char * src, int srcPitch, int width, int height, float radian)
{
	cudaTextureDesc texDesc = {};
	texDesc.addressMode[0]	= cudaAddressModeClamp;
	texDesc.addressMode[1]	= cudaAddressModeClamp;
	texDesc.filterMode		= cudaFilterModePoint;//cudaFilterModeLinear;
	texDesc.readMode		= cudaReadModeNormalizedFloat;
	texDesc.normalizedCoords= 1;

	cudaResourceDesc resDesc = {};
	resDesc.resType = cudaResourceTypePitch2D;
	resDesc.res.pitch2D.devPtr = src + srcPitch * height;
	resDesc.res.pitch2D.desc = cudaCreateChannelDesc<uchar2>();
	resDesc.res.pitch2D.width = width / 2;
	resDesc.res.pitch2D.height = height / 2;
	resDesc.res.pitch2D.pitchInBytes = srcPitch;

	cudaTextureObject_t texUV = 0;
	cudaCreateTextureObject(&texUV, &resDesc, &texDesc, NULL);

	unsigned char * dstUV = dst + (dstPitch * height);
	rotateNV12Chroma<uchar2><<<dim3((width + 31) / 32, (height + 31) / 32), dim3(16, 16)>>>(texUV, dst, dstUV, dstPitch, width, height, radian);

	cudaDestroyTextureObject(texUV);
}

void ESMNvrotator::Rotator::rotate_yv12(unsigned char * dst, int dstPitch, unsigned char * src, int srcPitch, int width, int height, float radian)
{
	int srcChromaPitch = srcPitch >> 1;
	int dstChromaPitch = dstPitch >> 1;
	int chromaWidth = width >> 1;
	int chromaHeight = height >> 1;

	unsigned char * srcU = src + (srcPitch * height);
	unsigned char * srcV = srcU + (srcChromaPitch * chromaHeight);

	unsigned char * dstU = dst + (dstPitch * height);
	unsigned char * dstV = dstU + (dstChromaPitch * chromaHeight);

	rotateYV12(dst, dstPitch, src, srcPitch, width, height, radian);
	rotateYV12(dstU, dstChromaPitch, srcU, srcChromaPitch, chromaWidth, chromaHeight, radian);
	rotateYV12(dstV, dstChromaPitch, srcV, srcChromaPitch, chromaWidth, chromaHeight, radian);
}

void ESMNvrotator::Rotator::rotate_nv12(unsigned char * dst, int dstPitch, unsigned char * src, int srcPitch, int width, int height, float radian)
{
	rotateNV12Luma(dst, dstPitch, src, srcPitch, width, height, radian);
	rotateNV12Chroma(dst, dstPitch, src, srcPitch, width, height, radian);
}