#pragma once

#include "ESMNvrotator.h"
#include <cuda_runtime.h>

class ESMNvrotator::Rotator
{
public:
	static void rotate_nv12(unsigned char * dst, int dstPitch, unsigned char * src, int srcPitch, int width, int height, float radian);
	static void rotate_yv12(unsigned char * dst, int dstPitch, unsigned char * src, int srcPitch, int width, int height, float radian);
};