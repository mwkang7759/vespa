#include "ESMNvrotator.h"
#include "ESMNvrotatorCore.h"

ESMNvrotator::ESMNvrotator(void)
{
	_core = new ESMNvrotator::Core();
}

ESMNvrotator::~ESMNvrotator(void)
{
	if(_core)
	{
		delete _core;
		_core = NULL;
	}
}

bool ESMNvrotator::IsInitialized(void)
{
	return _core->IsInitialized();
}

int ESMNvrotator::Initialize(ESMNvrotator::CONTEXT_T * ctx)
{
	return _core->Initialize(ctx);
}

int ESMNvrotator::Release(void)
{
	return _core->Release();
}

int ESMNvrotator::Rotate(unsigned char * src, int srcPitch, unsigned char ** dst, int & dstPitch, float degree)
{
	return _core->Rotate(src, srcPitch, dst, dstPitch, degree);
}