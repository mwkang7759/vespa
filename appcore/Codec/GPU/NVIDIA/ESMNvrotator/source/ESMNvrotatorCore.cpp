﻿#include "ESMNvrotatorCore.h"
#include "rotator.h"
#include <math.h>

#define PI 3.14159265

ESMNvrotator::Core::Core(void)
	: _initialized(false)
	, _cuContext(NULL)
	, _cuPitch(0)
	, _cuFrame(NULL)
{

}

ESMNvrotator::Core::~Core(void)
{

}

bool ESMNvrotator::Core::IsInitialized(void)
{
	return _initialized;
}

int ESMNvrotator::Core::Initialize(ESMNvrotator::CONTEXT_T * ctx)
{
	_context = ctx;
	int ngpu = 0;
	CUresult cret = ::cuInit(0);
	cret = ::cuDeviceGetCount(&ngpu);
	if((_context->deviceIndex < 0) || (_context->deviceIndex >= ngpu))
		return -1;
	CUdevice cuDevice;
	cret = ::cuDeviceGet(&cuDevice, _context->deviceIndex);
	cret = ::cuCtxCreate(&_cuContext, CU_CTX_SCHED_AUTO, cuDevice);
	//cret = ::cuCtxPopCurrent(&current_context);
	if(_context->colorspace == ESMNvrotator::COLORSPACE_T::BGRA)
	{
		::cuCtxPushCurrent(_cuContext);
		::cuMemAllocPitch((CUdeviceptr*)&_cuFrame, &_cuPitch, 4 * _context->width, _context->height, 16);
		::cuCtxPopCurrent(NULL);
	}
	else
	{
		::cuCtxPushCurrent(_cuContext);
		::cuMemAllocPitch((CUdeviceptr*)&_cuFrame, &_cuPitch, _context->width, (_context->height>>1) * 3, 16);
		::cuCtxPopCurrent(NULL);
	}
	_initialized = true;

	//return ESMNvrotator::ERR_CODE_T::SUCCESS;
	if (cret == CUDA_SUCCESS)
		return ESMNvrotator::ERR_CODE_T::SUCCESS;
	else
		return ESMNvrotator::ERR_CODE_T::GENERIC_FAIL;
}

int ESMNvrotator::Core::Release(void)
{
	::cuCtxPushCurrent(_cuContext);
	::cuMemFree((CUdeviceptr)(_cuFrame));
	::cuCtxPopCurrent(NULL);
	::cuCtxDestroy(_cuContext);
	_initialized = false;
	return ESMNvrotator::ERR_CODE_T::SUCCESS;
}

int ESMNvrotator::Core::Rotate(unsigned char * src, int srcPitch, unsigned char ** dst, int & dstPitch, float degree)
{
	float radian = (float)degree*PI/180;
	if(_context->colorspace==ESMNvrotator::COLORSPACE_T::NV12)
	{
		ESMNvrotator::Rotator::rotate_nv12(_cuFrame, (int)_cuPitch, src, srcPitch, _context->width, _context->height, radian);
	}
	else if(_context->colorspace==ESMNvrotator::COLORSPACE_T::YV12)
	{
		ESMNvrotator::Rotator::rotate_yv12(_cuFrame, (int)_cuPitch, src, srcPitch, _context->width, _context->height, radian);
	}
	*dst = _cuFrame;
	dstPitch = (int)_cuPitch;
	return ESMNvrotator::ERR_CODE_T::SUCCESS;
}