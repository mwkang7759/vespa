cmake_minimum_required(VERSION 3.16)



find_package(CUDA 11.0 REQUIRED)
message(STATUS "Found CUDA ${CUDA_VERSION_STRING} at ${CUDA_TOOLKIT_ROOT_DIR}")
message(STATUS "Found CUDA Runtime Library : ${CMAKE_CUDA_RUNTIME_LIBRARY}")
if(CUDA_FOUND)
    set(CMAKE_CUDA_COMPILER "${CUDA_TOOLKIT_ROOT_DIR}/bin/nvcc")
endif()
enable_language("CUDA")



#find_package(CUDA 11.0 REQUIRED)
#set(CMAKE_CUDA_COMPILER "${CUDA_TOOLKIT_ROOT_DIR}/bin/nvcc")
#enable_language("CUDA")

# add all current files to SRC_FILES
# file(GLOB_RECURSE SRC_FILES CONFIGURE_DEPENDS
# 	${CMAKE_CURRENT_SOURCE_DIR}/*.cpp
# 	${CMAKE_CURRENT_SOURCE_DIR}/*.c
# 	../resize/resizer.cu
# 	../colorspace/colorspace_converter.cu
# 	)

# make static library
add_library(ESMNvdec STATIC 
			ESMNvdec.cpp
			ESMNvdecCore.cpp
			../resize/resizer.cu
			../colorspace/colorspace_converter.cu
			)

# header path for using compile library
target_include_directories(ESMNvdec PUBLIC ${CMAKE_SOURCE_DIR}/include
	${CMAKE_SOURCE_DIR}/Codec/GPU/NVIDIA/Interface
	../include
	../colorspace
	../resize
	${CMAKE_SOURCE_DIR}/Codec/GPU/Base/include
	${CMAKE_SOURCE_DIR}/_3rdparty_/linux/cuda11.0/include)

target_link_directories(ESMNvdec
					   PRIVATE ${CMAKE_SOURCE_DIR}/_3rdparty_/linux/cuda11.0/lib64
					   PRIVATE ${CMAKE_SOURCE_DIR}/_3rdparty_/linux/nvsdk/lib
                       )

set_target_properties(ESMNvdec PROPERTIES CUDA_SEPARABLE_COMPILATION ON)
set_target_properties(ESMNvdec PROPERTIES CUDA_ARCHITECTURES "35;50;75")
#set_target_properties(ESMNvdec PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/out/lib/${CMAKE_BUILD_TYPE})            

target_link_libraries(ESMNvdec
						PRIVATE pthread
                        PRIVATE nvcuvid
						PRIVATE cuda
						PRIVATE cudadevrt
						PRIVATE cudart
                        )


# compile options
#target_compile_options(ESMNvdec PRIVATE -Wall -Wno-error=unused-but-set-variable -Wno-error=unused-variable -Werror -DLinux)
target_compile_options(ESMNvdec PRIVATE $<$<COMPILE_LANGUAGE:CUDA>:-arch=compute_60 -code=sm_60,sm_61,sm_62,sm_70,sm_72,sm_75>)
