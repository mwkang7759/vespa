﻿#include "ESMNvdecCore.h"
#ifdef WIN32
#include <ESMLocks.h>
#else
#include <fd_locks.h>
#endif
#include "colorspace_converter.h"
#include "resizer.h"

ESMNvdec::Core::Core(void)
	: _initialized(false)
	, _cuContext(NULL)
	, _cuParser(NULL)
	, _cuDecoder(NULL)
	, _cuWidth(0)
	, _cuHeight(0)
	, _cuSurfaceHeight(0)
	, _cuCodec(cudaVideoCodec_NumCodecs)
	, _cuPitch(0)
	, _cuPitchResized(0)
	, _cuPitchConverted(0)
	, _cuPitch2(0)
{
	::memset(&_cuFormat, 0x00, sizeof(_cuFormat));
#ifdef WIN32
	::InitializeCriticalSection(&_lock);
	::InitializeCriticalSection(&_lock2);
	::InitializeCriticalSection(&_frameLock);
#else
	pthread_mutex_init(&_lock, nullptr);
	pthread_mutex_init(&_lock2, nullptr);
	pthread_mutex_init(&_frame_lock, nullptr);
#endif
}

ESMNvdec::Core::~Core(void)
{
#ifdef WIN32
	::DeleteCriticalSection(&_frameLock);
	::DeleteCriticalSection(&_lock2);
	::DeleteCriticalSection(&_lock);
#else
	pthread_mutex_destroy(&_frame_lock);
    pthread_mutex_destroy(&_lock2);
    pthread_mutex_destroy(&_lock);
#endif
	_initialized = false;
}

bool ESMNvdec::Core::IsInitialized(void)
{
	return _initialized;
}

void *	ESMNvdec::Core::GetContext(void)
{
	return (void*)&_cuContext;
}

int ESMNvdec::Core::Initialize(ESMNvdec::CONTEXT_T * ctx)
{
	_context = ctx;
	int ngpu = 0;
	CUresult cret = ::cuInit(0);
	cret = ::cuDeviceGetCount(&ngpu);
	if((_context->deviceIndex < 0) || (_context->deviceIndex >= ngpu))
		return -1;
	CUdevice cuDevice;
	cret = ::cuDeviceGet(&cuDevice, _context->deviceIndex);
	cret = ::cuCtxCreate(&_cuContext, CU_CTX_SCHED_AUTO, cuDevice);
	cret = ::cuvidCtxLockCreate(&_cuCtxLock, _cuContext);

	CUVIDPARSERPARAMS videoParserParameters = {};
	switch (_context->codec)
	{
	case ESMNvdec::VIDEO_CODEC_T::AVC :
		videoParserParameters.CodecType = cudaVideoCodec_H264;
		break;
	case ESMNvdec::VIDEO_CODEC_T::HEVC :
		videoParserParameters.CodecType = cudaVideoCodec_HEVC;
		break;
	}
	videoParserParameters.ulMaxNumDecodeSurfaces = 1;
	videoParserParameters.ulClockRate = 1000;// clkRate;
	videoParserParameters.ulMaxDisplayDelay = 1; // jyhwang : throughput???믪씠?ㅻ㈃ ulMaxDisplayDelay瑜?1 ?댁긽?쇰줈 ?ㅼ젙?댁빞 ??
	videoParserParameters.pUserData = this;
	videoParserParameters.pfnSequenceCallback = ESMNvdec::Core::ProcessVideoSequence;
	videoParserParameters.pfnDecodePicture = ESMNvdec::Core::ProcessPictureDecode;
	videoParserParameters.pfnDisplayPicture = ESMNvdec::Core::ProcessPictureDisplay;
	{
#ifdef WIN32
		ESMAutolock lock(&_lock);
#else
		fd::locks::autolock lock(&_lock);
#endif
		::cuvidCreateVideoParser(&_cuParser, &videoParserParameters);
	}
	_initialized = true;
	return ESMNvdec::ERR_CODE_T::SUCCESS;
}

int ESMNvdec::Core::Release(void)
{
#ifdef WIN32
	ESMAutolock lock2(&_lock2);
#else
	fd::locks::autolock lock2(&_lock2);
#endif
	CUresult cret;
	if (_cuParser)
	{
		cret = ::cuvidDestroyVideoParser(_cuParser);
		_cuParser = NULL;
	}
	if (_cuDecoder)
	{
#ifdef WIN32
		ESMAutolock lock(&_lock);
#else
		fd::locks::autolock lock(&_lock);
#endif
		::cuCtxPushCurrent(_cuContext);
		cret = ::cuvidDestroyDecoder(_cuDecoder);
		::cuCtxPopCurrent(NULL);
		_cuDecoder = NULL;
	}

	{
#ifdef WIN32
		ESMAutolock lock(&_frameLock);
#else
		fd::locks::autolock lock(&_frame_lock);
#endif
		std::vector<unsigned char*>::iterator iter;
		for(iter = _vFrame.begin(); iter!=_vFrame.end(); iter++)
		{
#ifdef WIN32
			ESMAutolock lock2(&_lock);
#else
			fd::locks::autolock lock2(&_lock);
#endif
			::cuCtxPushCurrent(_cuContext);
			::cuMemFree((CUdeviceptr)(*iter));
			::cuCtxPopCurrent(NULL);
		}
		_vFrame.clear();

		if (_context != nullptr && ((_context->width != _cuWidth) || (_context->height != _cuHeight)))
		{
			// #ifdef WIN32
			// ESMAutolock lock(&_frameLock);
			// #else
			// fd::locks::autolock lock(&_frame_lock);
			// #endif
			for(iter = _vFrameResized.begin(); iter!=_vFrameResized.end(); iter++)
			{
				#ifdef WIN32
				ESMAutolock lock2(&_lock);
				#else
				fd::locks::autolock lock2(&_lock);
				#endif
				::cuCtxPushCurrent(_cuContext);
				::cuMemFree((CUdeviceptr)(*iter));
				::cuCtxPopCurrent(NULL);
			}
			_vFrameResized.clear();
		}

		if (_context != nullptr && (_context->colorspace != ESMNvdec::COLORSPACE_T::NV12))
		{
			// #ifdef WIN32
			// ESMAutolock lock(&_frameLock);
			// #else
			// fd::locks::autolock lock(&_frame_lock);
			// #endif
			for(iter = _vFrameConverted.begin(); iter!=_vFrameConverted.end(); iter++)
			{
				#ifdef WIN32
				ESMAutolock lock2(&_lock);
				#else
				fd::locks::autolock lock2(&_lock);
				#endif
				::cuCtxPushCurrent(_cuContext);
				::cuMemFree((CUdeviceptr)(*iter));
				::cuCtxPopCurrent(NULL);
			}
			_vFrameConverted.clear();
		}

		/*
		for(iter = _vFrame2.begin(); iter!=_vFrame2.end(); iter++)
		{
			ESMAutolock lock2(&_lock);
			::cuCtxPushCurrent(_cuContext);
			::cuMemFree((CUdeviceptr)(*iter));
			::cuCtxPopCurrent(NULL);
		}
		*/
		_vFrame2.clear();
	}
	::cuvidCtxLockDestroy(_cuCtxLock);
	::cuCtxDestroy(_cuContext);
	_mAppUserData.clear();
	_initialized = false;
	return ESMNvdec::ERR_CODE_T::SUCCESS;
}

int ESMNvdec::Core::Decode(unsigned char* bitstream, int bitstreamSize, long long bitstreamTimestamp, unsigned char*** nv12, int* numberOfDecoded, long long** timetstamp, void* userdata, void*** returnedUserData, unsigned long flags)
{
	#ifdef WIN32
	ESMAutolock lock2(&_lock2);
	#else
	fd::locks::autolock lock2(&_lock2);
	#endif

	LOG_D("[Nvdec] Decode Begin..time(%d)", bitstreamTimestamp);

	if (!_cuParser)
		return ESMNvdec::ERR_CODE_T::GENERIC_FAIL;

	_nDecodedFrame = 0;
	CUVIDSOURCEDATAPACKET packet = { 0 };
	packet.payload = bitstream;
	packet.payload_size = bitstreamSize;
	packet.flags = CUVID_PKT_TIMESTAMP | CUVID_PKT_ENDOFPICTURE | flags; 
	packet.timestamp = bitstreamTimestamp;
	if (!bitstream || bitstreamSize == 0) {
		packet.flags |= CUVID_PKT_ENDOFSTREAM;
	}
	else 
	{
		if (userdata != nullptr) 
		{
			// bitstream ???좏슚???뚮쭔 userdata瑜????
			if (_mAppUserData.find(bitstreamTimestamp) != _mAppUserData.end()) {
				_mAppUserData.erase(_mAppUserData.find(bitstreamTimestamp));
			}
			_mAppUserData.insert(std::make_pair(bitstreamTimestamp, userdata));
		}
	}

	{
		#ifdef WIN32
		ESMAutolock lock(&_lock);
		#else
		fd::locks::autolock lock(&_lock);
		#endif
		::cuvidParseVideoData(_cuParser, &packet);
		_cuvidStream = 0;
	}

	if (_nDecodedFrame > 0)
	{
		_vFrame2.clear();
		if(nv12)
		{
			int index = 0;
			#ifdef WIN32
			ESMAutolock lock(&_frameLock);
			#else
			fd::locks::autolock lock(&_frame_lock);
			#endif
			std::vector<unsigned char*>::iterator iter;
			for (iter = _vFrame.begin(); iter != (_vFrame.begin() + _nDecodedFrame); iter++, index++)
			{
				if(_context->colorspace==ESMNvdec::COLORSPACE_T::BGRA)
				{
					if ((_context->width != _cuWidth) || (_context->height != _cuHeight))
					{
						resizer::resize_nv12((unsigned char *)_vFrameResized[index], (int)_cuPitchResized, _context->width, _context->height, (*iter), (int)_cuPitch, _cuWidth, _cuHeight);
						converter::convert_nv12_to_bgra32((uint8_t*)_vFrameResized[index], (int)_cuPitchResized, (uint8_t*)_vFrameConverted[index], (int)_cuPitchConverted, _context->width, _context->height);
					}
					else
					{
						converter::convert_nv12_to_bgra32((*iter), (int)_cuPitch, (uint8_t*)_vFrameConverted[index], (int)_cuPitchConverted, _context->width, _context->height);
					}
					_cuPitch2 = _cuPitchConverted;
					_vFrame2.push_back(_vFrameConverted[index]);
				}
				else if(_context->colorspace==ESMNvdec::COLORSPACE_T::I420)
				{
					if ((_context->width != _cuWidth) || (_context->height != _cuHeight))
					{
						resizer::resize_nv12((unsigned char *)_vFrameResized[index], (int)_cuPitchResized, _context->width, _context->height, (*iter), (int)_cuPitch, _cuWidth, _cuHeight);
						converter::convert_nv12_to_i420((uint8_t*)_vFrameResized[index], (int)_cuPitchResized, (uint8_t*)_vFrameConverted[index], (int)_cuPitchConverted, _context->width, _context->height);
					}
					else
					{
						converter::convert_nv12_to_i420((*iter), (int)_cuPitch, (uint8_t*)_vFrameConverted[index], (int)_cuPitchConverted, _context->width, _context->height);
					}
					_cuPitch2 = _cuPitchConverted;
					_vFrame2.push_back(_vFrameConverted[index]);
				}
				else if(_context->colorspace==ESMNvdec::COLORSPACE_T::YV12)
				{
					if ((_context->width != _cuWidth) || (_context->height != _cuHeight))
					{
						resizer::resize_nv12((unsigned char *)_vFrameResized[index], (int)_cuPitchResized, _context->width, _context->height, (*iter), (int)_cuPitch, _cuWidth, _cuHeight);
						converter::convert_nv12_to_yv12((uint8_t*)_vFrameResized[index], (int)_cuPitchResized, (uint8_t*)_vFrameConverted[index], (int)_cuPitchConverted, _context->width, _context->height);
					}
					else
					{
						converter::convert_nv12_to_yv12((*iter), (int)_cuPitch, (uint8_t*)_vFrameConverted[index], (int)_cuPitchConverted, _context->width, _context->height);
					}
					_cuPitch2 = _cuPitchConverted;
					_vFrame2.push_back(_vFrameConverted[index]);
				}
				else
				{
					if ((_context->width != _cuWidth) || (_context->height != _cuHeight))
					{
						resizer::resize_nv12((unsigned char *)_vFrameResized[index], (int)_cuPitchResized, _context->width, _context->height, (*iter), (int)_cuPitch, _cuWidth, _cuHeight);
						_cuPitch2 = _cuPitchResized;
						_vFrame2.push_back(_vFrameResized[index]);	
					}
					else
					{
						_cuPitch2 = _cuPitch;
						_vFrame2.push_back((*iter));	
					}
				}
			}
			*nv12 = &_vFrame2[0];
		}
		if(timetstamp)
		{
			*timetstamp = &_vTimestamp[0];
		}
		if(returnedUserData)
			*returnedUserData = &_vReturedUserData[0];
		 
	}
	if(numberOfDecoded)
	{
		*numberOfDecoded = _nDecodedFrame;
	}
	LOG_D("[Nvdec] Decode End.._nDecodedFrame(%d)", _nDecodedFrame);
	return ESMNvdec::ERR_CODE_T::SUCCESS;
}

size_t ESMNvdec::Core::GetPitch(void)
{
	return _cuPitch;
}

size_t ESMNvdec::Core::GetPitch2(void)
{
	return _cuPitch2;
}

unsigned char* ESMNvdec::Core::Alloc(size_t bytesize)
{
	CUdeviceptr buf = 0;
	::cuCtxPushCurrent(_cuContext);
	::cuMemAlloc(&buf, bytesize);
	::cuCtxPopCurrent(NULL);

	return (unsigned char*)buf;
}

void ESMNvdec::Core::Free(void* pFrame)
{
	if (pFrame)
	{
		::cuCtxPushCurrent(_cuContext);
		::cuMemFree((CUdeviceptr)pFrame);
		::cuCtxPopCurrent(NULL);
	}
}

size_t ESMNvdec::Core::Copy(void* pDstFrame, const void* pSrcFrame, size_t bytesize)
{
	size_t ret;
	::cuCtxPushCurrent(_cuContext);
	ret = ::cuMemcpy((CUdeviceptr)pDstFrame, (CUdeviceptr)pSrcFrame, bytesize);
	::cuCtxPopCurrent(NULL);
	return ret;
}

size_t ESMNvdec::Core::GetPitchResized(void)
{
	return _cuPitchResized;
}

size_t ESMNvdec::Core::GetPitchConverted(void)
{
	return _cuPitchConverted;
}

int ESMNvdec::Core::ProcessVideoSequence(CUVIDEOFORMAT * format)
{
	LOG_D("[Nvdec] ProcessVideoSequence Begin..");

	int numberOfDecodeSurfaces = GetNumberofDecodeSurfaces(format->codec, format->coded_width, format->coded_height);
	if (_cuWidth && _cuHeight) 
	{
		if((format->coded_width== _cuFormat.coded_width) && (format->coded_height== _cuFormat.coded_height))
			return numberOfDecodeSurfaces;
		return numberOfDecodeSurfaces; // this error means current cuda device isn't support dynamic resolution change
	}

	_cuCodec = format->codec;
	_cuChromaFormat = format->chroma_format;
	_cuBitdepthMinus8 = format->bit_depth_luma_minus8;
	_cuFormat = *format;

	CUVIDDECODECREATEINFO videoDecodeCreateInfo = { 0 };
	videoDecodeCreateInfo.CodecType = format->codec;
	videoDecodeCreateInfo.ChromaFormat = format->chroma_format;
	videoDecodeCreateInfo.OutputFormat = cudaVideoSurfaceFormat_NV12;
	videoDecodeCreateInfo.DeinterlaceMode = cudaVideoDeinterlaceMode_Adaptive;// cudaVideoDeinterlaceMode_Weave;
	videoDecodeCreateInfo.ulNumOutputSurfaces = 2;
	videoDecodeCreateInfo.ulCreationFlags = cudaVideoCreate_PreferCUVID;
	videoDecodeCreateInfo.ulNumDecodeSurfaces = numberOfDecodeSurfaces;
	videoDecodeCreateInfo.vidLock = _cuCtxLock;
	videoDecodeCreateInfo.ulWidth = format->coded_width;
	videoDecodeCreateInfo.ulHeight = format->coded_height;
	videoDecodeCreateInfo.display_area.left = format->display_area.left;
	videoDecodeCreateInfo.display_area.top = format->display_area.top;
	videoDecodeCreateInfo.display_area.right = format->display_area.right;
	videoDecodeCreateInfo.display_area.bottom = format->display_area.bottom;
	{
		if (format->coded_width != (unsigned int)_context->width || format->coded_height != (unsigned int)_context->height)
		{
			_cuWidth = _context->width;
			_cuHeight = _context->height;
		}
		else 
		{
			_cuWidth = format->coded_width;
			_cuHeight = format->coded_height;
		}
		videoDecodeCreateInfo.ulTargetWidth = _cuWidth;
		videoDecodeCreateInfo.ulTargetHeight = _cuHeight;
	}
	_cuSurfaceHeight = videoDecodeCreateInfo.ulTargetHeight;

	::cuCtxPushCurrent(_cuContext);
	CUresult cret = ::cuvidCreateDecoder(&_cuDecoder, &videoDecodeCreateInfo);
	::cuCtxPopCurrent(NULL);
	LOG_D("[Nvdec] ProcessVideoSequence End..");
	return numberOfDecodeSurfaces;
}

int ESMNvdec::Core::ProcessPictureDecode(CUVIDPICPARAMS * picture)
{
	LOG_D("[Nvdec] ProcessPictureDecode Begin..");
	if (!_cuDecoder)
		return -1;
	::cuCtxPushCurrent(_cuContext);
	::cuvidDecodePicture(_cuDecoder, picture);
	::cuCtxPopCurrent(NULL);
	LOG_D("[Nvdec] ProcessPictureDecode End..");
	return 1;
}

int ESMNvdec::Core::ProcessPictureDisplay(CUVIDPARSERDISPINFO * display)
{
	LOG_D("[Nvdec] ProcessPictureDisplay Begin..");

	CUVIDPROCPARAMS videoProcessingParameters = {};
	videoProcessingParameters.progressive_frame = display->progressive_frame;
	videoProcessingParameters.second_field = display->repeat_first_field + 1;
	videoProcessingParameters.top_field_first = display->top_field_first;
	videoProcessingParameters.unpaired_field = display->repeat_first_field < 0;
	videoProcessingParameters.output_stream = _cuvidStream;

	CUdeviceptr dpSrcFrame = 0;
	unsigned int srcPitch = 0;
	::cuCtxPushCurrent(_cuContext);
	::cuvidMapVideoFrame(_cuDecoder, display->picture_index, &dpSrcFrame, &srcPitch, &videoProcessingParameters);
	unsigned char * pDecodedFrame = NULL;
	{	
		#ifdef WIN32	 
		ESMAutolock lock(&_frameLock);
		#else
		fd::locks::autolock lock(&_frame_lock);
		#endif
		if (size_t(++_nDecodedFrame) > _vFrame.size())
		{
			//_nframe_alloc++;
			unsigned char * pFrame = NULL;
			::cuMemAllocPitch((CUdeviceptr*)&pFrame, &_cuPitch, _cuWidth * (_cuBitdepthMinus8 ? 2 : 1), (_cuHeight >> 1) * 3, 16);
			_vFrame.push_back(pFrame);

			if ((_context->width != _cuWidth) || (_context->height != _cuHeight))
			{
				::cuMemAllocPitch((CUdeviceptr*)&pFrame, &_cuPitchResized, _context->width * (_cuBitdepthMinus8 ? 2 : 1), (_context->height >> 1) * 3, 16);
				_vFrameResized.push_back(pFrame);
			}
			if (_context->colorspace != ESMNvdec::COLORSPACE_T::NV12)
			{
				if(_context->colorspace == ESMNvdec::COLORSPACE_T::BGRA)
				{
					::cuMemAllocPitch((CUdeviceptr*)&pFrame, &_cuPitchConverted, 4 * _context->width, _context->height, 16);
					_vFrameConverted.push_back(pFrame);
				}
				else
				{
					size_t cuPitchConverted = 0;
					CUresult cret = ::cuMemAllocPitch((CUdeviceptr*)&pFrame, &cuPitchConverted, _context->width, (_context->height>>1) * 3, 16);

					_vFrameConverted.push_back(pFrame);
					_cuPitchConverted = cuPitchConverted;
				}
			}

			/*
			if(_context->colorspace == ESMNvdec::COLORSPACE_T::BGRA)
			{
				pFrame = NULL;
				::cuCtxPushCurrent(_cuContext);
				::cuMemAllocPitch((CUdeviceptr*)&pFrame, &_cuPitch2, _cuWidth << 2, _cuHeight, 16);
				::cuCtxPopCurrent(NULL);
				_vFrame2.push_back(pFrame);
			}
			else if(_context->colorspace == ESMNvdec::COLORSPACE_T::YV12)
			{
				pFrame = NULL;
				::cuCtxPushCurrent(_cuContext);
				::cuMemAllocPitch((CUdeviceptr*)&pFrame, &_cuPitch2, _cuWidth, (_cuHeight >> 1) * 3, 16);
				::cuCtxPopCurrent(NULL);
				_vFrame2.push_back(pFrame);
			}
			else if(_context->colorspace == ESMNvdec::COLORSPACE_T::I420)
			{
				pFrame = NULL;
				::cuCtxPushCurrent(_cuContext);
				::cuMemAllocPitch((CUdeviceptr*)&pFrame, &_cuPitch2, _cuWidth, (_cuHeight >> 1) * 3, 16);
				::cuCtxPopCurrent(NULL);
				_vFrame2.push_back(pFrame);
			}
			*/
		}
		pDecodedFrame = _vFrame[_nDecodedFrame - 1];
	}

	CUDA_MEMCPY2D m = { 0 };
	m.srcMemoryType = CU_MEMORYTYPE_DEVICE;
	m.srcDevice = dpSrcFrame;
	m.srcPitch = srcPitch;
	m.dstMemoryType = CU_MEMORYTYPE_DEVICE;
	m.dstDevice = (CUdeviceptr)(pDecodedFrame);
	m.dstPitch = _cuPitch ? _cuPitch : _cuWidth * (_cuBitdepthMinus8 ? 2 : 1);
	m.WidthInBytes = _cuWidth * (_cuBitdepthMinus8 ? 2 : 1);
	m.Height = _cuHeight;
	::cuMemcpy2DAsync(&m, _cuvidStream);

	m.srcDevice = (CUdeviceptr)((unsigned char *)dpSrcFrame + m.srcPitch * _cuSurfaceHeight);
	m.dstDevice = (CUdeviceptr)(m.dstHost = pDecodedFrame + m.dstPitch * _cuHeight);
	m.Height = _cuHeight >> 1;
	::cuMemcpy2DAsync(&m, _cuvidStream);
	::cuStreamSynchronize(_cuvidStream);
	::cuCtxPopCurrent(NULL);

	if (int(_vTimestamp.size()) < _nDecodedFrame)
		_vTimestamp.resize(_vFrame.size());
	_vTimestamp[_nDecodedFrame - 1] = display->timestamp;

	if (int(_vReturedUserData.size()) < _nDecodedFrame)
		_vReturedUserData.resize(_vFrame.size());


	auto it = _mAppUserData.find(display->timestamp);
	void* pUserdata = nullptr;
	if ( it != _mAppUserData.end() ) {
		pUserdata = it->second;
		_mAppUserData.erase(it);
	}
	_vReturedUserData[_nDecodedFrame - 1] = pUserdata;

	::cuvidUnmapVideoFrame(_cuDecoder, dpSrcFrame);

	LOG_D("[Nvdec] ProcessPictureDisplay End.._nDecodedFrame(%d)", _nDecodedFrame);
	return 1;
}

int ESMNvdec::Core::GetNumberofDecodeSurfaces(cudaVideoCodec codec, int width, int height)
{
	if (codec == cudaVideoCodec_H264) 
		return 20;
	if (codec == cudaVideoCodec_HEVC) 
	{
		// ref HEVC spec: A.4.1 General tier and level limits
		// currently assuming level 6.2, 8Kx4K
		int MaxLumaPS = 35651584;
		int MaxDpbPicBuf = 6;
		int PicSizeInSamplesY = (int)(width * height);
		int MaxDpbSize;
		if (PicSizeInSamplesY <= (MaxLumaPS >> 2))
			MaxDpbSize = MaxDpbPicBuf * 4;
		else if (PicSizeInSamplesY <= (MaxLumaPS >> 1))
			MaxDpbSize = MaxDpbPicBuf * 2;
		else if (PicSizeInSamplesY <= ((3 * MaxLumaPS) >> 2))
			MaxDpbSize = (MaxDpbPicBuf * 4) / 3;
		else
			MaxDpbSize = MaxDpbPicBuf;
		return (std::min)(MaxDpbSize, 16) + 4;
	}
	return 8;
}

int ESMNvdec::Core::ProcessVideoSequence(void * user_data, CUVIDEOFORMAT * format)
{
	return (static_cast<ESMNvdec::Core*>(user_data))->ProcessVideoSequence(format);
}

int ESMNvdec::Core::ProcessPictureDecode(void * user_data, CUVIDPICPARAMS * picture)
{
	return (static_cast<ESMNvdec::Core*>(user_data))->ProcessPictureDecode(picture);
}

int ESMNvdec::Core::ProcessPictureDisplay(void * user_data, CUVIDPARSERDISPINFO * display)
{
	return (static_cast<ESMNvdec::Core*>(user_data))->ProcessPictureDisplay(display);
}


// add
size_t	ESMNvdec::Core::GetBitdepth() {
	return _cuBitdepthMinus8;
}
CUstream ESMNvdec::Core::GetCuvidStream() {
	return _cuvidStream;
}
size_t	ESMNvdec::Core::GetSurfaceHeight() {
	return _cuSurfaceHeight;
}