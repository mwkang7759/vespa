#include "ESMNvdec.h"
#include "ESMNvdecCore.h"

ESMNvdec::ESMNvdec(void)
{
	_core = new ESMNvdec::Core();
}

ESMNvdec::~ESMNvdec(void)
{
	if(_core)
	{
		if(_core->IsInitialized())
			_core->Release();
		delete _core;
		_core = NULL;
	}
}

bool ESMNvdec::IsInitialized(void)
{
	return _core->IsInitialized();
}

void * ESMNvdec::GetContext(void)
{
	return _core->GetContext();
}

int ESMNvdec::Initialize(ESMNvdec::CONTEXT_T * ctx)
{
	return _core->Initialize(ctx);
}

int ESMNvdec::Release(void)
{
	return _core->Release();
}

int ESMNvdec::Decode(unsigned char * bitstream, int bitstreamSize, long long bitstreamTimestamp, unsigned char *** decoded, int * numberOfDecoded, long long ** timetstamp, void * userdata, void *** returedUserData, unsigned long flags)
{
	return _core->Decode(bitstream, bitstreamSize, bitstreamTimestamp, decoded, numberOfDecoded, timetstamp, userdata, returedUserData, flags);
}

int ESMNvdec::Flush()
{
	unsigned char** ppDecoded = NULL;
	int nDecoded = 0;
	long long* pTimetstamp = NULL;
	return _core->Decode(nullptr, 0, 0, &ppDecoded, &nDecoded, &pTimetstamp, nullptr, nullptr, 0x01);
}

size_t ESMNvdec::GetPitch(void)
{
	return _core->GetPitch();
}

size_t ESMNvdec::GetPitchResized(void)
{
	return _core->GetPitchResized();
}

size_t ESMNvdec::GetPitchConverted(void)
{
	return _core->GetPitchConverted();
}

size_t ESMNvdec::GetPitch2(void)
{
	return _core->GetPitch2();
}

unsigned char* ESMNvdec::Alloc(size_t bytesize)
{
	return _core->Alloc(bytesize);
}

void ESMNvdec::Free(void* pFrame)
{
	_core->Free(pFrame);
}

size_t ESMNvdec::Copy(void* pDstFrame, const void* pSrcFrame, size_t bytesize)
{
	return _core->Copy(pDstFrame, pSrcFrame, bytesize);
}


void* ESMNvdec::GetStream() {
	return _core->GetCuvidStream();
}

// add
size_t ESMNvdec::GetBitdepth() {
	return _core->GetBitdepth();
}

size_t	ESMNvdec::GetSurfaceHeight() {
	return _core->GetSurfaceHeight();
}