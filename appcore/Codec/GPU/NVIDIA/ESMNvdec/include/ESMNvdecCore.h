#pragma once

#include "ESMNvdec.h"
#include <nvcuvid.h>
#include <cuda.h>
//#include <dynlink_cudaD3D11.h>
#include <vector>
#include <map>

#include "TraceAPI.h"

class ESMNvdec::Core
{
public:
	Core(void);
	virtual ~Core(void);

	bool	IsInitialized(void);
	void *	GetContext(void);

	int		Initialize(ESMNvdec::CONTEXT_T * ctx);
	int		Release(void);

	int		Decode(unsigned char* bitstream, int bitstreamSize, long long bitstreamTimestamp, unsigned char*** nv12, int* numberOfDecoded, long long** timetstamp, void* userdata, void*** returedUserData, unsigned long flags = 0);
	size_t	GetPitch(void);
	size_t	GetPitchResized(void);
	size_t	GetPitchConverted(void);
	size_t	GetPitch2(void);

	unsigned char* Alloc(size_t bytesize);
	void	Free(void* pFrame);
	size_t	Copy(void* pDstFrame, const void* pSrcFrame, size_t bytesize);

	// add mwkang
	size_t	GetBitdepth();
	CUstream	GetCuvidStream();
	size_t	GetSurfaceHeight();
	CUstream			m_cuvidStream = 0;

private:
	int ProcessVideoSequence(CUVIDEOFORMAT * format);
	int ProcessPictureDecode(CUVIDPICPARAMS * picture);
	int ProcessPictureDisplay(CUVIDPARSERDISPINFO * display);
	int GetNumberofDecodeSurfaces(cudaVideoCodec codec, int width, int height);

#ifdef WIN32
	static int __stdcall ProcessVideoSequence(void * user_data, CUVIDEOFORMAT * format);
	static int __stdcall ProcessPictureDecode(void * user_data, CUVIDPICPARAMS * picture);
	static int __stdcall ProcessPictureDisplay(void * user_data, CUVIDPARSERDISPINFO * display);
#else
	static int ProcessVideoSequence(void * user_data, CUVIDEOFORMAT * format);
	static int ProcessPictureDecode(void * user_data, CUVIDPICPARAMS * picture);
	static int ProcessPictureDisplay(void * user_data, CUVIDPARSERDISPINFO * display);
#endif

private:
	Core(const ESMNvdec::Core & clone);

private:
	bool _initialized;
	ESMNvdec::CONTEXT_T * _context;
#ifdef WIN32
	CRITICAL_SECTION	_lock;
	CRITICAL_SECTION	_lock2;
	CRITICAL_SECTION			_frameLock;
#else
	pthread_mutex_t _lock;
    pthread_mutex_t _lock2;
	pthread_mutex_t         _frame_lock;
#endif
	CUcontext			_cuContext;
	CUvideoctxlock		_cuCtxLock;
	CUvideoparser		_cuParser;
	CUvideodecoder		_cuDecoder;
	CUstream			_cuvidStream = 0;

	int						_cuWidth;
	int						_cuHeight;
	int						_cuSurfaceHeight;
	cudaVideoCodec			_cuCodec;
	cudaVideoChromaFormat	_cuChromaFormat;
	int						_cuBitdepthMinus8;
	CUVIDEOFORMAT			_cuFormat;
	size_t					_cuPitch;
	size_t					_cuPitchResized;
	size_t					_cuPitchConverted;
	size_t					_cuPitch2;

	std::vector<unsigned char*>	_vFrame;
	std::vector<unsigned char*>	_vFrameResized;
	std::vector<unsigned char*>	_vFrameConverted;
	std::vector<unsigned char*>	_vFrame2;
	std::vector<long long>		_vTimestamp;	
	
	std::vector<void*>		_vReturedUserData;	
	std::map<long long, void*> _mAppUserData;
	int							_nDecodedFrame;
	int							_ndecodedFrameReturned;
};