#pragma once

#if defined(WIN32) && defined(_USRDLL)
#if defined(EXPORT_ESM_NVDEC)
#define ESM_NVDEC_CLASS __declspec(dllexport)
#else
#define ESM_NVDEC_CLASS __declspec(dllimport)
#endif
#else
#define ESM_NVDEC_CLASS 
#endif

#include <ESMBase.h>

//class ESM_NVDEC_CLASS ESMNvdec
class ESMNvdec
	: public ESMBase
{
public:
	class Core;
public:
	struct CONTEXT_T
	{
		int deviceIndex = 0;
		int width = 3840;
		int height = 2160;
		int codec = ESMNvdec::VIDEO_CODEC_T::AVC;
		int colorspace = ESMNvdec::COLORSPACE_T::BGRA;
		bool operator==(const CONTEXT_T& rhs) {
			if (this->deviceIndex != rhs.deviceIndex ||
				this->width != rhs.width ||
				this->height != rhs.height ||
				this->codec != rhs.codec ||
				this->colorspace != rhs.colorspace)
				return false;
			return true;
		}
		bool operator!=(const CONTEXT_T& rhs) {
			return !(*this == rhs);
		}
	};


	ESMNvdec(void);
	virtual ~ESMNvdec(void);

	bool	IsInitialized(void);
	void *	GetContext(void);

	int		Initialize(ESMNvdec::CONTEXT_T * ctx);
	int		Release(void);
	int		Decode(unsigned char * bitstream, int bitstreamSize, long long bitstreamTimestamp, unsigned char *** decoded, int * numberOfDecoded, long long ** timetstamp, void * userdata, void *** returedUserData, unsigned long flags = 0);
	int		Flush();
	
	size_t	GetPitch(void);
	size_t	GetPitchResized(void);
	size_t	GetPitchConverted(void);
	size_t	GetPitch2(void);

	// add mwkang
	size_t	GetBitdepth();
	size_t	GetSurfaceHeight();
	void* GetStream();
	
	unsigned char* Alloc(size_t bytesize);
	void	Free(void* pFrame);
	size_t	Copy(void* pDstFrame, const void* pSrcFrame, size_t bytesize);

private:
	ESMNvdec(const ESMNvdec & clone);

private:
	ESMNvdec::Core * _core;
};
