﻿#include "ESMNvresizerCore.h"
#include "resizer.h"

ESMNvresizer::Core::Core(void)
	: _initialized(false)
	, _cuContext(NULL)
	, _cuPitch(0)
	, _cuFrame(NULL)
{

}

ESMNvresizer::Core::~Core(void)
{

}

bool ESMNvresizer::Core::IsInitialized(void)
{
	return _initialized;
}

int ESMNvresizer::Core::Initialize(ESMNvresizer::CONTEXT_T * ctx)
{
	_context = ctx;
	//jsyang 2020-11-11
	//참조 안된 지역변수 Warning 제거 
	//CUcontext current_context;
	int ngpu = 0;
	CUresult cret = ::cuInit(0);
	cret = ::cuDeviceGetCount(&ngpu);
	if((_context->deviceIndex < 0) || (_context->deviceIndex >= ngpu))
		return -1;
	CUdevice cuDevice;
	cret = ::cuDeviceGet(&cuDevice, _context->deviceIndex);
	cret = ::cuCtxCreate(&_cuContext, CU_CTX_SCHED_AUTO, cuDevice);
	//cret = ::cuCtxPopCurrent(&current_context);
	if(_context->colorspace == ESMNvresizer::COLORSPACE_T::BGRA)
	{
		::cuCtxPushCurrent(_cuContext);
		::cuMemAllocPitch((CUdeviceptr*)&_cuFrame, &_cuPitch, 4 * _context->outputWidth, _context->outputHeight, 16);
		::cuCtxPopCurrent(NULL);
	}
	else
	{
		::cuCtxPushCurrent(_cuContext);
		::cuMemAllocPitch((CUdeviceptr*)&_cuFrame, &_cuPitch, _context->outputWidth, (_context->outputHeight>>1) * 3, 16);
		::cuCtxPopCurrent(NULL);
	}
	_initialized = true;

	//return ESMNvresizer::ERR_CODE_T::SUCCESS;
	if (cret == CUDA_SUCCESS)
		return ESMNvresizer::ERR_CODE_T::SUCCESS;
	else
		return ESMNvresizer::ERR_CODE_T::GENERIC_FAIL;
	
}

int ESMNvresizer::Core::Release(void)
{
	::cuCtxPushCurrent(_cuContext);
	::cuMemFree((CUdeviceptr)(_cuFrame));
	::cuCtxPopCurrent(NULL);

	::cuCtxDestroy(_cuContext);
	_initialized = false;

	return ESMNvresizer::ERR_CODE_T::SUCCESS;
}

int ESMNvresizer::Core::Resize(unsigned char * input, int inputPitch, unsigned char ** output, int & outputPitch)
{
	// add mwkang 2020.05.25
	if (_context->colorspace == ESMNvresizer::COLORSPACE_T::YV12)
		ESMNvresizer::CUDAResizer::resize_yv12(_cuFrame, _cuPitch, _context->outputWidth, _context->outputHeight, input, inputPitch, _context->inputWidth, _context->inputHeight);
	else if (_context->colorspace == ESMNvresizer::COLORSPACE_T::NV12)
		ESMNvresizer::CUDAResizer::resize_nv12(_cuFrame, _cuPitch, _context->outputWidth, _context->outputHeight, input, inputPitch, _context->inputWidth, _context->inputHeight);
	else
		ESMNvresizer::CUDAResizer::resize_yv12(_cuFrame, _cuPitch, _context->outputWidth, _context->outputHeight, input, inputPitch, _context->inputWidth, _context->inputHeight);

	*output = _cuFrame;
	outputPitch = _cuPitch;

	return ESMNvresizer::ERR_CODE_T::SUCCESS;
}