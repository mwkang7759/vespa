#include "ESMNvresizer.h"
#include <cuda.h>

class ESMNvresizer::Core
{
public:
	Core(void);
	virtual ~Core(void);

	bool	IsInitialized(void);

	int		Initialize(ESMNvresizer::CONTEXT_T * ctx);
	int		Release(void);
	int		Resize(unsigned char * input, int inputPitch, unsigned char ** output, int & outputPitch);

private:
	bool						_initialized;
	CUcontext					_cuContext;
	ESMNvresizer::CONTEXT_T *	_context;
	size_t						_cuPitch;
	unsigned char *				_cuFrame;
};