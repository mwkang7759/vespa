#pragma once

#include "ESMNvenc.h"
#include <nvEncodeAPI.h>
#include "ESMNvencQueue.h"
#include <vector>
#include <cuda.h>

class ESMNvenc::Core
{
public:
	static const int MAX_ENCODE_QUEUE = 32;

	typedef struct _INPUT_BUFFER_T
	{
		int						width;
		int						height;
		void *					dptr;
		NV_ENC_REGISTERED_PTR	registeredPtr;
		int						chromaOffsets[2];
		int						nChromaPlanes;
		int						pitch;
		int						chromaPitch;
		NV_ENC_BUFFER_FORMAT	format;
		NV_ENC_INPUT_PTR		inputPtr;
		long long				timestamp;
		_INPUT_BUFFER_T(void)
			: width(0)
			, height(0)
			, dptr(nullptr)
			, registeredPtr(nullptr)
			, nChromaPlanes(0)
			, pitch(0)
			, chromaPitch(0)
			, format(NV_ENC_BUFFER_FORMAT_NV12)
			, timestamp(0)
		{}
	} INPUT_BUFFER_T;

	typedef struct _OUTPUT_BUFFER_T
	{
		NV_ENC_OUTPUT_PTR		buffer;
		bool					async;
#ifdef WIN32
		HANDLE					asyncEvent;
#endif
		bool					eos;
#ifdef WIN32
		_OUTPUT_BUFFER_T(void)
			: buffer(nullptr)
			, asyncEvent(INVALID_HANDLE_VALUE)
			, eos(false)
		{}
#else
		_OUTPUT_BUFFER_T(void)
			: buffer(nullptr)
			, eos(false)
		{}
#endif
	} OUTPUT_BUFFER_T;

	typedef struct _BUFFER_T
	{
		ESMNvenc::Core::INPUT_BUFFER_T	input;
		ESMNvenc::Core::OUTPUT_BUFFER_T	output;
	} BUFFER_T;

	typedef struct _CIRCULAR_BUFFER_T
	{
		long long	timestamp;
		int			amount;
		_CIRCULAR_BUFFER_T * prev;
		_CIRCULAR_BUFFER_T * next;
	} CIRCULAR_BUFFER_T;

	Core(void);
	virtual ~Core(void);

	bool	IsInitialized(void);
	int		Initialize(ESMNvenc::CONTEXT_T * ctx);
	int		Release(void);

	int		Encode(void * input, int inputStride, long long timetstamp, unsigned char * bitstream, int bitstreamCapacity, int & bitstreamSize, long long & bitstreamTimestamp);
	int		Encode(void* input, int inputStride, long long timetstamp, unsigned char* bitstream, int bitstreamCapacity, int& bitstreamSize, long long& bitstreamTimestamp, bool forceIdr, int& frameType);																				
	NVENCSTATUS FlushEncoder(void);

	unsigned char * GetExtradata(int & size);
private:
	//void	BeginYUVDebug(void);
	//void	ProcessYUVDebug(unsigned char * dptr, int pitch);
	//void	EndYUVDebug(void);

	//int		Encode(void * input, int inputStride, long long timestamp, ESMNvenc::ENTITY_T * output);
	int		Encode(void* input, int inputStride, long long timestamp, bool forceIdr, ESMNvenc::ENTITY_T* output);
	int		Encode(ESMNvenc::ENTITY_T * input, ESMNvenc::ENTITY_T * output);

private:
	NVENCSTATUS InitializeCuda(int deviceId);
	NVENCSTATUS ReleaseCuda(void);

	NVENCSTATUS	InitializeESMNvenc(void * device, NV_ENC_DEVICE_TYPE type);
	NVENCSTATUS ReleaseESMNvenc(void);

	NVENCSTATUS AllocateBuffers(int width, int height, bool encodeAsync, NV_ENC_BUFFER_FORMAT format);
	NVENCSTATUS ReleaseBuffers(void);
	NVENCSTATUS ProcessOutput(ESMNvenc::Core::BUFFER_T * nvencBuffer, ESMNvenc::ENTITY_T * bitstream, bool flush = false);
	int			NVGetCapability(GUID codec, NV_ENC_CAPS caps);
	NVENCSTATUS NVEncodeFrame(ESMNvenc::Core::BUFFER_T * nvencBuffer, ESMNvenc::ENTITY_T * input);

	int			NVGetChromaPitch(const NV_ENC_BUFFER_FORMAT format, const int lumaP);
	int			NVGetNumberChromaPlanes(const NV_ENC_BUFFER_FORMAT format);
	int			NVGetChromaHeight(const NV_ENC_BUFFER_FORMAT format, const int lumaH);
	int			NVGetChromaWidthInBytes(const NV_ENC_BUFFER_FORMAT format, const uint32_t lumaW);
	void		NVGetChromaSubplaneOffsets(const NV_ENC_BUFFER_FORMAT format, const int pitch, const int height, std::vector<int> & chroma_offsets);
	int			NVGetWidthInBytes(const NV_ENC_BUFFER_FORMAT format, const int width);

	NV_ENC_REGISTERED_PTR NVRegisterResource(void * buffer, NV_ENC_INPUT_RESOURCE_TYPE type, int width, int height, int pitch, NV_ENC_BUFFER_FORMAT format);

	bool		ConvertWide2Multibyte(wchar_t * src, char ** dst);
	bool		ConvertMultibyte2Wide(char * src, wchar_t ** dst);

private:
	Core(const ESMNvenc::Core & clone);

private:
	ESMNvenc::CONTEXT_T * _context;
	void *		_nvencContext;
	bool		_isInitialized;
#ifdef WIN32
	HINSTANCE	_nvencInstance;
#endif
	NV_ENCODE_API_FUNCTION_LIST _nvenc;
	void *		_ESMNvenc;
	NV_ENC_BUFFER_FORMAT _format;
	ESMNvenc::Core::BUFFER_T _nvencBuffer[MAX_ENCODE_QUEUE];
	ESMNvencQueue<ESMNvenc::Core::BUFFER_T> _nvencBufferQueue;
	int			_nvencBufferCount;
#ifdef WIN32
	HANDLE		_eosEvent;
	HANDLE		_dump;
#endif
	unsigned char	_extradata[100];
	int				_extradataSize;

	//
	//NV_ENC_SEI_PAYLOAD	_sei;
	//uint8_t				_sei_payload[1800];
	////HANDLE			_yuv_file;
	
};