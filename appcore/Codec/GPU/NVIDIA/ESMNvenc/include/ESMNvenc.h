#pragma once

#if defined(WIN32) && defined(_USRDLL)
#if defined(EXPORT_ESM_NVENC)
#define ESM_NVENC_CLASS __declspec(dllexport)
#else
#define ESM_NVENC_CLASS __declspec(dllimport)
#endif
#else
#define ESM_NVENC_CLASS
#endif

#include <ESMBase.h>

//class ESM_NVENC_CLASS ESMNvenc
class ESMNvenc
	: public ESMBase
{
public:
	class Core;
public:
	struct CONTEXT_T
	{
		int deviceIndex = 0;
		int width = 3840;
		int height = 2160;
		int codec = ESMNvenc::VIDEO_CODEC_T::HEVC;
		int profile = ESMNvenc::HEVC_PROFILE_T::DP;
		int gop = 1;
		int bitrate = 70000000;
		int colorspace = ESMNvenc::COLORSPACE_T::NV12;
		int fps_num = 30;
		int fps_den = 1;
		bool operator==(const CONTEXT_T& rhs) {
			if (this->deviceIndex != rhs.deviceIndex ||
				this->width != rhs.width ||
				this->height != rhs.height ||
				this->codec != rhs.codec ||
				this->profile != rhs.profile ||
				this->gop != rhs.gop ||
				this->bitrate != rhs.bitrate ||
				this->colorspace != rhs.colorspace ||
				this->fps_num != rhs.fps_num ||
				this->fps_den != rhs.fps_den)
				return false;
			return true;
		}
		bool operator!=(const CONTEXT_T& rhs)
		{
			return !(*this == rhs);
		}
	};

	typedef struct _ENTITY_T
	{
		void *		data;
		int			dataPitch;
		int			dataSize;
		int			dataCapacity;
		long long	timestamp;
		int			frameType;
		bool		forceIdr;		  
		_ENTITY_T(void)
			: data(NULL)
			, dataPitch(0)
			, dataSize(0)
			, dataCapacity(0)
			, timestamp(0)
			, frameType(0)
			, forceIdr(false)	 
		{}

	} ENTITY_T;
	
	ESMNvenc(void);
	virtual ~ESMNvenc(void);
	
	

	bool	IsInitialized(void);
	int		Initialize(ESMNvenc::CONTEXT_T * ctx);
	int		Release(void);

	int		Encode(void * input, int inputStride, long long timestamp, unsigned char * bitstream, int bitstreamCapacity, int & bitstreamSize, long long & bitstreamTimestamp);
	int		Encode(void* input, int inputStride, long long timestamp, unsigned char* bitstream, int bitstreamCapacity, int& bitstreamSize, long long& bitstreamTimestamp, bool forceIdr, int& frameType);																																															   
	int		FlushEncoder(void);

	unsigned char * GetExtradata(int & size);

private:
	ESMNvenc(const ESMNvenc & clone);

private:
	ESMNvenc::Core* _core;
};