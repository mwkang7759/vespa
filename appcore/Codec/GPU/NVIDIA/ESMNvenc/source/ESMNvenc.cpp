#include "ESMNvenc.h"
#include "ESMNvencCore.h"

ESMNvenc::ESMNvenc()
{
	_core = new ESMNvenc::Core();
}

ESMNvenc::~ESMNvenc()
{
	if(_core)
	{
		if(_core->IsInitialized())
			_core->Release();
		delete _core;
		_core = NULL;
	}
}

bool ESMNvenc::IsInitialized(void)
{
	return _core->IsInitialized();
}

int ESMNvenc::Initialize(ESMNvenc::CONTEXT_T * ctx)
{
	return _core->Initialize(ctx);
}

int ESMNvenc::Release()
{
	return _core->Release();
}

int ESMNvenc::Encode(void * input, int inputStride, long long timestamp, unsigned char * bitstream, int bitstreamCapacity, int & bitstreamSize, long long & bitstreamTimestamp)
{
	return _core->Encode(input, inputStride, timestamp, bitstream, bitstreamCapacity, bitstreamSize, bitstreamTimestamp);
}

int ESMNvenc::Encode(void* input, int inputStride, long long timestamp, unsigned char* bitstream, int bitstreamCapacity, int& bitstreamSize, long long& bitstreamTimestamp, bool forceIdr, int& frameType)
{
	return _core->Encode(input, inputStride, timestamp, bitstream, bitstreamCapacity, bitstreamSize, bitstreamTimestamp, forceIdr, frameType);
}

int ESMNvenc::FlushEncoder(void)
{
	return _core->FlushEncoder();
}

unsigned char * ESMNvenc::GetExtradata(int & size)
{
	return _core->GetExtradata(size);
}