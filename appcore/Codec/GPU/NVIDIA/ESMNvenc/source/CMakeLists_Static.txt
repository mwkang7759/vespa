cmake_minimum_required(VERSION 3.16)

set(CMAKE_CUDA_COMPILER "${CUDA_TOOLKIT_ROOT_DIR}/bin/nvcc")

enable_language("CUDA")

# add all current files to SRC_FILES
file(GLOB_RECURSE SRC_FILES CONFIGURE_DEPENDS
	${CMAKE_CURRENT_SOURCE_DIR}/*.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/*.c
	)
# make static library
add_library(ESMNvenc STATIC ${SRC_FILES})

# header path for using compile library
target_include_directories(ESMNvenc PUBLIC ${CMAKE_SOURCE_DIR}/include
	../include
	${CMAKE_SOURCE_DIR}/Codec/GPU/Base/include
	${CMAKE_SOURCE_DIR}/Codec/GPU/NVIDIA/Interface
	${CMAKE_SOURCE_DIR}/_3rdparty_/linux/cuda11.0/include)

# compile options
#target_compile_options(ESMNvenc PRIVATE -Wall -Werror -Wno-error=deprecated-declarations -Wno-error=unused-variable -DLinux)
target_compile_options(ESMNvenc PRIVATE $<$<COMPILE_LANGUAGE:CUDA>:-arch=compute_60 -code=sm_60,sm_61,sm_62,sm_70,sm_72,sm_75>)
