#pragma once

#if defined(WIN32) && defined(_USRDLL)
#if defined(EXPORT_ESM_NVCONVERTER)
#define ESM_NVCONVERTER_CLASS __declspec(dllexport)
#else
#define ESM_NVCONVERTER_CLASS __declspec(dllimport)
#endif
#else
#define ESM_NVCONVERTER_CLASS
#endif

#include <ESMBase.h>

//class ESM_NVCONVERTER_CLASS ESMNvconverter
class ESMNvconverter
	: public ESMBase
{
public:
	class CUDAConverter;
	class Core;
public:
	typedef struct _CONTEXT_T
	{
		int deviceIndex;
		int inputColorspace;
		int outputColorspace;
		int width;
		int height;
		_CONTEXT_T()
			: deviceIndex(0)
			, inputColorspace(ESMNvconverter::COLORSPACE_T::BGRA)
			, outputColorspace(ESMNvconverter::COLORSPACE_T::YV12)
			, width(3840)
			, height(2160)
		{}
	} CONTEXT_T;

	ESMNvconverter(void);
	virtual ~ESMNvconverter(void);

	bool	IsInitialized(void);

	int		Initialize(ESMNvconverter::CONTEXT_T * ctx);
	int		Release(void);
	int		ConvertYUV2BGRA(unsigned char * input, int inputPitch, unsigned char ** output, int & outputPitch);
	int		ConvertBGRA2YUV(unsigned char* src, int srcPitch, unsigned char* dst, int dstPitch, int width, int height);

private:
	ESMNvconverter::Core * _core;
};
