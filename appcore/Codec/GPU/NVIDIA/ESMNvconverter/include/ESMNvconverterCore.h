#include "ESMNvconverter.h"
#include <cuda.h>

class ESMNvconverter::Core
{
public:
	Core(void);
	virtual ~Core(void);

	bool	IsInitialized(void);

	int		Initialize(ESMNvconverter::CONTEXT_T * ctx);
	int		Release(void);
	int		ConvertYUV2BGRA(unsigned char * input, int inputPitch, unsigned char ** output, int & outputPitch);
	int		ConvertBGRA2YUV(unsigned char* src, int srcPitch, unsigned char* dst, int dstPitch, int width, int height);
private:
	bool						_initialized;
	CUcontext					_cuContext;
	ESMNvconverter::CONTEXT_T *	_context;
	size_t						_cuPitch;
	unsigned char *				_cuFrame;
};