cmake_minimum_required(VERSION 3.16)

set(CMAKE_CUDA_COMPILER "${CUDA_TOOLKIT_ROOT_DIR}/bin/nvcc")

enable_language("CUDA")

# add all current files to SRC_FILES
file(GLOB_RECURSE SRC_FILES CONFIGURE_DEPENDS
	${CMAKE_CURRENT_SOURCE_DIR}/*.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/*.c
	../convert/converter.cu
	)
# make static library
add_library(ESMNvconverter STATIC ${SRC_FILES})

# header path for using compile library
target_include_directories(ESMNvconverter PUBLIC ${CMAKE_SOURCE_DIR}/include
	../include
	../convert
	${CMAKE_SOURCE_DIR}/_3rdparty_/linux/cuda11.0/include
	${CMAKE_SOURCE_DIR}/Codec/GPU/Base/include)

set_target_properties(ESMNvconverter PROPERTIES CUDA_SEPARABLE_COMPILATION ON)
set_target_properties(ESMNvconverter PROPERTIES CUDA_ARCHITECTURES "35;50;75")

# compile options
#target_compile_options(ESMNvconverter PRIVATE -DLinux -Wall -Wno-error=unused-but-set-variable)
target_compile_options(ESMNvconverter PRIVATE $<$<COMPILE_LANGUAGE:CUDA>:-arch=compute_60 -code=sm_60,sm_61,sm_62,sm_70,sm_72,sm_75>)
