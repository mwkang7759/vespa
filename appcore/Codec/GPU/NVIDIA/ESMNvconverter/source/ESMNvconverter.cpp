#include "ESMNvconverter.h"
#include "ESMNvconverterCore.h"

ESMNvconverter::ESMNvconverter(void)
{
	_core = new ESMNvconverter::Core();
}

ESMNvconverter::~ESMNvconverter(void)
{
	if(_core)
	{
		delete _core;
	}
	_core = NULL;
}

bool ESMNvconverter::IsInitialized(void)
{
	return _core->IsInitialized();
}

int	ESMNvconverter::Initialize(ESMNvconverter::CONTEXT_T * ctx)
{
	return _core->Initialize(ctx);
}

int	ESMNvconverter::Release(void)
{
	return _core->Release();
}

int	ESMNvconverter::ConvertYUV2BGRA(unsigned char * input, int inputPitch, unsigned char ** output, int & outputPitch)
{
	return _core->ConvertYUV2BGRA(input, inputPitch, output, outputPitch);
}


int	ESMNvconverter::ConvertBGRA2YUV(unsigned char* src, int srcPitch, unsigned char* dst, int dstPitch, int width, int height)
{
	return _core->ConvertBGRA2YUV(src, srcPitch, dst, dstPitch, width, height);
}