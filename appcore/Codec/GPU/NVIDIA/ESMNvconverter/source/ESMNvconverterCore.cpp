﻿#include "ESMNvconverterCore.h"
#include "converter.h"

ESMNvconverter::Core::Core(void)
	: _initialized(false)
	, _cuContext(NULL)
	, _cuPitch(0)
	, _cuFrame(NULL)
{

}

ESMNvconverter::Core::~Core(void)
{

}

bool ESMNvconverter::Core::IsInitialized(void)
{
	return _initialized;
}

int ESMNvconverter::Core::Initialize(ESMNvconverter::CONTEXT_T * ctx)
{
	_context = ctx;
	int ngpu = 0;
	CUresult cret = ::cuInit(0);
	cret = ::cuDeviceGetCount(&ngpu);
	if((_context->deviceIndex < 0) || (_context->deviceIndex >= ngpu))
		return -1;
	CUdevice cuDevice;
	cret = ::cuDeviceGet(&cuDevice, _context->deviceIndex);
	cret = ::cuCtxCreate(&_cuContext, CU_CTX_SCHED_BLOCKING_SYNC, cuDevice);
	if(_context->outputColorspace == ESMNvconverter::COLORSPACE_T::BGRA)
	{
		::cuCtxPushCurrent(_cuContext);
		::cuMemAllocPitch((CUdeviceptr*)&_cuFrame, &_cuPitch, 4 * _context->width, _context->height, 16);
		::cuCtxPopCurrent(NULL);
	}
	else
	{
		::cuCtxPushCurrent(_cuContext);
		::cuMemAllocPitch((CUdeviceptr*)&_cuFrame, &_cuPitch, _context->width, (_context->height>>1) * 3, 16);
		::cuCtxPopCurrent(NULL);
	}
	_initialized = true;

	return ESMNvconverter::ERR_CODE_T::SUCCESS;
}

int ESMNvconverter::Core::Release(void)
{
	::cuCtxPushCurrent(_cuContext);
	::cuMemFree((CUdeviceptr)(_cuFrame));
	::cuCtxPopCurrent(NULL);
	::cuCtxDestroy(_cuContext);
	_initialized = false;

	return ESMNvconverter::ERR_CODE_T::SUCCESS;
}

int ESMNvconverter::Core::ConvertYUV2BGRA(unsigned char * input, int inputPitch, unsigned char ** output, int & outputPitch)
{
	if(_context->inputColorspace == ESMNvconverter::COLORSPACE_T::YV12)
	{
		ESMNvconverter::CUDAConverter::convert_yv12_to_bgra32(input, inputPitch, _cuFrame, int(_cuPitch), _context->width, _context->height);
	}
	else
	{
		ESMNvconverter::CUDAConverter::convert_nv12_to_bgra32(input, inputPitch, _cuFrame, int(_cuPitch), _context->width, _context->height);
	}

	*output = _cuFrame;
	outputPitch = int(_cuPitch);

	return ESMNvconverter::ERR_CODE_T::SUCCESS;
}


int	ESMNvconverter::Core::ConvertBGRA2YUV(unsigned char* src, int srcPitch, unsigned char* dst, int dstPitch, int width, int height)
{
	if (_context->outputColorspace == ESMNvconverter::COLORSPACE_T::YV12)
	{
		ESMNvconverter::CUDAConverter::convert_bgra32_to_yv12(src, srcPitch, dst, dstPitch, width, height);
	}
	else if (_context->outputColorspace == ESMNvconverter::COLORSPACE_T::UYVY)
	{
		ESMNvconverter::CUDAConverter::convert_bgra32_to_uyvy(src, srcPitch, dst, dstPitch, width, height);
	}
	else
	{
		ESMNvconverter::CUDAConverter::convert_bgra32_to_nv12(src, srcPitch, dst, dstPitch, width, height);
	}

	return ESMNvconverter::ERR_CODE_T::SUCCESS;
}
