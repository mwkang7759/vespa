﻿#include "ESMFFQsvdec.h"
#include <vector>
#include <string>
#include <memory>

#pragma warning(disable:4819)

extern "C"{
	#include <libavcodec/avcodec.h>
	#include <libavformat/avformat.h>
	#include <libavutil/mem.h>
	#include <libavutil/opt.h>
	#include <libavutil/imgutils.h>
	#include <libswscale/swscale.h>
	#include <libavutil/mathematics.h>
	#include <libavutil/time.h>
	#include <libavutil/buffer.h>
	#include <libavutil/error.h>
	#include <libavutil/hwcontext.h>
	#include <libavutil/hwcontext_qsv.h>
	#include <libavutil/mem.h>
};

#include <ppl.h>
#include <concurrent_queue.h>

class ESMFFQsvdec::Core
{
public:
	Core(void);
	virtual ~Core(void);
	
	BOOL	IsInitialized(void);
	int32_t	Initialize(ESMFFQsvdec::CONTEXT_T * ctx);
	int32_t	Release(void);
	int32_t	Decode(unsigned char* bitstream, int32_t bitstreamSize, unsigned char*** decoded, int32_t* numberOfDecoded, BOOL drawLogo);

private:
	BOOL FileExists(LPSTR szPath);

private:
	BOOL						_isInitialized;
	ESMFFQsvdec::CONTEXT_T *	_context;
	AVPacket* _av_packet;
	AVCodecContext* _av_codec_ctx;
	AVCodec* _av_codec;
	AVBufferRef *				_qsv;
	AVFrame *					_yuv_frame;
	AVFrame *					_rgb_frame;
	AVFrame *					_uyvy_frame;
	
	int32_t						_uyvy_buffer_size;
	uint8_t*					_uyvy_buffer;
	int32_t						_rgb_buffer_size;
	uint8_t*					_rgb_buffer;

	SwsContext* _sws_yuv_ctx;
	SwsContext* _sws_rgb_ctx;

	std::vector<uint8_t*> _queue;

	BOOL _is_icon_loaded;

	//FILE* _fp_yuv;

	uint8_t* _logo;
	uint8_t* _alpha;



};
