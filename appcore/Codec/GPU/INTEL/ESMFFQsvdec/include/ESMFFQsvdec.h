﻿#pragma once

#ifdef _USRDLL
#if defined(EXPORT_ESM_FF_QSV_DECODER_LIB)
#  define EXP_ESM_FF_QSV_DECODER_CLASS __declspec(dllexport)
#else
#  define EXP_ESM_FF_QSV_DECODER_CLASS __declspec(dllimport)
#endif
#else
#define EXP_ESM_FF_QSV_DECODER_CLASS
#endif

// add static lib
//#ifdef EXP_ESM_FF_QSV_DECODER_CLASS
//#undef EXP_ESM_FF_QSV_DECODER_CLASS
//#endif

#include <ESMBase.h>
struct AVCodecParameters;
struct AVPacket;
struct AVFrame;
class EXP_ESM_FF_QSV_DECODER_CLASS ESMFFQsvdec : public ESMBase
{
public:
	class Core;
public:
	typedef struct EXP_ESM_FF_QSV_DECODER_CLASS _CONTEXT_T
	{
		int32_t	codec;
		int32_t	width;
		int32_t	height;
		uint8_t* extradata;
		int32_t	extradata_size;
		char logo[MAX_PATH];
		AVCodecParameters * codecpar;
		_CONTEXT_T(void);
		~_CONTEXT_T(void);
	} CONTEXT_T;

	typedef struct _STATUS_T
	{
		static const int SUCCESS = 0;
		static const int FAIL = 1;
	} STATUS_T;

	ESMFFQsvdec(void);
	~ESMFFQsvdec(void);

	BOOL	IsInitialized(void);

	int32_t	Initialize(ESMFFQsvdec::CONTEXT_T * ctx);
	int32_t	Release(void);

	int32_t	Decode(unsigned char* bitstream, int32_t bitstreamSize, unsigned char*** decoded, int32_t* numberOfDecoded, BOOL drawLogo);

private:
	ESMFFQsvdec::Core * _core;
};