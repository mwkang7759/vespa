﻿#include "ESMFFQsvdec.h"
#include "ESMFFQsvdecCore.h"

#if _DEBUG
#include<crtdbg.h>
#define new new(_NORMAL_BLOCK, __FILE__, __LINE__)
#define malloc(s) _malloc_dbg(s, _NORMAL_BLOCK, __FILE__, __LINE__)
#endif

ESMFFQsvdec::_CONTEXT_T::_CONTEXT_T()
	: codec(ESMFFQsvdec::VIDEO_CODEC_T::AVC)
	, width(1920)
	, height(1080)
	, extradata(nullptr)
	, extradata_size(0)
	, logo{}
{
	codecpar = avcodec_parameters_alloc();
}

ESMFFQsvdec::_CONTEXT_T::~_CONTEXT_T()
{
	avcodec_parameters_free(&codecpar);
}

ESMFFQsvdec::ESMFFQsvdec(void)
{
	_core = new ESMFFQsvdec::Core();
}

ESMFFQsvdec::~ESMFFQsvdec(void)
{
	if(_core)
		delete _core;
	_core = NULL;
}

BOOL ESMFFQsvdec::IsInitialized(void)
{
	return _core->IsInitialized();
}

int32_t	ESMFFQsvdec::Initialize(ESMFFQsvdec::CONTEXT_T * ctx)
{
	return _core->Initialize(ctx);
}

int32_t	ESMFFQsvdec::Release(void)
{
	return _core->Release();
}

int32_t	ESMFFQsvdec::Decode(unsigned char* bitstream, int32_t bitstreamSize, unsigned char*** decoded, int32_t* numberOfDecoded, BOOL drawLogo)
{
	return _core->Decode(bitstream, bitstreamSize, decoded, numberOfDecoded, drawLogo);
}