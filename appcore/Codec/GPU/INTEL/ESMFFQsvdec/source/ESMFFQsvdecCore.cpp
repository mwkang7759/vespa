﻿#include "ESMFFQsvdecCore.h"
#include <process.h>
#include "lodepng.h"
#include <Simd/SimdLib.h>

//#pragma comment(lib, "avcodec.lib")
//#pragma comment(lib, "avutil.lib")
//#pragma comment(lib, "swscale.lib")
#include "TraceAPI.h"

#if _DEBUG
#include<crtdbg.h>
#define new new(_NORMAL_BLOCK, __FILE__, __LINE__)
#define malloc(s) _malloc_dbg(s, _NORMAL_BLOCK, __FILE__, __LINE__)
#endif

static enum AVPixelFormat get_format(AVCodecContext* avctx, const enum AVPixelFormat* pix_fmts)
{
	while (*pix_fmts != AV_PIX_FMT_NONE) 
	{
		if (*pix_fmts == AV_PIX_FMT_QSV) 
		{
			AVBufferRef* hw_device_ref = (AVBufferRef*)avctx->opaque;
			AVHWFramesContext * frames_ctx;
			AVQSVFramesContext * frames_hwctx;
			int ret;
			
			/* create a pool of surfaces to be used by the decoder */
			avctx->hw_frames_ctx = av_hwframe_ctx_alloc(hw_device_ref);
			if (!avctx->hw_frames_ctx)
				return AV_PIX_FMT_NONE;
			frames_ctx = (AVHWFramesContext*)avctx->hw_frames_ctx->data;
			frames_hwctx = (AVQSVFramesContext*)frames_ctx->hwctx;
			frames_ctx->format = AV_PIX_FMT_QSV;
			frames_ctx->sw_format = avctx->sw_pix_fmt;
			frames_ctx->width = FFALIGN(avctx->coded_width, 32);
			frames_ctx->height = FFALIGN(avctx->coded_height, 32);
			frames_ctx->initial_pool_size = 32;
			frames_hwctx->frame_type = MFX_MEMTYPE_SYSTEM_MEMORY;
			ret = av_hwframe_ctx_init(avctx->hw_frames_ctx);
			if (ret < 0)
				return AV_PIX_FMT_NONE;
			return AV_PIX_FMT_QSV;
		}
		pix_fmts++;
	}
	return AV_PIX_FMT_NONE;
}

ESMFFQsvdec::Core::Core(void)
	: _context(nullptr)
	, _isInitialized(FALSE)
	, _uyvy_buffer_size(0)
	, _sws_yuv_ctx(nullptr)
	, _sws_rgb_ctx(nullptr)
	, _uyvy_buffer(nullptr)
	, _rgb_buffer(nullptr)
	, _logo(nullptr)
	, _alpha(nullptr)
	, _is_icon_loaded(FALSE)
{

}

ESMFFQsvdec::Core::~Core(void)
{

}

BOOL ESMFFQsvdec::Core::IsInitialized(void)
{
	return _isInitialized;
}

int32_t	ESMFFQsvdec::Core::Initialize(ESMFFQsvdec::CONTEXT_T * ctx)
{
	if(ctx==NULL)
		return ESMFFQsvdec::STATUS_T::FAIL;

	_context = ctx;

	LOG_I("[ESMFFQsv] Core::Initialize begin..");
	av_register_all();

	_is_icon_loaded = FALSE;
	if (strlen(_context->logo) > 0 && FileExists(_context->logo))
	{
		std::vector<unsigned char> png;
		std::vector<unsigned char> img;
		unsigned int width, height;
		lodepng::State state;
		unsigned error = lodepng::load_file(png, _context->logo);
		if (!error)
		{
			error = lodepng::decode(img, width, height, png);
			if (!error && _context->width == width && _context->height == height)
			{
				_logo = (uint8_t*)malloc(width * height * 3);
				_alpha = (uint8_t*)malloc(width * height);
				if (_logo != NULL && _alpha != NULL)
				{
					::memset(_logo, 0x00, width * height * 3);
					::memset(_alpha, 0x00, width * height);

					int logoIndex = 0;
					int alphaIndex = 0;
					for (int i = 0; i < img.size(); i++)
					{
						if ((i + 1) % 4 == 0)
						{
							_alpha[alphaIndex] = img.at(i);
							alphaIndex++;
						}
						else
						{
							_logo[logoIndex] = img.at(i);
							logoIndex++;
						}
					}
					_is_icon_loaded = TRUE;
				}
			}
		}
	}

	LOG_I("[ESMFFQsv] Core::Initialize av_hwdevice_ctx_create() before..");

	int ret = av_hwdevice_ctx_create(&_qsv, AV_HWDEVICE_TYPE_QSV, "sw", NULL, 0);
	if (ret < 0) {
		char errbuf[1024] = "";
		int errbuf_size = 1024;
		av_strerror(ret, errbuf, errbuf_size);
		LOG_I("[ESMFFQsv] av_hwdevice_ctx_create error..(%s)", errbuf);
		return ESMFFQsvdec::STATUS_T::FAIL;
	}
		
	
	switch (_context->codec)
	{
		case ESMFFQsvdec::VIDEO_CODEC_T::AVC:
		{
			_av_codec = avcodec_find_decoder_by_name("h264_qsv");
			break;
		}
		case ESMFFQsvdec::VIDEO_CODEC_T::HEVC:
		{
			_av_codec = avcodec_find_decoder_by_name("h265_qsv");
			break;
		}
	}

	if(_av_codec==NULL)
		return ESMFFQsvdec::STATUS_T::FAIL;
	
	_av_codec_ctx = avcodec_alloc_context3(_av_codec);
	if(!_av_codec_ctx)
		return ESMFFQsvdec::STATUS_T::FAIL;

	switch (_context->codec)
	{
		case ESMFFQsvdec::VIDEO_CODEC_T::AVC:
		{
			_av_codec_ctx->codec_id = AV_CODEC_ID_H264;
			break;
		}
		case ESMFFQsvdec::VIDEO_CODEC_T::HEVC:
		{
			_av_codec_ctx->codec_id = AV_CODEC_ID_H265;
			break;
		}
	}

	if (_context->extradata && (_context->extradata_size > 0))
	{
		_av_codec_ctx->extradata = static_cast<uint8_t*>(av_mallocz(_context->extradata_size + AV_INPUT_BUFFER_PADDING_SIZE));
		_av_codec_ctx->extradata_size = _context->extradata_size;
		memcpy(_av_codec_ctx->extradata, _context->extradata, _av_codec_ctx->extradata_size);
	}
	_av_codec_ctx->refcounted_frames = 1;
	_av_codec_ctx->opaque = _qsv;
	_av_codec_ctx->get_format = get_format;
	avcodec_parameters_to_context(_av_codec_ctx, _context->codecpar);

	ret = avcodec_open2(_av_codec_ctx, NULL, NULL);
	if (ret < 0)
	{
		Release();
		return ESMFFQsvdec::STATUS_T::FAIL;
	}

	_yuv_frame = av_frame_alloc();
	if (!_yuv_frame)
	{
		Release();
		return ESMFFQsvdec::ERR_CODE_T::GENERIC_FAIL;
	}

	_uyvy_frame = av_frame_alloc();
	if (!_uyvy_frame)
	{
		Release();
		return ESMFFQsvdec::ERR_CODE_T::GENERIC_FAIL;
	}

	_rgb_frame = av_frame_alloc();
	if (!_rgb_frame)
	{
		Release();
		return ESMFFQsvdec::ERR_CODE_T::GENERIC_FAIL;
	}

	_av_packet = av_packet_alloc();
	if (!_av_packet)
	{
		Release();
		return ESMFFQsvdec::ERR_CODE_T::GENERIC_FAIL;
	}

	_uyvy_buffer_size = av_image_get_buffer_size(AV_PIX_FMT_UYVY422, _context->width, _context->height, 1);
	_uyvy_buffer = (uint8_t*)av_malloc(_uyvy_buffer_size);
	av_image_fill_arrays(_uyvy_frame->data, _uyvy_frame->linesize, _uyvy_buffer, AV_PIX_FMT_UYVY422, _context->width, _context->height, 1);

	_rgb_buffer_size = av_image_get_buffer_size(AV_PIX_FMT_RGB24, _context->width, _context->height, 1);
	_rgb_buffer = (uint8_t*)av_malloc(_rgb_buffer_size);
	av_image_fill_arrays(_rgb_frame->data, _rgb_frame->linesize, _rgb_buffer, AV_PIX_FMT_RGB24, _context->width, _context->height, 1);

	_sws_yuv_ctx = sws_getContext(_context->width, _context->height, AV_PIX_FMT_RGB24, _context->width, _context->height, AV_PIX_FMT_UYVY422, SWS_FAST_BILINEAR, NULL, NULL, NULL);
	_sws_rgb_ctx = sws_getContext(_context->width, _context->height, AV_PIX_FMT_YUV420P, _context->width, _context->height, AV_PIX_FMT_RGB24, SWS_FAST_BILINEAR, NULL, NULL, NULL);
	_isInitialized = TRUE;

	return ESMFFQsvdec::STATUS_T::SUCCESS;
}

int32_t	ESMFFQsvdec::Core::Release(void)
{
	sws_freeContext(_sws_rgb_ctx);
	sws_freeContext(_sws_yuv_ctx);

	_uyvy_buffer_size = 0;
	_rgb_buffer_size = 0;

	if (_uyvy_buffer)
	{
		av_free(_uyvy_buffer);
		_uyvy_buffer = nullptr;
	}

	if (_rgb_buffer)
	{
		av_free(_rgb_buffer);
		_rgb_buffer = nullptr;
	}

	if (_av_packet)
	{
		av_packet_free(&_av_packet);
		_av_packet = nullptr;
	}

	if (_uyvy_frame)
	{
		av_frame_free(&_uyvy_frame);
		_uyvy_frame = nullptr;
	}

	if (_yuv_frame)
	{
		av_frame_free(&_yuv_frame);
		_yuv_frame = nullptr;
	}

	if (_av_codec_ctx)
	{
		avcodec_free_context(&_av_codec_ctx);
		_av_codec_ctx = nullptr;
	}

	if (_alpha)
	{
		free(_alpha);
		_alpha = nullptr;
	}

	if (_logo)
	{
		free(_logo);
		_logo = nullptr;
	}

	_isInitialized = FALSE;
	return ESMFFQsvdec::STATUS_T::SUCCESS;
}

int32_t	ESMFFQsvdec::Core::Decode(unsigned char* bitstream, int32_t bitstreamSize, unsigned char*** decoded, int32_t* numberOfDecoded, BOOL drawLogo)
{
	std::vector<uint8_t*>::iterator iter;
	for (iter = _queue.begin(); iter < _queue.end(); iter++)
		if (*(iter))
			free(*iter);
	_queue.clear();

	int32_t value = ESMFFQsvdec::ERR_CODE_T::GENERIC_FAIL;
	int32_t result = -1;

	if (bitstream && bitstreamSize > 0)
	{
		//av_packet_ref(_av_packet, _av_packet);
		_av_packet->data = reinterpret_cast<uint8_t*>(bitstream);
		_av_packet->size = bitstreamSize;

		if (!_av_packet->size && _av_packet->data)
			return AVERROR(EINVAL);
		if (!avcodec_is_open(_av_codec_ctx))
			return AVERROR(EINVAL);
		if (!av_codec_is_decoder(_av_codec))
			return AVERROR(EINVAL);
		result = AVERROR(EINVAL);
		result = avcodec_send_packet(_av_codec_ctx, _av_packet);
	}
	else
	{
		result = avcodec_send_packet(_av_codec_ctx, NULL);
	}

	if (result < 0)
	{
		char error[MAX_PATH] = { 0 };
		av_make_error_string(error, MAX_PATH, result);
		return ESMFFQsvdec::ERR_CODE_T::SUCCESS;
	}

	while (result >= 0)
	{
		result = avcodec_receive_frame(_av_codec_ctx, _yuv_frame);
		if (result == AVERROR(EAGAIN) || result == AVERROR_EOF)
			break;
		else if (result < 0)
			break;

		::memset(_uyvy_buffer, 0x00, _uyvy_buffer_size);

		int32_t y_width = _yuv_frame->width;
		int32_t y_height = _yuv_frame->height;
		int32_t uv_width = _yuv_frame->width >> 1;
		int32_t uv_height = _yuv_frame->height >> 1;

		int32_t src_y_stride = _yuv_frame->linesize[0];
		int32_t src_u_stride = _yuv_frame->linesize[1];
		int32_t src_v_stride = _yuv_frame->linesize[2];
		uint8_t* src_y_plane = _yuv_frame->data[0];
		uint8_t* src_u_plane = _yuv_frame->data[1];
		uint8_t* src_v_plane = _yuv_frame->data[2];

		_sws_rgb_ctx = sws_getCachedContext(_sws_rgb_ctx, _context->width, _context->height, AV_PIX_FMT_YUV420P, _context->width, _context->height, AV_PIX_FMT_RGB24, SWS_FAST_BILINEAR, NULL, NULL, NULL);
		sws_scale(_sws_rgb_ctx, _yuv_frame->data, _yuv_frame->linesize, 0, _context->height, _rgb_frame->data, _rgb_frame->linesize);

		/*if (_is_icon_loaded && drawLogo)
			SimdAlphaBlending(_logo, _context->width * 3, _context->width, _context->height, 3, _alpha, _context->width, _rgb_frame->data[0], _context->width * 3);*/

		_sws_yuv_ctx = sws_getCachedContext(_sws_yuv_ctx, _context->width, _context->height, AV_PIX_FMT_RGB24, _context->width, _context->height, AV_PIX_FMT_UYVY422, SWS_FAST_BILINEAR, NULL, NULL, NULL);
		sws_scale(_sws_yuv_ctx, _rgb_frame->data, _rgb_frame->linesize, 0, _context->height, _uyvy_frame->data, _uyvy_frame->linesize);

		//fwrite(_uyvy_frame->data[0], size_t(_context->width) * size_t(_context->height), 2, _fp_yuv);


		uint8_t* buffer = (uint8_t*)malloc(_uyvy_buffer_size);
		memmove(buffer, _uyvy_frame->data[0], _uyvy_buffer_size);
		_queue.push_back(buffer);

		break;
	}

	if (_queue.size() > 0)
	{
		*decoded = &_queue[0];
		*numberOfDecoded = _queue.size();
	}
	else
	{
		*numberOfDecoded = 0;
	}
	return ESMFFQsvdec::STATUS_T::SUCCESS;
}


BOOL ESMFFQsvdec::Core::FileExists(LPSTR szPath)
{
	DWORD dwAttrib = GetFileAttributesA(szPath);
	return (dwAttrib != INVALID_FILE_ATTRIBUTES &&
		!(dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
}