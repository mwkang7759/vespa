#ifndef _FD_H_
#define _FD_H_

#define FD_PACKAGE_VERSION "0.0.1"
#define FD_PACKAGE_LICENSE "4DReplay Commercial License"
#define FD_PACKAGE_NAME "4DReplay RAPA"
#define FD_PACKAGE_ORIGIN "http://www.4dreplay.com"

#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <memory>
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

namespace fd
{
    class base
    {
    public:
        static const int32_t MAX_PATH = 260;
        typedef struct _err_code_t
        {
            static const int32_t unknown = -1;
            static const int32_t success = 0;
            static const int32_t generic_fail = 1;
            static const int32_t memory_alloc = 2;
            static const int32_t invalid_argument = 3;
            static const int32_t not_implemented = 4;
        } err_code_t;

        typedef struct _media_type_t
        {
            static const int32_t unknown = 0x00;
            static const int32_t video = 0x01;
            static const int32_t audio = 0x02;
        } media_type_t;

        typedef struct _video_codec_t
        {
            static const int32_t unknown = -1;
            static const int32_t avc = 0;
            static const int32_t hevc = 1;
        } video_codec_t;

        typedef struct _avc_profile_t
        {
            static const int32_t bp = 0;
            static const int32_t hp = 1;
            static const int32_t mp = 2;
        } avc_profile_t;

        typedef struct _hevc_profile_t
        {
            static const int32_t dp = 0;
            static const int32_t mp = 1;
        } hevc_profile_t;        

        typedef struct _video_colorspace_t
        {
            static const int32_t unknown = -1;            
            static const int32_t nv12 = 0;
            static const int32_t yv12 = 1;
            static const int32_t bgra = 2;
        } video_colorspace_t;

        typedef struct _video_memory_t
        {
            static const int32_t unknown = -1;
            static const int32_t host = 0;
            static const int32_t cuda = 1;
            static const int32_t opencl = 2;
        } video_memory_t;

        typedef struct _audio_codec_t
        {
            static const int32_t unknown = -1;
            static const int32_t mp3 = 0;
            static const int32_t alaw = 1;
            static const int32_t mlaw = 2;
            static const int32_t aac = 3;
            static const int32_t ac3 = 4;
            static const int32_t opus = 5;
        } audio_codec_t;

        typedef struct _audio_sample_t
        {
            static const int32_t unknown = -1;
            static const int32_t fmt_u8 = 0;
            static const int32_t fmt_s16 = 1;
            static const int32_t fmt_s32 = 2;
            static const int32_t fmt_flt = 3;
            static const int32_t fmt_dbl = 4;
            static const int32_t fmt_s64 = 5;
            static const int32_t fmt_u8p = 6;
            static const int32_t fmt_s16p = 7;
            static const int32_t fmt_s32p = 8;
            static const int32_t fmt_fltp = 9;
            static const int32_t fmt_dblp = 10;
            static const int32_t fmt_s64p = 11;
        } audio_sample_t;        
    };
};

#endif