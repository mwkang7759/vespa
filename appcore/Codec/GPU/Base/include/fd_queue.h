#ifndef _FD_QUEUE_H_
#define _FD_QUEUE_H_

namespace fd
{
    template<class T>
    class queue
    {
    public:
        queue(void)
            : _buffer(nullptr)
            , _size(0)
            , _pending_count(0)
            , _available_index(0)
            , _pending_index(0)
        {}

        virtual ~queue(void)
        {}

        void initialize(T * pitems, int32_t size)
        {
            _size = size;
            _pending_count = 0;
            _available_index = 0;
            _pending_index = 0;
            _buffer = new T *[_size];
            for(int32_t i=0; i<_size; i++)
            {
                _buffer[i] = &pitems[i];
            }
        }

        void release(void)
        {
            if(_buffer)
            {
                delete [] _buffer;
                _buffer = nullptr;
            }
        }

        T* available(void)
        {
            T * pitem = nullptr;
            if(_pending_count==_size)
                return nullptr;
            
            pitem = _buffer[_available_index];
            _available_index = (_available_index + 1)%_size;
            _pending_count += 1;
            return pitem;
        }

        T* pending(void)
        {
            if(_pending_count==0)
                return nullptr;
            T * pitem = _buffer[_pending_index];
            _pending_index = (_pending_index +1 )%_size;
            _pending_count -= 1;
            return pitem;
        }
    
    protected:
        T**         _buffer;
        int32_t     _size;
        int32_t     _pending_count;
        int32_t     _available_index;
        int32_t     _pending_index;
    };
};


#endif