

#include "SystemAPI.h"
#include "TraceAPI.h"

#include <sys/types.h>
#include <sys/stat.h>


static FILE* g_fp = NULL;

#ifdef USE_TLOG
static TLogInfo g_tLog;

void SetTraceLevel(tLOG_LEVEL tLogLevel)
{
	g_tLog.logLevel = 0;

	if (tLogLevel.bLOG_CRI)
		g_tLog.logLevel |= e_LOG_CRI;

	if (tLogLevel.bLOG_MAJ)
		g_tLog.logLevel |= e_LOG_MAJ;

	if (tLogLevel.bLOG_MIN)
		g_tLog.logLevel |= e_LOG_MIN;

	if (tLogLevel.bLOG_INF)
		g_tLog.logLevel |= e_LOG_INF;

	if (tLogLevel.bLOG_DB0)
		g_tLog.logLevel |= e_LOG_DB0;

	if (tLogLevel.bLOG_DB1)
		g_tLog.logLevel |= e_LOG_DB1;

	if (tLogLevel.bLOG_DB2)
		g_tLog.logLevel |= e_LOG_DB2;

	if (tLogLevel.bLOG_DB3)
		g_tLog.logLevel |= e_LOG_DB3;

	if (tLogLevel.bLOG_ERR)
		g_tLog.logLevel |= e_LOG_ERR;
}
#endif

BOOL SetTraceFileNameEx(char* lpszFileName, INT32 nMode)
{
#ifdef	__PRINT_TRACE__
	if(g_fp)
	{
		fclose(g_fp);
		g_fp = NULL;
	}

	if(lpszFileName)
	{
		g_fp = fopen(lpszFileName, "wt");
		if(g_fp == NULL)
			return FALSE;
	}
#endif // __PRINT_TRACE__
	return TRUE;
}

#if ( (!defined(USE_TLOG)) && (!defined(SUPPORT_IOS)) )
BOOL SetTraceLogLevel(INT32 nLogLevel)
{
	if (gLogHandle)
	{
		switch(nLogLevel)
		{
		case FDR_LOG_LEVEL_FATAL:
			FrLogSetLevel(gLogHandle, e_LOG_FATAL);
			break;

		case FDR_LOG_LEVEL_ERROR:
			FrLogSetLevel(gLogHandle, e_LOG_ERROR);
			break;

		case FDR_LOG_LEVEL_WARN:
			FrLogSetLevel(gLogHandle, e_LOG_WARN);
			break;

		case FDR_LOG_LEVEL_INFO:
			FrLogSetLevel(gLogHandle, e_LOG_INFO);
			break;

		case FDR_LOG_LEVEL_DEBUG:
			FrLogSetLevel(gLogHandle, e_LOG_DEBUG);
			break;

		case FDR_LOG_LEVEL_TRACE:
			FrLogSetLevel(gLogHandle, e_LOG_TRACE);
			break;

		default :
			FrLogSetLevel(gLogHandle, e_LOG_TRACE);
		}

		TRACEX(FDR_LOG_INFO, "[Trace] SetTraceLogLevel (%d)", nLogLevel);
	}
	else
		TRACEX(FDR_LOG_WARN, "[Trace] gLogHandle is null");

	return TRUE;
}

BOOL SetTraceRollingFileName(char* lpszFileName, int nMaxFileSize, int nIndexCnt)
{
	LRSLT	lRet = FR_OK;
	char	szLogUnitName[MAX_PATH] = "";

	if (gLogHandle)
	{
		FrLogClose(gLogHandle);
		gLogHandle = NULL;
	}

	if (lpszFileName == NULL)
		return TRUE;

	sprintf(szLogUnitName, "DTB.log");
	if (lpszFileName)
		lRet = FrLogRollingFileOpen(&gLogHandle, szLogUnitName, lpszFileName, NULL, nMaxFileSize, nIndexCnt);
	else
	{
		FrLogClose(gLogHandle);
		gLogHandle = NULL;
		return TRUE;
	}

	if (FRSUCCEEDED(lRet))
	{
		FrLogSetLevel(gLogHandle, e_LOG_TRACE);
		return TRUE;
	}
	else
	{
		return FALSE;
	}

#ifdef USE_TLOG
	INT32 nRet;

	if (lpszFileName)
	{
		strcpy(g_tLog.filePath, lpszFileName);
		g_tLog.logLevel = 0;
		g_tLog.logLevel |= e_LOG_CRI;
		g_tLog.logLevel |= e_LOG_MAJ;
		g_tLog.logLevel |= e_LOG_MIN;
		g_tLog.logLevel |= e_LOG_INF;
		g_tLog.logLevel |= e_LOG_ERR;
		g_tLog.logLevel |= e_LOG_DB0;
		g_tLog.logLevel |= e_LOG_DB1;
		g_tLog.logLevel |= e_LOG_DB2;
		g_tLog.logLevel |= e_LOG_DB3;

		FrDeleteMutex(&g_tLog.m_xSession);
		FrCreateMutex(&g_tLog.m_xSession);

		FrDeleteMutex(&g_tLog.m_xFwrite);
		FrCreateMutex(&g_tLog.m_xFwrite);
	}
	else
		g_tLog.filePath[0] = '\0';

	nRet = tLogInit(&g_tLog);

	if (nRet == -1)
	{
		FrDeleteMutex(&g_tLog.m_xSession);
		FrDeleteMutex(&g_tLog.m_xFwrite);
	}
	else
		RenameTraceFile(g_tLog.filePath);

	return TRUE;
#endif

#ifdef	__PRINT_TRACE__
	if(g_fp)
	{
		fclose(g_fp);
		g_fp = NULL;
	}

	if(lpszFileName)
	{
		g_fp = fopen(lpszFileName, "at");
		if(g_fp == NULL)
			return FALSE;
	}
#endif // __PRINT_TRACE__
	return TRUE;

}

BOOL FrSetTraceFileName(char* lpszFileName)
{
	LRSLT	lRet = FR_OK;
	char	szLogUnitName[MAX_PATH] = "";

	if (gLogHandle)
	{
		FrLogClose(gLogHandle);
		gLogHandle = NULL;
	}

	if (lpszFileName == NULL)
		return TRUE;

//#ifdef _DEBUG
//	lRet = DTBLogConsoleOpen(&gLogHandle, "DTB.log", NULL);
//#else

	//sprintf(szLogUnitName, "DTB_%d.log", FrGetTickCount());
	sprintf(szLogUnitName, "DTB.log");
	if (lpszFileName)
		lRet = FrLogFileOpen(&gLogHandle, szLogUnitName, lpszFileName, NULL, TRUE);
	else
	{
		FrLogClose(gLogHandle);
		gLogHandle = NULL;
		return TRUE;
	}
//#endif

	if (FRSUCCEEDED(lRet))
	{
		FrLogSetLevel(gLogHandle, e_LOG_TRACE);
		return TRUE;
	}
	else
	{
		return FALSE;
	}

#ifdef USE_TLOG
	INT32 nRet;

	if (lpszFileName)
	{
		strcpy(g_tLog.filePath, lpszFileName);
		g_tLog.logLevel = 0;
		g_tLog.logLevel |= e_LOG_CRI;
		g_tLog.logLevel |= e_LOG_MAJ;
		g_tLog.logLevel |= e_LOG_MIN;
		g_tLog.logLevel |= e_LOG_INF;
		g_tLog.logLevel |= e_LOG_ERR;
		g_tLog.logLevel |= e_LOG_DB0;
		g_tLog.logLevel |= e_LOG_DB1;
		g_tLog.logLevel |= e_LOG_DB2;
		g_tLog.logLevel |= e_LOG_DB3;

		FrDeleteMutex(&g_tLog.m_xSession);
		FrCreateMutex(&g_tLog.m_xSession);

		FrDeleteMutex(&g_tLog.m_xFwrite);
		FrCreateMutex(&g_tLog.m_xFwrite);
	}
	else
		g_tLog.filePath[0] = '\0';

    nRet = tLogInit(&g_tLog);

	if (nRet == -1)
	{
		FrDeleteMutex(&g_tLog.m_xSession);
		FrDeleteMutex(&g_tLog.m_xFwrite);
	}
	else
		RenameTraceFile(g_tLog.filePath);

	return TRUE;
#endif

#ifdef	__PRINT_TRACE__
	if(g_fp)
	{
		fclose(g_fp);
		g_fp = NULL;
	}

	if(lpszFileName)
	{
		g_fp = fopen(lpszFileName, "at");
		if(g_fp == NULL)
			return FALSE;
	}
#endif // __PRINT_TRACE__
	return TRUE;
}

BOOL SetTraceFileName2(char* lpszFileName)
#else
BOOL SetTraceFileName(char* lpszFileName)
#endif
{
#ifdef	__PRINT_TRACE__
	if(g_fp)
	{
		fclose(g_fp);
		g_fp = NULL;
	}

	if(lpszFileName)
	{
		g_fp = fopen(lpszFileName, "wt");
		if(g_fp == NULL)
			return FALSE;
	}
#endif // __PRINT_TRACE__
	return TRUE;
}

BOOL ReleaseTraceFileName()
{
#if ( (!defined(USE_TLOG)) && (!defined(SUPPORT_IOS)) )
	if (gLogHandle)
	{
		FrLogClose(gLogHandle);
		gLogHandle = NULL;
	}
#endif
	return TRUE;
}

#ifdef USE_TLOG
void tLogPrintf(char *lpszFormat, ...)
{
#ifdef	WIN32
	char str[MAX_BUF_LENG];
	va_list argList;

	if (g_tLog.fp)
	{
		va_start(argList, lpszFormat);
		vsprintf( str, lpszFormat, argList );

		tLogPrn(NULL, 0, e_LOG_DB3, &g_tLog, (char *)str);

		va_end(argList);
	}
#else
	char str[MAX_BUF_LENG];
	va_list argList;
	va_start(argList, lpszFormat);
	vsprintf( str, lpszFormat, argList );

	fputs(str,stderr);
	fflush(stderr);
	va_end(argList);
#endif
}

#endif // USE_TLOG

void DebugPrintf(char* lpszFormat, ...)
{
#ifdef	WIN32
	char str[MAX_BUF_LENG];
	va_list argList;
	va_start(argList, lpszFormat);
	vsprintf( str, lpszFormat, argList );

	OutputDebugString(str);
	va_end(argList);
#else
	char str[MAX_BUF_LENG];
	va_list argList;
	va_start(argList, lpszFormat);
	vsprintf( str, lpszFormat, argList );

	fputs(str,stderr);
	fflush(stderr);
	va_end(argList);
#endif
}

void DebugPrintfEx(char* file_name, INT32 file_line, INT32 log_level, char* lpszFormat, ...)
{
#ifdef	WIN32
	char str[MAX_BUF_LENG];
	va_list argList;
	va_start(argList, lpszFormat);
	vsprintf( str, lpszFormat, argList );

	OutputDebugString(str);
	va_end(argList);
#else
	char str[MAX_BUF_LENG];
	va_list argList;
	va_start(argList, lpszFormat);
	vsprintf( str, lpszFormat, argList );

	fputs(str,stderr);
	fflush(stderr);
	va_end(argList);
#endif
}

void DebugFprintf(char* lpszFormat, ...)
{
#ifdef	WIN32
	SYSTEMTIME	Time;
	char str[MAX_BUF_LENG];
	va_list argList;

	va_start(argList, lpszFormat);
	GetLocalTime(&Time);
	sprintf(str, "[%4.4d.%2.2d.%2.2d][%2.2d:%2.2d:%2.2d.%3.3d]", Time.wYear, Time.wMonth, Time.wDay,
			Time.wHour, Time.wMinute, Time.wSecond, Time.wMilliseconds);
	if (strlen(lpszFormat) > 4096)
		vsprintf( str+strlen(str), "Debug message is very loog!!!", argList );
	else
		vsprintf( str+strlen(str), lpszFormat, argList );

	if (g_fp)
	{
		fputs(str, g_fp);
		fflush(g_fp);
	}

	va_end(argList);
#else
	//char strFile[MAX_BUF_LENG];
	char str[MAX_BUF_LENG];
	time_t rawtime;
	struct tm * timeinfo;

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	sprintf(str, "[%4.4d.%2.2d.%2.2d][%2.2d:%2.2d:%2.2d]", 	 timeinfo->tm_year + 1900,
        timeinfo->tm_mon + 1,
        timeinfo->tm_mday,
        timeinfo->tm_hour,
        timeinfo->tm_min,
        timeinfo->tm_sec);

	va_list argList;
	va_start(argList, lpszFormat);

	if (strlen(lpszFormat) > MAX_BUF_LENG)
		vsprintf( str+strlen(str), "Debug message is very loog!!!", argList );
	else
		vsprintf( str+strlen(str), lpszFormat, argList );

	/*if (!g_fp) {
		sprintf(strFile, "TRACE_%u.txt", (INT32)FrGetProcessNum() );
		g_fp = fopen(strFile, "wt");
	}*/

	if (g_fp)
	{
		fputs(str, g_fp);
		fflush(g_fp);
	}

	va_end(argList);
#endif
}

void DebugFprintfEx(char* file_name, INT32 file_line, INT32 log_level, char* lpszFormat, ...)
{
	#ifdef	WIN32
	SYSTEMTIME	Time;
	char str[MAX_BUF_LENG];
	va_list argList;

	va_start(argList, lpszFormat);
	GetLocalTime(&Time);
	sprintf(str, "[%4.4d.%2.2d.%2.2d][%2.2d:%2.2d:%2.2d.%3.3d]", Time.wYear, Time.wMonth, Time.wDay,
			Time.wHour, Time.wMinute, Time.wSecond, Time.wMilliseconds);
	if (strlen(lpszFormat) > 4096)
		vsprintf( str+strlen(str), "Debug message is very loog!!!", argList );
	else
		vsprintf( str+strlen(str), lpszFormat, argList );

	if (!g_fp)
#ifdef	_WIN32_WCE
		g_fp = fopen("TRACE.txt", "wt");
#else
		g_fp = fopen("c:\\TRACE.txt", "wt");
#endif
	fputs(str, g_fp);
	fflush(g_fp);

	va_end(argList);
#else
	char strFile[MAX_BUF_LENG];
	char str[MAX_BUF_LENG];
	time_t rawtime;
	struct tm * timeinfo;

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	sprintf(str, "[%4.4d.%2.2d.%2.2d][%2.2d:%2.2d:%2.2d]", 	 timeinfo->tm_year + 1900,
        timeinfo->tm_mon + 1,
        timeinfo->tm_mday,
        timeinfo->tm_hour,
        timeinfo->tm_min,
        timeinfo->tm_sec);

	va_list argList;
	va_start(argList, lpszFormat);

	if (strlen(lpszFormat) > MAX_BUF_LENG)
		vsprintf( str+strlen(str), "Debug message is very loog!!!", argList );
	else
		vsprintf( str+strlen(str), lpszFormat, argList );

	if (!g_fp) {
		//sprintf(strFile, "TRACE_%u.txt", (INT32)McGetProcessNum() );
		sprintf(strFile, "TRACE_%u.txt", (INT32)111 );
		g_fp = fopen(strFile, "wt");
	}
	fputs(str, g_fp);
	fflush(g_fp);

	va_end(argList);
#endif
}

#ifdef USE_TLOG
BOOL RenameTraceFile(char *lpszFileName)
{
#if defined(WIN32) && !defined(_WIN32_WCE)
	struct stat file_info;
	struct tm *logfiletime, *curtime;
	time_t long_time;
	DWORD32	dwLogFileTime;
	DWORD32	dwCurTime;
	char	szNewFileName[MAX_PATH];
	BOOL	bRet;
	INT32		nRet;

	nRet = stat(lpszFileName, &file_info) ;
	if (nRet != -1)
    {
		char drive[_MAX_DRIVE];
		char dir[_MAX_DIR];
		char fname[_MAX_FNAME];
		char ext[_MAX_EXT];
		char szTemp[MAX_PATH];

		logfiletime = localtime( &file_info.st_mtime );	// Based on Modify Time
		//logfiletime = localtime( &file_info.st_ctime ); // Based on Creation Time

		dwLogFileTime = (logfiletime->tm_year + 1900) * 10000 + (logfiletime->tm_mon + 1) * 100 + logfiletime->tm_mday;

		time( &long_time );
		curtime = localtime( &long_time );
		dwCurTime = (curtime->tm_year + 1900) * 10000 + (curtime->tm_mon + 1) * 100 + curtime->tm_mday;

		if (dwCurTime != dwLogFileTime)
		{
			_splitpath( lpszFileName, drive, dir, fname, ext );
			sprintf(szNewFileName, "%s%s%s_%d.txt", drive, dir, fname, dwLogFileTime);

			//TRACE("Since passed a day, these logs is going to be moved to '%s' file.\r\n", szNewFileName);
			sprintf(szTemp, "Since passed a day, these logs are going to be moved to '%s' file.\r\n", szNewFileName);
			tLogPrn(NULL, 0, e_LOG_DB3, &g_tLog, (char *)szTemp);

			if (g_tLog.fp)
			{
				fclose(g_tLog.fp);
				g_tLog.fp = NULL;
			}

			nRet = MoveFileExA  ((LPCTSTR)lpszFileName, (LPCTSTR)szNewFileName, MOVEFILE_COPY_ALLOWED | MOVEFILE_WRITE_THROUGH);

			if (nRet == 0)
			{
				bRet = CopyFileA((LPCTSTR)lpszFileName, (LPCTSTR)szNewFileName, FALSE);
				if (bRet)
					DeleteFileA((LPCTSTR)lpszFileName);
			}
#if 0
			SetTraceFileName(lpszFileName);
#else
			if(!(g_tLog.fp = fopen(g_tLog.filePath, "r")))
			{
				if(!(g_tLog.fp = fopen(g_tLog.filePath, "w+"))) {
#ifndef WIN32
					fprintf(stdout, "[%s] file create error: %s\n", __func__, _error);
#endif
					g_tLog.fp = NULL;
					return FALSE;
				}
			}
			else
			{
				fclose(g_tLog.fp);
				g_tLog.fp = NULL;
				if(!(g_tLog.fp = fopen(g_tLog.filePath, "a"))) {
#ifndef WIN32
					fprintf(stdout, "[%s] file open error: %s\n", __func__, _error);
#endif
					g_tLog.fp = NULL;
					return FALSE;
				}
			}
#endif
			//TRACE("Since passed a day, you can check the old trace log through '%s' file.\r\n", szNewFileName);
			sprintf(szTemp, "Since passed a day, you can check the old trace log through '%s' file.\r\n", szNewFileName);
			tLogPrn(NULL, 0, e_LOG_DB3, &g_tLog, (char *)szTemp);
		}
    }
#endif
	return TRUE;
}

char* tLogNameEx(int nLogLevel)
{
   static char szLogName[8] = {0, };

   switch (nLogLevel)
   {
       case e_LOG_FATAL:
           strcpy(szLogName, "FATAL");
           break;
       case e_LOG_ASSERT:
           strcpy(szLogName, "ASSERT");
           break;
       case e_LOG_WARN:
           strcpy(szLogName, "WARN");
           break;
       case e_LOG_INFO:
           strcpy(szLogName, "INFO");
           break;
       case e_LOG_DEBUG:
           strcpy(szLogName, "DEBUG");
           break;
       case e_LOG_TRACE:
           strcpy(szLogName, "TRACE");
           break;
   }

   return szLogName;
}

char* timeToString(struct tm *t) {
   static char s[32];
   sprintf(s, "%04d-%02d-%02d %02d:%02d:%02d",
           t->tm_year + 1900, t->tm_mon + 1, t->tm_mday,
           t->tm_hour, t->tm_min, t->tm_sec
           );
   return s;
}
char* timeToStringEx()
{
   static char s[32];
   struct timeval val;
   struct tm *ptm;
   gettimeofday(&val, NULL);
   ptm = localtime(&val.tv_sec);
   // format : YYMMDDhhmmssuuu
   sprintf(s, "%04d-%02d-%02d %02d:%02d:%02d.%03d"
           , ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
           , ptm->tm_hour, ptm->tm_min, ptm->tm_sec
           , val.tv_usec/1000);
   return s;
}



void tPrintf(char* file_name, int file_line, int log_level, char* lpszFormat, ...)
{
    char *pstDate;
    char *pstLogName;


#if defined(_DEBUG)

#ifdef	WIN32
	char str[MAX_BUF_LENG];
	va_list argList;
	va_start(argList, lpszFormat);
	vsprintf( str, lpszFormat, argList );

	OutputDebugString(str);
	OutputDebugString("\r\n");
	va_end(argList);
#else
	char str[MAX_BUF_LENG];
	va_list argList;
	va_start(argList, lpszFormat);
	vsprintf( str, lpszFormat, argList );

    pstDate = timeToStringEx();
    pstLogName = tLogNameEx(log_level);

    fprintf(stdout, "%s (%s:%d)(%s) %s\n", pstDate, file_name, file_line, pstLogName, str);

    //fputs(pstDate, stdout);
    //fputs(pstLogName, stdout);

    //fputs(str, stdout);
	//fputs("\r\n", stdout);

	fflush(stdout);
	va_end(argList);
#endif

#endif // _DEBUG
}

#endif
