

#include "SystemAPI.h"
#include "TraceAPI.h"

void* FILEDUMPOPEN(char* pFilename, char* pExt, DWORD32 dwHandle)
{
	char		szFilename[MAX_STRING_LENGTH];
	FILE_HANDLE	hFile = NULL;

	sprintf(szFilename, "%s_%u.%s", pFilename, dwHandle, pExt);
	//hFile = McOpenLocalFile(szFilename, FILE_CREATE|FILE_WRITE);

	return	hFile;
}

void FPRINTF(void* hFile, char* lpszFormat, ...)
{
	char str[MAX_BUF_LENG];
	va_list argList;
	va_start(argList, lpszFormat);
	vsprintf(str, lpszFormat, argList);
	
	FrWriteFile((FILE_HANDLE)hFile, str, (UINT32)strlen(str));
	va_end(argList);
}

void FILEDUMP(void* hFile, BYTE* pData, DWORD32 dwSize)
{
	DWORD32		i;

	FPRINTF(hFile, "		");
	for (i = 0;i < dwSize;i++) {
		FPRINTF(hFile, "%2.2x ", pData[i]);
		if (!((i+1) % 16))
			FPRINTF(hFile, "\n		");
	}
	FPRINTF(hFile, "\n");
}
