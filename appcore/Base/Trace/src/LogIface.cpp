
#include "SystemAPI.h"
#include "LogIface.h"

#include <sys/types.h>
#include <sys/stat.h>
#ifdef WIN32
#include <conio.h>
#endif

#if defined(SUPPORT_ANDROID)

#if defined(_SUPPORT_SPDLOG_)
#include "spdlog/sinks/android_sink.h"
#else
#include <android/log.h>
#endif

#elif defined(_SUPPORT_SPDLOG_)
#include <iostream>
#include <cstdio>
#include "spdlog/spdlog.h"
#include "spdlog/cfg/env.h" // for loading levels from the environment variable
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/sinks/basic_file_sink.h"
#include "spdlog/sinks/rotating_file_sink.h"
#include "spdlog/sinks/daily_file_sink.h"
#include "spdlog/fmt/bin_to_hex.h"
#include "spdlog/pattern_formatter.h"
#else
#include <log4cxx/basicconfigurator.h>
#include <log4cxx/propertyconfigurator.h>
#include <log4cxx/xml/domconfigurator.h>

#include <log4cxx/patternlayout.h>
#include <log4cxx/logmanager.h>
#include <log4cxx/consoleappender.h>
#include <log4cxx/fileappender.h>
#include <log4cxx/rollingfileappender.h>
#include <log4cxx/dailyrollingfileappender.h>

#include <log4cxx/helpers/transcoder.h>
#include <log4cxx/stream.h>
#endif /// for #if defined(SUPPORT_ANDROID)

#if _MSC_VER >= 1900 // Visual Studio 2015 (vc140)

#if defined(WIN32) && !defined(L4CLIB_STATIC)

#endif


#elif _MSC_VER >= 1600 // Visual Studio 2010 (vc100)

#if defined(WIN32)
# if defined(X64)
#  if defined(_DEBUG)
#   pragma comment(lib, "log4cxxDX64_vc100.lib")
#  else
#   pragma comment(lib, "log4cxxX64_vc100.lib")
#  endif
# else
#  if defined(_DEBUG)
#    pragma comment(lib, "log4cxxD_vc100.lib")
#  else
#   pragma comment(lib, "log4cxx_vc100.lib")
#  endif
# endif
#endif

#else

#if defined(WIN32)
# if defined(X64)
#  if defined(_DEBUG)
#   pragma comment(lib, "log4cxxDX64.lib")
#  else
#   pragma comment(lib, "log4cxxX64.lib")
#  endif
# else
#  if defined(_DEBUG)
#    pragma comment(lib, "log4cxxD.lib")
#  else
#   pragma comment(lib, "log4cxx.lib")
#  endif
# endif
#endif

#endif

#if !defined(SUPPORT_ANDROID) && !defined(IOS) && !defined(_SUPPORT_SPDLOG_)
/**
Set the location information

@param filename the file name where the logging request was issued
@param linenum the line number from where the logging request was issued
*/
#define DTB_LOG4CXX_LOCATION(filename, linenum) ::log4cxx::spi::LocationInfo(filename, \
           __LOG4CXX_FUNC__,                                                         \
           linenum)

/**
Logs a message to a specified logger with the DEBUG level.

@param logger the logger to be used.
@param filename the file name where the logging request was issued
@param linenum the line number from where the logging request was issued
@param message the message string to log.
*/
#define DTB_LOG4CXX_DEBUG(logger, filename, linenum, message) { \
        if (logger->isDebugEnabled()) {\
           ::log4cxx::helpers::MessageBuffer oss_; \
           logger->forcedLog(::log4cxx::Level::getDebug(), oss_.str(oss_ << message), DTB_LOG4CXX_LOCATION(filename, linenum)); }}

/**
Logs a message to a specified logger with the TRACE level.

@param logger the logger to be used.
@param filename the file name where the logging request was issued
@param linenum the line number from where the logging request was issued
@param message the message string to log.
*/
#define DTB_LOG4CXX_TRACE(logger, filename, linenum, message) { \
        if (logger->isTraceEnabled()) {\
           ::log4cxx::helpers::MessageBuffer oss_; \
           logger->forcedLog(::log4cxx::Level::getTrace(), oss_.str(oss_ << message), DTB_LOG4CXX_LOCATION(filename, linenum)); }}


/**
Logs a message to a specified logger with the INFO level.

@param logger the logger to be used.
@param filename the file name where the logging request was issued
@param linenum the line number from where the logging request was issued
@param message the message string to log.
*/
#define DTB_LOG4CXX_INFO(logger, filename, linenum, message) { \
        if (logger->isInfoEnabled()) {\
           ::log4cxx::helpers::MessageBuffer oss_; \
           logger->forcedLog(::log4cxx::Level::getInfo(), oss_.str(oss_ << message), DTB_LOG4CXX_LOCATION(filename, linenum)); }}

/**
Logs a message to a specified logger with the WARN level.

@param logger the logger to be used.
@param filename the file name where the logging request was issued
@param linenum the line number from where the logging request was issued
@param message the message string to log.
*/
#define DTB_LOG4CXX_WARN(logger, filename, linenum, message) { \
        if (logger->isWarnEnabled()) {\
           ::log4cxx::helpers::MessageBuffer oss_; \
           logger->forcedLog(::log4cxx::Level::getWarn(), oss_.str(oss_ << message), DTB_LOG4CXX_LOCATION(filename, linenum)); }}

/**
Logs a message to a specified logger with the ERROR level.

@param logger the logger to be used.
@param filename the file name where the logging request was issued
@param linenum the line number from where the logging request was issued
@param message the message string to log.
*/
#define DTB_LOG4CXX_ERROR(logger, filename, linenum, message) { \
        if (logger->isErrorEnabled()) {\
           ::log4cxx::helpers::MessageBuffer oss_; \
           logger->forcedLog(::log4cxx::Level::getError(), oss_.str(oss_ << message), DTB_LOG4CXX_LOCATION(filename, linenum)); }}

/**
Logs a error if the condition is not true.

@param logger the logger to be used.
@param filename the file name where the logging request was issued
@param linenum the line number from where the logging request was issued
@param condition condition
@param message the message string to log.
*/
#define DTB_LOG4CXX_ASSERT(logger, filename, linenum, condition, message) { \
        if (!(condition) && logger->isErrorEnabled()) {\
           ::log4cxx::helpers::MessageBuffer oss_; \
           logger->forcedLog(::log4cxx::Level::getError(), oss_.str(oss_ << message), DTB_LOG4CXX_LOCATION(filename, linenum)); }}


/**
Logs a message to a specified logger with the FATAL level.

@param logger the logger to be used.
@param filename the file name where the logging request was issued
@param linenum the line number from where the logging request was issued
@param message the message string to log.
*/
#define DTB_LOG4CXX_FATAL(logger, filename, linenum, message) { \
        if (logger->isFatalEnabled()) {\
           ::log4cxx::helpers::MessageBuffer oss_; \
           logger->forcedLog(::log4cxx::Level::getFatal(), oss_.str(oss_ << message), DTB_LOG4CXX_LOCATION(filename, linenum)); }}


/**
@brief Structure for making the handler of logger class
*/
typedef struct _DTB_LoggerPtr_ {
	log4cxx::LoggerPtr logger;			///<log4cxx::LoggerPtr class
	INT32 log_level;
} DTB_LoggerPtr;


#ifdef _WIN32

///const std::string strPattern = "[%d] %-5p %l %M (%12.12c) %m%n";
const std::string strPattern = "[%d{yyyy.MM.dd HH:mm:ss,SSS}][%p] %m%n";  ///< Basic log pattern

#else

///const std::string strPattern = "[%d] %-5p %l %M (%12.12c) %m%n";
//const std::string strPattern = "[%d{yyyy.MM.dd HH:mm:ss,SSS}](%-14F,%4L)[%p] %m%n";  ///< Basic log pattern
//const std::string strPattern = "[%d{yyyy.MM.dd HH:mm:ss,SSS}](%-14F,%4L) %m%n";  ///< Basic log pattern
#endif

#elif defined(SUPPORT_ANDROID) || defined(_SUPPORT_SPDLOG_)
/**
@brief Structure for making the handler of logger class
*/

char* logLevel2str(INT32 level)
{
	switch(level)
	{
        case e_LOG_FATAL: return (char*)"[FATAL]";
    	case e_LOG_ASSERT: return (char*)"[ASSERT]";
    	case e_LOG_ERROR: return (char*)"[ERROR]";
    	case e_LOG_WARN: return (char*)"[WARN]";
    	case e_LOG_INFO: return (char*)"[INFO]";
    	case e_LOG_DEBUG: return (char*)"[DEBUG]";
    	case e_LOG_TRACE: return (char*)"[TRACE]";
    	default:
    		return (char*)"[???]";
	}

	return NULL;
}



#if defined(SUPPORT_ANDROID)
typedef struct _DTB_LoggerPtr_ {
	INT32 log_level;			///<log4cxx::LoggerPtr class
} DTB_LoggerPtr;

#define  LOG_TAG    "dreamtobe"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)

char* get_datestr(char* strDate)
{
    time_t*         secs;
    struct timeval  tval;
    struct tm       tmval;

    gettimeofday(&tval, NULL);
    secs = &tval.tv_sec;
    localtime_r(secs, &tmval);

    memset(strDate, 0x00, 9);
    sprintf(strDate, "%04d%02d%02d",
                  tmval.tm_year+1900, tmval.tm_mon+1, tmval.tm_mday);
    return strDate;
}


char* get_timestr(char* strTime)
{
    time_t*         secs;
    struct timeval  tval;
    struct tm       tmval;

    gettimeofday(&tval, NULL);
    secs = &tval.tv_sec;
    localtime_r(secs, &tmval);

    memset(strTime, 0x00, 11);
    sprintf(strTime, "%02d%02d%02d.%03d",
                     tmval.tm_hour, tmval.tm_min, tmval.tm_sec, (int)tval.tv_usec/1000);
    return strTime;
}


#elif defined(_SUPPORT_SPDLOG_)
//spdlog::set_pattern("[%Y-%m-%d %X.%e] [PID:%P] [thread %t] [%^%l%$] [%s:%#] - %v");
const std::string strPattern = "[%Y-%m-%d %H:%M:%S,%e] [thread %t] %v";
//const std::string strPattern = "[%Y-%m-%d %X.%e] [thread %t] [%s:%#] - %v";
//const std::string strPattern = "[%Y-%m-%d %X.%e] [%^%l%$] [%s:%#] - %v";
typedef struct _DTB_LoggerPtr_ {
	INT32 log_level;
    std::shared_ptr<spdlog::logger> logger;
    char unitName[1024];
} DTB_LoggerPtr;
#endif

#endif //// #if !defined(SUPPORT_ANDROID) && !defined(IOS)

/**
@remarks global variable for log handle
*/
FrLogHandle                gLogHandle  = NULL;

/**
@brief Open the console logger
@remarks ������� console�� ����ϴ� logger handler�� �����Ѵ�.
@param phHandle logger handler
@param szUnitName logger �ĺ��� (�� �α� ���� Unique)
@param szPattern Pattern string (���� NULL�̸� default pattern ���)
@return ó�� ��� �ڵ�
*/
LOG4RESULT FrLogConsoleOpen(FrLogHandle* phHandle, char *szUnitName, char *szPattern)
{
#if !defined(SUPPORT_ANDROID) && !defined(IOS) && !defined(_SUPPORT_SPDLOG_)
	DTB_LoggerPtr *pLogger = (DTB_LoggerPtr *)MALLOCZ(sizeof(DTB_LoggerPtr));

	if (pLogger == NULL) {
		return FR_FAIL;
	}

	log4cxx::Logger::getRootLogger()->removeAllAppenders();

	pLogger->logger = log4cxx::Logger::getLogger(szUnitName);

	*phHandle = (DTBLogHandle)pLogger;

	log4cxx::ConsoleAppenderPtr appender(new log4cxx::ConsoleAppender());

	LOG4CXX_DECODE_CHAR(pattern, (szPattern == NULL)?strPattern:szPattern);

	log4cxx::LayoutPtr layout(new log4cxx::PatternLayout(pattern));
	appender->setLayout(layout);

	log4cxx::helpers::Pool pool;
	appender->activateOptions(pool);

	pLogger->logger->addAppender(appender);

	pLogger->logger->setLevel(log4cxx::Level::getDebug());

	log4cxx::LogManager::getLoggerRepository()->setConfigured(true);
#elif defined(SUPPORT_ANDROID) || defined(_SUPPORT_SPDLOG_)
    DTB_LoggerPtr *pLogger = (DTB_LoggerPtr *)MALLOCZ(sizeof(DTB_LoggerPtr));

	if (pLogger == NULL) {
		return FR_FAIL;
	}

    *phHandle = (FrLogHandle)pLogger;

    pLogger->log_level = e_LOG_TRACE;
#if defined(_SUPPORT_SPDLOG_)
    pLogger->logger = spdlog::stdout_color_mt(szUnitName);
    spdlog::set_pattern((szPattern == NULL)?strPattern:szPattern);
    spdlog::set_level(spdlog::level::trace);
    pLogger->log_level = e_LOG_TRACE;
    strcpy(pLogger->unitName , szUnitName);
#endif
#endif
	return FR_OK;
}

/**
@brief Open the file logger
@remarks ������� ������ ����ϴ� logger handler�� �����Ѵ�.
@param phHandle logger handler
@param szUnitName logger �ĺ��� (�� �α� ���� Unique)
@param szLogFilename �α� ���� �̸� (full path)
@param szPattern Pattern string (���� NULL�̸� default pattern ���)
@return ó�� ��� �ڵ�
*/
LOG4RESULT FrLogFileOpen(FrLogHandle* phHandle, char *szUnitName, char *szLogFilename, char *szPattern, LOG4LONG nAppend)
{
#if !defined(SUPPORT_ANDROID) && !defined(IOS) && !defined(_SUPPORT_SPDLOG_)
	DTB_LoggerPtr *pLogger = (DTB_LoggerPtr *)MALLOCZ(sizeof(DTB_LoggerPtr));

	if (pLogger == NULL) {
		return FR_FAIL;
	}

	log4cxx::Logger::getRootLogger()->removeAllAppenders();

	pLogger->logger = log4cxx::Logger::getLogger(szUnitName);

	*phHandle = (DTBLogHandle)pLogger;

	log4cxx::FileAppenderPtr appender(new log4cxx::FileAppender());

	LOG4CXX_DECODE_CHAR(pattern, (szPattern == NULL)?strPattern:szPattern);
	log4cxx::LayoutPtr layout(new log4cxx::PatternLayout(pattern));
	appender->setLayout(layout);

	LOG4CXX_DECODE_CHAR(filename, szLogFilename);
	appender->setFile(filename);

	// �̾�� ����
	//appender->setAppend(false);

	if (nAppend > 0)
		appender->setAppend(true);
	else
		appender->setAppend(false);

	log4cxx::helpers::Pool pool;
	appender->activateOptions(pool);

	pLogger->logger->addAppender(appender);

	log4cxx::LogManager::getLoggerRepository()->setConfigured(true);
#elif defined(SUPPORT_ANDROID) || defined(_SUPPORT_SPDLOG_)
    DTB_LoggerPtr *pLogger = (DTB_LoggerPtr *)MALLOCZ(sizeof(DTB_LoggerPtr));

    if (pLogger == NULL) {
        return FR_FAIL;
    }

	*phHandle = (FrLogHandle)pLogger;

    pLogger->log_level = e_LOG_TRACE;
#if defined(_SUPPORT_SPDLOG_)

#if defined(SUPPORT_ANDROID)
    std::string tag = "spdlog-android";
    pLogger->logger = spdlog::android_logger_mt(szUnitName, tag);
    spdlog::set_pattern((szPattern == NULL)?strPattern:szPattern);
    spdlog::set_level(spdlog::level::trace);
    pLogger->log_level = e_LOG_TRACE;
    strcpy(pLogger->unitName , szUnitName);
#else
    pLogger->logger = spdlog::basic_logger_mt(szUnitName, szLogFilename);
    spdlog::set_pattern((szPattern == NULL)?strPattern:szPattern);
    spdlog::set_level(spdlog::level::trace);
    pLogger->log_level = e_LOG_TRACE;
    strcpy(pLogger->unitName , szUnitName);
#endif  //SUPPORT_ANDROID
#endif  //SUPPORT_SDPLOG


#endif
	return FR_OK;
}

/**
@brief Open the rolling file logger
@remarks ������� ������ ����ϴ� logger handler�� �����Ѵ�. �̶� ������ ������ ���ǿ� ���� ��� �����Ͽ� rolling�ȴ�.
@param phHandle logger handler
@param szUnitName logger �ĺ��� (�� �α� ���� Unique)
@param szLogFilename �α� ���� �̸� (full path)
@param szPattern Pattern string (���� NULL�̸� default pattern ���)
@param nMaxFileSize ������Ϸ� roll over �Ǵ� ���� ������
@param nIndexCnt wraparound �Ǵ� ��� ������ �ִ� ����
@return ó�� ��� �ڵ�
*/
LOG4RESULT FrLogRollingFileOpen(FrLogHandle* phHandle, char *szUnitName, char *szLogFilename, char *szPattern, LOG4LONG nMaxFileSize, LOG4LONG nIndexCnt)
{
#if !defined(SUPPORT_ANDROID) && !defined(IOS) && !defined(_SUPPORT_SPDLOG_)
	DTB_LoggerPtr *pLogger = (DTB_LoggerPtr *)MALLOCZ(sizeof(DTB_LoggerPtr));

	if (pLogger == NULL) {
		return FR_FAIL;
	}

	log4cxx::Logger::getRootLogger()->removeAllAppenders();

	pLogger->logger = log4cxx::Logger::getLogger(szUnitName);

	*phHandle = (DTBLogHandle)pLogger;

	log4cxx::RollingFileAppenderPtr appender(new log4cxx::RollingFileAppender());

	LOG4CXX_DECODE_CHAR(pattern, (szPattern == NULL)?strPattern:szPattern);
	log4cxx::LayoutPtr layout(new log4cxx::PatternLayout(pattern));
	appender->setLayout(layout);

	LOG4CXX_DECODE_CHAR(filename, szLogFilename);
	appender->setFile(filename);

	// Set max file size.
	appender->setMaximumFileSize(nMaxFileSize);

	// Set max backup index
	appender->setMaxBackupIndex(nIndexCnt);

	log4cxx::helpers::Pool pool;
	appender->activateOptions(pool);

	pLogger->logger->addAppender(appender);
	//log4cxx::Logger::getRootLogger()->addAppender(appender);

	log4cxx::LogManager::getLoggerRepository()->setConfigured(true);

#elif defined(SUPPORT_ANDROID) || defined(_SUPPORT_SPDLOG_)
    DTB_LoggerPtr *pLogger = (DTB_LoggerPtr *)MALLOCZ(sizeof(DTB_LoggerPtr));

    if (pLogger == NULL) {
        return FR_FAIL;
    }

	*phHandle = (FrLogHandle)pLogger;

    pLogger->log_level = e_LOG_TRACE;
#if defined(_SUPPORT_SPDLOG_)
    pLogger->logger = spdlog::rotating_logger_mt(szUnitName, szLogFilename, nMaxFileSize, nIndexCnt);
    spdlog::set_pattern((szPattern == NULL)?strPattern:szPattern);
    spdlog::set_level(spdlog::level::trace);
    pLogger->log_level = e_LOG_TRACE;
    strcpy(pLogger->unitName , szUnitName);

#endif


#endif
	return FR_OK;
}

/**
@brief Open the daily rolling file logger
@remarks ������� ������ ����ϴ� logger handler�� �����Ѵ�. �̶� daily�� ��� �����Ͽ� rolling�ȴ�.
@param phHandle logger handler
@param szUnitName logger �ĺ��� (�� �α� ���� Unique)
@param szLogFilename �α� ���� �̸� (full path)
@param szPattern Pattern string (���� NULL�̸� default pattern ���)
@return ó�� ��� �ڵ�
*/
LOG4RESULT FrLogDailyRollingFileOpen(FrLogHandle* phHandle, char *szUnitName, char *szLogFilename, char *szPattern)
{
#if !defined(SUPPORT_ANDROID) && !defined(IOS) && !defined(_SUPPORT_SPDLOG_)
	DTB_LoggerPtr *pLogger = (DTB_LoggerPtr *)MALLOCZ(sizeof(DTB_LoggerPtr));

	if (pLogger == NULL) {
		return FR_FAIL;
	}

	log4cxx::Logger::getRootLogger()->removeAllAppenders();

	pLogger->logger = log4cxx::Logger::getLogger(szUnitName);

	*phHandle = (DTBLogHandle)pLogger;

	log4cxx::DailyRollingFileAppenderPtr appender(new log4cxx::DailyRollingFileAppender());

	LOG4CXX_DECODE_CHAR(pattern, (szPattern == NULL)?strPattern:szPattern);
	log4cxx::LayoutPtr layout(new log4cxx::PatternLayout(pattern));
	appender->setLayout(layout);

	LOG4CXX_DECODE_CHAR(filename, szLogFilename);
	appender->setFile(filename);

	// Set date pattern of file name
	LOG4CXX_DECODE_CHAR(datePattern, ".yyyy.MM.dd.HH.mm");
	appender->setDatePattern(datePattern);

	log4cxx::helpers::Pool pool;
	appender->activateOptions(pool);

	pLogger->logger->addAppender(appender);
	//log4cxx::Logger::getRootLogger()->addAppender(appender);

	log4cxx::LogManager::getLoggerRepository()->setConfigured(true);

#elif defined(SUPPORT_ANDROID) || defined(_SUPPORT_SPDLOG_)
    DTB_LoggerPtr *pLogger = (DTB_LoggerPtr *)MALLOCZ(sizeof(DTB_LoggerPtr));

    if (pLogger == NULL) {
        return FR_FAIL;
    }

	*phHandle = (FrLogHandle)pLogger;

    pLogger->log_level = e_LOG_TRACE;
#if defined(_SUPPORT_SPDLOG_)
    pLogger->logger =  spdlog::daily_logger_mt(szUnitName, szLogFilename, 0, 0);
    spdlog::set_pattern((szPattern == NULL)?strPattern:szPattern);
    spdlog::set_level(spdlog::level::trace);
    pLogger->log_level = e_LOG_TRACE;
    strcpy(pLogger->unitName , szUnitName);
#endif


#endif
	return FR_OK;
}

/**
@brief Open the external configured logger
@remarks �ܺ� �������Ϸ� ���� ���� ������ �����ϴ� logger handler�� �����Ѵ�.
@param phHandle logger handler
@param szUnitName logger �ĺ��� (�� �α� ���� Unique)
@param szConfigFile ���� ���� ���� �̸�
@return ó�� ��� �ڵ�
*/
LOG4RESULT FrLogOpenFromCFGFile(FrLogHandle* phHandle, char *szUnitName, char *szConfigFile)
{
#if !defined(SUPPORT_ANDROID) && !defined(IOS) && !defined(_SUPPORT_SPDLOG_)
	DTB_LoggerPtr *pLogger = (DTB_LoggerPtr *)MALLOCZ(sizeof(DTB_LoggerPtr));

	if (pLogger == NULL) {
		return FR_FAIL;
	}

	log4cxx::Logger::getRootLogger()->removeAllAppenders();

	pLogger->logger = log4cxx::Logger::getLogger(szUnitName);

	*phHandle = (DTBLogHandle)pLogger;

	if ( (strlen(szConfigFile) > 4) && (strncmp(szConfigFile + (strlen(szConfigFile) - 4) , ".xm;" , 4) == 0) ) {
		log4cxx::xml::DOMConfigurator::configure(szConfigFile);
	}
	else {
		log4cxx::PropertyConfigurator::configure(szConfigFile);
	}

#elif defined(SUPPORT_ANDROID) || defined(_SUPPORT_SPDLOG_)
    return FR_FAIL;
#endif
	return FR_OK;
}

/**
@brief Close the logger
@remarks logger handle�� �����Ѵ�.
@param phHandle logger handle
*/
void FrLogClose(FrLogHandle phHandle)
{
#if !defined(SUPPORT_ANDROID) && !defined(IOS) && !defined(_SUPPORT_SPDLOG_)
	DTB_LoggerPtr *pLogger;
	pLogger = (DTB_LoggerPtr *)phHandle;

	if (pLogger == NULL) {
		return;
	}

	// 1. Append ��忡�� ���� UnitName���� DTBLogFileOpen �� ������ ȣ���Ұ��
	// �Ʒ� logger�� release ���� ������ DTBLogPrn ȣ��� DTBLogFileOpen ȣ�⸸ŭ
	// ������ �αװ� ��µ�
    // 2. �޸𸮸� �߻�

	pLogger->logger->closeNestedAppenders();
	pLogger->logger->removeAllAppenders();

	log4cxx::Logger::getRootLogger()->closeNestedAppenders();
	log4cxx::Logger::getRootLogger()->removeAllAppenders();

	//log4cxx::Logger::getRootLogger()->releaseRef();

	pLogger->logger->releaseRef();

#elif defined(SUPPORT_ANDROID) || defined(_SUPPORT_SPDLOG_)
    DTB_LoggerPtr *pLogger;
	pLogger = (DTB_LoggerPtr *)phHandle;

	if (pLogger == NULL) {
		return;
	}

#if defined(_SUPPORT_SPDLOG_)
    //spdlog::shutdown();
    //spdlog::drop(pLogger->unitName);
    spdlog::drop(pLogger->logger->name());
    pLogger->logger.reset();

#endif

#endif

    if (phHandle != NULL)
		FREE(phHandle);

}

/**
@brief Select the log level
@remarks �α� ������ �����Ѵ�.
@param phHandle logger handle
@param log_level ��µ� �α��� �ּ� ���� (������ ���� �̻��� ������ ��� ���)
*/
LOG4RESULT FrLogSetLevel(FrLogHandle phHandle, INT32 log_level)
{
#ifndef IOS
	DTB_LoggerPtr *pLogger;
	pLogger = (DTB_LoggerPtr *)phHandle;

	if (pLogger == NULL) {
		return FR_FAIL;
	}
#endif

#if defined(SUPPORT_ANDROID) || defined(_SUPPORT_SPDLOG_)
    pLogger->log_level = log_level;
#elif !defined(IOS)
    pLogger->log_level = log_level;

	switch (log_level) {
		case e_LOG_FATAL :
			pLogger->logger->setLevel(log4cxx::Level::getFatal());
			break;
			/*
		case e_LOG_ASSERT :
			pLogger->logger->setLevel(log4cxx::Level::getAssert());
			break;
			*/
		case e_LOG_ERROR :
			pLogger->logger->setLevel(log4cxx::Level::getError());
			break;
		case e_LOG_WARN :
			pLogger->logger->setLevel(log4cxx::Level::getWarn());
			break;
		case e_LOG_INFO :
			pLogger->logger->setLevel(log4cxx::Level::getInfo());
			break;
		case e_LOG_TRACE :
			pLogger->logger->setLevel(log4cxx::Level::getTrace());
			break;
		case e_LOG_DEBUG:
			pLogger->logger->setLevel(log4cxx::Level::getDebug());
			break;
		default :
			pLogger->logger->setLevel(log4cxx::Level::getDebug());
			break;
	}
#endif
	return FR_OK;
}

/**
@brief Get the logger
@remarks ������ Unit Name�� logger ��ü�� ���´�.
@param phHandle logger handler
@param szUnitName logger �ĺ��� (�� �α� ���� Unique)
@return ó�� ��� �ڵ�
*/
LOG4RESULT FrGetLogger(FrLogHandle* phHandle, char *szUnitName)
{
#if !defined(SUPPORT_ANDROID) && !defined(IOS) && !defined(_SUPPORT_SPDLOG_)
	DTB_LoggerPtr *pLogger = (DTB_LoggerPtr *)MALLOCZ(sizeof(DTB_LoggerPtr));

	if (pLogger == NULL) {
		return FR_FAIL;
	}

	pLogger->logger = log4cxx::Logger::getLogger(szUnitName);

	*phHandle = (DTBLogHandle)pLogger;
#elif defined(SUPPORT_ANDROID) || defined(_SUPPORT_SPDLOG_)
    return FR_FAIL;
#endif
	return FR_OK;
}


/**
@brief Print the log message
@remarks ������ ������ �޽����� ������ ������ �α׷� ����Ѵ�. ��� ����� Open�Լ��� �����ǰų� �������Ϸ� �����ȴ�.
@param phHandle logger handle
@param file_name �α� ��� ��û�� �ҽ� ���� �̸� (full path)
@param file_line �α� ��� ��û�� �ҽ� ������ ����
@param log_level log level (�α� ������ ���� : LOG_FATAL, LOG_ERROR, LOG_WARN, LOG_INFO, LOG_TRACE, LOG_DEBUG)
@param format ��µ� �α� ���� (printf�� ������ ����)
*/
void FrLogPrn(FrLogHandle phHandle, char* file_name, INT32 file_line, INT32 log_level , const char *format, ... )
{
	va_list argList;
	char szBuffer[MAX_BUF_LENG];

#ifndef IOS
	DTB_LoggerPtr *pLogger;
	pLogger = (DTB_LoggerPtr *)phHandle;

	if (pLogger && log_level > pLogger->log_level) {
		return;
	}
#endif

	va_start(argList,format);

#ifdef WIN32
    vsprintf_s(szBuffer, MAX_BUF_LENG - 1, format, argList);
#else
    vsnprintf(szBuffer, MAX_BUF_LENG - 1, format, argList);
#endif
    va_end(argList);

#if defined(SUPPORT_ANDROID)

    if (pLogger) {

        if (log_level > pLogger->log_level) {
            return;
        }

        char szTimeBuffer[512];
        char szTmpBufferDate[128];
        char szTmpBufferTime[128];
        snprintf(szTimeBuffer, 512, "[%s:%s](%-14s,%4d) %s",
    					get_datestr(szTmpBufferDate),
    					get_timestr(szTmpBufferTime),
    					file_name,
    					file_line,
    					logLevel2str(log_level));

        switch (log_level) {
    		case e_LOG_FATAL :
                LOGE("%s %s" , szTimeBuffer, szBuffer);
    			break;
    		case e_LOG_ERROR :
    			LOGE("%s %s" , szTimeBuffer, szBuffer);
    			break;
    		case e_LOG_WARN :
    			LOGI("%s %s" , szTimeBuffer, szBuffer);
    			break;
    		case e_LOG_INFO :
    			LOGI("%s %s" , szTimeBuffer, szBuffer);
    			break;
    		case e_LOG_TRACE :
                LOGD("%s %s" , szTimeBuffer, szBuffer);
    			break;
    		case e_LOG_DEBUG:
    			LOGD("%s %s" , szTimeBuffer, szBuffer);
    			break;
    		default :
    			LOGD("%s %s" , szTimeBuffer, szBuffer);
    			break;
    	}
    }
#elif defined(SUPPORT_ANDROID) && defined(_SUPPORT_SPDLOG_)
    if (pLogger) {

        if (log_level > pLogger->log_level) {
            return;
        }

        char szTimeBuffer[MAX_BUF_LENG+1];


        snprintf(szTimeBuffer, MAX_BUF_LENG, "(%-14s,%4d) %s %s",
                        file_name,
                        file_line,
                        logLevel2str(log_level),
                        szBuffer);


        switch (log_level) {
    		case e_LOG_FATAL :
                pLogger->logger->critical(szTimeBuffer);
    			break;
    		case e_LOG_ERROR :
    			pLogger->logger->error(szTimeBuffer);
    			break;
    		case e_LOG_WARN :
    			pLogger->logger->warn(szTimeBuffer);
    			break;
    		case e_LOG_INFO :
    			pLogger->logger->info(szTimeBuffer);
    			break;
    		case e_LOG_TRACE :
                pLogger->logger->trace(szTimeBuffer);
    			break;
    		case e_LOG_DEBUG:
    			pLogger->logger->debug(szTimeBuffer);
    			break;
    		default :
    			pLogger->logger->debug(szTimeBuffer);
    			break;
    		}

		pLogger->logger->flush();
    }
#elif defined(_SUPPORT_SPDLOG_)
    if (pLogger) {

            if (log_level > pLogger->log_level) {
                return;
            }

            char szTimeBuffer[MAX_BUF_LENG+1];

#ifdef _WIN32
            snprintf(szTimeBuffer, MAX_BUF_LENG, "(%-14s,%4d) %s %s",
                            strrchr(file_name, '\\') ? strrchr(file_name, '\\') + 1:file_name,
                            file_line,
                            logLevel2str(log_level),
                            szBuffer);

#else

            snprintf(szTimeBuffer, MAX_BUF_LENG, "(%-14s,%4d) %s %s",
                            strrchr(file_name, '/') ? strrchr(file_name, '/') + 1:file_name,
                            file_line,
                            logLevel2str(log_level),
                            szBuffer);
#endif

        switch (log_level) {
    		case e_LOG_FATAL :
                pLogger->logger->critical(szTimeBuffer);
    			break;
    		case e_LOG_ERROR :
    			pLogger->logger->error(szTimeBuffer);
    			break;
    		case e_LOG_WARN :
    			pLogger->logger->warn(szTimeBuffer);
    			break;
    		case e_LOG_INFO :
    			pLogger->logger->info(szTimeBuffer);
    			break;
    		case e_LOG_TRACE :
                pLogger->logger->trace(szTimeBuffer);
    			break;
    		case e_LOG_DEBUG:
    			pLogger->logger->debug(szTimeBuffer);
    			break;
    		default :
    			pLogger->logger->debug(szTimeBuffer);
    			break;
    		}

		pLogger->logger->flush();
    }
#elif !defined(IOS)
	if (pLogger)
	{
		switch (log_level) {
		case e_LOG_FATAL :
			DTB_LOG4CXX_FATAL(pLogger->logger , file_name, file_line, szBuffer);
			break;
			/*
		case e_LOG_ASSERT :
			DTB_LOG4CXX_ASSERT(pLogger->logger , file_name, file_line, szBuffer);
			break;
			*/
		case e_LOG_ERROR :
			DTB_LOG4CXX_ERROR(pLogger->logger , file_name, file_line, szBuffer);
			break;
		case e_LOG_WARN :
			DTB_LOG4CXX_WARN(pLogger->logger , file_name, file_line, szBuffer);
			break;
		case e_LOG_INFO :
			DTB_LOG4CXX_INFO(pLogger->logger , file_name, file_line, szBuffer);
			break;
		case e_LOG_TRACE :
//			DTB_LOG4CXX_TRACE(pLogger->logger , file_name, file_line, szBuffer);
            if (pLogger->logger->isTraceEnabled()) {
                   ::log4cxx::helpers::MessageBuffer oss_;
                   pLogger->logger->forcedLog(::log4cxx::Level::getTrace(),
                    oss_.str(oss_ << szBuffer), DTB_LOG4CXX_LOCATION(file_name, file_line));
            }

			break;
		case e_LOG_DEBUG:
			DTB_LOG4CXX_DEBUG(pLogger->logger , file_name, file_line, szBuffer);
			break;
		default :
			DTB_LOG4CXX_DEBUG(pLogger->logger , file_name, file_line, szBuffer);
			break;
		}
	}

#if defined(_WIN32) && defined(_DEBUG)
	strcat(szBuffer, "\r\n");
# ifdef 	_UNICODE
	OutputDebugString((LPCWSTR)szBuffer);
# else
	OutputDebugString(szBuffer);
# endif
#endif

#endif  /// for SUPPORT_ANDROID
}



