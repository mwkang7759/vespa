

#include "SystemAPI.h"
#include "TraceAPI.h"

///////////////////////////////////
// Msg
///////////////////////////////////
void BoxQuestion (const char* msg)
{
#ifdef	_WIN32
	MessageBox ( NULL, msg, "Error Message . . .", MB_OK | MB_ICONQUESTION );
#else
	TRACE("[System]Question Message . . . %s", msg);
#endif
}

void BoxError(const char* msg)
{
#ifdef	_WIN32
	MessageBox ( NULL, msg, "Error Message . . .", MB_OK | MB_ICONERROR | MB_ICONSTOP | MB_ICONHAND );
#else
	TRACE("[System]Error Message . . . %s", msg);
#endif
}

void BoxWarning(const char* msg)
{
#ifdef	_WIN32
	MessageBox ( NULL, msg, "Error Message . . .", MB_OK | MB_ICONWARNING | MB_ICONEXCLAMATION );
#else
	TRACE("[System]Warning Message . . . %s", msg);
#endif
}

void BoxInform(const char* msg)
{
#ifdef	_WIN32
	MessageBox ( NULL, msg, "Error Message . . .", MB_OK | MB_ICONINFORMATION | MB_ICONASTERISK );
#else
	TRACE("[System]Inform Message . . . %s", msg);
#endif
}
