
#include "SystemAPI.h"
#include "TraceAPI.h"
#include "UtilAPI.h"

INT32 VSPRINTF(char* s0, const char *fmt, va_list args)
{
	char	Num[80];
	char*	s;

	for (s=s0;*fmt;++fmt)
	{
		if (fmt[0]=='%' && fmt[1])
		{
			const char *str;
			BOOL Left = 0;
			BOOL Sign = 0;
			BOOL Large = 0;
			BOOL ZeroPad = 0;
			INT32 Width = -1;
			INT32 Type = -1;
			INT32 Base = 10;
			char ch,cs;
			INT32 Len;
			INT32 n;

			for (;;)
			{
				switch (*(++fmt)) 
				{
				case '-': Left = 1; continue;
				case '0': ZeroPad = 1; continue;
				default: break;
				}
				break;
			}

			if (ISDIGIT(*fmt))
			{
				Width = 0;
				for (;ISDIGIT(*fmt);++fmt)
					Width = Width*10 + (*fmt-'0');
			}
			else
			if (*fmt == '*')
			{
				++fmt;
				Width = va_arg(args, INT32);
				if (Width < 0)
				{
					Left = 1;
					Width = -Width;
				}
			}

			if (*fmt == 'h' || 
				*fmt == 'L' ||
				*fmt == 'l')
				Type = *(fmt++);

			switch (*fmt)
			{
			case 'c':
				for (;!Left && Width>1;--Width)
					*(s++) = ' ';
				*(s++) = (char)va_arg(args,INT32);
				for (;Width>1;--Width)
					*(s++) = ' ';
				continue;
			case 's':
				str = va_arg(args,const char*);
				if (!s)
					str = NULL;
				Len = (INT32)strlen(str);
				for (;!Left && Width>Len;--Width)
					*(s++) = ' ';
				for (;Len>0;--Len,--Width)
					*(s++) = *(str++);
				for (;Width>0;--Width)
					*(s++) = ' ';
				continue;
			case 'o':
				Base = 8;
				break;
			case 'X':
				Large = 1;
			case 'x':
				Base = 16;
				break;
			case 'i':
			case 'd':
				Sign = 1;
			case 'u':
				break;
			default:
				if (*fmt != '%')
					*(s++) = '%';
				*(s++) = *fmt;
				continue;
			}

			if (Type == 'l')
				n = va_arg(args,UINT32);
			else
			if (Type == 'h')
				if (Sign)
					n = (short)va_arg(args,INT32);
				else
					n = (unsigned short)va_arg(args,UINT32);
			else
			if (Sign)
				n = va_arg(args,INT32);
			else
				n= va_arg(args,UINT32);

			if (Left)
				ZeroPad = 0;
			if (Large)
				str = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			else
				str = "0123456789abcdefghijklmnopqrstuvwxyz";

			ch = ' ';
			if (ZeroPad)
				ch = '0';
			cs = 0;

			if (n<0 && Sign)
			{
				cs = '-';
				n=-n;
			}

			Len = 0;
			if (n==0)
				Num[Len++] = '0';
			else
			{
				UINT32 un = n;
				while (un != 0)
				{
					Num[Len++] = str[un%Base];
					un /= Base;
				}
			}

			if (cs)
				++Len;

			for (;!Left && Width>Len;--Width)
				*(s++) = ch;

			if (cs)
			{
				*(s++) = cs;
				--Len;
				--Width;
			}

			for (;Len;--Width)
				*(s++) = Num[--Len];

			for (;Width>0;--Width)
				*(s++) = ' ';
		}
		else
			*(s++) = *fmt;
	}
	*(s++) = 0;
	return (INT32)(s-s0);
}

void SPRINTF(char* pStrBuff, char* lpszFormat, ...)
{
	va_list	ap;
	char	*fmt = lpszFormat;
	char	SDigit[12];
	char	*SPtr;
	char	*pStr = pStrBuff;

	va_start(ap, lpszFormat);

	while (*fmt != '\0') {
		if (*fmt == '%') {
			fmt++;
			switch (*fmt) {
				case '0':
				fmt++;
				switch (*fmt) {
					case '2':
						fmt++;
						switch (*fmt) {
							case 'x':case 'X':
								ITOX(va_arg(ap,UINT32),SDigit);
								strncpy(pStr, SDigit+6, 2);
								pStr += 2;
								break;
							case 'u':case 'U':
								ITOU(va_arg(ap,UINT32),SDigit);
								strncpy(pStr, SDigit+6, 2);
								pStr += 2;
								break;
							default:
								*pStr = *fmt;
								pStr++;
						}
						break;
					case '4':
						fmt++;
						switch (*fmt) {
							case 'x':case 'X':
								ITOX(va_arg(ap,UINT32),SDigit);
								strncpy(pStr, SDigit+4, 4);
								pStr += 4;
								break;
							case 'u':case 'U':
								ITOU(va_arg(ap,UINT32),SDigit);
								strncpy(pStr, SDigit+4, 4);
								pStr += 4;
								break;
							default:
								*pStr = *fmt;
								pStr++;
						}
						break;
					case '8':
						fmt++;
						switch (*fmt) {
							case 'x':case 'X':
								ITOX(va_arg(ap,UINT32),SDigit);
								strncpy(pStr, SDigit, 8);
								pStr += 8;
								break;
							case 'u':case 'U':
								ITOU(va_arg(ap,UINT32),SDigit);
								strncpy(pStr, SDigit, 8);
								pStr += 8;
								break;
							default:
								*pStr = *fmt;
								pStr++;
						}
						break;
					default:
						*pStr = *fmt;
						pStr++;
					}
					break;
				case 'x':case 'X':
					ITOX(va_arg(ap,UINT32),SDigit);
					SPtr = SDigit;
					while ((*SPtr) == '0') {
						SPtr++;
					}
					if (*SPtr == '\0') {
						SPtr--;
					}
					strncpy(pStr, SPtr, strlen(SPtr));
					pStr += strlen(SPtr);
					break;
				case 'u':case 'U':
					ITOU(va_arg(ap,UINT32),SDigit);
					SPtr = SDigit;
					while ((*SPtr) == '0') {
						SPtr++;
					}
					if(*SPtr == '\0') {
						SPtr--;
					}
					strncpy(pStr, SPtr, strlen(SPtr));
					pStr += strlen(SPtr);
					break;
				case 'd':case 'D':
					ITOA(va_arg(ap,INT32),SDigit);
					strncpy(pStr, SDigit, strlen(SDigit));
					pStr += strlen(SDigit);
					break;
				case 's':case 'S':
					SPtr=va_arg(ap,char*);
					strncpy(pStr, SPtr, strlen(SPtr));
					pStr += strlen(SPtr);
					break;
				case 'c':case 'C':
					SDigit[0] = va_arg(ap,INT32);
					*pStr = SDigit[0];
					pStr++;
					break;
				default:
					*pStr = *fmt;
					pStr++;
			}
			fmt++;
		}
		else{
			*pStr = *fmt;
			pStr++;
			fmt++;
		}
	}
	*pStr = '\0';
	va_end(ap);
}
