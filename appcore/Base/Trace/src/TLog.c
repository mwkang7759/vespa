
#if 0
#define _TLog_c_

/*=============================================================================
                               I N C L U D E
=============================================================================*/
#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW)
#include <sys/time.h>		/* struct timeval */
#include <unistd.h>

/* log lib update added by jykim(20060821) */
#include <pthread.h>
#endif

#ifdef WIN32
#include <Windows.h>
#endif

#include <time.h>			/* struct tm */
#include <sys/types.h>
#include <sys/stat.h>
//#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

//#include "SystemType.h"		// change type header file, mwkang 2009.06.05
#include "TLogAPI.h"
//#include "CErrCode.h"

#ifdef CYGWIN /*---------------------------------------------------------------*/
#include <string.h>
#include <reent.h>
#include <sys/time.h>
#endif /*----------------------------------------------------------------------*/

/*#define printf(fmt, args...) */

/*=============================================================================
                              C O N S T A N T
=============================================================================*/

/*=============================================================================
                    C L A S S   A N D   S T R U C T U R E
=============================================================================*/

/*=============================================================================
                               M A C R O
=============================================================================*/

/*=============================================================================
                      S T A T I C   V A R I A B L E
=============================================================================*/

/*=============================================================================
                       S T A T I C   F U C T I O N
=============================================================================*/
#ifdef _WIN32

static INT32 tLogOn(TLogInfo* tLog, INT32 log_level);
static char* logLevel2str(INT32 level);
static void resetlog(TLogInfo* tLog);
static void get_old_index(TLogInfo* tLog);
static char* get_datestr(char* strDate);
static char* get_timestr(char* strTime);

INT32 tLogOn(TLogInfo* tLog, INT32 log_level)
{
    return (tLog->logLevel & log_level) ? 1 : 0;
}


INT32 tLogInit(TLogInfo* tLog)
{	
	if(strlen(tLog->filePath) == 0 || tLog->logLevel == 0) 
	{
        //printf("Initialize fail. filePath[%s], logLevel[%d]\n",
        //   tLog->filePath, tLog->logLevel);
    
		McEnterMutex(&tLog->m_xFwrite);

		if (tLog->fp)
		{
			fclose(tLog->fp);
			tLog->fp = NULL;
		}

		McLeaveMutex(&tLog->m_xFwrite);

		return -1;

		//return -1;
    }

    tLog->fileSizeLimit = DEF_SIZE_OF_FILE;
    tLog->fileBackupMax = DEF_NUM_OF_FILE;
    tLog->fileBackupSeq = 0;
	tLog->bRenameLogFile = FALSE;

    // Save Init FileName
    strcpy(tLog->filePathInit, tLog->filePath);

    if(!(tLog->fp = fopen(tLog->filePath, "r")))
    {
        if(!(tLog->fp = fopen(tLog->filePath, "w+"))) {
#ifndef WIN32
            fprintf(stdout, "[%s] file create error: %s\n", __func__, _error);
#endif
            return -1;
        }
    }
    else
    {
		McEnterMutex(&tLog->m_xFwrite);

		if (tLog->fp)
		{
			fclose(tLog->fp);
			tLog->fp = NULL;
		}

		McLeaveMutex(&tLog->m_xFwrite);

        if(!(tLog->fp = fopen(tLog->filePath, "a"))) {
#ifndef WIN32
            fprintf(stdout, "[%s] file open error: %s\n", __func__, _error);
#endif
            return -1;
        }

        get_old_index(tLog);
    }


//	if (tLog->fp)
//	{
//		if(ftell(tLog->fp) > DEF_SIZE_OF_FILE) resetlog(tLog);

//		fflush(tLog->fp);
//	}	

	get_datestr(tLog->strLastDate);

    return 0;
}

void tLogRename(TLogInfo* tLog, char* file_name, INT32 file_line, INT32 log_level)
{
	INT32		nRet;
	BOOL	bRenameLogFile = FALSE;
	char	szCurData[MAX_PATH] = {0,};
	DWORD32	dwLastDate = 0, dwCurDate = 0;
	char	szTemp[MAX_PATH];	

	char szNewFileName[MAX_PATH];
	char drive[_MAX_DRIVE];
	char dir[_MAX_DIR];
	char fname[_MAX_FNAME];
	char ext[_MAX_EXT];	

#if 0
	if (tLog->bRenameLogFile == FALSE)
	{
		get_datestr(szCurData);
		dwCurDate = atoi(szCurData);	
		dwLastDate = atoi(tLog->strLastDate);		
	
		// Ȥ �ٸ� �����忡�� tLog->strLastDate �����͸� �������϶� �Ʒ� ������ ���� �������� �±����� 'dwLastDate & 0x1000000' �����ʿ�
		//if (dwLastDate != dwCurDate && (dwCurDate & 0x1000000) && (dwLastDate & 0x1000000))
		if ( (tLog->bRenameLogFile == FALSE) && (dwLastDate != dwCurDate) && (dwLastDate & 0x1000000) )
		{
			tLog->bRenameLogFile = TRUE;
			strcpy(tLog->strLastDate, szCurData);

			sprintf(szTemp, "LastDate : %d, CurDate : %d\r\n", dwLastDate, dwCurDate);
			if (file_name == NULL)
			{
				if (tLog->fp)
					fprintf(tLog->fp, "[%s:%s] %s %s", get_datestr(tLog->strDate), get_timestr(tLog->strTime), logLevel2str(log_level), szTemp);
			}
			else
			{
				if (tLog->fp)
					fprintf(tLog->fp, "[%s:%s](%-14s,%4d) %s %s", get_datestr(tLog->strDate), get_timestr(tLog->strTime), file_name, file_line, logLevel2str(log_level), szTemp);			
			}		
		}
		else
		{
			tLog->bRenameLogFile = FALSE;
			dwLastDate = 0;
		}
	}	

	if (tLog->bRenameLogFile && (dwLastDate & 0x1000000))
	{
#else

	// ��Ƽ�����忡�� �Ʒ� ���ؽ��� �ɸ� Deadlock ���� ����.
	// 20100504 : by Andy : UBOX �������� ���� �� ����� ������ ����µ� Ȯ�� �ʿ� (Deadlock ���� �ƴҼ� ����)
	McEnterMutex(&tLog->m_xSession);

	get_datestr(szCurData);
	dwCurDate = atoi(szCurData);	
	dwLastDate = atoi(tLog->strLastDate);		

	if (dwLastDate != dwCurDate)
	{
		strcpy(tLog->strLastDate, szCurData);
		bRenameLogFile = TRUE;
	}

	McLeaveMutex(&tLog->m_xSession);

	if (bRenameLogFile == TRUE)
	{

#endif
		tLog->bRenameLogFile = FALSE;

		_splitpath( tLog->filePath, drive, dir, fname, ext );			
		sprintf(szNewFileName, "%s%s%s_%d.txt", drive, dir, fname, dwLastDate);

		sprintf(szTemp, "Since passed a day, these logs are going to be moved to '%s' file.\r\n", szNewFileName);
		if (file_name == NULL)
		{
			McEnterMutex(&tLog->m_xFwrite);

			if (tLog->fp)
				fprintf(tLog->fp, "[%s:%s] %s %s", get_datestr(tLog->strDate), get_timestr(tLog->strTime), logLevel2str(log_level), szTemp);

			McLeaveMutex(&tLog->m_xFwrite);
		}
		else
		{
			McEnterMutex(&tLog->m_xFwrite);

			if (tLog->fp)
				fprintf(tLog->fp, "[%s:%s](%-14s,%4d) %s %s", get_datestr(tLog->strDate), get_timestr(tLog->strTime), file_name, file_line, logLevel2str(log_level), szTemp);			

			McLeaveMutex(&tLog->m_xFwrite);
		}		

		McEnterMutex(&tLog->m_xFwrite);

		if (tLog->fp)
		{
			fclose(tLog->fp);
			tLog->fp = NULL;
		}

		McLeaveMutex(&tLog->m_xFwrite);
			
		nRet = MoveFileExA  ((LPCTSTR)tLog->filePath, (LPCTSTR)szNewFileName, MOVEFILE_COPY_ALLOWED | MOVEFILE_WRITE_THROUGH);

		if (nRet == 0)
		{
			BOOL bRet = CopyFileA((LPCTSTR)tLog->filePath, (LPCTSTR)szNewFileName, FALSE);
			if (bRet)
				DeleteFileA((LPCTSTR)tLog->filePath);
		}
		
		if(!(tLog->fp = fopen(tLog->filePath, "a"))) 
		{
			tLog->fp = NULL;
			//return FALSE;
		}	
		
		sprintf(szTemp, "Since passed a day, you can check the old trace log through '%s' file.\r\n", szNewFileName);		
		if (file_name == NULL)
		{
			McEnterMutex(&tLog->m_xFwrite);

			if (tLog->fp)
				fprintf(tLog->fp, "[%s:%s] %s %s", get_datestr(tLog->strDate), get_timestr(tLog->strTime), logLevel2str(log_level), szTemp);

			McLeaveMutex(&tLog->m_xFwrite);
		}
		else
		{
			McEnterMutex(&tLog->m_xFwrite);

			if (tLog->fp)
				fprintf(tLog->fp, "[%s:%s](%-14s,%4d) %s %s", get_datestr(tLog->strDate), get_timestr(tLog->strTime), file_name, file_line, logLevel2str(log_level), szTemp);			

			McLeaveMutex(&tLog->m_xFwrite);
		}		
	}
}


void tLogPrn(char* file_name, INT32 file_line, INT32 log_level, TLogInfo* tLog, char* format, ... )
{
	va_list args;
    char    date[16] = "";
    char    buffer[2048] = "";	
	
	tLogRename(tLog, file_name, file_line, log_level);		

//#ifdef _DEBUG
#if 0
#ifdef WIN32
    char str[MAX_BUF_LENG];
    va_list argList;

    if(!tLogOn(tLog, log_level))
        return;

    va_start(argList, format);
    vsprintf(str, format, argList);
    OutputDebugString(str);
    va_end(argList);
#endif // WIN32
#else    

    if(tLog->fp && tLogOn(tLog, log_level))
    {
        if(strncmp(tLog->filePath, tLog->filePathInit, strlen(tLog->filePathInit)))
        {
			McEnterMutex(&tLog->m_xFwrite);

			if(tLog->fp)
			{
				fclose(tLog->fp);
				tLog->fp = NULL;
			}

			McLeaveMutex(&tLog->m_xFwrite);

            sprintf(tLog->filePath, "%s", tLog->filePathInit);
            tLogInit(tLog);
        }
    }
    else
    {
        return;
    }

    //if(ftell(tLog->fp) > DEF_SIZE_OF_FILE) resetlog(tLog);

	// by Andy : �αװ� �ʹ� ������� ���ϸ� ���� ������ ������� �ʵ��� �Ѵ�.
	file_name = NULL;

	if (file_name == NULL)
	{
		McEnterMutex(&tLog->m_xFwrite);

		if (tLog->fp)
		{
			fprintf(tLog->fp, "[%s:%s] %s",
						  get_datestr(tLog->strDate),
						  get_timestr(tLog->strTime),
						  logLevel2str(log_level));
		}

		McLeaveMutex(&tLog->m_xFwrite);
	}
	else
	{
		McEnterMutex(&tLog->m_xFwrite);

		if (tLog->fp)
		{
			fprintf(tLog->fp, "[%s:%s](%-14s,%4d) %s",
						  get_datestr(tLog->strDate),
						  get_timestr(tLog->strTime),
						  file_name,
						  file_line,
						  logLevel2str(log_level));
		}

		McLeaveMutex(&tLog->m_xFwrite);
	}

    va_start(args,format);
#ifdef WIN32
    _vsnprintf(buffer, 2048-1, format, args);
#else
    vsnprintf(buffer, 2048-1, format, args);
#endif
    va_end(args);

	McEnterMutex(&tLog->m_xFwrite);

	if (tLog->fp)
		fprintf(tLog->fp, "%s", buffer);	

	if (tLog->fp)
		fflush(tLog->fp);

	McLeaveMutex(&tLog->m_xFwrite);

#endif
    return;
}


char* logLevel2str(INT32 level)
{
	switch(level)
	{
    	case e_LOG_CRI: return (char*)"[CRI]";
    	case e_LOG_MAJ: return (char*)"[MAJ]";
    	case e_LOG_MIN: return (char*)"[MIN]";
    	case e_LOG_INF: return (char*)"[INF]";
    	case e_LOG_DB0: return (char*)"[DB0]";
    	case e_LOG_DB1: return (char*)"[DB1]";
    	case e_LOG_DB2: return (char*)"[DB2]";
    	case e_LOG_DB3: return (char*)"[DB3]";
    	default:
    		return (char*)"[???]";
	}

	return NULL;
}


void resetlog(TLogInfo* tLog)
{
    char file_path[256] = "";

    sprintf(file_path, "%s.%d",
                     tLog->filePath, tLog->fileBackupSeq++ % tLog->fileBackupMax);

	McEnterMutex(&tLog->m_xFwrite);

	if (tLog->fp)
	{
		fprintf(tLog->fp, " log continues to %s old(%d) fileBackupMax(%d)\n",
						  file_path, tLog->fileBackupSeq, tLog->fileBackupMax);
	}

	McLeaveMutex(&tLog->m_xFwrite);

	McEnterMutex(&tLog->m_xFwrite);

	if (tLog->fp)
	{
		fclose(tLog->fp);
		tLog->fp = NULL;
		rename(tLog->filePath, file_path);    // rename(oldname, newname)
	}

	McLeaveMutex(&tLog->m_xFwrite);

    sprintf(tLog->filePath, tLog->filePathInit);
    if(!(tLog->fp = fopen(tLog->filePath, "r")))
    {
        if(!(tLog->fp = fopen(tLog->filePath, "w+")))
        {
#ifndef WIN32
            fprintf(stdout, "[%s] file create error: %s\n", __func__, _error);
#endif
            return;
        }
    }
    else
    {
		McEnterMutex(&tLog->m_xFwrite);

        if(tLog->fp) 
		{
			fclose(tLog->fp);
			tLog->fp = NULL;
		}

		McLeaveMutex(&tLog->m_xFwrite);

        if(!(tLog->fp = fopen(tLog->filePath, "a")))
        {
#ifndef WIN32
            fprintf(stdout, "[%s] file open error: %s\n", __func__, _error);
#endif
            return;
        }

        get_old_index(tLog);
    }

	McEnterMutex(&tLog->m_xFwrite);

	if (tLog->fp)
		fprintf(tLog->fp, "== %s log started at %s. ==\n\n", tLog->filePathInit, get_timestr(tLog->strTime));

	if (tLog->fp)
		fflush(tLog->fp);

	McLeaveMutex(&tLog->m_xFwrite);

    return;
}


void get_old_index(TLogInfo* tLog)
{
    FILE* fp;
    char  file_path[256];

    while(1)
    {
        memset(file_path, 0x00, sizeof(file_path));
        sprintf(file_path, "%s.%d", tLog->filePath, tLog->fileBackupSeq);
        if(fp = fopen(file_path, "r")) {
            fclose(fp);
            tLog->fileBackupSeq++;
        } else {
            break;
        }
    }

    return;
}


char* get_datestr(char* strDate)
{
#ifdef WIN32
    SYSTEMTIME      st;

    GetLocalTime(&st);

    memset(strDate, 0x00, 9);
    sprintf(strDate, "%04d%02d%02d", st.wYear, st.wMonth, st.wDay);
#else
    time_t*         secs;
    struct timeval  tval;
    struct tm       tmval;

    gettimeofday(&tval, NULL);
    secs = &tval.tv_sec;
    localtime_r(secs, &tmval);

    memset(strDate, 0x00, 9);
    sprintf(strDate, "%04d%02d%02d",
                  tmval.tm_year+1900, tmval.tm_mon+1, tmval.tm_mday);
#endif

    return strDate;
}


char* get_timestr(char* strTime)
{
#ifdef WIN32
	SYSTEMTIME		st;

	GetLocalTime(&st);

    memset(strTime, 0x00, 11);
    sprintf(strTime, "%02d%02d%02d.%03d", st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
#else
    time_t*         secs;
    struct timeval  tval;
    struct tm       tmval;

    gettimeofday(&tval, NULL);
    secs = &tval.tv_sec;
    localtime_r(secs, &tmval);

    memset(strTime, 0x00, 11);
    sprintf(strTime, "%02d%02d%02d.%03d",
                     tmval.tm_hour, tmval.tm_min, tmval.tm_sec, tval.tv_usec/1000);
#endif

    return strTime;
}

#endif // _WIN32
#endif