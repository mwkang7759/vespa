
#include "SystemAPI.h"
#include "TraceAPI.h"
#include "cuda_runtime_api.h"
#include "driver_types.h"

//#pragma comment(lib, "cudart.lib")

void FrSleep(DWORD32 dwMilliseconds) {
#ifdef	WIN32
	Sleep(dwMilliseconds);
#endif

#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_ANDROID) || defined(SUPPORT_IOS)
	struct timespec	tp;
	tp.tv_sec = dwMilliseconds / 1000;
	tp.tv_nsec = (dwMilliseconds % 1000) * 1000000L;
	nanosleep(&tp, NULL);
#endif
}

DWORD32 FrGetTickCount() {
#ifdef	WIN32
	return	GetTickCount();
#endif

#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_ANDROID) || defined(SUPPORT_IOS)
	//INT32				iRet;
	struct timeval	tp;

	gettimeofday(&tp, NULL);
	return	(tp.tv_sec * 1000 + tp.tv_usec / 1000);
#endif

}


INT64 FrGetQueryFreq()
{
#ifdef WIN32
	LARGE_INTEGER freq;
	QueryPerformanceFrequency(&freq);
	return freq.QuadPart;
#endif

	return 0;
}

INT64 FrGetQueryCounter()
{
#ifdef WIN32
	LARGE_INTEGER counter;
	QueryPerformanceCounter(&counter);
	return counter.QuadPart;
#endif

	return 0;
}


DWORD32 RAND() {
#ifdef	WIN32
	srand(GetTickCount());
	return	(10000000 + (DWORD32)((rand()*100000000.0)/(MAX_DWORD+1.0)));
#else
	struct timeval now;

	gettimeofday(&now, 0);
	srand(now.tv_usec);

	return	(10000000 + (DWORD32)((rand()*100000000.0)/(MAX_DWORD+1.0)));
#endif
}


DWORD32 McGetPid() {
#ifdef WIN32
	return _getpid();
#elif defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_ANDROID) || defined(SUPPORT_IOS)
	return getpid();
#endif

	return 0;
}


DWORD32 McGetSubtract(DWORD32 dwBig, DWORD32 dwSmall) {
	DWORD32 dwRet;

	if (dwBig >= dwSmall) {
		dwRet = dwBig - dwSmall;
	}
	else // wrap around
	{
		dwRet = 0xFFFFFFFF - dwSmall;
		dwRet += dwBig;
	}

	return dwRet;
}


BOOL FrSysInit()
{
	BOOL bRet = TRUE;
	
	struct cudaDeviceProp  prop;

	int count;
	cudaGetDeviceCount(&count);

	for (int i = 0; i < count; i++) {
		cudaGetDeviceProperties(&prop, i);
		LOG_I("   --- General Information for device %d ---", i);
		LOG_I("Name:  %s", prop.name);
		LOG_I("Compute capability:  %d.%d", prop.major, prop.minor);
		LOG_I("Clock rate:  %d", prop.clockRate);
		LOG_I("Device copy overlap:  ");
		if (prop.deviceOverlap)
			LOG_I("Enabled");
		else
			LOG_I("Disabled");
		LOG_I("Kernel execution timeout :  ");
		if (prop.kernelExecTimeoutEnabled)
			LOG_I("Enabled");
		else
			LOG_I("Disabled");
		LOG_I("\n");

		LOG_I("   --- Memory Information for device %d ---", i);
		LOG_I("Total global mem:  %ud", prop.totalGlobalMem);
		LOG_I("Total constant Mem:  %ld", prop.totalConstMem);
		LOG_I("Max mem pitch:  %ld", prop.memPitch);
		LOG_I("Texture Alignment:  %ld", prop.textureAlignment);
		LOG_I("\n");

		LOG_I("   --- MP Information for device %d ---", i);
		LOG_I("Multiprocessor count:  %d", prop.multiProcessorCount);
		LOG_I("Shared mem per mp:  %ld", prop.sharedMemPerBlock);
		LOG_I("Registers per mp:  %d", prop.regsPerBlock);
		LOG_I("Threads in warp:  %d", prop.warpSize);
		LOG_I("Max threads per block:  %d", prop.maxThreadsPerBlock);
		LOG_I("Max thread dimensions:  (%d, %d, %d)", prop.maxThreadsDim[0], prop.maxThreadsDim[1], prop.maxThreadsDim[2]);
		LOG_I("Max grid dimensions:  (%d, %d, %d)", prop.maxGridSize[0], prop.maxGridSize[1], prop.maxGridSize[2]);
		LOG_I("\n");
	}

	
	/*if (g_bNetworkInit == FALSE) {
		g_bNetworkInit = TRUE;
		McSocketInitNetwork();
	}*/


	return bRet;
}

BOOL FrSysClose()
{
	BOOL bRet = TRUE;

	

	/*if (g_bNetworkInit) {
		g_bNetworkInit = FALSE;
		McSocketCloseNetwork();
	}*/

	return bRet;
}



#if 0

BOOL McSystemCOMInit()
{
	//Context_Init(NULL, NULL, 0, NULL);
#if defined(WIN32) && !defined(_WIN32_WCE)
	// Initialize COM

	// Multi Thread할때 확인후 사용
	//HRESULT hr = CoInitializeEx(NULL, COINIT_MULTITHREADED);
	//if (FAILED(hr))
	HRESULT	hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);

//	if (FAILED(hr))
//		return FALSE;
#endif

	return TRUE;
}

void McSystemCOMClose()
{
	//Context_Done();
#if defined(WIN32) && !defined(_WIN32_WCE)
	// COM cleanup
	CoUninitialize( );
#endif

}


void McSleep(DWORD32 dwMilliseconds)
{
#ifdef	WIN32
	Sleep(dwMilliseconds);
#endif

#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_ANDROID) || defined(SUPPORT_IOS)
	struct timespec	tp;
	tp.tv_sec = dwMilliseconds / 1000;
	tp.tv_nsec = (dwMilliseconds % 1000) * 1000000L;
	nanosleep(&tp, NULL);
#endif
}

void McMicroSleep(DWORD32 dwMicroSeconds)
{
#ifdef	WIN32
	Sleep(dwMicroSeconds/1000);
#endif

#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_ANDROID) || defined(SUPPORT_IOS)
	struct timespec	tp;
	tp.tv_sec = dwMicroSeconds / 1000000;
	tp.tv_nsec = (dwMicroSeconds % 1000000) * 1000L;
	nanosleep(&tp, NULL);
#endif
}



INT64 McGetMicroTickCount()
{
#ifdef    WIN32
    return    GetTickCount() * 1000;
#endif

#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_ANDROID) || defined(SUPPORT_IOS)
    INT32                iRet;
    struct timeval    tp;

    iRet = gettimeofday(&tp, NULL);
    return    (tp.tv_sec * 1000000 + tp.tv_usec);
#endif

}

void McGetTime(WORD* pwHour, WORD* pwMin, WORD* pwSec, WORD *pwMil)
{
#ifdef	WIN32
	SYSTEMTIME Time;

	GetLocalTime(&Time);
	*pwHour	= Time.wHour;
	*pwMin	= Time.wMinute;
	*pwSec	= Time.wSecond;
	*pwMil	= Time.wMilliseconds;
#endif
#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_ANDROID) || defined(SUPPORT_IOS)

#endif
}

void McGetTimeByString(char *szTime)
{
#ifdef	WIN32
	SYSTEMTIME Time;

	GetLocalTime(&Time);
	srand((unsigned int)time(NULL));
	sprintf(szTime, "%04d%02d%02d_%02d%02d%02d%03d_%d",
		Time.wYear, Time.wMonth, Time.wDay, Time.wHour, Time.wMinute, Time.wSecond, Time.wMilliseconds, rand()%10000+0);

	//*pwHour	= Time.wHour;
	//*pwMin	= Time.wMinute;
	//*pwSec	= Time.wSecond;
	//*pwMil	= Time.wMilliseconds;
#endif
#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_ANDROID) || defined(SUPPORT_IOS)
    struct timeval now;
	struct tm *lt;

    gettimeofday(&now , NULL);
    lt = (struct tm*)localtime(&(now.tv_sec));

    sprintf(szTime, "%04d%02d%02d_%02d%02d%02d%06d_%d" ,
	    lt->tm_year + 1900 , lt->tm_mon + 1 , lt->tm_mday, lt->tm_hour, lt->tm_min, lt->tm_sec, (int)now.tv_usec, (int)getpid());

#endif
}

void McGetDateByString(char *szDate)
{
#ifdef	WIN32
	SYSTEMTIME Time;

	GetLocalTime(&Time);
	srand((unsigned int)time(NULL));
	sprintf(szDate, "%04d%02d%02d", Time.wYear, Time.wMonth, Time.wDay);
#endif
#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_ANDROID) || defined(SUPPORT_IOS)
    struct timeval now;
	struct tm *lt;

    gettimeofday(&now , NULL);
    lt = (struct tm*)localtime(&(now.tv_sec));

    sprintf(szDate, "%04d%02d%02d", lt->tm_year + 1900 , lt->tm_mon + 1 , lt->tm_mday);

#endif
}

void McGetDateFromTime(DWORD32 dwTime, WORD* pwHour, WORD* pwMin, WORD* pwSec, WORD *pwMil)
{
#ifdef	WIN32
	*pwHour = (WORD)(dwTime/60/60/1000);
	*pwMin  = (WORD)((dwTime/60/1000) % 60);
	*pwSec  = (WORD)((dwTime/1000) % 60);
	*pwMil  = (WORD)(dwTime - ((*pwHour * 3600000) + (*pwMin * 60000) + (*pwSec * 1000)));
#endif
}


INT64 McGetMicroSubtract(INT64 n64Big, INT64 n64Small)
{
    INT64 n64Ret;

    if (n64Big >= n64Small)
    {
        n64Ret = n64Big - n64Small;
    }
    else // wrap around
    {
        n64Ret = 0xFFFFFFFFFFFFFFFF - n64Small;
        n64Ret += n64Big;
    }

    return n64Ret;
}

DWORD32 McGetHostName(char* pchHostName, DWORD32 dwBufLeng)
{
	INT32	nRet= FR_OK;

	char*	pchTemp = NULL;

	if(pchHostName == NULL || dwBufLeng == 0)
	{
		LOG_I("[System]	Invalid Host Name Parameter or Buffer Length Error!");
		return COMMON_ERR_MEM;
	}

	pchTemp = (char*)MALLOC(sizeof(char) * dwBufLeng);
	if(pchTemp == NULL)
		return COMMON_ERR_MEM;

	nRet = gethostname(pchTemp, dwBufLeng);
	if(nRet != 0)
	{
		LOG_I("[System]	Failed to get Local Host Name!");
#if WIN32
		nRet = WSAGetLastError();
		switch(nRet)
		{
		case WSAEFAULT:
			LOG_I("[System]	The name parameter is not a valid part of the user address space, or the buffer size specified by namelen parameter is too small to hold the complete host name.");
			break;
		case WSANOTINITIALISED:
			LOG_I("[System]	A successful WSAStartup call must occur before using this function.");
			break;
		case WSAENETDOWN:
			LOG_I("[System]	The network subsystem has failed.");
			break;
		case WSAEINPROGRESS:
			LOG_I("[System]	A blocking Windows Sockets 1.1 call is in progress, or the service provider is still processing a callback function.");
			break;
		}
#endif
		nRet = COMMON_ERR_SYSTEM;
	}

	if(pchTemp)
		strcpy(pchHostName, pchTemp);

	FREE(pchTemp);

	return FR_OK;
}


DWORD32 McGetProcessNum()
{
#ifdef	WIN32
	SYSTEM_INFO		Info;
	GetSystemInfo(&Info);
	return	Info.dwNumberOfProcessors;
#endif

#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_ANDROID) || defined(SUPPORT_IOS)
	return	1;
#endif
}

BOOL McGetDate(char* pchInFormat, char* pchOutFormat, INT32 nLengthOfOutFormat)
{
#ifdef WIN32
	INT32			nRet = FR_OK;

	if(pchInFormat == NULL || pchOutFormat == NULL)
		return COMMON_ERR_NODATA;

	nRet = GetDateFormatA(LOCALE_SYSTEM_DEFAULT, LOCALE_USE_CP_ACP, NULL, pchInFormat, pchOutFormat, nLengthOfOutFormat);
	switch(GetLastError())
	{
	case ERROR_INSUFFICIENT_BUFFER:
		LOG_I("[System]	Date Format Buffer is insufficient...(%d/%d)", nRet, nLengthOfOutFormat);
		break;
	case ERROR_INVALID_FLAGS:
		LOG_I("[System]	Date Format Flags are invalid.");
		break;
	case ERROR_INVALID_PARAMETER:
		if(!nRet)
			LOG_I("[System]	Date Parameters are invalid.");
		break;
	default:
		if(!nLengthOfOutFormat)
			LOG_I("[System]	Date Format needs %d length.", nRet);
		else
			nRet = FR_OK;
		break;
	}
	return nRet;
#endif

	return FALSE;
}

BOOL McCheckExecDup(char* pchBinId)
{
#ifdef WIN32
	if(pchBinId == NULL)
		return FALSE;

	CreateMutexA(NULL, TRUE, pchBinId);
	if(GetLastError() == ERROR_ALREADY_EXISTS)
		return FALSE;
#endif//WIN32
	return TRUE;
}

BOOL McGetFileVersion(char* pchBinName, char* pchVerInfo)
{
#if defined(WIN32) && !defined(_WIN32_WCE)

#pragma comment(lib, "version.lib")

	DWORD32	dwVerLeng;
	DWORD32	dwHandle;
	char	szBuffer[1024]	= "";
	char*	pstrRrcPos		= NULL;

	if(pchBinName == NULL || pchVerInfo == NULL)
		return FALSE;

	dwVerLeng = GetFileVersionInfoSizeA(pchBinName, &dwHandle);
	if(dwVerLeng)
	{
		if(GetFileVersionInfoA(pchBinName, dwHandle, dwVerLeng, (char*)szBuffer))
		{
			VerQueryValue(szBuffer, TEXT("\\StringFileInfo\\041204b0\\FileVersion"), ((void**)&pstrRrcPos), &dwVerLeng);
			strncpy((CHAR*)pchVerInfo, (CHAR*)pstrRrcPos, dwVerLeng);

			return TRUE;
		}
	}
	return FALSE;

#endif//WIN32

	return TRUE;
}

#if defined(WIN32) && !defined(_WIN32_WCE)
INT32 McGetDiffSecond(SYSTEMTIME sTm1, SYSTEMTIME sTm2)
{
	INT32		lRtn;
	FILETIME	fTm1, fTm2;
	UINT64		*ullVal1, *ullVal2;
	UINT64		ullDif;

	SystemTimeToFileTime(&sTm1, &fTm1);
	SystemTimeToFileTime(&sTm2, &fTm2);

	ullVal1 = (UINT64 *)&fTm1;
	ullVal2 = (UINT64 *)&fTm2;

	if(*ullVal1 > *ullVal2)
	{
		ullDif = *ullVal1 - *ullVal2;
	}
	else
	{
		ullDif = *ullVal2 - *ullVal1;
	}

	//nRtn = long(ullDif / 10000000 / 60 / 60); // Diff Hour
	lRtn = (INT32)(ullDif / 10000000);

	return lRtn;
}
#endif//WIN32


INT64 McGetQueryFreq()
{
#ifdef WIN32
	LARGE_INTEGER freq;
	QueryPerformanceFrequency(&freq);
	return freq.QuadPart;
#endif
    
    return 0;
}

INT64 McGetQueryCounter()
{
#ifdef WIN32
	LARGE_INTEGER counter;
	QueryPerformanceCounter(&counter);
	return counter.QuadPart;
#endif
    
    return 0;
}

DWORD32 McGetQueryDuringTime(INT64 nFreq, INT64 nEnd, INT64 nBegin)
{
#ifdef WIN32
	INT64 elapsed;
	double duringtime;

	elapsed = nEnd- nBegin; 
	duringtime = (double)elapsed / (double)nFreq;
 
	duringtime *=1000;  //ms로 변환
	duringtime = floor(duringtime + 0.5);

	return (DWORD32)duringtime;
	
#endif
    
    return 0;
}

INT64 McGetQueryDiffTime(INT64 nFreq, INT64 nEnd, INT64 nBegin)
{
#ifdef WIN32
	INT64 elapsed;
	double duringtime;

	elapsed = nEnd - nBegin;
	duringtime = (double)elapsed / (double)nFreq;

	duringtime *= 1000000;  //micro sec 로 변환
	duringtime = floor(duringtime + 0.5);

	return (INT64)duringtime;

#endif

	return 0;
}


#endif
