/*****************************************************************************
*                                                                            *
*                            System Library									 *
*                                                                            *
*   Copyright (c) 2004 by Mcubeworks, Incoporated. All Rights Reserved.      *
******************************************************************************

    File Name       : SystemHeader.h
    Author(s)       : Kim, Kun-Tae
    Created         : 11 Jan 2005

    Description     : File API
    Notes           :

==============================================================================
                      Modification History (Reverse Order)
==============================================================================
    Date        Author      Location (variables) / Description
------------------------------------------------------------------------------

------------------------------------------------------------------------------*/

#ifndef _SYSTEMHEADER_H_
#define _SYSTEMHEADER_H_

/****************************************/
/*		Default Header Files			*/
/****************************************/

#include "SystemAPI.h"
#include "TraceAPI.h"
#include "UtilAPI.h"

#endif	// _SYSTEMHEADER_H_
