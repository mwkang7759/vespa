
#include "SystemAPI.h"
#include "TraceAPI.h"

/************************/
/*		Event			*/
/************************/
EVENT_HANDLE FrCreateEvent(char* strName)
{
	EVENT_HANDLE	hEvent;
#ifdef	WIN32
	hEvent = (DWORD32)CreateEventA(
					NULL,						/* SECURITY_ATTRIBUTES	*/
					FALSE,						/* manual reset type */
					FALSE,						/* initial state */
					strName						/* object name */
					);
	if (!hEvent)
	{
		TRACEX(FDR_LOG_WARN, "[System]	Error in McCreateEvent(%s) error=%d", strName, GetLastError());
		return	FALSE;
	}
#endif

#if defined(Linux) || defined(SUPPORT_ANDROID)
	hEvent = (EVENT_HANDLE)MALLOC(sizeof(pthread_cond_t));

	pthread_cond_init((pthread_cond_t *)hEvent, NULL);
#endif

	return	hEvent;
}

BOOL FrDeleteEvent(EVENT_HANDLE hEvent)
{
#ifdef	WIN32
	BOOL	bRet;

	bRet = CloseHandle((HANDLE)hEvent);					/* handle to object */
	if (!bRet)
	{
		TRACEX(FDR_LOG_WARN, "[System]	Error in McDeleteEvent() error=%d", GetLastError());
		return	FALSE;
	}
#endif

#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_ANDROID) || defined(SUPPORT_IOS)
	pthread_cond_destroy((pthread_cond_t *)hEvent);

	FREE((pthread_cond_t *)hEvent);
#endif

	return	TRUE;
}

BOOL FrResetEvent(EVENT_HANDLE hEvent)
{
#ifdef	WIN32
	BOOL	bRet;

	bRet = ResetEvent((HANDLE)hEvent);					/* handle to event */
	if (!bRet)
	{
		TRACEX(FDR_LOG_WARN, "[System]	Error in McResetEvent() error=%d", GetLastError());
		return	FALSE;
	}
#endif

#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_ANDROID) || defined(SUPPORT_IOS)
	pthread_mutex_t	hMutex = PTHREAD_MUTEX_INITIALIZER;
	struct timespec	timeout;
	INT32				nRetVal;

	timeout.tv_sec = 0;
	timeout.tv_nsec = 0;

	nRetVal = pthread_cond_timedwait( (pthread_cond_t *)hEvent, &hMutex, &timeout);
	if (nRetVal && nRetVal != ETIMEDOUT)
	{
		LOG_W("[System]	Error in McResetEvent() 0x%x", nRetVal);
		return	FALSE;
	}
#endif

	return	TRUE;
}

BOOL FrSendEvent(EVENT_HANDLE hEvent)
{
#ifdef	WIN32
	BOOL	bRet;

	bRet = SetEvent((HANDLE)hEvent);					/* handle to event */
	if (!bRet)
	{
		TRACEX(FDR_LOG_WARN, "[System]	Error in McSendEvent() error=%d", GetLastError());
		return	FALSE;
	}
#endif

#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_ANDROID) || defined(SUPPORT_IOS)
	INT32		nRetVal;

	nRetVal = pthread_cond_signal( (pthread_cond_t *)hEvent );
	if (nRetVal)
	{
		LOG_W("[System]	Error in McSendEvent() ");
		return	FALSE;
	}
#endif

	return	TRUE;
}

BOOL FrWaitEvent(EVENT_HANDLE hEvent, DWORD32 dwMSec)
{
#ifdef	WIN32
	DWORD32	dwRet;

	dwRet = WaitForSingleObject(
					(HANDLE)hEvent,						/* handle to object */
					dwMSec						/* time-out interval */
					);
	if (dwRet == WAIT_OBJECT_0)
	{
	}
	else if (dwRet == WAIT_ABANDONED)
	{
		TRACEX(FDR_LOG_WARN, "[System]	Error in McWaitEvent() : WAIT_ABANDONED");
		return	FALSE;
	}
	else if (dwRet == WAIT_TIMEOUT)
	{
		TRACEX(FDR_LOG_WARN, "[System]	Error in McWaitEvent() : WAIT_TIMEOUT");
		return	FALSE;
	}
	else
	{
		TRACEX(FDR_LOG_WARN, "[System]	Error in McWaitEvent() error=%d", GetLastError());
		return	FALSE;
	}
#endif

#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_ANDROID) || defined(SUPPORT_IOS)
	pthread_mutex_t	hMutex = PTHREAD_MUTEX_INITIALIZER;
	struct timeval	now;
	struct timespec	timeout;
	INT32				nRetVal;

	gettimeofday(&now, NULL);

	timeout.tv_sec = now.tv_sec + ( dwMSec / 1000 );
	timeout.tv_nsec = ( now.tv_usec + ( dwMSec % 1000 ) * 1000 ) * 1000;

	nRetVal = pthread_cond_timedwait( (pthread_cond_t *)hEvent, &hMutex, &timeout);
	if (nRetVal)
	{
		LOG_W("[System]	Error in McWaitEvent() 0x%x", nRetVal);
		return	FALSE;
	}
#endif

	return	TRUE;
}

/************************/
/*		Semaphore		*/
/************************/
BOOL FrCreateSema(PSEMA_HANDLE phSema, UINT32 lInitCount, UINT32 lMaxCount, char* strName)
{
#ifdef	WIN32
	*phSema = (SEMA_HANDLE)CreateSemaphoreA(
					NULL,						/* SECURITY_ATTRIBUTES	*/
					(LONG)lInitCount,			/* initial count */
					(LONG)lMaxCount,			/* maximum count */
					strName						/* object name */
					);
	if (!*phSema) {
		TRACEX(FDR_LOG_WARN, "[System]	Error in McCreateSema(%s) error=%d", strName, GetLastError());
		return	FALSE;
	}
#endif

#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_ANDROID) || defined(SUPPORT_IOS)
	
	if (sem_init(phSema, 0, lMaxCount) != 0)
		return FALSE;
#endif

	return	TRUE;
}

BOOL FrDeleteSema(PSEMA_HANDLE phSema)
{
#ifdef	WIN32
	BOOL	bRet;

	bRet = CloseHandle((HANDLE)*phSema);					/* handle to object */
	if (!bRet) {
		TRACEX(FDR_LOG_WARN, "[System]	Error in McDeleteSema() error=%d", GetLastError());
		return	FALSE;
	}
#endif
#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_ANDROID) || defined(SUPPORT_IOS)
	if (sem_destroy(phSema) != 0)
		return FALSE;
#endif
	return	TRUE;
}

BOOL Fr_P_Sema(PSEMA_HANDLE phSema, DWORD32 dwMSec)
{
#ifdef	WIN32
	DWORD32	dwRet;

	dwRet = WaitForSingleObject(
					(HANDLE)*phSema,				/* handle to object */
					dwMSec						/* time-out interval */
					);
	if (dwRet == WAIT_OBJECT_0) {
	} else if (dwRet == WAIT_ABANDONED) {
		TRACEX(FDR_LOG_WARN, "[System]	Error in P_SRSema() : WAIT_ABANDONED");
		return	FALSE;
	} else if (dwRet == WAIT_TIMEOUT) {
		TRACEX(FDR_LOG_WARN, "[System]	Error in P_SRSema() : WAIT_TIMEOUT");
		return	FALSE;
	} else {
		TRACEX(FDR_LOG_WARN, "[System]	Error in P_SRSema() error=%d", GetLastError());
		return	FALSE;
	}
#endif
#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_IOS)
	if (sem_wait(phSema) != 0)
		return FALSE;
#endif
	return	TRUE;
}

BOOL Fr_V_Sema(PSEMA_HANDLE phSema)
{
#ifdef	WIN32
	BOOL	bRet;

	bRet = ReleaseSemaphore(
					(HANDLE)*phSema,				/* handle to semaphore */
					1,							/* count increment amount */
					NULL						/* previous count (LPLONG) */
					);
	if (!bRet) {
		TRACEX(FDR_LOG_WARN, "[System]	Error in V_SRSema() error=%d", GetLastError());
		return	FALSE;
	}
#endif
#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_IOS)
	if (sem_post(phSema) != 0)
		return FALSE;
#endif
	return	TRUE;
}

/************************/
/*		Mutex			*/
/************************/
BOOL FrCreateMutex(PMUTEX_HANDLE phMutex)
{
#ifdef	WIN32
	InitializeCriticalSection(
				(LPCRITICAL_SECTION)phMutex		/* critical section */
				);
#endif

#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_IOS)
	*phMutex = (MUTEX_HANDLE)MALLOC(sizeof(pthread_mutex_t));
//	memset(*phMutex, 0, sizeof(pthread_mutex_t));
	pthread_mutex_init((pthread_mutex_t*)*phMutex, NULL);
#endif

	return	TRUE;
}

BOOL FrDeleteMutex(PMUTEX_HANDLE phMutex)
{
#ifdef	WIN32
	DeleteCriticalSection(
				(LPCRITICAL_SECTION)phMutex		/* critical section */
				);
#endif

#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_IOS)
	if(phMutex)	//hjchoi
	{
		pthread_mutex_destroy((pthread_mutex_t *)*phMutex);
		FREE((void *)*phMutex);
	}
#endif

	return	TRUE;
}

BOOL FrEnterMutex(PMUTEX_HANDLE phMutex)
{
#ifdef	WIN32
	EnterCriticalSection(
			(LPCRITICAL_SECTION)phMutex		/* critical section */
			);
#endif

#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_IOS)
	pthread_mutex_lock((pthread_mutex_t *)*phMutex);
#endif

	return	TRUE;
}

BOOL FrLeaveMutex(PMUTEX_HANDLE phMutex)
{
#ifdef	WIN32
	LeaveCriticalSection(
			(LPCRITICAL_SECTION)phMutex		/* critical section */
			);
#endif

#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_IOS)
	pthread_mutex_unlock((pthread_mutex_t *)*phMutex);
#endif

	return	TRUE;
}
