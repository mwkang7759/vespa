#pragma once

#include <chrono>
#include <thread>
#include <vector>
#include <string>
#include <filesystem>
#include <iostream>
#include <fstream>

#include "cuda_runtime_api.h"
#include "driver_types.h"

#if !defined(uchar) && !defined(_uchar_defined)
#define _uchar_defined
typedef unsigned char				uchar;
#endif


#if !defined(uint32) && !defined(_uint32_defined)
#define _uint32_defined
typedef unsigned int				uint32;
#endif

#if !defined(int64) && !defined(_int64_defined)
#define _int64_defined
typedef long long				int64;
#endif

#if !defined(uint64) && !defined(_uint64_defined)
#define _uint64_defined
typedef unsigned long long				uint64;
#endif


#if _DEBUG
#include<crtdbg.h>
#define malloc(s) _malloc_dbg(s, _NORMAL_BLOCK, __FILE__, __LINE__)
#endif

namespace fr {
	void Sleep(int msec);
	bool GetCudaDeviceInfo();
	unsigned long long GetSizeOfFile(std::string& name);
	void ReadFile(std::string& name);
	void WriteFile(std::string& name);
	void OpenFile(std::string& name, int mode);
	void CloseFile(std::string& name);

	class File {
	public:
		const static int	READ_MODE				= 1;
		const static int 	WRITE_MODE				= 2;

	
	public:
		int Open(std::string& name, int mode);
		int64 Read(char* buf, uint32 size);
		void Write(char* buf, uint32 size);
		void Close();
	private:
		std::string _name;
		std::ofstream _fout;
		std::ifstream _fin;
		int _mode;
	};
}
