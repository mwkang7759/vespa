/*****************************************************************************
*                                                                            *
*                            System Library									 *
*                                                                            *
*   Copyright (c) 2004 by Mcubeworks, Incoporated. All Rights Reserved.      *
******************************************************************************

    File Name       : SystemMath.c
    Author(s)       : Kim, Kun-Tae
    Created         : 11 Jan 2005

    Description     : File API
    Notes           : 

==============================================================================
                      Modification History (Reverse Order)                    
==============================================================================
    Date        Author      Location (variables) / Description
------------------------------------------------------------------------------

------------------------------------------------------------------------------*/

#include "SystemHeader.h"
#include "SystemAPI.h"
#include "TraceAPI.h"
