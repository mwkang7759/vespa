/*****************************************************************************
*                                                                            *
*                            System Library									 *
*                                                                            *
*   Copyright (c) 2004 by Mcubeworks, Incoporated. All Rights Reserved.      *
******************************************************************************

    File Name       : SystemTask.c
    Author(s)       : Kim, Kun-Tae
    Created         : 11 Jan 2005

    Description     : File API
    Notes           :

==============================================================================
                      Modification History (Reverse Order)
==============================================================================
    Date        Author      Location (variables) / Description
------------------------------------------------------------------------------

------------------------------------------------------------------------------*/

#include "SystemHeader.h"
#include "SystemAPI.h"
#include "TraceAPI.h"

#if defined(WIN32) && !defined(_WIN32_WCE)
#include <process.h>
#endif//WIN32

TASK_HANDLE McCreateTask(
			char*				strName,
			PTASK_START_ROUTINE pStartAddress,
			void*				lpParam,
			INT32				nPriority,
			DWORD32				dwStackSize,
			DWORD32				dwCreateFlag
			)
{
	TASK_HANDLE	hTask;

#ifdef	WIN32
	DWORD32		dwID;									/* for Win98 */


#if defined(WIN32) && !defined(_WIN32_WCE)
	hTask = (DWORD32)_beginthreadex(
				NULL,									/* SECURITY_ATTRIBUTES	*/
				dwStackSize,							/* initial stack size	*/
				(LPTHREAD_START_ROUTINE)pStartAddress,	/* thread function		*/
				lpParam,								/* thread argument		*/
				dwCreateFlag,							/* creation option		*/
				&dwID									/* thread identifier	*/
				);
#else
	hTask = (DWORD32)CreateThread(
				NULL,									/* SECURITY_ATTRIBUTES	*/
				dwStackSize,							/* initial stack size	*/
				(LPTHREAD_START_ROUTINE)pStartAddress,	/* thread function		*/
				lpParam,								/* thread argument		*/
				0,										/* creation option		*/
				&dwID									/* thread identifier	*/
				);
#endif
	if (!hTask) {
		TRACEX(FDR_LOG_WARN, "[System]	Error in McCreateTask(%s) error=%d", strName, GetLastError());
		return	INVALID_TASK;
	} else {
		SetThreadPriority((HANDLE)hTask, nPriority);			/* set thread priority	*/
	}
#endif

#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_IOS)
	DWORD32			dwCurStackSize;
	pthread_attr_t	attr;
	INT32			iRet;

	pthread_attr_init(&attr);
	//pthread_attr_setstacksize(&attr, 256 * 1024);

#ifndef SUPPORT_IOS
	pthread_attr_getstacksize(&attr, (size_t *)&dwCurStackSize);
	//LOG_I("[System] current stack size %d", dwCurStackSize);
#endif
    
	if (dwStackSize)
	{
		pthread_attr_setstacksize(&attr, (size_t)dwStackSize);
		pthread_attr_getstacksize(&attr, (size_t *)&dwCurStackSize);
	}

	pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	iRet = pthread_create((pthread_t *)&hTask, &attr, pStartAddress, lpParam);
	pthread_attr_destroy(&attr);

	if (iRet)
	{
		LOG_W("[System]	Error in McCreateTask(%s) error=%d", strName, iRet);
		return	INVALID_TASK;
	}
#endif

	return	hTask;
}

void McCloseTask(TASK_HANDLE hTask)
{
#ifdef	WIN32
	if(hTask)
		CloseHandle((HANDLE)hTask);
#endif

#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_IOS)
#endif
}

DWORD32 McResumeTask(TASK_HANDLE hTask)
{
#ifdef	WIN32
	if(hTask)
		return ResumeThread((HANDLE)hTask);
#endif

#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_IOS)
	return 1;
#endif

	return 1;
}

DWORD32 McSuspendTask(TASK_HANDLE hTask)
{
#ifdef	WIN32
	if(hTask)
		return SuspendThread((HANDLE)hTask);
#endif

#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_IOS)
	return 0;
#endif

	return 0;
}

void McExitTask(void)
{
#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_IOS)
	DWORD32	dwExitCode;
	pthread_exit((void *)&dwExitCode);
#endif

#if defined(WIN32) && !defined(_WIN32_WCE)
	_endthreadex(0);
#elif defined(_WIN32_WCE)
	ExitThread(0L);
#endif
}

BOOL McDeleteTask(TASK_HANDLE hTask)
{
#ifdef WIN32
	BOOL	fRet;
#endif

#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_IOS)
	INT32		iRet;
#endif

	if(hTask == (TASK_HANDLE)NULL || hTask == INVALID_TASK)
	{
		TRACEX(FDR_LOG_WARN, "[System]	Error in McDeleteTask() : Task Handle is invalid.");
		return FALSE;
	}

#ifdef	WIN32
	fRet = TerminateThread((HANDLE)hTask, 0L);
	if (!fRet)
	{
		TRACEX(FDR_LOG_WARN, "[System]	Error in McDeleteTask() error=%d", GetLastError());
		return	FALSE;
	}
	else
	{
		DWORD32 dwExitCode;

		fRet = GetExitCodeThread((HANDLE)hTask, &dwExitCode);

		if (dwExitCode == STILL_ACTIVE)
		{
			TRACEX(FDR_LOG_WARN, "[System]	McDeleteTask : It's failed to terminate a task (0x%x)", hTask);
			return FALSE;
		}
		else
			TRACEX(FDR_LOG_INFO, "[System]	McDeleteTask : It's okay to terminate a task (0x%x)", hTask);
	}
#endif
    

#if !defined(SUPPORT_ANDROID)
#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(SUPPORT_IOS)
	iRet = pthread_cancel(hTask);
	if (iRet)
	{
		LOG_W("[System]	Error in McDeleteTask() error=%d", iRet);
		return	FALSE;
	}
#endif
#endif

#if defined(MINGW) || defined(SUPPORT_ANDROID)
	//...........
	LOG_W("[System]	Need to find a function to kill a task (NDK)");
#endif

	return	TRUE;
}
