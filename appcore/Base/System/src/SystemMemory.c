/*****************************************************************************
*                                                                            *
*                            System Library									 *
*                                                                            *
*   Copyright (c) 2004 by Mcubeworks, Incoporated. All Rights Reserved.      *
******************************************************************************

    File Name       : SystemMemory.c
    Author(s)       : Kim, Kun-Tae
    Created         : 11 Jan 2005

    Description     : File API
    Notes           : 

==============================================================================
                      Modification History (Reverse Order)                    
==============================================================================
    Date        Author      Location (variables) / Description
------------------------------------------------------------------------------

------------------------------------------------------------------------------*/

#include "SystemHeader.h"
#include "SystemAPI.h"
#include "TraceAPI.h"

//#define		MEM_TRACE_OUT	

void* MALLOC(DWORD32 dwSize)
{	
	void*	ptr;

	if (!dwSize)
		return	NULL;
#ifdef	WIN32
	ptr = malloc(dwSize);
#endif

#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_IOS)
	ptr = malloc(dwSize);
#endif
#ifdef	MEM_TRACE_OUT
	TRACE("[MEM] Alloc Malloc  size %10d ptr 0x%08x", dwSize, ptr);
#endif
	return ptr;
}

void* MALLOCZ(DWORD32 dwSize)
{
	void*	ptr;

	if (!dwSize)
		return	NULL;
#ifdef	WIN32
	ptr = malloc(dwSize);

	if (ptr)
		memset(ptr, 0, dwSize);	
#endif

#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_IOS)
	ptr = malloc(dwSize);

	if (ptr)
		memset(ptr, 0, dwSize);	
#endif
#ifdef	MEM_TRACE_OUT
	TRACE("[MEM] Alloc Mallocz size %10d ptr 0x%08x", dwSize, ptr);
#endif
	return	ptr;
}

void* REALLOC(void* pMem, DWORD32 dwSize)
{
	void*	ptr;

#ifdef	MEM_TRACE_OUT
	if (pMem)
		TRACE("[MEM] Free ptr 0x%08x", pMem);
#endif

	if (!dwSize)
		return	pMem;
#ifdef	WIN32
	ptr = realloc(pMem, dwSize);
#endif

#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_IOS)
	ptr	= realloc(pMem, dwSize);
#endif
#ifdef	MEM_TRACE_OUT
	TRACE("[MEM] Alloc Realloc size %10d ptr 0x%08x", dwSize, ptr);
#endif
	return ptr;
}

void* CALLOC(DWORD32 dwNum, DWORD32 dwSize)
{
	void*	ptr;

	if (!dwSize)
		return	NULL;
#ifdef	WIN32
	ptr	= calloc(dwNum, dwSize);
#endif

#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_IOS)
	ptr	= calloc(dwNum, dwSize);
#endif
#ifdef	MEM_TRACE_OUT
	TRACE("[MEM] Alloc Calloc  size %10d ptr 0x%08x", dwSize, ptr);
#endif
	return ptr;
}

void FREE(void* pMem)
{
#ifdef	WIN32
	if (pMem)
		free(pMem);
#endif

#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_IOS)
	if (pMem)
		free(pMem);
#endif
#ifdef	MEM_TRACE_OUT
	if (pMem)
		TRACE("[MEM] Free ptr 0x%08x", pMem);
#endif
}
