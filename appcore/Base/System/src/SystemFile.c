
#ifdef _WIN32
#include <conio.h>
#include <io.h>
#include <direct.h>
#include <fcntl.h>
#else
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <dirent.h>
#include <fcntl.h>
#endif

#include <stdio.h>
#include <time.h>
#include <string.h>
#include <errno.h>

#include "mediaerror.h"
#include "UtilAPI.h"
#include "TraceAPI.h"


#ifdef WIN32

#define	DTB_CREAT	_O_CREAT
#define	DTB_TRUNC	_O_TRUNC
#define	DTB_APPEND	_O_APPEND
#define DTB_RDONLY	_O_RDONLY | _O_BINARY
#define	DTB_WRONLY	_O_WRONLY | _O_BINARY
#define DTB_RDWR	_O_RDWR | _O_BINARY
#define	DTB_IWRITE	_S_IWRITE

#define	open		_open
#define	close		_close
#define lseek64		_lseeki64
#define read		_read
#define write		_write
#else

#define	DTB_CREAT	O_CREAT
#define	DTB_TRUNC	O_TRUNC
#define	DTB_APPEND	O_APPEND
#define DTB_RDONLY	O_RDONLY
#define	DTB_WRONLY	O_WRONLY
#define DTB_RDWR	O_RDWR
#define	DTB_IWRITE	S_IRWXU | S_IRWXO

#endif


#ifdef WIN32
BOOL OpenFileWindow(FILE_HANDLE	hFile, const TCHAR* strFilename, INT32 eMode) {
	if (eMode & FILE_CREATE) {
		if ((eMode & 0x3) == FILE_READ) {
			TRACEX(FDR_LOG_WARN, "[System]	Error in McOpenFile : Create with Read Mode");
			FREE(hFile->m_pReadBuffer);
			FREE(hFile);
			return	FALSE;
		}
		else if ((eMode & 0x3) == FILE_WRITE) {
			hFile->m_pFile = fopen(strFilename, "w");
		}
		else if ((eMode & 0x3) == FILE_READ_WRITE) {
			hFile->m_pFile = fopen(strFilename, "w");
		}
		else {
			hFile->m_pFile = fopen(strFilename, "w");
		}
	}
	else {
		if ((eMode & 0x3) == FILE_READ) {
			hFile->m_pFile = fopen(strFilename, "r");
		}
		else if ((eMode & 0x3) == FILE_WRITE) {
			hFile->m_pFile = fopen(strFilename, "r");
		}
		else if ((eMode & 0x3) == FILE_READ_WRITE) {
			hFile->m_pFile = fopen(strFilename, "r");
		} else {
			TRACEX(FDR_LOG_WARN, "[System]	Error in McOpenFile : No Mode");
			FREE(hFile->m_pReadBuffer);
			FREE(hFile);
			return	FALSE;
		}
	}

	//if (hFile->m_nFd > 0) {
	//	hFile->m_pFile = (void*)(&hFile->m_nFd);	// dummy file pointer.
	//	//hFile->m_bUniOpen = TRUE;
	//}

	return TRUE;
}
#else
BOOL OpenFileLinux(FILE_HANDLE	hFile, const TCHAR* strFilename, INT32 eMode) {
	if (eMode & FILE_CREATE)
	{
		if ((eMode & 0x3) == FILE_READ)
		{
			LOG_W("[System]	Error in McOpenFile : Create with Read Mode");
			FREE(hFile->m_pReadBuffer);
			FREE(hFile);
			return	FALSE;
		}
		else if ((eMode & 0x3) == FILE_WRITE)
		{
			hFile->m_nFd = open(strFilename, DTB_CREAT | DTB_WRONLY | DTB_TRUNC, DTB_IWRITE);
		}
		else if ((eMode & 0x3) == FILE_READ_WRITE)
		{
			//hFile->m_nFd = _open(strFilename, _O_CREAT | _O_WRONLY | _O_TRUNC | _O_BINARY, S_IWRITE);		// window overwrite ok..
			hFile->m_nFd = open(strFilename, DTB_CREAT | DTB_RDWR | DTB_TRUNC, DTB_IWRITE);
		}
		else
		{
			hFile->m_nFd = open(strFilename, DTB_CREAT | DTB_WRONLY | DTB_TRUNC, DTB_IWRITE);
		}
	}
	else
	{
		if ((eMode & 0x3) == FILE_READ)
		{
			hFile->m_nFd = open(strFilename, DTB_RDONLY);
		}
		else if ((eMode & 0x3) == FILE_WRITE)
		{
			hFile->m_nFd = open(strFilename, DTB_WRONLY | DTB_APPEND, DTB_IWRITE);
		}
		else if ((eMode & 0x3) == FILE_READ_WRITE)
		{
			hFile->m_nFd = open(strFilename, DTB_RDWR | DTB_APPEND, DTB_IWRITE);
		}
		else
		{
			LOG_W("[System]	Error in McOpenFile : No Mode");
			FREE(hFile->m_pReadBuffer);
			FREE(hFile);
			return FALSE;
		}
	}

	if (hFile->m_nFd > 0)
	{
		hFile->m_pFile = (void*)(&hFile->m_nFd);	// dummy file pointer.
		//hFile->m_bUniOpen = FALSE;
	}

	return TRUE;
}
#endif

FILE_HANDLE	FrOpenFile(const TCHAR* strFilename, INT32 eMode) {
	FILE_HANDLE	hFile = NULL;
	//BOOL		bText = FALSE;

	//INT32			nRet = 0;
	//DWORD32       dwCount = 0;
	//INT64		i64Ret = 0;
	//DWORD32		dwMode = 0;
	//char        *pchEnd = NULL;
    
	hFile = (FILE_HANDLE)MALLOCZ(sizeof(FILE_STRUCT));
	if (hFile) {
		// if((eMode & FILE_TEXT) == FILE_TEXT)
		// 	bText = TRUE;

		/*hFile->m_dwReadBufferSize = 10 * 1024;
		hFile->m_dwMaxReadBufferSize = hFile->m_dwReadBufferSize;
		hFile->m_pReadBuffer = (BYTE*)MALLOCZ(hFile->m_dwMaxReadBufferSize);
		if (!hFile->m_pReadBuffer) {
			TRACEX(FDR_LOG_WARN, "[System]	Error in OpenFile : Failed to alloc MaxReadBufferSize (%d)", hFile->m_dwMaxReadBufferSize);
			FREE(hFile);
			return	NULL;
		}
		else
			TRACEX(FDR_LOG_INFO, "[System]	McOpenFile : LOCAL : ReadBufferSize (%d / %d)", hFile->m_dwReadBufferSize, hFile->m_dwMaxReadBufferSize);*/

        // open, fopen don't support utf8 in window
#ifdef _WIN32
		if (!OpenFileWindow(hFile, strFilename, eMode)) {
			FREE(hFile);
			return NULL;
		}
        
#else
		if (!OpenFileLinux(hFile, strFilename, eMode)) {
			FREE(hFile);
			return NULL;
		}
#endif

		if (!hFile->m_pFile || hFile->m_nFd < 0) {
			TRACEX(FDR_LOG_WARN, "[System]	Error in McOpenFile : File handle is null");
			TRACEX(FDR_LOG_WARN, "[System]	Error Msg : %s", strerror(errno));

#ifdef _WIN32
		if (FrAccessFile(strFilename, 0) == -1)
			TRACEX(FDR_LOG_DEBUG, "[System]	McOpenFile - There is no file in the temp folder.");
#endif
			//FREE(hFile->m_pReadBuffer);
			FREE(hFile);
			return	NULL;
		}

		hFile->m_bMem = FALSE;
	}

	return	hFile;
}

void FrCloseFile(FILE_HANDLE hFile) {
	if (hFile) {
		if (!hFile->m_bMem)	{

#ifdef WIN32
			if (hFile->m_pFile)
				fclose((FILE*)hFile->m_pFile);
#else			
			if (hFile->m_nFd)
				close(hFile->m_nFd);
#endif

			if (hFile->m_pBuffer)
				FREE(hFile->m_pBuffer);
		}

		FREE(hFile->m_pReadBuffer);
		FREE(hFile);
		hFile = NULL;
	}
}


INT32 FrReadFile(FILE_HANDLE hFile, BYTE* pBuf, DWORD32 dwSize) {
	INT32		iSize = 0;
	//DWORD32   dwCount = 0;

	if (hFile == NULL) {
		TRACEX(FDR_LOG_WARN, "[System]	McReadFile : File Handle is NULL.");
		return -1;
	}

	if (!dwSize) {
		TRACEX(FDR_LOG_WARN, "[System]	McReadFile : Invalid Reading Size(0) Error!");
		return 0;
	}

	//LOG_I("[System]	McReadFile : dwSize : %ld, PhyPos : %lld, TruePos : %lld", dwSize, hFile->m_qwPhyFilePos, hFile->m_qwTrueFilePos);

	if (hFile->m_bMem)
	{

	}
	else
	{
#ifdef WIN32
		iSize = fread(pBuf, sizeof(char), dwSize, hFile->m_pFile);
#else
#endif

	}

	return	iSize;
}

INT32 FrRead8File(FILE_HANDLE hFile) {
	BYTE	data[2];
	INT32		val, iRet;

	iRet = FrReadFile(hFile, data, 1);
	if (iRet <= 0)
		return	FR_FAIL;

	val = (BYTE)data[0];
	return	val;
}

INT32 FrReadBE16File(FILE_HANDLE hFile) {
	BYTE	data[2];
	WORD	val=0;
	INT32		iRet;

	iRet = FrReadFile(hFile, data, 2);
	if (iRet <= 0)
		return	FR_FAIL;

	val |=((WORD)data[0]<<8 );
	val |=((WORD)data[1]    );

	return	(INT32)val;
}

INT32 FrReadLE16File(FILE_HANDLE hFile) {
	BYTE	data[2];
	WORD	val=0;
	INT32		iRet;

	iRet = FrReadFile(hFile, data, 2);
	if (iRet <= 0)
		return	FR_FAIL;

	val |=((WORD)data[0]    );
	val |=((WORD)data[1]<<8 );

	return	(INT32)val;
}

INT32 FrReadBE32File(FILE_HANDLE hFile, INT32* iRet) {
	BYTE	data[4];
	DWORD32	val=0;

	*iRet = FrReadFile(hFile, data, 4);
	if (*iRet <= 0)	{
		if (!*iRet && FrTellFile(hFile) >= FrSizeOfFile(hFile))
			*iRet = COMMON_ERR_ENDOFDATA;
		else if (!*iRet)
			*iRet = COMMON_ERR_NODATA;

		return	0;
	}

	val |=((DWORD32)data[0]<<24);
	val |=((DWORD32)data[1]<<16);
	val |=((DWORD32)data[2]<<8 );
	val |=((DWORD32)data[3]    );

	return	(INT32)val;
}

INT32 FrReadLE32File(FILE_HANDLE hFile, INT32* iRet) {
	BYTE	data[4];
	DWORD32	val=0;

	*iRet = FrReadFile(hFile, data, 4);
	if (*iRet <= 0)	{
		if (!*iRet && FrTellFile(hFile) >= FrSizeOfFile(hFile))
			*iRet = COMMON_ERR_ENDOFDATA;
		else if (!*iRet)
			*iRet = COMMON_ERR_NODATA;
		return	0;
	}

	val |=((DWORD32)data[0]    );
	val |=((DWORD32)data[1]<<8 );
	val |=((DWORD32)data[2]<<16);
	val |=((DWORD32)data[3]<<24);
	return	(INT32)val;
}

INT32 FrReadBE24File(FILE_HANDLE hFile) {
	BYTE	data[3];
	DWORD32	val=0;
	INT32		iRet;

	if (hFile == NULL) {
		TRACEX(FDR_LOG_INFO, "[System]	McReadBE24File : File Handle is NULL.");
		return FR_FAIL;
	}

	iRet = FrReadFile(hFile, data, 3);
	if (iRet <= 0)
		return	FR_FAIL;
	val |=((DWORD32)data[0]<<16);
	val |=((DWORD32)data[1]<<8 );
	val |=((DWORD32)data[2]    );

	return	(INT32)val;
}

INT32 FrReadLE24File(FILE_HANDLE hFile) {
	BYTE	data[3];
	DWORD32	val=0;
	INT32		iRet;

	if (hFile == NULL) {
		TRACEX(FDR_LOG_INFO, "[System]	McReadLE24File : File Handle is NULL.");
		return FR_FAIL;
	}

	iRet = FrReadFile(hFile, data, 3);
	if (iRet <= 0)
		return	FR_FAIL;
	val |=((DWORD32)data[0]    );
	val |=((DWORD32)data[1]<<8 );
	val |=((DWORD32)data[2]<<16);
	return	(INT32)val;
}

QWORD FrReadBE64File(FILE_HANDLE hFile, INT32* iRet) {
	BYTE	data[8];
	QWORD	val=0;

	*iRet = FrReadFile(hFile, data, 8);
	if (*iRet <= 0)	{
		if (!*iRet && FrTellFile(hFile) >= FrSizeOfFile(hFile))
			*iRet = COMMON_ERR_ENDOFDATA;
		else if (!*iRet)
			*iRet = COMMON_ERR_NODATA;

		return	0;
	}

	val |=((QWORD)data[0]<<56);
	val |=((QWORD)data[1]<<48);
	val |=((QWORD)data[2]<<40);
	val |=((QWORD)data[3]<<32);
	val |=((QWORD)data[4]<<24);
	val |=((QWORD)data[5]<<16);
	val |=((QWORD)data[6]<<8 );
	val |=((QWORD)data[7]    );

	return	val;
}

QWORD FrReadLE64File(FILE_HANDLE hFile, INT32* iRet) {
	BYTE	data[8];
	QWORD	val=0;

	*iRet = FrReadFile(hFile, data, 8);
	if (*iRet <= 0)	{
		if (!*iRet && FrTellFile(hFile) >= FrSizeOfFile(hFile))
			*iRet = COMMON_ERR_ENDOFDATA;
		else if (!*iRet)
			*iRet = COMMON_ERR_NODATA;

		return	0;
	}

	val |=((QWORD)data[0]    );
	val |=((QWORD)data[1]<<8 );
	val |=((QWORD)data[2]<<16);
	val |=((QWORD)data[3]<<24);
	val |=((QWORD)data[4]<<32);
	val |=((QWORD)data[5]<<40);
	val |=((QWORD)data[6]<<48);
	val |=((QWORD)data[7]<<56);
	return	val;
}

INT32 FrWriteFile(FILE_HANDLE hFile, char* pBuf, DWORD32 dwSize) {
	INT32		iRet;

	if (hFile == NULL) {
		TRACEX(FDR_LOG_WARN, "[System]	McWriteFile : File Handle is NULL.");
		return -1;
	}

#ifdef	WIN32
	if (hFile->m_bMem) {
		
	}
	else {
		iRet = (INT32)fwrite(pBuf, 1, dwSize, (FILE*)hFile->m_pFile);
		fflush((FILE*)hFile->m_pFile);
	}
#endif	// WIN32

#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_IOS)
	if (hFile->m_bMem) {
		memcpy(hFile->m_pPos, pBuf, dwSize);
		hFile->m_pPos+= dwSize;
		iRet = dwSize;
	}
	else
	{
		//iRet = fwrite(pBuf, 1, dwSize, (FILE*)hFile->m_pFile);
		//fflush((FILE*)hFile->m_pFile);
		iRet = write(hFile->m_nFd, pBuf, dwSize);
#ifndef	_NORMAL_FILE_READ_
		hFile->m_qwTrueFilePos += iRet;
#endif
	}
#endif	// Linux

	return	iRet;
}



INT32 FrSeekFile(FILE_HANDLE hFile, INT64 iOffSet, INT32 iFromWhere) {
	INT32		iRet = 0;
	
	/*if (hFile == NULL) {
		TRACEX(FDR_LOG_WARN, "[System]	McSeekFile : File Handle is NULL.");
		return FR_FAIL;
	}

	

	if (hFile->m_bMem)
	{
		
	}
	else
	{

	}*/

	return	iRet;
}


QWORD FrTellFile(FILE_HANDLE hFile) {
	QWORD	iRet = 0;

	/*if (hFile == NULL) {
		TRACEX(FDR_LOG_WARN, "[System]	McTellFile : File Handle is NULL.");
		return -1;
	}

	if (hFile->m_bMem) {
		iRet = hFile->m_pPos - (BYTE*)hFile->m_pFile;
	}
	else {	
        iRet = lseek64(hFile->m_nFd, 0, SEEK_CUR);
		if ((INT64)iRet < 0) {
			return -1;
		}

	}*/

	return	iRet;
}

QWORD FrSizeOfFile(FILE_HANDLE hFile) {
	QWORD		iRet = 0;

	//if (hFile == NULL) {
	//	TRACEX(FDR_LOG_WARN, "[System]	McSizeOfFile : File Handle is NULL.");
	//	return -1;
	//}

	//if (hFile->m_bMem) {
	//	iRet = hFile->m_dwLen;
	//}
	//else {

	//	INT64   i64Pos = 0;
 //       i64Pos = lseek64(hFile->m_nFd, 0, SEEK_CUR);
 //       //LOG_T("[System]  McSizeOfFile : before current i64Pos = %lld.", i64Pos);

 //       iRet = lseek64(hFile->m_nFd, 0, SEEK_END);
 //       //LOG_T("[System]  McSizeOfFile : fd=%d, iRet = %lld.", hFile->m_nFd, iRet);

 //       i64Pos = lseek64(hFile->m_nFd, i64Pos, SEEK_SET);
 //       //LOG_T("[System]  McSizeOfFile : current i64Pos = %lld.", i64Pos);
	//}

	return	iRet;
}



BOOL FrCheckFile(TCHAR* pFilename) {
	FILE_HANDLE	hFile;
	BOOL		bRet = TRUE;

	hFile = FrOpenFile(pFilename, FILE_READ);
	if (!hFile)
		bRet = FALSE;
	FrCloseFile(hFile);

	return	bRet;
}

INT32 FrAccessFile(const TCHAR* pFilename, INT32 nMode) {
#ifdef	WIN32

	INT32 nRet = 0;

	//wchar_t szUniFileName[MAX_LONG_STRING_LENG*2] = L"";

	//LOG_I("[System]	McAccessFile : the URL type is UTF8 (%s) (nMode:%d)", pFilename, nMode);
	
	//UtilUniUTF8toUTF16(pFilename, strlen(pFilename), (UTF16*)szUniFileName, MAX_LONG_STRING_LENG*2);

	//nRet = _waccess(szUniFileName, nMode); // 00: Existence only, 02: Write-only, 04: Read-only, 06: Read and write

	//LOG_I("[System]	McAccessFile : RET (%d)", nRet);

	return nRet;

#else

	return -1;

#endif

}



INT32 FrFlushFile(FILE_HANDLE hFile) {
#ifdef	WIN32
	return fflush(hFile->m_pFile);
#endif

	return -1;
}
