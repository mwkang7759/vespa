#include "FrSystem.h"


namespace fr {
	void Sleep(int msec) {
		std::this_thread::sleep_for(std::chrono::milliseconds(msec));
	}

	bool GetCudaDeviceInfo(std::vector<cudaDeviceProp>& vProp) {
		bool bRet = true;
		struct cudaDeviceProp  prop;
		int count;
		
		cudaGetDeviceCount(&count);
		if (!count) {
			return false;
		}

		for (int i = 0; i < count; i++) {
			cudaGetDeviceProperties(&prop, i);

			vProp.push_back(prop);

			/*LOG_I("   --- General Information for device %d ---", i);
			LOG_I("Name:  %s", prop.name);
			LOG_I("Compute capability:  %d.%d", prop.major, prop.minor);
			LOG_I("Clock rate:  %d", prop.clockRate);
			LOG_I("Device copy overlap:  ");
			if (prop.deviceOverlap)
				LOG_I("Enabled");
			else
				LOG_I("Disabled");
			LOG_I("Kernel execution timeout :  ");
			if (prop.kernelExecTimeoutEnabled)
				LOG_I("Enabled");
			else
				LOG_I("Disabled");
			LOG_I("\n");

			LOG_I("   --- Memory Information for device %d ---", i);
			LOG_I("Total global mem:  %ud", prop.totalGlobalMem);
			LOG_I("Total constant Mem:  %ld", prop.totalConstMem);
			LOG_I("Max mem pitch:  %ld", prop.memPitch);
			LOG_I("Texture Alignment:  %ld", prop.textureAlignment);
			LOG_I("\n");

			LOG_I("   --- MP Information for device %d ---", i);
			LOG_I("Multiprocessor count:  %d", prop.multiProcessorCount);
			LOG_I("Shared mem per mp:  %ld", prop.sharedMemPerBlock);
			LOG_I("Registers per mp:  %d", prop.regsPerBlock);
			LOG_I("Threads in warp:  %d", prop.warpSize);
			LOG_I("Max threads per block:  %d", prop.maxThreadsPerBlock);
			LOG_I("Max thread dimensions:  (%d, %d, %d)", prop.maxThreadsDim[0], prop.maxThreadsDim[1], prop.maxThreadsDim[2]);
			LOG_I("Max grid dimensions:  (%d, %d, %d)", prop.maxGridSize[0], prop.maxGridSize[1], prop.maxGridSize[2]);
			LOG_I("\n");*/
		}

		return bRet;
	}

	unsigned long long GetSizeOfFile(std::string& name) {
		return std::filesystem::file_size(name);
	}

	void OpenFile(std::string& name, int mode) {

	}

	int File::Open(std::string& name, int mode) {
		_mode = mode;
		if (mode == READ_MODE) {
			_fin.open(name, std::ios_base::in | std::ios::binary);
			if (!_fin.is_open())
				return -1;
		}
		else if (mode == WRITE_MODE) {
			_fout.open(name, std::ios_base::out | std::ios::binary);
			if (!_fout.is_open()) {
				return -1;
			}
		}
		else {
			return -1;
		}
	}

	void File::Close() {
		if (_mode == READ_MODE) {
			_fin.close();
		}
		else if (_mode == WRITE_MODE) {
			_fout.close();
		}
	}

	int64 File::Read(char* buf, uint32 size) {
		if (_fin.is_open()) {
			return _fin.read(buf, size).gcount();
		}

		return -1;
	}

	void File::Write(char* buf, uint32 size) {
		if (_fout.is_open()) {
			_fout.write(buf, size);
		}
	}
}