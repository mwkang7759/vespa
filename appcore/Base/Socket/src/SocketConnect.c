
#include "SocketAPI.h"

LRSLT McSocketConnect(SOCK_HANDLE hSock, char* pAddr, WORD wPort, DWORD32 dwTimeOut) {
	struct sockaddr_in	this_sin;
	INT32				iRet = 0;
	LRSLT				lRet;


#if defined(WIN32)
	ULONG				lHow = 1;	// Non-blocking Mode
#else	//LINUX
	INT32					nFlags = 0;
	INT32					nLeng = 0;
	INT32					error = 0;
	//fd_set				rset, wset;
	//struct				timeval	tval;
#endif

	if (hSock==NULL) {
		TRACE("[Socket] McSocketConnect - null socket handle");
		return COMMON_ERR_NULLPOINTER;
	}

	this_sin.sin_family	= AF_INET;
	this_sin.sin_port = htons(wPort);

/*	if (ISDIGIT(pAddr[0]))
		this_sin.sin_addr.s_addr = INET_ADDR(pAddr);
	else
		this_sin.sin_addr.s_addr = McSocketGetHostByName(pAddr);*/

	this_sin.sin_addr.s_addr = INET_ADDR(pAddr);
	if(this_sin.sin_addr.s_addr == INADDR_NONE)	{
		LOG_I("[Socket]	Try to call McSocketGetHostByName func. (%s)", pAddr);
		this_sin.sin_addr.s_addr = McSocketGetHostByName(pAddr);
		LOG_I("[Socket]	End to call McSocketGetHostByName func.");
	}

	if (dwTimeOut) {
#if defined(WIN32)
    	if (ioctlsocket(hSock->m_hSocket, FIONBIO, &lHow) == SOCKET_ERROR) {
    		GetErrorMessage("McSocketConnect 1");
    		LOG_E("[Socket]	Failed : ioctlsocket, it'll return COMMON_ERR_SOCKET");
    		return	COMMON_ERR_SOCKET;
    	}
#else	//LINUX
    	nFlags = fcntl(hSock->m_hSocket, F_SETFL, nFlags | O_NONBLOCK);
    	if (fcntl(hSock->m_hSocket, F_SETFL, nFlags | O_NONBLOCK) < 0) {
    		// by Andy : �������Ͻ� McSocketClose() ���� close �ϱ� ����
    		//close(hSock->m_hSocket);      // just in case
    		GetErrorMessage("McSocketConnect 1");
    		LOG_E("[Socket]	Failed : ioctlsocket, it'll return COMMON_ERR_SOCKET");
    		return	COMMON_ERR_SOCKET;
    	}
#endif

	}

    //LOG_I("[Socket]	Try to call connect func, hSock[%d]", hSock);
    iRet = connect((SOCKET)hSock->m_hSocket, (struct sockaddr*)&this_sin, sizeof(this_sin));
    //LOG_I("[Socket]	End to call connect func (hSock[%d], Ret:%d, SOCKET_ERROR:%d)", hSock, iRet, SOCKET_ERROR);

	if (iRet == SOCKET_ERROR) {
		GetErrorMessage("McSocketConnect 2");	// 2007.08.01 hjchoi - Print first connect error.

		if (dwTimeOut) {
#if defined(WIN32)
			lRet = McSocketWait2(hSock, dwTimeOut);
			if (FRFAILED(lRet))	{
				iRet = GetLastError();
				if (lRet == (LRSLT)COMMON_ERR_TIMEOUT)	{
					LOG_I("[Socket]	Try to Reconnect!");
					iRet = connect((SOCKET)hSock->m_hSocket, (struct sockaddr*)&this_sin, sizeof(this_sin));
					if (iRet == SOCKET_ERROR) {
						iRet = GetLastError();
						if (iRet != WSAEISCONN) {
							LOG_W("[Socket]	* Reconnect Fail!");
							GetErrorMessage("McSocketConnect");
							return	COMMON_ERR_SVR_CONNECT;
						}
						LOG_I("[Socket]	Reconnect Success!");
					}
				}
				else {
					LOG_E("[Socket]	McSocketWait2 Return Error!");
					GetErrorMessage("McSocketConnect");
					return	COMMON_ERR_SVR_CONNECT;
				}
			}
#else	//LINUX
			/*FD_ZERO(&rset);
			FD_SET(hSock->m_hSocket, &rset);
			wset = rset;
			tval.tv_sec = dwTimeOut / 1000;
			tval.tv_usec = (dwTimeOut % 1000) * 1000;*/

			//if ( (iRet = select(hSock->m_hSocket+1, &rset, &wset, NULL, dwTimeOut ? &tval : NULL)) == 0)
			lRet = McSocketWait2(hSock, dwTimeOut);
			if (FRFAILED(lRet)) {
				if (lRet == (LRSLT)COMMON_ERR_TIMEOUT) {
					LOG_E("[Socket]	McSocketWait2 timeout (%d) error!", dwTimeOut);
				}
				else {
					LOG_E("[Socket]	McSocketWait2 Return Error!");
				}

				GetErrorMessage("McSocketConnect");
				return	COMMON_ERR_SVR_CONNECT;
			}

			//if (FD_ISSET(hSock->m_hSocket, &rset) || FD_ISSET(hSock->m_hSocket, &wset))
			{
				nLeng = sizeof(error);
				iRet = getsockopt(hSock->m_hSocket, SOL_SOCKET, SO_ERROR, (void *)&error, (socklen_t *)&nLeng);
				if (iRet < 0 || error) {
					LOG_E("[Socket]	getsockopt error(Ret:%d, error:%d)", iRet, error);
					// Solaris pending error
					GetErrorMessage("McSocketConnect");
					return	COMMON_ERR_SVR_CONNECT;
				}
			}
#endif
		}
		else {
			GetErrorMessage("McSocketConnect 4");
			return	COMMON_ERR_SVR_CONNECT;
		}
	}

	LOG_T("[Socket]		Connected.........");

	return	FR_OK;
}
