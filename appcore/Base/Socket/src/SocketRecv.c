
#include "SocketAPI.h"
//#include "LibOpenSSLAPI.h"
//#include "DtbWebSocketAPI.h"

LRSLT McSocketRecv(SOCK_HANDLE hSock, char *pBuf, INT32 iLen) {
	INT32			iRecv=0;
#ifdef _WEBSOCKET_ENABLE
    int             nBin = 0;
#endif
	if (hSock==NULL) {
		TRACE("[Socket] McSocketRecv - null socket handle");
		return COMMON_ERR_NULLPOINTER;
	}

#ifdef _WEBSOCKET_ENABLE
    if (hSock->m_hWebSocket) {
        iRecv = WebSocket_Recv(hSock->m_hWebSocket, pBuf, iLen, &nBin);
        //LOG_I("[Socket] WebSocket Recv mode = [%d]", nBin);
    }
	else
#endif
    if (hSock->m_bSSL) {
        //iRecv = DtbSSLRead(hSock->m_hSsl, pBuf, iLen);
	}
	else {
		iRecv = recv((SOCKET)hSock->m_hSocket, pBuf, iLen, 0);
	}

	if (iRecv == SOCKET_ERROR || !iRecv) {
        LOG_E("[Socket] McSocketRecv(%d/%d)", iRecv, iLen);
		GetErrorMessage("McSocketRecv");
		return	COMMON_ERR_RECV;
	}

	return	(LRSLT)iRecv;
}

INT32 DtbSocketRecv(SOCK_HANDLE hSock, char* ptr, INT32 nbytes)
{
	INT32			nrecvbytes=0, nleft;
#ifdef _WEBSOCKET_ENABLE
    int             nBin;
#endif

	if (hSock==NULL) {
		TRACE("[Socket] DtbSocketRecv - null socket handle");
		return COMMON_ERR_NULLPOINTER;
	}

	nleft = nbytes;
	while(nleft > 0) {
#ifdef _WEBSOCKET_ENABLE
        if (hSock->m_hWebSocket) {
            nrecvbytes = WebSocket_Recv(hSock->m_hWebSocket, ptr, nleft, &nBin);
            LOG_I("[Socket] WebSocket Recv mode = [%d]", nBin);
        }
        else
#endif
		if (hSock->m_bSSL) {
			//nrecvbytes = DtbSSLRead(hSock->m_hSsl, ptr, nleft);
		}
		else {
			nrecvbytes = recv((SOCKET)hSock->m_hSocket, ptr, nleft, 0);
		}

		if(nrecvbytes == 0)	{
			LOG_E("[DtbRecv] Disconnect : remote host gracefully close.....!!");
			return COMMON_ERR_RECV;
		}
		else if(nrecvbytes == SOCKET_ERROR)	{
			GetErrorMessage("DtbRecv");
			return COMMON_ERR_RECV;
		}
		else {
			nleft -= nrecvbytes;
			ptr   += nrecvbytes;
		}
	}
	return (nbytes - nleft);
}

