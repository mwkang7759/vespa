/*****************************************************************************
*                                                                            *
*                            Socket Library									 *
*                                                                            *
*   Copyright (c) 2004 by Mcubeworks, Incoporated. All Rights Reserved.      *
******************************************************************************

    File Name       : SocketGethostbyaddr.c
    Author(s)       : Kim, Kun-Tae
    Created         : 11 Jan 2005

    Description     : SIP API
    Notes           :

==============================================================================
                      Modification History (Reverse Order)
==============================================================================
    Date        Author      Location (variables) / Description
------------------------------------------------------------------------------

------------------------------------------------------------------------------*/
#include "SocketAPI.h"

const char* McSocketGetHostByAddr(char* pAddr) {
	struct sockaddr_in	this_sin;
	struct hostent* pHostEnt;
	const char*		pHostAddr;

	this_sin.sin_family	= AF_INET;
	this_sin.sin_addr.s_addr = INET_ADDR(pAddr);

	if ((pHostEnt = gethostbyaddr((char*)&(this_sin.sin_addr.s_addr), 4, PF_INET)) == NULL)	{
		GetErrorMessage("McSocketGetHostByAddr");
		return	NULL;
	}

	pHostAddr = pHostEnt->h_name;

	return	pHostAddr;
}
