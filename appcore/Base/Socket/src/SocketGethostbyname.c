/*****************************************************************************
*                                                                            *
*                            Socket Library									 *
*                                                                            *
*   Copyright (c) 2004 by Mcubeworks, Incoporated. All Rights Reserved.      *
******************************************************************************

    File Name       : SocketGethostbyname.c
    Author(s)       : Kim, Kun-Tae
    Created         : 11 Jan 2005

    Description     : SIP API
    Notes           :

==============================================================================
                      Modification History (Reverse Order)
==============================================================================
    Date        Author      Location (variables) / Description
------------------------------------------------------------------------------

------------------------------------------------------------------------------*/
#include "SocketAPI.h"

DWORD32 McSocketGetHostByName(const char* pchName) {
	struct hostent* pHostEnt;
	DWORD32*			pdwAddr;

	if ((pHostEnt = gethostbyname(pchName)) == NULL)
		return	GetErrorMessage("McSocketGetHostByName");

	pdwAddr = (DWORD32*) pHostEnt->h_addr_list[0];

	return	*pdwAddr;
}
