/*****************************************************************************
*                                                                            *
*                            Socket Library									 *
*                                                                            *
*   Copyright (c) 2004 by Mcubeworks, Incoporated. All Rights Reserved.      *
******************************************************************************

    File Name       : SocketSelect.c
    Author(s)       : Kim, Kun-Tae
    Created         : 11 Jan 2005

    Description     : SIP API
    Notes           :

==============================================================================
                      Modification History (Reverse Order)
==============================================================================
    Date        Author      Location (variables) / Description
------------------------------------------------------------------------------

------------------------------------------------------------------------------*/
#include "SocketAPI.h"

LRSLT McSocketSelect(FD_SET_TYPE *rSet, FD_SET_TYPE *wSet, FD_SET_TYPE *eSet) {
	INT32				iRet;
	struct timeval	tv;

	tv.tv_sec = 0;
	//tv.tv_usec = 100000;
	tv.tv_usec = 10000;
#ifdef	WIN32
	iRet = select(0, (fd_set*)rSet, (fd_set*)wSet, (fd_set*)eSet, &tv);
#else
	iRet = select(FD_SETSIZE, (fd_set*)rSet, (fd_set*)wSet, (fd_set*)eSet, &tv);
#endif
	if (!iRet)
		return	COMMON_ERR_TIMEOUT;
	else if (iRet == SOCKET_ERROR)
	{
		GetErrorMessage("McSocketSelect");
		return	COMMON_ERR_SOCKET;
	}

	return	(LRSLT)iRet;
}

LRSLT McSocketSelect2(FD_SET_TYPE *rSet, FD_SET_TYPE *wSet, FD_SET_TYPE *eSet, DWORD32 dwTimeout) {
	INT32				iRet;
	struct timeval	tv;

	tv.tv_sec = dwTimeout/1000;
	tv.tv_usec = (dwTimeout%1000)*1000;
#ifdef	WIN32
	iRet = select(0, (fd_set*)rSet, (fd_set*)wSet, (fd_set*)eSet, &tv);
#else
	iRet = select(FD_SETSIZE, (fd_set*)rSet, (fd_set*)wSet, (fd_set*)eSet, &tv);
#endif
	if (!iRet)
		return	COMMON_ERR_TIMEOUT;
	else if (iRet == SOCKET_ERROR)
	{
		GetErrorMessage("McSocketSelect");
		return	COMMON_ERR_SOCKET;
	}

	return	(LRSLT)iRet;
}
