/*****************************************************************************
*                                                                            *
*                            Socket Library									 *
*                                                                            *
*   Copyright (c) 2004 by Mcubeworks, Incoporated. All Rights Reserved.      *
******************************************************************************

    File Name       : SocketUtil.c
    Author(s)       : Kim, Kun-Tae
    Created         : 11 Jan 2005

    Description     : SIP API
    Notes           :

==============================================================================
                      Modification History (Reverse Order)
==============================================================================
    Date        Author      Location (variables) / Description
------------------------------------------------------------------------------

------------------------------------------------------------------------------*/
#include "SocketAPI.h"
#include "UtilAPI.h"

DWORD32 INET_ADDR(char *psz) {
   DWORD32 	ul = 0;
   INT32 		nByte = 0;
   char 	c;

   if(!psz)
      return 0;

   while (ISDIGIT(*psz)) {
      INT32 n = 0;
      while ( ISDIGIT(c=*psz)) {
         n = n*10 + (c - '0');
         ++psz;
      }
      ((char*)&ul)[nByte++] = n;

      if (nByte == 4 || *psz != '.')
         break;

      ++psz;
   }

   if (nByte < 4 || ISALNUM(*psz))
      ul = 0xFFFFFFFF;    // Invalid address

   return ul;
}

void INET_NTOA(char* psz, DWORD32 dwAddr) {
	INT32		nTemp;
	char*	p = psz;
	INT32		i;

	for (i = 3;i >= 0;i--) {
		nTemp = (dwAddr >> 8*i) & 0x000000ff;
		ITOA(nTemp, p);
		p = psz + strlen(psz);
		if (i)
			*p++ = '.';
	}
}
