/*****************************************************************************
*                                                                            *
*                            Socket Library									 *
*                                                                            *
*   Copyright (c) 2004 by Mcubeworks, Incoporated. All Rights Reserved.      *
******************************************************************************

    File Name       : SocketRecvfrom.c
    Author(s)       : Kim, Kun-Tae
    Created         : 11 Jan 2005

    Description     : SIP API
    Notes           :

==============================================================================
                      Modification History (Reverse Order)
==============================================================================
    Date        Author      Location (variables) / Description
------------------------------------------------------------------------------

------------------------------------------------------------------------------*/
#include "SocketAPI.h"

LRSLT McSocketRecvFrom(SOCK_HANDLE hSock, char *pBuf, INT32 iLen, DWORD32* pdwAddr, WORD* pwPort) {
	struct sockaddr_in		this_sin;
	INT32					iLen_this_sin;
	INT32					iRecv;

	if (hSock==NULL) {
		TRACE("[Socket] McSocketRecvFrom - null socket handle");
		return COMMON_ERR_NULLPOINTER;
	}

#ifdef _WEBSOCKET_ENABLE
    if (hSock->m_hWebSocket)
	{
		TRACE("[Socket] McSocketRecvFrom - websocket socket handle (NOT SUPPORT)");
		return FR_FAIL;
	}
#endif

	if (hSock->m_bSSL)	{
		TRACE("[Socket] McSocketRecvFrom - ssl socket handle (NOT SUPPORT)");
		return FR_FAIL;
	}

	this_sin.sin_family			= AF_INET;
	this_sin.sin_port			= htons(0);
	this_sin.sin_addr.s_addr	= htonl(INADDR_ANY);
	iLen_this_sin				= sizeof(this_sin);

#if defined(WIN32) || defined(HPUX)
	iRecv = recvfrom((SOCKET)hSock->m_hSocket, pBuf, iLen, 0, (struct sockaddr*)&this_sin, &iLen_this_sin);
#else
	iRecv = recvfrom((SOCKET)hSock->m_hSocket, pBuf, iLen, 0, (struct sockaddr*)&this_sin, (socklen_t*)&iLen_this_sin);
#endif//WIN32
	if (iRecv == SOCKET_ERROR || !iRecv) {
		GetErrorMessage("McSocketRecvFrom");
		return	COMMON_ERR_RECV;
	}

	if (pdwAddr != NULL)
		*pdwAddr = ntohl(this_sin.sin_addr.s_addr);
	if (pwPort != NULL)
		*pwPort = ntohs(this_sin.sin_port);

	return	(LRSLT)iRecv;
}

