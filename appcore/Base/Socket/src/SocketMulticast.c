/*****************************************************************************
*                                                                            *
*                            Socket Library									 *
*                                                                            *
*   Copyright (c) 2004 by Mcubeworks, Incoporated. All Rights Reserved.      *
******************************************************************************

    File Name       : SocketListen.c
    Author(s)       : Oh, Mu-Hwan
    Created         : 11 Jan 2005

    Description     : Socket API
    Notes           :

==============================================================================
                      Modification History (Reverse Order)
==============================================================================
    Date        Author      Location (variables) / Description
------------------------------------------------------------------------------

------------------------------------------------------------------------------*/

#ifndef MULTI_CAST
#define	MULTI_CAST
#endif
#include "SocketAPI.h"

#ifdef WIN32
#include <ws2tcpip.h>
#endif


BOOL McSocketMulticastTTL(SOCK_HANDLE hSock, char* pMultiAddr, char* pLocalAddr, WORD wPort, BYTE nMulticastTTL)
{
    struct sockaddr_in  mcast_group;
    struct in_addr      iaddr;
    unsigned char ttl = nMulticastTTL;
    unsigned char one = 0;

	if (hSock==NULL) {
		TRACE("[Socket] McSocketMulticastTTL - null socket handle");
		return FALSE;
	}

#ifdef _WEBSOCKET_ENABLE
    if (hSock->m_hWebSocket)
    {
        TRACE("[Socket] McSocketMulticastTTL - websocket socket handle (NOT SUPPORT)");
        return FALSE;
    }
#endif

    memset(&mcast_group, 0, sizeof(struct sockaddr_in));
    memset(&iaddr, 0, sizeof(struct in_addr));

    // Set destination multicast address
    mcast_group.sin_family = AF_INET;
    mcast_group.sin_port = htons(wPort);
    if (ISDIGIT(pMultiAddr[0]))
        mcast_group.sin_addr.s_addr = INET_ADDR(pMultiAddr);
    else
        mcast_group.sin_addr.s_addr = McSocketGetHostByName(pMultiAddr);

    if (pLocalAddr)
        iaddr.s_addr = INET_ADDR(pLocalAddr);
    else
        iaddr.s_addr = INADDR_ANY;

    // Set the outgoing interface to DEFAULT
    if (setsockopt(hSock->m_hSocket, IPPROTO_IP, IP_MULTICAST_IF, (CHAR*)&iaddr, sizeof(struct in_addr)) < 0) {
        GetErrorMessage("McSocketMulticastTTL");
		return FALSE;
    }

    // Set multicast packet TTL to 3; default TTL is 1
    if (setsockopt(hSock->m_hSocket, IPPROTO_IP, IP_MULTICAST_TTL, (CHAR*)&ttl, sizeof(unsigned char)) < 0) {
        GetErrorMessage("McSocketMulticastTTL");
        return FALSE;
    }

    // Send multicast traffic to myself(Loopback) too
    if (setsockopt(hSock->m_hSocket, IPPROTO_IP, IP_MULTICAST_LOOP, (CHAR*)&one, sizeof(unsigned char)) < 0) {
        GetErrorMessage("McSocketMulticastTTL");
        return FALSE;
    }

	// 0 : success, -1 : failure
	if (bind(hSock->m_hSocket, (struct sockaddr *)&mcast_group, sizeof(mcast_group)) < 0) {
		GetErrorMessage("McSocketMulticastJoin");
		return	FALSE;
	}

    return TRUE;
}

BOOL McSocketMulticastJoin(SOCK_HANDLE hSock, char* pMultiAddr, char* pLocalAddr, WORD wPort) {
	UINT32		nYes = 1;
	struct sockaddr_in	mcast_group;
	struct ip_mreq		mreq;

	if (hSock==NULL) {
		TRACE("[Socket] McSocketMulticastJoin - null socket handle");
		return FALSE;
	}

#ifdef _WEBSOCKET_ENABLE
    if (hSock->m_hWebSocket)
    {
        TRACE("[Socket] McSocketMulticastJoin - websocket socket handle (NOT SUPPORT)");
        return FALSE;
    }
#endif

	// Name the socket (assign the local port number to receive on)
	memset(&mcast_group, 0, sizeof(mcast_group));
    mcast_group.sin_family = AF_INET;
	mcast_group.sin_port = htons(wPort);
	if (ISDIGIT(pMultiAddr[0]))
		mcast_group.sin_addr.s_addr = INET_ADDR(pMultiAddr);
	else
		mcast_group.sin_addr.s_addr = McSocketGetHostByName(pMultiAddr);

	// ip_mreq 구조체 지정
	mreq.imr_multiaddr = mcast_group.sin_addr;
	if (pLocalAddr)
		mreq.imr_interface.s_addr = INET_ADDR(pLocalAddr);		// 자신의 인터페이스(IP 주소)
	else
		mreq.imr_interface.s_addr = htonl(INADDR_ANY);

	// 멀티캐스트 그룹에 가입
	if (setsockopt(hSock->m_hSocket, IPPROTO_IP, IP_ADD_MEMBERSHIP, (const char *)&mreq, sizeof(mreq)) < 0)	{
		GetErrorMessage("McSocketMulticastJoin");
		return	FALSE;
	}

	// 소켓 재사용 옵션 지정
	if (setsockopt(hSock->m_hSocket, SOL_SOCKET, SO_REUSEADDR, (const char *)&nYes, sizeof(nYes)) < 0) {
		GetErrorMessage("McSocketMulticastJoin");
		return	FALSE;
	}

	// 0 : success, -1 : failure
	if (bind (hSock->m_hSocket, (struct sockaddr *)&mcast_group, sizeof(mcast_group)) < 0) {
		GetErrorMessage("McSocketMulticastJoin");
		return	FALSE;
	}

    return	TRUE;
}

void McSocketMulticastDrop(SOCK_HANDLE hSock, char* pMultiAddr, char* pLocalAddr, WORD wPort) {
	struct sockaddr_in	mcast_group;
	struct ip_mreq		mreq;

	if (hSock==NULL) {
		TRACE("[Socket] McSocketMulticastDrop - null socket handle");
		return;
	}

#ifdef _WEBSOCKET_ENABLE
    if (hSock->m_hWebSocket)
    {
        TRACE("[Socket] McSocketMulticastDrop - websocket socket handle (NOT SUPPORT)");
        return ; // FALSE
    }
#endif

	// Name the socket (assign the local port number to receive on)
	memset(&mcast_group, 0, sizeof(mcast_group));
    mcast_group.sin_family = AF_INET;
	mcast_group.sin_port = htons(wPort);
	if (ISDIGIT(pMultiAddr[0]))
		mcast_group.sin_addr.s_addr = INET_ADDR(pMultiAddr);
	else
		mcast_group.sin_addr.s_addr = McSocketGetHostByName(pMultiAddr);

	// ip_mreq 구조체 지정
	mreq.imr_multiaddr = mcast_group.sin_addr;
	if (pLocalAddr)
		mreq.imr_interface.s_addr = INET_ADDR(pLocalAddr);		// 자신의 인터페이스(IP 주소)
	else
		mreq.imr_interface.s_addr = htonl(INADDR_ANY);

	setsockopt(hSock->m_hSocket, IPPROTO_IP, IP_DROP_MEMBERSHIP, (const char *)&mreq, sizeof(mreq));
}

