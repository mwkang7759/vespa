/*****************************************************************************
*                                                                            *
*                            Socket Library									 *
*                                                                            *
*   Copyright (c) 2004 by Mcubeworks, Incoporated. All Rights Reserved.      *
******************************************************************************

    File Name       : SocketListen.c
    Author(s)       : Kim, Kun-Tae
    Created         : 11 Jan 2005

    Description     : SIP API
    Notes           :

==============================================================================
                      Modification History (Reverse Order)
==============================================================================
    Date        Author      Location (variables) / Description
------------------------------------------------------------------------------

------------------------------------------------------------------------------*/
#include "SocketAPI.h"

BOOL McSocketListen(SOCK_HANDLE hSock) {
	INT32		iRet;

	if (hSock==NULL) {
		TRACE("[Socket] McSocketListen - null socket handle");
		return COMMON_ERR_NULLPOINTER;
	}

    iRet = listen(hSock->m_hSocket, 5);
    if (iRet == SOCKET_ERROR) {
    	GetErrorMessage("McSocketListen");
    	return	FALSE;
    }

	return	TRUE;
}