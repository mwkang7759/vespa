
#include "SocketAPI.h"

// Only Receive event Check
LRSLT McSocketWait(SOCK_HANDLE hSock, DWORD32 dwTimeout) {
	INT32				iRet;

#ifdef	WIN32
	FD_SET_TYPE		rSet;
	struct timeval	tv;
#endif

// Use select
#ifdef	WIN32
	if (hSock==NULL) {
		TRACE("[Socket] McSocketWait - null socket handle");
		return COMMON_ERR_NULLPOINTER;
	}

	tv.tv_sec = dwTimeout/1000;
	tv.tv_usec = (dwTimeout%1000)*1000;

	FD_ZERO(&rSet);
	FD_SET(hSock->m_hSocket, &rSet);

	iRet = select(0, (fd_set*)&rSet, NULL, NULL, &tv);

// Use epoll
#elif defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_ANDROID)

	int	nEPollSize = 3;
	struct epoll_event ev, events[nEPollSize];
	int efd;

	if (hSock==NULL) {
		TRACE("[Socket] McSocketWait - null socket handle");
		return COMMON_ERR_NULLPOINTER;
	}

	// epoll_create를 이용해서 epoll 지정자를 생성한다.
	if ((efd = epoll_create(nEPollSize)) < 0) {
		//close(efd);
		GetErrorMessage("McSocketWait:epoll_create");
		return	COMMON_ERR_SOCKET;
	}

	// 소켓을 epoll 이벤트 풀에 추가한다.
	ev.events = EPOLLIN;
	ev.data.fd = hSock->m_hSocket;
	if ((iRet = epoll_ctl(efd, EPOLL_CTL_ADD, hSock->m_hSocket, &ev)) < 0) {
		GetErrorMessage("McSocketWait:epoll_ctl");
        close(efd);
		return	COMMON_ERR_SOCKET;
	}

	// epoll이벤트 풀에서 이벤트가 발생했는지를 검사한다.
	memset(events, 0,nEPollSize* sizeof(struct epoll_event *));
	iRet = epoll_wait(efd, events, nEPollSize, dwTimeout);

	if (iRet > 0) {
		if (events[0].data.fd != hSock->m_hSocket)	{
			GetErrorMessage("McSocketWait:epoll_wait");
            close(efd);
			return	COMMON_ERR_SOCKET;
		}
	}

	close(efd);

// Use poll
#else
	struct pollfd fds[1];

	if (hSock==NULL) {
		TRACE("[Socket] McSocketWait - null socket handle");
		return COMMON_ERR_NULLPOINTER;
	}

	fds[0].fd = hSock->m_hSocket;
	fds[0].events = POLLIN;

	iRet = poll(fds, 1, dwTimeout);
	if (iRet > 0) {
		if ( !(fds[0].revents & POLLIN) ) {
			GetErrorMessage("McSocketWait:poll");
			return	COMMON_ERR_SOCKET;
		}
	}

#endif

	if (!iRet)
		return	COMMON_ERR_TIMEOUT;
	else if (iRet == SOCKET_ERROR)	{
		iRet = GetErrorMessage("McSocketWaitRecv");
        // smlee add 20200623
        switch(iRet) {
            case EINTR :
                TRACE("[Socket] McSocketWait - Interrupt error, Retry..");
                return COMMON_ERR_TIMEOUT; // retry
                break;
        }

		return	COMMON_ERR_SOCKET;
	}

	return	FR_OK;
}

// Send and Receive event Check
LRSLT McSocketWait2(SOCK_HANDLE hSock, DWORD32 dwTimeout)
{
	INT32				iRet;
#ifdef	WIN32
	FD_SET_TYPE		rSet, wSet;
	struct timeval	tv;
#endif

// Use select
#ifdef	WIN32
	if (hSock==NULL) {
		TRACE("[Socket] McSocketWait2 - null socket handle");
		return COMMON_ERR_NULLPOINTER;
	}

	tv.tv_sec = dwTimeout/1000;
	tv.tv_usec = (dwTimeout%1000)*1000;

	FD_ZERO(&rSet);
	FD_SET(hSock->m_hSocket, &rSet);

	FD_ZERO(&wSet);
	FD_SET(hSock->m_hSocket, &wSet);

	iRet = select(0, (fd_set*)&rSet, (fd_set*)&wSet, NULL, &tv);

// Use epoll
#elif defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_ANDROID)
	int	nEPollSize = 3;
	struct epoll_event ev, events[nEPollSize];
	int efd;

	if (hSock==NULL) {
		TRACE("[Socket] McSocketWait2 - null socket handle");
		return COMMON_ERR_NULLPOINTER;
	}

	// epoll_create를 이용해서 epoll 지정자를 생성한다.
	if ((efd = epoll_create(nEPollSize)) < 0) {
		//close(efd);
		GetErrorMessage("McSocketWait2:epoll_create");
		return	COMMON_ERR_SOCKET;
	}

	// 소켓을 epoll 이벤트 풀에 추가한다.
	ev.events = EPOLLIN;
	ev.events |= EPOLLOUT;
	ev.data.fd = hSock->m_hSocket;
	if ((iRet = epoll_ctl(efd, EPOLL_CTL_ADD, hSock->m_hSocket, &ev)) < 0) {
		GetErrorMessage("McSocketWait2:epoll_ctl");
        close(efd);
		return	COMMON_ERR_SOCKET;
	}

	// epoll이벤트 풀에서 이벤트가 발생했는지를 검사한다.
	iRet = epoll_wait(efd, events, nEPollSize, dwTimeout);
	if (iRet > 0) {
		if (events[0].data.fd != hSock->m_hSocket) {
			GetErrorMessage("McSocketWait2:epoll_wait");
            close(efd);
			return	COMMON_ERR_SOCKET;
		}
	}

	close(efd);

// Use poll
#else
	struct pollfd fds[1];

	if (hSock==NULL) {
		TRACE("[Socket] McSocketWait2 - null socket handle");
		return COMMON_ERR_NULLPOINTER;
	}

	fds[0].fd = hSock->m_hSocket;
	fds[0].events = POLLIN | POLLOUT;

	iRet = poll(fds, 1, dwTimeout);
	if (iRet > 0) {
		if ( !(fds[0].revents & POLLIN) && !(fds[0].revents & POLLOUT) ) {
			GetErrorMessage("McSocketWait2:poll");
			return	COMMON_ERR_SOCKET;
		}
	}

#endif
	if (!iRet) {
		return	COMMON_ERR_TIMEOUT;
	}
	else if (iRet == SOCKET_ERROR) {
		iRet = GetErrorMessage("McSocketWaitRecv");
        // smlee add 20200623
        switch(iRet) {
            case EINTR :
                TRACE("[Socket] McSocketWait - Interrupt error, Retry..");
                return COMMON_ERR_TIMEOUT; // retry
                break;
        }
		return	COMMON_ERR_SOCKET;
	}

	return	FR_OK;
}

LRSLT McSocketWait3(SOCK_HANDLE hSock, DWORD dwTimeout) {
	FD_SET_TYPE		rSet;
	struct timeval	tv;
	int				iRet;

	if (!hSock)
		return FR_FAIL;

	tv.tv_sec = dwTimeout/1000;
	tv.tv_usec = (dwTimeout%1000)*1000;

	if (hSock==NULL) {
		TRACE("[Socket] McSocketWait3 - null socket handle");
		return COMMON_ERR_NULLPOINTER;
	}

	FD_ZERO(&rSet);
	FD_SET(hSock->m_hSocket, &rSet);

#ifdef	WIN32
	iRet = select(0, (fd_set*)&rSet, NULL, NULL, &tv);
#else
	iRet = select(hSock->m_hSocket+1, (fd_set*)&rSet, NULL, NULL, &tv);
#endif
	if (!iRet)
		return	COMMON_ERR_TIMEOUT;
	else if (iRet == SOCKET_ERROR) {
		iRet = GetErrorMessage("McSocketWaitRecv");
        // smlee add 20200623
        switch(iRet) {
            case EINTR :
                TRACE("[Socket] McSocketWait - Interrupt error, Retry..");
                return COMMON_ERR_TIMEOUT; // retry
                break;
        }
		return	COMMON_ERR_SOCKET;
	}

	return	FR_OK;
}
