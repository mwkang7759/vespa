/*****************************************************************************
*                                                                            *
*                            Socket Library									 *
*                                                                            *
*   Copyright (c) 2004 by Mcubeworks, Incoporated. All Rights Reserved.      *
******************************************************************************

    File Name       : SocketBind.c
    Author(s)       : Kim, Kun-Tae
    Created         : 11 Jan 2005

    Description     : SIP API
    Notes           :

==============================================================================
                      Modification History (Reverse Order)
==============================================================================
    Date        Author      Location (variables) / Description
------------------------------------------------------------------------------

------------------------------------------------------------------------------*/
#include "SocketAPI.h"

#define	MAX_PENDDING_CONNECTION		10

BOOL McSocketBind(SOCK_HANDLE hSock, WORD wPort, BOOL bTCP) {
	struct sockaddr_in	this_sin;
	INT32					iRet;

	if (hSock==NULL) {
		TRACE("[Socket] McSocketBind - null socket handle");
		return FALSE;
	}

	this_sin.sin_family			= AF_INET;
	this_sin.sin_port			= htons(wPort);
	this_sin.sin_addr.s_addr	= htonl(INADDR_ANY);

    iRet = bind((SOCKET)hSock->m_hSocket, (struct sockaddr*)&this_sin, sizeof(this_sin));
    if (iRet == SOCKET_ERROR) {
    	GetErrorMessage("McSocketBind");
    	return	FALSE;
    }

    if (bTCP) {
    	iRet = listen((SOCKET)hSock->m_hSocket, MAX_PENDDING_CONNECTION);
    	if (iRet == SOCKET_ERROR) {
    		GetErrorMessage("McSocketBind");
    		return	FALSE;
    	}
    }


	return	TRUE;
}

BOOL McSocketBind2(SOCK_HANDLE hSock, char *pAddr, WORD wPort, BOOL bTCP) {
	struct sockaddr_in	this_sin;
	INT32					iRet;

	if (hSock==NULL) {
		TRACE("[Socket] McSocketBind - null socket handle");
		return FALSE;
	}

	this_sin.sin_family			= AF_INET;
	this_sin.sin_port			= htons(wPort);
	//this_sin.sin_addr.s_addr	= htonl(INET_ADDR(pAddr));
    this_sin.sin_addr.s_addr	= inet_addr(pAddr);

    iRet = bind((SOCKET)hSock->m_hSocket, (struct sockaddr*)&this_sin, sizeof(this_sin));
    if (iRet == SOCKET_ERROR) {
    	GetErrorMessage("McSocketBind");
    	return	FALSE;
    }

    if (bTCP) {
    	iRet = listen((SOCKET)hSock->m_hSocket, MAX_PENDDING_CONNECTION);
    	if (iRet == SOCKET_ERROR) {
    		GetErrorMessage("McSocketBind");
    		return	FALSE;
    	}
    }

	return	TRUE;
}

