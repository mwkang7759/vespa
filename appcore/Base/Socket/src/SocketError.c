/*****************************************************************************
*                                                                            *
*                            Socket Library									 *
*                                                                            *
*   Copyright (c) 2004 by Mcubeworks, Incoporated. All Rights Reserved.      *
******************************************************************************

    File Name       : SocketError.c
    Author(s)       : Kim, Kun-Tae
    Created         : 11 Jan 2005

    Description     : SIP API
    Notes           :

==============================================================================
                      Modification History (Reverse Order)
==============================================================================
    Date        Author      Location (variables) / Description
------------------------------------------------------------------------------

------------------------------------------------------------------------------*/
#include "SocketAPI.h"
#include "TraceAPI.h"

ULONG GetErrorMessage(char* szFunc)
{
	ULONG	lErrno = 0;

#ifdef	WIN32

	//LOG_I("[Socket]	%s - ", szFunc);
	switch (lErrno = WSAGetLastError())
	{
	case 0					:
		LOG_I("[Socket]	Disconnected (%u)", lErrno);
		break;
	case WSAEACCES			:	// 10013
		LOG_I("[Socket]	WSAEACCES : Invalid Multicast address was used.");
		break;
	case WSAEADDRINUSE		:	// 10048
		LOG_I("[Socket]	WSAEADDRINUSE : The specified address is already in use");
		break;
	case WSAEADDRNOTAVAIL	:	// 10049
		LOG_I("[Socket]	WSAEADDRNOTAVAIL : The specified address is not available from the local machine");
		break;
	case WSAEAFNOSUPPORT	:	// 10047
		LOG_I("[Socket]	WSAEAFNOSUPPORT : Addresses in the specified family cannot be used with this socket.");
		break;
	case WSAEALREADY		:	// 10037
		LOG_I("[Socket]	WSAEALREADY : An operation was attempted on a non-blocking socket that already had an operation in progress.");
		break;
	case WSAECONNABORTED	:	// 10053
		LOG_I("[Socket]	WSAECONNABORTED : The connection was aborted due to timeout or other failure.");
		break;
	case WSAECONNREFUSED	:	// 10061
		LOG_I("[Socket]	WSAECONNREFUSED : The attempt to connect was forcefully rejected.(Refused)");
		break;
	case WSAECONNRESET		:	// 10054
		LOG_I("[Socket]	WSAECONNRESET : The connection was reset by the remote side.");
		break;
	case WSAEDESTADDRREQ	:	// 10039 - add hjchoi
		LOG_I("[Socket]	WSAEDESTADDRREQ : A required address was omitted from an operation on a socket. For example, this error is returned if sendto is called with the remote address of ADDR_ANY.");
		break;
	case WSAEFAULT			:	// 10014
		LOG_I("[Socket]	WSAEFAULT : The buf,namelen or buf length argument is incorrect.");
		break;
	case WSAEHOSTDOWN		:	// 10064 - add hjchoi
		LOG_I("[Socket]	WSAEHOSTDOWN : A socket operation failed because the destination host is down.");
		break;
	case WSAEHOSTUNREACH	:	// 10065
		LOG_I("[Socket]	WSAEHOSTUNREACH : A socket operation was attempted to an unreachable host.");
		break;
	case WSAEINPROGRESS		:	// 10036 - add hjchoi
		LOG_I("[Socket]	WSAEINPROGRESS : A blocking operation is currently executing.");
		break;
	case WSAEINTR			:	// 10004
		LOG_I("[Socket]	WSAEINTR : Interrupted function call.");
		break;
	case WSAEINVAL			:	// 10022
		LOG_I("[Socket]	WSAEINVAL : Invalid argument.");
		break;
	case WSAEISCONN			:	// 10056
		LOG_I("[Socket]	WSAEISCONN : The socket is already connected.");
		break;
	case WSAEMFILE			:	// 10024
		LOG_I("[Socket]	WSAEMFILE : No more file descriptors are available.");
		break;
	case WSAEMSGSIZE		:	// 10040
		LOG_I("[Socket]	WSAEMSGSIZE : the length of message is larger than the predefined internal buffer size.");
		break;
	case WSAENETDOWN		:	// 10013
		LOG_I("[Socket]	WSAENETDOWN : The network subsystem has failed.");
		break;
	case WSAENETRESET		:	// 10052 - add hjchoi
		LOG_I("[Socket]	WSAENETRESET : The connection has been broken due to keep-alive activity detecting a failure while the operation was in progress.");
		break;
	case WSAENETUNREACH		:	// 10051
		LOG_I("[Socket]	WSAENETUNREACH : The network can't be reached from this host at this time.(unreached)");
		break;
	case WSAENOBUFS			:	// 10055
		LOG_I("[Socket]	WSAENOBUFS : No buffer space is available. The socket cannot be connected.");
		break;
	case WSAENOPROTOOPT		:	// 10042 - add hjchoi
		LOG_I("[Socket]	WSAENOPROTOOPT : An unknown, invalid or unsupported option or level was specified in a getsockopt or setsockopt call.");
		break;
	case WSAENOTCONN		:	// 10050
		LOG_I("[Socket]	WSAENOTCONN : The socket is not connected.");
		break;
	case WSAENOTSOCK		:	// 10038
		LOG_I("[Socket]	WSAENOTSOCK : Socket Descriptor was cleared.");
		break;
	case WSAEOPNOTSUPP		:	// 10045 - add hjchoi
		LOG_I("[Socket]	WSAEOPNOTSUPP : The attempted operation is not supported for the type of object referenced.");
		break;
	case WSAEPFNOSUPPORT	:	// 10046 - add hjchoi
		LOG_I("[Socket]	WSAEPFNOSUPPORT : The protocol family has not been configured into the system or no implementation for it exists.");
		break;
	case WSAEPROCLIM		:	// 10067 - add hjchoi
		LOG_I("[Socket]	WSAEPROCLIM : A Windows Sockets implementation may have a limit on the number of applications that can use it simultaneously.");
		break;
	case WSAEPROTONOSUPPORT :	// 10043 - add hjchoi
		LOG_I("[Socket]	WSAEPROTONOSUPPORT : The requested protocol has not been configured into the system, or no implementation for it exists.");
		break;
	case WSAEPROTOTYPE		:	// 10041 - add hjchoi
		LOG_I("[Socket]	WSAEPROTOTYPE : A protocol was specified in the socket function call that does not support the semantics of the socket type requested.");
		break;
	case WSAESHUTDOWN		:	// 10058 - add hjchoi
		LOG_I("[Socket]	WSAESHUTDOWN : A request to send or receive data was disallowed because the socket had already been shut down in that direction with a previous shutdown call.");
		break;
	case WSAESOCKTNOSUPPORT :	// 10044 - add hjchoi
		LOG_I("[Socket]	WSAESOCKTNOSUPPORT : The support for the specified socket type does not exist in this address family.");
		break;
	case WSAETIMEDOUT		:	// 10060
		LOG_I("[Socket]	WSAETIMEDOUT : Attempt to connect timed out without establishing a connection.");
		break;
	case WSAEWOULDBLOCK		:	// 10035 - It's not error. It means 'now connecting'.
		//LOG_I("WSAEWOULDBLOCK : A non-blocking socket operation could not be completed immediately.");
		break;
	case WSAHOST_NOT_FOUND	:	// 11001 - add hjchoi
		LOG_I("[Socket]	WSAHOST_NOT_FOUND : No such host is known.");
		break;
	case WSANOTINITIALISED	:	// 10093
		LOG_I("[Socket]	WSANOTINITIALISED : A successful WSAStartup call must occur before using this function.");
		break;
	case WSASYSNOTREADY		:	// 10091
		LOG_I("[Socket]	WSASYSNOTREADY : System not ready");
		break;
	case WSATRY_AGAIN		:	// 11002 - add hjchoi
		LOG_I("[Socket]	WSATRY_AGAIN : This is usually a temporary error during host name resolution and means that the local server did not receive a response from an authoritative server.");
		break;
	case WSAVERNOTSUPPORTED	:	// 10092 - add hjchoi
		LOG_I("[Socket]	WSAVERNOTSUPPORTED : The current Windows Sockets implementation does not support the Windows Sockets specification version requested by the application.");
		break;
	case WSAEBADF			:	// 10009
		LOG_I("[Socket]	WSAEBADF :  The file handle supplied is not valid.");
		break;
#ifdef MULTI_CAST
	//////////////////////////// Winsock I/O Mode Error //////////////////////////////////////////////
	case WSA_INVALID_HANDLE :
		LOG_I("[Socket]	WSA_INVALID_HANDLE : Specified event object handle is invalid.");
		break;
	case WSA_INVALID_PARAMETER :
		LOG_I("[Socket]	WSA_INVALID_PARAMETER : One or more parameters are invalid.");
		break;
	case WSA_IO_INCOMPLETE :
		LOG_I("[Socket]	WSA_IO_INCOMPLETE : Overlapped I/O event object not in signaled state.");
		break;
	case WSA_IO_PENDING :
		LOG_I("[Socket]	WSA_IO_PENDING : Overlapped operations will complete later.");
		break;
	case WSA_NOT_ENOUGH_MEMORY :
		LOG_I("[Socket]	WSA_NOT_ENOUGH_MEMORY : Insufficient memory available.");
		break;
	case WSANO_DATA :
		LOG_I("[Socket]	WSANO_DATA : Valid name, no data record of requested type.");
		break;
	case WSANO_RECOVERY :
		LOG_I("[Socket]	WSANO_RECOVERY : This is a nonrecoverable error.");
		break;
	case WSASYSCALLFAILURE :
		LOG_I("[Socket]	WSASYSCALLFAILURE : System call failure.");
		break;
	case WSAEDISCON :
		LOG_I("[Socket]	WSAEDISCON : Graceful shutdown in progress.");
		break;
	case WSA_OPERATION_ABORTED :
		LOG_I("[Socket]	WSA_OPERATION_ABORTED : Overlapped operation aborted.");
		break;
#endif//MULTI_CAST
	default:
		LOG_I("[Socket]	Unknown Error(%u)", lErrno);
	}
#else // Not Win32 Platform
	lErrno = errno;
	LOG_I("[Socket]	%s - ", szFunc);
	switch (lErrno)
	{
  case 0			:
		LOG_I("[Socket]	Disconnected (%u)", lErrno);
		break;
	case EBADF	:
		LOG_I("[Socket]	Invalid file descriptors is included.");
		break;
	case EINTR	:
		LOG_I("[Socket]	Interrupted function call.");
		break;
	case EINVAL	:
		LOG_I("[Socket]	Invalid argument.");
		break;
	case ENOMEM	:
		LOG_I("[Socket]	memory alloc failed.");
		break;
	case EINPROGRESS  :
		LOG_I("[Socket]	Operation now in progress. it's not error when it's nonblocking mode");
		break;

	default:
		LOG_I("[Socket]	Unknown Error(%u)", errno);
	}
#endif

	return	lErrno;
}
