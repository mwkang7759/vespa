
#include "SocketAPI.h"

LRSLT FrSocketInitNetwork() {
#ifdef	WIN32
	WORD			wVersionRequested;
	WSADATA			wsaData;
	INT32				err;

	wVersionRequested = MAKEWORD( 2, 2 );
	err = WSAStartup( wVersionRequested, &wsaData );
	if ( err != 0 )	{
		LOG_I("[Socket]	McSocketInitNetwork - Failed WSAStartup (%d)", err);
		return COMMON_ERR_NETWORK;
	}

	if(wsaData.wVersion != wVersionRequested) {
		LOG_I("[Socket]	McSocketInitNetwork - Not support Socket Version 2.2!");
		return COMMON_ERR_NETWORK;
	}
#endif // WIN32

	return	FR_OK;
}


void FrSocketCloseNetwork() {
#ifdef WIN32
	WSACleanup( );
#endif // WIN32

}

LRSLT McSocketGetHostName(char* pchHostName, DWORD32 dwBufLen)
{
#ifdef WIN32
	LRSLT	lRet = FR_OK;

	if(pchHostName == NULL || dwBufLen == 0)
		return COMMON_ERR_NODATA;

	lRet = gethostname(pchHostName, (INT32)dwBufLen);
	if(lRet == SOCKET_ERROR)
	{
		switch(lRet)
		{
		case WSAEFAULT:
			LOG_I("[Socket] McSocketGetHostName - The name parameter is not a valid part of the user address space, or the buffer size specified by namelen parameter is too small to hold the complete host name.");
			break;
		case WSANOTINITIALISED:
			LOG_I("[Socket] McSocketGetHostName - A successful WSAStartup call must occur before using this function.");
			break;
		case WSAENETDOWN:
			LOG_I("[Socket] McSocketGetHostName - The network subsystem has failed.");
			break;
		case WSAEINPROGRESS:
			LOG_I("[Socket] McSocketGetHostName - A blocking Windows Sockets 1.1 call is in progress, or the service provider is still processing a callback function.");
			break;
		}
		return COMMON_ERR_NETWORK;
	}
#endif//WIN32
	return FR_OK;
}

BOOL McSocketIsValid(SOCK_HANDLE hSock)
{
	if (hSock==NULL)
		return FALSE;

	return (hSock->m_hSocket!=INVALID_SOCKET);
}

VOID McSocketSetInValid(SOCK_HANDLE hSock)
{
	if (hSock)
		hSock->m_hSocket = INVALID_SOCKET;
}
