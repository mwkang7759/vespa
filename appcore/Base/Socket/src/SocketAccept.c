/*****************************************************************************
*                                                                            *
*                            Socket Library									 *
*                                                                            *
*   Copyright (c) 2004 by Mcubeworks, Incoporated. All Rights Reserved.      *
******************************************************************************

    File Name       : SocketAccept.c
    Author(s)       : Kim, Kun-Tae
    Created         : 11 Jan 2005

    Description     : SIP API
    Notes           :

==============================================================================
                      Modification History (Reverse Order)
==============================================================================
    Date        Author      Location (variables) / Description
------------------------------------------------------------------------------

------------------------------------------------------------------------------*/
#include "SocketAPI.h"
//#include "DtbWebSocketAPI.h"

LRSLT McSocketAccept(SOCK_HANDLE* phSock, SOCK_HANDLE hSock, DWORD32* pdwAddr, WORD* pwPort) {
	SOCKET_HANDLE			hSocket;

#ifdef _WEBSOCKET_ENABLE
    DWORD32                 dwAddress;
#endif

	struct sockaddr_in		this_sin;
	INT32					nLengthAddr;
	SOCK_HANDLE				hDtbSock=NULL;

	if (hSock==NULL) {
		TRACE("[Socket] McSocketAccept - null socket handle");
		return COMMON_ERR_NULLPOINTER;
	}

	this_sin.sin_family			= AF_INET;
	this_sin.sin_port			= htons(0);
	this_sin.sin_addr.s_addr	= htonl(INADDR_ANY);
	nLengthAddr					= sizeof(this_sin);


#if defined(WIN32) || defined(HPUX)
 #ifdef MULTI_CAST
	if ((hSocket = WSAAccept(hSock->m_hSocket, (struct sockaddr*)&this_sin, &nLengthAddr, NULL, 0)) == INVALID_SOCKET)
 #else
	if ((hSocket = accept(hSock->m_hSocket, (struct sockaddr*)&this_sin, &nLengthAddr)) == INVALID_SOCKET)
 #endif//MULTI_CAST
#else
	if ((hSocket = accept(hSock->m_hSocket, (struct sockaddr*)&this_sin, (socklen_t *)&nLengthAddr)) <= INVALID_SOCKET)
#endif
	{
		*phSock = NULL;
		GetErrorMessage("McSocketAccept");
		return	COMMON_ERR_CLI_ACCEPT;
	}

	if (pdwAddr != NULL)
		*pdwAddr = ntohl(this_sin.sin_addr.s_addr);
	if (pwPort != NULL)
		*pwPort = ntohs(this_sin.sin_port);

	// Memory alloc
	hDtbSock = (SOCK_HANDLE)MALLOCZ(sizeof(SOCKET_STRUCT));
	if (hDtbSock==NULL)
	{
		*phSock = NULL;
		LOG_E("[Socket] McSocketAccept memory alloc error");
		return COMMON_ERR_MEM;
	}
	// Init
	memcpy(hDtbSock, hSock, sizeof(SOCKET_STRUCT));

    hDtbSock->m_hSocket = hSocket;

#ifdef _WEBSOCKET_ENABLE

    if(hDtbSock->m_bWebSocketServer) {
        dwAddress = ntohl(this_sin.sin_addr.s_addr);

        if(DtbWebSocketAccept(hDtbSock, hSock, dwAddress) < 0) {
            LOG_E("[Socket] McSocketAccept DtbWebSocketAccept Error.");
#ifdef	WIN32

    		if (closesocket((SOCKET)hDtbSock->m_hSocket) == SOCKET_ERROR)
    			GetErrorMessage("McSocketClose");
#else
    		close((SOCKET)hDtbSock->m_hSocket);
#endif
            FREE(hDtbSock);
            *phSock = NULL;

            return COMMON_ERR_INVALID_PROTOCOL;
        }
    }
#endif

	*phSock = hDtbSock;

	return	FR_OK;
}
