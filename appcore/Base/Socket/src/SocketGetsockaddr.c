/*****************************************************************************
*                                                                            *
*                            Socket Library									 *
*                                                                            *
*   Copyright (c) 2004 by Mcubeworks, Incoporated. All Rights Reserved.      *
******************************************************************************

    File Name       : SocketGetsockaddr.c
    Author(s)       : Kim, Kun-Tae
    Created         : 11 Jan 2005

    Description     : SIP API
    Notes           :

==============================================================================
                      Modification History (Reverse Order)
==============================================================================
    Date        Author      Location (variables) / Description
------------------------------------------------------------------------------

------------------------------------------------------------------------------*/
#include "SocketAPI.h"

DWORD32 McSocketGetSockAddr(SOCK_HANDLE hSock) {
	struct sockaddr_in		this_sin;
	INT32					nLengthAddr;

	if (hSock==NULL) {
		TRACE("[Socket] McSocketGetSockAddr - null socket handle");
		return COMMON_ERR_NULLPOINTER;
	}

	nLengthAddr = sizeof(this_sin);

#if defined(WIN32) || defined(HPUX)
	if (getsockname(hSock->m_hSocket, (struct sockaddr*)&this_sin, &nLengthAddr) == SOCKET_ERROR)
#else
	if (getsockname(hSock->m_hSocket, (struct sockaddr*)&this_sin, (socklen_t*)&nLengthAddr) == SOCKET_ERROR)
#endif
    {
		GetErrorMessage("McSocketGetSockAddr");
    }

	return	ntohl(this_sin.sin_addr.s_addr);
}

WORD McSocketGetSockPort(SOCK_HANDLE hSock) {
	struct sockaddr_in		this_sin;
	INT32					nLengthAddr;

	if (hSock==NULL) {
		TRACE("[Socket] McSocketGetSockPort - null socket handle");
		return 0;
	}

	nLengthAddr = sizeof(this_sin);

#if defined(WIN32) || defined(HPUX)
	if (getsockname(hSock->m_hSocket, (struct sockaddr*)&this_sin, &nLengthAddr) == SOCKET_ERROR)
#else
	if (getsockname(hSock->m_hSocket, (struct sockaddr*)&this_sin, (socklen_t*)&nLengthAddr) == SOCKET_ERROR)
#endif
    {
		GetErrorMessage("McSocketGetSockPort");
    }

	return	ntohs(this_sin.sin_port);
}
