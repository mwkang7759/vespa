
#include "SocketAPI.h"

void McSocketClose(SOCK_HANDLE hSock) {
	if (hSock==NULL) {
		TRACE("[Socket] McSocketClose - null socket handle");
		return;
	}
    else {
        TRACE("[Socket] McSocketClose - hSocket (%d)", hSock->m_hSocket);
    }

    if (hSock->m_hSocket != INVALID_SOCK && hSock->m_hSocket != INVALID_SOCKET) {
#ifdef	WIN32
    	if (closesocket((SOCKET)hSock->m_hSocket) == SOCKET_ERROR)
    		GetErrorMessage("McSocketClose");
#else
    	close((SOCKET)hSock->m_hSocket);
#endif
    }

    if (hSock->m_bSSL)
    	TRACE("[Socket] McSocketClose - 0x%x - SSL", hSock);
    else
    	TRACE("[Socket] McSocketClose - 0x%x", hSock);

	FREE(hSock);
}

BOOL McSocketCheckClosed(SOCK_HANDLE hSock, DWORD32 dwTimeout) {
	LRSLT lRet = FR_OK;
	FD_SET_TYPE	rSet;
	DWORD32 dwBaseTick, dwCurTick;
	char szBuffer[10000];

	if (hSock==NULL) {
		TRACE("[Socket] McSocketCheckClosed - null socket handle");
		return TRUE;
	}

	if (hSock->m_hSocket != INVALID_SOCK && hSock->m_hSocket != INVALID_SOCKET)	{
		FD_ZERO(&rSet);
		FD_SET(hSock->m_hSocket, &rSet);

		dwBaseTick = FrGetTickCount();

		while(1) {
			lRet = McSocketSelect2(&rSet, NULL, NULL, dwTimeout);

			LOG_I("[Socket]	McSocketCheckClosed : lRet(0x%x) : hSock:(0x%x)", (UINT32)lRet, (UINT32)hSock->m_hSocket);

			if (lRet == (LRSLT)COMMON_ERR_SOCKET)
				break;
			else if (lRet == (LRSLT)COMMON_ERR_TIMEOUT)
				break;
			else if (FRSUCCEEDED(lRet))	{
				lRet = McSocketRecv(hSock, szBuffer, 10000);
				if (FRFAILED(lRet))	{
					lRet = COMMON_ERR_SOCKET;
					break;
				}
			}

			dwCurTick = FrGetTickCount();

			if (McGetSubtract(dwCurTick, dwBaseTick) >= dwTimeout)
				break;
			else
				FrSleep(100);
		}
	}
	else
	{
		LOG_I("[Socket]	McSocketCheckClosed : It's INVALID_SOCK");
		return TRUE;
	}

	if (lRet == (LRSLT)COMMON_ERR_SOCKET)
		return TRUE;
	else
		return FALSE;

	return TRUE;
}
