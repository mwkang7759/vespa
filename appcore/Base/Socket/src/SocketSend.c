/*****************************************************************************
*                                                                            *
*                            Socket Library									 *
*                                                                            *
*   Copyright (c) 2004 by Mcubeworks, Incoporated. All Rights Reserved.      *
******************************************************************************

    File Name       : SocketSend.c
    Author(s)       : Kim, Kun-Tae
    Created         : 11 Jan 2005

    Description     : SIP API
    Notes           :

==============================================================================
                      Modification History (Reverse Order)
==============================================================================
    Date        Author      Location (variables) / Description
------------------------------------------------------------------------------

------------------------------------------------------------------------------*/
#include "SocketAPI.h"
//#include "LibOpenSSLAPI.h"
//#include "DtbWebSocketAPI.h"

LRSLT McSocketSend(SOCK_HANDLE hSock, char *pBuf, INT32 iLen) {
	INT32			iSend = 0;
	INT32			iTotalSendSize = 0;

	if (hSock==NULL) {
		TRACE("[Socket] McSocketSend - null socket handle");
		return COMMON_ERR_NULLPOINTER;
	}

	while(1) {
#ifdef _WEBSOCKET_ENABLE
        if (hSock->m_hWebSocket) {
            iSend = WebSocket_Send(hSock->m_hWebSocket, pBuf+iTotalSendSize, iLen-iTotalSendSize, 1);
        }
		else
#endif
        if (hSock->m_bSSL) {
			//iSend = DtbSSLWrite(hSock->m_hSsl, pBuf+iTotalSendSize, iLen-iTotalSendSize);
		}
		else {
			iSend = send((SOCKET)hSock->m_hSocket, pBuf+iTotalSendSize, iLen-iTotalSendSize, 0);
		}

		if (iSend == SOCKET_ERROR || !iSend) {
            /*
			switch(GetErrorMessage("McSocketSend")) {
                case EAGAIN :
                case EWOULDBLOCK :
                case EINTR :
                    return 0;
			}
			*/
            GetErrorMessage("McSocketSend");
			return	COMMON_ERR_SEND;
		}

		iTotalSendSize += iSend;
		if (iTotalSendSize == iLen)
			break;
		else
			FrSleep(10);
	}

	return	(LRSLT)iTotalSendSize;
}

LRSLT McSocketOneSend(SOCK_HANDLE hSock, char *pBuf, INT32 iLen) {
	INT32			iSend = 0;
	INT32			iTotalSendSize = 0;

	if (hSock==NULL) {
		TRACE("[Socket] McSocketSend - null socket handle");
		return COMMON_ERR_NULLPOINTER;
	}
	
#ifdef _WEBSOCKET_ENABLE
    if (hSock->m_hWebSocket) {
        iSend = WebSocket_Send(hSock->m_hWebSocket, pBuf+iTotalSendSize, iLen-iTotalSendSize, 1);
    }
	else
#endif
    if (hSock->m_bSSL) {
		//iSend = DtbSSLWrite(hSock->m_hSsl, pBuf+iTotalSendSize, iLen-iTotalSendSize);
	}
	else {
		iSend = send((SOCKET)hSock->m_hSocket, pBuf+iTotalSendSize, iLen-iTotalSendSize, 0);
	}

	if (iSend == SOCKET_ERROR || !iSend) {
        /*
		switch(GetErrorMessage("McSocketSend")) {
            case EAGAIN :
            case EWOULDBLOCK :
            case EINTR :
                return 0;
		}
		*/
        GetErrorMessage("McSocketSend");
		return	COMMON_ERR_SEND;
	}
	iTotalSendSize += iSend;

	return	(LRSLT)iTotalSendSize;
}


LRSLT McSocketMsgSend(SOCK_HANDLE hSock, char *pBuf, INT32 iLen) {
	INT32			nLen = iLen;
	INT32			nSendSize=0, iSend=0;
	char			szBuffer[MAX_MSG_LEN]="";	// 4 kbytes
	char*			pos = pBuf;

	if (hSock==NULL) {
		TRACE("[Socket] McSocketMsgSend - null socket handle");
		return COMMON_ERR_NULLPOINTER;
	}

	while (nLen) {
		memset(szBuffer, 0, MAX_MSG_LEN);
		if (nLen >= MAX_MSG_LEN) {
			memcpy(szBuffer, pos, MAX_MSG_LEN);
			pos += MAX_MSG_LEN;
			nSendSize = MAX_MSG_LEN;
		}
		else {
			memcpy(szBuffer, pos, nLen);
			nSendSize = nLen;
		}

#ifdef _WEBSOCKET_ENABLE
        if (hSock->m_hWebSocket) {
            iSend = WebSocket_Send(hSock->m_hWebSocket, szBuffer, nSendSize, 1);
        }
        else
#endif
		if (hSock->m_bSSL) {
			//iSend = DtbSSLWrite(hSock->m_hSsl, szBuffer, nSendSize);
		}
		else {
			iSend = send((SOCKET)hSock->m_hSocket, szBuffer, nSendSize, 0);
		}

		if (iSend == SOCKET_ERROR || !iSend) {
			GetErrorMessage("McSocketMsgSend");
			return	COMMON_ERR_SEND;
		}

		nLen -= iSend;
	}

	return FR_OK;
}

INT32 DtbSocketSend(SOCK_HANDLE hSock, const char* ptr, INT32 nbytes) {
	INT32			nsendbytes=0, nleft;

	if (hSock==NULL) {
		TRACE("[Socket] DtbSocketSend - null socket handle");
		return COMMON_ERR_NULLPOINTER;
	}

	nleft = nbytes;
	while(nleft > 0) {
#ifdef _WEBSOCKET_ENABLE
        if (hSock->m_hWebSocket) {
            nsendbytes = WebSocket_Send(hSock->m_hWebSocket, (void*)ptr, nleft, 1);
        }
        else
#endif
		if (hSock->m_bSSL) {
            //nsendbytes = DtbSSLWrite(hSock->m_hSsl, ptr, nleft);
		}
		else {
			nsendbytes = send((SOCKET)hSock->m_hSocket, ptr, nleft, 0);
		}

		if(nsendbytes == 0)	// remote host gracefully close
		{
			LOG_I("[DtbSocketSend] Disconnected : remote host gracefully close.......!!");
			return COMMON_ERR_SEND;
		}
		else if(nsendbytes == SOCKET_ERROR) {
			GetErrorMessage("[DtbSocketSend]");
			return COMMON_ERR_SEND;
		}
		else {
			nleft -= nsendbytes;
			ptr   += nsendbytes;
		}
	}

	return(nbytes - nleft);
}

LONG McSocketSendNonBlock(SOCK_HANDLE hSock, char *pBuf, int iLen, int mstime) {
	int				iSend=0;
    fd_set			wset;
    struct timeval	t;

	if (hSock==NULL) {
		TRACE("[Socket] McSocketSendNonBlock - null socket handle");
		return COMMON_ERR_NULLPOINTER;
	}

    // 1 milsec 단위로 변경
    t.tv_sec = mstime / 1000;
    t.tv_usec = (mstime % 1000) * 1000;

    FD_ZERO(&wset);
    FD_SET((u_int)hSock->m_hSocket, &wset);

    iSend = select(hSock->m_hSocket+1, NULL, &wset, NULL, ((mstime != 0) ? &t : NULL));

    if (!iSend)
		return	COMMON_ERR_TIMEOUT;
	else if (iSend == SOCKET_ERROR)
	{
		GetErrorMessage("McSocketSelect");
		return	COMMON_ERR_SOCKET;
	}

#ifdef _WEBSOCKET_ENABLE
    if (hSock->m_hWebSocket) {
        iSend = WebSocket_Send(hSock->m_hWebSocket, pBuf, iLen, 1);
    }
    else
#endif
	if (hSock->m_bSSL) {
		//iSend = DtbSSLWrite(hSock->m_hSsl, pBuf, iLen);
	}
	else {
		iSend = send((SOCKET)hSock->m_hSocket, pBuf, iLen, 0);
	}

	if (iSend == SOCKET_ERROR || !iSend) {
		GetErrorMessage("McSocketSend");
		return	COMMON_ERR_SEND;
	}

	return	(LONG)iSend;
}
