
#include "SocketAPI.h"
//#include "LibOpenSSLAPI.h"
//#include "DtbWebSocketAPI.h"

LRSLT McSocketOpen(SOCK_HANDLE* phSock, INT32 iType) {
	SOCKET			hSock;
	SOCK_HANDLE		hDtbSock;

	INT32		opt = 1;
    //INT32       size = 0;
	//long	lHow = 1;

	// Memory alloc
	hDtbSock = (SOCK_HANDLE)MALLOCZ(sizeof(SOCKET_STRUCT));
	if (hDtbSock==NULL)	{
		*phSock = NULL;
		LOG_E("[Socket] McSocketOpen memory alloc error");
		return COMMON_ERR_MEM;
	}
	// Init
	hDtbSock->m_hSocket = INVALID_SOCKET;

    if (iType == FR_SOCK_STREAM)
    	iType = SOCK_STREAM;
    else
    	iType = SOCK_DGRAM;

    hSock = socket(PF_INET, iType, 0);
    if (hSock == INVALID_SOCKET)
    	goto failed;

    /* buffer size */
#ifdef	WIN32
    opt = 0x40000;
    if (setsockopt(hSock, SOL_SOCKET, SO_RCVBUF, (char*)&opt, sizeof(opt)) == SOCKET_ERROR)
    	goto failed;

    opt = 0x40000;
    if (setsockopt(hSock, SOL_SOCKET, SO_SNDBUF, (char*)&opt, sizeof(opt)) == SOCKET_ERROR)
    	goto failed;

#elif defined(Linux) || defined(SunOS) || defined(HPUX) || defined(SUPPORT_IOS)
    opt = 256000;
    if (setsockopt(hSock, SOL_SOCKET, SO_SNDBUF, (char *)&opt, (socklen_t)sizeof(opt)))
    	goto failed;

    opt = 256000;
    if (setsockopt(hSock, SOL_SOCKET, SO_RCVBUF, (char *)&opt, sizeof(opt)))
    	goto failed;
#endif

    /* Reuse Port */
    opt = 0;
    if (setsockopt(hSock, SOL_SOCKET, SO_REUSEADDR, (char*)&opt, sizeof(opt)) == SOCKET_ERROR)
    	goto failed;

    // by default
    hDtbSock->m_bSSL = FALSE;
    hDtbSock->m_hSocket = (SOCKET_HANDLE)hSock;
    hDtbSock->m_hCtx = NULL;
    hDtbSock->m_hSsl = NULL;


    *phSock = hDtbSock;

	TRACE("[Socket] McSocketOpen - 0x%x", hDtbSock);

    return	FR_OK;

failed:
	FREE(hDtbSock);
	*phSock = NULL;

#ifdef	WIN32
    closesocket(hSock);
    GetErrorMessage("McSocketOpen");
#elif defined(Linux) || defined(SunOS) || defined(HPUX)
    close(hSock);
#endif
	return	COMMON_ERR_SOCKET;
}

/////////////////////////////////////////////////////////////////
//		TCP Open
/////////////////////////////////////////////////////////////////
LRSLT McSocketTcpOpen(SOCK_HANDLE* phSock, char* pAddr, WORD wPort, BOOL bServer, DWORD32 dwTimeout) {
	LRSLT		lRet;

	lRet = McSocketOpen(phSock, FR_SOCK_STREAM);
	if (FRFAILED(lRet))
		return	lRet;

	if (bServer) {
        TRACE("[McSocketTcpOpen] McSocketBind ");
		if (!McSocketBind(*phSock, wPort, TRUE)) {
			McSocketClose(*phSock);
			*phSock = INVALID_SOCK;
			return	COMMON_ERR_SOCKET;
		}
	}
	else
	{
        TRACE("[McSocketTcpOpen] McSocketConnect time out %d", dwTimeout);
		lRet = McSocketConnect(*phSock, pAddr, wPort, dwTimeout);
        if (FRFAILED(lRet))	{
			McSocketClose(*phSock);
			*phSock = INVALID_SOCK;
			return	lRet;
		}
	}

	return	FR_OK;
}

LRSLT McSocketTcpOpen2(SOCK_HANDLE* phSock, char* pAddr, WORD wPort, BOOL bServer, DWORD32 dwTimeout, BOOL bSSL) {
   LRSLT        lRet;

   bServer = FALSE;
   lRet = McSocketTcpOpen(phSock, pAddr, wPort, bServer, dwTimeout);
   if (FRFAILED(lRet))
       return  lRet;

   if (bSSL)
	   TRACE("[Socket] McSocketOpen2 - 0x%x - SSL", *phSock);
   else
	   TRACE("[Socket] McSocketOpen2 - 0x%x", *phSock);

   //if (bSSL)
	//   return DtbSSLClientOpen(*phSock, dwTimeout);
   //else
	 return FR_OK;
}

LRSLT McSocketWebSocketOpen(SOCK_HANDLE* phSock, char* pAddr, WORD wPort, char* pUri, BOOL bServer, DWORD32 dwTimeout, BOOL bSSL, DTB_SSL_SERVER_INFO_HANDLE hSSLServerInfo)
{
#ifdef _WEBSOCKET_ENABLE
    LRSLT        lRet;

	//bServer = FALSE;
    lRet = McSocketTcpOpen(phSock, pAddr, wPort, bServer, dwTimeout);
    if (FRFAILED(lRet))
    return  lRet;

    if (bSSL)
        TRACE("[Socket] McSocketWebSocketOpen - 0x%x - SSL", *phSock);
    else
        TRACE("[Socket] McSocketWebSocketOpen - 0x%x", *phSock);

    if(!bServer) {
        // Client
        if (bSSL) {
            lRet = DtbSSLClientOpen(*phSock, dwTimeout);
        	if (lRet != FR_OK)
        	{
        		TRACE("[Socket] McSocketWebSocketOpen - SSL open error 0x%x", lRet);
        		McSocketClose(*phSock);
        		return lRet;
        	}
        }

        if((lRet = DtbWebSocketClientOpen(*phSock, pAddr, wPort, pUri)) != 0)
    	{
            McSocketClose(*phSock);
            return COMMON_ERR_SOCKET;
        }
    }
    else {
        // Server
        if(bSSL) {
            lRet = DtbSSLServerOpen(*phSock, hSSLServerInfo);
            if(lRet != FR_OK) {
                TRACE("[Socket] McSocketWebSocketOpen (SSL Server) - SSL open error 0x%x", lRet);
        		McSocketClose(*phSock);
        		return lRet;
            }
        }

        if((lRet = DtbWebSocketServerOpen(*phSock, wPort)) != 0)
    	{
            McSocketClose(*phSock);
            return COMMON_ERR_SOCKET;
        }
    }
#else
    TRACE("[Socket] McSocketWebSocketOpen - not support..");
    return COMMON_ERR_SOCKET;
#endif

    return FR_OK;
}


/////////////////////////////////////////////////////////////////
//		UDP Open
/////////////////////////////////////////////////////////////////
LRSLT McSocketUdpOpen2(SOCK_HANDLE* phSock, char* pMultiAddr, char* pLocalAddr, WORD* pwPort, BOOL bMulti, BYTE nMulticastTTL)
{
	LRSLT		lRet;
	//INT32		opt = 1;
	//INT32			i;

	lRet = McSocketOpen(phSock, FR_SOCK_DGRAM);
	if (FRFAILED(lRet))
		return	lRet;

	if (bMulti)	{
		if (!McSocketMulticastTTL(*phSock, pMultiAddr, pLocalAddr, *pwPort, nMulticastTTL))	{
			McSocketClose(*phSock);
			*phSock = INVALID_SOCK;
			return	COMMON_ERR_SOCKET;
		}
	}
	else
	{
		lRet = McSocketBind(*phSock, INADDR_ANY, FALSE);
		if (FRFAILED(lRet))
			return	lRet;

		*pwPort = McSocketGetSockPort(*phSock);
	}

    return	FR_OK;
}

LRSLT McSocketUdpOpen(SOCK_HANDLE* phSock, char* pMultiAddr, char* pLocalAddr, WORD* pwPort, BOOL bMulti)
{
	LRSLT		lRet;
	//INT32		opt = 1;
	//INT32			i;

	lRet = McSocketOpen(phSock, FR_SOCK_DGRAM);
	if (FRFAILED(lRet))
		return	lRet;

	if (bMulti)	{
		if (!McSocketMulticastJoin(*phSock, pMultiAddr, pLocalAddr, *pwPort)) {
			McSocketClose(*phSock);
			*phSock = INVALID_SOCK;
			return	COMMON_ERR_SOCKET;
		}
	}
	else
	{
		lRet = McSocketBind(*phSock, INADDR_ANY, FALSE);
		if (FRFAILED(lRet))
			return	lRet;

		*pwPort = McSocketGetSockPort(*phSock);
	}

	return	FR_OK;
}


LRSLT McSocketUdpOpenFixed(SOCK_HANDLE* phSock, char* pMultiAddr, char* pLocalAddr, WORD* pwPort, BOOL bMulti)
{
	LRSLT		lRet;
	INT32			i;

	lRet = McSocketOpen(phSock, FR_SOCK_DGRAM);
	if (FRFAILED(lRet))
		return	lRet;

	if (bMulti)	{
		if (!McSocketMulticastJoin(*phSock, pMultiAddr, pLocalAddr, *pwPort)) {
			McSocketClose(*phSock);
			*phSock = INVALID_SOCK;
			return	COMMON_ERR_SOCKET;
		}
	}
	else
	{
		for (i = 0;i < 200;i++)	{
			if (McSocketBind(*phSock, *pwPort, FALSE))
				break;
			*pwPort += 2;
		}
		if (i == 200) {
			McSocketClose(*phSock);
			*phSock = INVALID_SOCK;
			return	COMMON_ERR_SOCKET;
		}
	}

	return	FR_OK;
}

LRSLT McSocketUdpOpenFixed2(SOCK_HANDLE* phSock, char* pMultiAddr, char* pLocalAddr, WORD* pwPort, BOOL bMulti) {
	LRSLT		lRet;
	
	lRet = McSocketOpen(phSock, FR_SOCK_DGRAM);
	if (FRFAILED(lRet))
		return	lRet;

	if (bMulti)	{
		if (!McSocketMulticastJoin(*phSock, pMultiAddr, pLocalAddr, *pwPort)) {
			McSocketClose(*phSock);
			*phSock = INVALID_SOCK;
			return	COMMON_ERR_SOCKET;
		}
	}
	else
	{
		if (!McSocketBind2(*phSock, pLocalAddr, *pwPort, FALSE)) {
			McSocketClose(*phSock);
			*phSock = INVALID_SOCK;
			return	COMMON_ERR_SOCKET;
		}
	}

	return	FR_OK;
}


LRSLT McSocketUdpOpenRangeFixed(SOCK_HANDLE* phSock, char* pMultiAddr, char* pLocalAddr, WORD* pwPort, BOOL bMulti, DWORD32 dwRange)
{
	LRSLT		lRet;
	DWORD32		i;

	lRet = McSocketOpen(phSock, FR_SOCK_DGRAM);
	if (FRFAILED(lRet))
		return	lRet;

	if (bMulti) {
		if (!McSocketMulticastJoin(*phSock, pMultiAddr, pLocalAddr, *pwPort)) {
			McSocketClose(*phSock);
			*phSock = INVALID_SOCK;
			return	COMMON_ERR_SOCKET;
		}
	}
	else
	{
		for (i = 0;i < dwRange;i++)	{
			if (McSocketBind(*phSock, *pwPort, FALSE))
				break;
			*pwPort += 2;
		}
		if (i == dwRange) {
			McSocketClose(*phSock);
			*phSock = INVALID_SOCK;
			return	COMMON_ERR_SOCKET;
		}
	}

	return	FR_OK;
}
