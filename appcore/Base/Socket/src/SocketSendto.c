/*****************************************************************************
*                                                                            *
*                            Socket Library									 *
*                                                                            *
*   Copyright (c) 2004 by Mcubeworks, Incoporated. All Rights Reserved.      *
******************************************************************************

    File Name       : SocketSendto.c
    Author(s)       : Kim, Kun-Tae
    Created         : 11 Jan 2005

    Description     : SIP API
    Notes           :

==============================================================================
                      Modification History (Reverse Order)
==============================================================================
    Date        Author      Location (variables) / Description
------------------------------------------------------------------------------

------------------------------------------------------------------------------*/
#include "SocketAPI.h"

LRSLT McSocketSendTo(SOCK_HANDLE hSock, char* pBuf, INT32 iLen, char* pAddr, WORD wPort) {
	struct			sockaddr_in	this_sin;
	INT32			iSend;

	if (hSock==NULL) {
		TRACE("[Socket] McSocketSendTo - null socket handle");
		return COMMON_ERR_NULLPOINTER;
	}

#ifdef _WEBSOCKET_ENABLE
    if (hSock->m_hWebSocket)
    {
        TRACE("[Socket] McSocketSendTo - websocket socket handle (NOT SUPPORT)");
        return FR_FAIL;
    }
#endif

	if (hSock->m_bSSL)	{
		TRACE("[Socket] McSocketSendTo - ssl socket handle (NOT SUPPORT)");
		return FR_FAIL;
	}

	this_sin.sin_family			= AF_INET;
	this_sin.sin_port			= htons(wPort);
	if (ISDIGIT(pAddr[0]))
		this_sin.sin_addr.s_addr = INET_ADDR(pAddr);
	else
		this_sin.sin_addr.s_addr = McSocketGetHostByName(pAddr);

	iSend = sendto((SOCKET)hSock->m_hSocket, pBuf, iLen, 0, (struct sockaddr*)&this_sin, sizeof(this_sin));
	if (iSend == SOCKET_ERROR || !iSend) {
		GetErrorMessage("McSocketSendTo");
		return	COMMON_ERR_SEND;
	}

	return	(LRSLT)iSend;
}
