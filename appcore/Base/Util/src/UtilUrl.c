
#include "Util.h"

TCHAR* UtilURLGetContentLocation(const TCHAR *url)
{
	TCHAR*	pAddr;
	TCHAR*	pPath;
	TCHAR*	pPort;
	DWORD32	dwLen;

	if (!url)
		return	NULL;

	pPath = UtilStrGetAddress((TCHAR*)url, &pAddr, &dwLen);
	pPort = UtilStrFindString(pPath, ":");
	if (pPort)
		pPath = UtilStrFindString(pPath, "/");

	if (!pPath)
		return	NULL;

	if (!_tcslen(pPath))
		return	NULL;

	return	UtilStrStrdup(pPath);
}

TCHAR* UtilURLGetContentBase(const TCHAR *url)
{
	TCHAR*	pFilename = _tcsrchr((char*)url, (INT32)('/'))+1;
	if (!url)
		return	NULL;
	return	UtilStrStrdupn(url, (INT32)(pFilename-url));
}

char* UtilURLGetAddr(const char *url)
{
	char*	pAddr;
	DWORD32	dwLen;

	if (!url)
		return	NULL;

	if( !UtilStrGetAddress((CHAR*)url, &pAddr, &dwLen) )
		return NULL;

	return	UtilStrStrdupn(pAddr, dwLen);
}

WORD UtilURLGetPort(const char *url)
{
	char*	pAddr;
	char*	pPath;
	DWORD32	dwLen;
	char*	pPort;
	DWORD32	dwPort;

	if (!url)
		return	0;

	pPath = UtilStrGetAddress((CHAR*)url, &pAddr, &dwLen);
	if(pPath[0] == ':')	
	{
		pPort = UtilStrFindString(pPath, ":");
		if (pPort)
		{
			UtilStrGetDecNumber(pPort, NULL, &dwPort);
			return (WORD)dwPort;
		}
	}
	return 0;
}

// Various URL Forms
/*
Case 1   : rtsp://url/filename
Case 2   : rtsp://url/filename?params
Case 3-1 : rtsp://url/filename?params&url
Case 3-2 : rtsp://url/filename?params&url/filename
Case 4   : rtsp://url/filename?params&url/filename?params

Case 4 Example : 
rtsp://211.234.235.48:662/0123456789.skm?sctype=9&svctype=0&svcdomain=101&surl=http://adimg.donga.com/madv/img/outback/480_360.flv?playtm=15
*/

TCHAR* UtilURLGetFilename(const TCHAR *url)
{
	TCHAR*	pFilename;
	TCHAR*	pQuestion;

	if (!url)
		return	NULL;

	pFilename = _tcsrchr((char*)url, (INT32)('/'));
	if (!pFilename)
		pFilename = _tcsrchr((char*)url, (INT32)('\\'));

	if (!pFilename)
		return	NULL;
	pFilename++;

	pQuestion = _tcsrchr((char*)url, (INT32)('?'));

	if (pQuestion)
	{
		// Case 2, 4
		if(pQuestion > pFilename)
			return 	UtilStrStrdupn(pFilename, (INT32)(pQuestion-pFilename));
	}

	// Case 1, 3
	return	UtilStrStrdup(pFilename);
}

TCHAR* UtilURLGetRandFilename(const TCHAR *url)
{
	char*	pTempFile;
	char*	pFilename;
	DWORD32	dwPos = 0;

	if (!url)
		return	NULL;

	pFilename = _tcsrchr((char*)url, (INT32)('/'));
	if (!pFilename)
		pFilename = _tcsrchr((char*)url, (INT32)('\\'));

	if (!pFilename)
		return	NULL;

	pFilename++;

	while(!ISNOFILENAME(pFilename[dwPos]))
		dwPos++;

	//pTempFile = (CHAR*)MALLOC(MAX_STRING_LENGTH);
	pTempFile = (TCHAR*)MALLOC(MAX_URL_LENGTH * sizeof(TCHAR));
	if (pTempFile)
	{
		if(!dwPos)
		{
			_stprintf(pTempFile, _T("mcube_%u_%u"), McGetPid(), FrGetTickCount());
		}
		else
		{
			// change rand name, ex) xxx.avi_12322_3 =>, xxx_12322_3.avi
			//char	szExt[1024]="";
			//char	szTemp[1024]="";
			//char	szTemp2[1024]="";
			char	*pszExt = (CHAR*)MALLOCZ(1024);
			char	*pszTemp =(CHAR*)MALLOCZ(1024);
			char	*pszTemp2 =(CHAR*)MALLOCZ(1024);
			char	*pName = NULL;
			char	*pTemp = NULL;
			char	*pQMark = NULL;

			if (!pszExt || !pszTemp || !pszTemp2)
			{
				FREE(pTempFile);
				FREE(pszExt);
				FREE(pszTemp);
				FREE(pszTemp2);
				return NULL;
			}

			pQMark = strrchr((char*)pFilename, (INT32)('?'));
			if (pQMark)
			{
				strncpy(pszTemp2, pFilename, pQMark - pFilename);
				pName = strrchr((char*)pszTemp2, (INT32)('.'));
			}
			else
				pName = strrchr((char*)pFilename, (INT32)('.'));			

			if (pName)
			{
				if (pQMark)
					strncpy(pszTemp, pFilename, pName - pszTemp2);
				else
					strncpy(pszTemp, pFilename, pName - pFilename);

				pTemp = strrchr((char*)(pName+1), (INT32)('?'));
				if (pTemp)
					strncpy(pszExt, pName+1, pTemp-pName-1);
				else
					strcpy(pszExt, pName+1);
			}
			else
				strcpy(pszTemp, pFilename);

			sprintf(pTempFile, "%s_%u_%u.%s", pszTemp, McGetPid(), FrGetTickCount(), pszExt);

			FREE(pszExt);
			FREE(pszTemp);
			FREE(pszTemp2);

			//_tcsncpy(pTempFile, pFilename, dwPos);
			//_stprintf(pTempFile + dwPos, _T("_%u_%u"), McGetProcessNum(), McGetTickCount());
		}
		return pTempFile;
	}	
	return	NULL;
}

TCHAR* UtilURLGetFoldername(const TCHAR *url)
{
	TCHAR*	pPath = strrchr((char*)url, (INT32)('/'));
	TCHAR*	pFoldername = strchr((char*)url, (INT32)('/'))+1;

	if (!url)
		return	NULL;
	pFoldername = strchr(pFoldername, (INT32)('/'))+1;
	return	UtilStrStrdupn(pFoldername, (INT32)(pPath-pFoldername));
}

TCHAR* UtilURLGetFileExt(const TCHAR *filename)
{
	TCHAR*	pDot = strrchr((char*)filename, (INT32)('.'));
	if (!filename)
		return	NULL;
	return	UtilStrStrdup(pDot);
}

void UtilURLConvWin32FilePath(TCHAR *filename)
{
	INT32		i, n = (INT32)strlen(filename);

	for (i = 0;i < n;i++)
	{
		if (filename[i] == '/')
			filename[i] = '\\';
	}
}

char* UtilURLEncoding(char *enc, char *src)
{
	char buf[2+1];
	INT32 i, j;
	unsigned char c;
	if(src==0||enc==0) return 0;

	/*for(i=j= 0; src[i]; i++)
	{
		c = (unsigned char)src[i];
		if (c == ' ') enc[j++] = '+';
		if ((c >= '0') && (c <= '9')) enc[j++] = c;
		else if ((c >= 'A') && (c <= 'Z')) enc[j++] = c;
		else if ((c >= 'a') && (c <= 'z')) enc[j++] = c;
		else if ((c == '@') || (c == '.')) enc[j++] = c;
		else {
			sprintf(buf, "%02x", c);
			enc[j++] = '%';
			enc[j++] = buf[0];
			enc[j++] = buf[1];
		}
	}*/

	for(i = j = 0; src[i]; i++) 
	{
		c = (unsigned char)src[i];
		if((c >= '0') && (c <= '9')) enc[j++] = c;
		else if((c >= 'A') && (c <= 'Z')) enc[j++] = c;
		else if((c >= 'a') && (c <= 'z')) enc[j++] = c;
		else if((c == '@') || (c == '.') || (c == '/') || (c == '\\')
			|| (c == '-') || (c == '_') || (c == ':') ) 
			enc[j++] = c;
		else 
		{
			sprintf(buf, "%02x", c);
			enc[j++] = '%';
			enc[j++] = buf[0];
			enc[j++] = buf[1];
		}
	}

	enc[j] = '\0';

	return enc;
}

char *UtilURLDecoding(char *dec, char *src)
{
	INT32 i, j;
	unsigned char c=0;
	if(src==0 || dec==0) return 0;

	for(i=j=0; src[i]; i++,j++)
	{
		if(src[i] == '+') 
		{
			dec[j] = ' ';
			continue;
		}

		dec[j] = src[i];
		if(src[i] != '%') continue;

		c = src[++i];
		if(c >= '0' && c <= '9')            dec[j] = c - '0';
		else if(c >= 'A' && c <= 'F')        dec[j] = c - 55;
		else if(c >= 'a' && c <= 'f')        dec[j] = c - 87;
		else return 0;

		c = src[++i];
		dec[j] *= 16;
		if(c >= '0' && c <= '9')          dec[j] += (c - '0');
		else if(c >= 'A' && c <= 'F')     dec[j] += (c - 55);
		else if(c >= 'a' && c <= 'f')     dec[j] += (c - 87);
		else return 0;
	}

	dec[j] = '\0';
	return dec;
}
