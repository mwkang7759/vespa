
#include "Util.h"
#include "TraceAPI.h"

#define		MAX_ALLOWED_AUDIO_TIME_DIFFERENCE	200

// Time
#if 0
typedef struct
{
	BOOL			m_bStop;
	MUTEX_HANDLE	m_xLock;
	float			m_fRatio;

	DWORD32			m_dwCurTime;		// Current audio cts just played out, namely, periodically updated by AudioDevOut (McTimeSetCurTime())
										// Never updated for video-only contents
										// Or the same as m_dwRefCTS. (McTimeSetRefTime())
	DWORD32			m_dwPrevTime;		// Set when m_dwCurTime is set
										// Same as m_dwCurTime for video-only contents
	DWORD32			m_dwRefCTS;			// Set when the playout starts. 
										// or Reset when the difference between the cts of source audio and that of audio just played is slightly large
	DWORD32			m_dwTick;			// TickCount when m_dwRefCTS are reset

} McTimeStruct;
#endif

FrTimeHandle FrTimeOpen(DWORD32 dwStart, float fRatio) {
	FrTimeStruct*	hTime;

	hTime = (FrTimeStruct*)MALLOC(sizeof(FrTimeStruct));
	if (hTime) {
		memset(hTime, 0, sizeof(FrTimeStruct));
		hTime->m_fRatio = fRatio;
		hTime->m_dwRefCTS = dwStart;
		hTime->m_dwCurTime = dwStart;
		hTime->m_dwPrevTime = dwStart;
		hTime->m_bStop = TRUE;

		FrCreateMutex(&hTime->m_xLock);
	}

	return	hTime;
}

void FrTimeClose(FrTimeHandle hHandle) {
	FrTimeStruct*	hTime = (FrTimeStruct*)hHandle;

	if (hTime) {
		FrDeleteMutex(&hTime->m_xLock);
		FREE(hTime);
	}
}

BOOL FrTimeStart(FrTimeHandle hHandle) {
	FrTimeStruct*	hTime = (FrTimeStruct*)hHandle;

	if (!hTime)
		return	FALSE;

	FrEnterMutex(&hTime->m_xLock);
	if (!hTime->m_bStop) {
		LOG_D("[UtilTime]	McTimeStart Stop 1 RefTime %d CurTime %d Tick %d", hTime->m_dwRefCTS, hTime->m_dwCurTime, hTime->m_dwTick);
		FrLeaveMutex(&hTime->m_xLock);
		return	FALSE;
	}
	hTime->m_dwTick = FrGetTickCount();
	hTime->m_bStop = FALSE;

	LOG_D("[UtilTime]	McTimeStart Stop 1 RefTime %d CurTime %d Tick %d", hTime->m_dwRefCTS, hTime->m_dwCurTime, hTime->m_dwTick);

	FrLeaveMutex(&hTime->m_xLock);

	return	TRUE;
}

BOOL FrTimeStop(FrTimeHandle hHandle) {
	FrTimeStruct*	hTime = (FrTimeStruct*)hHandle;

	if (!hTime)
		return	FALSE;

	FrEnterMutex(&hTime->m_xLock);
	if (hTime->m_bStop)	{
		LOG_D("[UtilTime]	McTimeStop Stop 1 RefTime %d CurTime %d Tick %d", hTime->m_dwRefCTS, hTime->m_dwCurTime, hTime->m_dwTick);
		FrLeaveMutex(&hTime->m_xLock);
		return	FALSE;
	}

	// Video only
	if (hTime->m_dwCurTime == hTime->m_dwRefCTS)
		hTime->m_dwRefCTS += (DWORD32)((double)(FrGetTickCount() - hTime->m_dwTick)*hTime->m_fRatio);
	else
		hTime->m_dwRefCTS = hTime->m_dwCurTime;
	hTime->m_dwCurTime = hTime->m_dwRefCTS;
	hTime->m_dwPrevTime = hTime->m_dwRefCTS;
	hTime->m_dwTick = FrGetTickCount();

	hTime->m_bStop = TRUE;

	LOG_D("[UtilTime]	McTimeStop RefTime %d CurTime %d Tick %d", hTime->m_dwRefCTS, hTime->m_dwCurTime, hTime->m_dwTick);

	FrLeaveMutex(&hTime->m_xLock);

	return	TRUE;
}

BOOL FrTimeSetTimeRatio(FrTimeHandle hHandle, float fRatio)
{
	FrTimeStruct*	hTime = (FrTimeStruct*)hHandle;

	if (!hTime)
		return	FALSE;

	FrEnterMutex(&hTime->m_xLock);

	// video only
	if (hTime->m_dwRefCTS == hTime->m_dwCurTime)
	{
		hTime->m_dwRefCTS += (DWORD32)((float)(FrGetTickCount() - hTime->m_dwTick)*hTime->m_fRatio);
		LOG_I("[Time] McTimeSetTimeRatio - Video Only - Current Time %d, play ratio %f", hTime->m_dwRefCTS, fRatio);
	}
	else
	{
		hTime->m_dwRefCTS = hTime->m_dwCurTime;
		LOG_I("[Time] McTimeSetTimeRatio - Audio - Current Time %d, play ratio %f", hTime->m_dwRefCTS, fRatio);
	}
	hTime->m_dwCurTime = hTime->m_dwRefCTS;

	hTime->m_dwPrevTime = hTime->m_dwRefCTS;
	hTime->m_dwTick = FrGetTickCount();
	hTime->m_fRatio = fRatio;
	FrLeaveMutex(&hTime->m_xLock);

	return	TRUE;
}

// To update the current time with the Audio CTS just played out
// Not called for video-only contents
BOOL McTimeSetCurTime(FrTimeHandle hHandle, DWORD32 dwTime)
{
	FrTimeStruct*	hTime = (FrTimeStruct*)hHandle;

	if (!hTime)
		return	FALSE;

	if (hTime->m_fRatio != 1.0)
		return TRUE;

	FrEnterMutex(&hTime->m_xLock);
	hTime->m_dwCurTime = dwTime;
	hTime->m_dwPrevTime = dwTime;

	if (hTime->m_bStop==FALSE)
	{
		// Time elapsed on the machine
		dwTime = hTime->m_dwRefCTS + (DWORD32)((double)(FrGetTickCount() - hTime->m_dwTick)*hTime->m_fRatio);
		
		//LOG_I("[Time] SetCurTime : Ref=[%d], Time=[%d], CurTick=[%d], Diff=[%d]", 
		
		if (MAX_ALLOWED_AUDIO_TIME_DIFFERENCE < (INT32)(dwTime - hTime->m_dwCurTime))
		{
			LOG_I("[Time] SetCurTime - Correct Ref Time : Ref=[%d], Time=[%d], CurTick=[%d], Diff=[%d]", 
				hTime->m_dwRefCTS, dwTime, hTime->m_dwCurTime, dwTime - hTime->m_dwCurTime);
			hTime->m_dwRefCTS = hTime->m_dwCurTime;
			hTime->m_dwTick = FrGetTickCount();
		}
	}
	FrLeaveMutex(&hTime->m_xLock);

	return	TRUE;
}

// To initialize all the time-related variables after seek or resume
BOOL FrTimeSetRefTime(FrTimeHandle hHandle, DWORD32 dwTime) {
	FrTimeStruct*	hTime = (FrTimeStruct*)hHandle;

	if (!hTime)
		return	FALSE;

	FrEnterMutex(&hTime->m_xLock);
	hTime->m_dwCurTime = dwTime;
	hTime->m_dwPrevTime = dwTime;
	hTime->m_dwRefCTS = dwTime;
	hTime->m_dwTick = FrGetTickCount();

	if (hTime->m_bStop==FALSE)
		LOG_D("[UtilTime]	McTimeSetRefTime RefTime %d CurTime %d Tick %d", hTime->m_dwRefCTS, hTime->m_dwCurTime, hTime->m_dwTick);
	else
		LOG_D("[UtilTime]	McTimeSetRefTime Stop 1 RefTime %d CurTime %d Tick %d", hTime->m_dwRefCTS, hTime->m_dwCurTime, hTime->m_dwTick);

	FrLeaveMutex(&hTime->m_xLock);

	return	TRUE;
}

DWORD32 FrTimeGetTime(FrTimeHandle hHandle) {
	FrTimeStruct*	hTime = (FrTimeStruct*)hHandle;
	DWORD32			dwTime;

	if (!hTime)
		return	0;

	FrEnterMutex(&hTime->m_xLock);
	if (hTime->m_bStop)	{
		dwTime = MAX(hTime->m_dwCurTime, hTime->m_dwPrevTime);
	}
	else {
		// video only
		if (hTime->m_dwCurTime == hTime->m_dwRefCTS)
			dwTime = hTime->m_dwRefCTS + (DWORD32)((float)(FrGetTickCount() - hTime->m_dwTick)*hTime->m_fRatio);
		else
			dwTime = hTime->m_dwCurTime;
		
		//LOG_I("[Time]	Ref=%d Time=%d CurTick=%d", hTime->m_dwRefCTS, dwTime, hTime->m_dwCurTime);
		//dwTime = MAX(dwTime, hTime->m_dwCurTime);
		hTime->m_dwPrevTime = dwTime;
	}
	FrLeaveMutex(&hTime->m_xLock);

	return	dwTime;
}

DWORD32 FrTimeGetRefTime(FrTimeHandle hHandle)
{
	FrTimeStruct*	hTime = (FrTimeStruct*)hHandle;

	if (!hTime)
		return	0;

	return	hTime->m_dwRefCTS;
}
