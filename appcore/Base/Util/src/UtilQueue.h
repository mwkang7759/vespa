
#ifndef	_UTILQUEUE_H_
#define _UTILQUEUE_H_

#include "SystemAPI.h"
#include "TraceAPI.h"

typedef struct _QueNodeStruct
{
	DWORD32					m_dwLen;
	void*					m_pData;
	struct _QueNodeStruct*	m_pNext;
} QueNodeStruct, *QueNodeHandle;

typedef struct _AQueNodeStruct
{
    DWORD32                 m_dwLen;
    void*                   m_pData;
} AQueNodeStruct, *AQueNodeHandle;

typedef struct
{
	QueNodeHandle			m_pHead;
	QueNodeHandle			m_pTail;
	QueNodeHandle			m_pCur;
	QueNodeHandle			m_pStart;		// for Live TS
	QueNodeHandle			m_pEnd;			// for Live TS

	MUTEX_HANDLE			m_xLock;

	int                     m_nCnt;         // filled queue count for Queue Count management
    int                     m_nSize;        // queue size for Queue Count management

} UtilQueStruct;

typedef struct
{
    DWORD32                 m_dwElement;
    DWORD32                 m_dwWriteIdx;
    DWORD32                 m_dwReadIdx;
    AQueNodeStruct*         m_pBuffer;
} UtilAQueStruct;

#include "UtilAPI.h"

#endif	// _UTILQUEUE_H_
