
#ifndef	_UTILBUFFER_H_
#define _UTILBUFFER_H_

#include "SystemAPI.h"
#include "TraceAPI.h"

#define	MAX_REF_NUM			8

typedef struct _BufNodeStruct
{
	DWORD32					m_dwRef;
	DWORD32					m_dwIndex;
	DWORD32					m_dwLen;
	BYTE*					m_pData;
	struct _BufNodeStruct*	m_pPrev;
	struct _BufNodeStruct*	m_pNext;
} BufNodeStruct, *BufNodeHandle;

typedef struct
{
	DWORD32					m_dwCurBuffSize;
	DWORD32					m_dwCurBuffCnt;
	DWORD32					m_dwRef;
	BufNodeHandle			m_pHead;
	BufNodeHandle			m_pCur[MAX_REF_NUM];
	BufNodeHandle			m_pShow;
	BufNodeHandle			m_pTail;

	MUTEX_HANDLE			m_xLock;

} UtilBufStruct;

#include "UtilAPI.h"

#endif // _UTILBUFFER_H_
