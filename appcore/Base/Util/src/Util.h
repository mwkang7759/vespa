
#ifndef _UTIL_H_
#define _UTIL_H_

#include "SystemAPI.h"
#include "TraceAPI.h"

// Unicode
#define UNI_REPLACEMENT_CHAR	(UTF32)0x0000FFFD
#define UNI_MAX_BMP				(UTF32)0x0000FFFF
#define UNI_MAX_UTF16			(UTF32)0x0010FFFF
#define UNI_MAX_UTF32			(UTF32)0x7FFFFFFF
#define UNI_MAX_LEGAL_UTF32		(UTF32)0x0010FFFF

#define UNI_SUR_HIGH_START		(UTF32)0xD800
#define UNI_SUR_HIGH_END		(UTF32)0xDBFF
#define UNI_SUR_LOW_START		(UTF32)0xDC00
#define UNI_SUR_LOW_END			(UTF32)0xDFFF

#define	UNI_HALF_SHIFT			10
#define	UNI_HALF_BASE			(UTF32)0x0010000UL
#define	UNI_HALF_MASK			(UTF32)0x3FFUL

#define	UNI_BYTE_MASK			0xBF
#define	UNI_BYTE_MARK			0x80

// ColorConv
//#define	COLOR_CONV_USE_TABLE
#define MB_SIZE					16
#define B_SIZE					8
#define UC_RANGE(X)				(((X) < 0) ? 0 : ((X) > 255) ? 255 : (X))

enum RGBpixelType { RGB565, RGB555, RGB24bpp, RGB32bpp, BGR32bpp };

// Bit
typedef struct _bitfile
{
	/* bit input */
	unsigned char *rdptr;
	INT32 incnt;
	INT32 bitcnt;
	INT32 framebits;
	INT32	bitlength;

	/* bookmarks */
	unsigned char *book_rdptr;
	INT32 book_incnt;
	INT32 book_bitcnt;
	INT32 book_framebits;
	INT32 bookmark;

} BitStruct;

// Time
typedef struct {
	BOOL			m_bStop;
	MUTEX_HANDLE	m_xLock;
	float			m_fRatio;

	DWORD32			m_dwCurTime;
	DWORD32			m_dwPrevTime;
	DWORD32			m_dwRefCTS;
	DWORD32			m_dwTick;

} FrTimeStruct;

// ColorConv
#ifdef	WIN32
extern void asm_YUVtoRGB16_row_MMX(
		BYTE *ARGB1_pointer,
		BYTE *ARGB2_pointer,
		BYTE *Y1_pointer,
		BYTE *Y2_pointer,
		BYTE *U_pointer,
		BYTE *V_pointer,
		INT32 width
		);
extern void asm_YUVtoRGB16_555_row_MMX(
		BYTE *ARGB1_pointer,
		BYTE *ARGB2_pointer,
		BYTE *Y1_pointer,
		BYTE *Y2_pointer,
		BYTE *U_pointer,
		BYTE *V_pointer,
		INT32 width
		);
extern void asm_YUVtoRGB32_row_MMX(
		BYTE *ARGB1_pointer,
		BYTE *ARGB2_pointer,
		BYTE *Y1_pointer,
		BYTE *Y2_pointer,
		BYTE *U_pointer,
		BYTE *V_pointer,
		INT32 width
		);
extern void asm_YUVtoRGB24_row_MMX(
		BYTE *ARGB1_pointer,
		BYTE *ARGB2_pointer,
		BYTE *Y1_pointer,
		BYTE *Y2_pointer,
		BYTE *U_pointer,
		BYTE *V_pointer,
		INT32 width
		);
#endif

#include "UtilAPI.h"

#endif		// _UTIL_H_
