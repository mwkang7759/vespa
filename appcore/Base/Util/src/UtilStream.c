
#include "Util.h"
#include "StreamUtilAPI.h"

static const char HexEncoding[] = "0123456789ABCDEF";

DWORD32 UtilBin2HexString(BYTE *pInData, DWORD32	 dwInSize, BYTE* pOutData, DWORD32 dwOutBuf)
{
	DWORD32	i;

	if (dwInSize*2 > dwOutBuf)
		return	0;

	for (i=0; i<dwInSize; i++, pInData++)
	{
		*pOutData++ = HexEncoding[(*pInData>>4) & 0xF];
		*pOutData++ = HexEncoding[(*pInData) & 0xF];
	}
	*pOutData = 0;

	return	dwInSize*2;
}

DWORD32 UtilMakeSmoothDuration(QWORD *pDuration, DWORD32 dwSize, BYTE* pOutData, DWORD32 dwOutData, QWORD qwTotalDuration, CHAR * pIndent1, CHAR* pIndent2)
{
	DWORD32	i, dwLen, dwBufSize = dwOutData;

	for (i=0; i<dwSize-1; i++)
	{
		dwLen = sprintf((CHAR*)pOutData, "%s<c\r\n%sd=\"%llu\" />\r\n", pIndent1, pIndent2, pDuration[i+1]-pDuration[i]);
		pOutData += dwLen;
		dwBufSize -= dwLen;
	}

	dwLen = sprintf((CHAR*)pOutData, "%s<c\r\n%sd=\"%llu\" />\r\n", pIndent1, pIndent2, qwTotalDuration-pDuration[dwSize-1]);
	pOutData += dwLen;
	dwBufSize -= dwLen;

	return	dwOutData - dwBufSize;
}

VOID ConvQWORDTo8BYTE(QWORD a, BYTE* b)
{
	BYTE* p = (BYTE*)&a;

	b[0] = p[7];
	b[1] = p[6];
	b[2] = p[5];
	b[3] = p[4];
	b[4] = p[3];
	b[5] = p[2];
	b[6] = p[1];
	b[7] = p[0];
}

DWORD32 make_h264stream_rtp_format_from_ts_format(BYTE* pOutData, DWORD32 out_buff_size, BYTE* pInData, DWORD32 in_size)
{
	DWORD32		i, nal_size = 0, nal_st = 0, written_size = 0, st_code_bytes;
	BOOL		bFirst = TRUE;
	BYTE* pIn = pInData, * pOut = pOutData;
	DWORD32		nal_count = 0;

	if (!pIn || !pOut || !out_buff_size || !in_size)
		return	0;

	if (in_size < 4)
		return	0;

	// No startcode
	if (!IS_4B_START_CODE(pIn) && !IS_3B_START_CODE(pIn))
	{
		memcpy(pOut, pInData, in_size);
		return	in_size;
	}

	for (i = 0; i < in_size - 3;i++, pIn++)
	{
		if (IS_4B_START_CODE(pIn) || IS_3B_START_CODE(pIn))
		{
			nal_count++;
			if (IS_4B_START_CODE(pIn))
				st_code_bytes = 4;
			else
				st_code_bytes = 3;

			i += st_code_bytes;
			pIn += st_code_bytes;

			if (bFirst)
			{
				bFirst = FALSE;
			}
			else
			{
				pOut[0] = (BYTE)(nal_size >> 24);
				pOut[1] = (BYTE)(nal_size >> 16);
				pOut[2] = (BYTE)(nal_size >> 8);
				pOut[3] = (BYTE)(nal_size);
				written_size += 4;
				pOut += 4;

				if (written_size + nal_size <= out_buff_size)
					memcpy(pOut, &pInData[nal_st], nal_size);
				else
					return	0;

				written_size += nal_size;
				pOut += nal_size;
				nal_size = 0;
			}
			nal_st = i;
		}
		if (pIn[2] && pIn[3])
		{
			nal_size += 2;
			i += 2;
			pIn += 2;
		}
		nal_size++;
	}

	// Add a few bytes at the end
	if (i < in_size)
		nal_size += in_size - i;

	// process a nal at the end
	if (nal_size)
	{
		pOut[0] = (BYTE)(nal_size >> 24);
		pOut[1] = (BYTE)(nal_size >> 16);
		pOut[2] = (BYTE)(nal_size >> 8);
		pOut[3] = (BYTE)(nal_size);
		written_size += 4;
		pOut += 4;

		if (written_size + nal_size <= out_buff_size)
			memcpy(pOut, &pInData[nal_st], nal_size);
		else
			return	0;

		written_size += nal_size;
		pOut += nal_size;
		nal_size = 0;
	}

	if (nal_count <= 2 && 5 < written_size && ((pOutData[4] & 0x1F) == 0x7))
	{
		TRACE("[StreamUtil] make_h264stream_rtp_format - met a stream (len %d) with sps and pps only", written_size);
		written_size = 0;
	}

	return	written_size;
}


DWORD32 make_h264_config_data_from_skt(BYTE* pDst, DWORD32 dwDst, BYTE* pSrc, DWORD32 dwSrc)
{
	INT32		nNum, nSrc, nLen;
	DWORD32		dwConfig;

	if (!pDst || !dwDst || !pSrc || !dwSrc)
		return 0;

	nSrc = dwConfig = dwSrc;
	while (nSrc > 0)
	{
		nNum = (DWORD32)(*pSrc);
		pSrc++; nSrc--;
		dwConfig--;

		while (nNum)
		{
			nLen = (INT32)ConvByteToWORD(pSrc) + 2;
			memcpy(pDst, pSrc, nLen);
			pSrc += nLen; nSrc -= nLen; pDst += nLen;
			nNum--;
		}
	}

	return dwConfig;
}

static void update_hvcc_config_using_vps(hvcCStruct* hvcc)
{
	BYTE* pData;

	if (!hvcc->m_dwSpropVps || !hvcc->m_pVpsPara[0].m_pNal)
		return;

	pData = hvcc->m_pVpsPara[0].m_pNal;

	pData += 2;			// skip nal size 2B
	pData += 2;			// skip nal header 2B
	pData += 4;			// skip over to profile_tier_level

	hvcc->m_bGeneralProfileSpace = (*pData) >> 6;
	hvcc->m_bGeneralTierFlag = ((*pData) >> 5) & 0x1;
	hvcc->m_bGeneralProfileIdc = (*pData) & 0x1F;

	pData++;
	memcpy((BYTE*)&hvcc->m_uGeneralProfileCompatibilityFlags, pData, 4);
	pData += 4;
	memcpy((BYTE*)&hvcc->m_u64GeneralConstraintsIndicatorFlags, pData, 6);
	pData += 6;
	if (*pData)
		hvcc->m_bGeneralLevelIdc = *pData;
}

DWORD32 make_hvcC_config(BYTE* pConfig, DWORD32 dwConfig, hvcCStruct* hvcc)
{
	BYTE* pCurPos = pConfig;
	BYTE		bValue;
	USHORT		sValue;
	DWORD32		dwLen = 0, i;

	if (!hvcc || hvcc->m_bNumOfArrays < 3 || !hvcc->m_dwSpropVps || !hvcc->m_dwSpropSps || !hvcc->m_dwSpropPps)
	{
		TRACE("[StreamUtil] make_hvcC_config - don't have enough config data or null pointer");
		return 0;
	}

	// set some hvcC values using vps
	update_hvcc_config_using_vps(hvcc);

	// Some init values
	// We only support writing HEVCDecoderConfigurationRecord version 1
	hvcc->m_bConfigurationVersion = 1;
	//hvcc->lengthSizeMinusOne = 3;		// 4 bytes
	hvcc->m_bConstantFrameRate = 0;		// can not guarantee frame rate is constant

	// If min_spatial_segmentation_idc is invalid, reset to 0 (unspecified).
	if (4096 < hvcc->m_sMinSpatialSegmentationIdc)
		hvcc->m_sMinSpatialSegmentationIdc = 0;

	// parallelismType indicates the type of parallelism that is used to meet
	// the restrictions imposed by min_spatial_segmentation_idc when the value
	// of min_spatial_segmentation_idc is greater than 0.
	if (!hvcc->m_sMinSpatialSegmentationIdc)
		hvcc->m_bParallelismType = 0;

	// unsigned int(8) configuration_version
	bValue = hvcc->m_bConfigurationVersion;
	*pCurPos++ = bValue; dwLen++;
	// unsigned int(2) general_profile_space;
	// unsigned int(1) general_tier_flag;
	// unsigned int(5) general_profile_idc;
	bValue = hvcc->m_bGeneralProfileSpace << 6 | hvcc->m_bGeneralTierFlag << 5 | hvcc->m_bGeneralProfileIdc;
	*pCurPos++ = bValue; dwLen++;
	// unsigned int(32) general_profile_compatibility_flags
	//dwValue = hvcc->m_uGeneralProfileCompatibilityFlags;
	//Copy4BytesFromDWORD32(pCurPos, dwValue);
	memcpy(pCurPos, (BYTE*)&hvcc->m_uGeneralProfileCompatibilityFlags, 4);
	pCurPos += 4; dwLen += 4;
	// unsigned int(48) general_constraint_indicator_flags;
	//dwValue = (DWORD32)(hvcc->m_u64GeneralConstraintsIndicatorFlags>>16);
	//Copy4BytesFromDWORD32(pCurPos, dwValue);
	//pCurPos+=4; dwLen+=4;
	//sValue = (USHORT)hvcc->m_u64GeneralConstraintsIndicatorFlags;
	//Copy2BytesFromDWORD32(pCurPos, sValue);
	memcpy(pCurPos, (BYTE*)&hvcc->m_u64GeneralConstraintsIndicatorFlags, 6);
	pCurPos += 6; dwLen += 6;
	// unsigned int(8) general_level_idc
	bValue = hvcc->m_bGeneralLevelIdc;
	*pCurPos++ = bValue; dwLen++;
	// bit(4) reserved = 0xF
	// unsigned int(12) min_spatial_segmentation_idc
	sValue = hvcc->m_sMinSpatialSegmentationIdc | 0xF000;
	Copy2BytesFromDWORD32(pCurPos, sValue);
	pCurPos += 2; dwLen += 2;
	// bit(6) reserved = 0x3F
	// unsigned int(2) parallelismType;
	bValue = hvcc->m_bParallelismType | 0xFC;
	*pCurPos++ = bValue; dwLen++;
	// bit(6) reserved = 0x3F
	// unsigned int(2) chromaFormat;
	bValue = hvcc->m_bChromaFormat | 0xFC;
	*pCurPos++ = bValue; dwLen++;
	// bit(5) reserved = 0x1F
	// unsigned int(3) bitDepthLumaMinus8;
	bValue = hvcc->m_bBitDepthLumaMinus8 | 0xF8;
	*pCurPos++ = bValue; dwLen++;
	// bit(5) reserved = 0x1F
	// unsigned int(3) bitDepthChromaMinus8;
	bValue = hvcc->m_bBitDepthChromaMinus8 | 0xF8;
	*pCurPos++ = bValue; dwLen++;
	// unsigned int(16) avgFrameRate;
	sValue = (USHORT)hvcc->m_sAvgFrameRate;
	Copy2BytesFromDWORD32(pCurPos, sValue);
	pCurPos += 2; dwLen += 2;
	// bit(2) constantFrameRate;
	// bit(3) numTemporalLayers;
	// bit(1) temporalIdNested;
	// unsigned int(2) lengthSizeMinusOne;
	bValue = hvcc->m_bConstantFrameRate << 6 |
		hvcc->m_bNumTemporalLayers << 3 |
		hvcc->m_bTemporalIdNested << 2 |
		hvcc->m_bLengthSizeMinusOne;
	*pCurPos++ = bValue; dwLen++;
	// unsigned int(8) numOfArrays
	bValue = hvcc->m_bNumOfArrays;
	*pCurPos++ = bValue; dwLen++;

	// vps *******************************************************************//
	// bit(1) array_completeness;
	// unsigned int(1) reserved = 0;
	// unsigned int(6) NAL_unit_type;
	bValue = 32 | 0x80;
	*pCurPos++ = bValue; dwLen++;
	// unsigned int(16) numNalus
	sValue = (USHORT)hvcc->m_dwSpropVps;
	Copy2BytesFromDWORD32(pCurPos, sValue);
	pCurPos += 2; dwLen += 2;
	for (i = 0; i < hvcc->m_dwSpropVps; i++)
	{
		paraSetStruct* pPara = hvcc->m_pVpsPara + i;
		// unsigned int(16) nalUnitLength
		// bit(8*nalUnitLength) nalUnit
		memcpy(pCurPos, pPara->m_pNal, pPara->m_sLength);
		pCurPos += pPara->m_sLength;
		dwLen += pPara->m_sLength;
	}

	// sps *******************************************************************//
	// bit(1) array_completeness;
	// unsigned int(1) reserved = 0;
	// unsigned int(6) NAL_unit_type;
	bValue = 33 | 0x80;
	*pCurPos++ = bValue; dwLen++;
	// unsigned int(16) numNalus
	sValue = (USHORT)hvcc->m_dwSpropSps;
	Copy2BytesFromDWORD32(pCurPos, sValue);
	pCurPos += 2; dwLen += 2;
	for (i = 0; i < hvcc->m_dwSpropSps; i++)
	{
		paraSetStruct* pPara = hvcc->m_pSpsPara + i;
		// unsigned int(16) nalUnitLength
		// bit(8*nalUnitLength) nalUnit
		memcpy(pCurPos, pPara->m_pNal, pPara->m_sLength);
		pCurPos += pPara->m_sLength;
		dwLen += pPara->m_sLength;
	}

	// pps *******************************************************************//
	// bit(1) array_completeness;
	// unsigned int(1) reserved = 0;
	// unsigned int(6) NAL_unit_type;
	bValue = 34 | 0x80;
	*pCurPos++ = bValue; dwLen++;
	// unsigned int(16) numNalus
	sValue = (USHORT)hvcc->m_dwSpropPps;
	Copy2BytesFromDWORD32(pCurPos, sValue);
	pCurPos += 2; dwLen += 2;
	for (i = 0; i < hvcc->m_dwSpropPps; i++)
	{
		paraSetStruct* pPara = hvcc->m_pPpsPara + i;
		// unsigned int(16) nalUnitLength
		// bit(8*nalUnitLength) nalUnit
		memcpy(pCurPos, pPara->m_pNal, pPara->m_sLength);
		pCurPos += pPara->m_sLength;
		dwLen += pPara->m_sLength;
	}

	return dwLen;
}

BOOL check_h264_config_only(BYTE* pData, DWORD32 size)
{
	INT32	nLen = 0, nTotalLen = 0, i;
	BYTE* pCurData = pData;

	// Bigsize frame is skipped over
	if (500 < size)
		return FALSE;

	if (pData[0] == 0 && pData[1] == 0 && pData[2] == 0 && pData[3] == 0x1)
	{
		//bAnnexB = TRUE;
		goto bAnnexB;
	}

	while (1)
	{
		if ((INT32)size <= nTotalLen + 4)
			return TRUE;

		nLen = (INT32)ConvByteToDWORD(pCurData);
		if ((0x1F & pCurData[4]) != 0x7 && (0x1F & pCurData[4]) != 0x8 && (0x1F & pCurData[4]) != 0xd)
			return FALSE;

		nTotalLen += nLen + 4;
		pCurData += nTotalLen;
	}

	return TRUE;


bAnnexB:
	for (i = 0; i < (INT32)size - 4;i++, pData++)
	{
		if (IS_3B_START_CODE(pData))
		{
			if (((pData[4] & 0x1F) != 0x7) && ((pData[4] & 0x1F) != 0x8) && ((pData[4] & 0x1F) != 0x9))
				return FALSE;

			pData += 2;
		}
	}

	return TRUE;
}


// Check if a h264 frame is intra
BOOL is_a_h264_frame_intra(BYTE* pFrame, INT32 nFrameLen)
{
	BYTE* tmpptr;
	BOOL		bRet = FALSE;
	INT32		nalCount = 0;
	INT32		i;
	DWORD32		dwLen;
	BYTE        c;

	for (i = 0; i < nFrameLen; )
	{
		tmpptr = pFrame + i;
		dwLen = ConvByteToDWORD(tmpptr);

		if ((*(tmpptr + 4) & 0x1F) == 5 || (*(tmpptr + 4) & 0x1F) == 7 || (*(tmpptr + 4) & 0x1F) == 8)	// Intra, SPS, PPS
		{
			bRet = TRUE;
			break;
		}
		else if ((*(tmpptr + 4) & 0x1F) == 1)	// Inter
		{
			//dwLen = ConvByteToDWORD(tmpptr);
			c = *(tmpptr + 5);
			//i += dwLen + 4;

			// Single Nal and I Slice. This is a walkaround about MarshMallow encoder bug
			// This is always true for the nal (naltype == 1)
			// 0xB0 means the slice is the 1st slice in the current frame and its slice type is intra
			if (nalCount == 0 && (nFrameLen <= (INT32)dwLen + 4))
				if ((c & 0xF0) == 0xB0)
				{
					bRet = TRUE;
					break;
				}

			bRet = FALSE;
			break;
		}

		i += dwLen + 4;
		nalCount++;
	}

	return    bRet;
}


BOOL is_a_h265_frame_intra(BYTE* pFrame, int nFrameLen)
{
	BYTE* tmpptr;
	BOOL	bRet = FALSE;
	INT32	i;
	BYTE	type;

	for (i = 0; i < nFrameLen;)
	{
		tmpptr = pFrame + i;
		type = (*(tmpptr + 4) & 0x7E) >> 1;
		// Intra Nals
		if (H265_NAL_BLA_W_LP <= type && type <= H265_NAL_IRAP_VCL23)
		{
			bRet = TRUE;
			break;
		}
		// Vps, Sps or Pps
		else if (H265_NAL_VPS <= type && type <= H265_NAL_PPS)
		{
			bRet = TRUE;
			break;
		}
		else
		{
			// move to the next nal
			i += ConvByteToDWORD(tmpptr) + 4;
		}
	}

	return	bRet;
}


static BOOL check_inside_of_nal(BYTE* pFrame, DWORD32 dwFrame)
{
	INT32		i;
	BYTE* pData = pFrame;
	//	* pStart = pFrame;
	//DWORD32		dwStart = 0;
	//INT32		nNalLen = 0;
	//BOOL		bStartCode = FALSE;

	for (i = 0; i < (INT32)(dwFrame - 4); i++, pData++)
	{
		if (!pData[0] && !pData[1] && pData[2] == 0x1)
		{
			return TRUE;
		}
	}

	return FALSE;
}

#define		OPUS_CONTOL_CODE1	0x7f
#define		OPUS_CONTOL_CODE2	0xE0

DWORD32 check_h2645_rtp_nal_stream(BYTE* pFrame, DWORD32 dwFrame)
{
	INT32		i;
	BYTE* pData = pFrame;
	BOOL		bAnnexB = FALSE,
		bMixed = FALSE;
	DWORD32		dwLoc[100] = { 0, },
		dwLocPos[100] = { 0, },
		dwLocStartCode[100] = { 0, },
		dwLocCount = 0;

	for (i = 0; i < (INT32)(dwFrame - 4); i++, pData++)
	{
		if (!pData[0] && !pData[1] && !pData[2] && pData[3] == 0x1)
		{
			bAnnexB = TRUE;
			break;
		}
		else
		{
			bMixed = check_inside_of_nal(pData, ConvByteToDWORD(pData));
			if (bMixed)
				break;

			i += ConvByteToDWORD(pData) + 3;
			pData += ConvByteToDWORD(pData) + 3;
		}
	}

	// Normal RTP
	if (!bAnnexB && !bMixed)
		return dwFrame;

	// Convert into AnnexB
	if (bMixed)
	{
		pData = pFrame;
		for (i = 0; i < (INT32)(dwFrame - 4);)
		{
			DWORD32		dwLen = ConvByteToDWORD(pData) + 4;

			// StartCode
			pData[0] = pData[1] = pData[2] = 0;
			pData[3] = 1;

			i += dwLen;
			pData += dwLen;
		}
	}


	// Get Lock Count
	pData = pFrame;
	for (i = 0; i < (INT32)(dwFrame - 4); i++, pData++)
	{
		if (!pData[0] && !pData[1] && !pData[2] && pData[3] == 0x1)
		{
			dwLoc[dwLocCount] = i;
			dwLocStartCode[dwLocCount] = 4;
			dwLocCount++;

			i += 3;
			pData += 3;
		}
		else if (!pData[0] && !pData[1] && pData[2] == 0x1)
		{
			dwLoc[dwLocCount] = i;
			dwLocStartCode[dwLocCount] = 3;
			dwLocCount++;

			i += 2;
			pData += 2;
		}
	}
	dwLoc[dwLocCount] = dwFrame;
	dwLocStartCode[dwLocCount] = 3;

	// Calc a new nal start postion
	dwLocPos[0] = 0;
	for (i = 1; i <= (INT32)dwLocCount; i++)
	{
		DWORD32		dwNalSize = dwLoc[i] - (dwLoc[i - 1] + dwLocStartCode[i - 1]);
		dwLocPos[i] = dwLocPos[i - 1] + 4 + dwNalSize;
	}

	// Restore nal sizes
	for (i = dwLocCount - 1; 0 <= i; i--)
	{
		DWORD32		dwNalSize = dwLoc[i + 1] - (dwLoc[i] + dwLocStartCode[i]);
		BYTE* pOrg = pFrame + dwLoc[i] + dwLocStartCode[i];
		BYTE* pNew = pFrame + dwLocPos[i];

		// copy nal payload first
		memmove(pNew + 4, pOrg, dwNalSize);
		// Next copy nal size
		ConvDWORDToBYTE(dwNalSize, pNew);
	}

	return dwLocPos[dwLocCount];
}

DWORD32 check_h264_config(BYTE* pConfig, DWORD32 dwConfig)
{
	DWORD32		dwPos = 0, dwSize = 0;
	BYTE* p = pConfig;
	BOOL		bMP4Type = FALSE;
	BOOL		bSps = FALSE, bPps = FALSE;

	// Check if the config is right
	if (dwConfig < 5)
		return	0;

	//return 2;

	if (p[0] == 0xe1)
	{
		p++;
		dwConfig--;
		bMP4Type = TRUE;
	}
	else if (p[0] == 1 && (p[5] & 0x1F) == 1)
		return 1;

	dwSize = p[0] << 8 | p[1];
	dwSize += 2;
	if (dwSize > dwConfig)
		return	FALSE;							// invalid config

	if (bMP4Type)
	{
		//dwPPSNum = p[dwSize];
		dwSize++;
	}

	while (dwPos < dwConfig)
	{
		if (bSps && bPps)
			break;

		dwSize = (p[dwPos] << 8) + p[dwPos + 1];
		dwPos += 2;

		if ((p[dwPos] & 0x1F) == 0x7)
			bSps = TRUE;
		if ((p[dwPos] & 0x1F) == 0x8)
			bPps = TRUE;

		dwPos += dwSize;
	}

	if (bSps && bPps)
		return	bMP4Type + 1;
	else
		return 0;
}

// unsigned Colomb
DWORD32 vlc_Golomb(McBitHandle hBit)
{
	DWORD32		value, aa = 1;
	INT32			i = 0;
	INT32			loop = 1, j;

	if (UtilBitGetBit(hBit, 1))
		return	0;
	else
		i = 1;

	while (loop)
	{
		if (!UtilBitGetBit(hBit, 1))
			i++;
		else
			break;
	}

	for (j = 0;j < i;j++)
		aa *= 2;

	value = UtilBitGetBit(hBit, i);
	value += aa - 1;

	return value;
}

static BOOL checkProfile(INT32 nProfile)
{
	if (nProfile == 100 || nProfile == 110 ||
		nProfile == 122 || nProfile == 244 || nProfile == 44 ||
		nProfile == 83 || nProfile == 86 || nProfile == 118)
		return TRUE;
	return FALSE;
}


// Auto Dectection of Sps Stream Type
BOOL GetH264HeaderInfo(BYTE* pBuffer, DWORD32 dwLen, FrVideoInfo* hInfo)
{
	McBitHandle		hBit;
	DWORD32			dwTemp;
	DWORD32			dwType, dwSkipSize = 0, dwSpsLen = 0, dwPpsLen = 0;

	if (!pBuffer || dwLen < 2)
		return	FALSE;

	if (pBuffer[0] == 0 && pBuffer[1] == 0)
	{
		if (pBuffer[2] == 0 && pBuffer[3] == 1)
			dwType = TYPE_TS;
		else
		{
			LOG_E("[Codec] GetH264HeaderInfo - unknown Sps Stream Type");
			return FALSE;
		}
	}
	else
		dwType = TYPE_RTP;

	hBit = UtilBitOpen(pBuffer, dwLen);
	UtilBitByteAlign(hBit);
	if (dwType == TYPE_RTP)
	{
		dwSpsLen = UtilBitGetBit(hBit, 16) + 2;

		if (dwSpsLen > dwLen)
			return	FALSE;				// SPS Length
	}
	else if (dwType == TYPE_TS)
	{
		if (UtilBitGetBit64(hBit, 32) != 1)
			return	FALSE;
	}// start code

	dwTemp = UtilBitGetBit(hBit, 8);	// Nal header

	//  SEI Info Payload Type
	if (dwTemp == 0x06)
	{
		do
		{
			dwTemp = UtilBitGetBit(hBit, 8);
			if (dwTemp == 0xFF)
				dwSkipSize += 255;
		} while (dwTemp == 0xFF);
		dwSkipSize += dwTemp;

		// Sei Info Payload Size
		do
		{
			dwTemp = UtilBitGetBit(hBit, 8);
			if (dwTemp == 0xFF)
				dwSkipSize += 255;
		} while (dwTemp == 0xFF);
		dwSkipSize += dwTemp;

		UtilBitGetBit(hBit, 8 * dwSkipSize);

		dwTemp = UtilBitGetBit(hBit, 8);	// Nal header
	}

	if ((dwTemp & 0x1F) != 0x07)
	{
		LOG_E("[CodecDec] H264 decoder config not start with SPS (Nal type %d)", dwTemp & 0x1F);
		return FALSE;
	}

	hInfo->nProfile = UtilBitGetBit(hBit, 8);	// profile idc
	UtilBitGetBit(hBit, 8);				// constraint
	UtilBitGetBit(hBit, 8);				// level idc
	dwTemp = vlc_Golomb(hBit);			// seq set parameter id

	// default color format 420P
	hInfo->eColorFormat = VideoFmtYUV420P;

	// check profile idc
	if (checkProfile(hInfo->nProfile))	// high profile or higher
	{
		BOOL	bFlag = FALSE;
		BOOL	bScale[8];
		DWORD32	dwBits;

		dwTemp = vlc_Golomb(hBit);		// chroma format idc ([1] 4:2:0, [2] 4:2:2, [3] 4:4:4)
		if (dwTemp == 3)
			hInfo->m_DecoderInfo.H264.m_bSeparateColourPlaneFlag = UtilBitGetBit(hBit, 1);
		// residual_colour_transform_flag
		dwBits = vlc_Golomb(hBit);		// bit_depth_luma_minus8
		dwBits = vlc_Golomb(hBit);		// bit_depth_chroma_minus8

		// color format
		switch (dwTemp)
		{
		case	3:	// 4:4:4
			break;
		case	2:	// 4:2:2
			if (dwBits == 2)	// 10 bits
				hInfo->eColorFormat = VideoFmtYUV422P10LE;
			else
				hInfo->eColorFormat = VideoFmtYUV422P;
			break;
		case	1:	// 4:2:0
		default:
			if (dwBits == 2)	// 10 bits
				hInfo->eColorFormat = VideoFmtYUV420P10LE;
			else
				hInfo->eColorFormat = VideoFmtYUV420P;
			break;
		}
		UtilBitGetBit(hBit, 1);			// qpprime_y_zero_transform_bypass_flag
		bFlag = UtilBitGetBit(hBit, 1);	// seq_scalling_matrix_present_flag

		if (bFlag)
		{
			INT32 i;
			for (i = 0; i < ((dwTemp != 3) ? 8 : 12); i++)
			{
				bScale[i] = UtilBitGetBit(hBit, 1);
				if (bScale[i])
				{
					INT32 delta_scale, j;
					INT32 lastScale = 8;
					INT32 nextScale = 8;
					INT32 scalingList;

					//scaling_list();
					for (j = 0; j < ((i < 6) ? 16 : 64); j++)
					{
						if (nextScale != 0)
						{
							delta_scale = vlc_Golomb(hBit);
							nextScale = (lastScale + delta_scale + 256) % 256;
						}
						scalingList = (nextScale == 0) ? lastScale : nextScale;
						lastScale = scalingList;
					}
				}
			}
		}
	}

	dwTemp = vlc_Golomb(hBit);					// log2_max_frame_num_minus4
	hInfo->m_DecoderInfo.H264.m_dwFrameNumBitLength = dwTemp + 4;
	dwTemp = vlc_Golomb(hBit);					// pic order cnt type
	hInfo->m_DecoderInfo.H264.m_dwPicOrderCntType = dwTemp;
	if (!dwTemp)
	{
		dwTemp = vlc_Golomb(hBit);				// log2_max_pic_order_cnt_lsb_minus4
		hInfo->m_DecoderInfo.H264.m_dwPOCBitLength = dwTemp + 4;
	}
	else if (dwTemp == 1)
	{
		DWORD32	i;

		hInfo->m_DecoderInfo.H264.m_bDeltaPicOrderAlwaysZeroFlag = UtilBitGetBit(hBit, 1);
		// delta_pic_order_always_zero_flag
		dwTemp = vlc_Golomb(hBit);				// offset for nrf
		dwTemp = vlc_Golomb(hBit);				// offset for top to bottom filed
		dwTemp = vlc_Golomb(hBit);				// num ref frames in picture order count cycle

		for (i = 0;i < dwTemp;i++)
		{
			dwTemp = vlc_Golomb(hBit);			// offset for ref frames
			if (i == 0)
				hInfo->m_DecoderInfo.H264.m_dwReorderBuffer = dwTemp / 2;
		}
	}

	dwTemp = vlc_Golomb(hBit);					// num of ref frames
	dwTemp = UtilBitGetBit(hBit, 1);			// gaps in frame num
	dwTemp = vlc_Golomb(hBit);					// pic width in mbs minus 1
	hInfo->dwWidth = (dwTemp + 1) * 16;

	dwTemp = vlc_Golomb(hBit);					// pic height in mbs minus 1
	hInfo->dwHeight = (dwTemp + 1) * 16;

	// add, mwkang 2008.01.30
	dwTemp = UtilBitGetBit(hBit, 1);			// frame mbs only flag
	hInfo->m_DecoderInfo.H264.m_bFrameMbsOnlyFlag = dwTemp;
	if (!dwTemp)
	{
		dwTemp = UtilBitGetBit(hBit, 1);		// mb adaptive frame field flag
		hInfo->dwHeight *= 2;
	}

	dwTemp = UtilBitGetBit(hBit, 1);	// direct_8x8_inference_flag
	dwTemp = UtilBitGetBit(hBit, 1);	// frame cropping flag
	if (dwTemp)
	{
		dwTemp = vlc_Golomb(hBit);		// left
		if (dwTemp)
			hInfo->dwWidth -= (dwTemp * 2);

		dwTemp = vlc_Golomb(hBit);		// right
		if (dwTemp)
			hInfo->dwWidth -= (dwTemp * 2);

		dwTemp = vlc_Golomb(hBit);		// top
		if (dwTemp)
			hInfo->dwHeight -= (dwTemp * 2);

		dwTemp = vlc_Golomb(hBit);		// bottom
		if (dwTemp)
			hInfo->dwHeight -= (dwTemp * 2);

	}

	// Now PPS
	UtilBitByteAlign(hBit);
	dwTemp = UtilBitGetProcessed(hBit) / 8;
	if (dwLen < dwTemp + 2)
	{
		LOG_E("[CodecDec] H264 no enough data for PPS (data size %d)", dwLen - dwTemp);
		return TRUE;
	}

	if (dwType == TYPE_RTP)
	{
		if (dwSpsLen < dwTemp)
		{
			LOG_E("[CodecDec] H264 Sps parsed (%d) more than that in the stream", dwTemp, dwSpsLen);
			return TRUE;
		}

		UtilBitFlush(hBit, (dwSpsLen - dwTemp) * 8);
		dwPpsLen = UtilBitGetBit(hBit, 16) + 2;
		if (dwPpsLen < dwLen - dwSpsLen)
			return	TRUE;				// PPS Length
	}
	else
	{
		DWORD32		dwRemain = dwTemp;
		while (1)
		{
			if (UtilBitGetBit64(hBit, 32) != 1)
				UtilBitFlush(hBit, 8);
			if (--dwRemain <= 4)
				return FALSE;
		}
	}

	vlc_Golomb(hBit);					// pic set parameter id
	vlc_Golomb(hBit);					// seq set parameter id
	UtilBitGetBit(hBit, 1);				// entropy_coding_mode_flag
	hInfo->m_DecoderInfo.H264.m_bBottomFieldPicOrderInFramePresentFlag = UtilBitGetBit(hBit, 1);
	// bottom_field_pic_order_in_frame_present_flag
	dwTemp = vlc_Golomb(hBit);			// num_slice_groups_minus1
	if (dwTemp)
	{
		dwTemp = vlc_Golomb(hBit);			// slice_group_map_type;

		if (dwTemp == 6)
		{
			dwTemp = vlc_Golomb(hBit);			// pic_size_in_map_units_minus1;
		}
	}

	UtilBitClose(hBit);

	return	TRUE;
}

BOOL FrVideoGetInfo(BYTE* pBuffer, DWORD32 dwBuffLen, FrVideoInfo* hInfo)
{
	switch (hInfo->dwFourCC)
	{
	case FOURCC_AVC1:
	case FOURCC_h264:
	case FOURCC_H264:
	case FOURCC_CUDA:
		GetH264HeaderInfo(pBuffer, dwBuffLen, hInfo);
		break;
	case FOURCC_H265:
	case FOURCC_HEVC:
	
		//GetH265HeaderInfo(pBuffer, dwBuffLen, hInfo, TYPE_TS);
		break;
	default:
		return TRUE;
	}

	return	TRUE;
}