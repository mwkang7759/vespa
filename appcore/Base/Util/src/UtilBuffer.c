
#include "UtilBuffer.h"

McBufHandle UtilBufOpen(DWORD32 dwRef)
{
	UtilBufStruct*	hBuf;

	hBuf = (UtilBufStruct*)MALLOCZ(sizeof(UtilBufStruct));
	if (hBuf)
	{
		hBuf->m_dwRef = dwRef;
		FrCreateMutex(&hBuf->m_xLock);
	}

	return	hBuf;
}

void UtilBufClose(McBufHandle hHandle)
{
	UtilBufStruct*	hBuf = (UtilBufStruct*)hHandle;
	BufNodeHandle	hNode;
	DWORD32			dwCnt = 0;

	if (hBuf)
	{
		FrDeleteMutex(&hBuf->m_xLock);
		hNode = hBuf->m_pHead;
		while (hBuf->m_pHead)
		{
			//LOG_T("[UtilBuffer]	head(0x%x), cnt(%d)", hBuf->m_pHead, dwCnt);

			hNode = hBuf->m_pHead;
			hBuf->m_pHead = hNode->m_pNext;
			dwCnt++;
			FREE(hNode->m_pData);
			FREE(hNode);
		}
		FREE(hBuf);
		LOG_I("[UtilBuffer]	UtilBufClose");
	}
}

void UtilBufReset(McBufHandle hHandle)
{
	UtilBufStruct*	hBuf = (UtilBufStruct*)hHandle;
	BufNodeHandle	hNode;
	DWORD32			i;

	if (hBuf)
	{
		FrEnterMutex(&hBuf->m_xLock);

		hBuf->m_dwCurBuffSize = 0;
		hBuf->m_dwCurBuffCnt = 0;
		while (hBuf->m_pHead)
		{
			hNode = hBuf->m_pHead;
			hBuf->m_pHead = hNode->m_pNext;
			FREE(hNode->m_pData);
			FREE(hNode);
		}
		hBuf->m_pHead = NULL;
		hBuf->m_pTail = NULL;
		hBuf->m_pShow = NULL;
		for (i = 0;i < hBuf->m_dwRef;i++)
			hBuf->m_pCur[i] = NULL;

		FrLeaveMutex(&hBuf->m_xLock);
	}
}

void UtilBufResetRef(McBufHandle hHandle, DWORD32 dwRefID)
{
	UtilBufStruct*	hBuf = (UtilBufStruct*)hHandle;
	BufNodeHandle	hNode;

	FrEnterMutex(&hBuf->m_xLock);

	while (hBuf->m_pCur[dwRefID])
	{
		hNode = hBuf->m_pCur[dwRefID];
		hBuf->m_pCur[dwRefID] = hNode->m_pNext;

		hNode->m_dwRef--;
		if (!hNode->m_dwRef)
		{
			FREE(hNode->m_pData);
			FREE(hNode);
		}
	}
	hBuf->m_dwRef--;

	FrLeaveMutex(&hBuf->m_xLock);
}

void UtilBufSetRef(McBufHandle hHandle, DWORD32 dwRef)
{
	UtilBufStruct*	hBuf = (UtilBufStruct*)hHandle;
	BufNodeHandle	hNode;

	FrEnterMutex(&hBuf->m_xLock);

	hNode = hBuf->m_pHead;
	while (hNode)
	{
		hNode->m_dwRef = dwRef;
		hNode = hNode->m_pNext;
	}
	hBuf->m_dwRef = dwRef;

	FrLeaveMutex(&hBuf->m_xLock);
}

INT32 CompareIndex(DWORD32 dwIndex, DWORD32 dwIndex2)
{
	INT32		nDiff;

	if (dwIndex > dwIndex2)
	{
		nDiff = dwIndex - dwIndex2;
		if (nDiff <= MAX_WORD/2)
			return	-nDiff;
		else
			return	nDiff - MAX_WORD;
	}
	else if (dwIndex < dwIndex2)
	{
		nDiff = dwIndex2 - dwIndex;
		if (nDiff <= MAX_WORD/2)
			return	nDiff;
		else
			return	MAX_WORD - nDiff;
	}

	return	0;
}

INT32 InsertNode(BufNodeHandle hNode, BufNodeHandle hNewNode, BOOL bNext)
{
	INT32 nRet=0;

	while (hNode)
	{
		LOG_D("[UtilBuf] InsertNode : Start..cur idx(%d), new idx(%d)", hNode->m_dwIndex, hNewNode->m_dwIndex);

		//if (CompareIndex(hNode->m_dwIndex, hNewNode->m_dwIndex) > 0)
		nRet = CompareIndex(hNode->m_dwIndex, hNewNode->m_dwIndex);
		//if (nRet == 1)
		if (nRet > 0)
		{
			// insert
			/*
			if (hNode->m_pPrev)
			{
				hNewNode->m_pPrev = hNode->m_pPrev;
				hNode->m_pPrev->m_pNext = hNewNode;
			}
			hNewNode->m_pNext = hNode;
			hNode->m_pPrev = hNewNode;
			return	TRUE;
			*/
			if (hNode->m_pNext->m_dwIndex == hNewNode->m_dwIndex)
			{
				LOG_D("[UtilBuf] InsertNode : cur_next_idx(%d) , new_idx(%d), Ret (1) return -1..", 
							hNode->m_pNext->m_dwIndex, hNewNode->m_dwIndex);
				return -1;		// cancel
			}

			if (hNode->m_pNext)
			{
				hNewNode->m_pNext = hNode->m_pNext;
				hNode->m_pNext->m_pPrev = hNewNode;
			}
			hNewNode->m_pPrev = hNode;
			hNode->m_pNext = hNewNode;
			//return	TRUE;
			LOG_D("[UtilBuf] InsertNode : Success.. Ret (1) return 1..");
			return 1;	// insert

		}
		else if(nRet == 0)
		{
			LOG_D("[UtilBuf] InsertNode : Ret (0) return -1..");
			return -1;		// cancel
		}
		//else	// mobile youtube temporal code..
		//{
		//	return -1;		// cancel
		//}
		if (bNext)
			hNode = hNode->m_pNext;
		else
			hNode = hNode->m_pPrev;

		LOG_D("[UtilBuf] InsertNode : next node (0x%x).nRet(%d).", hNode, nRet);
	}

	LOG_D("[UtilBuf] InsertNode : End, return 0");

	return	0;		// append
}

BOOL UtilBufPutData(McBufHandle hHandle, BYTE* pData, DWORD32 dwLen, DWORD32 dwIndex)
{
	UtilBufStruct*	hBuf = (UtilBufStruct*)hHandle;
	BufNodeHandle	hNewNode;
	INT32				nHeadDiff, nTailDiff, nRet=0;

	FrEnterMutex(&hBuf->m_xLock);

	hNewNode = (BufNodeHandle)MALLOCZ(sizeof(BufNodeStruct));
	hNewNode->m_dwIndex = dwIndex;
	hNewNode->m_pData = pData;
	hNewNode->m_dwLen = dwLen;
	hNewNode->m_dwRef = hBuf->m_dwRef;

	//LOG_T("[UtilBuf] UiltBufPut: Node pos(0x%x), pData(0x%x)", hNewNode, pData);

	if (!hBuf->m_pHead)
	{
		hBuf->m_pHead = hNewNode;
		hBuf->m_pTail = hNewNode;
	}
	else
	{
		if (CompareIndex(hBuf->m_pTail->m_dwIndex, dwIndex) > 0)
		{
			// defense code..
			if (hBuf->m_pTail->m_dwIndex > dwIndex)
			{
				LOG_W("[UtilBuf] UiltBufPut: Do not append tail(%d), idx(%d)", hBuf->m_pTail->m_dwIndex, dwIndex);
				
				FREE(hNewNode->m_pData);
				FREE(hNewNode);
				FrLeaveMutex(&hBuf->m_xLock);
				return FALSE;
			}
			
			// append
			hBuf->m_pTail->m_pNext = hNewNode;
			hNewNode->m_pPrev = hBuf->m_pTail;
			hBuf->m_pTail = hNewNode;
		}
		else
		{
			if (hBuf->m_pHead->m_dwIndex > dwIndex)
			{
				LOG_D("[UtilBuf] UiltBufPut: Head index exchange..Head(%d), dwIdx(%d)", hBuf->m_pHead->m_dwIndex, dwIndex);

				hNewNode->m_pNext = hBuf->m_pHead;
				hBuf->m_pHead->m_pPrev = hNewNode;
				hBuf->m_pHead = hNewNode;

				hBuf->m_dwCurBuffSize += dwLen;
				hBuf->m_dwCurBuffCnt++;

				FrLeaveMutex(&hBuf->m_xLock);
				return	TRUE;
			
				/*
				LOG_D("[UtilBuf] insert fail..: diff head(%d), tail(%d), dwidx(%d)", 
						hBuf->m_pHead->m_dwIndex, hBuf->m_pTail->m_dwIndex, dwIndex);
				FREE(hNewNode->m_pData);
				FREE(hNewNode);
				McLeaveMutex(&hBuf->m_xLock);
				return FALSE;
				*/
			}

			// insert
			nHeadDiff = CompareIndex(hBuf->m_pHead->m_dwIndex, dwIndex);
			nTailDiff = CompareIndex(dwIndex, hBuf->m_pTail->m_dwIndex);

			LOG_D("[UtilBuf] insert : diff head(%d), tail(%d), dwidx(%d)", 
						hBuf->m_pHead->m_dwIndex, hBuf->m_pTail->m_dwIndex, dwIndex);

			if (nHeadDiff <= nTailDiff)
				nRet = InsertNode(hBuf->m_pHead, hNewNode, TRUE);
			else
				nRet = InsertNode(hBuf->m_pTail, hNewNode, FALSE);
			// modified by mwkang 2007.03.16
			//if (!bRet)
			//{
			//	hBuf->m_pTail->m_pNext = hNewNode;
			//	hNewNode->m_pPrev = hBuf->m_pTail;
			//	hBuf->m_pTail = hNewNode;
			//}
			if(nRet==0)		// append node.
			{
				hBuf->m_pTail->m_pNext = hNewNode;
				hNewNode->m_pPrev = hBuf->m_pTail;
				hBuf->m_pTail = hNewNode;
			}
			else if(nRet < 0)
			{
				FREE(hNewNode->m_pData);
				FREE(hNewNode);
				FrLeaveMutex(&hBuf->m_xLock);
				return FALSE;
			}
		}
	}
	hBuf->m_dwCurBuffSize += dwLen;
	hBuf->m_dwCurBuffCnt++;

	FrLeaveMutex(&hBuf->m_xLock);

	return	TRUE;
}

BYTE* UtilBufGetData(McBufHandle hHandle, DWORD32* pdwLen, DWORD32 dwRefID)
{
	UtilBufStruct*	hBuf = (UtilBufStruct*)hHandle;
	BufNodeHandle	hNode;
	BYTE*			pData = NULL;

	if (!hBuf->m_pHead || hBuf->m_pCur[dwRefID] == hBuf->m_pTail)
	{
		//LOG_T("[UtilBuf] hBuf->m_pHead is 0x%x, Tail is 0x%x Cur is 0x%x",
		//	(LPPTR)hBuf->m_pHead, (LPPTR)hBuf->m_pTail, (LPPTR)hBuf->m_pCur[dwRefID]);
		*pdwLen = 0;
		return	NULL;
	}

	FrEnterMutex(&hBuf->m_xLock);

	if (!hBuf->m_pCur[dwRefID])
		hBuf->m_pCur[dwRefID] = hBuf->m_pHead;
	else
	{
		hNode = hBuf->m_pCur[dwRefID];
		hBuf->m_pCur[dwRefID] = hNode->m_pNext;

		hNode->m_dwRef--;
		if (!hNode->m_dwRef)
		{
			hBuf->m_pHead = hNode->m_pNext;
			hBuf->m_dwCurBuffSize -= hNode->m_dwLen;
			hBuf->m_dwCurBuffCnt--;
			
			//LOG_T("[UtilBuf] UiltBufGet: Node pos(0x%x), pData(0x%x)", hNode, hNode->m_pData);
			
			FREE(hNode->m_pData);
			FREE(hNode);
		}
	}

	*pdwLen = hBuf->m_pCur[dwRefID]->m_dwLen;
	pData = hBuf->m_pCur[dwRefID]->m_pData;

	FrLeaveMutex(&hBuf->m_xLock);

	return	pData;
}

BYTE* UtilBufShowFirstData(McBufHandle hHandle, DWORD32* pdwLen)
{
	UtilBufStruct*	hBuf = (UtilBufStruct*)hHandle;
	BYTE*			pData = NULL;

	if (!hBuf->m_pHead)
	{
		*pdwLen = 0;
		return	NULL;
	}

	FrEnterMutex(&hBuf->m_xLock);

	*pdwLen = hBuf->m_pHead->m_dwLen;
	pData = hBuf->m_pHead->m_pData;

	FrLeaveMutex(&hBuf->m_xLock);

	return	pData;
}

BYTE* UtilBufShowLastData(McBufHandle hHandle, DWORD32* pdwLen)
{
	UtilBufStruct*	hBuf = (UtilBufStruct*)hHandle;
	BYTE*			pData = NULL;

	if (!hBuf->m_pHead)
	{
		*pdwLen = 0;
		return	NULL;
	}

	FrEnterMutex(&hBuf->m_xLock);

	*pdwLen = hBuf->m_pTail->m_dwLen;
	pData = hBuf->m_pTail->m_pData;

	FrLeaveMutex(&hBuf->m_xLock);

	return	pData;
}

BYTE* UtilBufShowData(McBufHandle hHandle, DWORD32* pdwLen)
{
	UtilBufStruct*	hBuf = (UtilBufStruct*)hHandle;
	BYTE*			pData = NULL;

	if (!hBuf->m_pHead)
	{
		*pdwLen = 0;
		return	NULL;
	}

	if (!hBuf->m_pShow)
		hBuf->m_pShow = hBuf->m_pHead;
	else
	{
		if (!hBuf->m_pShow->m_pNext)
		{
			*pdwLen = 0;
			return	NULL;
		}
		hBuf->m_pShow = hBuf->m_pShow->m_pNext;
	}

	*pdwLen = hBuf->m_pShow->m_dwLen;
	pData = hBuf->m_pShow->m_pData;

	return	pData;
}

void UtilBufShowReset(McBufHandle hHandle)
{
	UtilBufStruct*	hBuf = (UtilBufStruct*)hHandle;
	hBuf->m_pShow = NULL;
}

DWORD32 UtilBufGetBufferedSize(McBufHandle hHandle)
{
	UtilBufStruct*	hBuf = (UtilBufStruct*)hHandle;
	return	hBuf->m_dwCurBuffSize;
}

DWORD32 UtilBufGetBufferedCnt(McBufHandle hHandle)
{
	UtilBufStruct*	hBuf = (UtilBufStruct*)hHandle;
	return	hBuf->m_dwCurBuffCnt;
}


BOOL UtilBufCheckLostData(McBufHandle hHandle)
{
	UtilBufStruct*	hBuf = (UtilBufStruct*)hHandle;
	BufNodeHandle	hNode;
	BOOL			bRet = FALSE;
	DWORD32			dwLost = 0, dwDiff;

	if (!hBuf->m_pHead)
	{
		LOG_T("[UtilBuf] hBuf->m_pHead is NULL..");
		return	FALSE;
	}

	FrEnterMutex(&hBuf->m_xLock);

	hNode = hBuf->m_pHead->m_pNext;
	if (hNode)
	{
		if (hBuf->m_pHead->m_dwIndex < hNode->m_dwIndex)
		{
			dwLost = hNode->m_dwIndex - hBuf->m_pHead->m_dwIndex - 1;
		}
		else
		{
			dwDiff = hBuf->m_pHead->m_dwIndex - hNode->m_dwIndex;
			if (dwDiff >= MAX_WORD/2)
				dwLost = MAX_WORD - dwDiff - 1;
		}

		if (dwLost)
			bRet = TRUE;

		if (bRet)
			LOG_D("[UtilBuf] UtilBufCheckLostData : Head Index(%d), Next Index(%d), dwLost(%d), bRet(%d)",
									hBuf->m_pHead->m_dwIndex, hNode->m_dwIndex, dwLost, bRet);
	}

	FrLeaveMutex(&hBuf->m_xLock);

	return	bRet;
}