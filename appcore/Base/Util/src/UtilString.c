
#include "Util.h"

char* UtilStrGetLowerCaseLetter(char *pBuffer, INT32 iLen)
{
	INT32		i;

	if (!pBuffer)
		return	NULL;

	for (i = 0;i < iLen;i++)
	{
		if (pBuffer[i] >='A' && pBuffer[i] <='Z')
			pBuffer[i] +='a'-'A';
	}
	//pBuffer[i] = '\0';

	return	pBuffer;
}

char* UtilStrGetUpperCaseLetter(char *pBuffer, INT32 iLen)
{
	INT32		i;

	if (!pBuffer)
		return	NULL;

	for (i = 0;i< iLen;i++)
	{
		if (pBuffer[i] >='a' && pBuffer[i] <='z')
			pBuffer[i] -='a'-'A';
	}
	return	pBuffer;
}

char* UtilStrFindString(char* pBuffer, char* pFindString)
{
	if (!pBuffer)
		return	NULL;

	while (strncmp(pBuffer, pFindString, strlen(pFindString)))
	{
		if (ISENDLINE(*pBuffer))
			return	NULL;
		pBuffer++;
	}

	return	pBuffer;
}

char* UtilStrIFindString(char* pBuffer, char* pFindString)
{
	if (!pBuffer)
		return	NULL;

	while (UtilStrStrnicmp(pBuffer, pFindString, strlen(pFindString)))
	{
		if (ISNULL(*pBuffer))
			return	NULL;
		pBuffer++;
	}

	return	pBuffer;
}

char* UtilStrReverseIFindString(char* pBuffer, char* pFindString)
{
	int nLen = 0;
	int nIndx = 0;

	if (!pBuffer)
		return	NULL;

	nLen = strlen(pBuffer);
	if (nLen == 0)
		return NULL;

	pBuffer = pBuffer+nLen-1;

	for (nIndx=nLen; 0<nIndx; nIndx--)
	{
		if (ISNULL(*pBuffer))
			return	NULL;

		if (!_strnicmp(pBuffer, pFindString, strlen(pFindString)))
			break;

		pBuffer--;
	}

	if (nIndx == 0)
		return pBuffer+1;
	else
		return	pBuffer;
}

char* UtilStrStrdupn(const char *string, INT32 n)
{
	char *s = NULL;

	if (!string)
		return	NULL;

	if (n<=0)
		return NULL;

	s = (CHAR*)MALLOCZ(n+2);
	strncpy(s, string, n);

	return s;
}

TCHAR* UtilStrStrdup(const TCHAR *string)
{
	TCHAR *s = NULL;
	DWORD32 dwSize = 0;

	if (!string)
		return	NULL;

	dwSize = (DWORD32)_tcslen(string) + 1;

#ifdef UNICODE
	dwSize *= sizeof(WCHAR);
#endif

	s = (TCHAR*)MALLOC(dwSize);
	_tcscpy(s, string);

	return s;
}

TCHAR* UtilStrStrdupEx(const TCHAR *string, DWORD32 dwExtraSize)
{
	TCHAR *s = NULL;
	DWORD32 dwSize = 0;

	if (!string)
		return	NULL;

	dwSize = (DWORD32)_tcslen(string) + 1;

#ifdef UNICODE
	dwSize *= sizeof(WCHAR);
#endif

	s = (TCHAR*)MALLOC(dwSize + dwExtraSize);
	_tcscpy(s, string);

	return s;
}

char* UtilStrStrcat(const char *string, const char *string2)
{
	char *s = NULL;

	if (!string)
		return	NULL;

	s = (CHAR*)MALLOC((UINT32)strlen(string) + (UINT32)strlen(string2) + 1);

	strcpy(s, string);
	strcat(s, string2);

	return s;
}

INT32 UtilStrStricmp(const char * dst, const char * src)
{
    INT32 f,l;

    do
    {
        if ( ((f = (unsigned char)(*(dst++))) >= 'A') && (f <= 'Z') )
            f -= ('A' - 'a');

        if ( ((l = (unsigned char)(*(src++))) >= 'A') && (l <= 'Z') )
            l -= ('A' - 'a');
    } while ( f && (f == l) );

    return(f - l);
}

INT32 UtilStrStrnicmp(const char * first, const char * last, size_t count)
{
    INT32 f,l;

    if ( count )
    {
        do
        {
            if ( ((f = (unsigned char)(*(first++))) >= 'A') && (f <= 'Z') )
                f -= 'A' - 'a';

            if ( ((l = (unsigned char)(*(last++))) >= 'A') && (l <= 'Z') )
                l -= 'A' - 'a';

        } while ( --count && f && (f == l) );
        return( f - l );
    }

    return( 0 );
}

BOOL UtilStrIsDecNumber(char* pBuffer)
{
	if (!pBuffer)
		return	FALSE;

	while (ISSPACE(*pBuffer))
	{
		if (ISENDLINE(*pBuffer))
			return	FALSE;
		pBuffer++;
	}

	if (ISDIGIT(*pBuffer))
		return	TRUE;
	else
		return	FALSE;
}

char* UtilStrGetLine(char* pBuffer, DWORD32* dwLen)
{
	char*	pCRLF;

	if (!pBuffer)
		return	NULL;

	while (ISSPACE(*pBuffer))
		pBuffer++;
	pCRLF = pBuffer;

	do {
		while (!ISLINEFEED(*pCRLF))
		{
			if (ISNULL(*pCRLF))
			{
				pCRLF--;
				break;
			}
			pCRLF++;
		}

		if (ISSPACE(*(pCRLF+1)))
		{
			if (ISRETURN(*(pCRLF-1)))
				*(pCRLF-1) = ' ';
			*pCRLF = ' ';
		}
		else
			break;
	} while (TRUE);

	pCRLF++;

	*dwLen = (DWORD32)(pCRLF - pBuffer);

	return	pCRLF;
}

char* UtilStrGetLineEx(char* pBuffer, DWORD32* dwLen, BOOL bReplace)
{
	char*	pCRLF;

	if (!pBuffer)
		return	NULL;

	while (bReplace && ISSPACE(*pBuffer))
		pBuffer++;
	pCRLF = pBuffer;

	do {
		while (!ISLINEFEED(*pCRLF))
		{
			if (ISNULL(*pCRLF))
			{
				pCRLF--;
				break;
			}
			pCRLF++;
		}

		if (bReplace && ISSPACE(*(pCRLF+1)))
		{
			if (ISRETURN(*(pCRLF-1)))
				*(pCRLF-1) = ' ';
			*pCRLF = ' ';
		}
		else
			break;
	} while (TRUE);

	pCRLF++;

	*dwLen = (DWORD32)(pCRLF - pBuffer);

	return	pCRLF;
}

char* UtilStrGetDecNumber(char* pBuffer, char* pString, DWORD32* dwNum)
{
	char	*pStr;
	char	strNum[20];

	if (!pBuffer)
		return	NULL;

	if (pString)
	{
		if ((pStr = UtilStrFindString(pBuffer, pString)) == NULL)
			return	NULL;
		pStr = pStr + strlen(pString);
	}
	else
	{
		pStr = pBuffer;
	}

	while (!ISDIGIT(*pStr))
	{
		if (ISENDLINE(*pStr))
			return	NULL;
		pStr++;
	}

	pBuffer = pStr;
	while (ISDIGIT(*pBuffer))
		pBuffer++;

	if(sizeof(strNum) < pBuffer-pStr+1)
	{
		TRACE("[Util] UtilStrGetDecNumber - # of digits in the string is larger than the size of strNum buffer (size %d)",
			sizeof(strNum));
		strNum[0] = '\0';
	}
	else
	{
		memcpy(strNum, pStr, pBuffer-pStr);
		strNum[pBuffer-pStr] = '\0';
	}

	if (dwNum)
		*dwNum = ATOI(strNum);

	return	pBuffer;
}

char* UtilStrIGetDecNumber(char* pBuffer, char* pString, DWORD32* dwNum)
{
	char	*pStr;
	char	strNum[20];

	if (!pBuffer)
		return	NULL;

	if (pString)
	{
		if ((pStr = UtilStrIFindString(pBuffer, pString)) == NULL)
			return	NULL;
		pStr = pStr + strlen(pString);
	}
	else
	{
		pStr = pBuffer;
	}

	while (!ISDIGIT(*pStr))
	{
		if (ISENDLINE(*pStr))
			return	NULL;
		pStr++;
	}

	pBuffer = pStr;
	while (ISDIGIT(*pBuffer))
		pBuffer++;

	memcpy(strNum, pStr, pBuffer-pStr);
	strNum[pBuffer-pStr] = '\0';

	if (dwNum)
		*dwNum = ATOI(strNum);

	return	pBuffer;
}

char* UtilStrGetIntNumber(char* pBuffer, char* pString, INT32* pnNum)
{
	char	*pStr;
	char	strNum[20];
	
	if (!pBuffer)
		return	NULL;

	if (pString)
	{
		if ((pStr = UtilStrFindString(pBuffer, pString)) == NULL)
			return	NULL;
		pStr = pStr + strlen(pString);
	}
	else
	{
		pStr = pBuffer;
	}

	while (!ISDIGIT(*pStr) && !ISHIPON(*pStr))
	{
		if (ISENDLINE(*pStr))
			return	NULL;
		pStr++;
	}

	if (ISHIPON(*pStr))
	{
		pStr++;
	}

	pBuffer = pStr;
	while (ISDIGIT(*pBuffer))
		pBuffer++;

	memcpy(strNum, pStr, pBuffer-pStr);
	strNum[pBuffer-pStr] = '\0';

	if (pnNum)
	{
		/*if (bMinus)
			*pnNum = (INT)(-1 * ATOI(strNum));
		else*/
			*pnNum = ATOI(strNum);
	}

	return	pBuffer;
}

char* UtilStrGetHexNumber(char* pBuffer, char* pString, DWORD32* dwNum)
{
	char	*pStr;
	char	strNum[20];

	if (!pBuffer)
		return	NULL;

	if (pString)
	{
		if ((pStr = UtilStrFindString(pBuffer, pString)) == NULL)
			return	NULL;
		pStr = pStr + strlen(pString);
	}
	else
	{
		pStr = pBuffer;
	}

	while (!ISHEXDIGIT(*pStr))
	{
		if (ISENDLINE(*pStr))
			return	NULL;
		pStr++;
	}

	pBuffer = pStr;
	while (ISHEXDIGIT(*pBuffer))
		pBuffer++;

	memcpy(strNum, pStr, pBuffer-pStr);
	strNum[pBuffer-pStr] = '\0';

	if (dwNum)
		*dwNum = ATOI_HEX(strNum);

	return	pBuffer;
}

char* UtilStrGetFloatNumber(char* pBuffer, char* pString, DWORD32* dwNum)
{
	char	*pStr;
	char	strNum[20];

	if (!pBuffer)
		return	NULL;

	if (pString)
	{
		if ((pStr = UtilStrFindString(pBuffer, pString)) == NULL)
			return	NULL;
		pStr = pStr + strlen(pString);
	}
	else
	{
		pStr = pBuffer;
	}

	while (!ISFLOAT(*pStr))
	{
		if (ISENDLINE(*pStr))
			return	NULL;
		pStr++;
	}

	pBuffer = pStr;
	while (ISFLOAT(*pBuffer))
		pBuffer++;

	memcpy(strNum, pStr, pBuffer-pStr);
	strNum[pBuffer-pStr] = '\0';

	if (dwNum)
		*dwNum = ATOI_MS(strNum);

	return	pBuffer;
}

char* UtilStrGetDoubleNumber(char* pBuffer, char* pString, double* dbNum)
{
	char	*pStr;
	char	strNum[20];

	if (!pBuffer)
		return	NULL;

	if (pString)
	{
		if ((pStr = UtilStrFindString(pBuffer, pString)) == NULL)
			return	NULL;
		pStr = pStr + strlen(pString);
	}
	else
	{
		pStr = pBuffer;
	}

	while (!ISFLOAT(*pStr))
	{
		if (ISENDLINE(*pStr))
			return	NULL;
		pStr++;
	}

	pBuffer = pStr;
	while (ISFLOAT(*pBuffer))
		pBuffer++;

	memcpy(strNum, pStr, pBuffer-pStr);
	strNum[pBuffer-pStr] = '\0';

	if (dbNum)
		*dbNum = ATOF(strNum);

	return	pBuffer;
}

char* UtilStrGetQwordNumber(char* pBuffer, char* pString, QWORD* qwNum)
{
	char	*pStr;
	char	strNum[20];

	if (!pBuffer)
		return	NULL;

	if (pString)
	{
		if ((pStr = UtilStrFindString(pBuffer, pString)) == NULL)
			return	NULL;
		pStr = pStr + strlen(pString);
	}
	else
	{
		pStr = pBuffer;
	}

	while (!ISDIGIT(*pStr))
	{
		if (ISENDLINE(*pStr))
			return	NULL;
		pStr++;
	}

	pBuffer = pStr;
	while (ISDIGIT(*pBuffer))
		pBuffer++;

	memcpy(strNum, pStr, pBuffer-pStr);
	strNum[pBuffer-pStr] = '\0';

	if (qwNum)
		*qwNum = ATOQ(strNum);

	return	pBuffer;
}

char* UtilStrGetTimeNumber(char* pBuffer, char* pString, DWORD32* dwNum)
{
	char	*pStr;
	char	strNum[20];

	if (!pBuffer)
		return	NULL;

	if (pString)
	{
		if ((pStr = UtilStrFindString(pBuffer, pString)) == NULL)
			return	NULL;
		pStr = pStr + strlen(pString);
	}
	else
	{
		pStr = pBuffer;
	}

	while (!ISTIME(*pStr))
	{
		if (ISENDLINE(*pStr))
			return	NULL;
		pStr++;
	}

	pBuffer = pStr;
	while (ISTIME(*pBuffer))
		pBuffer++;

	memcpy(strNum, pStr, pBuffer-pStr);
	strNum[pBuffer-pStr] = '\0';

	if (dwNum)
		*dwNum = ATOI_TS(strNum);

	return	pBuffer;
}

char* UtilStrGetString(char* pBuffer, char** pString, DWORD32* dwLen)
{
	char*	pStr;

	if (!pBuffer)
		return	NULL;

	while (!ISCHAR(*pBuffer))
	{
		if (ISENDLINE(*pBuffer))
			return	NULL;
		pBuffer++;
	}

	pStr = pBuffer;
	while (ISCHAR(*pBuffer))
		pBuffer++;

	*pString = pStr;
	*dwLen = (DWORD32)(pBuffer - pStr);

	return	pBuffer;
}

char* UtilStrGetStringEx(char* pBuffer, char** pString, DWORD32* dwLen)
{
	char*	pStr;
	BOOL	bDoubleQuotes = FALSE;

	if (!pBuffer)
		return	NULL;

	while (!ISCHAR(*pBuffer))
	{
		if (ISENDLINE(*pBuffer))
			return	NULL;

		if (ISDBLQUOTES(*pBuffer))
		{
			bDoubleQuotes = TRUE;
			pBuffer++;
			break;
		}
		pBuffer++;
	}

	pStr = pBuffer;

	if (bDoubleQuotes)
	{
		while (!ISDBLQUOTES(*pBuffer))
		{
			if (ISENDLINE(*pBuffer))
				break;

			pBuffer++;
		}
	}
	else
	{
		while (ISCHAR(*pBuffer))
			pBuffer++;
	}

	*pString = pStr;
	*dwLen = (DWORD32)(pBuffer - pStr);

	return	pBuffer;
}

char* UtilStrGetHexString(char* pBuffer, char** pString, DWORD32* dwLen)
{
	char*	pStr;

	if (!pBuffer)
		return	NULL;

	while (!ISHEXDIGIT(*pBuffer))
	{
		if (ISENDLINE(*pBuffer))
			return	NULL;
		pBuffer++;
	}

	pStr = pBuffer;
	while (ISHEXDIGIT(*pBuffer))
		pBuffer++;

	*pString = pStr;
	*dwLen = (DWORD32)(pBuffer - pStr);

	return	pBuffer;
}

char* UtilStrGetAddress(char* pBuffer, char** pString, DWORD32* dwLen)
{
	char*	pStr;

	if (!pBuffer)
		return	NULL;

	pStr = UtilStrFindString(pBuffer, "://");
	if (pStr)
	{
		pBuffer = pStr + strlen("://");
	}

	while (!ISADDR(*pBuffer))
	{
		if (ISENDLINE(*pBuffer))
			return	NULL;
		pBuffer++;
	}

	pStr = pBuffer;
	while (ISADDR(*pBuffer))
		pBuffer++;

	*pString = pStr;
	*dwLen = (DWORD32)(pBuffer - pStr);

	return	pBuffer;
}

char* UtilStrGetSafeString(char* pBuffer, char** pString, DWORD32* dwLen)
{
	char*	pStr;

	if (!pBuffer)
		return	NULL;

	while (!ISSAFE(*pBuffer))
	{
		if (ISENDLINE(*pBuffer))
			return	NULL;
		pBuffer++;
	}

	pStr = pBuffer;

	while (ISSAFE(*pBuffer))
		pBuffer++;

	*pString = pStr;
	*dwLen = (DWORD32)(pBuffer - pStr);

	return	pBuffer;
}

char* UtilStrGetSafeFileName(char* pBuffer, char** pString, DWORD32* dwLen)
{
	char*	pStr;

	if (!pBuffer)
		return	NULL;

	while (!ISSAFE(*pBuffer))
	{
		if (ISENDLINE(*pBuffer))
			return	NULL;
		pBuffer++;
	}

	pStr = pBuffer;

	while (!ISNOFILENAME(*pBuffer))
		pBuffer++;

	*pString = pStr;
	*dwLen = (DWORD32)(pBuffer - pStr);

	return	pBuffer;
}

char* UtilStrGetContext(char* pBuffer, char** pContext, DWORD32* dwLen)
{
	char*	pStr;

	if (!pBuffer)
		return	NULL;

	while (ISSPACE(*pBuffer))
		pBuffer++;

	pStr = pBuffer;
	while (!ISRETURN(*pBuffer))
		pBuffer++;

	*pContext = pStr;
	*dwLen = (DWORD32)(pBuffer - pStr);

	return	pBuffer;
}

char* UtilStrGetBase64String(char* pBuffer, char** pString, DWORD32* dwLen)
{
	char*	pStr;

	if (!pBuffer)
		return	NULL;

	while (!ISBASE64(*pBuffer))
	{
		if (ISENDLINE(*pBuffer))
			return	NULL;
		pBuffer++;
	}

	pStr = pBuffer;
	while (ISBASE64(*pBuffer))
		pBuffer++;

	*pString = pStr;
	*dwLen = (DWORD32)(pBuffer - pStr);

	return	pBuffer;
}

DWORD32 UtilStrConvStringToBin(char* pInData, DWORD32 dwInData, BYTE* pOutData, DWORD32 dwOutBuf)
{
	DWORD32	dwOutSize = 0;
	char*	s;
	INT32		v1, v2;

	pInData = UtilStrGetLowerCaseLetter(pInData, dwInData);

	s = pInData;

	while (dwInData) {
		if (*s >= 'a' && *s <= 'f' )
			v1 = (*s - 'a' + 10 );
		else
			v1 = *s - '0';

		if (*(s+1) >= 'a' && *(s+1) <= 'f')
			v2 = (*(s+1) - 'a' + 10 );
		else
			v2 = *(s+1) - '0';

		 *(pOutData + dwOutSize++) = (BYTE)(v1 * 16 + v2);
		 s+=2;
		 dwInData -=2;
	}

	return	dwOutSize;
}

char* UtilStrConvBinToString(BYTE* pData, DWORD32 dwData)
{
	char*	pString;
	char*	p;

	pString = (CHAR*)MALLOC(dwData * 2 + 1);
	p = pString;
	while (dwData)
	{
		sprintf(p++, "%x", (char)(*pData >> 4));
		sprintf(p++, "%x", (char)(*pData & 0xf));
		pData++;
		dwData--;
	}
	*p = '\0';

	return	pString;
}

BOOL UtilFindStartCode(BYTE** pBuffer, DWORD32* pdwLen, INT32* pnCodeID)
{
	DWORD32	dwCode;

    //while (*pdwLen > 4)
	while (*pdwLen > 8)
	{
		dwCode = Conv3ByteToDWORD(*pBuffer);

		if (dwCode == 0x000001)
		{
			*pnCodeID = ((dwCode << 8) | (DWORD32)((*pBuffer)[3]));
			return	TRUE;
		}
		(*pBuffer)++;
		(*pdwLen)--;
    }

    return	FALSE;
}

BOOL UtilFindStartCode2(BYTE** pBuffer, DWORD32* pdwLen, DWORD32 dwCodeID)
{
	DWORD32	dwCode;

    while (*pdwLen > 4)
	{
		dwCode = ConvByteToDWORD(*pBuffer);

		if (dwCode == dwCodeID)
			return	TRUE;

		(*pBuffer)++;
		(*pdwLen)--;
    }

    return	FALSE;
}

char* UtilStrGetHTTPHeader(char* pBuffer, DWORD32* pdwLen)
{
    // pdwLen : header's length
    char* pszToken;

    while(ISSPACE(*pBuffer))
    {
        pBuffer++;
    }

    if(strstr(pBuffer, ":"))
    {
        pszToken = UtilStrFindString(pBuffer, ":");
        *pdwLen = (DWORD32)(pszToken - pBuffer) + 1;
    }
    else
    {
        return NULL;
    }

    return pBuffer;
}

char* UtilStrTrim(char *pBuffer)
{
	INT32 i=0, j=0;

	for (i = 0, j= 0; i < (INT32)strlen(pBuffer); i++)
	{
	    if (pBuffer[i] != ' ')
			pBuffer[j++] = pBuffer[i];
	}

	pBuffer[j]='\0';

	return pBuffer;
}

char* UtilStrReplace(char *pBuffer, char chOld, char chNew)
{

	INT32 i=0;

	for (i=0; i<(INT32)strlen(pBuffer); i++)
	{
		if (pBuffer[i] == chOld)
			pBuffer[i] = chNew;
	}

	return pBuffer;
}

char* UtilStrLeftOneTrim(char *pBuffer)
{
	INT32 i=0, j=0;

	if (pBuffer[0] == ' ')
	{
		for (i = 1, j= 0; i < (INT32)strlen(pBuffer); i++)
		{
			pBuffer[j++] = pBuffer[i];
		}

		pBuffer[j]='\0';
	}

	return pBuffer;
}

char *UtilStrReplaceAll(char *s, const char *olds, const char *news) {
	char *result, *sr;
	size_t i, count = 0;
	size_t oldlen = strlen(olds);
	size_t newlen = strlen(news);

	if (oldlen < 1) return s;

	if (newlen != oldlen) {
		for (i = 0; s[i] != '\0';) {
			if (memcmp(&s[i], olds, oldlen) == 0) count++, i += oldlen;
			else i++;
		}
	} else i = strlen(s);


	result = (char *) malloc(i + 1 + count * (newlen - oldlen));
	if (result == NULL) return NULL;


	sr = result;
	while (*s) {
		if (memcmp(s, olds, oldlen) == 0) {
			memcpy(sr, news, newlen);
			sr += newlen;
			s  += oldlen;
		} else *sr++ = *s++;
	}
	*sr = '\0';

	return result;
}
