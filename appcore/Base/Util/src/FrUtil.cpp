#include "FrUtil.h"


#ifdef WiN32
#if defined(_DEBUG)
#pragma comment(lib, "opencv_core440d.lib")
#pragma comment(lib, "opencv_highgui440d.lib")
#pragma comment(lib, "opencv_imgproc440d.lib")
#pragma comment(lib, "opencv_imgcodecs440d.lib")
#pragma comment(lib, "opencv_calib3d440d.lib")
#pragma comment(lib, "opencv_cudaimgproc440d.lib")
#pragma comment(lib, "opencv_cudawarping440d.lib")
#pragma comment(lib, "opencv_cudaarithm440d.lib")
#pragma comment(lib, "opencv_video440d.lib")
#pragma comment(lib, "opencv_videoio440d.lib")
#pragma comment(lib, "opencv_cudacodec440d.lib")
#else
#pragma comment(lib, "opencv_core440.lib")
#pragma comment(lib, "opencv_highgui440.lib")
#pragma comment(lib, "opencv_imgproc440.lib")
#pragma comment(lib, "opencv_imgcodecs440.lib")
#pragma comment(lib, "opencv_calib3d440.lib")
#pragma comment(lib, "opencv_cudaimgproc440.lib")
#pragma comment(lib, "opencv_cudawarping440.lib")
#pragma comment(lib, "opencv_cudaarithm440.lib")
#pragma comment(lib, "opencv_video440.lib")
#pragma comment(lib, "opencv_videoio440.lib")
#pragma comment(lib, "opencv_cudacodec440.lib")
#endif
#endif

namespace fr {
	void cvYUV2BGR24(int w, int h, int pitch, uchar* y, uchar* u, uchar* v, uchar* bgr) {
		std::vector<cv::Mat> vecImage, vecOutput(3);

		int lumaWidth = w;
		int lumaHeight = h;
		int chromaWidth = lumaWidth >> 1;
		int chromaHeight = lumaHeight >> 1;
		int lumaPitch = pitch;
		int chromaPitch = lumaPitch >> 1;

		unsigned char* yDeviceptr = (unsigned char*)y;
		unsigned char* uDeviceptr = u;
		unsigned char* vDeviceptr = v;

		cv::Mat gpuY(lumaHeight, lumaWidth, CV_8UC1, yDeviceptr, lumaPitch);
		cv::Mat gpuU(chromaHeight, chromaWidth, CV_8UC1, uDeviceptr, chromaPitch);
		cv::Mat gpuV(chromaHeight, chromaWidth, CV_8UC1, vDeviceptr, chromaPitch);

		vecImage.push_back(gpuY);
		vecImage.push_back(gpuU);
		vecImage.push_back(gpuV);

		vecImage[0].copyTo(vecOutput[0]);
		vecImage[1].copyTo(vecOutput[1]);
		vecImage[2].copyTo(vecOutput[2]);

		cv::resize(vecOutput[1], vecOutput[1], cv::Size(vecOutput[1].cols * 2, vecOutput[1].rows * 2), cv::InterpolationFlags::INTER_CUBIC);
		cv::resize(vecOutput[2], vecOutput[2], cv::Size(vecOutput[2].cols * 2, vecOutput[2].rows * 2), cv::InterpolationFlags::INTER_CUBIC);

		cv::Mat gResult;
		cv::merge(vecOutput, gResult);
		cv::cvtColor(gResult, gResult, cv::ColorConversionCodes::COLOR_YUV2BGR); //COLOR_YUV2BGR      = 84, //COLOR_YUV2RGB

		memcpy(bgr, gResult.data, gResult.cols * gResult.rows * 3);
	};

	void conv::YUVToBGR24(int w, int h, int pitch, uchar* y, uchar* u, uchar* v, uchar* bgr) {
		std::vector<cv::Mat> vecImage, vecOutput(3);

		int lumaWidth = w;
		int lumaHeight = h;
		int chromaWidth = lumaWidth >> 1;
		int chromaHeight = lumaHeight >> 1;
		int lumaPitch = pitch;
		int chromaPitch = lumaPitch >> 1;

		unsigned char* yDeviceptr = (unsigned char*)y;
		unsigned char* uDeviceptr = u;
		unsigned char* vDeviceptr = v;

		cv::Mat gpuY(lumaHeight, lumaWidth, CV_8UC1, yDeviceptr, lumaPitch);
		cv::Mat gpuU(chromaHeight, chromaWidth, CV_8UC1, uDeviceptr, chromaPitch);
		cv::Mat gpuV(chromaHeight, chromaWidth, CV_8UC1, vDeviceptr, chromaPitch);

		vecImage.push_back(gpuY);
		vecImage.push_back(gpuU);
		vecImage.push_back(gpuV);

		vecImage[0].copyTo(vecOutput[0]);
		vecImage[1].copyTo(vecOutput[1]);
		vecImage[2].copyTo(vecOutput[2]);

		cv::resize(vecOutput[1], vecOutput[1], cv::Size(vecOutput[1].cols * 2, vecOutput[1].rows * 2), cv::InterpolationFlags::INTER_CUBIC);
		cv::resize(vecOutput[2], vecOutput[2], cv::Size(vecOutput[2].cols * 2, vecOutput[2].rows * 2), cv::InterpolationFlags::INTER_CUBIC);

		cv::Mat gResult;
		cv::merge(vecOutput, gResult);
		cv::cvtColor(gResult, gResult, cv::ColorConversionCodes::COLOR_YUV2BGR); //COLOR_YUV2BGR      = 84, //COLOR_YUV2RGB

		memcpy(bgr, gResult.data, gResult.cols * gResult.rows * 3);
	};
};
