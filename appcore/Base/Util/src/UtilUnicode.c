
#include "Util.h"
#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_ANDROID) || defined(SUPPORT_IOS)
#include <iconv.h>
#endif

static const char trailingBytesForUTF8[256] = {
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
    2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2, 3,3,3,3,3,3,3,3,4,4,4,4,5,5,5,5
};

static const FUTF32 offsetsFromUTF8[6] = { 0x00000000UL, 0x00003080UL, 0x000E2080UL,
		     0x03C82080UL, 0xFA082080UL, 0x82082080UL };

static const FUTF8 firstByteMark[7] = { 0x00, 0x00, 0xC0, 0xE0, 0xF0, 0xF8, 0xFC };

static BOOL IsLegalUTF8(const FUTF8 *utf8, INT32 length)
{
	const UTF8*	srcptr = utf8+length;
	FUTF8		a;

    switch (length)
	{
		default:
			return	FALSE;
		/* Everything else falls through when "true"... */
		case 4:
			if ((a = (*--srcptr)) < 0x80 || a > 0xBF)
				return	FALSE;
		case 3:
			if ((a = (*--srcptr)) < 0x80 || a > 0xBF)
				return	FALSE;
		case 2:
			if ((a = (*--srcptr)) > 0xBF)
				return	FALSE;

			switch (*utf8)
			{
				/* no fall-through in this inner switch */
				case 0xE0: if (a < 0xA0) return FALSE; break;
				case 0xED: if (a > 0x9F) return FALSE; break;
				case 0xF0: if (a < 0x90) return FALSE; break;
				case 0xF4: if (a > 0x8F) return FALSE; break;
				default:   if (a < 0x80) return FALSE;
			}

		case 1:
			if (*utf8 >= 0x80 && *utf8 < 0xC2)
				return	FALSE;
    }

    if (*utf8 > 0xF4)
		return	FALSE;

    return	TRUE;
}

DWORD32 UtilUniUTF8Length(const FUTF8* utf8)
{
	UTF8 const *ptr = utf8;

	while (*ptr)
		++ptr;

	return (DWORD32)(ptr - utf8) + 1;
}

DWORD32 UtilUniUTF16Length(const UTF16* utf16)
{
	UTF16 const *ptr = utf16;

	while (*ptr)
		++ptr;

	return (DWORD32)(ptr - utf16) + 1;
}

DWORD32 UtilUniUTF32Length(const UTF32* utf32)
{
	UTF32 const *ptr = utf32;

	while (*ptr)
		++ptr;

	return (DWORD32)(ptr - utf32) + 1;
}

DWORD32 UtilUniUCS2Length(const UCS2* ucs2)
{
	UCS2 const *ptr = ucs2;

	while (*ptr)
		++ptr;

	return (DWORD32)(ptr - ucs2) + 1;
}

DWORD32 UtilUniUCS4Length(const UCS4* ucs4)
{
	UCS4 const *ptr = ucs4;

	while (*ptr)
		++ptr;

	return (DWORD32)(ptr - ucs4) + 1;
}

DWORD32 UtilUniChartoUCS2(CHAR* pChar, DWORD32 dwCharLen, UCS2* ucs2, DWORD32 dwUcs2Size)
{
	DWORD32	dwUcs2Len = 0;

	if (!dwCharLen)
		dwCharLen = (DWORD32)strlen(pChar);

	while (dwCharLen)
	{
		if (dwUcs2Size <= dwUcs2Len)
			break;

		*ucs2++ = (UCS2)(*pChar++);
		dwUcs2Len++;
		dwCharLen--;
	}

	return	dwUcs2Len;
}

DWORD32 UtilUniUCS2toChar(UCS2* ucs2, DWORD32 dwUcs2Len, CHAR* pChar,  DWORD32 dwCharSize)
{
	DWORD32	dwCharLen = 0;

	if (!dwUcs2Len)
		dwUcs2Len = UtilUniUCS2Length(ucs2);

	while (dwUcs2Len)
	{
		if (dwCharSize <= dwCharLen)
			break;

		*pChar++ = (CHAR)(*ucs2++);
		dwCharLen++;
		dwUcs2Len--;
	}

	return	dwCharLen;
}

DWORD32 UtilUniChartoUCS4(CHAR* pChar, DWORD32 dwCharLen, UCS4* ucs4, DWORD32 dwUcs4Size)
{
	DWORD32	dwUcs4Len = 0;

	if (!dwCharLen)
		dwCharLen = (DWORD32)strlen(pChar);

	while (dwCharLen)
	{
		if (dwUcs4Size <= dwUcs4Len)
			break;

		*ucs4++ = (UCS4)(*pChar++);
		dwUcs4Len++;
		dwCharLen--;
	}

	return	dwUcs4Len;
}

DWORD32 UtilUniUCS4toChar(UCS4* ucs4, DWORD32 dwUcs4Len, CHAR* pChar,  DWORD32 dwCharSize)
{
	DWORD32	dwCharLen = 0;

	if (!dwUcs4Len)
		dwUcs4Len = UtilUniUCS4Length(ucs4);

	while (dwUcs4Len)
	{
		if (dwCharSize <= dwCharLen)
			break;

		*pChar++ = (CHAR)(*ucs4++);
		dwCharLen++;
		dwUcs4Len--;
	}

	return	dwCharLen;
}

DWORD32 UtilUniUTF8toUCS2(FUTF8* utf8, DWORD32 dwUtf8Len, WCHAR* ucs2, DWORD32 dwUcs2Size, BOOL bBigEndian)
{
	WCHAR	ch;
	DWORD32	dwUcs2Len = 0;

	if (!dwUtf8Len)
		dwUtf8Len = UtilUniUTF8Length(utf8);

	while (dwUtf8Len)
	{
		if (dwUcs2Size <= dwUcs2Len)
			break;

		ch = (*utf8 & 0xE0);

		if (ch == 0xE0)
		{
			if (dwUtf8Len < 3)
				break;

			*ucs2 = (*utf8++ & 0x0F) << 12;
			*ucs2 |= (*utf8++ & 0x3f) << 6;
			*ucs2 |= (*utf8++ & 0x3f);
			dwUtf8Len -= 3;
		}
		else if (ch == 0xC0)
		{
			if (dwUtf8Len < 2)
				break;

			*ucs2 = (*utf8++ & 0x1f) << 6;
			*ucs2 |= (*utf8++ & 0x3f);
			dwUtf8Len -= 2;
		}
		else
		{
			if (!dwUtf8Len)
				break;

			if (bBigEndian)
			{
				*ucs2 = (WCHAR)(*utf8++);
			}
			else
			{
				*ucs2 = ((WCHAR)(*utf8) & 0x00ff) << 8;
				*ucs2 |= ((WCHAR)(*utf8++) & 0xff00) >> 8;
			}
			dwUtf8Len--;
		}

		ucs2++;
		dwUcs2Len++;
	}
	*ucs2 = '\0';

	return	dwUcs2Len;
}

DWORD32 UtilUniUCS2toUTF8Length(WCHAR *ucs2)
{
	DWORD32	dwUcs2Len, dwUtf8Len = 0;

	dwUcs2Len = UtilUniUCS2Length((const UCS2*)ucs2);

	for (; dwUcs2Len > 0; dwUcs2Len--)
	{
		/* at least 1 byte per cahracter */
		dwUtf8Len++;

		if (*ucs2 > 0x7F)
		{
			/* for values > 0x7F, add 1 byte */
			dwUtf8Len++;

			if (*ucs2 > 0x7FF)
			{
				/* for values > 0x7FF, add another byte */
				dwUtf8Len++;
			}
		}
		/* check next character */
		ucs2++;
	}

	return	dwUtf8Len;
}

DWORD32 UtilUniUCS2toUTF8(UCS2 *ucs2, DWORD32 dwUcs2Len, FUTF8 *utf8, DWORD32 dwUtf8Size)
{
	WCHAR	ch;
	DWORD32	dwUtf8Len = 0;

	if (!dwUcs2Len)
		dwUcs2Len = UtilUniUCS2Length((const UCS2*)ucs2);

	while (dwUcs2Len)
	{
		ch = *ucs2++;
		dwUcs2Len--;

		if (ch>= 0xD800 && ch< 0xE000)
		{
			ch = UNI_REPLACEMENT_CHAR;
			LOG_I("[Util]	UtilUniUCS2toUTF8 - encountered part of a surrogate pair : ignoring");
		}

		if (ch < 0x80)  /* 0 <= c < 0x80 : plain ASCII byte, encode as-is */
		{
			if (dwUtf8Size <= dwUtf8Len)
				break;

			*utf8++ = (FUTF8)ch;
			dwUtf8Len++;
		}
		else if (ch < 0x800) /* 0x80 <= c < 0x800 : encode as 110xxxxx 10xxxxxx */
		{
			if (dwUtf8Size <= dwUtf8Len+1)
				break;

			utf8[1] = UNI_BYTE_MARK | (ch & 0x3f); ch>>=6;		/* lower 6 bits in byte 2 */
			utf8[0] = 0xc0 | (ch & 0x1F);				/* upper 5 bits in byte 1 */
			utf8+=2;
			dwUtf8Len+=2;
		}
		else
		{
			if (dwUtf8Size <= dwUtf8Len+2)
				break;

			/* 0x800 <= c <= 0xFFFF : encode as 1110xxxx 10xxxxxx 10xxxxxx */
			utf8[2] = UNI_BYTE_MARK | (ch & 0x3F); ch>>=6;			/* lower 6 bits in byte 3 */
			utf8[1] = UNI_BYTE_MARK | (ch & 0x3F); ch>>=6;			/* middle 6 bits in byte 2 */
			utf8[0] = 0xE0 | (ch & 0xF);					/* upper 4 bits in byte 1 */
			utf8+=3;
			dwUtf8Len += 3;
		}
	}

	return	dwUtf8Len;
}

DWORD32 UtilUniUTF32toUTF16(const UTF32* utf32, DWORD32 dwUtf32Len, UTF16* utf16, DWORD32 dwUtf16Size)
{
	UTF32	ch;
	DWORD32	dwUtf16Len = 0;

	if (!dwUtf32Len)
		dwUtf32Len = UtilUniUTF32Length(utf32);

    while (dwUtf32Len)
	{
		ch = *utf32++;
		dwUtf32Len--;

		if (ch == 0)
		{
			LOG_I("[Util]	UtilUniUTF32toUTF16 - meet the null data. (UTF16Len : %d)", dwUtf16Len);

			break;
		}
		else if (ch <= UNI_MAX_BMP)					/* Target is a character <= 0xFFFF */
		{
			if (dwUtf16Size <= dwUtf16Len)
				break;

			/* UTF-16 surrogate values are illegal in UTF-32; 0xffff or 0xfffe are both reserved values */
			if (ch >= UNI_SUR_HIGH_START && ch <= UNI_SUR_LOW_END)
			{
				*utf16++ = UNI_REPLACEMENT_CHAR;
				dwUtf16Len++;
				LOG_I("[Util]	UtilUniUTF32toUTF16 - illegal utf32 code");
			}
			else
			{
				*utf16++ = (UTF16)ch;					/* normal case */
				dwUtf16Len++;
			}
		}
		else if (ch > UNI_MAX_LEGAL_UTF32)
		{
			if (dwUtf16Size <= dwUtf16Len)
				break;

			*utf16++ = UNI_REPLACEMENT_CHAR;
			dwUtf16Len++;
			LOG_I("[Util]	UtilUniUTF32toUTF16 - illegal utf32 code");
		}
		else
		{
			if (dwUtf16Size <= dwUtf16Len + 1)
				break;

			/* target is a character in range 0xFFFF - 0x10FFFF. */
			ch -= UNI_HALF_BASE;
			*utf16++ = (UTF16)((ch >> UNI_HALF_SHIFT) + UNI_SUR_HIGH_START);
			*utf16++ = (UTF16)((ch & UNI_HALF_MASK) + UNI_SUR_LOW_START);
			dwUtf16Len += 2;
		}
    }

    return	dwUtf16Len;
}

DWORD32 UtilUniUTF16toUTF32(const UTF16* utf16, DWORD32 dwUtf16Len, UTF32* utf32, DWORD32 dwUtf32Size)
{
    UTF32	ch, ch2;
	DWORD32	dwUtf32Len = 0;

	if (!dwUtf16Len)
		dwUtf16Len = UtilUniUTF16Length(utf16);

    while (dwUtf16Len)
	{
		ch = *utf16++;
		dwUtf16Len--;

		/* If we have a surrogate pair, convert to UTF32 first. */
		if (ch == 0)
		{
			LOG_I("[Util]	UtilUniUTF16toUTF32 - meet the null data. (UTF32Len : %d)", dwUtf32Len);

			break;
		}
		else if (ch >= UNI_SUR_HIGH_START && ch <= UNI_SUR_HIGH_END)
		{
			if (!dwUtf16Len)
				break;

			/* If the 16 bits following the high surrogate are in the source buffer... */
			ch2 = *utf16;

			/* If it's a low surrogate, convert to UTF32. */
			if (ch2 >= UNI_SUR_LOW_START && ch2 <= UNI_SUR_LOW_END)
			{
				ch = ((ch - UNI_SUR_HIGH_START) << UNI_HALF_SHIFT) + (ch2 - UNI_SUR_LOW_START) + UNI_HALF_BASE;
				++utf16;
			}
			else				/* it's an unpaired high surrogate */
			{
				LOG_I("[Util]	UtilUniUTF16toUTF32 - illegal utf16 code");
			}
		}
		else
		{
			/* UTF-16 surrogate values are illegal in UTF-32 */
			if (ch >= UNI_SUR_LOW_START && ch <= UNI_SUR_LOW_END)
			{
				LOG_I("[Util]	UtilUniUTF16toUTF32 - illegal utf16 code");
			}
		}

		if (dwUtf32Size <= dwUtf32Len)
			break;

		*utf32++ = ch;
		dwUtf32Len++;
    }

    return	dwUtf32Len;
}

DWORD32 UtilUniUTF16toUTF8(const UTF16* utf16, DWORD32 dwUtf16Len, FUTF8* utf8, DWORD32 dwUtf8Size)
{
	UTF32	ch, ch2;
	DWORD32	dwUtf8Len = 0;

	if (!dwUtf16Len)
		dwUtf16Len = UtilUniUTF16Length(utf16);

    while (dwUtf16Len)
	{
		unsigned short bytesToWrite = 0;

		ch = *utf16++;
		dwUtf16Len--;

		/* If we have a surrogate pair, convert to UTF32 first. */
		if (ch == 0)
		{
			LOG_I("[Util]	UtilUniUTF16toUTF8 - meet the null data. (UTF8Len : %d)", dwUtf8Len);

			break;
		}
		else if (ch >= UNI_SUR_HIGH_START && ch <= UNI_SUR_HIGH_END)
		{
			if (!dwUtf16Len)
				break;

			/* If the 16 bits following the high surrogate are in the source buffer... */
			ch2 = *utf16;

			/* If it's a low surrogate, convert to UTF32. */
			if (ch2 >= UNI_SUR_LOW_START && ch2 <= UNI_SUR_LOW_END)
			{
				ch = ((ch - UNI_SUR_HIGH_START) << UNI_HALF_SHIFT) + (ch2 - UNI_SUR_LOW_START) + UNI_HALF_BASE;
				++utf16;
				dwUtf16Len--;
			}
			else
			{
				LOG_I("[Util]	UtilUniUTF16toUTF8 - illegal utf16 code");
			}
		}
		else
		{
			/* UTF-16 surrogate values are illegal in UTF-32 */
			if (ch >= UNI_SUR_LOW_START && ch <= UNI_SUR_LOW_END)
			{
				LOG_I("[Util]	UtilUniUTF16toUTF8 - illegal utf16 code");
			}
		}

		/* Figure out how many bytes the result will require */
		if (ch < (UTF32)0x80)
			bytesToWrite = 1;
		else if (ch < (UTF32)0x800)
			bytesToWrite = 2;
		else if (ch < (UTF32)0x10000)
			bytesToWrite = 3;
		else if (ch < (UTF32)0x110000)
			bytesToWrite = 4;
		else
		{
			bytesToWrite = 3;
			ch = UNI_REPLACEMENT_CHAR;
		}

		if (dwUtf8Len + bytesToWrite > dwUtf8Size)
			break;

		utf8 += bytesToWrite;
		switch (bytesToWrite)		/* note: everything falls through. */
		{
			case 4: *--utf8 = (FUTF8)((ch | UNI_BYTE_MARK) & UNI_BYTE_MASK); ch >>= 6;
			case 3: *--utf8 = (FUTF8)((ch | UNI_BYTE_MARK) & UNI_BYTE_MASK); ch >>= 6;
			case 2: *--utf8 = (FUTF8)((ch | UNI_BYTE_MARK) & UNI_BYTE_MASK); ch >>= 6;
			case 1: *--utf8 = (FUTF8)(ch | firstByteMark[bytesToWrite]);
		}
		utf8 += bytesToWrite;
		dwUtf8Len += bytesToWrite;
    }

    return	dwUtf8Len;
}

DWORD32 UtilUniUTF8toUTF16(const FUTF8* utf8, DWORD32 dwUtf8Len, UTF16* utf16, DWORD32 dwUtf16Size)
{
	UTF32	ch;
	DWORD32	dwUtf16Len = 0;

	if (!dwUtf8Len)
		dwUtf8Len = UtilUniUTF8Length(utf8);

    while (dwUtf8Len)
	{
		unsigned short extraBytesToRead = trailingBytesForUTF8[*utf8];

		ch = 0;

		/* Do this check whether lenient or strict */
		if (!IsLegalUTF8(utf8, extraBytesToRead+1))
		{
			LOG_I("[Util]	UtilUniUTF8toUTF16 - illegal utf8 code");
		}

		if (dwUtf8Len < (DWORD32)(extraBytesToRead + 1))
			break;

		/*
		 * The cases all fall through. See "Note A" below.
		 */
		switch (extraBytesToRead)
		{
			case 5: ch += *utf8++; dwUtf8Len--; ch <<= 6; /* remember, illegal UTF-8 */
			case 4: ch += *utf8++; dwUtf8Len--; ch <<= 6; /* remember, illegal UTF-8 */
			case 3: ch += *utf8++; dwUtf8Len--; ch <<= 6;
			case 2: ch += *utf8++; dwUtf8Len--; ch <<= 6;
			case 1: ch += *utf8++; dwUtf8Len--; ch <<= 6;
			case 0: ch += *utf8++; dwUtf8Len--;
		}
		ch -= offsetsFromUTF8[extraBytesToRead];

		if (ch == 0)
		{
			LOG_I("[Util]	UtilUniUTF8toUTF16 - meet the null data. (UTF16Len : %d)", dwUtf16Len);

			break;
		}
		else if (ch <= UNI_MAX_BMP)					/* Target is a character <= 0xFFFF */
		{
			if (dwUtf16Size <= dwUtf16Len)
				break;

			/* UTF-16 surrogate values are illegal in UTF-32 */
			if (ch >= UNI_SUR_HIGH_START && ch <= UNI_SUR_LOW_END)
			{
				*utf16++ = UNI_REPLACEMENT_CHAR;
				dwUtf16Len++;
				LOG_I("[Util]	UtilUniUTF8toUTF16 - illegal utf8 code");
			}
			else
			{
				*utf16++ = (UTF16)ch;		/* normal case */
				dwUtf16Len++;
			}
		}
		else if (ch > UNI_MAX_UTF16)
		{
			if (dwUtf16Size <= dwUtf16Len)
				break;

			*utf16++ = UNI_REPLACEMENT_CHAR;
			dwUtf16Len++;
			LOG_I("[Util]	UtilUniUTF8toUTF16 - illegal utf8 code");
		}
		else
		{
			if (dwUtf16Size <= dwUtf16Len+1)
				break;

			/* target is a character in range 0xFFFF - 0x10FFFF. */
			ch -= UNI_HALF_BASE;
			*utf16++ = (UTF16)((ch >> UNI_HALF_SHIFT) + UNI_SUR_HIGH_START);
			*utf16++ = (UTF16)((ch & UNI_HALF_MASK) + UNI_SUR_LOW_START);
			dwUtf16Len += 2;
		}
    }

    return dwUtf16Len;
}

DWORD32 UtilUniUTF32toUTF8(const UTF32* utf32, DWORD32 dwUtf32Len, FUTF8* utf8, DWORD32 dwUtf8Size)
{
	UTF32	ch;
	DWORD32	dwUtf8Len = 0;

	if (!dwUtf32Len)
		dwUtf32Len = UtilUniUTF32Length(utf32);

    while (dwUtf32Len)
	{
		unsigned short bytesToWrite = 0;

		ch = *utf32++;
		dwUtf32Len--;

		/* UTF-16 surrogate values are illegal in UTF-32 */
		if (ch >= UNI_SUR_HIGH_START && ch <= UNI_SUR_LOW_END)
		{
			LOG_I("[Util]	UtilUniUTF32toUTF8 - illegal utf32 code");
		}

		/*
		 * Figure out how many bytes the result will require. Turn any
		 * illegally large UTF32 things (> Plane 17) into replacement chars.
		 */
		if (ch == 0)
		{
			LOG_I("[Util]	UtilUniUTF32toUTF8 - meet the null data. (UTF8Len : %d)", dwUtf8Len);

			break;
		}else if (ch < (UTF32)0x80)
		    bytesToWrite = 1;
		else if (ch < (UTF32)0x800)
			bytesToWrite = 2;
		else if (ch < (UTF32)0x10000)
			bytesToWrite = 3;
		else if (ch <= UNI_MAX_LEGAL_UTF32)
			bytesToWrite = 4;
		else
		{
			bytesToWrite = 3;
			ch = UNI_REPLACEMENT_CHAR;
		}

		if (dwUtf8Len + bytesToWrite > dwUtf8Size)
			break;

		utf8 += bytesToWrite;
		switch (bytesToWrite)		/* note: everything falls through. */
		{
			case 4: *--utf8 = (FUTF8)((ch | UNI_BYTE_MARK) & UNI_BYTE_MASK); ch >>= 6;
			case 3: *--utf8 = (FUTF8)((ch | UNI_BYTE_MARK) & UNI_BYTE_MASK); ch >>= 6;
			case 2: *--utf8 = (FUTF8)((ch | UNI_BYTE_MARK) & UNI_BYTE_MASK); ch >>= 6;
			case 1: *--utf8 = (FUTF8) (ch | firstByteMark[bytesToWrite]);
		}
		utf8 += bytesToWrite;
		dwUtf8Len += bytesToWrite;
    }

    return	dwUtf8Len;
}

DWORD32 UtilUniUTF8toUTF32(const FUTF8* utf8, DWORD32 dwUtf8Len, UTF32* utf32, DWORD32 dwUtf32Size)
{
	UTF32	ch;
	DWORD32	dwUtf32Len = 0;

	if (!dwUtf8Len)
		dwUtf8Len = UtilUniUTF8Length(utf8);

    while (dwUtf8Len)
	{
		unsigned short	extraBytesToRead = trailingBytesForUTF8[*utf8];

		ch = 0;

		/* Do this check whether lenient or strict */
		if (!IsLegalUTF8(utf8, extraBytesToRead+1))
		{
			LOG_I("[Util]	UtilUniUTF8toUTF32 - illegal utf8 code");
		}

		if (dwUtf8Len < (DWORD32)(extraBytesToRead + 1))
			break;

		/*
		 * The cases all fall through. See "Note A" below.
		 */
		switch (extraBytesToRead)
		{
			case 5: ch += *utf8++; dwUtf8Len--; ch <<= 6;
			case 4: ch += *utf8++; dwUtf8Len--; ch <<= 6;
			case 3: ch += *utf8++; dwUtf8Len--; ch <<= 6;
			case 2: ch += *utf8++; dwUtf8Len--; ch <<= 6;
			case 1: ch += *utf8++; dwUtf8Len--; ch <<= 6;
			case 0: ch += *utf8++; dwUtf8Len--;
		}
		ch -= offsetsFromUTF8[extraBytesToRead];

		if (dwUtf32Size <= dwUtf32Len)
			break;

		if (ch == 0)
		{
			LOG_T("[Util]	UtilUniUTF8toUTF32 - meet the null data. (UTF32Len : %d)", dwUtf32Len);

			break;
		}else if (ch <= UNI_MAX_LEGAL_UTF32)
		{
			/*
			 * UTF-16 surrogate values are illegal in UTF-32, and anything
			 * over Plane 17 (> 0x10FFFF) is illegal.
			 */
			if (ch >= UNI_SUR_HIGH_START && ch <= UNI_SUR_LOW_END)
			{
				*utf32++ = UNI_REPLACEMENT_CHAR;
				dwUtf32Len++;
				LOG_I("[Util]	UtilUniUTF8toUTF32 - illegal utf8 code");
			}
			else
			{
				*utf32++ = ch;
				dwUtf32Len++;
			}
		}
		else			/* i.e., ch > UNI_MAX_LEGAL_UTF32 */
		{
			*utf32++ = UNI_REPLACEMENT_CHAR;
			dwUtf32Len++;
			LOG_I("[Util]	UtilUniUTF8toUTF32 - illegal utf8 code");
		}
    }

    return	dwUtf32Len;
}

DWORD32 UtilUniChartoUTF8(CHAR* pChar, DWORD32 dwCharLen, FUTF8 *utf8, DWORD32 dwUtf8Size)
{
	DWORD32	dwRet	= 0;

	if (!dwCharLen)
		dwCharLen = UtilUniUTF8Length((const FUTF8*)pChar);

	dwRet = dwCharLen<dwUtf8Size ? dwCharLen : dwUtf8Size;
	memcpy(pChar, utf8, dwRet);

	return dwRet;
}

DWORD32 UtilUniChartoUTF16(CHAR* pChar, DWORD32 dwCharLen, FUTF16 *utf16, DWORD32 dwUtf16Size)
{
	return	UtilUniChartoUCS2(pChar, dwCharLen, (UCS2*)utf16, dwUtf16Size);
}

DWORD32 UtilUniChartoUTF32(CHAR* pChar, DWORD32 dwCharLen, UTF32 *utf32, DWORD32 dwUtf32Size)
{
	return	UtilUniChartoUCS4(pChar, dwCharLen, (UCS4*)utf32, dwUtf32Size);
}

DWORD32 UtilUniUTF8toChar(FUTF8 *utf8, DWORD32 dwUtf8Len, char* pChar, DWORD32 dwCharSize)
{
	UCS2*	ucs2	= NULL;
	DWORD32	dwRet	= 0;

	ucs2 = (UCS2*)MALLOCZ(sizeof(UCS2) * (dwUtf8Len + 1));
	if(!ucs2)
		return 0;

	dwRet = UtilUniUTF8toUCS2(utf8, dwUtf8Len, (WCHAR*)ucs2, sizeof(UCS2) * dwUtf8Len, TRUE);
	if(dwRet)
	{
		dwRet = UtilUniUCS2toChar(ucs2, dwRet, pChar, dwCharSize);
		if(dwRet)
			pChar[dwRet] = '\0';
	}
	FREE(ucs2);

	return dwRet;
}

DWORD32 UtilUniUTF16toChar(const UTF16* utf16, DWORD32 dwUtf16Len, char* pChar, DWORD32 dwCharSize)
{
	FUTF8*	utf8	= NULL;
	DWORD32	dwRet	= 0;

	utf8 = (UTF8*)MALLOCZ(sizeof(UTF8) * (dwUtf16Len + 1));
	if(!utf8)
		return 0;

	dwRet = UtilUniUTF16toUTF8(utf16, dwUtf16Len, utf8, sizeof(UTF8) * dwUtf16Len);
	if(dwRet)
	{
		dwRet = UtilUniUTF8toChar(utf8, dwRet, pChar, dwCharSize);
		if(dwRet)
			pChar[dwRet] = '\0';
	}
	FREE(utf8);

	return dwRet;
}

DWORD32 UtilUniUTF32toChar(const UTF32* utf32, DWORD32 dwUtf32Len, char* pChar, DWORD32 dwCharSize)
{
	UTF8*	utf8	= NULL;
	DWORD32	dwRet	= 0;

	utf8 = (UTF8*)MALLOCZ(sizeof(UTF8) * (dwUtf32Len + 1));
	if(!utf8)
		return 0;

	dwRet = UtilUniUTF32toUTF8(utf32, dwUtf32Len, utf8, sizeof(UTF8) * dwUtf32Len);
	if(dwRet)
	{
		dwRet = UtilUniUTF8toChar(utf8, dwRet, pChar, dwCharSize);
		if(dwRet)
			pChar[dwRet] = '\0';
	}
	FREE(utf8);

	return dwRet;
}

BOOL UtilUniIsUTF8(const UTF8* utf8, DWORD32 dwUtf8Len)
{
	UTF32	ch;

    while (dwUtf8Len)
	{
		unsigned short extraBytesToRead = trailingBytesForUTF8[*utf8];

		ch = 0;

		/* Do this check whether lenient or strict */
		if (!IsLegalUTF8(utf8, extraBytesToRead+1))
		{
			LOG_I("[Util]	UtilUniIsUTF8 - illegal utf8 code - String is ANSI");
			return FALSE;
		}

		if (dwUtf8Len < (DWORD32)(extraBytesToRead + 1))
			break;

		switch (extraBytesToRead)
		{
			case 5: ch += *utf8++; dwUtf8Len--; ch <<= 6;
			case 4: ch += *utf8++; dwUtf8Len--; ch <<= 6;
			case 3: ch += *utf8++; dwUtf8Len--; ch <<= 6;
			case 2: ch += *utf8++; dwUtf8Len--; ch <<= 6;
			case 1: ch += *utf8++; dwUtf8Len--; ch <<= 6;
			case 0: ch += *utf8++; dwUtf8Len--;
		}
		ch -= offsetsFromUTF8[extraBytesToRead];
    }

    return TRUE;
}

///////////////////////////////////////////////////////////////////
//
//         ANSI <===> Unicode Conversion API
//
///////////////////////////////////////////////////////////////////
#if defined (_WIN32)
UINT WindowCodePage[] = { 0, 949, 932, 936, 950 };		// System CodePage(Kor), Kor, Jap, Chn-GBK, Chn-Big5
#else
char *LinuxCodePage[] = {
#if defined (_CODEPAGE_KOR_)
    "CP949" , /// for Korean (EUC-KR, CP949, ISO-2022-KR, JOHAB
#elif defined (_CODEPAGE_JAP_)
    "CP932" , /// for Japanese (EUC-JP, SHIFT_JIS, CP932, ISO-2022-JP, ISO-2022-JP-2, ISO-2022-JP-1)
#elif defined (_CODEPAGE_CHN_GBK_)
    "CP936" , /// for Chinese (EUC-CN, HZ, GBK, CP936, GB18030, EUC-TW, BIG5, CP950, BIG5-HKSCS, BIG5-HKSCS:2004, BIG5-HKSCS:2001, BIG5-HKSCS:1999, ISO-2022-CN, ISO-2022-CN-EXT)
#elif defined (_CODEPAGE_CHN_BIG5_)
    "CP950" , /// for Chinese
#else
    "CP949" ,
#endif
    "CP949" , /// for Korean (EUC-KR, CP949, ISO-2022-KR, JOHAB
    "CP932" , /// for Japanese (EUC-JP, SHIFT_JIS, CP932, ISO-2022-JP, ISO-2022-JP-2, ISO-2022-JP-1)
    "CP936" , /// for Chinese (EUC-CN, HZ, GBK, CP936, GB18030, EUC-TW, BIG5, CP950, BIG5-HKSCS, BIG5-HKSCS:2004, BIG5-HKSCS:2001, BIG5-HKSCS:1999, ISO-2022-CN, ISO-2022-CN-EXT)
    "CP950" /// for Chinese
};

/*
struct _CodePageMappingTable {
    UINT nCodePage;
    char szCodeString[64];
} CodePageMap[] = {
#if defined (_CODEPAGE_KOR_)
        {0, "CP949"} , /// for Korean (EUC-KR, CP949, ISO-2022-KR, JOHAB
#elif defined (_CODEPAGE_JAP_)
        {0 , "CP932"} , /// for Japanese (EUC-JP, SHIFT_JIS, CP932, ISO-2022-JP, ISO-2022-JP-2, ISO-2022-JP-1)
#elif defined (_CODEPAGE_CHN_GBK_)
        {0 , "CP936"} , /// for Chinese (EUC-CN, HZ, GBK, CP936, GB18030, EUC-TW, BIG5, CP950, BIG5-HKSCS, BIG5-HKSCS:2004, BIG5-HKSCS:2001, BIG5-HKSCS:1999, ISO-2022-CN, ISO-2022-CN-EXT)
#elif defined (_CODEPAGE_CHN_BIG5_)
        {0 , "CP950"} , /// for Chinese
#else
        {0 , "CP949"} ,
#endif
        {949 , "CP949"} , /// for Korean (EUC-KR, CP949, ISO-2022-KR, JOHAB
        {932 , "CP932"} , /// for Japanese (EUC-JP, SHIFT_JIS, CP932, ISO-2022-JP, ISO-2022-JP-2, ISO-2022-JP-1)
        {936 , "CP936"} , /// for Chinese (EUC-CN, HZ, GBK, CP936, GB18030, EUC-TW, BIG5, CP950, BIG5-HKSCS, BIG5-HKSCS:2004, BIG5-HKSCS:2001, BIG5-HKSCS:1999, ISO-2022-CN, ISO-2022-CN-EXT)
        {950 , "CP950"} , /// for Chinese
};
*/
#endif

DWORD32 UtilUniANSI2UTF8(const CHAR* pstrMultibyte, DWORD32 dwMultiSize, UTF8* pstrUtf8, DWORD32 dwUtfSize, DtbWindowCopePage eCodePage)
{
#ifdef	_WIN32
	WCHAR *pstrUnicode;
	INT32 nLen, nUnicodeSize;

	if (!dwMultiSize)
		dwMultiSize = (DWORD32)strlen(pstrMultibyte);

	// ANSI -> UTF-16
	nUnicodeSize = MultiByteToWideChar(WindowCodePage[eCodePage], 0, pstrMultibyte, -1, 0, 0);
	pstrUnicode = (WCHAR *)MALLOCZ(sizeof(WCHAR) * (nUnicodeSize + 1));

	if (!pstrUnicode)
	{
		LOG_W("[Util]	UtilUniANSI2UTF8 - Can not alloc %d bytes", sizeof(WCHAR) * (nUnicodeSize + 1));
		return 0;
	}

	nLen = MultiByteToWideChar(WindowCodePage[eCodePage], 0, pstrMultibyte, -1, pstrUnicode, nUnicodeSize);

	// UTF-16 -> UTF-8
	nLen = WideCharToMultiByte (CP_UTF8, 0, pstrUnicode, nUnicodeSize, pstrUtf8, dwUtfSize, NULL, NULL);

	FREE(pstrUnicode);

	return (DWORD32)nLen;
#elif defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_ANDROID) || defined(SUPPORT_IOS)
    char *outbuffer = NULL;
    DWORD32 outlen = 0;

    DWORD32 nRet = UtilUniIconv((char *)pstrMultibyte, dwMultiSize, &outbuffer, &outlen, "UTF-8", LinuxCodePage[eCodePage]);

    if (nRet != FR_OK) {
        LOG_W("[Util]    UtilUniANSI2UTF8 - Can not convert with iconv");
		return 0;
    }

    if (dwUtfSize < outlen) {
        outlen = dwUtfSize;
    }

    memcpy((void *)pstrUtf8 , (void *)outbuffer , outlen);
    FREE(outbuffer);

    return outlen;
#endif
}

DWORD32 UtilUniANSI2UTF16(const CHAR* pstrMultibyte, DWORD32 dwMultiSize, UTF16* pstrUtf16, DWORD32 dwUtf16Size, DtbWindowCopePage eCodePage)
{
#ifdef	_WIN32
	INT32 nLen;

	if (!dwMultiSize)
		dwMultiSize = (DWORD32)strlen(pstrMultibyte);

	// ANSI -> UTF-16
	nLen = MultiByteToWideChar(WindowCodePage[eCodePage], 0, pstrMultibyte, dwMultiSize, (WCHAR*)pstrUtf16, dwUtf16Size);

	return	(DWORD32)nLen;
#elif defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_ANDROID) || defined(SUPPORT_IOS)
    char *outbuffer = NULL;
    DWORD32 outlen = 0;

    DWORD32 nRet = UtilUniIconv((char *)pstrMultibyte, dwMultiSize, &outbuffer, &outlen, "UTF-16", LinuxCodePage[eCodePage]);

    if (nRet != FR_OK) {
        LOG_W("[Util]    UtilUniANSI2UTF16 - Can not convert with iconv, outlen(%d)", outlen);
		return 0;
    }

    if (dwUtf16Size < outlen) {
        outlen = dwUtf16Size;
    }

    //memcpy((void *)pstrUtf16 , (void *)outbuffer , outlen);
    // eliminate BOM
    memcpy((void*)pstrUtf16, (void*)(outbuffer+2), outlen-2);
    FREE(outbuffer);

    //return outlen;
    return outlen-2;
#endif
}

DWORD32 UtilUniUTF8toANSI(const UTF8* pstrUtf8, DWORD32 dwUtf8Size, CHAR* pstrMultibyte, DWORD32 dwMultiSize, DtbWindowCopePage eCodePage)
{
#ifdef	_WIN32
	UTF16 *pstrUnicode;
	INT32 nLen;

	if (!dwUtf8Size)
		dwUtf8Size = UtilUniUTF8Length(pstrUtf8);
	pstrUnicode = (UTF16 *)MALLOCZ(sizeof(UTF16) * (dwUtf8Size + 1));

	if (!pstrUnicode)
	{
		LOG_W("[Util]	UtilUniUTF8toANSI - Can not alloc %d bytes", sizeof(UTF16) * (dwUtf8Size + 1));
		return 0;
	}

	// UTF-8 -> UTF-16
	nLen = UtilUniUTF8toUTF16(pstrUtf8, dwUtf8Size, pstrUnicode, dwUtf8Size);
	// UTF-16 -> ANSI
	nLen = WideCharToMultiByte (WindowCodePage[eCodePage], 0, pstrUnicode, nLen, pstrMultibyte, dwMultiSize, NULL, NULL);

	FREE(pstrUnicode);

	return (DWORD32)nLen;
#elif defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_ANDROID) || defined(SUPPORT_IOS)
    char *outbuffer = NULL;
    DWORD32 outlen = 0;

    DWORD32 nRet = UtilUniIconv((char *)pstrUtf8, dwUtf8Size, &outbuffer, &outlen, LinuxCodePage[eCodePage], "UTF-8");

    if (nRet != FR_OK) {
        LOG_W("[Util]    UtilUniUTF8toANSI - Can not convert with iconv");
		return 0;
    }

    if (dwMultiSize < outlen) {
        outlen = dwMultiSize;
    }

    memcpy((void *)pstrMultibyte , (void *)outbuffer , outlen);
    FREE(outbuffer);

    return outlen;

#endif
}

DWORD32 UtilUniUTF16toANSI(const UTF16* pstrUtf16, DWORD32 dwUtf16Size, CHAR* pstrMultibyte, DWORD32 dwMultiSize, DtbWindowCopePage eCodePage)
{
#ifdef	_WIN32
	INT32 nLen;

	if (!dwUtf16Size)
		dwUtf16Size = UtilUniUTF16Length(pstrUtf16);

	// UTF-16 -> ANSI
	nLen = WideCharToMultiByte (WindowCodePage[eCodePage], 0, pstrUtf16, dwUtf16Size, pstrMultibyte, dwMultiSize, NULL, NULL);

	return (DWORD32)nLen;
#elif defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_ANDROID) || defined(SUPPORT_IOS)
    char *outbuffer = NULL;
    DWORD32 outlen = 0;

    DWORD32 nRet = UtilUniIconv((char *)pstrUtf16, dwUtf16Size, &outbuffer, &outlen, LinuxCodePage[eCodePage], "UTF-8");

    if (nRet != FR_OK) {
        LOG_W("[Util]    UtilUniUTF16toANSI - Can not convert with iconv");
		return 0;
    }

    if (dwMultiSize < outlen) {
        outlen = dwMultiSize;
    }

    memcpy((void *)pstrMultibyte , (void *)outbuffer , outlen);
    FREE(outbuffer);

    return outlen;
#endif
}

#if defined(Linux) || defined(SunOS) || defined(HPUX) || defined(MINGW) || defined(SUPPORT_ANDROID) || defined(SUPPORT_IOS)
#define OUTBUF_SIZE 32768
#define OUTBUF_LIMIT 10

DWORD32 UtilUniIconv(char *inbuf, DWORD32 inlen, char **outbuffer, DWORD32 *outlen, char *to, char *from)
{
    char *outbuf = NULL, *outbuf_top = NULL;
    size_t bufsize = 0;
    size_t inbytesleft, outbytesleft, status;
    iconv_t handle;

    handle = iconv_open(to, from);
    if (handle == (iconv_t)-1) {
        if (errno == EINVAL) {
			LOG_E("[Util]    UtilUniIconv : Conversion from %s to %s is not supported.", to, from);
          	return FR_FAIL;
        }
		LOG_E("iconv_open failed!");
        return FR_FAIL;
    }

    //inbytesleft = inlen + 1;
    inbytesleft = inlen;
    outbytesleft = 0;
    while (inbytesleft > 0) 
    {
        if (outbytesleft < OUTBUF_LIMIT) 
        {
            size_t outsize = outbuf - outbuf_top;
            outbuf_top = (char *)REALLOC(outbuf_top, bufsize+=OUTBUF_SIZE);
            if (!outbuf_top) {
				LOG_E("[Util]    UtilUniIconv : Unable to allocate memory."); 
				return FR_FAIL;
            }
            outbuf = outbuf_top + outsize;
            outbytesleft += OUTBUF_SIZE;
        }
#if !defined(SUPPORT_IOS) && (defined(Linux) || defined(SunOS) || defined(MINGW) || defined(SUPPORT_ANDROID))
        status = iconv(handle, (char **)&inbuf, &inbytesleft, &outbuf, &outbytesleft);
#else
        status = iconv(handle, (char **)&inbuf, &inbytesleft, &outbuf, &outbytesleft);
#endif
        if (status == (size_t)-1) 
        {
            if (errno == EINVAL || errno == EILSEQ) {
				LOG_E("[Util]    UtilUniIconv : Invalid character. %d", errno);
            	iconv_close(handle);
            	return FR_FAIL;
            }
        }
    }
    iconv_close(handle);

    *outbuffer = outbuf_top;
    *outlen = bufsize - outbytesleft;

    return FR_OK;
}

#endif


unsigned short* UtilUniWcsrchr(const unsigned short* string, unsigned short ch)
{
    unsigned short *start = (unsigned short *)string;
    while(*string++)
        ;
    while(--string != start && *string != (unsigned short)ch)
        ;
    if (*string == (unsigned short)ch)
        return ((unsigned short *)string);

    return NULL;
}

unsigned short* UtilUniWcscpy(unsigned short* s1, const unsigned short * s2)
{
    unsigned short *cp;

    cp = s1;
    while ((*cp++ = *s2++) != L'\0')
        ;

    return s1;
}

unsigned short * UtilUniWcscat(unsigned short* s1, const unsigned short* s2)
{
    unsigned short *cp;

    cp = s1;
    while (*cp != L'\0')
        cp++;
    while ((*cp++ = *s2++) != L'\0')
        ;

    return s1;
}

unsigned short * UtilUniWcsstr(const unsigned short * wcs1, const unsigned short * wcs2)
{
    unsigned short *cp = (unsigned short *)wcs1;
    unsigned short *s1, *s2;

    while (*cp)
    {
        s1 = cp;
        s2 = (unsigned short *)wcs2;

        while (*s1 && *s2 && !(*s1-*s2))
            s1++, s2++;

        if (!*s2)
            return cp;

        cp++;
    }

    return NULL;
}

DWORD32 UtilUniWcslen(const unsigned short* s)
{
    const unsigned short *p;

    p = s;
    while (*p)
        p++;

    return (DWORD32)(p - s);
}

unsigned short *UtilUniWcsncpy(unsigned short * dst, const unsigned short * src, size_t n)
{
    if (n != 0) 
	{
        unsigned short *d = dst;
        const unsigned short *s = src;

        do {
            if ((*d++ = *s++) == L'\0') {
                    /* NUL pad the remaining n-1 bytes */
                    while (--n != 0)
                            *d++ = L'\0';
                    break;
            }
        } while (--n != 0);
    }
    return (dst);
}
