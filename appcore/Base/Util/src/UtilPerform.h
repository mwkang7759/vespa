
#ifndef PERFORM_H
#define	PERFORM_H

#ifdef WIN32

#include <afxtempl.h>
#include <pdh.h>
#include <pdhmsg.h>
#include "UtilAPI.h"

#define MAX_RAW_VALUES 20

// NOTE: Find other counters using the function PdhBrowseCounters() (lookup in MSDN).
// This function was not implemented in this class.

typedef struct _tag_PDHCounterStruct {
	INT32 nIndex;				// The index of this counter, returned by AddCounter()
	LONG lValue;				// The current value of this counter
    HCOUNTER hCounter;			// Handle to the counter - given to use by PDH Library
    INT32 nNextIndex;			// element to get the next raw value
    INT32 nOldestIndex;			// element containing the oldes raw value
    INT32 nRawCount;			// number of elements containing raw values
    PDH_RAW_COUNTER a_RawValue[MAX_RAW_VALUES]; // Ring buffer to contain raw values
} PDHCOUNTERSTRUCT, *PPDHCOUNTERSTRUCT;
#else	// linux

#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "unistd.h"
#include "UtilAPI.h"

#endif


typedef struct
{
#ifdef WIN32
	//// VARIABLES ////
	//CArray<PPDHCOUNTERSTRUCT, PPDHCOUNTERSTRUCT> m_aCounters;	// the current counters
	//McQueHandle	m_hQue;
	HQUERY		m_hQuery;											// the query to the PDH
	INT32		m_nNextIndex;
	void*		m_pData;
#else
	unsigned long long lastTotalUser;
	unsigned long long lastTotalUserLow;
	unsigned long long lastTotalSys;
	unsigned long long lastTotalIdle;
#endif

}UtilPerfStruct;

#endif