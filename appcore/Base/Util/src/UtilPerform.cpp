#include "UtilPerform.h"


#ifdef WIN32
#pragma comment(lib, "pdh.lib")
#endif

McPerfHandle UtilPerfOpen()
{
	UtilPerfStruct*	hPerf;

	hPerf = (UtilPerfStruct*)MALLOCZ(sizeof(UtilPerfStruct));
	if(hPerf)
	{
#ifdef WIN32
		hPerf->m_nNextIndex = 0;
		if(PdhOpenQuery(NULL, 1, &hPerf->m_hQuery) != ERROR_SUCCESS)
		{
			FREE(hPerf);
			return NULL;
		}
#else
		FILE *file = fopen("/proc/stat", "r");
		fscanf(file, "cpu %Ld %Ld %Ld %Ld", &hPerf->lastTotalUser, &hPerf->lastTotalUserLow, &hPerf->lastTotalSys, &hPerf->lastTotalIdle);
		fclose(file);
#endif
	}

	return	hPerf;
}

void UtilPerfClose(McPerfHandle hHandle)
{
	UtilPerfStruct*	hPerf = (UtilPerfStruct *)hHandle;
	if(hPerf)
	{
#ifdef WIN32
		PdhCloseQuery(hPerf->m_hQuery);
		FREE(hPerf);
#else
		FREE(hPerf);
#endif
	}
}

#ifdef WIN32
PPDHCOUNTERSTRUCT UtilPerfGetCounterStruct(McPerfHandle hHandle, INT32 nIndex)
{
	UtilPerfStruct*	hPerf = (UtilPerfStruct *)hHandle;

	//for (INT32 i=0;i<hPerf->m_aCounters.GetSize();i++)
	//{
	//	if (hPerf->m_aCounters.GetAt(i)->nIndex == nIndex)
	//		return hPerf->m_aCounters.GetAt(i);
	//}
	//return (PPDHCOUNTERSTRUCT)UtilQueGetData(hPerf->m_hQue, NULL);
	return (PPDHCOUNTERSTRUCT)hPerf->m_pData;
}
#endif

BOOL UtilPerfInit(McPerfHandle hHandle)
{
	UtilPerfStruct*	hPerf = (UtilPerfStruct *)hHandle;
	if(hPerf)
	{
		//hPerf->m_hQue = UtilQueOpen();
	}
	return TRUE;
}

void UtilPerfUninit(McPerfHandle hHandle)
{
#ifdef WIN32
	UtilPerfStruct*	hPerf = (UtilPerfStruct *)hHandle;
#endif	
	// clean memory
	//for (INT32 i=0;i<hPerf->m_aCounters.GetSize();i++)
	//{
	//	PPDHCOUNTERSTRUCT lpValue = hPerf->m_aCounters.GetAt(i);
	//	UtilPerfRemoveCounter(hPerf, lpValue->nIndex);
	//	delete lpValue;
	//	lpValue = NULL;
	//}
	//hPerf->m_aCounters.RemoveAll();
#ifdef WIN32
	//PPDHCOUNTERSTRUCT lpValue = (PPDHCOUNTERSTRUCT)UtilQueGetData(hPerf->m_hQue, NULL);
	PPDHCOUNTERSTRUCT lpValue = (PPDHCOUNTERSTRUCT)hPerf->m_pData;
	if (lpValue)
	{
		UtilPerfRemoveCounter(hPerf, lpValue->nIndex);
		delete lpValue;
		lpValue = NULL;
	}
#endif
	//UtilQueClose(hPerf->m_hQue);
}

INT32 UtilPerfAddCounter(McPerfHandle hHandle, const char *pszCounterName)
{
#ifdef WIN32
	UtilPerfStruct*	hPerf = (UtilPerfStruct *)hHandle;
#endif
#ifdef WIN32
	PPDHCOUNTERSTRUCT pCounter;
	pCounter = new PDHCOUNTERSTRUCT;
	if (!pCounter) 
		return -1;

	// add to current query
	if (PdhAddCounterA(hPerf->m_hQuery, pszCounterName, (DWORD32)pCounter, &(pCounter->hCounter)) != ERROR_SUCCESS)
	{
		delete pCounter; // clean mem
		return -1;
	}

	// insert counter into array(s)
	pCounter->nIndex = hPerf->m_nNextIndex++;
	pCounter->lValue = 0;
	pCounter->nNextIndex = 0;
	pCounter->nOldestIndex = 0;
	pCounter->nRawCount = 0;

	//hPerf->m_aCounters.Add(pCounter);
	//UtilQuePutData(hPerf->m_hQue, pCounter, sizeof(PDHCOUNTERSTRUCT));
	hPerf->m_pData = (void*)pCounter;

	return pCounter->nIndex;
#else
	return 1;
#endif
}

BOOL UtilPerfRemoveCounter(McPerfHandle hHandle, INT32 nIndex)
{
#ifdef WIN32
	UtilPerfStruct*	hPerf = (UtilPerfStruct *)hHandle;

	PPDHCOUNTERSTRUCT pCounter = UtilPerfGetCounterStruct(hPerf, nIndex);
	if (!pCounter) 
		return FALSE;
	
	if (PdhRemoveCounter(pCounter->hCounter) != ERROR_SUCCESS)
		return FALSE;
		
	return TRUE;
#endif
    
    return TRUE;
}

BOOL UtilPerfCollectQueryData(McPerfHandle hHandle)
{
#ifdef WIN32
	UtilPerfStruct*	hPerf = (UtilPerfStruct *)hHandle;

	if (PdhCollectQueryData(hPerf->m_hQuery) != ERROR_SUCCESS) 
		return FALSE;

	return TRUE;
#else
	return TRUE;
#endif
    
    return TRUE;
}

#ifdef WIN32
BOOL UtilPerfUpdateValue(McPerfHandle hHandle, PPDHCOUNTERSTRUCT pCounter)
{
	UtilPerfStruct*	hPerf = (UtilPerfStruct *)hHandle;
	PDH_FMT_COUNTERVALUE pdhFormattedValue;

	// get the value from the PDH
	if (PdhGetFormattedCounterValue(pCounter->hCounter, PDH_FMT_LONG, NULL, &pdhFormattedValue) != ERROR_SUCCESS)
		return FALSE;

	// test the value for validity
	if (pdhFormattedValue.CStatus != ERROR_SUCCESS)
		return FALSE;

	// set value
	pCounter->lValue = pdhFormattedValue.longValue;

	return TRUE;
}
#endif

#ifdef WIN32
BOOL UtilPerfUpdateRawValue(McPerfHandle hHandle, PPDHCOUNTERSTRUCT pCounter)
{
	UtilPerfStruct*	hPerf = (UtilPerfStruct *)hHandle;
	PPDH_RAW_COUNTER ppdhRawCounter;

    // Assign the next value into the array
    ppdhRawCounter = &(pCounter->a_RawValue[pCounter->nNextIndex]);

	if (PdhGetRawCounterValue(pCounter->hCounter, NULL, ppdhRawCounter) != ERROR_SUCCESS)
		return FALSE;
	
    // update raw counter - up to MAX_RAW_VALUES
    pCounter->nRawCount = min(pCounter->nRawCount + 1, MAX_RAW_VALUES);

    // Update next index - rolls back to zero upon reaching MAX_RAW_VALUES
    pCounter->nNextIndex = (pCounter->nNextIndex + 1) % MAX_RAW_VALUES;

    // The Oldest index remains zero until the array is filled.
    // It will now be the same as the 'next' index since it was previously assigned.
    if (pCounter->nRawCount >= MAX_RAW_VALUES)
        pCounter->nOldestIndex = pCounter->nNextIndex;

	return TRUE;
}
#endif

BOOL UtilPerfGetStatistics(McPerfHandle hHandle, UINT32 *nMin, UINT32 *nMax, UINT32 *nMean, INT32 nIndex)
{
	UtilPerfStruct*	hPerf = (UtilPerfStruct *)hHandle;
#ifdef WIN32
	PDH_STATISTICS pdhStats;
	PPDHCOUNTERSTRUCT pCounter = UtilPerfGetCounterStruct(hPerf, nIndex);
	if (!pCounter) 
		return FALSE;

	if (PdhComputeCounterStatistics(pCounter->hCounter, PDH_FMT_LONG, pCounter->nOldestIndex, pCounter->nRawCount, pCounter->a_RawValue, &pdhStats) != ERROR_SUCCESS) 
		return FALSE;

	// set values
	if (pdhStats.min.CStatus != ERROR_SUCCESS)
		*nMin = 0;
	else
		*nMin = pdhStats.min.longValue;

	if (pdhStats.max.CStatus != ERROR_SUCCESS)
		*nMax = 0;
	else
		*nMax = pdhStats.max.longValue;

	if (pdhStats.mean.CStatus != ERROR_SUCCESS)
		*nMean = 0;
	else
		*nMean = pdhStats.mean.longValue;

	return TRUE;
#else
	double percent=0.0;
	FILE* file;
	unsigned long long totalUser, totalUserLow, totalSys, totalIdle, total;

	file = fopen("/proc/stat", "r");
	fscanf(file, "cpu %Ld %Ld %Ld %Ld", &totalUser, &totalUserLow, &totalSys, &totalIdle);
	fclose(file);

	if (totalUser < hPerf->lastTotalUser || totalUserLow < hPerf->lastTotalUserLow || totalSys < hPerf->lastTotalSys || totalIdle < hPerf->lastTotalIdle)
	{
		// overflow detection..just skip this value..
		//percent = -1.0;
		return FALSE;
	}
	else
	{
		total = (totalUser - hPerf->lastTotalUser) + (totalUserLow - hPerf->lastTotalUserLow) + (totalSys - hPerf->lastTotalSys) + (totalIdle - hPerf->lastTotalIdle) ;
		//printf("total cpu val = %2d\n", total);
		percent = ((double)(totalUser - hPerf->lastTotalUser) / total) * 100;
		*nMax = (UINT32)percent;
		*nMin = (UINT32)percent;
		*nMean = (UINT32)percent;
		//printf("user mode cpu val = %2.1f\n", percent);
		//percent = ((double)(totalSys - lastTotalSys) / total) * 100;
		//printf("system mode cpu val = %2.1f\n", percent);
	}
	hPerf->lastTotalUser = totalUser;
	hPerf->lastTotalUserLow = totalUserLow;
	hPerf->lastTotalSys = totalSys;
	hPerf->lastTotalIdle = totalIdle;

	return TRUE;
#endif
}

LONG UtilPerfGetCounterValue(McPerfHandle hHandle, INT32 nIndex)
{
#ifdef WIN32
	UtilPerfStruct*	hPerf = (UtilPerfStruct *)hHandle;

	PPDHCOUNTERSTRUCT pCounter = UtilPerfGetCounterStruct(hPerf, nIndex);
	if (!pCounter) 
		return -999L;

	// update the value(s)
	if (!UtilPerfUpdateValue(hPerf, pCounter))
		return -999L;
	if (!UtilPerfUpdateRawValue(hPerf, pCounter))
		return -999L;

	// return the value 
	return pCounter->lValue;
#else
	return 1;
#endif
}
