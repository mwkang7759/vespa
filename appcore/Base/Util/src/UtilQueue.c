
#include "UtilQueue.h"

McQueHandle UtilQueOpen()
{
	UtilQueStruct*	hQue;

	hQue = (UtilQueStruct*)MALLOCZ(sizeof(UtilQueStruct));
	if (hQue)
	{
		FrCreateMutex(&hQue->m_xLock);
        hQue->m_nCnt = 0;
	}

	return	hQue;
}

McQueHandle UtilQueCntOpen(int nCnt)
{
	UtilQueStruct*	hQue;

	hQue = (UtilQueStruct*)MALLOCZ(sizeof(UtilQueStruct));
	if (hQue)
	{
		FrCreateMutex(&hQue->m_xLock);
        hQue->m_nSize = nCnt;
	}

	return	hQue;
}

McAQueHandle UtilAQueCntOpen(int nCnt)
{
    UtilAQueStruct*  hAQue;

    hAQue = (UtilAQueStruct*)MALLOCZ(sizeof(UtilAQueStruct));
    if (hAQue)
    {
        hAQue->m_dwElement = nCnt;
        hAQue->m_pBuffer = (AQueNodeStruct *)MALLOCZ(sizeof(AQueNodeStruct) * hAQue->m_dwElement);
        if (!hAQue->m_pBuffer)
        {
            LOG_E("[UtilQue] UtilAQueCntOpen - memory alloc failed");
            FREE(hAQue);
            hAQue = NULL;
        }
    }

    return hAQue;
}

void UtilQueClose(McQueHandle hHandle)
{
	UtilQueStruct*	hQue = (UtilQueStruct*)hHandle;
	QueNodeHandle	hNode;

	TRACE("[Util] Queue close start (Count %d)", hQue ? hQue->m_nCnt: 0);
	if (hQue)
	{
		FrEnterMutex(&hQue->m_xLock);
		while (hQue->m_pHead)
		{
			hNode = hQue->m_pHead;
			hQue->m_pHead = hNode->m_pNext;
			FREE(hNode->m_pData);
			FREE(hNode);
		}

		FrLeaveMutex(&hQue->m_xLock);
		FrDeleteMutex(&hQue->m_xLock);
		FREE(hQue);
	}
	TRACE("[Util] Queue close ends");
}

void UtilAQueClose(McAQueHandle hHandle)
{
    UtilAQueStruct*  hAQue = (UtilAQueStruct*)hHandle;
    DWORD32          i;

    TRACE("[UtilQue] AQueue close start");
    if (hAQue)
    {
        for(i=0; i<hAQue->m_dwElement; i++)
            FREE(hAQue->m_pBuffer[i].m_pData);

        FREE(hAQue->m_pBuffer);
        FREE(hAQue);
    }
    TRACE("[UtilQue] AQueue close ends");
}

void UtilQueReset(McQueHandle hHandle)
{
	UtilQueStruct*	hQue = (UtilQueStruct*)hHandle;
	QueNodeHandle	hNode;

	TRACE("[Util] Queue reset start");
	if (hQue)
	{
		FrEnterMutex(&hQue->m_xLock);
		while (hQue->m_pHead)
		{
			hNode = hQue->m_pHead;
			hQue->m_pHead = hNode->m_pNext;
			FREE(hNode->m_pData);
			FREE(hNode);
		}
		hQue->m_pHead = NULL;
		hQue->m_pTail = NULL;
		hQue->m_nCnt = 0;
		FrLeaveMutex(&hQue->m_xLock);
	}
	TRACE("[Util] Queue reset end");
}

BOOL UtilQuePutData(McQueHandle hHandle, void *pData, DWORD32 dwLen)
{
	UtilQueStruct*	hQue = (UtilQueStruct*)hHandle;
	QueNodeHandle	hNode;

	if (!hQue)
		return FALSE;

	FrEnterMutex(&hQue->m_xLock);

	if ((hQue->m_nSize > hQue->m_nCnt) || (hQue->m_nSize == 0)) {
		hNode = (QueNodeHandle)MALLOC(sizeof(QueNodeStruct));
		hNode->m_pData = pData;
		hNode->m_dwLen = dwLen;

		if (!hQue->m_pHead)
		{
			hNode->m_pNext = NULL;
			hQue->m_pHead = hNode;
			hQue->m_pTail = hNode;
		}
		else
		{
            //LOG_I("[UtilQue] UtilQuePutData() - pHead(0x%x) pTail(0x%x) Cnt(%d)", hQue->m_pHead, hQue->m_pTail, hQue->m_nCnt);
            
            //if (!hQue->m_pTail)
            //    LOG_I("[UtilQue] UtilQuePutData() - Error !!!!!!!!!!!!!!!!");
            
			hQue->m_pTail->m_pNext = hNode;
			hNode->m_pNext = NULL;
			hQue->m_pTail = hNode;
		}

		hQue->m_nCnt++;
        //LOG_I("[UtilQue] UtilQuePutData() Cnt(%d)", hQue->m_nCnt);
    }
    else {
        FrLeaveMutex(&hQue->m_xLock);
        return FALSE;
    }

	FrLeaveMutex(&hQue->m_xLock);

	return	TRUE;
}

BOOL UtilAQuePutData(McAQueHandle hHandle, void *pData, DWORD32 dwLen)
{
    UtilAQueStruct*  hAQue = (UtilAQueStruct*)hHandle;
    AQueNodeStruct*  pElement;
    DWORD32          dwNextWriteIdx;

    if (!hAQue)
        return FALSE;

    dwNextWriteIdx = hAQue->m_dwWriteIdx+1;

    if (hAQue->m_dwElement <= dwNextWriteIdx)
        dwNextWriteIdx = 0;

    if (hAQue->m_dwReadIdx == dwNextWriteIdx)
        return FALSE;

    pElement = hAQue->m_pBuffer + hAQue->m_dwWriteIdx;

    pElement->m_pData = (BYTE *)MALLOC(dwLen);
    if (!pElement->m_pData)
    {
        LOG_E("[UtilQue] UtilAQuePutData - memory alloc failed");
        return FALSE;
    }

    pElement->m_dwLen = dwLen;
    memcpy(pElement->m_pData, pData, pElement->m_dwLen);

    hAQue->m_dwWriteIdx = dwNextWriteIdx;

    return TRUE;
}

BOOL UtilQuePutDataX(McQueHandle hHandle, void *pData, DWORD32 dwLen)
{
	UtilQueStruct*	hQue = (UtilQueStruct*)hHandle;
	QueNodeHandle	hNode;

	if (!hQue)
		return FALSE;

    hNode = (QueNodeHandle)MALLOC(sizeof(QueNodeStruct));
	hNode->m_pData = pData;
	hNode->m_dwLen = dwLen;
    hNode->m_pNext = NULL;

	if (!hQue->m_pHead)
	{
	  //hNode->m_pNext = NULL;
	  //hQue->m_pHead = hNode;
		hQue->m_pTail = hNode;
	}
	else
	{
		hQue->m_pTail->m_pNext = hNode;
	  //hNode->m_pNext = NULL;
		hQue->m_pTail = hNode;
	}

	return	TRUE;
}

void* UtilQueGetDataX(McQueHandle hHandle, DWORD32* pdwLen)
{
	UtilQueStruct*	hQue = (UtilQueStruct*)hHandle;
	QueNodeHandle	hNode;
	void*			pData;

	if (!hQue)
		return NULL;

	if (!hQue->m_pHead)
	{
		if (pdwLen)
			*pdwLen = 0;
		return	NULL;
	}

	hNode = hQue->m_pHead;
	pData = hNode->m_pData;
	if (pdwLen)
		*pdwLen = hNode->m_dwLen;
	hQue->m_pHead = hNode->m_pNext;
  //if (!hQue->m_pHead)
  //    hQue->m_pTail = NULL;

    FREE(hNode);

	return	pData;
}

void* UtilQueGetData(McQueHandle hHandle, DWORD32* pdwLen)
{
	UtilQueStruct*	hQue = (UtilQueStruct*)hHandle;
	QueNodeHandle	hNode;
	void*			pData;

	if (!hQue)
		return NULL;

	FrEnterMutex(&hQue->m_xLock);

	if (!hQue->m_pHead)
	{
		if (pdwLen)
			*pdwLen = 0;
		FrLeaveMutex(&hQue->m_xLock);
		return	NULL;
	}

	hNode = hQue->m_pHead;
	pData = hNode->m_pData;
	if (pdwLen)
		*pdwLen = hNode->m_dwLen;
	hQue->m_pHead = hNode->m_pNext;
	if (!hQue->m_pHead)
		hQue->m_pTail = NULL;
	FREE(hNode);

	hQue->m_nCnt--;

	FrLeaveMutex(&hQue->m_xLock);

	return	pData;
}

void* UtilQuePeekData(McQueHandle hHandle, DWORD32* pdwLen)
{
	UtilQueStruct*	hQue = (UtilQueStruct*)hHandle;
	QueNodeHandle	hNode;
	void*			pData;

	if (!hQue)
		return NULL;

	FrEnterMutex(&hQue->m_xLock);

	if (!hQue->m_pHead)
	{
		if (pdwLen)
			*pdwLen = 0;
		FrLeaveMutex(&hQue->m_xLock);
		return	NULL;
	}

	hNode = hQue->m_pHead;
	pData = hNode->m_pData;
	if (pdwLen)
		*pdwLen = hNode->m_dwLen;

	FrLeaveMutex(&hQue->m_xLock);

	return	pData;
}

void* UtilQuePeekDataEx(McQueHandle hHandle, DWORD32* pdwLen, DWORD32 dwIndex)
{
	UtilQueStruct*	hQue = (UtilQueStruct*)hHandle;
	QueNodeHandle	hHead;
	QueNodeHandle	hNode;
	void*			pData = NULL;
	int nIndx = 0;

	if (!hQue)
		return NULL;

	FrEnterMutex(&hQue->m_xLock);

	if (!hQue->m_pHead)
	{
		if (pdwLen)
			*pdwLen = 0;
		FrLeaveMutex(&hQue->m_xLock);
		return	NULL;
	}

	hHead = hQue->m_pHead;
	for (nIndx=0; nIndx<dwIndex; nIndx++)
	{
		if (hHead)
			hHead = hHead->m_pNext;
		else
			break;
	}

	if (hHead)
	{
		hNode = hHead;
		pData = hNode->m_pData;
		if (pdwLen)
			*pdwLen = hNode->m_dwLen;
	}

	FrLeaveMutex(&hQue->m_xLock);

	return	pData;
}

BOOL UtilAQueGetData(McAQueHandle hHandle, void* pData)
{
    UtilAQueStruct*  hAQue = (UtilAQueStruct*)hHandle;
    AQueNodeStruct*  pElement;

    if (hAQue->m_dwReadIdx == hAQue->m_dwWriteIdx)
        return FALSE;

    pElement = hAQue->m_pBuffer + hAQue->m_dwReadIdx;

    memcpy(pData, pElement->m_pData, pElement->m_dwLen);
    free(pElement->m_pData);
    pElement->m_pData = NULL;

    hAQue->m_dwReadIdx++;
    if (hAQue->m_dwElement <= hAQue->m_dwReadIdx)
        hAQue->m_dwReadIdx = 0;

    return TRUE;
}

int UtilQueGetCount(McQueHandle hHandle)
{
	UtilQueStruct*	hQue = (UtilQueStruct*)hHandle;

	if (hQue)
		return hQue->m_nCnt;
	else
		return 0;
}

int UtilAQueGetCount(McAQueHandle hHandle)
{
    UtilAQueStruct*  hAQue = (UtilAQueStruct*)hHandle;

    if (hAQue)
    {
        if (hAQue->m_dwReadIdx <= hAQue->m_dwWriteIdx)
            return hAQue->m_dwWriteIdx - hAQue->m_dwReadIdx;
        else
            return hAQue->m_dwWriteIdx + hAQue->m_dwElement - hAQue->m_dwReadIdx;
    }
    else
        return 0;
}

int UtilAQueGetSize(McAQueHandle hHandle)
{
    UtilAQueStruct*  hAQue = (UtilAQueStruct*)hHandle;

	if (hAQue)
		return hAQue->m_dwElement;
	else
		return 0;
}

void* UtilQueSetHeadData(McQueHandle hHandle, DWORD32* pdwLen)
{
	UtilQueStruct*	hQue = (UtilQueStruct*)hHandle;
	void*			pData;

	FrEnterMutex(&hQue->m_xLock);

	if (!hQue->m_pHead)
	{
		if (pdwLen)
			*pdwLen = 0;
		FrLeaveMutex(&hQue->m_xLock);
		return	NULL;
	}

	pData = hQue->m_pHead->m_pData;
	if (pdwLen)
		*pdwLen = hQue->m_pHead->m_dwLen;
	hQue->m_pCur = hQue->m_pHead;

	FrLeaveMutex(&hQue->m_xLock);

	return	pData;
}

void* UtilQueShowData(McQueHandle hHandle, DWORD32* pdwLen)
{
	UtilQueStruct*	hQue = (UtilQueStruct*)hHandle;
	void*			pData;

	FrEnterMutex(&hQue->m_xLock);

	if (!hQue->m_pCur)
	{
		if (pdwLen)
			*pdwLen = 0;
		FrLeaveMutex(&hQue->m_xLock);
		return	NULL;
	}

	pData = hQue->m_pCur->m_pData;
	if (pdwLen)
		*pdwLen = hQue->m_pCur->m_dwLen;
	hQue->m_pCur = hQue->m_pCur->m_pNext;

	FrLeaveMutex(&hQue->m_xLock);

	return	pData;
}

// start of live TS
BOOL UtilQueWriteData(McQueHandle hHandle, void *pData, DWORD32 dwLen)
{
	UtilQueStruct*	hQue = (UtilQueStruct*)hHandle;
	QueNodeHandle	hNode;

	FrEnterMutex(&hQue->m_xLock);

	hNode = (QueNodeHandle)MALLOC(sizeof(QueNodeStruct));
	hNode->m_pData = pData;
	hNode->m_dwLen = dwLen;

	if (!hQue->m_pHead)
	{
		hNode->m_pNext = NULL;
		hQue->m_pHead = hNode;
		hQue->m_pTail = hNode;
		hQue->m_pStart = hNode;		// mwkang
		hQue->m_pEnd = NULL;
	}
	else
	{
		hQue->m_pTail->m_pNext = hNode;
		hNode->m_pNext = NULL;
		hQue->m_pTail = hNode;
	}

	FrLeaveMutex(&hQue->m_xLock);

	return	TRUE;
}

void* UtilQueReadData(McQueHandle hHandle, DWORD32* pdwLen)
{
	UtilQueStruct*	hQue = (UtilQueStruct*)hHandle;
	QueNodeHandle	hNode;
	void*			pData;

	FrEnterMutex(&hQue->m_xLock);

	if (!hQue->m_pHead)
	{
		if (pdwLen)
			*pdwLen = 0;
		FrLeaveMutex(&hQue->m_xLock);
		return	NULL;
	}

	hNode = hQue->m_pHead;
	pData = hNode->m_pData;
	if (pdwLen)
		*pdwLen = hNode->m_dwLen;
	hQue->m_pHead = hNode->m_pNext;
	if (!hQue->m_pHead)
		hQue->m_pTail = NULL;
	//FREE(hNode);

	FrLeaveMutex(&hQue->m_xLock);

	return	pData;
}

void* UtilQueShowHeadData(McQueHandle hHandle, DWORD32* pdwLen)
{
	UtilQueStruct*	hQue = (UtilQueStruct*)hHandle;
	QueNodeHandle	hNode;
	void*			pData;

	FrEnterMutex(&hQue->m_xLock);

	if (!hQue->m_pHead)
	{
		if (pdwLen)
			*pdwLen = 0;
		FrLeaveMutex(&hQue->m_xLock);
		return	NULL;
	}

	hNode = hQue->m_pHead;
	pData = hNode->m_pData;
	if (pdwLen)
		*pdwLen = hNode->m_dwLen;
	FrLeaveMutex(&hQue->m_xLock);

	return	pData;
}

void* UtilQueSetStart(McQueHandle hHandle)
{
	UtilQueStruct*	hQue = (UtilQueStruct*)hHandle;
	QueNodeHandle	hNode;
	void*			pData;

	FrEnterMutex(&hQue->m_xLock);

	if (!hQue->m_pStart)
	{
		FrLeaveMutex(&hQue->m_xLock);
		return	NULL;
	}

	hNode = hQue->m_pEnd;
	if(hNode)
		FREE(hNode);

	hQue->m_pHead = hQue->m_pStart;
	hQue->m_pEnd = hQue->m_pStart;
	hNode = hQue->m_pHead;

	pData = hNode->m_pData;
	hQue->m_pStart = hNode->m_pNext;

	FrLeaveMutex(&hQue->m_xLock);

	return	pData;
}

void UtilQueClear(McQueHandle hHandle)
{
	UtilQueStruct*	hQue = (UtilQueStruct*)hHandle;
	QueNodeHandle	hNode;

	if (hQue)
	{
		FrDeleteMutex(&hQue->m_xLock);

		hNode = hQue->m_pEnd;
		if(hNode)
			FREE(hNode);

		while (hQue->m_pStart)
		{
			hNode = hQue->m_pStart;
			hQue->m_pStart = hNode->m_pNext;
			FREE(hNode->m_pData);
			FREE(hNode);
		}

		FREE(hQue);
	}
}

// end of live TS
