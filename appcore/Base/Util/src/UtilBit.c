
#include "Util.h"

/* to mask the n least significant bits of an integer */

static const UINT32 msk[33] =
{
	0x00000000, 0x00000001, 0x00000003, 0x00000007,
	0x0000000f, 0x0000001f, 0x0000003f, 0x0000007f,
	0x000000ff, 0x000001ff, 0x000003ff, 0x000007ff,
	0x00000fff, 0x00001fff, 0x00003fff, 0x00007fff,
	0x0000ffff, 0x0001ffff, 0x0003ffff, 0x0007ffff,
	0x000fffff, 0x001fffff, 0x003fffff, 0x007fffff,
	0x00ffffff, 0x01ffffff, 0x03ffffff, 0x07ffffff,
	0x0fffffff, 0x1fffffff, 0x3fffffff, 0x7fffffff,
	0xffffffff
};

/* initialize buffer, call once before first getbits or showbits */

McBitHandle UtilBitOpen(unsigned char* pBuffer, INT32 iBufferLen)
{
	BitStruct*	hBit;

	hBit = (BitStruct*)MALLOC(sizeof(BitStruct));
	if (hBit)
	{
		hBit->incnt = 0;
		hBit->framebits = 0;
		hBit->bitcnt = 0;
		hBit->rdptr = pBuffer;
		hBit->bookmark = 0;
		hBit->bitlength = iBufferLen * 8;
	}

	return	hBit;
}

void UtilBitClose(McBitHandle hHandle)
{
	BitStruct*	hBit = (BitStruct*)hHandle;

	if (hBit)
		FREE(hBit);
}

UINT32 UtilBitShow(McBitHandle hHandle, INT32 n)
{
	BitStruct*	hBit = (BitStruct*)hHandle;
	unsigned char *v = hBit->rdptr;
	INT32 rbit = 32 - hBit->bitcnt;
	UINT32 b;

	b = ConvByteToDWORD(v);
	return ((b & msk[rbit]) >> (rbit-n));
}

void UtilBitFlush(McBitHandle hHandle, INT32 n)
{
	BitStruct*	hBit = (BitStruct*)hHandle;

	hBit->bitcnt += n;
	hBit->rdptr += (hBit->bitcnt>>3);
	hBit->bitcnt &= 7;
	hBit->framebits += n;
}

// return next n bits (right adjusted
// n must be less than 32
UINT32 UtilBitGetBit(McBitHandle hHandle, INT32 n)
{
	BitStruct*	hBit = (BitStruct*)hHandle;
	UINT32 l;
	unsigned char *v = hBit->rdptr;
	INT32 rbit = 32 - hBit->bitcnt;
	INT32 remainbit = hBit->bitlength - hBit->framebits;
	//UINT32 b;

	if (n==0)
		return 0;

	if (hBit->framebits + n > hBit->bitlength)
	{
		LOG_I("[Util] UtilBitGetBit - hBit->framebits+n(%d, %d), hBit->bitlength(%d)", hBit->framebits, n, hBit->bitlength);
		hBit->framebits = hBit->bitlength;
		return -1;
	}

	//l = faad_showbits(ld, n);
	//faad_flushbits(ld, n);

	if (remainbit <= 8)
		l = v[0] << 24;
	else if (remainbit <= 16)
		l = ConvByteToWORD(v) << 16;
	else if (remainbit <= 24)
		l = Conv3ByteToDWORD(v) << 8;
	else
		l = ConvByteToDWORD(v);
	
	l = ((l & msk[rbit]) >> (rbit-n));
	hBit->bitcnt += n;
	hBit->rdptr += (hBit->bitcnt>>3);
	hBit->bitcnt &= 7;
	hBit->framebits += n;

	return l;
}

// return next n bits (right adjusted
// n must be less than 32
UINT64 UtilBitGetBit64(McBitHandle hHandle, INT32 n)
{
	BitStruct*	hBit = (BitStruct*)hHandle;
	UINT64 l;
	//unsigned char *v = hBit->rdptr;
	//INT32 rbit = 32 - hBit->bitcnt;
	//INT32 remainbit = hBit->bitlength - hBit->framebits;
	//UINT32 b;

	if (64 <= n)
	{
		LOG_I("[Util] UtilBitGetBit64 - length n (%d) must be less than 64", n);
		return (UINT64)-1;
	}

	if (hBit->framebits + n > hBit->bitlength)
	{
		LOG_I("[Util] UtilBitGetBit64 - hBit->framebits+n(%d, %d), hBit->bitlength(%d)", hBit->framebits, n, hBit->bitlength);
		hBit->framebits = hBit->bitlength;
		return (UINT64)-1;
	}

	if (n <= 32)
		l = UtilBitGetBit(hHandle, n);
	else
	{
		l = UtilBitGetBit(hHandle, 32);
		n = n - 32;
		l = l << n;
		l += UtilBitGetBit(hHandle, n);
	}

	return l;
}

void UtilBitPutBit(McBitHandle hHandle, INT32 n, UINT32 m)
{
	BitStruct*	hBit = (BitStruct*)hHandle;
	UINT32 l;
	unsigned char *v = hBit->rdptr;
	INT32 wbit = 32 - hBit->bitcnt;

	l = ConvByteToDWORD(v);
	l = ((l>>wbit)<<wbit);
	m = ((m & msk[n]) << (wbit-n));
	l = l | m;
	ConvDWORDToBYTE(l, v);

	hBit->bitcnt += n;
	hBit->rdptr += (hBit->bitcnt>>3);
	hBit->bitcnt &= 7;
	hBit->framebits += n;
}

UINT32 UtilBitGetFast(McBitHandle hHandle, INT32 n)
{
	BitStruct*	hBit = (BitStruct*)hHandle;
	UINT32 l;

	l =  (unsigned char) (hBit->rdptr[0] << hBit->bitcnt);
	l |= ((UINT32) hBit->rdptr[1] << hBit->bitcnt)>>8;
	l <<= n;
	l >>= 8;

	hBit->bitcnt += n;
	hBit->framebits += n;
	hBit->rdptr += (hBit->bitcnt>>3);
	hBit->bitcnt &= 7;

	return l;
}

UINT32 UtilBitGet1Bit(McBitHandle hHandle)
{
	BitStruct*	hBit = (BitStruct*)hHandle;
	unsigned char l;

	l = *hBit->rdptr << hBit->bitcnt;

	hBit->bitcnt++;
	hBit->framebits++;
	hBit->rdptr += (hBit->bitcnt>>3);
	hBit->bitcnt &= 7;

	return l>>7;
}

void UtilBitBookmark(McBitHandle hHandle, INT32 state)
{
	BitStruct*	hBit = (BitStruct*)hHandle;

	if (state != 0) {
		//assert(hBit->bookmark == 0);
		hBit->book_rdptr = hBit->rdptr;
		hBit->book_incnt = hBit->incnt;
		hBit->book_bitcnt = hBit->bitcnt;
		hBit->book_framebits = hBit->framebits;
		hBit->bookmark = 1;
	} else {
		//assert(hBit->bookmark == 1);
		hBit->rdptr = hBit->book_rdptr;
		hBit->incnt = hBit->book_incnt;
		hBit->bitcnt = hBit->book_bitcnt;
		hBit->framebits = hBit->book_framebits;
		hBit->bookmark = 0;
	}
}

INT32 UtilBitGetProcessed(McBitHandle hHandle)
{
	BitStruct*	hBit = (BitStruct*)hHandle;
	return (hBit->framebits);
}

INT32 UtilBitLeftBits(McBitHandle hHandle)
{
	BitStruct*	hBit = (BitStruct*)hHandle;
	return (hBit->bitlength - hBit->framebits);
}

UINT32 UtilBitByteAlign(McBitHandle hHandle)
{
	BitStruct*	hBit = (BitStruct*)hHandle;
    INT32			i=0;
	
	while(hBit->bitcnt!=0)
	{
		UtilBitGet1Bit(hBit);
		i += 1;
	}
	
    return	(i);
}
