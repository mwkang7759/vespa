
#include "Util.h"

static const char base64_encode[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

static const INT32 base64_decode[128] = {
	0, 0, 0, 0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0,  0, 0, 0, 0, 0, 0, 0,62, 0, 0, 0,63,52,53,54,55, 56,57,58,59,60,61, 0, 0, 0, 0, 0, 0,
	0, 0, 1, 2,  3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16,17,18, 19,20,21,22,23,24,25, 0, 0, 0, 0, 0,
	0,26,27,28, 29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44, 45,46,47,48,49,50,51, 0, 0, 0, 0, 0
};

BYTE* UtilBase64Encoding(BYTE *inData, INT32 inSize, INT32 *outSize)
{
	BYTE *buf;
	INT32 i = 0;
	INT32 mod = inSize % 3;
	INT32 n = inSize / 3;

	buf = (BYTE*)MALLOC((INT32)((inSize+2)*4/3)+2);
    memset(buf, 0x00, (INT32)((inSize+2)*4/3)+2);

	for(i = 0; i < inSize/3; i++)
	{
		buf[i*4+0] = base64_encode[  inData[i*3+0] / (BYTE)4];
		buf[i*4+1] = base64_encode[((inData[i*3+0] & (BYTE)3)    * (BYTE)16) | (inData[i*3+1] / (BYTE)(16*1))];
		buf[i*4+2] = base64_encode[((inData[i*3+1] & (BYTE)0x0f) * (BYTE)4)  | (inData[i*3+2] / (BYTE)(16*4))];
		buf[i*4+3] = base64_encode[ (inData[i*3+2] & (BYTE)0x03f)];
	}

	if (mod == 1)
	{
		buf[i*4+0] = base64_encode[  inData[n*3+0] / (BYTE)4];
		buf[i*4+1] = base64_encode[((inData[n*3+0] & (BYTE)3)    * (BYTE)16) | (inData[n*3+1] / (BYTE)(16*1))];
		buf[i*4+2] = '=';
		buf[i*4+3] = '=';
	}
	if (mod == 2)
	{
		buf[i*4+0] = base64_encode[  inData[n*3+0] / (BYTE)4];
		buf[i*4+1] = base64_encode[((inData[n*3+0] & (BYTE)3)    * (BYTE)16) | (inData[n*3+1] / (BYTE)(16*1))];
		buf[i*4+2] = base64_encode[((inData[n*3+1] & (BYTE)0x0f) * (BYTE)4)  | (inData[n*3+2] / (BYTE)(16*4))];
		buf[i*4+3] = '=';
	}
	*outSize = (INT32)strlen((CHAR*)buf);
	return buf;
}

BYTE* UtilBase64Decoding(BYTE* pInData, BYTE* pOutData, DWORD32 inSize, DWORD32* outSize)
{
    BYTE	data1, data2, data3, data4;
    DWORD32	dataofs, optr;

    for (dataofs=0, optr=0; dataofs < inSize - 3;)
    {
        do
        {
            data1 = *pInData++;
            dataofs ++;
        } while (!(ISALNUM(data1) || data1 == '+' || data1 == '/' || data1 == '='));

        do
        {
            data2 = *pInData++;
            dataofs ++;
        } while (!(ISALNUM(data2) || data2 == '+' || data2 == '/' || data2 == '='));

        do
        {
            data3 = *pInData++;
            dataofs ++;
        } while (!(ISALNUM(data3) || data3 == '+' || data3 == '/' || data3 == '='));

        do
        {
            data4 = *pInData++;
            dataofs ++;
        } while (!(ISALNUM(data4) || data4 == '+' || data4 == '/' || data4 == '='));


        if (data1 == '=')
			break;
		else
			pOutData[optr++] = (BYTE)((base64_decode[data1] << 2) + (base64_decode[data2] >> 4));

        if (data3 == '=')
			break;
		else
			pOutData[optr++] = (BYTE)((base64_decode[data2] << 4) + (base64_decode[data3] >> 2));

        if (data4 == '=')
			break;
		else
			pOutData[optr++] = (BYTE)((base64_decode[data3] << 6) + (base64_decode[data4]));

		if (ISCOMMA(*pInData))
			break;
    }
	*outSize = optr;

	return	pInData;
}

BYTE* UtilBase64ConvToBin(BYTE *pInData, DWORD32 dwInSize, DWORD32* pdwOutSize)
{
	BYTE*	pBuf;

	if (!pInData)
		return	NULL;

	pBuf = (BYTE*)MALLOC(dwInSize);
	if (!pBuf)
		return	NULL;

	UtilBase64Decoding(pInData, pBuf, dwInSize, pdwOutSize);

	return	pBuf;
}

DWORD32 UtilBase64ConvToBinWLen(BYTE *pInData, DWORD32 dwInSize, BYTE* pOutData, DWORD32 dwOutBuf)
{
	BYTE	*pPtr, *pTemp;
	DWORD32	dwOutSize;
	DWORD32	dwLen;
	WORD	wLen;

	pPtr = pOutData;

	while (dwInSize)
	{
		pTemp = UtilBase64Decoding(pInData, pPtr+2, dwInSize, &dwLen);
		dwInSize -= (UINT32)((ULONG)pTemp - (ULONG)pInData);
		pInData = pTemp;

		while (dwInSize && !ISCOMMA(*pInData) && !ISEQUAL(*pInData))
		{
			if (ISENDLINE(*pInData))
				break;
			pInData++;
			dwInSize--;
		}
		wLen = htons((WORD)dwLen);
		memcpy(pPtr, &wLen, 2);
		pPtr += (dwLen + 2);
	}

	dwOutSize = (DWORD32)(pPtr - pOutData);

	return	dwOutSize;
}
