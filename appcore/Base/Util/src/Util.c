
#include "Util.h"
#include "mediaerror.h"

static void reverse(char s[]) {
	UINT32	c,i,j;

	for ( i = 0, j = (UINT32)strlen(s)-1; i < j; i++, j--) {
		c = s[i];
		s[i] = s[j];
		s[j] = c;
	}
}

void ITOU(UINT32 n ,char s[] ) {
	INT32	i = 0;
	//INT32	a;

	do {
		s[i++] = (char)((n % 10) + '0');
		n /= 10;
	}
	while(i < 8);
	s[i] = '\0';
	reverse(s);
}

void ITOX(UINT32 n ,char s[] )
{
	INT32	i = 0;
	INT32	a;

	do {
		if ((a = (n % 16)) < 10) {
			s[i++] = a + '0';
		}
		else {
			s[i++] = a + '7';
		}
		n /= 16;

		if (n == 0)
			break;
	}
	while(i < 8);
	s[i] = '\0';
	reverse(s);
}

void ITOA(INT32 n ,char s[] )
{

	INT32	i ,sign;

	if ((sign = n) < 0) {
		n = -n;
	}
	i = 0;
	do {
		s[i++] = (n % 10) + '0';
	}
	while((n /= 10) > 0);
	if (sign < 0) {
		s[i++] = '-';
	}
	s[i] = '\0';
	reverse(s);
}

QWORD ATOQ(char* strString)
{
	char*	pStr;
	QWORD	qwNum = 0;

	pStr = strString;
	while (*pStr != '\0') {
		qwNum *= 10;
		qwNum += (*pStr - 48);
		pStr++;
	}

	return	qwNum;
}

DWORD32 ATOI(char* strString)
{
	char*	pStr;
	DWORD32		iNum = 0;

	pStr = strString;
	while (*pStr != '\0') {
		iNum *= 10;
		iNum += (*pStr - 48);
		pStr++;
	}

	return	iNum;
}

INT32 ATOI_MS(char* strString)
{
	char*	pStr;
	INT32		iNum = 0;
	char	strNum[4];

	pStr = strString;
	while (*pStr != '\0') {
		if (*pStr == '.')
			break;
		iNum *= 10;
		iNum += (*pStr - 48);
		pStr++;
	}
	iNum *= 1000;
	if (*pStr == '.') {
		pStr++;
		if (*pStr != '\0') {
			strNum[0] = *pStr;
			strNum[1] = '0';
			strNum[2] = '0';
			strNum[3] = '\0';
			pStr++;
			if (*pStr != '\0') {
				strNum[1] = *pStr;
				pStr++;
				if (*pStr != '\0') {
					strNum[2] = *pStr;
				}
			}
			iNum += ATOI(strNum);
		}
	}

	return	iNum;
}

INT32 ATOI_TS(char* strString)
{
	char*	pStr;
	INT32		iNum = 0;
	char	strNum[3];

	pStr = strString;
	strNum[0] = *pStr++;
	strNum[1] = *pStr++;
	strNum[2] = 0;
	iNum += ATOI(strNum)*3600000;

	pStr++;
	strNum[0] = *pStr++;
	strNum[1] = *pStr++;
	strNum[2] = 0;
	iNum += ATOI(strNum)*60000;

	pStr++;
	iNum += ATOI_MS(pStr);

	return	iNum;
}

INT32 ATOI_HEX(char* strString)
{
	char*	pStr;
	INT32		iNum = 0;

	pStr = strString;
	while (*pStr != '\0') {
		iNum *= 16;
		switch (*pStr) {
		case 'A':
		case 'a':
			iNum += 10;
			break;
		case 'B':
		case 'b':
			iNum += 11;
			break;
		case 'C':
		case 'c':
			iNum += 12;
			break;
		case 'D':
		case 'd':
			iNum += 13;
			break;
		case 'E':
		case 'e':
			iNum += 14;
			break;
		case 'F':
		case 'f':
			iNum += 15;
			break;
		default :
			iNum += (*pStr - 48);
			break;
		}
		pStr++;
	}

	return	iNum;
}

double ATOF(char *strString)
{
	double dbNum;

	if(strString == NULL)
		return (double)0;

	dbNum = atof(strString);

	return	dbNum;
}

INT32 WTOI(WCHAR* strString)
{
#ifdef	WIN32
	return	_wtoi(strString);
#else
	WCHAR*	pStr;
	INT32		iNum = 0;

	pStr = strString;
	while (*pStr != 0) {
		iNum *= 10;
		iNum += (*pStr - 48);
		pStr++;
	}

	return	iNum;
#endif
}

#ifdef SUPPORT_IOS
INT32 ABS2(INT32 a)
#else
INT32 ABS(INT32 a)
#endif
{
	if (a >= 0)
		return	a;
	else
		return	-a;
}

INT64 ABS64(INT64 a)
{
	if (a >= 0)
		return	a;
	else
		return	-a;
}

void McAddTime(DWORD32* pdwDate, DWORD32* pdwTime, DWORD32 dwTime)
{
	DWORD32	dwYear, dwMonth, dwDay, dwHour, dwMin, dwSec, dwMs;
	DWORD32	dwTemp;

	dwTemp = *pdwTime;
	dwMs = dwTemp % 1000;		dwTemp /= 1000;
	dwSec = dwTemp % 100;		dwTemp /= 100;
	dwMin = dwTemp % 100;		dwTemp /= 100;
	dwHour = dwTemp % 100;

	dwTime += (dwHour * 3600000 + dwMin * 60000 + dwSec * 1000 + dwMs);

	dwHour = dwTime / 3600000;
	dwMin = (dwTime - dwHour*3600000) / 60000;
	dwSec = (dwTime - dwHour*3600000 - dwMin*60000) / 1000;
	dwMs  = dwTime % 1000;

	if (dwHour >= 24)
	{
		dwDay = dwHour / 24;
		dwHour -= (dwDay * 24);
	}
	else
		dwDay = 0;
	*pdwTime = dwHour*10000000 + dwMin*100000 + dwSec*1000 + dwMs;

	if (dwDay)
	{
		dwYear  = *pdwDate / 10000;
		dwMonth = (*pdwDate - dwYear*10000) / 100;
		dwDay  += (*pdwDate - dwYear*10000 - dwMonth*100);

		while (1)
		{
			if (dwMonth == 1 || dwMonth == 3 || dwMonth == 5 || dwMonth == 7 || dwMonth == 8 || dwMonth == 10 || dwMonth == 12)
			{
				if (dwDay <= 31)
					break;
				else
				{
					dwMonth++;
					dwDay -= 31;
				}
			}
			else if (dwMonth == 4 || dwMonth == 6 || dwMonth == 9 || dwMonth == 11)
			{
				if (dwDay <= 30)
					break;
				else
				{
					dwMonth++;
					dwDay -= 30;
				}
			}
			else if (dwMonth == 2)
			{
				if (!dwYear % 4)
				{
					if (dwDay <= 29)
						break;
					else
					{
						dwMonth++;
						dwDay -= 29;
					}
				}
				else
				{
					if (dwDay <= 28)
						break;
					else
					{
						dwMonth++;
						dwDay -= 28;
					}
				}
			}
			else
			{
				dwYear++;
				dwMonth = 1;
			}
		}
		*pdwDate = dwYear * 1000 + dwMonth * 10 + dwDay;
	}
}

DWORD32 McGetRangeFromTime(DWORD32 dwStartDate, DWORD32 dwStartTime, DWORD32 dwEndDate, DWORD32 dwEndTime)
{
	DWORD32	dwSYear, dwSMon, dwSDay, dwSHour, dwSMin, dwSSec, dwSMs;
	DWORD32	dwEYear, dwEMon, dwEDay, dwEHour, dwEMin, dwESec, dwEMs;
	DWORD32	dwRange;

	dwSDay = dwStartDate % 100;		dwStartDate /= 100;
	dwSMon = dwStartDate % 100;		dwStartDate /= 100;
	dwSYear = dwStartDate % 10000;

	dwSMs = dwStartTime % 1000;		dwStartTime /= 1000;
	dwSSec = dwStartTime % 100;		dwStartTime /= 100;
	dwSMin = dwStartTime % 100;		dwStartTime /= 100;
	dwSHour = dwStartTime % 100;

	dwEDay = dwEndDate % 100;		dwEndDate /= 100;
	dwEMon = dwEndDate % 100;		dwEndDate /= 100;
	dwEYear = dwEndDate % 10000;

	dwEMs = dwEndTime % 1000;		dwEndTime /= 1000;
	dwESec = dwEndTime % 100;		dwEndTime /= 100;
	dwEMin = dwEndTime % 100;		dwEndTime /= 100;
	dwEHour = dwEndTime % 100;

	if (dwSMs > dwEMs)		{	dwESec--;	dwEMs += 1000;	}	dwEMs -= dwSMs;
	if (dwSSec > dwESec)	{	dwEMin--;	dwESec += 60;	}	dwESec -= dwSSec;
	if (dwSMin > dwEMin)	{	dwEHour--;	dwEMin += 60;	}	dwEMin -= dwSMin;
	if (dwSHour > dwEHour)	{	dwEDay--;	dwEHour += 24;	}	dwEHour -= dwSHour;

	dwRange = 0;
	while (dwEYear != dwSYear || dwSMon != dwEMon)
	{
		if (dwSMon == 1 || dwSMon == 3 || dwSMon == 5 || dwSMon == 7 || dwSMon == 8 || dwSMon == 10 || dwSMon == 12)
			dwRange += (31 - dwSDay);
		else if (dwSMon == 4 || dwSMon == 6 || dwSMon == 9 || dwSMon == 11)
			dwRange += (30 - dwSDay);
		else if (dwSMon == 2)
		{
			if (!dwSYear % 4)
				dwRange += (29 - dwSDay);
			else
				dwRange += (28 - dwSDay);
		}
		dwSDay = 0;
		dwSMon++;
		if (dwSMon > 12)
		{
			dwSMon = 1;
			dwSYear++;
		}
	}
	dwRange = dwRange * 86400000 + dwEHour * 3600000 + dwEMin * 60000 + dwESec * 1000 + dwEMs;

	return	dwRange;
}

char* MwGetErrorMessageKor(LRSLT lError)
{
	switch (lError)
	{
	case COMMON_ERR_MEM				:		return	"�޸𸮰� �����մϴ�.";
	case COMMON_ERR_THREAD			:		return	"�����带 ������ �� �����ϴ�.";
	case COMMON_ERR_SYSTEM			:		return	"�ý��� �ʱ�ȭ�� �����߽��ϴ�.";
	case COMMON_ERR_NETWORK			:		return	"��Ʈ��ũ �ý����� Fail�Դϴ�.";
	case COMMON_ERR_FOPEN			:		return	"������ ã�� ���� �����ϴ�.";
	case COMMON_ERR_FCREATE			:		return	"������ ���� �� ���� �����ϴ�.";
	case COMMON_ERR_URL				:		return	"�������� �ʴ� URL �����Դϴ�.";
	case COMMON_ERR_ENCRYPTION		:		return	"��ȣȭ�� �������Դϴ�.";
	case COMMON_ERR_EXPIRED_DATA	:		return	"��ȿ�Ⱓ�� ����Ǿ����ϴ�.";
	case COMMON_ERR_FUNCTION		:		return	"�������� �ʴ� ����Դϴ�.";
	case COMMON_ERR_INVALID_RANGE	:		return	"���� �׼����� �����߽��ϴ�.";
	case COMMON_ERR_FILETYPE		:		return	"�������� �ʴ� ���� �����Դϴ�.";
	case COMMON_ERR_STREAMSYNTAX	:		return	"��Ʈ�� ������ �߻��Ͽ����ϴ�.";
	case COMMON_ERR_MEDIAPARAM		:		return	"�߸��� �̵�� �Ķ���� �����Դϴ�.";
	case COMMON_ERR_NOMEDIA			:		return  "�̵�� ������ Ȯ���Ͻʽÿ�.";
	case COMMON_ERR_AUDIOCODEC		:		return	"�������� �ʴ� ����� �����Դϴ�.";
	case COMMON_ERR_VIDEOCODEC		:		return	"�������� �ʴ� ���� �����Դϴ�.";
	case COMMON_ERR_TEXTCODEC		:		return	"�������� �ʴ� �ؽ�Ʈ �����Դϴ�.";
	case COMMON_ERR_AUDIODEC		:		return	"����� ���ڴ��� �ʱ�ȭ�ϱ� ���߽��ϴ�.";
	case COMMON_ERR_VIDEODEC		:		return	"���� ���ڴ��� �ʱ�ȭ�ϱ� ���߽��ϴ�.";
	case COMMON_ERR_TEXTDEC			:		return	"�ؽ�Ʈ ���ڴ��� �ʱ�ȭ�ϱ� ���߽��ϴ�.";
	case COMMON_ERR_AUDIOENC		:		return	"����� ���ڴ��� �ʱ�ȭ�ϱ� ���߽��ϴ�.";
	case COMMON_ERR_VIDEOENC		:		return	"���� ���ڴ��� �ʱ�ȭ�ϱ� ���߽��ϴ�.";
	case COMMON_ERR_TEXTENC			:		return	"�ؽ�Ʈ ���ڴ��� �ʱ�ȭ�ϱ� ���߽��ϴ�.";
	case COMMON_ERR_AUDIODEVIN		:		return	"����� �Է���ġ�� �ʱ�ȭ�ϱ� ���߽��ϴ�.";
	case COMMON_ERR_VIDEODEVIN		:		return	"���� �Է���ġ�� �ʱ�ȭ�ϱ� ���߽��ϴ�.";
	case COMMON_ERR_AUDIODEVOUT		:		return	"����� �����ġ�� �ʱ�ȭ�ϱ� ���߽��ϴ�.";
	case COMMON_ERR_VIDEODEVOUT		:		return	"���� �����ġ�� �ʱ�ȭ�ϱ� ���߽��ϴ�.";
	case COMMON_ERR_NOTCAPABLE		:		return	"�ִ� ���� ó�� ũ�⸦ �ʰ��߽��ϴ�.";
	case COMMON_ERR_SOCKET			:		return	"���� ������ �߻��߽��ϴ�.";
	case COMMON_ERR_INVALID_PROTOCOL:		return	"�������� Syntax ������ �߻��Ͽ����ϴ�.";
	case COMMON_ERR_SEND			:		return	"Send ����";
	case COMMON_ERR_RECV			:		return	"Recv ����";
	case COMMON_ERR_TIMEOUT			:		return	"�ð��� �ʰ��Ͽ����ϴ�.";
	case COMMON_ERR_SERVER			:		return	"�������� ������ �߻��߽��ϴ�.";
	case COMMON_ERR_SVR_CONNECT		:		return	"������ ������ ���� �����ϴ�.";
	case COMMON_ERR_SVR_DISCONNECT	:		return	"�������� ������ ���������ϴ�.";
	case COMMON_ERR_SVR_NORESPONSE	:		return	"�����κ��� ������ �����ϴ�.";
	case COMMON_ERR_SVR_NODATA		:		return	"�����κ��� ���۵Ǵ� �����Ͱ� �����ϴ�.";
	case COMMON_ERR_CLIENT			:		return	"�߸��� ��û�� �õ��Ͽ����ϴ�.";
	case COMMON_ERR_CLI_ACCEPT		:		return	"�ܸ��� ���� ��û�� �źεǾ����ϴ�.";
	case COMMON_ERR_CLI_DISCONNECT	:		return	"�ܸ����� ������ ���������ϴ�.";
	case COMMON_ERR_CLI_NOREQUEST	:		return	"�ܸ��κ��� ��û�� �����ϴ�.";
	case COMMON_ERR_NOTMATCH_SCID	:		return	"SID CID�� ��ġ���� �ʴ´�.";
	default							:		return	"�˼����� ������ �߻��߽��ϴ�.";
	}

	return	NULL;
}

char* MwGetErrorMessageEng(LRSLT lError)
{
	switch (lError)
	{
	case COMMON_ERR_MEM				:		return	"Memory allocation failed.";
	case COMMON_ERR_THREAD			:		return	"Thread creation failed";
	case COMMON_ERR_SYSTEM			:		return	"System init failed";
	case COMMON_ERR_NETWORK			:		return	"Network system failed";
	case COMMON_ERR_FOPEN			:		return	"File not found";
	case COMMON_ERR_FCREATE			:		return	"File creation failed";
	case COMMON_ERR_URL				:		return	"Unsupported URL";
	case COMMON_ERR_ENCRYPTION		:		return	"Encryption file";
	case COMMON_ERR_EXPIRED_DATA	:		return	"Expired file";
	case COMMON_ERR_FUNCTION		:		return	"Unsupported function";
	case COMMON_ERR_INVALID_RANGE	:		return	"Random access failed";
	case COMMON_ERR_FILETYPE		:		return	"Unsupported file";
	case COMMON_ERR_STREAMSYNTAX	:		return	"Stream syntax error";
	case COMMON_ERR_MEDIAPARAM		:		return	"Media parameter error";
	case COMMON_ERR_AUDIOCODEC		:		return	"Unsupported audio codec";
	case COMMON_ERR_VIDEOCODEC		:		return	"Unsupported video codec";
	case COMMON_ERR_TEXTCODEC		:		return	"Unsupported text codec";
	case COMMON_ERR_AUDIODEC		:		return	"Audio decoder init failed";
	case COMMON_ERR_VIDEODEC		:		return	"Video decoder init failed";
	case COMMON_ERR_TEXTDEC			:		return	"Text decoder init failed";
	case COMMON_ERR_AUDIOENC		:		return	"Audio encoder init failed";
	case COMMON_ERR_VIDEOENC		:		return	"Video encoder init failed";
	case COMMON_ERR_TEXTENC			:		return	"Text encoder init failed";
	case COMMON_ERR_AUDIODEVIN		:		return	"Audio input device init failed";
	case COMMON_ERR_VIDEODEVIN		:		return	"Video input device init failed";
	case COMMON_ERR_AUDIODEVOUT		:		return	"Audio output device init failed";
	case COMMON_ERR_VIDEODEVOUT		:		return	"Video output device init failed";
	case COMMON_ERR_SOCKET			:		return	"Socket creation failed";
	case COMMON_ERR_INVALID_PROTOCOL:		return	"Protocol syntax error";
	case COMMON_ERR_SEND			:		return	"Send error";
	case COMMON_ERR_RECV			:		return	"Recv error";
	case COMMON_ERR_TIMEOUT			:		return	"Time out is occurerd";
	case COMMON_ERR_SERVER			:		return	"Internal server error";
	case COMMON_ERR_SVR_CONNECT		:		return	"No connection to server";
	case COMMON_ERR_SVR_DISCONNECT	:		return	"Disconnected from sever";
	case COMMON_ERR_SVR_NORESPONSE	:		return	"No response from server";
	case COMMON_ERR_SVR_NODATA		:		return	"No data is received from server";
	case COMMON_ERR_CLIENT			:		return	"Invalid request";
	case COMMON_ERR_CLI_ACCEPT		:		return	"Acception from client is denied";
	case COMMON_ERR_CLI_DISCONNECT	:		return	"Disconnected from client";
	case COMMON_ERR_CLI_NOREQUEST	:		return	"No request from client";
	case COMMON_ERR_NOTMATCH_SCID	:		return	"SID CID is not matched";
	default							:		return	"Unknown error";
	}

	return	NULL;
}

// calculate psnr y,u,v value and return only y in now, mwkang 2008.03.27
double MwCalcPsnr(BYTE* psrc, BYTE* pdst, INT32 width, INT32 height)
{
	INT32 i,j;
	INT32 nyN = width*height;
	//INT32 nuvN = width*height/4;
	
	double noise=0.0, psnrY;

	//pdst_u = pdst + nyN;
	//pdst_v = pdst_u + nuvN;
	//psrc_u = psrc + nyN;
	//psrc_v = psrc_u + nuvN;

	// y
	noise = 0.0;
	for(j=0; j<height; j++)	{
		for(i=0; i<width; i++) {
			noise = noise + pow(pdst[i + j*width] - psrc[i + j*width], 2);
		}
	}

	noise = noise / nyN;
	psnrY = 10 * log10( (255.0 * 255.0) / noise );

	/*
	// u
	noise = 0.0;
	for(j=0; j<height/2; j++)
	{
		for(i=0; i<width/2; i++)
		{
			noise = noise + pow(pdst_u[i + j*width/2] - psrc_u[i + j*width/2], 2);
		}
	}

	noise = noise / nuvN;
	psnrU = 10 * log10( (255.0 * 255.0) / noise );

	// v
	noise = 0.0;
	for(j=0; j<height/2; j++)
	{
		for(i=0; i<width/2; i++)
		{
			noise = noise + pow(pdst_v[i + j*width/2] - psrc_v[i + j*width/2], 2);
		}
	}

	noise = noise / nuvN;
	psnrV = 10 * log10( (255.0 * 255.0) / noise );
	*/

	return psnrY;
}
