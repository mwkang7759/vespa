#include "DevOutVideoBase.h"
#include "DeviceOutAPI.h"

#if defined(VIDEOOUT_GDI) || defined(VIDEOOUT_D3D9DRAW)

#ifdef	_WIN32_WCE
//#if 0
HFONT CreateTextFont(int nTotalHeight, BOOL m_bFullscreen)
{
	LOGFONT lFont;

	if(m_bFullscreen)
	{
		lFont.lfHeight=-14; 
		lFont.lfEscapement=2700; 
	}
	else
	{
		lFont.lfHeight=-14; 
		lFont.lfEscapement=0; 
	}

	lFont.lfWidth=0; 
	
	lFont.lfWeight=FW_LIGHT; 
	lFont.lfItalic=FALSE; 
	lFont.lfUnderline=FALSE; 
	lFont.lfStrikeOut=FALSE; 
	lFont.lfCharSet=ANSI_CHARSET; 
	lFont.lfOutPrecision=OUT_DEFAULT_PRECIS; 
	lFont.lfClipPrecision=CLIP_DEFAULT_PRECIS; 
	lFont.lfQuality=DEFAULT_QUALITY; 
	lFont.lfPitchAndFamily= DEFAULT_PITCH| FF_DONTCARE;
#ifdef _WIN32_WCE
	wcscpy(lFont.lfFaceName,_T("����ü")); 
#endif

	return CreateFontIndirect(&lFont);
}
#else

#endif
void GetDisplayRectangle(McVideoOutStructHandle hDevice, RECT* rcDisplay)
{
	DWORD	dwWidth;
	DWORD	dwHeight;
	int		iTempX, iTempY;
	int		iRatioW = 1;
	int		iRatioH = 1;

	if(hDevice->m_nDisplayType == VIEW_TYPE_RATIO_FULL)
	{
		SetRect(rcDisplay, 0, 0, hDevice->m_dwWndWidth, hDevice->m_dwWndHeight);
	}
	else if(hDevice->m_nDisplayType == VIEW_TYPE_ORG_SIZE)
	{
		if(hDevice->m_dwWndWidth >hDevice->m_dwWidth)
		{
			if(hDevice->m_dwWndHeight>hDevice->m_dwHeight)
				SetRect(rcDisplay,(hDevice->m_dwWndWidth-hDevice->m_dwWidth)/2,
							(hDevice->m_dwWndHeight-hDevice->m_dwHeight)/2,
							(hDevice->m_dwWndWidth-hDevice->m_dwWidth)/2+hDevice->m_dwWidth,
							(hDevice->m_dwWndHeight-hDevice->m_dwHeight)/2+hDevice->m_dwHeight);
			else
				SetRect(rcDisplay,(hDevice->m_dwWndWidth-hDevice->m_dwWidth)/2, 0,
							(hDevice->m_dwWndWidth-hDevice->m_dwWidth)/2+hDevice->m_dwWidth,
							hDevice->m_dwWndHeight);
		}
		else
		{
			if(hDevice->m_dwWndHeight>hDevice->m_dwHeight)
				SetRect(rcDisplay, 0, (hDevice->m_dwWndHeight-hDevice->m_dwHeight)/2, hDevice->m_dwWndWidth,
							(hDevice->m_dwWndHeight-hDevice->m_dwHeight)/2+hDevice->m_dwHeight);
			else
				SetRect(rcDisplay, 0, 0, hDevice->m_dwWndWidth, hDevice->m_dwWndHeight);
		}
	}
	else
	{
		if(hDevice->m_nDisplayType==VIEW_TYPE_RATIO_SUPPORT)
		{
			iRatioW = hDevice->m_dwWidth;
			iRatioH = hDevice->m_dwHeight;
		}
		else if(hDevice->m_nDisplayType==VIEW_TYPE_RATIO_4_3)
		{
			iRatioW = 4;
			iRatioH = 3;
		}
		else if(hDevice->m_nDisplayType==VIEW_TYPE_RATIO_16_9)
		{
			iRatioW = 16;
			iRatioH = 9;
		}
		else if(hDevice->m_nDisplayType==VIEW_TYPE_RATIO_185_1)
		{
			iRatioW = 185;
			iRatioH = 100;
		}
		else if(hDevice->m_nDisplayType==VIEW_TYPE_RATIO_235_1)
		{
			iRatioW = 235;
			iRatioH = 100;
		}
		else if(hDevice->m_nDisplayType==VIEW_TYPE_RATIO_USER_DEFINE)
		{
			//iRatioW = 235;
			//iRatioH = 100;
			iRatioW = hDevice->m_dwUserDefineRatioX;
			iRatioH = hDevice->m_dwUserDefineRatioY;
		}

		iTempX = hDevice->m_dwWndWidth * iRatioH;
		iTempY = hDevice->m_dwWndHeight * iRatioW;

		if (iTempX > iTempY)
		{
			dwWidth=hDevice->m_dwWndHeight*iRatioW/iRatioH;
			dwHeight=hDevice->m_dwWndHeight;
			SetRect(rcDisplay, (hDevice->m_dwWndWidth-dwWidth)/2, 0, (hDevice->m_dwWndWidth-dwWidth)/2+dwWidth,dwHeight);
		}
		else if (iTempX < iTempY)
		{
			dwWidth=hDevice->m_dwWndWidth;
			dwHeight=hDevice->m_dwWndWidth*iRatioH/iRatioW;
			SetRect(rcDisplay, 0, (hDevice->m_dwWndHeight-dwHeight)/2, dwWidth,(hDevice->m_dwWndHeight-dwHeight)/2+dwHeight);
		}
		else
		{
			dwWidth=hDevice->m_dwWndWidth;
			dwHeight=hDevice->m_dwWndHeight;
			SetRect(rcDisplay, 0, 0, dwWidth, dwHeight);		
		}
	}
}
//#endif

LRSLT McVideoOutOpen(McVideoOutHandle* phDevice, FrVideoInfo* hInfo, HANDLE hWnd, BOOL bFullScreen)
{
	McVideoOutStructHandle	hDevice;

	TRACE("[DevOutVideo]	McVideoOutOpen width=%d height=%d Wnd=%x", hInfo->dwWidth, hInfo->dwHeight, hWnd);

	/*if (hInfo->m_dwOutWidth == 0 || hInfo->m_dwOutHeight == 0)
	{
		TRACE("[DevOutVideo]	McVideoOutOpen - Input source size is null");
		return MC_FAIL;
	}*/

	hDevice = (McVideoOutStructHandle)MALLOC(sizeof(McVideoOutStruct));
	if (hDevice)
	{
		memset(hDevice, 0, sizeof(McVideoOutStruct));

		// Video
		hDevice->m_hWnd		= (HWND)hWnd;
		hDevice->m_dwFourCC = hInfo->dwFourCC;
		hDevice->m_dwBitCount = hInfo->dwBitCount;
		hDevice->m_dwWidth = hInfo->dwWidth;
		hDevice->m_dwHeight = hInfo->dwHeight;

		// add param
		hDevice->m_nDevBitCount = hInfo->dwBitCount;;
		hDevice->m_nDisplayType = VIEW_TYPE_RATIO_SUPPORT;
		/*if (hInfo->eFlip == MIRROR_ROTATE_90 || hInfo->eFlip == MIRROR_ROTATE_270)
		{
			hDevice->m_dwWidth	= hInfo->dwOutHeight;
			hDevice->m_dwHeight	= hInfo->dwOutWidth;
		}
		else
		{
			hDevice->m_dwWidth	= hInfo->dwOutWidth;
			hDevice->m_dwHeight	= hInfo->dwOutHeight;
		}*/
		SetRect(&hDevice->m_rcSrc, 0, 0, hDevice->m_dwWidth, hDevice->m_dwHeight);
#if defined(VIDEOOUT_DDRAW) || defined(VIDEOOUT_D3D9DRAW)
		hDevice->m_bDDraw = TRUE;
		if(!DirectDrawOpen(hDevice))
		{
#endif
			if(!GDIOpen(hDevice))
			{
				McVideoOutClose(hDevice);
				return	COMMON_ERR_VIDEODEVOUT;
			}
			hDevice->m_bDDraw = FALSE;
#if defined(VIDEOOUT_DDRAW) || defined(VIDEOOUT_D3D9DRAW)
		}
#endif

		hDevice->m_bDrawBG = FALSE;
		*phDevice = (McVideoOutHandle)hDevice;

#ifdef	VIDEO_DUMP
		hDevice->m_hFile = McOpenFile("video.raw", FILE_CREATE|FILE_WRITE);
#endif

		hDevice->m_n64FreqTime = FrGetQueryFreq();
	}
	else
		return	COMMON_ERR_MEM;

	TRACE("[DevOutVideo]	McVideoOutOpen end");

	return	FR_OK;
}

void McVideoOutClose(McVideoOutHandle hHandle)
{
	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;	

	TRACE("[DevOutVideo]	McVideoOutClose");
	if (hDevice)
	{
		FrSleep(50);			// wait for rendering


#if defined(VIDEOOUT_DDRAW) || defined(VIDEOOUT_D3D9DRAW)
	if (hDevice->m_bDDraw)
		DirectDrawClose(hDevice);
	else
#endif
		GDIClose(hDevice);

#ifdef	VIDEO_DUMP
		McCloseFile(hDevice->m_hFile);
#endif

		FREE(hDevice);
	}
}

BOOL McVideoOutSetWndHandle(McVideoOutHandle hHandle, HANDLE hWnd)
{
	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;	

	if (!hDevice)
		return	FALSE;

	if (hDevice->m_hWnd && hDevice->m_hWnd == hWnd)
		return TRUE;

	hDevice->m_hWnd = (HWND)hWnd;
	if(hDevice->m_hWnd == NULL)
		return	FALSE;


#if defined(VIDEOOUT_DDRAW) || defined(VIDEOOUT_D3D9DRAW)
	if (hDevice->m_bDDraw)
		DirectDrawSetWndHandle(hDevice);
#endif

	return	TRUE;
}

BOOL McVideoOutSetTextWndHandle(McVideoOutHandle hHandle, HANDLE hTextWnd)
{
	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;

	if (!hDevice)
		return	FALSE;

	if (hDevice->m_hTextWnd && hDevice->m_hTextWnd == hTextWnd)
		return TRUE;

	hDevice->m_hTextWnd = (HWND)hTextWnd;
	if (hDevice->m_hTextWnd == NULL)
		return	FALSE;

	return	TRUE;
}

BOOL McVideoOutConversion(McVideoOutHandle hHandle, FrRawVideo* pMedia)
{
	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;	

	//if (!hDevice || pMedia->m_pY == NULL)
	//if (!hDevice || pMedia == NULL || (pMedia->pY == NULL && pMedia->pDevPtr == NULL))
	if (!hDevice || pMedia == NULL || (pMedia->pY == NULL))
		return	FALSE;

	if (hDevice->m_hWnd == NULL)
		return	TRUE;

#ifdef VIDEO_DUMP
	McWriteFile(hDevice->m_hFile, (char*)pMedia->m_pY, hDevice->m_dwWidth*hDevice->m_dwHeight);
	McWriteFile(hDevice->m_hFile, (char*)pMedia->m_pU, hDevice->m_dwWidth/2*hDevice->m_dwHeight/2);
	McWriteFile(hDevice->m_hFile, (char*)pMedia->m_pV, hDevice->m_dwWidth/2*hDevice->m_dwHeight/2);
#endif	

#if defined(VIDEOOUT_DDRAW) || defined(VIDEOOUT_D3D9DRAW)
	if (hDevice->m_bDDraw)
		DirectDrawTransToBuffer(hDevice, pMedia);
	else
#endif
		GDITransToBuffer(hDevice, pMedia);
		

	return	TRUE;
}

BOOL McVideoOutClear(McVideoOutHandle hHandle)
{
	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;	

	if (!hDevice)
		return	FALSE;


#if defined(VIDEOOUT_DDRAW) || defined(VIDEOOUT_D3D9DRAW)
	if (hDevice->m_bDDraw)
		DirectDrawClear(hDevice);
	else
#endif
		GDIClear(hDevice);

	return	TRUE;
}

BOOL McVideoOutPlayWaitForVSync(McVideoOutHandle hHandle)
{
	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;

	if (!hDevice)
		return	FALSE;

#if defined(VIDEOOUT_DDRAW) || defined(VIDEOOUT_D3D9DRAW)
	if (hDevice->m_bDDraw)
		return DirectDrawPlayWaitForVSync(hDevice);
	else
#endif
		return FALSE;
}

BOOL McVideoOutPlay(McVideoOutHandle hHandle)
{
	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;	

	if (!hDevice)
		return	FALSE;

#if defined(VIDEOOUT_DDRAW) || defined(VIDEOOUT_D3D9DRAW)
	if (hDevice->m_bDDraw)
		DirectDrawPlay(hDevice);
	else
#endif
		GDIPlay(hDevice);
		
	return	TRUE;
}


//BOOL McVideoOutPutString(McVideoOutHandle hHandle, FrMediaText* strString)
//{
//	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;	
//
//	if (!hDevice)
//		return	FALSE;
//
//	if (strString)
//		memcpy(&hDevice->m_TextBlock, strString, sizeof(McMediaText));
//	else
//		memset(&hDevice->m_TextBlock, 0, sizeof(McMediaText));
//
//	hDevice->m_bText = TRUE;
//
//	return	TRUE;
//}

BOOL McVideoOutPlayString(McVideoOutHandle hHandle)
{
	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;	

#if defined(VIDEOOUT_DDRAW) || defined(VIDEOOUT_D3D9DRAW)
	if (hDevice->m_bDDraw)
	{
		hDevice->m_bText = TRUE;
		DirectDrawPlayString(hDevice);
	}
	else
#endif
		GDIPlayString(hDevice);
		
	return	TRUE;
}

BOOL McVideoOutSetVSync(McVideoOutHandle hHandle, BOOL bVSync)
{
	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;	

	if (!hDevice)
		return	FALSE;

	hDevice->m_bVSyncOff = bVSync?FALSE:TRUE;

	return TRUE;
}

BOOL McVideoOutSetDisplayType(McVideoOutHandle hHandle, DWORD dwType, DWORD dwRatioX, DWORD dwRatioY)
{
	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;	

	if (!hDevice)
		return	FALSE;

	hDevice->m_nDisplayType = dwType;
	hDevice->m_bDrawBG = TRUE;
	hDevice->m_dwUserDefineRatioX=dwRatioX;
	hDevice->m_dwUserDefineRatioY=dwRatioY;
	
	return TRUE;
}

BOOL McVideoOutSetTextDisplay(McVideoOutHandle hHandle,BOOL bTextShowHide, DWORD dwType,DWORD dwUpPos,DWORD dwDownPos,DWORD dwLeftPos,DWORD dwRightPos)
{
	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;	

	if (!hDevice)
		return	FALSE;
	hDevice->m_bTextShowHide=bTextShowHide;

	hDevice->m_dwTextDisplayType = dwType;
	hDevice->m_dwUpMovePos = dwUpPos;
	hDevice->m_dwDownMovePos = dwDownPos;
	hDevice->m_dwLeftMovePos = dwLeftPos;
	hDevice->m_dwRightMovePos = dwRightPos;

	return TRUE;
}

BOOL McVideoOutSetTextFont(McVideoOutHandle hHandle, DWORD dwSize,BOOL bBold, BOOL bUserDefineColor , DWORD dwUserDefineColor , char *szFontName)
{
	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;	

	if (!hDevice)
		return	FALSE;

	hDevice->m_dwTextSize = dwSize;
	hDevice->m_bBold = bBold;
	hDevice->m_bUserDefineColor = bUserDefineColor;
	hDevice->m_dwUserDefineColor = dwUserDefineColor;

	strcpy(hDevice->m_strFont,szFontName);

	return TRUE;
}


//BOOL McVideoOutSetStatusString(McVideoOutHandle hHandle, char *szStatusString , int nColor,int nFontSize)
//{
//	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;	
//
//	if (!hDevice)
//		return	FALSE;
//
//	if(szStatusString==NULL || strlen(szStatusString)==0)
//		memset(hDevice->m_szStatus,0x00,sizeof(hDevice->m_szStatus));
//	else
//	{
//		strcpy(hDevice->m_szStatus,szStatusString);
//	}
//	hDevice->m_nStatusFontColor=nColor;
//	hDevice->m_nStatusFontSize=nFontSize;
//
//	hDevice->m_bText = TRUE;
//
//	return TRUE;
//
//}

BOOL McVideoOutSetPerfInfoString(McVideoOutHandle hHandle, char *szPerfInfoString, int nColor, int nFontSize)
{
	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;

	if (!hDevice)
		return	FALSE;

	if (szPerfInfoString == NULL || strlen(szPerfInfoString) == 0)
		memset(hDevice->m_szPerfInfo, 0x00, sizeof(hDevice->m_szPerfInfo));
	else
	{
		strcpy(hDevice->m_szPerfInfo, szPerfInfoString);
	}
	hDevice->m_nPerfInfoFontColor = nColor;
	hDevice->m_nPerfInfoFontSize = nFontSize;

	hDevice->m_bText = TRUE;

	return TRUE;
}


void McVideoOutUpdateResolution(McVideoOutHandle hHandle, DWORD dwWidth)
{
	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;	

	if (!hDevice)
		return;

	hDevice->m_dwWidth = dwWidth;
	switch(dwWidth)
	{
	case 352:
		hDevice->m_dwHeight = 288;
		break;
	case 176:
		hDevice->m_dwHeight = 144;
		break;
	case 704:
		hDevice->m_dwHeight = 576;
		break;
	case 320:
		hDevice->m_dwHeight = 240;
		break;
	case 640:
		hDevice->m_dwHeight = 480;
		break;
	}

	TRACE("[VideoDev] width = %d, height = %d", hDevice->m_dwWidth, hDevice->m_dwHeight);
}

BOOL McVideoOutReset(McVideoOutHandle hHandle)
{
	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;	

	TRACE("[DevOutVideo] McVideoOutReset - start");
	TRACE("[DevOutVideo] McVideoOutReset - end");

	return	TRUE;
}


#endif	// VIDEOOUT_DDRAW || VIDEOOUT_D3D9DRAW