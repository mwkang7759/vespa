/*****************************************************************************
*                                                                            *
*                            DeviceOutput Library							 *
*                                                                            *
*   Copyright (c) 2018 - by DreamToBe, Incoporated. All Rights Reserved.      *
******************************************************************************

    File Name       : DevOutVideoSDL.h
    Author(s)       : CHANG, Joonho
    Created         : 9 May 2018

    Description     : DeviceOutput API Header for SDL
    Notes           :

==============================================================================
                      Modification History (Reverse Order)
==============================================================================
    Date        Author      Location (variables) / Description
------------------------------------------------------------------------------

------------------------------------------------------------------------------*/

#ifndef	_DEVICEVIDEOOUT_SDL_H_
#define	_DEVICEVIDEOOUT_SDL_H_

#include "DevOutVideoBase.h"

#ifdef	VIDEOOUT_SDL

#define	MAX_VIDEO_QUEUE_ALLOWED		10

#include "SDL.h" 

typedef enum {
	SDL_VIDEO_OUT_I420 = 0,
	SDL_VIDEO_OUT_BGR,
	SDL_VIDEO_OUT_NV12,
	SDL_VIDEO_OUT_RGB,
	SDL_VIDEO_OUT_NUM,
} eSDL_VIDEO_OUT_FORMAT;

//typedef struct GLES2_Context
//{
//#define SDL_PROC(ret,func,params) ret (APIENTRY *func) params;
//#include "SDL_gles2funcs.h"
//#undef SDL_PROC
//} GLES2_Context;

// SDL Handle
typedef struct {
   // SDL init flags
    char **argv;
    Uint32 flags;
    Uint32 verbose;

	int fsaa;
	int accel;

    // Video info
    const char *videodriver;
    int display;
    const char *window_title;
    const char *window_icon;
    Uint32 window_flags;
    int window_x;
    int window_y;
    int window_w;
    int window_h;
    int window_minW;
    int window_minH;
    int window_maxW;
    int window_maxH;
    int logical_w;
    int logical_h;
    float scale;
    int depth;
    int refresh_rate;
    int num_windows;
    SDL_Window **windows;

    // Renderer info
    const char *renderdriver;
    Uint32 render_flags;
    SDL_bool skip_renderer;
    SDL_Renderer **renderers;
    SDL_Texture **targets;

    // Audio info
   /* const char *audiodriver;
    SDL_AudioSpec audiospec;*/

	void* hWnd;
	SDL_Window *window_temp;
} tDtbSDLStruct, *hDtbSDLStruct;

// Queue element
typedef struct {
  int32_t	  frame_width_;
  int32_t	  frame_height_;
  BYTE* 	  buffer_;
  bool		  new_texture_;
  DWORD32	  cts_;
} queue_ele_t;

// Renderer class
class SDLOutVideo {
 public:
  explicit SDLOutVideo(HANDLE hWnd, FrTimeHandle hTime)
      : width_(0),
        height_(0),
        bKeepSourceAspectRatio_(true),
        queue_num_(0),
		video_format_checked_(false),
		video_format_(SDL_VIDEO_OUT_I420),
		frame_count_(0),
		sdl_(NULL),
		context_(NULL)
		{
			TRACE("[SDLOutVideo] created start");
		    FrCreateMutex(&mutex_);
			
			queue_read_idx_ = queue_write_idx_ = 0;
			texture_width_ = texture_height_ = texture_pitch_ = 0;
			texture_pitch_ratio_ = 0.0;
			hTime_ = hTime;

			fbo_used_ = false;
			fbo_width_ = 0;
			fbo_height_ = 0;
			texture_created_ = false;
			
			view_pos_w_ = 0.5;
			view_pos_h_ = 0.5;
			view_filter_w_ = 1.0;
			view_filter_h_ = 1.0;

			memset(&queue_, 0, sizeof(queue_));
			
			sdl_ = NULL;			
			context_ = NULL;
			
			hWnd_ = hWnd;

			zone_ = 0;
			
			TRACE("[SDLOutVideo] created end");

			return;
		}
  
  virtual ~SDLOutVideo() {
  	  TRACE("[SDLOutVideo] delete start");

	  //if (gles2_if_)
	  //{
		 // glClearColor(0, 0, 0, 0.0f);
		 // gles2_if_->glClear(GL_COLOR_BUFFER_BIT);
		 // //SDL_GL_SwapWindow(sdl_->windows[0]);

		 // DeleteFBO();

		 // if (texture_y_)
			//gles2_if_->glDeleteTextures(1, &texture_y_);
		 // if (texture_u_)
			//gles2_if_->glDeleteTextures(1, &texture_u_);
		 // if (texture_v_)
			//gles2_if_->glDeleteTextures(1, &texture_v_);
		 // if (texture_rgb_)
			//gles2_if_->glDeleteTextures(1, &texture_rgb_);
		 // if (texture_logo_rgb_)
			//gles2_if_->glDeleteTextures(1, &texture_logo_rgb_);

		 // if (program_yuv_)
			//gles2_if_->glDeleteProgram(program_yuv_);

		 // CLOSE();

		 // if (pLogImg_)
			//  FREE(pLogImg_);
	  //}

	  FrDeleteMutex(&mutex_);	  

	  TRACE("[SDLOutVideo] deleted");
  } 

  void DidChangeView2(int32_t new_width, int32_t new_height, bool render);
  void DidChangeViewPort(float new_center_hor, float new_center_ver, float new_width, float new_height);
  void GetView(uint32_t *width, uint32_t *height) {
  	if (width)
		*width = width_;
	if(height)
		*height = height_;
  };

  bool INIT(int32_t src_width, int32_t src_height, int32_t target_width, int32_t target_height, int bitCount);

  // Video frame management
  void InitQueue(int32_t queue_num, int32_t frame_width, int32_t frame_height);
  bool PutOneFrame(FrRawVideo *pMedia);
  void EmptyQueue();
  void ResetQueue();
  bool QueueIsFull();
  void SetDisplayeMode(bool bKeepSourceAspectRatio) {
  	TRACE("log:  SetDisplayMode : %d", bKeepSourceAspectRatio);
  	bKeepSourceAspectRatio_ =  bKeepSourceAspectRatio;
	SetNewViewPort();
  };
  bool GetCurrentQueue(queue_ele_t*);
  DWORD32 GetCurrentQueueNum();

  bool RemoveOneFrame();
  DWORD32 GetCurTime() { return FrTimeGetTime(hTime_);};
  bool RenderOneFrame();
  void PlayOutOneFrame();
 
  bool SetLogoRender(CHAR* pszLogoFile, DWORD32 dwInterval, DWORD32 dwDuration, FLOAT fStartW, FLOAT fSizeW, FLOAT fStartH, FLOAT fSizeH);

 private:
  bool InitGL(int32_t new_width, int32_t new_height);
  void DrawYUV(DWORD32 dwDummy);
  void DrawYUV_TopView(DWORD32 dwZone);
  void DrawRGB();
  void DrawRGB_logo(DWORD32 dwCTS);
  BOOL AllocFBO(int32_t width, int32_t height);
  //GLuint CreateTexture(int32_t width, int32_t height, int unit, eSDL_VIDEO_OUT_FORMAT video_format_);
  void SetNewViewPort();
  void CreateGLObjects();
  //void CreateShader(GLuint program, GLenum type, const char* source);
  void CreateTextures();
  void DeleteFBO();
  void Render(int32_t frame_width, int32_t frame_height, int32_t frame_pitch, int32_t cts, BYTE *data);
  void Render(int32_t frame_width, int32_t frame_height, int32_t frame_pitch, int32_t cts, BYTE *data_y, BYTE *data_u, BYTE *data_v, DWORD32 dwZone);
  bool CLOSE();
  bool PutOneFrameWithQueue(FrRawVideo *pMedia);
  bool RenderOneFrameDirectly(FrRawVideo *pMedia);
  void processLogoFile();
  void processLogoFile2();

  DWORD32 zone_;
  // 
  bool texture_created_;
  // size of display area
  int32_t width_;
  int32_t height_;
  bool bKeepSourceAspectRatio_;

  // Queue
  int32_t queue_num_;
  MUTEX_HANDLE	mutex_;
  queue_ele_t   queue_[MAX_VIDEO_QUEUE_ALLOWED];
  DWORD32 queue_write_idx_;
  DWORD32 queue_read_idx_;
  DWORD32 queue_read_idx_prev;

  bool video_format_checked_;
  eSDL_VIDEO_OUT_FORMAT video_format_;
  /*GLuint program_yuv_;
  GLuint buffer_;
  GLuint texture_y_;
  GLuint texture_u_;
  GLuint texture_v_;
  GLuint texture_rgb_;
  GLuint texture_logo_rgb_;
  GLuint fbo_;
  GLuint rbo_;
  GLuint fbo_texture_;*/
  bool	 fbo_used_;
  DWORD32	fbo_width_;
  DWORD32	fbo_height_;

  // size of video frame from decoders
  int32_t texture_width_;
  int32_t texture_height_;
  int32_t texture_pitch_;
  float texture_pitch_ratio_;
  float texture_width_ratio;
  float texture_height_ratio;
  int32_t black_zone_width_;
  int32_t black_zone_height_;
  int32_t active_width_;
  int32_t active_height_;

  float	view_pos_w_;
  float view_pos_h_;
  float view_filter_w_;
  float view_filter_h_;

  BYTE	frame_count_;
  /*GLfloat	trans_[4][4];
  GLfloat	proj_[16];
  GLfloat	rot_[16];
  GLfloat	final_[16];
  GLfloat	coef_[100];*/

  // Unowned pointers.
  // SDL context
  hDtbSDLStruct	sdl_;
  SDL_GLContext* context_;
  //GLES2_Context* gles2_if_;

  FrTimeHandle hTime_;
  HANDLE hWnd_;

  SDL_Window* m_pScreen;
  SDL_Renderer* m_pRenderer;
  SDL_Texture* m_pTexture;
  SDL_RendererInfo info;

  SDL_Rect srcRect;
  SDL_Rect dstRect;


#ifdef	WIN32
  RECT rcWnd_;
#endif


  FLOAT			fFade_[100];		// Fade coeff
};

#ifdef __cplusplus
extern "C"
{
#endif	/* __cplusplus */

//int LoadContext(GLES2_Context* data);

#ifdef __cplusplus
}
#endif	/* __cplusplus */

#endif

#endif	// VIDEOOUT_SDL