
#ifndef	_DEVICETEXTOUT_DRAWTEXT_H_
#define	_DEVICETEXTOUT_DRAWTEXT_H_

#include "SystemAPI.h"
#include "TraceAPI.h"
#include "mediainfo.h"

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_STROKER_H

#define DTB_MDPI 

typedef enum {
	USER_FT_FACE = 0,
	DEFAULT_FT_FACE,
	SYS_KOR_FT_FACE,
	SYS_S_FT_FACE
}	E_FT_FACE;
#define E_FT_FACE_NUM 4

#define DTB_MDPI 160.0

typedef enum {
	OUTLINE_NONE = 0,
	OUTLINE_THIN,
	OUTLINE_NORMAL,
	OUTLINE_THICK
}	OUTLINE_TYPE;

typedef	struct {
	BYTE*	m_Buff;
	BYTE*	m_pMediaBuff;

	HWND		m_hWnd;
	DWORD32		m_dwBitCount;		//
	DWORD32		m_dwWidth;
	DWORD32		m_dwHeight;
	DWORD32		m_dwFourCC;
	DWORD32		m_dwPitch;

	RECT		m_rcWnd;
	DWORD32		m_dwWndWidth;
	DWORD32		m_dwWndHeight;

	BOOL		m_bFtInitialize;

	FT_Library	m_ft_library;
	FT_Face		m_ft_face[E_FT_FACE_NUM];	// 0: User Font, 1:Default Font, 2:System Font (Kor character), 3: System Font (Special character) 
	float		m_ftColor[4];

	BOOL		m_bFindKorFont;
	BOOL		m_bFindSFont;

	float		mElapsed;
    double		mLastTime;

	DWORD32		m_dwTextSurfaceWidth;
	DWORD32		m_dwTextSurfaceHeight;
	float		m_fScreenXdpi;
	float		m_fScreenYdpi;
	float		m_fScreenRatio;

    DWORD32     m_dwTextSize;
	DWORD32     m_dwTextLeft;
	DWORD32     m_dwTextBottom;
	DWORD32     m_dwTextYPosGap;
	//DWORD32     m_dwTextOutlineSize;
	OUTLINE_TYPE m_eOutlineType;

    BOOL        m_bBold;
	BOOL		m_bShadow;
    BOOL        m_bUserDefineColor;
    DWORD32     m_dwUserDefineColor;
    char        m_szFont[MAX_STRING_LENGTH];

	// Block Data
    UTF32		m_szBlockData[MAX_TEXT_SUBBLOCK][MAX_STRING_LENGTH];
	DWORD32		m_dwBlockDataWidth[MAX_TEXT_SUBBLOCK];
	DWORD32		m_dwBlockDataHeight[MAX_TEXT_SUBBLOCK];
	BOOL		m_bBlockDataCR[MAX_TEXT_SUBBLOCK];
	BOOL		m_bBlockDataBold[MAX_TEXT_SUBBLOCK];
	FLOAT		m_ftBlockDataFontColor[MAX_TEXT_SUBBLOCK][4];
    DWORD32     m_dwBlockSize;

} DrawTextStruct, *DrawTextHandle;


#ifdef __cplusplus
extern "C" {
#endif

// Internal functions
BOOL Init_ft_resources(DrawTextHandle hHandle); 
void Free_ft_resources(DrawTextHandle hHandle);

BOOL SetSystemDefaultFont(DrawTextHandle hDevice, char* szPath);
BOOL IsAvailableFont(DrawTextHandle hDevice, char *pszFontName, UTF32 ulChar, FT_Face *pftType);

void render_text(DrawTextHandle hHandle, const UTF32 *text, BYTE* pOut, INT32 nTextHeight, DWORD32 dwStartW, DWORD32 dwStartH, DWORD32 dwPitch);
//void measure_text(DrawTextHandle hHandle, const UTF32 *text, int *pnWidth, int *pnHeight);
void measure_text(DrawTextHandle hHandle, const UTF32 *text, int nEglWidth, int *pnWidth, int *pnHeight, int *pnTextNum);

void render_text2(DrawTextHandle hHandle, const UTF32 *text, float x, float y, float sx, float sy, float *ftColor, int nOutlineSize, BOOL bBold, BOOL bShadow);

// APIs
LRSLT DrawTextOpen(DrawTextHandle* phDevice, McTextInfo* hInfo);
void DrawTextClose(DrawTextHandle hHandle);
BOOL DrawTextPutString(DrawTextHandle hHandle, McMediaText* pstText, McMediaVideo* pMedia);
BOOL DrawTextPutString4(DrawTextHandle hHandle, CHAR* pstText, McMediaVideo *pMedia);
BOOL DrawTextPutString3(DrawTextHandle hHandle, McMediaText* pstText);
BOOL DrawTextReset(DrawTextHandle hHandle);
BOOL DrawTextSetSurfaceChanged(DrawTextHandle hHandle, HWND hWnd);
#ifdef __cplusplus
}
#endif
#endif