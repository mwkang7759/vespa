
#ifndef	_DEVICEVIDEOOUT_BASE_H_
#define	_DEVICEVIDEOOUT_BASE_H_

//#define		VIDEOOUT_D3D9DRAW
//#define		VIDEOOUT_GDI
#define		VIDEOOUT_SDL

#if defined(_WIN32 ) && (defined(VIDEOOUT_DDRAW) || defined(VIDEOOUT_D3D9DRAW))
#include "stdafx.h"
#endif

#include "SystemAPI.h"
#include "TraceAPI.h"
#include "UtilAPI.h"
#include "mediainfo.h"

#include <vector>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/types_c.h>

#define VIEW_TYPE_RATIO_NOT_SUPPORT		0x00	// 비율 유지 안함 
#define VIEW_TYPE_RATIO_SUPPORT			0x01	// 비율 유지
#define VIEW_TYPE_RATIO_FULL			0x02    // 꽉찬 비율 
#define	VIEW_TYPE_RATIO_4_3				0x03    // 4:3 비율
#define	VIEW_TYPE_RATIO_16_9			0x04	// 16:9 비율
#define	VIEW_TYPE_RATIO_185_1			0x05	// 1.85:1 비율
#define	VIEW_TYPE_RATIO_235_1			0x06	// 2.35:1 비율
#define VIEW_TYPE_ORG_SIZE				0x07	// 원본 고정
#define VIEW_TYPE_RATIO_USER_DEFINE		0x08	// 사용자 정의 비율

#define TEXT_SHOW						0x00
#define TEXT_HIDE						0x01

#define	TEXT_VIDEO_DISPLAY				0x00
#define	TEXT_OVERLAY_DISPLAY			0x01

#define	MAX_FOURCC						5
#define	MAX_NUM_FOURCC					30

#define VIEW_RATIO_ORIGINAL					0
#define VIEW_RATIO_4_3						1.33
#define VIEW_RATIO_16_9						1.78
#define VIEW_RATIO_WIDESCREEN				1.85
#define VIEW_RATIO_CINEMASCOPE				2.35
typedef enum 
{
	VIDEO_ORIGINAL,
	VIDEO_4_3,
	VIDEO_16_9,
	VIDEO_WIDESCREEN,
	VIDEO_CINEMASCOPE,
} VIDEOOUT_RATIO;

typedef enum 
{
	VIDEO_DATA_NONE,
	VIDEO_RGB_16_555,
	VIDEO_RGB_16_565,
	VIDEO_RGB_24,
	VIDEO_YUV_420
} VIDEO_DATATYPE;

#define	VIDEO_MEDIA_BUFFER		3

////////////////////////////////
//		WINDOWS
////////////////////////////////

#if defined(VIDEOOUT_GDI) && !defined(VIDEOOUT_SDL)
typedef	struct {

	// Bitmap
	BITMAPINFOHEADER			m_pBitmapInfo;
	HDC							m_hDC;
	BYTE*						m_Buff;
	BYTE*						m_BuffBG;

	BOOL						m_bDDraw;
	BOOL						m_bDrawBG;

	HWND						m_hWnd;
	HWND						m_hTextWnd;
	HDC							m_hTextDC;

	INT32						m_nDevBitCount;		//
	DWORD32						m_dwFourCC;			//
	DWORD32						m_dwBitCount;		//	

	RECT						m_rcWnd;
	DWORD32						m_dwWndWidth;
	DWORD32						m_dwWndHeight;
	INT32						m_nWndX;
	INT32						m_nWndY;
	RECT						m_rcSrc;
	DWORD32						m_dwWidth;
	DWORD32						m_dwHeight;
	RECT						m_rcDest;
	INT32						m_nDisplayType;
	DWORD32						m_dwUserDefineRatioX;
	DWORD32						m_dwUserDefineRatioY;

	// Text
	BOOL						m_bText;
	RECT						m_rcText;

	HFONT						m_hTextFont;
	//McMediaText					m_TextBlock;
	INT32						m_FontColor;		// RGBA in network byte order

	BOOL						m_bTextShowHide;
	INT32						m_dwTextDisplayType;
	char						m_strFont[MAX_STRING_LENGTH];
	DWORD32						m_dwTextSize;
	BOOL						m_bBold;
	BOOL						m_bUserDefineColor;
	DWORD32						m_dwUserDefineColor;
	DWORD32						m_dwUpMovePos;
	DWORD32						m_dwDownMovePos;
	DWORD32						m_dwLeftMovePos;
	DWORD32						m_dwRightMovePos;

	// Status text
	/*char						m_szStatus[MAX_TEXT_STRING_LEN];
	INT32						m_nStatusFontColor;
	DWORD32						m_nStatusFontSize;*/

	// Performance Info text
	char						m_szPerfInfo[DEF_LENGTH_2048];
	INT32						m_nPerfInfoFontColor;
	DWORD32						m_nPerfInfoFontSize;

	POINT						m_nDisplayPostionXY;	// on test..
	BOOL						m_bFullscreen;			// on test..

	INT64						m_n64FreqTime;
	INT64						m_n64BegineTime;
	INT64						m_n64EndTime;

	//DrawTextHandle				m_hDrawText;

#ifdef	VIDEO_DUMP
	FILE_HANDLE					m_hFile;
#endif
	BOOL						m_bVSyncOff;

} McVideoOutStruct, *McVideoOutStructHandle;


void CaptureFrame(McVideoOutStructHandle hDevice, FrRawVideo *pMedia);
void ColorControl(McVideoOutStructHandle hDevice, FrRawVideo *pMedia);
void GetDisplayRectangle(McVideoOutStructHandle hDevice, RECT* rcDisplay);

BOOL GDIOpen(McVideoOutStructHandle hDevice);
void GDIClose(McVideoOutStructHandle hDevice);
BOOL GDITransToBuffer(McVideoOutStructHandle hDevice, FrRawVideo* pMedia);
BOOL GDIClear(McVideoOutStructHandle hDevice);
BOOL GDIPlay(McVideoOutStructHandle hDevice);
BOOL GDIPlayString(McVideoOutStructHandle hDevice);
#endif


#if defined(VIDEOOUT_SDL) && !defined(VIDEOOUT_GDI)
class SDLOutVideo;

typedef	struct {
	BOOL			m_bBufferEnable;
	FrRawVideo	m_Buffer[VIDEO_MEDIA_BUFFER];
	DWORD32			m_dwReadBuffer;
	DWORD32			m_dwWriteBuffer;
	DWORD32			m_dwBitCount;		//
	DWORD32			m_dwWidth;			//
	DWORD32			m_dwHeight;
	DWORD32			m_dwFourCC;
	DWORD32			m_dwPitch;

	BYTE* m_pImgBuffer;

	float 			mElapsed;
	double 			mLastTime;

	DWORD32			m_dwTargetWidth;
	FLOAT		 	m_fTargetHeightR;
	FLOAT			m_fTargetWidthR;

	DWORD32			m_dwOutWidth;			// OpenGL ViewPort size
	DWORD32			m_dwOutHeight;			//

	INT32			m_nViewCenterHor;		// center point w
	INT32			m_nViewCenterVer;	// center point h
	INT32			m_nViewWidth;			// view width
	INT32			m_nViewHeight;			// view height

	FLOAT			m_fViewCenterHor;			// normalized
	FLOAT			m_fViewCenterVer;
	FLOAT			m_fViewWidth;
	FLOAT			m_fViewHeight;

	HANDLE			m_hWnd;				// Window handle
#ifdef	WIN32	
	RECT			m_rcWnd;			// Window size & postion
#endif

	SDLOutVideo* m_hDev;				// SDL handle

} McVideoOutStruct, * McVideoOutStructHandle;

#endif // VIDEOOUT_SDL


















#endif //	_DEVICEVIDEOOUT_BASE_H_

