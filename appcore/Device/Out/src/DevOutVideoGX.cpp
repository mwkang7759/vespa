/*****************************************************************************
*                                                                            *
*                            DeviceOutput Library							 *
*                                                                            *
*   Copyright (c) 2004 by Mcubeworks, Incoporated. All Rights Reserved.      *
******************************************************************************

    File Name       : DevOutVideoGX.c
    Author(s)       : Kang, MinWoo
    Created         : 31 Jan 2006

    Description     : DeviceOutput API
    Notes           : 

==============================================================================
                      Modification History (Reverse Order)                    
==============================================================================
    Date        Author      Location (variables) / Description
------------------------------------------------------------------------------

------------------------------------------------------------------------------*/
#include "DevOutVideo.h"

#define	DISPLAY_RATIO	(0.77)

#ifdef _WIN32_WCE

BOOL GXOpen(McVideoOutHandle hDevice)
{
	hDevice->m_nBitCount = 16;
	// check fourcc
	if(hDevice->m_dwFourCC == BI_BITFIELDS)
	{
		// display RGB directly..
		hDevice->m_bDirectRGB = TRUE;
	}

	// Buffer
	hDevice->m_Buff=NULL;
	hDevice->m_Buff = (BYTE*)MALLOC(hDevice->m_dwWidth * hDevice->m_dwHeight * (hDevice->m_nBitCount>>3));
	if(hDevice->m_Buff==NULL)
		return FALSE;

	hDevice->m_BuffBG=NULL;
	hDevice->m_BuffBG = (BYTE*)MALLOC(hDevice->m_dwWidth * hDevice->m_dwHeight * (hDevice->m_nBitCount>>3));
	if(hDevice->m_BuffBG==NULL)
	{
		GXClose(hDevice);
		return FALSE;
	}

	memset(hDevice->m_Buff, 0, hDevice->m_dwWidth * hDevice->m_dwHeight * (hDevice->m_nBitCount>>3));
	memset(hDevice->m_BuffBG, 0, hDevice->m_dwWidth * hDevice->m_dwHeight * (hDevice->m_nBitCount>>3));

	// add to resize, mwkang
	SYSTEM_INFO	sysInfo;
	GetSystemInfo(&sysInfo);

	if(hDevice->m_bDirectRGB)
	{
		// on test..
		hDevice->m_InRGB.iXsize = hDevice->m_dwWidth;
		hDevice->m_InRGB.iYsize = hDevice->m_dwHeight;
	}
	else
	{
		hDevice->m_pOutYUV = (BYTE*)MALLOC(MAX_WIDTH * MAX_HEIGHT * 3 / 2);
		if(hDevice->m_pOutYUV==NULL)
		{
			GXClose(hDevice);
			return FALSE;
		}

		hDevice->m_pInYUV = (BYTE*)MALLOC(hDevice->m_dwWidth * hDevice->m_dwHeight * 3 / 2);
		if(hDevice->m_pInYUV==NULL)
		{
			GXClose(hDevice);
			return FALSE;
		}
		
		hDevice->m_InSize.pImage = hDevice->m_pInYUV;			// input start pointer
		hDevice->m_InSize.iXsize = hDevice->m_dwWidth;			// input image width
		hDevice->m_InSize.iYsize = hDevice->m_dwHeight;			// input image height
		hDevice->m_InSize.iXoffset = 0;							// input image start offset in x direction
		hDevice->m_InSize.iYoffset = 0;							// input image start offset in y direction
		hDevice->m_InSize.iBufferWidth = hDevice->m_dwWidth;	// input buffer width
		hDevice->m_InSize.iBufferHeight = hDevice->m_dwHeight;	// input buffer height

		hDevice->m_OutSize.pImage = hDevice->m_pOutYUV;			// output start pointer

		hDevice->m_dwSrcYSize = hDevice->m_dwWidth * hDevice->m_dwHeight;
		hDevice->m_dwSrcUVSize = hDevice->m_dwWidth * hDevice->m_dwHeight / 4;
	}
	
	// get screen size
	RECT rc;
	::GetWindowRect(hDevice->m_hWnd, &rc);
	hDevice->m_dwDisplayWidth = rc.right-rc.left;
	hDevice->m_dwDisplayHeight = rc.bottom-rc.top;

	// text..
	memset((void *)hDevice->m_strText, 0x00, MAX_TEXT_STRING_LEN*4);
	hDevice->m_bIsTextStart = FALSE;
	//hDevice->m_bFullscreen = FALSE;
	//hDevice->m_bFullscreen = bFullScreen;
	
	// init GAPI..
	if (GXIsDisplayDRAMBuffer() == TRUE)
		GXSetViewport(55, 155, 0, 0);  // 프레임 버퍼와 상관없이 320*240 에 대한 Viewport 크기
		
	GXOpenDisplay(NULL, GX_FULLSCREEN);
	hDevice->m_gxProperty = GXGetDisplayProperties(); 
	hDevice->m_pStartRGB16Bufgx = (unsigned char *) GXBeginDraw();
	GXEndDraw();
	GXCloseDisplay();

	hDevice->m_bFullSameSize = FALSE;
	if(hDevice->m_dwWidth == hDevice->m_gxProperty.cyHeight && 
		hDevice->m_dwHeight == hDevice->m_gxProperty.cxWidth)
	{
		hDevice->m_bFullSameSize = TRUE;
	}

	TRACE("[DevOutVideo]	McVideoOutOpen - GX\n");

	return	TRUE;
}

void GXClose(McVideoOutHandle hDevice)
{
	if (hDevice->m_Buff)
		FREE(hDevice->m_Buff);
	if (hDevice->m_BuffBG)
		FREE(hDevice->m_BuffBG);
	if(hDevice->m_pOutYUV)
		FREE(hDevice->m_pOutYUV);
	if(hDevice->m_pInYUV)
		FREE(hDevice->m_pInYUV);
	//hDevice->m_hDC->ReleaseDC(hDevice->m_hWnd);
}


BOOL GXTransToBuffer(McVideoOutHandle hDevice, McMediaVideo* pMedia)
{
	if(hDevice)
	{
		int nPos=0;
		
		if(hDevice->m_bDirectRGB)
		{
			hDevice->m_InRGB.pImage = pMedia->m_pY;
		}
		else
		{
			if( pMedia->m_pY == NULL || pMedia->m_pU == NULL || pMedia->m_pV == NULL)
				return TRUE;

			memcpy(hDevice->m_pInYUV, pMedia->m_pY, hDevice->m_dwSrcYSize);
			nPos += hDevice->m_dwSrcYSize;
			memcpy(hDevice->m_pInYUV + nPos, pMedia->m_pU, hDevice->m_dwSrcUVSize);
			nPos += hDevice->m_dwSrcUVSize;
			memcpy(hDevice->m_pInYUV+ nPos, pMedia->m_pV, hDevice->m_dwSrcUVSize);	
		}
	}

	return	TRUE;
}

// check window existing or not to over display rectangle..
BOOL CheckOverWindow(McVideoOutHandle hDevice)
{
	RECT	rc, rcScreen;
	POINT	BottomRight, TopRight;

	// get screen size
	::GetWindowRect(hDevice->m_hWnd, &rc);

	hDevice->m_dwDisplayWidth = rc.right-rc.left;
	hDevice->m_dwDisplayHeight = rc.bottom-rc.top;
	hDevice->m_StartPos.x = rc.left;
	hDevice->m_StartPos.y = rc.top;

	hDevice->m_dwMaxWidth = hDevice->m_dwDisplayWidth;
	hDevice->m_dwMaxHeight = (DWORD)(hDevice->m_dwDisplayWidth * DISPLAY_RATIO);		// ratio
	// for display list button..
	if(hDevice->m_dwMaxHeight > hDevice->m_dwDisplayHeight)
	{
		//hDevice->m_dwMaxWidth = hDevice->m_dwDisplayWidth >> 1;
		hDevice->m_dwMaxHeight = hDevice->m_dwDisplayHeight;
		
		//hDevice->m_StartPos.x = rc.left + hDevice->m_dwDisplayWidth >> 2;
		//hDevice->m_StartPos.y = rc.top;
	}
	if(hDevice->m_dwMaxHeight % 2 != 0)
		hDevice->m_dwMaxHeight++;
	if(hDevice->m_dwMaxWidth % 2 != 0)
		hDevice->m_dwMaxWidth++;

	// the window with which the user is currently working, ???
	//hCheckWnd=::GetForegroundWindow();

	// retrieves the coordinates of a window’s client area.
	::GetClientRect(hDevice->m_hWnd, &rcScreen);

	// retrieves the handle to the window that contains the specified point
	if(WindowFromPoint(hDevice->m_StartPos)!= hDevice->m_hWnd)
		return TRUE;

	TopRight.x = hDevice->m_StartPos.x + hDevice->m_dwDisplayWidth - 1;
	TopRight.y = hDevice->m_StartPos.y;
	if(WindowFromPoint(TopRight) != hDevice->m_hWnd)
		return TRUE;

	TopRight.x = hDevice->m_StartPos.x + 80 - 1;
	TopRight.y = hDevice->m_StartPos.y;
	if(WindowFromPoint(TopRight) != hDevice->m_hWnd)
		return TRUE;

	TopRight.x = hDevice->m_StartPos.x + 160 - 1;
	TopRight.y = hDevice->m_StartPos.y;
	if(WindowFromPoint(TopRight) != hDevice->m_hWnd)
		return TRUE;

	BottomRight.x = hDevice->m_StartPos.x + hDevice->m_dwDisplayWidth - 1;
	BottomRight.y = hDevice->m_StartPos.y + hDevice->m_dwDisplayHeight - 1;
	if(WindowFromPoint(BottomRight) != hDevice->m_hWnd)
		return TRUE;
	
	return FALSE;
}


BOOL GXPlay(McVideoOutHandle hDevice)
{
	RECT		rc;
	RECT		rcUpdateTop , rcUpdateBottom , rcUpdateLeft, rcUpdateRight;
	INFO_SIZE	SizeInfo;
	BYTE*		pDeviceStartPos=NULL;
	long		nMovePos=0;
	//int			nWidth;
	int			nHeight=9;
	TCHAR		strText[1024];
	memset((void *)strText,0x00,1024);
	
	if(hDevice)
	{
		//if(hDevice->m_pYUV.pbY == NULL || hDevice->m_pYUV.pbU == NULL || hDevice->m_pYUV.pbV == NULL)
		//	return TRUE;
		hDevice->m_FontColor = hDevice->m_TextBlock.block[0].font_color;
		for (int i = 0;i < hDevice->m_TextBlock.block_num;i++)
		{
			if(i==0)
				wcscpy(strText, (WCHAR *)hDevice->m_TextBlock.block[i].data);
			else
				wcscat(strText, (WCHAR *)hDevice->m_TextBlock.block[i].data);
		}

		// normal window mode..
		if(!hDevice->m_bFullscreen)
		{
			if(CheckOverWindow(hDevice) == TRUE)
				return TRUE;

			// start position to frame buffer..
			pDeviceStartPos = hDevice->m_pStartRGB16Bufgx;

			if(hDevice->m_bDirectRGB)
			{
				//memcpy(&pDeviceStartPos[0], &hDevice->m_InRGB.pImage[0], (hDevice->m_dwWidth*hDevice->m_dwHeight*2));
				// need to resize
				// ..

			} // end if(direct rgb)
			else
			{
				// color conversion parameters..
				SizeInfo.YUV_CropWidth	= hDevice->m_dwMaxWidth;
				SizeInfo.YUV_CropHeight	= hDevice->m_dwMaxHeight;
				SizeInfo.YUV_yStep		= hDevice->m_dwMaxWidth;

				SizeInfo.RGB_xStep = hDevice->m_gxProperty.cbxPitch;
				SizeInfo.RGB_yStep = hDevice->m_gxProperty.cbyPitch;

				// resize parameters..
				hDevice->m_OutSize.iXsize = hDevice->m_dwMaxWidth;			// output image width
				hDevice->m_OutSize.iYsize = hDevice->m_dwMaxHeight;			// output image height
				hDevice->m_OutSize.iXoffset = 0;							// output image start offset in x direction
				hDevice->m_OutSize.iYoffset = 0;							// output image start offset in y direction
				hDevice->m_OutSize.iBufferWidth = hDevice->m_dwMaxWidth;	// output buffer width
				hDevice->m_OutSize.iBufferHeight = hDevice->m_dwMaxHeight;	// output buffer height

				YUV420_ImageResize(&hDevice->m_InSize, &hDevice->m_OutSize);
				
				hDevice->m_pYUV.pbY = hDevice->m_OutSize.pImage;
				hDevice->m_pYUV.pbU = hDevice->m_pYUV.pbY + hDevice->m_dwMaxWidth * hDevice->m_dwMaxHeight;
				hDevice->m_pYUV.pbV = hDevice->m_pYUV.pbU + hDevice->m_dwMaxWidth * hDevice->m_dwMaxHeight / 4;

				// to update text window later, get upper RECT, left RECT, right RECT, bottom RECT
				//SetRect(&rcUpdateTop,0,0,hDevice->m_dwDisplayWidth,(hDevice->m_dwDisplayHeight-nHeight)/2);
				//SetRect(&rcUpdateBottom,0,nHeight+(hDevice->m_dwDisplayHeight-nHeight)/2,hDevice->m_dwDisplayWidth,hDevice->m_dwDisplayHeight);
				//SetRect(&rcUpdateLeft,0,0,(hDevice->m_dwDisplayWidth-nWidth)/2,hDevice->m_dwDisplayHeight);
				//SetRect(&rcUpdateRight,nWidth+(hDevice->m_dwDisplayWidth-nWidth)/2,0,hDevice->m_dwDisplayWidth,hDevice->m_dwDisplayHeight);
				SetRect(&rcUpdateTop,0,0,hDevice->m_dwDisplayWidth,(hDevice->m_dwDisplayHeight-hDevice->m_dwMaxHeight)/2);
				SetRect(&rcUpdateBottom,0,hDevice->m_dwMaxHeight+(hDevice->m_dwDisplayHeight-hDevice->m_dwMaxHeight)/2,hDevice->m_dwDisplayWidth,hDevice->m_dwDisplayHeight);
				SetRect(&rcUpdateLeft,0,0,(hDevice->m_dwDisplayWidth-hDevice->m_dwMaxWidth)/2,hDevice->m_dwDisplayHeight);
				SetRect(&rcUpdateRight,hDevice->m_dwMaxWidth+(hDevice->m_dwDisplayWidth-hDevice->m_dwMaxWidth)/2,0,hDevice->m_dwDisplayWidth,hDevice->m_dwDisplayHeight);

				nMovePos = SizeInfo.RGB_xStep*hDevice->m_StartPos.x + 
						SizeInfo.RGB_yStep*(hDevice->m_StartPos.y+((hDevice->m_dwDisplayHeight - hDevice->m_dwMaxHeight) >> 1));
				pDeviceStartPos += nMovePos;

				YUV420_To_RGB16_S ((unsigned short *)pDeviceStartPos, &hDevice->m_pYUV, &SizeInfo);
//				YUV420_To_RGB16_4_3 ((unsigned short *)pDeviceStartPos, &hDevice->m_pYUV, &SizeInfo);
/*
void    YUV420_To_RGB16_Q   (unsigned short *pwRGB16, POINTER_YUV *pYUVP, INFO_SIZE *pSizeInfo);
void    YUV420_To_RGB16_H   (unsigned short *pwRGB16, POINTER_YUV *pYUVP, INFO_SIZE *pSizeInfo);
void    YUV420_To_RGB16_S   (unsigned short *pwRGB16, POINTER_YUV *pYUVP, INFO_SIZE *pSizeInfo);
void    YUV420_To_RGB16_4_3 (unsigned short *pwRGB16, POINTER_YUV *pYUVP, INFO_SIZE *pSizeInfo);
void    YUV420_To_RGB16_D   (unsigned short *pwRGB16, POINTER_YUV *pYUVP, INFO_SIZE *pSizeInfo);
*/
			}
		}
		// full screen mode..
		else 
		{
			pDeviceStartPos = hDevice->m_pStartRGB16Bufgx;
			
			if(hDevice->m_bDirectRGB)
			{	
				// guess normal image..
				//for (unsigned int x=0; x< hDevice->m_dwWidth; x++)		// 320
				//{
				//	for (unsigned int y=0; y<hDevice->m_dwHeight; y++)	// 240
				//	{
				//		pDeviceStartPos[y*2 + x*hDevice->m_dwHeight*2] = hDevice->m_InRGB.pImage[(hDevice->m_dwWidth * (hDevice->m_dwHeight-1)+x)*2 - y*hDevice->m_dwWidth*2];
				//		pDeviceStartPos[y*2 + 1 + x*hDevice->m_dwHeight*2] = hDevice->m_InRGB.pImage[(hDevice->m_dwWidth * (hDevice->m_dwHeight-1)+x)*2 + 1 - y*hDevice->m_dwWidth*2];
				//	}
				//}

				// guess updown image..
				for (unsigned int x=0; x< hDevice->m_dwWidth; x++)		// 320
				{
					for (unsigned int y=0; y<hDevice->m_dwHeight; y++)	// 240
					{
						pDeviceStartPos[y*2 + x*hDevice->m_dwHeight*2] = hDevice->m_InRGB.pImage[(hDevice->m_dwWidth*hDevice->m_dwHeight-1-x)*2 - y*hDevice->m_dwWidth*2];
						pDeviceStartPos[y*2 + 1 + x*hDevice->m_dwHeight*2] = hDevice->m_InRGB.pImage[(hDevice->m_dwWidth*hDevice->m_dwHeight-1-x)*2 + 1 - y*hDevice->m_dwWidth*2];
					}
				}
			}
			else
			{
				SizeInfo.RGB_xStep = hDevice->m_gxProperty.cbyPitch;
				SizeInfo.RGB_yStep = -hDevice->m_gxProperty.cbxPitch;

				SizeInfo.YUV_CropWidth	= hDevice->m_gxProperty.cyHeight;
				SizeInfo.YUV_CropHeight	= hDevice->m_gxProperty.cxWidth;
				SizeInfo.YUV_yStep		= hDevice->m_gxProperty.cyHeight;

				if(!hDevice->m_bFullSameSize)
				{
					// resize parameters..
					hDevice->m_OutSize.iXsize = hDevice->m_gxProperty.cyHeight;
					hDevice->m_OutSize.iYsize = hDevice->m_gxProperty.cxWidth;
					hDevice->m_OutSize.iXoffset = 0;
					hDevice->m_OutSize.iYoffset = 0;
					hDevice->m_OutSize.iBufferWidth = hDevice->m_gxProperty.cyHeight;
					hDevice->m_OutSize.iBufferHeight = hDevice->m_gxProperty.cxWidth;

					YUV420_ImageResize(&hDevice->m_InSize, &hDevice->m_OutSize);
					
					hDevice->m_pYUV.pbY = hDevice->m_OutSize.pImage;
					hDevice->m_pYUV.pbU = hDevice->m_pYUV.pbY + hDevice->m_gxProperty.cxWidth * hDevice->m_gxProperty.cyHeight;
					hDevice->m_pYUV.pbV = hDevice->m_pYUV.pbU + hDevice->m_gxProperty.cxWidth * hDevice->m_gxProperty.cyHeight / 4;
				}
				else
				{
					hDevice->m_pYUV.pbY = hDevice->m_pInYUV;
					hDevice->m_pYUV.pbU = hDevice->m_pYUV.pbY + hDevice->m_gxProperty.cxWidth * hDevice->m_gxProperty.cyHeight;
					hDevice->m_pYUV.pbV = hDevice->m_pYUV.pbU + hDevice->m_gxProperty.cxWidth * hDevice->m_gxProperty.cyHeight / 4;
				}

				nMovePos = hDevice->m_gxProperty.cbyPitch;
				pDeviceStartPos += nMovePos;
				YUV420_To_RGB16_S ((unsigned short *)pDeviceStartPos, &hDevice->m_pYUV, &SizeInfo);

			} // end if(DirectRGB)
		}// end if(fullscreen)
	}

	// display text..
	if(hDevice->m_TextBlock.block_num && hDevice->m_TextBlock.block[0].len != 0 && wcslen(strText)>1 )
	{
		if(!hDevice->m_bFullscreen)
		{
			int	   oldmode;

			RECT rcText;
			RECT rect;

			hDevice->m_hDC = ::GetDC(hDevice->m_hWnd);
			HDC hdcMem = CreateCompatibleDC(hDevice->m_hDC);

			hDevice->m_hDC = ::GetDC(hDevice->m_hWnd);
			if(_tcscmp(hDevice->m_strText,strText))
			{
				if(!hDevice->m_bIsTextStart)
				{
					hDevice->m_bIsTextStart=TRUE;
				}
				else
				{
					InvalidateRect(hDevice->m_hWnd,&rcUpdateTop,FALSE);
					InvalidateRect(hDevice->m_hWnd,&rcUpdateBottom,FALSE);
					InvalidateRect(hDevice->m_hWnd,&rcUpdateLeft,FALSE);
					InvalidateRect(hDevice->m_hWnd,&rcUpdateRight,FALSE);
				}
//					::InvalidateRect(hDevice->m_hWnd,&rcUpdate,FALSE);
				_tcscpy(hDevice->m_strText,strText);
			}

			HFONT hTextFont=CreateTextFont(0,hDevice->m_bFullscreen);
			::SelectObject(hDevice->m_hDC,hTextFont);
			oldmode=::SetBkMode(hDevice->m_hDC,TRANSPARENT);
			::SetTextColor(hDevice->m_hDC, hDevice->m_FontColor);

			::DrawText(hDevice->m_hDC,strText,wcslen(strText), &rect, DT_LEFT | DT_CALCRECT);

			rcText.left=0;
			rcText.right=hDevice->m_dwDisplayWidth;
			rcText.bottom=hDevice->m_dwDisplayHeight;
			rcText.top=rcText.bottom - (rect.bottom-rect.top);


			::DrawText(hDevice->m_hDC,strText,wcslen(strText),&rcText,DT_CENTER);
			::SetBkMode(hDevice->m_hDC,oldmode);
			DeleteObject(hTextFont);
			::DeleteDC(hdcMem);

			::ReleaseDC(hDevice->m_hWnd, hDevice->m_hDC);
		}
		else	// fullscreen mode..
		{
			int	   oldmode;
			char	szTempBuff[1024];
			TCHAR	szLineText[512];
			RECT rcText;

			memset(szTempBuff,0x00,sizeof(szTempBuff));
			int ret = WideCharToMultiByte(CP_ACP, 0, strText, -1, szTempBuff, sizeof(szTempBuff), NULL, NULL);
			szTempBuff[ret+1]='\0';

			HDC hdcMem = CreateCompatibleDC(hDevice->m_hDC);
			//rcText=rc;
			hDevice->m_hDC = ::GetDC(hDevice->m_hWnd);
			if(_tcscmp(hDevice->m_strText,strText))
			{
				if(!hDevice->m_bIsTextStart)
				{
					hDevice->m_bIsTextStart=TRUE;
				}
				else
				{
					//InvalidateRect(hDevice->m_hWnd,&rcUpdateTop,FALSE);
					//InvalidateRect(hDevice->m_hWnd,&rcUpdateBottom,FALSE);
					//InvalidateRect(hDevice->m_hWnd,&rcUpdateLeft,FALSE);
					//InvalidateRect(hDevice->m_hWnd,&rcUpdateRight,FALSE);
				}
				_tcscpy(hDevice->m_strText,strText);
			}

			HFONT hTextFont=CreateTextFont(nHeight,hDevice->m_bFullscreen);
			::SelectObject(hDevice->m_hDC,hTextFont);

			oldmode=::SetBkMode(hDevice->m_hDC,TRANSPARENT);
			::SetTextColor(hDevice->m_hDC, hDevice->m_FontColor);

			rcText.top=0; // MAX_WIDTH/2;
			rcText.bottom=MAX_WIDTH;//rcText.top+nWidth;
			rcText.left=24;
			rcText.right=6;

			char *pstr=NULL;
			pstr=strrchr(szTempBuff,'\n');
			if(pstr==NULL)
			{
				if(strlen(szTempBuff)>0)
				{
					SIZE	XY;
					::GetTextExtentPoint32(hDevice->m_hDC,strText,wcslen(strText),&XY);
					if(XY.cx < (LONG)hDevice->m_gxProperty.cyHeight)
						rcText.top+=(MAX_WIDTH-XY.cx)/2;

					::DrawText(hDevice->m_hDC,strText,wcslen(strText),&rcText, DT_LEFT|DT_SINGLELINE);
				}
			}
			else
			{
				pstr[0]='\0';
				while(1)
				{
					memset(szLineText,0x00,512);
					pstr++;
					if(strlen(pstr)>0)
					{
						if(pstr[strlen(pstr)-1]=='\r')
							pstr[strlen(pstr)-1]='\0';

						MultiByteToWideChar ( CP_ACP, 0, pstr, -1, szLineText, strlen(pstr) );
						SIZE	XY;
						::GetTextExtentPoint32(hDevice->m_hDC,szLineText,wcslen(szLineText),&XY);
						if(XY.cx < (LONG)hDevice->m_gxProperty.cyHeight)
							rcText.top+=(hDevice->m_gxProperty.cyHeight-XY.cx)/2;

						::DrawText(hDevice->m_hDC,szLineText,wcslen(szLineText),&rcText, DT_LEFT );
						int nFontHeight=rcText.left-rcText.right;
						rcText.left+=nFontHeight;
						rcText.right+=nFontHeight;
						rcText.top=(MAX_WIDTH-hDevice->m_gxProperty.cyHeight)/2;
						rcText.bottom=rcText.top+hDevice->m_gxProperty.cyHeight;
					}
					pstr=strrchr(szTempBuff,'\n');
					if(pstr==NULL)
					{
						if(strlen(szTempBuff)>0)
						{
							if(szTempBuff[strlen(szTempBuff)-1]=='\r')
								szTempBuff[strlen(szTempBuff)-1]='\0';

							MultiByteToWideChar ( CP_ACP, 0, szTempBuff, -1, szLineText, strlen(szTempBuff) );
							SIZE	XY;
							::GetTextExtentPoint32(hDevice->m_hDC,szLineText,wcslen(szLineText),&XY);
							if(XY.cx<(LONG)hDevice->m_gxProperty.cyHeight)
								rcText.top+=(hDevice->m_gxProperty.cyHeight-XY.cx)/2;

							::DrawText(hDevice->m_hDC,szLineText,wcslen(szLineText),&rcText,DT_CENTER );

							int nFontHeight=rcText.left-rcText.right;
							rcText.left+=nFontHeight;
							rcText.right+=nFontHeight;
							rcText.top=(MAX_WIDTH-hDevice->m_gxProperty.cyHeight)/2;
							rcText.bottom=rcText.top+hDevice->m_gxProperty.cyHeight;
						}
						break;
					}
					else
						pstr[0]='\0';
				}
			}
			::SetBkMode(hDevice->m_hDC,oldmode);
			DeleteObject(hTextFont);
			::DeleteDC(hdcMem);
			::ReleaseDC(hDevice->m_hWnd, hDevice->m_hDC);
		}
	} // end if(hDevice)

	return TRUE;
}


/*
void GXPlay_old(McVideoHandle hDevice)
{
	if(CheckOverWindow(hDevice) == TRUE)
		return TRUE;

	// start position to frame buffer..
	pDeviceStartPos = hDevice->m_pStartRGB16Bufgx;
	
	SizeInfo.YUV_CropWidth	= hDevice->m_dwDisplayWidth;
	SizeInfo.YUV_CropHeight	= hDevice->m_dwDisplayHeight;
	SizeInfo.YUV_yStep		= hDevice->m_dwDisplayWidth;

	SizeInfo.RGB_xStep = hDevice->m_gxProperty.cbxPitch;
	SizeInfo.RGB_yStep = hDevice->m_gxProperty.cbyPitch;

	SizeInfo.YUV_CropWidth	= hDevice->m_dwWidth;
	SizeInfo.YUV_CropHeight	= hDevice->m_dwHeight;
	SizeInfo.YUV_yStep		= hDevice->m_dwWidth;

	if(hDevice->m_dwDisplayWidth > hDevice->m_dwWidth)
	{		
		if(hDevice->m_dwDisplayHeight > hDevice->m_dwHeight)
		{
			nWidth = hDevice->m_dwWidth;
			nHeight = hDevice->m_dwHeight;

			// to update window later, get upper RECT, left RECT, right RECT, bottom RECT
			SetRect(&rcUpdateTop,0,0,hDevice->m_dwDisplayWidth,(hDevice->m_dwDisplayHeight-nHeight)/2);
			SetRect(&rcUpdateBottom,0,nHeight+(hDevice->m_dwDisplayHeight-nHeight)/2,hDevice->m_dwDisplayWidth,hDevice->m_dwDisplayHeight);
			SetRect(&rcUpdateLeft,0,0,(hDevice->m_dwDisplayWidth-nWidth)/2,hDevice->m_dwDisplayHeight);
			SetRect(&rcUpdateRight,nWidth+(hDevice->m_dwDisplayWidth-nWidth)/2,0,hDevice->m_dwDisplayWidth,hDevice->m_dwDisplayHeight);

			nMovePos = SizeInfo.RGB_xStep * (hDevice->m_nDisplayPostionXY.x+(hDevice->m_dwDisplayWidth-hDevice->m_dwWidth)/2)
						+ SizeInfo.RGB_yStep * (hDevice->m_nDisplayPostionXY.y+(hDevice->m_dwDisplayHeight-hDevice->m_dwHeight)/2);
			pDeviceStartPos += nMovePos;
			
			YUV420_To_RGB16_S ((unsigned short *)pDeviceStartPos, &hDevice->m_pYUV, &SizeInfo);
			
		}
		else
		{
			if(hDevice->m_dwDisplayHeight>hDevice->m_dwHeight/2)
			{
				nWidth=hDevice->m_dwWidth/2;
				nHeight=hDevice->m_dwHeight/2;

				SetRect(&rcUpdateTop,0,0,hDevice->m_dwDisplayWidth,(hDevice->m_dwDisplayHeight-nHeight)/2);
				SetRect(&rcUpdateBottom,0,nHeight+(hDevice->m_dwDisplayHeight-nHeight)/2,hDevice->m_dwDisplayWidth,hDevice->m_dwDisplayHeight);
				SetRect(&rcUpdateLeft,0,0,(hDevice->m_dwDisplayWidth-nWidth)/2,hDevice->m_dwDisplayHeight);
				SetRect(&rcUpdateRight,nWidth+(hDevice->m_dwDisplayWidth-nWidth)/2,0,hDevice->m_dwDisplayWidth,hDevice->m_dwDisplayHeight);

				nMovePos=SizeInfo.RGB_xStep*(hDevice->m_nDisplayPostionXY.x+(hDevice->m_dwDisplayWidth-nWidth)/2)+SizeInfo.RGB_yStep*(hDevice->m_nDisplayPostionXY.y+(hDevice->m_dwDisplayHeight-nHeight)/2);
				pDeviceStartPos+=nMovePos;
				YUV420_To_RGB16_H ((unsigned short *)pDeviceStartPos, &hDevice->m_pYUV, &SizeInfo);
			}
			else
			{
				if(hDevice->m_dwDisplayHeight>hDevice->m_dwHeight/4)
				{
					nWidth=hDevice->m_dwWidth/4;
					nHeight=hDevice->m_dwHeight/4;

					SetRect(&rcUpdateTop,0,0,hDevice->m_dwDisplayWidth,(hDevice->m_dwDisplayHeight-nHeight)/2);
					SetRect(&rcUpdateBottom,0,nHeight+(hDevice->m_dwDisplayHeight-nHeight)/2,hDevice->m_dwDisplayWidth,hDevice->m_dwDisplayHeight);
					SetRect(&rcUpdateLeft,0,0,(hDevice->m_dwDisplayWidth-nWidth)/2,hDevice->m_dwDisplayHeight);
					SetRect(&rcUpdateRight,nWidth+(hDevice->m_dwDisplayWidth-nWidth)/2,0,hDevice->m_dwDisplayWidth,hDevice->m_dwDisplayHeight);

					nMovePos=SizeInfo.RGB_xStep*(hDevice->m_nDisplayPostionXY.x+(hDevice->m_dwDisplayWidth-nWidth)/2)+SizeInfo.RGB_yStep*(hDevice->m_nDisplayPostionXY.y+(hDevice->m_dwDisplayHeight-nHeight)/2);
					pDeviceStartPos+=nMovePos;
					YUV420_To_RGB16_Q ((unsigned short *)pDeviceStartPos, &hDevice->m_pYUV, &SizeInfo);
				}
				else
					return TRUE;
			}
		}
	}
	else
	{
		if(hDevice->m_dwDisplayWidth>hDevice->m_dwWidth/2)
		{
			if(hDevice->m_dwDisplayHeight>hDevice->m_dwHeight)
			{
				nWidth=hDevice->m_dwWidth/2;
				nHeight=hDevice->m_dwHeight/2;

				SetRect(&rcUpdateTop,0,0,hDevice->m_dwDisplayWidth,(hDevice->m_dwDisplayHeight-nHeight)/2);
				SetRect(&rcUpdateBottom,0,nHeight+(hDevice->m_dwDisplayHeight-nHeight)/2,hDevice->m_dwDisplayWidth,hDevice->m_dwDisplayHeight);
				SetRect(&rcUpdateLeft,0,0,(hDevice->m_dwDisplayWidth-nWidth)/2,hDevice->m_dwDisplayHeight);
				SetRect(&rcUpdateRight,nWidth+(hDevice->m_dwDisplayWidth-nWidth)/2,0,hDevice->m_dwDisplayWidth,hDevice->m_dwDisplayHeight);

				nMovePos=SizeInfo.RGB_xStep*(hDevice->m_nDisplayPostionXY.x+(hDevice->m_dwDisplayWidth-nWidth)/2)+SizeInfo.RGB_yStep*(hDevice->m_nDisplayPostionXY.y+(hDevice->m_dwDisplayHeight-nHeight)/2);
				pDeviceStartPos+=nMovePos;
				YUV420_To_RGB16_H ((unsigned short *)pDeviceStartPos, &hDevice->m_pYUV, &SizeInfo);
			}
			else
			{
				if(hDevice->m_dwDisplayHeight>hDevice->m_dwHeight/2)
				{
					nWidth=hDevice->m_dwWidth/2;
					nHeight=hDevice->m_dwHeight/2;

					SetRect(&rcUpdateTop,0,0,hDevice->m_dwDisplayWidth,(hDevice->m_dwDisplayHeight-nHeight)/2);
					SetRect(&rcUpdateBottom,0,nHeight+(hDevice->m_dwDisplayHeight-nHeight)/2,hDevice->m_dwDisplayWidth,hDevice->m_dwDisplayHeight);
					SetRect(&rcUpdateLeft,0,0,(hDevice->m_dwDisplayWidth-nWidth)/2,hDevice->m_dwDisplayHeight);
					SetRect(&rcUpdateRight,nWidth+(hDevice->m_dwDisplayWidth-nWidth)/2,0,hDevice->m_dwDisplayWidth,hDevice->m_dwDisplayHeight);

					nMovePos=SizeInfo.RGB_xStep*(hDevice->m_nDisplayPostionXY.x+(hDevice->m_dwDisplayWidth-nWidth)/2)+SizeInfo.RGB_yStep*(hDevice->m_nDisplayPostionXY.y+(hDevice->m_dwDisplayHeight-nHeight)/2);
					pDeviceStartPos+=nMovePos;
					YUV420_To_RGB16_H ((unsigned short *)pDeviceStartPos, &hDevice->m_pYUV, &SizeInfo);
				}
				else
				{
					if(hDevice->m_dwDisplayHeight>hDevice->m_dwHeight/4)
					{
						nWidth=hDevice->m_dwWidth/4;
						nHeight=hDevice->m_dwHeight/4;

						SetRect(&rcUpdateTop,0,0,hDevice->m_dwDisplayWidth,(hDevice->m_dwDisplayHeight-nHeight)/2);
						SetRect(&rcUpdateBottom,0,nHeight+(hDevice->m_dwDisplayHeight-nHeight)/2,hDevice->m_dwDisplayWidth,hDevice->m_dwDisplayHeight);
						SetRect(&rcUpdateLeft,0,0,(hDevice->m_dwDisplayWidth-nWidth)/2,hDevice->m_dwDisplayHeight);
						SetRect(&rcUpdateRight,nWidth+(hDevice->m_dwDisplayWidth-nWidth)/2,0,hDevice->m_dwDisplayWidth,hDevice->m_dwDisplayHeight);

						nMovePos=SizeInfo.RGB_xStep*(hDevice->m_nDisplayPostionXY.x+(hDevice->m_dwDisplayWidth-nWidth)/2)+SizeInfo.RGB_yStep*(hDevice->m_nDisplayPostionXY.y+(hDevice->m_dwDisplayHeight-nHeight)/2);
						pDeviceStartPos+=nMovePos;
						YUV420_To_RGB16_Q ((unsigned short *)pDeviceStartPos, &hDevice->m_pYUV, &SizeInfo);
					}
					else
						return TRUE;
				}
			}
		}
	}
	// full screen mode..
	else 
	{
		pDeviceStartPos = hDevice->m_pStartRGB16Bufgx;
		SizeInfo.RGB_xStep = hDevice->m_gxProperty.cbyPitch;
		SizeInfo.RGB_yStep = -hDevice->m_gxProperty.cbxPitch;

		SizeInfo.YUV_CropWidth	 = hDevice->m_dwWidth;
		SizeInfo.YUV_CropHeight = hDevice->m_dwHeight;
		SizeInfo.YUV_yStep		 = hDevice->m_dwWidth;


		if((unsigned int)hDevice->m_dwWidth > hDevice->m_gxProperty.cyHeight)
		{
			if((unsigned int)hDevice->m_dwWidth/2>hDevice->m_gxProperty.cyHeight)
			{
				if((unsigned int)hDevice->m_dwWidth/4>hDevice->m_gxProperty.cyHeight)
					return TRUE;
				else
				{
					if((unsigned int)hDevice->m_dwHeight/4>hDevice->m_gxProperty.cxWidth)
							return TRUE;
					else
					{
						nWidth=hDevice->m_dwWidth/2;
						nHeight=hDevice->m_dwHeight/2;

						SetRect(&rcUpdateTop,0,0,MAX_HEIGHT,(MAX_WIDTH-nWidth)/2);
						SetRect(&rcUpdateBottom,0,nWidth+(MAX_WIDTH-nWidth)/2,MAX_HEIGHT,MAX_WIDTH);
						SetRect(&rcUpdateLeft,0,0,(MAX_HEIGHT-nHeight)/2,MAX_WIDTH);
						SetRect(&rcUpdateRight,nHeight+(MAX_HEIGHT-nHeight)/2,0,MAX_HEIGHT,MAX_WIDTH);

						pDeviceStartPos += (long)(hDevice->m_gxProperty.cbxPitch*(hDevice->m_gxProperty.cxWidth-1));  // 239로 했을때 위쪽에 1픽셀 밀린다.
						YUV420_To_RGB16_H ((unsigned short *)pDeviceStartPos, &hDevice->m_pYUV, &SizeInfo);
					}
				}
			}
			else
			{
				if((unsigned int)hDevice->m_dwHeight/2>hDevice->m_gxProperty.cxWidth)
				{
					if((unsigned int)hDevice->m_dwHeight/4>hDevice->m_gxProperty.cxWidth)
					{
						return TRUE;
					}
					else
					{
						SizeInfo.YUV_CropWidth	 = hDevice->m_dwWidth;
						SizeInfo.YUV_CropHeight = hDevice->m_dwHeight;
						SizeInfo.YUV_yStep		 = hDevice->m_dwWidth;
						pDeviceStartPos += (long)(hDevice->m_gxProperty.cbxPitch*(hDevice->m_gxProperty.cxWidth-1));
						nMovePos=SizeInfo.RGB_yStep *((hDevice->m_gxProperty.cxWidth-hDevice->m_dwHeight/2)/2)+SizeInfo.RGB_xStep*((hDevice->m_gxProperty.cyHeight-hDevice->m_dwWidth/2)/2);
						pDeviceStartPos+=nMovePos;

						nWidth=hDevice->m_dwWidth/2;
						nHeight=hDevice->m_dwHeight/2;

						SetRect(&rcUpdateTop,0,0,MAX_HEIGHT,(MAX_WIDTH-nWidth)/2);
						SetRect(&rcUpdateBottom,0,nWidth+(MAX_WIDTH-nWidth)/2,MAX_HEIGHT,MAX_WIDTH);
						SetRect(&rcUpdateLeft,0,0,(MAX_HEIGHT-nHeight)/2,MAX_WIDTH);
						SetRect(&rcUpdateRight,nHeight+(MAX_HEIGHT-nHeight)/2,0,MAX_HEIGHT,MAX_WIDTH);

						YUV420_To_RGB16_H ((unsigned short *)pDeviceStartPos, &hDevice->m_pYUV, &SizeInfo);
					}
				}
				else
				{
					pDeviceStartPos += (long)(hDevice->m_gxProperty.cbxPitch*239); 
					nMovePos=SizeInfo.RGB_yStep *((hDevice->m_gxProperty.cxWidth-hDevice->m_dwHeight/4)/2)+SizeInfo.RGB_xStep*((hDevice->m_gxProperty.cyHeight-hDevice->m_dwWidth/4)/2);
					pDeviceStartPos+=nMovePos;

					nWidth=hDevice->m_dwWidth/4;
					nHeight=hDevice->m_dwHeight/4;

					SetRect(&rcUpdateTop,0,0,MAX_HEIGHT,(MAX_WIDTH-nWidth)/2);
					SetRect(&rcUpdateBottom,0,nWidth+(MAX_WIDTH-nWidth)/2,MAX_HEIGHT,MAX_WIDTH);
					SetRect(&rcUpdateLeft,0,0,(MAX_HEIGHT-nHeight)/2,MAX_WIDTH);
					SetRect(&rcUpdateRight,nHeight+(MAX_HEIGHT-nHeight)/2,0,MAX_HEIGHT,MAX_WIDTH);

					YUV420_To_RGB16_Q ((unsigned short *)pDeviceStartPos, &hDevice->m_pYUV, &SizeInfo);
				}
			}
		}
		else
		{
			if((unsigned int)hDevice->m_dwHeight>hDevice->m_gxProperty.cxWidth)
			{
				if((unsigned int)hDevice->m_dwHeight/2>hDevice->m_gxProperty.cxWidth)
				{
					if((unsigned int)hDevice->m_dwHeight/4>hDevice->m_gxProperty.cxWidth)
					{
						return TRUE;	
					}
					else
					{
						pDeviceStartPos += (long)(hDevice->m_gxProperty.cbxPitch*(hDevice->m_gxProperty.cxWidth-1));  // 239로 했을때 위쪽에 1픽셀 밀린다.
						nMovePos=SizeInfo.RGB_yStep *((hDevice->m_gxProperty.cxWidth-hDevice->m_dwHeight/4)/2)+SizeInfo.RGB_xStep*((hDevice->m_gxProperty.cyHeight-hDevice->m_dwWidth/4)/2);
						pDeviceStartPos+=nMovePos;

						nWidth=hDevice->m_dwWidth/4;
						nHeight=hDevice->m_dwHeight/4;

						SetRect(&rcUpdateTop,0,0,MAX_HEIGHT,(MAX_WIDTH-nWidth)/2);
						SetRect(&rcUpdateBottom,0,nWidth+(MAX_WIDTH-nWidth)/2,MAX_HEIGHT,MAX_WIDTH);
						SetRect(&rcUpdateLeft,0,0,(MAX_HEIGHT-nHeight)/2,MAX_WIDTH);
						SetRect(&rcUpdateRight,nHeight+(MAX_HEIGHT-nHeight)/2,0,MAX_HEIGHT,MAX_WIDTH);

						YUV420_To_RGB16_Q ((unsigned short *)pDeviceStartPos, &hDevice->m_pYUV, &SizeInfo);
					}	
				}
				else
				{
					pDeviceStartPos += (long)(hDevice->m_gxProperty.cbxPitch*(hDevice->m_gxProperty.cxWidth-1));  // 239로 했을때 위쪽에 1픽셀 밀린다.
					nMovePos=SizeInfo.RGB_yStep *((hDevice->m_gxProperty.cxWidth-hDevice->m_dwHeight/2)/2)+SizeInfo.RGB_xStep*((hDevice->m_gxProperty.cyHeight-hDevice->m_dwWidth/2)/2);
					pDeviceStartPos+=nMovePos;

					nWidth=hDevice->m_dwWidth/2;
					nHeight=hDevice->m_dwHeight/2;

					SetRect(&rcUpdateTop,0,0,MAX_HEIGHT,(MAX_WIDTH-nWidth)/2);
					SetRect(&rcUpdateBottom,0,nWidth+(MAX_WIDTH-nWidth)/2,MAX_HEIGHT,MAX_WIDTH);
					SetRect(&rcUpdateLeft,0,0,(MAX_HEIGHT-nHeight)/2,MAX_WIDTH);
					SetRect(&rcUpdateRight,nHeight+(MAX_HEIGHT-nHeight)/2,0,MAX_HEIGHT,MAX_WIDTH);

					YUV420_To_RGB16_H ((unsigned short *)pDeviceStartPos, &hDevice->m_pYUV, &SizeInfo);
				}
			}
			else
			{
				if((unsigned int)(hDevice->m_dwWidth*2)<=hDevice->m_gxProperty.cyHeight)
				{

					if((unsigned int)(hDevice->m_dwHeight*2)<=hDevice->m_gxProperty.cyHeight)
					{
						pDeviceStartPos += (long)(hDevice->m_gxProperty.cbxPitch*(hDevice->m_gxProperty.cxWidth-1));
						nMovePos=SizeInfo.RGB_yStep *((hDevice->m_gxProperty.cxWidth-hDevice->m_dwHeight*2)/2)+SizeInfo.RGB_xStep*((hDevice->m_gxProperty.cyHeight-hDevice->m_dwWidth*2)/2);
						pDeviceStartPos+=nMovePos;

						nWidth=hDevice->m_dwWidth*2;
						nHeight=hDevice->m_dwHeight*2;

						SetRect(&rcUpdateTop,0,0,MAX_HEIGHT,(MAX_WIDTH-nWidth)/2);
						SetRect(&rcUpdateBottom,0,nWidth+(MAX_WIDTH-nWidth)/2,MAX_HEIGHT,MAX_WIDTH);
						SetRect(&rcUpdateLeft,0,0,(MAX_HEIGHT-nHeight)/2,MAX_WIDTH);
						SetRect(&rcUpdateRight,nHeight+(MAX_HEIGHT-nHeight)/2,0,MAX_HEIGHT,MAX_WIDTH);

						YUV420_To_RGB16_D ((unsigned short *)pDeviceStartPos, &hDevice->m_pYUV, &SizeInfo);
					}
					else
					{
						pDeviceStartPos += (long)(hDevice->m_gxProperty.cbxPitch*(hDevice->m_gxProperty.cxWidth-1));
						nMovePos=SizeInfo.RGB_yStep *((hDevice->m_gxProperty.cxWidth-hDevice->m_dwHeight)/2)+SizeInfo.RGB_xStep*((hDevice->m_gxProperty.cyHeight-hDevice->m_dwWidth)/2);
						pDeviceStartPos+=nMovePos;

						nWidth=hDevice->m_dwWidth;
						nHeight=hDevice->m_dwHeight;

						SetRect(&rcUpdateTop,0,0,MAX_HEIGHT,(MAX_WIDTH-nWidth)/2);
						SetRect(&rcUpdateBottom,0,nWidth+(MAX_WIDTH-nWidth)/2,MAX_HEIGHT,MAX_WIDTH);
						SetRect(&rcUpdateLeft,0,0,(MAX_HEIGHT-nHeight)/2,MAX_WIDTH);
						SetRect(&rcUpdateRight,nHeight+(MAX_HEIGHT-nHeight)/2,0,MAX_HEIGHT,MAX_WIDTH);

						YUV420_To_RGB16_S ((unsigned short *)pDeviceStartPos, &hDevice->m_pYUV, &SizeInfo);
					}
				}
				else
				{
					pDeviceStartPos += (long)(hDevice->m_gxProperty.cbxPitch*(hDevice->m_gxProperty.cxWidth-1));
					nMovePos=SizeInfo.RGB_yStep *((hDevice->m_gxProperty.cxWidth-hDevice->m_dwHeight)/2)+SizeInfo.RGB_xStep*((hDevice->m_gxProperty.cyHeight-hDevice->m_dwWidth)/2);
					pDeviceStartPos+=nMovePos;

					nWidth=hDevice->m_dwWidth;
					nHeight=hDevice->m_dwHeight;

					SetRect(&rcUpdateTop,0,0,MAX_HEIGHT,(MAX_WIDTH-nWidth)/2);
					SetRect(&rcUpdateBottom,0,nWidth+(MAX_WIDTH-nWidth)/2,MAX_HEIGHT,MAX_WIDTH);
					SetRect(&rcUpdateLeft,0,0,(MAX_HEIGHT-nHeight)/2,MAX_WIDTH);
					SetRect(&rcUpdateRight,nHeight+(MAX_HEIGHT-nHeight)/2,0,MAX_HEIGHT,MAX_WIDTH);

					YUV420_To_RGB16_S ((unsigned short *)pDeviceStartPos, &hDevice->m_pYUV, &SizeInfo);
				}
			}
		}
	}
}
*/


#endif //_WIN32_WCE