/*****************************************************************************
*                                                                            *
*                            DeviceOutput Library							 *
*                                                                            *
*   Copyright (c) 2004 by Mcubeworks, Incoporated. All Rights Reserved.      *
******************************************************************************

    File Name       : DevOutVideoGDI.c
    Author(s)       : Kim, Kun-Tae
    Created         : 11 Jan 2005

    Description     : DeviceOutput API
    Notes           : 

==============================================================================
                      Modification History (Reverse Order)                    
==============================================================================
    Date        Author      Location (variables) / Description
------------------------------------------------------------------------------

------------------------------------------------------------------------------*/

/*
	Bitmap Functions

	|---------------------------|-----------------------------------------------------------------------|
	|	Function				|	Description															|
	|---------------------------|-----------------------------------------------------------------------|
	|	AlphaBlend				|	Displays a bitmap with transparent or semitransparent pixels.		|
	|	BitBlt					|	Performs a bit-block transfer.										|
	|	CreateBitmap			|	Creates a bitmap.													|
	|	CreateBitmapIndirect	|	Creates a bitmap.													|
	|	CreateCompatibleBitmap	|	Creates a bitmap compatible with a device.							|
	|	CreateDIBitmap			|	Creates a device-dependent bitmap (DDB) from a DIB.					|
	|	CreateDIBSection		|	Creates a DIB that applications can write to directly.				|
	|	ExtFloodFill			|	Fills an area of the display surface with the current brush.		|
	|	GetBitmapDimensionEx	|	Gets the dimensions of a bitmap.									|
	|	GetDIBColorTable		|	Retrieves RGB color values from a DIB section bitmap.				|
	|	GetDIBits				|	Copies a bitmap into a buffer.										|
	|	GetPixel				|	Gets the RGB color value of the pixel at a given coordinate.		|
	|	GetStretchBltMode		|	Gets the current stretching mode.									|
	|	GradientFill			|	Fills rectangle and triangle structures.							|
	|	LoadBitmap				|	Loads a bitmap from a module's executable file.						|
	|	MaskBlt					|	Combines the color data in the source and destination bitmaps.		|
	|	PlgBlt					|	Performs a bit-block transfer.										|
	|	SetBitmapDimensionEx	|	Sets the preferred dimensions to a bitmap.							|
	|	SetDIBColorTable		|	Sets RGB values in a DIB.											|
	|	SetDIBits				|	Sets the pixels in a bitmap using color data from a DIB.			|
	|	SetDIBitsToDevice		|	Sets the pixels in a rectangle using color data from a DIB.			|
	|	SetPixel				|	Sets the color for a pixel.											|
	|	SetPixelV				|	Sets a pixel to the best approximation of a color.					|
	|	SetStretchBltMode		|	Sets the bitmap stretching mode.									|
	|	StretchBlt				|	Copies a bitmap and stretches or compresses it.						|
	|	StretchDIBits			|	Copies the color data in a DIB.										|
	|	TransparentBlt			|	Performs a bit-block transfer of color data.						|
	|---------------------------|-----------------------------------------------------------------------|
*/
#include "DevOutVideoBase.h"
#ifndef _WIN32_WCE
#include <vfw.h>
#pragma comment(lib, "vfw32.lib")
#endif

#if defined(_DEBUG)
#pragma comment(lib, "opencv_core440d.lib")
#pragma comment(lib, "opencv_highgui440d.lib")
#pragma comment(lib, "opencv_imgproc440d.lib")
#pragma comment(lib, "opencv_imgcodecs440d.lib")
#pragma comment(lib, "opencv_calib3d440d.lib")
#pragma comment(lib, "opencv_cudaimgproc440d.lib")
#pragma comment(lib, "opencv_cudawarping440d.lib")
#pragma comment(lib, "opencv_cudaarithm440d.lib")
#pragma comment(lib, "opencv_video440d.lib")
#pragma comment(lib, "opencv_videoio440d.lib")
#pragma comment(lib, "opencv_cudacodec440d.lib")
#else
#pragma comment(lib, "opencv_core440.lib")
#pragma comment(lib, "opencv_highgui440.lib")
#pragma comment(lib, "opencv_imgproc440.lib")
#pragma comment(lib, "opencv_imgcodecs440.lib")
#pragma comment(lib, "opencv_calib3d440.lib")
#pragma comment(lib, "opencv_cudaimgproc440.lib")
#pragma comment(lib, "opencv_cudawarping440.lib")
#pragma comment(lib, "opencv_cudaarithm440.lib")
#pragma comment(lib, "opencv_video440.lib")
#pragma comment(lib, "opencv_videoio440.lib")
#pragma comment(lib, "opencv_cudacodec440.lib")
#endif
#if defined(VIDEOOUT_GDI)

BOOL GDIOpen(McVideoOutStructHandle hDevice)
{
	// Video Format
	hDevice->m_pBitmapInfo.biSize = sizeof(BITMAPINFOHEADER);
	hDevice->m_pBitmapInfo.biWidth = hDevice->m_dwWidth;
	
	hDevice->m_pBitmapInfo.biHeight = hDevice->m_dwHeight;
	hDevice->m_pBitmapInfo.biCompression = BI_RGB;
	hDevice->m_pBitmapInfo.biBitCount = hDevice->m_nDevBitCount;
	hDevice->m_pBitmapInfo.biSizeImage = hDevice->m_dwWidth * hDevice->m_dwHeight * (hDevice->m_nDevBitCount>>3);

	hDevice->m_pBitmapInfo.biPlanes = 1;
	hDevice->m_pBitmapInfo.biXPelsPerMeter = 0;
	hDevice->m_pBitmapInfo.biYPelsPerMeter = 0;
	hDevice->m_pBitmapInfo.biClrUsed = 0;
	hDevice->m_pBitmapInfo.biClrImportant = 0;

	// Buffer
	hDevice->m_Buff=NULL;
	hDevice->m_Buff = (BYTE*)MALLOC(hDevice->m_dwWidth * hDevice->m_dwHeight * (hDevice->m_nDevBitCount>>3));
	if(hDevice->m_Buff==NULL)
		return FALSE;

	hDevice->m_BuffBG=NULL;
	hDevice->m_BuffBG = (BYTE*)MALLOC(hDevice->m_dwWidth * hDevice->m_dwHeight * (hDevice->m_nDevBitCount>>3));
	if(hDevice->m_BuffBG==NULL)
		return FALSE;

	memset(hDevice->m_Buff, 0, hDevice->m_dwWidth * hDevice->m_dwHeight * (hDevice->m_nDevBitCount>>3));
	memset(hDevice->m_BuffBG, 0, hDevice->m_dwWidth * hDevice->m_dwHeight * (hDevice->m_nDevBitCount>>3));

	GDIClear(hDevice);

	TRACE("[DevOutVideo]	McVideoOutOpen - GDI\n");

	return	TRUE;
}

void GDIClose(McVideoOutStructHandle hDevice)
{
	if (hDevice->m_Buff)
		FREE(hDevice->m_Buff);
	if (hDevice->m_BuffBG)
		FREE(hDevice->m_BuffBG);
	//hDevice->m_hDC->ReleaseDC(hDevice->m_hWnd);
}

BOOL GDITransToBuffer(McVideoOutStructHandle hDevice, FrRawVideo* pMedia)
{
	//if (hDevice->m_nDevBitCount == 32)
	//	IYUV_to_BGRA(hDevice->m_Buff, hDevice->m_dwWidth, hDevice->m_dwHeight, hDevice->m_dwWidth*4,
	//			pMedia->pY, pMedia->pU, pMedia->pV, pMedia->dwPitch);
	//else if (hDevice->m_nDevBitCount == 24)
	//	IYUV_to_BGR24(hDevice->m_Buff, hDevice->m_dwWidth, hDevice->m_dwHeight, hDevice->m_dwWidth*3,
	//			pMedia->pY, pMedia->pU, pMedia->pV, pMedia->dwPitch);
	if (hDevice->m_nDevBitCount == 16) {


		std::vector<cv::Mat> vecImage, vecOutput(3);

		int lumaWidth = hDevice->m_dwWidth;
		int lumaHeight = hDevice->m_dwHeight;
		int chromaWidth = lumaWidth >> 1;
		int chromaHeight = lumaHeight >> 1;
		int lumaPitch = pMedia->dwPitch;
		int chromaPitch = lumaPitch >> 1;
		int dpYUVPitch = lumaPitch;

		unsigned char* yDeviceptr = (unsigned char*)pMedia->pY;
		unsigned char* uDeviceptr = pMedia->pU;
		unsigned char* vDeviceptr = pMedia->pV;

		/*unsigned char* yDeviceptr = (unsigned char*)tmpData.pV;
		unsigned char* uDeviceptr = (unsigned char*)tmpData.pU;
		unsigned char* vDeviceptr = (unsigned char*)tmpData.pV;*/

		cv::Mat gpuY(lumaHeight, lumaWidth, CV_8UC1, yDeviceptr, lumaPitch);
		cv::Mat gpuU(chromaHeight, chromaWidth, CV_8UC1, uDeviceptr, chromaPitch);
		cv::Mat gpuV(chromaHeight, chromaWidth, CV_8UC1, vDeviceptr, chromaPitch);

		vecImage.push_back(gpuY);
		vecImage.push_back(gpuU);
		vecImage.push_back(gpuV);

		vecImage[0].copyTo(vecOutput[0]);
		vecImage[1].copyTo(vecOutput[1]);
		vecImage[2].copyTo(vecOutput[2]);

		cv::resize(vecOutput[1], vecOutput[1], cv::Size(vecOutput[1].cols * 2, vecOutput[1].rows * 2), cv::InterpolationFlags::INTER_CUBIC);
		cv::resize(vecOutput[2], vecOutput[2], cv::Size(vecOutput[2].cols * 2, vecOutput[2].rows * 2), cv::InterpolationFlags::INTER_CUBIC);

		cv::Mat gResult;
		cv::merge(vecOutput, gResult);
		cv::cvtColor(gResult, gResult, cv::ColorConversionCodes::COLOR_YUV2BGR); //COLOR_YUV2BGR      = 84, //COLOR_YUV2RGB

		memcpy(hDevice->m_Buff, gResult.data, gResult.cols * gResult.rows * 3);
	}
	else if (hDevice->m_nDevBitCount == 24) {
		memcpy(hDevice->m_Buff, pMedia->pY, hDevice->m_dwHeight * hDevice->m_dwWidth * 3);
	}
	return	TRUE;
}

BOOL GDIClear(McVideoOutStructHandle hDevice)
{
	RECT	rc;
	int		iRet;
	DWORD	dwError;

	hDevice->m_hDC = GetDC(hDevice->m_hWnd);

	// Window
	GetWindowRect(hDevice->m_hWnd, &rc);
	hDevice->m_dwWndWidth = rc.right - rc.left+GetSystemMetrics(SM_CXBORDER)*2;
	hDevice->m_dwWndHeight = rc.bottom - rc.top+GetSystemMetrics(SM_CYBORDER)*2;

	iRet = StretchDIBits(hDevice->m_hDC, 0, 0, hDevice->m_dwWndWidth, hDevice->m_dwWndHeight, 
					   0, hDevice->m_dwHeight, hDevice->m_dwWidth, -(int)hDevice->m_dwHeight,
					   hDevice->m_BuffBG, (BITMAPINFO*)&hDevice->m_pBitmapInfo,
					   DIB_RGB_COLORS, SRCCOPY);

	if (iRet == GDI_ERROR)
	{
		dwError = GetLastError();
	}

	::ReleaseDC(hDevice->m_hWnd, hDevice->m_hDC);

	return	TRUE;
}


BOOL GDIPlay(McVideoOutStructHandle hDevice)
{
	RECT	rc, rcDest;
	DWORD	dwWndWidth, dwWndHeight;
	int		iRet;

	hDevice->m_hDC = GetDC(hDevice->m_hWnd);

	// Window
	GetWindowRect(hDevice->m_hWnd, &rc);
	dwWndWidth = rc.right - rc.left;
	dwWndHeight = rc.bottom - rc.top;

	if (hDevice->m_nWndX != rc.left)
	{
		hDevice->m_nWndX = rc.left;
		hDevice->m_bDrawBG = TRUE;
	}
	if (hDevice->m_nWndY != rc.top)
	{
		hDevice->m_nWndY = rc.top;
		hDevice->m_bDrawBG = TRUE;
	}
	if (hDevice->m_dwWndWidth != dwWndWidth)
	{
		hDevice->m_dwWndWidth = dwWndWidth;
		hDevice->m_bDrawBG = TRUE;
	}
	if (hDevice->m_dwWndHeight != dwWndHeight)
	{
		hDevice->m_dwWndHeight = dwWndHeight;
		hDevice->m_bDrawBG = TRUE;
	}

	// Display
	GetDisplayRectangle(hDevice, &rcDest);

	HDC hMainCtrl, hMemDC;	
//	HBITMAP hOldMemDC;
	hMainCtrl	= CreateCompatibleDC(hDevice->m_hDC);
	hMemDC		= CreateCompatibleDC(hDevice->m_hDC);

	HBITMAP	hBmpBuffer;
	HBITMAP hOldMainCtrl;
			
	hBmpBuffer	= CreateCompatibleBitmap(hDevice->m_hDC, dwWndWidth, dwWndHeight); 
	hOldMainCtrl = (HBITMAP)SelectObject(hMainCtrl, hBmpBuffer);
	int			nRet = TRUE;
		
	SetStretchBltMode(hMainCtrl, HALFTONE);
	iRet = StretchDIBits(hMainCtrl, 0, 0, dwWndWidth, dwWndHeight, 
					   0, hDevice->m_dwHeight, hDevice->m_dwWidth, -(int)hDevice->m_dwHeight,
					   hDevice->m_BuffBG, (BITMAPINFO*)&hDevice->m_pBitmapInfo,
					   DIB_RGB_COLORS, SRCCOPY);

	iRet = StretchDIBits(hMainCtrl, rcDest.left, rcDest.top, rcDest.right-rcDest.left, rcDest.bottom-rcDest.top, 
					   0, hDevice->m_dwHeight, hDevice->m_dwWidth, -(int)hDevice->m_dwHeight,
					   hDevice->m_Buff, (BITMAPINFO*)&hDevice->m_pBitmapInfo,
					   DIB_RGB_COLORS, SRCCOPY);
	BitBlt(hDevice->m_hDC, 0, 0, dwWndWidth, dwWndHeight, hMainCtrl, 0, 0, SRCCOPY);
	SelectObject(hMainCtrl, hOldMainCtrl);
	DeleteObject(hBmpBuffer);

	DeleteDC(hMemDC);
	DeleteDC(hMainCtrl);

	::ReleaseDC(hDevice->m_hWnd,hDevice->m_hDC);

	return TRUE;
}

BOOL GDIPlayString(McVideoOutStructHandle hDevice)
{
	return	TRUE;
}

#endif // VIDEOOUT_DDRAW || VIDEOOUT_D3D9DRAW