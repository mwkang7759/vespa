/*****************************************************************************
*                                                                            *
*                            DeviceOutput Library							 *
*                                                                            *
*   Copyright (c) 2018 - by DreamToBe, Incoporated. All Rights Reserved.      *
******************************************************************************

    File Name       : DevOutVideoSDL.cpp
    Author(s)       : CHANG, Joonho
    Created         : 9 May 2018

    Description     : DeviceOutput API for SDL
    Notes           :

==============================================================================
                      Modification History (Reverse Order)
==============================================================================
    Date        Author      Location (variables) / Description
------------------------------------------------------------------------------

------------------------------------------------------------------------------*/
#include "DevOutVideoSDL.h"
#include "DeviceOutAPI.h"
#include "FrUtil.h"

#ifdef VIDEOOUT_SDL

#ifdef	WIN32
# pragma warning(disable : 4309)
# if 0 && _MSC_VER >= 1600 // Visual Studio 2010 (vc100)
# else
#  pragma comment(lib, "SDL2.lib")
#  pragma comment(lib, "opengl32.lib")
# endif
#endif

#define	FADE_COUNT	15

//#define	USE_EXTERNAL_WINDOW
#define	NORMAL_DISPLAY

#define AssertNoGLError(x) \
       { \
          GLenum glError = x->glGetError(); \
          if(glError != GL_NO_ERROR) { \
            TRACEX(DTB_LOG_ERROR, "glGetError() = %i (0x%.8x) at line %i\n", glError, glError, __LINE__); \
          } \
        }

#define		PI		3.1415926535897932384626433832795028

#ifndef GL_EXT_sRGB_write_control
# define	USE_HW_GAMMA_CORRECTION
#endif

bool SDLOutVideo::INIT(int32_t src_width, int32_t src_height, int32_t target_width, int32_t target_height, int bitCount) {
	
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		return FALSE;
	}

	// create window
	m_pScreen = SDL_CreateWindowFrom(hWnd_);
	
	// create render
	m_pRenderer = SDL_CreateRenderer(m_pScreen, -1, SDL_RENDERER_ACCELERATED);

	SDL_GetRendererInfo(m_pRenderer, &info);

	if (bitCount == 16) {
		m_pTexture = SDL_CreateTexture(m_pRenderer, SDL_PIXELFORMAT_YV12, SDL_TEXTUREACCESS_STREAMING, src_width, src_height);
	}
	else {
		m_pTexture = SDL_CreateTexture(m_pRenderer, SDL_PIXELFORMAT_BGR24, SDL_TEXTUREACCESS_STREAMING, src_width, src_height);
	}

	width_ = src_width;

	int srcWidth = src_width;
	int srcHeight = src_height;
	srcRect = { 0, 0, src_width, src_height };
	int dstWidth = 0;
	int dstHeight = 0;
	SDL_GetWindowSize(m_pScreen, &dstWidth, &dstHeight);
	
	// keep ratio
	double srcRatio = srcWidth * 1.0 / srcHeight;
	double dstRatio = dstWidth * 1.0 / dstHeight;
	if (srcRatio > dstRatio)
	{
		dstRect.x = 0;
		dstRect.y = (dstHeight - dstWidth / srcRatio) / 2;
		dstRect.w = dstWidth;
		dstRect.h = dstWidth / srcRatio;
	}
	else
	{
		dstRect.x = (dstWidth - dstHeight * srcRatio) / 2;
		dstRect.y = 0;
		dstRect.w = dstHeight * srcRatio;
		dstRect.h = dstHeight;
	}

	return true;
}

bool SDLOutVideo::CLOSE()
{
	if (m_pRenderer)
	{
		SDL_DestroyRenderer(m_pRenderer);
		m_pRenderer = nullptr;
	}
	if (m_pTexture)
	{
		SDL_DestroyTexture(m_pTexture);
		m_pTexture = nullptr;
	}
	if (m_pScreen)
	{
		SDL_DestroyWindow(m_pScreen);
		m_pScreen = nullptr;
	}

	SDL_Quit();

	return true;
}

bool SDLOutVideo::PutOneFrame(FrRawVideo* pMedia)
{
	int ret;
	if (pMedia->eColorFormat == VideoFmtBGR24)
		ret = SDL_UpdateTexture(m_pTexture, nullptr, pMedia->pY, width_ * 3);
	else
		ret = SDL_UpdateTexture(m_pTexture, nullptr, pMedia->pY, width_);
	if (ret != 0)
	{
		LOG_I("SDL_UpdateTexture(): %s", SDL_GetError());
	}

	//SDL_UpdateTexture(sdlTexture, NULL, imgSrc1.data, imgSrc1.cols * 3);

	SDL_RenderClear(m_pRenderer);
	SDL_RenderCopy(m_pRenderer, m_pTexture, &srcRect, &dstRect);
	//SDL_RenderPresent(m_pRenderer);
	

	return true;
}

void SDLOutVideo::PlayOutOneFrame()
{
	SDL_RenderPresent(m_pRenderer);
	// Switch to the back framebuffer
	/*if (queue_num_ == 0)
		SDL_GL_SwapWindow(sdl_->windows[0]);
	else if (RenderOneFrame() == true)
		SDL_GL_SwapWindow(sdl_->windows[0]);*/
}


/////////////////////////////////////////////////////////
//		Device Functions
/////////////////////////////////////////////////////////
LRSLT McVideoOutOpen(McVideoOutHandle* phDevice, FrVideoInfo* hInfo, HANDLE hWnd, BOOL bFull)
{
	McVideoOutStructHandle	hDevice;

	if (hInfo==NULL)
	{
		TRACE("McVideoOutOpen - null pointer input");
		return COMMON_ERR_NULLPOINTER;
	}

	TRACE("[DevOutVideo] McVideoOutOpen - SDL - width=%d height=%d bit=%d", hInfo->dwWidth, hInfo->dwHeight, hInfo->dwBitCount);

	hDevice = (McVideoOutStructHandle)MALLOCZ(sizeof(McVideoOutStruct));
	if (hDevice)
	{
		/*if (hInfo->m_bVideoRenderBuffer)
		{
			TRACE("[DevOutVideo] McVideoOutOpen - render buffer enabled");
			hDevice->m_bBufferEnable = TRUE;
			*phDevice = hDevice;
		}
		else*/
		{
			hDevice->m_hDev = new SDLOutVideo(hWnd, NULL);
			if (hDevice->m_hDev == NULL)
			{
				FREE(hDevice);
				TRACE("[DevOutVideo] McVideoOutOpen - memory allocation error");

				return COMMON_ERR_MEM;
			}

			hDevice->m_dwFourCC = hInfo->dwFourCC;
			hDevice->m_dwBitCount = hInfo->dwBitCount;
			hDevice->m_dwWidth	= hInfo->dwWidth;
			hDevice->m_dwHeight	= hInfo->dwHeight;

			hDevice->m_pImgBuffer = (BYTE*)MALLOCZ(hDevice->m_dwWidth * hDevice->m_dwHeight * 3);

			
#ifdef	WIN32
			hDevice->m_hWnd = hWnd;
			GetWindowRect((HWND)hDevice->m_hWnd, &hDevice->m_rcWnd);
			hDevice->m_dwOutWidth = hDevice->m_rcWnd.right - hDevice->m_rcWnd.left;
			hDevice->m_dwOutHeight = hDevice->m_rcWnd.bottom - hDevice->m_rcWnd.top;
#else
			hDevice->m_dwOutWidth = hInfo->m_dwOutWidth;
			hDevice->m_dwOutHeight = hInfo->m_dwOutHeight;
#endif

			// Init SDL resources
			if (hDevice->m_hDev->INIT(hInfo->dwWidth, hInfo->dwHeight, hDevice->m_dwOutWidth, hDevice->m_dwOutHeight, hDevice->m_dwBitCount) == false)
			{
				TRACE("[DevOutVideo] McVideoOutOpen - INIT failed");
				delete hDevice->m_hDev;
				FREE(hDevice);
				return COMMON_ERR_VIDEODEVOUT;
			}

			// Set the size of Video display
			//hDevice->m_hDev->DidChangeView2(hDevice->m_dwOutWidth, hDevice->m_dwOutHeight, true);
			//hDevice->m_hDev->DidChangeViewPort(hDevice->m_fViewCenterHor, hDevice->m_fViewCenterVer, hDevice->m_fViewWidth, hDevice->m_fViewHeight);


			//Create rendering buffer :: default queue size 3
			//hDevice->m_hDev->InitQueue(3, hInfo->m_dwWidth, hInfo->m_dwHeight);

			TRACE("[DevOutVideo] McVideoOutOpen Success..");

			*phDevice = (McVideoOutHandle)hDevice;

#ifdef	VIDEO_DUMP
			hDevice->m_hFile = McOpenFile("video.raw", FILE_CREATE|FILE_WRITE);
#endif
		}
	}
	else
		return	COMMON_ERR_MEM;

	return	FR_OK;
}

void McVideoOutClose(McVideoOutHandle hHandle)
{
	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;

	TRACE("[DevOutVideo]	McVideoOutClose begin, hDevice (x0%x)", (unsigned int)hDevice);
	if (hDevice)
	{
		if (hDevice->m_bBufferEnable == FALSE)
		{
			if (hDevice->m_hDev)
			{
				//hDevice->m_hDev->EmptyQueue();
				delete hDevice->m_hDev;
			}
		}
		if (hDevice->m_pImgBuffer)
			FREE(hDevice->m_pImgBuffer);
		FREE(hDevice);

        TRACE("[DevOutVideo]	McVideoOutClose End, hDevice (0x%x)", (unsigned int)hDevice);
	}
}

void McVideoOutStop(McVideoOutHandle hHandle)
{
	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;

	TRACE("[DevOutVideo]	McVideoOutStop begin, hDevice (x0%x)", (unsigned int)hDevice);
	if (hDevice)
	{
        TRACE("[DevOutVideo]	McVideoOutStop End, hDevice (0x%x)", (unsigned int)hDevice);
	}
}

BOOL McVideoOutSetWndHandle(McVideoOutHandle hHandle, HANDLE hWnd)
{
	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;

	if (!hDevice)
		return	FALSE;

	hDevice->m_hWnd = hWnd;

	return	TRUE;
}

BOOL McVideoOutSetTextWndHandle(McVideoOutHandle hHandle, HANDLE hTextWnd)
{
	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;

	if (!hDevice)
		return	FALSE;

	return	TRUE;
}

static BYTE *pNV12Buffer;

BOOL McVideoOutConversion(McVideoOutHandle hHandle, FrRawVideo* pMedia)
{
	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;

	if (hDevice==NULL)
		return FALSE;

	//TRACE("[DevOutVideo] McVideoOutConversion Begin..width(%d), pitch(%d)", hDevice->m_dwWidth, pMedia->m_dwPitch);

	if (!hDevice || pMedia == NULL)
		return	FALSE;

	if (hDevice->m_bBufferEnable)
	{
		DWORD32		dwNew = (hDevice->m_dwWriteBuffer + 1)%VIDEO_MEDIA_BUFFER;

		if (dwNew == hDevice->m_dwReadBuffer)
		{
			TRACE("[DevOutVideo] McVideoOutConversion - buffer is full");
		}
		else
		{
			TRACE("[DevOutVideo] McVideoOutConversion - WriteBuffer %d", hDevice->m_dwWriteBuffer);
			memcpy(&hDevice->m_Buffer[hDevice->m_dwWriteBuffer], pMedia, sizeof(FrRawVideo));
			hDevice->m_dwWriteBuffer = dwNew;
		}
	}
	else
	{
		//Copy into rendering buffer
		if (hDevice->m_hDev) {
			if (pMedia->eColorFormat == VideoFmtYV12) {
				fr::cvYUV2BGR24(hDevice->m_dwWidth, hDevice->m_dwHeight, pMedia->dwPitch, pMedia->pY, pMedia->pU, pMedia->pV, hDevice->m_pImgBuffer);
				pMedia->pY = hDevice->m_pImgBuffer;
			}
			else if (pMedia->eColorFormat == VideoFmtBGR24) {
				TRACE("[DevOutVideo] McVideoOutConversion - BGR24..bypass data");
			}
			else {
				BYTE* pos = hDevice->m_pImgBuffer;
				memcpy(pos, pMedia->pY, pMedia->dwPitch * hDevice->m_dwHeight);
				pos += pMedia->dwPitch * hDevice->m_dwHeight;

				/*memcpy(pos, pMedia->pU, pMedia->dwPitch * hDevice->m_dwHeight / 4);
				pos += pMedia->dwPitch * hDevice->m_dwHeight / 4;
				memcpy(pos, pMedia->pV, pMedia->dwPitch * hDevice->m_dwHeight / 4);*/
				memcpy(pos, pMedia->pV, pMedia->dwPitch * hDevice->m_dwHeight / 4);
				pos += pMedia->dwPitch * hDevice->m_dwHeight / 4;
				memcpy(pos, pMedia->pU, pMedia->dwPitch * hDevice->m_dwHeight / 4);

				pMedia->pY = hDevice->m_pImgBuffer;
			}
			hDevice->m_hDev->PutOneFrame(pMedia);
		}
			
	}

	//TRACE("[DevOutVideo] McVideoOutConversion End..");

	return	TRUE;
}

#if 0
BOOL McVideoOutGetBuffer(McVideoOutHandle hHandle, McMediaVideo* pMedia)
{
	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;
	DWORD32				dwNew = (hDevice->m_dwReadBuffer + 1)%VIDEO_MEDIA_BUFFER;

	if (hDevice->m_bBufferEnable == FALSE)
		return FALSE;

	if(hDevice->m_dwReadBuffer == hDevice->m_dwWriteBuffer)
	{
		TRACE("[DevOutVideo] McVideoOutGetBuffer - buffer is empty");
		return FALSE;
	}

	if (pMedia)
		memcpy(pMedia, &hDevice->m_Buffer[hDevice->m_dwReadBuffer], sizeof(McMediaVideo));

	TRACE("[DevOutVideo] McVideoOutGetBuffer - ReadBuffer %d", hDevice->m_dwReadBuffer);
	hDevice->m_dwReadBuffer = dwNew;

	return TRUE;
}
#endif

BOOL McVideoOutReset(McVideoOutHandle hHandle)
{
	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;

	TRACE("[DevOutVideo] McVideoOutReset - start");

	if(hDevice && hDevice->m_hDev)
	{
		//hDevice->m_hDev->ResetQueue();
	}
	else
		hDevice->m_dwReadBuffer = hDevice->m_dwWriteBuffer = 0;

	TRACE("[DevOutVideo] McVideoOutReset - end");

	return	TRUE;
}

BOOL McVideoOutClear(McVideoOutHandle hHandle)
{
	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;

	if (!hDevice)
		return	FALSE;

	hDevice->m_dwReadBuffer = hDevice->m_dwWriteBuffer = 0;

	return	TRUE;
}

BOOL McVideoOutPlay(McVideoOutHandle hHandle)
{
	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;

	//TRACE("[DevOutVideo] McVideoOutPlay - start");

	if (!hDevice)
		return	FALSE;

	if (hDevice->m_hDev)
		hDevice->m_hDev->PlayOutOneFrame();

	//TRACE("[DevOutVideo] McVideoOutPlay - end");

	return	TRUE;
}


BOOL McVideoOutDidChangeView(McVideoOutHandle hHandle, DWORD32 new_width, DWORD32 new_height)
{
	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;
	BOOL bRet = FALSE;

	TRACE("[McVideoOutDidChangeView] begin");

	if (hDevice && hDevice->m_hDev) {

		TRACE("[McVideoOutDidChangeView] new (%d * %d), old (%d * %d)", new_width, new_height, hDevice->m_dwOutWidth, hDevice->m_dwOutHeight);

		if (hDevice->m_dwOutWidth != new_width || hDevice->m_dwOutHeight != new_height) {

			//TRACE("McVideoOutDidChangeView [%dx%d]", new_width, new_height);

			//hDevice->m_hDev->DidChangeView2((int32_t)new_width, (int32_t)new_height, true);
			hDevice->m_dwOutWidth = new_width;
			hDevice->m_dwOutHeight = new_height;
		}
		else 
		{
			TRACE("McVideoOutDidChangeView - no changeview due to same view");
		}

		bRet = TRUE;
	}

	TRACE("[McVideoOutDidChangeView] end (%d)", bRet);

	return bRet;
}
//
//// Draw a portion of video whose size is (new_width, newHeight) and centered at (new_center_hor, new_center_ver) into a target window
//BOOL McVideoOutDidChangeViewPort(McVideoOutHandle hHandle, INT32 new_center_hor, INT32 new_center_ver, DWORD32 new_width, DWORD32 new_height)
//{
//	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;
//	BOOL bRet = FALSE;
//
//	TRACE("[McVideoOutDidChangeViewPort] begin");
//
//	if (hDevice && hDevice->m_hDev) {
//
//		if (!hDevice->m_dwWidth || !hDevice->m_dwHeight)
//		{
//			TRACE("[McVideoOutDidChangeViewPort] zero source size (%d * %d)", hDevice->m_dwWidth, hDevice->m_dwHeight);
//			return FALSE;
//		}
//
//		TRACE("[McVideoOutDidChangeViewPort] (%d * %d) at (%d * %d)", new_width, new_height, new_center_hor, new_center_ver);
//
//		if (new_width && new_height)
//		{
//			hDevice->m_nViewCenterHor = new_center_hor;
//			hDevice->m_nViewCenterVer = new_center_ver;
//			hDevice->m_nViewWidth = new_width;
//			hDevice->m_nViewHeight = new_height;
//
//			hDevice->m_fViewCenterHor = (FLOAT)new_center_hor / hDevice->m_dwWidth;
//			hDevice->m_fViewCenterVer = (FLOAT)new_center_ver / hDevice->m_dwHeight;
//			hDevice->m_fViewWidth = (FLOAT)new_width / hDevice->m_dwWidth;
//			hDevice->m_fViewHeight = (FLOAT)new_height / hDevice->m_dwHeight;
//		}
//		else
//		{
//			TRACE("[McVideoOutDidChangeViewPort] Reset to no zoom and center position");
//			hDevice->m_nViewCenterHor = hDevice->m_dwWidth / 2;
//			hDevice->m_nViewCenterVer = hDevice->m_dwHeight / 2;
//			hDevice->m_nViewWidth = hDevice->m_dwWidth;
//			hDevice->m_nViewHeight = hDevice->m_dwHeight;
//
//			hDevice->m_fViewCenterHor = 0.5;
//			hDevice->m_fViewCenterVer = 0.5;
//			hDevice->m_fViewWidth = 1.0;
//			hDevice->m_fViewHeight = 1.0;
//		}
//
//		hDevice->m_hDev->DidChangeViewPort(hDevice->m_fViewCenterHor, hDevice->m_fViewCenterVer, hDevice->m_fViewWidth, hDevice->m_fViewHeight);
//	}
//
//	TRACE("[McVideoOutDidChangeViewPort] end (%d)", bRet);
//
//	return bRet;
//}
//
//BOOL McVideoOutSetDisplayType(McVideoOutHandle hHandle, DWORD dwType, DWORD dwRatioX, DWORD dwRatioY)
//{
//	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;
//
//	TRACE("[McVideoOutSetDisplayType] begin : bKeepRatio(0x%x)", dwType);
//
//	if (!hDevice)
//	{
//		TRACE("[McVideoOutSetDisplayType] Failed to set DisplayYype : hDevice(0x%x)", hDevice);
//		return	FALSE;
//	}
//	else if (!hDevice->m_hDev)
//	{
//		TRACE("[McVideoOutSetDisplayType] Failed to set DisplayYype : hDevice->m_hDev(0x%x)", hDevice->m_hDev);
//		return	FALSE;
//	}
//
//	// Keep source aspect ratio
//	if (dwType && VIEW_TYPE_RATIO_SUPPORT)
//		hDevice->m_hDev->SetDisplayeMode(true);
//	// fill the window
//	else if (dwType && VIEW_TYPE_RATIO_FULL) 
//		hDevice->m_hDev->SetDisplayeMode(false);
//
//	return TRUE;
//}
//
//BOOL McVideoOutReady(McVideoOutHandle hHandle)
//{
//	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;
//
//	if (!hDevice)
//	{
//		TRACE("[McVideoOutReady] Failed - dev is null");
//		return	FALSE;
//	}
//	else if (!hDevice->m_hDev)
//	{
//		TRACE("[McVideoOutReady] Failed - dev handle is null");
//		return	FALSE;
//	}
//
//	return hDevice->m_hDev->QueueIsFull()==false;
//}
//
//BOOL McVideoOutSetTextDisplay(McVideoOutHandle hHandle,BOOL bTextShowHide, DWORD dwType,DWORD dwUpPos,DWORD dwDownPos,DWORD dwLeftPos,DWORD dwRightPos)
//{
//	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;
//
//	if (!hDevice)
//		return	FALSE;
//
//	return TRUE;
//}
//
//BOOL McVideoOutSetTextFont(McVideoOutHandle hHandle, DWORD dwSize,BOOL bBold, BOOL bUserDefineColor , DWORD dwUserDefineColor , char *szFontName)
//{
//	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;
//
//	if (!hDevice)
//		return	FALSE;
//
//	return TRUE;
//}
//BOOL McVideoOutPutString(McVideoOutHandle hHandle, McMediaText* strString)
//{
//	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;
//
//	if (!hDevice)
//		return	FALSE;
//
//	return	TRUE;
//}
//
//BOOL McVideoOutSetDisplayPos(McVideoOutHandle hHandle, DWORD32 x, DWORD32 y, DWORD32 w, DWORD32 h)
//{
//	return TRUE;
//}
//
//BOOL McVideoOutSet360VRInfo(McVideoOutHandle hHandle, Mc360VRInfo *hVRInfo)
//{
//	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;
//    if (!hDevice || hVRInfo)
//        return FALSE;
//
//	hDevice->m_b360VR = TRUE;
//	hDevice->m_h360VRInfo = hVRInfo;
//
//	return TRUE;
//}
//
//BOOL McVideoOutSetLogo(McVideoOutHandle hHandle, CHAR* pszLogoFile, DWORD32 dwInterval, DWORD32 dwDuration, FLOAT fStartW, FLOAT fSizeW, FLOAT fStartH, FLOAT fSizeH)
//{
//	McVideoOutStructHandle	hDevice = (McVideoOutStructHandle)hHandle;
//
//	if (!hDevice)
//		return	FALSE;
//
//	hDevice->m_hDev->SetLogoRender(pszLogoFile, dwInterval, dwDuration, fStartW, fSizeW, fStartH, fSizeH);
//
//	return TRUE;
//}
//
//BOOL McVideoOutSetPerfInfoString(McVideoOutHandle hHandle, char *szPerfInfoString, int nColor, int nFontSize)
//{
//	return TRUE;
//}
//
//BOOL McVideoOutSetVSync(McVideoOutHandle hHandle, BOOL bVSync)
//{
//	return TRUE;
//}


#endif // End of VIDEOOUT_SDL