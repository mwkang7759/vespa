/*****************************************************************************
*                                                                            *
*                            DeviceOutput Library							 *
*                                                                            *
*   Copyright (c) 2020- by DreamToBe, Incoporated. All Rights Reserved.      *
******************************************************************************

File Name       : DevOutDrawText.cpp
Author(s)       : CHANG, Joonho
Created         : 13 May 2020

Description     : DevOutDrawText API
Notes           :

==============================================================================
Modification History (Reverse Order)
==============================================================================
Date        Author      Location (variables) / Description
------------------------------------------------------------------------------

------------------------------------------------------------------------------*/

#define VIDEOOUT_D3D9DRAW

#if defined(_WIN32)

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <windirent.h>

#include "DevOutDrawText.h"

#ifdef _DEBUG
 #pragma comment(lib, "..\\..\\Lib\\freetype2\\x86\\Debug\\freetypeD.lib")
#else
 #pragma comment(lib, "..\\..\\Lib\\freetype2\\x86\\Release\\freetype.lib")
#endif // DEBUG

// lStat windows implementation
static inline time_t filetime_to_time_t(const FILETIME *ft)
{
	long long winTime = ((long long)ft->dwHighDateTime << 32) + ft->dwLowDateTime;
	winTime -= 116444736000000000LL; /* Windows to Unix Epoch conversion */
	winTime /= 10000000;		 /* Nano to seconds resolution */
	return (time_t)winTime;
}

/* We keep the do_lstat code in a separate function to avoid recursion.
* When a path ends with a slash, the stat will fail with ENOENT. In
* this case, we strip the trailing slashes and stat again.
*/
static int do_lstat(const char *file_name, struct stat *buf)
{
	WIN32_FILE_ATTRIBUTE_DATA fdata;

	if (GetFileAttributesExA(file_name, GetFileExInfoStandard, &fdata)) {
		int fMode = S_IREAD;
		if (fdata.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			fMode |= S_IFDIR;
		else
			fMode |= S_IFREG;
		if (!(fdata.dwFileAttributes & FILE_ATTRIBUTE_READONLY))
			fMode |= S_IWRITE;

		buf->st_ino = 0;
		buf->st_gid = 0;
		buf->st_uid = 0;
		buf->st_nlink = 1;
		buf->st_mode = fMode;
		buf->st_size = fdata.nFileSizeLow; /* Can't use nFileSizeHigh, since it's not a stat64 */
		buf->st_dev = buf->st_rdev = (_getdrive() - 1);
		buf->st_atime = filetime_to_time_t(&(fdata.ftLastAccessTime));
		buf->st_mtime = filetime_to_time_t(&(fdata.ftLastWriteTime));
		buf->st_ctime = filetime_to_time_t(&(fdata.ftCreationTime));
		errno = 0;
		return 0;
	}

	switch (GetLastError()) {
	case ERROR_ACCESS_DENIED:
	case ERROR_SHARING_VIOLATION:
	case ERROR_LOCK_VIOLATION:
	case ERROR_SHARING_BUFFER_EXCEEDED:
		errno = EACCES;
		break;
	case ERROR_BUFFER_OVERFLOW:
		errno = ENAMETOOLONG;
		break;
	case ERROR_NOT_ENOUGH_MEMORY:
		errno = ENOMEM;
		break;
	default:
		errno = ENOENT;
		break;
	}
	return -1;
}

/* We provide our own lstat/fstat functions, since the provided
* lstat/fstat functions are so slow. These stat functions are
* tailored for Git's usage (read: fast), and are not meant to be
* complete. Note that Git stat()s are redirected to mingw_lstat()
* too, since Windows doesn't really handle symlinks that well.
*/
int lstat(const char *file_name, struct stat *buf)
{
	int namelen;
	static char alt_name[PATH_MAX];

	if (!do_lstat(file_name, buf))
		return 0;

	/* if file_name ended in a '/', Windows returned ENOENT;
	* try again without trailing slashes
	*/
	if (errno != ENOENT)
		return -1;

	namelen = strlen(file_name);
	if (namelen && file_name[namelen - 1] != '/')
		return -1;
	while (namelen && file_name[namelen - 1] == '/')
		--namelen;
	if (!namelen || namelen >= PATH_MAX)
		return -1;

	memcpy(alt_name, file_name, namelen);
	alt_name[namelen] = 0;
	return do_lstat(alt_name, buf);
}


typedef struct {
	float x;
	float y;
	float s;
	float t;
} point;

BOOL Init_ft_resources(DrawTextHandle hHandle)
{
	DrawTextStruct*	hDrawText = (DrawTextStruct*)hHandle;

	TRACEX(DTB_LOG_INFO, "[DevOutText:Init_ft_resources]	begin");

	/* Initialize the FreeType2 library */
	if (FT_Init_FreeType(&hDrawText->m_ft_library)) {
		TRACEX(DTB_LOG_ERROR, "[DevOutText:Init_ft_resources]	Could not init freetype library");
		return FALSE;
	}
	else
	{
		TRACEX(DTB_LOG_INFO, "[DevOutText:Init_ft_resources]	FT_LIBRARY (0x%x)", hDrawText->m_ft_library);
	}

	TRACEX(DTB_LOG_INFO, "[DevOutText:Init_ft_resources] end\n");
	return TRUE;
}

void Free_ft_resources(DrawTextHandle hHandle)
{
	DrawTextStruct*	hDrawText = (DrawTextStruct*)hHandle;
	int nIndx = 0;

	TRACEX(DTB_LOG_INFO, "[DevOutText:Free_ft_resources] begin");

	for (nIndx=0; nIndx<E_FT_FACE_NUM; nIndx++)
	{
		if (hDrawText->m_ft_face[nIndx])
		{
			TRACEX(DTB_LOG_INFO, "[DevOutText:Free_ft_resources] Release FT_FACE[%d] (0x%x)", nIndx, hDrawText->m_ft_face[nIndx]);
			FT_Done_Face(hDrawText->m_ft_face[nIndx]);
			hDrawText->m_ft_face[nIndx] = NULL;
		}
	}

	if (hDrawText->m_ft_library)
	{
		TRACEX(DTB_LOG_INFO, "[DevOutText:Free_ft_resources] Release FT_LIBRARY (0x%x)", hDrawText->m_ft_library);
		FT_Done_FreeType(hDrawText->m_ft_library);
	}

	TRACEX(DTB_LOG_INFO, "[DevOutText:Free_ft_resources] end");
}


void measure_text(DrawTextHandle hHandle, const UTF32 *text, int nEglWidth, int *pnWidth, int *pnHeight, int *pnTextNum)
{
	DrawTextStruct*	hDrawText = (DrawTextStruct*)hHandle;

	UTF32 *p;
	FT_GlyphSlot g;
	FT_Face ft_face;
	int nLastSpaceIndex = 0;
	int nLastWidth = 0;
	int nCharIndex = 0;

	int nIndx = 0;

	//TRACEX(DTB_LOG_TRACE, "[DevOutText:measure_text] begin");

	//TRACEX(DTB_LOG_TRACE, "[DevOutText:measure_text] ### EglWidth:%d, width:%d, height:%d, TextNum:%d", nEglWidth, *pnWidth, *pnHeight, *pnTextNum);
	//TRACEX(DTB_LOG_TRACE, "[DevOutText:measure_text] ###### [0x%x]", text[0]);



	for (p = (UTF32 *)text; *p; p++) {

		//TRACEX(DTB_LOG_TRACE, "[DevOutText:measure_text] ### [0x%x]", *p);

		*pnTextNum += 1;

		for (nIndx=0; nIndx<E_FT_FACE_NUM; nIndx++)
		{
			nCharIndex = 0;

			if (hDrawText->m_ft_face[nIndx])
			{
				nCharIndex = FT_Get_Char_Index(hDrawText->m_ft_face[nIndx], *p);
				if (nCharIndex == 0) {
					//TRACEX(DTB_LOG_INFO, "[DevOutText:measure_text] (FT:%d) undefined character code : [0x%04x]", nIndx, *p);
				}
				else
				{
					ft_face = hDrawText->m_ft_face[nIndx];
					g = ft_face->glyph;

					//TRACEX(DTB_LOG_TRACE, "[DevOutText:measure_text] (FT:%d) (%d : 0x%04x)", nIndx, nCharIndex, *p);

					break;
				}
			}
		}

		if (nCharIndex == 0)
			continue;

		/* Try to load and render the character */
		if (FT_Load_Char(ft_face, *p, FT_LOAD_RENDER))
			continue;

		//TRACEX(DTB_LOG_INFO, "[DevOutText:measure_text] %s, x:%f, y:%f, sx:%f, sy:%f", p, x, y, sx, sy);

		*pnWidth += g->advance.x >> 6;

		if (*p == 0x20) // Space
		{
			nLastSpaceIndex = *pnTextNum;
			nLastWidth = *pnWidth;

			//TRACEX(DTB_LOG_TRACE, "[DevOutText:measure_text] SPACE[0x%x] : N:%d, W:%d", *p, nLastSpaceIndex, nLastWidth);
		}

		if (nEglWidth < *pnWidth)
		{
			//TRACEX(DTB_LOG_TRACE, "[DevOutText:measure_text] OVER[0x%x] : N:%d, W:%d", *p, *pnTextNum, *pnWidth);

			if (*p == 0x20) // Space
			{
				*pnTextNum -= 1;
				*pnWidth -= g->advance.x >> 6;
			}
			else
			{
				if (nLastSpaceIndex > 0)
				{
					*pnTextNum = nLastSpaceIndex;
					*pnWidth = nLastWidth;
				}
				else
				{
					*pnTextNum -= 1;
					*pnWidth -= g->advance.x >> 6;
				}
			}

			break;
		}

		if (*pnHeight < g->bitmap.rows)
			*pnHeight = g->bitmap.rows;
	}

	//TRACEX(DTB_LOG_TRACE, "[DevOutText:measure_text] EglWidth:%d, width:%d, height:%d, TextNum:%d", nEglWidth, *pnWidth, *pnHeight, *pnTextNum);

	//TRACEX(DTB_LOG_TRACE, "[DevOutText:measure_text] end");
}

/**
 * Render text using the currently loaded font and currently set font size.
 * Rendering starts at coordinates (x, y), z is always 0.
 * The pixel coordinates that the FreeType2 library uses are scaled by (sx, sy).
 */
void render_text(DrawTextHandle hHandle, const UTF32 *text, BYTE* pOut, INT32 nTextHeight, DWORD32 dwStartW, DWORD32 dwStartH, DWORD32 dwPitch)
{
	DrawTextStruct*	hDrawText = (DrawTextStruct*)hHandle;

	UTF32 *p;
	FT_GlyphSlot g;

	int nWidth = 0;
	int nHeight = 0;
	unsigned long usChar;

	int nCharIndex=0;
	FT_Face ft_face;
	int nIndx = 0;

	/* Loop through all characters */
	for (p = (UTF32 *)text; *p; p++) {

		//TRACEX(DTB_LOG_TRACE, "[DevOutText:render_text] ### [0x%x]", *p);

		for (nIndx=0; nIndx<E_FT_FACE_NUM; nIndx++)
		{
			nCharIndex = 0;

			if (hDrawText->m_ft_face[nIndx])
			{
				nCharIndex = FT_Get_Char_Index(hDrawText->m_ft_face[nIndx], *p);	//
				if (nCharIndex == 0) {
					//TRACEX(DTB_LOG_TRACE, "[DevOutText:render_text] (FT:%d) undefined character code : [0x%04x]", nIndx, *p);
				}
				else
				{
					ft_face = hDrawText->m_ft_face[nIndx];
					g = ft_face->glyph;

					//TRACEX(DTB_LOG_TRACE, "[DevOutText:render_text] (FT:%d) : %d : 0x%04x", nIndx, nCharIndex, *p);

					break;
				}
			}
		}

		if (nCharIndex == 0)
			continue;

		//else
		//	TRACEX(DTB_LOG_TRACE, "[DevOutText:render_text] character code : %d", nCharIndex);

		//TRACEX(DTB_LOG_TRACE, "[DevOutText:render_text] ### [0x%x] [0x%x]", usChar, *p);

		/* Try to load and render the character */
		if (FT_Load_Char(ft_face, *p, FT_LOAD_RENDER))
			continue;

		//TRACEX(DTB_LOG_TRACE, "[DevOutText:render_text] %s, x:%f, y:%f, sx:%f, sy:%f", p, x, y, sx, sy);

		/* Upload the "bitmap", which contains an 8-bit grayscale image, as an alpha texture */
		//glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, g->bitmap.width, g->bitmap.rows, 0, GL_ALPHA, GL_UNSIGNED_BYTE, g->bitmap.buffer);

		//sleep(100);

		/* Calculate the vertex and texture coordinates */
		//float x2 = x + g->bitmap_left;
		//float y2 = -y - g->bitmap_top;
		//float w = g->bitmap.width;
		//float h = g->bitmap.rows;

		int		i, j;
		BYTE	*pCur;

		for (i = 0; i <g->bitmap.rows; i++)
		{
			pCur = pOut + (dwStartH + nTextHeight - g->bitmap_top +i)*dwPitch + dwStartW*4 + g->bitmap_left*4;
			for (j = 0; j < g->bitmap.width; j++)
			{
				//alpha = g->bitmap.buffer[g->bitmap.pitch*i + j + g->bitmap_left];
				//c = (255 - alpha)*pCur[0] + alpha * 255;	pCur[0] = (c >> 8);
				//c = (255 - alpha)*pCur[1] + alpha * 255;	pCur[1] = (c >> 8);
				//c = (255 - alpha)*pCur[2] + alpha * 255;	pCur[2] = (c >> 8);
				
				if (g->bitmap.buffer[g->bitmap.pitch*i + j])
					pCur[0] = pCur[1] = pCur[2] = g->bitmap.buffer[g->bitmap.pitch*i + j];
				pCur += 4;
			}
		}

		/* Advance the cursor to the start of the next character */
		//x += (g->advance.x >> 6);
		//y += (g->advance.y >> 6);

		nWidth += g->advance.x >> 6;


		dwStartW += g->advance.x >> 6;
		//dwStartW += g->bitmap.pitch;

		if (nHeight < g->bitmap.rows)
			nHeight = g->bitmap.rows;

		//TRACEX(DTB_LOG_TRACE, "[DevOutText:render_text] bitmap : left:%d, width:%d, rows:%d, top:%d, p:%d, adv.x:%d, adv.y:%d, nWidth:%d, nHeight:%d", g->bitmap_left, g->bitmap.width, g->bitmap.rows, g->bitmap_top, g->bitmap.pitch, g->advance.x, g->advance.y, nWidth, nHeight);
	}

	//TRACEX(DTB_LOG_TRACE, "[DevOutText:render_text] end");
}

void render_text2(DrawTextHandle hHandle, const UTF32 *text, float x, float y, float sx, float sy, float *ftColor, int nOutlineSize, BOOL bBold, BOOL bShadow)
{
	DrawTextStruct*	hDrawText = (DrawTextStruct*)hHandle;

	UTF32 *p;

	FT_Error ft_err;

	FT_BitmapGlyph ft_org_bitmap_glyph = NULL;
	FT_BitmapGlyph ft_bitmap_glyph = NULL;

	FT_Glyph ft_org_glyph = NULL;
	FT_Glyph ft_glyph = NULL;

	FT_Stroker stroker = NULL;
	FT_GlyphSlot g;
	FT_GlyphSlot slot;

	FT_Int orgBitmapLeft = 0;
	FT_Int orgBitmapTop = 0;
	FT_Int orgBitmapWidth = 0;
	FT_Int orgBitmapRows = 0;
	FT_Int orgBitmapPitch = 0;
	FT_Pos orgAdvanceX = 0;
	FT_Pos orgAdvanceY = 0;

	FT_Int outline_thickness = nOutlineSize;
	FT_Int bold_thickness = 0;

	//float black[4] =	{ 0, 0, 0, 1 };
	//float red[4] =	{ 1, 0, 0, 1 };
	//float white[4] =	{ 1, 1, 1, 1 };
	//float gray[4] =	{ 50/255., 50/255., 50/255., 1 };

	// Calculate the vertex and texture coordinates
	float x2;
	float y2;
	float w;
	float h;
	point box[4] = {0,};

	int nWidth = 0;
	int nHeight = 0;
	unsigned long usChar;

	int nCharIndex=0;
	FT_Face ft_face;
	int nIndx = 0;

	//TRACEX(DTB_LOG_TRACE, "[DevOutText:render_text2] begin");
	//TRACEX(DTB_LOG_TRACE, "[DevOutText:render_text2] OutlineSize:%d, Bold:%d, Shadow:%d", nOutlineSize, bBold, bShadow);

	/* Loop through all characters */
	for (p = (UTF32 *)text; *p; p++) {

		//TRACEX(DTB_LOG_TRACE, "[DevOutText:render_text2] ### [0x%x]", *p);

		for (nIndx=0; nIndx<E_FT_FACE_NUM; nIndx++)
		{
			nCharIndex = 0;

			if (hDrawText->m_ft_face[nIndx])
			{
				nCharIndex = FT_Get_Char_Index(hDrawText->m_ft_face[nIndx], *p);	//
				if (nCharIndex == 0) {
					//TRACEX(DTB_LOG_TRACE, "[DevOutText:render_text2] (FT:%d) undefined character code : [0x%04x]", nIndx, *p);
				}
				else
				{
					ft_face = hDrawText->m_ft_face[nIndx];
					g = ft_face->glyph;

					//TRACEX(DTB_LOG_TRACE, "[DevOutText:render_text2] (FT:%d) : %d : 0x%04x", nIndx, nCharIndex, *p);

					break;
				}
			}
		}

		//else
		//	TRACEX(DTB_LOG_TRACE, "[DevOutText:render_text2] character code : %d", nCharIndex);

		//TRACEX(DTB_LOG_TRACE, "[DevOutText:render_text2] ### [0x%x] [0x%x]", usChar, *p);

		/* Try to load and render the character */
		if (FT_Load_Char(ft_face, *p, FT_LOAD_RENDER))
			continue;

		//TRACEX(DTB_LOG_TRACE, "[DevOutText:render_text2] %s, x:%f, y:%f, sx:%f, sy:%f", p, x, y, sx, sy);

		orgBitmapLeft = g->bitmap_left;
		orgBitmapTop = g->bitmap_top;
		orgBitmapWidth = g->bitmap.width;
		orgBitmapRows = g->bitmap.rows;
		orgBitmapPitch = g->bitmap.pitch;
		orgAdvanceX = g->advance.x;
		orgAdvanceY = g->advance.y;

		if (bBold)
			bold_thickness = 2;

		if (outline_thickness)
		{
			FT_Int32 flags = 0;

			slot = ft_face->glyph;

			flags |= FT_LOAD_NO_BITMAP;
			flags |= FT_LOAD_NO_HINTING;

			ft_err = FT_Load_Glyph(ft_face, nCharIndex, flags);
			if ( ft_err )
			{
				TRACEX(DTB_LOG_INFO, "[DevOutText:render_text2] Fail FT_Load_Glyph() : FT_ERROR (%d)", ft_err);
				continue;
			}
			else
			{
				if (bold_thickness > 0)
				{
					if ( slot->format == FT_GLYPH_FORMAT_OUTLINE )
					{
						if (!(ft_face->style_flags & FT_STYLE_FLAG_BOLD))
						{
							ft_err = FT_Outline_Embolden( &slot->outline, bold_thickness * 64 );
							FT_Outline_Translate(&slot->outline, -bold_thickness * 32, -bold_thickness * 32);

							if (ft_err)
								TRACEX(DTB_LOG_INFO, "[DevOutText:render_text2]	Fail FT_Outline_Embolden (%d)", ft_err);
						}
					}
				}
			}

			// Set up a stroker.
			ft_err = FT_Stroker_New(hDrawText->m_ft_library, &stroker);
			if (ft_err)
			{
				TRACEX(DTB_LOG_INFO, "[DevOutText:render_text2]	Fail FT_Stroker_New (%d)", ft_err);
			}
			else
			{
				FT_Stroker_Set( stroker, (int)(outline_thickness * 64), FT_STROKER_LINECAP_ROUND, FT_STROKER_LINEJOIN_ROUND, 0);
			}

			/* Get_Glyph must be accompanied by Done_Glyph */
			ft_err = FT_Get_Glyph(slot, &ft_glyph);
			if (ft_err)
			{
				TRACEX(DTB_LOG_INFO, "[DevOutText:render_text2]	Fail FT_Get_Glyph (%d)", ft_err);
			}
			else
			{
				ft_err = FT_Glyph_Copy( ft_glyph, &ft_org_glyph );
				if (ft_err)
				{
					TRACEX(DTB_LOG_INFO, "[DevOutText:render_text2]	Fail FT_Glyph_Copy (%d)", ft_err);
				}

				// OUTLINE LINE
				ft_err = FT_Glyph_Stroke( &ft_glyph, stroker, 1 );
				if (ft_err)
				{
					TRACEX(DTB_LOG_INFO, "[DevOutText:render_text2]	Fail FT_Glyph_Stroke !!!");
				}
				//else
				//	TRACEX(DTB_LOG_INFO, "[DevOutText:render_text2]	Success FT_Glyph_Stroke !!!");

				FT_Glyph_To_Bitmap( &ft_glyph, FT_RENDER_MODE_NORMAL, 0, 1);
				ft_bitmap_glyph = (FT_BitmapGlyph) ft_glyph;

				if (bShadow)
				{
					FT_Int shadowPos = hDrawText->m_dwTextSize / 20;

					x2 = x + (orgBitmapLeft - (outline_thickness/1) + (shadowPos)) * sx;
					y2 = -y - (orgBitmapTop + (outline_thickness/1) - (shadowPos)) * sy;
					w = ft_bitmap_glyph->bitmap.width * sx;
					h = ft_bitmap_glyph->bitmap.rows * sy;

					box[0].x = x2;		box[0].y = -y2;		box[0].s = 0; box[0].t = 0;
					box[1].x = x2 + w;	box[1].y = -y2;		box[1].s = 1; box[1].t = 0;
					box[2].x = x2;		box[2].y = -y2 - h; box[2].s = 0; box[2].t = 1;
					box[3].x = x2 + w;	box[3].y = -y2 - h; box[3].s = 1; box[3].t = 1;

					// Draw background of shadow
					//glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
					//glUniform4fv(hDrawText->m_nUniform_color, 1, black);
					//glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, ft_bitmap_glyph->bitmap.width, ft_bitmap_glyph->bitmap.rows, 0, GL_ALPHA, GL_UNSIGNED_BYTE, ft_bitmap_glyph->bitmap.buffer);
					//glBufferData(GL_ARRAY_BUFFER, sizeof box, box, GL_DYNAMIC_DRAW);
					//glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

					// Draw shadow
					//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
					//glUniform4fv(hDrawText->m_nUniform_color, 1, gray);
					//glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, ft_bitmap_glyph->bitmap.width, ft_bitmap_glyph->bitmap.rows, 0, GL_ALPHA, GL_UNSIGNED_BYTE, ft_bitmap_glyph->bitmap.buffer);
					//glBufferData(GL_ARRAY_BUFFER, sizeof box, box, GL_DYNAMIC_DRAW);
					//glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

					//TRACEX(DTB_LOG_TRACE, "[DevOutText:render_text2] Shadow : x2:%d, y2:%d, w:%d, h:%d", x2, y2, w, h);
				}

				// Calculate the vertex and texture coordinates
				x2 = x + (orgBitmapLeft - (outline_thickness/1)) * sx;
				y2 = -y - (orgBitmapTop + (outline_thickness/1)) * sy;
				w = ft_bitmap_glyph->bitmap.width * sx;
				h = ft_bitmap_glyph->bitmap.rows * sy;

				box[0].x = x2;		box[0].y = -y2;		box[0].s = 0; box[0].t = 0;
				box[1].x = x2 + w;	box[1].y = -y2;		box[1].s = 1; box[1].t = 0;
				box[2].x = x2;		box[2].y = -y2 - h; box[2].s = 0; box[2].t = 1;
				box[3].x = x2 + w;	box[3].y = -y2 - h; box[3].s = 1; box[3].t = 1;

				/*point box[4] = {
					{x2, -y2, 0, 0},
					{x2 + w, -y2, 1, 0},
					{x2, -y2 - h, 0, 1},
					{x2 + w, -y2 - h, 1, 1},
				};*/

				// Draw the character on the screen
				//glBufferData(GL_ARRAY_BUFFER, sizeof box, box, GL_DYNAMIC_DRAW);
				//glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

				//TRACEX(DTB_LOG_TRACE, "[DevOutText:render_text2] OUTLINE : x2:%d, y2:%d, w:%d, h:%d", x2, y2, w, h);
			}
		}
		else
		{
			if (bold_thickness > 0)
			{
				if ( slot->format == FT_GLYPH_FORMAT_OUTLINE )
				{
					ft_err = FT_Outline_Embolden( &slot->outline, bold_thickness * 64 );
					FT_Outline_Translate(&slot->outline, -bold_thickness * 32, -bold_thickness * 32);

					if (ft_err)
						TRACEX(DTB_LOG_INFO, "[DevOutText:render_text2]	Fail FT_Outline_Embolden (%d)", ft_err);
				}
			}
		}

		//TRACEX(DTB_LOG_TRACE, "[DevOutText:render_text2] ft_org_glyph : 0x%x", ft_org_glyph);

		if (ft_org_glyph)
		{
			ft_err = FT_Glyph_To_Bitmap( &ft_org_glyph, FT_RENDER_MODE_NORMAL, 0, 1);
			if (ft_err)
			{
				TRACEX(DTB_LOG_INFO, "[DevOutText:render_text2]	Fail FT_Glyph_To_Bitmap !!!");
			}
			else
			{
				ft_org_bitmap_glyph = (FT_BitmapGlyph) ft_org_glyph;
			}
		}


		if (!outline_thickness && bShadow)
		{
			FT_Int shadowPos = hDrawText->m_dwTextSize / 20;

			x2 = x + (orgBitmapLeft + (bold_thickness/2) + (shadowPos)) * sx;
			y2 = -y - (orgBitmapTop - (bold_thickness/2) - (shadowPos)) * sy;
			w = orgBitmapWidth * sx;
			h = orgBitmapRows * sy;

			box[0].x = x2;		box[0].y = -y2;		box[0].s = 0; box[0].t = 0;
			box[1].x = x2 + w;	box[1].y = -y2;		box[1].s = 1; box[1].t = 0;
			box[2].x = x2;		box[2].y = -y2 - h; box[2].s = 0; box[2].t = 1;
			box[3].x = x2 + w;	box[3].y = -y2 - h; box[3].s = 1; box[3].t = 1;

			//glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
			//glUniform4fv(hDrawText->m_nUniform_color, 1, black);

			// Draw background of shadow
			if (ft_org_bitmap_glyph)
				;// glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, ft_org_bitmap_glyph->bitmap.width, ft_org_bitmap_glyph->bitmap.rows, 0, GL_ALPHA, GL_UNSIGNED_BYTE, ft_org_bitmap_glyph->bitmap.buffer);
			else
				;// glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, g->bitmap.width, g->bitmap.rows, 0, GL_ALPHA, GL_UNSIGNED_BYTE, g->bitmap.buffer);

			//glBufferData(GL_ARRAY_BUFFER, sizeof box, box, GL_DYNAMIC_DRAW);
			//glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

			// Draw shadow
			//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			//glUniform4fv(hDrawText->m_nUniform_color, 1, gray);

			if (ft_org_bitmap_glyph)
				;// glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, ft_org_bitmap_glyph->bitmap.width, ft_org_bitmap_glyph->bitmap.rows, 0, GL_ALPHA, GL_UNSIGNED_BYTE, ft_org_bitmap_glyph->bitmap.buffer);
			else
				;// glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, g->bitmap.width, g->bitmap.rows, 0, GL_ALPHA, GL_UNSIGNED_BYTE, g->bitmap.buffer);

			//glBufferData(GL_ARRAY_BUFFER, sizeof box, box, GL_DYNAMIC_DRAW);
			//glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		}


		x2 = x + (orgBitmapLeft + (bold_thickness/2)) * sx;
		y2 = -y - (orgBitmapTop - (bold_thickness/2)) * sy;
		w = orgBitmapWidth * sx;
		h = orgBitmapRows * sy;

		box[0].x = x2;		box[0].y = -y2;		box[0].s = 0; box[0].t = 0;
		box[1].x = x2 + w;	box[1].y = -y2;		box[1].s = 1; box[1].t = 0;
		box[2].x = x2;		box[2].y = -y2 - h; box[2].s = 0; box[2].t = 1;
		box[3].x = x2 + w;	box[3].y = -y2 - h; box[3].s = 1; box[3].t = 1;

		/*point box2[4] = {
			{x2, -y2, 0, 0},
			{x2 + w, -y2, 1, 0},
			{x2, -y2 - h, 0, 1},
			{x2 + w, -y2 - h, 1, 1},
		};*/

		if (!outline_thickness && !bBold)
		{
			//glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
			//glUniform4fv(hDrawText->m_nUniform_color, 1, black);
			//glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, g->bitmap.width, g->bitmap.rows, 0, GL_ALPHA, GL_UNSIGNED_BYTE, g->bitmap.buffer);

			// Draw the character on the screen
			//glBufferData(GL_ARRAY_BUFFER, sizeof box, box, GL_DYNAMIC_DRAW);
			//glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		}

		//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//glUniform4fv(hDrawText->m_nUniform_color, 1, ftColor);

		//TRACEX(DTB_LOG_TRACE, "[DevOutText:render_text2] ft_org_bitmap_glyph : 0x%x", ft_org_bitmap_glyph);
		if (ft_org_bitmap_glyph)
		{
			//glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, ft_org_bitmap_glyph->bitmap.width, ft_org_bitmap_glyph->bitmap.rows, 0, GL_ALPHA, GL_UNSIGNED_BYTE, ft_org_bitmap_glyph->bitmap.buffer);
		}
		else
		{
			/* Upload the "bitmap", which contains an 8-bit grayscale image, as an alpha texture */
			//glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, g->bitmap.width, g->bitmap.rows, 0, GL_ALPHA, GL_UNSIGNED_BYTE, g->bitmap.buffer);
		}

		// Draw the character on the screen
		//glBufferData(GL_ARRAY_BUFFER, sizeof box, box, GL_DYNAMIC_DRAW);
		//glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

		if (stroker)
			FT_Stroker_Done(stroker);

		if (ft_glyph)
			FT_Done_Glyph(ft_glyph);

		if (ft_org_glyph)
			FT_Done_Glyph(ft_org_glyph);


		/* Advance the cursor to the start of the next character */
		x += ( (orgAdvanceX >> 6) /* + outline_thickness*/ ) * sx;
		y += (orgAdvanceY >> 6) * sy;

		nWidth += orgAdvanceX >> 6;

		if (nHeight < orgBitmapRows)
			nHeight = orgBitmapRows;

		//TRACEX(DTB_LOG_TRACE, "[DevOutText:render_text2] bitmap : left:%d, width:%d, rows:%d, top:%d, p:%d, adv.x:%d, adv.y:%d, nWidth:%d, nHeight:%d", g->bitmap_left, g->bitmap.width, g->bitmap.rows, g->bitmap_top, g->bitmap.pitch, g->advance.x, g->advance.y, nWidth, nHeight);
	}

	//glDisableVertexAttribArray(hDrawText->m_nAttribute_coord);
	//glDeleteTextures(1, &tex);

	//TRACEX(DTB_LOG_TRACE, "[DevOutText:render_text2] end");
}

LRSLT DrawTextOpen(DrawTextHandle* phDevice, McTextInfo* hInfo)
{
	DrawTextStruct*	hDrawText;


	TRACE("[DevOutText:DrawTextOpen] begin : width=%d height=%d GLES=%d", hInfo->m_dwOutWidth, hInfo->m_dwOutHeight, hInfo->m_bUseGLES);

	hDrawText = (DrawTextStruct*)MALLOCZ(sizeof(DrawTextStruct));
	if (hDrawText)
	{
		// CHECK later 
		// hDrawText->m_fScreenRatio = hDevice->m_fScreenXdpi / DTB_MDPI;

		hDrawText->m_hWnd = hInfo->m_hWnd;
		GetWindowRect(hDrawText->m_hWnd, &hDrawText->m_rcWnd);
		hDrawText->m_dwWndWidth = hDrawText->m_rcWnd.right - hDrawText->m_rcWnd.left;
		hDrawText->m_dwWndHeight = hDrawText->m_rcWnd.bottom - hDrawText->m_rcWnd.top;

		if (!Init_ft_resources(hDrawText))
		{
			TRACEX(DTB_LOG_ERROR, "[DevOutText:DrawTextOpen] Init_ft_resources fail");
			Free_ft_resources(hDrawText);

			*phDevice = hDrawText;
			return COMMON_ERR_SYSTEM;
		}

		hDrawText->m_bFtInitialize = TRUE;

		{
			McMediaText stText;
			char szFont[256] = {0, };
			UTF32 ulChar = 0;
			DWORD32 dwFontSize = 30;		// Pixel
			DWORD32 dwLeftMargin = 5;		// Pixel
			DWORD32 dwBottomMargin = 50;	// Pixel
			DWORD32 dwTextYPosGap = 10;		// Pixel
			//DWORD32 dwTextOutlineSize = 4;	// Pixel
			OUTLINE_TYPE eOutlinetype = OUTLINE_NORMAL;	// OUTLINE_THIN, OUTLINE_NORMAL, OUTLINE_THICK

			strcpy(szFont, "C:/Windows/Fonts");
			//DrawTextSetUserFont(hDrawText, szFont);
			DrawTextSetDefaultSystemFont(hDrawText, szFont);
			DrawTextSetDisplayPos(hDrawText, dwLeftMargin, dwBottomMargin, dwTextYPosGap, dwFontSize, eOutlinetype, FALSE, FALSE);

			//stText.block_num = 1;
			//stText.block->len = 1;
			//stText.block->ulData[0] = 0xD55C;
			//DrawTextPutString(hDrawText, &stText);


			//DrawTextSetSurfaceChanged(hDrawText, hDrawText->m_pANwindow, 0, 0);
		}

		TRACE("[DevOutText:DrawTextOpen] end (%x)", hDrawText);

		*phDevice = hDrawText;
	}
	else{
		TRACE("[DevOutText:DrawTextOpen] end (0x%x)", COMMON_ERR_MEM);
		return	COMMON_ERR_MEM;
	}

	return	MC_OK;
}

void DrawTextClose(DrawTextHandle hHandle)
{
	DrawTextStruct*	hDrawText = (DrawTextStruct*)hHandle;

	TRACE("[DevOutText:DrawTextClose]	begin, hDrawText (x0%x)", hDrawText);

	if (hDrawText)
	{

		DrawTextReset(hDrawText);

		McSleep(50);			// wait for rendering

		if (hDrawText->m_Buff)
			FREE(hDrawText->m_Buff);

		if (hDrawText->m_bFtInitialize == TRUE)
			Free_ft_resources(hDrawText);

		FREE(hDrawText);

		TRACE("[DevOutText:DrawTextClose]	End, hDrawText (0x%x)", hDrawText);
	}
}


BOOL DrawTextPutString(DrawTextHandle hHandle, McMediaText* pstText, McMediaVideo *pMedia)
{
	DrawTextStruct*	hDrawText = (DrawTextStruct*)hHandle;

	int nIndx = 0;

	float   fIndxX = 0.0;
	float	fIndxY = 0.0;
	float   fCharGap = 0.0;

	float sx = 0;
	float sy = 0;

	int nTextSurfaceWidth = 0;
	int nTextWidth= 0, nTextHeight = 0, nTextNum = 0;
	int nCurPoint = 0;
	int nTotalTextNum = 0;

	DWORD32	dwFontSize = 0;

	float fBaseX = 0, fBaseY = 0, fBaseYGap = 0;
	float fCurX = 0, fCurY = 0;

	OUTLINE_TYPE eOutlineType = OUTLINE_NONE;
	DWORD32 dwOutlineSize = 0;
	DWORD32 dwSize = 0;

	DWORD32	dwBaseTime = 0;
	DWORD32 dwCurTime = 0;

	if (!hDrawText)
		return	FALSE;

	//TRACEX(DTB_LOG_ERROR, "[DevOutText:DrawTextPutString] begin");

	dwBaseTime = McGetTickCount();
	TRACEX(DTB_LOG_TRACE, "[DevOutText:DrawTextPutString] begin");

	if (!hDrawText->m_dwWndWidth || !hDrawText->m_dwWndHeight)
	{
		TRACEX(DTB_LOG_ERROR, "[DevOutText:DrawTextPutString] Egl_width:%d, Egl_height:%d", hDrawText->m_dwWndWidth, hDrawText->m_dwWndHeight);
		return FALSE;
	}

	for (nIndx=0; nIndx<E_FT_FACE_NUM; nIndx++)
	{
		if (hDrawText->m_ft_face[nIndx] != NULL)
			break;
	}

	if (nIndx == E_FT_FACE_NUM)
	{
		TRACEX(DTB_LOG_ERROR, "[DevOutText:DrawTextPutString] FT_FACE is all null");
		return FALSE;
	}

	if (pstText == NULL)
	{
		TRACEX(DTB_LOG_ERROR, "[DevOutText:DrawTextPutString] TEXT is null");
		return FALSE;
	}

	sx = 2.0 / hDrawText->m_dwWndWidth;
	sy = 2.0 / hDrawText->m_dwWndWidth;

	// No OpenGL rendering
	sx = 1.0;
	sy = 1.0;

	//TRACE("[DevOutText:DrawTextPutString] block_num:%d, Egl_width:%d, Egl_height:%d, sx:%f, sy:%f", pstText->block_num, hDrawText->m_dwWndWidth, hDrawText->m_dwWndWidth, sx, sy);

	//float black[4] =	{ 0, 0, 0, 1 };
	//float red[4] =	{ 1, 0, 0, 1 };
	//float white[4] =	{ 1, 1, 1, 1 };
	//float transparent_green[4] = { 0, 1, 0, 0.5 };

	dwFontSize = hDrawText->m_dwTextSize;
	//dwOutlineSize = hDrawText->m_dwTextOutlineSize;
	eOutlineType = hDrawText->m_eOutlineType;

	fBaseX = hDrawText->m_dwTextLeft;
	fBaseY = hDrawText->m_dwTextBottom;
	fBaseYGap = hDrawText->m_dwTextYPosGap;

	//dwFontSize = 40;
	//TRACE("[DevOutText:DrawTextPutString] FontSize (%d : %f)", dwFontSize, hDrawText->m_fScreenRatio);

	for (nIndx=0; nIndx<E_FT_FACE_NUM; nIndx++)
	{
		if (hDrawText->m_ft_face[nIndx])
		{
			FT_Set_Pixel_Sizes(hDrawText->m_ft_face[nIndx], 0, dwFontSize);

			//FT_Set_Char_Size(hDrawText->m_ft_face[nIndx], 0, dwFontSize*64, 0, 240);
			//FT_Set_Char_Size(hDrawText->m_ft_face[nIndx], 0, dwFontSize*64, hDrawText->m_fScreenXdpi, hDrawText->m_fScreenYdpi);

			//TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextPutString] FT:%d : (0x%x : %d)", nIndx, hDrawText->m_ft_face[nIndx], dwFontSize);
		}
	}

	// limit the number of text blocks to be rendered
	if (MAX_TEXT_SUBBLOCK < pstText->block_num)
		pstText->block_num = MAX_TEXT_SUBBLOCK;

	// Text Window Size
	nTextSurfaceWidth = hDrawText->m_dwWndWidth - (2 * fBaseX);
	hDrawText->m_dwBlockSize = 0;

	for (nIndx=0; nIndx < pstText->block_num; nIndx++)
	{
		nCurPoint = 0;
		nTotalTextNum = 0;

		// get utf32 string
		UtilUniUTF16toUTF32(pstText->block[nIndx].data, sizeof(pstText->block[nIndx].data), pstText->block[nIndx].ulData, sizeof(pstText->block[nIndx].ulData));

		//TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextPutString] block_num(%d:%d)", nIndx+1, pstText->block_num);
		while (1)
		{
			if (MAX_TEXT_SUBBLOCK < hDrawText->m_dwBlockSize)
				break;

			nTextWidth = 0;
			nTextHeight = 0;
			nTextNum = 0;

			//nTextSurfaceWidth = 400;
			// Measure the dimension of rendered texts
			measure_text(hDrawText, (const UTF32*)&(pstText->block[nIndx].ulData[nCurPoint]), nTextSurfaceWidth, &nTextWidth, &nTextHeight, &nTextNum);
			if (nTextNum == 0)
			{
				//TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextPutString] It's weired (0x%x) !!!", pstText->block[nIndx].ulData[nCurPoint]);
				break;
			}

			// Store the width and height of the rendering of the current text block
			hDrawText->m_dwBlockDataWidth[hDrawText->m_dwBlockSize] = nTextWidth;
			hDrawText->m_dwBlockDataHeight[hDrawText->m_dwBlockSize] = nTextHeight;
			nTotalTextNum += nTextNum;
			
			// when the current block cannot be rendered in a line, its rendering will be done in a multiple of lines
			if (nTotalTextNum < pstText->block[nIndx].len)
			{
				dwSize = nTextNum * sizeof(UTF32);
				memset(hDrawText->m_szBlockData[hDrawText->m_dwBlockSize], 0x00, MAX_STRING_LENGTH*sizeof(UTF32));
				memcpy(hDrawText->m_szBlockData[hDrawText->m_dwBlockSize], &(pstText->block[nIndx].ulData[nCurPoint]), dwSize);

				hDrawText->m_dwBlockSize += 1;
				nCurPoint = nTotalTextNum;

				//TRACEX(DTB_LOG_TRACE, "[DevOutText:DrawTextPutString] 1:(block_num:%d/%d) TextNum(%d:%d:%d), CurPoint:%d", nIndx+1, pstText->block_num, nTextNum, nTotalTextNum, pstText->block[nIndx].len, nCurPoint);
				continue;
			}
			// the current text block is done
			else if (0 < nTextNum)
			{
				dwSize = nTextNum * sizeof(UTF32);
				memset(hDrawText->m_szBlockData[hDrawText->m_dwBlockSize], 0x00, MAX_STRING_LENGTH*sizeof(UTF32));
				memcpy(hDrawText->m_szBlockData[hDrawText->m_dwBlockSize], &(pstText->block[nIndx].ulData[nCurPoint]), dwSize);

				hDrawText->m_dwBlockSize += 1;

				//TRACEX(DTB_LOG_TRACE, "[DevOutText:DrawTextPutString] 2:(block_num:%d/%d) TextNum(%d:%d:%d)", nIndx+1, pstText->block_num, nTextNum, nTotalTextNum, pstText->block[nIndx].len);
				break;
			}
		}
	}

	switch (eOutlineType)
	{
	case OUTLINE_NONE :
		dwOutlineSize = 0;
		fCharGap = 1.0;
		break;

	case OUTLINE_THIN :
		dwOutlineSize = 2;
		//dwOutlineSize = (DWORD32)( 2.0 * hDrawText->m_fScreenRatio + 0.5);
		fCharGap = 1.0;		// 1.0(3*3), 0.5(5*5), 0.33(7*7), 0.25(9*9), 0.2(11*11), 0.1(19*19)
		break;

	case OUTLINE_NORMAL :
		dwOutlineSize = 4;
		//dwOutlineSize = (DWORD32)( 4.0 * hDrawText->m_fScreenRatio + 0.5);
		fCharGap = 0.5;
		break;

	case OUTLINE_THICK :
		dwOutlineSize = 6;
		//dwOutlineSize = (DWORD32)( 6.0 * hDrawText->m_fScreenRatio + 0.5);
		fCharGap = 0.25;
		//fBaseYGap *= 1.2;
		break;

	default :
		dwOutlineSize = 4;
		//dwOutlineSize = (DWORD32)( 4.0 * hDrawText->m_fScreenRatio + 0.5);
		fCharGap = 0.5;
		break;
	}

	//TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextPutString] BlockSize(%d), OT(%d:%d), CG(%f)", hDrawText->m_dwBlockSize, eOutlineType, dwOutlineSize, fCharGap);

	fBaseY = hDrawText->m_dwWndHeight - hDrawText->m_dwTextBottom;


	// Rendering
	for (nIndx=hDrawText->m_dwBlockSize-1; 0 <= nIndx; nIndx--)
	{
		nTextWidth = hDrawText->m_dwBlockDataWidth[nIndx];
		nTextHeight = hDrawText->m_dwBlockDataHeight[nIndx];

		if (nTextWidth <= hDrawText->m_dwWndWidth)
			fCurX = (hDrawText->m_dwWndWidth - nTextWidth) / 2; // 가운데 정렬
		else
			fCurX = fBaseX;

		//fCurX += fBaseX;
		//fCurY = fBaseY + nTextHeight;
		fCurY = fBaseY;

		//TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextPutString] (%d), CX:%f, CY:%f, nTextWidth:%d, nTextHeight:%d, EglWidth:%d, EglHeight:%d", nIndx, fCurX, fCurY, nTextWidth, nTextHeight, hDrawText->m_dwWndWidth, hDrawText->m_dwWndWidth);

		//dwOutlineSize = 6;

		//if (dwOutlineSize)
		{
			fIndxX = fIndxY = 0.0;

			//glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
			//for (fIndxY=-1.0; fIndxY <= 1.0; fIndxY+=fCharGap)
			{
				//for (fIndxX=-1.0; fIndxX <= 1.0; fIndxX+=fCharGap)
				{
					if (eOutlineType != OUTLINE_THIN)
					{
						if (abs(fIndxX)==1.0 && abs(fIndxY)==1.0)
							continue;
					}

					//TRACEX(DTB_LOG_TRACE, "[DevOutText:DrawTextPutString] X(%f:%f) : Y(%f:%f)", fIndxX, fIndxX*dwOutlineSize, fIndxY, fIndxY*dwOutlineSize);

					if (pMedia)
						render_text(hDrawText, hDrawText->m_szBlockData[nIndx], pMedia->m_pY, nTextHeight, (DWORD32)(-1 + (fCurX+ (fIndxX*dwOutlineSize))), (DWORD32)(-1 + (fCurY+ (fIndxY*dwOutlineSize))), pMedia->m_dwPitch);
					//McSleep(0);
				}
			}
		}

		if (!dwOutlineSize)
		{
			//glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
			//render_text(hDrawText, hDrawText->m_szBlockData[nIndx], -1 + (fCurX) * sx, -1 + (fCurY) * sy, sx, sy);
		}

		//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//glUniform4fv(hDrawText->m_nUniform_color, 1, hDrawText->m_ftColor /*white*/);
		//render_text(hDrawText, hDrawText->m_szBlockData[nIndx], -1 + (fCurX) * sx, -1 + (fCurY) * sy, sx, sy);

		fBaseY -= nTextHeight + fBaseYGap;
	}

	/*if (eglSwapBuffers(hDrawText->mDisplay, hDrawText->mSurface) != EGL_TRUE)
	{
		TRACEX(DTB_LOG_ERROR, "[DevOutText:DrawTextPutString] %d swapping buffers.", eglGetError());
		return FALSE;
	}*/

	dwCurTime = McGetTickCount();
	TRACEX(DTB_LOG_TRACE, "[DevOutText:DrawTextPutString] end (%d)", McGetSubtract(dwCurTime, dwBaseTime));
	//TRACEX(DTB_LOG_DEBUG, "[DevOutText:DrawTextPutString] end (%d)", McGetSubtract(dwCurTime, dwBaseTime));

	return	TRUE;
}

BOOL DrawTextPutString4(DrawTextHandle hHandle, CHAR* pszText, McMediaVideo *pMedia)
{
	DrawTextStruct*	hDrawText = (DrawTextStruct*)hHandle;
	McMediaText		TextMedia = { 0, }, 
					*pTextMedia = &TextMedia;
	
	if (!hHandle || !pszText || !pMedia)
		return FALSE;


	INT32		nLen = strlen(pszText);
	CHAR		*pCur = pszText, *pNew;
	INT32		nBlock = 0;

	while(*pCur)
	{
		pNew = strchr(pCur, '\n');
		if (pNew == NULL)
		{
			TEXT_PART	*pTextBlock = &pTextMedia->block[pTextMedia->block_num];

			pTextBlock->len = UtilUniANSI2UTF16(pCur, (pNew - pCur + 1), pTextBlock->data, sizeof(pTextBlock->data), DTB_CP_SYS);
			pTextMedia->block_num++;
			pTextBlock->data[pTextBlock->len] = 0;
			break;
		}
		else
		{
			TEXT_PART	*pTextBlock = &pTextMedia->block[pTextMedia->block_num];

			pTextBlock->len = UtilUniANSI2UTF16(pCur, (pNew - pCur + 1), pTextBlock->data, sizeof(pTextBlock->data), DTB_CP_SYS);
			pTextMedia->block_num++;
			pCur = pNew + 1;
		}
	}

	return	DrawTextPutString(hHandle, pTextMedia, pMedia);
}

BOOL DrawTextPutString2(DrawTextHandle hHandle, McMediaText* pstText)
{
	DrawTextStruct*	hDrawText = (DrawTextStruct*)hHandle;

	int nIndx = 0;

	float   fIndxX = 0.0;
	float	fIndxY = 0.0;
	float   fCharGap = 0.0;

	float sx = 0;
	float sy = 0;

	int nTextSurfaceWidth = 0;
	int nTextWidth= 0, nTextHeight = 0, nTextNum = 0;
	int nCurPoint = 0;
	int nTotalTextNum = 0;

	DWORD32	dwFontSize = 0;

	float fBaseX = 0, fBaseY = 0, fBaseYGap = 0;
	float fCurX = 0, fCurY = 0;

	OUTLINE_TYPE eOutlineType = OUTLINE_NONE;
	DWORD32 dwOutlineSize = 0;
	DWORD32 dwSize = 0;
	BOOL	bBold = FALSE;
	BOOL	bShadow = FALSE;

	DWORD32	dwBaseTime = 0;
	DWORD32 dwCurTime = 0;

	float black[4] =	{ 0, 0, 0, 1 };
	float red[4] =	{ 1, 0, 0, 1 };
	float white[4] =	{ 1, 1, 1, 1 };
	float transparent_green[4] = { 0, 1, 0, 0.5 };

	if (!hDrawText)
		return	FALSE;

	bBold = hDrawText->m_bBold;
	bShadow = hDrawText->m_bShadow;

	dwBaseTime = McGetTickCount();
	TRACEX(DTB_LOG_TRACE, "[DevOutText:DrawTextPutString2] begin");

	if (!hDrawText->m_dwWndWidth || !hDrawText->m_dwWndWidth)
	{
		TRACEX(DTB_LOG_ERROR, "[DevOutText:DrawTextPutString2] Egl_width:%d, Egl_height:%d", hDrawText->m_dwWndWidth, hDrawText->m_dwWndWidth);
		return FALSE;
	}

	for (nIndx=0; nIndx<E_FT_FACE_NUM; nIndx++)
	{
		if (hDrawText->m_ft_face[nIndx] != NULL)
			break;
	}

	if (nIndx == E_FT_FACE_NUM)
	{
		TRACEX(DTB_LOG_ERROR, "[DevOutText:DrawTextPutString2] FT_FACE is all null");
		return FALSE;
	}

	if (pstText == NULL)
	{
		TRACEX(DTB_LOG_ERROR, "[DevOutText:DrawTextPutString2] TEXT is null");
		return FALSE;
	}

	sx = 2.0 / hDrawText->m_dwWndWidth;
	sy = 2.0 / hDrawText->m_dwWndWidth;

	TRACE("[DevOutText:DrawTextPutString2] block_num:%d, Egl_width:%d, Egl_height:%d, sx:%f, sy:%f", pstText->block_num, hDrawText->m_dwWndWidth, hDrawText->m_dwWndWidth, sx, sy);


	dwFontSize = hDrawText->m_dwTextSize;
	eOutlineType = hDrawText->m_eOutlineType;

	fBaseX = hDrawText->m_dwTextLeft;
	fBaseY = hDrawText->m_dwTextBottom;
	fBaseYGap = hDrawText->m_dwTextYPosGap;

	//dwFontSize = 40;
	//TRACE("[DevOutText:DrawTextPutString2] FontSize (%d : %f)", dwFontSize, hDrawText->m_fScreenRatio);

	for (nIndx=0; nIndx<E_FT_FACE_NUM; nIndx++)
	{
		if (hDrawText->m_ft_face[nIndx])
		{
			FT_Set_Pixel_Sizes(hDrawText->m_ft_face[nIndx], 0, dwFontSize);

			//FT_Set_Char_Size(hDrawText->m_ft_face[nIndx], 0, dwFontSize*64, 0, 240);
			//FT_Set_Char_Size(hDrawText->m_ft_face[nIndx], 0, dwFontSize*64, hDrawText->m_fScreenXdpi, hDrawText->m_fScreenYdpi);

			TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextPutString2] FT:%d : (0x%x : %d)", nIndx, hDrawText->m_ft_face[nIndx], dwFontSize);
		}
	}


	hDrawText->m_dwBlockSize = 0;

	nTextSurfaceWidth = hDrawText->m_dwWndWidth - (2*fBaseX);

	if (pstText->block_num > MAX_TEXT_SUBBLOCK)
		pstText->block_num = MAX_TEXT_SUBBLOCK;

	for (nIndx=0; nIndx < pstText->block_num; nIndx++)
	{
		nCurPoint = 0;
		nTotalTextNum = 0;

		TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextPutString2] block_num(%d:%d)", nIndx+1, pstText->block_num);
		while (1)
		{
			if (hDrawText->m_dwBlockSize > MAX_TEXT_SUBBLOCK)
				break;

			nTextWidth = 0;
			nTextHeight = 0;
			nTextNum = 0;

			//nTextSurfaceWidth = 400;
			//TRACEX(DTB_LOG_TRACE, "[DevOutText:DrawTextPutString2] C:0x%x, CurPoint:%d, TextSurfaceWidth:%d", pstText->block[nIndx].ulData[nCurPoint], nCurPoint, nTextSurfaceWidth);
			measure_text(hDrawText, (const UTF32*)&(pstText->block[nIndx].ulData[nCurPoint]), nTextSurfaceWidth, &nTextWidth, &nTextHeight, &nTextNum);

			hDrawText->m_dwBlockDataWidth[hDrawText->m_dwBlockSize] = nTextWidth;
			hDrawText->m_dwBlockDataHeight[hDrawText->m_dwBlockSize] = nTextHeight;

			nTotalTextNum += nTextNum;
			if (nTextNum == 0)
			{
				TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextPutString2] It's weired (0x%x) !!!", pstText->block[nIndx].ulData[nCurPoint]);
				break;
			}
			else if (pstText->block[nIndx].len > nTotalTextNum)
			{
				dwSize = nTextNum * sizeof(UTF32);
				memset(hDrawText->m_szBlockData[hDrawText->m_dwBlockSize], 0x00, MAX_STRING_LENGTH*sizeof(UTF32));
				memcpy(hDrawText->m_szBlockData[hDrawText->m_dwBlockSize], &(pstText->block[nIndx].ulData[nCurPoint]), dwSize);

				hDrawText->m_dwBlockSize += 1;
				nCurPoint = nTotalTextNum;

				//TRACEX(DTB_LOG_TRACE, "[DevOutText:DrawTextPutString2] 1:(block_num:%d/%d) TextNum(%d:%d:%d), CurPoint:%d", nIndx+1, pstText->block_num, nTextNum, nTotalTextNum, pstText->block[nIndx].len, nCurPoint);
				continue;
			}
			else if (nTextNum > 0)
			{
				dwSize = nTextNum * sizeof(UTF32);
				memset(hDrawText->m_szBlockData[hDrawText->m_dwBlockSize], 0x00, MAX_STRING_LENGTH*sizeof(UTF32));
				memcpy(hDrawText->m_szBlockData[hDrawText->m_dwBlockSize], &(pstText->block[nIndx].ulData[nCurPoint]), dwSize);

				hDrawText->m_dwBlockSize += 1;

				//TRACEX(DTB_LOG_TRACE, "[DevOutText:DrawTextPutString2] 2:(block_num:%d/%d) TextNum(%d:%d:%d)", nIndx+1, pstText->block_num, nTextNum, nTotalTextNum, pstText->block[nIndx].len);
				break;
			}
		}
	}



	dwOutlineSize = hDrawText->m_dwTextSize / 10;
	switch (eOutlineType)
	{
	case OUTLINE_NONE :
		dwOutlineSize = 0;
		fCharGap = 1.0;
		break;

	case OUTLINE_THIN :
		//dwOutlineSize = 2;
		//dwOutlineSize = (DWORD32)( dwOutlineSize * hDrawText->m_fScreenRatio);
		fCharGap = 1.0;		// 1.0(3*3), 0.5(5*5), 0.33(7*7), 0.25(9*9), 0.2(11*11), 0.1(19*19)

		dwOutlineSize -= 2;
		if ((int)dwOutlineSize < 1)
			dwOutlineSize = 1;

		break;

	case OUTLINE_NORMAL :
		//dwOutlineSize = 3;
		//dwOutlineSize = (DWORD32)( dwOutlineSize * hDrawText->m_fScreenRatio);
		fCharGap = 0.5;

		if ((int)dwOutlineSize < 2)
			dwOutlineSize = 2;

		break;

	case OUTLINE_THICK :
		//dwOutlineSize = 4;
		//dwOutlineSize = (DWORD32)( dwOutlineSize * hDrawText->m_fScreenRatio);
		fCharGap = 0.25;
		//fBaseYGap *= 1.2;

		dwOutlineSize += 2;

		if ((int)dwOutlineSize < 3)
			dwOutlineSize = 3;

		break;

	default :
		//dwOutlineSize = 3;
		//dwOutlineSize = (DWORD32)( dwOutlineSize * hDrawText->m_fScreenRatio);
		fCharGap = 0.5;

		if ((int)dwOutlineSize < 2)
			dwOutlineSize = 2;

		break;
	}

	TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextPutString2] BlockSize(%d), OT(%d:%d), CG(%f)", hDrawText->m_dwBlockSize, eOutlineType, dwOutlineSize, fCharGap);

	for (nIndx=hDrawText->m_dwBlockSize-1; nIndx >= 0; nIndx--)
	{
		nTextWidth = hDrawText->m_dwBlockDataWidth[nIndx];
		nTextHeight = hDrawText->m_dwBlockDataHeight[nIndx];

		if (nTextWidth <= hDrawText->m_dwWndWidth)
			fCurX = (hDrawText->m_dwWndWidth- nTextWidth) / 2; // 가운데 정렬
		else
			fCurX = fBaseX;

		//fCurX += fBaseX;
		//fCurY = fBaseY + nTextHeight;
		fCurY = fBaseY;

		TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextPutString2] (%d), CX:%f, CY:%f, nTextWidth:%d, nTextHeight:%d, EglWidth:%d, EglHeight:%d", nIndx, fCurX, fCurY, nTextWidth, nTextHeight, hDrawText->m_dwWndWidth, hDrawText->m_dwWndWidth);


		//dwOutlineSize = 6;

		//render_text2(hDrawText, hDrawText->m_szBlockData[nIndx], -1 + (fCurX) * sx, -1 + (fCurY) * sy, sx, sy, hDrawText->m_ftColor, dwOutlineSize, bBold, bShadow);

		fBaseY += nTextHeight + fBaseYGap;
	}

	dwCurTime = McGetTickCount();
	TRACEX(DTB_LOG_TRACE, "[DevOutText:DrawTextPutString2] end (%d)", McGetSubtract(dwCurTime, dwBaseTime));

	return	TRUE;
}

BOOL DrawTextPutString3(DrawTextHandle hHandle, McMediaText* pstText)
{
	DrawTextStruct*	hDrawText = (DrawTextStruct*)hHandle;

	int nIndx = 0, nIndx2 = 0, nIndx3 = 0, nIndx4 = 0;
	int nCurBlockIndex = 0, nCurBlockSize = 0;;

	float   fIndxX = 0.0;
	float	fIndxY = 0.0;
	float   fCharGap = 0.0;

	float sx = 0;
	float sy = 0;

	int nTextSurfaceWidth = 0;
	int nTextWidth= 0, nTextHeight = 0, nTextNum = 0, nCurTextWidth = 0;
	int nCurPoint = 0;
	int nTotalTextNum = 0;

	DWORD32	dwFontSize = 0;

	float fBaseX = 0, fBaseY = 0, fBaseYGap = 0;
	float fCurX = 0, fCurY = 0;

	OUTLINE_TYPE eOutlineType = OUTLINE_NONE;
	DWORD32 dwOutlineSize = 0;
	DWORD32 dwSize = 0;
	BOOL	bBold = FALSE;
	BOOL	bShadow = FALSE;

	DWORD32	dwBaseTime = 0;
	DWORD32 dwCurTime = 0;

	if (!hDrawText)
		return	FALSE;

	bBold = hDrawText->m_bBold;
	bShadow = hDrawText->m_bShadow;

	dwBaseTime = McGetTickCount();
	TRACEX(DTB_LOG_TRACE, "[DevOutText:DrawTextPutString3] begin");



	if (!hDrawText->m_dwWndWidth || !hDrawText->m_dwWndWidth)
	{
		TRACEX(DTB_LOG_ERROR, "[DevOutText:DrawTextPutString3] Egl_width:%d, Egl_height:%d", hDrawText->m_dwWndWidth, hDrawText->m_dwWndWidth);
		return FALSE;
	}

	for (nIndx=0; nIndx<E_FT_FACE_NUM; nIndx++)
	{
		if (hDrawText->m_ft_face[nIndx] != NULL)
			break;
	}

	if (nIndx == E_FT_FACE_NUM)
	{
		TRACEX(DTB_LOG_ERROR, "[DevOutText:DrawTextPutString3] FT_FACE is all null");
		return FALSE;
	}

	if (pstText == NULL)
	{
		TRACEX(DTB_LOG_ERROR, "[DevOutText:DrawTextPutString3] TEXT is null");
		return FALSE;
	}

	sx = 2.0 / hDrawText->m_dwWndWidth;
	sy = 2.0 / hDrawText->m_dwWndWidth;

	TRACE("[DevOutText:DrawTextPutString3] block_num:%d, Egl_width:%d, Egl_height:%d, sx:%f, sy:%f", pstText->block_num, hDrawText->m_dwWndWidth, hDrawText->m_dwWndWidth, sx, sy);


	dwFontSize = hDrawText->m_dwTextSize;
	eOutlineType = hDrawText->m_eOutlineType;

	fBaseX = hDrawText->m_dwTextLeft;
	fBaseY = hDrawText->m_dwTextBottom;
	fBaseYGap = hDrawText->m_dwTextYPosGap;

	//dwFontSize = 40;
	//TRACE("[DevOutText:DrawTextPutString2] FontSize (%d : %f)", dwFontSize, hDrawText->m_fScreenRatio);

	for (nIndx=0; nIndx<E_FT_FACE_NUM; nIndx++)
	{
		if (hDrawText->m_ft_face[nIndx])
		{
			FT_Set_Pixel_Sizes(hDrawText->m_ft_face[nIndx], 0, dwFontSize);

			//FT_Set_Char_Size(hDrawText->m_ft_face[nIndx], 0, dwFontSize*64, 0, 240);
			//FT_Set_Char_Size(hDrawText->m_ft_face[nIndx], 0, dwFontSize*64, hDrawText->m_fScreenXdpi, hDrawText->m_fScreenYdpi);

			TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextPutString3] FT:%d : (0x%x : %d)", nIndx, hDrawText->m_ft_face[nIndx], dwFontSize);
		}
	}


	hDrawText->m_dwBlockSize = 0;

	nTextSurfaceWidth = hDrawText->m_dwWndWidth - (2*fBaseX);

	if (pstText->block_num > MAX_TEXT_SUBBLOCK)
		pstText->block_num = MAX_TEXT_SUBBLOCK;

	for (nIndx=0; nIndx < pstText->block_num; nIndx++)
	{
		nCurPoint = 0;
		nTotalTextNum = 0;

		TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextPutString3] block_num(%d:%d) (%d:%d, %f:%f:%f)", nIndx+1, pstText->block_num, pstText->block[nIndx].bBold, pstText->block[nIndx].bLineTerminator, pstText->block[nIndx].gl_color.fRed, pstText->block[nIndx].gl_color.fGreen, pstText->block[nIndx].gl_color.fBlue);
		while (1)
		{
			if (hDrawText->m_dwBlockSize > MAX_TEXT_SUBBLOCK)
				break;

			nTextWidth = 0;
			nTextHeight = 0;
			nTextNum = 0;

			//nTextSurfaceWidth = 400;
			//TRACEX(DTB_LOG_TRACE, "[DevOutText:DrawTextPutString3] C:0x%x, CurPoint:%d, TextSurfaceWidth:%d", pstText->block[nIndx].ulData[nCurPoint], nCurPoint, nTextSurfaceWidth);
			measure_text(hDrawText, (const UTF32*)&(pstText->block[nIndx].ulData[nCurPoint]), nTextSurfaceWidth-nCurTextWidth, &nTextWidth, &nTextHeight, &nTextNum);

			hDrawText->m_dwBlockDataWidth[hDrawText->m_dwBlockSize] = nTextWidth;
			hDrawText->m_dwBlockDataHeight[hDrawText->m_dwBlockSize] = nTextHeight;

			hDrawText->m_bBlockDataCR[hDrawText->m_dwBlockSize] = pstText->block[nIndx].bLineTerminator;
			hDrawText->m_bBlockDataBold[hDrawText->m_dwBlockSize] = pstText->block[nIndx].bBold;
			memcpy(hDrawText->m_ftBlockDataFontColor[hDrawText->m_dwBlockSize], &pstText->block[nIndx].gl_color, sizeof(FONT_COLOR));

			nTotalTextNum += nTextNum;
			if (nTextNum == 0)
			{
				TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextPutString3] It's weired (0x%x) !!!", pstText->block[nIndx].ulData[nCurPoint]);
				nCurTextWidth = 0;
				break;
			}
			else if (pstText->block[nIndx].len > nTotalTextNum)
			{
				dwSize = nTextNum * sizeof(UTF32);
				memset(hDrawText->m_szBlockData[hDrawText->m_dwBlockSize], 0x00, MAX_STRING_LENGTH*sizeof(UTF32));
				memcpy(hDrawText->m_szBlockData[hDrawText->m_dwBlockSize], &(pstText->block[nIndx].ulData[nCurPoint]), dwSize);

				hDrawText->m_bBlockDataCR[hDrawText->m_dwBlockSize] = TRUE;
				nCurTextWidth = 0;

				nCurPoint = nTotalTextNum;

				//TRACEX(DTB_LOG_TRACE, "[DevOutText:DrawTextPutString3] 1:(block_num:%d/%d) TextNum(%d:%d:%d), CurPoint:%d", nIndx+1, pstText->block_num, nTextNum, nTotalTextNum, pstText->block[nIndx].len, nCurPoint);

				hDrawText->m_dwBlockSize += 1;
				continue;
			}
			else if (nTextNum > 0)
			{
				dwSize = nTextNum * sizeof(UTF32);
				memset(hDrawText->m_szBlockData[hDrawText->m_dwBlockSize], 0x00, MAX_STRING_LENGTH*sizeof(UTF32));
				memcpy(hDrawText->m_szBlockData[hDrawText->m_dwBlockSize], &(pstText->block[nIndx].ulData[nCurPoint]), dwSize);

				if (hDrawText->m_bBlockDataCR[hDrawText->m_dwBlockSize])
					nCurTextWidth = 0;
				else
					nCurTextWidth += nTextWidth;

				//TRACEX(DTB_LOG_TRACE, "[DevOutText:DrawTextPutString3] 2:(block_num:%d/%d) TextNum(%d:%d:%d:%d) (%d:%d)", nIndx+1, pstText->block_num, nTextNum, nTotalTextNum, pstText->block[nIndx].len, nTextWidth, hDrawText->m_bBlockDataCR[hDrawText->m_dwBlockSize], nCurTextWidth);

				hDrawText->m_dwBlockSize += 1;
				break;
			}
		}
	}



	dwOutlineSize = hDrawText->m_dwTextSize / 10;
	switch (eOutlineType)
	{
	case OUTLINE_NONE :
		dwOutlineSize = 0;
		fCharGap = 1.0;
		break;

	case OUTLINE_THIN :
		//dwOutlineSize = 2;
		//dwOutlineSize = (DWORD32)( dwOutlineSize * hDrawText->m_fScreenRatio);
		fCharGap = 1.0;		// 1.0(3*3), 0.5(5*5), 0.33(7*7), 0.25(9*9), 0.2(11*11), 0.1(19*19)

		dwOutlineSize -= 2;
		if ((int)dwOutlineSize < 1)
			dwOutlineSize = 1;

		break;

	case OUTLINE_NORMAL :
		//dwOutlineSize = 3;
		//dwOutlineSize = (DWORD32)( dwOutlineSize * hDrawText->m_fScreenRatio);
		fCharGap = 0.5;

		if ((int)dwOutlineSize < 2)
			dwOutlineSize = 2;

		break;

	case OUTLINE_THICK :
		//dwOutlineSize = 4;
		//dwOutlineSize = (DWORD32)( dwOutlineSize * hDrawText->m_fScreenRatio);
		fCharGap = 0.25;
		//fBaseYGap *= 1.2;

		dwOutlineSize += 2;

		if ((int)dwOutlineSize < 3)
			dwOutlineSize = 3;

		break;

	default :
		//dwOutlineSize = 3;
		//dwOutlineSize = (DWORD32)( dwOutlineSize * hDrawText->m_fScreenRatio);
		fCharGap = 0.5;

		if ((int)dwOutlineSize < 2)
			dwOutlineSize = 2;

		break;
	}

	TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextPutString3] BlockSize(%d), OT(%d:%d), CG(%f)", hDrawText->m_dwBlockSize, eOutlineType, dwOutlineSize, fCharGap);

	for (nIndx=hDrawText->m_dwBlockSize-1; nIndx >= 0; nIndx--)
	{
		nTextWidth = 0;
		nTextHeight = 0;

		// 하나의 라인을 이루는 전체 블럭 찾기
		nCurBlockIndex = nIndx;
		for (nIndx2=nIndx; nIndx2 >= 0; nIndx2--)
		{
			if (nIndx2 == nIndx)
			{
				nTextWidth += hDrawText->m_dwBlockDataWidth[nIndx2];

				TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextPutString3] Bottom block of Current Line (%d : %d)", nIndx, hDrawText->m_dwBlockDataWidth[nIndx2]);
				continue;
			}

			// 위 라인 마지막 블럭
			if ( hDrawText->m_bBlockDataCR[nIndx2] == TRUE )
			{
				nCurBlockIndex = nIndx2 + 1;

				TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextPutString3] Top block of Current Line (%d : %d)", nCurBlockIndex, hDrawText->m_dwBlockDataWidth[nIndx2]);
				break;
			}
			else if (nIndx2 == 0)
			{
				nCurBlockIndex = 0;
			}

			nTextWidth += hDrawText->m_dwBlockDataWidth[nIndx2];
		}
		nCurBlockSize = nIndx - nCurBlockIndex + 1;

		TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextPutString3] CurBlockSize(%d), CurBlockIndex(%d), TextWidth(%d)", nCurBlockSize, nCurBlockIndex, nTextWidth);

		//
		if (nTextWidth <= hDrawText->m_dwWndWidth)
			fCurX = (hDrawText->m_dwWndWidth- nTextWidth) / 2; // 가운데 정렬
		else
			fCurX = fBaseX;

		fCurY = fBaseY;

		// 하나의 라인을 이루는 전체 블럭 출력
		for (nIndx3=nCurBlockIndex; nIndx3<nCurBlockSize+nCurBlockIndex; nIndx3++)
		{
			if (nTextHeight < hDrawText->m_dwBlockDataHeight[nIndx3])
				nTextHeight = hDrawText->m_dwBlockDataHeight[nIndx3];

			//TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextPutString3] (%d:%d), (%d, %d), CX:%f, CY:%f, nTextWidth:%d, nTextHeight:%d, EglWidth:%d, EglHeight:%d", nIndx, nIndx3, hDrawText->m_bBlockDataBold[nIndx3], hDrawText->m_bBlockDataCR[nIndx3], fCurX, fCurY, hDrawText->m_dwBlockDataWidth[nIndx3], hDrawText->m_dwBlockDataHeight[nIndx3], hDrawText->m_dwWndWidth, hDrawText->m_dwWndWidth);

			render_text2(hDrawText, hDrawText->m_szBlockData[nIndx3], -1 + (fCurX) * sx, -1 + (fCurY) * sy, sx, sy, hDrawText->m_ftBlockDataFontColor[nIndx3], dwOutlineSize, (hDrawText->m_bBlockDataBold[nIndx3])|(bBold), bShadow);
			fCurX += hDrawText->m_dwBlockDataWidth[nIndx3];
		}

		fBaseY += nTextHeight + fBaseYGap;

		nIndx -= (nCurBlockSize - 1);
	}

	dwCurTime = McGetTickCount();
	TRACEX(DTB_LOG_TRACE, "[DevOutText:DrawTextPutString3] end (%d)", McGetSubtract(dwCurTime, dwBaseTime));

	return	TRUE;
}

BOOL DrawTextDisplayString(DrawTextHandle hHandle)
{
	DrawTextStruct*	hDrawText = (DrawTextStruct*)hHandle;

	return TRUE;
}

BOOL DrawTextReset(DrawTextHandle hHandle)
{
	DrawTextStruct*	hDrawText = (DrawTextStruct*)hHandle;

	if (!hDrawText)
		return	FALSE;

	TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextReset] begin");

	if (hDrawText->m_bFtInitialize)
	{
		McMediaText stText;

		//glClearColor(0, 0, 0, 0);
		//glClear(GL_COLOR_BUFFER_BIT);

		stText.block_num = 1;
		stText.block->len = 1;
		stText.block->ulData[0] = 0x20;
		DrawTextPutString(hDrawText, &stText, NULL);
	}

	TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextReset] end");

	return TRUE;
}

BOOL DrawTextSetUserFont(DrawTextHandle hHandle, char *szUserFontName)
{
	DrawTextStruct*	hDrawText = (DrawTextStruct*)hHandle;

	FT_Face		ft_face;

	if (!hDrawText)
		return	FALSE;

	if (hDrawText->m_bFtInitialize == FALSE)
	{
		TRACEX(DTB_LOG_WARN, "[DevOutText:DrawTextSetUserFont] FT is not initialized !!!");
		return FALSE;
	}

	TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextSetUserFont] begin : UserFont:%s", szUserFontName);

	//hDrawText->m_bBold = bBold;
	//hDrawText->m_bUserDefineColor = bUserDefineColor;
	//hDrawText->m_dwUserDefineColor = dwUserDefineColor;

	if (szUserFontName && strlen(szUserFontName))
	{
		/* Load a font */
		if (FT_New_Face(hDrawText->m_ft_library, szUserFontName, 0, &ft_face))
		{
			TRACEX(DTB_LOG_ERROR, "[DevOutText:DrawTextSetUserFont]	Could not open user font");
			return FALSE;
		}
		else
		{
			if (hDrawText->m_ft_face[USER_FT_FACE])
			{
				TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextSetUserFont] Release USER_FT_FACE (0x%x)", hDrawText->m_ft_face[USER_FT_FACE]);
				FT_Done_Face(hDrawText->m_ft_face[USER_FT_FACE]);
				hDrawText->m_ft_face[USER_FT_FACE] = NULL;
			}

			hDrawText->m_ft_face[USER_FT_FACE] = ft_face;
			TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextSetUserFont] NEW USER_FT_FACE (0x%x)", hDrawText->m_ft_face[USER_FT_FACE]);

			strcpy(hDrawText->m_szFont, szUserFontName);

			TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextSetUserFont]	charmap : %d", hDrawText->m_ft_face[USER_FT_FACE]->charmap->encoding);
		}
	}
	else
	{
		if (hDrawText->m_ft_face[USER_FT_FACE])
		{
			TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextSetUserFont] Release USER_FT_FACE (0x%x)", hDrawText->m_ft_face[USER_FT_FACE]);
			FT_Done_Face(hDrawText->m_ft_face[USER_FT_FACE]);
			hDrawText->m_ft_face[USER_FT_FACE] = NULL;
		}
	}

	TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextSetUserFont] end");

	return TRUE;
}

BOOL DrawTextSetDefaultFont(DrawTextHandle hHandle, char *szDefaultFontName)
{
	DrawTextStruct*	hDrawText = (DrawTextStruct*)hHandle;

	FT_Face		ft_face;

	if (!hDrawText)
		return	FALSE;

	if (hDrawText->m_bFtInitialize == FALSE)
	{
		TRACEX(DTB_LOG_WARN, "[DevOutText:DrawTextSetDefaultFont] FT is not initialized !!!");
		return FALSE;
	}

	TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextSetDefaultFont] begin : D-Font:%s", szDefaultFontName);

	//hDrawText->m_bBold = bBold;
	//hDrawText->m_bUserDefineColor = bUserDefineColor;
	//hDrawText->m_dwUserDefineColor = dwUserDefineColor;

	if (szDefaultFontName && strlen(szDefaultFontName))
	{
		/* Load a font */
		if (FT_New_Face(hDrawText->m_ft_library, szDefaultFontName, 0, &ft_face))
		{
			TRACEX(DTB_LOG_ERROR, "[DevOutText:DrawTextSetDefaultFont]	Could not open default font");
			return FALSE;
		}
		else
		{
			if (hDrawText->m_ft_face[DEFAULT_FT_FACE])
			{
				TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextSetDefaultFont] Release DEFAULT_FT_FACE (0x%x)", hDrawText->m_ft_face[DEFAULT_FT_FACE]);
				FT_Done_Face(hDrawText->m_ft_face[DEFAULT_FT_FACE]);
				hDrawText->m_ft_face[DEFAULT_FT_FACE] = NULL;
			}

			hDrawText->m_ft_face[DEFAULT_FT_FACE] = ft_face;
			TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextSetDefaultFont] NEW DEFAULT_FT_FACE (0x%x)", hDrawText->m_ft_face[DEFAULT_FT_FACE]);

			strcpy(hDrawText->m_szFont, szDefaultFontName);

			TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextSetDefaultFont]	charmap : %d", hDrawText->m_ft_face[DEFAULT_FT_FACE]->charmap->encoding);
		}
	}
	else
	{
		if (hDrawText->m_ft_face[DEFAULT_FT_FACE])
		{
			TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextSetDefaultFont] Release DEFAULT_FT_FACE (0x%x)", hDrawText->m_ft_face[DEFAULT_FT_FACE]);
			FT_Done_Face(hDrawText->m_ft_face[DEFAULT_FT_FACE]);
			hDrawText->m_ft_face[DEFAULT_FT_FACE] = NULL;
		}
	}


	TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextSetDefaultFont] end");

	return TRUE;
}


BOOL IsAvailableFont(DrawTextHandle hHandle, char *pszFontName, UTF32 ulChar, FT_Face *pftType)
{
	DrawTextStruct*	hDrawText = (DrawTextStruct*)hHandle;
	FT_Face		ft_face;
	int			nCharIndex = 0;

	if (!hDrawText)
		return	FALSE;

	TRACEX(DTB_LOG_INFO, "[DevOutText:IsAvailableFont] begin : C:%04x : %s", ulChar, pszFontName);

	/* Load a font */
	if (FT_New_Face(hDrawText->m_ft_library, pszFontName, 0, &ft_face))
	{
		TRACEX(DTB_LOG_ERROR, "[DevOutText:DrawTextIsAvailableFont]	Could not open font %s", pszFontName);
		return FALSE;
	}
	else
	{
		nCharIndex = FT_Get_Char_Index(ft_face, ulChar);
		if (nCharIndex == 0) {
			TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextIsAvailableFont] undefined character code : [0x%04x]", ulChar);
			return FALSE;
		}
		else
			TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextIsAvailableFont] character code : %d (0x%x)", nCharIndex, ft_face);
	}

	if (pftType)
		*pftType = ft_face;

	TRACEX(DTB_LOG_INFO, "[DevOutText:IsAvailableFont] end");

	return TRUE;
}

BOOL DrawTextIsAvailableFont(DrawTextHandle hHandle, char *pszFontName, UTF32 ulChar)
{
	DrawTextStruct*	hDrawText = (DrawTextStruct*)hHandle;
	BOOL	bRet = TRUE;

	if (!hDrawText)
		return	FALSE;

	TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextIsAvailableFont] begin : C:%04x : %s", ulChar, pszFontName);

	if (hDrawText->m_bFtInitialize == FALSE)
	{
		TRACEX(DTB_LOG_WARN, "[DevOutText:DrawTextIsAvailableFont] FT is not initialized !!!");
		return FALSE;
	}

	bRet = IsAvailableFont(hDrawText, pszFontName, ulChar, NULL);

	TRACEX(DTB_LOG_INFO, "[DevOutText:DrawTextIsAvailableFont] end");

	return bRet;
}

BOOL DrawTextSetSurfaceChanged(DrawTextHandle hHandle, HWND hWnd)
{
	DrawTextStruct*	hDrawText = (DrawTextStruct*)hHandle;

	TRACE("[DevOutText:DrawTextSetSurfaceChanged] begin (0x%x) (0x%x)", hDrawText, hWnd);

	if (hDrawText)
	{
		hDrawText->m_hWnd = hWnd;
		GetWindowRect(hDrawText->m_hWnd, &hDrawText->m_rcWnd);
		hDrawText->m_dwWndWidth = hDrawText->m_rcWnd.right - hDrawText->m_rcWnd.left;
		hDrawText->m_dwWndHeight = hDrawText->m_rcWnd.bottom - hDrawText->m_rcWnd.top;

		TRACE("[DevOutText:DrawTextSetSurfaceChanged] end (0x%x) %dx%d", hDrawText, hDrawText->m_dwWndWidth, hDrawText->m_dwWndHeight);
	}
	else
		TRACE("[DevOutText:DrawTextSetSurfaceChanged] Null pointer end..");

	return TRUE;
}

BOOL DrawTextSetDisplayPos(DrawTextHandle hHandle, DWORD32 nLeft, DWORD32 nBottom, DWORD32 dwYPosGap, DWORD32 dwFontSize, OUTLINE_TYPE eOutlineType, BOOL bBold, BOOL bShadow)
{
	DrawTextStruct*	hDrawText = (DrawTextStruct*)hHandle;
	if (!hDrawText)
		return FALSE;

	TRACE("[DevOutText:DrawTextSetDisplayPos] begin (%d:%d:%d:%d:%d:%d:%d)", nLeft, nBottom, dwYPosGap, dwFontSize, eOutlineType, bBold, bShadow);
	//glViewport(x, y, x, h);

	//if (dwFontSize < 16 || dwFontSize > 60)
	//	dwFontSize = 24;

	//hDrawText->m_dwTextLeft = (DWORD32)( (float)nLeft * hDrawText->m_fScreenRatio + 0.5);
	//hDrawText->m_dwTextBottom = (DWORD32)( (float)nBottom * hDrawText->m_fScreenRatio + 0.5);
	//hDrawText->m_dwTextYPosGap = (DWORD32)( (float)dwYPosGap * hDrawText->m_fScreenRatio + 0.5);
	//hDrawText->m_dwTextSize = (DWORD32)( (float)dwFontSize * hDrawText->m_fScreenRatio + 0.5);

	hDrawText->m_dwTextLeft = (DWORD32)(nLeft);
	hDrawText->m_dwTextBottom = (DWORD32)(nBottom);
	hDrawText->m_dwTextYPosGap = (DWORD32)(dwYPosGap);
	hDrawText->m_dwTextSize = (DWORD32)(dwFontSize);

	hDrawText->m_eOutlineType = eOutlineType;
	hDrawText->m_bBold = bBold;
	hDrawText->m_bShadow = bShadow;

	TRACE("[DevOutText:DrawTextSetDisplayPos] end (%d:%d:%d:%d:%d:%d:%d)", hDrawText->m_dwTextLeft, hDrawText->m_dwTextBottom, hDrawText->m_dwTextYPosGap, hDrawText->m_dwTextSize, hDrawText->m_eOutlineType, hDrawText->m_bBold, hDrawText->m_bShadow);

	return TRUE;
}

BOOL SetSystemDefaultFont(DrawTextHandle hHandle, char* szPath)
{
	DrawTextStruct*	hDrawText = (DrawTextStruct*)hHandle;

	DIR *drp;
	struct dirent *list;
	struct stat state;
	int i;

	UTF32	ulChar = 0;

	char    szDefaultFontName[32] = "H2GTRM.ttf";

	FT_Face	ft_face;
	char	szFullPath[MAX_PATH] = {0, };
	char	szFont[MAX_PATH] = {0, };
	BOOL	bRet = TRUE;

	if (!hDrawText)
		return FALSE;

	TRACE("[DevOutText:SetSystemDefaultFont]	begin(%s)", szPath);

	if ((drp = opendir(szPath)) == NULL)
	{
		TRACE("[DevOutText:SetSystemDefaultFont] Can not open directory! (%s)", szPath);
		return FALSE;
	}

	chdir(szPath);

	while((list=readdir(drp)) != NULL )
	{
		lstat(list->d_name, &state);
		if(S_ISDIR(state.st_mode)) // Directory
		{
			if( strcmp(".", list->d_name) == 0 || strcmp("..", list->d_name) == 0)
				continue;

			TRACE("[DevOutText:SetSystemDefaultFont] D-Name : %s", list->d_name);

			sprintf(szFullPath, "%s/%s", szPath, list->d_name);
			SetSystemDefaultFont(hDrawText, szFullPath);
		}
		else // File
		{
			//TRACE("[DevOutText:SetSystemDefaultFont] F-Name : %s", list->d_name);

			sprintf(szFont, "%s/%s", szPath, list->d_name);

			if (hDrawText->m_bFindKorFont == FALSE)
			{
				if (!(hDrawText->m_ft_face[SYS_KOR_FT_FACE]) || UtilStrIFindString(list->d_name, szDefaultFontName))
				{
					ulChar = 0xD55C; // ('한' : 0xD55C), ('♪' : 0x266A)
					bRet = IsAvailableFont(hDrawText, szFont, ulChar, &ft_face);
					if (bRet)
					{
						TRACE("[DevOutText:SetSystemDefaultFont] [KOR:0x%x] %s", ft_face, szFont);
						hDrawText->m_ft_face[SYS_KOR_FT_FACE] = ft_face;

						if (UtilStrIFindString(list->d_name, szDefaultFontName))
						{
							hDrawText->m_bFindKorFont = TRUE;
						}
					}
				}
			}

			if (hDrawText->m_bFindSFont == FALSE)
			{
				ulChar = 0x266A; // ('♪' : 0x266A)
				bRet = IsAvailableFont(hDrawText, szFont, ulChar, &ft_face);
				if (bRet)
				{
					TRACE("[DevOutText:SetSystemDefaultFont] [S:0x%x] %s", ft_face, szFont);
					hDrawText->m_ft_face[SYS_S_FT_FACE] = ft_face;
					hDrawText->m_bFindSFont = TRUE;
				}
			}

			if (hDrawText->m_bFindKorFont && hDrawText->m_bFindSFont)
			{
				break;
			}
		}
	}

	chdir("..");
	closedir(drp);

	TRACE("[DevOutText:SetSystemDefaultFont]	end");

	return TRUE;
}

BOOL DrawTextSetDefaultSystemFont(DrawTextHandle hHandle, char* szPath)
{
	DrawTextStruct*	hDrawText = (DrawTextStruct*)hHandle;

	TRACE("[DevOutText:DrawTextSetDefaultSystemFont]	begin(%s)", szPath);

	hDrawText->m_bFindKorFont = FALSE;
	hDrawText->m_bFindSFont = FALSE;

	SetSystemDefaultFont(hDrawText, szPath);

	TRACE("[DevOutText:DrawTextSetDefaultSystemFont] [KOR:0x%x, S:0x%x]", hDrawText->m_ft_face[SYS_KOR_FT_FACE], hDrawText->m_ft_face[SYS_S_FT_FACE]);

	TRACE("[DevOutText:DrawTextSetDefaultSystemFont]	end");

	return TRUE;
}

BOOL DrawTextSetFontColor(DrawTextHandle hHandle, FONT_COLOR ftColor)
{
	DrawTextStruct*	hDrawText = (DrawTextStruct*)hHandle;

	TRACE("[DevOutText:DrawTextSetFontColor]	begin(%f:%f:%f:%f)", ftColor.fRed, ftColor.fGreen, ftColor.fBlue, ftColor.fAlpha);

	if (ftColor.fRed < 0.0 || ftColor.fRed > 1.0)
	{
		TRACEX(DTB_LOG_ERROR, "[DevOutText:DrawTextSetFontColor]	Unsupported color!!!");
		return FALSE;
	}
	else if (ftColor.fGreen < 0.0 || ftColor.fGreen > 1.0)
	{
		TRACEX(DTB_LOG_ERROR, "[DevOutText:DrawTextSetFontColor]	Unsupported color!!!");
		return FALSE;
	}
	else if (ftColor.fBlue < 0.0 || ftColor.fBlue > 1.0)
	{
		TRACEX(DTB_LOG_ERROR, "[DevOutText:DrawTextSetFontColor]	Unsupported color!!!");
		return FALSE;
	}
	else if (ftColor.fAlpha < 0.0 || ftColor.fAlpha > 1.0)
	{
		TRACEX(DTB_LOG_ERROR, "[DevOutText:DrawTextSetFontColor]	Unsupported color!!!");
		return FALSE;
	}

	memcpy(&(hDrawText->m_ftColor), &ftColor, sizeof(FONT_COLOR));

	TRACE("[DevOutText:DrawTextSetFontColor]	end");
}


#endif
