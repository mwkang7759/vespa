
#include "DevOutVideoBase.h"


#ifdef VIDEOOUT_D3D9DRAW

//#pragma comment(lib, "dxguid.lib")

/////////////////////////////////////////////////////////////////////////////
// Get monitor info
/////////////////////////////////////////////////////////////////////////////
#include <Winuser.h>
#include <vector>

//#include "mfxstructures.h"
//#include "mfxdefs.h"

class CMonitorInfoEx2 : public MONITORINFOEX
{
public:
    CMonitorInfoEx2();

    LPCRECT GetRect() const { return &rcMonitor; }
    LPCRECT GetWorkRect() const { return &rcWork; }
    LPCTSTR DeviceName() const { return szDevice; }

    bool IsPrimary() const { return (dwFlags & MONITORINFOF_PRIMARY) ? true : false; }

    int Width() const { return rcMonitor.right - rcMonitor.left; }
    int Height() const { return rcMonitor.bottom - rcMonitor.top; }
    int WorkWidth() const { return rcWork.right - rcWork.left; }
    int WorkHeight() const { return rcWork.bottom - rcWork.top; }
};

class CSysDisplays2
{
public:
    CSysDisplays2();

    void Update();

    int Count() const;
    const CMonitorInfoEx2& Get( int i ) const;
	RECT GetMaxMonitorSize();

private:
    std::vector<CMonitorInfoEx2> mInfo;
};

static BOOL CALLBACK MonitorEnumProc( __in  HMONITOR hMonitor, __in  HDC hdcMonitor, __in  LPRECT lprcMonitor, __in  LPARAM dwData )
{
    std::vector<CMonitorInfoEx2>& infoArray = *reinterpret_cast< std::vector<CMonitorInfoEx2>* >( dwData );
    CMonitorInfoEx2 info;
    GetMonitorInfo( hMonitor, &info );
    infoArray.push_back( info );
    return TRUE;
}

CMonitorInfoEx2::CMonitorInfoEx2()
{
    cbSize = sizeof(MONITORINFOEX);
}


CSysDisplays2::CSysDisplays2()
{
    Update();
}


void CSysDisplays2::Update()
{
    mInfo.clear();
    mInfo.reserve( ::GetSystemMetrics(SM_CMONITORS) );
    EnumDisplayMonitors( NULL, NULL, MonitorEnumProc, reinterpret_cast<LPARAM>(&mInfo) );
}


int CSysDisplays2::Count() const
{
    return (int)mInfo.size();
}


RECT CSysDisplays2::GetMaxMonitorSize()
{
	RECT			rect;
	unsigned int	i, width, height,
					max_width=0, max_height=0;

	for (i=0; i< (unsigned int)Count(); i++)
	{
		CMonitorInfoEx2	MonitorInfo = Get(i);

		width = MonitorInfo.GetRect()->right - MonitorInfo.GetRect()->left;
		height = MonitorInfo.GetRect()->bottom - MonitorInfo.GetRect()->top;
		width = ABS(width);
		height = ABS(height);

		if (max_width < width)
			max_width = width;
		if (max_height < height)
			max_height = height;
	}
	rect.left = 0;	rect.right = max_width;
	rect.top = 0;	rect.bottom = max_height;
	
	return rect;
}

const CMonitorInfoEx2& CSysDisplays2::Get( int i ) const
{
    return mInfo[i];
}

struct HandleData {
	unsigned long	ProcessId;
	HWND			hBestWnd;
};

////////////////////////////////////////////////////
//
////////////////////////////////////////////////////
static BOOL check_overlay_support(McVideoOutStructHandle hDevice)
{
	D3DCAPS9                d3d9caps;
	D3DOVERLAYCAPS          d3doverlaycaps = { 0 };
	IDirect3D9ExOverlayExtension *d3d9overlay = NULL;
	bool					 overlaySupported = false;

	memset(&d3d9caps, 0, sizeof(d3d9caps));
	HRESULT hr = hDevice->m_DDraw.m_pD3D9->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &d3d9caps);
	if (FAILED(hr) || !(d3d9caps.Caps & D3DCAPS_OVERLAY))
	{
		overlaySupported = false;
	}
	else
	{
		hr = hDevice->m_DDraw.m_pD3D9->QueryInterface(IID_PPV_ARGS(&d3d9overlay));
		if (FAILED(hr) || (d3d9overlay == NULL))
		{
			overlaySupported = false;
		}
		else
		{
			hr = d3d9overlay->CheckDeviceOverlayType(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL,
				hDevice->m_DDraw.m_D3DPP.BackBufferWidth,
				hDevice->m_DDraw.m_D3DPP.BackBufferHeight,
				hDevice->m_DDraw.m_D3DPP.BackBufferFormat, NULL,
				D3DDISPLAYROTATION_IDENTITY, &d3doverlaycaps);

			if (d3d9overlay)
				d3d9overlay->Release();

			if (FAILED(hr))
			{
				overlaySupported = false;
			}
			else
			{
				overlaySupported = true;
			}
		}
	}

	return overlaySupported;
}

// supported Color Format
// FOURCC_RGB
#define D3D9_YV12	MAKEFOURCC('Y', 'V', '1', '2')
#define D3D9_I420	MAKEFOURCC('I', '4', '2', '0')
#define D3D9_YUY2	MAKEFOURCC('Y', 'U', 'Y', '2')	
#define D3D9_UYUV	MAKEFOURCC('U', 'Y', 'V', 'Y')	
#define D3D9_YVYU	MAKEFOURCC('Y', 'V', 'Y', 'U')	 
#define D3D9_NV12	MAKEFOURCC('N', 'V', '1', '2')	 

#define D3DFMT_NV12 (D3DFORMAT)MAKEFOURCC('N','V','1','2')
#define D3DFMT_YV12 (D3DFORMAT)MAKEFOURCC('Y','V','1','2')

#ifdef ENABLE_PS
#define D3DFMT_NV16 (D3DFORMAT)MAKEFOURCC('N','V','1','6')
#define D3DFMT_P010 (D3DFORMAT)MAKEFOURCC('P','0','1','0')
#define D3DFMT_P210 (D3DFORMAT)MAKEFOURCC('P','2','1','0')
#define D3DFMT_IMC3 (D3DFORMAT)MAKEFOURCC('I','M','C','3')
#define D3DFMT_AYUV (D3DFORMAT)MAKEFOURCC('A','Y','U','V')
#define D3DFMT_Y210 (D3DFORMAT)MAKEFOURCC('Y','2','1','0')
#define D3DFMT_Y410 (D3DFORMAT)MAKEFOURCC('Y','4','1','0')
#define D3DFMT_P016 (D3DFORMAT)MAKEFOURCC('P','0','1','6')
#define D3DFMT_Y216 (D3DFORMAT)MAKEFOURCC('Y','2','1','6')
#define D3DFMT_Y416 (D3DFORMAT)MAKEFOURCC('Y','4','1','6')
#endif

static D3DFORMAT convert_fourcc_to_d3dFormat(DWORD32 dwFourCC, DWORD32 dwBits)
{
	switch (dwFourCC)
	{
	case D3D9_NV12:
		return D3DFMT_NV12;
	case D3D9_YV12:
	case D3D9_I420:
		return D3DFMT_YV12;
		//	case MFX_FOURCC_NV16:
		//		return D3DFMT_NV16;
	case D3D9_YUY2:
		return D3DFMT_YUY2;
	case FOURCC_RGB:
	{
		if (dwBits == 32)
			return D3DFMT_A8R8G8B8;
		if (dwBits == 24)
			return D3DFMT_R8G8B8;
		if (dwBits == 16)
			return D3DFMT_R5G6B5;
		return D3DFMT_UNKNOWN;
	}
	//	case MFX_FOURCC_P8:
	//		return D3DFMT_P8;
	//	case MFX_FOURCC_P010:
	//		return D3DFMT_P010;
	//	case MFX_FOURCC_AYUV:
	//		return D3DFMT_AYUV;
	//	case MFX_FOURCC_P210:
	//		return D3DFMT_P210;
#ifdef ENABLE_PS
	case MFX_FOURCC_Y210:
		return D3DFMT_Y210;
	case MFX_FOURCC_Y410:
		return D3DFMT_Y410;
	case MFX_FOURCC_P016:
		return D3DFMT_P016;
	case MFX_FOURCC_Y216:
		return D3DFMT_Y216;
	case MFX_FOURCC_Y416:
		return D3DFMT_Y416;
#endif
		//	case MFX_FOURCC_A2RGB10:
		//		return D3DFMT_A2R10G10B10;
		//	case MFX_FOURCC_ABGR16:
		//	case MFX_FOURCC_ARGB16:
		//		return D3DFMT_A16B16G16R16;
		//	case MFX_FOURCC_IMC3:
		//		return D3DFMT_IMC3;
	default:
		return D3DFMT_UNKNOWN;
	}
}

static void get_display_mode(McVideoOutStructHandle hDevice)
{
	// Reset will change the parameters, so use a copy instead.
	D3DDISPLAYMODE	dmode;
	D3DDISPLAYMODEEX	dmodex;
	D3DDISPLAYROTATION	drot;
	D3DPRESENT_PARAMETERS d3dpp = hDevice->m_DDraw.m_D3DPP;

	HRESULT hr = hDevice->m_DDraw.m_pD3D9->GetAdapterDisplayModeEx(hDevice->m_nAdapterNum, &hDevice->m_DDraw.m_Dmodex, NULL);

	/*
	dmodex.Size = sizeof(D3DDISPLAYMODEEX);
	dmodex.Format = dmode.Format;
	dmodex.Width = dmode.Width;
	dmodex.Height = dmode.Height;
	dmodex.RefreshRate = dmode.RefreshRate;
	dmodex.ScanLineOrdering = D3DSCANLINEORDERING_PROGRESSIVE;

	hDevice->m_DDraw.m_Dmode = dmode;
	hDevice->m_DDraw.m_Dmodex = dmodex;
	*/
}

static LRSLT fill_d3DPP(McVideoOutStructHandle hDevice, BOOL bIsA2rgb1, D3DPRESENT_PARAMETERS &D3DPP)
{
	LRSLT lRet = FR_OK;

	D3DPP.Windowed = TRUE;
	D3DPP.hDeviceWindow = (HWND)hDevice->m_hWnd;
	D3DPP.Flags = D3DPRESENTFLAG_VIDEO;
	D3DPP.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
	D3DPP.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE; // note that this setting leads to an implicit timeBeginPeriod call
	D3DPP.BackBufferCount = 1;

	D3DPP.BackBufferFormat = (bIsA2rgb1) ? D3DFMT_A2R10G10B10 : D3DFMT_X8R8G8B8;

	if (hDevice->m_hWnd)
	{
		RECT r;
		GetClientRect((HWND)hDevice->m_hWnd, &r);
		int x = GetSystemMetrics(SM_CXSCREEN);
		int y = GetSystemMetrics(SM_CYSCREEN);
		D3DPP.BackBufferWidth = min(r.right - r.left, x);
		D3DPP.BackBufferHeight = min(r.bottom - r.top, y);
	}
	else
	{
		D3DPP.BackBufferWidth = GetSystemMetrics(SM_CYSCREEN);
		D3DPP.BackBufferHeight = GetSystemMetrics(SM_CYSCREEN);
	}
	//
	// Mark the back buffer lockable if software DXVA2 could be used.
	// This is because software DXVA2 device requires a lockable render target
	// for the optimal performance.
	//
	{
		D3DPP.Flags |= D3DPRESENTFLAG_LOCKABLE_BACKBUFFER;
	}

#if 0
	bool isOverlaySupported = CheckOverlaySupport();
	if (2 == nViews && !isOverlaySupported)
		return MFX_ERR_UNSUPPORTED;

	bool needOverlay = (2 == nViews) ? true : false;

	D3DPP.SwapEffect = needOverlay ? D3DSWAPEFFECT_OVERLAY : D3DSWAPEFFECT_DISCARD;
#else
	bool isOverlaySupported = check_overlay_support(hDevice);
	isOverlaySupported = false;
	D3DPP.SwapEffect = isOverlaySupported ? D3DSWAPEFFECT_OVERLAY: D3DSWAPEFFECT_DISCARD;
#endif

	return lRet;
}

static LRSLT create_d3d9_surface(McVideoOutStructHandle hDevice)
{
	HRESULT hr;

	D3DFORMAT format = convert_fourcc_to_d3dFormat(hDevice->m_dwFourCC, hDevice->m_dwBitCount);
	if (format == D3DFMT_UNKNOWN)
	{
		LOG_E("[DevOutVideo] create_d3d9_surface - error unknowd color format 0x%x", hDevice->m_dwFourCC);
		return COMMON_ERR_VIDEODEVOUT;
	}

	DWORD   target;
	if (hDevice->m_bHWDecoder)
	{
		target = DXVA2_VideoDecoderRenderTarget;
	}
	else
	{
		target = DXVA2_VideoProcessorRenderTarget;
	}

	IDirectXVideoAccelerationService* videoService = NULL;

	if (target == DXVA2_VideoProcessorRenderTarget) {
		if (!hDevice->m_DDraw.m_hProcessor) 
		{
			hr = hDevice->m_DDraw.m_pDeviceManager9->OpenDeviceHandle(&hDevice->m_DDraw.m_hProcessor);
			if (FAILED(hr))
			{
				LOG_E("[DevOutVideo] OpenDeviceHandle - sw error 0x%x", hr);
				return COMMON_ERR_VIDEODEVOUT;
			}

			hr = hDevice->m_DDraw.m_pDeviceManager9->GetVideoService(hDevice->m_DDraw.m_hProcessor, IID_IDirectXVideoProcessorService, (void**)&hDevice->m_DDraw.m_processorService);
			if (FAILED(hr)) 
			{
				LOG_E("[DevOutVideo] GetVideoService - sw error 0x%x", hr);
				return COMMON_ERR_VIDEODEVOUT;
			}
		}
		videoService = hDevice->m_DDraw.m_processorService;
	}
	else {
		if (!hDevice->m_DDraw.m_hDecoder)
		{
			hr = hDevice->m_DDraw.m_pDeviceManager9->OpenDeviceHandle(&hDevice->m_DDraw.m_hDecoder);
			if (FAILED(hr))
			{
				LOG_E("[DevOutVideo] OpenDeviceHandle - hw error 0x%x", hr);
				return COMMON_ERR_VIDEODEVOUT;
			}

			hr = hDevice->m_DDraw.m_pDeviceManager9->GetVideoService(hDevice->m_DDraw.m_hDecoder, IID_IDirectXVideoDecoderService, (void**)&hDevice->m_DDraw.m_decoderService);
			if (FAILED(hr))
			{
				LOG_E("[DevOutVideo] GetVideoService - hw error 0x%x", hr);
				return COMMON_ERR_VIDEODEVOUT;
			}
		}
		videoService = hDevice->m_DDraw.m_decoderService;
	}

	hr = videoService->CreateSurface(hDevice->m_dwWidth, hDevice->m_dwHeight, 0, format,
		D3DPOOL_DEFAULT, 0, target, (IDirect3DSurface9**)&hDevice->m_DDraw.m_pSurface, (HANDLE*)&hDevice->m_DDraw.m_pSurface2nd);

	if (FAILED(hr)) 
	{
		LOG_E("[DevOutVideo] GetVideoService - hw error 0x%x", hr);
		return COMMON_ERR_VIDEODEVOUT;
	}

	hDevice->m_Format = format;

	return FR_OK;
}

BOOL DirectDrawOpen(McVideoOutStructHandle hDevice)
{
	D3DSURFACE_DESC		ddsd;
	HRESULT				hRet;
	int					cx,cy;
	bool				bSupportBackbuffer = FALSE;

	// D3D9Draw Create
	HRESULT hr = Direct3DCreate9Ex(D3D_SDK_VERSION, &hDevice->m_DDraw.m_pD3D9);
	if (!hDevice->m_DDraw.m_pD3D9 || FAILED(hr))
	{
		LOG_E("[DevOutVideo] DirectrawOpen - Direct3DCreate9Ex error 0x%x", hr);
		return FALSE;
	}

	hDevice->m_nAdapterNum = D3DADAPTER_DEFAULT;
	memset(&hDevice->m_DDraw.m_D3DPP, 0, sizeof(hDevice->m_DDraw.m_D3DPP));

	//get_display_mode(hDevice);
	fill_d3DPP(hDevice, FALSE, hDevice->m_DDraw.m_D3DPP);

	hr = hDevice->m_DDraw.m_pD3D9->CreateDeviceEx(
		hDevice->m_nAdapterNum,
		D3DDEVTYPE_HAL,
		(HWND)hDevice->m_hWnd,
		D3DCREATE_SOFTWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED | D3DCREATE_FPU_PRESERVE,
		&hDevice->m_DDraw.m_D3DPP,
		NULL,
		&hDevice->m_DDraw.m_pD3DD9);
	if (FAILED(hr))
	{
		LOG_E("[DevOutVideo] DirectrawOpen - CreateDeviceEx error 0x%x", hr);
		return FALSE;
	}

	UINT nMaxLatency;

	hr = hDevice->m_DDraw.m_pD3DD9->SetMaximumFrameLatency(1);
	if (FAILED(hr))
	{
		LOG_E("[DevOutVideo] DirectrawOpen - SetMaximumFrameLatency error 0x%x", hr);
		return FALSE;
	}

	hDevice->m_DDraw.m_pD3DD9->GetMaximumFrameLatency(&nMaxLatency);
	LOG_E("[DevOutVideo] DirectrawOpen - GetMaximumFrameLatency %d", nMaxLatency);

	if (hDevice->m_hWnd)
	{
		hr = hDevice->m_DDraw.m_pD3DD9->ResetEx(&hDevice->m_DDraw.m_D3DPP, NULL);
		if (FAILED(hr))
		{
			LOG_E("[DevOutVideo] DirectrawOpen - ResetEx error 0x%x", hr);
			return FALSE;
		}
		hr = hDevice->m_DDraw.m_pD3DD9->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);
		if (FAILED(hr))
		{
			LOG_E("[DevOutVideo] DirectrawOpen - Clear error 0x%x", hr);
			return FALSE;
		}
	}
	
	LRSLT lRet = FR_OK;
	if (0)
	{
		UINT resetToken = 0;

		hr = DXVA2CreateDirect3DDeviceManager9(&resetToken, &hDevice->m_DDraw.m_pDeviceManager9);
		if (FAILED(hr))
		{
			LOG_E("[DevOutVideo] DirectrawOpen - DXVA2CreateDirect3DDeviceManager9 error 0x%x", hr);
			return FALSE;
		}

		hr = hDevice->m_DDraw.m_pDeviceManager9->ResetDevice(hDevice->m_DDraw.m_pD3DD9, resetToken);
		if (FAILED(hr))
		{
			LOG_E("[DevOutVideo] DirectrawOpen - ResetDevice error 0x%x", hr);
			return FALSE;
		}
		hDevice->m_DDraw.m_nResetToken = resetToken;

		//hDevice->m_dwFourCC
		lRet = create_d3d9_surface(hDevice);
	}
	else
	{
		D3DFORMAT format = convert_fourcc_to_d3dFormat(hDevice->m_dwFourCC, hDevice->m_dwBitCount);
		if (format == D3DFMT_UNKNOWN)
		{
			LOG_E("[DevOutVideo] create_d3d9_surface - error unknowd color format 0x%x", hDevice->m_dwFourCC);
			return COMMON_ERR_VIDEODEVOUT;
		}

		hr = hDevice->m_DDraw.m_pD3DD9->CreateOffscreenPlainSurfaceEx(
									hDevice->m_dwWidth, hDevice->m_dwHeight,
									format, D3DPOOL_DEFAULT, 
									(IDirect3DSurface9**)&hDevice->m_DDraw.m_pSurface, 
									(HANDLE*)&hDevice->m_DDraw.m_pSurface2nd,
									0);
		if (FAILED(hr))
		{
			LOG_E("[DevOutVideo] CreateOffscreenPlainSurface - error 0x%x", hr);
			return FALSE;
		}
		hDevice->m_Format = format;
	}
	
	if (FRFAILED(lRet))
		return FALSE;

	return	TRUE;
}

void DirectDrawClose(McVideoOutStructHandle hDevice)
{
	if (!hDevice)
		return;

	/*if (hDevice->m_hDrawText)
	{
		DrawTextClose(hDevice->m_hDrawText);
		hDevice->m_hDrawText = NULL;
	}*/

	// Black display
	HRESULT hr = hDevice->m_DDraw.m_pD3DD9->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);
	if (FAILED(hr))
	{
		LOG_E("[DevOutVideo] DirectrawOpen - Clear error 0x%x", hr);
	}
	if (SUCCEEDED(hr))
	{
		hr = hDevice->m_DDraw.m_pD3DD9->Present(NULL, NULL, NULL, NULL);
		hDevice->m_DDraw.m_pD3DD9->WaitForVBlank(0);
	}

	if (hDevice->m_DDraw.m_pD3D9)
	{
		hDevice->m_DDraw.m_pD3D9->Release();
		hDevice->m_DDraw.m_pD3D9 = NULL;
	}
	if (hDevice->m_DDraw.m_pD3DD9)
	{
		hDevice->m_DDraw.m_pD3DD9->Release();
		hDevice->m_DDraw.m_pD3DD9 = NULL;
	}
	if (hDevice->m_DDraw.m_pSurface)
	{
		hDevice->m_DDraw.m_pSurface->Release();
		hDevice->m_DDraw.m_pSurface = NULL;
	}
	if (hDevice->m_DDraw.m_pDeviceManager9)
	{
		if (hDevice->m_DDraw.m_hDecoder)
			hDevice->m_DDraw.m_pDeviceManager9->CloseDeviceHandle(hDevice->m_DDraw.m_hDecoder);
		if (hDevice->m_DDraw.m_hProcessor)
			hDevice->m_DDraw.m_pDeviceManager9->CloseDeviceHandle(hDevice->m_DDraw.m_hProcessor);
		hDevice->m_DDraw.m_hProcessor = NULL;
		hDevice->m_DDraw.m_pDeviceManager9->Release();
		hDevice->m_DDraw.m_pDeviceManager9 = NULL;
	}

	if (hDevice->m_hTextWnd && hDevice->m_hTextDC)
	{
		ReleaseDC(hDevice->m_hTextWnd, hDevice->m_hTextDC);
		hDevice->m_hTextDC = NULL;
	}
}

static BOOL reset_device(McVideoOutStructHandle hDevice)
{
	HRESULT hr = NO_ERROR;

	if (hDevice->m_DDraw.m_D3DPP.Windowed)
	{
		RECT r;
		GetClientRect((HWND)hDevice->m_DDraw.m_D3DPP.hDeviceWindow, &r);
		int x = GetSystemMetrics(SM_CXSCREEN);
		int y = GetSystemMetrics(SM_CYSCREEN);
		hDevice->m_DDraw.m_D3DPP.BackBufferWidth = min(r.right - r.left, x);
		hDevice->m_DDraw.m_D3DPP.BackBufferHeight = min(r.bottom - r.top, y);
	}
	else
	{
		hDevice->m_DDraw.m_D3DPP.BackBufferWidth = GetSystemMetrics(SM_CXSCREEN);
		hDevice->m_DDraw.m_D3DPP.BackBufferHeight = GetSystemMetrics(SM_CYSCREEN);
	}

	D3DPRESENT_PARAMETERS d3dpp = hDevice->m_DDraw.m_D3DPP;

	hr = hDevice->m_DDraw.m_pD3DD9->ResetEx(&d3dpp, NULL);
	if (FAILED(hr))
	{
		LOG_E("[DevOutVideo] reset_device - Reset error 0x%x", hr);
		return FALSE;
	}
	if (hDevice->m_DDraw.m_pDeviceManager9)
	{
		hr = hDevice->m_DDraw.m_pDeviceManager9->ResetDevice(hDevice->m_DDraw.m_pD3DD9, hDevice->m_DDraw.m_nResetToken);
		if (FAILED(hr))
		{
			LOG_E("[DevOutVideo] reset_device - ResetDevice error 0x%x", hr);
			return FALSE;
		}
	}

	hr = hDevice->m_DDraw.m_pD3DD9->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);
	if (FAILED(hr))
	{
		LOG_E( "[DevOutVideo] reset_device - Clear error 0x%x", hr);
		return FALSE;
	}

	return TRUE;
}

BOOL DirectDrawSetWndHandle(McVideoOutStructHandle hDevice)
{
	if (hDevice)
	{
		if (hDevice->m_hWnd)
		{
			hDevice->m_DDraw.m_D3DPP.hDeviceWindow = (HWND)hDevice->m_hWnd;
			/*if (hDevice->m_hDrawText)
				DrawTextSetSurfaceChanged(hDevice->m_hDrawText, hDevice->m_hWnd);*/
			hDevice->m_bReset = TRUE;
		}
	}
	return	TRUE;
}

BOOL DirectDrawRestore(McVideoOutStructHandle hDevice)
{
	HRESULT		hRet;

	return TRUE;
}

static void copy_data_2_surface(BYTE *pSurface, DWORD32 dwSurfacePitch, BYTE* pSrc, DWORD32 dwSrcPitch, DWORD32 dwWidth, DWORD32 dwHeight)
{
	DWORD				i;
	BYTE				*pSurfaceCur = pSurface,
						*pSrcCur = pSrc;

	for (i = 0; i < dwHeight; i++)
	{
		memcpy(pSurfaceCur, pSrcCur, dwWidth);
		pSurfaceCur += dwSurfacePitch;
		pSrcCur += dwSrcPitch;
	}
}

BOOL DirectDrawTransToBuffer(McVideoOutStructHandle hDevice, FrRawVideo* pMedia)
{
	IDirect3DSurface9	*pSurface;
	D3DSURFACE_DESC		desc;
	HRESULT				hr;
	BYTE				*pSurfaceBuf, *pTemp;
	DWORD				dwPitch, dwWidth, dwHeight;

	// Device reset
	if (hDevice->m_bReset)
	{
		LOG_E( "DirectDrawTransToBuffer reset");
		hDevice->m_bReset = FALSE;
		reset_device(hDevice);
	}

	/*if (pMedia->m_eColorFormat == VideoFmtQSV)
	{
		if (pMedia->m_pDevPtr)
		{
			hDevice->m_bUseHWSurface = TRUE;

			hDevice->m_pHWD3DD9 = (IDirect3DDevice9Ex*)pMedia->m_pDevPtr;
			mfxFrameSurface1 * pSurface = (mfxFrameSurface1 *)pMedia->m_pSurface;
			mfxHDLPair* dxMemId = (mfxHDLPair*)pSurface->Data.MemId;
			hDevice->m_pHWSurface = (IDirect3DSurface9*)dxMemId->first;
			return TRUE;
		}
		else
		{
			hDevice->m_bUseHWSurface = FALSE;
			hDevice->m_pHWSurface = NULL;
			return FALSE;
		}
	}*/

	pSurface = hDevice->m_DDraw.m_pSurface;
	hr = pSurface->GetDesc(&desc);
	if(hr != D3D_OK)
	{
		LOG_E("[DevOutVideo] DirectDrawTransToBuffer - error 0x%x", hr);
		return FALSE;
	}
	
	if (desc.Format != hDevice->m_Format)
	{
		LOG_E("[DevOutVideo] DirectDrawTransToBuffer - different color format (decodr 0x%x, surface 0x%x)", hDevice->m_Format, desc.Format);
		return FALSE;
	}

	// Lock surface memory
	D3DLOCKED_RECT locked;
	hr = pSurface->LockRect(&locked, 0, D3DLOCK_NOSYSLOCK);
	if (FAILED(hr))
	{
		LOG_E("[DevOutVideo] DirectDrawTransToBuffer - Lock failed error 0x%x", hr);
		return FALSE;
	}

	// Surface memory infos
	pSurfaceBuf = (BYTE*)locked.pBits;
	dwPitch = (DWORD32)locked.Pitch;
	dwWidth = desc.Width;
	dwHeight = desc.Height;

	// Copy data into surface
	switch (hDevice->m_Format)
	{
	case D3DFMT_A8R8G8B8:
	case D3DFMT_R8G8B8:
	case D3DFMT_R5G6B5:
		break;
	case D3DFMT_NV12:
		// Y
		pTemp = pSurfaceBuf;
		copy_data_2_surface(pTemp, dwPitch, pMedia->pY, pMedia->dwPitch, pMedia->dwDecodedWidth, pMedia->dwDecodedHeight);
		// UV
		pTemp += dwHeight * dwPitch;
		copy_data_2_surface(pTemp, dwPitch, pMedia->pU, pMedia->dwPitch, pMedia->dwDecodedWidth, pMedia->dwDecodedHeight / 2);
		break;
	case D3DFMT_YV12:
		// Y
		pTemp = pSurfaceBuf;
		copy_data_2_surface(pTemp, dwPitch, pMedia->pY, pMedia->dwPitch, pMedia->dwDecodedWidth, pMedia->dwDecodedHeight);
		// V
		pTemp += dwHeight * dwPitch;
		copy_data_2_surface(pTemp, dwPitch/2, pMedia->pV, pMedia->dwPitch/2, pMedia->dwDecodedWidth/2, pMedia->dwDecodedHeight/2);
		// U
		pTemp += dwHeight * dwPitch / 4;
		copy_data_2_surface(pTemp, dwPitch/2, pMedia->pU, pMedia->dwPitch/2, pMedia->dwDecodedWidth/2, pMedia->dwDecodedHeight/2);
		break;
	//	D3DFMT_NV16;
	case D3DFMT_YUY2:
	//	case D3DFMT_P8;
	//	case D3DFMT_P010;
	//	case D3DFMT_AYUV;
	//	case D3DFMT_P210;
#ifdef ENABLE_PS
	case D3DFMT_Y210;
	case D3DFMT_Y410;
	case D3DFMT_P016;
	case D3DFMT_Y216;
	case D3DFMT_Y416;
#endif
	//	case D3DFMT_A2R10G10B10;
	//	case D3DFMT_A16B16G16R16;
	//	case D3DFMT_IMC3;
	default:
		break;
	}

#if 0
	if (hDevice->m_DDraw.m_bRGB)
	{
		if (hDevice->m_dwFourCC != FOURCC_RGB)
		{
			if (hDevice->m_nDevBitCount == 32)
			{
				//
				IYUV_to_BGRA(OffScrBuff, hDevice->m_dwWidth, hDevice->m_dwHeight, ddsd.lPitch,
							pMedia->m_pY, pMedia->m_pU, pMedia->m_pV, pMedia->m_dwPitch);
			}
			else if (hDevice->m_nDevBitCount == 24)
				IYUV_to_BGR24(OffScrBuff, hDevice->m_dwWidth, hDevice->m_dwHeight, ddsd.lPitch,
							pMedia->m_pY, pMedia->m_pU, pMedia->m_pV, pMedia->m_dwPitch);
			else	// hDevice->m_nDevBitCount == 16
				IYUV_to_RGB565LE(OffScrBuff, hDevice->m_dwWidth, hDevice->m_dwHeight, ddsd.lPitch,
							pMedia->m_pY, pMedia->m_pU, pMedia->m_pV, pMedia->m_dwPitch);
		}
		else
		{
			if (hDevice->m_nDevBitCount == 32)
			{
				if (hDevice->m_dwBitCount == 32)
				{
					pTemp = pMedia->m_pY;
					for (i = 0;i < hDevice->m_dwHeight;i++)
					{
						memcpy(OffScrBuff, pTemp, hDevice->m_dwWidth*4);
						OffScrBuff += ddsd.lPitch;
						pTemp += pMedia->m_dwPitch;
					}		
				}
				else if (hDevice->m_dwBitCount == 24)
				{
					BGR24_to_BGRA(OffScrBuff, ddsd.lPitch, pMedia->m_pY, 
						pMedia->m_dwPitch, hDevice->m_dwWidth, hDevice->m_dwHeight, FALSE);
				}	
				else	// hDevice->m_dwBitCount == 16
					RGB16_to_BGRA(OffScrBuff, ddsd.lPitch, pMedia->m_pY, 
						pMedia->m_dwPitch, hDevice->m_dwWidth, hDevice->m_dwHeight, TRUE, TRUE, TRUE);
			}

		}
	}
	else
	{
		if (hDevice->m_DDraw.m_dwFourCC == MAKEFOURCC('Y','V','1','2'))
		{
			pTemp = pMedia->m_pY;
			for (i = 0;i < hDevice->m_dwHeight;i++)
			{
				memcpy(OffScrBuff, pTemp, hDevice->m_dwWidth);
				OffScrBuff += ddsd.lPitch;
				pTemp += pMedia->m_dwPitch;
			}
			pTemp = pMedia->m_pV;
			for (i = 0;i < hDevice->m_dwHeight/2;i++)
			{
				memcpy(OffScrBuff, pTemp, hDevice->m_dwWidth/2);
				OffScrBuff += ddsd.lPitch/2;
				pTemp += pMedia->m_dwPitch/2;
			}
			pTemp = pMedia->m_pU;
			for (i = 0;i < hDevice->m_dwHeight/2;i++)
			{		
				memcpy(OffScrBuff, pTemp, hDevice->m_dwWidth/2);
				OffScrBuff += ddsd.lPitch/2;
				pTemp += pMedia->m_dwPitch/2;
			}

			//TRACE("[DevOut] w=%d, h=%d, src pitch=%d, ddraw pitch=%d\n", 
			//	hDevice->m_dwWidth, hDevice->m_dwHeight, pMedia->m_dwPitch, ddsd.lPitch);
		}
		else if (hDevice->m_DDraw.m_dwFourCC == MAKEFOURCC('Y','U','Y','2'))
		{
			IYUV_to_YUY2(OffScrBuff, hDevice->m_dwWidth, hDevice->m_dwHeight, ddsd.lPitch/4,
						pMedia->m_pY, pMedia->m_pU, pMedia->m_pV, pMedia->m_dwPitch);
		}
		else if (hDevice->m_DDraw.m_dwFourCC == MAKEFOURCC('U','Y','V','Y'))
		{
			IYUV_to_UYVY(OffScrBuff, hDevice->m_dwWidth, hDevice->m_dwHeight, ddsd.lPitch/4,
						pMedia->m_pY, pMedia->m_pU, pMedia->m_pV, pMedia->m_dwPitch);
		}
		else if (hDevice->m_DDraw.m_dwFourCC == MAKEFOURCC('Y','V','Y','U'))
		{
			IYUV_to_YVYU(OffScrBuff, hDevice->m_dwWidth, hDevice->m_dwHeight, ddsd.lPitch/4,
						pMedia->m_pY, pMedia->m_pU, pMedia->m_pV, pMedia->m_dwPitch);
		}
		else if (hDevice->m_DDraw.m_dwFourCC == MAKEFOURCC('N','V','1','2'))
		{
			pTemp = pMedia->m_pY;
			for (i = 0;i < hDevice->m_dwHeight;i++)
			{
				memcpy(OffScrBuff, pTemp, hDevice->m_dwWidth);
				OffScrBuff += ddsd.lPitch;
				pTemp += pMedia->m_dwPitch;
			}
			pTemp = pMedia->m_pU;
			for (i = 0;i < hDevice->m_dwHeight/2;i++)
			{
				memcpy(OffScrBuff, pTemp, hDevice->m_dwWidth);
				OffScrBuff += ddsd.lPitch;
				pTemp += pMedia->m_dwPitch;
			}
		}
		else										// ('Y','U','Y','V')
		{
		}
	}
#endif
	// Unlock locked surface
	pSurface->UnlockRect();

	return	TRUE;
}

static BOOL DirectDrawRenderBlack(McVideoOutStructHandle hDevice)
{

	return	TRUE;
}

BOOL DirectDrawClear(McVideoOutStructHandle hDevice)
{

	return	TRUE;
}

BOOL DirectDrawPlay(McVideoOutStructHandle hDevice)
{
	HRESULT hr = S_OK;
	RECT		rc;
	DWORD32		dwGap = 0;
	INT64		n64Gap = 0;

	INT64 n64BeginTime = 0;
	INT64 n64EndTime = 0;

	// Device reset
	if (hDevice->m_bReset)
	{
		hDevice->m_bReset = FALSE;
		reset_device(hDevice);
	}

	// Set disaplay mode
	GetWindowRect(hDevice->m_hWnd, &hDevice->m_rcWnd);
	hDevice->m_dwWndWidth = hDevice->m_rcWnd.right - hDevice->m_rcWnd.left;
	hDevice->m_dwWndHeight = hDevice->m_rcWnd.bottom - hDevice->m_rcWnd.top;
	if (!hDevice->m_dwWndWidth || !hDevice->m_dwWndHeight)
		return	FALSE;

	SetRect(&rc, 0, 0, hDevice->m_dwWndWidth, hDevice->m_dwWndHeight);
	GetDisplayRectangle(hDevice, &hDevice->m_rcDest);

	/*if (hDevice->m_n64BegineTime == 0)
		hDevice->m_n64BegineTime = McGetQueryCounter();
	hDevice->m_n64EndTime = McGetQueryCounter();
	n64Gap = McGetQueryDiffTime(hDevice->m_n64FreqTime, hDevice->m_n64EndTime, hDevice->m_n64BegineTime);
	dwGap = (DWORD32)(n64Gap / 1000);
	hDevice->m_n64BegineTime = hDevice->m_n64EndTime;*/


	//TRACEX(DTB_LOG_DEBUG, "[DirectDrawPlay] begin Render : gap(%d)", dwGap);

	IDirect3DDevice9Ex	*pD3DD9;

	pD3DD9 = hDevice->m_bUseHWSurface ? hDevice->m_pHWD3DD9 : hDevice->m_DDraw.m_pD3DD9;

	if (!pD3DD9)
	{
		LOG_W("[DirectDrawPlay] Device handle is null");
		return FALSE;
	}

	hr = pD3DD9->TestCooperativeLevel();

	switch (hr)
	{
	case D3D_OK:
		break;

	case D3DERR_DEVICELOST:
	{
		LOG_E("[DevOutVideo] DirectDrawPlay - TestCooperativeLevel device lost error 0x%x ", hr);
		return FALSE;
		//return MFX_ERR_DEVICE_LOST;
	}

	case D3DERR_DEVICENOTRESET:
	{
		LOG_E("[DevOutVideo] DirectDrawPlay - TestCooperativeLevel device not reset error 0x%x ", hr);
		return FALSE;
		//return MFX_ERR_UNKNOWN;
	}

	default:
	{
		LOG_E("[DevOutVideo] DirectDrawPlay - TestCooperativeLevel unknown error 0x%x ", hr);
		return FALSE;
		//return MFX_ERR_UNKNOWN;
	}
	}

	// Get BackBuffer
	IDirect3DSurface9* pBackBuffer;
	hr = pD3DD9->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &pBackBuffer);
	if (FAILED(hr))
	{
		LOG_E("[DevOutVideo] DirectDrawPlay - GetBackBuffer 0x%x ", hr);
		return FALSE;
	}

	// This is for HW decoding
	// mfxHDLPair* dxMemId = (mfxHDLPair*)pSurface->Data.MemId;

	if (hDevice->m_bUseHWSurface)
	{
		if (hDevice->m_nDisplayType == VIEW_TYPE_RATIO_FULL)
			hr = pD3DD9->StretchRect(hDevice->m_pHWSurface, NULL, pBackBuffer, NULL, D3DTEXF_LINEAR);
		else
			hr = pD3DD9->StretchRect(hDevice->m_pHWSurface, NULL, pBackBuffer, &hDevice->m_rcDest, D3DTEXF_LINEAR);
		if (FAILED(hr))
		{
			LOG_E("[DevOutVideo] DirectDrawPlay - TestCooperativeLevel unknown error 0x%x ", hr);
			return FALSE;
		}
	}
	else
	{
		if (hDevice->m_nDisplayType == VIEW_TYPE_RATIO_FULL)
			hr = pD3DD9->StretchRect(hDevice->m_DDraw.m_pSurface, NULL, pBackBuffer, NULL, D3DTEXF_LINEAR);
		else
			hr = pD3DD9->StretchRect(hDevice->m_DDraw.m_pSurface, NULL, pBackBuffer, &hDevice->m_rcDest, D3DTEXF_LINEAR);
		if (FAILED(hr))
		{
			LOG_E("[DevOutVideo] DirectDrawPlay - TestCooperativeLevel unknown error 0x%x ", hr);
			return FALSE;
		}
	}

	//n64BeginTime = McGetQueryCounter();

	// SubTitle
	hDevice->m_DDraw.m_pBackBuffer = pBackBuffer;
	DirectDrawPlayString(hDevice);

	/*n64EndTime = McGetQueryCounter();
	n64Gap = McGetQueryDiffTime(hDevice->m_n64FreqTime, n64EndTime, n64BeginTime);

	LOG_T("[DevOutVideo] : DirectDrawPlay : DirectDrawPlayString : perf time(%0.2f)", n64Gap / 1000.);

	n64BeginTime = McGetQueryCounter();*/

	hr = pD3DD9->Present(NULL, NULL, NULL, NULL);

	/*n64EndTime = McGetQueryCounter();
	n64Gap = McGetQueryDiffTime(hDevice->m_n64FreqTime, n64EndTime, n64BeginTime);*/

	LOG_T("[DevOutVideo] : DirectDrawPlay : Present : perf time(%0.2f)", n64Gap / 1000.);

	if (!hDevice->m_bVSyncOff)
	{
		//n64BeginTime = McGetQueryCounter();
		hr = pD3DD9->WaitForVBlank(0);
		if (FAILED(hr))
		{
			LOG_W("[DevOutVideo] DirectDrawPlay - WaitForVBlank unknown error 0x%x ", hr);
		}

		/*n64EndTime = McGetQueryCounter();
		n64Gap = McGetQueryDiffTime(hDevice->m_n64FreqTime, n64EndTime, n64BeginTime);*/

		//LOG_T("[DevOutVideo] : DirectDrawPlay : WaitForVBlank : perf time(%0.2f)", n64Gap / 1000.);
	}

	if (SUCCEEDED(hr))
	{
		//hr = pD3DD9->Present(NULL, NULL, NULL, NULL);
	}

	return	SUCCEEDED(hr) ? TRUE : FALSE;
}

BOOL DirectDrawPlayWaitForVSync(McVideoOutStructHandle hDevice)
{
	BOOL	bRet = TRUE;
	HRESULT hr = S_OK;

	hr = hDevice->m_DDraw.m_pD3DD9->WaitForVBlank(0);
	if (FAILED(hr))
	{
		LOG_W("[DirectDrawPlayWaitForVSync] DirectDrawPlay - WaitForVBlank unknown error 0x%x ", hr);
		bRet = FALSE;
	}

	return bRet;
}

BOOL DrawString(McVideoOutStructHandle hDevice)
{
#if 0
	HRESULT		hRet;
	CFont		Textfont;
	CFont		Statusfont;
	CFont		PerfInfofont;

	CFont		Textbkfont;
	HDC			hDC;
	CDC		    dc;
	CString		strText;
	CRect		rcText;
	CRect		rcStatus;
	CRect		rcPerfInfo;
	CFont*		pOldFont;
	int			i;
	int			oldmode;
	wchar_t		szGetHeight[10] = L"a";
	int			nFontHeight = 0;
	int			nTotalHeight = 0;
	int			nLineCount = 0;
	wchar_t		*szLineptr = NULL;
	wchar_t		szBuff[MAX_TEXT_STRING_LEN];
	wchar_t		tmpBuff[MAX_TEXT_STRING_LEN];

	BOOL		bRet = TRUE;
	int			nRet = 0;

	if ((hDevice->m_TextBlock.block_num == 0) &&
		(strlen(hDevice->m_szStatus) == 0) &&
		(strlen(hDevice->m_szPerfInfo) == 0))
	{
		hDevice->m_bText = FALSE;
		return FALSE;
	}

	if (hDevice->m_hTextWnd)
	{
		if (!hDevice->m_hTextDC)
		{
			hDevice->m_hTextDC = GetDC(hDevice->m_hTextWnd);
		}
			
		hDC = hDevice->m_hTextDC;
		//TRACEX(DTB_LOG_INFO, "[DevOutVideo] DrawString - GetDC (0x%x) ", hDC);
			
	}
	else
	{
		// Get BackBuffer
		if (hDevice->m_DDraw.m_pBackBuffer == NULL)
		{
			// Get BackBuffer
			IDirect3DSurface9* pBackBuffer;
			HRESULT hr = hDevice->m_DDraw.m_pD3DD9->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &pBackBuffer);
			if (FAILED(hr))
			{
				LOG_E("[DevOutVideo] DrawString - GetBackBuffer 0x%x ", hr);
				return FALSE;
			}
			hDevice->m_DDraw.m_pBackBuffer = pBackBuffer;
		}

		// SubTitle
		//hRet = hDevice->m_DDraw.m_pSurface->GetDC(&hDC);
		hRet = hDevice->m_DDraw.m_pBackBuffer->GetDC(&hDC);
		if (hRet != D3D_OK)
		{
			LOG_E("DrawString - GetDC 0x%x", hRet);
			return	FALSE;
		}
	}

	bRet = dc.Attach(hDC);
	//TRACEX(DTB_LOG_ERROR, "DrawString - Attach (%d)", bRet);


	CreateTextFont(hDevice, hDevice->m_dwTextSize, hDevice->m_rcDest.bottom - hDevice->m_rcDest.top, &Textfont);
	pOldFont = dc.SelectObject(&Textfont);
	CRect	rect;
	nRet = dc.DrawText(szGetHeight, &rect, DT_LEFT | DT_CALCRECT);
	//TRACEX(DTB_LOG_INFO, "DrawString - DrawText (%d)", nRet);

	nFontHeight = rect.Height();

	hDevice->m_FontColor = hDevice->m_TextBlock.block[0].font_color;

	if (hDevice->m_dwTextDisplayType == TEXT_VIDEO_DISPLAY)
		SetRect(&rcText, 0, 0, hDevice->m_dwWndWidth, hDevice->m_rcDest.bottom);
	else
		SetRect(&rcText, 0, 0, hDevice->m_dwWndWidth, hDevice->m_dwWndHeight);

	rcText.left += 5;
	rcText.right -= 5;

	if (hDevice->m_dwLeftMovePos - hDevice->m_dwRightMovePos > 0)
		rcText.right -= hDevice->m_dwLeftMovePos - hDevice->m_dwRightMovePos;
	else
		rcText.left += hDevice->m_dwRightMovePos - hDevice->m_dwLeftMovePos;


	for (i = 0; i < hDevice->m_TextBlock.block_num; i++)
	{
		wchar_t *ptr = NULL;
		nLineCount = 0;
		memset(szBuff, 0x00, MAX_TEXT_STRING_LEN * sizeof(wchar_t));
		memcpy((VOID*)szBuff, (VOID*)hDevice->m_TextBlock.block[i].data, (hDevice->m_TextBlock.block[i].len + 1) * sizeof(wchar_t));
		memset(tmpBuff, 0x00, MAX_TEXT_STRING_LEN * sizeof(wchar_t));
		memcpy((VOID*)tmpBuff, (VOID*)hDevice->m_TextBlock.block[i].data, (hDevice->m_TextBlock.block[i].len + 1) * sizeof(wchar_t));

		szLineptr = tmpBuff;
		if ((ptr = wcsstr(szLineptr, L"\n")) != NULL)
		{
			wchar_t TempBuff[MAX_TEXT_STRING_LEN];
			while (1)
			{
				if ((ptr = wcsstr(szLineptr, L"\n")) == NULL)
					break;
				memset(TempBuff, 0x00, sizeof(TempBuff));
				ptr[0] = '\0';
				wcscpy(TempBuff, szLineptr);

				nLineCount = 0;
				dc.DrawText(szLineptr, &rect, DT_LEFT | DT_CALCRECT);
				//DrawTextW(hDC, szLineptr, wcslen(szLineptr), &rect, DT_LEFT | DT_CALCRECT);
				int nWidth = rect.Width();
				if (nWidth > (int)rcText.Width())
				{
					while (1)
					{
						nLineCount++;
						nWidth -= rcText.Width();
						if (nWidth < (int)rcText.Width())
							break;
					}
					nLineCount++;
				}
				else
				{
					nLineCount = 1;
				}
				nTotalHeight += (nLineCount*nFontHeight);
				szLineptr = ptr + 1;
			}

			if (wcslen(szLineptr) > 0)
			{
				nLineCount = 0;
				dc.DrawText(szLineptr, &rect, DT_LEFT | DT_CALCRECT);
				//DrawTextW(hDC, szLineptr, wcslen(szLineptr), &rect, DT_LEFT | DT_CALCRECT);
				int nWidth = rect.Width();
				if (nWidth > (int)rcText.Width())
				{
					while (1)
					{
						nLineCount++;
						nWidth -= rcText.Width();
						if (nWidth < (int)rcText.Width())
							break;
					}
					nLineCount++;
				}
				else
				{
					nLineCount = 1;
				}
				nTotalHeight += (nLineCount*nFontHeight);
			}
		}
		else
		{
			dc.DrawText(szBuff, &rect, DT_LEFT | DT_CALCRECT);
			//DrawTextW(hDC, szLineptr, wcslen(szLineptr), &rect, DT_LEFT | DT_CALCRECT);
			int nWidth = rect.Width();
			if (nWidth > (int)rcText.Width())
			{
				while (1)
				{
					nLineCount++;
					nWidth -= rcText.Width();
					if (nWidth < (int)rcText.Width())
						break;
				}
				nLineCount++;
			}
			else
			{
				nLineCount = 1;
			}
			nTotalHeight += (nLineCount*nFontHeight);
		}
		strText += szBuff;
	}


	if (hDevice->m_dwTextDisplayType == TEXT_VIDEO_DISPLAY)
		rcText.top = hDevice->m_rcDest.bottom - nTotalHeight - 10 - hDevice->m_dwUpMovePos + hDevice->m_dwDownMovePos;
	else
		rcText.top = hDevice->m_dwWndHeight - nTotalHeight - 10 - hDevice->m_dwUpMovePos + hDevice->m_dwDownMovePos;

	oldmode = dc.SetBkMode(TRANSPARENT);


	SetTextColor(hDC, RGB(0, 0, 0));
	CRect rcBK;
	if (hDevice->m_bTextShowHide == TEXT_SHOW)
	{
		for (i = 1; i <= 2; i++)
		{
			for (int j = 1; j <= 2; j++)
			{
				rcBK = rcText;
				rcBK.left += i;
				rcBK.top += j;
				rcBK.right += i;
				rcBK.bottom += j;
				dc.DrawText(strText, &rcBK, DT_CENTER | DT_WORDBREAK);
			}
		}

		if (hDevice->m_bUserDefineColor)
			SetTextColor(hDC, hDevice->m_dwUserDefineColor);
		else
			SetTextColor(hDC, hDevice->m_FontColor);

		dc.DrawText(strText, &rcText, DT_CENTER | DT_WORDBREAK);
	}

	if (strlen(hDevice->m_szStatus) > 0)
	{
		dc.SelectObject(pOldFont);
		CreateTextFont(hDevice, hDevice->m_nStatusFontSize, hDevice->m_rcDest.bottom - hDevice->m_rcDest.top, &Statusfont);
		pOldFont = dc.SelectObject(&Statusfont);

		dc.DrawText(hDevice->m_szStatus, &rcStatus, DT_LEFT | DT_CALCRECT);

		int nWidth = rcStatus.Width();
		int nHeight = rcStatus.Height();
		rcStatus.left = 10;
		rcStatus.top = 10;
		rcStatus.right = rcStatus.left + nWidth;
		rcStatus.bottom = rcStatus.top + nHeight;

		SetTextColor(hDC, hDevice->m_nStatusFontColor);
		dc.DrawText(hDevice->m_szStatus, &rcStatus, DT_LEFT);
	}

	if (strlen(hDevice->m_szPerfInfo) > 0)
	{
		int nLeft = 0;
		int nTop = 0;

		dc.SelectObject(pOldFont);
		CreateTextFont(hDevice, hDevice->m_nPerfInfoFontSize, hDevice->m_rcWnd.bottom - hDevice->m_rcWnd.top, &PerfInfofont);
		pOldFont = dc.SelectObject(&PerfInfofont);

		nRet = dc.DrawText(hDevice->m_szPerfInfo, &rcPerfInfo, DT_LEFT | DT_CALCRECT);
		//TRACEX(DTB_LOG_INFO, "DrawString - DrawText (%d)", nRet);

		int nWidth = rcPerfInfo.Width();
		int nHeight = rcPerfInfo.Height();

		// left-top
		nLeft = 10;
		nTop = 10;

		// right-top
		//nLeft = (hDevice->m_rcWnd.right - hDevice->m_rcWnd.left) / 2;
		//if (nLeft < nWidth)
		//{
		//	nLeft -= nWidth - nLeft;
		//}

		// right-bottom
		//nTop = (hDevice->m_rcWnd.bottom - hDevice->m_rcWnd.top) - nHeight - 10;

		// left-bottom
		//nLeft = 10;

		rcPerfInfo.left = nLeft;
		rcPerfInfo.top = nTop;
		rcPerfInfo.right = rcPerfInfo.left + nWidth + 10;
		rcPerfInfo.bottom = rcPerfInfo.top + nHeight + 10;
		
		if (hDevice->m_hTextWnd)
		{
			HBRUSH	hBrush;
			hBrush = CreateSolidBrush(RGB(255, 255, 1));
			SetBkMode(hDC, TRANSPARENT);
			FillRect(hDC, &rcPerfInfo, hBrush);			
			DeleteObject(hBrush);
		}

		SetTextColor(hDC, hDevice->m_nPerfInfoFontColor);
		SetBkMode(hDC, TRANSPARENT);
		nRet = dc.DrawText(hDevice->m_szPerfInfo, &rcPerfInfo, DT_LEFT | DT_WORDBREAK);
		//TRACEX(DTB_LOG_INFO, "DrawString - DrawText (%d)", nRet);
	}


	dc.SetBkMode(oldmode);
	dc.SelectObject(pOldFont);
	dc.Detach();

	if (hDevice->m_hTextWnd)
	{
		//ReleaseDC(hDevice->m_hTextWnd, hDC);
	}
	else
	{	
		//hRet = hDevice->m_DDraw.m_pSurface->ReleaseDC(hDC);
		hRet = hDevice->m_DDraw.m_pBackBuffer->ReleaseDC(hDC);
		if (hRet != D3D_OK)
		{
			TRACEX(DTB_LOG_ERROR, "DrawString - ReleaseDC 0x%x", hRet);
			return	FALSE;
		}
	}

	hDevice->m_DDraw.m_pBackBuffer = NULL;
#endif
	return	TRUE;
}

BOOL DrawStringFreeType(McVideoOutStructHandle hDevice)
{
#if 0
	HRESULT		hRet;
	CFont		Textfont;
	CFont		Statusfont;
	CFont		PerfInfofont;

	CFont		Textbkfont;
	CString		strText;
	CRect		rcText;
	CRect		rcStatus;
	CRect		rcPerfInfo;
	CFont*		pOldFont;
	int			i;
	int			oldmode;
	wchar_t		szGetHeight[10] = L"a";
	int			nFontHeight = 0;
	int			nTotalHeight = 0;
	int			nLineCount = 0;
	wchar_t		*szLineptr = NULL;
	wchar_t		szBuff[MAX_TEXT_STRING_LEN];
	wchar_t		tmpBuff[MAX_TEXT_STRING_LEN];

	if ((hDevice->m_TextBlock.block_num == 0) &&
		(strlen(hDevice->m_szStatus) == 0) &&
		(strlen(hDevice->m_szPerfInfo) == 0))
	{
		hDevice->m_bText = FALSE;
		return FALSE;
	}

	// Get BackBuffer
	if (hDevice->m_DDraw.m_pBackBuffer == NULL)
	{
		// Get BackBuffer
		IDirect3DSurface9* pBackBuffer;
		HRESULT hr = hDevice->m_DDraw.m_pD3DD9->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &pBackBuffer);
		if (FAILED(hr))
		{
			TRACEX(DTB_LOG_ERROR, "[DevOutVideo] DirectDrawPlay - GetBackBuffer 0x%x ", hr);
			return FALSE;
		}
		hDevice->m_DDraw.m_pBackBuffer = pBackBuffer;
	}

	if (hDevice->m_hDrawText == NULL)
	{
		McTextInfo	TextInfo = { 0, };

		TextInfo.m_hWnd = hDevice->m_hWnd;

		LRSLT lRet = DrawTextOpen(&hDevice->m_hDrawText, &TextInfo);
		if (lRet != MC_OK)
		{
			TRACEX(DTB_LOG_ERROR, "[DevOutVideo] DrawTextOpen failed 0x%x", lRet);
			return FALSE;
		}
	}

	// SubTitle

	//hDevice->m_dwTextSize

	CRect	rect;

	hDevice->m_FontColor = hDevice->m_TextBlock.block[0].font_color;

	if (hDevice->m_dwTextDisplayType == TEXT_VIDEO_DISPLAY)
		SetRect(&rcText, 0, 0, hDevice->m_dwWndWidth, hDevice->m_rcDest.bottom);
	else
		SetRect(&rcText, 0, 0, hDevice->m_dwWndWidth, hDevice->m_dwWndHeight);

	rcText.left += 5;
	rcText.right -= 5;

	if (hDevice->m_dwLeftMovePos - hDevice->m_dwRightMovePos>0)
		rcText.right -= hDevice->m_dwLeftMovePos - hDevice->m_dwRightMovePos;
	else
		rcText.left += hDevice->m_dwRightMovePos - hDevice->m_dwLeftMovePos;


	IDirect3DSurface9	*pSurface;
	D3DLOCKED_RECT		locked;

	pSurface = hDevice->m_DDraw.m_pBackBuffer;
	if (!pSurface)
		return FALSE;


	HRESULT				hr;
	D3DSURFACE_DESC		desc;

	DWORD32		dwTick;
		
	//TRACE("Rendering Time == %10d",  (dwTick = McGetTickCount()));

	hr = pSurface->LockRect(&locked, 0, D3DLOCK_NOSYSLOCK);
	if (FAILED(hr))
	{
		TRACEX(DTB_LOG_ERROR, "[DevOutVideo] DrawStringFreeType - Lock failed error 0x%x", hr);
		return FALSE;
	}

	if (1)
	{
		hr = pSurface->GetDesc(&desc);
		if (hr != D3D_OK)
		{
			TRACEX(DTB_LOG_ERROR, "[DevOutVideo] DrawStringFreeType - error 0x%x", hr);
			return FALSE;
		}
		switch (desc.Format)
		{
		case D3DFMT_A8R8G8B8:
		case D3DFMT_X8R8G8B8:
			break;
		default:
			return FALSE;
		}
	}

	BYTE				*pSurfaceBuf, *pTemp;
	DWORD				dwPitch, dwWidth, dwHeight;

	// Surface memory infos
	pSurfaceBuf = (BYTE*)locked.pBits;
	dwPitch = (DWORD32)locked.Pitch;
	dwWidth = desc.Width;
	dwHeight = desc.Height;


	McMediaVideo	VideoMedia = { 0, },
					*pMedia = &VideoMedia;

	pMedia->m_eColorFormat = VideoFmtRGBA;
	pMedia->m_pY = pSurfaceBuf;
	pMedia->m_dwPitch = dwPitch;

	// Subtitle
	if (1)
		DrawTextPutString(hDevice->m_hDrawText, &hDevice->m_TextBlock, pMedia);
	//TRACE("Rendering Time %5d (%10d => %10d)", dwTick2 - dwTick, dwTick, dwTick2);

	// Status
	if (0<strlen(hDevice->m_szStatus))
	{
		//dc.SelectObject(pOldFont);
		//CreateTextFont(hDevice, hDevice->m_nStatusFontSize, hDevice->m_rcDest.bottom - hDevice->m_rcDest.top, &Statusfont);
		//pOldFont = dc.SelectObject(&Statusfont);

		//dc.DrawText(hDevice->m_szStatus, &rcStatus, DT_LEFT | DT_CALCRECT);

		int nWidth = rcStatus.Width();
		int nHeight = rcStatus.Height();
		rcStatus.left = 10;
		rcStatus.top = 10;
		rcStatus.right = rcStatus.left + nWidth;
		rcStatus.bottom = rcStatus.top + nHeight;

		//SetTextColor(hDC, hDevice->m_nStatusFontColor);
		//dc.DrawText(hDevice->m_szStatus, &rcStatus, DT_LEFT);
	}

	//strcpy(hDevice->m_szPerfInfo, "this is test\n this is a test");

	// Performance 
	if (0 < strlen(hDevice->m_szPerfInfo))
	{
		CRect		rcPerfInfo;

		int nLeft = 0;
		int nTop = 0;

		//dc.SelectObject(pOldFont);
		//CreateTextFont(hDevice, hDevice->m_nPerfInfoFontSize, hDevice->m_rcWnd.bottom - hDevice->m_rcWnd.top, &PerfInfofont);
		//pOldFont = dc.SelectObject(&PerfInfofont);

		//dc.DrawText(hDevice->m_szPerfInfo, &rcPerfInfo, DT_LEFT | DT_CALCRECT);

		int nWidth = rcPerfInfo.Width();
		int nHeight = rcPerfInfo.Height();

		// left-top
		nLeft = 10;
		nTop = 10;

		// right-top
		nLeft = (hDevice->m_rcWnd.right - hDevice->m_rcWnd.left) / 2;
		if (nLeft < nWidth)
		{
			nLeft -= nWidth - nLeft;
		}

		// right-bottom
		//nTop = (hDevice->m_rcWnd.bottom - hDevice->m_rcWnd.top) - nHeight - 10;

		// left-bottom
		//nLeft = 10;

		rcPerfInfo.left = nLeft;
		rcPerfInfo.top = nTop;
		rcPerfInfo.right = rcPerfInfo.left + nWidth;
		rcPerfInfo.bottom = rcPerfInfo.top + nHeight;

		//SetTextColor(hDC, hDevice->m_nPerfInfoFontColor);
		//dc.DrawText(hDevice->m_szPerfInfo, &rcPerfInfo, DT_LEFT | DT_WORDBREAK);

		McMediaVideo	VideoMedia = { 0, },
						*pMedia = &VideoMedia;

		pMedia->m_eColorFormat = VideoFmtRGBA;
		pMedia->m_pY = pSurfaceBuf;
		pMedia->m_dwPitch = dwPitch;

		DrawTextPutString4(hDevice->m_hDrawText, hDevice->m_szPerfInfo, pMedia);
	}

	// Unlock locked surface
	pSurface->UnlockRect();
	DWORD32 dwTick2;

	dwTick2 = McGetTickCount();
#endif

	return	TRUE;
}

BOOL DirectDrawPlayString(McVideoOutStructHandle hDevice)
{
	HRESULT		hr;

	DrawString(hDevice);
	//DrawStringFreeType(hDevice);
	return TRUE;
}

void DirectDrawError(HRESULT hRet, char* strFunc, BOOL bBox)
{
}

#endif // VIDEOOUT_D3D9DRAW