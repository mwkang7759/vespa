/*****************************************************************************
*                                                                            *
*                             MWCODEC UTILITY                          *
*                                                                            *
*   Copyright (c) 2002-2003 by Mcubeworks, Incoporated. All Rights Reserved. *
*****************************************************************************/

/******************************* FILE HEADER *********************************

    File Name       : mpp_util.h
    Included files  : "mw_codec.h"
    Module          : Header for Utilily structure

    Author(s)       : Kerry Lee (kerrylee@mcubeworks.com)
    Created         : 18 Dec. 2002

    Description     : Header for Utility structure
    Notes           : 

==============================================================================
                      Modification History (Reverse Order)                    
==============================================================================
    Date        Author      Location (variables) / Description
------------------------------------------------------------------------------
  
------------------------------------------------------------------------------
*****************************************************************************/

#ifndef __MPP_UTIL_H__
#define __MPP_UTIL_H__

#include "mw_codec.h"

#define API_VERSION_MPP_UTIL    21218

#endif    // __CFG_VIDEO_H__
