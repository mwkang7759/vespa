/*****************************************************************************
*                                                                            *
*                          MWCODEC UTILITY YUV2RGB                          *
*                                                                            *
*   Copyright (c) 2002-2003 by Mcubeworks, Incoporated. All Rights Reserved. *
*****************************************************************************/

/******************************* FILE HEADER *********************************

    File Name       : mpp_util_yuv2rgb16.h
    Included files  : "mw_codec.h"
    Module          : Header for YUV420 to RGB16 conversion

    Author(s)       : Kerry Lee (kerrylee@mcubeworks.com)
    Created         : 2004/01/26

    Description     : Header for YUV to RGB structure
    Notes           : 

==============================================================================
                      Modification History (Reverse Order)                    
==============================================================================
    Date    Author  Location (variables) / Description
------------------------------------------------------------------------------
  040126		Kerry		Seperated from Janet. YUV420 to RGB12/16 only
------------------------------------------------------------------------------
*****************************************************************************/

#ifndef __MPP_UTIL_YUV2RGB16_H__
#define __MPP_UTIL_YUV2RGB16_H__

#include "mpp_util.h"

#define API_VERSION_MPP_UTIL_YUV2RGB16     40126

#define ApiVerMppUtilYUV2RGB16  { API_VERSION_MW_CODEC, \
                                  API_VERSION_MPP_UTIL, \
                                  API_VERSION_MPP_UTIL_YUV2RGB16, \
                                  NULL }

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Structure (YUV420 to RGB) - Specific Configurations
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#define ONE_BGR_SIZE            3

typedef struct   POINTER_YUV_tag        
{
    unsigned char *pbY;             // Y Left-Upper pointer
    unsigned char *pbU;             // U Left-Upper pointer
    unsigned char *pbV;             // V Left-Upper pointer
} POINTER_YUV;

typedef struct   INFO_SIZE_tag  
{
    int           YUV_yStep;              // mem-jump, moving down in y-direction
    int           YUV_CropWidth;
    int           YUV_CropHeight;
    int           RGB_xStep;              // RGB mem-jump in size of byte, moving in YUV x-direction
    int           RGB_yStep;              // RGB mem-jump in size of byte, moving in YUV y-direction
} INFO_SIZE;


// Function Declaration
#ifdef __cplusplus
extern "C" 
{
#endif

//========================== Check API Version & Get Codec Version ==============================================================
int     MppYUV2RGB_CheckVersion (const MW_API_VERSION *pApiVer, MW_CODEC_VERSION *pCodecVer);

//=============================== YUV2RGB Conversion Functions =========================================================================
#define YUV420_To_RGB12_Q   YUV420_To_RGB16_Q
#define YUV420_To_RGB12_H   YUV420_To_RGB16_H
void    YUV420_To_RGB12_S   (unsigned short *pwRGB12, POINTER_YUV *pYUVP, INFO_SIZE *pSizeInfo);
void    YUV420_To_RGB12_D   (unsigned short *pwRGB12, POINTER_YUV *pYUVP, INFO_SIZE *pSizeInfo);

void    YUV420_To_RGB16_Q   (unsigned short *pwRGB16, POINTER_YUV *pYUVP, INFO_SIZE *pSizeInfo);
void    YUV420_To_RGB16_H   (unsigned short *pwRGB16, POINTER_YUV *pYUVP, INFO_SIZE *pSizeInfo);
void    YUV420_To_RGB16_S   (unsigned short *pwRGB16, POINTER_YUV *pYUVP, INFO_SIZE *pSizeInfo);
void    YUV420_To_RGB16_4_3 (unsigned short *pwRGB16, POINTER_YUV *pYUVP, INFO_SIZE *pSizeInfo);
void    YUV420_To_RGB16_D   (unsigned short *pwRGB16, POINTER_YUV *pYUVP, INFO_SIZE *pSizeInfo);
//==================================================================================================
#ifdef __cplusplus
}
#endif

//================================= Description ============================================
// Name         :   YUV420 to RGB
// Author(s)    :   Kerry Lee
// Created      :   2002/12/20
//
// Purpose      :   To convert color format from YUV 4:2:0 to 16-bit or 12-bit RGB 

//------------------------------- Function Nomenclature ------------------------------------
//
// YUV[YUV Format(1)]_To_[RGB Order(2)]_[RGB Precision(3)]_[Display Scale(4)]_[Charteristic(5)]
//
//      (1) YUV Format          : 444, 422L[Landscape], 422P(Portrait), 420, 400(Y only))
//      (2) RGB Order           : RGB or BGR
//      (3) RGB Precision       :24 [8-bit + 8-bit + 8-bit],
//                               16 [5-bit + 6-bit + 5-bit],
//                              *12 (4-bit +(1-*ubit) + 4-bit +(2-ubit) + 4-bit + (1-ubit)) 
//                              *12 ; actually 16-bit is used but, totall 4-bits are unused bit(ubit)s.
//      (4) Display Scale       : D[Double], S[Same], H[Half], Q[Quarter] 
//      (5) Characteristic      : Flip ( Up-side Down ), Flop ( Left-Right mirror )

//--------------------------------- Input Arguments     ----------------------------------------        :
//      POINTER_YUV *pYUVP
//      - Structure of YUV pointers with STARTING OFFSET (incase Image Cropping is specified)
//      ! 1. pYUVP->pY must be aligned to 2-PIXEL boundary both on x and y)
//
//      INFO_SIZE *pSizeInfo
//      - Size information of YUV/RGB image
//      ! 1. pSizeInfo->YUV_CropWidth and pSizeInfo->YUV_CropHeight should be even value 
//              for Double/Equal scale and multiful of FOUR value for Half/Quarter scale. 
//      2. RGB_xStep and RGB_yStep can be positive or negative value.
//
//      int nWidth
//      - the size of Image pels
//      int nHeight
//      - the size of Image lines
//----------------------------------- Output Arguments -------------------------------------
//      - Pointer to output RGB frame buffer with STARTING OFFSET 
//      (usually LCD display buffer with Display Origin offset)
//
//      unsigned short *pwRGB12         // 12-bit RGB frame buffer
//
//      unsigned short *pwRGB16         // 16-bit RGB frame buffer 
//			
//      unsigned char *pBGR24Flip       // 24-bit RGB frame buffer
//			unsigned char *pRGB24						// 24-bit RGB frame buffer	// added by Kerry 030128
//
// Return       :   
// Side effects :
// Description  :   
// Modified     :
//----------------------------------- Example #1 -------------------------------------------------------
//
//GXDisplayProperties GX_Display_Properties = GXGetDisplayProperties();
// 
//----------------------------------- Example #3 -----------------------------------------------------
//YUV420_To_RGB16_D (pwRGB16, pYUVP, pSizeInfo);
//      pRGB16          = psOriginalRGB + LCD Display Offset
//
//      !* when the Display origin is Upper Right or Down Left, we do cropping to 160 * 120 
//      !* 입력의 YUV 크기가 QCIF의 경우 2배를 하게 되면 320x240의 Display 크기를 초과 하므로
//      이때 실제 입력 영상을 원하는 만끔 짤라서(crop)넣어 준다. 
//
//      ! 왼쪽 상단에서 160 x 120만큼 짜를려면 아래와 같이 하고..
//
//      pYUVP->pbY      = pbOriginalYUV;
//      pYUVP->pbU      = pbOriginalYUV + 176 * 144;
//      pYUVP->pbV      = pbOriginalYUV + 176 * 144 + 176 * 144/4; 
//
//      ! 만약 가운데를 중심으로 160 x 120 만클 자를 려면 아래와 같이 하면 된다.
//
//      pYUVP->pbY      = pbOriginalYUV +                               (12 * YUV_Width + 8);
//      pYUVP->pbU      = pbOriginalYUV + 176 * 144 +                   (3  * YUV_Width + 4);
//      pYUVP->pbV      = pbOriginalYUV + 176 * 144 + 176 * 144/4 +     (3  * YUV_Width + 4);
//
//      pSizeInfo->YUV_yStep            = YUV_Width (QCIF_WIDTH);
//      pSizeInfo->YUV_CropWidth        = QVGA_WIDTH / 2;
//      pSizeInfo->YUV_CropHeight       = QVGA_HEIGHT / 2;
//
// Up-Right Origin
//      pSizeInfo->RGB_xStep            = GX_Display_Properties.cbyPitch;               
//      pSizeInfo->RGB_yStep            = -GX_Display_Properties.cbxPitch;      
//
//----------------------------------- Example #4 ------------------------------------------------------
//YUV420_To_RGB16_S (pwRGB16, pYUVP, pSizeInfo);
//      pRGB16          = psOriginalRGB
//
//      pYUVP->pbY      = pbOriginalYUV; 
//      pYUVP->pbU      = pbOriginalYUV + YUV_Width * YUV_Height; 
//      pYUVP->pbV      = pbOriginalYUV + YUV_Width * YUV_Height + YUV_Width * YUV_Height/4 
//
//      pSizeInfo->YUV_yStep            = YUV_Width;
//      pSizeInfo->YUV_CropWidth        = YUV_Width;
//      pSizeInfo->YUV_CropHeight       = YUV_Height;
//
// Up-Left Origin
//      pSizeInfo->RGB_xStep            = GX_Display_Properties.cbxPitch;               
//      pSizeInfo->RGB_yStep            = GX_Display_Properties.cbyPitch;       
// Up-Right Origin
//      pSizeInfo->RGB_xStep            = GX_Display_Properties.cbyPitch;               
//      pSizeInfo->RGB_yStep            = -GX_Display_Properties.cbxPitch;      
// Down-Left Origin
//      pSizeInfo->RGB_xStep            = -GX_Display_Properties.cbyPitch;              
//      pSizeInfo->RGB_yStep            = GX_Display_Properties.cbxPitch;       
// Down-Right Origin
//      pSizeInfo->RGB_xStep            = -GX_Display_Properties.cbxPitch;              
//      pSizeInfo->RGB_yStep            = -GX_Display_Properties.cbyPitch;      

#endif    // __MPP_UTIL_YUV2RGB16_H__
