/*****************************************************************************
*                                                                            *
*                            McubeWorks Codec                                *
*                                                                            *
*   Copyright (c) 2002-2003 by Mcubeworks, Incoporated. All Rights Reserved. *
*****************************************************************************/

/******************************* FILE HEADER *********************************

    File Name       : mw_codec.h
    Included files  :
    Module          : Header for global type definition

    Author(s)       : Seung Jun Lee (jun@mcubeworks.com)
    Created         : 17 Dec. 2002

    Description     : Header for global type definition
    Notes           : 

==============================================================================
                      Modification History (Reverse Order)                    
==============================================================================
    Date        Author      Location (variables) / Description
------------------------------------------------------------------------------
  2002/12/17    jun         New API (based on previous mPion Projects)
  2003/02/19    jun         API 정리 (config 파일들 없앰. 기타 정리)
                            MW_DATA에 nLenAlloc추가
------------------------------------------------------------------------------
*****************************************************************************/

#ifndef __MW_CODEC_H__
#define __MW_CODEC_H__

#define API_VERSION_MW_CODEC       30219

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Conventional Type Definitions
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// BOOL Type Definition
#if !defined(BOOL)
      typedef int                  BOOL;
#endif

// TRUE, FALSE & NULL Definition
#if !defined(FALSE)
      #define FALSE                0
#endif
#if !defined(TRUE)
      #define TRUE                 1
#endif
#if !defined(NULL)
      #define NULL                 0
#endif

// RETURN VALUE Definition
#if defined(RET_SUCCESS)
   #undef RET_SUCCESS
#endif
#define   RET_SUCCESS              0

#if defined(RET_FAIL)
   #undef RET_FAIL
#endif
#define   RET_FAIL                -1

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Structure (API / CODEC MW_CODEC_VERSION)
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

typedef struct
{
// The value of each element is the date of update (ex. 21120)
        int              nApiMWCodec;
        int              nApiMedia;
        int              nApiCodec;
        char            *pchCheckString;
} MW_API_VERSION;

typedef struct
{
        int              nMajor;         // Version Major number
        int              nMinor;         // Version Minor number
        int              nPatch;         // Version Patch number
        char             szCode[32];     // Release Code (alpha, beta, ARM-20011115, etc...)
        char             szComment[128]; // Codec Developer's Comment Text
} MW_CODEC_VERSION;

typedef struct
{
        int              nLen;           // 실제 주고받는(effective data) pchData의 사이즈
        int              nLenAlloc;      // allocation된 pchData의 사이즈(MW_DATA가 출력아규먼트로 쓰일때만 유용)
                                         // 메모리를 allocation하는 쪽에서 해당값으로 채워줌
                                         // called party에서 alloc하기로 정한 경우에는 calling party가 이 값을 -1으로 채워 줄 것 !!
                                         // 이 경우에는 calling party에서 사용 후 free한다
                                         // '0'은 forbidden value, '-1 이 아닌 음수값'도 forbidden value
        char            *pchData;        // 실제 데이터의 포인터
                                         // 위에 적은 것처럼 calling party 또는 called party 어느 곳에서든
                                         // allocation 될 수 있다. 단, 이는 API function별로 미리 정하며
                                         // 위의 nLenAlloc값을 통하여 확인할 수 있다.
} MW_DATA;

#endif
