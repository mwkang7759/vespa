#ifndef __MCW_I420_RGB565_RCR_H__
#define __MCW_I420_RGB565_RCR_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
    int   iSrcRotate;  // Flag to inform whether Src is CW-rotated (1), CCW-rotated(-1), or no-rotated(0).
    int   iSrcW;			 // even
    int   iSrcH;	     // even
    int   iSrcYInc;
    int   iDstW;	     // even
    int   iDstH;	     // even
    int   iDstXInc;
    int   iDstYInc;
} MCW_I420_RGB565_GEOMETRY;

// CAUTION =====================================================================
//   Never use *ppbyI420 and *pSI420Rgb565Geom after calling this function.
//==============================================================================
extern void mcw_i420_rgb565_RenderFrame(
    unsigned short *pwRgb565/* 4-Byte aligned*/, unsigned char **ppbyI420, MCW_I420_RGB565_GEOMETRY *pSI420Rgb565Geom);

#ifdef __cplusplus
}
#endif

#endif  // #ifndef __MCW_I420_RGB565_RCR_H__
