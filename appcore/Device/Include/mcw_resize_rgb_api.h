/*****************************************************************************
*                                                                            *
*                                  Resize                                    *
*                                                                            *
*   Copyright (c) 2005- by Mcubeworks, Incoporated. All Rights Reserved.     *
*****************************************************************************/


/*============================== FILE HEADER =================================

    File Name       : mcw_resize_api.h
    Module          : Header for Resize

    Author(s)       : Jae-ill, Pi (issun@mcubeworks.com)
    Created         : 2006/01/16

    Description     : Header for Resize
    Notes           :

============================================================================*/

/*================ Modification History (Reverse Order) ======================
	2006/01/16		: Jae-ill,  create
	2006/02/21		: Jae-ill,  add RGB16_ImageResize()
============================================================================*/

#ifndef __MCW_RESIZE_API_H__
#define __MCW_RESIZE_API_H__

typedef struct RESIZE_ST {
    unsigned char *pImage;
    int            iXsize;
    int            iYsize;
} RESIZE_ST;


//=========================  FUNCTION DECLARATION   ==========================
#ifdef __cplusplus
extern "C"
{
#endif

//========================== JPEG Encoder Functions ==========================
void RGB24_ImageResize(RESIZE_ST *pIN, RESIZE_ST *pOUT);
void RGB16_ImageResize(RESIZE_ST *pIN, RESIZE_ST *pOUT);

//============================================================================
#ifdef __cplusplus
}
#endif

#endif // __MCW_RESIZE_API_H__
