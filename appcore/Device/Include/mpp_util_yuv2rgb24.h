/*****************************************************************************
*                                                                            *
*                          MWCODEC UTILITY YUV2RGB                          *
*                                                                            *
*   Copyright (c) 2002-2004 by Mcubeworks, Incoporated. All Rights Reserved. *
*****************************************************************************/

/******************************* FILE HEADER *********************************

    File Name       : mpp_util_yuv2rgb24.h
    Included files  : "mw_codec.h"
    Module          : Header for YUV420 to RGB24 conversion

    Author(s)       : Kerry Lee (kerrylee@mcubeworks.com)
    Created         : 2004/01/26

    Description     : Header for YUV420 to RGB24 structure
    Notes           : 

==============================================================================
                      Modification History (Reverse Order)                    
==============================================================================
    Date    Author  Location (variables) / Description
------------------------------------------------------------------------------
  040126		Kerry		Seperated from Janet. YUV420 to RGB24 only (Mostly for X86)
------------------------------------------------------------------------------
*****************************************************************************/

#ifndef __MPP_UTIL_YUV2RGB24_H__
#define __MPP_UTIL_YUV2RGB24_H__

#include "mpp_util.h"

#define API_VERSION_MPP_UTIL_YUV2RGB24     40126

#define ApiVerMppUtilYUV2RGB24  { API_VERSION_MW_CODEC, \
                                  API_VERSION_MPP_UTIL, \
                                  API_VERSION_MPP_UTIL_YUV2RGB24, \
                                  NULL }

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Structure (YUV420 to RGB) - Specific Configurations
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#define ONE_BGR_SIZE            3

typedef struct   POINTER_YUV_tag        
{
    unsigned char *pbY;             // Y Left-Upper pointer
    unsigned char *pbU;             // U Left-Upper pointer
    unsigned char *pbV;             // V Left-Upper pointer
} POINTER_YUV;

typedef struct   INFO_SIZE_tag  
{
    int           YUV_yStep;              // mem-jump, moving down in y-direction
    int           YUV_CropWidth;
    int           YUV_CropHeight;
    int           RGB_xStep;              // RGB mem-jump in size of byte, moving in YUV x-direction
    int           RGB_yStep;              // RGB mem-jump in size of byte, moving in YUV y-direction
} INFO_SIZE;


// Function Declaration
#ifdef __cplusplus
extern "C" 
{
#endif

//========================== Check API Version & Get Codec Version ==============================================================
int     MppYUV2RGB_CheckVersion (const MW_API_VERSION *pApiVer, MW_CODEC_VERSION *pCodecVer);

//=============================== YUV2RGB Conversion Functions =========================================================================
void	  YUV420_To_RGB24_S	        (unsigned char *pRGB24,     POINTER_YUV *pYUVP, int nWidth, int nHeight);	// added by kerry 030127 for 55xRGB conversion test
void    YUV420_To_BGR24_S_Flip    (unsigned char *pBGR24Flip, POINTER_YUV *pYUVP, int nWidth, int nHeight);
void	  YUV420_To_BGR24_S_Flip_MMX(unsigned char *pBGR24Flip, POINTER_YUV *pYUVP, int nWidth, int nHeight);  // added by kerry for 24bit RGB with x86 MMX 
//======================================================================================================================================================
#ifdef __cplusplus
}
#endif

//================================= Description ============================================
// Name         :   YUV420 to RGB24
// Author(s)    :   Kerry Lee
// Created      :   2004/01/26
//
// Purpose      :   To convert color format from YUV 4:2:0 to 24-bit RGB 

//------------------------------- Function Nomenclature ------------------------------------
//
// YUV[YUV Format(1)]_To_[RGB Order(2)]_[RGB Precision(3)]_[Display Scale(4)]_[Charteristic(5)]
//
//      (1) YUV Format          : 444, 422L[Landscape], 422P(Portrait), 420, 400(Y only))
//      (2) RGB Order           : RGB or BGR
//      (3) RGB Precision       :24 [8-bit + 8-bit + 8-bit],
//                               16 [5-bit + 6-bit + 5-bit],
//                              *12 (4-bit +(1-*ubit) + 4-bit +(2-ubit) + 4-bit + (1-ubit)) 
//                              *12 ; actually 16-bit is used but, totall 4-bits are unused bit(ubit)s.
//      (4) Display Scale       : D[Double], S[Same], H[Half], Q[Quarter] 
//      (5) Characteristic      : Flip ( Up-side Down ), Flop ( Left-Right mirror )

//--------------------------------- Input Arguments     ----------------------------------------        :
//      POINTER_YUV *pYUVP
//      - Structure of YUV pointers with STARTING OFFSET (incase Image Cropping is specified)
//      ! 1. pYUVP->pY must be aligned to 2-PIXEL boundary both on x and y)
//
//      INFO_SIZE *pSizeInfo
//      - Size information of YUV/RGB image
//      ! 1. pSizeInfo->YUV_CropWidth and pSizeInfo->YUV_CropHeight should be even value 
//              for Double/Equal scale and multiful of FOUR value for Half/Quarter scale. 
//      2. RGB_xStep and RGB_yStep can be positive or negative value.
//
//      int nWidth
//      - the size of Image pels
//      int nHeight
//      - the size of Image lines
//----------------------------------- Output Arguments -------------------------------------
//      - Pointer to output RGB frame buffer with STARTING OFFSET 
//      (usually LCD display buffer with Display Origin offset)
//			
//      unsigned char *pBGR24Flip       // 24-bit RGB frame buffer
//			unsigned char *pRGB24						// 24-bit RGB frame buffer	// added by Kerry 030128
//
// Return       :   
// Side effects :
// Description  :   
// Modified     :
//---------------------------------- Example #1 -----------------------------------------------
//              
//YUV420_To_BGR24_S_Flip (pBGR24Flip, pYUVP, nWidth, nHeight);
// 
//      pBGR24Flip = psOriginalRGB + YUV_Width * (YUV_Height - 1)

//      pYUVP->pbY      = pbOriginalYUV;
//      pYUVP->pbU      = pbOriginalYUV  + YUV_Width(ex 176 = QCIF_WIDTH) * YUV_Height(ex 144 = QCIF_HEIGHT);
//      pYUVP->pbV      = pYUVP->pbU + (YUV_Width/2) * (YUV_Height/2);
//      
//      nWidth          = YUV_Width
//      nHeight         = YUV_Height
#endif    // __CFG_VIDEO_H26L_H__
