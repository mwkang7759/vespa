
#include "Rtsp.h"

LRSLT FrRTSPOpen(FrRTSPHandle* phHandle, RTSP_INFO* pInfo)
{
	FrRTSPStruct*	hRTSP;

	hRTSP = (FrRTSPStruct*)MALLOCZ(sizeof(FrRTSPStruct));
	if (hRTSP)
	{
		hRTSP->m_bServer		= pInfo->m_bServer;
		hRTSP->m_bUDP			= pInfo->m_bUDP;
		hRTSP->m_bHTTP			= pInfo->m_bHTTP;
		hRTSP->m_bMulti			= pInfo->m_bMulti;
		hRTSP->m_bMgosp			= pInfo->m_bMgosp;
		hRTSP->m_bMMS			= pInfo->m_bMMS;
		hRTSP->m_bTS			= pInfo->m_bTS;
		hRTSP->m_b3GP			= pInfo->m_b3GP;
		hRTSP->m_bPublishing	= pInfo->m_bPublishing;
		hRTSP->m_bSRT			= pInfo->m_bSRT;
		hRTSP->m_bUDPfec		= pInfo->m_bUDPfec;

		LOG_I("[RTSP] RTSPOpen(): bServer(%d), bUDP(%d), bSRT(%d)", hRTSP->m_bServer, hRTSP->m_bUDP, hRTSP->m_bSRT);

		hRTSP->m_dwStartDate	= pInfo->m_dwStartDate;
		hRTSP->m_dwStartTime	= pInfo->m_dwStartTime;
		hRTSP->m_dwEndDate		= pInfo->m_dwEndDate;
		hRTSP->m_dwEndTime		= pInfo->m_dwEndTime;

		hRTSP->m_dwDtbResultCode=200;
        hRTSP->m_dwDtbKeyCode = 0;
		hRTSP->m_dwBufferTime = pInfo->m_dwBufferTime;

		// set default server name
		strcpy(hRTSP->m_szServerName, "DTB Server/1.0.0");

		if (pInfo->m_pURL)
		{
			char *pRtspsUrl = NULL;
			if (!_tcsncmp(pInfo->m_pURL, _T("rtsps://"), 5))
			{
				// relay mode for rtsps
				if (pInfo->m_bRelay)
					hRTSP->m_bRelay	= TRUE;
				pRtspsUrl = UtilStrReplaceAll(pInfo->m_pURL, "rtsps://", "rtsp://");
				hRTSP->m_szContentURL		= UtilStrStrdup(pRtspsUrl);
				FREE(pRtspsUrl);
			}
			else if (!_tcsncmp(pInfo->m_pURL, _T("rtspws://"), 9))
			{
				pRtspsUrl = UtilStrReplaceAll(pInfo->m_pURL, "rtspws://", "rtsp://");
				hRTSP->m_szContentURL		= UtilStrStrdup(pRtspsUrl);
				FREE(pRtspsUrl);
			}
			else if (!_tcsncmp(pInfo->m_pURL, _T("rtspwss://"), 10))
			{
				pRtspsUrl = UtilStrReplaceAll(pInfo->m_pURL, "rtspwss://", "rtsp://");
				hRTSP->m_szContentURL		= UtilStrStrdup(pRtspsUrl);
				FREE(pRtspsUrl);
			}
			else
				hRTSP->m_szContentURL		= UtilStrStrdup(pInfo->m_pURL);

			hRTSP->m_szFilename			= UtilURLGetFilename(pInfo->m_pURL);
			hRTSP->m_szContentLocation	= UtilURLGetContentLocation(pInfo->m_pURL);
		}
		if (pInfo->m_pUserAgent)
			hRTSP->m_szUserAgent	= UtilStrStrdup(pInfo->m_pUserAgent);
		if (pInfo->m_pUserName)
			hRTSP->m_szUserName		= UtilStrStrdup(pInfo->m_pUserName);
		if (pInfo->m_pUserPasswd)
			hRTSP->m_szUserPasswd = pInfo->m_pUserPasswd;

		if (pInfo->m_dwSession && hRTSP->m_bServer)
		{
			hRTSP->m_pSession = (char*)MALLOC(MAX_SESSION_LEN);
			sprintf(hRTSP->m_pSession, "%u", pInfo->m_dwSession);
		}

		*phHandle = (FrRTSPHandle)hRTSP;
	}
	else
		return	COMMON_ERR_MEM;

	return	FR_OK;
}

void FrRTSPClose(FrRTSPHandle hHandle)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	McChannelHandle	hChannel;
	DWORD32			dwIndex;

	if (hRTSP)
	{
		for (dwIndex = 0;dwIndex < hRTSP->m_dwChannelNum;dwIndex++)
		{
			hChannel = hRTSP->m_aryChannelInfo[dwIndex];
			FREE(hChannel->m_pControl);
			FREE(hChannel);
		}
		FREE(hRTSP->m_aryChannelInfo);
		hRTSP->m_aryChannelInfo = NULL;

		FREE(hRTSP->m_pSDP);
		FREE(hRTSP->m_pBase64SDP);
		FREE(hRTSP->m_pSession);
		FREE(hRTSP->m_pControl);
		FREE(hRTSP->m_szContentBase);
		FREE(hRTSP->m_szUserAgent);
		FREE(hRTSP->m_szUserName);

		FREE(hRTSP->m_szMultiAddr);
		FREE(hRTSP->m_szLocalAddr);
		FREE(hRTSP->m_szFilename);
		FREE(hRTSP->m_szContentURL);
		FREE(hRTSP->m_szContentLocation);
		FREE(hRTSP->m_szMasterKey);
		FREE(hRTSP->m_szSaltingKey);
		FREE(hRTSP->m_szUdpSrcAddr);

		FREE(hRTSP->m_szDigestRealm);
		FREE(hRTSP->m_szDigestNonce);
		FREE(hRTSP);
	}
}

LRSLT FrRTSPInit(FrRTSPHandle hHandle, DWORD32 dwChannelNum)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	McChannelHandle	hChannel;
	DWORD32			dwIndex;

	hRTSP->m_dwChannelNum = dwChannelNum;
	hRTSP->m_aryChannelInfo = (McChannelHandle*)MALLOCZ(sizeof(McChannelHandle*)*hRTSP->m_dwChannelNum);

	for (dwIndex = 0;dwIndex < hRTSP->m_dwChannelNum;dwIndex++)
	{
		hChannel = (McChannelHandle)MALLOCZ(sizeof(McChannelStruct));
		if (!hChannel)
			return	COMMON_ERR_MEM;

		hChannel->m_dwIndex = dwIndex;
		hRTSP->m_aryChannelInfo[dwIndex] = hChannel;
	}

	return	FR_OK;
}

void FrRTSPGetConfigInfo(FrRTSPHandle hHandle, RTSP_INFO* pInfo)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;

	pInfo->m_bServer	= hRTSP->m_bServer;
	pInfo->m_bHTTP		= hRTSP->m_bHTTP;
	pInfo->m_bMgosp		= hRTSP->m_bMgosp;
	pInfo->m_bMulti		= hRTSP->m_bMulti;
	pInfo->m_bUDP		= hRTSP->m_bUDP;
    pInfo->m_bUDPfec    = hRTSP->m_bUDPfec;
}

void FrRTSPSetConfigInfo(FrRTSPHandle hHandle, RTSP_INFO* pInfo)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;

	hRTSP->m_bServer	= pInfo->m_bServer;
	hRTSP->m_bUDP		= pInfo->m_bUDP;
	hRTSP->m_bHTTP		= pInfo->m_bHTTP;
	hRTSP->m_bMulti		= pInfo->m_bMulti;
	hRTSP->m_bMgosp		= pInfo->m_bMgosp;
    hRTSP->m_bUDPfec    = pInfo->m_bUDPfec;
	if (pInfo->m_pURL)
	{
		FREE(hRTSP->m_szFilename);
		FREE(hRTSP->m_szContentLocation);
		FREE(hRTSP->m_szContentURL);
		hRTSP->m_szFilename			= UtilURLGetFilename(pInfo->m_pURL);
		hRTSP->m_szContentLocation	= UtilURLGetContentLocation(pInfo->m_pURL);
		hRTSP->m_szContentURL		= UtilStrStrdup(pInfo->m_pURL);
	}
	if (pInfo->m_pUserAgent)
	{
		FREE(hRTSP->m_szUserAgent);
		hRTSP->m_szUserAgent	= UtilStrStrdup(pInfo->m_pUserAgent);
	}
}

// method
LRSLT FrRTSPHttpGetMethod(FrRTSPHandle hHandle, char* pBuffer, DWORD32 dwBuffLen)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;

	if (!RTSPMakeHTTPGetRequest(hRTSP, pBuffer))
		return	COMMON_ERR_INVALID_PROTOCOL;

	return	FR_OK;
}

LRSLT FrRTSPAnnounceMethod(FrRTSPHandle hHandle, char* pBuffer, DWORD32 dwBuffLen)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;

	if (!RTSPMakeAnnounceRequest(hRTSP, pBuffer))
		return	COMMON_ERR_INVALID_PROTOCOL;

	return	FR_OK;
}

LRSLT FrRTSPOptionsMethod(FrRTSPHandle hHandle, char* pBuffer, DWORD32 dwBuffLen)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;

	if (!RTSPMakeOptionsRequest(hRTSP, pBuffer))
		return	COMMON_ERR_INVALID_PROTOCOL;

	return	FR_OK;
}

LRSLT FrRTSPDescribeMethod(FrRTSPHandle hHandle, char* pBuffer, DWORD32 dwBuffLen)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	char			pTemp[MAX_RTSP_MSG_SIZE];

	if (hRTSP->m_bHTTP)
	{
		if (!RTSPMakeDescribeRequest(hRTSP, pTemp))
			return	COMMON_ERR_INVALID_PROTOCOL;
		if (!RTSPMakeHTTPPostRequest(hRTSP, pBuffer, pTemp))
			return	COMMON_ERR_INVALID_PROTOCOL;
	}
	else
	{
		if (!RTSPMakeDescribeRequest(hRTSP, pBuffer))
			return	COMMON_ERR_INVALID_PROTOCOL;
	}

	return	FR_OK;
}

LRSLT FrRTSPSetupMethod(FrRTSPHandle hHandle, char* pBuffer, DWORD32 dwBuffLen, DWORD32 dwIndex)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;

	LOG_I("[RTSP] FrRTSPSetupMethod - Start..");

	if (!hRTSP->m_dwChannelNum)
		return	COMMON_ERR_NOMEDIA;

	hRTSP->m_hReqChannel = hRTSP->m_aryChannelInfo[dwIndex];

	if (!RTSPMakeSetupRequest(hRTSP, pBuffer, dwIndex))
		return	COMMON_ERR_INVALID_PROTOCOL;

	return	FR_OK;
}

LRSLT FrRTSPPlayMethod(FrRTSPHandle hHandle, char* pBuffer, DWORD32 dwBuffLen, BOOL bResume, DWORD32 dwPos)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;

	if (!hRTSP->m_dwChannelNum)
		return	COMMON_ERR_NOMEDIA;

	if (!RTSPMakePlayRequest(hRTSP, pBuffer, bResume, dwPos))
		return	COMMON_ERR_INVALID_PROTOCOL;

	return	FR_OK;
}

LRSLT FrRTSPPlayClockMethod(FrRTSPHandle hHandle, char* pBuffer, DWORD32 dwBuffLen, DWORD32 dwDate, DWORD32 dwTime)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;

	if (!hRTSP->m_dwChannelNum)
		return	COMMON_ERR_NOMEDIA;

	if (!RTSPMakePlayClockRequest(hRTSP, pBuffer, dwDate, dwTime))
		return	COMMON_ERR_INVALID_PROTOCOL;

	return	FR_OK;
}

LRSLT FrRTSPPlayScaleMethod(FrRTSPHandle hHandle, char* pBuffer, DWORD32 dwBuffLen, int nScale)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;

	if (!hRTSP->m_dwChannelNum)
		return	COMMON_ERR_NOMEDIA;

	if (!RTSPMakePlayScaleRequest(hRTSP, pBuffer, nScale))
		return	COMMON_ERR_INVALID_PROTOCOL;

	return	FR_OK;
}

LRSLT FrRTSPPauseMethod(FrRTSPHandle hHandle, char* pBuffer, DWORD32 dwBuffLen)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;

	if (!hRTSP->m_dwChannelNum)
		return	COMMON_ERR_NOMEDIA;

	if (!RTSPMakePauseRequest(hRTSP, pBuffer))
		return	COMMON_ERR_INVALID_PROTOCOL;

	return	FR_OK;
}

LRSLT FrRTSPTeardownMethod(FrRTSPHandle hHandle, char* pBuffer, DWORD32 dwBuffLen)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;

	if (!hRTSP->m_dwChannelNum)
		return	COMMON_ERR_NOMEDIA;

	if (!RTSPMakeTeardownRequest(hRTSP, pBuffer))
		return	COMMON_ERR_INVALID_PROTOCOL;

	return	FR_OK;
}

LRSLT FrRTSPGetParamMethod(FrRTSPHandle hHandle, char* pBuffer, DWORD32 dwBuffLen, char* pContent)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;

	if (!hRTSP->m_dwChannelNum)
		return	COMMON_ERR_NOMEDIA;

	if (!RTSPMakeGetParamRequest(hRTSP, pBuffer, pContent))
		return	COMMON_ERR_INVALID_PROTOCOL;

	return	FR_OK;
}

LRSLT FrRTSPSetParamMethod(FrRTSPHandle hHandle, char* pBuffer, DWORD32 dwBuffLen, char* pContent)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;

	if (!RTSPMakeSetParamRequest(hRTSP, pBuffer, pContent))
		return	COMMON_ERR_INVALID_PROTOCOL;

	return	FR_OK;
}

LRSLT FrRTSPRecordMethod(FrRTSPHandle hHandle, char* pBuffer, DWORD32 dwBuffLen)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;

	if (!hRTSP->m_dwChannelNum)
		return	COMMON_ERR_NOMEDIA;

	if (!RTSPMakeRecordRequest(hRTSP, pBuffer))
		return	COMMON_ERR_INVALID_PROTOCOL;

	return	FR_OK;
}

LRSLT FrRTSPPlayTrickPlayMethod(FrRTSPHandle hHandle, char* pBuffer, DWORD32 dwBuffLen, BOOL bResume, DWORD32 dwPos, INT32 nScale, BOOL bIntra, DWORD32 dwIntraInterval)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;

	if (!hRTSP->m_dwChannelNum)
		return	COMMON_ERR_NOMEDIA;

	if (!RTSPMakePlayTrickPlayRequest(hRTSP, pBuffer, bResume, dwPos, nScale, bIntra, dwIntraInterval))
		return	COMMON_ERR_INVALID_PROTOCOL;

	return	FR_OK;
}

// message info
LRSLT FrRTSPGetErrorCode(FrRTSPHandle hHandle)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;

    LOG_I("[RTSP]	FrRTSPGetErrorCode : dwStatus=%d", hRTSP->m_dwStatusCode);

	if (FRFAILED(hRTSP->m_lError))
		return	hRTSP->m_lError;

	if (hRTSP->m_dwStatusCode == 200)
		return	FR_OK;

	else if (hRTSP->m_dwStatusCode >= 400 && hRTSP->m_dwStatusCode < 500)
	{
		if (hRTSP->m_dwStatusCode == 400)
			return COMMON_ERR_BADREQUEST;
		else if (hRTSP->m_dwStatusCode == 401)
			return COMMON_ERR_UNAUTHORIZED;
		else if (hRTSP->m_dwStatusCode == 403)
			return COMMON_ERR_FORBIDDEN;
		else if (hRTSP->m_dwStatusCode == 404)
			return	COMMON_ERR_FOPEN;
		else if (hRTSP->m_dwStatusCode == 405)
			return COMMON_ERR_NOTALLOWED;
		else if (hRTSP->m_dwStatusCode == 406)
			return COMMON_ERR_NOTACCEPT;
		else if (hRTSP->m_dwStatusCode == 407)
			return COMMON_ERR_NEEDPROXYAUTH;
		else if (hRTSP->m_dwStatusCode == 408)
			return COMMON_ERR_TIMEOUT;
		else if (hRTSP->m_dwStatusCode == 409)
			return COMMON_ERR_CONFLICT;
		else if (hRTSP->m_dwStatusCode == 410)
			return COMMON_ERR_GONE;
		else if (hRTSP->m_dwStatusCode == 411)
			return COMMON_ERR_NEEDLENGTH;
		else if (hRTSP->m_dwStatusCode == 412)
			return COMMON_ERR_PRECONDITION;
		else if (hRTSP->m_dwStatusCode == 413)
			return COMMON_ERR_ENTITYBIG;
		else if (hRTSP->m_dwStatusCode == 414)
			return COMMON_ERR_URLLONG;
		else if (hRTSP->m_dwStatusCode == 415)
			return COMMON_ERR_MEDIATYPE;
		else if (hRTSP->m_dwStatusCode == 455)
			return	COMMON_ERR_FUNCTION;
		else if (hRTSP->m_dwStatusCode == 457)
			return	COMMON_ERR_INVALID_RANGE;
		// mwkang
		else if (hRTSP->m_dwStatusCode == 461)
			return COMMON_ERR_UNSUPPORT_TRASNPORT;
		else
		{
			LRSLT	lRet;
			char*	pBuffer;
			pBuffer = (char*)MALLOC(MAX_RTSP_MSG_SIZE);
			ITOA(hRTSP->m_dwStatusCode, pBuffer);
			// pBuffer returned value is not same parameter pBuffer.
			//pBuffer = UtilStrGetHexNumber(pBuffer, NULL, &hRTSP->m_dwStatusCode);
			UtilStrGetHexNumber(pBuffer, NULL, &hRTSP->m_dwStatusCode);
			FREE(pBuffer);
			lRet = HTTP_ERR_BASE + hRTSP->m_dwStatusCode;
			return lRet;
			//return	COMMON_ERR_CLIENT;
		}
	}
	else if (hRTSP->m_dwStatusCode >= 500)
	{
		return	COMMON_ERR_SERVER;
	}

	return	FR_FAIL;
}

void FrRTSPSetErrorCode(FrRTSPHandle hHandle, DWORD32 dwErrorCode)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	hRTSP->m_dwStatusCode = dwErrorCode;
}

METHOD_TYPE FrRTSPGetReqMethod(FrRTSPHandle hHandle)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_eMethod;
}

DWORD32 FrRTSPGetReqIndex(FrRTSPHandle hHandle)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_hReqChannel->m_dwIndex;
}

char* FrRTSPParseMessage(FrRTSPHandle hHandle, char* pBuffer, DWORD32* pdwLen)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	RTSPParsingMessage(hRTSP, pBuffer, pdwLen);
}

BOOL FrRTSPMakeResponse(FrRTSPHandle hHandle, char* pBuffer, DWORD32 dwBuffLen)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;

	if (hRTSP->m_bResponse)
		return	FALSE;

	if (hRTSP->m_dwStatusCode != 200)
	{
		RTSPMakeErrorResponse(hRTSP, pBuffer, dwBuffLen);
		LOG_E("[RTSP]	FrRTSPMakeResponse - Error response send : [%d]", hRTSP->m_dwStatusCode);
		return	TRUE;
	}

	switch (hRTSP->m_eMethod)
	{
	case ANNOUNCE_METHOD:
		RTSPMakeAnnounceResponse(hRTSP, pBuffer, dwBuffLen);
		LOG_I("[RTSP]	FrRTSPMakeResponse - Announce response send");
		break;
	case REDIRECT_METHOD:
		RTSPMakeRedirectResponse(hRTSP, pBuffer, dwBuffLen);
		LOG_I("[RTSP]	FrRTSPMakeResponse - Redirect response send");
		break;
	case DESCRIBE_METHOD:
		RTSPMakeDescribeResponse(hRTSP, pBuffer, dwBuffLen);
		LOG_I("[RTSP]	FrRTSPMakeResponse - Describe response send");
		break;
	case SETUP_METHOD:
		RTSPMakeSetupResponse(hRTSP, pBuffer, dwBuffLen);
		LOG_I("[RTSP]	FrRTSPMakeResponse - Setup response send");
		break;
	case PLAY_METHOD:
		RTSPMakePlayResponse(hRTSP, pBuffer, dwBuffLen);
		LOG_I("[RTSP]	FrRTSPMakeResponse - Play response send");
		break;
    case RECORD_METHOD:
		RTSPMakeRecordResponse(hRTSP, pBuffer, dwBuffLen);
		LOG_I("[RTSP]	FrRTSPMakeResponse - Record response send");
        break;
	case PAUSE_METHOD:
		RTSPMakePauseResponse(hRTSP, pBuffer, dwBuffLen);
		LOG_I("[RTSP]	FrRTSPMakeResponse - Pause response send");
		break;
	case TEARDOWN_METHOD:
		RTSPMakeTeardownResponse(hRTSP, pBuffer, dwBuffLen);
		LOG_I("[RTSP]	FrRTSPMakeResponse - Teardown response send");
		break;
	case OPTIONS_METHOD:
		RTSPMakeOptionsResponse(hRTSP, pBuffer, dwBuffLen);
		LOG_I("[RTSP]	FrRTSPMakeResponse - Options response send");
		break;
	case SET_PARAMETER_METHOD:
		RTSPMakeSetparameterResponse(hRTSP, pBuffer, dwBuffLen);
		LOG_I("[RTSP]	FrRTSPMakeResponse - Set Parameter response send");
		break;
	case GET_PARAMETER_METHOD:
		RTSPMakeGetparameterResponse(hRTSP, pBuffer, dwBuffLen);
		LOG_I("[RTSP]	FrRTSPMakeResponse - Get Parameter response send");
		break;
	default:
		LOG_I("[RTSP]	FrRTSPMakeResponse - Not response send");
		return	FALSE;
	}
	hRTSP->m_bResponse = TRUE;

	return	TRUE;
}

// SDP
char* FrRTSPGetSDP(FrRTSPHandle hHandle)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_pSDP;
}

DWORD32 FrRTSPGetSDPLength(FrRTSPHandle hHandle)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_dwSDPLength;
}

void FrRTSPSetSDP(FrRTSPHandle hHandle, char* pSDP)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;

	hRTSP->m_dwSDPLength = strlen(pSDP);
	hRTSP->m_pSDP = (char*)MALLOC(hRTSP->m_dwSDPLength+1);
	strcpy(hRTSP->m_pSDP, pSDP);
}

// set media samplerate to RTSP
void FrRTSPSetSDPSampleRate(FrRTSPHandle hHandle, DWORD32 dwIndex, DWORD32 dwSampleRate)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	hRTSP->m_aryChannelInfo[dwIndex]->m_dwSampleRate = dwSampleRate;
}

// Announce & Describe Info
char* FrRTSPGetContentURL(FrRTSPHandle hHandle)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_szContentURL;
}

char* FrRTSPGetContentLocation(FrRTSPHandle hHandle)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_szContentLocation;
}

char* FrRTSPGetUserAgent(FrRTSPHandle hHandle)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_szUserAgent;
}

char* FrRTSPGetUserName(FrRTSPHandle hHandle)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_szUserName;
}

BOOL FrRTSPIsPreContent(FrRTSPHandle hHandle)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_bPreContent;
}

void FrRTSPSetPreContent(FrRTSPHandle hHandle, char* pContentDate, BOOL bPreContent)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	hRTSP->m_bPreContent = bPreContent;
	if (pContentDate)
		hRTSP->m_szContentDate = UtilStrStrdup(pContentDate);
}

BOOL FrRTSPIsJoinContent(FrRTSPHandle hHandle)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_bJoinContent;
}

void FrRTSPSetJoinContent(FrRTSPHandle hHandle, BOOL bJoinContent)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	hRTSP->m_bJoinContent = bJoinContent;
}

// Setup Info
void FrRTSPSetControl(FrRTSPHandle hHandle, char* pControl)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;

	if (pControl)
		hRTSP->m_pControl = UtilStrStrdup(pControl);
}

void FrRTSPSetTrackControl(FrRTSPHandle hHandle, DWORD32 dwIndex, char* pControl)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;

	if (pControl)
		hRTSP->m_aryChannelInfo[dwIndex]->m_pControl = UtilStrStrdup(pControl);
}

void FrRTSPSetLocalAddress(FrRTSPHandle hHandle, char* pAddr)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	hRTSP->m_szLocalAddr = UtilStrStrdup(pAddr);
}

void FrRTSPSetMultiAddress(FrRTSPHandle hHandle, char* pAddr)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	hRTSP->m_szMultiAddr = UtilStrStrdup(pAddr);
}

void FrRTSPSetRTPLocalPort(FrRTSPHandle hHandle, DWORD32 dwIndex, WORD wPort)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	hRTSP->m_aryChannelInfo[dwIndex]->m_wLocalRTPPort = wPort;
}

void FrRTSPSetRTCPLocalPort(FrRTSPHandle hHandle, DWORD32 dwIndex, WORD wPort)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	hRTSP->m_aryChannelInfo[dwIndex]->m_wLocalRTCPPort = wPort;
}

void FrRTSPSetRTPMultiPort(FrRTSPHandle hHandle, DWORD32 dwIndex, WORD wPort)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	hRTSP->m_aryChannelInfo[dwIndex]->m_wMultiRTPPort = wPort;
}

void FrRTSPSetRTCPMultiPort(FrRTSPHandle hHandle, DWORD32 dwIndex, WORD wPort)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	hRTSP->m_aryChannelInfo[dwIndex]->m_wMultiRTCPPort = wPort;
}

char* FrRTSPGetMultiAddress(FrRTSPHandle hHandle)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_szMultiAddr;
}

WORD FrRTSPGetRTPRemotePort(FrRTSPHandle hHandle, DWORD32 dwIndex)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_aryChannelInfo[dwIndex]->m_wRemoteRTPPort;
}

WORD FrRTSPGetRTCPRemotePort(FrRTSPHandle hHandle, DWORD32 dwIndex)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_aryChannelInfo[dwIndex]->m_wRemoteRTCPPort;
}

WORD FrRTSPGetRTPMultiPort(FrRTSPHandle hHandle, DWORD32 dwIndex)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_aryChannelInfo[dwIndex]->m_wMultiRTPPort;
}

WORD FrRTSPGetRTCPMultiPort(FrRTSPHandle hHandle, DWORD32 dwIndex)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_aryChannelInfo[dwIndex]->m_wMultiRTCPPort;
}

char *FrRTSPGetRTSPSessionID(FrRTSPHandle hHandle)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;

	return hRTSP->m_pSession;
}

// Play info
BOOL FrRTSPIsPlayRange(FrRTSPHandle hHandle)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_bPlayRange;
}

BOOL FrRTSPIsPlayRangeNtp(FrRTSPHandle hHandle)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_bRangeNtp;
}

void FrRTSPSetPlayRange(FrRTSPHandle hHandle, DWORD32 dwRange)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	hRTSP->m_dwPlayRange = dwRange;
}

void FrRTSPSetPlayFromRange(FrRTSPHandle hHandle, DWORD32 dwRange)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	hRTSP->m_dwPlayFromRange = dwRange;
}

void FrRTSPSetPlayFromDateTime(FrRTSPHandle hHandle, DWORD32 dwDate, DWORD32 dwTime)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	hRTSP->m_dwPlayFromDate = dwDate;
	hRTSP->m_dwPlayFromTime = dwTime;
}

void FrRTSPSetPlayToDateTime(FrRTSPHandle hHandle, DWORD32 dwDate, DWORD32 dwTime)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	hRTSP->m_dwPlayToDate = dwDate;
	hRTSP->m_dwPlayToTime = dwTime;
}

DWORD32 FrRTSPGetPlayFromRange(FrRTSPHandle hHandle)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_dwPlayFromRange;
}

DWORD32 FrRTSPGetPlayToRange(FrRTSPHandle hHandle)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_dwPlayToRange;
}

DWORD32 FrRTSPGetPlayFromDate(FrRTSPHandle hHandle)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_dwPlayFromDate;
}

DWORD32 FrRTSPGetPlayFromTime(FrRTSPHandle hHandle)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_dwPlayFromTime;
}

DWORD32 FrRTSPGetPlayToDate(FrRTSPHandle hHandle)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_dwPlayToDate;
}

DWORD32 FrRTSPGetPlayToTime(FrRTSPHandle hHandle)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_dwPlayToTime;
}

int FrRTSPGetScale(FrRTSPHandle hHandle)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_nScale;
}

char* FrRTSPGetFramesMode(FrRTSPHandle hHandle)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_szFramesMode;
}

DWORD32 FrRTSPGetFramesInterval(FrRTSPHandle hHandle)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_nIntraInterval;
}

BOOL FrRTSPGetRTPInfoComplete(FrRTSPHandle hHandle, DWORD32 dwIndex)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_aryChannelInfo[dwIndex]->m_bComplete;
}

WORD FrRTSPGetRTPInfoSeq(FrRTSPHandle hHandle, DWORD32 dwIndex)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_aryChannelInfo[dwIndex]->m_wSeq;
}

void FrRTSPSetRTPInfoSeq(FrRTSPHandle hHandle, DWORD32 dwIndex, WORD wSeq)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	hRTSP->m_aryChannelInfo[dwIndex]->m_wSeq = wSeq;
}

DWORD32 FrRTSPGetRTPInfoTime(FrRTSPHandle hHandle, DWORD32 dwIndex)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_aryChannelInfo[dwIndex]->m_dwRTPTime;
}

void FrRTSPSetRTPInfoTime(FrRTSPHandle hHandle, DWORD32 dwIndex, DWORD32 dwRTPTime)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	hRTSP->m_aryChannelInfo[dwIndex]->m_dwRTPTime = dwRTPTime;
}

DWORD32 FrRTSPGetRTPInfoSSRC(FrRTSPHandle hHandle, DWORD32 dwIndex)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_aryChannelInfo[dwIndex]->m_dwRTPSSRC;
}

void FrRTSPSetRTPInfoSSRC(FrRTSPHandle hHandle, DWORD32 dwIndex, DWORD32 dwRTPSSRC)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	hRTSP->m_aryChannelInfo[dwIndex]->m_dwRTPSSRC = dwRTPSSRC;
}

// Content
BOOL FrRTSPIsContent(FrRTSPHandle hHandle)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_bContent;
}

char* FrRTSPGetContent(FrRTSPHandle hHandle)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_szContent;
}

DWORD32 FrRTSPGetContentLength(FrRTSPHandle hHandle)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_dwContentLength;
}

void FrRTSPUpdateTransport(FrRTSPHandle hHandle, BOOL bFlag)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	hRTSP->m_bUDP = bFlag;
}


// enow..
BOOL FrRTSPMakeLog(FrRTSPHandle hHandle, char* pBuffer, char* pRet, DWORD32 dwBuffLen, FILE_HANDLE hFile, BOOL bPlay)
{
	//FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	//char	szBuffer[1024]="";

	return	TRUE;
}

// nhn
void FrRTSPSetDtbResultCode(FrRTSPHandle hHandle, DWORD32 dwResultCode)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;

	if (hRTSP)
	{
		hRTSP->m_dwDtbResultCode = dwResultCode;

		LOG_I("[RTSP] FrRTSPSetDtbResultCode - dtb result code (%d)", dwResultCode);
	}
}

void FrRTSPSetDtbKeyCode(FrRTSPHandle hHandle, DWORD32 dwKeyCode)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	hRTSP->m_dwDtbKeyCode = dwKeyCode;

	LOG_I("[RTSP] FrRTSPSetDtbKeyCode - dtb key code (%d)", dwKeyCode);
}

void FrRTSPSetServername(FrRTSPHandle hHandle, char* pszServer)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	strcpy(hRTSP->m_szServerName, pszServer);

	LOG_I("[RTSP] FrRTSPSetServername - set name (%s)", pszServer);
}

BOOL FrRTSPGetCheckSttup(FrRTSPHandle hHandle, DWORD32 dwIndex)
{
    FrRTSPStruct*   hRTSP = (FrRTSPStruct*)hHandle;
    McChannelHandle	hChannel;

    hChannel = hRTSP->m_aryChannelInfo[dwIndex];

    return hChannel->m_bCheckSetupChannel;
}

void FrRTSPSetCheckSetup(FrRTSPHandle hHandle, DWORD32 dwIndex)
{
    FrRTSPStruct*   hRTSP = (FrRTSPStruct*)hHandle;
    McChannelHandle	hChannel;

    hChannel = hRTSP->m_aryChannelInfo[dwIndex];

    LOG_I("[RTSP] FrRTSPSetCheckSetup - channel index (%d) set true", dwIndex);

    hChannel->m_bCheckSetupChannel = TRUE;
}

char* FrRTSPGetUDPSourcAddr(FrRTSPHandle hHandle)
{
	FrRTSPStruct*   hRTSP = (FrRTSPStruct*)hHandle;
	return hRTSP->m_szUdpSrcAddr;
}

BOOL FrRTSPIsFindRTPInfo(FrRTSPHandle hHandle)
{
	FrRTSPStruct*   hRTSP = (FrRTSPStruct*)hHandle;
	return hRTSP->m_bFindRTPInfo;
}

BOOL FrRTSPIsFindRange(FrRTSPHandle hHandle)
{
	FrRTSPStruct*   hRTSP = (FrRTSPStruct*)hHandle;
	return hRTSP->m_bFindRange;
}

void FrRTSPSetAnnounceCSeq(FrRTSPHandle hHandle)
{
	FrRTSPStruct*   hRTSP = (FrRTSPStruct*)hHandle;
	hRTSP->m_iSendCSeq = hRTSP->m_iRecvCSeq;

	LOG_I("[RTSP] FrRTSPSetAnnounceCSeq - Set Send CSeq (%d)", hRTSP->m_iSendCSeq);
}

VOID FrRTSPUnsetResponse(FrRTSPHandle hHandle)
{
	FrRTSPStruct*   hRTSP = (FrRTSPStruct*)hHandle;
	hRTSP->m_bRecvResponse = FALSE;
}

VOID FrRTSPSetResponse(FrRTSPHandle hHandle)
{
	FrRTSPStruct*   hRTSP = (FrRTSPStruct*)hHandle;
	hRTSP->m_bRecvResponse = TRUE;
}

BOOL FrRTSPGetResponse(FrRTSPHandle hHandle)
{
	FrRTSPStruct*   hRTSP = (FrRTSPStruct*)hHandle;
	return hRTSP->m_bRecvResponse;
}

BOOL FrRTSPGetNeedToGetParameter(FrRTSPHandle hHandle)
{
	FrRTSPStruct*   hRTSP = (FrRTSPStruct*)hHandle;
	return hRTSP->m_bNeedToSendGetParameter;
}

WORD FrRTSPGetSRTServerPort(FrRTSPHandle hHandle)
{
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	return	hRTSP->m_wSrtPort;
}

BOOL FrRTSPParseRtpInfo(FrRTSPHandle hHandle) {
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;
	if (!RTSPParseRtpInfo(hRTSP, hRTSP->m_szRTPInfo))
		return FALSE;

	return TRUE;
}
/*
DWORD32 FrRTSPGetBufferTime(FrRTSPHandle hHandle) {
	FrRTSPStruct*	hRTSP = (FrRTSPStruct*)hHandle;

	return hRTSP->m_dwBufferTime;
}
*/
