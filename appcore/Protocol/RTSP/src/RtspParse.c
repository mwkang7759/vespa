
#include "Rtsp.h"

BOOL RTSPParseTransport(FrRTSPStruct* hRTSP, char* pBuffer)
{
	char*	pParameter;
	char*	pAddr = NULL;
	DWORD32	dwParameter;
	DWORD32	dwAddr, dwPort;

	while (!ISCRLF(*pBuffer))
	{
		pBuffer = UtilStrGetString(pBuffer, &pParameter, &dwParameter);

		if (pBuffer==NULL)
			break;

		if (!UtilStrStrnicmp(pParameter, "rtp", strlen("rtp")))
		{
			pBuffer = UtilStrGetString(pBuffer, &pParameter, &dwParameter);		// avp
			if (hRTSP->m_bServer)
				hRTSP->m_bUDP = TRUE;

			if (*pBuffer == ';')
				continue;

			pBuffer = UtilStrGetString(pBuffer, &pParameter, &dwParameter);		// tcp
			if (hRTSP->m_bServer)
			{
				if (!UtilStrStrnicmp(pParameter, "tcp", strlen("tcp")))
					hRTSP->m_bUDP = FALSE;
			}
		}
		else if (!UtilStrStrnicmp(pParameter, "multicast", strlen("multicast")))
		{
			if (hRTSP->m_bServer)
				hRTSP->m_bMulti = TRUE;
		}
		else if (!UtilStrStrnicmp(pParameter, "unicast", strlen("unicast")))
		{
			if (hRTSP->m_bServer)
				hRTSP->m_bMulti = FALSE;
		}
		else if (!UtilStrStrnicmp(pParameter, "source", strlen("source")))
		{
			if (!hRTSP->m_bServer)
			{
				pBuffer = UtilStrGetAddress(pBuffer, &pAddr, &dwAddr);
				if (!hRTSP->m_szUdpSrcAddr)
					hRTSP->m_szUdpSrcAddr = UtilStrStrdupn(pAddr, dwAddr);
			}
		}
		else if (!UtilStrStrnicmp(pParameter, "server_port", strlen("server_port")))
		{
			if (!hRTSP->m_bServer)
			{
				if (hRTSP->m_bSRT == TRUE) {
					pBuffer = UtilStrGetDecNumber(pBuffer, NULL, &dwPort);
					hRTSP->m_wSrtPort = (WORD)dwPort;
				} else {
					pBuffer = UtilStrGetDecNumber(pBuffer, NULL, &dwPort);
					hRTSP->m_hReqChannel->m_wRemoteRTPPort = (WORD)dwPort;
					pBuffer = UtilStrGetDecNumber(pBuffer, NULL, &dwPort);
					hRTSP->m_hReqChannel->m_wRemoteRTCPPort = (WORD)dwPort;
				}
			}
		}
		else if (!UtilStrStrnicmp(pParameter, "client_port", strlen("client_port")))
		{
			if (hRTSP->m_bServer)
			{
				pBuffer = UtilStrGetDecNumber(pBuffer, NULL, &dwPort);
				hRTSP->m_hReqChannel->m_wRemoteRTPPort = (WORD)dwPort;
				pBuffer = UtilStrGetDecNumber(pBuffer, NULL, &dwPort);
				hRTSP->m_hReqChannel->m_wRemoteRTCPPort = (WORD)dwPort;
			}
		}
		else if (!UtilStrStrnicmp(pParameter, "destination", strlen("destination")))
		{
			pBuffer = UtilStrGetAddress(pBuffer, &pAddr, &dwAddr);
			if (!hRTSP->m_szMultiAddr)
				hRTSP->m_szMultiAddr = UtilStrStrdupn(pAddr, dwAddr);
		}
		else if (!UtilStrStrnicmp(pParameter, "port", strlen("port")))
		{
			pBuffer = UtilStrGetDecNumber(pBuffer, NULL, &dwPort);
			hRTSP->m_hReqChannel->m_wMultiRTPPort = (WORD)dwPort;
			pBuffer = UtilStrGetDecNumber(pBuffer, NULL, &dwPort);
			hRTSP->m_hReqChannel->m_wMultiRTCPPort = (WORD)dwPort;
		}
		else if (!UtilStrStrnicmp(pParameter, "interleaved", strlen("interleaved")))
		{
			pBuffer = UtilStrGetDecNumber(pBuffer, NULL, &dwPort);
			hRTSP->m_hReqChannel->m_wRemoteRTPPort = (WORD)dwPort;
			pBuffer = UtilStrGetDecNumber(pBuffer, NULL, &dwPort);
			hRTSP->m_hReqChannel->m_wRemoteRTCPPort = (WORD)dwPort;
		}
		else if (!UtilStrStrnicmp(pParameter, "ssrc", strlen("ssrc")))
		{
			pBuffer = UtilStrGetHexNumber(pBuffer, NULL, &hRTSP->m_hReqChannel->m_dwRTPSSRC);
		}
       
	}

	return	TRUE;
}

BOOL RTSPParseCSeq(FrRTSPStruct* hRTSP, char* pBuffer)
{
	int		iCSeq;

	if (hRTSP->m_bResponse)
	{
		pBuffer = UtilStrGetDecNumber(pBuffer, NULL, (DWORD32*)&iCSeq);
		if (iCSeq != hRTSP->m_iSendCSeq - 1)
		{
			if (hRTSP->m_iSendCSeq != 0 || iCSeq != 255)
			{
				LOG_I("[RTSP]	RTSPParseCSeq - Invalid CSeq value (%d) Req(%d)", iCSeq, hRTSP->m_iSendCSeq-1);
				//return	FALSE;
			}
		}
	}
	else
	{
		pBuffer = UtilStrGetDecNumber(pBuffer, NULL, (DWORD32*)&hRTSP->m_iRecvCSeq);
	}

	return	TRUE;
}

BOOL RTSPParseContentBase(FrRTSPStruct* hRTSP, char* pBuffer)
{
	char*	pLocation;
	DWORD32	dwLocation;

	if (!hRTSP->m_szContentBase)
	{
		pBuffer = UtilStrGetSafeString(pBuffer, &pLocation, &dwLocation);
		hRTSP->m_szContentBase = UtilStrStrdupn(pLocation, dwLocation);
	}

	return	TRUE;
}

BOOL RTSPParseSession(FrRTSPStruct* hRTSP, char* pBuffer)
{
	char*	pSession;
	DWORD32	dwSession;

	// session identifier is just url escaped code..
	//pBuffer = UtilStrGetString(pBuffer, &pSession, &dwSession);
	
	/*pSession = pBuffer;
	dwSession = strlen(pBuffer);*/

	if (hRTSP->m_bResponse)
	{
		pSession = pBuffer;
		dwSession = strlen(pBuffer);

		if (!hRTSP->m_pSession)
			hRTSP->m_pSession = UtilStrStrdupn(pSession, dwSession);
		else
		{
			if (strncmp(hRTSP->m_pSession, pSession, dwSession))
			{
				pSession[dwSession] = '\0';
				LOG_I("[RTSP]	RTSPParseSession - Session is not matched(%s)", pSession);
			}
		}
	}
	else
	{
		// session identifier is just url escaped code..
		pBuffer = UtilStrGetString(pBuffer, &pSession, &dwSession);

		if (!hRTSP->m_pSession)
			hRTSP->m_pSession = UtilStrStrdupn(pSession, dwSession);
		else if (hRTSP->m_pSession && strncmp(hRTSP->m_pSession, pSession, dwSession))
		{
			hRTSP->m_dwStatusCode = 454;
			pSession[dwSession] = '\0';
			LOG_I("[RTSP]	RTSPParseSession - Session is not matched(%s)", pSession);
		}
	}

	return	TRUE;
}

BOOL RTSPParseRange(FrRTSPStruct* hRTSP, char* pBuffer)
{
	char*	pTemp;

	pTemp = UtilStrFindString(pBuffer, "now");
	if (pTemp)
	{
		hRTSP->m_dwPlayFromRange = 0;
		hRTSP->m_dwPlayToRange = INFINITE;
		hRTSP->m_bRangeNtp = TRUE;
	}
	else
	{
		if (UtilStrFindString(pBuffer, "clock="))
		{
			pBuffer = UtilStrGetDecNumber(pBuffer, "clock=", &hRTSP->m_dwPlayFromDate);
			if (pBuffer)
			{
				pBuffer = UtilStrGetFloatNumber(pBuffer, "T", &hRTSP->m_dwPlayFromTime);
				if (!pBuffer)
					return	FALSE;

				pBuffer = UtilStrGetDecNumber(pBuffer, "-", &hRTSP->m_dwPlayToDate);
				if (pBuffer)
					pBuffer = UtilStrGetFloatNumber(pBuffer, "T", &hRTSP->m_dwPlayToTime);
				else
				{
					hRTSP->m_dwPlayToDate = 0;
					hRTSP->m_dwPlayToTime = 0;
				}
			}
			else
			{
				hRTSP->m_dwPlayFromDate = 0;
				hRTSP->m_dwPlayFromTime = 0;
			}
			hRTSP->m_bRangeNtp = FALSE;
			LOG_I("[RTSP]	RTSPParseRange - Play Range = %dT%d-%dT%d", hRTSP->m_dwPlayFromDate,
					hRTSP->m_dwPlayFromTime, hRTSP->m_dwPlayToDate, hRTSP->m_dwPlayToTime);
		}
		else
		{
			pTemp = UtilStrFindString(pBuffer, ":");
			if (pTemp)
				pBuffer = UtilStrGetTimeNumber(pBuffer, "npt=", &hRTSP->m_dwPlayFromRange);
			else
				pBuffer = UtilStrGetFloatNumber(pBuffer, "npt=", &hRTSP->m_dwPlayFromRange);

			if (pBuffer)
			{
				if (pTemp)
					pBuffer = UtilStrGetTimeNumber(pBuffer, "-", &hRTSP->m_dwPlayToRange);
				else
					pBuffer = UtilStrGetFloatNumber(pBuffer, "-", &hRTSP->m_dwPlayToRange);

				if (!pBuffer)
					hRTSP->m_dwPlayToRange = 0;
			}
			else
				hRTSP->m_dwPlayFromRange = 0;
			hRTSP->m_bRangeNtp = TRUE;
			LOG_I("[RTSP]	RTSPParseRange - Play Range = %d-%d", hRTSP->m_dwPlayFromRange, hRTSP->m_dwPlayToRange);
		}
	}
	hRTSP->m_bPlayRange = TRUE;

	return	TRUE;
}

BOOL RTSPParseRtpInfo(FrRTSPStruct* hRTSP, char* pBuffer)
{
	char*	pRtpInfo;
	char*	pParameter;
	char*	pURL = NULL;
	DWORD32	dwParameter, dwURL;
	DWORD32	dwRTPTime = (DWORD32)0, dwSeq = (DWORD32)0, dwSSRC = 0;
	McChannelHandle	hChannel;
	BOOL	bComplete = FALSE, bSeq = FALSE, bRtpTime = FALSE;

	pRtpInfo = pBuffer;

	while (!ISCRLF(*pRtpInfo))
	{
		pRtpInfo = UtilStrGetString(pRtpInfo, &pParameter, &dwParameter);
		if (!UtilStrStrnicmp(pParameter, "url", strlen("url")))
		{
			pRtpInfo++;		// '='
			pRtpInfo = UtilStrGetSafeString(pRtpInfo, &pURL, &dwURL);
		}
		else if (!UtilStrStrnicmp(pParameter, "seq", strlen("seq")))
		{
			pRtpInfo = UtilStrGetDecNumber(pRtpInfo, NULL, &dwSeq);
			if (pRtpInfo)
				bSeq = TRUE;
		}
		else if (!UtilStrStrnicmp(pParameter, "rtptime", strlen("rtptime")))
		{
			pRtpInfo = UtilStrGetDecNumber(pRtpInfo, NULL, &dwRTPTime);
			if (pRtpInfo)
				bRtpTime = TRUE;
		}
		else if (!UtilStrStrnicmp(pParameter, "ssrc", strlen("ssrc")))
		{
			pRtpInfo = UtilStrGetHexNumber(pRtpInfo, NULL, &dwSSRC);
		}
		else
		{
			pRtpInfo = UtilStrFindString(pRtpInfo, ";");
			if (!pRtpInfo)
				break;
		}

		while (ISSPACE(*pRtpInfo))
			pRtpInfo++;

		if (ISCOMMA(*pRtpInfo) || ISCRLF(*pRtpInfo))
		{
			LOG_I("[RTSP]	RTSPParseRtpInfo - RTP-Info seq=%d rtptime=%u exists %d", dwSeq, dwRTPTime, bRtpTime);
			hChannel = RTSPChannelFindByURL(hRTSP, pURL, dwURL);
			if (hChannel)
			{
				if (bRtpTime == FALSE)
					LOG_I("[RTSP]	RTSPParseRtpInfo - Incomplete Seq %d RTPTime %d", bSeq, bRtpTime);
				bComplete = bSeq && bRtpTime;
				hChannel->m_bComplete = bComplete;
				hChannel->m_wSeq = bSeq ? (WORD)dwSeq : (WORD)0;
				hChannel->m_dwRTPTime = bRtpTime ? dwRTPTime : (DWORD32)0;
				if (!hChannel->m_dwRTPSSRC)
					hChannel->m_dwRTPSSRC = dwSSRC;
				else
				{
					if (dwSSRC && hChannel->m_dwRTPSSRC != dwSSRC)
						LOG_I("[RTSP]	RTSPParseRtpInfo - not matched SSRC=%x %x", hChannel->m_dwRTPSSRC, dwSSRC);
				}
			}
			else
			{
				LOG_I("[RTSP]	RTSPParseRtpInfo - url(%s) is not found", pURL);
				return	FALSE;
			}
		}
	}

	return	TRUE;
}

BOOL RTSPParseURL(FrRTSPStruct* hRTSP, char* pBuffer)
{
	char*	pPort;
	DWORD32	dwLen;

	// Add - hjchoi 2007.03.27
	// UtilStrGetSafeString() ���� File Name�� ����ϱ� ������, URL �ڿ� ������ RTSP/1.0�� URL�� ���� ���Խ��� ������.
	// �̸� �����ϱ� ���� RTSP/1.0 �κ��� ���ڿ����� �����Ѵ�.
	char*	pTemp;

	pTemp = strstr(pBuffer, "RTSP/");

	if (pTemp)
		pTemp--;

	while (ISENDLINE(*pTemp) || ISSPACE(*pTemp))
	{
		pBuffer[pTemp - pBuffer] = '\0';
		pTemp--;
	}

	dwLen = strlen(pBuffer);

	FREE(hRTSP->m_szFilename);
	FREE(hRTSP->m_szContentURL);
	FREE(hRTSP->m_szContentLocation);
	hRTSP->m_szContentURL		= UtilStrStrdupn(pBuffer, dwLen);
	hRTSP->m_szFilename		    = UtilURLGetFilename(hRTSP->m_szContentURL);
	hRTSP->m_szContentLocation	= UtilURLGetContentLocation(hRTSP->m_szContentURL);

	if (hRTSP->m_bServer)
	{
		pPort = UtilStrFindString(hRTSP->m_szContentURL+7, ":");
		if (pPort)
		{
			hRTSP->m_szContentBase	= (char*)MALLOC(dwLen);
			dwLen = pPort - hRTSP->m_szContentURL;
			strncpy(hRTSP->m_szContentBase,	hRTSP->m_szContentURL, dwLen);
			hRTSP->m_szContentBase[dwLen] = '\0';

			if(hRTSP->m_szContentLocation)
				strcat(hRTSP->m_szContentBase, hRTSP->m_szContentLocation);
		}
		else
			hRTSP->m_szContentBase	= UtilStrStrdup(hRTSP->m_szContentURL);
	}

	return	TRUE;
}

BOOL RTSPParseTrack(FrRTSPStruct* hRTSP, char* pBuffer)
{
	char*	pURL;
	DWORD32	dwURL;

	pBuffer = UtilStrGetSafeString(pBuffer, &pURL, &dwURL);
	hRTSP->m_hReqChannel = RTSPChannelFindByURL(hRTSP, pURL, dwURL);
	if (!hRTSP->m_hReqChannel)
		return	FALSE;

	return	TRUE;
}

BOOL RTSPParseUserAgent(FrRTSPStruct* hRTSP, char* pBuffer, BOOL bMgosp)
{
	char*	pString;
	DWORD32	dwString;

	pBuffer = UtilStrGetContext(pBuffer, &pString, &dwString);
	if (bMgosp)
	{
		if (pString && !hRTSP->m_szMasterKey)
			hRTSP->m_szMasterKey = UtilStrStrdupn(pString, dwString);
	}
	else
	{
		if (pString && !hRTSP->m_szUserAgent)
			hRTSP->m_szUserAgent = UtilStrStrdupn(pString, dwString);
	}

	return	TRUE;
}

BOOL RTSPParseAuthorization(FrRTSPStruct* hRTSP, char* pBuffer)
{
	char*	pUserName;
	DWORD32	dwName;

	pBuffer = UtilStrFindString(pBuffer, "username=");
	if (!pBuffer)
		return	FALSE;

	pBuffer += strlen("username=");
	pBuffer = UtilStrGetString(pBuffer, &pUserName, &dwName);
	if (pUserName && !hRTSP->m_szUserName)
		hRTSP->m_szUserName = UtilStrStrdupn(pUserName, dwName);

	return	TRUE;
}

BOOL RTSPParseFrames(FrRTSPStruct* hRTSP, char* pBuffer)
{
    pBuffer = UtilStrFindString(pBuffer, "intra/");
    if (!pBuffer)
        return	FALSE;

    memcpy(hRTSP->m_szFramesMode, pBuffer, 5);
    pBuffer += strlen(hRTSP->m_szFramesMode)+strlen("/");
    UtilStrGetDecNumber(pBuffer, NULL, &hRTSP->m_nIntraInterval);

    return TRUE;
}

BOOL RTSPParseMGOSupport(FrRTSPStruct* hRTSP, char* pBuffer)
{
	char*	pKey;
	DWORD32	dwKey, dwSaltKey, dwSaltKey2;

	pBuffer = UtilStrGetString(pBuffer, &pKey, &dwKey);
	if (pKey && !UtilStrStrnicmp(pKey, "TRUE", strlen("TRUE")) && !hRTSP->m_szSaltingKey)
	{
		hRTSP->m_bMgosp = TRUE;
		hRTSP->m_szSaltingKey = (char*)MALLOC(MAX_KEY_LEN);
		dwSaltKey = RAND();
		if (dwSaltKey < 0x10000000)
			dwSaltKey = MAX_DWORD - dwSaltKey;
		dwSaltKey2 = RAND();
		if (dwSaltKey2 < 0x10000000)
			dwSaltKey2 = MAX_DWORD - dwSaltKey2;
		sprintf(hRTSP->m_szSaltingKey, "%u%u", dwSaltKey, dwSaltKey2);
	}

	return	TRUE;
}

BOOL RTSPParseMGOAuth(FrRTSPStruct* hRTSP, char* pBuffer)
{
	char*	pKey;
	DWORD32	dwKey;

	pBuffer = UtilStrGetString(pBuffer, &pKey, &dwKey);
	if (pKey && !hRTSP->m_szSaltingKey)
		hRTSP->m_szSaltingKey = UtilStrStrdupn(pKey, dwKey);

	return	TRUE;
}

BOOL RTSPParseBooleanValue(FrRTSPStruct* hRTSP, char* pBuffer, BOOL* pbValue)
{
	char*	pValue;
	DWORD32	dwValue;

	pBuffer = UtilStrGetString(pBuffer, &pValue, &dwValue);
	if (pValue && !UtilStrStrnicmp(pValue, "TRUE", strlen("TRUE")))
		*pbValue = TRUE;
	else
		*pbValue = FALSE;

	return	TRUE;
}

BOOL RTSPParsePublic(FrRTSPStruct* hRTSP, char* pBuffer)
{
	//char*	pParameter;
	//DWORD32	dwParameter;

	if (UtilStrIFindString(pBuffer, "get_parameter"))
	{
		hRTSP->m_bNeedToSendGetParameter = TRUE;
	}

	return	TRUE;
}

BOOL RTSPParseWWWAuthenticate(FrRTSPStruct* hRTSP, char* pBuffer)
{
	char*	pParameter;
	//char*	pAddr = NULL;
	DWORD32	dwParameter;
	//DWORD32	dwAddr, dwPort;

	while (!ISCRLF(*pBuffer))
	{
		pBuffer = UtilStrGetString(pBuffer, &pParameter, &dwParameter);
		if (pBuffer==NULL)
			break;

		if (!UtilStrStrnicmp(pParameter, "Basic", strlen("Basic")))
		{
		}
		else if (!UtilStrStrnicmp(pParameter, "Digest", strlen("Digest")))
		{
		}
		else if (!UtilStrStrnicmp(pParameter, "realm", strlen("realm")))
		{
			pBuffer = UtilStrGetStringEx(pBuffer, &pParameter, &dwParameter);		// avp
			if (!pBuffer)
				break;

			hRTSP->m_szDigestRealm = UtilStrStrdupn(pParameter, dwParameter);
		}
		else if (!UtilStrStrnicmp(pParameter, "nonce", strlen("nonce")))
		{
			pBuffer = UtilStrGetString(pBuffer, &pParameter, &dwParameter);		// avp
			if (!pBuffer)
				break;

			hRTSP->m_szDigestNonce = UtilStrStrdupn(pParameter, dwParameter);
		}
		else
		{
			pBuffer = UtilStrFindString(pBuffer, ",");
			if (!pBuffer)
				break;
		}
		//else
		//	break;
	}

	return	TRUE;
}

char* RTSPParsingMessage(FrRTSPStruct* hRTSP, char* pBuffer, DWORD32* dwLen)
{
	char	*pMessage, *pContent, *pLine, *pHeaderName, *pParameter;
	char	cTemp;
	DWORD32	dwMessage, dwLineLen, dwHeaderLen, dwContentLength = 0;
	BOOL	bStartLine = TRUE;
	BOOL	bSDP = FALSE;

    // reset
    hRTSP->m_nScale = 1;
    strcpy(hRTSP->m_szFramesMode, "");
    hRTSP->m_nIntraInterval = 0;

	while (ISCRLF(*pBuffer))
	{
		pBuffer += 2;
		*dwLen -= 2;
	}

	// Check the blank-line
	pContent = strstr(pBuffer, "\r\n\r\n");
	if (!pContent)
		return	pBuffer;
	pContent += strlen("\r\n\r\n");

	pMessage = pBuffer;
	dwMessage = *dwLen;

	pLine = pBuffer;
	while (*dwLen)
	{
		// 1. Find 1 Line
		pBuffer = UtilStrGetLine(pLine, &dwLineLen);

		if (ISCRLF(*pLine))
		{
			*dwLen -= 2;
			break;					// blank-line
		}

		*dwLen -= dwLineLen;

		// 2. Parse 1 line
		pParameter = UtilStrGetString(pLine, &pHeaderName, &dwHeaderLen);

		cTemp = pLine[dwLineLen];
		pLine[dwLineLen] = 0;

		// start-line
		if (bStartLine)
		{
			if (!strncmp(pHeaderName, "RTSP", strlen("RTSP")))			// response
			{
				pParameter += 4;				// "/1.0"
				pParameter = UtilStrGetDecNumber(pParameter, NULL, &hRTSP->m_dwStatusCode);

				hRTSP->m_bResponse = TRUE;
				hRTSP->m_eMethod = NOT_METHOD;
				//LOG_I("[RTSP]	RTSPParsingMessage - Response recv");
			}
			else if (!strncmp(pHeaderName, "HTTP", strlen("HTTP")))		// response
			{
				pParameter += 4;				// "/1.0"
				pParameter = UtilStrGetDecNumber(pParameter, NULL, &hRTSP->m_dwStatusCode);

				hRTSP->m_bResponse = TRUE;
				hRTSP->m_eMethod = NOT_METHOD;
				LOG_I("[RTSP]	RTSPParsingMessage - HTTP Response recv");
			}
			else														// request
			{
				if (!strncmp(pHeaderName, "ANNOUNCE", strlen("ANNOUNCE")))
				{
					hRTSP->m_eMethod = ANNOUNCE_METHOD;
					pParameter = UtilStrLeftOneTrim(pParameter);	// delete white space url name.
					if (!RTSPParseURL(hRTSP, pParameter))
						return	NULL;
					LOG_I("[RTSP]	RTSPParsingMessage - Announce recv");
				}
				else if (!strncmp(pHeaderName, "REDIRECT", strlen("REDIRECT")))
				{
					hRTSP->m_eMethod = REDIRECT_METHOD;
					LOG_I("[RTSP]	RTSPParsingMessage - Redirect recv");
				}
				else if (!strncmp(pHeaderName, "DESCRIBE", strlen("DESCRIBE")))
				{
					hRTSP->m_eMethod = DESCRIBE_METHOD;
					if (!RTSPParseURL(hRTSP, pParameter))
						return	NULL;
					LOG_I("[RTSP]	RTSPParsingMessage - Describe recv");
				}
				else if (!strncmp(pHeaderName, "SETUP", strlen("SETUP")))
				{
					hRTSP->m_eMethod = SETUP_METHOD;
					if (!RTSPParseTrack(hRTSP, pParameter))
						return	NULL;
					LOG_I("[RTSP]	RTSPParsingMessage - Setup recv");
				}
				else if (!strncmp(pHeaderName, "PLAY", strlen("PLAY")))
				{
					hRTSP->m_eMethod = PLAY_METHOD;
					hRTSP->m_bPlayRange = FALSE;
					hRTSP->m_nScale = 0;
					LOG_I("[RTSP]	RTSPParsingMessage - Play recv");
				}
				else if (!strncmp(pHeaderName, "RECORD", strlen("RECORD")))
				{
					hRTSP->m_eMethod = RECORD_METHOD;
					hRTSP->m_bPlayRange = FALSE;
					hRTSP->m_nScale = 0;
					LOG_I("[RTSP]	RTSPParsingMessage - Record recv");
				}
				else if (!strncmp(pHeaderName, "PAUSE", strlen("PAUSE")))
				{
					hRTSP->m_eMethod = PAUSE_METHOD;
					LOG_I("[RTSP]	RTSPParsingMessage - Pause recv");
				}
				else if (!strncmp(pHeaderName, "TEARDOWN", strlen("TEARDOWN")))
				{
					hRTSP->m_eMethod = TEARDOWN_METHOD;
					LOG_I("[RTSP]	RTSPParsingMessage - Teardown recv");
				}
				else if (!strncmp(pHeaderName, "OPTIONS", strlen("OPTIONS")))
				{
					hRTSP->m_eMethod = OPTIONS_METHOD;
					LOG_I("[RTSP]	RTSPParsingMessage - Options recv");
				}
				else if (!strncmp(pHeaderName, "SET_PARAMETER", strlen("SET_PARAMETER")))
				{
					hRTSP->m_eMethod = SET_PARAMETER_METHOD;
					LOG_I("[RTSP]	RTSPParsingMessage - Set Patameter recv");
				}
				else if (!strncmp(pHeaderName, "GET_PARAMETER", strlen("GET_PARAMETER")))
				{
					hRTSP->m_eMethod = GET_PARAMETER_METHOD;
					LOG_I("[RTSP]	RTSPParsingMessage - Get Patameter recv");
				}
				else
				{
					hRTSP->m_eMethod = NOT_METHOD;
					LOG_I("[RTSP]	RTSPParsingMessage - NotMethod recv");
				}
				hRTSP->m_dwStatusCode = 200;
				hRTSP->m_bResponse = FALSE;
			}
			bStartLine = FALSE;
		}
		else
		{
			pParameter = UtilStrFindString(pParameter, ":") + 1;			// find a colon

			if (!UtilStrStrnicmp(pHeaderName, "Transport", strlen("Transport")) &&
				dwHeaderLen == strlen("Transport"))
			{
				if (!RTSPParseTransport(hRTSP, pParameter))
					return	NULL;
			}
			else if (!UtilStrStrnicmp(pHeaderName, "Session", strlen("Session")) &&
				dwHeaderLen == strlen("Session"))
			{
				// add 
				/*char szAuthorization[DEF_LENGTH_2048] = { 0, };				
				UtilURLDecoding(szAuthorization, pParameter);
				if (!RTSPParseSession(hRTSP, szAuthorization))
					return	NULL;*/

				// src
				if (!RTSPParseSession(hRTSP, pParameter))
					return	NULL;

				// src
				/*if (!RTSPParseSession(hRTSP, pParameter))
					return	NULL;*/
			}
			else if (!UtilStrStrnicmp(pHeaderName, "CSeq", strlen("CSeq")) &&
				dwHeaderLen == strlen("CSeq"))
			{
				if (!RTSPParseCSeq(hRTSP, pParameter))
					return	NULL;
			}
			else if (!UtilStrStrnicmp(pHeaderName, "Range", strlen("Range")) &&
				dwHeaderLen == strlen("Range"))
			{
				hRTSP->m_bFindRange = TRUE;
				if (!RTSPParseRange(hRTSP, pParameter))
					return	NULL;
			}
			else if (!UtilStrStrnicmp(pHeaderName, "RTP-Info", strlen("RTP-Info")) &&
				dwHeaderLen == strlen("RTP-Info"))
			{
				if (hRTSP->m_bSRT == TRUE) {
					//BYTE *pLine = NULL;
					DWORD32 dwInfoLen=0;

					UtilStrGetLine(pParameter, &dwInfoLen);
					if (dwInfoLen > 0) {
						strncpy(hRTSP->m_szRTPInfo, pParameter, dwInfoLen);
					}
				} else {
					hRTSP->m_bFindRTPInfo = TRUE;
					if (!RTSPParseRtpInfo(hRTSP, pParameter))
						return	NULL;
				}
			}
			else if (!UtilStrStrnicmp(pHeaderName, "RTCP-Interval", strlen("RTCP-Interval")) &&
				dwHeaderLen == strlen("RTCP-Interval"))
			{
			}
			else if (!UtilStrStrnicmp(pHeaderName, "Content-Location", strlen("Content-Location")) &&
				dwHeaderLen == strlen("Content-Location"))
			{
				if (!RTSPParseContentBase(hRTSP, pParameter))
					return	NULL;
			}
			else if (!UtilStrStrnicmp(pHeaderName, "Content-Base", strlen("Content-Base")) &&
				dwHeaderLen == strlen("Content-Base"))
			{
				if (!RTSPParseContentBase(hRTSP, pParameter))
					return	NULL;
			}
			else if (!UtilStrStrnicmp(pHeaderName, "Content-Type", strlen("Content-Type")) &&
				dwHeaderLen == strlen("Content-Type"))
			{
				char*	pType;
				DWORD32	dwType;

				pParameter = UtilStrGetString(pParameter, &pType, &dwType);
				if (!pParameter)
					return	NULL;

				pParameter = UtilStrGetString(pParameter, &pType, &dwType);
				if (!pParameter)
					return	NULL;

				if (!strncmp(pType, "sdp", strlen("sdp")))
					bSDP = TRUE;
			}
			else if (!UtilStrStrnicmp(pHeaderName, "Content-Length", strlen("Content-Length")) &&
				dwHeaderLen == strlen("Content-Length"))
			{
				pParameter = UtilStrGetDecNumber(pParameter, NULL, &dwContentLength);
				// Check the content
				if (dwContentLength)
				{
					if (dwContentLength > dwMessage - (pContent - pMessage))
					{
						LOG_I("[RTSP]	RTSPParsingMessage - Content Body is not received!!");
						pLine[dwLineLen] = cTemp;

						*dwLen = dwMessage;
						return	pMessage;
					}
				}
			}
			else if (!UtilStrStrnicmp(pHeaderName, "User-Agent", strlen("User-Agent")) &&
				dwHeaderLen == strlen("User-Agent"))
			{
				RTSPParseUserAgent(hRTSP, pParameter, FALSE);
			}
			else if (!UtilStrStrnicmp(pHeaderName, "Authorization", strlen("Authorization")) &&
				dwHeaderLen == strlen("Authorization"))
			{
				RTSPParseAuthorization(hRTSP, pParameter);
			}
			else if (!UtilStrStrnicmp(pHeaderName, "Scale", strlen("Scale")) &&
				dwHeaderLen == strlen("Scale"))
			{
				UtilStrGetIntNumber(pParameter, NULL, &hRTSP->m_nScale);
			}
			else if (!UtilStrStrnicmp(pHeaderName, "Frames", strlen("Frames")) &&
				dwHeaderLen == strlen("Frames"))
			{
				RTSPParseFrames(hRTSP, pParameter);
			}
			else if (!UtilStrStrnicmp(pHeaderName, "X-MGO-Support", strlen("X-MGO-Support")) &&
				dwHeaderLen == strlen("X-MGO-Support"))
			{
				RTSPParseMGOSupport(hRTSP, pParameter);
			}
			else if (!UtilStrStrnicmp(pHeaderName, "X-MGO-Auth", strlen("X-MGO-Auth")) &&
				dwHeaderLen == strlen("X-MGO-Auth"))
			{
				RTSPParseMGOAuth(hRTSP, pParameter);
			}
			else if (!UtilStrStrnicmp(pHeaderName, "X-MGO-User-Agent", strlen("X-MGO-User-Agent")) &&
				dwHeaderLen == strlen("X-MGO-User-Agent"))
			{
				RTSPParseUserAgent(hRTSP, pParameter, TRUE);
			}
			else if (!UtilStrStrnicmp(pHeaderName, "X-PRE-CONTENT", strlen("X-PRE-CONTENT")) &&
				dwHeaderLen == strlen("X-PRE-CONTENT"))
			{
				RTSPParseBooleanValue(hRTSP, pParameter, &hRTSP->m_bPreContent);
			}
			else if (!UtilStrStrnicmp(pHeaderName, "X-UPSTREAMING-JOIN", strlen("X-UPSTREAMING-JOIN")) &&
				dwHeaderLen == strlen("X-UPSTREAMING-JOIN"))
			{
				RTSPParseBooleanValue(hRTSP, pParameter, &hRTSP->m_bJoinContent);
			}
			else if (!UtilStrStrnicmp(pHeaderName, "WWW-Authenticate", strlen("WWW-Authenticate")) &&
				dwHeaderLen == strlen("WWW-Authenticate"))
			{
				RTSPParseWWWAuthenticate(hRTSP, pParameter);
			}
			else if (!UtilStrStrnicmp(pHeaderName, "Public", strlen("Public")) &&
				dwHeaderLen == strlen("Public"))
			{
				RTSPParsePublic(hRTSP, pParameter);
			}
			//else if (!UtilStrStrnicmp(pHeaderName, "X-BufferTime", strlen("X-BufferTime")) && dwHeaderLen == strlen("X-BufferTime"))
			//{
			//	pParameter = UtilStrGetDecNumber(pParameter, NULL, &hRTSP->m_dwBufferTime);
			//	if (!pParameter)
			//		return	NULL;
			//}
			//else if (!UtilStrStrnicmp(pHeaderName, "X-Playlist-Gen-Id", strlen("X-Playlist-Gen-Id")))
			//{
			//	pParameter = UtilStrGetDecNumber(pParameter, NULL, &hRTSP->m_dwPlaylistGenId);
			//	if (!pParameter)
			//		return	NULL;
			//}
			else
			{
				// Accept
				// Accept-Encoding
				// Accept-Language
				// Allow
				// Blocksize
				// Last-Modified:
				// Date:
				// Expires:
				// Content-Language:
				// Supported:
				// Server:
				// Cache-Control:
			}
		}
		pLine[dwLineLen] = cTemp;

		pLine = pBuffer;
	}

	// SDP
	if (dwContentLength)
	{
		pBuffer += dwContentLength;
		*dwLen -= dwContentLength;
		hRTSP->m_bContent = TRUE;

		if (*dwLen)
		{
			LOG_E("[RTSP]	RTSPParsingMessage - ContentLength error!!");
		}

		if (bSDP)
		{
			if (hRTSP->m_bMgosp)
			{
			}
			else
			{
				hRTSP->m_pSDP = UtilStrStrdupn(pContent, dwContentLength);
				hRTSP->m_dwSDPLength = dwContentLength;
			}

			bSDP = FALSE;
		}
		else
		{
			FREE(hRTSP->m_szContent);
			hRTSP->m_szContent = UtilStrStrdupn(pContent, dwContentLength);
			hRTSP->m_dwContentLength = dwContentLength;
		}
	}
	else
		hRTSP->m_bContent = FALSE;

	return	pBuffer;
}
