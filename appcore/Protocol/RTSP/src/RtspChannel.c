
#include "Rtsp.h"

McChannelHandle RTSPChannelFindByURL(FrRTSPStruct* hRTSP, char* pURL, DWORD32 dwURL)
{
	McChannelHandle	hChannel = NULL;
	DWORD32			dwIndex;
	BYTE			cTemp;

	cTemp = pURL[dwURL];
	pURL[dwURL] = 0;

	for (dwIndex = 0;dwIndex < hRTSP->m_dwChannelNum;dwIndex++)
	{
		hChannel = hRTSP->m_aryChannelInfo[dwIndex];
		if (hChannel->m_pControl)
		{
			if (strstr(pURL, hChannel->m_pControl))
			{
				// android 3.0
				strcpy(hChannel->m_szControlUrl, pURL);
				break;
			}
		}
	}

	pURL[dwURL] = cTemp;

	if (dwIndex == hRTSP->m_dwChannelNum)
		hChannel = NULL;

	return	hChannel;
}
