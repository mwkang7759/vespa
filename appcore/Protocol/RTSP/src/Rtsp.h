
#ifndef	_RTSP_H_
#define	_RTSP_H_

#include "SocketAPI.h"
#include "SystemAPI.h"
#include "TraceAPI.h"
#include "mediaerror.h"
#include "UtilAPI.h"
#include "SdpAPI.h"
#include "RtpAPI.h"
#include "RtspAPI.h"

//#ifndef SUPPORT_IOS
//#include "aesctr.h"
//#endif


#define		RTSP_VERSION				"RTSP/1.0"
#define		HTTP_VERSION				"HTTP/1.0"
#define		MGOSP_VERSION				"MGO v1.0"
#define		MAX_SESSION_LEN				20
#define		MAX_KEY_LEN					50

typedef	struct {
	DWORD32			m_dwIndex;

	// Transport
	WORD			m_wLocalRTPPort;
	WORD			m_wLocalRTCPPort;
	WORD			m_wRemoteRTPPort;
	WORD			m_wRemoteRTCPPort;
	WORD			m_wMultiRTPPort;
	WORD			m_wMultiRTCPPort;

	// RTP-Info
	char*			m_pControl;
	BOOL			m_bComplete;
	WORD			m_wSeq;
	DWORD32			m_dwRTPTime;
	DWORD32			m_dwRTPSSRC;
	DWORD32			m_dwSampleRate;			// mwkang
	char			m_szControlUrl[1024];	// android 3.0

    BOOL            m_bCheckSetupChannel;
} McChannelStruct, *McChannelHandle;

typedef	struct {
	// configuration
	BOOL			m_bServer;
	BOOL			m_bUDP;
	BOOL			m_bHTTP;
	BOOL			m_bMulti;
	BOOL			m_bTS;
	LRSLT			m_lError;
	BOOL			m_b3GP;
	BOOL			m_bPublishing;
	BOOL			m_bSRT;
	BOOL			m_bRelay;
    BOOL            m_bUDPfec;

	// URL
	char*			m_szContentURL;
	char*			m_szContentLocation;
	char*			m_szFilename;
	DWORD32			m_dwStartDate;
	DWORD32			m_dwStartTime;
	DWORD32			m_dwEndDate;
	DWORD32			m_dwEndTime;

	// channel info
	McChannelHandle*	m_aryChannelInfo;
	McChannelHandle	m_hReqChannel;
	DWORD32			m_dwChannelNum;

	// Field value
	METHOD_TYPE		m_eMethod;							// Request Method
	char*			m_pSession;
	char*			m_pControl;
	char*			m_szContentBase;					// Response ContentBase
	char*			m_szUserAgent;
	char*			m_szUserName;
	char*			m_szLocalAddr;
	char*			m_szMultiAddr;
	DWORD32			m_dwPlayRange;
	DWORD32			m_dwPlayFromRange;					// Play ntp range
	DWORD32			m_dwPlayToRange;					// Play ntp range
	DWORD32			m_dwPlayFromDate;					// Play clock range
	DWORD32			m_dwPlayFromTime;					// Play clock range
	DWORD32			m_dwPlayToDate;						// Play clock range
	DWORD32			m_dwPlayToTime;						// Play clock range
	DWORD32			m_dwEndOfStream;
	BOOL			m_bPlayRange;
	BOOL			m_bRangeNtp;
	int				m_nScale;
	char			m_szFramesMode[16];
	DWORD32			m_nIntraInterval;
	DWORD32			m_dwStatusCode;						// Response Code
	int				m_iRecvCSeq;						// CSeq Header (recv)
	int				m_iSendCSeq;
    BOOL			m_bRecvResponse;
	BOOL			m_bResponse;
	char			m_szServerName[MAX_STRING_LENGTH];
	char*			m_szUdpSrcAddr;
	char*			m_szDigestRealm;
	char*			m_szDigestNonce;
	char*			m_szUserPasswd;

	// SDP
	char*			m_pSDP;
	char*			m_pBase64SDP;
	DWORD32			m_dwSDPLength;
	DWORD32			m_dwBase64SDPLength;

	// Content
	BOOL			m_bContent;
	char*			m_szContent;
	DWORD32			m_dwContentLength;

	// MGOSP
	BOOL			m_bMgosp;
	void*			m_pAesCtr;
	char*			m_szMasterKey;
	char*			m_szSaltingKey;

	// UPS
	BOOL			m_bMMS;
	BOOL			m_bPreContent;
	BOOL			m_bJoinContent;
	char*			m_szContentDate;

	// nhn
	DWORD32			m_dwDtbResultCode;
    DWORD32         m_dwDtbKeyCode;

	// s1
	BOOL			m_bFindRTPInfo;
	BOOL			m_bFindRange;

	// alive
	BOOL			m_bNeedToSendGetParameter;

	// srt
	WORD			m_wSrtPort;
	char			m_szRTPInfo[DEF_LENGTH_1024];

	// initial buffer time for fast streaming in vod
	DWORD32			m_dwBufferTime;

} FrRTSPStruct;

// request
BOOL RTSPMakeHTTPGetRequest(FrRTSPStruct* hRTSP, char* pBuffer);
BOOL RTSPMakeHTTPPostRequest(FrRTSPStruct* hRTSP, char* pBuffer, char* pContent);
BOOL RTSPMakeOptionsRequest(FrRTSPStruct* hRTSP, char* pBuffer);
BOOL RTSPMakeAnnounceRequest(FrRTSPStruct* hRTSP, char* pBuffer);
BOOL RTSPMakeDescribeRequest(FrRTSPStruct* hRTSP, char* pBuffer);
BOOL RTSPMakeSetupRequest(FrRTSPStruct* hRTSP, char* pBuffer, DWORD32 dwIndex);
BOOL RTSPMakePlayRequest(FrRTSPStruct* hRTSP, char* pBuffer, BOOL bResume, DWORD32 dwPos);
BOOL RTSPMakeRecordRequest(FrRTSPStruct* hRTSP, char* pBuffer);
BOOL RTSPMakePlayClockRequest(FrRTSPStruct* hRTSP, char* pBuffer, DWORD32 dwDate, DWORD32 dwTime);
BOOL RTSPMakePlayScaleRequest(FrRTSPStruct* hRTSP, char* pBuffer, int nScale);
BOOL RTSPMakePauseRequest(FrRTSPStruct* hRTSP, char* pBuffer);
BOOL RTSPMakeTeardownRequest(FrRTSPStruct* hRTSP, char* pBuffer);
BOOL RTSPMakeGetParamRequest(FrRTSPStruct* hRTSP, char* pBuffer, char* pContent);
BOOL RTSPMakeSetParamRequest(FrRTSPStruct* hRTSP, char* pBuffer, char* pContent);
BOOL RTSPMakePlayTrickPlayRequest(FrRTSPStruct* hRTSP, char* pBuffer, BOOL bResume, DWORD32 dwPos, INT32 nScale, BOOL bIntra, DWORD32 dwIntraInterval);

// response
void RTSPMakeAnnounceResponse(FrRTSPStruct* hRTSP, char* pBuffer, DWORD32 dwBuffLen);
void RTSPMakeRedirectResponse(FrRTSPStruct* hRTSP, char* pBuffer, DWORD32 dwBuffLen);
void RTSPMakeDescribeResponse(FrRTSPStruct* hRTSP, char* pBuffer, DWORD32 dwBuffLen);
void RTSPMakeSetupResponse(FrRTSPStruct* hRTSP, char* pBuffer, DWORD32 dwBuffLen);
void RTSPMakePlayResponse(FrRTSPStruct* hRTSP, char* pBuffer, DWORD32 dwBuffLen);
void RTSPMakeRecordResponse(FrRTSPStruct* hRTSP, char* pBuffer, DWORD32 dwBuffLen);
void RTSPMakePauseResponse(FrRTSPStruct* hRTSP, char* pBuffer, DWORD32 dwBuffLen);
void RTSPMakeTeardownResponse(FrRTSPStruct* hRTSP, char* pBuffer, DWORD32 dwBuffLen);
void RTSPMakeOptionsResponse(FrRTSPStruct* hRTSP, char* pBuffer, DWORD32 dwBuffLen);
void RTSPMakeSetparameterResponse(FrRTSPStruct* hRTSP, char* pBuffer, DWORD32 dwBuffLen);
void RTSPMakeGetparameterResponse(FrRTSPStruct* hRTSP, char* pBuffer, DWORD32 dwBuffLen);
void RTSPMakeErrorResponse(FrRTSPStruct* hRTSP, char* pBuffer, DWORD32 dwBuffLen);

// channel
McChannelHandle RTSPChannelFindByURL(FrRTSPStruct* hRTSP, char* pURL, DWORD32 dwURL);

// parse
char* RTSPParsingMessage(FrRTSPStruct* hRTSP, char* pBuffer, DWORD32* dwLen);
BOOL RTSPParseRtpInfo(FrRTSPStruct* hRTSP, char* pBuffer);

#endif	// _RTSP_H_
