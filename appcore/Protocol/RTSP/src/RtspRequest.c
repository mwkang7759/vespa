
#include "Rtsp.h"
//#include <openssl/md5.h>

static char* RTSPGetURL(FrRTSPStruct* hRTSP, DWORD32 dwIndex, char* pBuffer, BOOL bAggragate)
{
	char	*pURL, *pControl;
	int		nLen;

	if (bAggragate)
	{
		if (hRTSP->m_pControl && !strncmp(hRTSP->m_pControl, "rtsp://", 7))
			sprintf(pBuffer, "%s %s\r\n", hRTSP->m_pControl, RTSP_VERSION);
		else
		{
			if (hRTSP->m_szContentBase)
				sprintf(pBuffer, "%s %s\r\n", hRTSP->m_szContentBase, RTSP_VERSION);
			else
				sprintf(pBuffer, "%s %s\r\n", hRTSP->m_szContentURL, RTSP_VERSION);
		}
	}
	else
	{
		pControl = hRTSP->m_aryChannelInfo[dwIndex]->m_pControl;
		if (!pControl)
		{
			if (hRTSP->m_dwChannelNum == 1)
				sprintf(pBuffer, "%s %s\r\n", hRTSP->m_szContentURL, RTSP_VERSION);
			else
				return	NULL;
		}
		else
		{
			if (!strncmp(pControl, "rtsp://", 7))
				sprintf(pBuffer, "%s %s\r\n", pControl, RTSP_VERSION);
			else
			{
				if (hRTSP->m_pControl && !strncmp(hRTSP->m_pControl, "rtsp://", 7))
					pURL = hRTSP->m_pControl;
				else
				{
					if (hRTSP->m_szContentBase)
						pURL = hRTSP->m_szContentBase;
					else
						pURL = hRTSP->m_szContentURL;
				}

				nLen = strlen(pURL);
				if (pURL[nLen-1] == '/')
					sprintf(pBuffer, "%s%s %s\r\n", pURL, pControl, RTSP_VERSION);
				else
					sprintf(pBuffer, "%s/%s %s\r\n", pURL, pControl, RTSP_VERSION);
			}
		}
	}
	pBuffer += strlen(pBuffer);

	return	pBuffer;
}

static void RTSPGetMasterKey(FrRTSPStruct* hRTSP)
{
	DWORD32	dwMasterKey, dwMasterKey2;

	hRTSP->m_szMasterKey = (char*)MALLOC(MAX_KEY_LEN);
	dwMasterKey = RAND();
	if (dwMasterKey < 0x10000000)
		dwMasterKey = MAX_DWORD - dwMasterKey;
	dwMasterKey2 = RAND();
	if (dwMasterKey2 < 0x10000000)
		dwMasterKey2 = MAX_DWORD - dwMasterKey2;
	sprintf(hRTSP->m_szMasterKey, "%u%u", dwMasterKey, dwMasterKey2);
}

BOOL RTSPMakeHTTPGetRequest(FrRTSPStruct* hRTSP, char* pBuffer)
{
	sprintf(pBuffer, "GET %s %s\r\n", hRTSP->m_szFilename, HTTP_VERSION);
	pBuffer += strlen(pBuffer);

	sprintf(pBuffer, "x-sessioncookie: tD9hKgAAfB8ABCftAAAAAw\r\n");
	pBuffer += strlen(pBuffer);

	sprintf(pBuffer, "Accept: application/x-rtsp-tunnelled\r\n");
	pBuffer += strlen(pBuffer);

	// User-Agent
	if (hRTSP->m_szUserAgent)
	{
		sprintf(pBuffer, "User-Agent: %s\r\n", hRTSP->m_szUserAgent);
		pBuffer += strlen(pBuffer);
	}

	sprintf(pBuffer, "\r\n");

	return	TRUE;
}

BOOL RTSPMakeHTTPPostRequest(FrRTSPStruct* hRTSP, char* pBuffer, char* pContent)
{
	//char*	pBase64;
	//DWORD32	dwBase64;

	//pBase64 = (char*)UtilBase64Encoding(pContent, strlen(pContent), &dwBase64);

	sprintf(pBuffer, "POST %s %s\r\n", hRTSP->m_szFilename, HTTP_VERSION);
	pBuffer += strlen(pBuffer);

	// User-Agent
	if (hRTSP->m_szUserAgent)
	{
		sprintf(pBuffer, "User-Agent: %s\r\n", hRTSP->m_szUserAgent);
		pBuffer += strlen(pBuffer);
	}

	// Content-type
	sprintf(pBuffer, "Content-type: application/x-rtsp-tunnelled\r\n");
	pBuffer += strlen(pBuffer);

	// Pragma
	sprintf(pBuffer, "Pragma: no-cache\r\n");
	pBuffer += strlen(pBuffer);

	// Cache-Control
	sprintf(pBuffer, "Cache-Control: no-cache\r\n");
	pBuffer += strlen(pBuffer);

	// Content-Length
	sprintf(pBuffer, "Content-Length: %d\r\n", strlen(pContent));
	pBuffer += strlen(pBuffer);

	// Content
	sprintf(pBuffer, "\r\n");
	pBuffer += strlen(pBuffer);

	strcpy(pBuffer, pContent);
	//FREE(pBase64);

	return	TRUE;
}

void RTSPMakeHash(FrRTSPStruct* hRTSP, char* pszMethod, char *pRes)
{
	//int  i;

	//BYTE szMD5Sum_Val[16+1]="";
	//BYTE szSum[1024]="";
	//BYTE szResponseHA1[1024]="";
	//BYTE szResponseHA2[1024]="";

	////sprintf(szSum, "%s", "Mufasa:testrealm@host.com:Circle Of Life");
	//sprintf((char *)szSum, "%s:%s:%s", hRTSP->m_szUserName, hRTSP->m_szDigestRealm, hRTSP->m_szUserPasswd);
	//MD5(szSum, strlen((char *)szSum), (unsigned char*)&szMD5Sum_Val);
	//for (i=0; i<16; i++)
	//	sprintf((char *)&szResponseHA1[2*i], "%02x", szMD5Sum_Val[i]);

	//sprintf((char *)szSum, "%s:%s", pszMethod, hRTSP->m_szContentURL);
	//MD5(szSum, strlen((char *)szSum), (unsigned char*)&szMD5Sum_Val);
	//for (i=0; i<16; i++)
	//	sprintf((char *)&szResponseHA2[2*i], "%02x", szMD5Sum_Val[i]);

	//sprintf((char *)szSum, "%s:%s:%s", szResponseHA1, hRTSP->m_szDigestNonce, szResponseHA2);
	//MD5(szSum, strlen((char *)szSum), (unsigned char*)&szMD5Sum_Val);
	//for (i=0; i<16; i++)
	//	sprintf(&pRes[2*i], "%02x", szMD5Sum_Val[i]);

}

BOOL RTSPMakeOptionsRequest(FrRTSPStruct* hRTSP, char* pBuffer)
{
	if (hRTSP->m_bRelay)
	{
		CHAR		*pRtspsUrl;

		pRtspsUrl = UtilStrReplaceAll(hRTSP->m_szContentURL, "rtsp://", "rtsps://");
		sprintf(pBuffer, "OPTIONS %s %s\r\n", pRtspsUrl, RTSP_VERSION);
		pBuffer += strlen(pBuffer);
		FREE(pRtspsUrl);
	}
	else
	{
		sprintf(pBuffer, "OPTIONS %s %s\r\n", hRTSP->m_szContentURL, RTSP_VERSION);
		pBuffer += strlen(pBuffer);
	}

	// CSeq
	sprintf(pBuffer, "CSeq: %d\r\n", hRTSP->m_iSendCSeq++);
	pBuffer += strlen(pBuffer);
	if (hRTSP->m_iSendCSeq >= 256)
		hRTSP->m_iSendCSeq = 0;

	if (!hRTSP->m_szSaltingKey && hRTSP->m_bMgosp)
	{
		if (!hRTSP->m_szMasterKey)
		{
			if (hRTSP->m_szUserAgent)
				hRTSP->m_szMasterKey = UtilStrStrdup(hRTSP->m_szUserAgent);
			else
				RTSPGetMasterKey(hRTSP);
		}
		sprintf(pBuffer, "X-MGO-User-Agent: %s\r\n", hRTSP->m_szMasterKey);
		pBuffer += strlen(pBuffer);
		sprintf(pBuffer, "X-MGO-Support: TRUE\r\n");
		pBuffer += strlen(pBuffer);
	}

	if (hRTSP->m_szDigestRealm && hRTSP->m_szDigestNonce)
	{
		char szResponse[1024]="";
		RTSPMakeHash(hRTSP, "OPTIONS", szResponse);
		sprintf(pBuffer, "Authorization: Digest username=\"%s\", realm=\"%s\", nonce=\"%s\", uri=\"%s\", response=\"%s\"\r\n",
			hRTSP->m_szUserName, hRTSP->m_szDigestRealm, hRTSP->m_szDigestNonce, hRTSP->m_szContentURL, szResponse);
		pBuffer += strlen(pBuffer);
	}

	pBuffer += strlen(pBuffer);
	sprintf(pBuffer, "\r\n");

	return	TRUE;
}

BOOL RTSPMakeAnnounceRequest(FrRTSPStruct* hRTSP, char* pBuffer)
{
	sprintf(pBuffer, "ANNOUNCE %s %s\r\n", hRTSP->m_szContentURL, RTSP_VERSION);
	pBuffer += strlen(pBuffer);

	// CSeq
	sprintf(pBuffer, "CSeq: %d\r\n", hRTSP->m_iSendCSeq++);
	pBuffer += strlen(pBuffer);
	if (hRTSP->m_iSendCSeq >= 256)
		hRTSP->m_iSendCSeq = 0;

	// Content-Type
	sprintf(pBuffer, "Content-Type: application/sdp\r\n");
	pBuffer += strlen(pBuffer);

	// Content-Base
	//sprintf(pBuffer, "Content-Base: %s\r\n", hRTSP->m_szContentURL);
	//pBuffer += strlen(pBuffer);

	// X-UPSTREAMING-JOIN
	if (hRTSP->m_bMMS)
	{
		sprintf(pBuffer, "X-UPSTREAMING-JOIN: ");
		pBuffer += strlen(pBuffer);

		if (hRTSP->m_bJoinContent)
			sprintf(pBuffer, "TRUE\r\n");
		else
			sprintf(pBuffer, "FALSE\r\n");
		pBuffer += strlen(pBuffer);
	}

	// SDP
	if (hRTSP->m_bMgosp)
	{
	}
	else
	{
		// Content-Length
		sprintf(pBuffer, "Content-Length: %ld\r\n", hRTSP->m_dwSDPLength);
		pBuffer += strlen(pBuffer);

		sprintf(pBuffer, "\r\n");
		pBuffer += strlen(pBuffer);

		sprintf(pBuffer, "%s", hRTSP->m_pSDP);
	}

	return	TRUE;
}

BOOL RTSPMakeDescribeRequest(FrRTSPStruct* hRTSP, char* pBuffer)
{
	sprintf(pBuffer, "DESCRIBE %s %s\r\n", hRTSP->m_szContentURL, RTSP_VERSION);
	pBuffer += strlen(pBuffer);

	// CSeq
	sprintf(pBuffer, "CSeq: %d\r\n", hRTSP->m_iSendCSeq++);
	pBuffer += strlen(pBuffer);
	if (hRTSP->m_iSendCSeq >= 256)
		hRTSP->m_iSendCSeq = 0;

	// Accept
	sprintf(pBuffer, "Accept: application/sdp\r\n");
	pBuffer += strlen(pBuffer);

	// Accept-Language
	//sprintf(pBuffer, "Accept-Language: en-US\r\n");
	//pBuffer += strlen(pBuffer);

	// User-Agent
	if (hRTSP->m_bMgosp)
	{
		if (!hRTSP->m_szMasterKey)
		{
			if (hRTSP->m_szUserAgent)
				hRTSP->m_szMasterKey = UtilStrStrdup(hRTSP->m_szUserAgent);
			else
				RTSPGetMasterKey(hRTSP);
		}
		sprintf(pBuffer, "X-MGO-User-Agent: %s\r\n", hRTSP->m_szMasterKey);
		pBuffer += strlen(pBuffer);
		sprintf(pBuffer, "X-MGO-Support: TRUE\r\n");
		pBuffer += strlen(pBuffer);
	}
	else
	{
		//sprintf(pBuffer, "User-Agent: 01056SK32351117622027141831970276;156;6;49159;232;2236;;0;1;0x0230;0x800001;0x878010\r\n");
		//sprintf(pBuffer, "User-Agent: SKT46LG59251117622028141695699012;162;2;49153;49153;2236;0;1;0x0300;0x080001;0x028000;0x0000\r\n");
		//sprintf(pBuffer, "User-Agent: SKPC0SK9935112402962513181193619011;91;0;32768;30;2236\r\n");
		//sprintf(pBuffer, "User-Agent: SKT56SK41351124029625131812345678;136;1;33280;94;2236;0;1;0x0230;0x800001;0x878010\r\n");
		//sprintf(pBuffer, "User-Agent: SKPC0SK9935112402962513181193619011;91;0;32768;30;2236\r\n");
		//sprintf(pBuffer, "User-Agent: OakPlayer/0.1[[man=OTT,min=,spv=0x2101,lsz=0x01,col=0x01,vcd=0,acd=0x01]]\r\n");
		//sprintf(pBuffer, "User-Agent: XEPEG PLAYER(org=MW;model=20;terversion=00;min=820000000000;lcd=1;cl=2;vo=0;HwClass=1;version=0x0202;visualpro=0x800000;audiopro=0x028000)\r\n");
		if (hRTSP->m_szUserAgent)
		{
			sprintf(pBuffer, "User-Agent: %s\r\n", hRTSP->m_szUserAgent);
			pBuffer += strlen(pBuffer);
		}
	}

	// UserName
	//if (hRTSP->m_szUserName)
	//{
	//	sprintf(pBuffer, "Authorization: username=\"%s\"\r\n", hRTSP->m_szUserName);
	//	pBuffer += strlen(pBuffer);
	//}
	if (hRTSP->m_szDigestRealm && hRTSP->m_szDigestNonce && hRTSP->m_szUserName)
	{
		char szResponse[1024]="";
		RTSPMakeHash(hRTSP, "DESCRIBE", szResponse);
		sprintf(pBuffer, "Authorization: Digest username=\"%s\", realm=\"%s\", nonce=\"%s\", uri=\"%s\", response=\"%s\"\r\n",
			hRTSP->m_szUserName, hRTSP->m_szDigestRealm, hRTSP->m_szDigestNonce, hRTSP->m_szContentURL, szResponse);
		pBuffer += strlen(pBuffer);
	}

	// Range
	if (hRTSP->m_dwStartDate)
	{
		sprintf(pBuffer, "Range: clock=%8.8ldT%6.6ld.%3.3ldZ-", hRTSP->m_dwStartDate,
								hRTSP->m_dwStartTime/1000, hRTSP->m_dwStartTime%1000);
		pBuffer += strlen(pBuffer);

		if (hRTSP->m_dwEndDate)
			sprintf(pBuffer, "%8.8ldT%6.6ld.%3.3ldZ\r\n", hRTSP->m_dwEndDate,
								hRTSP->m_dwEndTime/1000, hRTSP->m_dwEndTime%1000);
		else
			sprintf(pBuffer, "\r\n");
		pBuffer += strlen(pBuffer);
	}

	// set low latency
	if (hRTSP->m_bSRT == TRUE) {
		sprintf(pBuffer, "X-RTPTransport: %d\r\n", 10000);	// dummy value.
		pBuffer += strlen(pBuffer);
	}

	// buffer time
	sprintf(pBuffer, "X-BufferTime: %d\r\n", hRTSP->m_dwBufferTime);
	pBuffer += strlen(pBuffer);

	sprintf(pBuffer, "\r\n");

	return	TRUE;
}

BOOL RTSPMakeSetupRequest(FrRTSPStruct* hRTSP, char* pBuffer, DWORD32 dwIndex)
{
	McChannelHandle	hChannel;

	hChannel = hRTSP->m_aryChannelInfo[dwIndex];

	sprintf(pBuffer, "SETUP ");
	pBuffer += strlen(pBuffer);

	pBuffer = RTSPGetURL(hRTSP, dwIndex, pBuffer, FALSE);
	if (!pBuffer)
		return	FALSE;

	// CSeq
	sprintf(pBuffer, "CSeq: %d\r\n", hRTSP->m_iSendCSeq++);
	pBuffer += strlen(pBuffer);
	if (hRTSP->m_iSendCSeq >= 256)
		hRTSP->m_iSendCSeq = 0;

	// Session..
	//if(dwIndex)
	if (hRTSP->m_pSession)
	{
		sprintf(pBuffer, "Session: %s\r\n", hRTSP->m_pSession);
		pBuffer += strlen(pBuffer);
	}

	// Transport
	if (hRTSP->m_bMulti)
	{
		sprintf(pBuffer, "Transport: RTP/AVP;multicast\r\n");
	}
	else
	{
		if (hRTSP->m_bUDP) {
			if (hRTSP->m_bPublishing)
				sprintf(pBuffer, "Transport: RTP/AVP;unicast;client_port=%d-%d;ssrc=%lx;mode=record\r\n",
							hChannel->m_wLocalRTPPort, hChannel->m_wLocalRTCPPort, hChannel->m_dwRTPSSRC);
			else {
                if(hRTSP->m_bUDPfec) {
                    sprintf(pBuffer, "Transport: RTP/AVP;unicast;client_port=%d-%d;ssrc=%lx;DTBFEC\r\n",
							    hChannel->m_wLocalRTPPort, hChannel->m_wLocalRTCPPort, hChannel->m_dwRTPSSRC);
                }
                else {
				    sprintf(pBuffer, "Transport: RTP/AVP;unicast;client_port=%d-%d;ssrc=%lx\r\n",
							    hChannel->m_wLocalRTPPort, hChannel->m_wLocalRTCPPort, hChannel->m_dwRTPSSRC);
                }
			}
		}
		else {
			if (hRTSP->m_bPublishing)
				sprintf(pBuffer, "Transport: RTP/AVP/TCP;unicast;interleaved=%d-%d;ssrc=%lx;mode=record\r\n",
							hChannel->m_wLocalRTPPort, hChannel->m_wLocalRTCPPort, hChannel->m_dwRTPSSRC);
			else
				sprintf(pBuffer, "Transport: RTP/AVP/TCP;unicast;interleaved=%d-%d;ssrc=%lx\r\n",
							hChannel->m_wLocalRTPPort, hChannel->m_wLocalRTCPPort, hChannel->m_dwRTPSSRC);
		}
			//sprintf(pBuffer, "Transport: RTP/AVP\r\n"
			//				);
	}
	pBuffer += strlen(pBuffer);

	// Authorization
	if (hRTSP->m_szDigestRealm && hRTSP->m_szDigestNonce && hRTSP->m_szUserName)
	{
		char szResponse[1024]="";
		RTSPMakeHash(hRTSP, "SETUP", szResponse);
		sprintf(pBuffer, "Authorization: Digest username=\"%s\", realm=\"%s\", nonce=\"%s\", uri=\"%s\", response=\"%s\"\r\n",
			hRTSP->m_szUserName, hRTSP->m_szDigestRealm, hRTSP->m_szDigestNonce, hRTSP->m_szContentURL, szResponse);
		pBuffer += strlen(pBuffer);
	}

	if (hRTSP->m_bTS)
	{
		// User-Agent
		if (hRTSP->m_szUserAgent)
		{
			sprintf(pBuffer, "User-Agent: %s\r\n", hRTSP->m_szUserAgent);
			pBuffer += strlen(pBuffer);
		}

		// UserName
		if (hRTSP->m_szUserName)
		{
			sprintf(pBuffer, "Authorization: username=\"%s\"\r\n", hRTSP->m_szUserName);
			pBuffer += strlen(pBuffer);
		}

		// Range
		if (hRTSP->m_dwStartDate)
		{
			sprintf(pBuffer, "Range: clock=%8.8ldT%6.6ld.%3.3ldZ-", hRTSP->m_dwStartDate,
									hRTSP->m_dwStartTime/1000, hRTSP->m_dwStartTime%1000);
			pBuffer += strlen(pBuffer);

			if (hRTSP->m_dwEndDate)
				sprintf(pBuffer, "%8.8ldT%6.6ld.%3.3ldZ\r\n", hRTSP->m_dwEndDate,
									hRTSP->m_dwEndTime/1000, hRTSP->m_dwEndTime%1000);
			else
				sprintf(pBuffer, "\r\n");
			pBuffer += strlen(pBuffer);
		}
	}
	sprintf(pBuffer, "\r\n");

	return	TRUE;
}

BOOL RTSPMakePlayRequest(FrRTSPStruct* hRTSP, char* pBuffer, BOOL bResume, DWORD32 dwPos)
{
	sprintf(pBuffer, "PLAY ");
	pBuffer += strlen(pBuffer);

	pBuffer = RTSPGetURL(hRTSP, 0, pBuffer, TRUE);
	if (!pBuffer)
		return	FALSE;

	// CSeq
	sprintf(pBuffer, "CSeq: %d\r\n", hRTSP->m_iSendCSeq++);
	pBuffer += strlen(pBuffer);
	if (hRTSP->m_iSendCSeq >= 256)
		hRTSP->m_iSendCSeq = 0;

	// Range
	if (!bResume)
	{
		DWORD32	dwSec, dwMS;

		dwSec = dwPos / 1000;
		dwMS = dwPos - dwSec * 1000;
		sprintf(pBuffer, "Range: npt=%ld.%3.3ld-\r\n", dwSec, dwMS);
		pBuffer += strlen(pBuffer);
	}

	// Authorization
	if (hRTSP->m_szDigestRealm && hRTSP->m_szDigestNonce && hRTSP->m_szUserName)
	{
		char szResponse[1024]="";
		RTSPMakeHash(hRTSP, "PLAY", szResponse);
		sprintf(pBuffer, "Authorization: Digest username=\"%s\", realm=\"%s\", nonce=\"%s\", uri=\"%s\", response=\"%s\"\r\n",
			hRTSP->m_szUserName, hRTSP->m_szDigestRealm, hRTSP->m_szDigestNonce, hRTSP->m_szContentURL, szResponse);
		pBuffer += strlen(pBuffer);
	}

	// Session
	sprintf(pBuffer, "Session: %s\r\n", hRTSP->m_pSession);
	pBuffer += strlen(pBuffer);
	sprintf(pBuffer, "\r\n");

	return	TRUE;
}

BOOL RTSPMakePlayClockRequest(FrRTSPStruct* hRTSP, char* pBuffer, DWORD32 dwDate, DWORD32 dwTime)
{
	sprintf(pBuffer, "PLAY ");
	pBuffer += strlen(pBuffer);

	pBuffer = RTSPGetURL(hRTSP, 0, pBuffer, TRUE);
	if (!pBuffer)
		return	FALSE;

	// CSeq
	sprintf(pBuffer, "CSeq: %d\r\n", hRTSP->m_iSendCSeq++);
	pBuffer += strlen(pBuffer);
	if (hRTSP->m_iSendCSeq >= 256)
		hRTSP->m_iSendCSeq = 0;

	// Session
	sprintf(pBuffer, "Session: %s\r\n", hRTSP->m_pSession);
	pBuffer += strlen(pBuffer);

	if (hRTSP->m_bTS)
	{
		// User-Agent
		if (hRTSP->m_szUserAgent)
		{
			sprintf(pBuffer, "User-Agent: %s\r\n", hRTSP->m_szUserAgent);
			pBuffer += strlen(pBuffer);
		}
	}

	// Range
	sprintf(pBuffer, "Range: clock=%8.8ldT%6.6ld.%3.3ldZ-\r\n", dwDate, dwTime/1000, dwTime%1000);
	pBuffer += strlen(pBuffer);

	sprintf(pBuffer, "\r\n");

	return	TRUE;
}

BOOL RTSPMakePlayScaleRequest(FrRTSPStruct* hRTSP, char* pBuffer, int nScale)
{
	sprintf(pBuffer, "PLAY ");
	pBuffer += strlen(pBuffer);

	pBuffer = RTSPGetURL(hRTSP, 0, pBuffer, TRUE);
	if (!pBuffer)
		return	FALSE;

	// CSeq
	sprintf(pBuffer, "CSeq: %d\r\n", hRTSP->m_iSendCSeq++);
	pBuffer += strlen(pBuffer);
	if (hRTSP->m_iSendCSeq >= 256)
		hRTSP->m_iSendCSeq = 0;

	// Session
	sprintf(pBuffer, "Session: %s\r\n", hRTSP->m_pSession);
	pBuffer += strlen(pBuffer);

	if (hRTSP->m_bTS)
	{
		// User-Agent
		if (hRTSP->m_szUserAgent)
		{
			sprintf(pBuffer, "User-Agent: %s\r\n", hRTSP->m_szUserAgent);
			pBuffer += strlen(pBuffer);
		}
	}

	// Scale
	if (nScale == -1)
		nScale = 1;

	sprintf(pBuffer, "Scale: %d\r\n", nScale);
	pBuffer += strlen(pBuffer);

	// Range
	if (hRTSP->m_bTS)
	{
		sprintf(pBuffer, "Range: clock=%8.8ldT%6.6ld.%3.3ldZ-\r\n", hRTSP->m_dwPlayFromDate,
						hRTSP->m_dwPlayFromTime/1000, hRTSP->m_dwPlayFromTime%1000);
		pBuffer += strlen(pBuffer);
	}

	sprintf(pBuffer, "\r\n");

	return	TRUE;
}

BOOL RTSPMakePauseRequest(FrRTSPStruct* hRTSP, char* pBuffer)
{
	sprintf(pBuffer, "PAUSE ");
	pBuffer += strlen(pBuffer);

	pBuffer = RTSPGetURL(hRTSP, 0, pBuffer, TRUE);
	if (!pBuffer)
		return	FALSE;

	// CSeq
	sprintf(pBuffer, "CSeq: %d\r\n", hRTSP->m_iSendCSeq++);
	pBuffer += strlen(pBuffer);
	if (hRTSP->m_iSendCSeq >= 256)
		hRTSP->m_iSendCSeq = 0;

	// Session
	sprintf(pBuffer, "Session: %s\r\n", hRTSP->m_pSession);
	pBuffer += strlen(pBuffer);
	sprintf(pBuffer, "\r\n");

	return	TRUE;
}

BOOL RTSPMakeTeardownRequest(FrRTSPStruct* hRTSP, char* pBuffer)
{
	sprintf(pBuffer, "TEARDOWN ");
	pBuffer += strlen(pBuffer);

	pBuffer = RTSPGetURL(hRTSP, 0, pBuffer, TRUE);
	if (!pBuffer)
		return	FALSE;

	// CSeq
	sprintf(pBuffer, "CSeq: %d\r\n", hRTSP->m_iSendCSeq++);
	pBuffer += strlen(pBuffer);
	if (hRTSP->m_iSendCSeq >= 256)
		hRTSP->m_iSendCSeq = 0;

	// Session
	sprintf(pBuffer, "Session: %s\r\n", hRTSP->m_pSession);
	pBuffer += strlen(pBuffer);

	// Connection
	sprintf(pBuffer, "Connection: close\r\n");
	pBuffer += strlen(pBuffer);

	sprintf(pBuffer, "\r\n");

	return	TRUE;
}

BOOL RTSPMakeGetParamRequest(FrRTSPStruct* hRTSP, char* pBuffer, char* pContent)
{
	sprintf(pBuffer, "GET_PARAMETER ");
	pBuffer += strlen(pBuffer);

	pBuffer = RTSPGetURL(hRTSP, 0, pBuffer, TRUE);
	if (!pBuffer)
		return	FALSE;

	// CSeq
	sprintf(pBuffer, "CSeq: %d\r\n", hRTSP->m_iSendCSeq++);
	pBuffer += strlen(pBuffer);
	if (hRTSP->m_iSendCSeq >= 256)
		hRTSP->m_iSendCSeq = 0;

	// Session
	sprintf(pBuffer, "Session: %s\r\n", hRTSP->m_pSession);
	pBuffer += strlen(pBuffer);

	// Authorization
	if (hRTSP->m_szDigestRealm && hRTSP->m_szDigestNonce && hRTSP->m_szUserName)
	{
		char szResponse[1024]="";
		RTSPMakeHash(hRTSP, "GET_PARAMETER", szResponse);
		sprintf(pBuffer, "Authorization: Digest username=\"%s\", realm=\"%s\", nonce=\"%s\", uri=\"%s\", response=\"%s\"\r\n",
			hRTSP->m_szUserName, hRTSP->m_szDigestRealm, hRTSP->m_szDigestNonce, hRTSP->m_szContentURL, szResponse);
		pBuffer += strlen(pBuffer);
	}


	// Content-Length
	if (pContent)
	{
		// Content-Type
		sprintf(pBuffer, "Content-Type: text/parameters\r\n");
		pBuffer += strlen(pBuffer);

		sprintf(pBuffer, "Content-Length: %d\r\n", strlen(pContent));
		pBuffer += strlen(pBuffer);
	}

	sprintf(pBuffer, "\r\n");
	pBuffer += strlen(pBuffer);

    if (pContent) {
		sprintf(pBuffer, "%s", pContent);
	}

	return	TRUE;
}

BOOL RTSPMakeSetParamRequest(FrRTSPStruct* hRTSP, char* pBuffer, char* pContent)
{
	DWORD32	dwContent;

	sprintf(pBuffer, "SET_PARAMETER ");
	pBuffer += strlen(pBuffer);

	pBuffer = RTSPGetURL(hRTSP, 0, pBuffer, TRUE);
	if (!pBuffer)
		return	FALSE;

	// CSeq
	sprintf(pBuffer, "CSeq: %d\r\n", hRTSP->m_iSendCSeq++);
	pBuffer += strlen(pBuffer);
	if (hRTSP->m_iSendCSeq >= 256)
		hRTSP->m_iSendCSeq = 0;

	// Session
	if (hRTSP->m_pSession)
	{
		sprintf(pBuffer, "Session: %s\r\n", hRTSP->m_pSession);
		pBuffer += strlen(pBuffer);
	}

	// Content-Type
	sprintf(pBuffer, "Content-Type: text/parameters\r\n");
	pBuffer += strlen(pBuffer);

	// Content
	dwContent = strlen(pContent);

	// Content-Length
	sprintf(pBuffer, "Content-Length: %ld\r\n", dwContent);
	pBuffer += strlen(pBuffer);

	sprintf(pBuffer, "\r\n");
	pBuffer += strlen(pBuffer);

	sprintf(pBuffer, "%s", pContent);

	return	TRUE;
}

BOOL RTSPMakeRecordRequest(FrRTSPStruct* hRTSP, char* pBuffer)
{
	sprintf(pBuffer, "RECORD ");
	pBuffer += strlen(pBuffer);

	pBuffer = RTSPGetURL(hRTSP, 0, pBuffer, TRUE);
	if (!pBuffer)
		return	FALSE;

	// CSeq
	sprintf(pBuffer, "CSeq: %d\r\n", hRTSP->m_iSendCSeq++);
	pBuffer += strlen(pBuffer);
	if (hRTSP->m_iSendCSeq >= 256)
		hRTSP->m_iSendCSeq = 0;

	// Range
	sprintf(pBuffer, "Range: npt=0.000-\r\n");
	pBuffer += strlen(pBuffer);

	// Session
	sprintf(pBuffer, "Session: %s\r\n", hRTSP->m_pSession);
	pBuffer += strlen(pBuffer);
	sprintf(pBuffer, "\r\n");

	return	TRUE;
}

BOOL RTSPMakePlayTrickPlayRequest(FrRTSPStruct* hRTSP, char* pBuffer, BOOL bResume, DWORD32 dwPos, INT32 nScale, BOOL bIntra, DWORD32 dwIntraInterval)
{
	sprintf(pBuffer, "PLAY ");
	pBuffer += strlen(pBuffer);

	pBuffer = RTSPGetURL(hRTSP, 0, pBuffer, TRUE);
	if (!pBuffer)
		return	FALSE;

	// CSeq
	sprintf(pBuffer, "CSeq: %d\r\n", hRTSP->m_iSendCSeq++);
	pBuffer += strlen(pBuffer);
	if (hRTSP->m_iSendCSeq >= 256)
		hRTSP->m_iSendCSeq = 0;

	// Range
	if (!bResume)
	{
		DWORD32	dwSec, dwMS;

		dwSec = dwPos / 1000;
		dwMS = dwPos - dwSec * 1000;
		sprintf(pBuffer, "Range: npt=%ld.%3.3ld-\r\n", dwSec, dwMS);
		pBuffer += strlen(pBuffer);
	}

	// Scale
	if (nScale)
	{
		sprintf(pBuffer, "Scale: %f\r\n", (float)nScale);
		pBuffer += strlen(pBuffer);
	}
	else
		nScale = 1;

	// Intra
	if (bIntra)
	{
		if (dwIntraInterval)
			sprintf(pBuffer, "Frames: intra/%d\r\n", dwIntraInterval);
		else
			sprintf(pBuffer, "Frames: intra\r\n");
		pBuffer += strlen(pBuffer);
	}

	// Authorization
	if (hRTSP->m_szDigestRealm && hRTSP->m_szDigestNonce && hRTSP->m_szUserName)
	{
		char szResponse[1024] = "";
		RTSPMakeHash(hRTSP, "PLAY", szResponse);
		sprintf(pBuffer, "Authorization: Digest username=\"%s\", realm=\"%s\", nonce=\"%s\", uri=\"%s\", response=\"%s\"\r\n",
			hRTSP->m_szUserName, hRTSP->m_szDigestRealm, hRTSP->m_szDigestNonce, hRTSP->m_szContentURL, szResponse);
		pBuffer += strlen(pBuffer);
	}

	// Session
	sprintf(pBuffer, "Session: %s\r\n", hRTSP->m_pSession);
	pBuffer += strlen(pBuffer);
	sprintf(pBuffer, "\r\n");

	return	TRUE;
}

