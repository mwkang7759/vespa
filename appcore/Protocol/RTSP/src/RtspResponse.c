
#include "Rtsp.h"

#define		NUM_OF_STATUS_CODE		44

int		g_nStatusCode[NUM_OF_STATUS_CODE] =
{
	100,	200,	201,	250,	300,	301,	302,	303,	304,	305,
	400,	401,	402,	403,	404,	405,	406,	407,	408,	410,
	411,	412,	413,	414,	415,	451,	452,	453,	454,	455,
	456,	457,	458,	459,	460,	461,	462,	500,	501,	502,
	503,	504,	505,	551
};

char	*g_strStatusCode[NUM_OF_STATUS_CODE] =
{
	"Continue",								// 100
	"OK",									// 200
	"Created",								// 201
	"Low on Storage Space",					// 250
	"Multiple Choices",						// 300
	"Moved Permanetly",						// 301
	"Moved Temporarily",					// 302
	"See Other",							// 303
	"Not Modified",							// 304
	"Use Proxy",							// 305
	"Bad Request",							// 400
	"Unauthorized",							// 401
	"Payment Required",						// 402
	"Forbidden",							// 403
	"Not Found",							// 404
	"Method Not Allowed",					// 405
	"Not Accepable",						// 406
	"Proxy Authentication Required",		// 407
	"Request Time-out",						// 408
	"Gone",									// 410
	"Length Required",						// 411
	"Precondition Failed",					// 412
	"Request Entity Too Large",				// 413
	"Request-URI Too Large",				// 414
	"Unsupported Media Type",				// 415
	"Parameter Not Understood",				// 451
	"Conference Not Found",					// 452
	"Not Enough Bandwidth",					// 453
	"Session Not Found",					// 454
	"Method Not Valid in This State",		// 455
	"Header Field Not Valid for Resource",	// 456
	"Invalid Range",						// 457
	"Parameter Is Read-Only",				// 458
	"Aggregate operation not allowed",		// 459
	"Only aggregate operation allowed",		// 460
	"Unsupported transport",				// 461
	"Destination unreachable",				// 462
	"Internal Server Error",				// 500
	"Not Implemented",						// 501
	"Bad Gateway",							// 502
	"Service Unavailable",					// 503
	"Gateway Time-out",						// 504
	"RTSP Version not supported",			// 505
	"Option not supported"					// 551
};

static char* RTSPGetStingStatusCode(DWORD32 dwStatusCode)
{
	int		i;

	for (i = 0;i < NUM_OF_STATUS_CODE;i++)
	{
		if (g_nStatusCode[i] == (int)dwStatusCode)
			break;
	}

	if (i == NUM_OF_STATUS_CODE)
		return	NULL;

	return	g_strStatusCode[i];
}

void RTSPMakeAnnounceResponse(FrRTSPStruct* hRTSP, char* pBuffer, DWORD32 dwBuffLen)
{
	sprintf(pBuffer, "%s %ld %s\r\n", RTSP_VERSION, hRTSP->m_dwStatusCode,
								RTSPGetStingStatusCode(hRTSP->m_dwStatusCode));
	pBuffer += strlen(pBuffer);

	// CSeq
	sprintf(pBuffer, "CSeq: %d\r\n", hRTSP->m_iRecvCSeq++);
	pBuffer += strlen(pBuffer);

	// Session
	if (hRTSP->m_pSession)
	{
		sprintf(pBuffer, "Session: %s\r\n", hRTSP->m_pSession);
		pBuffer += strlen(pBuffer);
	}

	// Server
	//sprintf(pBuffer, "Server: Transcoder/1.0.0 (Win32)\r\n");
	sprintf(pBuffer, "Server: %s\r\n", hRTSP->m_szServerName);
	pBuffer += strlen(pBuffer);

	// nhn, dtb result code
	sprintf(pBuffer, "X-dtb-result: %d\r\n", hRTSP->m_dwDtbResultCode);
	pBuffer += strlen(pBuffer);

	sprintf(pBuffer, "\r\n");
}

void RTSPMakeRedirectResponse(FrRTSPStruct* hRTSP, char* pBuffer, DWORD32 dwBuffLen)
{
	sprintf(pBuffer, "%s %ld %s\r\n", RTSP_VERSION, hRTSP->m_dwStatusCode,
								RTSPGetStingStatusCode(hRTSP->m_dwStatusCode));
	pBuffer += strlen(pBuffer);

	// CSeq
	sprintf(pBuffer, "CSeq: %d\r\n", hRTSP->m_iRecvCSeq++);
	pBuffer += strlen(pBuffer);

	// Server
	//sprintf(pBuffer, "Server: Transcoder/1.0.0 (Win32)\r\n");
	sprintf(pBuffer, "Server: %s\r\n", hRTSP->m_szServerName);
	pBuffer += strlen(pBuffer);

	// nhn, dtb result code
	sprintf(pBuffer, "X-dtb-result: %d\r\n", hRTSP->m_dwDtbResultCode);
	pBuffer += strlen(pBuffer);

	sprintf(pBuffer, "\r\n");
}

void RTSPMakeDescribeResponse(FrRTSPStruct* hRTSP, char* pBuffer, DWORD32 dwBuffLen)
{
	sprintf(pBuffer, "%s %ld %s\r\n", RTSP_VERSION, hRTSP->m_dwStatusCode,
								RTSPGetStingStatusCode(hRTSP->m_dwStatusCode));
	pBuffer += strlen(pBuffer);

	// CSeq
	sprintf(pBuffer, "CSeq: %d\r\n", hRTSP->m_iRecvCSeq);
	pBuffer += strlen(pBuffer);

	// Server
	//sprintf(pBuffer, "Server: Transcoder/1.0.0 (Win32)\r\n");
	sprintf(pBuffer, "Server: %s\r\n", hRTSP->m_szServerName);
	pBuffer += strlen(pBuffer);

	// Content-Type
	sprintf(pBuffer, "Content-Type: application/sdp\r\n");
	pBuffer += strlen(pBuffer);

	// Content-Base
	sprintf(pBuffer, "Content-Base: %s\r\n", hRTSP->m_szContentBase);
	pBuffer += strlen(pBuffer);

    // dtb Key
    if(hRTSP->m_dwDtbKeyCode != 0) {
    	sprintf(pBuffer, "x-dtb-key: %lu\r\n", hRTSP->m_dwDtbKeyCode);
    	pBuffer += strlen(pBuffer);
    }

	// nhn, dtb result code
	sprintf(pBuffer, "X-dtb-result: %d\r\n", hRTSP->m_dwDtbResultCode);
	pBuffer += strlen(pBuffer);

	if (hRTSP->m_dwStatusCode == 200)
	{
		// SDP
		if (hRTSP->m_bMgosp)
		{
		}
		else
		{
			// Content-Length
			sprintf(pBuffer, "Content-Length: %ld\r\n", hRTSP->m_dwSDPLength);
			pBuffer += strlen(pBuffer);

			sprintf(pBuffer, "\r\n");
			pBuffer += strlen(pBuffer);

			sprintf(pBuffer, "%s", hRTSP->m_pSDP);
		}
	}
	else
	{
		sprintf(pBuffer, "\r\n");
		pBuffer += strlen(pBuffer);
	}
}

void RTSPMakeSetupResponse(FrRTSPStruct* hRTSP, char* pBuffer, DWORD32 dwBuffLen)
{
	sprintf(pBuffer, "%s %ld %s\r\n", RTSP_VERSION, hRTSP->m_dwStatusCode,
								RTSPGetStingStatusCode(hRTSP->m_dwStatusCode));
	pBuffer += strlen(pBuffer);

	// CSeq
	sprintf(pBuffer, "CSeq: %d\r\n", hRTSP->m_iRecvCSeq);
	pBuffer += strlen(pBuffer);

	// Server
	//sprintf(pBuffer, "Server: Transcoder/1.0.0 (Win32)\r\n");
	sprintf(pBuffer, "Server: %s\r\n", hRTSP->m_szServerName);
	pBuffer += strlen(pBuffer);

	// Session
	sprintf(pBuffer, "Session: %s\r\n", hRTSP->m_pSession);
	pBuffer += strlen(pBuffer);

	// nhn, dtb result code
	sprintf(pBuffer, "X-dtb-result: %d\r\n", hRTSP->m_dwDtbResultCode);
	pBuffer += strlen(pBuffer);

	// Transport
	if (hRTSP->m_bUDP)
	{
		if (hRTSP->m_bMulti)
			sprintf(pBuffer, "Transport: RTP/AVP;multicast;destination=%s;port=%u-%u\r\n", hRTSP->m_szMultiAddr,
					hRTSP->m_hReqChannel->m_wMultiRTPPort, hRTSP->m_hReqChannel->m_wMultiRTCPPort);
		else
			sprintf(pBuffer, "Transport: RTP/AVP;unicast;source=%s;client_port=%u-%u;server_port=%u-%u\r\n", hRTSP->m_szLocalAddr,
					hRTSP->m_hReqChannel->m_wRemoteRTPPort, hRTSP->m_hReqChannel->m_wRemoteRTCPPort,
					hRTSP->m_hReqChannel->m_wLocalRTPPort, hRTSP->m_hReqChannel->m_wLocalRTCPPort);
	}
	else
		sprintf(pBuffer, "Transport: RTP/AVP/TCP;unicast;interleaved=%u-%u;ssrc=%lx\r\n",
					hRTSP->m_hReqChannel->m_wLocalRTPPort, hRTSP->m_hReqChannel->m_wLocalRTCPPort,
					hRTSP->m_hReqChannel->m_dwRTPSSRC);
	pBuffer += strlen(pBuffer);

	sprintf(pBuffer, "\r\n");
}

void RTSPMakePlayResponse(FrRTSPStruct* hRTSP, char* pBuffer, DWORD32 dwBuffLen)
{
    McChannelHandle hChannel;
    DWORD32         dwIndex;

    sprintf(pBuffer, "%s %ld %s\r\n", RTSP_VERSION, hRTSP->m_dwStatusCode,
                                RTSPGetStingStatusCode(hRTSP->m_dwStatusCode));
    pBuffer += strlen(pBuffer);

    // CSeq
    sprintf(pBuffer, "CSeq: %d\r\n", hRTSP->m_iRecvCSeq);
    pBuffer += strlen(pBuffer);

    // Server
    //sprintf(pBuffer, "Server: Transcoder/1.0.0 (Win32)\r\n");
    sprintf(pBuffer, "Server: %s\r\n", hRTSP->m_szServerName);
    pBuffer += strlen(pBuffer);

    // Session
    sprintf(pBuffer, "Session: %s\r\n", hRTSP->m_pSession);
    pBuffer += strlen(pBuffer);

    // nhn, dtb result code
    sprintf(pBuffer, "X-dtb-result: %d\r\n", hRTSP->m_dwDtbResultCode);
    pBuffer += strlen(pBuffer);

    // Range
    if (!hRTSP->m_dwPlayRange || hRTSP->m_dwPlayRange == INFINITE)
    {
        if (!hRTSP->m_dwPlayFromRange)
            sprintf(pBuffer, "Range: npt=now-\r\n");
        else
            // extend to down to three decimal point to match HASS range, mwkang 2008.08.18
            sprintf(pBuffer, "Range: npt=%.3f-\r\n", (float)hRTSP->m_dwPlayFromRange/1000.);
    }
    else
        // mwkang 2008.08.18
        sprintf(pBuffer, "Range: npt=%.3f-%.3f\r\n", (float)hRTSP->m_dwPlayFromRange/1000.,
                                                (float)hRTSP->m_dwPlayRange/1000.);
    pBuffer += strlen(pBuffer);

    // RTP-Info
    sprintf(pBuffer, "RTP-Info: ");
    pBuffer += strlen(pBuffer);
    for (dwIndex = 0;dwIndex < hRTSP->m_dwChannelNum;dwIndex++)
    {
        hChannel = hRTSP->m_aryChannelInfo[dwIndex];

        if(!hChannel->m_bCheckSetupChannel) {
            continue;
        }

        if (hRTSP->m_b3GP)
        {
            //sprintf(pBuffer, "url=%s;seq=%d;rtptime=%lu,", hChannel->m_pControl, hChannel->m_wSeq, hChannel->m_dwRTPTime);    // android 3.0
            sprintf(pBuffer, "url=%s;seq=%d;rtptime=%lu,", hChannel->m_szControlUrl, hChannel->m_wSeq, hChannel->m_dwRTPTime);
        }
        else
            sprintf(pBuffer, "url=%s;seq=%d;rtptime=%lu;ssrc=%lx,", hChannel->m_pControl, hChannel->m_wSeq, hChannel->m_dwRTPTime, hChannel->m_dwRTPSSRC);
        pBuffer += strlen(pBuffer);
    }
    pBuffer--;
    sprintf(pBuffer, "\r\n\r\n");
}

void RTSPMakeRecordResponse(FrRTSPStruct* hRTSP, char* pBuffer, DWORD32 dwBuffLen)
{
	sprintf(pBuffer, "%s %ld %s\r\n", RTSP_VERSION, hRTSP->m_dwStatusCode,
								RTSPGetStingStatusCode(hRTSP->m_dwStatusCode));
	pBuffer += strlen(pBuffer);

	// CSeq
	sprintf(pBuffer, "CSeq: %d\r\n", hRTSP->m_iRecvCSeq);
	pBuffer += strlen(pBuffer);

	// Server
	//sprintf(pBuffer, "Server: Transcoder/1.0.0 (Win32)\r\n");
	sprintf(pBuffer, "Server: %s\r\n", hRTSP->m_szServerName);
	pBuffer += strlen(pBuffer);

	// Session
	sprintf(pBuffer, "Session: %s\r\n", hRTSP->m_pSession);
	pBuffer += strlen(pBuffer);

	// nhn, dtb result code
	sprintf(pBuffer, "X-dtb-result: %d\r\n", hRTSP->m_dwDtbResultCode);
	pBuffer += strlen(pBuffer);

	// Range
	if (!hRTSP->m_dwPlayRange || hRTSP->m_dwPlayRange == INFINITE)
	{
		if (!hRTSP->m_dwPlayFromRange)
			sprintf(pBuffer, "Range: npt=now-\r\n");
		else
			// extend to down to three decimal point to match HASS range, mwkang 2008.08.18
			sprintf(pBuffer, "Range: npt=%.3f-\r\n", (float)hRTSP->m_dwPlayFromRange/1000.);
	}
	else
		// mwkang 2008.08.18
		sprintf(pBuffer, "Range: npt=%.3f-%.3f\r\n", (float)hRTSP->m_dwPlayFromRange/1000.,
												(float)hRTSP->m_dwPlayRange/1000.);
	pBuffer += strlen(pBuffer);
#if 0
	// RTP-Info
	sprintf(pBuffer, "RTP-Info: ");
	pBuffer += strlen(pBuffer);
	for (dwIndex = 0;dwIndex < hRTSP->m_dwChannelNum;dwIndex++)
	{
		hChannel = hRTSP->m_aryChannelInfo[dwIndex];

        if(!hChannel->m_bCheckSetupChannel) {
            continue;
        }

		if (hRTSP->m_b3GP)
		{
			//sprintf(pBuffer, "url=%s;seq=%d;rtptime=%lu,", hChannel->m_pControl, hChannel->m_wSeq, hChannel->m_dwRTPTime);	// android 3.0
			sprintf(pBuffer, "url=%s;seq=%d;rtptime=%lu,", hChannel->m_szControlUrl, hChannel->m_wSeq, hChannel->m_dwRTPTime);
		}
		else
			sprintf(pBuffer, "url=%s;seq=%d;rtptime=%lu;ssrc=%lx,", hChannel->m_pControl, hChannel->m_wSeq, hChannel->m_dwRTPTime, hChannel->m_dwRTPSSRC);
		pBuffer += strlen(pBuffer);
	}
#endif
	sprintf(pBuffer, "\r\n");
}

void RTSPMakePauseResponse(FrRTSPStruct* hRTSP, char* pBuffer, DWORD32 dwBuffLen)
{
	sprintf(pBuffer, "%s %ld %s\r\n", RTSP_VERSION, hRTSP->m_dwStatusCode,
								RTSPGetStingStatusCode(hRTSP->m_dwStatusCode));
	pBuffer += strlen(pBuffer);

	// CSeq
	sprintf(pBuffer, "CSeq: %d\r\n", hRTSP->m_iRecvCSeq);
	pBuffer += strlen(pBuffer);

	// Server
	//sprintf(pBuffer, "Server: Transcoder/1.0.0 (Win32)\r\n");
	sprintf(pBuffer, "Server: %s\r\n", hRTSP->m_szServerName);
	pBuffer += strlen(pBuffer);

	// Session
	sprintf(pBuffer, "Session: %s\r\n", hRTSP->m_pSession);
	pBuffer += strlen(pBuffer);

	// nhn, dtb result code
	sprintf(pBuffer, "X-dtb-result: %d\r\n", hRTSP->m_dwDtbResultCode);
	pBuffer += strlen(pBuffer);

	sprintf(pBuffer, "\r\n");
}

void RTSPMakeTeardownResponse(FrRTSPStruct* hRTSP, char* pBuffer, DWORD32 dwBuffLen)
{
	sprintf(pBuffer, "%s %ld %s\r\n", RTSP_VERSION, hRTSP->m_dwStatusCode,
								RTSPGetStingStatusCode(hRTSP->m_dwStatusCode));
	pBuffer += strlen(pBuffer);

	// CSeq
	sprintf(pBuffer, "CSeq: %d\r\n", hRTSP->m_iRecvCSeq);
	pBuffer += strlen(pBuffer);

	// Server
	//sprintf(pBuffer, "Server: Transcoder/1.0.0 (Win32)\r\n");
	sprintf(pBuffer, "Server: %s\r\n", hRTSP->m_szServerName);
	pBuffer += strlen(pBuffer);

	// Session
	sprintf(pBuffer, "Session: %s\r\n", hRTSP->m_pSession);
	pBuffer += strlen(pBuffer);

	// nhn, dtb result code
	sprintf(pBuffer, "X-dtb-result: %d\r\n", hRTSP->m_dwDtbResultCode);
	pBuffer += strlen(pBuffer);

	sprintf(pBuffer, "\r\n");
}

void RTSPMakeOptionsResponse(FrRTSPStruct* hRTSP, char* pBuffer, DWORD32 dwBuffLen)
{
	sprintf(pBuffer, "%s %ld %s\r\n", RTSP_VERSION, hRTSP->m_dwStatusCode,
								RTSPGetStingStatusCode(hRTSP->m_dwStatusCode));
	pBuffer += strlen(pBuffer);

	// Server
	//sprintf(pBuffer, "Server: Transcoder/1.0.0 (Win32)\r\n");
	sprintf(pBuffer, "Server: %s\r\n", hRTSP->m_szServerName);
	pBuffer += strlen(pBuffer);

	// CSeq
	sprintf(pBuffer, "CSeq: %d\r\n", hRTSP->m_iRecvCSeq);
	pBuffer += strlen(pBuffer);

	// Public
	//sprintf(pBuffer, "Public: OPTIONS\r\n");
	sprintf(pBuffer, "Public: ANNOUNCE, OPTIONS, DESCRIBE, SETUP, TEARDOWN, PLAY\r\n");		// on test..
	pBuffer += strlen(pBuffer);

	// Session
	if (hRTSP->m_pSession)
	{
		sprintf(pBuffer, "Session: %s\r\n", hRTSP->m_pSession);
		pBuffer += strlen(pBuffer);
	}

	// nhn, dtb result code
	sprintf(pBuffer, "X-dtb-result: %d\r\n", hRTSP->m_dwDtbResultCode);
	pBuffer += strlen(pBuffer);

	// Mgosp
	if (hRTSP->m_bMgosp)
	{
		sprintf(pBuffer, "X-MGO-User-Agent: %s\r\n", hRTSP->m_szMasterKey);
		pBuffer += strlen(pBuffer);
		sprintf(pBuffer, "X-MGO-Auth: %s\r\n", hRTSP->m_szSaltingKey);
		pBuffer += strlen(pBuffer);
	}

	sprintf(pBuffer, "\r\n");
}

void RTSPMakeSetparameterResponse(FrRTSPStruct* hRTSP, char* pBuffer, DWORD32 dwBuffLen)
{
	sprintf(pBuffer, "%s %ld %s\r\n", RTSP_VERSION, hRTSP->m_dwStatusCode,
								RTSPGetStingStatusCode(hRTSP->m_dwStatusCode));
	pBuffer += strlen(pBuffer);

	// CSeq
	sprintf(pBuffer, "CSeq: %d\r\n", hRTSP->m_iRecvCSeq);
	pBuffer += strlen(pBuffer);

	// Session
	if (hRTSP->m_pSession)
	{
		sprintf(pBuffer, "Session: %s\r\n", hRTSP->m_pSession);
		pBuffer += strlen(pBuffer);
	}

	// Server
	if (hRTSP->m_bServer)
	{
		//sprintf(pBuffer, "Server: Transcoder/1.0.0 (Win32)\r\n");
		sprintf(pBuffer, "Server: %s\r\n", hRTSP->m_szServerName);
		pBuffer += strlen(pBuffer);
	}

	// nhn, dtb result code
	sprintf(pBuffer, "X-dtb-result: %d\r\n", hRTSP->m_dwDtbResultCode);
	pBuffer += strlen(pBuffer);

	// MMS
	if (hRTSP->m_bMMS)
	{
		sprintf(pBuffer, "X-PRE-CONTENT : ");
		pBuffer += strlen(pBuffer);
		if (hRTSP->m_bPreContent)
			sprintf(pBuffer, "TRUE\r\n");
		else
			sprintf(pBuffer, "FALSE\r\n");
		pBuffer += strlen(pBuffer);

		sprintf(pBuffer, "X-PRE-CONTENT-TIME: %s\r\n", hRTSP->m_szContentDate);
		pBuffer += strlen(pBuffer);

	}

	sprintf(pBuffer, "\r\n");
}

void RTSPMakeGetparameterResponse(FrRTSPStruct* hRTSP, char* pBuffer, DWORD32 dwBuffLen)
{
	char	szContent[64];
	DWORD32	dwContent;

	sprintf(pBuffer, "%s %ld %s\r\n", RTSP_VERSION, hRTSP->m_dwStatusCode,
								RTSPGetStingStatusCode(hRTSP->m_dwStatusCode));
	pBuffer += strlen(pBuffer);

	// CSeq
	sprintf(pBuffer, "CSeq: %d\r\n", hRTSP->m_iRecvCSeq);
	pBuffer += strlen(pBuffer);

	// Server
	//sprintf(pBuffer, "Server: Transcoder/1.0.0 (Win32)\r\n");
	sprintf(pBuffer, "Server: %s\r\n", hRTSP->m_szServerName);
	pBuffer += strlen(pBuffer);

	// Session
	if (hRTSP->m_pSession)
	{
		sprintf(pBuffer, "Session: %s\r\n", hRTSP->m_pSession);
		pBuffer += strlen(pBuffer);
	}

	// nhn, dtb result code
	sprintf(pBuffer, "X-dtb-result: %d\r\n", hRTSP->m_dwDtbResultCode);
	pBuffer += strlen(pBuffer);

	// Content-Type
	sprintf(pBuffer, "Content-Type: text/parameters\r\n");
	pBuffer += strlen(pBuffer);

	// Content
	sprintf(szContent, "current_viewing_time: %8.8ldT%6.6ld.%3.3ldZ\r\n", hRTSP->m_dwPlayFromDate,
						hRTSP->m_dwPlayFromTime/1000, hRTSP->m_dwPlayFromTime%1000);
	dwContent = strlen(szContent);

	// Content-Length
	sprintf(pBuffer, "Content-Length: %ld\r\n", dwContent);
	pBuffer += strlen(pBuffer);

	sprintf(pBuffer, "\r\n");
	pBuffer += strlen(pBuffer);

	sprintf(pBuffer, "%s", szContent);
}

void RTSPMakeErrorResponse(FrRTSPStruct* hRTSP, char* pBuffer, DWORD32 dwBuffLen)
{
	sprintf(pBuffer, "%s %ld %s\r\n", RTSP_VERSION, hRTSP->m_dwStatusCode,
								RTSPGetStingStatusCode(hRTSP->m_dwStatusCode));
	pBuffer += strlen(pBuffer);

	// CSeq
	sprintf(pBuffer, "CSeq: %d\r\n", hRTSP->m_iRecvCSeq);
	pBuffer += strlen(pBuffer);

	// Server
	//sprintf(pBuffer, "Server: Transcoder/1.0.0 (Win32)\r\n");
	sprintf(pBuffer, "Server: %s\r\n", hRTSP->m_szServerName);
	pBuffer += strlen(pBuffer);

	// nhn, dtb result code
	sprintf(pBuffer, "X-dtb-result: %d\r\n", hRTSP->m_dwDtbResultCode);
	pBuffer += strlen(pBuffer);

	sprintf(pBuffer, "\r\n");
}
