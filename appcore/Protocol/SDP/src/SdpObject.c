
#include "Sdp.h"

char* SDPMakeObjectFmtp(FrMediaStruct*	hMedia, char* pBuffer)
{
	char*	pConfig = NULL, *pTemp = NULL, *pTemp2 = NULL, *pSPS = NULL, *pPPS = NULL;
	DWORD32	dwConfig = 0, dwConfig2 = 0;

	if (hMedia->m_eObjectType == MPEG4V_OBJECT)
	{
		sprintf(pBuffer, "a=fmtp:%ld profile-level-id=1;framesize=%ld-%ld;config=",
					hMedia->m_dwPayload, hMedia->m_Info.m_Video.m_dwWidth, hMedia->m_Info.m_Video.m_dwHeight);
		pBuffer += strlen(pBuffer);

		pConfig = UtilStrConvBinToString(hMedia->m_pConfig, hMedia->m_dwConfig);
		sprintf(pBuffer, "%s", pConfig);
		pBuffer += strlen(pBuffer);

		sprintf(pBuffer, "\r\n");
	}
	else if (hMedia->m_eObjectType == H263_OBJECT)
	{
		sprintf(pBuffer, "a=fmtp:%ld profile=0;level=10\r\n", hMedia->m_dwPayload);
		pBuffer += strlen(pBuffer);
		sprintf(pBuffer, "a=framesize:%ld %ld-%ld\r\n", hMedia->m_dwPayload,
							hMedia->m_Info.m_Video.m_dwWidth, hMedia->m_Info.m_Video.m_dwHeight);
	}
	else if (hMedia->m_eObjectType == H264_OBJECT)
	{
		char	szProfile[7];
		McBitHandle		hBit;
		int				iNum = 0, iLen = 0, iSPSLen = 0, iPPSLen = 0, i;

		sprintf(&szProfile[0], "%x", (unsigned char)hMedia->m_nProfile >> 4);
        sprintf(&szProfile[1], "%x", (char)hMedia->m_nProfile & 0x0F);
        sprintf(&szProfile[2], "%x", (unsigned char)hMedia->m_nCompatibility >> 4);
        sprintf(&szProfile[3], "%x", (char)hMedia->m_nCompatibility & 0x0F);
        sprintf(&szProfile[4], "%x", (unsigned char)hMedia->m_nLevel >> 4);
        sprintf(&szProfile[5], "%x", (char)hMedia->m_nLevel & 0x0F);
		szProfile[6] = '\0';

		if (hMedia->m_b3GP)
		{
			sprintf(pBuffer, "a=fmtp:%ld profile-level-id=%s;packetization-mode=1;sprop-parameter-sets=", hMedia->m_dwPayload, szProfile);

			pBuffer += strlen(pBuffer);

			// parse SPS PPS
			pSPS = (char *)MALLOCZ(hMedia->m_dwConfig);
			pPPS = (char *)MALLOCZ(hMedia->m_dwConfig);
			pConfig = (char *)hMedia->m_pConfig;
			hBit = UtilBitOpen((BYTE *)pConfig, (int) hMedia->m_dwConfig);
			UtilBitByteAlign(hBit);
			/* SPS */
			iNum = UtilBitGetBit(hBit, 8);
			pConfig++;
			for (i = 0; i < iNum; i++)
			{
				iLen = UtilBitGetBit(hBit, 16);
				pConfig += 2;
				memcpy(pSPS+iSPSLen, pConfig, iLen);
				iSPSLen += iLen;
				pConfig += iLen;
				UtilBitFlush(hBit, 8*iLen);
			}
			/* PPS */
			iNum = UtilBitGetBit(hBit, 8);
			pConfig++;
			for (i = 0; i < iNum; i++)
			{
				iLen = UtilBitGetBit(hBit, 16);
				pConfig += 2;
				memcpy(pPPS+iPPSLen, pConfig, iLen);
				iPPSLen += iLen;
				pConfig += iLen;
			}

			// SPS/PPS Base64 Encoding
			pTemp = (char *)UtilBase64Encoding((BYTE *)pSPS, iSPSLen, (int *)&dwConfig);
			pTemp2 = (char *)UtilBase64Encoding((BYTE *)pPPS, iPPSLen, (int *)&dwConfig2);

			pConfig = (char *)MALLOCZ(dwConfig + dwConfig2 + 2);

			memcpy(pConfig, pTemp, dwConfig);
			pConfig[dwConfig] = ',';
			memcpy(pConfig + (dwConfig+1), pTemp2, dwConfig2);

			UtilBitClose(hBit);
			FREE(pSPS);
			FREE(pPPS);
			FREE(pTemp);
			FREE(pTemp2);
		}
		else
		{
			sprintf(pBuffer, "a=fmtp:%ld profile-level-id=%s;packetization-mode=1;parameter-sets=", hMedia->m_dwPayload, szProfile);

			pBuffer += strlen(pBuffer);

			pConfig = UtilStrConvBinToString(hMedia->m_pConfig, hMedia->m_dwConfig);
		}

		sprintf(pBuffer, "%s", pConfig);
		pBuffer += strlen(pBuffer);

		sprintf(pBuffer, "\r\n");
	}
	else if (hMedia->m_eObjectType == H265_OBJECT)
	{
		CHAR	*pszFormat = (CHAR*)MALLOCZ(5000);

		sprintf(pBuffer, "a=fmtp:%d ", hMedia->m_dwPayload );
		pBuffer += strlen(pBuffer);

		// need to link function..
		//MakeH265SDPFormat(pszFormat, 5000, hMedia->m_pConfig, hMedia->m_dwConfig);
		sprintf(pBuffer, "%s", pszFormat);
		pBuffer += strlen(pBuffer);
		FREE(pszFormat);

		sprintf(pBuffer, "\r\n");
	}
	else if (hMedia->m_eObjectType == AAC_LATM_OBJECT || hMedia->m_eObjectType == HEAAC_LATM_OBJECT ||
			hMedia->m_eObjectType == PSAAC_LATM_OBJECT)
	{
		sprintf(pBuffer, "a=fmtp:%ld profile-level-id=15;object=2;cpresent=0;config=", hMedia->m_dwPayload);
		pBuffer += strlen(pBuffer);

		pConfig = UtilStrConvBinToString(hMedia->m_pConfig, hMedia->m_dwConfig);
		sprintf(pBuffer, "%s", pConfig);
		pBuffer += strlen(pBuffer);

		if (hMedia->m_eObjectType == HEAAC_LATM_OBJECT)
		{
			sprintf(pBuffer, ";SBR-enabled=1");
			pBuffer += strlen(pBuffer);
		}
		else if (hMedia->m_eObjectType == PSAAC_LATM_OBJECT)
		{
			sprintf(pBuffer, ";SBR-enabled=1;PS-enabled=1");
			pBuffer += strlen(pBuffer);
		}

		sprintf(pBuffer, "\r\n");
	}
	else if (hMedia->m_eObjectType == AAC_HBR_GENERIC_OBJECT)
	{
		sprintf(pBuffer, "a=fmtp:%ld profile-level-id=1;mode=AAC-hbr;sizeLength=13;indexLength=3;indexDeltalength=3;config=", hMedia->m_dwPayload);
		//sprintf(pBuffer, "a=fmtp:%ld profile-level-id=15;mode=AAC-lbr;config=", hMedia->m_dwPayload);
		pBuffer += strlen(pBuffer);

		pConfig = UtilStrConvBinToString(hMedia->m_pConfig, hMedia->m_dwConfig);
		sprintf(pBuffer, "%s", pConfig);
		pBuffer += strlen(pBuffer);

		sprintf(pBuffer, "\r\n");
	}
	else if (hMedia->m_eObjectType == AMR_OBJECT)
	{
		sprintf(pBuffer, "a=fmtp:%lu octet-align=1\r\n", hMedia->m_dwPayload);
	}
	else if (hMedia->m_eObjectType == AMR_WB_OBJECT)
	{
		sprintf(pBuffer, "a=fmtp:%lu octet-align=1\r\n", hMedia->m_dwPayload);
	}
	else if (hMedia->m_eObjectType == EVRC_OBJECT)
	{
		sprintf(pBuffer, "a=fmtp:%lu maxinterleave=0\r\n", hMedia->m_dwPayload);
	}
	else if (hMedia->m_eObjectType == QCELP_OBJECT)
	{
		sprintf(pBuffer, "a=fmtp:%lu streamtype=5\r\n", hMedia->m_dwPayload);
	}
	else if (hMedia->m_eObjectType == TEXT_OBJECT)
	{
		sprintf(pBuffer, "a=fmtp:%lu %lu\r\n", hMedia->m_dwPayload, hMedia->m_dwPayload-1);
	}
	else if (hMedia->m_eObjectType == OPUS_OBJECT)
	{
		sprintf(pBuffer, "a=fmtp:%ld stereo=1; sprop-stereo=1\r\n", hMedia->m_dwPayload);
		pBuffer += strlen(pBuffer);
		sprintf(pBuffer, "a=ptime:3\r\n");
		pBuffer += strlen(pBuffer);
		sprintf(pBuffer, "a=maxptime:10\r\n");
	}

	pBuffer += strlen(pBuffer);

	if (pConfig)
		FREE(pConfig);

	return	pBuffer;
}

char* SDPGetObjectString(OBJECT_TYPE eObject)
{
	switch (eObject)
	{
	case MPEG4V_OBJECT :
		return	"MP4V-ES";
	case H263_OBJECT :
		return	"H263-2000";
	case H264_OBJECT :
		return	"H264";
	case H265_OBJECT :
		return	"H265";
	case SVC_OBJECT :
		return	"H.264-SVC";
	case AAC_LATM_OBJECT :
	case HEAAC_LATM_OBJECT :
	case PSAAC_LATM_OBJECT :
		return	"MP4A-LATM";
	case AMR_OBJECT :
		return	"AMR";
	case AMR_WB_OBJECT :
		return	"AMR-WB";
	case EVRC_OBJECT :
		return	"EVRC";
	case QCELP_OBJECT :
		//return	"G711A";		// modified by mwkang, 2007.03.02
		return "QCELP";
	case G711A_OBJECT :
		//return	"G711A";
		return "PCMA";              // medified by smlee, jjh(samsung camera), 2019.09.27
	case G711U_OBJECT :
		//return	"G711U";
		return "PCMU";              // medified by smlee, jjh(samsung camera), 2019.09.27
	case TEXT_OBJECT :
		return	"T140";
	case AAC_HBR_GENERIC_OBJECT :
		return "MPEG4-GENERIC";
	case OPUS_OBJECT:
		return "opus";
	default :
		break;
	}

	return	NULL;
}

OBJECT_TYPE SDPGetObjectType(char* pObject)
{
	OBJECT_TYPE	eObject;

	if (!_strnicmp(pObject, "MP4V-ES", strlen("MP4V-ES")))
		eObject = MPEG4V_OBJECT;
	else if (!_strnicmp(pObject, "H263-2000", strlen("H263-2000")))
		eObject = H263_OBJECT;
	else if (!_strnicmp(pObject, "H263-1998", strlen("H263-1998")))
		eObject = H263_OBJECT;
	else if (!_strnicmp(pObject, "H264", strlen("H264")))
		eObject = H264_OBJECT;
	else if (!_strnicmp(pObject, "H265", strlen("H265")))
		eObject = H265_OBJECT;
	else if (!_strnicmp(pObject, "H.264-SVC", strlen("H.264-SVC")))
		eObject = SVC_OBJECT;
	else if (!_strnicmp(pObject, "MP4A-LATM", strlen("MP4A-LATM")))
		eObject = AAC_LATM_OBJECT;
	else if (!_strnicmp(pObject, "opus", strlen("opus")))
		eObject = OPUS_OBJECT;
	else if (!_strnicmp(pObject, "AMR-WB", strlen("AMR-WB")))
		eObject = AMR_WB_OBJECT;
	else if (!_strnicmp(pObject, "AMR", strlen("AMR")))
		eObject = AMR_OBJECT;
	else if (!_strnicmp(pObject, "EVRC", strlen("EVRC")))
		eObject = EVRC_OBJECT;
	else if (!_strnicmp(pObject, "QCELP", strlen("QCELP")))
		eObject = QCELP_OBJECT;
	else if (!_strnicmp(pObject, "G711A", strlen("G711A")))
		eObject = G711A_OBJECT;
	else if (!_strnicmp(pObject, "G711U", strlen("G711U")))
		eObject = G711U_OBJECT;
	else if (!_strnicmp(pObject, "PCMU", strlen("PCMU")))
		eObject = G711U_OBJECT;
	else if (!_strnicmp(pObject, "T140", strlen("T140")))
		eObject = TEXT_OBJECT;
	else if (!_strnicmp(pObject, "RED", strlen("RED")))
		eObject = TEXT_OBJECT;
	else if (!_strnicmp(pObject, "MPEG4-GENERIC", strlen("MPEG4-GENERIC")))
		eObject = AAC_HBR_GENERIC_OBJECT;
	else if (!_strnicmp(pObject, "VND.ONVIF.METADATA", strlen("VND.ONVIF.METADATA")))
		eObject = ONVIF_META_OBJECT;
	else if (!_strnicmp(pObject, "EAS1", strlen("EAS1")))		// nhn, on air
		eObject = ACO_OBJECT;
	else if (!_strnicmp(pObject, "EVS1", strlen("EVS1")))		// nhn, on air
		eObject = VCO_OBJECT;
	else
		eObject	= UNKNOWN_OBJECT;

	return	eObject;
}
