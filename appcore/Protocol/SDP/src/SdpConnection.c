
#include "SocketAPI.h"
#include "UtilAPI.h"
#include "Sdp.h"

typedef enum {
	S_nettype,
	S_addrtype,
	S_connaddr,
	S_ttl,
	S_slash,
	S_numaddr,
	S_space
} STATE_TYPE;

BOOL SDPParseConnectionField(FrSDPStruct* hSDP, char* pBuffer, DWORD32 dwLen)
{
	char*		pStart = pBuffer;
	int			bEnd = FALSE;
	STATE_TYPE	state = S_nettype, next = S_nettype;

	// at this time, we don't support multiple c= fields
	while (*pBuffer++ != '\0' && !bEnd)
	{
		switch (state) 
		{
		case S_space:
		    if (!ISSPACE((int)*pBuffer) && *pBuffer) 
			{
				state = next;
				pStart = pBuffer;
				pBuffer--;
			}
			break;
		case S_nettype:
			if (!(ISALNUM(*pBuffer))) 
			{
				//hSDP->m_strConnNettype = SDPUtilStrdupn(pStart, pBuffer-pStart);
				state = S_space;
				next  = S_addrtype;
				pBuffer--;
			}
			break;
		case S_addrtype:
			if (!(ISALNUM(*pBuffer))) 
			{
				//hSDP->m_strConnAddrtype = SDPUtilStrdupn(pStart, pBuffer-pStart);
				state = S_space;
				next  = S_connaddr;
				pBuffer--;
			}
			break;
		case S_connaddr:
			if (!(ISALNUM(*pBuffer) || (*pBuffer =='-') || (*pBuffer == '.') )) 
			{
				//[TODO] check this address is valid
				// e.g. IP4 or IP6 dependant on addrtype
				char	*temp, *ptr;
				int		t;

				temp = UtilStrStrdupn(pStart,pBuffer-pStart);
				if (temp) {
					ptr = strchr(temp,'.');
					if (ptr)
					{
						*ptr = 0;
						t = atoi(temp);
					}
					else 
						t = 0;
				}

				//if (strcmp(temp,"0.0.0.0") && t >=224) // if ismulticast
				//	hSDP->m_bMulticast = TRUE;
				bEnd = TRUE;

				if (temp)
					FREE(temp);
				
				state = S_slash;
				next = S_ttl;
			}
			break;
		case S_slash:
			if (*pBuffer == '/')
			{
				pStart = pBuffer + 1;
				state = next;
			}
			break;
		case S_ttl:
			if (!ISDIGIT(*pBuffer))
			{
				*pBuffer = 0;
				//hSDP->m_iMulticastTTL = atoi((char*)pStart);
				state = S_slash;
				next  = S_numaddr;
			}
			break;
		case S_numaddr:
			if (!ISDIGIT(*pBuffer))
			{
				*pBuffer = 0;
				//hSDP->m_iMulticastNum = atoi((char*)pStart);
			}
			break;
		}
	}

	return TRUE;
}
