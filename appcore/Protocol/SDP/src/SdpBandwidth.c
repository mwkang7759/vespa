
#include "Sdp.h"

BOOL SDPParseBandwidthFields(FrSDPStruct* hSDP, char* pBuffer, DWORD32 dwLen)
{
	FrMediaStruct*	hMedia = NULL;
	char*			pLine;
	char*			pMod;
	DWORD32			dwMod, dwBandwidth;

	if (hSDP->m_dwMediaSize)
		hMedia = hSDP->m_aryMedia[hSDP->m_dwMediaSize-1];

	// modifier
	pLine = UtilStrGetString(pBuffer, &pMod, &dwMod);
	pMod = UtilStrGetUpperCaseLetter(pMod, dwMod);

	pLine = UtilStrFindString(pLine, ":");			// find a colon
	if (pLine == NULL)
		return	FALSE;
	pLine++;
	dwLen -= (pLine - pBuffer);

	// bandwidth-value
	if ( !strncmp(pBuffer, "AS", strlen("AS")))							// max bitrate
	{
		pBuffer = UtilStrGetFloatNumber(pLine, NULL, &dwBandwidth);
		if (!pBuffer)
			return	FALSE;

		if (hMedia)
			hMedia->m_dwMaxBitrate = dwBandwidth;
		else
			hSDP->m_dwMaxBitrate = dwBandwidth;
	}
	else if ( !strncmp(pBuffer,"RS", strlen("RS")))
	{
		pBuffer = UtilStrGetFloatNumber(pLine, NULL, &dwBandwidth);
		if (!pBuffer)
			return	FALSE;
	}
	else if ( !strncmp(pBuffer,"RR", strlen("RR")))
	{
		pBuffer = UtilStrGetFloatNumber(pLine, NULL, &dwBandwidth);
		if (!pBuffer)
			return	FALSE;
	}
	else if ( !strncmp(pBuffer,"X-AB", strlen("X-AB")))					// average bitrate
	{
		pBuffer = UtilStrGetFloatNumber(pLine, NULL, &dwBandwidth);
		if (!pBuffer)
			return	FALSE;

		if (hMedia)
			hMedia->m_dwAvgBitrate = dwBandwidth;
		else
			hSDP->m_dwAvgBitrate = dwBandwidth;
	}

	return	TRUE;
}

DWORD32 SDPGetBitrate(FrMediaStruct* hMedia)
{
	DWORD32	dwAvgBitrate = 0;

	switch (hMedia->m_eObjectType)
	{
	case MPEG4V_OBJECT:
	case H263_OBJECT:
	case H264_OBJECT:
	case SVC_OBJECT:
		dwAvgBitrate = 300000;				// 300 kbps
		break;
	case H265_OBJECT:
		break;
	case AAC_LATM_OBJECT:
	case HEAAC_LATM_OBJECT:
	case PSAAC_LATM_OBJECT:
		dwAvgBitrate = 128000;				// 128 kbps
		break;
	case AMR_WB_OBJECT:
	case AMR_OBJECT:
	case EVRC_OBJECT:
	case QCELP_OBJECT:
		dwAvgBitrate = 20000;				// 20 kbps
		break;
	case G711A_OBJECT :
	case G711U_OBJECT :
		dwAvgBitrate = hMedia->m_dwSampleRate*hMedia->m_Info.m_Audio.m_dwChannelNum*8;
		break;
	case TEXT_OBJECT:
		dwAvgBitrate = 10000;				// 10 kbps
		break;
	case OPUS_OBJECT:
		dwAvgBitrate = 200000;				// 200 kbps
		break;
	case UNKNOWN_OBJECT:
		dwAvgBitrate = 0;
		break;
	}

	return	dwAvgBitrate;
}
