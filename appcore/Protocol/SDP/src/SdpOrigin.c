
#include "SocketAPI.h"
#include "Sdp.h"

typedef enum {
	S_username,
	S_space,
	S_sessid,
	S_sessversion, 
	S_nettype,
	S_addrtype,
	S_addr
} STATE_TYPE;

BOOL SDPParseOriginField(FrSDPStruct* hSDP, char* pBuffer, DWORD32 dwLen)
{
	char*		pStart = pBuffer;
	char		cTemp;
	BOOL		bEnd = FALSE;
	STATE_TYPE	state = S_username, next = S_username;

	while (*pBuffer++ != '\0' && !bEnd)
	{
		switch(state) 
		{
		case S_space:
		    if (!ISSPACE((int)*pBuffer) && *pBuffer) 
			{
				state = next;
				pStart = pBuffer;
				pBuffer--;
			}
			break;
		case S_username:
			if (!(ISSAFE(*pBuffer)))
			{
				//hSDP->m_strUsername = SDPUtilStrdupn(pStart, pBuffer-pStart);
				state = S_space;
				next  = S_sessid;
				pBuffer--;
			}
			break;
		case S_sessid:
			if (!(ISDIGIT(*pBuffer))) 
			{
				cTemp = *pBuffer;
				*pBuffer = (char)0;
				//hSDP->m_iSessId = atoi(pStart);
				state = S_space;
				next  = S_sessversion;
				*pBuffer = cTemp;
				pBuffer--;
			}
			break;
		case S_sessversion:
			if (!(ISDIGIT(*pBuffer))) 
			{
				cTemp = *pBuffer;
				*pBuffer = (char)0;
				//hSDP->m_iSessVersion = atoi(pStart);
				state = S_space;
				next  = S_nettype;
				*pBuffer = cTemp;
				pBuffer--;
			}
			break;
		case S_nettype:
			if (!(ISALNUM(*pBuffer))) 
			{
				//hSDP->m_strNettype = SDPUtilStrdupn(pStart, pBuffer-pStart);
				state = S_space;
				next  = S_addrtype;
				pBuffer--;
			}
			break;
		case S_addrtype:
			if (!(ISALNUM(*pBuffer))) 
			{
				//hSDP->m_strAddrtype = SDPUtilStrdupn(pStart, pBuffer-pStart);
				state = S_space;
				next  = S_addr;
				pBuffer--;
			}
			break;
		case S_addr:
			if (!(ISALNUM(*pBuffer) || (*pBuffer =='-') || (*pBuffer == '.') )) 
			{
				//[TODO] check this address is valid
				// e.g. IP4 or IP6 dependant on addrtype
				//hSDP->m_strAddr = SDPUtilStrdupn(pStart,pBuffer-pStart);
				bEnd = TRUE;
				pBuffer--;
			}
			break;
		}
	}

	return	TRUE;
}
