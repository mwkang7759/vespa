
#include "UtilAPI.h"
#include "Sdp.h"

LRSLT SDPMakingSDP(FrSDPStruct* hSDP, char* pBuffer, DWORD32 dwLen)
{
	FrMediaStruct*	hMedia;
	DWORD32			i;

	// Session description
	sprintf(pBuffer, "v=0\r\n");
	pBuffer += strlen(pBuffer);

	//sprintf(pBuffer, "o=- %s %s IN IP4 %s\r\n", hSDP->m_pSession, hSDP->m_pVersion, hSDP->m_pLocalAddr);
	sprintf(pBuffer, "o=- 0 0 IN IP4 %s\r\n", hSDP->m_pLocalAddr);
	pBuffer += strlen(pBuffer);

	sprintf(pBuffer, "s=RTSP Session\r\n");
	pBuffer += strlen(pBuffer);

	// add to pay per use of show message for system error, mwkang 2008.08.20
	if(hSDP->m_bCharge)
		sprintf(pBuffer, "i=charge_nonfree\r\n");
	else
		sprintf(pBuffer, "i=charge_free\r\n");
	pBuffer += strlen(pBuffer);

	sprintf(pBuffer, "u=http:///\r\n");
	pBuffer += strlen(pBuffer);

	sprintf(pBuffer, "e=admin@\r\n");
	pBuffer += strlen(pBuffer);

	if (hSDP->m_bMultiCast)
	{
		//if (hSDP->m_iMulticastNum > 0 )
		//	sprintf(pBuffer, "c=IN IP4 %s/%d/%d\r\n",hSDP->GetRemoteHostIP(), m_iMulticastTTL, m_iMulticastNum);
		//else
		//	sprintf(pBuffer, "c=IN IP4 %s/%d\r\n",hSDP->GetRemoteHostIP(), m_iMulticastTTL);
	}
	else
	{
		sprintf(pBuffer, "c=IN IP4 %s\r\n", hSDP->m_pLocalAddr);
	}
	pBuffer += strlen(pBuffer);

	// Time description
	sprintf(pBuffer, "t=0 0\r\n");
	pBuffer += strlen(pBuffer);

	// add token..
	if (hSDP->m_pAuthToken) {
		sprintf(pBuffer, "a=authToken:%s\r\n", hSDP->m_pAuthToken);
		pBuffer += strlen(pBuffer);
	}

	sprintf(pBuffer, "a=control:*\r\n");
	pBuffer += strlen(pBuffer);

	// range
	if (hSDP->m_bLive)
		sprintf(pBuffer, "a=range:npt=now-\r\n");
	else
		sprintf(pBuffer, "a=range:npt=0-%.3f\r\n", (float)hSDP->m_dwRange/1000.);
	pBuffer += strlen(pBuffer);

	// Media description
	for (i = 0; i < hSDP->m_dwMediaSize; i++)
	{
		hMedia = hSDP->m_aryMedia[i];

		// media
		if (hMedia->m_eMedia == MI_AUDIO)
			sprintf(pBuffer,"m=audio 0 RTP/AVP");
		else if (hMedia->m_eMedia == MI_VIDEO)
			sprintf(pBuffer,"m=video 0 RTP/AVP");
		else if (hMedia->m_eMedia == MI_TEXT)
			sprintf(pBuffer,"m=text 0 RTP/AVP");
		else if (hMedia->m_eMedia == MI_APPLICATION)
			sprintf(pBuffer,"m=application 0 RTP/AVP");
		else if (hMedia->m_eMedia == MI_DATA)
			sprintf(pBuffer,"m=data 0 RTP/AVP");
		else if (hMedia->m_eMedia == MI_CONTROL)
			sprintf(pBuffer,"m=control 0 RTP/AVP");
		pBuffer += strlen(pBuffer);

		sprintf(pBuffer," %lu", hMedia->m_dwPayload);
		pBuffer += strlen(pBuffer);

		sprintf(pBuffer,"\r\n");
		pBuffer += strlen(pBuffer);

		// media range
		//if (!hSDP->m_bLive)
		//{
		//	sprintf(pBuffer, "a=range:npt=0-%.3f\r\n", (float)hMedia->m_dwRange/1000.);
		//	pBuffer += strlen(pBuffer);
		//}

		// media control
		sprintf(pBuffer,"a=control:%s\r\n", hMedia->m_pControl);
		pBuffer += strlen(pBuffer);

		// rtpmap
		if (hMedia->m_eMedia == MI_AUDIO)
			sprintf(pBuffer,"a=rtpmap:%lu %s/%ld/%ld\r\n", hMedia->m_dwPayload, SDPGetObjectString(hMedia->m_eObjectType),
					hMedia->m_dwSampleRate, hMedia->m_Info.m_Audio.m_dwChannelNum);
		else
		{
			sprintf(pBuffer,"a=rtpmap:%lu %s/%ld\r\n", hMedia->m_dwPayload, SDPGetObjectString(hMedia->m_eObjectType),
					hMedia->m_dwSampleRate);
			if (hMedia->m_eObjectType == TEXT_OBJECT)
			{
				pBuffer += strlen(pBuffer);
				sprintf(pBuffer,"a=rtpmap:%lu red/%ld\r\n", hMedia->m_dwPayload-1, hMedia->m_dwSampleRate);
			}
		}
		pBuffer += strlen(pBuffer);

		// fmtp
		if (hSDP->m_b3GP)
			hMedia->m_b3GP = TRUE;
		pBuffer = SDPMakeObjectFmtp(hMedia, pBuffer);
		pBuffer += strlen(pBuffer);

        if (hMedia->m_eMedia == MI_VIDEO) {
            if(hSDP->m_nInitSeekDiffTime != 0) {
                sprintf(pBuffer, "a=initSeekDiff:%d\r\n", hSDP->m_nInitSeekDiffTime);
		        pBuffer += strlen(pBuffer);
            }
        }

		// bitrate
		sprintf(pBuffer,"b=AS:%lu\r\n", (DWORD32)(hMedia->m_dwAvgBitrate/1000));
		pBuffer += strlen(pBuffer);

		if (hSDP->m_bDelSKInfo == FALSE)
		{
			sprintf(pBuffer,"b=X-AB:%lu\r\n", (DWORD32)(hMedia->m_dwAvgBitrate/1000));
			pBuffer += strlen(pBuffer);

			if (!hMedia->m_dwBuffSize)
				hMedia->m_dwBuffSize = hMedia->m_dwAvgBitrate * 6 / 8;

			sprintf(pBuffer,"a=X-IBUFsize:%lu\r\n", hMedia->m_dwBuffSize);
			pBuffer += strlen(pBuffer);
		}

		sprintf(pBuffer,"\r\n");
	}
	*pBuffer = '\0';

	return	FR_OK;
}

LRSLT SDPParsingSDP(FrSDPStruct* hSDP, char* pBuffer, DWORD32 dwLen)
{
	char*	pLine = pBuffer;
	DWORD32	dwLineLen;
	BOOL	bRet = TRUE;

	while (dwLen)
	{
		// 1. Find 1 Line
		pBuffer = UtilStrGetLine(pLine, &dwLineLen);

		if (ISCRLF(*pLine))
			break;					// blank-line

		dwLen -= dwLineLen;

		// 2. Parse SDP pLine
		if (pLine[1] != '=' || (!(*pLine =='s' || *pLine == 'i') && ISSPACE(pLine[2])))
		{								// SDP pLine : '<type>=<value>' with no whitespace
			return	COMMON_ERR_INVALID_PROTOCOL;
		}

		switch (*pLine)					// <type>
		{
		case 'v':						// Proto-version
		 	break;
		case 'o':						// origin-field
			bRet = SDPParseOriginField(hSDP, pLine+2, dwLineLen-2);
			break;
		case 's':						// session-name-field
			//hSDP->m_strSessionName = SDPUtilStrdupn(pLine+2, dwLineLen-2);
			bRet = SDPParseSessionField(hSDP, pLine+2, dwLineLen-2);
			break;
		case 'i':						// information-field
			//hSDP->m_strInformation = SDPUtilStrdupn(pLine+2, dwLineLen-2);
			bRet = SDPParseInfoField(hSDP, pLine+2, dwLineLen-2);
			break;
		case 'u':						// uri-field
			//hSDP->m_strUri = SDPUtilStrdupn(pLine+2, dwLineLen-2);
			break;
		case 'e':						// email-fields
			// [Todo] multi email list
			//hSDP->m_strEmail = SDPUtilStrdupn(pLine+2, dwLineLen-2);
			break;
		case 'p': // phone-fields
			// [Todo] multi email list
			//hSDP->m_strPhone = SDPUtilStrdupn(pLine+2, dwLineLen-2);
			break;
		case 'c':						// connection-field
			bRet = SDPParseConnectionField(hSDP, pLine+2, dwLineLen-2);
			break;
		case 'b':						// bandwidth-fields
			bRet = SDPParseBandwidthFields(hSDP, pLine+2, dwLineLen-2);
			break;
		case 't':						// time-fields
			// One or more time descriptions (2327.6)
			SDPParseTimeFields(hSDP, pLine+2, dwLineLen-2);
			break;
		case 'r':						// repeat-field in time-fields
//			if (hSDP->m_bExistTimeField)
//			{
//				SDPParseRepeatFields(hSDP, pLine+2,dwLineLen-2);
//				hSDP->m_bExistRepeatField = TRUE;
//			}
			break;
		case 'z':						// zone-field in time-fields
	//		if (hSDP->m_bExistRepeatField)
	//			ParseZoneFields(pLine+2,dwLineLen-2);
			break;
		case 'k':						// key-field
			//hSDP->m_strKeyField = SDPUtilStrdupn(pLine+2, dwLineLen-2);
			break;
		case 'a':						// attribute-fields
			bRet = SDPParseAttributeFields(hSDP, pLine+2, dwLineLen-2);
			break;
		case 'm':						// m_eMedia-field
			bRet = SDPParseMediaField(hSDP, pLine+2, dwLineLen-2);
			break;
		default :
			return	FALSE;
		}

		if (!bRet)
			return	COMMON_ERR_INVALID_PROTOCOL;

		pLine = pBuffer;
	}

#if 1		// REMOVE_AUDIO_PCM
	if (hSDP->m_dwMediaSize)
	{
		DWORD32				i, j;
		FrMediaStruct		*hMedia;

		hSDP->m_dwMediaSizeOrg = hSDP->m_dwMediaSize;

		for(i=0; i<hSDP->m_dwMediaSize; i++)
		{
			hMedia = hSDP->m_aryMedia[i];

			if (hMedia->m_eMedia == MI_AUDIO &&
				(hMedia->m_eObjectType == G711A_OBJECT ||
				 hMedia->m_eObjectType == G711U_OBJECT) )
			{
				TRACE("[SDP] SDPParsiongSDP - Meet Audio PCM channel which will be disabled");
				for(j=i; j<hSDP->m_dwMediaSize-1; j++)
					hSDP->m_aryMedia[j] = hSDP->m_aryMedia[j+1];
				hSDP->m_aryMedia[j] = hMedia;
				hMedia->m_bDisabled = TRUE;
				i--;
				hSDP->m_dwMediaSize--;
			}
		}
	}
#endif

	return	FR_OK;
}

LRSLT SDPCheckSDP(FrSDPStruct* hSDP)
{
	FrMediaStruct	*hMedia;
	DWORD32			i;

	for (i = 0;i < hSDP->m_dwMediaSize;i++)
	{
		hMedia = hSDP->m_aryMedia[i];
		switch (hMedia->m_eMedia)
		{
		case MI_AUDIO :
			LOG_I("[SDP]	SDPCheckSDP - AUDIO MEDIA");
			break;
		case MI_VIDEO :
			LOG_I("[SDP]	SDPCheckSDP - VIDEO MEDIA");
			break;
		case MI_TEXT :
			LOG_I("[SDP]	SDPCheckSDP - TEXT MEDIA");
			break;
		case MI_APPLICATION :
			LOG_I("[SDP]	SDPCheckSDP - APPLICATION MEDIA");
			break;
		case MI_DATA :
			LOG_I("[SDP]	SDPCheckSDP - DATA MEDIA");
			break;
		case MI_CONTROL :
			LOG_I("[SDP]	SDPCheckSDP - CONTROL MEDIA");
			break;
		default :
			LOG_I("[SDP]	SDPCheckSDP - Unsupport MEDIA");
			return	COMMON_ERR_INVALID_PROTOCOL;
		}

		// check Range
		if (!hMedia->m_dwRange)
		{
			hMedia->m_dwRange = hSDP->m_dwRange;
		}
		else
		{
			if (hSDP->m_dwRange == INFINITE)
				hSDP->m_dwRange = hMedia->m_dwRange;
			else
			{
				if (hSDP->m_dwRange < hMedia->m_dwRange)
					hSDP->m_dwRange = hMedia->m_dwRange;
			}
		}
		LOG_I("[SDP]	SDPCheckSDP - Range=%d", hMedia->m_dwRange);

		// check control
		if (!hMedia->m_pControl)
		{
			LOG_E("[SDP]	SDPCheckSDP - No control");
			return	COMMON_ERR_INVALID_PROTOCOL;
		}
		LOG_I("[SDP]	SDPCheckSDP - Control=%s", hMedia->m_pControl);

		// check bitrate
		if (!hMedia->m_dwAvgBitrate)
		{
			if (hSDP->m_dwAvgBitrate)
				hMedia->m_dwAvgBitrate = hSDP->m_dwAvgBitrate;
			else if (hMedia->m_dwMaxBitrate)
				hMedia->m_dwAvgBitrate = hMedia->m_dwMaxBitrate;
			else if (hSDP->m_dwMaxBitrate)
				hMedia->m_dwAvgBitrate = hSDP->m_dwMaxBitrate;
			else
				hMedia->m_dwAvgBitrate = SDPGetBitrate(hMedia);
		}
		LOG_I("[SDP]	SDPCheckSDP - AvgBitrate=%d", hMedia->m_dwAvgBitrate);
	}

	if (i == 0)
		return	COMMON_ERR_INVALID_PROTOCOL;

	return	FR_OK;
}
