
#ifndef	_SDP_H
#define	_SDP_H

#include "SystemAPI.h"
#include "TraceAPI.h"
#include "mediainfo.h"
#include "mediaerror.h"
#include "mediafourcc.h"
#include "UtilAPI.h"
#include "SdpAPI.h"
#include "StreamUtilAPI.h"

#define	MAX_OBJECT			5
#define	MAX_MEDIA_RANGE		(3600000 * 4)
#define	MAX_CONTROL_LEN		32
#define	INIT_TRACK_ID		1

typedef enum {
	SDP_RECV_ONLY = 0,
	SDP_RECV_SEND,
	SDP_SEND_ONLY
} eSendRecvMode;

typedef struct
{
	MEDIA_TYPE2		m_eMedia;
	BOOL			m_bDisabled;
	DWORD32			m_dwRange;
	char*			m_pControl;
	DWORD32			m_dwMaxBitrate;
	DWORD32			m_dwAvgBitrate;
	DWORD32			m_dwBuffSize;

	OBJECT_TYPE		m_eObjectType;
	DWORD32			m_dwAudioObjectType;

	DWORD32			m_dwPayload;
	DWORD32			m_dwSampleRate;
	BYTE			m_pConfig[MAX_CONFIG_SIZE];
	DWORD32			m_dwConfig;
	BOOL			m_bH264ParamSet;

	int				m_nProfile;
	int				m_nLevel;
	int				m_nCompatibility;

	// H265
	union	{
		hvcCStruct	m_H265Sdp;
	} m_SdpInfo;

	BOOL			m_b3GP;	// for SDP set for 3GP not SKT

	union	{
		struct {
			DWORD32	m_dwWidth;
			DWORD32	m_dwHeight;
			DWORD32	m_dwFrameRate;
		} m_Video;

		struct {
			DWORD32	m_dwChannelNum;
			int		m_Mode;
			int		m_iSizeLen;
			int		m_iIndexLen;
			int		m_iIndexDeltaLen;
		} m_Audio;
	} m_Info;

	eSendRecvMode	m_eRecvSend;
} FrMediaStruct;

typedef struct
{
	char*			m_pSession;
	char*			m_pVersion;
	char*			m_pLocalAddr;
	char*			m_pContactURL;
	char*			m_pContactEmail;
	char*			m_pSessionFieldName;
	char*			m_pInfoFieldName;
    char*           m_pAuthToken;
	BOOL			m_bMultiCast;
	BOOL			m_bLive;
	BOOL			m_bCharge;			// flag to pay per use
	BOOL			m_bDelSKInfo;		// flag to delete SK Info
	BOOL			m_b3GP;				// flag to set SDP for 3GP

	FrMediaStruct**	m_aryMedia;
	DWORD32			m_dwMediaSize;
	DWORD32			m_dwMediaSizeOrg;	//

	char*			m_pControl;
	DWORD32			m_dwRange;
	DWORD32			m_dwMaxBitrate;
	DWORD32			m_dwAvgBitrate;

	// srt
	WORD			m_wSRTPort;

    INT32           m_nInitSeekDiffTime;     // List Play, Seek CTS - List StartTime

} FrSDPStruct;

LRSLT SDPMakingSDP(FrSDPStruct* hSDP, char* pBuffer, DWORD32 dwLen);
LRSLT SDPParsingSDP(FrSDPStruct* hSDP, char* pBuffer, DWORD32 dwLen);
LRSLT SDPCheckSDP(FrSDPStruct* hSDP);
DWORD32 SDPGetBitrate(FrMediaStruct* hMedia);

// media
FrMediaStruct* SDPNewMedia(FrSDPStruct* hSDP);

// object
char* SDPMakeObjectFmtp(FrMediaStruct*	hMedia, char* pBuffer);
char* SDPGetObjectString(OBJECT_TYPE eObject);
OBJECT_TYPE SDPGetObjectType(char* pObject);

// field
BOOL SDPParseOriginField(FrSDPStruct* hSDP, char* pBuffer, DWORD32 dwLen);
BOOL SDPParseConnectionField(FrSDPStruct* hSDP, char* pBuffer, DWORD32 dwLen);
BOOL SDPParseBandwidthFields(FrSDPStruct* hSDP, char* pBuffer, DWORD32 dwLen);
BOOL SDPParseTimeFields(FrSDPStruct* hSDP, char* pBuffer, DWORD32 dwLen);
BOOL SDPParseRepeatFields(FrSDPStruct* hSDP, char* pBuffer, DWORD32 dwLen);
BOOL SDPParseKeyField(FrSDPStruct* hSDP, char* pBuffer, DWORD32 dwLen);
BOOL SDPParseAttributeFields(FrSDPStruct* hSDP, char* pBuffer, DWORD32 dwLen);
BOOL SDPParseMediaField(FrSDPStruct* hSDP, char* pBuffer, DWORD32 dwLen);
BOOL SDPParseKeyMgmt(FrSDPStruct* hSDP, FrMediaStruct* hMedia, char* pBuffer);
BOOL SDPParseAccessToken(FrSDPStruct* hSDP, FrMediaStruct* hMedia, char* pBuffer);
BOOL SDPParseSessionField(FrSDPStruct* hSDP, char* pBuffer, DWORD32 dwLen);
BOOL SDPParseInfoField(FrSDPStruct* hSDP, char* pBuffer, DWORD32 dwLen);
BOOL SDPParseInitSeekDiff(FrSDPStruct* hSDP, FrMediaStruct* hMedia, char* pBuffer);

#endif // _SDP_H
