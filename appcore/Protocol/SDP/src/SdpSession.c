
#include "UtilAPI.h"
#include "Sdp.h"

BOOL SDPParseSessionField(FrSDPStruct* hSDP, char* pBuffer, DWORD32 dwLen)
{
	char*		pStart = pBuffer;
	int			bEnd = FALSE;
	
	// at this time, we don't support multiple c= fields
	while (*pBuffer++ != '\0' && !bEnd)
	{
		if (ISCRLF(*pBuffer))
		{
			bEnd = TRUE;
		}
	}
	hSDP->m_pSessionFieldName = UtilStrStrdupn(pStart, pBuffer-pStart);

	return TRUE;
}

BOOL SDPParseInfoField(FrSDPStruct* hSDP, char* pBuffer, DWORD32 dwLen)
{
	char*		pStart = pBuffer;
	int			bEnd = FALSE;
	
	// at this time, we don't support multiple c= fields
	while (*pBuffer++ != '\0' && !bEnd)
	{
		if (ISCRLF(*pBuffer))
		{
			bEnd = TRUE;
		}
	}
	hSDP->m_pInfoFieldName = UtilStrStrdupn(pStart, pBuffer-pStart);

	return TRUE;
}
