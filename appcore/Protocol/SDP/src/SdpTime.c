
#include "Sdp.h"

typedef	enum
{
	S_starttime,
	S_stoptime,
	S_space
} STATE_TYPE;

BOOL SDPParseTimeFields(FrSDPStruct* hSDP, char* pBuffer, DWORD32 dwLen)
{
	char*		pStart = pBuffer;
	char		cTemp;
	BOOL		bEnd = 0;
	STATE_TYPE	state = S_starttime, next = S_starttime;

	while (*pBuffer++ != '\0' && !bEnd)
	{
		switch(state) 
		{
		case S_space:
		    if (!ISSPACE((int)*pBuffer) && *pBuffer) 
			{
				state = next;
				pStart = pBuffer;
				pBuffer--;
			}
			break;
		case S_starttime:
			if (!ISDIGIT(*pBuffer)) 
			{
				cTemp = *pBuffer;
				*pBuffer = (char)0;
				ATOI((char*)pStart);			// StartTime
				state = S_space;
				next  = S_stoptime;
				*pBuffer = cTemp;
				pBuffer--;
			}
			break;
		case S_stoptime:
			if (!ISDIGIT(*pBuffer)) 
			{
				cTemp = *pBuffer;
				*pBuffer = (char)0;
				ATOI((char*)pStart);			// StopTime
				*pBuffer = cTemp;
				bEnd = 1;
			}
			break;
		}
	}

	return TRUE;
}
