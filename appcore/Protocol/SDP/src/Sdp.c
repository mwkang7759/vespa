
#include "Sdp.h"

LRSLT FrSDPOpen(FrSDPHandle* phSDP, char* pSDP, DWORD32 dwSDPLen, DWORD32 dwMedia)
{
	FrSDPStruct*	hSDP;
	FrMediaStruct*	hMedia;
	LRSLT			lRet;
	DWORD32			i;

	hSDP = (FrSDPStruct*)MALLOCZ(sizeof(FrSDPStruct));
	if (hSDP)
	{
		LOG_I("[SDP]	FrSDPOpen");
		hSDP->m_bLive = TRUE;	// default live
		hSDP->m_aryMedia = (FrMediaStruct**)MALLOCZ(sizeof(FrMediaStruct**)*MAX_TRACKS);
		if (!hSDP->m_aryMedia)
		{
			FREE(hSDP);
			return	COMMON_ERR_MEM;
		}

		if (pSDP)
		{
			lRet = SDPParsingSDP(hSDP, pSDP, dwSDPLen);
			if (FRFAILED(lRet))
			{
				FrSDPClose(hSDP);
				return	lRet;
			}

			lRet = SDPCheckSDP(hSDP);
			if (FRFAILED(lRet))
			{
				FrSDPClose(hSDP);
				return	lRet;
			}
		}
		else
		{
			for (i = 0;i < dwMedia;i++)
			{
				hMedia = SDPNewMedia(hSDP);
				hMedia->m_pControl = (char*)MALLOC(MAX_CONTROL_LEN);
				sprintf(hMedia->m_pControl, "trackID=%ld", i+INIT_TRACK_ID);
			}
		}

		*phSDP = hSDP;
	}
	else
		return	COMMON_ERR_MEM;

	return	FR_OK;
}

void FrSDPClose(FrSDPHandle hHandle)
{
	FrSDPStruct*	hSDP = (FrSDPStruct*)hHandle;
	DWORD32			i, j;

	if (hSDP)
	{
		for (i = 0;i < MAX_TRACKS;i++)
		{
			FrMediaStruct* hMedia = hSDP->m_aryMedia[i];

			if (hMedia)
			{
				FREE(hMedia->m_pControl);
				if (hMedia->m_eObjectType == H265_OBJECT)
				{
					if (hMedia->m_SdpInfo.m_H265Sdp.m_dwSpropVps)
					{
						for (j=0; j<hMedia->m_SdpInfo.m_H265Sdp.m_dwSpropVps; j++)
							FREE(hMedia->m_SdpInfo.m_H265Sdp.m_pVpsPara[j].m_pNal);
						FREE(hMedia->m_SdpInfo.m_H265Sdp.m_pVpsPara);
					}
					if (hMedia->m_SdpInfo.m_H265Sdp.m_dwSpropSps)
					{
						for (j=0; j<hMedia->m_SdpInfo.m_H265Sdp.m_dwSpropSps; j++)
							FREE(hMedia->m_SdpInfo.m_H265Sdp.m_pSpsPara[j].m_pNal);
						FREE(hMedia->m_SdpInfo.m_H265Sdp.m_pSpsPara);
					}
					if (hMedia->m_SdpInfo.m_H265Sdp.m_dwSpropPps)
					{
						for (j=0; j<hMedia->m_SdpInfo.m_H265Sdp.m_dwSpropPps; j++)
							FREE(hMedia->m_SdpInfo.m_H265Sdp.m_pPpsPara[j].m_pNal);
						FREE(hMedia->m_SdpInfo.m_H265Sdp.m_pPpsPara);
					}
				}
				FREE(hMedia);
			}
		}

		FREE(hSDP->m_pSession);
		FREE(hSDP->m_pVersion);
		FREE(hSDP->m_pLocalAddr);
		FREE(hSDP->m_pContactURL);
		FREE(hSDP->m_pContactEmail);
		FREE(hSDP->m_pControl);
		FREE(hSDP->m_aryMedia);
		FREE(hSDP->m_pSessionFieldName);
		FREE(hSDP->m_pInfoFieldName);
        FREE(hSDP->m_pAuthToken);
		FREE(hSDP);
		LOG_I("[SDP]	FrSDPClose");
	}
}

LRSLT FrSDPGetSDP(FrSDPHandle hHandle, char* pBuffer, DWORD32 dwLen)
{
	FrSDPStruct*	hSDP = (FrSDPStruct*)hHandle;
	LRSLT			lRet;

	LOG_I("[SDP]	FrSDPMakingSDP");

	lRet = SDPMakingSDP(hSDP, pBuffer, dwLen);
	if (FRFAILED(lRet))
		return	lRet;

	return	FR_OK;
}

/////////////////////////////////////////////////////////////
// SDP API
/////////////////////////////////////////////////////////////
char* FrSDPGetSDPControl(FrSDPHandle hHandle)
{
	FrSDPStruct*	hSDP = (FrSDPStruct*)hHandle;
	return	hSDP->m_pControl;
}

DWORD32 FrSDPGetSDPRange(FrSDPHandle hHandle)
{
	FrSDPStruct*	hSDP = (FrSDPStruct*)hHandle;
	return	hSDP->m_dwRange;
}

void FrSDPSetSDPRange(FrSDPHandle hHandle, DWORD32 dwRange)
{
	FrSDPStruct*	hSDP = (FrSDPStruct*)hHandle;

	if (!dwRange || dwRange == INFINITE)
	{
		hSDP->m_bLive = TRUE;
		hSDP->m_dwRange = INFINITE;
	}
	else {
		hSDP->m_dwRange = dwRange;
		hSDP->m_bLive = FALSE;
	}
}

DWORD32 FrSDPGetMediaNum(FrSDPHandle hHandle)
{
	FrSDPStruct*	hSDP = (FrSDPStruct*)hHandle;
	return	hSDP->m_dwMediaSize;
}

DWORD32 FrSDPGetMediaNumOrg(FrSDPHandle hHandle)
{
	FrSDPStruct*	hSDP = (FrSDPStruct*)hHandle;
	return	hSDP->m_dwMediaSizeOrg;
}

void FrSDPSetLocalAddr(FrSDPHandle hHandle, char* pAddr)
{
	FrSDPStruct*	hSDP = (FrSDPStruct*)hHandle;
	hSDP->m_pLocalAddr = UtilStrStrdup(pAddr);
}

void FrSDPSetCharge(FrSDPHandle hHandle, BOOL bCharge)
{
	FrSDPStruct*	hSDP = (FrSDPStruct*)hHandle;
	hSDP->m_bCharge = bCharge;
}

void FrSDPDelSKInfo(FrSDPHandle hHandle)
{
	FrSDPStruct*	hSDP = (FrSDPStruct*)hHandle;
	hSDP->m_bDelSKInfo = TRUE;
}

void FrSDPCheck3GP(FrSDPHandle hHandle)
{
	FrSDPStruct*	hSDP = (FrSDPStruct*)hHandle;
	hSDP->m_b3GP= TRUE;
}

/////////////////////////////////////////////////////////////
// Media API
/////////////////////////////////////////////////////////////
FrMediaHandle FrSDPGetMediaHandle(FrSDPHandle hHandle, DWORD32 dwIndex)
{
	FrSDPStruct*	hSDP = (FrSDPStruct*)hHandle;
	return	hSDP->m_aryMedia[dwIndex];
}

BOOL FrSDPGetMediaIsLive(FrSDPHandle hHandle)
{
	FrSDPStruct*	hSDP = (FrSDPStruct*)hHandle;
	return	hSDP->m_bLive;
}

char* FrSDPGetMediaControl(FrMediaHandle hHandle)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	return	hMedia->m_pControl;
}

MEDIA_TYPE2 FrSDPGetMediaType(FrMediaHandle hHandle)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	return	hMedia->m_eMedia;
}

BOOL FrSDPGetSendOnly(FrMediaHandle hHandle)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	return	hMedia->m_eRecvSend == SDP_SEND_ONLY;
}

void FrSDPSetMediaType(FrMediaHandle hHandle, MEDIA_TYPE2 tType)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	hMedia->m_eMedia = tType;
}

DWORD32 FrSDPGetMediaRange(FrMediaHandle hHandle)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	return	hMedia->m_dwRange;
}

void FrSDPSetMediaRange(FrMediaHandle hHandle, DWORD32 dwRange)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	hMedia->m_dwRange = dwRange;
}

DWORD32 FrSDPGetMediaAvgBitrate(FrMediaHandle hHandle)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	return	hMedia->m_dwAvgBitrate;
}

void FrSDPSetMediaAvgBitrate(FrMediaHandle hHandle, DWORD32 dwAvgBitrate)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	hMedia->m_dwAvgBitrate = dwAvgBitrate;
}

DWORD32 FrSDPGetMediaBuffSize(FrMediaHandle hHandle)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	return	hMedia->m_dwBuffSize;
}

void FrSDPSetMediaBuffSize(FrMediaHandle hHandle, DWORD32 dwBuffSize)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	hMedia->m_dwBuffSize = dwBuffSize;
}

OBJECT_TYPE FrSDPGetMediaObjectType(FrMediaHandle hHandle)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	return	hMedia->m_eObjectType;
}

DWORD32 FrSDPGetMediaPayloadType(FrMediaHandle hHandle)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	return	hMedia->m_dwPayload;
}

void FrSDPSetMediaPayloadType(FrMediaHandle hHandle, DWORD32 dwPayload)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	hMedia->m_dwPayload = dwPayload;
}

void FrSDPSetMediaObjectType(FrMediaHandle hHandle, OBJECT_TYPE eObjectType)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;

	hMedia->m_eObjectType = eObjectType;

	if (!hMedia->m_dwPayload)
	{
		switch (eObjectType)
		{
		case AAC_LATM_OBJECT :
		case HEAAC_LATM_OBJECT :
		case PSAAC_LATM_OBJECT :
			hMedia->m_dwPayload = 96;
			break;
		case AMR_OBJECT :
			hMedia->m_dwPayload = 106;
			break;
		case AMR_WB_OBJECT :
			hMedia->m_dwPayload = 107;
			break;
		case EVRC_OBJECT :
			hMedia->m_dwPayload = 97;
			break;
		case QCELP_OBJECT :
			hMedia->m_dwPayload = 105;
			break;
		case G711A_OBJECT :
			hMedia->m_dwPayload = 8;
			break;
		case G711U_OBJECT :
			hMedia->m_dwPayload = 0;
			break;
		case H264_OBJECT :
			hMedia->m_dwPayload = 102;
			break;
		case H265_OBJECT:
			hMedia->m_dwPayload = 103;
			break;
		case MPEG4V_OBJECT :
			hMedia->m_dwPayload = 100;
			break;
		case H263_OBJECT :
			hMedia->m_dwPayload = 34;
			break;
		case TEXT_OBJECT :
			hMedia->m_dwPayload = 126;
			break;
		case AAC_HBR_GENERIC_OBJECT :
			hMedia->m_dwPayload = 96;
			break;
		case OPUS_OBJECT:
			hMedia->m_dwPayload = 101;
			break;
		default :
			hMedia->m_dwPayload = 0;
			break;
		}
	}
}

DWORD32 FrSDPGetMediaSampleRate(FrMediaHandle hHandle)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	return	hMedia->m_dwSampleRate;
}

void FrSDPSetMediaSampleRate(FrMediaHandle hHandle, DWORD32 dwSampleRate)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;

	if (hMedia->m_eObjectType == HEAAC_LATM_OBJECT || hMedia->m_eObjectType == PSAAC_LATM_OBJECT)
		hMedia->m_dwSampleRate = dwSampleRate / 2;
	else
		hMedia->m_dwSampleRate = dwSampleRate;
}

BYTE* FrSDPGetMediaConfig(FrMediaHandle hHandle)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	return	hMedia->m_pConfig;
}

DWORD32 FrSDPGetMediaConfigLen(FrMediaHandle hHandle)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	return	hMedia->m_dwConfig;
}

BOOL FrSDPGetMediaH264ParamSet(FrMediaHandle hHandle)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	return	hMedia->m_bH264ParamSet;
}

void FrSDPSetMediaConfig(FrMediaHandle hHandle, BYTE* pConfig, DWORD32 dwConfigLen)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;

	memcpy(hMedia->m_pConfig, pConfig, dwConfigLen);
	hMedia->m_dwConfig = dwConfigLen;
}

void FrSDPSetMediaConfigLen(FrMediaHandle hHandle, DWORD32 dwConfigLen)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	hMedia->m_dwConfig = dwConfigLen;
}

void FrSDPSetMediaH264ParamSet(FrMediaHandle hHandle, BOOL bH264ParamSet)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	hMedia->m_bH264ParamSet = bH264ParamSet;
}

hvcCStruct* FrSDPGetH265hvcCInfo(FrMediaHandle hHandle)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	return	&hMedia->m_SdpInfo.m_H265Sdp;
}

int FrSDPGetMediaProfile(FrMediaHandle hHandle)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	return	hMedia->m_nProfile;
}

void FrSDPSetMediaProfile(FrMediaHandle hHandle, int nProfile)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	hMedia->m_nProfile = nProfile;
}

int FrSDPGetMediaLevel(FrMediaHandle hHandle)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	return	hMedia->m_nLevel;
}

void FrSDPSetMediaLevel(FrMediaHandle hHandle, int nLevel)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	hMedia->m_nLevel = nLevel;
}

int FrSDPGetMediaCompatibility(FrMediaHandle hHandle)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	return	hMedia->m_nCompatibility;
}

void FrSDPSetMediaCompatibility(FrMediaHandle hHandle, int nCompatibility)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	hMedia->m_nCompatibility = nCompatibility;
}

DWORD32 FrSDPGetMediaVideoWidth(FrMediaHandle hHandle)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	return	hMedia->m_Info.m_Video.m_dwWidth;
}

void FrSDPSetMediaVideoWidth(FrMediaHandle hHandle, DWORD32 dwWidth)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	hMedia->m_Info.m_Video.m_dwWidth = dwWidth;
}

DWORD32 FrSDPGetMediaVideoHeight(FrMediaHandle hHandle)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	return	hMedia->m_Info.m_Video.m_dwHeight;
}

void FrSDPSetMediaVideoHeight(FrMediaHandle hHandle, DWORD32 dwHeight)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	hMedia->m_Info.m_Video.m_dwHeight = dwHeight;
}

DWORD32 FrSDPGetMediaVideoFrameRate(FrMediaHandle hHandle)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	return	hMedia->m_Info.m_Video.m_dwFrameRate;
}

void FrSDPSetMediaVideoFrameRate(FrMediaHandle hHandle, DWORD32 dwFrameRate)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	hMedia->m_Info.m_Video.m_dwFrameRate = dwFrameRate;
}

DWORD32 FrSDPGetMediaAudioChannelNum(FrMediaHandle hHandle)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	return	hMedia->m_Info.m_Audio.m_dwChannelNum;
}

void FrSDPSetMediaAudioChannelNum(FrMediaHandle hHandle, DWORD32	dwChannelNum)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;

	if (hMedia->m_eObjectType == PSAAC_LATM_OBJECT)
		hMedia->m_Info.m_Audio.m_dwChannelNum = dwChannelNum / 2;
	else
		hMedia->m_Info.m_Audio.m_dwChannelNum = dwChannelNum;
}

void FrSDPGetMediaAACGenericInfo(FrMediaHandle hHandle, int* iMode, int* iSizeLen, int* iIndexLen, int* iIndexDeltaLen)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;

	*iMode = hMedia->m_Info.m_Audio.m_Mode;
	*iSizeLen = hMedia->m_Info.m_Audio.m_iSizeLen;
	*iIndexLen = hMedia->m_Info.m_Audio.m_iIndexLen;
	*iIndexDeltaLen = hMedia->m_Info.m_Audio.m_iIndexDeltaLen;
}

void FrSDPSetMediaAACGenericInfo(FrMediaHandle hHandle, int nMode, int nSizeLen, int nIndexLen, int nIndexDeltaLen)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;

	hMedia->m_Info.m_Audio.m_Mode = nMode;
	hMedia->m_Info.m_Audio.m_iSizeLen = nSizeLen;
	hMedia->m_Info.m_Audio.m_iIndexLen = nIndexLen;
	hMedia->m_Info.m_Audio.m_iIndexDeltaLen = nIndexDeltaLen;
}

DWORD32 FrSDPGetMediaAACObject(FrMediaHandle hHandle)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)hHandle;
	return	hMedia->m_dwAudioObjectType;
}

char* FrSDPGetSessionFieldName(FrSDPHandle hHandle)
{
	FrSDPStruct*	hSDP = (FrSDPStruct*)hHandle;
	return hSDP->m_pSessionFieldName;
}

char* FrSDPGetInfoFieldName(FrMediaHandle hHandle)
{
	FrSDPStruct*	hSDP = (FrSDPStruct*)hHandle;
	return hSDP->m_pInfoFieldName;
}

char* FrSDPGetAuthToken(FrSDPHandle hHandle)
{
	FrSDPStruct*	hSDP = (FrSDPStruct*)hHandle;
	return hSDP->m_pAuthToken;
}

void FrSDPSetAuthToken(FrSDPHandle hHandle, char* pToken)
{
	FrSDPStruct*	hSDP = (FrSDPStruct*)hHandle;
	if (!hSDP->m_pAuthToken) {
		hSDP->m_pAuthToken = (char*)MALLOCZ(strlen(pToken) + 1);
		strncpy(hSDP->m_pAuthToken, pToken, strlen(pToken));
	}
}

int FrSDPGetInitSeekDiffTime(FrSDPHandle hHandle)
{
    FrSDPStruct*    hSDP = (FrSDPStruct*)hHandle;
    if (hSDP) {
        return hSDP->m_nInitSeekDiffTime;
    }
    else
        return 0;
}

void FrSDPSetInitSeekDiffTime(FrSDPHandle hHandle, INT32 nDiffTime)
{
    FrSDPStruct*    hSDP = (FrSDPStruct*)hHandle;
    if (hSDP) {
        hSDP->m_nInitSeekDiffTime = nDiffTime;
    }
}

// Object && FourCC
OBJECT_TYPE ConvCodecToObject(DWORD32 dwFourCC) {
	OBJECT_TYPE	eObject = UNKNOWN_OBJECT;

	switch (dwFourCC) {
	case FOURCC_AVC1 :
	case FOURCC_H264 :
	case FOURCC_h264 :
		eObject = H264_OBJECT;
		break;
	case FOURCC_H265:
	case FOURCC_HEVC:
		eObject = H265_OBJECT;
		break;
	
	case FOURCC_AAC :
		eObject = AAC_LATM_OBJECT;
		break;
	
	default:
		eObject = UNKNOWN_OBJECT;
		break;
	}

	return	eObject;
}

DWORD32 ConvObjectToCodec(OBJECT_TYPE eObject)
{
	DWORD32	dwFourCC = 0;

	switch (eObject)
	{
	case MPEG4V_OBJECT:
		dwFourCC = FOURCC_MP4V;
		break;
	case H263_OBJECT:
		dwFourCC = FOURCC_H263;
		break;
	case H264_OBJECT:
		//dwFourCC = FOURCC_AVC1;
		dwFourCC = FOURCC_H264;
		break;
	case H265_OBJECT:
		//dwFourCC = FOURCC_AVC1;
		dwFourCC = FOURCC_H265;
		break;
	case SVC_OBJECT:
		dwFourCC = FOURCC_SVC1;
		break;
	case VCO_OBJECT:
		dwFourCC = FOURCC_VCO1;
		break;
	case AAC_LATM_OBJECT:
		dwFourCC = FOURCC_AAC;
		break;
	case HEAAC_LATM_OBJECT:
		dwFourCC = FOURCC_AAC_PLUS;
		break;
	case PSAAC_LATM_OBJECT:
		dwFourCC = FOURCC_AAC_PS;
		break;
	case AMR_WB_OBJECT:
		dwFourCC = FOURCC_AMR;
		break;
	case AMR_OBJECT:
		dwFourCC = FOURCC_AMR;
		break;
	case EVRC_OBJECT:
		dwFourCC = FOURCC_EVRC;
		break;
	case QCELP_OBJECT:
		dwFourCC = FOURCC_QCELP;
		break;
	case G711A_OBJECT:
		dwFourCC = FOURCC_G711A;
		break;
	case G711U_OBJECT:
		dwFourCC = FOURCC_G711U;
		break;
	case ACO_OBJECT:
		dwFourCC = FOURCC_ACO1;
		break;
	case TEXT_OBJECT:
		dwFourCC = FOURCC_T140;
		break;
	case AAC_HBR_GENERIC_OBJECT:
		dwFourCC = FOURCC_AAC;
		break;
	case OPUS_OBJECT:
		dwFourCC = FOURCC_OPUS;
		break;
	case UNKNOWN_OBJECT:
		break;
	}

	return	dwFourCC;
}
