
#include "UtilAPI.h"
#include "Sdp.h"

#if 0
BOOL SDPParseRFC2397DataURL(FrSDPStruct* hSDP, char* pBuffer, DWORD32 nLength)
{
	//data:[<mediatype>][;base64],<data>
	char *s = (char*)pBuffer, *data, *tmp;
	char *type = NULL, *subtype = NULL, *charset;
	BOOL bIsBase64 = FALSE;
	int datalen = 0;

	if ( UtilStrStrnicmp("data:",s,5) )
		return FALSE;

	s += 5;

	// find <data>
	if ( (data = strchr(s,',')) == NULL)
		return FALSE;

	*data = (char)0;
	data++;
	datalen = nLength - (data - s + 5);

	if (!(*s))
		goto analysis;

	// is base64?
	if ( (tmp = UtilStrFindString(s,";base64")) )
	{
		*tmp = (char)0;
		bIsBase64 = TRUE;
	}

	if (!(*s))
		goto analysis;

	// now mediatype is consist of
	// mediatype  := [ type "/" subtype ] *( ";" parameter )
	if ( (tmp = strchr(s,'/') ) != NULL)
	{
		// get type
		type = s;
		*tmp = (char)0;
		s= tmp +1;
	}

	if ( (tmp = strchr(s,';') ) != NULL)
	{
		// get subtype
		subtype = s;
		*tmp = (char)0;
		s= tmp + 1;
	}
	else
	{
		subtype = s;
		goto analysis;
	}

	if (!(*s))
		goto analysis;

	// parse parameters
	//parameter  := attribute "=" value

	if ( (tmp = UtilStrFindString(s,"charset=")) != NULL )
	{
		charset = tmp + 8;
		if ( ( tmp = strchr(s,';')) != NULL)
			*tmp = (char)0;
	}

analysis:
//[[TODO]]
	if (bIsBase64 && !SDPUtilStricmp(type, "application")
		&& !SDPUtilStricmp(subtype,"mpeg4-iod") )
	{
		//hSDP->m_strSessAttrMPEGIOD = Base64Decoding((BYTE*)data,datalen,
		//	&hSDP->m_nSessAttrMPEGIOD);
	}
	return TRUE;
}

#endif

typedef enum tagTransMode {
	CELP_CBR,
	CELP_VBR,
	AAC_LBR,
	AAC_HBR,
	TOTAL_MODE,
} TRANS_MODE;

TRANS_MODE UtilStrGetAACMode(char* pString)
{
	if (!UtilStrStrnicmp(pString, "CELP-cbr", strlen("CELP-cbr")))
		return	CELP_CBR;
	else if (!UtilStrStrnicmp(pString, "CELP-vbr", strlen("CELP-vbr")))
		return	CELP_VBR;
	else if (!UtilStrStrnicmp(pString, "AAC-lbr", strlen("AAC-lbr")))
		return	AAC_LBR;
	else if (!UtilStrStrnicmp(pString, "AAC-hbr", strlen("AAC-hbr")))
		return	AAC_HBR;
	else
		return	TOTAL_MODE;
}

BOOL SDPParseBuffSize(FrSDPStruct* hSDP, FrMediaStruct* hMedia, char* pBuffer)
{
	DWORD32	dwBuffSize;

	pBuffer = UtilStrGetDecNumber(pBuffer, NULL, &dwBuffSize);
	if (!pBuffer)
	{
		LOG_E("[SDP]	SDPParseBuffSize - Not buffsize");
		return	FALSE;
	}

	hMedia->m_dwBuffSize = dwBuffSize;

	return	TRUE;
}

BOOL SDPParseRtpMap(FrSDPStruct* hSDP, FrMediaStruct* hMedia, char* pBuffer)
{
	DWORD32			dwPayload;
	char*			pEncoding;
	DWORD32			dwEncoding;

	// payload
	pBuffer = UtilStrGetDecNumber(pBuffer, NULL, &dwPayload);
	if (!pBuffer || ISCRLF(*pBuffer))
	{
		LOG_E("[SDP]	SDPParseRtpMap - Not payload");
		return	FALSE;
	}

	// encoding name
	pBuffer = UtilStrGetString(pBuffer, &pEncoding, &dwEncoding);
	if (!pBuffer || ISCRLF(*pBuffer))
	{
		LOG_E("[SDP]	SDPParseRtpMap - Not encoding name");
		return	FALSE;
	}
	pEncoding = UtilStrGetUpperCaseLetter(pEncoding, dwEncoding);
	hMedia->m_eObjectType = SDPGetObjectType(pEncoding);

	//if (hMedia->m_eObjectType == UNKNOWN_OBJECT)
	//{
	//	TRACE("[SDP]	SDPParseRtpMap - Not object type");
	//	return	FALSE;
	//}

	// samplerate
	pBuffer = UtilStrGetDecNumber(pBuffer, NULL, &hMedia->m_dwSampleRate);
	if (!pBuffer)
	{
		LOG_E("[SDP]	SDPParseRtpMap - Not samplerate");
		return	FALSE;
	}

	// channel number
	if (hMedia->m_eMedia == MI_AUDIO)
	{
		if (ISCRLF(*pBuffer))
		{
			hMedia->m_Info.m_Audio.m_dwChannelNum = 1;
		}
		else
		{
			/*
			pBuffer = UtilStrGetDecNumber(pBuffer, NULL, &hMedia->m_Info.m_Audio.m_dwChannelNum);
			if (!pBuffer)
			{
				LOG_E("[SDP]	SDPParseRtpMap - Not channel number");
				return	FALSE;
			}
			*/
			char *pTmp = UtilStrGetDecNumber(pBuffer, NULL, &hMedia->m_Info.m_Audio.m_dwChannelNum);
			if (!pTmp && !pBuffer)
			{
				LOG_E("[SDP]	SDPParseRtpMap - Not channel number");
				return	FALSE;
			}
			else if (!pTmp && pBuffer) {
				hMedia->m_Info.m_Audio.m_dwChannelNum = 1;
				pBuffer = pTmp;
			}
		}
	}

	// Init
	if (hMedia->m_eObjectType == H265_OBJECT)
	{
		hvcCStruct	*hvcc = &hMedia->m_SdpInfo.m_H265Sdp;

		// H265 default values other than 0
		hvcc->m_bGeneralProfileIdc = 1;
		hvcc->m_bGeneralLevelIdc = 93;

		hvcc->m_bConfigurationVersion = 1;		// currently support version 1
		hvcc->m_bLengthSizeMinusOne = 3;		// by default, 4 bytes

		// The following fields have all their valid bits set by default,
		// the ProfileTierLevel parsing code will unset them when needed.
		hvcc->m_uGeneralProfileCompatibilityFlags = 0xffffffff;
		hvcc->m_u64GeneralConstraintsIndicatorFlags  = 0xfffffffffffffffULL;

		// Initialize this field with an invalid value which can be used to detect
		// whether we didn't see any VUI (in wich case it should be reset to zero). (Max 12 bit)
		hvcc->m_sMinSpatialSegmentationIdc = 4096 + 1;

		hvcc->m_bNumTemporalLayers = 1;
		hvcc->m_bTemporalIdNested = 1;
	}

	return	TRUE;
}

static VOID UpdateMPEGGenericObjectType(FrMediaStruct *hMedia)
{
	if (hMedia->m_eObjectType == AAC_HBR_GENERIC_OBJECT)
	{
		switch((TRANS_MODE)hMedia->m_Info.m_Audio.m_Mode)
		{
		case AAC_LBR:
			hMedia->m_eObjectType = AAC_LBR_GENERIC_OBJECT;
			break;
		case AAC_HBR:
			break;
		// currently unsupported
		case CELP_CBR:
		case CELP_VBR:
			hMedia->m_eObjectType = UNKNOWN_OBJECT;
			break;
		case TOTAL_MODE:
		default:
			hMedia->m_eObjectType = UNKNOWN_OBJECT;
		}
	}
}

/* H265 Default values
	profile-space: 0-3
	profile-id: 0-31
	tier-flag: 0-1
	level-id: 0-255
	interop-constraints: [base16]
	profile-compatibility-indicator: [base16]
	sprop-sub-layer-id: 0-6, defines highest possible value for TID, default: 6
	recv-sub-layer-id: 0-6
	max-recv-level-id: 0-255
	tx-mode: MSM,SSM
	sprop-vps: [base64]
	sprop-sps: [base64]
	sprop-pps: [base64]
	sprop-sei: [base64]
	sprop-max-don-diff: 0-32767
	sprop-depack-buf-bytes: 0-4294967295
	depack-buf-cap
	sprop-segmentation-id: 0-3
	sprop-spatial-segmentation-idc: [base16]
	dec-parallel-ca:
	include-dph
*/

BOOL SDPParseFmtp(FrSDPStruct* hSDP, FrMediaStruct* hMedia, char* pBuffer)
{
	DWORD32			dwPayload;
	DWORD32			dwTemp;
	char*			pString;
	char*			pTemp;
	char*			pFmtp;

	// payload
	if (UtilStrIsDecNumber(pBuffer))
	{
		pFmtp = UtilStrGetDecNumber(pBuffer, NULL, &dwPayload);
	}
	else
	{
		if (hMedia)
			dwPayload = hMedia->m_dwPayload;
		else
		{
			LOG_E("[SDP]	SDPParseFmtp - Not payload");
			return	FALSE;
		}
		pFmtp = pBuffer;
	}

	while (!ISCRLF(*pFmtp))
	{
		pFmtp = UtilStrGetString(pFmtp, &pString, &dwTemp);
		if (!pFmtp)
		{
			// mwkang, 2008.02.26
			// as thought fmtp has not field, return true. because of google streamer bad syntax.
			LOG_E("[SDP]	SDPParseFmtp - Not field");
			//return	FALSE;
			return TRUE;
		}

		if (!UtilStrStrnicmp(pString, "framesize", strlen("framesize")))
		{
			// width
			pFmtp = UtilStrGetDecNumber(pFmtp, NULL, &hMedia->m_Info.m_Video.m_dwWidth);
			if (!pFmtp)
			{
				LOG_E("[SDP]	SDPParseFmtp - Not width");
				return	FALSE;
			}

			// height
			pFmtp = UtilStrGetDecNumber(pFmtp, NULL, &hMedia->m_Info.m_Video.m_dwHeight);
			if (!pFmtp)
			{
				LOG_E("[SDP]	SDPParseFmtp - Not height");
				return	FALSE;
			}
		}
		else if (!UtilStrStrnicmp(pString, "config", strlen("config")))
		{
			pFmtp = UtilStrGetHexString(pFmtp, &pTemp, &dwTemp);
			if (!pFmtp)
			{
				LOG_E("[SDP]	SDPParseFmtp - Not config");
				return	FALSE;
			}

			hMedia->m_dwConfig = UtilStrConvStringToBin((char*)pTemp, dwTemp, hMedia->m_pConfig, MAX_CONFIG_SIZE);
			if (!hMedia->m_dwConfig)
			{
				LOG_E("[SDP]	SDPParseFmtp - config conversion error");
				return	FALSE;
			}
		}
		else if (!UtilStrStrnicmp(pString, "parameter-sets", strlen("parameter-sets")))						// H.264 config
		{
			pFmtp = UtilStrGetHexString(pFmtp, &pTemp, &dwTemp);
			if (!pFmtp)
			{
				LOG_E("[SDP]	SDPParseFmtp - Not parameter-set");
				return	FALSE;
			}

			hMedia->m_dwConfig = UtilStrConvStringToBin(pTemp, dwTemp, hMedia->m_pConfig, MAX_CONFIG_SIZE);
			if (!hMedia->m_dwConfig)
			{
				LOG_E("[SDP]	SDPParseFmtp - config conversion error");
				return	FALSE;
			}
			hMedia->m_bH264ParamSet = TRUE;
		}
		else if (!UtilStrStrnicmp(pString, "sprop-parameter-sets", strlen("sprop-parameter-sets")))			// H.264 config
		{
			DWORD32		dwM = 0;

			while (*pFmtp != '=')
				pFmtp++;
			pFmtp++;

			pFmtp = UtilStrGetBase64String(pFmtp, &pTemp, &dwTemp);
			if (!pFmtp)
			{
				LOG_E("[SDP]	SDPParseFmtp - Not parameter-set");
				return	FALSE;
			}

			hMedia->m_dwConfig = UtilBase64ConvToBinWLen((BYTE*)pTemp, dwTemp, hMedia->m_pConfig, MAX_CONFIG_SIZE);
			if (!hMedia->m_dwConfig)
			{
				LOG_E("[SDP]	SDPParseFmtp - base64 decoding error");
				return	FALSE;
			}

			// need to link function..
			dwM = check_h264_config(hMedia->m_pConfig, hMedia->m_dwConfig);
			if (dwM==0)
			{
				LOG_D("[SDP]	SDPParseFmtp - sprop-parameter-sets are not complete");
				hMedia->m_dwConfig = 0;
			}
		}
		else if (!UtilStrStrnicmp(pString, "profile-level-id", strlen("profile-level-id")))
		{
			// profile-level-id
			if (hMedia->m_eObjectType == H264_OBJECT)
			{
				BYTE	pProfile[3];
				DWORD32	dwProfile;

				// profile-level-id
				pFmtp = UtilStrGetHexString(pFmtp, &pTemp, &dwTemp);
				if (!pFmtp)
				{
					LOG_E("[SDP]	SDPParseFmtp - Not profile-level-id");
					return	FALSE;
				}

				if (dwTemp == 6) {
					dwProfile = UtilStrConvStringToBin(pTemp, dwTemp, pProfile, 3);
					if (!dwProfile)
					{
						LOG_E("[SDP]	SDPParseFmtp - profile conversion error");
						return	FALSE;
					}
					hMedia->m_nProfile = (int)pProfile[0];
					hMedia->m_nLevel = (int)pProfile[1];
					hMedia->m_nCompatibility = (int)pProfile[2];
				} else {
					hMedia->m_nProfile = 0;
					hMedia->m_nLevel = 0;
					hMedia->m_nCompatibility = 0;
				}
			}
			else
			{
				pFmtp = UtilStrGetDecNumber(pFmtp, NULL, &dwTemp);
				if (!pFmtp)
				{
					LOG_E("[SDP]	SDPParseFmtp - Not profile-level-id");
					return	FALSE;
				}
				hMedia->m_nProfile = (int)dwTemp;
			}
		}
		else if (!UtilStrStrnicmp(pString, "profile-space", strlen("profile-space")))
		{
			pFmtp = UtilStrGetDecNumber(pFmtp, NULL, &dwTemp);
			if (!pFmtp)
			{
				LOG_E("[SDP]	SDPParseFmtp - Not profile-space");
				return	FALSE;
			}
			hMedia->m_SdpInfo.m_H265Sdp.m_bGeneralProfileSpace = dwTemp;
		}
		else if (!UtilStrStrnicmp(pString, "profile-id", strlen("profile-id")))
		{
			pFmtp = UtilStrGetDecNumber(pFmtp, NULL, &dwTemp);
			if (!pFmtp)
			{
				LOG_E("[SDP]	SDPParseFmtp - Not profile-id");
				return	FALSE;
			}
			hMedia->m_SdpInfo.m_H265Sdp.m_bGeneralProfileIdc = dwTemp;
		}
		else if (!UtilStrStrnicmp(pString, "profile-compatibility-indicator", strlen("profile-compatibility-indicator")))
		{
			// skip
			pFmtp = UtilStrFindString(pFmtp, ";");
			if (!pFmtp)
				break;
		}
		else if (!UtilStrStrnicmp(pString, "interop-constraints", strlen("interop-constraints")))
		{
			// skip
			pFmtp = UtilStrFindString(pFmtp, ";");
			if (!pFmtp)
				break;
		}
		else if (!UtilStrStrnicmp(pString, "sprop-vps", strlen("sprop-vps")))						// H.265 config
		{
			DWORD32		dwConfig = 0;
			BYTE		pConfig[2000];

			while (*pFmtp != '=')
				pFmtp++;
			pFmtp++;

			pFmtp = UtilStrGetBase64String(pFmtp, &pTemp, &dwTemp);
			if (!pFmtp)
			{
				LOG_E("[SDP]	SDPParseFmtp - Not sprop-vps");
				return	FALSE;
			}

			dwConfig = UtilBase64ConvToBinWLen((BYTE *)pTemp, dwTemp, pConfig, sizeof(pConfig));
			if (!dwConfig)
			{
				LOG_E("[SDP]	SDPParseFmtp - sprop-vps conversion error");
				return	FALSE;
			}
			if (hMedia->m_eObjectType == H265_OBJECT)
			{
				paraSetStruct	*pPara;

				if (!hMedia->m_SdpInfo.m_H265Sdp.m_pVpsPara)
					hMedia->m_SdpInfo.m_H265Sdp.m_pVpsPara = (paraSetStruct *)MALLOCZ(sizeof(paraSetStruct));
				else
					hMedia->m_SdpInfo.m_H265Sdp.m_pVpsPara = (paraSetStruct *)REALLOC(hMedia->m_SdpInfo.m_H265Sdp.m_pVpsPara,
								(hMedia->m_SdpInfo.m_H265Sdp.m_dwSpropVps+1)*sizeof(paraSetStruct));
				pPara = hMedia->m_SdpInfo.m_H265Sdp.m_pVpsPara + hMedia->m_SdpInfo.m_H265Sdp.m_dwSpropVps;
				if (!hMedia->m_SdpInfo.m_H265Sdp.m_pVpsPara)
				{
					LOG_E("[SDP]	SDPParseFmtp - sprop-vps mem alloc error (size %d)", hMedia->m_SdpInfo.m_H265Sdp.m_dwSpropVps);
					return	FALSE;
				}
				pPara->m_sLength = dwConfig;
				pPara->m_pNal = (BYTE *)MALLOC(dwConfig);
				if (!pPara->m_sLength  || !pPara->m_pNal)
				{
					LOG_E("[SDP]	SDPParseFmtp - sprop-vps mem alloc error (size %d)", dwConfig);
					return FALSE;
				}

				memcpy(pPara->m_pNal, pConfig, pPara->m_sLength);
				hMedia->m_SdpInfo.m_H265Sdp.m_dwSpropVps++;
				hMedia->m_SdpInfo.m_H265Sdp.m_bNumOfArrays++;
			}
		}
		else if (!UtilStrStrnicmp(pString, "sprop-sps", strlen("sprop-sps")))						// H.265 config
		{
			DWORD32		dwConfig = 0;
			BYTE		pConfig[2000];

			while (*pFmtp != '=')
				pFmtp++;
			pFmtp++;

			pFmtp = UtilStrGetBase64String(pFmtp, &pTemp, &dwTemp);
			if (!pFmtp)
			{
				LOG_E("[SDP]	SDPParseFmtp - Not sprop-sps");
				return	FALSE;
			}

			dwConfig = UtilBase64ConvToBinWLen((BYTE *)pTemp, dwTemp, pConfig, sizeof(pConfig));
			if (!dwConfig)
			{
				LOG_E("[SDP]	SDPParseFmtp - sprop-sps conversion error");
				return	FALSE;
			}
			if (hMedia->m_eObjectType == H265_OBJECT)
			{
				paraSetStruct	*pPara;

				if (!hMedia->m_SdpInfo.m_H265Sdp.m_pSpsPara)
					hMedia->m_SdpInfo.m_H265Sdp.m_pSpsPara = (paraSetStruct *)MALLOCZ(sizeof(paraSetStruct));
				else
					hMedia->m_SdpInfo.m_H265Sdp.m_pSpsPara = (paraSetStruct *)REALLOC(hMedia->m_SdpInfo.m_H265Sdp.m_pSpsPara,
								(hMedia->m_SdpInfo.m_H265Sdp.m_dwSpropSps+1)*sizeof(paraSetStruct));
				pPara = hMedia->m_SdpInfo.m_H265Sdp.m_pSpsPara + hMedia->m_SdpInfo.m_H265Sdp.m_dwSpropSps;
				if (!hMedia->m_SdpInfo.m_H265Sdp.m_pSpsPara)
				{
					LOG_E("[SDP]	SDPParseFmtp - sprop-sps mem alloc error (size %d)", hMedia->m_SdpInfo.m_H265Sdp.m_pSpsPara);
					return	FALSE;
				}
				pPara->m_sLength = dwConfig;
				pPara->m_pNal = (BYTE *)MALLOC(dwConfig);
				if (!pPara->m_sLength  || !pPara->m_pNal)
				{
					LOG_E("[SDP]	SDPParseFmtp - sprop-sps mem alloc error (size %d)", dwConfig);
					return FALSE;
				}

				memcpy(pPara->m_pNal, pConfig, pPara->m_sLength);
				hMedia->m_SdpInfo.m_H265Sdp.m_dwSpropSps++;
				hMedia->m_SdpInfo.m_H265Sdp.m_bNumOfArrays++;
			}
		}
		else if (!UtilStrStrnicmp(pString, "sprop-pps", strlen("sprop-pps")))						// H.265 config
		{
			DWORD32		dwConfig = 0;
			BYTE		pConfig[2000];

			while (*pFmtp != '=')
				pFmtp++;
			pFmtp++;

			pFmtp = UtilStrGetBase64String(pFmtp, &pTemp, &dwTemp);
			if (!pFmtp)
			{
				LOG_E("[SDP]	SDPParseFmtp - Not sprop-pps");
				return	FALSE;
			}

			dwConfig = UtilBase64ConvToBinWLen((BYTE *)pTemp, dwTemp, pConfig, sizeof(pConfig));
			if (!dwConfig)
			{
				LOG_E("[SDP]	SDPParseFmtp - sprop-pps conversion error");
				return	FALSE;
			}
			if (hMedia->m_eObjectType == H265_OBJECT)
			{
				paraSetStruct	*pPara;

				if (!hMedia->m_SdpInfo.m_H265Sdp.m_pPpsPara)
					hMedia->m_SdpInfo.m_H265Sdp.m_pPpsPara = (paraSetStruct *)MALLOCZ(sizeof(paraSetStruct));
				else
					hMedia->m_SdpInfo.m_H265Sdp.m_pPpsPara = (paraSetStruct *)REALLOC(hMedia->m_SdpInfo.m_H265Sdp.m_pPpsPara,
								(hMedia->m_SdpInfo.m_H265Sdp.m_dwSpropPps+1)*sizeof(paraSetStruct));
				pPara = hMedia->m_SdpInfo.m_H265Sdp.m_pPpsPara + hMedia->m_SdpInfo.m_H265Sdp.m_dwSpropPps;
				if (!hMedia->m_SdpInfo.m_H265Sdp.m_pPpsPara)
				{
					LOG_E("[SDP]	SDPParseFmtp - sprop-pps mem alloc error (size %d)", hMedia->m_SdpInfo.m_H265Sdp.m_dwSpropPps);
					return	FALSE;
				}
				pPara->m_sLength = dwConfig;
				pPara->m_pNal = (BYTE *)MALLOC(dwConfig);
				if (!pPara->m_sLength  || !pPara->m_pNal)
				{
					LOG_E("[SDP]	SDPParseFmtp - sprop-pps mem alloc error (size %d)", dwConfig);
					return FALSE;
				}

				memcpy(pPara->m_pNal, pConfig, pPara->m_sLength);
				hMedia->m_SdpInfo.m_H265Sdp.m_dwSpropPps++;
				hMedia->m_SdpInfo.m_H265Sdp.m_bNumOfArrays++;
			}
		}
		else if (!UtilStrStrnicmp(pString, "sprop-max-don-diff", strlen("sprop-max-don-diff")))
		{
			pFmtp = UtilStrGetDecNumber(pFmtp, NULL, &dwTemp);
			if (!pFmtp)
			{
				LOG_E("[SDP]	SDPParseFmtp - Not sprop-max-don-diff");
				return	FALSE;
			}
			hMedia->m_SdpInfo.m_H265Sdp.m_dwSpropMaxDonDiff = dwTemp;

			if (dwTemp)
			{
				LOG_E("[SDP]	SDPParseFmtp - sprop-max-don-diff (%d) not supported", dwTemp);
				return FALSE;
			}
		}
		else if (!UtilStrStrnicmp(pString, "object", strlen("object")))
		{
			// object
			pFmtp = UtilStrGetDecNumber(pFmtp, NULL, &dwTemp);
			if (!pFmtp)
			{
				LOG_E("[SDP]	SDPParseFmtp - Not object-id");
				return	FALSE;
			}
			hMedia->m_dwAudioObjectType = dwTemp;
		}
		else if (!UtilStrStrnicmp(pString, "bitrate", strlen("bitrate")))
		{
			// bitrate
			pFmtp = UtilStrGetDecNumber(pFmtp, NULL, &dwTemp);
			if (!pFmtp)
			{
				LOG_E("[SDP]	SDPParseFmtp - Not bitrate");
				return	FALSE;
			}
		}
		else if (!UtilStrStrnicmp(pString, "cpresent", strlen("cpresent")))
		{
			// cpresent
			pFmtp = UtilStrGetDecNumber(pFmtp, NULL, &dwTemp);
			if (!pFmtp)
			{
				LOG_E("[SDP]	SDPParseFmtp - Not cpresent");
				return	FALSE;
			}
		}
		else if (!UtilStrStrnicmp(pString, "sbr-enabled", strlen("sbr-enabled")))
		{
			// cpresent
			pFmtp = UtilStrGetDecNumber(pFmtp, NULL, &dwTemp);
			if (!pFmtp)
			{
				LOG_E("[SDP]	SDPParseFmtp - Not sbr enabled");
				return	FALSE;
			}

			if (dwTemp == 1 && hMedia->m_eObjectType != PSAAC_LATM_OBJECT)
				hMedia->m_eObjectType = HEAAC_LATM_OBJECT;
		}
		else if (!UtilStrStrnicmp(pString, "ps-enabled", strlen("ps-enabled")))
		{
			// cpresent
			pFmtp = UtilStrGetDecNumber(pFmtp, NULL, &dwTemp);
			if (!pFmtp)
			{
				LOG_E("[SDP]	SDPParseFmtp - Not ps enabled");
				return	FALSE;
			}
			if (dwTemp == 1)
				hMedia->m_eObjectType = PSAAC_LATM_OBJECT;
		}
		else if (!UtilStrStrnicmp(pString, "mode", strlen("mode")))
		{
			// mode
			pFmtp = UtilStrGetString(pFmtp, &pString, &dwTemp);
			if (!pFmtp)
			{
				LOG_E("[SDP]	SDPParseFmtp - Not mode");
				return	FALSE;
			}

			hMedia->m_Info.m_Audio.m_Mode = UtilStrGetAACMode(pString);
		}
		else if (!UtilStrStrnicmp(pString, "sizelength", strlen("sizelength")))
		{
			// sizelength
			pFmtp = UtilStrGetDecNumber(pFmtp, NULL, &dwTemp);
			if (!pFmtp)
				return	FALSE;

			hMedia->m_Info.m_Audio.m_iSizeLen = dwTemp;
		}
		else if (!UtilStrStrnicmp(pString, "indexlength", strlen("indexlength")))
		{
			// sizelength
			pFmtp = UtilStrGetDecNumber(pFmtp, NULL, &dwTemp);
			if (!pFmtp)
				return	FALSE;

			hMedia->m_Info.m_Audio.m_iIndexLen = dwTemp;
		}
		else if (!UtilStrStrnicmp(pString, "indexdeltalength", strlen("indexdeltalength")))
		{
			// sizelength
			pFmtp = UtilStrGetDecNumber(pFmtp, NULL, &dwTemp);
			if (!pFmtp)
				return	FALSE;

			hMedia->m_Info.m_Audio.m_iIndexDeltaLen = dwTemp;
		}
		else if (!UtilStrStrnicmp(pString, "level", strlen("level")))
		{
			// level
			pFmtp = UtilStrGetDecNumber(pFmtp, NULL, &dwTemp);
			if (!pFmtp)
				return	FALSE;

			//break;
		}
		else
		{
			pFmtp = UtilStrFindString(pFmtp, ";");
			if (!pFmtp)
				break;
		}
	}

	UpdateMPEGGenericObjectType(hMedia);

	return	TRUE;
}

BOOL SDPParseControl(FrSDPStruct* hSDP, FrMediaStruct* hMedia, char* pBuffer)
{
	char*	pControl;
	DWORD32	dwControl;

	pBuffer = UtilStrGetSafeString(pBuffer, &pControl, &dwControl);
	if (!pBuffer)
	{
		LOG_E("[SDP]	SDPParseControl - Not control");
		return	FALSE;
	}

	if (hMedia)
		hMedia->m_pControl = UtilStrStrdupn(pControl, dwControl);
	else
		hSDP->m_pControl = UtilStrStrdupn(pControl, dwControl);

	return	TRUE;
}

BOOL SDPParseFramesize(FrSDPStruct* hSDP, FrMediaStruct* hMedia, char* pBuffer)
{
	DWORD32			dwPayload;
	char*			pFmtp;

	// payload
	pFmtp = UtilStrGetDecNumber(pBuffer, NULL, &dwPayload);
	if (!pFmtp)
	{
		LOG_E("[SDP]	SDPParseFramesize - Not payload");
		return	FALSE;
	}

	// width
	pFmtp = UtilStrGetDecNumber(pFmtp, NULL, &hMedia->m_Info.m_Video.m_dwWidth);
	if (!pFmtp)
	{
		LOG_E("[SDP]	SDPParseFramesize - Not width");
		return	FALSE;
	}

	// height
	pFmtp = UtilStrGetDecNumber(pFmtp, NULL, &hMedia->m_Info.m_Video.m_dwHeight);
	if (!pFmtp)
	{
		LOG_E("[SDP]	SDPParseFramesize - Not height");
		return	FALSE;
	}

	return	TRUE;
}

BOOL SDPParseFrameRate(FrSDPStruct* hSDP, FrMediaStruct* hMedia, char* pBuffer)
{
	DWORD32	dwFrameRate;

	pBuffer = UtilStrGetFloatNumber(pBuffer, NULL, &dwFrameRate);
	if (!pBuffer)
	{
		LOG_E("[SDP]	SDPParseFrameRate - Not FrameRate");
		return	FALSE;
	}

	hMedia->m_Info.m_Video.m_dwFrameRate = dwFrameRate;

	return	TRUE;
}

BOOL SDPParseRange(FrSDPStruct* hSDP, FrMediaStruct* hMedia, char* pBuffer)
{
	char*	pRangeType;
	char*	pTemp;
	DWORD32	dwFrom, dwTo, dwRangeType;

	// range type
	pBuffer = UtilStrGetString(pBuffer, &pRangeType, &dwRangeType);
	if (!pBuffer)
	{
		LOG_E("[SDP]	SDPParseRange - Not range type");
		return	FALSE;
	}

	pTemp = UtilStrFindString(pBuffer, "now");
	if (pTemp)
	{
		dwTo = INFINITE;
		hSDP->m_bLive = TRUE;
	}
	else
	{
		hSDP->m_bLive = FALSE;	// set vod.

		// from
		pBuffer = UtilStrGetFloatNumber(pBuffer, NULL, &dwFrom);
		if (!pBuffer)
		{
			LOG_E("[SDP]	SDPParseRange - Not from-range");
			return	FALSE;
		}

		// to
		pBuffer = UtilStrGetFloatNumber(pBuffer, NULL, &dwTo);
		if (!pBuffer)
		{
			LOG_E("[SDP]	SDPParseRange - Not to-range");
			dwTo = INFINITE;
			hSDP->m_bLive = TRUE;	// set live.
			//return	FALSE;
		}
	}

	if (hMedia)
		hMedia->m_dwRange = dwTo;
	else
		hSDP->m_dwRange = dwTo;

	return	TRUE;
}

BOOL SDPParseWMS(FrSDPStruct* hSDP, FrMediaStruct* hMedia, char* pBuffer)
{
	char*			pTemp;
	DWORD32			dwTemp;

	// data
	pBuffer = UtilStrGetString(pBuffer, &pTemp, &dwTemp);
	if (!pBuffer)
	{
		LOG_E("[SDP]	SDPParseWMS - Not data");
		return	FALSE;
	}

	// application
	pBuffer = UtilStrGetString(pBuffer, &pTemp, &dwTemp);
	if (!pBuffer)
	{
		LOG_E("[SDP]	SDPParseWMS - Not application");
		return	FALSE;
	}

	// application
	pBuffer = UtilStrGetString(pBuffer, &pTemp, &dwTemp);
	if (!pBuffer)
	{
		LOG_E("[SDP]	SDPParseWMS - Not application");
		return	FALSE;
	}

	// value
	if ( !UtilStrStrnicmp(pTemp, "x-wms-contentdesc", strlen("x-wms-contentdesc")))
	{

	}
	else if ( !UtilStrStrnicmp(pTemp, "vnd.ms.wms-hdr.asfv1", strlen("vnd.ms.wms-hdr.asfv1")))
	{
		char*	pConfig;
		DWORD32	dwConfig;

		BYTE*	pHdr;
		DWORD32	dwHdr;

		pBuffer = UtilStrGetString(pBuffer, &pTemp, &dwTemp);
		if (!pBuffer)
		{
			LOG_E("[SDP]	SDPParseWMS - Not base64");
			return	FALSE;
		}

		pBuffer = UtilStrGetBase64String(pBuffer, &pConfig, &dwConfig);
		if (!pBuffer)
		{
			LOG_E("[SDP]	SDPParseWMS - Not header info");
			return	FALSE;
		}

		pHdr = UtilBase64ConvToBin((BYTE*)pConfig, dwConfig, &dwHdr);
		FREE(pHdr);
	}

	return	TRUE;
}

BOOL SDPParseAttributeFields(FrSDPStruct* hSDP, char* pBuffer, DWORD32 dwLen)
{
	FrMediaStruct*	hMedia = NULL;
	char*			pAttribute;
	char*			pColon;
	DWORD32			dwAttribute;

	if (hSDP->m_dwMediaSize)
		hMedia = hSDP->m_aryMedia[hSDP->m_dwMediaSize-1];

	// attributes
	pBuffer = UtilStrGetString(pBuffer, &pAttribute, &dwAttribute);
	if (!pBuffer)
	{
		LOG_E("[SDP]	SDPParseAttributeFields - No attribute name");
		return	FALSE;
	}

	// find a colon
	pColon = UtilStrFindString(pBuffer, ":");
	if (pColon)
	{
		pBuffer++;

		// value
		if ( !UtilStrStrnicmp(pAttribute, "cat", strlen("cat")))
		{

		}
		else if ( !UtilStrStrnicmp(pAttribute, "keywds", strlen("keywds")))
		{

		}
		else if ( !UtilStrStrnicmp(pAttribute, "tool", strlen("tool")))
		{

		}
		else if ( !UtilStrStrnicmp(pAttribute, "ptime", strlen("ptime")))
		{

		}
		else if ( !UtilStrStrnicmp(pAttribute, "orient", strlen("orient")))
		{

		}
		else if ( !UtilStrStrnicmp(pAttribute, "type", strlen("type")))
		{

		}
		else if ( !UtilStrStrnicmp(pAttribute, "charset", strlen("charset")))
		{

		}
		else if ( !UtilStrStrnicmp(pAttribute, "sdplang", strlen("sdplang")))
		{

		}
		else if ( !UtilStrStrnicmp(pAttribute, "lang", strlen("lang")))
		{

		}
		else if ( !UtilStrStrnicmp(pAttribute, "rtpmap", strlen("rtpmap")))
		{
			if (!SDPParseRtpMap(hSDP, hMedia, pBuffer))
				return	FALSE;
		}
		else if ( !UtilStrStrnicmp(pAttribute, "fmtp", strlen("fmtp")))
		{
			if (!SDPParseFmtp(hSDP, hMedia, pBuffer))
				return	FALSE;
		}

		else if ( !UtilStrStrnicmp(pAttribute, "control", strlen("control")))
		{
			if (!SDPParseControl(hSDP, hMedia, pBuffer))
				return	FALSE;
		}
		else if ( !UtilStrStrnicmp(pAttribute, "range", strlen("range")))
		{
			if (!SDPParseRange(hSDP, hMedia, pBuffer))
				return	FALSE;
		}
		else if ( !UtilStrStrnicmp(pAttribute, "framesize", strlen("framesize")))
		{
			if (!SDPParseFramesize(hSDP, hMedia, pBuffer))
				return	FALSE;
		}
		else if ( !UtilStrStrnicmp(pAttribute, "framerate", strlen("framerate")))
		{
			if(!SDPParseFrameRate(hSDP, hMedia, pBuffer))
				return	FALSE;
		}
		else if ( !UtilStrStrnicmp(pAttribute, "mpeg4-esid", strlen("mpeg4-esid")))
		{

		}
		else if ( !UtilStrStrnicmp(pAttribute, "mpeg4-iod", strlen("mpeg4-iod")))
		{

		}
		else if ( !UtilStrStrnicmp(pAttribute, "x-providelgtbillingdata", strlen("x-providelgtbillingdata")))
		{

		}
		else if ( !UtilStrStrnicmp(pAttribute, "x-ibufsize", strlen("x-ibufsize")))
		{
			if (!SDPParseBuffSize(hSDP, hMedia, pBuffer))
				return	FALSE;
		}
		else if ( !UtilStrStrnicmp(pAttribute, "pgmpu", strlen("pgmpu")))
		{
			if (!SDPParseWMS(hSDP, hMedia, pBuffer))
				return	FALSE;
		}
		// nhn, on-air
		else if (!UtilStrStrnicmp(pAttribute, "key-mgmt", strlen("key-mgmt")))
		{
			LOG_E("[SDP]	SDPParseAttributeFields - key-mgmt name");
			if (!SDPParseKeyMgmt(hSDP, hMedia, pBuffer))
				return	FALSE;
		}
        else if (!UtilStrStrnicmp(pAttribute, "authToken", strlen("authToken")))
        {
            LOG_I("[SDP]	SDPParseAttributeFields - accessToken name");
			if (!SDPParseAccessToken(hSDP, hMedia, pBuffer))
				return	FALSE;
        }
        else if (!UtilStrStrnicmp(pAttribute, "initSeekDiff", strlen("initSeekDiff")))
        {
            LOG_I("[SDP]    SDPParseAttributeFields - initSeekDiff name");
            if (!SDPParseInitSeekDiff(hSDP, hMedia, pBuffer))
                return    FALSE;
        }
		//else if (!UtilStrStrnicmp(pAttribute, "X-SRTPort", strlen("X-SRTPort"))) {
		//	LOG_I("[SDP]	SDPParseAttributeFields - X-SRTPort");
		//	if (!SDPParseXSRTPort(hSDP, hMedia, pBuffer))
		//		return	FALSE;
		//} else if (!UtilStrStrnicmp(pAttribute, "X-RTPInfo", strlen("X-RTPInfo"))) {
		//	LOG_I("[SDP]	SDPParseAttributeFields - X-RTPInfo");
		//	if (!SDPParseXRTPInfo(hSDP, hMedia, pBuffer))
		//		return	FALSE;
		//}
	}
	else
	{
		if ( !UtilStrStrnicmp(pAttribute, "recvonly", strlen("recvonly")))
		{
			hMedia->m_eRecvSend = SDP_RECV_ONLY;
		}
		else if ( !UtilStrStrnicmp(pAttribute, "sendrecv", strlen("sendrecv")))
		{
			hMedia->m_eRecvSend = SDP_RECV_SEND;
		}
		else if ( !UtilStrStrnicmp(pAttribute, "sendonly", strlen("sendonly")))
		{
			hMedia->m_eRecvSend = SDP_SEND_ONLY;
		}
		else if ( !UtilStrStrnicmp(pAttribute, "isma-compliance", strlen("isma-compliance")))
		{

		}
		else if ( !UtilStrStrnicmp(pAttribute, "x-allowrecord", strlen("x-allowrecord")))
		{

		}
		else if ( !UtilStrStrnicmp(pAttribute, "x-disallowrandomaccess", strlen("x-disallowrandomaccess"))
				|| !UtilStrStrnicmp(pAttribute, "random_access_denied", strlen("random_access_denied")))
			// packet video random access deny support
		{

		}
	}

	return TRUE;
}

BOOL SDPParseKeyMgmt(FrSDPStruct* hSDP, FrMediaStruct* hMedia, char* pBuffer)
{
	char*			pMediaType;
	DWORD32			dwMediaType;
	char*			pKey;
	DWORD32			dwKey;

	// media type
	pBuffer = UtilStrGetString(pBuffer, &pMediaType, &dwMediaType);
	if (!pBuffer || ISCRLF(*pBuffer))
	{
		LOG_E("[SDP]	SDPParseKeyMgmt - Not encoding name");
		return	FALSE;
	}

	//pBuffer = UtilStrGetString(pBuffer, &pKey, &dwKey);
	pBuffer = UtilStrGetSafeString(pBuffer, &pKey, &dwKey);
	if (!pBuffer)
	{
		LOG_E("[SDP]	SDPParseKeyMgmt - Not encoding name");
		return	FALSE;
	}
	strncpy((char*)hMedia->m_pConfig, pKey, dwKey);
	hMedia->m_dwConfig = dwKey;

	return	TRUE;
}

BOOL SDPParseAccessToken(FrSDPStruct* hSDP, FrMediaStruct* hMedia, char* pBuffer)
{
	char*			pKey;
	DWORD32			dwKey;

	// accessToken
	pBuffer = UtilStrGetSafeString(pBuffer, &pKey, &dwKey);
	if (!pBuffer)
	{
		LOG_E("[SDP]	SDPParseAccessToken - Not encoding name");
		return	FALSE;
	}

    hSDP->m_pAuthToken = UtilStrStrdupn(pKey, dwKey);

	return	TRUE;
}

/*
BOOL SDPParseXSRTPort(FrSDPStruct* hSDP, FrMediaStruct* hMedia, char* pBuffer)
{
	//char*	pXInfo;
	//DWORD32	dwXInfo;

	// srt port
	if (UtilStrIsDecNumber(pBuffer)) {
		if (!UtilStrGetDecNumber(pBuffer, NULL, &hSDP->m_wSRTPort))
			return FALSE;
	} else
		return FALSE;

	//if (hMedia)
	//	hMedia->m_pControl = UtilStrStrdupn(pControl, dwControl);
	//else
	//	hSDP->m_pControl = UtilStrStrdupn(pControl, dwControl);

	return	TRUE;
}

BOOL SDPParseXRTPInfo(FrSDPStruct* hSDP, FrMediaStruct* hMedia, char* pBuffer)
{

	return	TRUE;
}
*/

BOOL SDPParseInitSeekDiff(FrSDPStruct* hSDP, FrMediaStruct* hMedia, char* pBuffer)
{
    char*            pKey;
    int              nInitSeekDiff=0;

    // initSeekDiff
    pBuffer = UtilStrGetIntNumber(pBuffer, NULL, &nInitSeekDiff);
    if (!pBuffer)
    {
        LOG_E("[SDP]    SDPParseInitSeekDiff - Not InitSeekDiff");
        return    FALSE;
    }

    hSDP->m_nInitSeekDiffTime = nInitSeekDiff;

    return    TRUE;
}
