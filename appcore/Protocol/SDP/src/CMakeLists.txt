cmake_minimum_required(VERSION 3.16)

# add all current files to SRC_FILES
file(GLOB_RECURSE SRC_FILES CONFIGURE_DEPENDS
	${CMAKE_CURRENT_SOURCE_DIR}/*.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/*.c
	)
# make static library
add_library(SDP STATIC ${SRC_FILES})

# header path for using compile library
target_include_directories(SDP PUBLIC ${CMAKE_SOURCE_DIR}/include)

# compile options
target_compile_options(SDP PRIVATE -Wall -Werror -Wno-error=switch -Wno-error=format -Wno-error=unused-variable -Wno-error=unused-but-set-variable -DLinux)
