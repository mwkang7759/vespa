
#include "UtilAPI.h"
#include "Sdp.h"

// m=<m_eMedia> <port>/<number of ports> <transport> <fmt list>

typedef enum
{
	S_media,
	S_port,
	S_slash,
	S_numberofports,
	S_transport,
	S_payloadtype,
	S_space
} STATE_TYPE;

FrMediaStruct* SDPNewMedia(FrSDPStruct* hSDP)
{
	FrMediaStruct*	hMedia = (FrMediaStruct*)MALLOCZ(sizeof(FrMediaStruct));

	if (hMedia)
	{
		// range value is copyed from the range of session level.
		hSDP->m_aryMedia[hSDP->m_dwMediaSize++] = hMedia;
	}

	return	hMedia;
}

BOOL SDPParseMediaField(FrSDPStruct* hSDP, char* pBuffer, DWORD32 dwLen)
{
	FrMediaStruct*	hMedia;
	char*			pStart = pBuffer;
	BYTE			cTemp;
	STATE_TYPE		state = S_media, next = S_media;

	hMedia = SDPNewMedia(hSDP);

	while (dwLen)
	{
		switch(state)
		{
		case S_space:
		    if (!ISSPACE((int)*pBuffer) && *pBuffer) 
			{
				state = next;
				pStart = pBuffer;
				//pBuffer--;
			}
			break;
		case S_media:
			if (!ISALPHA(*pBuffer))
			{
				cTemp = *pBuffer;
				*pBuffer = (char)0;

				if (!strncmp(pStart, "audio", strlen("audio")))
					hMedia->m_eMedia = MI_AUDIO;
				else if (!strncmp(pStart, "video", strlen("video")))
					hMedia->m_eMedia = MI_VIDEO;
				else if (!strncmp(pStart, "text", strlen("text")))
					hMedia->m_eMedia = MI_TEXT;
				else if (!strncmp(pStart, "application", strlen("application")))
					hMedia->m_eMedia = MI_APPLICATION;
				else if (!strncmp(pStart, "data", strlen("data")))
					hMedia->m_eMedia = MI_DATA;
				else if (!strncmp(pStart, "control", strlen("control")))
					hMedia->m_eMedia = MI_CONTROL;

				*pBuffer = cTemp;
				//pBuffer--;

				state = S_space;
				next  = S_port;
			}
			break;
		case S_port:
			if (!ISDIGIT(*pBuffer)) 
			{
				cTemp = *pBuffer;
				*pBuffer = (char)0;
				//hMedia->port = ATOI(pStart);
				
				*pBuffer = cTemp;
				//pBuffer--;

				state = S_slash;
				next  = S_numberofports;
			}
			break;
		case S_slash:
			if (*pBuffer == '/')
			{
				state = next;
				pStart = pBuffer+1;
				//pBuffer--;
			}
			else
			{
				state = S_space;
				next = S_transport;
			}
			break;
		case S_numberofports:
			if (!ISDIGIT(*pBuffer)) 
			{
				cTemp = *pBuffer;
				*pBuffer = (char)0;
				//hMedia->nPorts = ATOI(pStart);
				state = S_space;
				next  = S_transport;
				*pBuffer = cTemp;
				//pBuffer--;
			}
			break;
		case S_transport:
			if (!(ISALPHA(*pBuffer) || *pBuffer == '/') )
			{
				//if (!strncasecmp(pStart,"rtp/avp",7))
				//	hMedia->tr_profile = TR_Pr_AVP;
				//else
				//	hMedia->tr_profile = TR_Pr_UDP;
				state = S_space;
				next  = S_payloadtype;
				//pBuffer--;
			}
			break;
		case S_payloadtype:
			if (!ISDIGIT(*pBuffer))
			{
				cTemp = *pBuffer;
				*pBuffer = (char)0;

				hMedia->m_dwPayload = ATOI(pStart);		// payload
				*pBuffer = cTemp;

				state = S_space;
				next  = S_payloadtype;
				//pBuffer--;
			}
			break;
		}
		pBuffer++;
		dwLen--;
	}
			
	return	TRUE;
}
