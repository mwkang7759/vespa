#ifndef _HMAC_SHA1_H
#define	_HMAC_SHA1_H

//#pragma ident	"@(#)hmac_sha1.h	1.2	05/06/08 SMI"

#include <sys/types.h>
#include "sha1.h"

#ifdef	__cplusplus
extern "C" {
#endif

#define	HMAC_DIGEST_LEN	20

extern void HMACInit(SHA1_CTX *, const unsigned char *, size_t);
extern void HMACUpdate(SHA1_CTX *, const unsigned char *, size_t);
extern void HMACFinal(SHA1_CTX *sha1Context, const unsigned char *, size_t,
    unsigned char digest[HMAC_DIGEST_LEN]);


#ifdef	__cplusplus
}
#endif

#endif /* _HMAC_SHA1_H */
