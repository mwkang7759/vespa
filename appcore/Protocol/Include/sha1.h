#ifndef _SYS_SHA1_H
#define	_SYS_SHA1_H

//#pragma ident	"@(#)sha1.h	1.5	06/03/28 SMI"

#include <sys/types.h>		/* for uint_* */
#include <stdio.h>			// mwkang, declare size_t
#ifdef	__cplusplus
extern "C" {
#endif

/* SHA-1 context. */
typedef struct 	{
	unsigned int  state[5];	/* state (ABCDE) */
	unsigned int  count[2];	/* number of bits, modulo 2^64 (msb first) */
	union 	{
		unsigned char   buf8[64];	/* undigested input */
		unsigned int	buf32[16];	/* realigned input */
	} buf_un;
} SHA1_CTX;

void SHA1Init(SHA1_CTX *);
void SHA1Update(SHA1_CTX *, const void *, size_t);
void SHA1Final(void *, SHA1_CTX *);

#ifdef	__cplusplus
}
#endif

#endif /* _SYS_SHA1_H */

