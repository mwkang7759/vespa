#ifndef MCW_AES_CTR_H_
#define MCW_AES_CTR_H_

/*
 * Author	: Sae-Hun Oh (shoh@mcubeworks.com)
 * Date	: Thur May 26 2006
 */

#ifdef __cplusplus
extern "C" {
#endif

/*  
 Description : Open aes-ctr-128 function. Default aounter size is 4-byte
 args:
	pszMasterKey: Master Key for aes-ctr-128. Must be 16 bytes
	pszIv		: Initial Vector for aes-ctr-128. Must be 16 bytes
 Success	: Return void pointer
 Failure	: Return NULL
*/
void* AesCtrOpen( char *pszMasterKey, char *pszIv );

/*
 Description : Set counter size. It must be called before encryption.
 args:
	pCtr_st : returned handler from AesCtrOpen
	iCtrSize : Counter Size (1~16); 
 Success	: return iCtrSize
 Failure	: return -1
*/
int AesCtrSetCnt( void *pCtr_st, int iCtrSize );

/*
 Description : Close aes-ctr-128 function
 arg:
	 pCtr_st	: returned handler from AesCtrOpen
 Success	: Return 0
 Failure	: Return -1
*/
int	AesCtrClose( void *pCtr_st);

/*
 Description : Encript string using aes-ctr-128
 args	:	
	pCtr_st	: returned pointer from AesCtrOpen
	pszSrc	: pointer to string to encrypt
	pszDes	: pointer to buffer for encrypted string
	nSrcLen : source string length
 Success	: Return 0
 Failure	: Return -1
*/
int AesCtrEnc( void *pCtr_st, char *pszSrc, char *pszDes, unsigned int nSrcLen);

/*
 Description : Decript string using aes-ctr-128
 args	:
	pCtr_st	: returned pointer from AesCtrOpen
	pszSrc	: pointer to string to Decrypt
	pszDes	: pointer to buffer for decrypted string
	nSrcLen : encrypted string length
 Success	: Return 0
 Failure	: Return Non Zero
*/
int AesCtrDec( void *pCtr_st, char *pszSrc, char *pszDes, unsigned int nSrcLen);

#ifdef __cplusplus
}
#endif


#endif // MCW_AES_CTR_H_
