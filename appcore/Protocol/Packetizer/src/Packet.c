
#include "Packet.h"
#include "PacketH264.h"

FrPacketHandle FrPacketOpen(FrMediaHandle hMedia, BOOL bRTSP) {
	FrPacketStruct*	hPacket;
	BYTE*			pConfig;
	DWORD32			dwConfig;
	DWORD32			dwWidth;

	hPacket = (FrPacketStruct*)MALLOCZ(sizeof(FrPacketStruct));
	if (hPacket) {
		hPacket->m_eObject = FrSDPGetMediaObjectType(hMedia);
		hPacket->m_dwSampleRate = FrSDPGetMediaSampleRate(hMedia);

		pConfig = FrSDPGetMediaConfig(hMedia);
		dwConfig = FrSDPGetMediaConfigLen(hMedia);

		switch (hPacket->m_eObject)
		{
		case H264_OBJECT:
			if (bRTSP)
				hPacket->m_hPacket = PacketH264Open(NULL, 0);
			else
				hPacket->m_hPacket = PacketH264Open(pConfig, dwConfig);
#ifdef	PKT_DUMP
			hPacket->m_hPktFile = FILEDUMPOPEN("c:\\video_pkt", "pkt", (DWORD32)hPacket);
#endif
#ifdef	FILE_DUMP
			hPacket->m_hFile = FILEDUMPOPEN("c:\\video_pkt", "h264", (DWORD32)hPacket);
#endif
			break;
		
		default:
			FREE(hPacket);
			return	NULL;
		}
	}

	return	hPacket;
}

BOOL FrPacketInit(FrPacketHandle hHandle, BOOL bUDP) {
	FrPacketStruct*	hPacket = (FrPacketStruct*)hHandle;

	if (hPacket) {
		switch (hPacket->m_eObject)
		{		
		case H264_OBJECT:
			PacketH264Init((PacketH264Handle)hPacket->m_hPacket, bUDP);
			break;
		default:
			break;
		}
		hPacket->m_bSetCTS = FALSE;
	}

	return	TRUE;
}

void FrPacketClose(FrPacketHandle hHandle) {
	FrPacketStruct*	hPacket = (FrPacketStruct*)hHandle;

	if (hPacket) {
		switch (hPacket->m_eObject)
		{		
		case H264_OBJECT:
			PacketH264Close((PacketH264Handle)hPacket->m_hPacket);
			break;
		default:
			break;
		}
#ifdef	PKT_DUMP
		FrCloseFile(hPacket->m_hPktFile);
#endif
#ifdef	FILE_DUMP
		FrCloseFile(hPacket->m_hFile);
#endif

		FREE(hPacket);
		LOG_I("[Packet]	FrPacketClose");
	}
}

void FrPacketPutData(FrPacketHandle hHandle, BYTE* pData, DWORD32 dwData, DWORD32 dwCTS) {
	FrPacketStruct*	hPacket;

	if (!hHandle || !pData || !dwData)
		return;

#ifdef	FILE_DUMP
	if (pData)
	{
#ifdef	PARSE_DUMP
		FPRINTF(hPacket->m_hFile, "[FRAME] Size=%d\n", dwData);
		FILEDUMP(hPacket->m_hFile, pData, dwData);
#else
		FrWriteFile(hPacket->m_hFile, pData, dwData);
#endif
	}
#endif

	hPacket = (FrPacketStruct*)hHandle;

	switch (hPacket->m_eObject)
	{
	case H264_OBJECT:
		PacketH264PutData((PacketH264Handle)hPacket->m_hPacket, pData, dwData);
		break;
	default:
		break;
	}

	if (!hPacket->m_bSetCTS) {
		hPacket->m_dwCTS = dwCTS;
		hPacket->m_bSetCTS = TRUE;
	}
}

BYTE* FrPacketGetData(FrPacketHandle hHandle, DWORD32* pdwPacket, DWORD32* pdwCTS, BOOL* pbMBit) {
	FrPacketStruct*	hPacket = (FrPacketStruct*)hHandle;
	BYTE*			pPacket = NULL;

	*pbMBit = TRUE;

	switch (hPacket->m_eObject)
	{
	case H264_OBJECT:
		pPacket = PacketH264GetData((PacketH264Handle)hPacket->m_hPacket, pdwPacket, pbMBit);
		break;

	default:
		break;
	}

	if (!pPacket)
		return	NULL;

	*pdwCTS = hPacket->m_dwCTS;
	hPacket->m_bSetCTS = FALSE;

#ifdef	PKT_DUMP
#ifdef	PARSE_DUMP
	FPRINTF(hPacket->m_hPktFile, "[PACKET] Size=%d\n", *pdwPacket);
	FPRINTF(hPacket->m_hPktFile, "	[RTP Header]\n");
	FILEDUMP(hPacket->m_hPktFile, pPacket, 12);
	FPRINTF(hPacket->m_hPktFile, "	[Payload]\n");
	FILEDUMP(hPacket->m_hPktFile, pPacket+12, *pdwPacket-12);
#else
	FrWriteFile(hPacket->m_hPktFile, pPacket, dwData);
#endif
#endif

	return	pPacket;
}
