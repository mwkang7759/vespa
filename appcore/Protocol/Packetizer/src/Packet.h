
#ifndef	_PACKET_H_
#define	_PACKET_H_

#include "SocketAPI.h"
#include "SystemAPI.h"
#include "TraceAPI.h"
#include "mediainfo.h"
#include "SdpAPI.h"
#include "RtpAPI.h"
#include "RtspAPI.h"
#include "UtilAPI.h"

#ifdef	_DEBUG
//#define	PKT_DUMP
//#define	FILE_DUMP
//#define	PARSE_DUMP
#endif

typedef	struct
{
	OBJECT_TYPE		m_eObject;
	DWORD32			m_dwSampleRate;
	void*			m_hPacket;
	BOOL			m_bSetCTS;
	DWORD32			m_dwCTS;

#ifdef	PKT_DUMP
	FILE_HANDLE		m_hPktFile;
#endif
#ifdef	FILE_DUMP
	FILE_HANDLE		m_hFile;
#endif
} FrPacketStruct;

#include "PacketAPI.h"

#endif	// _PACKET_H_
