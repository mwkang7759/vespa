
#include "PacketH264.h"

PacketH264Handle PacketH264Open(BYTE* pConfig, DWORD32 dwConfig)
{
	PacketH264Handle	hH264 = NULL;

	hH264 = (PacketH264Handle)MALLOCZ(sizeof(PacketH264Struct));
	if (hH264)
	{
		hH264->m_Header.m_F = 0;

		hH264->m_FUStart.m_SBit = 1;
		hH264->m_FUStart.m_EBit = 0;
		hH264->m_FUStart.m_RBit = 0;

		hH264->m_FUMid.m_SBit = 0;
		hH264->m_FUMid.m_EBit = 0;
		hH264->m_FUMid.m_RBit = 0;

		hH264->m_FUEnd.m_SBit = 0;
		hH264->m_FUEnd.m_EBit = 1;
		hH264->m_FUEnd.m_RBit = 0;

		hH264->m_dwHeaderSize = RTP_HEADER_LENGTH;
		hH264->m_bFirst = TRUE;
		if (pConfig)
		{
			BYTE*	pSrc = pConfig;
			BYTE*	pDst;
			int		nNum, nSrc, nLen;

			hH264->m_pConfig = (BYTE*)MALLOC(dwConfig);
			hH264->m_dwConfig = dwConfig;
			pDst = hH264->m_pConfig;

			nSrc = (int)dwConfig;
			while (nSrc > 0)
			{
				nNum = (int)(*pSrc);
				pSrc++; nSrc--;
				hH264->m_dwConfig--;

				while (nNum)
				{
					nLen = (int)ConvByteToWORD(pSrc) + 2;
					memcpy(pDst, pSrc, nLen);
					pSrc += nLen; nSrc -= nLen; pDst += nLen;
					nNum--;
				}
			}
		}
	}

	return	hH264;
}

void PacketH264Init(PacketH264Handle hH264, BOOL bUDP)
{
	hH264->m_pFrame = NULL;
	hH264->m_dwFrameLen = 0;
	if (bUDP)
		hH264->m_dwHeaderSize = RTP_HEADER_LENGTH;
	else
		hH264->m_dwHeaderSize = RTP_HEADER_LENGTH + RTP_TCP_HEADER_LEN;

    hH264->m_bFU = FALSE;
}

void PacketH264Close(PacketH264Handle hH264)
{
	if (hH264)
	{
		FREE(hH264->m_pConfig);
		FREE(hH264);
	}
}

BOOL PacketH264PutData(PacketH264Handle hH264, BYTE* pData, DWORD32 dwData)
{
	DWORD32	dwNalLen;
	int		iLen;

	hH264->m_pFrame = pData;
	hH264->m_dwFrameLen = dwData;
	hH264->m_dwNalNum = 0;

	//TRACE("[Packet_H264]	PacketH264PutData - len(%d)", dwData);

	iLen = (int)dwData;
	//while (iLen > 0)
	while (iLen > 3)
	{
		dwNalLen = ConvByteToDWORD(pData);
		if (iLen < (int)(dwNalLen + BYTES_FOR_NAL_SIZE))
			return	FALSE;
		iLen -= (dwNalLen + BYTES_FOR_NAL_SIZE);
		pData += (dwNalLen + BYTES_FOR_NAL_SIZE);
		hH264->m_dwNalNum++;
	}
	// when there is some junk data at the end
	if (iLen > 0)
		hH264->m_dwFrameLen -= iLen;
	hH264->m_dwCurNal = hH264->m_dwNalNum;

	return	TRUE;
}

BYTE* PacketH264GetData(PacketH264Handle hH264, DWORD32* pdwPacket, BOOL* pbMBit)
{
	BYTE*	pBuffer = NULL;
	BYTE*	pBufPos;
	DWORD32	dwBufLen;

	// mwkang
	WORD	wNalSize;


	if ((int)hH264->m_dwFrameLen <= 0)
		return	NULL;

	dwBufLen = hH264->m_dwHeaderSize + RTP_MAX_MTU_SIZE;
	pBuffer = (BYTE*)MALLOC(dwBufLen);

	// RTP Header
	pBufPos = pBuffer + hH264->m_dwHeaderSize;
	dwBufLen = RTP_MAX_MTU_SIZE;
	*pdwPacket = hH264->m_dwHeaderSize;

	if (hH264->m_bFirst && hH264->m_pConfig)
	{
		hH264->m_Header.m_Type = PACK_MODE_STAP_A;
		hH264->m_Header.m_NRI = (hH264->m_pConfig[2]>>5) & 0x03;
		memcpy(pBufPos, &hH264->m_Header, H264_HDR_LEN);
		pBufPos += H264_HDR_LEN;
		dwBufLen -= H264_HDR_LEN;
		*pdwPacket += H264_HDR_LEN;

		memcpy(pBufPos, hH264->m_pConfig, hH264->m_dwConfig);
		pBufPos += hH264->m_dwConfig;
		dwBufLen -= hH264->m_dwConfig;
		*pdwPacket += hH264->m_dwConfig;
		hH264->m_bFirst = FALSE;

		*pbMBit = FALSE;

		return	pBuffer;
	}

	// NAL Length
	if (hH264->m_bFU)
	{
		memcpy(pBufPos, &hH264->m_Header, H264_HDR_LEN);
		pBufPos += H264_HDR_LEN;
		dwBufLen -= H264_HDR_LEN;
		*pdwPacket += H264_HDR_LEN;

		// FU end mode
		if (hH264->m_dwNalLen + H264_FU_HDR_LEN <= dwBufLen)
		{
			memcpy(pBufPos, &hH264->m_FUEnd, H264_FU_HDR_LEN);
			pBufPos += H264_FU_HDR_LEN;
			dwBufLen -= H264_FU_HDR_LEN;

			memcpy(pBufPos, hH264->m_pFrame, hH264->m_dwNalLen);
			//*pdwPacket += (hH264->m_dwFrameLen + H264_FU_HDR_LEN);	// mwkang..
			*pdwPacket += (hH264->m_dwNalLen + H264_FU_HDR_LEN);
			hH264->m_pFrame += hH264->m_dwNalLen;
			hH264->m_dwFrameLen -= hH264->m_dwNalLen;
			if (!hH264->m_dwCurNal)
				*pbMBit = TRUE;
			else
				*pbMBit = FALSE;

			hH264->m_bFU = FALSE;
			//LOG_T("[Packet_H264]	FU_end len(%d), Header type(%d), NRI(%d), FU type(%d), Ebit(%d)",
			//		*pdwPacket, hH264->m_Header.m_Type, hH264->m_Header.m_NRI, hH264->m_FUEnd.m_Type, hH264->m_FUEnd.m_EBit);
		}
		// FU mid mode
		else
		{
			memcpy(pBufPos, &hH264->m_FUMid, H264_FU_HDR_LEN);
			pBufPos += H264_FU_HDR_LEN;
			dwBufLen -= H264_FU_HDR_LEN;

			memcpy(pBufPos, hH264->m_pFrame, dwBufLen);
			hH264->m_pFrame += dwBufLen;
			hH264->m_dwFrameLen -= dwBufLen;
			hH264->m_dwNalLen -= dwBufLen;

			*pdwPacket += (dwBufLen + H264_FU_HDR_LEN);
			*pbMBit = FALSE;
			//LOG_T("[Packet_H264]	FU_mid len(%d), Header type(%d), NRI(%d), FU type(%d), Ebit(%d)",
			//	*pdwPacket, hH264->m_Header.m_Type, hH264->m_Header.m_NRI, hH264->m_FUMid.m_Type, hH264->m_FUMid.m_EBit);
		}
	}
	else
	{
		hH264->m_dwNalLen = ConvByteToDWORD(hH264->m_pFrame);
		hH264->m_pFrame += BYTES_FOR_NAL_SIZE;
		hH264->m_dwFrameLen -= BYTES_FOR_NAL_SIZE;

		//LOG_T("[Packet_H264]	NalLen(%d), NalNum(%d)", hH264->m_dwNalLen, hH264->m_dwCurNal);	// on test..

		// Simple mode
		if (hH264->m_dwNalLen <= dwBufLen && hH264->m_dwNalNum == 1)
		{
			memcpy(pBufPos, hH264->m_pFrame, hH264->m_dwNalLen);
			hH264->m_pFrame += hH264->m_dwNalLen;
			hH264->m_dwFrameLen -= hH264->m_dwNalLen;
			*pdwPacket += hH264->m_dwNalLen;
			*pbMBit = TRUE;
			//LOG_T("[Packet_H264]	Simple len(%d)", *pdwPacket);
		}
		// STAP mode
		else if (hH264->m_dwNalLen + H264_HDR_LEN + H264_STAP_NAL_SIZE <= dwBufLen && hH264->m_dwNalNum > 1)
		{
			hH264->m_Header.m_Type = PACK_MODE_STAP_A;
			hH264->m_Header.m_NRI = (hH264->m_pFrame[0]>>5) & 0x03;
			memcpy(pBufPos, &hH264->m_Header, H264_HDR_LEN);
			pBufPos += H264_HDR_LEN;
			dwBufLen -= H264_HDR_LEN;
			*pdwPacket += H264_HDR_LEN;

			while (1)
			{
				wNalSize = htons((WORD)hH264->m_dwNalLen);		// mwkang
				//LOG_T("[Packet_H264]	STAP first NalLen(%d)", hH264->m_dwNalLen);

				// NAL Size
				//memcpy(pBufPos, (WORD*)&htons((WORD)hH264->m_dwNalLen), H264_STAP_NAL_SIZE);
				memcpy(pBufPos, &wNalSize, H264_STAP_NAL_SIZE);
				pBufPos += H264_STAP_NAL_SIZE;
				dwBufLen -= H264_STAP_NAL_SIZE;
				*pdwPacket += H264_STAP_NAL_SIZE;

				memcpy(pBufPos, hH264->m_pFrame, hH264->m_dwNalLen);
				hH264->m_pFrame += hH264->m_dwNalLen;
				hH264->m_dwFrameLen -= hH264->m_dwNalLen;
				hH264->m_dwCurNal--;
				pBufPos += hH264->m_dwNalLen;
				dwBufLen -= hH264->m_dwNalLen;
				*pdwPacket += hH264->m_dwNalLen;

				if (!hH264->m_dwCurNal)
					break;

				hH264->m_dwNalLen = ConvByteToDWORD(hH264->m_pFrame);
				hH264->m_pFrame += BYTES_FOR_NAL_SIZE;
				hH264->m_dwFrameLen -= BYTES_FOR_NAL_SIZE;

				//LOG_T("[Packet_H264]	STAP second NalLen(%d)", hH264->m_dwNalLen);

				if (hH264->m_dwNalLen + H264_STAP_NAL_SIZE > dwBufLen)
				{
					hH264->m_pFrame -= BYTES_FOR_NAL_SIZE;
					hH264->m_dwFrameLen += BYTES_FOR_NAL_SIZE;
					break;
				}
			}
			if (!hH264->m_dwCurNal)
				*pbMBit = TRUE;
			else
				*pbMBit = FALSE;
			//LOG_T("[Packet_H264]	STAP len(%d)", *pdwPacket);
		}
		// mwkang
		else if (hH264->m_dwNalLen + H264_HDR_LEN + H264_STAP_NAL_SIZE > dwBufLen && hH264->m_dwNalNum > 1 &&
				hH264->m_dwNalLen-1 <= dwBufLen-2)
		{
			dwBufLen = hH264->m_dwHeaderSize + hH264->m_dwNalLen + H264_HDR_LEN + H264_STAP_NAL_SIZE;
			pBuffer = (BYTE*)REALLOC(pBuffer, dwBufLen);

			// RTP Header
			pBufPos = pBuffer + hH264->m_dwHeaderSize;
			dwBufLen = hH264->m_dwNalLen + H264_HDR_LEN + H264_STAP_NAL_SIZE;
			*pdwPacket = hH264->m_dwHeaderSize;

			hH264->m_Header.m_Type = PACK_MODE_STAP_A;
			hH264->m_Header.m_NRI = (hH264->m_pFrame[0]>>5) & 0x03;
			memcpy(pBufPos, &hH264->m_Header, H264_HDR_LEN);
			pBufPos += H264_HDR_LEN;
			dwBufLen -= H264_HDR_LEN;
			*pdwPacket += H264_HDR_LEN;

			while (1)
			{
				wNalSize = htons((WORD)hH264->m_dwNalLen);		// mwkang
				//LOG_T("[Packet_H264]	STAP first NalLen(%d)", hH264->m_dwNalLen);

				// NAL Size
				//memcpy(pBufPos, (WORD*)&htons((WORD)hH264->m_dwNalLen), H264_STAP_NAL_SIZE);
				memcpy(pBufPos, &wNalSize, H264_STAP_NAL_SIZE);
				pBufPos += H264_STAP_NAL_SIZE;
				dwBufLen -= H264_STAP_NAL_SIZE;
				*pdwPacket += H264_STAP_NAL_SIZE;

				memcpy(pBufPos, hH264->m_pFrame, hH264->m_dwNalLen);
				hH264->m_pFrame += hH264->m_dwNalLen;
				hH264->m_dwFrameLen -= hH264->m_dwNalLen;
				hH264->m_dwCurNal--;
				pBufPos += hH264->m_dwNalLen;
				dwBufLen -= hH264->m_dwNalLen;
				*pdwPacket += hH264->m_dwNalLen;

				if (!hH264->m_dwCurNal)
					break;

				hH264->m_dwNalLen = ConvByteToDWORD(hH264->m_pFrame);
				hH264->m_pFrame += BYTES_FOR_NAL_SIZE;
				hH264->m_dwFrameLen -= BYTES_FOR_NAL_SIZE;

				//LOG_T("[Packet_H264]	STAP second NalLen(%d)", hH264->m_dwNalLen);

				if (hH264->m_dwNalLen + H264_STAP_NAL_SIZE > dwBufLen)
				{
					hH264->m_pFrame -= BYTES_FOR_NAL_SIZE;
					hH264->m_dwFrameLen += BYTES_FOR_NAL_SIZE;
					break;
				}
			}
			if (!hH264->m_dwCurNal)
				*pbMBit = TRUE;
			else
				*pbMBit = FALSE;
			//LOG_T("[Packet_H264]	STAP len(%d)", *pdwPacket);

		}
		// FU start mode
		else
		{
			hH264->m_Header.m_Type = PACK_MODE_FU_A;
			hH264->m_Header.m_NRI = (hH264->m_pFrame[0]>>5) & 0x03;
			hH264->m_FUStart.m_Type = hH264->m_FUMid.m_Type = hH264->m_FUEnd.m_Type = hH264->m_pFrame[0] & 0x1F;
			hH264->m_pFrame += H264_HDR_LEN;
			hH264->m_dwFrameLen -= H264_HDR_LEN;
			hH264->m_dwNalLen -= H264_HDR_LEN;

			memcpy(pBufPos, &hH264->m_Header, H264_HDR_LEN);
			memcpy(pBufPos+1, &hH264->m_FUStart, H264_FU_HDR_LEN);
			pBufPos += (H264_HDR_LEN + H264_FU_HDR_LEN);
			dwBufLen -= (H264_HDR_LEN + H264_FU_HDR_LEN);
			*pdwPacket += (H264_HDR_LEN + H264_FU_HDR_LEN);

			memcpy(pBufPos, hH264->m_pFrame, dwBufLen);
			hH264->m_pFrame += dwBufLen;
			hH264->m_dwFrameLen -= dwBufLen;
			hH264->m_dwNalLen -= dwBufLen;
			*pdwPacket += dwBufLen;

			*pbMBit = FALSE;
			hH264->m_bFU = TRUE;

			hH264->m_dwCurNal--;
			//LOG_T("[Packet_H264]	FU_start len(%d), Header type(%d), NRI(%d), FU type(%d), Sbit(%d)",
			//		*pdwPacket, hH264->m_Header.m_Type, hH264->m_Header.m_NRI, hH264->m_FUStart.m_Type, hH264->m_FUStart.m_SBit);
		}
	}

	return	pBuffer;
}
