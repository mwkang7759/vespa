/*****************************************************************************
*                                                                            *
*                            Packetizer Library								 *
*                                                                            *
*   Copyright (c) 2017 by Mcubeworks, Incoporated. All Rights Reserved.      *
******************************************************************************

    File Name       : PacketH265.h
    Author(s)       : CHANG, Joonho
    Created         : March 6 2017

    Description     : File API
    Notes           : 

==============================================================================
                      Modification History (Reverse Order)                    
==============================================================================
    Date        Author      Location (variables) / Description
------------------------------------------------------------------------------

------------------------------------------------------------------------------*/

#ifndef _PacketH265_H_
#define _PacketH265_H_

#include "Packet.h"

#define		H265_NAL_TYPE(c)	((BYTE)(((c) &  0x7E) >> 1))

// Some constants
#define		BYTES_FOR_NAL_SIZE			4
#define		MAX_NAL_NUM					12
#define		H265_SINGLE_HDR_LEN			2
#define		H265_FU_HDR_LEN				3

// Nal types
#define		H265_NAL_BLA_W_LP			16						// BLA
#define		H265_NAL_BLA_W_RADL			17
#define		H265_NAL_BLA_N_LP			18
#define		H265_NAL_IDR_W_RADL			19						// IDR
#define		H265_NAL_IDR_N_LP			20
#define		H265_NAL_CRA_NUT			21						// CRA
#define		H265_NAL_IRAP_VCL22			22						// IRAP
#define		H265_NAL_IRAP_VCL23			23

#define		H265_NAL_VPS				32						// VPS
#define		H265_NAL_SPS				33						// SPS
#define		H265_NAL_PPS				34						// PPS

// Nal types for RTP Pakcets
#define		H265_PACKET_TYPE_SINGLE					255		// Never to real H265 nals
#define		H265_PACKET_TYPE_AGGREGATION			48
#define		H265_PACKET_TYPE_FRAGMENT				49
#define		H265_PACKET_TYPE_PACI					50

typedef struct H265NalHdr_t
{
	UCHAR		m_layer0:1;			
	UCHAR		m_type:6;				// expect 0 
	UCHAR		m_forbidden:1;		// expect 0 
	UCHAR		m_temporal:3;			// expect 1
	UCHAR		m_layer1:5;			// expect 0 
} H265NalHdr;

typedef struct H265FUHdr_t
{
	H265NalHdr	m_NalHeader;
	UCHAR		m_FuType:6;			// expect 0 
	UCHAR		m_EBit:1;		
	UCHAR		m_SBit:1;			// 
} H265_FU_HDR;

typedef	struct
{
	DWORD32			m_dwHeaderSize;
	H265_FU_HDR		m_FuHdr;

	BYTE*			m_pFrame;
	DWORD32			m_dwFrameLen;
	DWORD32			m_dwNalNum;
	DWORD32			m_dwCurNal;
	DWORD32			m_dwCurNalLen;
	BOOL			m_bFU;
	BOOL			m_bFirst;

	BYTE*			m_pConfig;
	DWORD32			m_dwConfig;

} PacketH265Struct, *PacketH265Handle;

PacketH265Handle PacketH265Open(BYTE* pConfig, DWORD32 dwConfig);
void PacketH265Init(PacketH265Handle hH265, BOOL bUDP);
void PacketH265Close(PacketH265Handle hH265);
BOOL PacketH265PutData(PacketH265Handle hH265, BYTE* pData, DWORD32 dwData);
BYTE* PacketH265GetData(PacketH265Handle hH265, DWORD32* pdwPacket, BOOL* pbMBit);

#endif	// _PacketH265_H_
