/*****************************************************************************
*                                                                            *
*                            Packetizer Library								 *
*                                                                            *
*   Copyright (c) 2004 by Mcubeworks, Incoporated. All Rights Reserved.      *
******************************************************************************

    File Name       : PacketH264.h
    Author(s)       : Kim, Kun-Tae
    Created         : 11 Jan 2005

    Description     : File API
    Notes           : 

==============================================================================
                      Modification History (Reverse Order)                    
==============================================================================
    Date        Author      Location (variables) / Description
------------------------------------------------------------------------------

------------------------------------------------------------------------------*/

#ifndef _PacketH264_H_
#define _PacketH264_H_

#include "Packet.h"

#define		PACK_MODE_STAP_A					0x18
#define		PACK_MODE_STAP_B					0x19
#define		PACK_MODE_MTAP16					0x1A
#define		PACK_MODE_MTAP24					0x1B
#define		PACK_MODE_FU_A						0x1C
#define		PACK_MODE_FU_B						0x1D
#define		PACK_MODE_SKTAP						0x1F

#define		H264_HDR_LEN						1
#define		H264_FU_HDR_LEN						1
#define		H264_STAP_NAL_SIZE					2
#define		BYTES_FOR_NAL_SIZE					4

typedef	struct {
#ifdef	BIN_ENDIAN
	BYTE	m_F:1;				/* Forbidden_zero bit */
	BYTE	m_NRI:2;			/* Nal_ref_idc */
	BYTE	m_Type:5;			/* Nal_unit_type */
#else
	BYTE	m_Type:5;			/* Nal_unit_type */
	BYTE	m_NRI:2;			/* Nal_ref_idc */
	BYTE	m_F:1;				/* Forbidden_zero bit */
#endif
} H264_HDR;

typedef	struct {
#ifdef	BIN_ENDIAN
	BYTE	m_SBit:1;			/* Start bit */
	BYTE	m_EBit:1;			/* End bit */
	BYTE	m_RBit:1;			/* Reserved bit */
	BYTE	m_Type:5;			/* Nal_unit_paylod_type */
#else
	BYTE	m_Type:5;			/* Nal_unit_paylod_type */
	BYTE	m_RBit:1;			/* Reserved bit */
	BYTE	m_EBit:1;			/* End bit */
	BYTE	m_SBit:1;			/* Start bit */
#endif
} H264_FU_HDR;

typedef	struct
{
	H264_HDR		m_Header;
	H264_FU_HDR		m_FUStart;
	H264_FU_HDR		m_FUMid;
	H264_FU_HDR		m_FUEnd;

	DWORD32			m_dwHeaderSize;
	BYTE*			m_pFrame;
	DWORD32			m_dwFrameLen;
	DWORD32			m_dwNalNum;
	DWORD32			m_dwCurNal;
	DWORD32			m_dwNalLen;
	BOOL			m_bFU;
	BOOL			m_bFirst;

	BYTE*			m_pConfig;
	DWORD32			m_dwConfig;

} PacketH264Struct, *PacketH264Handle;

PacketH264Handle PacketH264Open(BYTE* pConfig, DWORD32 dwConfig);
void PacketH264Init(PacketH264Handle hH264, BOOL bUDP);
void PacketH264Close(PacketH264Handle hH264);
BOOL PacketH264PutData(PacketH264Handle hH264, BYTE* pData, DWORD32 dwData);
BYTE* PacketH264GetData(PacketH264Handle hH264, DWORD32* pdwPacket, BOOL* pbMBit);

#endif	// _PacketH264_H_
