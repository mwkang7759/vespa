/*****************************************************************************
*                                                                            *
*                            Packetizer Library								 *
*                                                                            *
*   Copyright (c) 2017 by Mcubeworks, Incoporated. All Rights Reserved.      *
******************************************************************************

    File Name       : PacketH265.c
    Author(s)       : CHANG, Joonho
    Created         : March 6 2017

    Description     : File API
    Notes           :

==============================================================================
                      Modification History (Reverse Order)
==============================================================================
    Date        Author      Location (variables) / Description
------------------------------------------------------------------------------

------------------------------------------------------------------------------*/

#include "PacketH265.h"

PacketH265Handle PacketH265Open(BYTE* pConfig, DWORD32 dwConfig)
{
	PacketH265Handle	hH265 = NULL;

	hH265 = (PacketH265Handle)MALLOCZ(sizeof(PacketH265Struct));
	if (hH265)
	{
		hH265->m_dwHeaderSize = RTP_HEADER_LENGTH;
		hH265->m_bFirst = TRUE;
		if (pConfig && dwConfig)
		{
			// When config comes from encoder or ts stream
			if( IS_4B_START_CODE(pConfig) || IS_3B_START_CODE(pConfig))
			{
				BYTE		*pNewBuffer;
				DWORD32		dwNewBuffer;

				// convert hvcC format
				pNewBuffer = (BYTE *)MALLOCZ(dwConfig+100);
				dwNewBuffer = make_hevc_config_data2(pNewBuffer, pConfig, dwConfig, NULL);
				hH265->m_pConfig = pNewBuffer;
				hH265->m_dwConfig = dwNewBuffer;
			}
			else
			{
				hH265->m_pConfig = pConfig;
				hH265->m_dwConfig = dwConfig;
			}
		}
	}

	return	hH265;
}

void PacketH265Init(PacketH265Handle hH265, BOOL bUDP)
{
	hH265->m_pFrame = NULL;
	hH265->m_dwFrameLen = 0;
	if (bUDP)
		hH265->m_dwHeaderSize = RTP_HEADER_LENGTH;
	else
		hH265->m_dwHeaderSize = RTP_HEADER_LENGTH + RTP_TCP_HEADER_LEN;

    hH265->m_bFU = FALSE;
}

void PacketH265Close(PacketH265Handle hH265)
{
	if (hH265)
	{
		FREE(hH265->m_pConfig);
		FREE(hH265);
	}
}

BOOL PacketH265PutData(PacketH265Handle hH265, BYTE* pData, DWORD32 dwData)
{
	DWORD32	dwNalLen;
	int		iLen;

	hH265->m_pFrame = pData;
	hH265->m_dwFrameLen = dwData;
	hH265->m_dwNalNum = 0;

	//TRACE("[Packet_H265]	PacketH265PutData - len(%d)", dwData);

	iLen = (int)dwData;
	//while (iLen > 0)
	while (iLen > 3)
	{
		dwNalLen = ConvByteToDWORD(pData);
		if ((DWORD32)iLen < dwNalLen + BYTES_FOR_NAL_SIZE)
			return	FALSE;
		iLen -= (dwNalLen + BYTES_FOR_NAL_SIZE);
		pData += (dwNalLen + BYTES_FOR_NAL_SIZE);
		hH265->m_dwNalNum++;
	}
	// when there is some junk data at the end
	if (iLen > 0)
		hH265->m_dwFrameLen -= iLen;
	hH265->m_dwCurNal = hH265->m_dwNalNum;

	return	TRUE;
}

BYTE* PacketH265GetData(PacketH265Handle hH265, DWORD32* pdwPacket, BOOL* pbMBit)
{
	BYTE*	pBuffer = NULL;
	BYTE*	pBufPos;
	DWORD32	dwBufLen;
	WORD	wNalSize;

	if ((int)hH265->m_dwFrameLen <= 0)
		return	NULL;

	dwBufLen = hH265->m_dwHeaderSize + RTP_MAX_MTU_SIZE;
	pBuffer = (BYTE*)MALLOC(dwBufLen);

	// RTP Header
	pBufPos = pBuffer + hH265->m_dwHeaderSize;
	dwBufLen = RTP_MAX_MTU_SIZE;
	*pdwPacket = hH265->m_dwHeaderSize;

	if (hH265->m_bFirst)
	{
		// Add here if some thing are necessary when streaming starts
		hH265->m_bFirst = FALSE;
	}

	// NAL Length
	if (hH265->m_bFU)
	{
		// Fragment end mode
		if (hH265->m_dwCurNalLen + H265_FU_HDR_LEN <= dwBufLen)
		{
			hH265->m_FuHdr.m_SBit = 0;
			hH265->m_FuHdr.m_EBit = 1;

			// Fragment unit header
			memcpy(pBufPos,	&hH265->m_FuHdr, H265_FU_HDR_LEN);
			pBufPos += H265_FU_HDR_LEN;
			dwBufLen -= H265_FU_HDR_LEN;
			*pdwPacket += H265_FU_HDR_LEN;

			memcpy(pBufPos, hH265->m_pFrame, hH265->m_dwCurNalLen);
			*pdwPacket += hH265->m_dwCurNalLen;
			hH265->m_pFrame += hH265->m_dwCurNalLen;
			hH265->m_dwFrameLen -= hH265->m_dwCurNalLen;
			if (!hH265->m_dwCurNal)
				*pbMBit = TRUE;
			else
				*pbMBit = FALSE;

			hH265->m_bFU = FALSE;
			//LOG_T("[Packet_H265]	FU_end len(%d), Header type(%d), NRI(%d), FU type(%d), Ebit(%d)",
			//		*pdwPacket, hH265->m_Header.m_Type, hH265->m_Header.m_NRI, hH265->m_FUEnd.m_Type, hH265->m_FUEnd.m_EBit);
		}
		// Fragment mid mode
		else
		{
			hH265->m_FuHdr.m_SBit = 0;
			hH265->m_FuHdr.m_EBit = 0;

			// Fragment unit header
			memcpy(pBufPos,	&hH265->m_FuHdr, H265_FU_HDR_LEN);
			pBufPos += H265_FU_HDR_LEN;
			dwBufLen -= H265_FU_HDR_LEN;
			*pdwPacket += H265_FU_HDR_LEN;

			memcpy(pBufPos, hH265->m_pFrame, dwBufLen);
			hH265->m_pFrame += dwBufLen;
			hH265->m_dwFrameLen -= dwBufLen;
			hH265->m_dwCurNalLen -= dwBufLen;

			*pdwPacket += dwBufLen;
			*pbMBit = FALSE;
			//LOG_T("[Packet_H265]	FU_mid len(%d), Header type(%d), NRI(%d), FU type(%d), Ebit(%d)",
			//	*pdwPacket, hH265->m_Header.m_Type, hH265->m_Header.m_NRI, hH265->m_FUMid.m_Type, hH265->m_FUMid.m_EBit);
		}
	}
	else
	{
		hH265->m_dwCurNalLen = ConvByteToDWORD(hH265->m_pFrame);
		hH265->m_pFrame += BYTES_FOR_NAL_SIZE;
		hH265->m_dwFrameLen -= BYTES_FOR_NAL_SIZE;

		//LOG_T("[Packet_H265]	NalLen(%d), NalNum(%d)", hH265->m_dwCurNalLen, hH265->m_dwCurNal);	// on test..

		// Single mode
		if (hH265->m_dwCurNalLen <= dwBufLen)
		{
			memcpy(pBufPos, hH265->m_pFrame, hH265->m_dwCurNalLen);
			hH265->m_pFrame += hH265->m_dwCurNalLen;
			hH265->m_dwFrameLen -= hH265->m_dwCurNalLen;
			*pdwPacket += hH265->m_dwCurNalLen;
			hH265->m_dwCurNal--;
			if (hH265->m_dwCurNal == 0)
				*pbMBit = TRUE;
			else
				*pbMBit = FALSE;
			//LOG_T("[Packet_H265]	Simple len(%d)", *pdwPacket);
		}
		// Fragment mode
		else
		{
			USHORT		sHeader;

			// Copy nal header of the current nal in FuHdr
			memcpy(&hH265->m_FuHdr, hH265->m_pFrame, 2);
			hH265->m_FuHdr.m_NalHeader.m_type = H265_PACKET_TYPE_FRAGMENT;
			hH265->m_FuHdr.m_SBit = 1;
			hH265->m_FuHdr.m_EBit = 0;
			hH265->m_FuHdr.m_FuType	= H265_NAL_TYPE(hH265->m_pFrame[0]);

			// Fragment unit header
			memcpy(pBufPos,	&hH265->m_FuHdr, H265_FU_HDR_LEN);
			pBufPos += H265_FU_HDR_LEN;
			dwBufLen -= H265_FU_HDR_LEN;
			*pdwPacket += H265_FU_HDR_LEN;

			hH265->m_pFrame += H265_SINGLE_HDR_LEN;
			hH265->m_dwFrameLen -= H265_SINGLE_HDR_LEN;
			hH265->m_dwCurNalLen -= H265_SINGLE_HDR_LEN;

			// 1st Fragment of the Nal
			memcpy(pBufPos, hH265->m_pFrame, dwBufLen);
			hH265->m_pFrame += dwBufLen;
			hH265->m_dwFrameLen -= dwBufLen;
			hH265->m_dwCurNalLen -= dwBufLen;
			*pdwPacket += dwBufLen;

			*pbMBit = FALSE;
			hH265->m_bFU = TRUE;

			hH265->m_dwCurNal--;
			//LOG_T("[Packet_H265]	FU_start len(%d), Header type(%d), NRI(%d), FU type(%d), Sbit(%d)",
			//		*pdwPacket, hH265->m_Header.m_Type, hH265->m_Header.m_NRI, hH265->m_FUStart.m_Type, hH265->m_FUStart.m_SBit);
		}
	}

	return	pBuffer;
}
