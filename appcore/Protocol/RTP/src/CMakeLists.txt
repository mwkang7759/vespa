cmake_minimum_required(VERSION 3.16)

# add all current files to SRC_FILES
file(GLOB_RECURSE SRC_FILES CONFIGURE_DEPENDS
	${CMAKE_CURRENT_SOURCE_DIR}/*.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/*.c
	)
# make static library
add_library(RTP STATIC ${SRC_FILES})

# header path for using compile library
target_include_directories(RTP PUBLIC ${CMAKE_SOURCE_DIR}/include)

# compile options
target_compile_options(RTP PRIVATE -Wall -Werror -Wno-error=misleading-indentation -Wno-error=unused-variable -Wno-error=unused-but-set-variable -DLinux)
