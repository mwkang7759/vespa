
#include "Rtp.h"

LRSLT FrRTPOpen(FrRTPHandle* phRTP, FrMediaHandle hMedia)
{
	FrRTPStruct*	hRTP;

	hRTP = (FrRTPStruct*)MALLOCZ(sizeof(FrRTPStruct));
	if (hRTP)
	{
		hRTP->m_bFirst = TRUE;
		hRTP->m_dwRTCPPeriod = 2000;
		hRTP->m_dwRTCPTime = FrGetTickCount() + (hRTP->m_dwRTCPPeriod>>2);
		hRTP->m_dwSSRC = (int)(RAND());
		if (hRTP->m_dwSSRC < 0x10000000)
			hRTP->m_dwSSRC = MAX_DWORD - hRTP->m_dwSSRC;

		if (hMedia)
		{
			hRTP->m_dwSampleRate = FrSDPGetMediaSampleRate(hMedia);
			hRTP->m_wSendPT = (WORD)FrSDPGetMediaPayloadType(hMedia);
		}

		hRTP->m_TotalQuSize = 20;
		hRTP->m_DSISeq = 1;
		hRTP->m_dwPrevTS = hRTP->m_dwLastRecvTS = (DWORD32)-1;

		*phRTP = hRTP;

		LOG_I("[RTP] FrRTPOpen - Samplerate(%d), RTCPPeriod(%d), RTCPTime(%d), SSRC(%d)", hRTP->m_dwSampleRate, hRTP->m_dwRTCPPeriod, hRTP->m_dwRTCPTime, hRTP->m_dwSSRC);
	}
	else
		return	COMMON_ERR_MEM;

	return	FR_OK;
}

void FrRTPClose(FrRTPHandle hHandle)
{
	FrRTPStruct*	hRTP = (FrRTPStruct*)hHandle;

	if (hRTP)
		FREE(hRTP);
}

void FrRTPMakeRTPPacket(FrRTPHandle hHandle, BYTE* pPacket, DWORD32 dwCTS, BOOL bMBit)
{
	FrRTPStruct*	hRTP = (FrRTPStruct*)hHandle;
	RTP_HEADER*		pRtpHdr = (RTP_HEADER*)pPacket;
	QWORD			qTS;
	//double			dTS;

	// temporal code, mwkang 2008.08.26
	// rounding up rtp timestamp to test the w400 terminal
	//dTS = (((double)dwCTS - hRTP->m_dwStartSendCTS) * hRTP->m_dwSampleRate / 1000.) + hRTP->m_dwStartSendTS;
	//qTS = (QWORD)ceil(dTS);

	qTS = (QWORD)(dwCTS - hRTP->m_dwStartSendCTS) * hRTP->m_dwSampleRate / 1000 + hRTP->m_dwStartSendTS;
	if (qTS > MAX_DWORD)
	{
		qTS -= MAX_DWORD;
		hRTP->m_dwStartSendTS = (DWORD32)qTS;
		hRTP->m_dwStartSendCTS = dwCTS;
	}
	hRTP->m_dwSendTS = (DWORD32)qTS;

#ifdef	_NOT_BIT_OP_
	pRtpHdr->wHead			= bMBit;				// marker bit
	pRtpHdr->wHead			<<= 7;
	pRtpHdr->wHead			|= hRTP->m_wSendPT;		// payload type
	pRtpHdr->wHead			<<= 2;
	pRtpHdr->wHead	 		|= RTP_VERSION;			// protocol version
	pRtpHdr->wHead			<<= 1;
	pRtpHdr->wHead	 		|= 0;					// padding flag
	pRtpHdr->wHead			<<= 1;
	pRtpHdr->wHead	 		|= 0;					// header extension flag
	pRtpHdr->wHead			<<= 4;
	pRtpHdr->wHead	 		|= 0;					// CSRC count
#else
	pRtpHdr->cc				= 0;
	pRtpHdr->x				= 0;
	pRtpHdr->p		 		= 0;
	pRtpHdr->version 		= RTP_VERSION;
	pRtpHdr->pt				= hRTP->m_wSendPT;
	pRtpHdr->m				= bMBit;
#endif

	pRtpHdr->wSeq			= htons(hRTP->m_wSendSeq);
	if (hRTP->m_wSendSeq == MAX_WORD)
		hRTP->m_wSendSeq = 0;
	else
		hRTP->m_wSendSeq++;
	pRtpHdr->dwTimestamp	= htonl(hRTP->m_dwSendTS);
	pRtpHdr->dwSSRC			= htonl(hRTP->m_dwSSRC);
}

void FrRTPMakeRTPPacketUseInputCTS(FrRTPHandle hHandle, BYTE* pPacket, DWORD32 dwCTS, BOOL bMBit)
{
	FrRTPStruct*	hRTP = (FrRTPStruct*)hHandle;
	RTP_HEADER*		pRtpHdr = (RTP_HEADER*)pPacket;
	QWORD			qTS;
	//double			dTS;

	qTS = (QWORD)dwCTS;
	if (qTS > MAX_DWORD)
	{
		qTS -= MAX_DWORD;
		hRTP->m_dwStartSendTS = (DWORD32)qTS;
		hRTP->m_dwStartSendCTS = dwCTS;
	}
	hRTP->m_dwSendTS = (DWORD32)qTS;

#ifdef	_NOT_BIT_OP_
	pRtpHdr->wHead			= bMBit;				// marker bit
	pRtpHdr->wHead			<<= 7;
	pRtpHdr->wHead			|= hRTP->m_wSendPT;		// payload type
	pRtpHdr->wHead			<<= 2;
	pRtpHdr->wHead	 		|= RTP_VERSION;			// protocol version
	pRtpHdr->wHead			<<= 1;
	pRtpHdr->wHead	 		|= 0;					// padding flag
	pRtpHdr->wHead			<<= 1;
	pRtpHdr->wHead	 		|= 0;					// header extension flag
	pRtpHdr->wHead			<<= 4;
	pRtpHdr->wHead	 		|= 0;					// CSRC count
#else
	pRtpHdr->cc				= 0;
	pRtpHdr->x				= 0;
	pRtpHdr->p		 		= 0;
	pRtpHdr->version 		= RTP_VERSION;
	pRtpHdr->pt				= hRTP->m_wSendPT;
	pRtpHdr->m				= bMBit;
#endif

	pRtpHdr->wSeq			= htons(hRTP->m_wSendSeq);
	if (hRTP->m_wSendSeq == MAX_WORD)
		hRTP->m_wSendSeq = 0;
	else
		hRTP->m_wSendSeq++;
	pRtpHdr->dwTimestamp	= htonl(hRTP->m_dwSendTS);
	pRtpHdr->dwSSRC			= htonl(hRTP->m_dwSSRC);
}

LRSLT FrRTPParseRTPPacket(FrRTPHandle hHandle, BYTE* pPacket, DWORD32 dwPacket, WORD* pwSeq, DWORD32 dwTick, DWORD32* pdwStartTS)
{
	FrRTPStruct*	hRTP = (FrRTPStruct*)hHandle;
	RTP_HEADER*		pRtpHdr = (RTP_HEADER*)pPacket;
	WORD			wSeq;
	DWORD32			dwTime, dwTS, dwSSRC, dwCTS;
	INT32			iDelay;
	LRSLT			lRet = FR_OK;

	//LOG_T("[RTP]	FrRTPParseRTPPacket - packet PT(%d)", pRtpHdr->pt);

	// RTP Header parsing
	wSeq = ntohs(pRtpHdr->wSeq);
	dwTS = ntohl(pRtpHdr->dwTimestamp);
	dwSSRC = ntohl(pRtpHdr->dwSSRC);

	//LOG_T("[RTP] Put - PT(%d) Size=%d Seq=%d TS=%u M=%d SSRC=%x", pRtpHdr->pt, dwPacket, wSeq, dwTS, (pPacket[1] & 0x80) >> 7, dwSSRC);

	if (pwSeq)
		*pwSeq = wSeq;

    if (dwTick)
        dwTime = dwTick;
    else
    	dwTime = FrGetTickCount();

	if (hRTP->m_bFirst)
	{
		hRTP->m_dwSSRC_1 = dwSSRC;
		hRTP->m_wStartRecvSeq = wSeq;
		hRTP->m_bFirst = FALSE;

		hRTP->m_dwBaseTick = dwTime;
		hRTP->m_dwBaseRtptime = dwTS;
		if (pdwStartTS)
		{
			LOG_I("[RTP]	FrRTPParseRTPPacket StartTS %u", dwTS);
			*pdwStartTS = dwTS;
		}
		hRTP->m_dwPrevTS = dwTS;
		LOG_I("[RTP]	FrRTPParseRTPPacket - First packet Seq=%d Tick=%lu TS=%lu SSRC=%x", wSeq, dwTime, dwTS, dwSSRC);
	}
	else
	{
		if (hRTP->m_dwSSRC_1 != dwSSRC)
		{
			hRTP->m_dwSSRC_1 = dwSSRC;
			hRTP->m_wStartRecvSeq = wSeq;
			lRet = COMMON_ERR_NOTMATCH_SSRC;
			LOG_I("[RTP]	FrRTPParseRTPPacket - Reset SSRC=%x", dwSSRC);
		}
		else
		{
			iDelay = (dwTime-hRTP->m_dwRecvTime);
			if (iDelay < 0)
				iDelay = -iDelay;
			hRTP->m_iJitter += iDelay - ((hRTP->m_iJitter+8)>>4);

			//LOG_T("[RTP] FrRTPParseRTPPacket - iDelay(%d), Jiter(%d)", iDelay, hRTP->m_iJitter);
		}

#if 0
		{
			DWORD32 dwTickDiff = McGetSubtract(dwTime, hRTP->m_dwBaseTick);
			DWORD32	dwRtpDiff = McGetSubtract(dwTS, hRTP->m_dwBaseRtptime) / (hRTP->m_dwSampleRate / 1000);
			DWORD32	dwRealDiff = McGetSubtract(dwTickDiff, dwRtpDiff);

			LOG_T("[RTP] Put - PT(%d) Seq=%d TS=%u, Diff Tick=%u, Rtp=%u, Real=%d",
				pRtpHdr->pt, wSeq, dwTS, dwTickDiff, dwRtpDiff, dwRealDiff);
			if (ABS(dwRealDiff) >= 1000)
				LOG_D("[RTP] Put - PT(%d) Read Diff=%d / Tick=%u, Rtp=%u TS=%u ==!!!",
					pRtpHdr->pt, dwRealDiff, dwTickDiff, dwRtpDiff, dwTS);
		}
#endif
	}

	hRTP->m_wLastRecvSeq = wSeq;
	hRTP->m_dwLastRecvTS = dwTS;
	hRTP->m_dwRecvByte += dwPacket;
	hRTP->m_dwRecvTime = dwTime;
	hRTP->m_dwRecvPacket++;

#ifdef	_NOT_BIT_OP_
	hRTP->m_bLastMBit = (pPacket[1] & 0x80) >> 7;
#else
	hRTP->m_bLastMBit = pRtpHdr->m;
#endif

	//dwCTS = floor((dwTS - hRTP->m_dwBaseRtptime) * 1000 / hRTP->m_dwSampleRate + 0.5);
	//LOG_E("[RTP] Put - PT(%d) Size=%d Seq=%d TS=%u(%d) M=%d SSRC=%d", pRtpHdr->pt, dwPacket, wSeq, dwTS, dwCTS, hRTP->m_bLastMBit, dwSSRC);

	return	lRet;
}

DWORD32 FrRTPMakeRTCPPacket(FrRTPHandle hHandle, BYTE* pBuffer, DWORD32 dwBufDuration, WORD wBufRemain, BOOL bSend, BOOL bBYE)
{
	FrRTPStruct*	hRTP = (FrRTPStruct*)hHandle;
	int				iSend = 0;
	DWORD32			dwCurrTime;

	dwCurrTime = FrGetTickCount();
	if ((dwCurrTime - hRTP->m_dwRTCPTime > hRTP->m_dwRTCPPeriod) && hRTP->m_dwSSRC_1)
	{
		if (bSend)
		{
			iSend += RTCPSRPacket(hRTP, pBuffer);
			if (bBYE)
				iSend += RTCPSRPacket(hRTP, pBuffer+iSend);
		}
		else
		{
			iSend += RTCPRRPacket(hRTP, pBuffer, dwBufDuration, wBufRemain);
			iSend += RTCPSDESPacket(hRTP, pBuffer+iSend);
			//iSend += RTCPBRPacket(hRTP, pBuffer+iSend);	// nhn
		}
		hRTP->m_dwRTCPTime = dwCurrTime;

		//LOG_T("[RTP] FrRTPMakeRTCPPacket - RTCPTime(%d), iSend(%d)", hRTP->m_dwRTCPTime, iSend);
	}

	return	(DWORD32)iSend;
}

BOOL FrRTPParseRTCPPacket(FrRTPHandle hHandle, BYTE* pPacket, DWORD32 dwPacket)
{
	FrRTPStruct*	hRTP = (FrRTPStruct*)hHandle;
#ifdef	_NOT_BIT_OP_
	BYTE*			pRTCPPacket;
	BYTE			PT;
	WORD			wTemp;
#else
	RTCP_PACKET*	pRTCPPacket;
#endif
	DWORD32			dwCurrPos = 0;
	DWORD32			dwRTCPLen;
	DWORD32			dwRecvTime = FrGetTickCount();

	//LOG_T("[RTP]	FrRTPParseRTCPPacket - Start..");

	while (dwPacket != dwCurrPos)
	{
#ifdef	_NOT_BIT_OP_
		pRTCPPacket = (BYTE*)(pPacket+dwCurrPos);
		memcpy((BYTE*)&wTemp, (BYTE*)&pRTCPPacket[2], 2);
		dwRTCPLen = (ntohs(wTemp)+1) * 4;
#else
		pRTCPPacket = (RTCP_PACKET*)(pPacket+dwCurrPos);
		dwRTCPLen = (ntohs((WORD)pRTCPPacket->common.length)+1) * 4;
#endif

		dwCurrPos += dwRTCPLen;
		if (dwCurrPos > dwPacket)
		{
			LOG_T("[RTP]	FrRTPParseRTCPPacket - Length Error iLen(%d), dwCurrPos(%d)", dwPacket, dwCurrPos);
			break;
		}
#ifdef	_NOT_BIT_OP_
		memcpy((BYTE*)&PT, (BYTE*)&pRTCPPacket[1], 1);
		switch (PT)
		{
#else
		// nhn..
		//LOG_T("[RTP]	FrRTPParseRTCPPacket - RTCP common type=%d", pRTCPPacket->common.pt);
		switch (pRTCPPacket->common.pt)
		{
#endif
		case RTCP_SR:
			{
				DWORD32	dwSec, dwFrac;
				DWORD32	dwSSRC;
				// on test..
				DWORD32	dwRtptime;

#ifdef	_NOT_BIT_OP_
				memcpy((BYTE*)&dwSSRC, (BYTE*)&pRTCPPacket[4], 4);
#else
				dwSSRC = ntohl(pRTCPPacket->r.sr.ssrc);
#endif
				if (hRTP->m_dwSSRC_1 && (hRTP->m_dwSSRC_1 != dwSSRC))
					LOG_I("[RTP]	FrRTPParseRTCPPacket - Recv SR Not matched SSRC=%x", dwSSRC);

				hRTP->m_dwRecvSRtime = dwRecvTime;
#ifdef	_NOT_BIT_OP_
				memcpy((BYTE*)&dwSec, (BYTE*)&pRTCPPacket[8], 4);
				dwSec = ntohl(dwSec) << 16;
				memcpy((BYTE*)&dwFrac, (BYTE*)&pRTCPPacket[12], 4);
				dwFrac = ntohl(dwFrac) >> 16;
#else
				dwSec = ntohl(pRTCPPacket->r.sr.ntp_sec) << 16;
				dwFrac = ntohl(pRTCPPacket->r.sr.ntp_frac) >> 16;
				dwRtptime = ntohl(pRTCPPacket->r.sr.rtp_ts);
#endif

				hRTP->m_iLSR = dwSec + dwFrac;

				//LOG_T("[RTP]	FrRTPParseRTCPPacket - Recv SR SSRC_1=%x, LSR=%d, RecvSRtime=%d, RTP time=%u", hRTP->m_dwSSRC_1, hRTP->m_iLSR, hRTP->m_dwRecvSRtime, dwRtptime);
			}
			break;
		case RTCP_RR:
			{
				DWORD32	dwSSRC;

#ifdef	_NOT_BIT_OP_
				memcpy((BYTE*)&dwSSRC, (BYTE*)&pRTCPPacket[4], 4);
				dwSSRC = ntohl(dwSSRC);
#else
				dwSSRC = ntohl(pRTCPPacket->r.rr.dwSSRC);
#endif
				if (!hRTP->m_dwSSRC_1)
					hRTP->m_dwSSRC_1 = dwSSRC;
				else
				{
					if (hRTP->m_dwSSRC_1 != dwSSRC)
						LOG_I("[RTP]	FrRTPParseRTCPPacket - Recv RR Not matched SSRC=%x", dwSSRC);
				}
				//LOG_T("[RTP]	FrRTPParseRTCPPacket - Recv RR SSRC_1=%d", hRTP->m_dwSSRC_1);
			}
			break;
		case RTCP_SDES:
#ifdef	_NOT_BIT_OP_
			TRACE("[RTP]	FrRTPParseRTCPPacket - Recv SDES");
#else
			switch(pRTCPPacket->r.sdes.item->type)
			{
			case RTCP_SDES_END:
				LOG_T("[RTP]	FrRTPParseRTCPPacket - Recv SDES");
				break;
			case RTCP_SDES_CNAME:
				//LOG_T("[RTP]	FrRTPParseRTCPPacket - Recv SDES_CNAME");
				//if (pRTCPPacket->r.sdes.item->length) {
				//	char cname[256];

				//	memcpy(cname, pRTCPPacket->r.sdes.item->data, pRTCPPacket->r.sdes.item->length);
				//	cname[pRTCPPacket->r.sdes.item->length] = (char)0;
				//}
				break;
			case RTCP_SDES_NAME:
				LOG_T("[RTP]	FrRTPParseRTCPPacket - Recv SDES_NAME");
				break;
			case RTCP_SDES_EMAIL:
				LOG_T("[RTP]	FrRTPParseRTCPPacket - Recv SDES_EMAIL");
				break;
			case RTCP_SDES_PHONE:
				LOG_T("[RTP]	FrRTPParseRTCPPacket - Recv SDES_PHONE");
				break;
			case RTCP_SDES_LOC:
				LOG_T("[RTP]	FrRTPParseRTCPPacket - Recv SDES_LOC");
				break;
			case RTCP_SDES_TOOL:
				LOG_T("[RTP]	FrRTPParseRTCPPacket - Recv SDES_TOOL");
				break;
			case RTCP_SDES_NOTE:
				LOG_T("[RTP]	FrRTPParseRTCPPacket - Recv SDES_NOTE");
				break;
			case RTCP_SDES_PRIV:
				LOG_T("[RTP]	FrRTPParseRTCPPacket - Recv SDES_PRIV");
				break;
			default:
				break;
			}
#endif
			break;
		case RTCP_BYE:
			hRTP->m_bBYE = TRUE;
			LOG_T("[RTP]	FrRTPParseRTCPPacket - Recv BYE");
			break;
		case RTCP_APP:
			LOG_T("[RTP]	FrRTPParseRTCPPacket - Recv APP");
			break;
		default:
			break;
		}
	}

	return	TRUE;
}

void FrRTPSetStartTS(FrRTPHandle hHandle, DWORD32 dwStartTS)
{
	FrRTPStruct*	hRTP = (FrRTPStruct*)hHandle;

	hRTP->m_dwStartTS = dwStartTS;
	hRTP->m_dwStartCTS = (DWORD32)((QWORD)dwStartTS * 1000 / hRTP->m_dwSampleRate);
	hRTP->m_dwPacketCnt = 0;
	hRTP->m_dwPrevTS = hRTP->m_dwLastRecvTS = dwStartTS;
	hRTP->m_bSetStartTS = TRUE;
	LOG_I("[RTP]	FrRTPSetStartTS - StartTS = %lu", hRTP->m_dwStartTS);
}

void FrRTPSetPT(FrRTPHandle hHandle, WORD wPT)
{
	FrRTPStruct*	hRTP = (FrRTPStruct*)hHandle;

	if (wPT)
		hRTP->m_wSendPT = wPT;
}

DWORD32 FrRTPGetCTS(FrRTPHandle hHandle, BYTE* pPacket, BOOL bRewind)
{
	FrRTPStruct*	hRTP = (FrRTPStruct*)hHandle;
	RTP_HEADER*		pRtpHdr = (RTP_HEADER*)pPacket;
	WORD			wSeq;
	DWORD32			dwTS, dwCTS;
	BOOL			bPlus = TRUE, bWrapDown = FALSE, bWrapUp = FALSE;
	INT32			nTSInc = 0;

	// RTP Header parsing
	wSeq = ntohs(pRtpHdr->wSeq);
	dwTS = ntohl(pRtpHdr->dwTimestamp);
#ifdef	_NOT_BIT_OP_
	hRTP->m_bMBit = (pPacket[1] & 0x80) >> 7;
#else
	hRTP->m_bMBit = pRtpHdr->m;
#endif

	// check whether timestamp is wrapped, modified mwkang 2007.02.13
	if (hRTP->m_dwPacketCnt)
	{
		// TS inversion
		if (dwTS < hRTP->m_dwPrevTS && hRTP->m_wPrevSeq < wSeq)
		{
			// Trace 2 times only
			if (hRTP->m_dwVideoBFrames < 2)
				LOG_I("[RTP] FrRTPGetCTS - rtp time stamp turn around....Prev(%u), Cur(%u)", hRTP->m_dwPrevTS, dwTS);
			if ((MAX_DWORD / 2) < (hRTP->m_dwPrevTS - dwTS))
			{
				bWrapDown = TRUE;
				LOG_I("[RTP] FrRTPGetCTS - Wrap down....Prev(%u), Cur(%u)", hRTP->m_dwPrevTS, dwTS);
			}
			else
				// Probably a B Frame are met
				hRTP->m_dwVideoBFrames++;
		}
		// TS jump more than MAX_DWORD / 2
		else if (hRTP->m_dwPrevTS < dwTS && (MAX_DWORD / 2) < (dwTS - hRTP->m_dwPrevTS))
		{
			LOG_I("[RTP] FrRTPGetCTS - Jump up....Prev(%u), Cur(%u)", hRTP->m_dwPrevTS, dwTS);
			bWrapUp = TRUE;
		}
	}

	// Catch 1st TS as a starting TS
	if (hRTP->m_bSetStartTS==FALSE)
	{
		FrRTPSetStartTS(hHandle, dwTS);
		nTSInc = 0;
	}
	else
	{
		// It could be negative
		if (hRTP->m_dwPacketCnt)
		{
			if (bWrapDown)
				nTSInc =  dwTS + MAX_QWORD - hRTP->m_dwPrevTS;
			else if (bWrapUp)
				nTSInc =  dwTS - MAX_QWORD - hRTP->m_dwPrevTS;
			else
				nTSInc = dwTS - hRTP->m_dwPrevTS;
		}
		else
			nTSInc = dwTS - hRTP->m_dwStartTS;
	}

	// seq check
	hRTP->m_wPrevSeq = wSeq;
	hRTP->m_dwPrevTS = dwTS;

	// don't need any more becasue inversion does not cause negative time stamp thanks to the line 547
#if 0
	// Time inversion happens. Due to wrap around or B frames
	if (nTSInc<0 && hRTP->m_dwPacketCnt==0)
	{
		FrRTPSetStartTS(hHandle, dwTS);
			LOG_I("[RTP]	FrRTPParseRTPPacket - Reset StartTS = %u", hRTP->m_dwStartTS);
	}
#endif

	if (bWrapDown || bWrapUp)
	{
		QWORD	qwMaxTS;

		LOG_T("[RTP]	FrRTPGetCTS - WrapDown(%d) or WrapUp (%d)", bWrapDown, bWrapUp);

		if (bWrapDown)
			hRTP->m_dwWrappedCnt++;
		else if (bWrapUp)
			hRTP->m_dwWrappedCnt--;
		qwMaxTS = MAX_DWORD;
		qwMaxTS = (qwMaxTS + 1) * hRTP->m_dwWrappedCnt;
		hRTP->m_dwWrappedCTS = (DWORD32)(qwMaxTS * 1000 / hRTP->m_dwSampleRate);

		LOG_I("[RTP] FrRTPGetCTS - Wrap around ....WrapCTS(%lu), qwMaxTS(%lu)", hRTP->m_dwWrappedCTS, qwMaxTS);
	}

	if (bPlus)
		dwCTS = (DWORD32)((float)dwTS * 1000 / hRTP->m_dwSampleRate);
	else
		dwCTS = (DWORD32)((float)(INT32)dwTS * 1000 / hRTP->m_dwSampleRate);;

	if (hRTP->m_dwWrappedCnt)
		dwCTS += hRTP->m_dwWrappedCTS;

	// Must be corrected when a live stream is served longer than about 40 daies
	if (hRTP->m_dwStartCTS <= dwCTS || bRewind)
		dwCTS -= hRTP->m_dwStartCTS;
	else
		dwCTS = 0;
	hRTP->m_dwPacketCnt++;

	return	dwCTS;
}

DWORD32 FrRTPGetRtpTime(BYTE* pPacket)
{
	RTP_HEADER*		pRtpHdr = (RTP_HEADER*)pPacket;
	WORD			wSeq;
	DWORD32			dwRTPTime;

	// RTP Header parsing
	wSeq = ntohs(pRtpHdr->wSeq);
	dwRTPTime = ntohl(pRtpHdr->dwTimestamp);

	return dwRTPTime;
}

DWORD32 FrRTPGetBFrames(FrRTPHandle hHandle)
{
	FrRTPStruct*	hRTP = (FrRTPStruct*)hHandle;

	return hRTP->m_dwVideoBFrames;
}

DWORD32 FrRTPGetRecvTS(FrRTPHandle hHandle)
{
	FrRTPStruct*	hRTP = (FrRTPStruct*)hHandle;
	return	hRTP->m_dwPrevTS;
}

DWORD32 FrRTPGetLastRecvTS(FrRTPHandle hHandle)
{
	FrRTPStruct*	hRTP = (FrRTPStruct*)hHandle;
	return	hRTP->m_dwLastRecvTS;
}

WORD FrRTPGetRecvSeq(FrRTPHandle hHandle)
{
	FrRTPStruct*	hRTP = (FrRTPStruct*)hHandle;
	return	hRTP->m_wPrevSeq;
}

BOOL FrRTPGetRecvMBit(FrRTPHandle hHandle)
{
	FrRTPStruct*	hRTP = (FrRTPStruct*)hHandle;
	return	hRTP->m_bMBit;
}

BOOL FrRTPGetLastRecvMBit(FrRTPHandle hHandle)
{
	FrRTPStruct*	hRTP = (FrRTPStruct*)hHandle;
	return	hRTP->m_bLastMBit;
}

DWORD32 FrRTPGetSendTS(FrRTPHandle hHandle)
{
	FrRTPStruct*	hRTP = (FrRTPStruct*)hHandle;
	return	hRTP->m_dwSendTS;
}

WORD FrRTPGetSendSeq(FrRTPHandle hHandle)
{
	FrRTPStruct*	hRTP = (FrRTPStruct*)hHandle;

	if (!hRTP->m_wSendSeq)
		return	MAX_WORD;
	return	hRTP->m_wSendSeq-1;
}

DWORD32 FrRTPGetSSRC(FrRTPHandle hHandle)
{
	FrRTPStruct*	hRTP = (FrRTPStruct*)hHandle;
	return	hRTP->m_dwSSRC;
}

BOOL FrRTPIsRTCPBye(FrRTPHandle hHandle)
{
	FrRTPStruct*	hRTP = (FrRTPStruct*)hHandle;
	return	hRTP->m_bBYE;
}

WORD FrRTPGetPT(FrRTPHandle hHandle)
{
	FrRTPStruct*	hRTP = (FrRTPStruct*)hHandle;
	return	hRTP->m_wSendPT;
}


// nhn on air
DWORD32 FrRTPMakeFeedbackRTCPPacket(FrRTPHandle hHandle, BYTE* pBuffer, WORD wFbType)
{
	FrRTPStruct*	hRTP = (FrRTPStruct*)hHandle;
	int				iSend = 0;

	iSend += RTCPAPPPacket(hRTP, pBuffer, wFbType);

	LOG_I("[RTP] FrRTPMakeFeedbackRTCPPacket - Fbtype (%d)", wFbType);

	return	(DWORD32)iSend;
}

DWORD32 FrRTPGetLastCTS(FrRTPHandle hHandle)
{
	FrRTPStruct*	hRTP = (FrRTPStruct*)hHandle;
	DWORD32			dwCTS;

	dwCTS = (DWORD32)((float)hRTP->m_dwLastRecvTS * 1000 / hRTP->m_dwSampleRate);

	return dwCTS;
}

DWORD32 FrRTPGetBufferedDurationInMsec(FrRTPHandle hHandle)
{
	FrRTPStruct*	hRTP = (FrRTPStruct*)hHandle;
	QWORD			qwBufferedDur;
	DWORD32			dwDur;

	if (hRTP->m_dwPrevTS == (DWORD32)-1 || hRTP->m_dwLastRecvTS == (DWORD32)-1)
		qwBufferedDur = 0;
	else 	if (hRTP->m_dwPrevTS <= hRTP->m_dwLastRecvTS)
		qwBufferedDur = hRTP->m_dwLastRecvTS - hRTP->m_dwPrevTS;
	else
	{
		// Timestamp wrap aroud
		if (MAX_DWORD/2 < hRTP->m_dwPrevTS-hRTP->m_dwLastRecvTS)
			qwBufferedDur = (QWORD)hRTP->m_dwLastRecvTS + MAX_DWORD - hRTP->m_dwPrevTS;
		// Temporary reversal
		else
			qwBufferedDur = 0;
	}		
	
	//TRACE("FrRTPGetBufferedDurationInMsec StartTS %8u LastTS %8u Buf %5lld", hRTP->m_dwPrevTS, hRTP->m_dwLastRecvTS, qwBufferedDur);

	dwDur = (DWORD32)(qwBufferedDur * 1000 / hRTP->m_dwSampleRate);
	
	return dwDur > 50 ? dwDur-50 : 0;
}

DWORD32 FrRTPGetInitialDurationInMsec(FrRTPHandle hHandle, DWORD32 dwStartCTS)
{
	FrRTPStruct*	hRTP = (FrRTPStruct*)hHandle;
	QWORD			qwBufferedDur;
	DWORD32			dwDur;

	if (hRTP->m_dwPrevTS == (DWORD32)-1 || hRTP->m_dwLastRecvTS == (DWORD32)-1)
		qwBufferedDur = 0;
	else if (dwStartCTS <= hRTP->m_dwLastRecvTS)
		qwBufferedDur = hRTP->m_dwLastRecvTS - dwStartCTS;
	else
	{
		// Timestamp wrap aroud
		if (MAX_DWORD/2 < dwStartCTS-hRTP->m_dwLastRecvTS)
			qwBufferedDur = (QWORD)hRTP->m_dwLastRecvTS + MAX_DWORD - dwStartCTS;
		// Temporary reversal
		else
			qwBufferedDur = 0;
	}		

	//TRACE("FrRTPGetInitialDurationInMsec StartTS %8u LastTS %8u Buf %5lld", dwStartCTS, hRTP->m_dwLastRecvTS, qwBufferedDur);

	dwDur = (DWORD32)(qwBufferedDur * 1000 / hRTP->m_dwSampleRate);
	
	return dwDur > 50 ? dwDur-50 : 0;
}

DWORD32 FrRTPGetSSRCFromRTCP(BYTE* pBuffer) {
	RTCP_PACKET*	pRTCPPacket = (RTCP_PACKET*)(pBuffer);
	DWORD32			dwSSRC = ntohl(pRTCPPacket->r.sr.ssrc);

	//dwRTCPLen = (ntohs((WORD)pRTCPPacket->common.length)+1) * 4;

	return dwSSRC;
}