
#include "Rtp.h"

int RTCPRRPacket(FrRTPStruct* hRTP, BYTE *pBuf, DWORD32 dwBufDuration, WORD wBufRemain)
{
	RTCP_PACKET*	pPacket = (RTCP_PACKET*)pBuf;
	WORD			wTotalRecvSeq;
	DWORD32			dwPartRecvSeq;
	DWORD32			dwPartRecv, dwPartLost, dwFraction, dwDLSR;
	int				iLost;
	WORD			Qu, QuSize;

	wTotalRecvSeq = hRTP->m_wLastRecvSeq - hRTP->m_wStartRecvSeq + 1;
	dwPartRecvSeq = wTotalRecvSeq - hRTP->m_wTotalPrevRecvSeq;
	dwPartRecv = hRTP->m_dwRecvPacket - hRTP->m_dwPrevRecvPacket;

	hRTP->m_wTotalPrevRecvSeq = wTotalRecvSeq;
	hRTP->m_dwPrevRecvPacket = hRTP->m_dwRecvPacket;

	dwPartLost = dwPartRecvSeq - dwPartRecv;

	if (dwPartRecvSeq == 0 || dwPartLost <= 0) 
		dwFraction = 0;
	else
		dwFraction = (dwPartLost << 8) / dwPartRecvSeq;
	iLost = (wTotalRecvSeq - hRTP->m_dwRecvPacket) & 0xffffff;

	dwDLSR = (DWORD32)((float)(FrGetTickCount()-hRTP->m_dwRecvSRtime) * 65.536);

	//LOG_T("[RTP] RTCPRRPacket - dwPartLost(%d), dwFraction(%d), dwDLSR (%d)", dwPartLost, dwFraction, dwDLSR);

#ifdef	_NOT_BIT_OP_
	pPacket->common.dwHead			= htons(8);			// length
	pPacket->common.dwHead			<<= 8;
	pPacket->common.dwHead			|= RTCP_RR;			// pt
	pPacket->common.dwHead			<<= 2;
	pPacket->common.dwHead	 		|= RTP_VERSION;		// version
	pPacket->common.dwHead			<<= 1;
	pPacket->common.dwHead	 		|= 0;				// count
	pPacket->common.dwHead			<<= 5;
	pPacket->common.dwHead	 		|= 1;				// count
#else
	pPacket->common.version 		= RTP_VERSION;
	pPacket->common.pt				= RTCP_RR;
	pPacket->common.p				= 0;
	pPacket->common.count			= 1;
	pPacket->common.length			= htons(8);
#endif

	//LOG_T("[RTP] RTCPRRPacket - dwSSRC(%lx), rr[0].ssrc(%lx)", hRTP->m_dwSSRC, hRTP->m_dwSSRC_1);

	pPacket->r.rr.dwSSRC			= htonl(hRTP->m_dwSSRC);
	pPacket->r.rr.rr[0].ssrc		= htonl(hRTP->m_dwSSRC_1);
#ifdef	_NOT_BIT_OP_
	pPacket->r.rr.rr[0].dwHead		= htonl(iLost<<8);
	pPacket->r.rr.rr[0].dwHead		<<= 8;
	pPacket->r.rr.rr[0].dwHead		|= dwFraction;
#else
	pPacket->r.rr.rr[0].fraction	= dwFraction;
	pPacket->r.rr.rr[0].lost		= htonl(iLost<<8);
#endif
	pPacket->r.rr.rr[0].last_seq	= htonl(hRTP->m_wLastRecvSeq);
	pPacket->r.rr.rr[0].jitter		= htonl(hRTP->m_iJitter>>4);
	pPacket->r.rr.rr[0].lsr			= htonl(hRTP->m_iLSR);
	pPacket->r.rr.rr[0].dlsr		= htonl(dwDLSR);

	// DSI
	Qu = (BYTE)(dwBufDuration / 500 );
	if (Qu > 64)
		Qu = 64;
	QuSize = hRTP->m_TotalQuSize;

#ifdef	_NOT_BIT_OP_
	pPacket->r.rr.rr[0].dsi.DSIInfo		= hRTP->m_DSISeq;
	pPacket->r.rr.rr[0].dsi.DSIInfo		<<= 12;
	pPacket->r.rr.rr[0].dsi.DSIInfo		|= wBufRemain;
	pPacket->r.rr.rr[0].dsi.DSIInfo		<<= 6;
	pPacket->r.rr.rr[0].dsi.DSIInfo		|= QuSize;
	pPacket->r.rr.rr[0].dsi.DSIInfo		<<= 6;
	pPacket->r.rr.rr[0].dsi.DSIInfo		|= Qu;
#else
	pPacket->r.rr.rr[0].dsi.Qu			= Qu;
	pPacket->r.rr.rr[0].dsi.QuSize		= QuSize;
	pPacket->r.rr.rr[0].dsi.BugRemain	= wBufRemain;
	pPacket->r.rr.rr[0].dsi.Seq			= hRTP->m_DSISeq;
#endif

	if (hRTP->m_DSISeq == MAX_BYTE)
		hRTP->m_DSISeq = 0;
	else
		hRTP->m_DSISeq++;

	return	36;
}

int RTCPSRPacket(FrRTPStruct* hRTP, BYTE *pBuf)
{
	RTCP_PACKET*	pPacket = (RTCP_PACKET*)pBuf;

#ifdef	_NOT_BIT_OP_
	pPacket->common.dwHead			= htons(5);			// length
	pPacket->common.dwHead			<<= 8;
	pPacket->common.dwHead			|= RTCP_SR;			// pt
	pPacket->common.dwHead			<<= 2;
	pPacket->common.dwHead	 		|= RTP_VERSION;		// version
	pPacket->common.dwHead			<<= 1;
	pPacket->common.dwHead	 		|= 0;				// count
	pPacket->common.dwHead			<<= 5;
	pPacket->common.dwHead	 		|= 1;				// count
#else
	pPacket->common.version 		= RTP_VERSION;
	pPacket->common.pt				= RTCP_SR;
	pPacket->common.p				= 0;
	pPacket->common.count			= 1;
	pPacket->common.length			= htons(5);
#endif

	pPacket->r.sr.ssrc				= htonl(hRTP->m_dwSSRC);
	pPacket->r.sr.ntp_sec			= htonl(1);
	pPacket->r.sr.ntp_frac			= htonl(0);
	pPacket->r.sr.rtp_ts			= htonl(hRTP->m_dwSendTS);
	pPacket->r.sr.psent				= htonl(1);
	pPacket->r.sr.osent				= htonl(1);

	return	24;
}

int RTCPSDESPacket(FrRTPStruct* hRTP, BYTE *pBuf)
{
	RTCP_PACKET*	pPacket = (RTCP_PACKET*)pBuf;
	char			strCName[] = "owner@mcubeworks.com";
	int				ilength, iLen;
	int				l;

#ifdef	_NOT_BIT_OP_
	pPacket->common.dwHead 		= RTCP_SDES;	// pt
	pPacket->common.dwHead		<<= 2;
	pPacket->common.dwHead 		|= RTP_VERSION;	// version
	pPacket->common.dwHead		<<= 1;
	pPacket->common.dwHead 		|= 0;			// padding
	pPacket->common.dwHead		<<= 5;
	pPacket->common.dwHead 		|= 1;			// count SC bits
#else
	pPacket->common.version 	= RTP_VERSION;
	pPacket->common.pt 			= RTCP_SDES;
	pPacket->common.p 			= 0;
	pPacket->common.count 		= 1;		//SC bits
#endif
	/* the length field will be put in later */
	pPacket->r.sdes.dwSSRC 		= htonl(hRTP->m_dwSSRC);

	/* populate the type and length fields of the first and only SDES item */
	pPacket->r.sdes.item->type	= RTCP_SDES_CNAME;
	pPacket->r.sdes.item->length = (BYTE)strlen(strCName);
	memcpy(pPacket->r.sdes.item->data, strCName, pPacket->r.sdes.item->length);

	ilength = pPacket->r.sdes.item->length + 1 + 1 + 4;
	/* round temp_length to the next multiple of 4 */
	ilength = (ilength +3) & 0xfffc;
	iLen = 4 + ilength;
#ifdef	_NOT_BIT_OP_
	pPacket->common.dwHead |= (htons((unsigned short)((iLen)/4-1)))<<16;
#else
	pPacket->common.length = htons((unsigned short)((iLen)/4-1));
#endif

	pBuf += (10+ pPacket->r.sdes.item->length);
	/* or == += iLen + delta */
	l=(iLen - 10 - pPacket->r.sdes.item->length);
	if (l == 0)
	{
		memset(pBuf, 0, 4);
		iLen += 4;
		//pPacket->r.sdes.item->length += 4;
#ifdef	_NOT_BIT_OP_
		pPacket->common.dwHead &= 0xffff0000;
		pPacket->common.dwHead |= (htons((unsigned short)((iLen)/4-1)))<<16;
#else
		pPacket->common.length = htons((unsigned short)((iLen)/4-1));
#endif
	} else
		memset(pBuf, RTCP_SDES_END, l);

	return	iLen;
}

int RTCPBYEPacket(FrRTPStruct* hRTP, BYTE *pBuf)
{
	RTCP_PACKET*	pPacket = (RTCP_PACKET*)pBuf;

#ifdef	_NOT_BIT_OP_
	pPacket->common.dwHead		= htons(1);		// length
	pPacket->common.dwHead		<<= 8;
	pPacket->common.dwHead 		|= RTCP_BYE;	// pt
	pPacket->common.dwHead		<<= 2;
	pPacket->common.dwHead 		|= RTP_VERSION;	// version
	pPacket->common.dwHead		<<= 1;
	pPacket->common.dwHead 		|= 0;			// padding
	pPacket->common.dwHead		<<= 5;
	pPacket->common.dwHead 		|= 0;			// count SC bits
#else
	pPacket->common.version 	= RTP_VERSION;
	pPacket->common.pt 			= RTCP_BYE;
	pPacket->common.p 			= 0;
	pPacket->common.count 		= 0;			//SC bits
	pPacket->common.length		= htons(1);
#endif

	pPacket->r.dwBye			= 1;

	return	8;
}

int RTCPBRPacket(FrRTPStruct* hRTP, BYTE *pBuf)
{
	RTCP_PACKET*	pPacket = (RTCP_PACKET*)pBuf;
	char			szName[] = "bill";

#ifdef	_NOT_BIT_OP_
	pPacket->common.dwHead		= htons(8);
	pPacket->common.dwHead		<<= 8;
	pPacket->common.dwHead 		|= RTCP_APP;		// pt
	pPacket->common.dwHead		<<= 2;
	pPacket->common.dwHead 		|= RTP_VERSION;	// version
	pPacket->common.dwHead		<<= 1;
	pPacket->common.dwHead 		|= 0;			// padding
	pPacket->common.dwHead		<<= 5;
	pPacket->common.dwHead 		|= 0;			// count SC bits	
#else
	pPacket->common.version 	= RTP_VERSION;	// version
	pPacket->common.pt 			= RTCP_APP;		// pt
	pPacket->common.p 			= 0;			// padding
	pPacket->common.count 		= 0;			// count SC bits
	pPacket->common.length		= htons(8);
#endif

	pPacket->r.br.dwSSRC 		= htonl(hRTP->m_dwSSRC);
	memcpy(&pPacket->r.br.dwName, szName, 4);
	pPacket->r.br.dwSN 			= htonl(hRTP->m_dwBRSeq);
	pPacket->r.br.dwLastSN 		= htonl(hRTP->m_wLastRecvSeq);
	pPacket->r.br.dwTS 			= htonl(hRTP->m_dwRTCPTime);
	pPacket->r.br.dwLastTS 		= htonl(hRTP->m_dwLastRecvTS);
	pPacket->r.br.dwRecvBytes 	= htonl(hRTP->m_dwRecvByte);
	pPacket->r.br.dwRecvPacket 	= htonl(hRTP->m_dwRecvPacket);

	if (hRTP->m_dwBRSeq == MAX_DWORD)
		hRTP->m_dwBRSeq = 0;
	else
		hRTP->m_dwBRSeq++;

	return	36;
}

// nhn on air
INT32 RTCPAPPPacket(FrRTPStruct* hRTP, BYTE *pBuf, WORD wFbType)
{
	RTCP_PACKET*	pPacket = (RTCP_PACKET*)pBuf;
	char			szName[] = "amdf";

#ifdef	_NOT_BIT_OP_
	pPacket->common.dwHead 		= RTCP_SDES;	// pt
	pPacket->common.dwHead		<<= 2;
	pPacket->common.dwHead 		|= RTP_VERSION;	// version
	pPacket->common.dwHead		<<= 1;
	pPacket->common.dwHead 		|= 0;			// padding
	pPacket->common.dwHead		<<= 5;
	pPacket->common.dwHead 		|= 1;			// count SC bits
#else
	pPacket->common.version 	= RTP_VERSION;
	pPacket->common.p 			= 0;
	pPacket->common.count 		= 0x02;				//SC bits
	pPacket->common.pt 			= RTCP_APP;			//0x18;
	pPacket->common.length		= htons(3);
#endif

	pPacket->r.app.dwSSRC	= htonl(hRTP->m_dwSSRC);
	memcpy(&pPacket->r.app.dwName, szName, 4);
	pPacket->r.app.wFbType	= htons(wFbType);
	pPacket->r.app.wFbLength = htons(0);

	return 16;
}
