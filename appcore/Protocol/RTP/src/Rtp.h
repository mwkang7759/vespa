
#ifndef	_RTP_H_
#define	_RTP_H_

#include "SocketAPI.h"
#include "SystemAPI.h"
#include "TraceAPI.h"
#include "mediainfo.h"
#include "SdpAPI.h"
#include "RtpAPI.h"

/////////////
// RTP
/////////////
#define		RTP_VERSION					2

/////////////
// RTCP
/////////////
typedef enum {
	RTCP_SR   = 200,
	RTCP_RR   = 201,
	RTCP_SDES = 202,
	RTCP_BYE  = 203,
	RTCP_APP  = 204
} RTCP_TYPE;

typedef enum {
	RTCP_SDES_END   = 0,
	RTCP_SDES_CNAME = 1,
	RTCP_SDES_NAME  = 2,
	RTCP_SDES_EMAIL = 3,
	RTCP_SDES_PHONE = 4,
	RTCP_SDES_LOC   = 5,
	RTCP_SDES_TOOL  = 6,
	RTCP_SDES_NOTE  = 7,
	RTCP_SDES_PRIV  = 8
} RTCP_SDES_TYPE;

typedef	struct {
#ifdef	_NOT_BIT_OP_
	DWORD32		DSIInfo;
#else
	DWORD32		Qu:6;
	DWORD32		QuSize:6;
	DWORD32		BugRemain:12;
	DWORD32		Seq:8;
#endif
} DSI_TYPE;

typedef struct {
#ifdef	_NOT_BIT_OP_
	DWORD32		dwHead;
#else
	DWORD32		count:5;		/* varies by packet type */
	DWORD32		p:1;			/* padding flag */
	DWORD32		version:2;		/* protocol version */
	DWORD32		pt:8;			/* RTCP packet type */
	DWORD32		length:16;		/* pkt len in words, w/o this word */
#endif
} RTCP_COMMON;

typedef struct {
	DWORD32		ssrc;			/* data source being reported */
#ifdef	_NOT_BIT_OP_
	DWORD32		dwHead;
#else
	DWORD32		fraction:8;		/* fraction lost since last SR/RR */
	DWORD32		lost:24;		/* cumul. no. pkts lost (signed!) */
#endif
	DWORD32		last_seq;		/* extended last seq. no. received */
	DWORD32		jitter;			/* interarrival jitter */
	DWORD32		lsr;			/* last SR packet from this source */
	DWORD32		dlsr;			/* delay since last SR packet */

	DSI_TYPE	dsi;
} RTCP_RR_T;

typedef	struct {
	DWORD32		ssrc;			/* sender generating this report */
	DWORD32		ntp_sec;		/* NTP timestamp */
	DWORD32		ntp_frac;
	DWORD32		rtp_ts;			/* RTP timestamp */
	DWORD32		psent;			/* packets sent */
	DWORD32		osent;			/* octets sent */ 
	RTCP_RR_T	rr[1];			/* variable-length list */
} RTCP_SR_T;

typedef struct {
	BYTE		type;			/* type of item (rtcp_sdes_type_t) */
	BYTE		length;			/* length of item (in octets) */
	char		data[1];		/* text, not null-terminated */
} RTCP_SDES_ITEM;

typedef struct {
	RTCP_COMMON				common;		/* common header */
	union {
		RTCP_SR_T			sr;			/* sender report (SR) */

		struct {
			DWORD32			dwSSRC;		/* receiver generating this report */
			RTCP_RR_T		rr[1];		/* variable-length list */
		} rr;							/* reception report (RR) */

		struct {
			DWORD32			dwSSRC;		/* first SSRC/CSRC */
			RTCP_SDES_ITEM	item[1];	/* list of SDES items */
		} sdes;							/* source description (SDES) */

		DWORD32				dwBye;		/* BYE */

		struct {
			DWORD32			dwSSRC;
			DWORD32			dwName;
			DWORD32			dwSN;
			DWORD32			dwLastSN;
			DWORD32			dwTS;
			DWORD32			dwLastTS;
			DWORD32			dwRecvBytes;
			DWORD32			dwRecvPacket;
		} br;

		struct {
			DWORD32			dwSSRC;
			DWORD32			dwName;
			WORD			wFbType;
			WORD			wFbLength;
		} app;
	} r;
} RTCP_PACKET;

/////////////
// 
/////////////
typedef	struct {
	BOOL			m_bFirst;
	DWORD32			m_dwSSRC;
	DWORD32			m_dwSSRC_1;

	// Send
	WORD			m_wSendPT;
	WORD			m_wSendSeq;
	DWORD32			m_dwSendTS;
	DWORD32			m_dwStartSendCTS;
	DWORD32			m_dwStartSendTS;

	// Recv
	WORD			m_wPrevSeq;
	BOOL			m_bMBit;
	DWORD32			m_dwPrevTS;
	DWORD32			m_dwStartTS;
	DWORD32			m_dwSampleRate;
	BOOL			m_bSetStartTS;
	DWORD32			m_dwStartCTS;
	//BOOL			m_bWrappedTS;
	DWORD32			m_dwWrappedCTS;
	DWORD32			m_dwWrappedCnt;
	DWORD32			m_dwPacketCnt;
	DWORD32			m_dwVideoBFrames;

	// RTCP RR
	WORD			m_wStartRecvSeq;
	WORD			m_wLastRecvSeq;
	WORD			m_wLastSeq;
	BOOL			m_bLastMBit;
	WORD			m_wTotalPrevRecvSeq;
	DWORD32			m_dwLastRecvTS;
	DWORD32			m_dwRecvPacket;
	DWORD32			m_dwPrevRecvPacket;
	DWORD32			m_dwRecvByte;
	DWORD32			m_dwRecvTime;
	DWORD32			m_dwRecvSRtime;
	DWORD32			m_dwRTCPTime;
	INT32			m_iJitter;
	INT32			m_iLSR;
	BOOL			m_bBYE;
	DWORD32			m_dwRTCPPeriod;

	// BR
	DWORD32			m_dwBRSeq;

	// DSI
	BYTE			m_TotalQuSize;
	BYTE			m_DSISeq;
	// ONAIR
	DWORD32			m_dwBaseTick;
	DWORD32			m_dwBaseRtptime;

} FrRTPStruct;

INT32 RTCPRRPacket(FrRTPStruct* hRTP, BYTE *pBuf, DWORD32 dwBufDuration, WORD wBufRemain);
INT32 RTCPSRPacket(FrRTPStruct* hRTP, BYTE *pBuf);
INT32 RTCPSDESPacket(FrRTPStruct* hRTP, BYTE *pBuf);
INT32 RTCPBRPacket(FrRTPStruct* hRTP, BYTE *pBuf);
INT32 RTCPAPPPacket(FrRTPStruct* hRTP, BYTE *pBuf, WORD wFbType);

#endif	// _RTP_H_
