/*****************************************************************************
*                                                                            *
*                            Depacket Library								 *
*                                                                            *
*   Copyright (c) 2017 - by DreamToBe, Incoporated. All Rights Reserved.      *
******************************************************************************

    File Name       : DepacketH265.h
    Author(s)       : CHANG, Joonho
    Created         : March 29 2017

    Description     : File API
    Notes           : 

==============================================================================
                      Modification History (Reverse Order)                    
==============================================================================
    Date        Author      Location (variables) / Description
------------------------------------------------------------------------------

------------------------------------------------------------------------------*/

#ifndef _DEPACKETH265_H_
#define _DEPACKETH265_H_

#include "Depacket.h"

// Nal types
#define		BLA_W_LP		16						// BLA
#define		BLA_W_RADL		17
#define		BLA_N_LP		18
#define		IDR_W_RADL		19						// IDR
#define		IDR_N_LP		20
#define		CRA_NUT			21						// CRA
#define		RSV_IRAP_VCL22	22						// IRAP
#define		RSV_IRAP_VCL23	23

#define		VPS_NUT			32						// VPS
#define		SPS_NUT			33						// SPS
#define		PPS_NUT			34						// PPS

// Nal types for RTP Pakcets
#define		H265_PACKET_TYPE_SINGLE					255			// Never to real H265 nals
#define		H265_PACKET_TYPE_AGGREGATION			48
#define		H265_PACKET_TYPE_FRAGMENT				49
#define		H265_PACKET_TYPE_PACI					50

#define		BYTES_FOR_NAL_SIZE							4
#define		MAX_NO_NALS									50

typedef struct {
	BYTE*		m_pBuffer;
	DWORD32		m_dwBufPos;
	DWORD32		m_dwBufSize;					// AU total buffer size

	BYTE*		m_pPayload;
	DWORD32		m_dwPayload;
	DWORD32		m_dwCTSPayload;
	BOOL		m_bMBitPayload;

	DWORD32		m_dwDON;
	DWORD32		m_dwLastNalPos;
	DWORD32		m_dwLastNalHdr;
	DWORD32		m_dwE_bit;						// for FU-A mode
	DWORD32		m_dwCurCTS;
	BOOL		m_bFirst;
	BOOL		m_bMBit;
} DepacketH265Struct, *DepacketH265Handle;

DepacketH265Handle DepacketH265Open(DWORD32 dwBuffSize, DWORD dwDON);
void DepacketH265Init(DepacketH265Handle hDepack);
void DepacketH265Close(DepacketH265Handle hDepack);
BOOL DepacketH265PutData(DepacketH265Handle hDepack, BYTE *pPayload, DWORD32 dwPayload, DWORD32 dwCTS,
						 DWORD32 dwLost, BOOL bSameTS, BOOL bMBit);
BYTE* DepacketH265GetData(DepacketH265Handle hDepack, DWORD32* pdwData, DWORD32* pdwCTS);

#endif	// _DEPACKETH265_H_
