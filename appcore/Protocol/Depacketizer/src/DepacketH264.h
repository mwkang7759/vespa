
#ifndef _DEPACKETH264_H_
#define _DEPACKETH264_H_

#include "Depacket.h"

#define SKT_SPEC_V100
//#define	STARTER_CODE								1

#define PACK_MODE_STAP_A							24
#define PACK_MODE_STAP_B							25
#define PACK_MODE_MTAP16							26
#define PACK_MODE_MTAP24							27
#define PACK_MODE_FU_A								28
#define PACK_MODE_FU_B								29
#ifdef SKT_SPEC_V100
#define PACK_MODE_SKTAP								31
#endif

#define BYTES_FOR_NAL_SIZE							4
#define MAX_NO_NALS									50

typedef struct {
	BYTE*		m_pBuffer;
	DWORD32		m_dwBufPos;
	DWORD32		m_dwBufSize;					// AU total buffer size

	BYTE*		m_pPayload;
	DWORD32		m_dwPayload;
	DWORD32		m_dwCTSPayload;
	BOOL		m_bMBitPayload;

	DWORD32		m_dwLastNalPos;
	DWORD32		m_dwLastNalHdr;
	DWORD32		m_dwE_bit;							// for FU-A mode
	DWORD32		m_dwCurCTS;
	BOOL		m_bFirst;
	BOOL		m_bMBit;
	BOOL		m_bSVC;
} DepacketH264Struct, *DepacketH264Handle;

DepacketH264Handle DepacketH264Open(DWORD32 dwBuffSize, BOOL bSVC);
void DepacketH264Init(DepacketH264Handle hDepack);
void DepacketH264Close(DepacketH264Handle hDepack);
BOOL DepacketH264PutData(DepacketH264Handle hDepack, BYTE *pPayload, DWORD32 dwPayload, DWORD32 dwCTS,
						 DWORD32 dwLost, BOOL bSameTS, BOOL bMBit);
BYTE* DepacketH264GetData(DepacketH264Handle hDepack, DWORD32* pdwData, DWORD32* pdwCTS);

#endif	// _DEPACKETH264_H_
