
#ifndef	_DEPACKET_H_
#define	_DEPACKET_H_

#include "SocketAPI.h"
#include "SystemAPI.h"
#include "TraceAPI.h"
#include "SdpAPI.h"
#include "RtpAPI.h"
#include "UtilAPI.h"
#include "mediainfo.h"

#ifdef	_DEBUG
//#define	PKT_DUMP
//#define	FILE_DUMP
//#define	TXT_DUMP
#endif

typedef	struct
{
	MEDIA_TYPE2	m_eMedia;
	OBJECT_TYPE	m_eObject;
	DWORD32		m_dwSampleRate;
	void*		m_hDepacket;

	DWORD32		m_dwCurCTS;
	DWORD32		m_dwIncCTS;

	BOOL		m_bFirst;
	BOOL		m_bMBit;
	WORD		m_wPrevSeq;
	DWORD32		m_dwPrevTS;

#ifdef	PKT_DUMP
	FILE_HANDLE	m_hPktFile;
#endif
#ifdef	FILE_DUMP
	FILE_HANDLE	m_hFile;
#endif
} FrDepacketStruct;

#include "DepacketAPI.h"

#endif	// _DEPACKET_H_
