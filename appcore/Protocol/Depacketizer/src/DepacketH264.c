
#include "DepacketH264.h"

static BOOL IsIVOP(BYTE* pFrame, int nFrameLen)
{
	BYTE*	tmpptr;
	BOOL	bRet = FALSE;
	int		i;
	
	for (i = 0; i < nFrameLen;)
	{
		tmpptr = pFrame + i;
		if ((*(tmpptr+4) & 0x1F) == 5 || (*(tmpptr+4) & 0x1F) == 7 || (*(tmpptr+4) & 0x1F) == 8)
		{
			bRet = TRUE;
			break;
		}
		else
		{
			DWORD32		dwLen;
			CHAR		c;

			dwLen = ConvByteToDWORD(tmpptr);
			c = *(tmpptr+5);
			i += dwLen + 4;

			// Single Nal and I Slice. This is a walkaround about MarshMallow encoder bug
			if (nFrameLen <= i)
			{
				// when nal is inter
				if ( ((*(tmpptr+4) & 0x1F) == 1) && 
					 ((c&0xF0) == 0xB0) )
				{
					bRet = TRUE;
					break;
				}
			}
		}
	}

	return	bRet;
}

static BOOL IsIVOP_SVC(BYTE* pFrame, int nFrameLen)
{
	BYTE*	tmpptr;
	BOOL	bRet = FALSE;
	int		i;
	
	for (i = 0; i < nFrameLen;)
	{
		tmpptr = pFrame + i;
		if ((*(tmpptr+4) & 0x1F) == 5 || (*(tmpptr+4) & 0x1F) == 21)
		{
			bRet = TRUE;
			break;
		}
		else
		{
			DWORD32 dwLen;

			dwLen = ConvByteToDWORD(tmpptr);
			i += dwLen + 4;
		}
	}

	return	bRet;
}

static VOID copyDWORD32(BYTE *pPtr, DWORD32 dwValue)
{
	pPtr[3] = (dwValue>>24) & 0xFF;
	pPtr[2] = (dwValue>>16) & 0xFF;
	pPtr[1] = (dwValue>> 8) & 0xFF;
	pPtr[0] = (dwValue>> 0) & 0xFF;
}

static BOOL DepacketizeMode_STAP_A(DepacketH264Handle hH264, BYTE* pPayload, DWORD32 dwPayload)
{
	DWORD32	dwNalSize;

	if (dwPayload <= 1)
		return	FALSE;

	pPayload += 1;
	dwPayload -= 1;

	if (hH264->m_dwBufSize < hH264->m_dwBufPos + dwPayload + BYTES_FOR_NAL_SIZE * MAX_NO_NALS)
	{
		hH264->m_dwBufSize = hH264->m_dwBufPos + dwPayload + BYTES_FOR_NAL_SIZE * MAX_NO_NALS;
		hH264->m_pBuffer = (BYTE*)REALLOC(hH264->m_pBuffer, hH264->m_dwBufSize);
		if (!hH264->m_pBuffer)
		{
			hH264->m_dwBufSize = 0;
			return FALSE;
		}
	}

	while ((int)dwPayload > 0)
	{
		hH264->m_dwLastNalPos = hH264->m_dwBufPos;

		dwNalSize = (DWORD32)ntohs(*((WORD*)pPayload));
#ifdef	STARTER_CODE
		copyDWORD32(hH264->m_pBuffer + hH264->m_dwBufPos, ntohl(STARTER_CODE));
#else
		copyDWORD32(hH264->m_pBuffer + hH264->m_dwBufPos, ntohl(dwNalSize));
#endif
		pPayload += 2;
		dwPayload -= 2;
		hH264->m_dwBufPos += BYTES_FOR_NAL_SIZE;

		memcpy(hH264->m_pBuffer + hH264->m_dwBufPos, pPayload, dwNalSize);
		pPayload += dwNalSize;
		dwPayload -= dwNalSize;
		hH264->m_dwBufPos += dwNalSize;
	}

	if ((int)dwPayload < 0)
		hH264->m_dwBufPos = hH264->m_dwLastNalPos;
	hH264->m_dwLastNalHdr = PACK_MODE_STAP_A;

	return	TRUE;
}

static BOOL DepacketizeMode_FU_A(DepacketH264Handle hH264, BYTE* pPayload, DWORD32 dwPayload)
{
	BOOL	bSBit, bEBit;
	DWORD32	dwNalSize;

	if (dwPayload <= 2)
		return	FALSE;

	bEBit = pPayload[1] & 0x40;
	bSBit = pPayload[1] & 0x80;
	pPayload += 2;
	dwPayload -= 2;

	if (hH264->m_dwBufSize < hH264->m_dwBufPos + dwPayload + BYTES_FOR_NAL_SIZE + 1)
	{
		hH264->m_dwBufSize = hH264->m_dwBufPos + dwPayload + BYTES_FOR_NAL_SIZE + 1;
		hH264->m_pBuffer = (BYTE*)REALLOC(hH264->m_pBuffer, hH264->m_dwBufSize);
		if (!hH264->m_pBuffer)
		{
			hH264->m_dwBufSize = 0;
			return FALSE;
		}
	}

	if (bSBit)
	{
		hH264->m_dwLastNalPos = hH264->m_dwBufPos;
#ifdef	STARTER_CODE
		copyDWORD32(hH264->m_pBuffer + hH264->m_dwBufPos, ntohl(STARTER_CODE));
#else
		copyDWORD32(hH264->m_pBuffer + hH264->m_dwBufPos, ntohl(dwPayload));
#endif
		hH264->m_dwBufPos += BYTES_FOR_NAL_SIZE;
		hH264->m_pBuffer[hH264->m_dwBufPos++] = (BYTE)((pPayload[-1] & 0x1F) | (pPayload[-2] & 0xE0));
		hH264->m_dwE_bit = 0;
	}
	else if (bEBit)
	{
		// delete incomplete FU packet
		if (hH264->m_dwLastNalHdr != PACK_MODE_FU_A || hH264->m_dwE_bit == 1)
		{
			hH264->m_dwBufPos = hH264->m_dwLastNalPos;
			return	FALSE;
		}
		dwNalSize = dwPayload + hH264->m_dwBufPos - hH264->m_dwLastNalPos - BYTES_FOR_NAL_SIZE;
#ifdef	STARTER_CODE
		copyDWORD32(hH264->m_pBuffer + hH264->m_dwLastNalPos, ntohl(STARTER_CODE));
#else
		copyDWORD32(hH264->m_pBuffer + hH264->m_dwLastNalPos, ntohl(dwNalSize));
#endif
		hH264->m_dwE_bit = 1;
	}
	else
	{
		// delete incomplete FU packet
		if (hH264->m_dwLastNalHdr != PACK_MODE_FU_A || hH264->m_dwE_bit == 1)
		{
			hH264->m_dwBufPos = hH264->m_dwLastNalPos;
			return	FALSE;
		}
		dwNalSize = dwPayload + hH264->m_dwBufPos - hH264->m_dwLastNalPos - BYTES_FOR_NAL_SIZE;
#ifdef	STARTER_CODE
		copyDWORD32(hH264->m_pBuffer + hH264->m_dwLastNalPos, ntohl(STARTER_CODE));
#else
		copyDWORD32(hH264->m_pBuffer + hH264->m_dwLastNalPos, ntohl(dwNalSize));
#endif
		hH264->m_dwE_bit = 0;
	}

	memcpy(hH264->m_pBuffer + hH264->m_dwBufPos, pPayload, dwPayload);
	hH264->m_dwBufPos += dwPayload;
	hH264->m_dwLastNalHdr = PACK_MODE_FU_A;

	return	TRUE;
}

#ifdef SKT_SPEC_V100
static int DepacketizeMode_SKTAP(DepacketH264Handle hH264, BYTE* pPayload, DWORD32 dwPayload)
{
	DWORD32	dwNalHdr = 0;
	DWORD32	dwNalSize = 0;

	if (dwPayload <= 3)
		return 0;

	// for DON field (+2)
	pPayload += 3;
	dwPayload -= 3;

	if (hH264->m_dwBufSize < hH264->m_dwBufPos + dwPayload + BYTES_FOR_NAL_SIZE * MAX_NO_NALS)
	{
		hH264->m_dwBufSize = hH264->m_dwBufPos + dwPayload + BYTES_FOR_NAL_SIZE * MAX_NO_NALS;
		hH264->m_pBuffer = (BYTE*)REALLOC(hH264->m_pBuffer, hH264->m_dwBufSize);
		if (!hH264->m_pBuffer)
		{
			hH264->m_dwBufSize = 0;
			return FALSE;
		}
	}

	while ((int)dwPayload > 0)
	{
		hH264->m_dwLastNalPos = hH264->m_dwBufPos;

		dwNalSize = (DWORD32)ntohs(*((unsigned short *)pPayload));
		pPayload += 2;
		dwNalHdr = (DWORD32)pPayload[0];
		if ((dwNalHdr & 0x1F) >= PACK_MODE_FU_A)
			break;

#ifdef	STARTER_CODE
		copyDWORD32(hH264->m_pBuffer + hH264->m_dwBufPos, ntohl(STARTER_CODE));
#else
		copyDWORD32(hH264->m_pBuffer + hH264->m_dwBufPos, ntohl(dwNalSize));
#endif
		hH264->m_dwBufPos += BYTES_FOR_NAL_SIZE;

		memcpy(hH264->m_pBuffer + hH264->m_dwBufPos, pPayload, dwNalSize);
		hH264->m_dwBufPos += dwNalSize;
		pPayload += dwNalSize;
		dwPayload -= dwNalSize;
	}

	if ((int)dwPayload < 0)
		hH264->m_dwBufPos = hH264->m_dwLastNalPos;
	hH264->m_dwLastNalHdr = PACK_MODE_SKTAP;

	if ((dwNalHdr & 0x1F) == PACK_MODE_FU_A)
		DepacketizeMode_FU_A(hH264, pPayload, dwNalSize);

	return	TRUE;
}
#endif

DepacketH264Handle DepacketH264Open(DWORD32 dwBuffSize, BOOL bSVC)
{
	DepacketH264Handle	hH264;

	hH264 = (DepacketH264Handle)MALLOCZ(sizeof(DepacketH264Struct));
	if (hH264)
	{
		hH264->m_pBuffer = (BYTE*)MALLOC(dwBuffSize+MAX_RTP_PKT_SIZE);
		if (!hH264->m_pBuffer)
		{
			FREE(hH264);
			return	NULL;
		}
		hH264->m_dwBufPos = 0;
		hH264->m_dwLastNalPos =0;
		hH264->m_dwBufSize = dwBuffSize;
		hH264->m_bFirst = TRUE;
		hH264->m_dwE_bit = 1;
		hH264->m_bSVC = bSVC;
	}

	return	hH264;
}

void DepacketH264Init(DepacketH264Handle hH264)
{
	hH264->m_bFirst = TRUE;
	hH264->m_dwBufPos = 0;
	hH264->m_dwLastNalPos = 0;
	hH264->m_dwLastNalHdr = 0;
	hH264->m_dwE_bit = 1;
}

void DepacketH264Close(DepacketH264Handle hH264)
{
	if (hH264)
	{
		FREE(hH264->m_pBuffer);
		FREE(hH264->m_pPayload);
		FREE(hH264);
	}
}

BOOL DepacketH264PutPayload(DepacketH264Handle hH264, BYTE* pPayload, DWORD32 dwPayload)
{
	DWORD32		dwNalHdr;

	dwNalHdr = (DWORD32)(*pPayload & 0x1F);

	//LOG_E("[Depacket]	DepacketH264PutData Nal type %d", dwNalHdr);

	if (dwNalHdr == 0)
	{
		LOG_E("[Depacket]	DepacketH264PutData - Error : Nal Header is 0!");
		return	FALSE;
	}
	else if (dwNalHdr < PACK_MODE_STAP_A)						// single NAL unit mode
	{
		//LOG_T("[Depacket]	DepacketH264PutData - PACK_MODE_SNAL_A");
		if (hH264->m_dwBufSize < hH264->m_dwBufPos + dwPayload + BYTES_FOR_NAL_SIZE)
		{
			hH264->m_dwBufSize = hH264->m_dwBufPos + dwPayload + BYTES_FOR_NAL_SIZE;
			hH264->m_pBuffer = (BYTE*)REALLOC(hH264->m_pBuffer, hH264->m_dwBufSize);
		}

#ifdef	STARTER_CODE
		copyDWORD32(hH264->m_pBuffer + hH264->m_dwBufPos, ntohl(STARTER_CODE));
#else
		copyDWORD32(hH264->m_pBuffer + hH264->m_dwBufPos, ntohl(dwPayload));
#endif
		hH264->m_dwLastNalPos = hH264->m_dwBufPos;
		hH264->m_dwBufPos += BYTES_FOR_NAL_SIZE;

		memcpy(hH264->m_pBuffer + hH264->m_dwBufPos, pPayload, dwPayload);
		hH264->m_dwBufPos += dwPayload;

		hH264->m_dwLastNalHdr = dwNalHdr;
	}
	else if (dwNalHdr == PACK_MODE_STAP_A)
	{
		//LOG_T("[Depacket]	DepacketH264PutData - PACK_MODE_STAP_A");
		if (!DepacketizeMode_STAP_A(hH264, pPayload, dwPayload))
			return	FALSE;
	}
	else if (dwNalHdr == PACK_MODE_STAP_B)
	{
		LOG_E("[Depacket]	DepacketH264PutData - Error : Unsupport mode(PACK_MODE_STAP_B)");
		return FALSE;
	}
	else if (dwNalHdr == PACK_MODE_MTAP16)
	{
		LOG_E("[Depacket]	DepacketH264PutData - Error : Unsupport mode(PACK_MODE_MTAP16)");
		return FALSE;
	}
	else if (dwNalHdr == PACK_MODE_MTAP24)
	{
		LOG_E("[Depacket]	DepacketH264PutData - Error : Unsupport mode(PACK_MODE_MTAP24)");
		return FALSE;
	}
	else if (dwNalHdr == PACK_MODE_FU_A)
	{
		//TRACE("[Depacket]	DepacketH264PutData - PACK_MODE_FU_A");
		if (!DepacketizeMode_FU_A(hH264, pPayload, dwPayload))
			return	FALSE;
	}
	else if (dwNalHdr == PACK_MODE_FU_B)
	{
		LOG_E("[Depacket]	DepacketH264PutData - Error : Unsupport mode(PACK_MODE_FU_B)");
		return	FALSE;
	}
#ifdef SKT_SPEC_V100
	else if (dwNalHdr == PACK_MODE_SKTAP)
	{
		//TRACE("[Depacket]	DepacketH264PutData - PACK_MODE_SKTAP");
		if (!DepacketizeMode_SKTAP(hH264, pPayload, dwPayload))
			return	FALSE;
	} 
#endif
	else
	{
		LOG_E("[Depacket]	DepacketH264PutData - Error : Unsupport mode(%d)", dwNalHdr);
		return	FALSE;
	}

	return	TRUE;
}

BOOL DepacketH264PutData(DepacketH264Handle hH264, BYTE* pPayload, DWORD32 dwPayload, DWORD32 dwCTS,
						 DWORD32 dwLost, BOOL bSameTS, BOOL bMBit)
{
	BOOL	bRet;

	// Handle a lost rpt packet (whose seq number is not continuous) here
	if (hH264->m_pPayload)
	{
		DepacketH264PutPayload(hH264, hH264->m_pPayload, hH264->m_dwPayload);
		hH264->m_bMBit = hH264->m_bMBitPayload;
		hH264->m_dwCurCTS = hH264->m_dwCTSPayload;

		FREE(hH264->m_pPayload);
		hH264->m_pPayload = NULL;
	}

	if (dwLost)
	{
		// delete incomplete FU packet
		if (hH264->m_dwLastNalHdr == PACK_MODE_FU_A && hH264->m_dwE_bit != 1)
		{
			hH264->m_dwBufPos = hH264->m_dwLastNalPos;
			hH264->m_dwE_bit = 1;
		}

		// Packet loss happened and the current rtp packet does not belong to a current frame.
		// Hence the data received so far is thrown away and store a current rpt packet
		if (!bSameTS)
		{
			hH264->m_pPayload = (BYTE*)MALLOC(dwPayload);
			hH264->m_dwPayload = dwPayload;
			memcpy(hH264->m_pPayload, pPayload, dwPayload);
			hH264->m_dwCTSPayload = dwCTS;
			hH264->m_bMBitPayload = bMBit;
			hH264->m_bMBit = TRUE;
			return	TRUE;
		}
	}

	bRet = DepacketH264PutPayload(hH264, pPayload, dwPayload);
	hH264->m_bMBit = bMBit;
	hH264->m_dwCurCTS = dwCTS;

	return	bRet;
}

BYTE* DepacketH264GetData(DepacketH264Handle hH264, DWORD32* pdwData, DWORD32* pdwCTS)
{
	BYTE*	pData = NULL;
	BOOL	bRet;

	if (!hH264->m_dwBufPos)
	{
		// Handle a lost rpt packet (whose seq number is not continuous) here
		// , which is the last rtp packet received
		if (hH264->m_pPayload)
		{
			DepacketH264PutPayload(hH264, hH264->m_pPayload, hH264->m_dwPayload);
			hH264->m_bMBit = hH264->m_bMBitPayload;
			hH264->m_dwCurCTS = hH264->m_dwCTSPayload;

			FREE(hH264->m_pPayload);
			hH264->m_pPayload = NULL;
		}
	}

	if (hH264->m_bMBit && hH264->m_dwBufPos)
	{
		pData = hH264->m_pBuffer;
		*pdwData = hH264->m_dwBufPos;
		hH264->m_dwBufPos = 0;
		hH264->m_dwLastNalPos = 0;
		hH264->m_dwLastNalHdr = 0;
		hH264->m_bMBit = FALSE;

		if (hH264->m_bFirst)
		{
			if (hH264->m_bSVC)
				bRet = IsIVOP_SVC(pData, *pdwData);
			else
				bRet = IsIVOP(pData, *pdwData);
			if (!bRet)
				return	NULL;
			else
				hH264->m_bFirst = FALSE;
		}

		*pdwCTS = hH264->m_dwCurCTS;
	}

	return	pData;
}
