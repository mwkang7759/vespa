/*****************************************************************************
*                                                                            *
*                            Depacket Library								 *
*                                                                            *
*   Copyright (c) 2017 - by DreamToBe, Incoporated. All Rights Reserved.      *
******************************************************************************

    File Name       : DepacketH265.c
    Author(s)       : CHANG, Joonho
    Created         : March 29 2017

    Description     : File API
    Notes           : 

==============================================================================
                      Modification History (Reverse Order)                    
==============================================================================
    Date        Author      Location (variables) / Description
------------------------------------------------------------------------------

------------------------------------------------------------------------------*/

/*
	ver 1.0.0 
*/
/* 
	Completed (Single NAL unit + Non-Interleaved mode)
	- Single Nal unit mode
	- Single-time aggregation packet mode A
	- Fragmentation unit mode A
	- SKTAP

	Not yet (Interleaved mode + ...)
	- Single-time aggregation packet mode B
	- Multi-time aggregation packet mode 16, 24
	- Fragmentation unit mode B
*/
/*
	AU buffer
	//
	(size(4bytes, BIG endian) + Nal unit) + (size(4bytes) + Nal unit) + ...
*/



#include "DepacketH265.h"

static VOID copyDWORD32(BYTE *pPtr, DWORD32 dwValue)
{
	pPtr[3] = (dwValue>>24) & 0xFF;
	pPtr[2] = (dwValue>>16) & 0xFF;
	pPtr[1] = (dwValue>> 8) & 0xFF;
	pPtr[0] = (dwValue>> 0) & 0xFF;
}

static BOOL DepacketizeMode_Aggregation(DepacketH265Handle hH265, BYTE* pPayload, DWORD32 dwPayload)
{
	DWORD32	dwNalSize;

	if ((hH265->m_dwDON && dwPayload <= 4) || (!hH265->m_dwDON &&dwPayload <= 3))
		return	FALSE;

	pPayload += 2;
	dwPayload -= 2;

	// Skip over DON
	if (hH265->m_dwDON)
	{
		pPayload += 2;
		dwPayload -= 2;
	}

	if (hH265->m_dwBufSize < hH265->m_dwBufPos + dwPayload + BYTES_FOR_NAL_SIZE * MAX_NO_NALS)
	{
		hH265->m_dwBufSize = hH265->m_dwBufPos + dwPayload + BYTES_FOR_NAL_SIZE * MAX_NO_NALS;
		hH265->m_pBuffer = (BYTE*)REALLOC(hH265->m_pBuffer, hH265->m_dwBufSize);
		if (!hH265->m_pBuffer)
		{
			hH265->m_dwBufSize = 0;
			return FALSE;
		}
	}

	while ((int)dwPayload > 0)
	{
		hH265->m_dwLastNalPos = hH265->m_dwBufPos;

		// Skip over DON
		if (hH265->m_dwDON)
		{
			pPayload += 2;
			dwPayload -= 2;
		}

		dwNalSize = (DWORD32)ntohs(*((WORD*)pPayload));
#ifdef	STARTER_CODE
		copyDWORD32(hH265->m_pBuffer + hH265->m_dwBufPos, ntohl(STARTER_CODE));
#else
		copyDWORD32(hH265->m_pBuffer + hH265->m_dwBufPos, ntohl(dwNalSize));
#endif
		// Nal size
		pPayload += 2;
		dwPayload -= 2;
		hH265->m_dwBufPos += BYTES_FOR_NAL_SIZE;

		memcpy(hH265->m_pBuffer + hH265->m_dwBufPos, pPayload, dwNalSize);
		pPayload += dwNalSize;
		dwPayload -= dwNalSize;
		hH265->m_dwBufPos += dwNalSize;
	}

	if ((int)dwPayload < 0)
		hH265->m_dwBufPos = hH265->m_dwLastNalPos;
	hH265->m_dwLastNalHdr = H265_PACKET_TYPE_SINGLE;

	return	TRUE;
}

static BOOL DepacketizeMode_Fragment(DepacketH265Handle hH265, BYTE* pPayload, DWORD32 dwPayload)
{
	BOOL		bSBit, bEBit;
	DWORD32		dwNalSize;
	BYTE		cFuType;

	if ((hH265->m_dwDON && dwPayload <= 5) || (!hH265->m_dwDON &&dwPayload <= 3))
		return	FALSE;

	bEBit = pPayload[2] & 0x40;
	bSBit = pPayload[2] & 0x80;
	cFuType = pPayload[2] & 0x3F;

	pPayload += 3;
	dwPayload -= 3;

	if (hH265->m_dwBufSize < hH265->m_dwBufPos + dwPayload + BYTES_FOR_NAL_SIZE + 1)
	{
		hH265->m_dwBufSize = 2*(hH265->m_dwBufPos + dwPayload);
		hH265->m_pBuffer = (BYTE*)REALLOC(hH265->m_pBuffer, hH265->m_dwBufSize);
		if (!hH265->m_pBuffer)
		{
			hH265->m_dwBufSize = 0;
			return FALSE;
		}
	}

	if (bSBit)
	{
		hH265->m_dwLastNalPos = hH265->m_dwBufPos;
#ifdef	STARTER_CODE
		copyDWORD32(hH265->m_pBuffer + hH265->m_dwBufPos, ntohl(STARTER_CODE));
#else
		copyDWORD32(hH265->m_pBuffer + hH265->m_dwBufPos, ntohl(dwPayload));
#endif
		hH265->m_dwBufPos += BYTES_FOR_NAL_SIZE;
		hH265->m_pBuffer[hH265->m_dwBufPos++] = (BYTE)((pPayload[-3] & 0x81) | (cFuType<<1));
		hH265->m_pBuffer[hH265->m_dwBufPos++] = pPayload[-2];				// 2nd byte of nal header
		hH265->m_dwE_bit = 0;

		if (hH265->m_dwDON)
		{
			// Skip over DON (2B)
			pPayload += 2;
			dwPayload -= 2;
		}
	}
	else if (bEBit)
	{
		// delete incomplete FU packet
		if (hH265->m_dwLastNalHdr != H265_PACKET_TYPE_FRAGMENT || hH265->m_dwE_bit == 1)
		{
			hH265->m_dwBufPos = hH265->m_dwLastNalPos;
			return	FALSE;
		}

		if (hH265->m_dwDON)
		{
			// Skip over DON (2B)
			pPayload += 2;
			dwPayload -= 2;
		}

		dwNalSize = dwPayload + hH265->m_dwBufPos - hH265->m_dwLastNalPos - BYTES_FOR_NAL_SIZE;
#ifdef	STARTER_CODE
		copyDWORD32(hH265->m_pBuffer + hH265->m_dwLastNalPos, ntohl(STARTER_CODE));
#else
		copyDWORD32(hH265->m_pBuffer + hH265->m_dwLastNalPos, ntohl(dwNalSize));
#endif
		hH265->m_dwE_bit = 1;
	}
	else
	{
		// delete incomplete FU packet
		if (hH265->m_dwLastNalHdr != H265_PACKET_TYPE_FRAGMENT || hH265->m_dwE_bit == 1)
		{

			LOG_E("[Depacket] DepacketizeMode_Fragment - FU incomplete");
			hH265->m_dwBufPos = hH265->m_dwLastNalPos;
			return	FALSE;
		}

		if (hH265->m_dwDON)
		{
			// Skip over DON (2B)
			pPayload += 2;
			dwPayload -= 2;
		}

		dwNalSize = dwPayload + hH265->m_dwBufPos - hH265->m_dwLastNalPos - BYTES_FOR_NAL_SIZE;
#ifdef	STARTER_CODE
		copyDWORD32(hH265->m_pBuffer + hH265->m_dwLastNalPos, ntohl(STARTER_CODE));
#else
		copyDWORD32(hH265->m_pBuffer + hH265->m_dwLastNalPos, ntohl(dwNalSize));
#endif
		hH265->m_dwE_bit = 0;
	}

	memcpy(hH265->m_pBuffer + hH265->m_dwBufPos, pPayload, dwPayload);
	hH265->m_dwBufPos += dwPayload;
	hH265->m_dwLastNalHdr = H265_PACKET_TYPE_FRAGMENT;

	return	TRUE;
}


DepacketH265Handle DepacketH265Open(DWORD32 dwBuffSize, DWORD dwDON)
{
	DepacketH265Handle	hH265;

	hH265 = (DepacketH265Handle)MALLOCZ(sizeof(DepacketH265Struct));
	if (hH265)
	{
		hH265->m_pBuffer = (BYTE*)MALLOC(dwBuffSize+MAX_RTP_PKT_SIZE);
		if (!hH265->m_pBuffer)
		{
			FREE(hH265);
			return	NULL;
		}
		hH265->m_dwBufPos = 0;
		hH265->m_dwLastNalPos =0;
		hH265->m_dwBufSize = dwBuffSize;
		hH265->m_dwDON = dwDON;
		hH265->m_bFirst = TRUE;
		hH265->m_dwE_bit = 1;
	}

	return	hH265;
}

void DepacketH265Init(DepacketH265Handle hH265)
{
	hH265->m_bFirst = TRUE;
	hH265->m_dwBufPos = 0;
	hH265->m_dwLastNalPos = 0;
	hH265->m_dwLastNalHdr = 0;
	hH265->m_dwE_bit = 1;
}

void DepacketH265Close(DepacketH265Handle hH265)
{
	if (hH265)
	{
		FREE(hH265->m_pBuffer);
		FREE(hH265->m_pPayload);
		FREE(hH265);
	}
}

BOOL DepacketH265PutPayload(DepacketH265Handle hH265, BYTE* pPayload, DWORD32 dwPayload)
{
	DWORD32		dwNalHdr;

	dwNalHdr = (DWORD32)( (*pPayload & 0x7E) >> 1);

	//LOG_E("[Depacket]	DepacketH265PutData Nal type %d", dwNalHdr);

	if (dwNalHdr < H265_PACKET_TYPE_AGGREGATION)		// single NAL unit mode
	{
		if (!hH265->m_bFirst && H265_NAL_VPS <= dwNalHdr && dwNalHdr <= H265_NAL_PPS && 0)
		{
			//LOG_T("[Depacket]	DepacketH265PutData - In-band vps/sps/pps recevied");
					
			hH265->m_dwBufPos = 0;
			hH265->m_dwLastNalPos = 0;
			hH265->m_dwLastNalHdr = 0;
			hH265->m_bMBit = FALSE;
			hH265->m_dwE_bit = FALSE;

			return TRUE;
		}

		//LOG_T("[Depacket]	DepacketH265PutData - PACK_MODE_SNAL_A");
		if (hH265->m_dwBufSize < hH265->m_dwBufPos + dwPayload + BYTES_FOR_NAL_SIZE)
		{
			hH265->m_dwBufSize = 2* (hH265->m_dwBufPos + dwPayload);
			hH265->m_pBuffer = (BYTE*)REALLOC(hH265->m_pBuffer, hH265->m_dwBufSize);
		}

#ifdef	STARTER_CODE
		copyDWORD32(hH265->m_pBuffer + hH265->m_dwBufPos, ntohl(STARTER_CODE));
#else
		copyDWORD32(hH265->m_pBuffer + hH265->m_dwBufPos, ntohl(dwPayload));
#endif
		hH265->m_dwLastNalPos = hH265->m_dwBufPos;
		hH265->m_dwBufPos += BYTES_FOR_NAL_SIZE;

		if (hH265->m_dwDON)
		{
			if (4 < dwPayload)
			{
				// PayloadHdr (2B)
				memcpy(hH265->m_pBuffer + hH265->m_dwBufPos, pPayload, 2);
				hH265->m_dwBufPos += 2;
				pPayload += 2;
				dwPayload -= 2;
				// Skip over DON
				pPayload += 2;
				dwPayload -= 2;
				// Nal unit payload data
				memcpy(hH265->m_pBuffer + hH265->m_dwBufPos, pPayload, dwPayload);
			}
			else
				dwPayload = 0;
		}
			
		memcpy(hH265->m_pBuffer + hH265->m_dwBufPos, pPayload, dwPayload);
		hH265->m_dwBufPos += dwPayload;
		hH265->m_dwLastNalHdr = dwNalHdr;
	}
	else if (dwNalHdr == H265_PACKET_TYPE_AGGREGATION)
	{
		//LOG_T("[Depacket]	DepacketH265PutData - H265_PACKET_TYPE_AGGREGATION");
		if (!DepacketizeMode_Aggregation(hH265, pPayload, dwPayload))
			return	FALSE;
	}
	else if (dwNalHdr == H265_PACKET_TYPE_FRAGMENT)
	{
		//TRACE("[Depacket]	DepacketH265PutData - H265_PACKET_TYPE_FRAGMENT");
		if (!DepacketizeMode_Fragment(hH265, pPayload, dwPayload))
			return	FALSE;
	}
	else if (dwNalHdr == H265_PACKET_TYPE_PACI)
	{
		LOG_E("[Depacket]	DepacketH265PutData - Error : Unsupport mode(H265_PACKET_TYPE_PACI)");
		return	FALSE;
	}
	else
	{
		LOG_E("[Depacket]	DepacketH265PutData - Error : Unsupport mode(%d)", dwNalHdr);
		return	FALSE;
	}

	return	TRUE;
}

BOOL DepacketH265PutData(DepacketH265Handle hH265, BYTE* pPayload, DWORD32 dwPayload, DWORD32 dwCTS,
						 DWORD32 dwLost, BOOL bSameTS, BOOL bMBit)
{
	BOOL	bRet;

	if (hH265->m_pPayload)
	{
		//TRACE("Ts %5d", dwCTS);
		DepacketH265PutPayload(hH265, hH265->m_pPayload, hH265->m_dwPayload);
		hH265->m_bMBit = hH265->m_bMBitPayload;
		hH265->m_dwCurCTS = hH265->m_dwCTSPayload;

		FREE(hH265->m_pPayload);
		hH265->m_pPayload = NULL;
	}

	if (dwLost)
	{
		// delete incomplete FU packet
		if (hH265->m_dwLastNalHdr == H265_PACKET_TYPE_FRAGMENT && hH265->m_dwE_bit != 1)
		{
			hH265->m_dwBufPos = hH265->m_dwLastNalPos;
			hH265->m_dwE_bit = 1;
		}

		if (!bSameTS)
		{
			hH265->m_pPayload = (BYTE*)MALLOC(dwPayload);
			hH265->m_dwPayload = dwPayload;
			memcpy(hH265->m_pPayload, pPayload, dwPayload);
			hH265->m_dwCTSPayload = dwCTS;
			hH265->m_bMBitPayload = bMBit;
			hH265->m_bMBit = TRUE;
			return	TRUE;
		}
	}

	bRet = DepacketH265PutPayload(hH265, pPayload, dwPayload);
	hH265->m_bMBit = bMBit;
	hH265->m_dwCurCTS = dwCTS;

	return	bRet;
}

BYTE* DepacketH265GetData(DepacketH265Handle hH265, DWORD32* pdwData, DWORD32* pdwCTS)
{
	BYTE*	pData = NULL;
	BOOL	bRet;

	if (!hH265->m_dwBufPos)
	{
		if (hH265->m_pPayload)
		{
			DepacketH265PutPayload(hH265, hH265->m_pPayload, hH265->m_dwPayload);
			hH265->m_bMBit = hH265->m_bMBitPayload;
			hH265->m_dwCurCTS = hH265->m_dwCTSPayload;

			FREE(hH265->m_pPayload);
			hH265->m_pPayload = NULL;
		}
	}

	if (hH265->m_bMBit && hH265->m_dwBufPos)
	{
		pData = hH265->m_pBuffer;
		*pdwData = hH265->m_dwBufPos;
		hH265->m_dwBufPos = 0;
		hH265->m_dwLastNalPos = 0;
		hH265->m_dwLastNalHdr = 0;
		hH265->m_bMBit = FALSE;

		if (hH265->m_bFirst)
		{
			bRet = is_a_h265_frame_intra(pData, *pdwData);
			if (!bRet)
			{
                LOG_I("[DepacketH265GetData] need to skip because not a intra frame (cts:%d)", hH265->m_dwCurCTS);
				return	NULL;
			}
			else
			{
                LOG_I("[DepacketH265GetData] found a intra frame (cts:%d)", hH265->m_dwCurCTS);
				hH265->m_bFirst = FALSE;
			}
		}

		*pdwCTS = hH265->m_dwCurCTS;
	}

	return	pData;
}
