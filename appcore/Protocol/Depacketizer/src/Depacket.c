
#include "Depacket.h"
#include "DepacketH264.h"

FrDepacketHandle FrDepacketOpen(FrMediaHandle hMedia) {
	FrDepacketStruct*	hDepacket;
	BYTE*				pConfig;
	DWORD32				dwConfig;
	DWORD32				dwBuffSize;

	hDepacket = (FrDepacketStruct*)MALLOCZ(sizeof(FrDepacketStruct));
	if (hDepacket) {
		hDepacket->m_bFirst = TRUE;
		hDepacket->m_eObject = FrSDPGetMediaObjectType(hMedia);
		hDepacket->m_dwSampleRate = FrSDPGetMediaSampleRate(hMedia);
		hDepacket->m_eMedia = FrSDPGetMediaType(hMedia);

		pConfig = FrSDPGetMediaConfig(hMedia);
		dwConfig = FrSDPGetMediaConfigLen(hMedia);

		switch (hDepacket->m_eMedia)
		{
		case MI_VIDEO :
			dwBuffSize = FrSDPGetMediaVideoWidth(hMedia)*FrSDPGetMediaVideoHeight(hMedia);
			dwBuffSize /= 10;
			if (!dwBuffSize)
				dwBuffSize = 4096 * 4096 / 10;
			hDepacket->m_dwIncCTS = 0;
			break;
		default :
			dwBuffSize = MAX_RTP_PKT_SIZE;
			break;
		}

		switch (hDepacket->m_eObject)
		{
		case H264_OBJECT:
			dwBuffSize = MAX_RTP_PKT_SIZE;
			hDepacket->m_hDepacket = DepacketH264Open(dwBuffSize, FALSE);
#ifdef	PKT_DUMP
			hDepacket->m_hPktFile = FILEDUMPOPEN("c:\\video_depkt", "pkt", (DWORD32)hDepacket);
#endif
#ifdef	FILE_DUMP
			hDepacket->m_hFile = FILEDUMPOPEN("c:\\video_depkt", "h264", (DWORD32)hDepacket);
#endif
			break;
		
		default:
			FREE(hDepacket);
			return	NULL;
		}
	}

	return	hDepacket;
}

BOOL FrDepacketInit(FrDepacketHandle hHandle) {
	FrDepacketStruct*	hDepacket = (FrDepacketStruct*)hHandle;

	if (hDepacket) {
		switch (hDepacket->m_eObject)
		{
		case H264_OBJECT:
		case SVC_OBJECT:
			DepacketH264Init((DepacketH264Handle)hDepacket->m_hDepacket);
			break;
		
		default:
			break;
		}
		hDepacket->m_dwCurCTS = 0;
		hDepacket->m_bFirst = TRUE;
	}

	return	TRUE;
}

void FrDepacketClose(FrDepacketHandle hHandle) {
	FrDepacketStruct*	hDepacket = (FrDepacketStruct*)hHandle;
	LOG_I("DepacketClose Begin..");
	if (hDepacket) {
		switch (hDepacket->m_eObject)
		{
		case H264_OBJECT:
		case SVC_OBJECT:
			DepacketH264Close((DepacketH264Handle)hDepacket->m_hDepacket);
			break;

		default:
			break;
		}
#ifdef	PKT_DUMP
		FrCloseFile(hDepacket->m_hPktFile);
#endif
#ifdef	FILE_DUMP
		FrCloseFile(hDepacket->m_hFile);
#endif

		FREE(hDepacket);
		LOG_I("DepacketClose End..");
	}
}

void FrDepacketPutData(FrDepacketHandle hHandle, BYTE* pPacket, DWORD32 dwPacket, DWORD32 dwCTS) {
	FrDepacketStruct*	hDepacket = (FrDepacketStruct*)hHandle;
	RTP_HEADER*			pRtpHdr = (RTP_HEADER*)pPacket;
	BYTE*				pPayload;
	DWORD32				dwPayload, dwHeaderSize, dwTS, dwLost;
	WORD				wSeq, wPT;
	BOOL				bSameTS;
	BOOL				bExt;

	hDepacket->m_bMBit = (pPacket[1] & 0x80) >> 7;
	wPT = (pPacket[1] & 0x7f);
	dwHeaderSize = ((pPacket[0] & 0x0F) + 3) * 4;
	wSeq = ntohs(pRtpHdr->wSeq);
	dwTS = ntohl(pRtpHdr->dwTimestamp);
	bExt = pPacket[0] & 0x10;

	//if (hDepacket->m_eMedia==MI_VIDEO)
	//	LOG_D("[Depacket]	Put(%d) - Seq=%d ts=%d m=%d", wPT, wSeq, dwTS, hDepacket->m_bMBit);

	if (!hDepacket->m_bFirst) {
		if (hDepacket->m_wPrevSeq < wSeq)
			dwLost = wSeq - hDepacket->m_wPrevSeq - 1;
		else
			dwLost = MAX_WORD - hDepacket->m_wPrevSeq + wSeq;

		if (dwLost > MAX_WORD/2) {
			LOG_W("DepacketPutData: late packet dwLost=%d Prev=%d Seq=%d", dwLost, hDepacket->m_wPrevSeq, wSeq);
			return;
		}
	}
	else
		dwLost = 0;
//#ifdef	_DEBUG
	if (dwLost && !hDepacket->m_bFirst) {
		if (hDepacket->m_eMedia==MI_VIDEO)
			LOG_W("DepacketPutData	video - loss packet num=%d Prev=%d Seq=%d", dwLost, hDepacket->m_wPrevSeq, wSeq);
		else
			LOG_W("DepacketPutData	audio - loss packet num=%d Prev=%d Seq=%d", dwLost, hDepacket->m_wPrevSeq, wSeq);
	}
//#endif

	if (hDepacket->m_dwPrevTS == dwTS && !hDepacket->m_bFirst)
		bSameTS = TRUE;
	else
		bSameTS = FALSE;

	hDepacket->m_bFirst = FALSE;
	hDepacket->m_wPrevSeq = wSeq;
	hDepacket->m_dwPrevTS = dwTS;

	// Extension
	if (bExt) {
		WORD	wLength;
		wLength = *(pPacket + dwHeaderSize + 2) ;
		wLength = (wLength<<8) + (*(pPacket + dwHeaderSize + 3));
		dwHeaderSize += 4 * (1+wLength);
		//LOG_I("DepacketPutData Extension is exist..");
	}

	pPayload = pPacket + dwHeaderSize;
	dwPayload = dwPacket - dwHeaderSize;

#ifdef	PKT_DUMP
#ifdef	TXT_DUMP
	FPRINTF(hDepacket->m_hPktFile, "[PACKET] Size=%d\n", dwPacket);
	FPRINTF(hDepacket->m_hPktFile, "	[RTP Header] Seq=%d TS=%u\n", wSeq, dwTS);
	FILEDUMP(hDepacket->m_hPktFile, pPacket, dwHeaderSize);
	FPRINTF(hDepacket->m_hPktFile, "	[Payload]\n");
	FILEDUMP(hDepacket->m_hPktFile, pPacket+dwHeaderSize, dwPacket-dwHeaderSize);
#else
	FrWriteFile(hDepacket->m_hPktFile, pPacket, dwPacket);
#endif
#endif
	switch (hDepacket->m_eObject)
	{
	case H264_OBJECT:
	case SVC_OBJECT:
		if (!DepacketH264PutData((DepacketH264Handle)hDepacket->m_hDepacket, pPayload, dwPayload, dwCTS,
					dwLost, bSameTS, hDepacket->m_bMBit)) {
			hDepacket->m_bMBit = FALSE;
			return;
		}
		break;
	
	default:
		break;
	}

	hDepacket->m_dwCurCTS = dwCTS;
}

BYTE* FrDepacketGetData(FrDepacketHandle hHandle, DWORD32* pdwData, DWORD32* dwCTS) {
	FrDepacketStruct*	hDepacket = (FrDepacketStruct*)hHandle;
	BYTE*				pData = NULL;

	*pdwData = 0;

	switch (hDepacket->m_eObject)
	{
	case H264_OBJECT:
	case SVC_OBJECT:
		pData = DepacketH264GetData((DepacketH264Handle)hDepacket->m_hDepacket, pdwData, &hDepacket->m_dwCurCTS);
		break;
	
	default:
		break;
	}
	*dwCTS = hDepacket->m_dwCurCTS;
	hDepacket->m_dwCurCTS += hDepacket->m_dwIncCTS;

#ifdef	FILE_DUMP
	if (pData)
	{
#ifdef	TXT_DUMP
		FPRINTF(hDepacket->m_hFile, "[FRAME] Size=%d\n", *pdwData);
		FILEDUMP(hDepacket->m_hFile, pData, *pdwData);
#else
		FrWriteFile(hDepacket->m_hFile, pData, *pdwData);
#endif
	}
#endif

	return	pData;
}

void FrDepacketGetPacketInfo(FrDepacketHandle hHandle, DWORD32* pdwPacketNum, DWORD32* pdwPackeLen) {
	FrDepacketStruct*	hDepacket = (FrDepacketStruct*)hHandle;

	switch (hDepacket->m_eObject)
	{
	case VCO_OBJECT:
		//DepacketVcoGetPacketInfo((DepacketVcoHandle)hDepacket->m_hDepacket, pdwPacketNum, pdwPackeLen);
		break;
	}
}
